<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'Crypt/AESMYSQL.php';
class requestdu_IndexController extends Application_Main
{
	

	public function indexAction()
	{

		$this->_helper->layout()->setLayout('newlayout');

		$filter_clear 		= $this->_getParam('clearfilter');
		
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr = $CustomerUser->getAccounts();
		$this->view->AccArr = $AccArr;
		
		$transstatus = $this->_transferstatus;
		$transstatuscode = array_flip($transstatus['code']);
		$statusarr = array_combine(array_values($this->_transferstatus['code']),array_values($this->_transferstatus['desc']));
		
		$paymenttype = $this->_paymenttype;

		$arrPayType  = Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
		$this->view->arrPayType = $arrPayType;
		$typearr = array_combine(array_values($this->_paymenttype['code']),array_values($this->_paymenttype['desc']));
		
		$tratypearr = array_combine(array_values($this->_transfertype['code']),array_values($this->_transfertype['desc']));

        $select2 = $this->_db->select()
					        ->from('T_UNDERLYING')
                            ->where('DOC_STATUS =?', '1');


        $fields = array(
            'COMPANY'      	=> array('field' => 'COMPANY',
                                        'label' => $this->language->_('Company '),
                                        ),
            'DOC_ID'      	=> array('field' => 'DOC_ID',
                                        'label' => $this->language->_('Document ID '),
                                        ),
            'CCY'      	=> array('field' => 'CCY',
                                        'label' => $this->language->_('Currency '),
                                        ),
            'AMOUNT'      	=> array('field' => 'AMOUNT',
                                      'label' => $this->language->_('Amount '),
                                      ),
            'PAY_DATE'  	=> array('field' => 'PAY_DATE',
                                        'label' => $this->language->_('Payment Date '),
                                        ),
            'PAYMENT_TYPE'     		=> array('field' => 'PAYMENT_TYPE',
                                      'label' => $this->language->_('Payment Type '),
                                      ),
            'DOWNLOAD'  	=> array('field' => 'DOWNLOAD',
                                        'label' => $this->language->_('Download '),
                                        ),
            
          );

          $this->view->fields = $fields;
          

          $select3 = $this->_db->select()
                            ->from('T_UNDERLYING');
                            
        

        $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
        $password = $sessionNamespace->token; 
        $this->view->token = $sessionNamespace->token;
        $AESMYSQL = new Crypt_AESMYSQL();
        $PS_NUMBER      = urldecode($this->_getParam('DOC_ID'));
        $FILE_ID = $AESMYSQL->decrypt($PS_NUMBER, $password);

		if($FILE_ID)
		{
			$select3->where('DOC_ID =?',$FILE_ID);
			$data = $this->_db->fetchRow($select3);
			$attahmentDestination = UPLOAD_PATH . '/document/submit/';
			$this->_helper->download->file($data['DOC_NAME'],$attahmentDestination.$data['DOC_SYSNAME']);
		}

        $this->paging($select2);

        if($this->_request->isPost() )
        {
			
			$psNumberArr = $this->_getParam('change_id');
			$notes = $this->_getParam('change_note');
			
			//Zend_Debug::dump($this->_getParam('change_id')); die;
			
			if(count($psNumberArr) > 0)
			{			
			
				if($this->_getParam('deal'))
				{	
				          
					foreach($psNumberArr as $key=>$row){						
						try 
						{   
                            $this->_db->beginTransaction();                  
                            $data = array (

                                'DOC_LASTUPDATED' => new Zend_Db_Expr("now()"),
                                'DOC_LASTUPDATEDBY' => $this->_userIdLogin,
                                'DOC_APPROVED' => new Zend_Db_Expr("now()"),
                                'DOC_APPROVEDBY' => $this->_userIdLogin,
                                'NOTES' => $notes,
                                'DOC_STATUS' => '2'
                            );
                            $where['DOC_ID = ?'] = $row;
                            $this->_db->update('T_UNDERLYING',$data,$where);
							$this->_db->commit();
						}
						catch(Exception $e) 
						{
							$this->_db->rollBack();
                            var_dump($e);die;
							
						}
				    }     
				    $this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
					$this->_redirect('/notification/success');					
					
					
				}
                
                if($this->_getParam('reject'))
				{
					
					foreach($psNumberArr as $key=>$row){						
						try 
						{   
                            $this->_db->beginTransaction();                   
                            $data = array (

                                'DOC_LASTUPDATED' => new Zend_Db_Expr("now()"),
                                'DOC_LASTUPDATEDBY' => $this->_userIdLogin,
                                'NOTES' => $notes,
                                'DOC_STATUS' => '4'
                            );
                            $where['DOC_ID = ?'] = $row;
                            $this->_db->update('T_UNDERLYING',$data,$where);
							$this->_db->commit();
						}
						catch(Exception $e) 
						{
							$this->_db->rollBack();
                            var_dump($e);die;
							
						}
				    }     
				    $this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
					$this->_redirect('/notification/success');		
					
				}
			
			}
			
		}
    }

}