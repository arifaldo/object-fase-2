<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

ini_set("display_errors","Off");

class customer_ViewController extends customer_Model_Customer
{
  public function indexAction()
  {
    $this->_helper->layout()->setLayout('newlayout');

    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
    $password = $sessionNamespace->token; 
    $this->view->token = $sessionNamespace->token;


    $AESMYSQL = new Crypt_AESMYSQL();
    $cust_id = $AESMYSQL->decrypt($this->_getParam('cust_id'), $password);
    $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;

    $this->view->countryArr = Application_Helper_Array::listArray($this->getCountry(),'COUNTRY_CODE','COUNTRY_NAME');

    //pengaturan url untuk button back 
    $this->setbackURL('/'.$this->_request->getModuleName().'/view/index/cust_id/'.$this->_getParam('cust_id'));

    $setting = new Settings();         
    $tokenType = $setting->getSetting('tokentype');

    $system_type1 = $setting->getSetting('system_type');
    $this->view->system_type1 = $system_type1;

    $tokenTypeCode = array_flip($this->_tokenType['code']);
    $tokenTypeDesc = $this->_tokenType['desc'];

    $this->view->tokenTypeText = $tokenTypeDesc[$tokenTypeCode[$tokenType]];

    $globalchanges = $this->getGlobalchange($cust_id);

    $globalchangesboundry = $this->getGlobalchangeBoundary($cust_id);

    $this->view->hidematrix = 1;
    if(!empty($globalchangesboundry)){
      $this->view->hidematrix = 0;
    }
    // var_dump($globalchanges);die;
    if($this->_getParam('error') == 'global'){
      $this->view->showwarning = true;
    }

    $destination = LIBRARY_PATH.'/data/temp/';

    if($cust_id){
      $custModelArr = [
        '1' => $this->language->_('Applicant'),
        '2' => $this->language->_('Insurance'),
        '3' => $this->language->_('Special Obligee')
      ];
      $this->view->custModelArr = $custModelArr;

      $this->view->custTypeArr = Application_Helper_Array::listArray($this->getCompanyType(),'COMPANY_TYPE_CODE','COMPANY_TYPE_DESC');
      $this->view->businessEntityArr = Application_Helper_Array::listArray($this->getBusinessEntity(),'BUSINESS_ENTITY_CODE','BUSINESS_ENTITY_DESC');
      $this->view->debiturCodeArr = Application_Helper_Array::listArray($this->getDebitur(),'DEBITUR_CODE','DEBITUR_DESC');
      $this->view->collectCodeArr = Application_Helper_Array::listArray($this->getCreditQuality(),'CODE','DESCRIPTION');
      $this->view->cityArr = Application_Helper_Array::listArray($this->getCity(),'CITY_CODE','CITY_NAME');

      $resultdata = $this->getCustomer($cust_id);

      if($resultdata['CUST_ID']){ 
        $this->view->cust_id      = strtoupper($resultdata['CUST_ID']);
        $this->view->cust_name    = $resultdata['CUST_NAME'];
        $this->view->cust_securities    = $resultdata['CUST_SECURITIES'];
        $this->view->cust_code    = $resultdata['CUST_CODE'];
        $this->view->cust_cif     = $resultdata['CUST_CIF'];
        $this->view->cust_npwp = $resultdata["CUST_NPWP"];
        $this->view->cust_model    = $resultdata['CUST_MODEL'];

        if($resultdata['CUST_MODEL'] == '2'){
          $this->view->InsBranch = $this->getInsuranceBranch($cust_id);
        }

        if($resultdata['CUST_MODEL'] == '3'){
         $model                              = new specialobligee_Model_Specialobligee();
         $filterParam = array();
         $filterParam['fCompanyCode'] = $resultdata['CUST_ID'];
         $dataSp                               = $model->getData($filterParam,true);
			//var_dump($dataSp);die;
         $this->view->dataSp                   = $dataSp['0'];

         $select = $this->_db->select()
         ->from(
           array('A' => 'M_PRELIMINARY'),
           array('*')
         )
         ->where('A.CUST_ID = ?', $resultdata['CUST_ID']);
         $datapre = $this->_db->fetchRow($select);

         $datapre['CUST_NAME'] = $resultdata['CUST_NAME'];
         $this->view->datapre = $datapre;


         $select = $this->_db->select()
         ->from(
           array('A' => 'M_PRELIMINARY_MEMBER'),
           array('*')
         )
         ->where('A.CUST_ID = ?', $resultdata['CUST_ID']);
         $dataMember = $this->_db->fetchAll($select);
         $this->view->dataMember = $dataMember;

         $getCustAcctById                    = $model->getCustAcctById($resultdata['CUST_ID']);
         $debitedAcctArr                     = [];
         foreach ($getCustAcctById as $row) {
           $debitedAcctArr[$row['ACCT_NO']]   = $row['ACCT_NO'].' ('.$row['CCY_ID'].')'.' / '.$row['ACCT_NAME'].' / '.$row['ACCT_DESC'];
         }
         $this->view->debitedAcctArr         = $debitedAcctArr;

       }

       if($resultdata['CUST_MODEL'] == '1' || $resultdata['CUST_MODEL'] == '2'){
         $model = new linefacility_Model_Linefacility();

         $data = $model->getDataByCustId($resultdata['CUST_ID']);
         $this->view->data = $data;

         $limit_available = (float)$data['PLAFOND_LIMIT'] - (float)$data['LIMIT_USED'];
         $this->view->limit_available = $limit_available;

         $segmentationArr = [
          1 => 'CORPORATE',
          2 => 'COMMERCIAL',
          3 => 'SME',
          4 => 'FINANCIAL INSTITUTION'
        ];
        $this->view->segmentationArr = $segmentationArr;

        $getCreditQuality = $model->getCreditQuality();
        $creditArr = Application_Helper_Array::listArray($getCreditQuality,'CODE','DESCRIPTION');
        $this->view->creditArr = $creditArr;

        $getTempLinefacility = $model->getTempLinefacility($cust_id);
        if($getTempLinefacility){
          $this->view->is_update = true;
        }

        $config = Zend_Registry::get('config');

        $entityCode = $config["bg"]["entity"]["code"];
        $entityDesc = $config["bg"]["entity"]["desc"];
        $entityArr  = array_combine(array_values($entityCode),array_values($entityDesc));
        $this->view->entityArr = $entityArr;

        $citizenshipCode = $config["bg"]["citizen"]["code"];
        $citizenshipDesc = $config["bg"]["citizen"]["desc"];
        $citizenshipArr  = array_combine(array_values($citizenshipCode),array_values($citizenshipDesc));
        $this->view->citizenshipArr = $citizenshipArr;

        $statusCode = $config["bg"]["statusowner"]["code"];
        $statusDesc = $config["bg"]["statusowner"]["desc"];
        $statusArr  = array_combine(array_values($statusCode),array_values($statusDesc));
        $this->view->statusArr = $statusArr;


        $select = $this->_db->select()
        ->from(
          array('A' => 'M_POSITION'),
          array('*')
        );
        $position = $this->_db->fetchAll($select);
        $positionArr = Application_Helper_Array::listArray($position, 'CODE','POSITION');
        $this->view->positionArr = $positionArr;

        $select = $this->_db->select()
        ->from(
         array('A' => 'BG_RELATIONSHIP_WITH_REPORTER'),
         array('*')
       );
        $relationship    = $this->_db->fetchAll($select);
        $relationshipArr = Application_Helper_Array::listArray($relationship, 'NO','RELATION_WITH_BANK');
        $this->view->relationshipArr = $relationshipArr;

        $select = $this->_db->select()
        ->from(
         array('A' => 'M_PRELIMINARY'),
         array('*')
       )
        ->where('A.CUST_ID = ?', $resultdata['CUST_ID']);
        $datapre = $this->_db->fetchRow($select);

        $datapre['CUST_NAME'] = $resultdata['CUST_NAME'];
        $this->view->datapre = $datapre;


        $select = $this->_db->select()
        ->from(
         array('A' => 'M_PRELIMINARY_MEMBER'),
         array('*')
       )
        ->where('A.CUST_ID = ?', $resultdata['CUST_ID']);
        $dataMember = $this->_db->fetchAll($select);
        $this->view->dataMember = $dataMember;



      }



      $this->view->cust_type    = $resultdata['CUST_TYPE'];
      $this->view->cust_finance    = $resultdata['CUST_FINANCE'];
      $this->view->cust_special    = $resultdata['CUST_SPECIAL'];
      $this->view->cust_same_user      = $resultdata['CUST_SAME_USER'];
      $this->view->cust_workfield  = $resultdata['CUST_WORKFIELD'];
      $this->view->business_type  = $resultdata['BUSINESS_TYPE'];
      $this->view->go_public  = $resultdata['GO_PUBLIC'];
      $this->view->debitur_code  = $resultdata['DEBITUR_CODE'];
      $this->view->collect_code  = $resultdata['COLLECTIBILITY_CODE'];
      $this->view->cust_address = $resultdata['CUST_ADDRESS'];
      $this->view->cust_village = $resultdata['CUST_VILLAGE'];
      $this->view->cust_district = $resultdata['CUST_DISTRICT'];
      $this->view->cust_city    = $resultdata['CUST_CITY'];
      $this->view->cust_zip     = $resultdata['CUST_ZIP'];
      $this->view->cust_province = $resultdata['CUST_PROVINCE'];
      $this->view->country_code  = $resultdata['COUNTRY_CODE'];
      $this->view->cust_contact  = $resultdata['CUST_CONTACT'];
      $this->view->cust_phone    = $resultdata['CUST_PHONE'];
      $this->view->cust_ext      = $resultdata['CUST_EXT'];
      $this->view->cust_fax      = $resultdata['CUST_FAX'];
      $this->view->cust_contact_phone      = $resultdata['CUST_CONTACT_PHONE'];
      $this->view->cust_email    = $resultdata['CUST_EMAIL'];
      $this->view->cust_website  = $resultdata['CUST_WEBSITE'];
      $this->view->cust_status   = strtoupper($resultdata['CUST_STATUS']);

      $this->view->cust_charges_status  = $resultdata['CUST_CHARGES_STATUS'];
      $this->view->cust_admfee_status   = $resultdata['CUST_MONTHLYFEE_STATUS'];
      $this->view->cust_token_auth   = $resultdata['CUST_TOKEN_AUTH'];

      $this->view->cust_created     = $resultdata['CUST_CREATED'];
      $this->view->cust_createdby   = $resultdata['CUST_CREATEDBY'];
      $this->view->cust_suggested   = $resultdata['CUST_SUGGESTED'];
      $this->view->cust_suggestedby = $resultdata['CUST_SUGGESTEDBY'];
      $this->view->cust_updated     = $resultdata['CUST_UPDATED'];
      $this->view->cust_updatedby   = $resultdata['CUST_UPDATEDBY'];

      $this->view->cust_limit_idr     = Application_Helper_General::displayMoney($resultdata['CUST_LIMIT_IDR']);
      $this->view->cust_limit_usd   = Application_Helper_General::displayMoney($resultdata['CUST_LIMIT_USD']);
      $this->view->bglimit   = Application_Helper_General::displayMoney($resultdata['CUST_BG_LIMIT']);

      $this->view->cust_review   = strtoupper($resultdata['CUST_REVIEW']);
      $this->view->cust_approver   = strtoupper($resultdata['CUST_APPROVER']);
      $this->view->cust_app_token  = $resultdata['CUST_APP_TOKEN'];
        // $this->view->cust_rls_token  = $resultdata['CUST_RLS_TOKEN'];
      $this->view->cust_rls_token  = 1;
      $this->view->cust_secure = $resultdata['CUST_SECURITIES'];

      $selectRDN = $this->_db->select()
      ->from('M_RDN_ACCT')
      ->where("CUST_ID = ? ",$resultdata['CUST_ID']);
                               //->group('FUSER_ID');

      $datardn = $this->_db->fetchAll($selectRDN);

      $this->view->datardn = $datardn;

      $select = $this->_db->select()->distinct()
      ->from(array('M_CUSTOMER'),array('CUST_ID'))
      ->where("CUST_STATUS != '3'")
      ->order('CUST_ID ASC')
      -> query()->fetchAll();

      $comp = array();

      foreach($select as $vl){
        $comp[] = $vl['CUST_ID'];
      }

      if(!empty($resultdata['CUST_CHARGESID'])){
        $selectglobal = $this->_db->select()
        ->from(array('B' => 'M_CHARGES_WITHIN'),array('*'))
        ->joinleft(array('D' => 'M_CHARGES_OTHER'), 'B.CUST_ID = D.CUST_ID',array('*'))
        ->where("B.CUST_ID NOT IN (?) ",$comp)
        ->where("B.CHARGES_ID = ?",$resultdata['CUST_CHARGESID']);
      }else{
        $selectglobal = $this->_db->select()
        ->from(array('B' => 'M_CHARGES_WITHIN'),array('*'))
        ->joinleft(array('D' => 'M_CHARGES_OTHER'), 'B.CUST_ID = D.CUST_ID',array('*'))
        ->where("B.CUST_ID NOT IN (?) ",$comp)
        ->where("B.CUST_ID = ?",'GLOBAL');
      }
        //echo $selectglobal;die;

      $arrglobal = $this->_db->fetchAll($selectglobal);   
      $chargeArr = array();
        //echo '<pre>';
        //var_dump($arrglobal);die;
      foreach($arrglobal as $ky => $vl){
        if($vl['CHARGES_TYPE'] == '1'){
          $chargeArr['RTGS'] = $vl['CHARGES_AMT'];  
        }
        if($vl['CHARGES_TYPE'] == '2'){
          $chargeArr['SKN'] = $vl['CHARGES_AMT']; 
        }
        if($vl['CHARGES_TYPE'] == '8'){
          $chargeArr['ONLINE'] = $vl['CHARGES_AMT'];  
        }
        $chargeArr['INHOUSE'] = $vl['AMOUNT']; 

        $chargeArr['INHOUSE'] = $vl['AMOUNT'];  
        $chargeArr['APPROVED'] =  $vl['CHARGES_APPROVEDBY'].'('.$vl['CHARGES_APPROVED'].')';  
        $chargeArr['SUGGESTED'] =  $vl['CHARGES_SUGGESTEDBY'].'('.$vl['CHARGES_SUGGESTED'].')'; 
        $chargeArr['PACKAGE_NAME'] =  $vl['PACKAGE_NAME'];  
      }

      $this->view->chargedata = $chargeArr;
        //echo '<pre>';
        //var_dump($chargeArr);die;

        //$resultdata['CUST_SPECIAL'];

      $selectAd = $this->_db->select()
      ->from(array('C' => 'T_DIRECTDEBIT'),array('*','company'  => new Zend_Db_Expr("CONCAT(COMP_NAME , ' (' , COMP_ID , ')  ' )")))
      ->joinleft(array('X'=>'M_DOMESTIC_BANK_TABLE'), 'X.SWIFT_CODE = C.DEBIT_BANK',array('BANK_NAME'))
      ->where('DIR_TYPE = 1')
      ->where('COMP_ID =?', $resultdata['CUST_ID'] )
                  //  ->where('DIR_STATUS IN (1,0)');
      ->order('DIR_APPROVED DESC')
      ->query()->fetchall();

      $datadd = array(0=>array('DD_NO'=>'1002127172','DD_NAME'=>'ALDI TIANSYAH','DD_REF'=>'251621178','DD_STATUS'=>1));
      $datadd = array(1=>array('DD_NO'=>'1002182271','DD_NAME'=>'MARIA SUMARSONO','DD_REF'=>'251621179','DD_STATUS'=>1));
      $datadd = array(2=>array('DD_NO'=>'1002178189','DD_NAME'=>'GANDA WIJAYA','DD_REF'=>'251621190','DD_STATUS'=>1));
      $datadd = array(3=>array('DD_NO'=>'1002188162','DD_NAME'=>'MULIA KENTJONO','DD_REF'=>'251621191','DD_STATUS'=>1));
      $datadd = array(4=>array('DD_NO'=>'1002178249','DD_NAME'=>'AGUNG SYAHPUTRA','DD_REF'=>'251621192','DD_STATUS'=>1));

      $this->view->datadd = $selectAd;

      $selectMaker = $this->_db->select()
      ->from('M_FPRIVI_USER')
      ->where("(FPRIVI_ID = 'CRSP' OR FPRIVI_ID = 'CRVA' OR FPRIVI_ID = 'CDFT') AND FUSER_ID LIKE ?", "%".(string)$resultdata['CUST_ID']."%")
      ->group('FUSER_ID');

      $userMaker = $this->_db->fetchAll($selectMaker);

      foreach ($userMaker as $row) {
        $userIdMaker = explode($resultdata['CUST_ID'], $row['FUSER_ID']);
          //var_dump($userIdMaker);
          //get user name
        $selectMakerName = $this->_db->select()
        ->from('M_USER')
        ->where("USER_ID = ?", (string)$userIdMaker[1])
        ->where("CUST_ID = ?", (string)$resultdata['CUST_ID']);
          //echo $selectMakerName;
        $userMakerName = $this->_db->fetchAll($selectMakerName);
        if(!empty($userMakerName)){
          $MakerList .= $userMakerName[0]['USER_FULLNAME'].'<br>';
        } 
          //var_dump($MakerList);
      }

      $imgClass = '';
      if($resultdata['CUST_FINANCE'] == '3'){
        $imgClass = "imgSepia";
      }

        // var_dump($MakerList);exit();
      $MakerNameCircle = '<a class="hovertext" style="text-decoration: none;" disabled><img class="'.$imgClass.'" src="/assets/themes/assets/newlayout/img/maker.PNG">
      <span class="hovertextcontent" style="left: -100px;">'.$MakerList.'</span></a>';

      $this->view->makerNameCircle = $MakerNameCircle;

      $selectReviewer = $this->_db->select()
      ->from('M_FPRIVI_USER')
      ->where("FPRIVI_ID = 'RVPV' AND FUSER_ID LIKE ?", "%".(string)$resultdata['CUST_ID']."%");

      $userReviewer = $this->_db->fetchAll($selectReviewer);

      foreach ($userReviewer as $row) {
        $userIdReviewer = explode($resultdata['CUST_ID'], $row['FUSER_ID']);

          //get user name
        $selectReviewerName = $this->_db->select()
        ->from('M_USER')
        ->where("USER_ID = ?", (string)$userIdReviewer[1])
        ->where("CUST_ID = ?", (string)$resultdata['CUST_ID']);

        $userReviewerName = $this->_db->fetchAll($selectReviewerName);
        if(!empty($userReviewerName)){
          $ReviewerList .= $userReviewerName[0]['USER_FULLNAME'].'<br>';
        }
      }

        // var_dump($ReviewerList);exit();
      $ReviewerNameCircle = '<a class="hovertext" style="text-decoration: none;" disabled><img class="'.$imgClass.'" src="/assets/themes/assets/newlayout/img/reviewer.png">
      <span class="hovertextcontent" style="left: -100px;">'.$ReviewerList.'</span></a>';

      $this->view->reviewerNameCircle = $ReviewerNameCircle; 




      $selectApprover = $this->_db->select()
      ->from('M_FPRIVI_USER')
      ->where("FPRIVI_ID = 'PAPV' AND FUSER_ID LIKE ?", "%".(string)$resultdata['CUST_ID']."%");

      $userApprover = $this->_db->fetchAll($selectApprover);
        //var_dump($userApprover);
      foreach ($userApprover as $row) {
        $userIdApprover = explode($resultdata['CUST_ID'], $row['FUSER_ID']);

          //get user name
        $selectApproverName = $this->_db->select()
        ->from('M_USER')
        ->where("USER_ID = ?", (string)$userIdApprover[1]);
                    //->where("CUST_ID = ?", (string)$resultdata['CUST_ID']);

        $userApproverName = $this->_db->fetchAll($selectApproverName);
        if(!empty($userApproverName)){
          $ApproverList .= $userApproverName[0]['USER_FULLNAME'].'<br>';
        }
      }

        // var_dump($ApproverList);exit();
      $ApproverNameCircle = '<a class="hovertext" style="text-decoration: none;" disabled><img class="'.$imgClass.'" src="/assets/themes/assets/newlayout/img/approver.png">
      <span class="hovertextcontent" style="left: -100px;">'.$ApproverList.'</span></a>';

      $this->view->approverNameCircle = $ApproverNameCircle; 

      $selectReleaser = $this->_db->select()
      ->from('M_FPRIVI_USER')
      ->where("FPRIVI_ID = 'PRLP' AND FUSER_ID LIKE ?", "%".(string)$resultdata['CUST_ID']."%");

      $userReleaser = $this->_db->fetchAll($selectReleaser);

      foreach ($userReleaser as $row) {
        $userIdReleaser = explode($resultdata['CUST_ID'], $row['FUSER_ID']);

          //get user name
        $selectReleaserName = $this->_db->select()
        ->from('M_USER')
        ->where("USER_ID = ?", (string)$userIdReleaser[1])
        ->where("CUST_ID = ?", (string)$resultdata['CUST_ID']);

        $userReleaserName = $this->_db->fetchAll($selectReleaserName);
        if(!empty($userReleaserName)){
          $releaserList .= $userReleaserName[0]['USER_FULLNAME'].'<br>';
        }
      }

        // var_dump($releaserList);exit();
      $releaserNameCircle = '<a class="hovertext" style="text-decoration: none;" disabled><img class="'.$imgClass.'" src="/assets/themes/assets/newlayout/img/releaser.png">
      <span class="hovertextcontent" style="left: -100px;">'.$releaserList.'</span></a>';

      $this->view->releaserNameCircle = $releaserNameCircle; 

      /*------------------------------------approval matrix--------------------------------------------------------*/
      $selectUsergroup = $this->_db->select()
      ->from(array('M_APP_GROUP_USER'),array('*'))
      ->where('CUST_ID = ?', $cust_id)
      ->group('GROUP_USER_ID')
          // echo $selectUser;die;
      ->query()->fetchall();

      $selectUsers = $this->_db->select()
      ->from(array('M_APP_GROUP_USER'),array('*'))
      ->where('CUST_ID = ?', $cust_id)
                    // echo $selectUser;die;
      ->query()->fetchall();
        // print_r($selectUser);die;

      $userlists = '';
      foreach ($selectUsergroup as $key => $value) {
        foreach ($selectUsers as $no => $val) {
          if($val['GROUP_USER_ID'] == $value['GROUP_USER_ID']){
            if(empty($userlists)){
              $userlists .= $val['USER_ID'];
            }else{
              $userlists .= ', '.$val['USER_ID'];
            }
          }
        }
        $selectUsergroup[$key]['USER'] .= $userlists;
        $userlists = '';
        $spesials = 'S_'.$cust_id;
        if($value['GROUP_USER_ID']==$spesials){
          $selectUsergroup[$key]['GID'] .= 'SG';
        }else{
          $groups = explode('_', $value['GROUP_USER_ID']);
          $alphabets = array(1=>'A',2=>'B',3=>'C',4=>'D',5=>'E',6=>'F',7=>'G',8=>'H',9=>'I',10=>'J',11=>'K',12=>'L',13=>'M',14=>'N',15=>'O',16=>'P',17=>'Q',18=>'R',19=>'S',20=>'T',21=>'U',22=>'V',23=>'W',24=>'X',25=>'Y',26=>'Z');
            // $cust = explode('_', $value['GROUP_USER_ID'])
          $selectUsergroup[$key]['GID'] .= $alphabets[(int)$groups[2]];
        }
      }
      $this->view->selectUsergroup = $selectUsergroup;

      $select = $this->_db->select()
      ->from(array('MAB'=>'M_APP_BOUNDARY'))
      ->join(array('MABG'=>'M_APP_BOUNDARY_GROUP'), 'MAB.BOUNDARY_ID = MABG.BOUNDARY_ID', array('GROUP_USER_ID'))
      ->where('MAB.CUST_ID = ?', (string)$cust_id)
      ->group('BOUNDARY_ID');
        // echo $select;
      $result = $this->_db->fetchAll($select);

      $dataArr = array();
        // $alf = 'A';
        // $alf++;
        // ++$alf;
        // print_r((int)$alf);
        // $nogroup = sprintf("%02d", 1);
        // print_r($result);die;

      foreach($result as $row){
        list($grouptype,$groupname,$groupnum) = explode("_",$row['GROUP_USER_ID']);
        if($grouptype == 'N'){
          $name = 'Group '.trim($groupnum,'0');
        }else{
          $name = 'Special Group';
        }

        $selectUser = $this->_db->select()
        ->from(array('M_APP_GROUP_USER'),array('USER_ID'))
        ->where('GROUP_USER_ID = ?', $row['GROUP_USER_ID'])
                      // echo $selectUser;die;
        ->query()->fetchall();

        $userlist = '';

        $policythen = explode(' THEN ', $row['POLICY']);

        foreach ($policythen as $keythen => $valuethen) {
          $policy = explode(' AND ', $valuethen);

          foreach ($policy as $key => $value) {
            $replaceVal = str_replace('(', '', $value);
            $replaceVal = str_replace(')', '', $replaceVal);
            $policy[$key] = $replaceVal;
          }

          foreach ($policy as $key => $value) {
            if($value == 'SG'){
              $group = 'S_'.$cust_id;
              $selectUser = $this->_db->select()
              ->from(array('M_APP_GROUP_USER'),array('USER_ID'))
              ->where('GROUP_USER_ID = ?', $group)
                        // echo $selectUser;die;
              ->query()->fetchall();  
              foreach($selectUser as $val){
                if(empty($userlist)){
                  $userlist .= $val['USER_ID'];
                }else{
                  $userlist .= ', '.$val['USER_ID'];
                }
              }                         
            }else{
              $alphabet = array("A"=>1,"B"=>2,"C"=>3,"D"=>4,"E"=>5,"F"=>6,"G"=>7,"H"=>8,"I"=>9,"J"=>10,"K"=>11,"L"=>12,"M"=>13,"N"=>14,"O"=>15,"P"=>16,"Q"=>17,"R"=>18,"S"=>19,"T"=>20,"U"=>21,"V"=>22,"W"=>23,"X"=>24,"Y"=>25,"Z"=>26,);

              $policyor = explode(' OR ', $value);

                // print_r($policyor);die;
              foreach ($policyor as $numb => $valpol) {
                if($valpol == 'SG'){
                  $group = 'S_'.$cust_id;
                  $selectUser = $this->_db->select()
                  ->from(array('M_APP_GROUP_USER'),array('USER_ID'))
                  ->where('GROUP_USER_ID = ?', $group)
                            // echo $selectUser;die;
                  ->query()->fetchall();  
                  foreach($selectUser as $val){
                    if(empty($userlist)){
                      $userlist .= $val['USER_ID'];
                    }else{
                      $userlist .= ', '.$val['USER_ID'];
                    }
                  }
                }else{
                  $nogroup = sprintf("%02d", $alphabet[$valpol]);
                    // print_r($valpol);
                  $group = 'N_'.$cust_id.'_'.$nogroup;
                  $selectUser = $this->_db->select()
                  ->from(array('M_APP_GROUP_USER'),array('USER_ID'))
                  ->where('GROUP_USER_ID = ?', $group)
                          // echo $selectUser;die;
                  ->query()->fetchall();  
                        // print_r($selectUser);  
                  foreach($selectUser as $val){
                    if(empty($userlist)){
                      $userlist .= $val['USER_ID'];
                    }else{
                      $userlist .= ', '.$val['USER_ID'];
                    }
                  }
                }
              }
            }
          }
        }
          // print_r($userlist);die;
          // $nogroup = sprintf("%02d", $key+1);

          // foreach($selectUser as $val){
          //  if(empty($userlist))
          //    $userlist .= $val['USER_ID'];
          //  else
          //    $userlist .= ', '.$val['USER_ID'];
          // }

        $arrTraType     = array_combine($this->_paymenttype["code"],$this->_paymenttype["desc"]);
        $arrTraType['19'] = 'Cash Pooling Same Bank';
        $arrTraType['20'] = 'Cash Pooling Same Bank';
        $arrTraType['23'] = 'Cash Pooling Other Bank';

        $userl = explode(', ',$userlist);
        $userlist = array_unique($userl);
        $userlistnew = '';

        foreach($userlist as $ky  => $val){
            //var_dump($ky);
          if($ky == 0){
            $userlistnew .= $val;
              //var_dump($userlistnew);
          }else{
            $userlistnew .= ', '.$val;
          }
        }

        $changepolicy = explode(" ", $row['POLICY']);
        $data = array();
        foreach ($changepolicy as $keypolicy => $valpolicy) {
          if ($valpolicy == 'AND') {
            $valpolicy = '<span style="color: blue;">and</span>';
          }elseif ($valpolicy == 'OR') {
            $valpolicy = '<span style="color: blue;">or</span>';
          }elseif ($valpolicy == 'THEN') {
            $valpolicy = '<span style="color: blue;">then</span>';
          }

          $replacepolicy1 = str_replace('(', '<span style="color: blue;">(</span>', $valpolicy);
          $replacepolicy2 = str_replace(')', '<span style="color: blue;">)</span>', $replacepolicy1);

          $data[$keypolicy] = $this->language->_($replacepolicy2);
        }

        $valpolicy2 = implode(" ", $data);

        $dataArr[] = array( 
          'TRANSFER_TYPE' => $arrTraType[$row['TRANSFER_TYPE']],
          'CURRENCY' => $row['CCY_BOUNDARY'],
          'BOUNDARY' => Application_Helper_General::displayMoney($row['BOUNDARY_MIN'])." - ".Application_Helper_General::displayMoney($row['BOUNDARY_MAX']),
          'GROUP_NAME' => $valpolicy2,
          'USERS' => $userlistnew);
      }
        //var_dump($dataArr);
        // print_r($dataArr);die;
      /* print_r($row);die();*/

      $this->view->dataBoundary = $dataArr;
      /*------------------------------------end approval matrix--------------------------------------------------------*/

        //get adapter profile data
      $select = $this->_db->select()
      ->from(array('H' => 'M_CUST_ADAPTER_PROFILE'),array('*'))
      ->where('H.CUST_ID = '.$this->_db->quote($cust_id));

      $profileData = $this->_db->fetchAll($select);

      foreach ($profileData as $key => $value) {
        if ($value['ADAPTER_PROFILE_ID'] !== 'defaults') {
            //get adapter profile data
          $selectAdapterName = $this->_db->select()
          ->from(array('M_ADAPTER_PROFILE'),array('PROFILE_NAME','STATUS'))
          ->where('PROFILE_ID = '.$this->_db->quote($value['ADAPTER_PROFILE_ID']));

          $profileName = $this->_db->fetchAll($selectAdapterName);
          $statusname = '';
          if($profileName[0]['STATUS'] == '3'){
            $statusname = ' - Profile Status: Deleted';
          }
          if($profileName[0]['STATUS'] == '2'){
            $statusname = ' - Profile Status: Suspended';
          }
          $profileData[$key]['PROFILE_NAME'] = $profileName[0]['PROFILE_NAME'].$statusname;
        }else{
          $profileData[$key]['PROFILE_NAME'] = 'default';
        }
      }

      foreach ($profileData as $key => $value) {
        if ($value['TRF_TYPE'] == 'payroll') {
          $this->view->payrollAdapter = $value['PROFILE_NAME'];
        }

        if ($value['TRF_TYPE'] == 'manytomany') {
          $this->view->manytomanyAdapter = $value['PROFILE_NAME'];
        }

        if ($value['TRF_TYPE'] == 'emoney') {
          $this->view->emoneyAdapter = $value['PROFILE_NAME'];
        }

        if ($value['TRF_TYPE'] == 'onetomany') {
          $this->view->onetomanyAdapter = $value['PROFILE_NAME'];
        }

        if ($value['TRF_TYPE'] == 'manytoone') {
          $this->view->manytooneAdapter = $value['PROFILE_NAME'];
        }
      }

        //--------------------------------------------------Bank Account--------------------------------------------
      $this->view->bankAccount     = $this->getCustomerAcct($cust_id);
      $this->view->benefBankAcc    = $this->getBenefBankAcc($cust_id);
      $this->view->userAccount     = $this->getUserAcct($cust_id);
      $this->view->userLimit       = $this->getUserLimit($cust_id);
      $this->view->useropenLimit   = $this->getUserOpenLimit($cust_id);
        //echo '<pre>';
        //var_dump($this->view->useropenLimit);die;
      $this->view->userDailyLimit  = $this->getUserDailyLimit($cust_id);
        // $this->view->userTempDailyLimit  = $this->getUserTempDailyLimit($cust_id);

      $getAppGroup = $this->getAppGroup($cust_id);
      $getAppGroupArray = null;
      foreach($getAppGroup as $row){
        $getAppGroupArray[ $row['GROUP_USER_ID'] ][] = $row['USER_ID'];
      }
      $this->view->appGroup = $getAppGroupArray;

      $getAppBoundaryGroup = $this->getAppBoundary($cust_id);
      $boundaryGroup = null;
      foreach($getAppBoundaryGroup as $row){
          //example : N_CUSTCHRIS_01 & S_CUSTCHRIS
        $explodeGroup = explode('_',$row['GROUP_USER_ID']);

        if($explodeGroup[0] == 'N')      $group_desc = 'Group ' . (int)$explodeGroup[2];
        else if($explodeGroup[0] == 'S') $group_desc = 'Special Group';

        $boundaryGroup[$row['BOUNDARY_ID']]['GROUP_USER_ID'][] = $group_desc;
        $boundaryGroup[$row['BOUNDARY_ID']]['BOUNDARY_MIN']    = $row['BOUNDARY_MIN'];
        $boundaryGroup[$row['BOUNDARY_ID']]['BOUNDARY_MAX']    = $row['BOUNDARY_MAX'];
        $boundaryGroup[$row['BOUNDARY_ID']]['CCY_BOUNDARY']    = $row['CCY_BOUNDARY'];
      }
      $this->view->appBoundary = $boundaryGroup;

      $conf = Zend_Registry::get('config');
      $paymentType = $conf['payment']['type'];
      $paymentTypeFlip = array_flip($paymentType['code']);
      
      $this->view->masterbankname = $conf['app']['bankname'];

      $this->view->paymentType = $paymentType;
      $this->view->paymentTypeFlip = $paymentTypeFlip;

      $this->view->benefUser   = $this->getBenefUser($cust_id);

      $this->view->benefAccount   = $this->getBenefAccount($cust_id);

        //----------------------------------------------- END Bank Account------------------------------------------
      $colomn = array('account_number','account_name','account_currency');
      $acctlist = $this->_db->fetchAll(
        $this->_db->select()
        ->from(array('A' => 'M_APIKEY'))
        ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
        ->join(array('B' => 'M_BANK_TABLE'),'A.BANK_CODE = B.BANK_CODE',array('bankname' => 'B.BANK_NAME'))
                    // ->group('A.APIKEY_ID')
        ->where('UPPER(A.CUST_ID)='.$this->_db->quote((string)$cust_id))
        ->where('A.`FIELD` IN (?)',$colomn)
        ->order('A.APIKEY_ID ASC')
      );

      $account = array();
      foreach ($acctlist as $key => $value) {
        $account[$value['ID']][$value['FIELD']] = $value['VALUE'];
        $account[$value['ID']]['bankname'] = $value['bankname'];
        $account[$value['ID']]['BANK_CODE'] = $value['BANK_CODE'];
      }

      $datapers = array();

      $totalonline = 0;

      foreach ($account as $key => $request) {
        $datapers[$key-1]['bank_name']  = $request['bankname'];
        $datapers[$key-1]['bank_code']  = $request['BANK_CODE'];
        $datapers[$key-1]['account_number']  = $request['account_number'];
        $datapers[$key-1]['account_name']  = $request['account_name'];
        if (isset($request['account_alias'])) {
          $datapers[$key-1]['account_alias']  = $request['account_alias'];
        }else{
          $datapers[$key-1]['account_alias']  = '-';
        }
        $datapers[$key-1]['account_currency']  = 'IDR';
        $datapers[$key-1]['account_status'] = '1';
      }

      $total = count($datapers);

      $acctmanual = $this->_db->fetchAll(
        $this->_db->select()
        ->from(array('A' => 'T_BALANCE'))
        ->join(array('C' => 'M_BANK_TABLE'),'A.BANK_CODE = C.BANK_CODE',array('bankname' => 'C.BANK_NAME'))
        ->where('A.ACCT_STATUS = ?','5')
        ->where('UPPER(A.CUST_ID)='.$this->_db->quote((string)$cust_id))
        ->group("A.ACCT_NO")
        ->group("A.BANK_CODE")
        ->limit(100)
      );
        // echo $acctmanual;die;
      $totalmanual = 0;
      foreach ($acctmanual as $key => $value) {
        $datapersmanual[$total]['bank_code']  = $value['BANK_CODE'];
        $datapersmanual[$total]['bank_name']  = $value['bankname'];
        $datapersmanual[$total]['account_number']  = $value['ACCT_NO'];
        $datapersmanual[$total]['account_currency']  = $value['CCY_ID'];
        $datapersmanual[$total]['account_alias']  = $value['ACCT_ALIAS'];
        $datapersmanual[$total]['account_name']  = $value['ACCT_ALIAS'];
        $datapersmanual[$total]['account_status'] = $value['ACCT_STATUS'];
        $total++;
      }

      if (count($datapersmanual) > 0) {
        $datapersonal = array_merge($datapers, $datapersmanual);
      }else{
        $datapersonal = $datapers;
      }

      $this->view->datapersonal = $datapersonal;

      $select2 = $this->_db->select()
      ->from(array('BG' => 'T_BANK_GUARANTEE'),array('*'))
      ->join(array('C' => 'M_CUSTOMER'), 'BG.CUST_ID = C.CUST_ID', array('CUST_NAME' => 'C.CUST_NAME'))
      ->order('BG_UPDATED DESC');
      $select2->where('BG.CUST_ID = ?',$cust_id);       
      $bglist = $this->_db->fetchAll($select2);
      $usedbg = 0;
      foreach($bglist as $vl){
        if($vl['BG_STATUS'] == '5' && $vl['COUNTER_WARRANTY_TYPE'] == '2'){
          $usedbg = $usedbg+$vl['BG_AMOUNT'];
        }
      }
      $this->view->bgused = Application_Helper_General::displayMoney($usedbg);
      $this->view->bglist = $bglist;

      $config       = Zend_Registry::get('config');
      $BgType     = $config["bg"]["status"]["desc"];
      $BgCode     = $config["bg"]["status"]["code"];

      $arrStatus = array_combine(array_values($BgCode),array_values($BgType));

      $this->view->arrStatus = $arrStatus;
      
      $arrWaranty = array(
        1 => 'Full Cover',
        2 => 'Line Facility',
        3 => 'Insurance'

      );
      $this->view->arrWaranty = $arrWaranty;

      $result = $this->getTempCustomerId($cust_id);

      if($result)  $temp = 0;
      else  $temp = 1;

      $this->view->cust_temp = $temp;
    }else{
      $cust_id = null;
    }
  }
    // End if cust_id == true

  if(!$cust_id){
    $error_remark = 'Invalid Cust ID';
      //insert log
    try{
      $this->_db->beginTransaction();

      Application_Helper_General::writeLog('CCLS','View Detail Customer [Invalid Cust ID]');

      $this->_db->commit();
    }
    catch(Exception $e){
      $this->_db->rollBack();
    }

    $this->_helper->getHelper('FlashMessenger')->addMessage('F');
    $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
    $this->_redirect($this->_helper->url->url(array('module'=>$this->_request->getModuleName(),'controller'=>'index','action'=>'index')));
  }

  $this->view->cust_id = $cust_id;
  $this->view->status_type = $this->_masterglobalstatus;
  $this->view->modulename = $this->_request->getModuleName();

    //insert log
  try{
    $this->_db->beginTransaction();

    Application_Helper_General::writeLog('CCLS','View Customer Detail ['.$cust_id.']');

    $this->_db->commit();
  }
  catch(Exception $e){
    $this->_db->rollBack();
  }
}
}