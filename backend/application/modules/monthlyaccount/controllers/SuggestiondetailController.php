<?php

require_once 'Zend/Controller/Action.php';

class Monthlyaccount_SuggestiondetailController extends Application_Main
{
  public function indexAction() 
  { 
  	$changeid = $this->_getParam('changes_id');
  	
  	$select = $this->_db->select()
					        ->from(array('A' => 'T_GLOBAL_CHANGES'),array('*'));
	$select -> where("A.CHANGES_ID = ?", $changeid);
	$select -> where("A.CHANGES_STATUS = 'WA' OR A.CHANGES_STATUS = 'RR'");
	$result = $select->query()->FetchAll();
	$this->view->result=$result;
	$custid = $result[0]['KEY_FIELD'];
	
  	if(!$result)
	{
		$this->_redirect('/notification/invalid/index');
	}
	
	//Zend_Debug::dump($result2);die;
	
	
		$selectprk = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER_ACCT'),array('A.ACCT_NO', 'A.CCY_ID', 'A.ACCT_NAME'));
		$selectprk -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		
		/*$selectnoprk = $this->_db->select()
					        ->from(array('A' => 'M_BENEFICIARY'),array('ACCT_NO' => 'A.BENEFICIARY_ACCOUNT', 'CCY_ID' => 'A.CURR_CODE', 'ACCT_NAME' => 'A.BENEFICIARY_NAME'));
		$selectnoprk -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));*/
		
		$selectnoprk2 = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER_ACCT'),array('A.ACCT_NO', 'A.CCY_ID', 'A.ACCT_NAME'));
		$selectnoprk2 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		$selectnoprk2 -> where("A.ACCT_SOURCE NOT LIKE '1'");
		
		//$selectprk = $selectprk->__toString();
		//$selectnoprk = $selectnoprk->__toString();
		//$selectnoprk2 = $selectnoprk2->__toString();
		
		//$unionquery = $this->_db->select()
								//->union(array($selectprk,$selectnoprk));
		
		$resultunion = $selectprk->query()->FetchAll();
		$this->view->resultaccount = $resultunion;
								
		//$selectunion = $this->_db->select()
		//					->from (($unionquery),array('*'));
		//$resultunion = $selectunion->query()->FetchAll();
		//$this->view->resultaccount = $resultunion;
		//Zend_Debug::dump($resultunion);die;
		
		/*$chargeaccount = $this->_db->select()
								->union(array($selectnoprk2,$selectnoprk));*/
		$resultlist = $selectnoprk2->query()->FetchAll();
		$this->view->result2 = $resultlist;
		
  	$select4 = $this->_db->select()->distinct()
			        	->from(array('A' => 'M_CHARGES_MONTHLY'),array('A.ACCT_NO'));
	$select4 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
	$select4 -> where("MONTHLYFEE_TYPE = '3'");
	$result4 = $this->_db->fetchAll($select4);
	$this->view->result4cur = $result4;
		
	foreach($result4 as $curlist)
	{
		$select42 = $this->_db->select()
			        ->from(array('A' => 'M_CHARGES_MONTHLY'),array('*'));
		$select42 -> where("A.ACCT_NO LIKE ".$this->_db->quote($curlist['ACCT_NO']));
		$select42 -> order("TRA_FROM ASC");
		$result42 = $this->_db->fetchAll($select42);
			
		$select3 = $this->_db->select()
			      ->from(($selectprk),array('*'));
		$select3 -> where("ACCT_NO LIKE ".$this->_db->quote($curlist['ACCT_NO']));
		$result3 = $this->_db->fetchRow($select3);
		$acctno = $curlist['ACCT_NO'].'current';
		
		if($result3)
		{
			$this->view->$acctno = $result3['ACCT_NO'].' / '.$result3['ACCT_NAME'].' ('.$result3['CCY_ID'].')';	
		}
		else
		{
			$this->view->$acctno = '-';
		}
			
			//Zend_Debug::dump($result42);die;
		$cur = 0;
		$curid = 'cur'.$curlist['ACCT_NO'];
		
		foreach($result42 as $list)
		{
			$idccy = 'ccy'.$list['ACCT_NO'].'current'.$cur;
   			$idamt = 'amount'.$list['ACCT_NO'].'current'.$cur;
   			$idfrom = 'from'.$list['ACCT_NO'].'current'.$cur;
   			$idto = 'to'.$list['ACCT_NO'].'current'.$cur;
			$amt = Application_Helper_General::convertDisplayMoney($list['AMOUNT']);
					
			$select3 = $this->_db->select()
			        ->from(array('A' => $selectprk),array('*'));
			$select3 -> where("A.ACCT_NO LIKE ".$this->_db->quote($list['CHARGES_ACCT_NO']));
			$result3 = $this->_db->fetchRow($select3);
					
			$cekacct = 'cek'.$list['ACCT_NO'].'current';
			$this->view->$idamt = $amt;
			$this->view->$idccy = $list['CCY'];
			$this->view->$idfrom = $list['TRA_FROM'];
			$this->view->$idto = $list['TRA_TO'];
			if($result3)
			$this->view->$cekacct = $result3['ACCT_NO'].' / '.$result3['ACCT_NAME'].' ('.$result3['CCY_ID'].')';	
				 
			$this->view->$curid = $cur;
			$cur++;
		}
 	}
		
		
  		$select4 = $this->_db->select()->distinct()
			        	->from(array('A' => 'TEMP_CHARGES_MONTHLY'),array('A.ACCT_NO'));
		$select4 -> where("A.CHANGES_ID = ?", $changeid);
		$result4 = $this->_db->fetchAll($select4);
		$this->view->result4sug = $result4;
		
		foreach($result4 as $suglist)
		{
			$select42 = $this->_db->select()
			        	->from(array('A' => 'TEMP_CHARGES_MONTHLY'),array('*'));
			$select42 -> where("A.CHANGES_ID LIKE ".$this->_db->quote($changeid));
			$select42 -> where("A.ACCT_NO LIKE ".$this->_db->quote($suglist['ACCT_NO']));
			$select42 -> order("TRA_FROM ASC");
			$result42 = $this->_db->fetchAll($select42);
			
			$select3 = $this->_db->select()->distinct()
			        ->from(array('A' => $selectprk),array('*'));
			$select3 -> where("A.ACCT_NO LIKE ".$this->_db->quote($suglist['ACCT_NO']));
			$result3 = $this->_db->fetchRow($select3);
			$acctno = $suglist['ACCT_NO'].'suggest';
			$this->view->$acctno = $result3['ACCT_NO'].' / '.$result3['ACCT_NAME'].' ('.$result3['CCY_ID'].')';
			
			//Zend_Debug::dump($result42);die;
			$sug = 0;
			$sugid = 'sug'.$suglist['ACCT_NO'];
			foreach($result42 as $list)
			{
				$idccy = 'ccy'.$list['ACCT_NO'].'suggest'.$sug;
   				$idamt = 'amount'.$list['ACCT_NO'].'suggest'.$sug;
   				$idfrom = 'from'.$list['ACCT_NO'].'suggest'.$sug;
   				$idto = 'to'.$list['ACCT_NO'].'suggest'.$sug;
				$amt = Application_Helper_General::convertDisplayMoney($list['AMOUNT']);
					
				$select3 = $this->_db->select()
			        ->from(array('A' => $selectprk),array('*'));
				$select3 -> where("A.ACCT_NO LIKE ".$this->_db->quote($list['CHARGES_ACCT_NO']));
				$result3 = $this->_db->fetchRow($select3);
					
				$cekacct = 'cek'.$list['ACCT_NO'].'suggest';
				$this->view->$idccy = $list['CCY'];
				$this->view->$idamt = $amt;
				$this->view->$idfrom = $list['TRA_FROM'];
				$this->view->$idto = $list['TRA_TO'];
				
				if($result3)
				{
					$this->view->$cekacct = $result3['ACCT_NO'].' / '.$result3['ACCT_NAME'].' ('.$result3['CCY_ID'].')';	
				}
				else
				{
					$this->view->$cekacct = '-';
				}	
				 
				$this->view->$sugid = $sug;
				$sug++;
			}
 		}

	$this->view->changes_id = $changeid;
    $this->view->typeCode = array_flip($this->_changeType['code']);
    $this->view->typeDesc = $this->_changeType['desc'];
    $this->view->modulename = $this->_request->getModuleName();
	//echo $select2; die;
	
    Application_Helper_General::writeLog('CHUD','View Monthly transaction account charges changes list page ('.$custid.')');
  }
}