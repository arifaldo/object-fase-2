<?php

Class language_Model_Index{
	protected $_db;
	
	public function __construct(){
		$this->_db = Zend_Db_Table::getDefaultAdapter();
	}
	
	public function getDictionary($param = array()){
		$sql = $this->_db->select()
			->from(
					array('MTD'=>'M_TRANSLATE_DETAIL'),
					array(
						'WORD' => 'MT.TEXT',
						'MTL.CODE',
						'MTD.TEXT'
					)
			)

			->joinLeft(array('MTL'=>'M_TRANSLATE_LANG'), 'MTD.M_TRANSLATE_LANG_ID = MTL.ID',array())
			->joinLeft(array('MT'=>'M_TRANSLATE'), 'MT.ID = MTD.M_TRANSLATE_ID',array())
			->joinLeft(array('MTU'=>'M_TRANSLATE_UI'), 'MT.M_TRANSLATE_UI_ID = MTU.ID',array())
		;
		
		$uiList = $this->getUi(array());
		$uiList = Application_Helper_Array::simpleArray($uiList, 'ID');
			
		if (isset($param['ui']) && !empty($param['ui']) && in_array($param['ui'], $uiList)){
			$sql->where('MTU.ID = ?',$this->_db->quote($param['ui']));
		}
		// echo $sql;die;
		$sql = $this->_db->fetchAll($sql);
		
		if(count($sql)){
			return $sql;
		}else{
			return array();
		}
	}
  
    public function getLang($param = array()){
		$sql = $this->_db->select()
			->from(
				array('M_TRANSLATE_LANG'),
				array('ID','CODE','DESC')
			)
		;
		$sql = $this->_db->fetchAll($sql);
		
		if(count($sql)){
			return $sql;
		}else{
			return array();
		}
    }
    
    public function getUi($param = array()){
    	$sql = $this->_db->select()
		->from(
				array('M_TRANSLATE_UI'),
				array('ID','DESC')
		)
		;
		//$sql->where('ID= ?', '1');
		$sql = $this->_db->fetchAll($sql);
    
    	if(count($sql)){
    		return $sql;
    	}else{
    		return array();
    	}
    }
	
    public function getList($param = array()){
    	$sql = $this->_db->select()
    		->from(
    			array('MT' => 'M_TRANSLATE'),
    			array(
    				'UI' => 'MTU.DESC',
    				'LANGUAGE' => 'MTL.DESC',
    				'PHRASE' => 'MT.TEXT',
    				'TRANSLATED' => 'MTD.TEXT',
    				'MTDUPDATED' => 'MTD.UPDATED',
    			)
    		)
    		->join(array('MTU' => 'M_TRANSLATE_UI'), 'MT.M_TRANSLATE_UI_ID = MTU.ID', array())
    		->join(array('MTD' => 'M_TRANSLATE_DETAIL'), 'MT.ID = MTD.M_TRANSLATE_ID', array())
    		->join(array('MTL' => 'M_TRANSLATE_LANG'), 'MTD.M_TRANSLATE_LANG_ID = MTL.ID', array())
    		->order($param['sortBy'].' '.$param['sortDir'])
    	;
		
    	if (!empty($param['ui'])){
    		$sql->where('MTU.ID= ?', $param['ui']);
		}
		
		if (!empty($param['lang'])){
			$sql->where('MTL.CODE = ?', $param['lang']);
		}
		
		if (!empty($param['oriPhrase'])){
			$sql->where("MT.TEXT LIKE ?", '%'.$param['oriPhrase'].'%');
		}
		
		if (!empty($param['traPhrase'])){
			$sql->where("MTD.TEXT LIKE ?", '%'.$param['traPhrase'].'%');
		}
		
// 		echo $sql;
    	$sql = $this->_db->fetchAll($sql);
    
    	if(count($sql)){
    		return $sql;
    	}else{
    		return array();
    	}
    }
}