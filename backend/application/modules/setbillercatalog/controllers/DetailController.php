<?php


require_once 'Zend/Controller/Action.php';


class Setbillercatalog_DetailController extends Application_Main
{
	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');
		$model 			 = new biller_Model_Biller();
		$billerCharges = new setbillercharges_Model_Setbillercharges();
		$providerArr 	 = $billerCharges->getServiceProvider();
    	$options = array_combine(array_values($this->_masterStatus['code']),array_values($this->_masterStatus['desc']));
		unset($options[3]);
		array_unshift($options,'---'.$this->language->_('Any Value').'---');
		$this->view->options = $options;
		$this->view->var = $providerArr;	
		
		$fields = array	(
							'sgo_product_code'  			=> array	(
																	'field' => 'SGO_PRODUCT_CODE',
																	'label' => $this->language->_('Product Code'),
																	'sortable' => true
																),
							'biller_code'  			=> array	(
																	'field' => 'PRODUCT_CODE',
																	'label' => $this->language->_('Product Name'),
																	'sortable' => true
																),
							'product_alias_name'  			=> array	(
																	'field' => 'PRODUCT_ALIAS_NAME',
																	'label' => $this->language->_('Product Alias Name'),
																	'sortable' => true
																),
							
							'provider_suggested'  		=> array	(
																	'field' => 'PROVIDER_SUGGESTED',
																	'label' => $this->language->_('Suggestion Date'),
																	'sortable' => true
																),
							'provider_suggestedby'  		=> array	(
																	'field' => 'PROVIDER_SUGGESTEDBY',
																	'label' => $this->language->_('Suggester'),
																	'sortable' => true
																),
							'provider_updated'  		=> array	(
																	'field' => 'PROVIDER_UPDATED',
																	'label' => $this->language->_('Updated Date'),
																	'sortable' => true
																),
							'provider_updatedby'  		=> array	(
																	'field' => 'PROVIDER_UPDATEDBY',
																	'label' => $this->language->_('Updated By'),
																	'sortable' => true
																),

							'status'  		=> array	(
																	'field' => 'CATALOG_STATUS',
																	'label' => 'Status',
																	'sortable' => true
																),
						);
						
		$filterArr = array('filter' 		=> array('StripTags','StringTrim'),
	                       'billerCode'    	=> array('StripTags','StringTrim'),
	                       'billerName'  	=> array('StripTags','StringTrim','StringToUpper'),
	                       'billerStatus'  	=> array('StripTags','StringTrim'),
						   'suggestor'  	=> array('StripTags','StringTrim','StringToUpper'),
						   'approver'  	=> array('StripTags','StringTrim','StringToUpper'),
						   'fDateFrom'  	=> array('StripTags','StringTrim'),
						   'ufDateFrom'  	=> array('StripTags','StringTrim'),
						   'fDateTo'  		=> array('StripTags','StringTrim'),
						   'ufDateTo'  		=> array('StripTags','StringTrim'),
	                      );
	                      
	    $validator = array('filter' 		=> array(),
	                       'billerCode'    	=> array(),
	                       'billerName'  	=> array(),
	                       'billerStatus'  	=> array(),
						   'suggestor'  	=> array(),
						   'approver'  	=> array(),
						   'fDateFrom'  	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						   'ufDateFrom'  	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						   'fDateTo'  		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						   'ufDateTo'  		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
	                      );
	                      
		$fParam = array();	                      
	    $zf_filter = new Zend_Filter_Input($filterArr,$validator,$this->_request->getParams());
	    $filter = $zf_filter->getEscaped('filter');
	    $billerCode = $fParam['billerCode'] = html_entity_decode($zf_filter->getEscaped('billerCode'));
		$billerName = $fParam['billerName'] = html_entity_decode($zf_filter->getEscaped('billerName'));
		$billerStatus = $fParam['billerStatus'] = html_entity_decode($zf_filter->getEscaped('billerStatus'));
		$suggestor 	= $fParam['suggestor'] 	= html_entity_decode($zf_filter->getEscaped('suggestor'));
		$approver 	= $fParam['approver'] 	= html_entity_decode($zf_filter->getEscaped('approver'));
		$datefrom 	= $fParam['datefrom'] 	= html_entity_decode($zf_filter->getEscaped('fDateFrom'));
		$udatefrom 	= $fParam['udatefrom'] 	= html_entity_decode($zf_filter->getEscaped('ufDateFrom'));
		$dateto 	= $fParam['dateto'] 	= html_entity_decode($zf_filter->getEscaped('fDateTo'));
		$udateto 	= $fParam['udateto'] 	= html_entity_decode($zf_filter->getEscaped('ufDateTo'));
		
		$page = $this->_getParam('page');
		$csv  = $this->_getParam('csv');
		$pdf  = $this->_getParam('pdf');
		$fParam['provid'] = $this->_getParam('provid');
		$this->view->provid = $fParam['provid'] = $this->_getParam('provid'); 
		
		
		$page = (Zend_Validate::is($page,'Digits')) ? $page : 1;
		$sortBy  = $this->_getParam('sortby');
   		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
   		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
		
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		if($filter == 'Set Filter')
		{
			$this->view->fDateTo    = $dateto;
			$this->view->ufDateTo    = $udateto;
			$this->view->fDateFrom  = $datefrom;
			$this->view->ufDateFrom  = $udatefrom;
			 
			if(!empty($datefrom))
			{
				$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
				$fParam['datefrom']   = $FormatDate->toString($this->_dateDBFormat);	
			}
					
			if(!empty($dateto))
			{
				$FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
				$fParam['dateto']    = $FormatDate->toString($this->_dateDBFormat);
			}
			if(!empty($udatefrom))
			{
				$FormatDate = new Zend_Date($udatefrom, $this->_dateDisplayFormat);
				$fParam['udatefrom']   = $FormatDate->toString($this->_dateDBFormat);	
			}
					
			if(!empty($udateto))
			{
				$FormatDate = new Zend_Date($udateto, $this->_dateDisplayFormat);
				$fParam['udateto']    = $FormatDate->toString($this->_dateDBFormat);
			}

		    if($billerCode)
		    {
	       		$this->view->billerCode = $billerCode;
		    }
		    
			if($billerName)
			{
	       		$this->view->billerName = $billerName;
			}
			
			if($billerStatus)
			{
	       		$this->view->billerStatus = $billerStatus;
			}
			
			if($suggestor)
		    {
	       		$this->view->suggestor = $suggestor;
		    }
			if($approver)
		    {
	       		$this->view->approver = $approver;
		    }
		}

		//$data = $model->getProvider($fParam,$sortBy,$sortDir,$filter);
		$data = $model->getBillerDetailCatalog($fParam,$sortBy,$sortDir,$filter);
		
    	
		$this->paging($data);
    	$this->view->fields = $fields;
    	$this->view->filter = $filter;

    	if($csv || $pdf)
    	{
    		$arr = $data;
    		
    		foreach($arr as $key=>$value)
			{
				unset($arr[$key]["PROVIDER_ID"]);
				$arr[$key]["PROVIDER_SUGGESTED"] = Application_Helper_General::convertDate($value["PROVIDER_SUGGESTED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
			}
    		
	    	if($csv)
			{
				Application_Helper_General::writeLog('VBIC','Download CSV Setup Biller Catalog');
				$this->_helper->download->csv(array('Biller Code','Biller Name','Service Catagory','Service Type','Status','Latest Suggestion','Suggester','Latest Approval','Latest Approver'),$arr,null,'Setup Biller Charges');
			}
			
			if($pdf)
			{
				Application_Helper_General::writeLog('VBIC','Download PDF Setup Biller Catalog');
				$this->_helper->download->pdf(array('Biller Code','Biller Name','Service Catagory','Service Type','Status','Latest Suggestion','Suggester','Latest Approval','Latest Approver'),$arr,null,'Setup Biller Charges');
			}
    	}
    	else
    	{
    		Application_Helper_General::writeLog('VBIC','View Detail Biller Catalog');
    	}
	}
}