<?php
require_once 'Zend/Controller/Action.php';

class registerechannel_CheckcompanyController extends Application_Main{

  	public function indexAction()
  	{
		$this->_helper->layout()->setLayout('popup');

	    $model = new registerechannel_Model_Registerechannel();
	    $changes_id = $this->_getParam('changes_id');
	    $changes_id = (Zend_Validate::is($changes_id,'Digits'))? $changes_id : 0;

	    if($changes_id)
	    {
	    	$resultdata = $model->getTempBis($changes_id);

			if(!empty($resultdata))
			{
				$app = Zend_Registry::get('config');
				$app = $app['app']['bankcode'];

				$cif = $resultdata['CUST_CIF'];

				$svcAccount = new Service_Account($cif, '360', $app);
				$result		= $svcAccount->cifAccountInquiry();

				$responseCode = $result['ResponseCode'];
				$responseDesc = $result['ResponseDesc'];

				//$responseCode = '00';

				if($responseCode == '00'){
					$this->view->company_cif = $cif;
					$this->view->company_name = $result['AccountName'];
					//$this->view->company_name = $resultdata['CUST_NAME'];

					//nantinya mau ada service utk inquiry company

					foreach($resultdata as $key => $val){
						$fieldname = strtolower($key);
						$this->view->$fieldname = $val;
					}

					$branchArr = Application_Helper_Array::listArray($model->getBranch(),'CODE','NAME');

					$this->view->branch_name = $branchArr['001'];

					$this->view->countryArr = Application_Helper_Array::listArray($model->getCountry(),'COUNTRY_CODE','COUNTRY_NAME');
				}
				else{
					$this->view->error = 1;
					$this->view->message = 'Error : '.$responseDesc;
				}
			}
			else{ $changes_id = 0; }
		}

	    if(!$changes_id)
	    {
	      	$error_remark = $this->language->_('Company data is not found');

		    $this->view->error = 1;
		    $this->view->message = $error_remark;
	    }

	   	//Application_Helper_General::writeLog('CCCL','View Customer Change List : '. $user_id);
	}
}