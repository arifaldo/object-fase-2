<?php
require_once 'Zend/Controller/Action.php';

class registerechannel_CheckaccController extends Application_Main{

  	public function indexAction()
  	{
		$this->_helper->layout()->setLayout('popup');

	    $model = new registerechannel_Model_Registerechannel();
	    $id = $this->_getParam('id');
	    $id = (Zend_Validate::is($id,'Digits'))? $id : 0;

	    $type = $this->_getParam('type');

	    if($id)
	    {
	    	if($type == 'bis')
	    		$resultdata = $model->getBisAcctDet($id);
	    	else
	    		$resultdata = $model->getIndAcctDet($id);

			if(!empty($resultdata))
			{
				$app = Zend_Registry::get('config');
				$app = $app['app']['bankcode'];

				$acct_no = $resultdata['ACCT_NO'];
				$ccy_code = Application_Helper_General::getCurrNum($resultdata['ACCT_CCY']);

				$svcAccount = new Service_Account($acct_no, $ccy_code, $app);
				$result		= $svcAccount->accountInquiry();

				$responseCode = $result['ResponseCode'];
				$responseDesc = $result['ResponseDesc'];

				//$responseCode = '00';

				// $result['Cif'] = '123000123';
				// $result['AccountName'] = 'Yunita';
				// $result['Currency'] = 'IDR';

				if($responseCode == '00'){
					$this->view->acct_cif = $result['Cif'];
					$this->view->acct_no = $acct_no;
					$this->view->acct_name = $result['AccountName'];
					$this->view->acct_ccy = $result['Currency'];
				}
				else{
					$this->view->error = 1;
					$this->view->message = 'Error : '.$responseDesc;
				}
			}
			else{ $id = 0; }
		}

	    if(!$id)
	    {
	      	$error_remark = $this->language->_('Account data is not found');

		    $this->view->error = 1;
		    $this->view->message = $error_remark;
	    }

	   	//Application_Helper_General::writeLog('CCCL','View Customer Change List : '. $user_id);
	}
}