<?php
require_once 'Zend/Controller/Action.php';

class registerechannel_CheckuserController extends Application_Main{

  	public function indexAction()
  	{
		$this->_helper->layout()->setLayout('popup');

	    $model = new registerechannel_Model_Registerechannel();
	    $id = $this->_getParam('id');
	    $id = (Zend_Validate::is($id,'Digits'))? $id : 0;

	    if($id)
	    {
			$result = $model->getBisUserDet($id);
			if(!empty($result))
			{
				foreach($result as $key => $val){
					$fieldname = strtolower($key);
					$this->view->$fieldname = (empty($val))? "-" : $val;
				}
			}
			else{ $id = 0; }
		}

	    if(!$id)
	    {
	      	$error_remark = $this->language->_('User data is not found');

		    $this->view->error = 1;
		    $this->view->message = $error_remark;
	    }

	   	//Application_Helper_General::writeLog('CCCL','View Customer Change List : '. $user_id);
	}
}