<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class bgreport_OngoingController extends Application_Main
{

  protected $_moduleDB = 'RTF'; // masih harus diganti

  public function detailAction()
  {
    $this->_helper->_layout->setLayout('newlayout');

    $bg = $this->_getParam('bg');

    $sessToken  = new Zend_Session_Namespace('Tokenenc');
    $password   = $sessToken->token;

    $AESMYSQL = new Crypt_AESMYSQL();
    $decryption = urldecode($bg);
    $decryption_bg = $AESMYSQL->decrypt($decryption, $password);

    $conf = Zend_Registry::get('config');

    $select = $this->_db->select()
      ->from(
        array('TBG' => 'TEMP_BANK_GUARANTEE'),
        array('*')
      )
      ->joinLeft(
        array('MB' => 'M_BRANCH'),
        'MB.BRANCH_CODE = TBG.BG_BRANCH',
        array('BRANCH_NAME')
      )
      ->joinLeft(
        array('MC' => 'M_CUSTOMER'),
        'MC.CUST_ID = TBG.CUST_ID',
        array(
          'CUST_ID',
          'CUST_NAME',
          'CUST_NPWP',
          'CUST_ADDRESS',
          'CUST_CITY',
          'CUST_FAX',
          'CUST_CONTACT',
          'CUST_PHONE',
        )
      )
      ->joinLeft(
        array('MCST' => 'M_CUSTOMER'),
        'MCST.CUST_ID = TBG.SP_OBLIGEE_CODE',
        array(
          "SP_OBLIGEE_NAME" => 'MCST.CUST_NAME',
        )
      )
      ->joinLeft(
        array('INSURANCE' => 'M_CUSTOMER'),
        'INSURANCE.CUST_ID = TBG.BG_INSURANCE_CODE',
        array(
          "INSURANCE_NAME" => 'INSURANCE.CUST_NAME',
        )
      )
      ->joinLeft(
        array('MCL' => 'M_CITYLIST'),
        'MCL.CITY_CODE = MC.CUST_CITY',
        array('CITY_NAME')
      )
      ->joinLeft(
        array('TBGD' => 'TEMP_BANK_GUARANTEE_DETAIL'),
        'TBGD.BG_REG_NUMBER = TBG.BG_REG_NUMBER',
        array('TBGD.USER_ID', 'TBGD.PS_FIELDNAME')
      )
      ->joinLeft(
        array('TBGS' => 'TEMP_BANK_GUARANTEE_SPLIT'),
        'TBGS.BG_NUMBER = TBG.BG_REG_NUMBER',
        [
          "ACCT", "BANK_CODE", "NAME", "AMOUNT", "FLAG"
        ]
      )
      ->where('TBG.BG_REG_NUMBER = ?', $decryption_bg);
    //->where('TBG.CUST_ID = ?', $this->_custIdLogin);
    //die();
    $auth = Zend_Auth::getInstance()->getIdentity();
    if ($auth->userHeadQuarter == "NO") {
      $select->where('MB.ID = ?', $auth->userBranchId); // Add Bahri
    }
    $data = $this->_db->fetchRow($select);

    if ($data['BG_OLD']) {
      $bgOld = $this->_db->select()
        ->from('T_BANK_GUARANTEE', ['BG_REG_NUMBER', 'PROVISION_FEE'])
        ->where('BG_NUMBER = ?', $data['BG_OLD'])
        ->query()->fetch();

      $this->view->bgRegNumberOld = $bgOld['BG_REG_NUMBER'];
      $this->view->prevProv = $bgOld['PROVISION_FEE'];
    }

    $settings = new Settings();
    $claim_period = $settings->getSettingNew('max_claim_period');
		$this->view->BG_CLAIM_PERIOD = $claim_period;

    $BG_NUMBER = $decryption_bg;
    $this->view->BG_REG_NUMBER = $data['BG_REG_NUMBER'];

    if ($data['COUNTER_WARRANTY_TYPE'] == '3') {
      $this->view->isinsurance = true;
    }

    // history
    $selectapprover    = $this->_db->select()
      ->from(array('C' => 'T_BGAPPROVAL'), array(
        'USER_ID'
      ))
      // ->where("C.USER_ID 	= ?", (string) $this->_userIdLogin)
      ->where("C.REG_NUMBER = ?", (string) $data['BG_REG_NUMBER']);
    // ->where("C.GROUP = ?" , (string)$value);
    // echo $selectapprover;die;
    $usergroup = $this->_db->fetchAll($selectapprover);
      
    if (!empty($usergroup)) {
      // die;
      // $this->view->pdf = true;
      $this->view->validbtn = false;
    } else {
      if ($data['COUNTER_WARRANTY_TYPE'] == '1') {
        $boundary = $this->validatebtn('50', $data['BG_AMOUNT'], 'IDR', $BG_NUMBER);
      } else {
        $boundary = $this->validatebtn('51', $data['BG_AMOUNT'], 'IDR', $BG_NUMBER);
      }
      if ($boundary) {
        // die;
        $this->view->validbtn = false;
      } else {
        // die;
        $this->view->validbtn = true;
      }
    }

    if ($data['COUNTER_WARRANTY_TYPE'] == '1') {
      $policyBoundary = $this->findPolicyBoundary(50, $data['BG_AMOUNT']);
      $approverUserList = $this->findUserBoundary(50, $data['BG_AMOUNT']);
    } else {
      $policyBoundary = $this->findPolicyBoundary(51, $data['BG_AMOUNT']);
      $approverUserList = $this->findUserBoundary(51, $data['BG_AMOUNT']);
    }
    //echo '<pre>';
    //var_dump($policyBoundary);die;
    $this->view->policyBoundary = $policyBoundary;

    $selectcomp = $this->_db->select()
      ->from(array('A' => 'M_CUSTOMER'), array('*'))
      //  ->joinLeft(array('B' => 'M_COUNTRY'),'A.COUNTRY_CODE = B.COUNTRY_CODE',array('COUNTRY_NAME'))
      ->joinRight(array('B' => 'M_CITYLIST'), 'A.CUST_CITY = B.CITY_CODE', array('B.CITY_NAME'))
      ->where('A.CUST_ID =' . $this->_db->quote((string)$data['CUST_ID']))
      ->query()->fetchAll();

    $selectHistory    = $this->_db->select()
      ->from('T_BANK_GUARANTEE_HISTORY')
      ->where("BG_REG_NUMBER = ?", $BG_NUMBER);

    $history = $this->_db->fetchAll($selectHistory);

    $cust_approver = 1;

    $bg_submission_hisotrys = $this->_db->select()
      ->from('T_BANK_GUARANTEE_HISTORY')
      ->where("BG_REG_NUMBER = ?", $BG_NUMBER)
      ->where("CUST_ID IN (?) ", [$selectcomp[0]["CUST_ID"], $data["BG_INSURANCE_CODE"]]);

    $bg_submission_hisotrys = $this->_db->fetchAll($bg_submission_hisotrys);

    $getBoundary = $this->_db->select()
      ->from('M_APP_BOUNDARY')
      ->where('CUST_ID = ?', $data['CUST_ID'])
      ->where('BOUNDARY_MIN <= \'' . $data['BG_AMOUNT'] . '\'')
      ->where('BOUNDARY_MAX >= \'' . $data['BG_AMOUNT'] . '\'')
      ->query()->fetch();

    $checkBoundary = strpos($getBoundary['POLICY'], 'THEN');
    if ($checkBoundary === false) $checkBoundary = strpos($getBoundary['POLICY'], 'AND');
    $checkCount = 1;

    foreach ($bg_submission_hisotrys as $bg_submission_hisotry) {

      // maker
      if ($bg_submission_hisotry['HISTORY_STATUS'] == 1 || $bg_submission_hisotry['HISTORY_STATUS'] == 33) {
        $makerStatus = 'active';
        $makerIcon = '<i class="fas fa-check"></i>';

        $custlogin = $bg_submission_hisotry['USER_LOGIN'];

        $selectCust    = $this->_db->select()
          ->from('M_USER')
          ->where("USER_ID = ?", $custlogin)
          ->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

        $customer = $this->_db->fetchAll($selectCust);

        $custFullname = $customer[0]['USER_FULLNAME'];
        // $custFullname = $customer[0]['USER_ID'];

        $efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));

        $align = 'align="center"';

        $this->view->makerBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
      }

      // approver
      if ($checkBoundary === false) {
        if ($bg_submission_hisotry['HISTORY_STATUS'] == 2) {
          $approverStatus = 'active';
          $approverIcon = '<i class="fas fa-check"></i>';

          $custlogin = $bg_submission_hisotry['USER_LOGIN'];

          $selectCust    = $this->_db->select()
            ->from('M_USER')
            ->where("USER_ID = ?", $custlogin)
            ->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

          $customer = $this->_db->fetchAll($selectCust);

          $custFullname = $customer[0]['USER_FULLNAME'];
          // $custFullname = $customer[0]['USER_ID'];

          $efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));

          $align = 'align="center"';

          $this->view->approverBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';

          $this->view->approverStatus = $approverStatus;
        }
      } else {
        if ($bg_submission_hisotry['HISTORY_STATUS'] == 2) {
          $approverStatus = 'active';
          $approverIcon = '<i class="fas fa-check"></i>';

          $custlogin = $bg_submission_hisotry['USER_LOGIN'];

          $selectCust    = $this->_db->select()
            ->from('M_USER')
            ->where("USER_ID = ?", $custlogin)
            ->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

          $customer = $this->_db->fetchAll($selectCust);

          if ($checkCount === 1) {
            $custFullname = $customer[0]['USER_FULLNAME'];
            // $custFullname = $customer[0]['USER_ID'];
            $checkCount++;
          } else {
            $custFullname = $custFullname . "<br><span>" . $customer[0]['USER_FULLNAME'] . "</span>";
            $checkCount = 1;
          }

          $efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));

          $align = 'align="center"';

          $this->view->approverBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';

          $this->view->approverStatus = $approverStatus;
        }
      }

      if ($data["COUNTER_WARRANTY_TYPE"] == '3') {
        //if releaser
        if ($bg_submission_hisotry['HISTORY_STATUS'] == 5 || $bg_submission_hisotry['HISTORY_STATUS'] == 10) {
          $releaserStatus = 'active';
          $releaserIcon = '<i class="fas fa-check"></i>';

          $custlogin = $bg_submission_hisotry['USER_LOGIN'];

          $selectCust    = $this->_db->select()
            ->from('M_USER')
            ->where("USER_ID = ?", $custlogin)
            ->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

          $customer = $this->_db->fetchAll($selectCust);

          $custFullname = $customer[0]['USER_FULLNAME'];
          // $custFullname = $customer[0]['USER_ID'];

          $efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));

          $align = 'align="center"';

          $this->view->releaserBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
          $this->view->releaserStatus = $releaserStatus;
        }
      } else {
        //if releaser
        if ($bg_submission_hisotry['HISTORY_STATUS'] == 5) {
          $releaserStatus = 'active';
          $releaserIcon = '<i class="fas fa-check"></i>';

          $custlogin = $bg_submission_hisotry['USER_LOGIN'];

          $selectCust    = $this->_db->select()
            ->from('M_USER')
            ->where("USER_ID = ?", $custlogin)
            ->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

          $customer = $this->_db->fetchAll($selectCust);

          $custFullname = $customer[0]['USER_FULLNAME'];
          // $custFullname = $customer[0]['USER_ID'];

          $efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));

          $align = 'align="center"';

          $this->view->releaserBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
          $this->view->releaserStatus = $releaserStatus;
        }
      }


      // if ($bg_submission_hisotry['HISTORY_STATUS'] == 4 || $bg_submission_hisotry['HISTORY_STATUS'] == 14) {
      // 	$releaserStatus = 'active';
      // 	$releaserIcon = '<i class="fas fa-check"></i>';

      // 	$custlogin = $bg_submission_hisotry['USER_LOGIN'];

      // 	$selectCust	= $this->_db->select()
      // 		->from('M_USER')
      // 		->where("USER_ID = ?", $custlogin)
      // 		->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

      // 	$customer = $this->_db->fetchAll($selectCust);

      // 	$custFullname = $customer[0]['USER_FULLNAME'];

      // 	$efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));

      // 	$align = 'align="center"';

      // 	$this->view->releaserBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span></span></div>';
      // 	$this->view->releaserStatus = $releaserStatus;
      // }
    }

    foreach ($history as $row) {

      if ($data["COUNTER_WARRANTY_TYPE"] == '3') {
        if ($row['HISTORY_STATUS'] == 8 || $row['HISTORY_STATUS'] == 24) {
          $makerStatus = 'active';
          $insuranceIcon = '<i class="fas fa-check"></i>';
          /*$approveStatus = 'active';
							$reviewStatus = 'active';
							$releaseStatus = 'active';
							$releaseIcon = '<i class="fas fa-check"></i>';*/
          $insuranceStatus = 'active';
          $reviewStatus = '';
          //$releaseStatus = '';
          //$releaseIcon = '';

          $makerOngoing = '';
          $reviewerOngoing = '';
          $approverOngoing = '';
          //$releaserOngoing = '';

          $custlogin = $row['USER_LOGIN'];

          $selectCust    = $this->_db->select()
            ->from('M_USER')
            ->where("USER_ID = ?", $custlogin);
          //->where("CUST_ID = ?", $row['CUST_ID']);

          $customer = $this->_db->fetchAll($selectCust);

          // $custFullname = $customer[0]['USER_ID'];
          $custFullname = $customer[0]['USER_FULLNAME'];
          // $custEmail 	  = $customer[0]['USER_EMAIL'];
          // $custPhone	  = $customer[0]['USER_PHONE'];

          $insuranceApprovedBy = $custFullname;

          $align = 'align="center"';
          $marginLeft = '';
          if ($cust_reviewer == 0 && $cust_approver == 0) {
            $align = '';
            $marginLeft = 'style="margin-left: 15px;"';
          }

          $efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
          $this->view->insuranceApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
        }
      }

      if ($row['HISTORY_STATUS'] == 6) {
        $verifyStatus = 'active';
        $verifyIcon = '<i class="fas fa-check"></i>';

        $verifyOngoing = '';
        if ($cust_reviewer == 0 && $cust_approver == 0) { //kalau tidak ada priv reviewer & approver
          $reviewerOngoing = '';
          $verifyOngoing = '';
          $approverOngoing = '';
          //$releaserOngoing = 'ongoing';
          $releaserOngoing = '';
        } else {
          //$reviewerOngoing = 'ongoing';
          $reviewerOngoing = '';
          $verifyOngoing = '';
          $approverOngoing = '';
          $releaserOngoing = '';
        }

        $custlogin = $row['USER_LOGIN'];

        $selectCust    = $this->_db->select()
          ->from('M_BUSER')
          ->where("BUSER_ID = ?", $custlogin);

        $customer = $this->_db->fetchAll($selectCust);

        $custFullname = $customer[0]['BUSER_NAME'];

        $efdate = date('d-M-Y', strtotime($row['DATE_TIME']));

        $align = 'align="center"';
        $marginRight = '';

        if ($cust_reviewer == 0 && $cust_approver == 0) {
          $align = '';
          $marginRight = 'style="margin-right: 15px;"';
        }

        $this->view->verifyApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span ' . $marginRight . '>' . $custFullname . '</span></div>';
      }

      //if reviewer done
      if ($row['HISTORY_STATUS'] == 7) {
        $bankReviewStatus = "active";
        $bankReviewOngoing = "ongoing";
        $bankReviewIcon = '<i class="fas fa-check"></i>';

        $custlogin = $row['USER_LOGIN'];

        $selectCust    = $this->_db->select()
          ->from('M_BUSER')
          ->where("BUSER_ID = ?", $custlogin);

        $customer = $this->_db->fetchAll($selectCust);

        $custFullname = $customer[0]['BUSER_NAME'];

        $align = 'align="center"';

        $efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
        $this->view->bankReviewedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
      }

      //if releaser done
      if ($row['HISTORY_STATUS'] == 14) {
        $makerStatus = 'active';
        /*$approveStatus = 'active';
						$reviewStatus = 'active';
						$releaseStatus = 'active';
						$releaseIcon = '<i class="fas fa-check"></i>';*/
        $bankApproveStatus = 'active';
        $bankApproveIcon = '<i class="fas fa-check"></i>';
        $bankApproveOngoing = '';
        $reviewStatus = '';
        $releaseStatus = '';
        $releaseIcon = '';

        $makerOngoing = '';
        $reviewerOngoing = '';
        $approverOngoing = '';
        $releaserOngoing = '';

        $custlogin = $row['USER_LOGIN'];

        $selectCust    = $this->_db->select()
          ->from('M_BUSER')
          ->where("BUSER_ID = ?", $row['USER_LOGIN']);

        $customer = $this->_db->fetchAll($selectCust);

        $custFullname = $customer[0]['BUSER_NAME'];
        // $custEmail 	  = $customer[0]['USER_EMAIL'];
        // $custPhone	  = $customer[0]['USER_PHONE'];

        $releaserApprovedBy = $custFullname;

        $align = 'align="center"';
        $marginLeft = '';
        // if ($cust_reviewer == 0 && $cust_approver == 0) {
        //     $align = '';
        //     $marginLeft = 'style="margin-left: 15px;"';
        // }

        $efdate = date('d-M-Y', strtotime($row['DATE_TIME']));

        $getApprover = $this->_db->select()
          ->from(['TBA' => 'T_BGAPPROVAL'], ['*'])
          ->joinLeft(['MB' => 'M_BUSER'], 'TBA.USER_ID = MB.BUSER_ID', ['BUSER_NAME'])
          ->where('REG_NUMBER = ?', $data['BG_REG_NUMBER'])
          ->query()->fetchAll();

        $approverText = '';
        foreach ($getApprover as $userApprove) {
          # code...
          $approverText .= '<div ' . $align . ' class="textTheme">' . date('d-M-Y', strtotime($userApprove['CREATED'])) . '<br><span ' . $marginRight . '>' . $userApprove['BUSER_NAME'] . '</span></div>';
        }

        $this->view->bankApproverBy = $approverText;
      }

      if ($row['HISTORY_STATUS'] == 17) {
        $signPrepareStatus = 'active';
        $signPrepareIcon = '<i class="fas fa-check"></i>';

        $custlogin = $row['USER_LOGIN'];

        $selectCust    = $this->_db->select()
          ->from('M_BUSER')
          ->where("BUSER_ID = ?", $row['USER_LOGIN']);

        $customer = $this->_db->fetchAll($selectCust);

        $custFullname = $customer[0]['BUSER_NAME'];

        // $releaserApprovedBy = $custFullname;

        $align = 'align="center"';

        $efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
        $this->view->signPrepareBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
      }
    }

    if ($data["DOCUMENT_ID"]) {
      $checkSigner = $this->_db->select()
        ->from("T_BANK_GUARANTEE_SIGNER")
        ->where("DOCUMENT_ID = ?", $data["DOCUMENT_ID"])
        ->query()->fetchAll();

      if ($checkSigner) {
        $getCustFullName = $this->_db->select()
          ->from("M_BUSER", ["BUSER_NAME"])
          // ->where("BUSER_EMAIL = ?", $checkSigner[0]["SIGNER_EMAIL"])
          ->where("BUSER_ID = ?", $checkSigner[0]["SIGNER_NAME"])
          ->query()->fetch();

        $efdate = date("d-M-Y", strtotime($checkSigner[0]["SIGNER_DATE"]));
        $this->view->checkSign = true;
        $this->view->signer1 = '<div align="center" class="textTheme">' . $efdate . '<br><span>' . $getCustFullName["BUSER_NAME"] . '</span></div>';
      }
    }

    $this->view->updateEnd = Application_Helper_General::convertDate($data['TIME_PERIOD_END'], $this->view->viewDateFormat, $this->view->defaultDateFormat);


    //approvernamecircle jika sudah ada yang approve
    if (!empty($userid)) {

      $alphabet = array('01' => 'A', '02' => 'B', '03' => 'C', '04' => 'D', '05' => 'E', '06' => 'F', '07' => 'G', '08' => 'H', '09' => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z', 27 => 'SG');

      $flipAlphabet = array_flip($alphabet);

      $approvedNameList = array();
      $i = 0;
      //var_dump($userid);die;
      foreach ($userid as $key => $value) {

        //select utk nama dan email
        $selectusername = $this->_db->select()
          ->from(array('M_BUSER'), array(
            '*'
          ))
          ->where("BUSER_ID = ?", (string) $value);

        $username = $this->_db->fetchAll($selectusername);

        //select utk cek user berada di grup apa
        $selectusergroup    = $this->_db->select()
          ->from(array('C' => 'M_APP_GROUP_BUSER'), array(
            '*'
          ))

          ->where("C.BUSER_ID 	= ?", (string) $value);

        $usergroup = $this->_db->fetchAll($selectusergroup);

        $groupuserid = $usergroup[0]['GROUP_BUSER_ID'];
        $groupusername = $usergroup[0]['BUSER_ID'];
        $groupuseridexplode = explode("_", $groupuserid);

        if ($groupuseridexplode[0] == "S") {
          $usergroupid = "SG";
        } else {
          $usergroupid = $alphabet[$groupuseridexplode[2]];
        }

        // $tempuserid = "";
        // foreach ($approverNameCircle as $row => $data) {
        // 	foreach ($data as $keys => $val) {
        // 		if ($keys == $usergroupid) {
        // 			if (preg_match("/active/", $val)) {
        // 				continue;
        // 			}else{
        // 				if ($groupuserid == $tempuserid) {
        // 					continue;
        // 				}else{
        // 					$approverNameCircle[$row][$keys] = '<button class="btnCircleGroup active hovertext" style="margin-right: 1%; margin-top: 12%;" disabled>3
        // 			<span class="hovertextcontent" style="padding-left: 15px;">'.$username[0]['USER_FULLNAME'].'<br>'.$username[0]['USER_EMAIL'].'</span></button>';
        // 				}
        // 				$tempuserid = $groupuserid;
        // 			}
        // 		}
        // 	}
        // }

        array_push($approvedNameList, $username[0]['BUSER_NAME']);

        $efdate = $approveEfDate[$i];

        $approverApprovedBy[] = '<div align="center" class="textTheme">' . $efdate . '<br>' . $username[0]['BUSER_NAME'] . ' (' . $usergroupid . ')</div>';
        $i++;
      }

      $this->view->approverApprovedBy = $approverApprovedBy;


      //kalau sudah approve semua
      if (!$checkBoundary) {
        $approveStatus = '';
        $approverOngoing = '';
        $approveIcon = '';
        $releaserOngoing = 'ongoing';
      }
    }

    $selectsuperuser = $this->_db->select()
      ->from(array('C' => 'T_BGAPPROVAL'))
      ->where("C.REG_NUMBER 	= ?", $BG_NUMBER)
      ->where("C.GROUP 	= 'SG'");

    $superuser = $this->_db->fetchAll($selectsuperuser);

    if (!empty($superuser)) {
      $userid = $superuser[0]['USER_ID'];

      //select utk nama dan email
      $selectusername = $this->_db->select()
        ->from(array('M_BUSER'), array(
          '*'
        ))
        ->where("BUSER_ID = ?", (string) $userid);

      $username = $this->_db->fetchAll($selectusername);

      $approverApprovedBy[] = '<div align="center" class="textTheme">' . $efdate . '<br>' . $username[0]['BUSER_NAME'] . ' (' . $usergroupid . ')</div>';

      $approveStatus = '';
      $approverOngoing = '';
      $approveIcon = '';
      $releaserOngoing = 'ongoing';
    }
    // <span class="hovertextcontent" style="text-align: center;"><p class="m-0 text-white-50" style="font-size: 13px">'.$makerApprovedBy.'</p></span>
    //define circle
    $makerNameCircle = '<button id="makerCircle" style="cursor:default" class="btnCircleGroup ' . $makerStatus . ' ' . $makerOngoing . ' hovertext" disabled>' . $makerIcon . ' </button>';

    $insuranceNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $insuranceStatus . ' ' . $insuranceOngoing . ' hovertext" disabled>' . $insuranceIcon . ' </button>';

    foreach ($reviewerList as $key => $value) {

      $textColor = '';
      if ($value == $reviewerApprovedBy) {
        $textColor = 'text-white-50';
      }

      $reviewerListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value . '</p>';
    }
    // 
    $bankReviewNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $bankReviewStatus . ' hovertext" disabled>' . $bankReviewIcon . ' </button>';

    $groupNameList = $approverUserList['GROUP_NAME'];
    unset($approverUserList['GROUP_NAME']);

    if ($approverUserList != '') {
      //echo '<pre>';
      //var_dump($approverUserList);die;
      foreach ($approverUserList as $key => $value) {
        $approverListdata .= $key . ' (' . $groupNameList[$key] . ')' . '<br>';
        $i = 1;
        foreach ($value as $key2 => $value2) {

          $textColor = '';
          if (in_array($value2, $approvedNameList)) {
            $textColor = 'text-white-50';
          }

          if ($i == count($value)) {
            $approverListdata .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value2 . '</p><br>';
          } else {
            $approverListdata .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value2 . '</p>';
          }
          $i++;
        }
      }
    } else {
      $approverListdata = 'There is no Approver User';
    }
    // 
    $spandata = '';
    if (!empty($approverListdata) && !$error_msg2) {
      $spandata = '<span class="hovertextcontentapprover" style="text-align: center;">' . $approverListdata . '</span>';
    }

    $approverNameCircle = '<button class="btnCircleGroup ' . $approveStatus . ' ' . $approverOngoing . ' hovertext" disabled>' . $approveIcon . '
						' . $spandata . '
					</button>';

    foreach ($releaserList as $key => $value) {

      $textColor = '';
      if ($value == $releaserApprovedBy) {
        $textColor = 'text-white-50';
      }

      $releaserListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value . '</p>';
    }
    // 
    // $releaserNameCircle = '<button id="releaserCircle" class="btnCircleGroup ' . $releaseStatus . ' ' . $releaserOngoing . ' hovertext" disabled>' . $releaseIcon . ' <span class="hovertextcontent" style="left: -100px; text-align: center;">' . $releaserListView . '</span> </button>';
    $releaserNameCircle = '<button id="releaserCircle" class="btnCircleGroup ' . $releaseStatus . ' ' . $releaserOngoing . ' hovertext" disabled>' . $releaseIcon . '</button>';

    $verifyNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $verifyStatus . ' ' . $verifyOngoing . ' hovertext" disabled>' . $verifyIcon . ' </button>';

    $bankApproverNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $bankApproveStatus . ' ' . $bankApproveOngoing . ' hovertext" disabled>' . $bankApproveIcon . ' </button>';

    $signPrepareNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $signPrepareStatus . ' hovertext" disabled>' . $signPrepareIcon . ' </button>';

    $this->view->policyBoundary = $policyBoundary;
    $this->view->makerNameCircle = $makerNameCircle;
    $this->view->bankReviewNameCircle = $bankReviewNameCircle;
    $this->view->approverNameCircle = $approverNameCircle;
    $this->view->signPrepareNameCircle = $signPrepareNameCircle;
    $this->view->bankApprover = $bankApproverNameCircle;
    $this->view->releaserNameCircle = $releaserNameCircle;
    $this->view->verifyNameCircle = $verifyNameCircle;
    $this->view->insuranceNameCircle = $insuranceNameCircle;

    $this->view->verifyStatus = $verifyStatus;
    $this->view->makerStatus = $makerStatus;
    $this->view->approveStatus = $approveStatus;
    $this->view->signPrepareStatus = $signPrepareStatus;
    $this->view->reviewStatus = $reviewStatus;
    $this->view->releaseStatus = $releaseStatus;
    // end history

    $kontra = $data['COUNTER_WARRANTY_TYPE'];
    $timePeriodStart = $data['TIME_PERIOD_START'];
    $timePeriodEnd = $data['TIME_PERIOD_END'];
    $bgAmount = $data['BG_AMOUNT'];
    $custId = $data['CUST_ID'];
    $usagePurpose = $data['USAGE_PURPOSE'];
    $usagePurposeDesc = $data['USAGE_PURPOSE_DESC'];
    $insuranceCode = $data['BG_INSURANCE_CODE'];

    // penkondisian kontra biaya provisi
    if ($kontra == '3') {

      $paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $kontra;
      $paramProvisionFee['CUST_ID'] = $insuranceCode;
      $paramProvisionFee['GRUP'] = $custId;
      $paramProvisionFee['USAGE_PURPOSE'] = $usagePurpose;
      $paramProvisionFee['USAGE_PURPOSE_DESC'] = $usagePurposeDesc;
      $getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);

      $provisionPercentage = $getProvisionFee['provisionFee'];

      $getLfIns = $this->_db->select()
        ->from(["MCLF" => "M_CUST_LINEFACILITY"], ["FEE_DEBITED"])
        ->where("CUST_ID = ?", $insuranceCode)
        ->query()->fetch();

      $this->view->feeDebited = $getLfIns['FEE_DEBITED'];
    }

    if ($kontra == '1') {
      $paramProvisionFee['TIME_PERIOD_START'] = $timePeriodStart;
      $paramProvisionFee['TIME_PERIOD_END'] = $timePeriodEnd;
      $paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $kontra;
      $paramProvisionFee['CUST_ID'] = $custId;
      $paramProvisionFee['GRUP'] = $custId;
      $paramProvisionFee['USAGE_PURPOSE'] = $usagePurpose;
      $paramProvisionFee['USAGE_PURPOSE_DESC'] = $usagePurposeDesc;
      $getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);

      $provisionPercentage = $getProvisionFee['provisionFee'];
    }

    if ($kontra == '2') {

      $paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $kontra;
      $paramProvisionFee['CUST_ID'] = $custId;
      $paramProvisionFee['GRUP'] = $custId;
      $paramProvisionFee['USAGE_PURPOSE'] = $usagePurpose;
      $paramProvisionFee['USAGE_PURPOSE_DESC'] = $usagePurposeDesc;
      $getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);

      $provisionPercentage = $getProvisionFee['provisionFee'];
    }
    // end pengkondisian kontra

    $provisi = Application_Helper_General::countProvision($kontra, ['TIME_PERIOD_START' => $timePeriodStart, 'TIME_PERIOD_END' => $timePeriodEnd, 'BG_AMOUNT' => $bgAmount], $provisionPercentage);

    $this->view->allToProvision = [$provisionPercentage, $provisi, $getProvisionFee];

    $CustomerUser = new CustomerUser($data['CUST_ID'], $this->_userIdLogin);

    $param = array('CCY_IN' => 'IDR', 'ACCT_NO' => $data['FEE_CHARGE_TO']);
    $AccArr = $CustomerUser->getAccountsBG($param);

    if (!empty($AccArr)) {
      $this->view->src_name = $AccArr['0']['ACCT_NAME'];
      $this->view->currency_type = $AccArr['0']['CCY_ID'];
    }

    if (!empty($data['BG_OLD'])) {
      $tbgdata = $this->_db->select()
        ->from(["A" => "T_BANK_GUARANTEE"], ["*"])
        ->joinLeft(array('B' => 'M_CUSTOMER'), 'A.BG_INSURANCE_CODE = B.CUST_ID', array('B.CUST_NAME'))
        // ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
        ->where('A.BG_NUMBER = ?', $data['BG_OLD'])
        ->query()->fetch();
    }

    $getGuarantedTransanctions = $this->_db->select()
      ->from('TEMP_BANK_GUARANTEE_UNDERLYING')
      ->where('BG_REG_NUMBER = ?', $decryption_bg)
      ->query()->fetchAll();

    $this->view->guarantedTransanctions = $getGuarantedTransanctions;

    if (!empty($tbgdata['BG_REG_NUMBER'])) {
      $getGuarantedTransanctionsT = $this->_db->select()
        ->from('T_BANK_GUARANTEE_UNDERLYING')
        ->where('BG_REG_NUMBER = ?', $tbgdata['BG_REG_NUMBER'] ? $tbgdata['BG_REG_NUMBER'] : '')
        ->query()->fetchAll();

      $this->view->guarantedTransanctionsT = $getGuarantedTransanctionsT;
    }

    switch ($data["CHANGE_TYPE"]) {
      case '0':
        $this->view->suggestion_type = "New";
        break;
      case '1':
        $this->view->suggestion_type = "Amendment Changes";
        break;
      case '2':
        $this->view->suggestion_type = "Amendment Draft";
        break;
    }

    //echo '<pre>';print_r($data);
    $conf = Zend_Registry::get('config');
    // BG TYPE
    $bgType         = $conf["bg"]["type"]["desc"];
    $bgCode         = $conf["bg"]["type"]["code"];

    $arrbgType = array_combine(array_values($bgCode), array_values($bgType));

    $this->view->arrbgType = $arrbgType;

    //BG Counter Guarantee Type
    $bgcgType         = $conf["bgcg"]["type"]["desc"];
    $bgcgCode         = $conf["bgcg"]["type"]["code"];

    $arrbgcg = array_combine(array_values($bgcgCode), array_values($bgcgType));

    $this->view->warranty_type_text_new = $arrbgcg[$data['COUNTER_WARRANTY_TYPE']];

    if ($data['COUNTER_WARRANTY_TYPE'] == '1') {

      $bgdatasplit = $this->_db->select()
        ->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
        ->where('A.BG_REG_NUMBER = ?', $decryption_bg)
        ->query()->fetchAll();

      foreach ($bgdatasplit as $key => $value) {
        $temp_save = $this->_db->select()
          ->from("M_CUSTOMER_ACCT")
          ->where("ACCT_NO = ?", $value["ACCT"])
          ->query()->fetchAll();

        $bgdatasplit[$key]["CURRENCY"] = $temp_save[0]["CCY_ID"];
        $bgdatasplit[$key]["TYPE"] = $temp_save[0]["ACCT_DESC"];
      }

      $bgdatasplitT = $this->_db->select()
        ->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
        ->where('A.BG_NUMBER = ?', $data['BG_OLD'] ? $data['BG_OLD'] : '')
        ->query()->fetchAll();

      foreach ($bgdatasplitT as $key => $value) {
        $temp_save = $this->_db->select()
          ->from("M_CUSTOMER_ACCT")
          ->where("ACCT_NO = ?", $value["ACCT"])
          ->query()->fetchAll();

        $bgdatasplitT[$key]["CURRENCY"] = $temp_save[0]["CCY_ID"];
        $bgdatasplitT[$key]["TYPE"] = $temp_save[0]["ACCT_DESC"];
      }

      $this->view->fullmember = $bgdatasplit;
      $this->view->fullmemberA = $bgdatasplit[0];
      $this->view->fullmemberT = $bgdatasplitT[0];
    }


    $sqlbgdetail = $this->_db->select()
      ->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL'), array('*'))
      //->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
      ->where('A.BG_REG_NUMBER = ?', $data["BG_REG_NUMBER"]);
    //  die('a');
    $bgdatadetail = $sqlbgdetail->query()->fetchAll();

    if (!empty($tbgdata['BG_REG_NUMBER'])) {
      $tbgdatadetail = $this->_db->select()
        ->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
        ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
        ->where('A.BG_REG_NUMBER = ?', $tbgdata['BG_REG_NUMBER'])
        ->query()->fetchAll();
    }

    if ($data["COUNTER_WARRANTY_TYPE"] == "3") {
      $getInsuranceBranch = array_search("Insurance Branch", array_column($bgdatadetail, "PS_FIELDNAME"));
      $getInsuranceBranch = $bgdatadetail[$getInsuranceBranch];

      $insuranceBranch = $this->_db->select()
        ->from("M_INS_BRANCH")
        ->where("INS_BRANCH_CODE = ?", $getInsuranceBranch["PS_FIELDVALUE"])
        ->query()->fetchAll();

      $this->view->insuranceBranch = $insuranceBranch[0]["INS_BRANCH_NAME"];
      $this->view->insuranceBranchAcct = $insuranceBranch[0]["INS_BRANCH_ACCT"];
    }

    if (!empty($bgdatadetail)) {
      foreach ($bgdatadetail as $key => $value) {

        if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
          if ($value['PS_FIELDNAME'] == 'Insurance Name') {
            $this->view->insuranceName =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
            $this->view->PrincipalAgreement =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Amount') {
            $this->view->insurance_amount =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Principal Agreement Start Date') {
            $this->view->paDateStart =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Principal Agreement End Date') {
            $this->view->paDateEnd =   $value['PS_FIELDVALUE'];
          }
        } else {

          if ($value['PS_FIELDNAME'] == 'Plafond Owner') {
            $this->view->owner1 =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Amount Owner') {
            $this->view->amountowner1 =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
            $this->view->owner2 =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
            $this->view->amountowner2 =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
            $this->view->owner3 =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
            $this->view->amountowner3 =   $value['PS_FIELDVALUE'];
          }
        }
      }
    }

    if (!empty($tbgdatadetail)) {
      foreach ($tbgdatadetail as $key => $value) {

        if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
          if ($value['PS_FIELDNAME'] == 'Insurance Name') {
            $this->view->tinsuranceName =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
            $this->view->tPrincipalAgreement =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Amount') {
            $this->view->tinsurance_amount =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Principal Agreement Start Date') {
            $this->view->tpaDateStart =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Principal Agreement End Date') {
            $this->view->tpaDateEnd =   $value['PS_FIELDVALUE'];
          }
        } else {

          if ($value['PS_FIELDNAME'] == 'Plafond Owner') {
            $this->view->towner1 =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Amount Owner') {
            $this->view->tamountowner1 =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
            $this->view->towner2 =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
            $this->view->tamountowner2 =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
            $this->view->towner3 =   $value['PS_FIELDVALUE'];
          }

          if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
            $this->view->tamountowner3 =   $value['PS_FIELDVALUE'];
          }
        }
      }
    }

    $principleData = [];
    // if ($data["COUNTER_WARRANTY_TYPE"] == "3") {
    foreach ($bgdatadetail as $key => $value) {
      $principleData[$value["PS_FIELDNAME"]] = $value["PS_FIELDVALUE"];
    }

    $this->view->principleData = $principleData;
    // }

    $this->view->BG_REG_NUMBER = $data["BG_REG_NUMBER"];

    // $provision_insurance = $this->_db->select()
    //   ->from(["A" => "M_CUST_LINEFACILITY"], ["FEE_PROVISION", "FEE_ADMIN", "FEE_STAMP"])
    //   ->where("CUST_ID = ?", $data["BG_INSURANCE_CODE"])
    //   ->query()->fetchAll();

    // if ($data["COUNTER_WARRANTY_TYPE"] == "3") $this->view->provision_insurance = $provision_insurance[0];

    if ($data['COUNTER_WARRANTY_TYPE'] == '1') {

      $paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $data['COUNTER_WARRANTY_TYPE'];
      $paramProvisionFee['TIME_PERIOD_START'] = $data['TIME_PERIOD_START'];
      $paramProvisionFee['TIME_PERIOD_END'] = $data['TIME_PERIOD_END'];
      $getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);
      $this->view->provisionFee = $getProvisionFee['provisionFee'];
      $this->view->adminFee = $getProvisionFee['adminFee'];
      $this->view->stampFee = $getProvisionFee['stampFee'];


      $bgdatasplit = $this->_db->select()
        ->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
        ->where('A.BG_REG_NUMBER = ?', $data['BG_REG_NUMBER'])
        ->query()->fetchAll();

      foreach ($bgdatasplit as $key => $value) {
        $temp_save = $this->_db->select()
          ->from("M_CUSTOMER_ACCT")
          ->where("ACCT_NO = ?", $value["ACCT"])
          ->query()->fetchAll();

        $bgdatasplit[$key]["CURRENCY"] = $temp_save[0]["CCY_ID"];
        $bgdatasplit[$key]["TYPE"] = $temp_save[0]["ACCT_DESC"];
      }

      $this->view->fullmember = $bgdatasplit;
    }

    if ($data["COUNTER_WARRANTY_TYPE"] == "2") {
      $paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $data['COUNTER_WARRANTY_TYPE'];
      $paramProvisionFee['CUST_ID'] = $data['CUST_ID'];
      $paramProvisionFee['USAGE_PURPOSE'] = $data['USAGE_PURPOSE'];
      $paramProvisionFee['USAGE_PURPOSE_DESC'] = $data['USAGE_PURPOSE_DESC'];
      $getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);
      $this->view->provisionFee = $getProvisionFee['provisionFee'];
      $this->view->adminFee = $getProvisionFee['adminFee'];
      $this->view->stampFee = $getProvisionFee['stampFee'];
    }

    if ($data["COUNTER_WARRANTY_TYPE"] == "3") {

      $paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $data['COUNTER_WARRANTY_TYPE'];
      $paramProvisionFee['CUST_ID'] = $data["BG_INSURANCE_CODE"];
      $paramProvisionFee['GRUP'] = $data["CUST_ID"];
      $paramProvisionFee['USAGE_PURPOSE'] = $data['USAGE_PURPOSE'];
      $paramProvisionFee['USAGE_PURPOSE_DESC'] = $data['USAGE_PURPOSE_DESC'];
      $getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);
      $this->view->provisionFee = $getProvisionFee['provisionFee'];
      $this->view->adminFee = $getProvisionFee['adminFee'];
      $this->view->stampFee = $getProvisionFee['stampFee'];

      $getInsuranceBranch = array_search("Insurance Branch", array_column($bgdatadetail, "PS_FIELDNAME"));
      $getInsuranceBranch = $bgdatadetail[$getInsuranceBranch];

      $insuranceBranch = $this->_db->select()
        ->from("M_INS_BRANCH")
        ->where("INS_BRANCH_CODE = ?", $getInsuranceBranch["PS_FIELDVALUE"])
        ->query()->fetchAll();

      $this->view->insuranceBranch = $insuranceBranch[0]["INS_BRANCH_NAME"];
      $this->view->insuranceAcct = $insuranceBranch[0]["INS_BRANCH_ACCT"];
    }

    // Marginal Deposit View ------------------------
    // Marginal Deposit Principal ---------------
    $this->view->bank_amount = $data['BG_AMOUNT'];
    $numb = $data['BG_REG_NUMBER'];
    $bgdatadetailmd = $this->_db->select()
      ->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL'), array('*'))
      ->where('A.BG_REG_NUMBER = ?', $numb)
      ->query()->fetchAll();

    if (!empty($bgdatadetailmd)) {
      foreach ($bgdatadetailmd as $key => $value) {
        if ($value['PS_FIELDNAME'] == 'Marginal Deposit Percentage') {
          $this->view->marginalDepositPercentage =   $value['PS_FIELDVALUE'];
        }
      }
    }
    // Marginal Deposit Principal ---------------

    // Marginal Deposit Eksisting ---------------
    $bgdatamdeks = $this->_db->select()
      ->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
      // ->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
      // ->join(array('B' => 'T_BANK_GUARANTEE_SPLIT'), 'A.BG_REG_NUMBER = B.BG_REG_NUMBER')
      ->where('A.BG_REG_NUMBER = ?', $tbgdata['BG_REG_NUMBER'] ? $tbgdata['BG_REG_NUMBER'] : $numb)
      ->query()->fetchAll();

    $this->view->bgdatamdeks = $bgdatamdeks;
    // Marginal Deposit Eksisting ---------------

    // Top Up Marginal Deposit ---------------
    $bgdatamd = $this->_db->select()
      ->from(array('A' => 'TEMP_BANK_GUARANTEE'), array('*'))
      ->join(array('B' => 'TEMP_BANK_GUARANTEE_SPLIT'), 'A.BG_REG_NUMBER = B.BG_REG_NUMBER')
      ->where('B.BG_REG_NUMBER = ?', $numb)
      ->query()->fetchAll();

    $this->view->bgdatamd = $bgdatamd;
    // Top Up Marginal Deposit ---------------
    // Marginal Deposit View ------------------------

    $get_linefacility = $this->_db->select()
      ->from("M_CUST_LINEFACILITY", ["PLAFOND_LIMIT", "FEE_PROVISION", "FEE_ADMIN", "FEE_STAMP"])
      ->where("CUST_ID = ?", $data["CUST_ID"])
      ->query()->fetchAll();

    $get_bgamount_on_risks = $this->_db->select()
      ->from("T_BANK_GUARANTEE", ["BG_AMOUNT"])
      ->where("BG_STATUS = 20 AND COUNTER_WARRANTY_TYPE = '2' AND CUST_ID = " . $this->_db->quote($data["CUST_ID"]))
      ->query()->fetchAll();

    $total_bgamount_on_risk = 0;

    foreach ($get_bgamount_on_risks as $key => $get_bgamount_on_risk) {
      $total_bgamount_on_risk += $get_bgamount_on_risk["BG_AMOUNT"];
    }

    $total_bgamount_on_temp = 0;

    $get_bgamount_on_temps = $this->_db->select()
      ->from("TEMP_BANK_GUARANTEE", ["BG_AMOUNT"])
      ->where("COUNTER_WARRANTY_TYPE = '2'")
      ->query()->fetchAll();

    foreach ($get_bgamount_on_temps as $key => $get_bgamount_on_temp) {
      $total_bgamount_on_temp += $get_bgamount_on_temp["BG_AMOUNT"];
    }

    $this->view->current_limit = $get_linefacility[0]["PLAFOND_LIMIT"] - $total_bgamount_on_risk - $total_bgamount_on_temp;

    if ($data['COUNTER_WARRANTY_TYPE'] == '2') {
      // get linefacillity

      $paramLimit = array();

      $paramLimit['CUST_ID'] =  $data['CUST_ID'];
      $paramLimit['COUNTER_WARRANTY_TYPE'] = $data['COUNTER_WARRANTY_TYPE'];
      $getLineFacility = Application_Helper_General::getLineFacility($paramLimit);

      $this->view->current_limit = $getLineFacility['currentLimit'];
      $this->view->max_limit =  $getLineFacility['plafondLimit'];
      $this->view->ticketSize =  $getLineFacility['ticketSize'];

      //$this->view->linefacility = $get_linefacility[0];

      // end get linefacility
    }

    $this->view->max_limit = $get_linefacility[0]["PLAFOND_LIMIT"];

    $this->view->linefacility = $get_linefacility[0];

    $this->view->acct = $data['FEE_CHARGE_TO'];

    $conf = Zend_Registry::get('config');
    $this->view->bankname = $conf['app']['bankname'];

    $CustomerUser = new CustomerUser($data['CUST_ID'], $data['BG_CREATEDBY']);
    $param = array('CCY_IN' => 'IDR', 'ACCT_NO' => $data['FEE_CHARGE_TO']);
    $AccArr = $CustomerUser->getAccounts($param);
    // var_dump($data['FEE_CHARGE_TO']);die;

    if (!empty($AccArr)) {
      $this->view->src_name = $AccArr['0']['ACCT_NAME'];
    }

    $feeChargeAcct = $this->_db->select()
      ->from("M_CUSTOMER_ACCT")
      ->where("ACCT_NO = ?", $data["FEE_CHARGE_TO"])
      // ->where("CUST_ID = ?", $data['CUST_ID'])
      ->query()->fetch();

    $this->view->src_name = $feeChargeAcct['ACCT_NAME'];

    if (!empty($principleData["SLIK OJK Document"])) {
      $temp_slikojk = explode("_", $principleData["SLIK OJK Document"]);
      $new_temp_name = $temp_slikojk[0] . "_" . $temp_slikojk[1] . "_" . $temp_slikojk[2] . "_" . $temp_slikojk[3] . "_";

      $this->view->slikojk = str_replace($new_temp_name, "", $principleData["SLIK OJK Document"]);

      if ($data["BG_STATUS"] == 5) {
        $this->view->slikojk = "-";
      }
    }

    $get_cash_collateral = $this->_db->select()
      ->from("M_CHARGES_OTHER", ["CHARGES_PCT", "CHARGES_ADM", "CHARGES_STAMP"])
      ->where("CUST_ID = ?", "GLOBAL")
      ->where("CHARGES_TYPE = ?", "10")
      ->query()->fetchAll();

    $this->view->cash_collateral = $get_cash_collateral[0];

    $bgpublishType     = $conf["bgpublish"]["type"]["desc"];
    $bgpublishCode     = $conf["bgpublish"]["type"]["code"];

    $arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

    $this->view->publishForm = $arrbgpublish[$data['BG_PUBLISH']];

    $arrBankFormat = array(
      1 => 'Bank Standard',
      2 => 'Special Format (with bank approval)'
    );

    $this->view->bankFormat = $arrBankFormat[$data['BG_FORMAT']];
    $this->view->bankFormatNumber = $data['BG_FORMAT'];

    $arrLang = array(
      1 => 'Indonesian',
      2 => 'English',
      3 => 'Billingual',
    );

    $this->view->languagetext = $arrLang[$data['BG_LANGUAGE']];

    $checkOthersAttachment = $this->_db->select()
      ->from(["A" => "TEMP_BANK_GUARANTEE_FILE"], ["*"])
      ->where("BG_REG_NUMBER = '" . $data['BG_REG_NUMBER'] . "'")
      ->order('A.INDEX ASC')
      ->query()->fetchAll();

    if (count($checkOthersAttachment) > 0) {
      $this->view->othersAttachment = $checkOthersAttachment;
    }

    if (!empty($tbgdata['BG_REG_NUMBER'])) {
      $checkOthersAttachmentT = $this->_db->select()
        ->from(["A" => "T_BANK_GUARANTEE_FILE"], ["*"])
        ->where("BG_REG_NUMBER = ?", $tbgdata['BG_REG_NUMBER'])
        ->order('A.INDEX ASC')
        ->query()->fetchAll();

      $this->view->othersAttachmentT = $checkOthersAttachmentT;
    }

    // Get data T_BANK_GUARANTEE_HISTORY
    $select = $this->_db->select()
      ->from(
        array('TBGH' => 'T_BANK_GUARANTEE_HISTORY'),
        array('*')
      )
      ->where('TBGH.BG_REG_NUMBER = ?', $decryption_bg);
    // ->where('TBGH.CUST_ID = ?', $this->_custIdLogin);
    $dataHistory = $this->_db->fetchAll($select);

    // Get data TEMP_BANK_GUARANTEE_SPLIT
    $select = $this->_db->select()
      ->from(
        array('TBGS' => 'TEMP_BANK_GUARANTEE_SPLIT'),
        array('*')
      )
      ->where('TBGS.BG_REG_NUMBER = ?', $decryption_bg);
    $dataAccSplit = $this->_db->fetchAll($select);

    if (!empty($data["AGREE_FORMAT"])) {
      $temp_agreeformat = explode("_", $data["AGREE_FORMAT"]);
      $new_temp_name = $temp_agreeformat[0] . "_" . $temp_agreeformat[1] . "_" . $temp_agreeformat[2] . "_" . $temp_agreeformat[3] . "_";

      $this->view->agreeformat = str_replace($new_temp_name, "", $data["AGREE_FORMAT"]);
    }

    if (!empty($data["MEMO_LEGAL"])) {
      $temp_memolegal = explode("_", $data["MEMO_LEGAL"]);
      $new_temp_name = $temp_memolegal[0] . "_" . $temp_memolegal[1] . "_" . $temp_memolegal[2] . "_" . $temp_memolegal[3] . "_";

      $this->view->memolegal = str_replace($new_temp_name, "", $data["MEMO_LEGAL"]);
    }

    $config = Zend_Registry::get('config');

    $docTypeCode = $config["bgdoc"]["type"]["code"];
    $docTypeDesc = $config["bgdoc"]["type"]["desc"];
    $docTypeArr  = array_combine(array_values($docTypeCode), array_values($docTypeDesc));

    $statusCode = $config["bg"]["status"]["code"];
    $statusDesc = $config["bg"]["status"]["desc"];
    $statusArr  = array_combine(array_values($statusCode), array_values($statusDesc));

    $historyStatusCode = $config["history"]["status"]["code"];
    $historyStatusDesc = $config["history"]["status"]["desc"];
    $historyStatusArr  = array_combine(array_values($historyStatusCode), array_values($historyStatusDesc));

    $counterTypeCode = $config["bgcg"]["type"]["code"];
    $counterTypeDesc = $config["bgcg"]["type"]["desc"];
    $counterTypeArr  = array_combine(array_values($counterTypeCode), array_values($counterTypeDesc));

    //$config    		= Zend_Registry::get('config');
    $BgType     = $config["bg"]["status"]["desc"];
    $BgCode     = $config["bg"]["status"]["code"];

    $arrStatus = array_combine(array_values($BgCode), array_values($BgType));

    // $arrStatus = array(
    //   '7'  => 'Canceled',
    //   '20' => 'On Risk',
    //   '21' => 'Off Risk',
    //   '22' => 'Claimed On Process',
    //   '23' => 'Claimed'
    // );

    $this->view->arrStatus = $arrStatus;



    $this->view->warranty_type = $data['COUNTER_WARRANTY_TYPE'];
    $this->view->data               = $data;
    $this->view->tbgdata = $tbgdata;
    //Zend_Debug::dump($data);
    $this->view->dataHistory        = $dataHistory;
    $this->view->dataAccSplit       = $dataAccSplit;
    $this->view->docTypeArr         = $docTypeArr;
    $this->view->statusArr          = $statusArr;
    $this->view->historyStatusArr   = $historyStatusArr;
    $this->view->counterTypeArr     = $counterTypeArr;

    $download = $this->_getParam('download');
    if ($download == 1) {
      $attahmentDestination = UPLOAD_PATH . '/document/submit/';
      $this->_helper->download->file($data['FILE'], $attahmentDestination . $data['FILE']);
    }
  }

  public function indexAction()
  {
    $this->_helper->_layout->setLayout('newlayout');

    $settings = new Settings();
    $system_type = $settings->getSetting('system_type');

    $claim_period = $settings->getSettingNew('max_claim_period');
		$this->view->BG_CLAIM_PERIOD = $claim_period;

    $this->view->systemType = $system_type;

    $conf = Zend_Registry::get('config');
    $this->_bankName = $conf['app']['bankname'];
    $this->view->masterbankname = $this->_bankName;

    $csv = $this->_getParam('csv');
    $pdf = $this->_getParam('pdf');

    $this->CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

    $fields = array(
      /*'alias'  => array('field' => 'BENEFICIARY_ALIAS',
                                   'label' => $this->language->_('Alias Name'),
                                   'sortable' => true),*/
      'regno'     => array(
        'field'    => 'BG_REG_NUMBER',
        'label'    => $this->language->_('Reg No# /  BG No#'),
      ),
      'subject'     => array(
        'field'    => 'BG_SUBJECT',
        'label'    => $this->language->_('Subject'),
      ),
      'principal'     => array(
        'field'    => 'CUST_NAME',
        'label'    => $this->language->_('Principal Name'),
      ),
      'obligee'     => array(
        'field'    => 'RECIPIENT_NAME',
        'label'    => $this->language->_('Obligee Name'),
      ),
      'branch'     => array(
        'field'    => 'BRANCH_NAME',
        'label'    => $this->language->_('Bank Branch'),
      ),
      'countertype'     => array(
        'field'    => 'COUNTERTYPE',
        'label'    => $this->language->_('Counter Type'),
      ),
      'changetype'     => array(
        'field'    => 'CHANGETYPE',
        'label'    => $this->language->_('Change Type'),
      ),
      'status'     => array(
        'field'    => 'STATUS',
        'label'    => $this->language->_('Status'),
      )



    );

    $filterlist = array('BG_SUBJECT', 'BG_STATUS', 'BG_NUMBER', 'BG_REG_NUMBER');

    $this->view->filterlist = $filterlist;

    $filterArr = array(
      'BG_SUBJECT'  => array('StringTrim', 'StripTags'),
      'BG_STATUS'  => array('StringTrim', 'StripTags'),
      'BG_NUMBER'   => array('StringTrim', 'StripTags'),
      'BG_REG_NUMBER'   => array('StringTrim', 'StripTags')
    );

    // print_r($filterArr);die;
    // if POST value not null, get post, else get param ,"SOURCE_ACCOUNT_NAME","BENEFICIARY_ACCOUNT","BENEFICIARY_ACCOUNT_NAME","TRANSACTION_ID","PS_CCY"
    $dataParam = array('BG_SUBJECT', 'BG_STATUS', 'BG_NUMBER', 'BG_REG_NUMBER');
    $dataParamValue = array();
    foreach ($dataParam as $dtParam) {

      // print_r($dtParam);die;
      if (!empty($this->_request->getParam('wherecol'))) {
        $dataval = $this->_request->getParam('whereval');
        foreach ($this->_request->getParam('wherecol') as $key => $value) {
          if ($dtParam == $value) {
            if (!empty($dataParamValue[$dtParam])) {
              $dataval[$key] = $dataParamValue[$dtParam] . ',' . $dataval[$key];
            }
            $dataParamValue[$dtParam] = $dataval[$key];
          }
        }
      }

      // $dataPost = $this->_request->getPost($dtParam);
      // $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
    }


    $options = array('allowEmpty' => true);
    $validators = array(
      'BG_SUBJECT'        => array(),
      'BG_STATUS'        => array(),
      // 'SOURCE_ACCOUNT'     => array(array('InArray', array('haystack' => array_keys($optarrAccount)))),
      'BG_NUMBER'     => array(),
      'BG_REG_NUMBER'     => array()

    );

    $zf_filter  = new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);

    // print_r($fCreatedEnd);die;


    $subject    = $zf_filter->getEscaped('BG_SUBJECT');
    $status    = $zf_filter->getEscaped('BG_STATUS');
    $fbgnumb    = $zf_filter->getEscaped('BG_NUMBER');
    $fbgregnumb     = $zf_filter->getEscaped('BG_REG_NUMBER');




    if ($filter == NULL && $clearfilter != 1) {
      //      $fUpdatedStart  = date("d/m/Y");
      //      $fUpdatedEnd  = date("d/m/Y");

      // echo "f0 c1";
    } else {
    }

    //$whereIn = [7, 20, 21, 22, 23];
    $config        = Zend_Registry::get('config');
    $BgType     = $config["bg"]["status"]["desc"];
    $BgCode     = $config["bg"]["status"]["code"];

    $arrStatus = array_combine(array_values($BgCode), array_values($BgType));

    $this->view->arrStatus = $arrStatus;

    $arrChangeType = array(
      '0' => 'New',
      //'2' => 'Indirect',
      '1' => 'Amendment Changes',
      '2' => 'Amendment Draft'
    );

    $this->view->arrChangeType = $arrChangeType;

    $arrWarrantyType = array(
      '1' => 'Full Cover',
      //'2' => 'Indirect',
      '2' => 'Line Facility',
      '3' => 'Insurance'
    );

    $this->view->arrWarrantyType = $arrWarrantyType;

    $ctdesc = $conf["bgclosing"]["changetype"]["desc"];
    $ctcode = $conf["bgclosing"]["changetype"]["code"];
    $arr_type = array_combine(array_values($ctcode), array_values($ctdesc));
    $this->view->arr_type_close = $arr_type;

    $bgclosingStatus = $conf["bgclosing"]["status"]["desc"];
    $bgclosingstatusCode = $conf["bgclosing"]["status"]["code"];
    $arrbgclosingStatus = array_combine(array_values($bgclosingstatusCode), array_values($bgclosingStatus));
    $this->view->arr_status_close = $arrbgclosingStatus;



    //print 
    $caseStatus = "(CASE A.BG_STATUS ";
    foreach ($arrStatus as $key => $val) {
      $caseStatus .= " WHEN " . $key . " THEN '" . $val . "'";
    }
    $caseStatus .= " END)";

    $caseCounter = "(CASE A.COUNTER_WARRANTY_TYPE ";
    foreach ($arrWarrantyType as $key => $val) {
      $caseCounter .= " WHEN " . $key . " THEN '" . $val . "'";
    }
    $caseCounter .= " END)";

    $caseType = "(CASE A.CHANGE_TYPE ";
    foreach ($arrChangeType as $key => $val) {
      $caseType .= " WHEN " . $key . " THEN '" . $val . "'";
    }
    $caseType .= " END)";


    $select = $this->_db->select()
      ->from(
        array('A' => 'TEMP_BANK_GUARANTEE'),
        array(
          'A.BG_REG_NUMBER',
          'A.BG_SUBJECT',
          'C.CUST_NAME',
          'A.RECIPIENT_NAME',
          'B.BRANCH_NAME',
          'COUNTERTYPE' => $caseCounter,
          'CHANGETYPE' => $caseType,
          'STATUS' => $caseStatus,
          'A.BG_INSURANCE_CODE',
          'A.COUNTER_WARRANTY_TYPE',
          'A.CHANGE_TYPE',
          'A.BG_STATUS'
        )
      )
      ->joinLeft(array('B' => 'M_BRANCH'), 'B.BRANCH_CODE = A.BG_BRANCH', array('BRANCH_NAME'))
      ->joinLeft(array('C' => 'M_CUSTOMER'), 'C.CUST_ID = A.CUST_ID', array('CUST_NAME'))
      //  ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
      ->where('A.BG_STATUS NOT IN (?)', '15', '16');
    //->order('BG_CREATED DESC');

    $auth = Zend_Auth::getInstance()->getIdentity();
    if ($auth->userHeadQuarter == "NO") {
      $select->where('B.ID = ?', $auth->userBranchId); // Add Bahri
    }

    if ($status) {
      $select->where("BG_STATUS = ? ", $status);
    }

    if ($subject) {
      $subjectArr = explode(',', $subject);
      //  $select->where("UPPER(BG_SUBJECT)  like ?",'%'.$subjectArr.'%' );

      $select->where("BG_SUBJECT LIKE " . $this->_db->quote('%' . $subject . '%'));
    }

    if ($fbgnumb)
    // {  $select->where("(select count(PS_NUMBER) from T_TRANSACTION where PS_NUMBER = P.PS_NUMBER and SOURCE_ACCOUNT = ?) > 0", (string)$fAcctsrc);   }
    { //$select->where("T.SOURCE_ACCOUNT LIKE ".$this->_db->quote('%'.$fAcctsrc.'%'));  
      $fAcctsrcArr = explode(',', $fbgnumb);
      $select->where("BG_NUMBER  LIKE ?", '%' . $fAcctsrcArr . '%');
    } // ACCSRC ?????

    if ($fbgregnumb) { //$select->where("T.BENEFICIARY_ACCOUNT LIKE ".$this->_db->quote('%'.$fBenefsrc.'%'));  
      $fBenefsrcArr = explode(',', $fbgregnumb);
      $select->where("BG_REG_NUMBER  LIKE ?", '%' . $fBenefsrcArr . '%');
    }



    if ($csv || $pdf || $this->_request->getParam('print')) {

      $arr = $this->_db->fetchAll($select);

      foreach ($arr as $key => $val) {

        if ($val['COUNTER_WARRANTY_TYPE'] == 3) {
          $arr[$key]['COUNTERTYPE'] = $val['COUNTERTYPE'] . "(" . $val["BG_INSURANCE_CODE"] . ")";
        }
        unset($arr[$key]['BG_INSURANCE_CODE']);
        unset($arr[$key]['COUNTER_WARRANTY_TYPE']);
        unset($arr[$key]['CHANGE_TYPE']);
        unset($arr[$key]['BG_STATUS']);
      }

      $header = Application_Helper_Array::simpleArray($fields, 'label');
      if ($csv) {

        $this->_helper->download->csv($header, $arr, null, $this->language->_('Customer List BG Ongoing'));
        Application_Helper_General::writeLog('RBGR', 'Download CSV Customer List BG Ongoing');
      } else if ($pdf) {
        $this->_helper->download->pdf($header, $arr, null, $this->language->_('Customer List BG Ongoing'));
        Application_Helper_General::writeLog('RBGR', 'Download PDF Customer List BG Ongoing');
      } else if ($this->_request->getParam('print') == 1) {

        $this->_forward('print', 'index', 'widget', array('data_content' => $arr, 'data_caption' => 'Customer List BG Ongoing', 'data_header' => $fields));
      }
    }

    $select = $select->query()->fetchAll();

    try {
      $this->_db->beginTransaction();

      $selectClose = $this->_db->select()
        ->from(
          array('A' => 'T_BANK_GUARANTEE'),
          array(
            'A.BG_REG_NUMBER',
            'A.BG_NUMBER',
            'A.BG_SUBJECT',
            'C.CUST_NAME',
            'A.RECIPIENT_NAME',
            'B.BRANCH_NAME',
            'COUNTERTYPE' => $caseCounter,
            'CHANGETYPE' => $caseType,
            'STATUS' => $caseStatus,
            'A.BG_INSURANCE_CODE',
            'A.COUNTER_WARRANTY_TYPE',
            'A.CHANGE_TYPE',
            'A.BG_STATUS',
            'CHANGETYPECLOSE' => 'D.CHANGE_TYPE',
            'SUGGESTIONSTATUS' => 'D.SUGGESTION_STATUS',
          )
        )
        ->join(array('D' => 'TEMP_BANK_GUARANTEE_CLOSE'), 'D.BG_NUMBER = A.BG_NUMBER', array('CLOSE_REF_NUMBER'))
        ->joinLeft(array('B' => 'M_BRANCH'), 'B.BRANCH_CODE = A.BG_BRANCH', array('BRANCH_NAME'))
        ->joinLeft(array('C' => 'M_CUSTOMER'), 'C.CUST_ID = A.CUST_ID', array('CUST_NAME'));

      if ($auth->userHeadQuarter == "NO") {
        $selectClose->where('B.ID = ?', $auth->userBranchId); // Add Bahri
      }
      $selectClose = $selectClose->query()->fetchAll();

      $this->_db->commit();
    } catch (\Throwable $th) {
      $selectClose = [];
    }

    $this->paging(array_merge($select, $selectClose));

    $conf = Zend_Registry::get('config');


    $this->view->bankname = $conf['app']['bankname'];


    // $arrStatus = array('1' => 'Waiting for review',
    //                     '2' => 'Waiting for approve',
    //                     '3' => 'Waiting to release',
    //                     '4' => 'Waiting for bank approval',
    //                     '5' => 'Issued',
    //                     '6' => 'Expired',
    //                     '7' => 'Canceled',
    //                     '8' => 'Claimed by applicant',
    //                     '9' => 'Claimed by recipient',
    //                     '10' => 'Request Repair',
    //                     '11' => 'Reject',
    //                   );





    $arrType = array(
      1 => 'Standard',
      2 => 'Custom'
    );

    $arrLang = array(
      1 => 'Indonesian',
      2 => 'English',
      3 => 'Bilingual'
    );
    $this->view->langArr = $arrLang;
    $this->view->formatArr = $arrType;
    $this->view->fields = $fields;



    if (!empty($dataParamValue)) {

      $this->view->createdStart = $dataParamValue['BG_CREATED'];
      $this->view->createdEnd = $dataParamValue['BG_CREATED_END'];
      $this->view->efdateStart = $dataParamValue['BG_DATEFROM'];
      $this->view->efdateEnd = $dataParamValue['BG_DATEFROM_END'];



      unset($dataParamValue['BG_DATEFROM_END']);
      unset($dataParamValue['BG_CREATED_END']);

      //var_dump($dataParamValue);
      foreach ($dataParamValue as $key => $value) {
        //foreach($value as $ss => $vs){

        $duparr = explode(',', $value);
        if (!empty($duparr)) {

          foreach ($duparr as $ss => $vs) {
            $wherecol[] = $key;
            $whereval[] = $vs;
          }
        } else {
          $wherecol[] = $key;
          $whereval[] = $value;
        }

        //}


      }
      //var_dump($wherecol);
      //var_dump($whereval);die;

      $this->view->wherecol     = $wherecol;
      $this->view->whereval     = $whereval;
    }
  }

  public function findPolicyBoundary($transfertype, $amount)
  {


    $selectuser    = $this->_db->select()
      ->from(array('C' => 'M_APP_BGBOUNDARY'), array(
        'BOUNDARY_MIN'     => 'C.BOUNDARY_MIN',
        'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
        'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
        'C.TRANSFER_TYPE',
        'C.POLICY'
      ))

      ->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
      ->where("C.BOUNDARY_MIN 	<= ?", $amount)
      ->where("C.BOUNDARY_MAX 	>= ?", $amount);


    //echo $selectuser;die;
    $datauser = $this->_db->fetchAll($selectuser);

    return $datauser[0]['POLICY'];
  }

  //return tombol jika blm ada yg approve
  public function findUserBoundary($transfertype, $amount)
  {



    $selectuser    = $this->_db->select()
      ->from(array('C' => 'M_APP_BOUNDARY'), array(
        'BOUNDARY_MIN'     => 'C.BOUNDARY_MIN',
        'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
        'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
        'C.TRANSFER_TYPE',
        'C.POLICY'
      ))

      ->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
      ->where("C.BOUNDARY_MIN 	<= ?", $amount)
      ->where("C.BOUNDARY_MAX 	>= ?", $amount);


    //echo $selectuser;die();
    $datauser = $this->_db->fetchAll($selectuser);

    $command = str_replace('(', '', $datauser[0]['POLICY']);
    $command = str_replace(')', '', $command);
    $command = $command . ' SG';
    $list = explode(' ', $command);

    $alphabet = array('01' => 'A', '02' => 'B', '03' => 'C', '04' => 'D', '05' => 'E', '06' => 'F', '07' => 'G', '08' => 'H', '09' => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z', 27 => 'SG');

    $flipAlphabet = array_flip($alphabet);

    foreach ($list as $row => $data) {
      foreach ($alphabet as $key => $value) {
        if ($data == $value) {
          $groupuser[] = $flipAlphabet[$data];
        }
      }
    }

    $uniqueGroupUser = array_unique($groupuser);

    foreach ($uniqueGroupUser as $key => $value) {
      if ($value == '27') {
        $selectGroupName    = $this->_db->select()
          ->from(array('C' => 'M_APP_GROUP_BUSER'), array('C.GROUP_NAME'))
          ->where("C.GROUP_BUSER_ID LIKE ?", '%S_%');
      } else {
        $selectGroupName    = $this->_db->select()
          ->from(array('C' => 'M_APP_GROUP_BUSER'), array('C.GROUP_NAME'))
          ->where("C.GROUP_BUSER_ID LIKE ?", '%_' . $value . '%');
      }

      $groupNameList = $this->_db->fetchAll($selectGroupName);

      array_unique($groupNameList[0]);

      $uniqueGroupName[$value] = $groupNameList[0]['GROUP_NAME'];
    }

    foreach ($uniqueGroupName as $row => $data) {
      foreach ($alphabet as $key => $value) {
        if ($row == $key) {
          $newUniqueGroupName[$value] = $data;
        }
      }
    }

    foreach ($groupuser as $key => $value) {

      //if special group
      if ($value == 27) {
        $likecondition = "S_%";
      } else {
        $likecondition = "%_" . $value . "%";
      }

      $selectgroup = $this->_db->select()
        ->from(array('C' => 'M_APP_GROUP_BUSER'), array(
          'BUSER_ID'
        ))

        ->where("C.GROUP_BUSER_ID LIKE ?", (string) $likecondition);

      $group_user = $this->_db->fetchAll($selectgroup);

      $groups[][$alphabet[$value]] = $group_user;
    }
    //	echo '<pre>';
    //var_dump($groups);
    $tempGroup = array();
    foreach ($groups as $key => $value) {

      foreach ($value as $data => $values) {

        foreach ($values as $row => $val) {
          $userid = $val['BUSER_ID'];

          $selectusername = $this->_db->select()
            ->from(array('M_BUSER'), array(
              '*'
            ))

            ->where("BUSER_ID = ?", (string) $userid);
          //echo $selectusername;echo ' ';
          $username = $this->_db->fetchAll($selectusername);

          if (!in_array($data, $tempGroup)) {
            $userlist[$data][] = $username[0]['BUSER_NAME'];
          }
        }

        array_push($tempGroup, $data);

        // $approverbtn[][$data] = '<button class="btnCircleGroup hovertext" style="margin-right: 1%; margin-top: 12%;" disabled>'.$data.'
        // 	<span class="hovertextcontent" style="padding-left: 15px;">'.$userlist.'</span></button>';
      }
    }

    $userlist['GROUP_NAME'] = $newUniqueGroupName;

    return $userlist;
  }




  public function validatebtn($transfertype, $amount, $ccy, $psnumb)
  {
    //die;


    $selectuser    = $this->_db->select()
      ->from(array('C' => 'M_APP_BGBOUNDARY'), array(
        'BOUNDARY_MIN'     => 'C.BOUNDARY_MIN',
        'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
        'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
        'C.TRANSFER_TYPE',
        'C.POLICY'
      ))

      ->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
      ->where("C.BOUNDARY_MIN 	<= ?", $amount)
      ->where("C.BOUNDARY_MAX 	>= ?", $amount);


    // echo $selectuser;

    $datauser = $this->_db->fetchAll($selectuser);
    if (empty($datauser)) {

      return true;
    }

    $selectusergroup    = $this->_db->select()
      ->from(array('C' => 'M_APP_GROUP_BUSER'), array(
        '*'
      ))

      ->where("C.BUSER_ID 	= ?", (string) $this->_userIdLogin);

    $usergroup = $this->_db->fetchAll($selectusergroup);




    $this->view->boundarydata = $datauser;
    //var_dump($usergroup);die;
    // print_r($this->view->boundarydata);die;
    if (!empty($usergroup)) {
      $cek = false;

      foreach ($usergroup as $key => $value) {
        $group = explode('_', $value['GROUP_BUSER_ID']);
        $alphabet = array(1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'E', 6 => 'F', 7 => 'G', 8 => 'H', 9 => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z');
        $groupalfa = $alphabet[(int) $group[2]];
        // print_r($groupalfa);echo '-';
        $usergroup[$key]['GROUP'] = $groupalfa;
        //var_dump($usergroup);
        // print_r($datauser);die;
        foreach ($datauser as $nub => $val) {
          $command = str_replace('(', '', $val['POLICY']);
          $command = str_replace(')', '', $command);
          $list = explode(' ', $command);

          //var_dump($list);
          foreach ($list as $row => $data) {

            if ($data == $groupalfa) {
              $cek = true;
              // die('ter');
              break;
            }
          }
        }
      }
      //die;



      if ($group[0] == 'S') {
        $cek = true;
      }
      // echo $cek;
      // print_r($cek);die;
      if (!$cek) {
        // die('here');
        return false;
      }
    }
    $tempusergroup = $usergroup;

    if ($cek) {




      $command = ' ' . $datauser['0']['POLICY'] . ' ';
      $command = strtoupper($command);

      $cleanCommand = trim(preg_replace('/\s+/', ' ', $command));

      //transform to php logical operator syntak
      $translate = array(
        'AND' => '&&',
        'OR' => '||',
        'THEN' => 'THEN$',
        'A' => '$A',
        'B' => '$B',
        'C' => '$C',
        'D' => '$D',
        'E' => '$E',
        'F' => '$F',
        'G' => '$G',
        'H' => '$H',
        'I' => '$I',
        'J' => '$J',
        'K' => '$K',
        'L' => '$L',
        'M' => '$M',
        'N' => '$N',
        'O' => '$O',
        'P' => '$P',
        'Q' => '$Q',
        'R' => '$R',
        // 'S' => '$S',
        'T' => '$T',
        'U' => '$U',
        'V' => '$V',
        'W' => '$W',
        'X' => '$X',
        'Y' => '$Y',
        'Z' => '$Z',
        'SG' => '$SG',
      );

      $phpCommand =  strtr($cleanCommand, $translate);
      //var_dump($phpCommand);die;
      $param = array(
        '0' => '$A',
        '1' => '$B',
        '2' => '$C',
        '3' => '$D',
        '4' => '$E',
        '5' => '$F',
        '6' => '$G',
        '7' => '$H',
        '8' => '$I',
        '9' => '$J',
        '10' => '$K',
        '11' => '$L',
        '12' => '$M',
        '13' => '$N',
        '14' => '$O',
        '15' => '$P',
        '16' => '$Q',
        '17' => '$R',
        // '18' => '$S',
        '19' => '$T',
        '20' => '$U',
        '21' => '$V',
        '22' => '$W',
        '23' => '$X',
        '24' => '$Y',
        '25' => '$Z',
        '26' => '$SG',
      );
      // print_r($phpCommand);die;
      function str_replace_first($from, $to, $content, $row)
      {
        $from = '/' . preg_quote($from, '/') . '/';
        return preg_replace($from, $to, $content, $row);
      }

      $command = str_replace('(', ' ', $val['POLICY']);
      $command = str_replace(')', ' ', $command);
      $list = explode(' ', $command);
      // print_r($list);die;
      // var_dump($command)

      $thendata = explode('THEN', $command);
      // print_r($thendata);echo '<br>';die;
      $cthen = count($thendata);
      // print_r($thendata);die;
      $secondcommand = str_replace('(', '', trim($thendata[0]));
      $secondcommand = str_replace(')', '', $secondcommand);
      $secondcommand = str_replace('AND', '', $secondcommand);
      $secondcommand = str_replace('OR', '', $secondcommand);
      $secondlist = explode(' ', $secondcommand);
      // print_r($secondlist);die;
      // print_r($grouplist);die;
      // print_r($thendata[$i]);die;
      // return true;
      if (!empty($secondlist)) {
        foreach ($usergroup as $key => $value) {
          foreach ($secondlist as $row => $thenval) {
            // print_r($value['GROUP']);echo ' == ';echo $thenval;echo '<br/>';
            if (trim($value['GROUP']) == trim($thenval)) {
              $thengroup = true;
              $grouplist[] = trim($thenval);
              //die('here');
            }
          }
        }
      }

      //var_dump($cthen);
      if ($cthen >= 2) {
        foreach ($usergroup as $key => $value) {
          // print_r($value);
          foreach ($thendata as $row => $thenval) {
            // echo '|';print_r($thenval);echo '==';
            // print_r($value['GROUP']);echo '|';echo '<br/>';
            // $thengroup = true;
            $newsecondcommand = str_replace('(', '', trim($thenval));
            $newsecondcommand = str_replace(')', '', $newsecondcommand);
            $newsecondcommand = str_replace('AND', '', $newsecondcommand);
            $newsecondcommand = str_replace('OR', '', $newsecondcommand);
            $newsecondlist = explode(' ', $newsecondcommand);
            //var_dump($newsecondcommand);
            if (in_array(trim($value['GROUP']), $newsecondlist)) {
              //if (trim($value['GROUP']) == trim($thenval)) {
              $thengroup = true;
              $grouplist[] = trim($thenval);
              //die('here');
            }
          }
        }
      }
      //var_dump($grouplist);die;
      //var_dump($thengroup);die;
      // var_dump($thengroup);die;
      // // print_r($group);die;
      // // echo $thengroup;die;
      //echo '<pre>';
      //var_dump($thengroup);
      //var_dump($thendata);
      //print_r($thendata);echo '<br/>';die('here');
      if ($thengroup == true) {


        for ($i = 1; $i <= $cthen; ++$i) {
          $oriCommand = $phpCommand;
          //echo $oriCommand;die;
          $indno = $i;
          //echo $oriCommand;echo '<br>';

          for ($a = $cthen - $indno; $a >= 1; --$a) {

            if ($i > 1) {
              $replace = 'THEN$ $' . trim($thendata[$a + 1]);
            } else {
              $replace = 'THEN$ $' . trim($thendata[$a]);
            }

            $oriCommand = str_replace($replace, "", $oriCommand);
          }


          //print_r($thendata);echo '<br>';die();

          //die;
          // if($i == 3){
          // echo 'command : ';echo $oriCommand;echo '<br/>';
          // }
          //print_r($oriCommand);echo '<br>';
          //print_r($list);echo '<br>';die;


          $result = $this->generate($oriCommand, $list, $param, $psnumb, $groupalfa, $thengroup);
          // print_r($i);
          // var_dump($result);
          //echo 'result-';var_dump($result);echo '-';die;
          if ($result) {
            // die;
            // echo $thendata[$i+1];die('eere');
            // print_r($i);
            // print_r($thendata);die;



            $replace = 'THEN$ $' . trim($thendata[$i + 1]);
            // var_dump($replace);die;
            // print_r($i);
            if (!empty($thendata[$i + 1])) {
              //die;
              $oriCommand = str_replace($replace, "", $phpCommand);
            } else {
              // die;
              $thirdcommand = str_replace('(', '', trim($thendata[$i - 1]));
              $thirdcommand = str_replace(')', '', $thirdcommand);
              $thirdcommand = str_replace('AND', '', $thirdcommand);
              $thirdcommand = str_replace('OR', '', $thirdcommand);
              $thirdlist = explode(' ', $thirdcommand);
              //						var_dump($secondlist);
              //						var_dump($grouplist);
              //							die;

              if (!empty($secondlist)) {
                foreach ($grouplist as $key => $valg) {
                  foreach ($secondlist as $row => $value) {
                    if ($value == $valg) {
                      //echo 'sini';
                      return false;
                    }
                  }
                }
              }
              $oriCommand = $phpCommand;
            }
            // print_r($thendata[$i]);die;
            // if($i == 3){
            // echo $oriCommand;die;
            // echo '<br/>';	
            // }
            // print_r($i);
            //echo $oriCommand;
            $result = $this->generate($oriCommand, $list, $param, $psnumb, $groupalfa, $thengroup);
            //var_dump($result);echo '<br/>';
            if (!$result) {
              // die;
              //print_r($groupalfa);
              //print_r($thendata[$i]);

              if ($groupalfa == trim($thendata[$i])) {
                //echo 'heer';
                return true;
              } else {

                //return true;
              }
              /*
            foreach ($grouplist as $key => $valg) {
               
              if (trim($valg) == trim($thendata[$i])) {
                // die;
                // print_r($i);
                // print_r($thendata);
                // print_r($thendata[$i]);die;
                if ($thendata[$i + 1] == $valg) {
                  return true;
                } else {
                  // die('here');
                  return false;
                }
              }
            } */
            } else {

              //return false;

              // $result = $this->generate($phpCommand,$list,$param,$psnumb);
              // print_r($phpCommand);
              // if($result){}
              // die('here');
            }
            // var_dump($result);
            // die;


          } else {
            //die('here');
            $secondcommand = str_replace('(', '', trim($thendata[$i - 1]));
            $secondcommand = str_replace(')', '', $secondcommand);
            $secondcommand = str_replace('AND', '', $secondcommand);
            $secondcommand = str_replace('OR', '', $secondcommand);
            $secondlist = explode(' ', $secondcommand);
            //var_dump($i);
            // var_dump($thendata);
            //print_r($grouplist);
            //die;
            //if($groupalfa == $gro)
            $approver = array();
            $countlist = array_count_values($list);
            foreach ($list as $key => $value) {
              if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
                $selectapprover    = $this->_db->select()
                  ->from(array('C' => 'T_BGAPPROVAL'), array(
                    'USER_ID'
                  ))

                  // ->where("C.USER_ID 	= ?" , (string)$this->_userIdLogin)
                  ->where("C.REG_NUMBER = ?", (string) $psnumb)
                  ->where("C.GROUP = ?", (string) $value);
                //	 echo $selectapprover;
                $usergroup = $this->_db->fetchAll($selectapprover);
                // print_r($usergroup);
                $approver[$value] = $usergroup;
                if ($countlist[$value] == count($approver[$value]) && $tempusergroup['0']['GROUP'] == $value) {
                  //var_dump($countlist[$value]);
                  //var_dump($tempusergroup['0']['GROUP']);  
                  //die('gere');
                  return false;
                }
              }
            }

            //$array = array(1, "hello", 1, "world", "hello");


            //echo '<pre>';
            //var_dump($list);
            //var_dump($approver);
            //die;
            //var_dump($secondlist[0]);die;
            //foreach($approver as $app => $valpp){
            if (!empty($thendata[$i])) {
              if (empty($approver[$secondlist[0]]) && $groupalfa != $secondlist['0']) {
                //die('gere');
                return false;
              }
            }

            //echo '<pre>';
            //var_dump($thendata);
            //var_dump($grouplist);
            //var_dump($approver);
            //var_dump($groupalfa);
            //die;
            foreach ($grouplist as $key => $vg) {
              $newgroupalpa = str_replace('AND', '', $vg);
              $newgroupalpa = str_replace('OR', '', $newgroupalpa);
              $groupsecondlist = explode(' ', $newgroupalpa);
              //var_dump($newgroupalpa);
              //&& empty($approver[$groupalfa])
              if (in_array($groupalfa, $groupsecondlist)) {
                // echo 'gere';die;
                return true;
                //die('ge');
              }

              if ($vg == $groupalfa  && count($approver[$groupalfa]) > 0) {
                // echo 'gere';die;
                return true;
                //die('ge');
              }
            }

            // var_dump($approver[$groupalfa]);
            // var_dump($grouplist);
            //var_dump($groupalfa);die;

            //echo 'ger';die;
            //print_r($secondlist);die;
            //	 return true;
            if (!empty($secondlist)) {
              foreach ($grouplist as $key => $valg) {
                foreach ($secondlist as $row => $value) {
                  if ($value == $valg) {

                    if (empty($thendata[1])) {
                      //die;
                      return true;
                    }
                    //else{
                    //	return false;
                    //	}
                  }
                }
              }
            }

            //	echo 'here';die;
            $secondresult = $this->generate($thendata[$i - 1], $list, $param, $psnumb, $groupalfa, $thengroup);
            // var_dump($secondresult);
            //print_r($thendata[$i-1]);
            //die;
            //if()
            //	 echo '<pre>';
            //var_dump($grouplist);die;
            foreach ($grouplist as $key => $valgroup) {
              //print_r($valgroup);
              //var_dump($thendata[$i]); 


              if (trim($valg) == trim($thendata[$i])) {
                $cekgroup = false;
                //die('here');
                if ($secondresult) {
                  return false;
                } else {
                  return true;
                }

                //break;
              }

              //else if (trim($valg) == trim($thendata[$i - 1])) {
              //		$cekgroup = false;
              // die('here');
              //	return false;
              //	}
            }
            //die;
            //if (!$cekgroup) {
            // die('here');
            //return false;
            //}
          }

          //		    echo '<br/>';
          //	${$command} = $oriCommand;
        }
      } else if (!empty($thendata) && $thengroup == false) {

        //var_dump($groupalfa)die;
        foreach ($thendata as $ky => $vlue) {
          $newsecondcommand = str_replace('(', '', trim($vlue));
          $newsecondcommand = str_replace(')', '', $newsecondcommand);
          $newsecondcommand = str_replace('AND', '', $newsecondcommand);
          $newsecondcommand = str_replace('OR', '', $newsecondcommand);
          $newsecondlist = explode(' ', $newsecondcommand);
          if ($newsecondlist['0'] == $groupalfa) {
            return true;
          }
        }
        return false;
      }




      $approver = array();
      // print_r($list);die;  	
      foreach ($list as $key => $value) {
        if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
          $selectapprover    = $this->_db->select()
            ->from(array('C' => 'T_BGAPPROVAL'), array(
              'USER_ID'
            ))

            // ->where("C.USER_ID 	= ?" , (string)$this->_userIdLogin)
            ->where("C.REG_NUMBER = ?", (string) $psnumb)
            ->where("C.GROUP = ?", (string) $value);
          //	 echo $selectapprover;
          $usergroup = $this->_db->fetchAll($selectapprover);
          // print_r($usergroup);
          $approver[$value] = $usergroup;
        }
      }
      //die;




      // print_r($phpCommand);die;
      foreach ($param as $url) {
        if (strpos($phpCommand, $url) !== FALSE) {
          $ta = substr_count($phpCommand, $url);
          // print_r($list);die;

          if (!empty($approver)) {
            // print_r($approver);die;
            foreach ($list as $key => $value) {
              if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
                foreach ($approver[$value] as $row => $val) {
                  // print_r($approver);die;
                  if (!empty($val)) {
                    $values = 'G' . $value;
                    ${$values}[$row + 1] = true;
                    // print_r($B);
                  }

                  // print_r($val);
                }
              }
            }
          }

          for ($i = 1; $i <= $ta; $i++) {
            // print_r($list);die;
            foreach ($list as $key => $value) {
              if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
                $values = 'G' . $value;
                // print_r(${$values});
                if (empty(${$values}[$i])) {
                  ${$values}[$i] = false;
                }
                // if(${$value}[$i])
              }
            }
            // print_r($phpCommand);die;
            $BG_NUMBER = $i;
            $label = str_replace('$', '$G', $url);

            $replace = $label . '[' . $BG_NUMBER . ']';

            $alf = str_replace('$', '', $url);
            $values = 'G' . $alf;
            // print_r($values);die;
            if (${$values}[$i] == $replace) {
              $phpCommand = str_replace_first($url, 'true', $phpCommand, 1);
              // print_r($phpCommand);
            } else {
              $phpCommand = str_replace_first($url, 'false', $phpCommand, 1);
              // print_r($phpCommand);die;
            }
            // }
            // }

          }
          // print_r($GB);die;

        }
      }

      $keywords = preg_split("/[\s,]+/", $cleanCommand);
      $result =  false;
      $thendata = explode('THEN$', $phpCommand);
      //var_dump($thendata);die;
      if (!empty($thendata['1'])) {
        $phpCommand = '';
        foreach ($thendata as $tkey => $tval) {
          $phpCommand .= '(';
          $phpCommand .= $tval . ')';
          if (!empty($thendata[$tkey + 1])) {
            $phpCommand .= ' && ';
          }
        }
      } else {

        $phpCommand = str_replace('THEN$', '&&', $phpCommand);
      }
      //var_dump($phpCommand);
      if (!empty($phpCommand)) {
        eval('$result = ' . "$phpCommand;");
      } else {
        return false;
      }
      //var_dump($result);die;
      if (!$result) {

        return true;
      }
      // die('here2');
      //var_dump ($result);die;
      //return $result;
    } else {
      // die('here');
      return true;
    }
  }

  public function generate($command, $list, $param, $psnumb, $group, $thengroup)
    {

        $phpCommand = $command;

        // echo $command;die;

        $approver = array();

        $count_list = array_count_values($list);
        //print_r($count_list);
        foreach ($list as $key => $value) {
            if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
                $selectapprover    = $this->_db->select()
                    ->from(array('C' => 'T_BGAPPROVAL'), array(
                        'USER_ID'
                    ))

                    // ->where("C.USER_ID 	= ?" , (string)$this->_userIdLogin)
                    ->where("C.REG_NUMBER = ?", (string) $psnumb)
                    ->where("C.GROUP = ?", (string) $value);
                // echo $selectapprover;
                $usergroup = $this->_db->fetchAll($selectapprover);
                // print_r($usergroup);
                $approver[$value] = $usergroup;
            }
        }
        //var_dump($param);die;
        //var_dump($group);
        foreach ($approver as $appval) {
            $totaldata = count($approver[$group]);
            $totalgroup = $count_list[$group];
            //var_dump($totaldata);
            //var_dump($totalgroup);
            if ($totalgroup == $totaldata && $totalgroup != 0) {

                return false;
            }
        } //die;
        //die;





        foreach ($param as $url) {

            if (strpos($phpCommand, $url) !== FALSE) {
                $ta = substr_count($phpCommand, $url);
                // print_r($list);die;

                if (!empty($approver)) {
                    // print_r($approver);die;
                    foreach ($list as $key => $value) {
                        if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
                            foreach ($approver[$value] as $row => $val) {
                                // print_r($approver);die;
                                if (!empty($val)) {
                                    $values = 'G' . $value;
                                    ${$values}[$row + 1] = true;
                                    // print_r($B);
                                }

                                // print_r($val);
                            }
                        }
                    }
                }


                // print_r($approver);die;

                for ($i = 1; $i <= $ta; $i++) {

                    foreach ($list as $key => $value) {
                        if (!empty($value)) {
                            if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
                                $values = 'G' . $value;
                                // print_r(${$values});
                                if (empty(${$values}[$i])) {
                                    ${$values}[$i] = false;
                                }
                                // if(${$value}[$i])
                            }
                        }
                    }


                    $BG_NUMBER = $i;
                    $label = str_replace('$', '$G', $url);
                    // print_r($phpCommand);die('here');
                    $replace = $label . '[' . $BG_NUMBER . ']';

                    $alf = str_replace('$', '', $url);
                    $values = 'G' . $alf;

                    if (${$values}[$i] == $replace) {
                        $phpCommand = str_replace_first($url, 'true', $phpCommand, 1);
                        // print_r($phpCommand);
                    } else {
                        $phpCommand = str_replace_first($url, 'false', $phpCommand, 1);
                        // print_r($phpCommand);die;
                    }
                    // }
                    // }

                }
                // print_r($GB);die;

            }
        }

        $keywords = preg_split("/[\s,]+/", $cleanCommand);
        $result =  false;
        $phpCommand = str_replace('THEN$', '&&', $phpCommand);
        //print_r($phpCommand);echo '<br/>';

        if (!empty($phpCommand)) {
            eval('$result = ' . "$phpCommand;");
            //var_dump($thengroup);
            // var_dump($result);

            if ($result) {
                //var_dump($thengroup);die;
                if ($thengroup) {
                    return true;
                } else {
                    return false;
                }
            } else {

                if ($thengroup) {
                    return false;
                } else {

                    return true;
                }
            }
            // return $result;
        } else {
            return false;
        }

        // var_dump ($result);die;




    }
}
