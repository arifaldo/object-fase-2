<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'SGO/Extendedmodule/phpqrcode/qrlib.php';

class bgreport_DetailController extends Application_Main
{

	protected $_moduleDB = 'RTF'; // masih harus diganti

	public function indexAction()
	{
		$this->_helper->_layout->setLayout('newlayout');

		$bg = $this->_getParam('bg');
		$this->view->bg = $bg;

		$sessToken  = new Zend_Session_Namespace('Tokenenc');
		$password   = $sessToken->token;

		$AESMYSQL = new Crypt_AESMYSQL();
		$decryption = urldecode($bg);
		$decryption_bg = $AESMYSQL->decrypt($decryption, $password);

		$conf = Zend_Registry::get('config');

		$select = $this->_db->select()
			->from(
				array('TBG' => 'T_BANK_GUARANTEE'),
				array('*')
			)
			->joinLeft(
				array('MB' => 'M_BRANCH'),
				'MB.BRANCH_CODE = TBG.BG_BRANCH',
				array('BRANCH_NAME')
			)
			->joinLeft(
				array('MC' => 'M_CUSTOMER'),
				'MC.CUST_ID = TBG.CUST_ID',
				array(
					'CUST_ID',
					'CUST_NAME',
					'CUST_NPWP',
					'CUST_ADDRESS',
					'CUST_CITY',
					'CUST_FAX',
					'CUST_CONTACT',
					'CUST_PHONE',
				)
			)
			->joinLeft(
				array('MCST' => 'M_CUSTOMER'),
				'MCST.CUST_ID = TBG.SP_OBLIGEE_CODE',
				array(
					"SP_OBLIGEE_NAME" => 'MCST.CUST_NAME',
				)
			)
			->joinLeft(
				array('INSURANCE' => 'M_CUSTOMER'),
				'INSURANCE.CUST_ID = TBG.BG_INSURANCE_CODE',
				array(
					"INSURANCE_NAME" => 'INSURANCE.CUST_NAME',
				)
			)
			->joinLeft(
				array('MCL' => 'M_CITYLIST'),
				'MCL.CITY_CODE = MC.CUST_CITY',
				array('CITY_NAME')
			)
			->joinLeft(
				array('TBGD' => 'T_BANK_GUARANTEE_DETAIL'),
				'TBGD.BG_REG_NUMBER = TBG.BG_REG_NUMBER',
				array('TBGD.USER_ID', 'TBGD.PS_FIELDNAME')
			)
			->joinLeft(
				array('TBGS' => 'T_BANK_GUARANTEE_SPLIT'),
				'TBGS.BG_NUMBER = TBG.BG_REG_NUMBER',
				[
					"ACCT", "BANK_CODE", "NAME", "AMOUNT", "FLAG"
				]
			)
			->where('TBG.BG_REG_NUMBER = ?', $decryption_bg);
		//->where('TBG.CUST_ID = ?', $this->_custIdLogin);
		//die();
		$data = $this->_db->fetchRow($select);

		if ($data['BG_OLD']) {
			$bgOld = $this->_db->select()
				->from('T_BANK_GUARANTEE', ['BG_REG_NUMBER', 'PROVISION_FEE'])
				->where('BG_NUMBER = ?', $data['BG_OLD'])
				->query()->fetch();

			$this->view->bgRegNumberOld = $bgOld['BG_REG_NUMBER'];
			$this->view->prevProv = $bgOld['PROVISION_FEE'];
		}

		$bgClose = $this->_db->select()
			->from('TEMP_BANK_GUARANTEE_CLOSE', ['CHANGE_TYPE', 'SUGGESTION_STATUS'])
			->where('BG_NUMBER = ?', $data['BG_NUMBER'])
			->order('LASTUPDATED DESC')
			->limit(1)
			// ->where('CHANGE_TYPE = 3')
			// ->where('SUGGESTION_STATUS != "7" OR SUGGESTION_STATUS != "11"')
			->query()->fetchAll();
		// echo '<pre>';
		// var_dump($bgClose);
		// die;

		// CEK APAKAH BG SEDANG DI AMENDEMEN
		$cekAmen = $this->_db->select()
			->from('TEMP_BANK_GUARANTEE')
			->where('BG_OLD = ?', $data['BG_NUMBER'])
			->order('BG_CREATED DESC')
			->query()->fetch();

		if ($cekAmen) {
			if (in_array($cekAmen['BG_STATUS'], [25, 9, 12, 13, 18])) $cekAmen = false;
		}

		$this->view->cekAmen = $cekAmen;

		$this->view->bgClose = $bgClose[0];

		// cek data claim date --------------------------
		$bgclaimdate = $this->_db->select()->from(array('TBG' => 'T_BANK_GUARANTEE'), array('*'));
		$bgclaimdate->where('TBG.BG_REG_NUMBER = ?', $decryption_bg);
		$bgclaimdate->where("DATE(NOW()) <= DATE(TBG.BG_CLAIM_DATE)");
		$dataBgClaimDate = $this->_db->fetchRow($bgclaimdate);
		if (!empty($dataBgClaimDate)) {
		  $this->view->check_bg_claim_date = true;
		} else {
		  $this->view->check_bg_claim_date = false;
		}

		// GET PAPER USED
		$getPaperUsed = $this->_db->select()
			->from('M_PAPER')
			->where('NOTES = ?', $data['BG_NUMBER'])
			->query()->fetchAll();

		$paperUsed = array_column($getPaperUsed, 'PAPER_ID');
		$this->view->paperUsed = $paperUsed;

		if ($data['BG_NUMBER_NEW']) {
			$bgNew = $this->_db->select()
				->from('T_BANK_GUARANTEE', ['BG_REG_NUMBER', 'PROVISION_FEE'])
				->where('BG_NUMBER = ?', $data['BG_NUMBER_NEW'])
				->query()->fetch();

			$this->view->bgRegNumberNew = $bgNew['BG_REG_NUMBER'];
		}

		if ($this->_request->getPost('jenis_jaminan')) {
			$attahmentDestination = UPLOAD_PATH . '/document/submit/';
			$this->_helper->download->file($data['CLOSING_FILE'], $attahmentDestination . $data['CLOSING_FILE']);
		}

		Application_Helper_General::writeLog('RBGL', 'Lihat Bank Garansi BG No :  ' . $data['BG_NUMBER']);

		switch ($data["CHANGE_TYPE"]) {
			case '0':
				$this->view->suggestion_type = "New";
				break;
			case '1':
				$this->view->suggestion_type = "Amendment Changes";
				break;
			case '2':
				$this->view->suggestion_type = "Amendment Draft";
				break;
		}

		//echo '<pre>';print_r($data);
		$conf = Zend_Registry::get('config');
		// BG TYPE
		$bgType         = $conf["bg"]["type"]["desc"];
		$bgCode         = $conf["bg"]["type"]["code"];

		$arrbgType = array_combine(array_values($bgCode), array_values($bgType));

		$this->view->arrbgType = $arrbgType;

		//BG Counter Guarantee Type
		$bgcgType         = $conf["bgcg"]["type"]["desc"];
		$bgcgCode         = $conf["bgcg"]["type"]["code"];

		$arrbgcg = array_combine(array_values($bgcgCode), array_values($bgcgType));

		$this->view->warranty_type_text_new = $arrbgcg[$data['COUNTER_WARRANTY_TYPE']];

		if ($data['COUNTER_WARRANTY_TYPE'] == '1' || $data['COUNTER_WARRANTY_TYPE'] == '3') {

			$saveHoldAmount = null;

			$bgdatasplit = $this->_db->select()
				->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
				// ->joinLeft(["B" => "M_CUSTOMER_ACCT"], "A.ACCT = B.ACCT_NO", ["TYPE" => "B.ACCT_DESC", "M_ACCT_TYPE" => "B.ACCT_TYPE", "CURRENCY" => "B.CCY_ID", 'CUST_ID'])
				->joinLeft(["B" => "M_CUSTOMER_ACCT"], "A.ACCT = B.ACCT_NO AND B.CUST_ID = '" . $data['CUST_ID'] . "'", ["TYPE" => "B.ACCT_DESC", "M_ACCT_TYPE" => "B.ACCT_TYPE", "CURRENCY" => "B.CCY_ID", 'CUST_ID'])
				->where('A.BG_REG_NUMBER = ?', $decryption_bg)
				->query()->fetchAll();

			$getEscrow = array_search("Escrow", array_column($bgdatasplit, "ACCT_DESC"));

			if ($getEscrow !== false) {
				$serviceAccount = new Service_Account($bgdatasplit[$getEscrow]["ACCT"], null);

				$getResultService = $serviceAccount->inquiryAccontInfo();
				$cif = $getResultService["cif"];

				$serviceCif = new Service_Account(null, null, null, null, null, $cif);
				$saveResult = $serviceCif->inquiryCIFAccount()["accounts"];
				$getCurrencyEscrow = array_search($bgdatasplit[$getEscrow]["ACCT"], array_column($saveResult, "account_number"));

				$escrowCurrency = 'IDR';

				if ($getCurrencyEscrow !== false) {
					$escrowCurrency = $saveResult[$getCurrencyEscrow]["currency"];
					$escrowType = $saveResult[$getCurrencyEscrow]["type_desc"];
				}

				$this->view->escrowCurrency = $escrowCurrency;
				$this->view->escrowType = $escrowType;
			}

			foreach ($bgdatasplit as $key => $value) {
				if ($value['CUST_ID'] != $data['CUST_ID'] && $value['FLAG'] == 1) continue;
				if (strtolower($value["ACCT_DESC"]) == 'escrow') continue;

				// if (strtolower($value["M_ACCT_DESC"]) != "giro" && $value["M_ACCT_TYPE"] != "D" && $value["M_ACCT_TYPE"] != "20" && $value["M_ACCT_TYPE"] != 20) continue;

				$saveHoldAmount += intval($value["AMOUNT"]);
			}

			// foreach ($bgdatasplit as $key => $value) {
			//   $temp_save = $this->_db->select()
			//     ->from("M_CUSTOMER_ACCT")
			//     ->where("ACCT_NO = ?", $value["ACCT"])
			//     ->query()->fetchAll();

			//   $bgdatasplit[$key]["CURRENCY"] = $temp_save[0]["CCY_ID"];
			//   $bgdatasplit[$key]["TYPE"] = $temp_save[0]["ACCT_DESC"];
			// }

			$this->view->fullmember = $bgdatasplit;
			$this->view->holdAmount = $saveHoldAmount;
		}

		// Marginal Deposit ---------------
		$bgdatamdsplit = $this->_db->select()
			->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
			->joinLeft(["B" => "M_CUSTOMER_ACCT"], "A.ACCT = B.ACCT_NO AND B.CUST_ID = '" . $data['CUST_ID'] . "'", ["TYPE" => "B.ACCT_DESC", "M_ACCT_TYPE" => "B.ACCT_TYPE", "CURRENCY" => "B.CCY_ID", 'CUST_ID'])
			->where('A.BG_REG_NUMBER = ?', $decryption_bg)
			->query()->fetchAll();

		if ($data['COUNTER_WARRANTY_TYPE'] == '3' && !empty($bgdatamdsplit)) {
			$bgdatadetailmd = $this->_db->select()
				->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
				->where('A.BG_REG_NUMBER = ?', $decryption_bg)
				->query()->fetchAll();

			if (!empty($bgdatadetailmd)) {
				foreach ($bgdatadetailmd as $key => $value) {
					if ($value['PS_FIELDNAME'] == 'Marginal Deposit Percentage') {
						$this->view->marginalDepositPercentage = $value['PS_FIELDVALUE'];
					}
				}
				$mdpercent = intval($this->view->marginalDepositPercentage);
				$percent = $mdpercent / 100;
				$mdamount = intval($data['BG_AMOUNT']);
				$mdtotal = $mdamount * $percent;

				$this->view->mdtotal = $mdtotal;
			}

			$mdAmount = null;
			$getEscrow = array_search("Escrow", array_column($bgdatamdsplit, "ACCT_DESC"));

			if ($getEscrow !== false) {
				$serviceAccount = new Service_Account($bgdatamdsplit[$getEscrow]["ACCT"], null);
				$getResultService = $serviceAccount->inquiryAccontInfo();
				$cif = $getResultService["cif"];
				$serviceCif = new Service_Account(null, null, null, null, null, $cif);
				$saveResult = $serviceCif->inquiryCIFAccount()["accounts"];
				$getCurrencyEscrow = array_search($bgdatamdsplit[$getEscrow]["ACCT"], array_column($saveResult, "account_number"));
				$escrowCurrency = 'IDR';

				if ($getCurrencyEscrow !== false) {
					$escrowCurrency = $saveResult[$getCurrencyEscrow]["currency"];
					$escrowType = $saveResult[$getCurrencyEscrow]["type_desc"];
				}

				$this->view->escrowCurrency = $escrowCurrency;
				$this->view->escrowType = $escrowType;
			}

			foreach ($bgdatamdsplit as $key => $valmd) {
				if ($valmd['CUST_ID'] != $data['CUST_ID'] && $valmd['FLAG'] == 1) continue;
				if (strtolower($valmd["ACCT_DESC"]) == 'escrow') continue;
				$mdAmount += intval($valmd['AMOUNT']);
			}

			$this->view->bgdatamdsplit = $bgdatamdsplit;
			$this->view->mdAmount = $mdAmount;
		}
		// Marginal Deposit ---------------

		$sqlbgdetail = $this->_db->select()
			->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
			//->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
			->where('A.BG_REG_NUMBER = ?', $data["BG_REG_NUMBER"]);
		//  die('a');
		$bgdatadetail = $sqlbgdetail
			->query()->fetchAll();

		// if (!empty($getDataBg['BG_OLD'])) {
		//   $tbgdata = $this->_db->select()
		//     ->from(["A" => "T_BANK_GUARANTEE"], ["*"])
		//     ->joinLeft(array('B' => 'M_CUSTOMER'), 'A.BG_INSURANCE_CODE = B.CUST_ID', array('B.CUST_NAME'))
		//     ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
		//     ->where('A.BG_NUMBER = ?', $getDataBg['BG_OLD'])
		//     ->query()->fetch();
		//   $this->view->tbgdata = $tbgdata;
		// }

		// if (!empty($tbgdata['BG_REG_NUMBER'])) {
		//   $tbgdatadetail = $this->_db->select()
		//     ->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
		//     ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
		//     ->where('A.BG_REG_NUMBER = ?', $tbgdata['BG_REG_NUMBER'])
		//     ->query()->fetchAll();
		// }

		// if (!empty($tbgdatadetail)) {
		//   foreach ($tbgdatadetail as $key => $value) {

		//     if ($getDataBg['COUNTER_WARRANTY_TYPE'] == 3) {
		//       if ($value['PS_FIELDNAME'] == 'Insurance Name') {
		//         $this->view->tinsuranceName =   $value['PS_FIELDVALUE'];
		//       }

		//       if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
		//         $this->view->tPrincipalAgreement =   $value['PS_FIELDVALUE'];
		//       }

		//       if ($value['PS_FIELDNAME'] == 'Amount') {
		//         $this->view->tinsurance_amount =   $value['PS_FIELDVALUE'];
		//       }

		//       if ($value['PS_FIELDNAME'] == 'Principal Agreement Start Date') {
		//         $this->view->tpaDateStart =   $value['PS_FIELDVALUE'];
		//       }

		//       if ($value['PS_FIELDNAME'] == 'Principal Agreement End Date') {
		//         $this->view->tpaDateEnd =   $value['PS_FIELDVALUE'];
		//       }
		//     } else {

		//       if ($value['PS_FIELDNAME'] == 'Plafond Owner') {
		//         $this->view->towner1 =   $value['PS_FIELDVALUE'];
		//       }

		//       if ($value['PS_FIELDNAME'] == 'Amount Owner') {
		//         $this->view->tamountowner1 =   $value['PS_FIELDVALUE'];
		//       }

		//       if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
		//         $this->view->towner2 =   $value['PS_FIELDVALUE'];
		//       }

		//       if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
		//         $this->view->tamountowner2 =   $value['PS_FIELDVALUE'];
		//       }

		//       if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
		//         $this->view->towner3 =   $value['PS_FIELDVALUE'];
		//       }

		//       if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
		//         $this->view->tamountowner3 =   $value['PS_FIELDVALUE'];
		//       }
		//     }
		//   }
		// }

		// $getGuarantedTransanctions = $this->_db->select()
		//   ->from('TEMP_BANK_GUARANTEE_UNDERLYING')
		//   ->where('BG_REG_NUMBER = ?', $bgRegNumberDecrypt)
		//   ->query()->fetchAll();

		// $this->view->guarantedTransanctions = $getGuarantedTransanctions;

		// if (!empty($tbgdata['BG_REG_NUMBER'])) {
		//   $getGuarantedTransanctionsT = $this->_db->select()
		//     ->from('T_BANK_GUARANTEE_UNDERLYING')
		//     ->where('BG_REG_NUMBER = ?', $tbgdata['BG_REG_NUMBER'])
		//     ->query()->fetchAll();

		//   $this->view->guarantedTransanctionsT = $getGuarantedTransanctionsT;
		// }

		// if (!empty($tbgdata['BG_REG_NUMBER'])) {
		//   $checkOthersAttachmentT = $this->_db->select()
		//     ->from(["A" => "T_BANK_GUARANTEE_FILE"], ["*"])
		//     ->where("BG_REG_NUMBER = ?", $tbgdata['BG_REG_NUMBER'])
		//     ->order('A.INDEX ASC')
		//     ->query()->fetchAll();

		//   $this->view->othersAttachmentT = $checkOthersAttachmentT;
		// }

		// if ($getDataBg['COUNTER_WARRANTY_TYPE'] == '1') {
		//   $bgdatasplit = $this->_db->select()
		//     ->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
		//     ->where('A.BG_REG_NUMBER = ?', $bgRegNumberDecrypt)
		//     ->query()->fetchAll();

		//   foreach ($bgdatasplit as $key => $value) {
		//     $temp_save = $this->_db->select()
		//       ->from("M_CUSTOMER_ACCT")
		//       ->where("ACCT_NO = ?", $value["ACCT"])
		//       ->query()->fetchAll();

		//     $bgdatasplit[$key]["CURRENCY"] = $temp_save[0]["CCY_ID"];
		//     $bgdatasplit[$key]["TYPE"] = $temp_save[0]["ACCT_DESC"];
		//   }

		//   $bgdatasplitT = $this->_db->select()
		//     ->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
		//     ->where('A.BG_NUMBER = ?', $getDataBg['BG_OLD'] ? $getDataBg['BG_OLD'] : '')
		//     ->query()->fetchAll();

		//   foreach ($bgdatasplitT as $key => $value) {
		//     $temp_save = $this->_db->select()
		//       ->from("M_CUSTOMER_ACCT")
		//       ->where("ACCT_NO = ?", $value["ACCT"])
		//       ->query()->fetchAll();

		//     $bgdatasplitT[$key]["CURRENCY"] = $temp_save[0]["CCY_ID"];
		//     $bgdatasplitT[$key]["TYPE"] = $temp_save[0]["ACCT_DESC"];
		//   }

		//   $this->view->fullmemberA = $bgdatasplit[0];
		//   $this->view->fullmemberT = $bgdatasplitT[0];
		// }


		if (!empty($bgdatadetail)) {
			foreach ($bgdatadetail as $key => $value) {

				// if($data['COUNTER_WARRANTY_TYPE'] != 1){
				if ($value['PS_FIELDNAME'] == 'SLIK OJK Document') {
					$this->view->slikOjkDocument =   $value['PS_FIELDVALUE'];
				}
				// }

				if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
					if ($value['PS_FIELDNAME'] == 'Insurance Name') {
						$this->view->insuranceName =   $value['PS_FIELDVALUE'];
					}

					if ($value['PS_FIELDNAME'] == 'Principle Agreement Number') {
						$this->view->PrincipalAgreement =   $value['PS_FIELDVALUE'];
					}

					if ($value['PS_FIELDNAME'] == 'Principle Agreement Granted Date') {
						$this->view->PrincipleAgreementGrantedDate =   $value['PS_FIELDVALUE'];
					}

					if ($value['PS_FIELDNAME'] == 'Counter Guarantee Number') {
						$this->view->CounterGuaranteeNumber =   $value['PS_FIELDVALUE'];
					}

					if ($value['PS_FIELDNAME'] == 'Counter Guarantee Granted Date') {
						$this->view->CounterGuaranteeGrantedDate =   $value['PS_FIELDVALUE'];
					}


					if ($value['PS_FIELDNAME'] == 'Amount') {
						$this->view->insurance_amount =   $value['PS_FIELDVALUE'];
					}

					if ($value['PS_FIELDNAME'] == 'Principal Agreement Start Date') {
						$this->view->paDateStart =   $value['PS_FIELDVALUE'];
					}

					if ($value['PS_FIELDNAME'] == 'Principal Agreement End Date') {
						$this->view->paDateEnd =   $value['PS_FIELDVALUE'];
					}
				} else {

					if ($value['PS_FIELDNAME'] == 'Plafond Owner 1') {
						$this->view->owner1 =   $value['PS_FIELDVALUE'];
					}

					if ($value['PS_FIELDNAME'] == 'Amount Owner 1') {
						$this->view->amountowner1 =   $value['PS_FIELDVALUE'];
					}

					if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
						$this->view->owner2 =   $value['PS_FIELDVALUE'];
					}

					if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
						$this->view->amountowner2 =   $value['PS_FIELDVALUE'];
					}

					if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
						$this->view->owner3 =   $value['PS_FIELDVALUE'];
					}

					if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
						$this->view->amountowner3 =   $value['PS_FIELDVALUE'];
					}
				}
			}
		}

		$this->view->BG_REG_NUMBER = $data["BG_REG_NUMBER"];

		$checkCounterGuaranteeFile = $this->_db->select()
			->from(["A" => "T_BANK_GUARANTEE_DETAIL"], ["PS_FIELDVALUE"])
			->where("BG_REG_NUMBER = ?", $data["BG_REG_NUMBER"])
			->where("PS_FIELDNAME = ?", "Counter Guarantee Document")
			->query()->fetchAll();

		$this->view->checkCounterGuaranteeFile = count($checkCounterGuaranteeFile);

		$principleData = [];
		// if ($data["COUNTER_WARRANTY_TYPE"] == "3") {
		foreach ($bgdatadetail as $key => $value) {
			$principleData[$value["PS_FIELDNAME"]] = $value["PS_FIELDVALUE"];
		}

		$this->view->principleData = $principleData;
		// }

		if (!empty($principleData["SLIK OJK Document"])) {
			$temp_slikojk = explode("_", $principleData["SLIK OJK Document"]);
			$new_temp_name = $temp_slikojk[0] . "_" . $temp_slikojk[1] . "_" . $temp_slikojk[2] . "_" . $temp_slikojk[3] . "_";

			$this->view->slikojk = str_replace($new_temp_name, "", $principleData["SLIK OJK Document"]);
		}

		if (!empty($data["KUASA_DIREKSI_FILE"])) {
			$temp_suratpermohonan = explode("_", $data["KUASA_DIREKSI_FILE"]);
			$new_temp_name = $temp_suratpermohonan[0] . "_" . $temp_suratpermohonan[1] . "_" . $temp_suratpermohonan[2] . "_" . $temp_suratpermohonan[3] . "_";

			$this->view->suratpermohonan = str_replace($new_temp_name, "", $data["KUASA_DIREKSI_FILE"]);
		}

		if (!empty($data["AGREE_FORMAT"])) {
			$temp_agreeformat = explode("_", $data["AGREE_FORMAT"]);
			$new_temp_name = $temp_agreeformat[0] . "_" . $temp_agreeformat[1] . "_" . $temp_agreeformat[2] . "_" . $temp_agreeformat[3] . "_";

			$this->view->agreeformat = str_replace($new_temp_name, "", $data["AGREE_FORMAT"]);
		}

		if (!empty($data["MEMO_LEGAL"])) {
			$temp_memolegal = explode("_", $data["MEMO_LEGAL"]);
			$new_temp_name = $temp_memolegal[0] . "_" . $temp_memolegal[1] . "_" . $temp_memolegal[2] . "_" . $temp_memolegal[3] . "_";

			$this->view->memolegal = str_replace($new_temp_name, "", $data["MEMO_LEGAL"]);
		}

		if ($bgdata[0]['COUNTER_WARRANTY_TYPE'] == '3') {
			$this->view->isinsurance = true;
		}

		switch ($bgdata[0]["CHANGE_TYPE"]) {
			case '0':
				$this->view->suggestionType = "New";
				break;
			case '1':
				$this->view->suggestionType = "Amendment Changes";
				break;
			case '2':
				$this->view->suggestionType = "Amendment Draft";
				break;
		}

		$config        = Zend_Registry::get('config');
		$BgcomplationType     = $config["bgclosing"]["changetype"]["desc"];
		$BgcomplationCode     = $config["bgclosing"]["changetype"]["code"];

		$arrStatusSettlement = array_combine(array_values($BgcomplationCode), array_values($BgcomplationType));

		$this->view->arrStatusSettlement = $arrStatusSettlement;

		$provision_insurance = $this->_db->select()
			->from(["A" => "M_CUST_LINEFACILITY"], ["FEE_PROVISION", "FEE_ADMIN", "FEE_STAMP"])
			->where("CUST_ID = ?", $data["BG_INSURANCE_CODE"])
			->query()->fetchAll();

		if ($data["COUNTER_WARRANTY_TYPE"] == "3") $this->view->provision_insurance = $provision_insurance[0];


		$get_linefacility = $this->_db->select()
			->from("M_CUST_LINEFACILITY", ["PLAFOND_LIMIT", "FEE_PROVISION", "FEE_ADMIN", "FEE_STAMP"])
			->where("CUST_ID = ?", $data["CUST_ID"])
			->query()->fetchAll();

		$this->view->current_limit = $get_linefacility[0]["PLAFOND_LIMIT"] - $total_bgamount_on_risk - $total_bgamount_on_temp;
		$this->view->max_limit = $get_linefacility[0]["PLAFOND_LIMIT"];

		$this->view->linefacility = $get_linefacility[0];

		$this->view->acct = $data['FEE_CHARGE_TO'];

		$conf = Zend_Registry::get('config');
		$this->view->bankname = $conf['app']['bankname'];

		// $CustomerUser = new CustomerUser($data['CUST_ID'], $data['BG_CREATEDBY']);
		// $param = array('CCY_IN' => 'IDR', 'ACCT_NO' => $data['FEE_CHARGE_TO']);
		// $AccArr = $CustomerUser->getAccounts();
		//var_dump($AccArr);die;

		// if (!empty($AccArr)) {
		// $this->view->src_name = $AccArr['0']['ACCT_NAME'];
		// }

		$feeChargeAcct = $this->_db->select()
			->from("M_CUSTOMER_ACCT")
			->where("ACCT_NO = ?", $data["FEE_CHARGE_TO"])
			// ->where("CUST_ID = ?", $data['CUST_ID'])
			->query()->fetch();

		$this->view->src_name = $feeChargeAcct['ACCT_NAME'];

		$get_cash_collateral = $this->_db->select()
			->from("M_CHARGES_OTHER", ["CHARGES_PCT", "CHARGES_ADM", "CHARGES_STAMP"])
			->where("CUST_ID = ?", "GLOBAL")
			->where("CHARGES_TYPE = ?", "10")
			->query()->fetchAll();

		$this->view->cash_collateral = $get_cash_collateral[0];

		$bgpublishType     = $conf["bgpublish"]["type"]["desc"];
		$bgpublishCode     = $conf["bgpublish"]["type"]["code"];

		$arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

		$this->view->publishForm = $arrbgpublish[$data['BG_PUBLISH']];

		if ($data["COUNTER_WARRANTY_TYPE"] == "3") {
			$getInsuranceBranch = array_search("Insurance Branch", array_column($bgdatadetail, "PS_FIELDNAME"));
			$getInsuranceBranch = $bgdatadetail[$getInsuranceBranch];


			$insuranceBranch = $this->_db->select()
				->from("M_INS_BRANCH")
				->where("INS_BRANCH_CODE = ?", $getInsuranceBranch["PS_FIELDVALUE"])
				->query()->fetchAll();

			$this->view->insuranceBranch = $insuranceBranch[0]["INS_BRANCH_NAME"];
			$this->view->insuranceBranchAcct = $insuranceBranch[0]["INS_BRANCH_ACCT"];
		}

		$arrBankFormat = array(
			1 => 'Bank Standard',
			2 => 'Special Format (with bank approval)'
		);

		$this->view->bankFormat = $arrBankFormat[$data['BG_FORMAT']];
		$this->view->bankFormatNumber = $data['BG_FORMAT'];

		$arrLang = array(
			1 => 'Indonesian',
			2 => 'English',
			3 => 'Billingual',
		);

		$this->view->languagetext = $arrLang[$data['BG_LANGUAGE']];

		$checkOthersAttachment = $this->_db->select()
			->from(["A" => "T_BANK_GUARANTEE_FILE"], ["*"])
			->where("BG_REG_NUMBER = '" . $data['BG_REG_NUMBER'] . "'")
			->order('A.INDEX ASC')
			->query()->fetchAll();

		if (count($checkOthersAttachment) > 0) {
			$this->view->othersAttachment = $checkOthersAttachment;
		}

		// Get data T_BANK_GUARANTEE_HISTORY
		$select = $this->_db->select()
			->from(
				array('TBGH' => 'T_BANK_GUARANTEE_HISTORY'),
				array('*')
			)
			->where('TBGH.BG_REG_NUMBER = ?', $decryption_bg);
		// ->where('TBGH.CUST_ID = ?', $this->_custIdLogin);
		$dataHistory = $this->_db->fetchAll($select);

		// Get data T_BANK_GUARANTEE_HISTORY_CLOSE
		$selectClose = $this->_db->select()
			->from(
				array('TBGH' => 'T_BANK_GUARANTEE_HISTORY_CLOSE'),
				array('*')
			)
			->where('TBGH.BG_NUMBER = ?', $data['BG_NUMBER']);
		// ->where('TBGH.CUST_ID = ?', $this->_custIdLogin);
		$dataHistoryClose = $this->_db->fetchAll($selectClose);

		$historyStatusCode = $config["bgclosinghistory"]["status"]["code"];
		$historyStatusDesc = $config["bgclosinghistory"]["status"]["desc"];
		$historyStatusArr  = array_combine(array_values($historyStatusCode), array_values($historyStatusDesc));
		$this->view->historyStatusArrClose   = $historyStatusArr;

		// Get data TEMP_BANK_GUARANTEE_SPLIT
		$select = $this->_db->select()
			->from(
				array('TBGS' => 'T_BANK_GUARANTEE_SPLIT'),
				array('*')
			)
			->where('TBGS.BG_REG_NUMBER = ?', $decryption_bg);
		$dataAccSplit = $this->_db->fetchAll($select);

		$config = Zend_Registry::get('config');

		$docTypeCode = $config["bgdoc"]["type"]["code"];
		$docTypeDesc = $config["bgdoc"]["type"]["desc"];
		$docTypeArr  = array_combine(array_values($docTypeCode), array_values($docTypeDesc));

		$statusCode = $config["bg"]["status"]["code"];
		$statusDesc = $config["bg"]["status"]["desc"];
		$statusArr  = array_combine(array_values($statusCode), array_values($statusDesc));

		$historyStatusCode = $config["history"]["status"]["code"];
		$historyStatusDesc = $config["history"]["status"]["desc"];
		$historyStatusArr  = array_combine(array_values($historyStatusCode), array_values($historyStatusDesc));

		$counterTypeCode = $config["bgcg"]["type"]["code"];
		$counterTypeDesc = $config["bgcg"]["type"]["desc"];
		$counterTypeArr  = array_combine(array_values($counterTypeCode), array_values($counterTypeDesc));

		//$config    		= Zend_Registry::get('config');
		$BgType     = $config["bg"]["status"]["desc"];
		$BgCode     = $config["bg"]["status"]["code"];

		$arrStatus = array_combine(array_values($BgCode), array_values($BgType));

		// $arrStatus = array(
		//   '7'  => 'Canceled',
		//   '20' => 'On Risk',
		//   '21' => 'Off Risk',
		//   '22' => 'Claimed On Process',
		//   '23' => 'Claimed'
		// );

		$this->view->arrStatus = $arrStatus;



		$this->view->warranty_type = $data['COUNTER_WARRANTY_TYPE'];
		$this->view->data               = $data;
		//Zend_Debug::dump($data);
		$this->view->dataHistory        = $dataHistory;
		$this->view->dataHistoryClose   = $dataHistoryClose;
		$this->view->dataAccSplit       = $dataAccSplit;
		$this->view->docTypeArr         = $docTypeArr;
		$this->view->statusArr          = $statusArr;
		$this->view->historyStatusArr   = $historyStatusArr;
		$this->view->counterTypeArr     = $counterTypeArr;

		$download = $this->_getParam('download');
		if ($download == 1) {
			$attahmentDestination = UPLOAD_PATH . '/document/submit/';
			// $this->_helper->download->file($data['FILE'], $attahmentDestination . $data['FILE']);
			$this->_helper->download->file("BG_NO_" . $data["BG_NUMBER"] . ".pdf", $attahmentDestination . $data['DOCUMENT_ID'] . ".pdf");
		}

		// check pengajuan 
		$checkTempDataExist = $this->_db->select()
			->from('TEMP_BANK_GUARANTEE_CLOSE')
			->where('BG_REG_NUMBER = ?', $data['BG_REG_NUMBER'])
			->query()->fetch();

		$allow = true;
		if ($checkTempDataExist) {
			$allow = false;
		}

		$this->view->allow = $allow;

		if ($this->_request->isPost() && $allow) {

			$filters = [
				'jenisPenutupan' => 'StringTrim',
			];

			$validators = [
				'jenisPenutupan' => [
					'presence' => 'required',
					"NotEmpty",
					"messages" => [
						'jenis penutupan tidak boleh kosong'
					]
				],
			];

			$input = new Zend_Filter_Input($filters, $validators, $this->_request->getPost());

			// file validation
			$adapter = new Zend_File_Transfer_Adapter_Http();

			$attachmentDesctination = UPLOAD_PATH . '/document/submit/';
			$adapter->setDestination($attachmentDesctination);

			$extensionValidator = new Zend_Validate_File_Extension(array(false, 'pdf'));
			$extensionValidator->setMessage(
				'File harus berformat *.pdf'
			);
			$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => (1024 * 10000)));
			$sizeValidator->setMessage(
				'Ukuran file tidak boleh lebih dari 10 MB'
			);

			$adapter->setValidators([
				$extensionValidator,
				$sizeValidator,
			], 'filePenutupan');

			if ($input->isValid() && $adapter->isValid()) {
				try {
					//code...
					$newName = date('dmy') . "_" . date('His') . "_" . uniqid() . "_" . $adapter->getFileName('filePenutupan', false);
					$adapter->addFilter('Rename', $newName, 'filePenutupan');
					$adapter->receive('filePenutupan');

					$change_id = $this->suggestionWaitingApproval('Penutupan BG', 'Usulan penutupan BG : ' . $data['BG_NUMBER'], 'N', null, 'T_BANK_GUARANTEE', 'TEMP_BANK_GUARANTEE_CLOSE', $data['BG_NUMBER'], $data['BG_SUBJECT'], $data['CUST_ID'], $data['CUST_NAME']);

					$this->_db->insert('TEMP_BANK_GUARANTEE_CLOSE', [
						"CHANGES_ID" => $change_id,
						"BG_REG_NUMBER" => $data['BG_REG_NUMBER'],
						"CLOSING_TYPE" => $input->jenisPenutupan,
						"CLOSING_FILE" => $adapter->getFileName('filePenutupan', false),
					]);

					switch ($input->jenisPenutupan) {
						case 1:
							$penutupan = 'Habis Masa';
							break;

						case 2:
							$penutupan = 'Permintaan Principal';
							break;

						case 3:
							$penutupan = 'Klaim Obligee';
							break;
					}

					Application_Helper_General::writeLog('CRBG', 'Mengajukan Usulan Penutupan Bank Garansi No. ' . $data['BG_NUMBER'] . '. Jenis Penutupan: ' . $penutupan . '');

					$this->setbackURL($this->view->url());
					$this->_redirect('/notification/submited/index');
				} catch (\Throwable $th) {
					//throw $th;
				}
			} else {

				$this->view->fileErrors = $adapter->getMessages();
				$this->view->errors = $input->getMessages();
			}
		}
	}

	public function Terbilang($nilai)
	{
		$huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
		if ($nilai == 0) {
			return "";
		} elseif ($nilai < 12 & $nilai != 0) {
			return "" . $huruf[$nilai];
		} elseif ($nilai < 20) {
			return $this->Terbilang($nilai - 10) . " Belas ";
		} elseif ($nilai < 100) {
			return $this->Terbilang($nilai / 10) . " Puluh " . $this->Terbilang($nilai % 10);
		} elseif ($nilai < 200) {
			return " Seratus " . $this->Terbilang($nilai - 100);
		} elseif ($nilai < 1000) {
			return $this->Terbilang($nilai / 100) . " Ratus " . $this->Terbilang($nilai % 100);
		} elseif ($nilai < 2000) {
			return " Seribu " . $this->Terbilang($nilai - 1000);
		} elseif ($nilai < 1000000) {
			return $this->Terbilang($nilai / 1000) . " Ribu " . $this->Terbilang($nilai % 1000);
		} elseif ($nilai < 1000000000) {
			return $this->Terbilang($nilai / 1000000) . " Juta " . $this->Terbilang($nilai % 1000000);
		} elseif ($nilai < 1000000000000) {
			return $this->Terbilang($nilai / 1000000000) . " Milyar " . $this->Terbilang($nilai % 1000000000);
		} elseif ($nilai < 100000000000000) {
			return $this->Terbilang($nilai / 1000000000000) . " Trilyun " . $this->Terbilang($nilai % 1000000000000);
		} elseif ($nilai <= 100000000000000) {
			return "Maaf Tidak Dapat di Prose Karena Jumlah nilai Terlalu Besar ";
		}
	}

	public function Terbilangen($nilai)
	{
		$huruf = array("", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", 'Nineteen');
		if ($nilai == 0) {
			return "";
		} elseif ($nilai < 20 & $nilai != 0) {
			return "" . $huruf[$nilai];
		} elseif ($nilai < 100) {
			return $this->Terbilangen($nilai / 10) . "ty " . $this->Terbilangen($nilai % 10);
		} elseif ($nilai < 200) {
			return " Seratus " . $this->Terbilangen($nilai - 100);
		} elseif ($nilai < 1000) {
			return $this->Terbilangen($nilai / 100) . " hundred " . $this->Terbilangen($nilai % 100);
		} elseif ($nilai < 2000) {
			return " Seribu " . $this->Terbilangen($nilai - 1000);
		} elseif ($nilai < 1000000) {
			return $this->Terbilangen($nilai / 1000) . " thausand " . $this->Terbilangen($nilai % 1000);
		} elseif ($nilai < 1000000000) {
			return $this->Terbilangen($nilai / 1000000) . " million " . $this->Terbilangen($nilai % 1000000);
		} elseif ($nilai < 1000000000000) {
			return $this->Terbilangen($nilai / 1000000000) . " Bilion " . $this->Terbilangen($nilai % 1000000000);
		} elseif ($nilai < 100000000000000) {
			return $this->Terbilangen($nilai / 1000000000000) . " Trillion " . $this->Terbilangen($nilai % 1000000000000);
		} elseif ($nilai <= 100000000000000) {
			return "Maaf Tidak Dapat di Prose Karena Jumlah nilai Terlalu Besar ";
		}
	}

	public function indodate($tanggal)
	{
		$bulan = array(
			1 =>   'Januari',
			'Februari',
			'Maret',
			'April',
			'Mei',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember'
		);
		$pecahkan = explode('-', $tanggal);

		// variabel pecahkan 0 = tanggal
		// variabel pecahkan 1 = bulan
		// variabel pecahkan 2 = tahun

		return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
	}

	public function escrowdetailAction()
	{
		$this->_helper->_layout->setLayout('popup');

		$sessToken  = new Zend_Session_Namespace('Tokenenc');
		$password   = $sessToken->token;

		$AESMYSQL = new Crypt_AESMYSQL();
		$decryption = urldecode($this->_request->getParam("bgnumb"));
		$decryption_bg = $AESMYSQL->decrypt($decryption, $password);

		$getCustName = $this->_db->select()
			->from(["A" => "T_BANK_GUARANTEE"])
			->joinLeft(["B" => "M_CUSTOMER"], "A.CUST_ID = B.CUST_ID", ["CUST_NAME", 'CUST_ID'])
			->where("BG_REG_NUMBER = ?", $decryption_bg)
			->query()->fetch();

		$this->view->custName = $getCustName["CUST_NAME"];

		$bgdatasplit = $this->_db->select()
			->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
			->joinLeft(["B" => "M_CUSTOMER_ACCT"], "A.ACCT = B.ACCT_NO AND B.CUST_ID = '" . $getCustName['CUST_ID'] . "'", ["M_ACCT_DESC" => "B.ACCT_DESC", "M_ACCT_TYPE" => "B.ACCT_TYPE", "M_CCY_ID" => "B.CCY_ID", "CUST_ID"])
			->where('A.BG_REG_NUMBER = ?', $decryption_bg)
			->where("LOWER(B.ACCT_TYPE) = 'd' OR B.ACCT_TYPE = '20' OR LOWER(A.ACCT_DESC) = 'giro' OR LOWER(A.ACCT_TYPE) = 'giro'")
			->query()->fetchAll();

		// $escrow_acct = null;
		// if (array_search('Escrow', array_column($bgdatasplit, "ACCT_DESC")) !== false) {
		//   $escrow_acct = $bgdatasplit[array_search('Escrow', array_column($bgdatasplit, "ACCT_DESC"))]["ACCT"];
		//   unset($bgdatasplit[array_search('Escrow', array_column($bgdatasplit, "ACCT_DESC"))]);
		// }

		$escrow_acct = $this->_db->select()
			->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
			->where('A.BG_REG_NUMBER = ?', $decryption_bg)
			->where('ACCT_DESC = ?', 'Escrow')
			->query()->fetch();

		$bgdatasplit = array_values($bgdatasplit);

		$this->view->escrow_acct = $escrow_acct['ACCT'];
		$this->view->fullmember = $bgdatasplit;
	}

	public function downloadfileAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$bg = $this->_getParam('bgnumb');
		$this->view->bg = $bg;
		$file = $this->_getParam('file');

		$sessToken  = new Zend_Session_Namespace('Tokenenc');
		$password   = $sessToken->token;

		$AESMYSQL = new Crypt_AESMYSQL();
		$decryption = urldecode($bg);
		$decryption_bg = $AESMYSQL->decrypt($decryption, $password);

		$getSlik = $this->_db->select()
			->from(['TBGD' => 'T_BANK_GUARANTEE_DETAIL'], ['PS_FIELDVALUE'])
			->where('TBGD.BG_REG_NUMBER = TBG.BG_REG_NUMBER')
			->where('TBGD.PS_FIELDNAME = ?', 'SLIK OJK Document');

		$getAllData = $this->_db->select()
			->from(['TBG' => 'T_BANK_GUARANTEE'], [
				'*',
				'SLIK_OJK_DOCUMENT' => new Zend_Db_Expr('(' . $getSlik . ')'),
			])
			->where('BG_REG_NUMBER = ?', $decryption_bg)
			->query()->fetch();

		$attahmentDestination = UPLOAD_PATH . '/document/submit/';

		switch (intval($file)) {
			case 1:
				$this->_helper->download->file($getAllData['AGREE_FORMAT'], $attahmentDestination . $getAllData['AGREE_FORMAT']);
				break;
			case 2:
				$this->_helper->download->file($getAllData['MEMO_LEGAL'], $attahmentDestination . $getAllData['MEMO_LEGAL']);
				break;
			case 3:
				$this->_helper->download->file($getAllData['SLIK_OJK_DOCUMENT'], $attahmentDestination . $getAllData['SLIK_OJK_DOCUMENT']);
				break;
			case 4:
				$this->_helper->download->file($getAllData['KUASA_DIREKSI_FILE'], $attahmentDestination . $getAllData['KUASA_DIREKSI_FILE']);
				break;
			default:
				$this->_redirect('/home/dashboard');
				break;
		}
	}

	public function showfinaldocAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$bg = $this->_getParam('bgnumb');
		$this->view->bg = $bg;

		$sessToken  = new Zend_Session_Namespace('Tokenenc');
		$password   = $sessToken->token;

		$AESMYSQL = new Crypt_AESMYSQL();
		$decryption = urldecode($bg);
		$decryption_bg = $AESMYSQL->decrypt($decryption, $password);


		$getDataBg = $this->_db->select()
			->from('T_BANK_GUARANTEE')
			->where('BG_REG_NUMBER = ?', $decryption_bg)
			->query()->fetch();

		$get_file_name = $getDataBg["DOCUMENT_ID"] . ".pdf";

		header("Content-type: application/pdf");
		header("Content-Disposition: inline; filename=" . $get_file_name);
		// @readfile('/app/bg/library/data/uploads/document/submit/' . $get_file_name);
		@readfile(UPLOAD_PATH . '/document/submit/' . $get_file_name);
	}
}
