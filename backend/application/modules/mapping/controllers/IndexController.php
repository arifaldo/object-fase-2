<?php
class mapping_IndexController extends Application_Main {


	public function indexAction(){

		$this->_helper->layout()->setLayout('newlayout');
		
		
		$this->view->modulename = $this->_request->getModuleName();
		$this->view->controllername = $this->_request->getControllerName();
		$this->view->actionname = $this->_request->getActionName();
		
		$headers = array	(
			'tag'	=> array(
				'field'	=> 'tag',
				'label'	=> $this->language->_('Tag'),
				'sortable'	=> true
			),
			'text'	=> array(
				'field' => 'text',
				'label' => $this->language->_('Text'),
				'sortable'	=> true
			),
			'mappingto'	=> array(
				'field' => 'mappingto',
				'label' => $this->language->_('Mapping To'),
				'sortable' => true
			),
			'desc'	=> array(
				'field' => 'desc',
				'label' => $this->language->_('Description'),
				'sortable' => true
			)
		);

		$filterlist = array("tag","text","mappingto","desc");
		
		$this->view->filterlist = $filterlist;
		
		$field = Application_Helper_Array::simpleArray($headers, 'field');
		
		$sortBy 	= (array_key_exists($this->_getParam('sortby'), $headers) ? $headers[$this->_getParam('sortby')]['field'] : $field[4]);
		$sortDir 	= (in_array($this->_getParam('sortdir'), array('asc','desc')) ? $this->_getParam('sortdir') : 'desc');
		$page 		= (is_numeric($this->_getParam('page')) ? $this->_getParam('page') : 1);
		
		$stringParam = $params;
		unset($stringParam['module']);
		unset($stringParam['controller']);
		unset($stringParam['action']);
		unset($stringParam['filter']);
		
		$filter = array(
				'*'	=> array('StringTrim','StripTags'),
		);
		$options = array('allowEmpty' => true);
		
		$validators = array(
			'tag'			=> 	array(),
			'text'			=> array(),
			'mappingto'		=> array(),
			'desc'			=> 	array(),
		);


		$dataParam = array("tag","text","mappingto","desc");
		$dataParamValue = array();
		// print_r($this->_request->getParam('wherecol'));die;
		foreach ($dataParam as $dtParam)
		{
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}
				
			}

		}
		
		$filter  = new Zend_Filter_Input($filter, $validators, $dataParamValue, $options);
		$data = array();
		
		$paramsSql = array(
			'sortBy' => $sortBy,
			'sortDir' => $sortDir,
		);

		$selectMapping = $this->_db->select()
	    			 ->distinct()
					 ->from(array('T' => 'M_MAPPING'));
			
		
		
		if ($filter->isValid()){
			foreach ($dataParamValue as $key => $val){
				$key = $val;
				$this->view->{$key} = $val;
			}
			
			$tag = $filter->getEscaped('tag');
			if (!empty($tag)){
				$selectMapping->where("T.TAG LIKE ".$this->_db->quote('%'.$tag.'%'));
			}
			
			$text = $filter->getEscaped('text');
			if (!empty($text)){
				$selectMapping->where("T.TEXT LIKE ".$this->_db->quote('%'.$text.'%'));
			}
			
			$mappingto = $filter->getEscaped('mappingto');
			if (!empty($mappingto)){
				$selectMapping->where("T.MAPPING_TO LIKE ".$this->_db->quote('%'.$mappingto.'%'));
			}
			
			$desc = $filter->getEscaped('desc');
			if (!empty($desc)){
				$selectMapping->where("T.DESCRIPTION LIKE ".$this->_db->quote('%'.$desc.'%'));
			}
		}else{
			Zend_Debug::dump($filter->getMessages());
		}

		$data = $this->_db->fetchAll($selectMapping);
		
		$this->view->headers 		= $headers;
		$this->view->sortBy 		= $sortBy;
		$this->view->sortDir 		= $sortDir;
		$this->view->page 			= $page;
		
		$this->view->query_string_params = $stringParam;
		$this->view->paginator 			= $data;
		$this->view->data	 			= $data;
		// print_r($dataParamValue);die;
		if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				
				// $wherecol[]	= $key;
				// $whereval[] = $value;
				// print_r($wherecol);
			}
			// die;
			// print_r($wherecol);die;
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
    }
		$this->paging($data);
	}	


	public function uiAction(){
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');
        $model = new language_Model_Index();
		
        $sql = $model->getUi();
        // print_r($sql);die;
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";

        if($tblName == '1'){
            $selecty= 'selected';
        }else if($tblName == '0'){
            $selectn= 'selected';
        }

        // $arrUi = array(''=>'-- '.$this->language->_('Please Select').' --');
		if (count($sql)){
			$select = '';
			foreach ($sql as $key => $val){
				if($val['ID'] == $tblName){
					$select = 'selected';
				}
				$optHtml .="<option value='".$val['ID']."' ".$select.">".$this->language->_($val['DESC'])."</option>";
				$select = '';
				// $optHtml += array($val['ID'] => $val['DESC']);
			}
		}

        // $optHtml.="<option value='1' ".$selecty.">".$this->language->_('Yes')."</option>";
        // $optHtml.="<option value='0' ".$selectn.">".$this->language->_('No')."</option>";
        
        echo $optHtml;


        }

        public function langAction(){
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');
        $model = new language_Model_Index();
		
        $sql = $model->getLang();
        // print_r($sql);die;
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";

        if($tblName == '1'){
            $selecty= 'selected';
        }else if($tblName == '0'){
            $selectn= 'selected';
        }

        // $arrUi = array(''=>'-- '.$this->language->_('Please Select').' --');
		if (count($sql)){
			$select = '';
			foreach ($sql as $key => $val){
				if($val['CODE'] == $tblName){
					$select = 'selected';
				}
				$optHtml .="<option value='".$val['CODE']."' ".$select.">".$this->language->_($val['DESC'])."</option>";
				$select = '';
				// $optHtml += array($val['ID'] => $val['DESC']);
			}
		}

        // $optHtml.="<option value='1' ".$selecty.">".$this->language->_('Yes')."</option>";
        // $optHtml.="<option value='0' ".$selectn.">".$this->language->_('No')."</option>";
        
        echo $optHtml;


        }
}
