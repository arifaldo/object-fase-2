<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class bgclosingworkflow_reviewdetailController extends Application_Main
{

	protected $_moduleDB = 'RTF'; // masih harus diganti

	public function indexAction()
	{
		$this->_helper->_layout->setLayout('newlayout');

		$settings = new Settings();
		$system_type = $settings->getSetting('system_type');

		// BEGIN GLOBAL VARIABEL
		$config = Zend_Registry::get('config');
		$bgpublishType  = $config["bgpublish"]["type"]["desc"];
		$bgpublishCode  = $config["bgpublish"]["type"]["code"];
		$arrbgpublish   = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

		$bgcgType   = $config["bgcg"]["type"]["desc"];
		$bgcgCode   = $config["bgcg"]["type"]["code"];
		$arrbgcg 	= array_combine(array_values($bgcgCode), array_values($bgcgType));

		$bgType   = $config["bg"]["type"]["desc"];
		$bgCode   = $config["bg"]["type"]["code"];
		$arrbg 	= array_combine(array_values($bgCode), array_values($bgType));
		$this->view->arrbg = $arrbg;

		$bgclosingType   = $config["bgclosing"]["changetype"]["desc"];
		$bgclosingCode   = $config["bgclosing"]["changetype"]["code"];
		$arrbgclosingType 	= array_combine(array_values($bgclosingCode), array_values($bgclosingType));
		$this->view->arrbgclosingType = $arrbgclosingType;

		$bgclosingStatus   = $config["bgclosing"]["status"]["desc"];
		$bgclosingstatusCode   = $config["bgclosing"]["status"]["code"];
		$arrbgclosingStatus 	= array_combine(array_values($bgclosingstatusCode), array_values($bgclosingStatus));
		$this->view->arrbgclosingStatus = $arrbgclosingStatus;

		// END GLOBAL VARIABEL

		// BEGIN PENUTUPAN BANK GARANSI
		// BEGIN DETAIL BANK GARANSI

		$bgparam = $this->_getParam('bgnumb');
		$this->view->bgnumb_enc = $bgparam;
		$AESMYSQL = new Crypt_AESMYSQL();
		$decryption = urldecode($bgparam);
		$numb = $AESMYSQL->decrypt($decryption, $password);

		if (!empty($numb)) {
			$bgdata = $this->_db->select()
				->from(array('A' => 'T_BANK_GUARANTEE'), array('*', 'CUST_CP_T' => 'A.CUST_CP', 'CUST_CONTACT_NUMBER_T' => 'A.CUST_CONTACT_NUMBER', 'CUST_EMAIL_T' => 'A.CUST_EMAIL'))
				->joinleft(array('B' => 'M_CUSTOMER'), 'A.CUST_ID = B.CUST_ID', array('B.*'))
				->joinleft(array('C' => 'M_BRANCH'), 'A.BG_BRANCH = C.BRANCH_CODE', array('C.BRANCH_NAME'))
				->joinleft(array('D' => 'TEMP_BANK_GUARANTEE_CLOSE'), 'A.BG_NUMBER = D.BG_NUMBER', array('D.CHANGE_TYPE', 'D.SUGGESTION_STATUS', 'D.CLOSE_REF_NUMBER', 'D.CLAIM_FIRST_VERIFIED'))
				->joinleft(array('E' => 'M_CITYLIST'), 'B.CUST_CITY = E.CITY_CODE', array('E.*'))
				//->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
				->where('A.BG_NUMBER = ?', $numb)
				->query()->fetchAll();

			if (!empty($bgdata)) {
				$getBG = $bgdata['0'];

				if ($getBG['BG_NUMBER'] == '') {
					$getBG['BG_NUMBER'] = '-';
				}

				if ($getBG['BG_SUBJECT'] == '') {
					$getBG['BG_SUBJECT'] = '- no subject -';
				}

				$this->view->getBG = $getBG;
				$this->view->time_calculation = $this->calculate_time_span($getBG['CLAIM_FIRST_VERIFIED']);


				$this->view->publishDesc = $arrbgpublish[$getBG['BG_PUBLISH']];
				if ($getBG['BG_PUBLISH'] == 1) {
					$getPaperPrint = $this->_db->select()
						->from('M_PAPER')
						->where('notes = ?', $getBG['BG_NUMBER'])
						->where('STATUS = 1')
						->query()->fetchAll();

					$totalKertas = count($getPaperPrint);

					$this->view->nomorRefKertas = ' - Nomor Ref Kertas : ' . $getPaperPrint[0]['PAPER_ID'] . ' - ' . $getPaperPrint[$totalKertas - 1]['PAPER_ID'] . ' (' . $totalKertas . ' lembar)';
				}

				// BEGIN NILAI BANK GARANSI

				$bgdetaildata = $this->_db->select()
					->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
					->where('A.BG_REG_NUMBER = ?', $getBG["BG_REG_NUMBER"])
					->query()->fetchAll();


				if (!empty($bgdetaildata)) {
					foreach ($bgdetaildata as $key => $value) {
						if ($value['PS_FIELDNAME'] == 'Currency') {
							$this->view->bgCurrency =   $value['PS_FIELDVALUE'];
						}

						if ($getBG['COUNTER_WARRANTY_TYPE'] == '3') {

							if ($value['PS_FIELDNAME'] == 'Insurance Name') {
								$insuranceName = $this->_db->select()
									->from("M_CUSTOMER")
									->where("CUST_ID = ?", $value['PS_FIELDVALUE'])
									->query()->fetchAll();

								$this->view->insuraceName =  $insuranceName[0]['CUST_NAME'];
							}

							if ($value['PS_FIELDNAME'] == 'Insurance Branch') {

								$insuranceBranch = $this->_db->select()
									->from("M_INS_BRANCH")
									->where("INS_BRANCH_CODE = ?", $value['PS_FIELDVALUE'])
									->query()->fetchAll();

								$this->view->insuranceBranch = $insuranceBranch[0]["INS_BRANCH_NAME"];
							}
						}
					}
				}

				$this->view->BG_AMOUNT  = $getBG['BG_AMOUNT'];
				$this->view->updateStart = Application_Helper_General::convertDate($getBG['TIME_PERIOD_START'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->updateEnd = Application_Helper_General::convertDate($getBG['TIME_PERIOD_END'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->bgClaimDate = Application_Helper_General::convertDate($getBG['BG_CLAIM_DATE'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->bgIssuedDate = Application_Helper_General::convertDate($getBG['BG_ISSUED'], $this->view->viewDateFormat, $this->view->defaultDateFormat);

				// END NILAI BANK GARANSI

				// BEGIN KONTRA GARANSI
				$this->view->counterWarrantyType = $getBG['COUNTER_WARRANTY_TYPE'];
				$this->view->counterWarrantyDesc = $arrbgcg[$getBG['COUNTER_WARRANTY_TYPE']];
				$this->view->changeType = $getBG['CHANGE_TYPE'];

				// END KONTRA GARANSI

				// BEGIN LAMPIRAN DOKUMEN

				$bgdatafile = $this->_db->select()
					->from(array('A' => 'TEMP_BANK_GUARANTEE_FILE_CLOSE'), array('*'))
					->where('A.CLOSE_REF_NUMBER = ?', $getBG['CLOSE_REF_NUMBER'])
					->query()->fetchAll();
				$this->view->lampiranDokumen = $bgdatafile;

				// END LAMPIRAN DOKUMEN

				// BEGIN MARGINAL DEPOSIT PRINCIPAL
				$saveHoldAmount = null;
				$saveEscrow = 0;

				$bgdatasplit = $this->_db->select()
					->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
					->joinLeft(['B' => 'M_CUSTOMER_ACCT'], 'A.ACCT = B.ACCT_NO AND B.CUST_ID = ' . $this->_db->quote($getBG['CUST_ID']) . '', ["M_ACCT_DESC" => "B.ACCT_DESC", "M_ACCT_TYPE" => "B.ACCT_TYPE", 'CUST_ID', 'CCY_ID'])
					->where('A.BG_REG_NUMBER = ?', $getBG['BG_REG_NUMBER'])
					->query()->fetchAll();


				foreach ($bgdatasplit as $key => $value) {
					// if ($value['CUST_ID'] != $getBG['CUST_ID']) continue;
					// if ($value["ACCT_DESC"]) continue;

					// if (strtolower($value["M_ACCT_DESC"]) == "giro" || $value["M_ACCT_TYPE"] == "D") continue;
					// $saveHoldAmount += floatval($value["AMOUNT"]);

					if (strtolower($value["M_ACCT_DESC"]) == "giro" || $value["M_ACCT_TYPE"] == "D") {
						continue;
					};
					$serviceAccount = new Service_Account($value['ACCT'], null);
					$accountInfo = $serviceAccount->inquiryAccontInfo();
					$serviceCif = new Service_Account(null, null, null, null, null, $accountInfo["cif"]);
					$saveResult = $serviceCif->inquiryCIFAccount()["accounts"];
					$listAccount = array_search($value['ACCT'], array_column($saveResult, "account_number"));

					if ($saveResult[$listAccount]["product_type"] == 'J1') {
						continue;
					} else {
						$saveHoldAmount += floatval($value["AMOUNT"]);
					}

					if (strtolower($value["ACCT_DESC"] == 'Escrow')) $saveEscrow += floatval($value["AMOUNT"]);

					// $saveHoldAmount += intval($value["AMOUNT"]);
				}

				$this->view->holdAmount = $saveHoldAmount;
				$this->view->escrow = $saveEscrow;
				// END MARGINAL DEPOSIT PRINCIPAL

				// BEGIN PELEPASAN DANA
				// BEGIN LIST ACCOUNT PELEPASAN DANA
				$dataListAccount = $this->_db->select()
					->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL_CLOSE'), array('*'))
					->where('A.CLOSE_REF_NUMBER = ?', $getBG['CLOSE_REF_NUMBER'])
					->query()->fetchAll();

				$jmlListAccount = $this->_db->select()
					->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL_CLOSE'), array('*'))
					->where('A.CLOSE_REF_NUMBER = ?', $getBG['CLOSE_REF_NUMBER'])
					->group(array("ACCT_INDEX"))
					->query()->fetchAll();
				// echo '<pre>';
				// var_dump($jumlahAccount);
				// die;

				if (!empty($jmlListAccount)) {
					$jumlahAccount = count($jmlListAccount);
				}

				$htmlListAccount .= '<table class="table table-sm" id="guarantedTransactions">
												<thead>
													<tr style="padding-left: 2%;">
														<th>Own Account </th>
														<th>Account Number</th>
														<th></th>
														<th>Account Name</th>
														<th>Currency</th>
														<th>Nominal</th>
														
													</tr>
												</thead>
											<tbody id="tbody">';
				foreach ($dataListAccount as $key => $val) {
					$data[$val['PS_FIELDNAME']][$val['ACCT_INDEX']] = $val['PS_FIELDVALUE'];
				}

				$this->view->jumlahAccount = $jumlahAccount;

				$arrOwn = array("0" => "T", "1" => "Y");
				for ($i = 1; $i <= $jumlahAccount; $i++) {

					if (!$data['Beneficiary Account Number'][$i]) {
						$this->view->jumlahAccount = 0;
					}
					$htmlListAccount .= '<tr>
										<td style="padding-left: 2%;">' . $data['Beneficiary is own account'][$i] . '</td>
										<td class="listrekeningMD">' . $data['Beneficiary Account Number'][$i] . '</td>
										<td><div class="hidden ml-2 border border-danger rounded-circle position-relative" style="width: 20px; height: 20px;" id="iconErrorMD' . $data['Beneficiary Account Number'][$i] . '"><div class="d-flex justify-content-center text-danger">i</div>
                                            <div class="position-absolute border border-danger text-danger hidden p-2" style="background-color: white; width: 214px;z-index: 9999; top: 0; left: 30px"><span id="errorMD' . $data['Beneficiary Account Number'][$i] . '">Status rekening tidak aktif</span></div>
                                        </div></td>
										<td>' . $data['Beneficiary Account Name'][$i] . '</td>
										<td>' . $data['Beneficiary Account CCY'][$i] . '</td>
										<td>' . Application_Helper_General::displayMoney($data['Transaction Amount'][$i]) . '</td>
										
									</tr>
									';
					$totalAmount += intval(str_replace(',', '', $data['Transaction Amount'][$i]));
				}

				$htmlListAccount .= '</tbody></table>';

				$this->view->listAccount = $htmlListAccount;
				$this->view->transferAmount = $totalAmount;
				$this->view->totalPelepasanDana = Application_Helper_General::displayMoney($totalAmount);
				// END LIST ACCOUNT PELEPASAN DANA
				// END PELEPASAN DANA

			}
		}
		// END DETAIL BANK GARANSI

		// END PENUTUPAN BANK GARANSI


		if (!empty($numb)) {


			$bgdata = $this->_db->select()
				->from(["A" => "T_BANK_GUARANTEE"], ["*"])
				->join(array('B' => 'M_CUSTOMER'), 'A.CUST_ID = B.CUST_ID')
				->join(array('C' => 'M_BRANCH'), 'A.BG_BRANCH = C.BRANCH_CODE')
				->join(array('D' => 'TEMP_BANK_GUARANTEE_CLOSE'), 'A.BG_NUMBER = D.BG_NUMBER')
				->where('A.BG_NUMBER = ?', $numb)
				->where('D.SUGGESTION_STATUS IN (5)')
				->query()->fetchAll();

			$tbgdata = $this->_db->select()
				->from(["A" => "T_BANK_GUARANTEE"], ["*"])
				->joinLeft(array('B' => 'M_CUSTOMER'), 'A.BG_INSURANCE_CODE = B.CUST_ID', array('B.CUST_NAME'))
				->join(array('D' => 'TEMP_BANK_GUARANTEE_CLOSE'), 'A.BG_NUMBER = D.BG_NUMBER')
				->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
				->where('A.BG_REG_NUMBER = ?', $numb)
				->where('A.BG_STATUS IN (2, 22)')
				->query()->fetchAll();

			$bgdatadetail = $this->_db->select()
				->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
				->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
				->where('A.BG_REG_NUMBER = ?', $numb)
				->query()->fetchAll();

			// Guaranted Transaction -----------------------

			$getGuarantedTransanctions = $this->_db->select()
				->from('TEMP_BANK_GUARANTEE_UNDERLYING')
				->where('BG_REG_NUMBER = ?', $numb)
				->query()->fetchAll();

			$getGuarantedTransanctionsT = $this->_db->select()
				->from('T_BANK_GUARANTEE_UNDERLYING')
				->where('BG_REG_NUMBER = ?', $numb)
				->query()->fetchAll();

			$this->view->guarantedTransanctions = $getGuarantedTransanctions;
			$this->view->guarantedTransanctionsT = $getGuarantedTransanctionsT;

			$bgdocType 		= $conf["bgdoc"]["type"]["desc"];
			$bgdocCode 		= $conf["bgdoc"]["type"]["code"];

			$arrbgdoc = array_combine(array_values($bgdocCode), array_values($bgdocType));
			$this->view->arrbgdoc = $arrbgdoc;

			// end Guaranted Transaction -------------------

			if (!empty($bgdata)) {

				$this->view->bgdata = $bgdata[0];
				$this->view->tbgdata = $tbgdata[0];

				// if ($bgdata[0]['CHANGE_TYPE'] == '0' && !empty($tbgdata[0])) {
				// 	$diff = array_diff($bgdata[0], $tbgdata[0]);
				// 	if (!empty($diff)) {
				// 		echo '<pre>';
				// 		var_dump($diff);
				// 		die;
				// 	}
				// }

				// switch ($bgdata[0]["CHANGE_TYPE"]) {
				// 	case '0':
				// 		$this->view->suggestion_type = "New";
				// 		break;
				// 	case '1':
				// 		$this->view->suggestion_type = "Amendment Changes";
				// 		break;
				// 	case '2':
				// 		$this->view->suggestion_type = "Amendment Draft";
				// 		break;
				// }

				// new code untuk amendemen
				$getct        = Zend_Registry::get('config');
				$ctdesc     = $getct["bg"]["changetype"]["desc"];
				$ctcode     = $getct["bg"]["changetype"]["code"];
				$suggestion_type = array_combine(array_values($ctcode), array_values($ctdesc));

				$this->view->suggestion_type = $suggestion_type;

				$this->view->boundaryExist = false;

				$boundaryCek = $this->_db->select()
					->from("M_APP_GROUP_USER")
					->where("CUST_ID = ?", $this->_custIdLogin)
					->where("USER_ID = ?", $this->_userIdLogin);
				$boundaryCek = $this->_db->fetchRow($boundaryCek);



				if (!empty($boundaryCek)) {
					$cekExistBoundary = $this->_db->select()
						->from("M_APP_BOUNDARY_GROUP")
						->where("GROUP_USER_ID = ?", $boundaryCek["GROUP_USER_ID"]);
					$cekExistBoundary = $this->_db->fetchRow($cekExistBoundary);

					if (!empty($cekExistBoundary)) {
						$this->view->boundaryExist = true;
					}
				}

				$boundary = $this->validatebtn('38', $bgdata['0']['BG_AMOUNT'], 'IDR', $numb);


				if ($boundary) {
					// die;
					$this->view->validbtn = false;
				} else {
					// die;
					$this->view->validbtn = true;
				}

				$policyBoundary = $this->findPolicyBoundary(38, $bgdata['0']['BG_AMOUNT']);
				//echo '<pre>';
				//var_dump($policyBoundary);die;
				$this->view->policyBoundary = $policyBoundary;

				$approverUserList = $this->findUserBoundary(38, $bgdata['0']['BG_AMOUNT']);


				$releaserList = $this->findUserPrivi('RLBG');

				$bgpublishType 		= $config["bgpublish"]["type"]["desc"];
				$bgpublishCode 		= $config["bgpublish"]["type"]["code"];

				$arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

				$this->view->BG_PUBLISH = $arrbgpublish[$bgdata[0]['BG_PUBLISH']];

				if ($bgdata[0]["COUNTER_WARRANTY_TYPE"] == "3") {
					$getInsuranceBranch = array_search("Insurance Branch", array_column($bgdetaildata, "PS_FIELDNAME"));
					$getInsuranceBranch = $bgdetaildata[$getInsuranceBranch];

					$insuranceBranch = $this->_db->select()
						->from("M_INS_BRANCH")
						->where("INS_BRANCH_CODE = ?", $getInsuranceBranch["PS_FIELDVALUE"])
						->query()->fetchAll();

					$this->view->insurance_branch = $insuranceBranch[0]["INS_BRANCH_NAME"];
				}

				if ($bgdata['0']['COUNTER_WARRANTY_TYPE'] == '1') {
					$bgdatasplit = $this->_db->select()
						->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
						->where('A.BG_REG_NUMBER = ?', $numb)
						->query()->fetchAll();

					foreach ($bgdatasplit as $key => $value) {
						$temp_save = $this->_db->select()
							->from("M_CUSTOMER_ACCT")
							->where("ACCT_NO = ?", $value["ACCT"])
							->query()->fetchAll();

						$bgdatasplit[$key]["CURRENCY"] = $temp_save[0]["CCY_ID"];
						$bgdatasplit[$key]["TYPE"] = $temp_save[0]["ACCT_DESC"];
					}

					$this->view->fullmember = $bgdatasplit;
				}

				//BG Counter Guarantee Type
				$bgcgType         = $conf["bgcg"]["type"]["desc"];
				$bgcgCode         = $conf["bgcg"]["type"]["code"];

				$arrbgcg = array_combine(array_values($bgcgCode), array_values($bgcgType));

				$this->view->warranty_type_text_new = $arrbgcg[$bgdata[0]['COUNTER_WARRANTY_TYPE']];

				$selectHistory	= $this->_db->select()
					->from(['TBGHC' => 'T_BANK_GUARANTEE_HISTORY_CLOSE'])
					->joinLeft(['MB' => 'M_BUSER'], 'TBGHC.USER_LOGIN = MB.BUSER_ID', ['BUSER_NAME'])
					->order('TBGHC.DATE_TIME DESC')
					->where("BG_NUMBER = ?", $numb);

				$history = $this->_db->fetchAll($selectHistory);

				$config = Zend_Registry::get('config');

				$historyStatusCode = $config["bgclosinghistory"]["status"]["code"];
				$historyStatusDesc = $config["bgclosinghistory"]["status"]["desc"];
				$historyStatusArr  = array_combine(array_values($historyStatusCode), array_values($historyStatusDesc));

				$this->view->historyStatusArr   = $historyStatusArr;
				$this->view->dataHistory        = $history;

				$cust_approver = 1;

				$bg_submission_hisotrys = $this->_db->select()
					->from('T_BANK_GUARANTEE_HISTORY_CLOSE')
					->where("BG_NUMBER = ?", $numb)
					->order('DATE_TIME DESC')
					->where("CUST_ID IN (?) ", [$getBG["CUST_ID"], $getBG["BG_INSURANCE_CODE"]]);

				$bg_submission_hisotrys = $this->_db->fetchAll($bg_submission_hisotrys);

				$getBoundary = $this->_db->select()
					->from('M_APP_BOUNDARY')
					->where('CUST_ID = ?', $getBG['CUST_ID'])
					->where('BOUNDARY_MIN <= \'' . $getBG['BG_AMOUNT'] . '\'')
					->where('BOUNDARY_MAX >= \'' . $getBG['BG_AMOUNT'] . '\'')
					->query()->fetch();

				$checkBoundary = strpos($getBoundary['POLICY'], 'THEN');
				if ($checkBoundary === false) $checkBoundary = strpos($getBoundary['POLICY'], 'AND');
				$checkCount = 1;

				foreach ($bg_submission_hisotrys as $bg_submission_hisotry) {

					// maker
					if ($bg_submission_hisotry['HISTORY_STATUS'] == 1) {
						$makerStatus = 'active';
						$makerIcon = '<i class="fas fa-check"></i>';

						$custlogin = $bg_submission_hisotry['USER_LOGIN'];

						$selectCust	= $this->_db->select()
							->from('M_USER')
							->where("USER_ID = ?", $custlogin)
							->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

						$customer = $this->_db->fetchAll($selectCust);

						$custFullname = $customer[0]['USER_FULLNAME'];
						// $custFullname = $customer[0]['USER_ID'];

						$efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));

						$align = 'align="center"';

						$this->view->makerBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
					}

					// approver
					if ($checkBoundary === false) {
						if ($bg_submission_hisotry['HISTORY_STATUS'] == 2) {
							$approverStatus = 'active';
							$approverIcon = '<i class="fas fa-check"></i>';

							$custlogin = $bg_submission_hisotry['USER_LOGIN'];

							$selectCust	= $this->_db->select()
								->from('M_USER')
								->where("USER_ID = ?", $custlogin)
								->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

							$customer = $this->_db->fetchAll($selectCust);

							$custFullname = $customer[0]['USER_FULLNAME'];
							// $custFullname = $customer[0]['USER_ID'];

							$efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));

							$align = 'align="center"';

							$this->view->approverBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';

							$this->view->approverStatus = $approverStatus;
						}
					} else {
						if ($bg_submission_hisotry['HISTORY_STATUS'] == 2) {
							$approverStatus = 'active';
							$approverIcon = '<i class="fas fa-check"></i>';

							$custlogin = $bg_submission_hisotry['USER_LOGIN'];

							$selectCust	= $this->_db->select()
								->from('M_USER')
								->where("USER_ID = ?", $custlogin)
								->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

							$customer = $this->_db->fetchAll($selectCust);

							// $custFullname = $customer[0]['USER_FULLNAME'];
							if ($checkCount === 1) {
								$custFullname = $customer[0]['USER_FULLNAME'];
								$checkCount++;
							} else {
								$custFullname = $custFullname . "<br><span>" . $customer[0]['USER_FULLNAME'] . "</span>";
								$checkCount = 1;
							}

							$efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));

							$align = 'align="center"';

							$this->view->approverBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';

							$this->view->approverStatus = $approverStatus;
						}
					}

					if ($bgdata[0]["COUNTER_WARRANTY_TYPE"] == '3') {
						//if releaser
						if ($bg_submission_hisotry['HISTORY_STATUS'] == 3) {
							$releaserStatus = 'active';
							$releaserIcon = '<i class="fas fa-check"></i>';

							$custlogin = $bg_submission_hisotry['USER_LOGIN'];

							$selectCust	= $this->_db->select()
								->from('M_USER')
								->where("USER_ID = ?", $custlogin)
								->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

							$customer = $this->_db->fetchAll($selectCust);

							$custFullname = $customer[0]['USER_FULLNAME'];
							// $custFullname = $customer[0]['USER_ID'];

							$efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));

							$align = 'align="center"';

							$this->view->releaserBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
							$this->view->releaserStatus = $releaserStatus;
						}
					} else {
						//if releaser
						if ($bg_submission_hisotry['HISTORY_STATUS'] == 3) {
							$releaserStatus = 'active';
							$releaserIcon = '<i class="fas fa-check"></i>';

							$custlogin = $bg_submission_hisotry['USER_LOGIN'];

							$selectCust	= $this->_db->select()
								->from('M_USER')
								->where("USER_ID = ?", $custlogin)
								->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

							$customer = $this->_db->fetchAll($selectCust);

							$custFullname = $customer[0]['USER_FULLNAME'];
							// $custFullname = $customer[0]['USER_ID'];

							$efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));

							$align = 'align="center"';

							$this->view->releaserBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
							$this->view->releaserStatus = $releaserStatus;
						}
					}
				}

				foreach ($history as $row) {

					if ($data["COUNTER_WARRANTY_TYPE"] == '3') {
						if ($row['HISTORY_STATUS'] == 8 || $row['HISTORY_STATUS'] == 24) {
							$makerStatus = 'active';
							$insuranceIcon = '<i class="fas fa-check"></i>';
							/*$approveStatus = 'active';
							$reviewStatus = 'active';
							$releaseStatus = 'active';
							$releaseIcon = '<i class="fas fa-check"></i>';*/
							$insuranceStatus = 'active';
							$reviewStatus = '';
							//$releaseStatus = '';
							//$releaseIcon = '';

							$makerOngoing = '';
							$reviewerOngoing = '';
							$approverOngoing = '';
							//$releaserOngoing = '';

							$custlogin = $row['USER_LOGIN'];

							$selectCust    = $this->_db->select()
								->from('M_USER')
								->where("USER_ID = ?", $custlogin);
							//->where("CUST_ID = ?", $row['CUST_ID']);

							$customer = $this->_db->fetchAll($selectCust);

							// $custFullname = $customer[0]['USER_ID'];
							$custFullname = $customer[0]['USER_FULLNAME'];
							// $custEmail 	  = $customer[0]['USER_EMAIL'];
							// $custPhone	  = $customer[0]['USER_PHONE'];

							$insuranceApprovedBy = $custFullname;

							$align = 'align="center"';
							$marginLeft = '';
							if ($cust_reviewer == 0 && $cust_approver == 0) {
								$align = '';
								$marginLeft = 'style="margin-left: 15px;"';
							}

							$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
							$this->view->insuranceApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
						}
					}

					if ($row['HISTORY_STATUS'] == 6) {
						$verifyStatus = 'active';
						$verifyIcon = '<i class="fas fa-check"></i>';

						$verifyOngoing = '';
						if ($cust_reviewer == 0 && $cust_approver == 0) { //kalau tidak ada priv reviewer & approver
							$reviewerOngoing = '';
							$verifyOngoing = '';
							$approverOngoing = '';
							//$releaserOngoing = 'ongoing';
							$releaserOngoing = '';
						} else {
							//$reviewerOngoing = 'ongoing';
							$reviewerOngoing = '';
							$verifyOngoing = '';
							$approverOngoing = '';
							$releaserOngoing = '';
						}

						$custlogin = $row['USER_LOGIN'];

						$selectCust	= $this->_db->select()
							->from('M_BUSER')
							->where("BUSER_ID = ?", $custlogin);

						$customer = $this->_db->fetchAll($selectCust);

						$custFullname = $customer[0]['BUSER_NAME'];

						$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));

						$align = 'align="center"';
						$marginRight = '';

						if ($cust_reviewer == 0 && $cust_approver == 0) {
							$align = '';
							$marginRight = 'style="margin-right: 15px;"';
						}

						$this->view->verifyApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span ' . $marginRight . '>' . $custFullname . '</span></div>';
					}
					//if reviewer done

					//if approver done
					if ($row['HISTORY_STATUS'] == 16) {
						$makerStatus = 'active';
						$approveStatus = '';
						$reviewStatus = '';

						$makerOngoing = '';
						$reviewerOngoing = '';
						$approverOngoing = 'ongoing';
						$releaserOngoing = '';

						$custlogin = $row['USER_LOGIN'];

						$selectuserapp	= $this->_db->select()
							->from(array('C' => 'T_APPROVAL'), array(
								'*'
							))
							->where("C.PS_NUMBER = ?", $numb)
							->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
							//->where("C.USER_ID 	= ?", (string) $this->_userIdLogin)
						;

						$userapprove = $this->_db->fetchAll($selectuserapp);
						//tampung data user yang sudah approve
						if (!empty($userapprove) && empty($userid)) {
							foreach ($userapprove as $vl) {
								$userid[] = $vl['USER_ID'];
							}
						}
						//$userid[] = $custlogin;

						$approveEfDate[] = date('d-M-Y', strtotime($row['DATE_TIME']));
					}
					//if releaser done
					if ($row['HISTORY_STATUS'] == 5) {
						$makerStatus = 'active';
						/*$approveStatus = 'active';
						$reviewStatus = 'active';
						$releaseStatus = 'active';
						$releaseIcon = '<i class="fas fa-check"></i>';*/
						$approveStatus = '';
						$reviewStatus = '';
						$releaseStatus = '';
						$releaseIcon = '';

						$makerOngoing = '';
						$reviewerOngoing = '';
						$approverOngoing = '';
						$releaserOngoing = '';

						$custlogin = $row['USER_LOGIN'];

						$selectCust	= $this->_db->select()
							->from('M_USER')
							->where("USER_ID = ?", $custlogin)
							->where("CUST_ID = ?", $row['CUST_ID']);

						$customer = $this->_db->fetchAll($selectCust);

						$custFullname = $customer[0]['USER_FULLNAME'];
						// $custEmail 	  = $customer[0]['USER_EMAIL'];
						// $custPhone	  = $customer[0]['USER_PHONE'];

						$releaserApprovedBy = $custFullname;

						$align = 'align="center"';
						$marginLeft = '';
						if ($cust_reviewer == 0 && $cust_approver == 0) {
							$align = '';
							$marginLeft = 'style="margin-left: 15px;"';
						}

						$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
						//$this->view->releaserApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span ' . $marginLeft . '>' . $custFullname . '</span></div>';
					}
				}

				//approvernamecircle jika sudah ada yang approve
				if (!empty($userid)) {

					$alphabet = array('01' => 'A', '02' => 'B', '03' => 'C', '04' => 'D', '05' => 'E', '06' => 'F', '07' => 'G', '08' => 'H', '09' => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z', 27 => 'SG');

					$flipAlphabet = array_flip($alphabet);

					$approvedNameList = array();
					$i = 0;
					//var_dump($userid);die;
					foreach ($userid as $key => $value) {

						//select utk nama dan email
						$selectusername = $this->_db->select()
							->from(array('M_USER'), array(
								'*'
							))
							->where("USER_ID = ?", (string) $value)
							->where("CUST_ID = ?", (string) $this->_custIdLogin);

						$username = $this->_db->fetchAll($selectusername);

						//select utk cek user berada di grup apa
						$selectusergroup	= $this->_db->select()
							->from(array('C' => 'M_APP_GROUP_USER'), array(
								'*'
							))
							->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
							->where("C.USER_ID 	= ?", (string) $value);

						$usergroup = $this->_db->fetchAll($selectusergroup);

						$groupuserid = $usergroup[0]['GROUP_USER_ID'];
						$groupusername = $usergroup[0]['USER_ID'];
						$groupuseridexplode = explode("_", $groupuserid);

						if ($groupuseridexplode[0] == "S") {
							$usergroupid = "SG";
						} else {
							$usergroupid = $alphabet[$groupuseridexplode[2]];
						}

						// $tempuserid = "";
						// foreach ($approverNameCircle as $row => $data) {
						// 	foreach ($data as $keys => $val) {
						// 		if ($keys == $usergroupid) {
						// 			if (preg_match("/active/", $val)) {
						// 				continue;
						// 			}else{
						// 				if ($groupuserid == $tempuserid) {
						// 					continue;
						// 				}else{
						// 					$approverNameCircle[$row][$keys] = '<button class="btnCircleGroup active hovertext" style="margin-right: 1%; margin-top: 12%;" disabled>3
						// 			<span class="hovertextcontent" style="padding-left: 15px;">'.$username[0]['USER_FULLNAME'].'<br>'.$username[0]['USER_EMAIL'].'</span></button>';
						// 				}
						// 				$tempuserid = $groupuserid;
						// 			}
						// 		}
						// 	}
						// }

						array_push($approvedNameList, $username[0]['USER_FULLNAME']);

						$efdate = $approveEfDate[$i];

						$approverApprovedBy[] = '<div align="center" class="textTheme">' . $efdate . '<br>' . $username[0]['USER_FULLNAME'] . ' (' . $usergroupid . ')</div>';
						$i++;
					}
					//var_dump($approverApprovedBy);die;
					$this->view->approverApprovedBy = $approverApprovedBy;


					//kalau sudah approve semua
					if (!$checkBoundary) {
						$approveStatus = '';
						$approverOngoing = '';
						$approveIcon = '';
						$releaserOngoing = 'ongoing';
					}
				}

				$selectsuperuser = $this->_db->select()
					->from(array('C' => 'T_APPROVAL'))
					->where("C.PS_NUMBER 	= ?", $numb)
					->where("C.GROUP 	= 'SG'");

				$superuser = $this->_db->fetchAll($selectsuperuser);

				if (!empty($superuser)) {
					$userid = $superuser[0]['USER_ID'];

					//select utk nama dan email
					$selectusername = $this->_db->select()
						->from(array('M_USER'), array(
							'*'
						))
						->where("USER_ID = ?", (string) $userid)
						->where("CUST_ID = ?", (string) $this->_custIdLogin);

					$username = $this->_db->fetchAll($selectusername);

					$approverApprovedBy[] = '<div align="center" class="textTheme">' . $efdate . '<br>' . $username[0]['USER_FULLNAME'] . ' (' . $usergroupid . ')</div>';

					$approveStatus = 'active';
					$approverOngoing = '';
					$approveIcon = '<i class="fas fa-check"></i>';
					$releaserOngoing = 'ongoing';
				}
				// <span class="hovertextcontent" style="text-align: center;"><p class="m-0 text-white-50" style="font-size: 13px">'.$makerApprovedBy.'</p></span>
				//define circle
				$makerNameCircle = '<button id="makerCircle" style="cursor:default" class="btnCircleGroup ' . $makerStatus . ' ' . $makerOngoing . ' hovertext" disabled>' . $makerIcon . ' </button>';

				$insuranceNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $insuranceStatus . ' ' . $insuranceOngoing . ' hovertext" disabled>' . $insuranceIcon . ' </button>';

				foreach ($reviewerList as $key => $value) {

					$textColor = '';
					if ($value == $reviewerApprovedBy) {
						$textColor = 'text-white-50';
					}

					$reviewerListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value . '</p>';
				}
				// 
				$reviewerNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $reviewStatus . ' ' . $reviewerOngoing . ' hovertext" disabled>' . $reviewIcon . ' </button>';

				$verifyNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $verifyStatus . ' ' . $verifyOngoing . ' hovertext" disabled>' . $verifyIcon . ' </button>';

				$bankReviewNameCircle = '<button style="cursor:default" class="btnCircleGroup hovertext" disabled></button>';


				$groupNameList = $approverUserList['GROUP_NAME'];
				unset($approverUserList['GROUP_NAME']);

				if ($approverUserList != '') {
					//echo '<pre>';
					//var_dump($approverUserList);die;
					foreach ($approverUserList as $key => $value) {
						$approverListdata .= $key . ' (' . $groupNameList[$key] . ')' . '<br>';
						$i = 1;
						foreach ($value as $key2 => $value2) {

							$textColor = '';
							if (in_array($value2, $approvedNameList)) {
								$textColor = 'text-white-50';
							}

							if ($i == count($value)) {
								$approverListdata .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value2 . '</p><br>';
							} else {
								$approverListdata .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value2 . '</p>';
							}
							$i++;
						}
					}
				} else {
					$approverListdata = 'There is no Approver User';
				}
				// 
				$spandata = '';
				if (!empty($approverListdata) && !$error_msg2) {
					$spandata = '<span class="hovertextcontentapprover" style="text-align: center;">' . $approverListdata . '</span>';
				}

				$approverNameCircle = '<button class="btnCircleGroup ' . $approveStatus . ' ' . $approverOngoing . ' hovertext" disabled>' . $approveIcon . '
						' . $spandata . '
					</button>';

				foreach ($releaserList as $key => $value) {

					$textColor = '';
					if ($value == $releaserApprovedBy) {
						$textColor = 'text-white-50';
					}

					$releaserListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value . '</p>';
				}
				// 
				// $releaserNameCircle = '<button id="releaserCircle" class="btnCircleGroup ' . $releaseStatus . ' ' . $releaserOngoing . ' hovertext" disabled>' . $releaseIcon . ' <span class="hovertextcontent" style="left: -100px; text-align: center;">' . $releaserListView . '</span> </button>';
				$releaserNameCircle = '<button id="releaserCircle" class="btnCircleGroup ' . $releaseStatus . ' ' . $releaserOngoing . ' hovertext" disabled>' . $releaseIcon . '</button>';


				$this->view->policyBoundary = $policyBoundary;
				$this->view->makerNameCircle = $makerNameCircle;
				$this->view->reviewerNameCircle = $reviewerNameCircle;
				$this->view->verifyNameCircle = $verifyNameCircle;
				$this->view->insuranceNameCircle = $insuranceNameCircle;
				$this->view->bankReviewNameCircle = $bankReviewNameCircle;

				$this->view->approverNameCircle = $approverNameCircle;
				$this->view->releaserNameCircle = $releaserNameCircle;

				$this->view->makerStatus = $makerStatus;
				$this->view->verifyStatus = $verifyStatus;
				$this->view->approveStatus = $approveStatus;
				$this->view->reviewStatus = $reviewStatus;
				$this->view->releaseStatus = $releaseStatus;


				$data = $bgdata['0'];

				if (!empty($data['BG_BRANCH'])) {
					$selectbranch = $this->_db->select()
						->from(array('A' => 'M_BRANCH'), array('*'))
						->where('A.BRANCH_CODE = ?', $data['BG_BRANCH'])
						->query()->fetchAll();
					//var_dump($selectbranch[0]['BRANCH_NAME']);die;
					$this->view->branchname = $selectbranch[0]['BRANCH_NAME'];
				}

				$this->view->updateStart = Application_Helper_General::convertDate($data['TIME_PERIOD_START'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->updateEnd = Application_Helper_General::convertDate($data['TIME_PERIOD_END'], $this->view->viewDateFormat, $this->view->defaultDateFormat);

				$this->view->TIME_PERIOD_START = $data['TIME_PERIOD_START'];
				$this->view->TIME_PERIOD_END = $data['TIME_PERIOD_END'];


				//$config    		= Zend_Registry::get('config');

				$BgType 		= $config["bg"]["status"]["desc"];
				$BgCode 		= $config["bg"]["status"]["code"];

				$arrStatus = array_combine(array_values($BgCode), array_values($BgType));

				$this->view->arrStatus = $arrStatus;
				$cust_id = $this->_custIdLogin;
				if ($data["COUNTER_WARRANTY_TYPE"] == 3 || $data["COUNTER_WARRANTY_TYPE"] == '3') {
					$cust_id = $this->_db->select()
						->from("T_BANK_GUARANTEE_DETAIL")
						->where("BG_REG_NUMBER = ?", $getBG["BG_REG_NUMBER"])
						->where("PS_FIELDNAME = ?", 'Insurance Name')
						->query()->fetch();

					$cust_id = $cust_id["PS_FIELDVALUE"];
				}
				$CustomerUser = new CustomerUser($cust_id, $this->_userIdLogin);
				$param = array('CCY_IN' => 'IDR', 'ACCT_NO' => $data['FEE_CHARGE_TO']);
				$AccArr = $CustomerUser->getAccountsBG($param);
				// var_dump($AccArr);
				// die;

				if (!empty($AccArr)) {
					$this->view->src_name = $AccArr['0']['ACCT_NAME'];
					$this->view->currency_type = $AccArr['0']['CCY_ID'];
				}

				$arrBankFormat = array(
					1 => 'Bank Standard',
					2 => 'Special Format (with bank approval)'
				);

				$this->view->bankFormat = $arrBankFormat[$data['BG_FORMAT']];
				$this->view->bankFormatNumber = $data['BG_FORMAT'];

				$arrLang = array(
					1 => 'Indonesian',
					2 => 'English',
					3 => 'Billingual',
				);

				$this->view->languagetext = $arrLang[$data['BG_LANGUAGE']];

				$arrWaranty = array(
					1 => 'Full Cover, Savings Account (Giro / Time Deposit / Savings) or Cash Deposit (MD)',
					2 => 'Indirect Credit Facility (Non Cash Loan) / Bank Guarantee Ceiling',
					3 => 'Insurance'

				);

				$this->view->repairto = ["prinsipal" => "Prinsipal", "asuransi" => "Asuransi"];
				if ($data['COUNTER_WARRANTY_TYPE'] == 1 || $data['COUNTER_WARRANTY_TYPE'] == 2) {
					$this->view->repairto = ["prinsipal" => "Prinsipal"];
				}

				$this->view->warranty_type_text = $arrWaranty[$data['COUNTER_WARRANTY_TYPE']];
				/*		
                         if(!empty($data['USAGE_PURPOSE'])){
                            $data['USAGE_PURPOSE'] = explode(',', $data['USAGE_PURPOSE']);
                            foreach($data['USAGE_PURPOSE'] as $key => $val){
                                $str = 'checkp'.$val;
                                //var_dump($str);
                                $this->view->$str =  'checked';
                            }
                        }
						*/
				$this->view->usage_purpose = $data['USAGE_PURPOSE'];

				if ($data['BG_NUMBER'] == '') {
					$data['BG_NUMBER'] = '-';
				}
				if ($data['BG_SUBJECT'] == '') {
					$data['BG_SUBJECT'] = '- no subject -';
				}
				$this->view->BG_REG_NUMBER = $data['BG_REG_NUMBER'];
				$this->view->BG_NUMBER = $data['BG_NUMBER'];
				$this->view->BG_SUBJECT = $data['BG_SUBJECT'];
				$this->view->data = $data;

				$this->view->recipent_name = $data['RECIPIENT_NAME'];
				$this->view->address = $data['RECIPIENT_ADDRES'];
				$this->view->city = $data['RECIPIENT_CITY'];
				$this->view->contact_number = $data['RECIPIENT_CONTACT'];
				$this->view->contact_person = $data['RECIPIENT_CP'];
				$this->view->phone = $data['RECIPIENT_OFFICE_NUMBER'];
				$this->view->contact_email = $data['RECIPIENT_EMAIL'];

				$this->view->comment = $data['SERVICE'];

				$this->view->fileName = $data['FILE'];
				$this->view->GT_DATE = Application_Helper_General::convertDate($data['GT_DOC_DATE'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->GT_NUMBER = $data['GT_DOC_NUMBER'];
				$bgdocType 		= $conf["bgdoc"]["type"]["desc"];
				$bgdocCode 		= $conf["bgdoc"]["type"]["code"];

				$arrbgdoc = array_combine(array_values($bgdocCode), array_values($bgdocType));

				$this->view->GT_TYPE = $arrbgdoc[$data['GT_DOC_TYPE']];
				//$this->view->GT_TYPE = $data['GT_DOC_TYPE'];
				$this->view->GT_OTHERS = $data['GT_DOC_OTHER'];
				$bgpublishType 		= $conf["bgpublish"]["type"]["desc"];
				$bgpublishCode 		= $conf["bgpublish"]["type"]["code"];

				$arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

				$this->view->publishForm = $arrbgpublish[$data['BG_PUBLISH']];


				$this->view->bank_amount = $data['BG_AMOUNT'];
				$this->view->BG_CLAIM_DATE = $data['BG_CLAIM_DATE'];
				$this->view->warranty_type = $data['COUNTER_WARRANTY_TYPE'];
				$this->view->account_number = $data['COUNTER_WARRANTY_ACCT_NO'];
				$this->view->account_name = $data['COUNTER_WARRANTY_ACCT_NAME'];

				$this->view->amount = $data['COUNTER_WARRANTY_AMOUNT'];
				$this->view->acct = $data['FEE_CHARGE_TO'];
				$this->view->status = $data['BG_STATUS'];
				$Settings = new Settings();
				$claim_period = $Settings->getSetting('claim_period');
				$this->view->claim_period = $claim_period;

				if ($data['BG_STATUS'] == '7' ||  $data['BG_STATUS'] == '10' || !empty($data['BG_REJECT_NOTES']) || !empty($data['BG_CANCEL_NOTES'])) {
					$selectQuery  = "SELECT
                                a.USER_LOGIN,
                                b.`USER_FULLNAME` AS u_name,
                                c.`BUSER_NAME` AS b_name,
                                a.DATE_TIME,
                                a.BG_REASON,
                                a.HISTORY_STATUS,
                                a.BG_REG_NUMBER
                                
                                
                              FROM
                                TEMP_BANK_GUARANTEE_HISTORY AS a
                                LEFT JOIN M_USER AS b ON a.`USER_LOGIN` = b.`USER_ID` AND a.`CUST_ID` = b.`CUST_ID`
                                LEFT JOIN `M_BUSER` AS c ON a.`USER_LOGIN` = c.`BUSER_ID`
                              WHERE a.BG_REG_NUMBER = " . $this->_db->quote((string) $data['BG_REG_NUMBER']) . " AND a.HISTORY_STATUS = " . $this->_db->quote((string) $data['BG_STATUS']) . " GROUP BY HISTORY_ID ORDER BY DATE_TIME";
					$result =  $this->_db->fetchAll($selectQuery);
					if (!empty($result)) {
						$data['REASON'] = $result['0']['BG_REASON'];
					}
					$this->view->reqrepair = true;
					$this->view->reason = $data['REASON'] . '' . $data['BG_REJECT_NOTES'] . $data['BG_CANCEL_NOTES'];
				}

				if (!empty($bgdatadetail)) {
					foreach ($bgdatadetail as $key => $value) {

						if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
							if ($value['PS_FIELDNAME'] == 'Insurance Name') {
								$this->view->insuranceName =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
								$this->view->PrincipalAgreement =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount') {
								$this->view->insurance_amount =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement Start Date') {
								$this->view->paDateStart =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement End Date') {
								$this->view->paDateEnd =   $value['PS_FIELDVALUE'];
							}
						} else {

							if ($value['PS_FIELDNAME'] == 'Plafond Owner') {
								$this->view->owner1 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner') {
								$this->view->amountowner1 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
								$this->view->owner2 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
								$this->view->amountowner2 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
								$this->view->owner3 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
								$this->view->amountowner3 =   $value['PS_FIELDVALUE'];
							}
						}
					}
				}



				$conf = Zend_Registry::get('config');
				$this->view->bankname = $conf['app']['bankname'];

				$check_boundary	= $this->_db->select()
					->from(array('C' => 'M_APP_BOUNDARY'), array(
						'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
						'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
						'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
						'C.TRANSFER_TYPE',
						'C.POLICY'
					))
					->where("C.TRANSFER_TYPE 	= ?", (string) '38')
					->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
					->where("C.BOUNDARY_MIN 	<= ?", $bgdata['0']['BG_AMOUNT'])
					->where("C.BOUNDARY_MAX 	>= ?", $bgdata['0']['BG_AMOUNT']);
				//echo $check_boundary;
				$check_boundary = $this->_db->fetchAll($check_boundary);

				$this->view->inOfBoundary = true;
				if (empty($check_boundary)) {
					$this->view->inOfBoundary = false;
				}

				$errLimitIns = false;
				if ($data["COUNTER_WARRANTY_TYPE"] == "3" && $data['BG_STATUS'] == '22') {
					$principleData = [];
					// if ($data["COUNTER_WARRANTY_TYPE"] == "3") {
					foreach ($bgdatadetail as $key => $value) {
						$principleData[$value["PS_FIELDNAME"]] = $value["PS_FIELDVALUE"];
					}
					$cust_id = $principleData["Insurance Name"];
					$get_linefacilityINS = $this->_db->select()
						->from("M_CUST_LINEFACILITY", ["CUST_ID", "PLAFOND_LIMIT", "FEE_PROVISION", "FEE_ADMIN", "FEE_STAMP", "CUST_SEGMENT", "STATUS"])
						->where("CUST_ID = ?", $cust_id)
						->query()->fetchAll();


					$check_all_detail = $this->_db->select()
						->from("T_BANK_GUARANTEE_DETAIL")
						->where("PS_FIELDNAME = ?", "Insurance Name")
						->where("PS_FIELDVALUE = ?", $cust_id)
						->query()->fetchAll();

					$total_bgamount_on_risk = 0;

					if (count($check_all_detail) > 0) {
						$save_bg_reg_number = [];
						foreach ($check_all_detail as $value) {
							array_push($save_bg_reg_number, $value["BG_REG_NUMBER"]);
						}

						$get_bgamount_on_risks = $this->_db->select()
							->from("T_BANK_GUARANTEE", ["BG_AMOUNT"])
							->where("(BG_STATUS = 15) AND COUNTER_WARRANTY_TYPE = '3'")
							->where("BG_REG_NUMBER IN (?)", $save_bg_reg_number)
							->query()->fetchAll();

						foreach ($get_bgamount_on_risks as $get_bgamount_on_risk) {
							$total_bgamount_on_risk += $get_bgamount_on_risk["BG_AMOUNT"];
						}
					}

					$check_all_detail = $this->_db->select()
						->from("TEMP_BANK_GUARANTEE_DETAIL")
						->where("PS_FIELDNAME = ?", "Insurance Name")
						->where("PS_FIELDVALUE = ?", $cust_id)
						->query()->fetchAll();

					$total_bgamount_on_temp = 0;

					if (count($check_all_detail) > 0) {

						$save_bg_reg_number = [];
						foreach ($check_all_detail as $value) {
							array_push($save_bg_reg_number, $value["BG_REG_NUMBER"]);
						}

						$get_bgamount_on_temps = $this->_db->select()
							->from("TEMP_BANK_GUARANTEE", ["BG_AMOUNT", "BG_REG_NUMBER"])
							->where("COUNTER_WARRANTY_TYPE = '3'")
							->where("BG_REG_NUMBER IN (?)", $save_bg_reg_number)
							->where("BG_STATUS IN (?)", ["5", "6", "7", '10', "14", "17", "20", '21', '22', '23', '24'])
							->query()->fetchAll();

						foreach ($get_bgamount_on_temps as $key => $get_bgamount_on_temp) {
							$total_bgamount_on_temp += $get_bgamount_on_temp["BG_AMOUNT"];
						}
					}

					$current_limitINS = $get_linefacilityINS[0]["PLAFOND_LIMIT"] - $total_bgamount_on_risk - $total_bgamount_on_temp;

					$this->view->current_limit_ins = $current_limitINS;
					// if ($data['BG_AMOUNT'] >= $current_limitINS) {
					if ($current_limitINS < 0) {
						$errLimitIns = true;

						$errmsg = 'Limit asuransi tidak tersedia';
						$this->view->errorTickerSize = $errmsg;
					}

					// die;

				}

				$bgdatadetailclose = $this->_db->select()
					->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL_CLOSE'), array('*'))
					->where('A.CLOSE_REF_NUMBER = ?', $getBG['CLOSE_REF_NUMBER'])
					->query()->fetchAll();

				if (!empty($bgdatadetailclose)) {
					foreach ($bgdatadetailclose as $key => $value) {
						if ($value['PS_FIELDNAME'] == 'Beneficiary is own account') {
							$this->view->rekeningPenerima =   $value['PS_FIELDVALUE'];
						}

						if ($value['PS_FIELDNAME'] == 'SWIFT CODE') {
							$domesticBanks = $this->_db->select()
								->from(array('A' => 'M_DOMESTIC_BANK_TABLE'), array('*'))
								->where('A.SWIFT_CODE = ?', $value['PS_FIELDVALUE'])
								->query()->fetchAll();

							$this->view->swiftCode = $domesticBanks[0]['BANK_NAME'];
							$this->view->swiftCodeParam = $value['PS_FIELDVALUE'];
						}

						if ($value['PS_FIELDNAME'] == 'Beneficiary Account CCY') {
							$this->view->valutaRekening =   $value['PS_FIELDVALUE'];
						}

						if ($value['PS_FIELDNAME'] == 'Beneficiary Account Number') {
							$this->view->nomorRekening =   $value['PS_FIELDVALUE'];
						}

						if ($value['PS_FIELDNAME'] == 'Beneficiary Account Name') {
							$this->view->namaRekening =   $value['PS_FIELDVALUE'];
						}

						if ($value['PS_FIELDNAME'] == 'Beneficiary Address') {
							$this->view->alamatRekening =   $value['PS_FIELDVALUE'];
						}

						if ($value['PS_FIELDNAME'] == 'Transaction CCY') {
							$this->view->valutaTrasaksi =   $value['PS_FIELDVALUE'];
						}

						if ($value['PS_FIELDNAME'] == 'Transaction Amount') {
							$this->view->totalTransaksi =   $value['PS_FIELDVALUE'];
						}

						if ($value['PS_FIELDNAME'] == 'Remark 1') {
							$this->view->berita =   $value['PS_FIELDVALUE'];
						}
					}

					// $response = $this->inquiryRekening($acct_no, $swiftcode, $acct_name);
					// $this->view->resInqRekStatus = $response['status'];
					// $this->view->resInqRekMessage = $response['message'];

				}

				$download = $this->_getParam('download');
				//print_r($edit);die;
				if ($download) {
					$attahmentDestination = UPLOAD_PATH . '/document/submit/';
					$this->_helper->download->file($data['FILE'], $attahmentDestination . $data['FILE']);
				}

				// $klaimErr = false;
				// $cekKlaimStatus = $this->_db->select()
				// 	->from('TEMP_BANK_GUARANTEE_CLOSE')
				// 	->where('BG_NUMBER = ?', $getBG['BG_NUMBER'])
				// 	->where('CHANGE_TYPE = 3')
				// 	->where('SUGGESTION_STATUS NOT IN (7,11)')
				// 	->query()->fetch();

				// if (!empty($cekKlaimStatus)) {
				// 	$klaimErr = true;
				// }
				// $this->view->klaimErr = $klaimErr;

				if ($this->_request->isPost()) {
					$review = $this->_getParam('review');
					$BG_NUMBER = $this->_getParam('bgnumb');
					$BG_NUMBER     = urldecode($this->_getParam('bgnumb'));

					$BG_NUMBER = $AESMYSQL->decrypt($BG_NUMBER, $password);
					$reject = $this->_getParam('reject');
					$reqrever = $this->_getParam('reqrever');
					$repair = $this->_getParam('repair');

					//$datas = $this->_request->getParams();
					//echo '<pre>';
					//var_dump($datas);die;
					
					// BEGIN GLOBAL VARIABEL
					$config = Zend_Registry::get('config');

					$bgcgType   = $config["bgcg"]["type"]["desc"];
					$bgcgCode   = $config["bgcg"]["type"]["code"];
					$arrbgcg 	= $this->combineCodeAndDesc($bgcgCode, $bgcgType);
					$this->view->arrbgcg = $arrbgcg;

					$bgclosingType   = $config["bgclosing"]["changetype"]["desc"];
					$bgclosingCode   = $config["bgclosing"]["changetype"]["code"];
					$arrbgclosingType 	= $this->combineCodeAndDesc($bgclosingCode, $bgclosingType);
					$this->view->arrbgclosingType = $arrbgclosingType;

					$bgclosingStatus   = $config["bgclosing"]["status"]["desc"];
					$bgclosingstatusCode   = $config["bgclosing"]["status"]["code"];
					$arrbgclosingStatus 	= $this->combineCodeAndDesc($bgclosingstatusCode, $bgclosingStatus);

					$this->view->arrbgclosingStatus = $arrbgclosingStatus;
					// END GLOBAL VARIABEL
					if ($review) {

						$historyInsert = array(
							'DATE_TIME'         => new Zend_Db_Expr("now()"),
							'CLOSE_REF_NUMBER'  => $getBG['CLOSE_REF_NUMBER'],
							'BG_NUMBER'         => $getBG['BG_NUMBER'],
							'CUST_ID'           => "-",
							'USER_FROM'       	=> 'BANK',
							'USER_LOGIN'        => $this->_userIdLogin,
							'BG_REASON' 		=> 'NoRef Pengajuan : ' . $getBG['CLOSE_REF_NUMBER'] . '',
							'HISTORY_STATUS'    => 10,
						);

						$this->_db->insert('T_BANK_GUARANTEE_HISTORY_CLOSE', $historyInsert);

						$dataUpdate = array(
							'SUGGESTION_STATUS' => 6,
							'LASTUPDATED' => new Zend_Db_Expr("now()"),
							'LASTUPDATEDFROM' => 'BANK',
							'LASTUPDATEDBY' => $this->_userIdLogin,
						);

						$where['CLOSE_REF_NUMBER = ?'] = $getBG['CLOSE_REF_NUMBER'];
						$this->_db->update('TEMP_BANK_GUARANTEE_CLOSE', $dataUpdate, $where);

						if ($getBG['COUNTER_WARRANTY_TYPE'] == '1') {
							if ($getBG['CHANGE_TYPE'] == '1' || $getBG['CHANGE_TYPE'] == '2') {
								Application_Helper_General::writeLog('RCCS', 'Review Pengajuan Penutupan untuk BG No : ' . $getBG['BG_NUMBER'] . ', Principal : ' . $getBG['CUST_ID'] . ', NoRef Pengajuan : ' . $getBG['CLOSE_REF_NUMBER'] . '');
							} else {
								Application_Helper_General::writeLog('RCCS', 'Review Pengajuan Klaim untuk BG No : ' . $getBG['BG_NUMBER'] . ', Principal : ' . $getBG['CUST_ID'] . ', NoRef Pengajuan : ' . $getBG['CLOSE_REF_NUMBER'] . '');
							}
						} else {
							if ($getBG['CHANGE_TYPE'] == '1' || $getBG['CHANGE_TYPE'] == '2') {
								Application_Helper_General::writeLog('RNCS', 'Review Pengajuan Penutupan untuk BG No : ' . $getBG['BG_NUMBER'] . ', Principal : ' . $getBG['CUST_ID'] . ', NoRef Pengajuan : ' . $getBG['CLOSE_REF_NUMBER'] . '');
							} else {
								Application_Helper_General::writeLog('RNCS', 'Review Pengajuan Klaim untuk BG No : ' . $getBG['BG_NUMBER'] . ', Principal : ' . $getBG['CUST_ID'] . ', NoRef Pengajuan : ' . $getBG['CLOSE_REF_NUMBER'] . '');
							}
						}

						// NOTIF EMAIL

						$setting = new Settings();
						$allSetting = $setting->getAllSetting();

						$contractName = $this->handleDokumenUnderlying($getBG['BG_REG_NUMBER']);
						$subjectToApprover = $arrbgclosingStatus[6] . ' BG NO. ' . $getBG["BG_NUMBER"];

						if ($bgdata['0']['CHANGE_TYPE'] == 1) {
							$titleChangeType = 'PENUTUPAN';
						} else if ($bgdata['0']['CHANGE_TYPE'] == 3) {
							$titleChangeType = 'KLAIM';
						} else {
							$titleChangeType = 'PENUTUPAN / KLAIM';
						}

						$data = [
							'[[title_change_type]]' => $titleChangeType,
							'[[close_ref_number]]' => $bgdata['0']['CLOSE_REF_NUMBER'],
							'[[bg_number]]' => $bgdata['0']['BG_NUMBER'],
							'[[contract_name]]' => $contractName,
							'[[usage_purpose]]' => $bgdata[0]["USAGE_PURPOSE_DESC"],
							'[[cust_name]]' => $bgdata['0']["CUST_NAME"],
							'[[recipient_name]]' => $bgdata['0']['RECIPIENT_NAME'],
							'[[counter_warranty_type]]' => $arrbgcg[$bgdata['0']['COUNTER_WARRANTY_TYPE']],
							'[[bg_amount]]' => $this->view->valutaTrasaksi . ' ' . number_format($bgdata[0]["BG_AMOUNT"], 2),
							'[[change_type]]' => $arrbgclosingType[$bgdata['0']['CHANGE_TYPE']],
							'[[suggestion_status]]' => $arrbgclosingStatus[6],
							'[[repair_notes]]' => '-',
							'[[master_bank_name]]' => $allSetting["master_bank_name"],
						];

						$getEmailTemplate = $allSetting['bemailtemplate_6A'];
						$getEmailTemplate = strtr($getEmailTemplate, $data);

						$privi = $bgdata['0']['COUNTER_WARRANTY_TYPE'] == '1' ? ['APCC'] : ['APNC'];
						$buserEmails = $this->buserEmail($privi, $bgdata['0']['BG_BRANCH']);

						if ($bgdata['0']['CHANGE_TYPE'] == 1) {
							$approverBUserList = $this->findBUserBoundary(50, $bgdata['0']['BG_AMOUNT']);
							unset($approverBUserList['GROUP_NAME']);
	
							if ($approverBUserList || $buserEmails) {
								foreach ($approverBUserList as $subArray) {
									foreach ($subArray as $item) {
										if (in_array($item, array_column($buserEmails, 'BUSER_EMAIL')) ) {
											$this->sendEmail($item, $subjectToApprover, $getEmailTemplate);
										}
									}
								}
							}

						} else {
							if (!empty($buserEmails)) {
								foreach ($buserEmails as $buser) {
									$this->sendEmail($buser['BUSER_EMAIL'], $subjectToApprover, $getEmailTemplate);
								}
							}
						}

						
						$this->setbackURL('/' . $this->_request->getModuleName() . '/review/');
						$this->_redirect('/notification/success/index');
					}

					if ($reject) {
						$notes = $this->_getParam('PS_REASON_REJECT');

						$historyInsert = array(
							'DATE_TIME'         => new Zend_Db_Expr("now()"),
							'CLOSE_REF_NUMBER'         => $getBG['CLOSE_REF_NUMBER'],
							'BG_NUMBER'         => $getBG['BG_NUMBER'],
							'CUST_ID'           => "-",
							'USER_FROM'        => 'BANK',
							'USER_LOGIN'        => $this->_userIdLogin,
							'BG_REASON'        => $notes,
							'HISTORY_STATUS'        => 7,
						);

						$this->_db->insert('T_BANK_GUARANTEE_HISTORY_CLOSE', $historyInsert);

						$dataUpdate = array(
							'SUGGESTION_STATUS' => 7,
							'LASTUPDATED' => new Zend_Db_Expr("now()"),
							'LASTUPDATEDFROM' => 'BANK',
							'LASTUPDATEDBY' => $this->_userIdLogin,
						);

						$where['CLOSE_REF_NUMBER = ?'] = $getBG['CLOSE_REF_NUMBER'];
						$this->_db->update('TEMP_BANK_GUARANTEE_CLOSE', $dataUpdate, $where);

						if ($getBG['COUNTER_WARRANTY_TYPE'] == '1') {
							if ($getBG['CHANGE_TYPE'] == '1' || $getBG['CHANGE_TYPE'] == '2') {
								Application_Helper_General::writeLog('RCCS', 'Penolakan Pengajuan Penutupan untuk BG No : ' . $getBG['BG_NUMBER'] . ', Principal : ' . $getBG['CUST_ID'] . ', NoRef Pengajuan : ' . $getBG['CLOSE_REF_NUMBER'] . ', Catatan : ' . $notes . '');
							} else {
								Application_Helper_General::writeLog('RCCS', 'Penolakan Pengajuan Klaim untuk BG No : ' . $getBG['BG_NUMBER'] . ', Principal : ' . $getBG['CUST_ID'] . ', NoRef Pengajuan : ' . $getBG['CLOSE_REF_NUMBER'] . ', Catatan : ' . $notes . '');
							}
						} else {
							if ($getBG['CHANGE_TYPE'] == '1' || $getBG['CHANGE_TYPE'] == '2') {
								Application_Helper_General::writeLog('RNCS', 'Penolakan Pengajuan Penutupan untuk BG No : ' . $getBG['BG_NUMBER'] . ', Principal : ' . $getBG['CUST_ID'] . ', NoRef Pengajuan : ' . $getBG['CLOSE_REF_NUMBER'] . ', Catatan : ' . $notes . '');
							} else {
								Application_Helper_General::writeLog('RNCS', 'Penolakan Pengajuan Klaim untuk BG No : ' . $getBG['BG_NUMBER'] . ', Principal : ' . $getBG['CUST_ID'] . ', NoRef Pengajuan : ' . $getBG['CLOSE_REF_NUMBER'] . ', Catatan : ' . $notes . '');
							}
						}

						// SEND EMAIL

						// BEGIN GLOBAL VARIABEL
						$config = Zend_Registry::get('config');

						$bgcgType   = $config["bgcg"]["type"]["desc"];
						$bgcgCode   = $config["bgcg"]["type"]["code"];
						$arrbgcg 	= $this->combineCodeAndDesc($bgcgCode, $bgcgType);
						$this->view->arrbgcg = $arrbgcg;

						$bgclosingType   = $config["bgclosing"]["changetype"]["desc"];
						$bgclosingCode   = $config["bgclosing"]["changetype"]["code"];
						$arrbgclosingType 	= $this->combineCodeAndDesc($bgclosingCode, $bgclosingType);
						$this->view->arrbgclosingType = $arrbgclosingType;

						$bgclosingStatus   = $config["bgclosing"]["status"]["desc"];
						$bgclosingstatusCode   = $config["bgclosing"]["status"]["code"];
						$arrbgclosingStatus 	= $this->combineCodeAndDesc($bgclosingstatusCode, $bgclosingStatus);

						$this->view->arrbgclosingStatus = $arrbgclosingStatus;
						// END GLOBAL VARIABEL

						$setting = new Settings();
						$allSetting = $setting->getAllSetting();

						if ($bgdata['0']['CHANGE_TYPE'] == 1) {
							$titleChangeType = 'PENUTUPAN';
						} else if ($bgdata['0']['CHANGE_TYPE'] == 3) {
							$titleChangeType = 'KLAIM';
						} else {
							$titleChangeType = 'PENUTUPAN / KLAIM';
						}

						$contractName = $this->handleDokumenUnderlying($getBG['BG_REG_NUMBER']);

						$dataEmail = [
							'[[title_change_type]]' => $titleChangeType,
							'[[close_ref_number]]' => $bgdata['0']['CLOSE_REF_NUMBER'],
							'[[bg_number]]' => $bgdata['0']['BG_NUMBER'],
							'[[contract_name]]' => $contractName,
							'[[usage_purpose]]' => $bgdata['0']["USAGE_PURPOSE_DESC"],
							'[[cust_cp]]' => $bgdata['0']['CUST_CP'],
							'[[cust_name]]' => $bgdata['0']["CUST_NAME"],
							'[[recipient_cp]]' => $bgdata['0']['RECIPIENT_CP'],
							'[[recipient_name]]' => $bgdata['0']['RECIPIENT_NAME'],
							'[[bg_amount]]' => $this->view->valutaTrasaksi . ' ' . number_format($bgdata['0']["BG_AMOUNT"], 2),
							'[[counter_warranty_type]]' => $arrbgcg[$bgdata['0']['COUNTER_WARRANTY_TYPE']],
							'[[change_type]]' => $arrbgclosingType[$bgdata['0']['CHANGE_TYPE']],
							'[[suggestion_status]]' => $arrbgclosingStatus[7],
							'[[repair_notes]]' => $notes,
							'[[master_bank_email]]' => $allSetting["master_bank_email"],
							'[[master_bank_telp]]' => $allSetting["master_bank_telp"],
							'[[master_app_name]]' => $allSetting["master_bank_app_name"],
							'[[master_bank_name]]' => $allSetting["master_bank_name"],
						];

						if ($bgdata['0']['CHANGE_TYPE'] == 3) {
							// Notifikasi email ke Kacab Penerbitan
							$subjectToBranch = 'PEMBERITAHUAN PENGAJUAN KLAIM BG NO. ' . $data["BG_NUMBER"];
							$emailTemplateToBranch = $allSetting['bemailtemplate_7A'];
							$emailContentToBranch = strtr($emailTemplateToBranch, $dataEmail);
							$this->sendEmail($selectbranch[0]['BRANCH_EMAIL'], $subjectToBranch, $emailContentToBranch);

							// Notifikasi email ke Group Kontra
							switch (true) {
								case $getBG['COUNTER_WARRANTY_TYPE'] == '1':
									// SEND EMAIL TO GROUP CASH COL
									if ($allSetting['email_group_cashcoll']) Application_Helper_Email::sendEmail($allSetting['email_group_cashcoll'], $subjectToBranch, $emailContentToBranch);
									break;

								case $getBG['COUNTER_WARRANTY_TYPE'] == '2':
									// SEND EMAIL TO GROUP LINE FACILITY
									if ($allSetting['email_group_linefacility']) Application_Helper_Email::sendEmail($allSetting['email_group_linefacility'], $subjectToBranch, $emailContentToBranch);
									break;

								case $getBG['COUNTER_WARRANTY_TYPE'] == '3':
									// SEND EMAIL TO GROUP ASURANSI
									if ($allSetting['email_group_insurance']) Application_Helper_Email::sendEmail($allSetting['email_group_insurance'], $subjectToBranch, $emailContentToBranch);
									break;

								default:
									break;
							}

							// Notifikasi email ke Asuransi
							if ($getBG['COUNTER_WARRANTY_TYPE'] == 3 && $bgdata['0']['CHANGE_TYPE'] == 3) {
								$emailTemplateToInsurance = $allSetting['bemailtemplate_7D'];
								$subjectToInsurance = 'PEMBERITAHUAN KEPUTUSAN PENGAJUAN KLAIM BG NO. ' . $bgdata['0']["BG_NUMBER"];

								$branchFullName = $this->_db->select()
									->from('M_CUSTOMER')
									->where('CUST_ID = ?', $insuranceBranch[0]["CUST_ID"])
									->query()->fetchAll();
		
								$dataContentToInsurance = array_merge($dataEmail, [
									'[[insurance_name]]' => $branchFullName[0]['CUST_NAME'],
									'[[insurance_branch_name]]' => $insuranceBranch[0]["INS_BRANCH_NAME"],
								]);

								$emailInsurance = $insuranceBranch[0]["INS_BRANCH_EMAIL"];
								$emailTemplateToInsurance = strtr($emailTemplateToInsurance, $dataContentToInsurance);
								$this->sendEmail($emailInsurance, $subjectToInsurance, $emailTemplateToInsurance);
							}
						}

						// Notifikasi email ke Nasabah/Prinsipal 
						$getEmailTemplatePrinsipal = $allSetting['bemailtemplate_7B'];
						$subjectToPrinciple = 'PEMBERITAHUAN KEPUTUSAN PENGAJUAN ' . $titleChangeType . ' BG NO. ' . $bgdata['0']["BG_NUMBER"];
						$getEmailTemplatePrinsipal = strtr($getEmailTemplatePrinsipal, $dataEmail);
						$emailAddresses = $getBG['CUST_EMAIL_T'];
						$this->sendEmail($emailAddresses, $subjectToPrinciple, $getEmailTemplatePrinsipal);

						// Notifikasi email ke Obligee
						$getEmailTemplateObligee = $allSetting['bemailtemplate_7C'];
						$subjectToObligee = 'PEMBERITAHUAN KEPUTUSAN PENGAJUAN ' . $titleChangeType . ' BG NO. ' . $bgdata['0']["BG_NUMBER"];
						$getEmailTemplateObligee = strtr($getEmailTemplateObligee, $dataEmail);
						$emailAddresses = $bgdata['0']['RECIPIENT_EMAIL'];
						$this->sendEmail($emailAddresses, $subjectToObligee, $getEmailTemplateObligee);
						
						$this->setbackURL('/' . $this->_request->getModuleName() . '/review/');
						$this->_redirect('/notification/success/index');
					}


					if ($reqrever) {
						$notes = $this->_getParam('PS_REASON_REVER');

						$historyInsert = array(
							'DATE_TIME'         => new Zend_Db_Expr("now()"),
							'CLOSE_REF_NUMBER'  => $getBG['CLOSE_REF_NUMBER'],
							'BG_NUMBER'         => $getBG['BG_NUMBER'],
							'CUST_ID'           => '-',
							'USER_FROM'        => 'BANK',
							'USER_LOGIN'        => $this->_userIdLogin,
							'BG_REASON'        => $notes,
							'HISTORY_STATUS'        => 9,
						);

						$this->_db->insert('T_BANK_GUARANTEE_HISTORY_CLOSE', $historyInsert);

						$dataUpdate = array(
							'SUGGESTION_STATUS' => 4,
							'LASTUPDATED' => new Zend_Db_Expr("now()"),
							'LASTUPDATEDFROM' => 'BANK',
							'LASTUPDATEDBY' => $this->_userIdLogin,
						);

						$where['CLOSE_REF_NUMBER = ?'] = $getBG['CLOSE_REF_NUMBER'];
						$this->_db->update('TEMP_BANK_GUARANTEE_CLOSE', $dataUpdate, $where);

						if ($getBG['COUNTER_WARRANTY_TYPE'] == '1') {
							if ($getBG['CHANGE_TYPE'] == '1' || $getBG['CHANGE_TYPE'] == '2') {
								Application_Helper_General::writeLog('RCCS', 'Permintaan Perbaikan Pengajuan Penutupan untuk BG No : ' . $getBG['BG_NUMBER'] . ', Principal : ' . $getBG['CUST_ID'] . ', NoRef Pengajuan : ' . $getBG['CLOSE_REF_NUMBER'] . ', Catatan : ' . $notes . '');
							} else {
								Application_Helper_General::writeLog('RCCS', 'Permintaan Perbaikan Pengajuan Klaim untuk BG No : ' . $getBG['BG_NUMBER'] . ', Principal : ' . $getBG['CUST_ID'] . ', NoRef Pengajuan : ' . $getBG['CLOSE_REF_NUMBER'] . ', Catatan : ' . $notes . '');
							}
						} else {
							if ($getBG['CHANGE_TYPE'] == '1' || $getBG['CHANGE_TYPE'] == '2') {
								Application_Helper_General::writeLog('RNCS', 'Permintaan Perbaikan Pengajuan Penutupan untuk BG No : ' . $getBG['BG_NUMBER'] . ', Principal : ' . $getBG['CUST_ID'] . ', NoRef Pengajuan : ' . $getBG['CLOSE_REF_NUMBER'] . ', Catatan : ' . $notes . '');
							} else {
								Application_Helper_General::writeLog('RNCS', 'Permintaan Perbaikan Pengajuan Klaim untuk BG No : ' . $getBG['BG_NUMBER'] . ', Principal : ' . $getBG['CUST_ID'] . ', NoRef Pengajuan : ' . $getBG['CLOSE_REF_NUMBER'] . ', Catatan : ' . $notes . '');
							}
						}

						// NOTIF EMAIL
						$setting = new Settings();
						$allSetting = $setting->getAllSetting();

						$contractName = $this->handleDokumenUnderlying($getBG['BG_REG_NUMBER']);

						$dataEmail = [
							'[[title_change_type]]' => 'PENUTUPAN',
							'[[close_ref_number]]' => $bgdata['0']['CLOSE_REF_NUMBER'],
							'[[bg_number]]' => $bgdata['0']['BG_NUMBER'],
							'[[contract_name]]' => $contractName,
							'[[usage_purpose]]' => $bgdata[0]["USAGE_PURPOSE_DESC"],
							'[[cust_name]]' => $bgdata['0']["CUST_NAME"],
							'[[recipient_name]]' => $bgdata['0']['RECIPIENT_NAME'],
							'[[counter_warranty_type]]' => $arrbgcg[$bgdata['0']['COUNTER_WARRANTY_TYPE']],
							'[[bg_amount]]' => $this->view->valutaTrasaksi . ' ' . number_format($bgdata[0]["BG_AMOUNT"], 2),
							'[[change_type]]' => $arrbgclosingType[$bgdata['0']['CHANGE_TYPE']],
							'[[suggestion_status]]' => $arrbgclosingStatus[9],
							'[[repair_notes]]' => $notes,
							'[[master_bank_name]]' => $allSetting["master_bank_name"],
						];

						$getEmailTemplate = $allSetting['bemailtemplate_6A'];
						$getEmailTemplate = strtr($getEmailTemplate, $dataEmail);
						$subjectToVerify = $arrbgclosingStatus[9] . ' BG NO. ' . $getBG["BG_NUMBER"];
						
						$privi = $bgdata['0']['COUNTER_WARRANTY_TYPE'] == '1' ? ['VCCS'] : ['VNCS', 'PADK'];
						$buserEmails = $this->buserEmail($privi, $bgdata['0']['BG_BRANCH']);
						
						if (!empty($buserEmails)) {
							foreach ($buserEmails as $buser) {
								$this->sendEmail($buser['BUSER_EMAIL'], $subjectToVerify, $getEmailTemplate);
							}
						}

						$this->setbackURL('/' . $this->_request->getModuleName() . '/review/');
						$this->_redirect('/notification/success/index');
					}

					if ($repair) {
						$notes = $this->_getParam('PS_REASON_REPAIR');

						// Update data ke table TEMP_BANK_GUARANTEE_CLOSE
						$dataclose = [
							'SUGGESTION_STATUS' => '10',
							'LASTUPDATED' => new Zend_Db_Expr("now()"),
							'LASTUPDATEDFROM' => 'BANK',
							'LASTUPDATEDBY' => $this->_userIdLogin,
						];

						$whereclose['CLOSE_REF_NUMBER = ?'] = $getBG['CLOSE_REF_NUMBER'];
						$this->_db->update('TEMP_BANK_GUARANTEE_CLOSE', $dataclose, $whereclose);

						// Insert data ke table T_BANK_GUARANTEE_HISTORY_CLOSE
						$historyInsert = [
							'DATE_TIME' => new Zend_Db_Expr("now()"),
							'CLOSE_REF_NUMBER' => $getBG['CLOSE_REF_NUMBER'],
							'BG_NUMBER' => $getBG['BG_NUMBER'],
							'CUST_ID' => '-',
							'USER_FROM' => 'BANK',
							'USER_LOGIN' => $this->_userIdLogin,
							'HISTORY_STATUS' => '6',
							'BG_REASON' => $notes,
						];
						$this->_db->insert('T_BANK_GUARANTEE_HISTORY_CLOSE', $historyInsert);

						// Simpan log aktifitas user ke table T_FACTIVITY
						if ($getBG['COUNTER_WARRANTY_TYPE'] == '1') {
							if ($getBG['CHANGE_TYPE'] == '1' || $getBG['CHANGE_TYPE'] == '2') {
								Application_Helper_General::writeLog('RCCS', 'Permintaan Perbaikan Pengajuan Penutupan untuk BG No : ' . $getBG['BG_NUMBER'] . ', Principal : ' . $getBG['CUST_ID'] . ', NoRef Pengajuan : ' . $getBG['CLOSE_REF_NUMBER'] . ', Catatan : ' . $notes . '');
							} else {
								Application_Helper_General::writeLog('RCCS', 'Permintaan Perbaikan Pengajuan Klaim untuk BG No : ' . $getBG['BG_NUMBER'] . ', Principal : ' . $getBG['CUST_ID'] . ', NoRef Pengajuan : ' . $getBG['CLOSE_REF_NUMBER'] . ', Catatan : ' . $notes . '');
							}
						} else {
							if ($getBG['CHANGE_TYPE'] == '1' || $getBG['CHANGE_TYPE'] == '2') {
								Application_Helper_General::writeLog('RNCS', 'Permintaan Perbaikan Pengajuan Penutupan untuk BG No : ' . $getBG['BG_NUMBER'] . ', Principal : ' . $getBG['CUST_ID'] . ', NoRef Pengajuan : ' . $getBG['CLOSE_REF_NUMBER'] . ', Catatan : ' . $notes . '');
							} else {
								Application_Helper_General::writeLog('RNCS', 'Permintaan Perbaikan Pengajuan Klaim untuk BG No : ' . $getBG['BG_NUMBER'] . ', Principal : ' . $getBG['CUST_ID'] . ', NoRef Pengajuan : ' . $getBG['CLOSE_REF_NUMBER'] . ', Catatan : ' . $notes . '');
							}
						}

						// SEND EMAIL

						// BEGIN GLOBAL VARIABEL
						$config = Zend_Registry::get('config');

						$bgcgType   = $config["bgcg"]["type"]["desc"];
						$bgcgCode   = $config["bgcg"]["type"]["code"];
						$arrbgcg 	= $this->combineCodeAndDesc($bgcgCode, $bgcgType);
						$this->view->arrbgcg = $arrbgcg;

						$bgclosingType   = $config["bgclosing"]["changetype"]["desc"];
						$bgclosingCode   = $config["bgclosing"]["changetype"]["code"];
						$arrbgclosingType 	= $this->combineCodeAndDesc($bgclosingCode, $bgclosingType);
						$this->view->arrbgclosingType = $arrbgclosingType;

						$bgclosingStatus   = $config["bgclosing"]["status"]["desc"];
						$bgclosingstatusCode   = $config["bgclosing"]["status"]["code"];
						$arrbgclosingStatus 	= $this->combineCodeAndDesc($bgclosingstatusCode, $bgclosingStatus);

						$this->view->arrbgclosingStatus = $arrbgclosingStatus;
						// END GLOBAL VARIABEL

						$setting = new Settings();
						$allSetting = $setting->getAllSetting();
						$getEmailTemplate = $allSetting['bemailtemplate_6A'];

						$contractName = $this->handleDokumenUnderlying($getBG['BG_REG_NUMBER']);

						$data = [
							'[[title_change_type]]' => 'KLAIM',
							'[[close_ref_number]]' => $bgdata['0']['CLOSE_REF_NUMBER'],
							'[[bg_number]]' => $bgdata['0']['BG_NUMBER'],
							'[[contract_name]]' => $contractName,
							'[[usage_purpose]]' => $bgdata['0']["USAGE_PURPOSE_DESC"],
							'[[cust_name]]' => $bgdata['0']["CUST_NAME"],
							'[[recipient_name]]' => $bgdata['0']['RECIPIENT_NAME'],
							'[[bg_amount]]' => $this->view->valutaTrasaksi . ' ' . number_format($bgdata['0']["BG_AMOUNT"], 2),
							'[[counter_warranty_type]]' => $arrbgcg[$bgdata['0']['COUNTER_WARRANTY_TYPE']],
							'[[change_type]]' => $arrbgclosingType[$bgdata['0']['CHANGE_TYPE']],
							'[[suggestion_status]]' => $arrbgclosingStatus[10],
							'[[repair_notes]]' => $notes,
							'[[master_bank_name]]' => $allSetting["master_bank_name"],
						];

						$getEmailTemplate = strtr($getEmailTemplate, $data);

						$privi = $bgdata['0']['COUNTER_WARRANTY_TYPE'] == '1' ? ['VPCC'] : ['VPNC', 'PADK'];
						// $emailAddresses = $this->getBuserEmailWithPrivi($privi);

						$buserEmails = $this->buserEmail($privi, $bgdata['0']['BG_BRANCH']);
						foreach ($buserEmails as $email) {
							$this->sendEmail($email['BUSER_EMAIL'], $arrbgclosingStatus[10] . ' BG NO. ' . $getBG["BG_NUMBER"], $getEmailTemplate);
						}

						$this->setbackURL('/' . $this->_request->getModuleName() . '/review/');
						$this->_redirect('/notification/success/index');
					}

					$back = $this->_getParam('back');
					if ($back) {
						$this->_redirect('/bgclosingworkflow/review');
					}
				}
			}

			if ($getBG['COUNTER_WARRANTY_TYPE'] == '1') {
				if ($getBG['CHANGE_TYPE'] == '1' || $getBG['CHANGE_TYPE'] == '2') {
					Application_Helper_General::writeLog('RCCS', 'Lihat Detail Pengajuan Penutupan dalam Proses Review Bank untuk BG No : ' . $getBG['BG_NUMBER'] . ', Principal : ' . $getBG['CUST_ID'] . ', NoRef Pengajuan : ' . $getBG['CLOSE_REF_NUMBER'] . '');
				} else {
					Application_Helper_General::writeLog('RCCS', 'Lihat Detail Pengajuan Klaim dalam Proses Review Bank untuk BG No : ' . $getBG['BG_NUMBER'] . ', Principal : ' . $getBG['CUST_ID'] . ', NoRef Pengajuan : ' . $getBG['CLOSE_REF_NUMBER'] . '');
				}
			} else {
				if ($getBG['CHANGE_TYPE'] == '1' || $getBG['CHANGE_TYPE'] == '2') {
					Application_Helper_General::writeLog('RNCS', 'Lihat Detail Pengajuan Penutupan dalam Proses Review Bank untuk BG No : ' . $getBG['BG_NUMBER'] . ', Principal : ' . $getBG['CUST_ID'] . ', NoRef Pengajuan : ' . $getBG['CLOSE_REF_NUMBER'] . '');
				} else {
					Application_Helper_General::writeLog('RNCS', 'Lihat Detail Pengajuan Klaim dalam Proses Review Bank untuk BG No : ' . $getBG['BG_NUMBER'] . ', Principal : ' . $getBG['CUST_ID'] . ', NoRef Pengajuan : ' . $getBG['CLOSE_REF_NUMBER'] . '');
				}
			}
		}
	}

	function handleDokumenUnderlying($bgRegNumber) {
		$dokumenUnderlying = $this->_db->select()
			->from('T_BANK_GUARANTEE_UNDERLYING')
			->where('BG_REG_NUMBER = ?', $bgRegNumber)
			->query()->fetchAll();
		
		if (empty($dokumenUnderlying)) {
			return '-';
		}
		
		$docName = array_column($dokumenUnderlying, 'DOC_NAME');
		$combinedDocNames = implode(', ', $docName);

		if (strlen($combinedDocNames) > 90) {
			$combinedDocNames = substr($combinedDocNames, 0, 90) . '...';
		}

		$combinedDocNames .= ' (total ' . count($dokumenUnderlying) . ' dokumen)';
		return $combinedDocNames;
	}

	function buserEmail($bprivi, $bgBranch) {
		$bpriviGroup = $this->_db->select()
			->from('M_BPRIVI_GROUP')
			->where('BPRIVI_ID IN (?)', $bprivi)
			->query()->fetchAll();

		$bpriviGroups = array_column($bpriviGroup, 'BGROUP_ID');

		$busers = $this->_db->select()
			->from(['MB' => 'M_BUSER'], ['MB.BUSER_EMAIL', 'MB.BUSER_NAME'])
			->joinLeft(['MBG' => 'M_BGROUP'], 'MB.BGROUP_ID = MBG.BGROUP_ID', ['MBG.BGROUP_DESC'])
			->joinLeft(['MBR' => 'M_BRANCH'], 'MB.BUSER_BRANCH = MBR.ID', [])
			->where('MB.BGROUP_ID IN (?)', $bpriviGroups)
			->where('MBR.BRANCH_CODE = ?', $bgBranch)
			->query()->fetchAll();
		
		return $busers;
	}

	private function getBuserEmailWithPrivi(array $privi): array
	{
		$priviJoin = array_map(function ($item) {
			return "'$item'";
		}, $privi);
		$priviJoin = join(',', $priviJoin);

		$result = $this->_db->select()
			->from(['MB' => 'M_BUSER'], ['MB.BUSER_EMAIL'])
			->where('MB.BGROUP_ID IN (?)', new Zend_Db_Expr('(SELECT BGROUP_ID FROM M_BPRIVI_GROUP mbg WHERE mbg.BPRIVI_ID IN(' . $priviJoin . ') GROUP BY BGROUP_ID)'))
			->where('MB.BUSER_EMAIL != ?', '')
			->group('MB.BUSER_EMAIL')
			->query()->fetchAll();

		return $result;
	}

	private function sendEmail($emailAddress, $subjectEmail, $emailTemplate)
	{
		$setting = new Settings();
		$allSetting = $setting->getAllSetting();

		$data = [
			'[[master_bank_name]]' => $allSetting["master_bank_name"],
			'[[master_bank_app_name]]' => $allSetting["master_bank_app_name"],
			'[[master_bank_email]]' => $allSetting["master_bank_email"],
			'[[master_bank_telp]]' => $allSetting["master_bank_telp"],
		];

		$getEmailTemplate = strtr($emailTemplate, $data);
		Application_Helper_Email::sendEmail($emailAddress, $subjectEmail, $getEmailTemplate);
	}

	private function combineCodeAndDesc($arrayCode, $arrayDesc)
	{
		return array_combine($arrayCode, array_map(function ($arrayMap) use ($arrayDesc, $arrayCode) {
			return $arrayDesc[$arrayMap];
		}, array_keys($arrayCode)));
	}

	public function findPolicyBoundary($transfertype, $amount)
	{


		$selectuser	= $this->_db->select()
			->from(array('C' => 'M_APP_BOUNDARY'), array(
				'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
				'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
				'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
				'C.TRANSFER_TYPE',
				'C.POLICY'
			))
			->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
			->where("C.BOUNDARY_MIN 	<= ?", $amount)
			->where("C.BOUNDARY_MAX 	>= ?", $amount);


		//echo $selectuser;die;
		$datauser = $this->_db->fetchAll($selectuser);

		return $datauser[0]['POLICY'];
	}

	//return tombol jika blm ada yg approve
	public function findUserBoundary($transfertype, $amount)
	{



		$selectuser	= $this->_db->select()
			->from(array('C' => 'M_APP_BOUNDARY'), array(
				'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
				'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
				'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
				'C.TRANSFER_TYPE',
				'C.POLICY'
			))
			->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
			->where("C.BOUNDARY_MIN 	<= ?", $amount)
			->where("C.BOUNDARY_MAX 	>= ?", $amount);


		//echo $selectuser;die();
		$datauser = $this->_db->fetchAll($selectuser);

		$command = str_replace('(', '', $datauser[0]['POLICY']);
		$command = str_replace(')', '', $command);
		$command = $command . ' SG';
		$list = explode(' ', $command);

		$alphabet = array('01' => 'A', '02' => 'B', '03' => 'C', '04' => 'D', '05' => 'E', '06' => 'F', '07' => 'G', '08' => 'H', '09' => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z', 27 => 'SG');

		$flipAlphabet = array_flip($alphabet);

		foreach ($list as $row => $data) {
			foreach ($alphabet as $key => $value) {
				if ($data == $value) {
					$groupuser[] = $flipAlphabet[$data];
				}
			}
		}

		$uniqueGroupUser = array_unique($groupuser);

		foreach ($uniqueGroupUser as $key => $value) {
			if ($value == '27') {
				$selectGroupName	= $this->_db->select()
					->from(array('C' => 'M_APP_GROUP_USER'), array('C.GROUP_NAME'))
					->where("C.GROUP_USER_ID LIKE ?", '%S_' . $this->_custIdLogin . '%');
			} else {
				$selectGroupName	= $this->_db->select()
					->from(array('C' => 'M_APP_GROUP_USER'), array('C.GROUP_NAME'))
					->where("C.GROUP_USER_ID LIKE ?", '%' . $this->_custIdLogin . '_' . $value . '%');
			}

			$groupNameList = $this->_db->fetchAll($selectGroupName);

			array_unique($groupNameList[0]);

			$uniqueGroupName[$value] = $groupNameList[0]['GROUP_NAME'];
		}

		foreach ($uniqueGroupName as $row => $data) {
			foreach ($alphabet as $key => $value) {
				if ($row == $key) {
					$newUniqueGroupName[$value] = $data;
				}
			}
		}

		foreach ($groupuser as $key => $value) {

			//if special group
			if ($value == 27) {
				$likecondition = "S_%";
			} else {
				$likecondition = "%" . $this->_custIdLogin . "_" . $value . "%";
			}

			$selectgroup = $this->_db->select()
				->from(array('C' => 'M_APP_GROUP_USER'), array(
					'USER_ID'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.GROUP_USER_ID LIKE ?", (string) $likecondition);

			$group_user = $this->_db->fetchAll($selectgroup);

			$groups[][$alphabet[$value]] = $group_user;
		}
		//	echo '<pre>';
		//var_dump($groups);
		$tempGroup = array();
		foreach ($groups as $key => $value) {

			foreach ($value as $data => $values) {

				foreach ($values as $row => $val) {
					$userid = $val['USER_ID'];

					$selectusername = $this->_db->select()
						->from(array('M_USER'), array(
							'*'
						))
						->where("CUST_ID = ?", (string) $this->_custIdLogin)
						->where("USER_ID = ?", (string) $userid);
					//echo $selectusername;echo ' ';
					$username = $this->_db->fetchAll($selectusername);

					if (!in_array($data, $tempGroup)) {
						$userlist[$data][] = $username[0]['USER_FULLNAME'];
					}
				}

				array_push($tempGroup, $data);

				// $approverbtn[][$data] = '<button class="btnCircleGroup hovertext" style="margin-right: 1%; margin-top: 12%;" disabled>'.$data.'
				// 	<span class="hovertextcontent" style="padding-left: 15px;">'.$userlist.'</span></button>';
			}
		}

		$userlist['GROUP_NAME'] = $newUniqueGroupName;

		return $userlist;
	}

	public function findBUserBoundary($transfertype, $amount)
	{



		$selectuser	= $this->_db->select()
			->from(array('C' => 'M_APP_BGBOUNDARY'), array(
				'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
				'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
				'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
				'C.TRANSFER_TYPE',
				'C.POLICY'
			))
			//->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
			->where("C.BOUNDARY_MIN 	<= ?", $amount)
			->where("C.BOUNDARY_MAX 	>= ?", $amount);


		//echo $selectuser;die();
		$datauser = $this->_db->fetchAll($selectuser);

		$command = str_replace('(', '', $datauser[0]['POLICY']);
		$command = str_replace(')', '', $command);
		$command = $command . ' SG';
		$list = explode(' ', $command);

		$alphabet = array('01' => 'A', '02' => 'B', '03' => 'C', '04' => 'D', '05' => 'E', '06' => 'F', '07' => 'G', '08' => 'H', '09' => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z', 27 => 'SG');

		$flipAlphabet = array_flip($alphabet);

		foreach ($list as $row => $data) {
			foreach ($alphabet as $key => $value) {
				if ($data == $value) {
					$groupuser[] = $flipAlphabet[$data];
				}
			}
		}

		$uniqueGroupUser = array_unique($groupuser);

		foreach ($uniqueGroupUser as $key => $value) {
			if ($value == '27') {
				$selectGroupName	= $this->_db->select()
					->from(array('C' => 'M_APP_GROUP_BUSER'), array('C.GROUP_NAME'))
					->where("C.GROUP_BUSER_ID LIKE ?", '%S__' . $value . '%');
			} else {
				$selectGroupName	= $this->_db->select()
					->from(array('C' => 'M_APP_GROUP_BUSER'), array('C.GROUP_NAME'))
					->where("C.GROUP_BUSER_ID LIKE ?", '%N__' . $value . '%');
			}

			$groupNameList = $this->_db->fetchAll($selectGroupName);

			array_unique($groupNameList[0]);

			$uniqueGroupName[$value] = $groupNameList[0]['GROUP_NAME'];
		}

		foreach ($uniqueGroupName as $row => $data) {
			foreach ($alphabet as $key => $value) {
				if ($row == $key) {
					$newUniqueGroupName[$value] = $data;
				}
			}
		}

		foreach ($groupuser as $key => $value) {

			//if special group
			if ($value == 27) {
				$likecondition = "S_%";
			} else {
				$likecondition = "%N__" . $value . "%";
			}

			$selectgroup = $this->_db->select()
				->from(array('C' => 'M_APP_GROUP_BUSER'), array(
					'BUSER_ID'
				))
				//->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.GROUP_BUSER_ID LIKE ?", (string) $likecondition);

			$group_user = $this->_db->fetchAll($selectgroup);

			$groups[][$alphabet[$value]] = $group_user;
		}
		//	echo '<pre>';
		//var_dump($groups);
		$tempGroup = array();
		foreach ($groups as $key => $value) {

			foreach ($value as $data => $values) {

				foreach ($values as $row => $val) {
					$userid = $val['BUSER_ID'];

					$selectusername = $this->_db->select()
						->from(array('M_BUSER'), array(
							'*'
						))
						//->where("CUST_ID = ?", (string) $this->_custIdLogin)
						->where("BUSER_ID = ?", (string) $userid);
					//echo $selectusername;echo ' ';
					$username = $this->_db->fetchAll($selectusername);

					if (!in_array($data, $tempGroup)) {
						$userlist[$data][] = $username[0]['BUSER_EMAIL'];
					}
				}

				array_push($tempGroup, $data);

				// $approverbtn[][$data] = '<button class="btnCircleGroup hovertext" style="margin-right: 1%; margin-top: 12%;" disabled>'.$data.'
				// 	<span class="hovertextcontent" style="padding-left: 15px;">'.$userlist.'</span></button>';
			}
		}

		$userlist['GROUP_NAME'] = $newUniqueGroupName;

		return $userlist;
	}

	public function findUserPrivi($privID)
	{



		$selectuser	= $this->_db->select()
			->from(array('A' => 'M_FPRIVI_USER'))
			->joinLeft(array('B' => 'M_USER'), 'A.FUSER_ID = CONCAT(B.CUST_ID,B.USER_ID)', array('USER_FULLNAME'))
			->where("A.FPRIVI_ID 	= ?", (string) $privID)
			->where("B.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->order('B.USER_FULLNAME ASC');

		//echo $selectuser;die();
		return $datauser = $this->_db->fetchAll($selectuser);
	}

	public function validatebtn($transfertype, $amount, $ccy, $psnumb)
	{
		//die;

		$selectuser	= $this->_db->select()
			->from(array('C' => 'M_APP_BOUNDARY'), array(
				'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
				'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
				'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
				'C.TRANSFER_TYPE',
				'C.POLICY'
			))
			->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
			->where("C.BOUNDARY_MIN 	<= ?", $amount)
			->where("C.BOUNDARY_MAX 	>= ?", $amount);


		// echo $selectuser;

		$datauser = $this->_db->fetchAll($selectuser);
		if (empty($datauser)) {

			return true;
		}

		$selectusergroup	= $this->_db->select()
			->from(array('C' => 'M_APP_GROUP_USER'), array(
				'*'
			))
			->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->where("C.USER_ID 	= ?", (string) $this->_userIdLogin);

		$usergroup = $this->_db->fetchAll($selectusergroup);




		$this->view->boundarydata = $datauser;
		// print_r($this->view->boundarydata);die;
		if (!empty($usergroup)) {
			$cek = false;

			foreach ($usergroup as $key => $value) {
				$group = explode('_', $value['GROUP_USER_ID']);
				$alphabet = array(1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'E', 6 => 'F', 7 => 'G', 8 => 'H', 9 => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z');
				$groupalfa = $alphabet[(int) $group[2]];
				// print_r($groupalfa);echo '-';
				$usergroup[$key]['GROUP'] = $groupalfa;
				//var_dump($usergroup);
				// print_r($datauser);die;
				foreach ($datauser as $nub => $val) {
					$command = str_replace('(', '', $val['POLICY']);
					$command = str_replace(')', '', $command);
					$list = explode(' ', $command);

					//var_dump($list);
					foreach ($list as $row => $data) {

						if ($data == $groupalfa) {
							$cek = true;
							// die('ter');
							break;
						}
					}
				}
			}
			//die;

			//var_dump($group);die;

			if ($group[0] == 'S') {
				return true;
			}
			// echo $cek;
			// print_r($cek);die;
			if (!$cek) {
				// die('here');
				return false;
			}
		}
		$tempusergroup = $usergroup;

		if ($cek) {




			$command = ' ' . $datauser['0']['POLICY'] . ' ';
			$command = strtoupper($command);

			$cleanCommand = trim(preg_replace('/\s+/', ' ', $command));

			//transform to php logical operator syntak
			$translate = array(
				'AND' => '&&',
				'OR' => '||',
				'THEN' => 'THEN$',
				'A' => '$A',
				'B' => '$B',
				'C' => '$C',
				'D' => '$D',
				'E' => '$E',
				'F' => '$F',
				'G' => '$G',
				'H' => '$H',
				'I' => '$I',
				'J' => '$J',
				'K' => '$K',
				'L' => '$L',
				'M' => '$M',
				'N' => '$N',
				'O' => '$O',
				'P' => '$P',
				'Q' => '$Q',
				'R' => '$R',
				// 'S' => '$S',
				'T' => '$T',
				'U' => '$U',
				'V' => '$V',
				'W' => '$W',
				'X' => '$X',
				'Y' => '$Y',
				'Z' => '$Z',
				'SG' => '$SG',
			);

			$phpCommand =  strtr($cleanCommand, $translate);
			//var_dump($phpCommand);die;
			$param = array(
				'0' => '$A',
				'1' => '$B',
				'2' => '$C',
				'3' => '$D',
				'4' => '$E',
				'5' => '$F',
				'6' => '$G',
				'7' => '$H',
				'8' => '$I',
				'9' => '$J',
				'10' => '$K',
				'11' => '$L',
				'12' => '$M',
				'13' => '$N',
				'14' => '$O',
				'15' => '$P',
				'16' => '$Q',
				'17' => '$R',
				// '18' => '$S',
				'19' => '$T',
				'20' => '$U',
				'21' => '$V',
				'22' => '$W',
				'23' => '$X',
				'24' => '$Y',
				'25' => '$Z',
				'26' => '$SG',
			);
			// print_r($phpCommand);die;
			function str_replace_first($from, $to, $content, $row)
			{
				$from = '/' . preg_quote($from, '/') . '/';
				return preg_replace($from, $to, $content, $row);
			}

			$command = str_replace('(', ' ', $val['POLICY']);
			$command = str_replace(')', ' ', $command);
			$list = explode(' ', $command);
			// print_r($list);die;
			// var_dump($command)

			$thendata = explode('THEN', $command);
			// print_r($thendata);echo '<br>';die;
			$cthen = count($thendata);
			// print_r($thendata);die;
			$secondcommand = str_replace('(', '', trim($thendata[0]));
			$secondcommand = str_replace(')', '', $secondcommand);
			$secondcommand = str_replace('AND', '', $secondcommand);
			$secondcommand = str_replace('OR', '', $secondcommand);
			$secondlist = explode(' ', $secondcommand);
			// print_r($secondlist);die;
			// print_r($grouplist);die;
			// print_r($thendata[$i]);die;
			// return true;
			if (!empty($secondlist)) {
				foreach ($usergroup as $key => $value) {
					foreach ($secondlist as $row => $thenval) {
						// print_r($value['GROUP']);echo ' == ';echo $thenval;echo '<br/>';
						if (trim($value['GROUP']) == trim($thenval)) {
							$thengroup = true;
							$grouplist[] = trim($thenval);
							//die('here');
						}
					}
				}
			}

			//var_dump($cthen);
			if ($cthen >= 2) {
				foreach ($usergroup as $key => $value) {
					// print_r($value);
					foreach ($thendata as $row => $thenval) {
						// echo '|';print_r($thenval);echo '==';
						// print_r($value['GROUP']);echo '|';echo '<br/>';
						// $thengroup = true;
						$newsecondcommand = str_replace('(', '', trim($thenval));
						$newsecondcommand = str_replace(')', '', $newsecondcommand);
						$newsecondcommand = str_replace('AND', '', $newsecondcommand);
						$newsecondcommand = str_replace('OR', '', $newsecondcommand);
						$newsecondlist = explode(' ', $newsecondcommand);
						//var_dump($newsecondcommand);
						if (in_array(trim($value['GROUP']), $newsecondlist)) {
							//if (trim($value['GROUP']) == trim($thenval)) {
							$thengroup = true;
							$grouplist[] = trim($thenval);
							//die('here');
						}
					}
				}
			}
			//var_dump($grouplist);die;
			//var_dump($thengroup);die;
			// var_dump($thengroup);die;
			// // print_r($group);die;
			// // echo $thengroup;die;
			//echo '<pre>';
			//var_dump($thengroup);
			//var_dump($thendata);
			//print_r($thendata);echo '<br/>';die('here');
			if ($thengroup == true) {


				for ($i = 1; $i <= $cthen; ++$i) {
					$oriCommand = $phpCommand;
					//echo $oriCommand;die;
					$indno = $i;
					//echo $oriCommand;echo '<br>';

					for ($a = $cthen - $indno; $a >= 1; --$a) {

						if ($i > 1) {
							$replace = 'THEN$ $' . trim($thendata[$a + 1]);
						} else {
							$replace = 'THEN$ $' . trim($thendata[$a]);
						}

						$oriCommand = str_replace($replace, "", $oriCommand);
					}


					//print_r($thendata);echo '<br>';die();

					//die;
					// if($i == 3){
					// echo 'command : ';echo $oriCommand;echo '<br/>';
					// }
					//print_r($oriCommand);echo '<br>';
					//print_r($list);echo '<br>';die;


					$result = $this->generate($oriCommand, $list, $param, $psnumb, $groupalfa, $thengroup);
					// print_r($i);
					//var_dump($result);
					//echo 'result-';var_dump($result);echo '-';die;
					if ($result) {
						// die;
						// echo $thendata[$i+1];die('eere');
						// print_r($i);
						// print_r($thendata);die;



						$replace = 'THEN$ $' . trim($thendata[$i + 1]);
						// var_dump($replace);die;
						// print_r($i);
						if (!empty($thendata[$i + 1])) {
							//die;
							$oriCommand = str_replace($replace, "", $phpCommand);
						} else {
							// die;
							$thirdcommand = str_replace('(', '', trim($thendata[$i - 1]));
							$thirdcommand = str_replace(')', '', $thirdcommand);
							$thirdcommand = str_replace('AND', '', $thirdcommand);
							$thirdcommand = str_replace('OR', '', $thirdcommand);
							$thirdlist = explode(' ', $thirdcommand);
							//						var_dump($secondlist);
							//						var_dump($grouplist);
							//							die;

							if (!empty($secondlist)) {
								foreach ($grouplist as $key => $valg) {
									foreach ($secondlist as $row => $value) {
										if ($value == $valg) {
											//echo 'sini';
											return false;
										}
									}
								}
							}
							$oriCommand = $phpCommand;
						}
						// print_r($thendata[$i]);die;
						// if($i == 3){
						// echo $oriCommand;die;
						// echo '<br/>';	
						// }
						// print_r($i);
						//echo $oriCommand;
						$result = $this->generate($oriCommand, $list, $param, $psnumb, $groupalfa, $thengroup);
						//var_dump($result);echo '<br/>';
						if (!$result) {
							// die;
							//print_r($groupalfa);
							//print_r($thendata[$i]);

							if ($groupalfa == trim($thendata[$i])) {

								return true;
							} else {

								//return true;
							}
							/*
							foreach ($grouplist as $key => $valg) {
								 
								if (trim($valg) == trim($thendata[$i])) {
									// die;
									// print_r($i);
									// print_r($thendata);
									// print_r($thendata[$i]);die;
									if ($thendata[$i + 1] == $valg) {
										return true;
									} else {
										// die('here');
										return false;
									}
								}
							} */
						} else {

							//return false;

							// $result = $this->generate($phpCommand,$list,$param,$psnumb);
							// print_r($phpCommand);
							// if($result){}
							// die('here');
						}
						// var_dump($result);
						// die;


					} else {
						//die('here');
						$secondcommand = str_replace('(', '', trim($thendata[$i - 1]));
						$secondcommand = str_replace(')', '', $secondcommand);
						$secondcommand = str_replace('AND', '', $secondcommand);
						$secondcommand = str_replace('OR', '', $secondcommand);
						$secondlist = explode(' ', $secondcommand);
						//var_dump($i);
						// var_dump($thendata);
						//print_r($grouplist);
						//die;
						//if($groupalfa == $gro)
						$approver = array();
						$countlist = array_count_values($list);
						foreach ($list as $key => $value) {
							if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
								$selectapprover	= $this->_db->select()
									->from(array('C' => 'T_APPROVAL'), array(
										'USER_ID'
									))
									->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
									// ->where("C.USER_ID 	= ?" , (string)$this->_userIdLogin)
									->where("C.PS_NUMBER = ?", (string) $psnumb)
									->where("C.GROUP = ?", (string) $value);
								//	 echo $selectapprover;
								$usergroup = $this->_db->fetchAll($selectapprover);
								// print_r($usergroup);
								$approver[$value] = $usergroup;
								if ($countlist[$value] == count($approver[$value]) && $tempusergroup['0']['GROUP'] == $value) {
									//var_dump($countlist[$value]);
									//var_dump($tempusergroup['0']['GROUP']);  
									//die('gere');
									return false;
								}
							}
						}

						//$array = array(1, "hello", 1, "world", "hello");


						//echo '<pre>';
						//var_dump($list);
						//var_dump($approver);
						//die;
						//var_dump($secondlist[0]);die;
						//foreach($approver as $app => $valpp){
						if (!empty($thendata[$i])) {
							if (empty($approver[$secondlist[0]]) && $groupalfa != $secondlist['0']) {
								//die('gere');
								return false;
							}
						}

						//echo '<pre>';
						//var_dump($thendata);
						//var_dump($grouplist);
						//var_dump($approver);
						//var_dump($groupalfa);
						//die;
						foreach ($grouplist as $key => $vg) {
							$newgroupalpa = str_replace('AND', '', $vg);
							$newgroupalpa = str_replace('OR', '', $newgroupalpa);
							$groupsecondlist = explode(' ', $newgroupalpa);
							//var_dump($newgroupalpa);
							//&& empty($approver[$groupalfa])
							if (in_array($groupalfa, $groupsecondlist)) {

								return true;
								//die('ge');
							}

							if ($vg == $groupalfa  && count($approver[$groupalfa]) > 0) {
								// echo 'gere';die;

								return true;
							}
						}

						// var_dump($approver[$groupalfa]);
						// var_dump($grouplist);
						//var_dump($groupalfa);die;

						//echo 'ger';die;
						//print_r($secondlist);die;
						//	 return true;
						if (!empty($secondlist)) {
							foreach ($grouplist as $key => $valg) {
								foreach ($secondlist as $row => $value) {
									if ($value == $valg) {

										if (empty($thendata[1])) {
											//die;

											return true;
										}
										//else{
										//	return false;
										//	}
									}
								}
							}
						}

						//	echo 'here';die;
						$secondresult = $this->generate($thendata[$i - 1], $list, $param, $psnumb, $groupalfa, $thengroup);
						// var_dump($secondresult);
						//print_r($thendata[$i-1]);
						//die;
						//if()
						//	 echo '<pre>';
						//var_dump($grouplist);die;
						foreach ($grouplist as $key => $valgroup) {
							//print_r($valgroup);
							//var_dump($thendata[$i]); 


							if (trim($valg) == trim($thendata[$i])) {
								$cekgroup = false;
								//die('here');
								if ($secondresult) {
									return false;
								} else {

									return true;
								}

								//break;
							}

							//else if (trim($valg) == trim($thendata[$i - 1])) {
							//		$cekgroup = false;
							// die('here');
							//	return false;
							//	}
						}
						//die;
						//if (!$cekgroup) {
						// die('here');
						//return false;
						//}
					}

					//		    echo '<br/>';
					//	${$command} = $oriCommand;
				}
			} else if (!empty($thendata) && $thengroup == false) {

				//var_dump($groupalfa)die;
				foreach ($thendata as $ky => $vlue) {
					$newsecondcommand = str_replace('(', '', trim($vlue));
					$newsecondcommand = str_replace(')', '', $newsecondcommand);
					$newsecondcommand = str_replace('AND', '', $newsecondcommand);
					$newsecondcommand = str_replace('OR', '', $newsecondcommand);
					$newsecondlist = explode(' ', $newsecondcommand);
					if ($newsecondlist['0'] == $groupalfa) {
						return true;
					}
				}
				return false;
			}




			$approver = array();
			// print_r($list);die;  	
			foreach ($list as $key => $value) {
				if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
					$selectapprover	= $this->_db->select()
						->from(array('C' => 'T_APPROVAL'), array(
							'USER_ID'
						))
						->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
						// ->where("C.USER_ID 	= ?" , (string)$this->_userIdLogin)
						->where("C.PS_NUMBER = ?", (string) $psnumb)
						->where("C.GROUP = ?", (string) $value);
					//	 echo $selectapprover;
					$usergroup = $this->_db->fetchAll($selectapprover);
					// print_r($usergroup);
					$approver[$value] = $usergroup;
				}
			}
			//die;




			// print_r($phpCommand);die;
			foreach ($param as $url) {
				if (strpos($phpCommand, $url) !== FALSE) {
					$ta = substr_count($phpCommand, $url);
					// print_r($list);die;

					if (!empty($approver)) {
						// print_r($approver);die;
						foreach ($list as $key => $value) {
							if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
								foreach ($approver[$value] as $row => $val) {
									// print_r($approver);die;
									if (!empty($val)) {
										$values = 'G' . $value;
										${$values}[$row + 1] = true;
										// print_r($B);
									}

									// print_r($val);
								}
							}
						}
					}

					for ($i = 1; $i <= $ta; $i++) {
						// print_r($list);die;
						foreach ($list as $key => $value) {
							if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
								$values = 'G' . $value;
								// print_r(${$values});
								if (empty(${$values}[$i])) {
									${$values}[$i] = false;
								}
								// if(${$value}[$i])
							}
						}
						// print_r($phpCommand);die;
						$numb = $i;
						$label = str_replace('$', '$G', $url);

						$replace = $label . '[' . $numb . ']';

						$alf = str_replace('$', '', $url);
						$values = 'G' . $alf;
						// print_r($values);die;
						if (${$values}[$i] == $replace) {
							$phpCommand = str_replace_first($url, 'true', $phpCommand, 1);
							// print_r($phpCommand);
						} else {
							$phpCommand = str_replace_first($url, 'false', $phpCommand, 1);
							// print_r($phpCommand);die;
						}
						// }
						// }

					}
					// print_r($GB);die;

				}
			}

			$keywords = preg_split("/[\s,]+/", $cleanCommand);
			$result =  false;
			$thendata = explode('THEN$', $phpCommand);
			//var_dump($thendata);die;
			if (!empty($thendata['1'])) {
				$phpCommand = '';
				foreach ($thendata as $tkey => $tval) {
					$phpCommand .= '(';
					$phpCommand .= $tval . ')';
					if (!empty($thendata[$tkey + 1])) {
						$phpCommand .= ' && ';
					}
				}
			} else {

				$phpCommand = str_replace('THEN$', '&&', $phpCommand);
			}
			//var_dump($phpCommand);
			if (!empty($phpCommand)) {
				eval('$result = ' . "$phpCommand;");
			} else {
				return false;
			}
			//var_dump($result);die;
			if (!$result) {

				return true;
			}
			// die('here2');
			//var_dump ($result);die;
			//return $result;
		} else {

			// die('here');
			return true;
		}
	}

	public function generate($command, $list, $param, $psnumb, $group, $thengroup)
	{

		$phpCommand = $command;

		// echo $command;die;

		$approver = array();

		$count_list = array_count_values($list);
		//print_r($count_list);
		foreach ($list as $key => $value) {
			if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
				$selectapprover	= $this->_db->select()
					->from(array('C' => 'T_APPROVAL'), array(
						'USER_ID'
					))
					->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
					// ->where("C.USER_ID 	= ?" , (string)$this->_userIdLogin)
					->where("C.PS_NUMBER = ?", (string) $psnumb)
					->where("C.GROUP = ?", (string) $value);
				// echo $selectapprover;
				$usergroup = $this->_db->fetchAll($selectapprover);
				// print_r($usergroup);
				$approver[$value] = $usergroup;
			}
		}
		//var_dump($param);die;
		//var_dump($group);
		foreach ($approver as $appval) {
			$totaldata = count($approver[$group]);
			$totalgroup = $count_list[$group];
			//var_dump($totaldata);
			//var_dump($totalgroup);
			if ($totalgroup == $totaldata && $totalgroup != 0) {

				return false;
			}
		} //die;
		//die;





		foreach ($param as $url) {

			if (strpos($phpCommand, $url) !== FALSE) {
				$ta = substr_count($phpCommand, $url);
				// print_r($list);die;

				if (!empty($approver)) {
					// print_r($approver);die;
					foreach ($list as $key => $value) {
						if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
							foreach ($approver[$value] as $row => $val) {
								// print_r($approver);die;
								if (!empty($val)) {
									$values = 'G' . $value;
									${$values}[$row + 1] = true;
									// print_r($B);
								}

								// print_r($val);
							}
						}
					}
				}


				// print_r($approver);die;

				for ($i = 1; $i <= $ta; $i++) {

					foreach ($list as $key => $value) {
						if (!empty($value)) {
							if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
								$values = 'G' . $value;
								// print_r(${$values});
								if (empty(${$values}[$i])) {
									${$values}[$i] = false;
								}
								// if(${$value}[$i])
							}
						}
					}


					$numb = $i;
					$label = str_replace('$', '$G', $url);
					// print_r($phpCommand);die('here');
					$replace = $label . '[' . $numb . ']';

					$alf = str_replace('$', '', $url);
					$values = 'G' . $alf;

					if (${$values}[$i] == $replace) {
						$phpCommand = str_replace_first($url, 'true', $phpCommand, 1);
						// print_r($phpCommand);
					} else {
						$phpCommand = str_replace_first($url, 'false', $phpCommand, 1);
						// print_r($phpCommand);die;
					}
					// }
					// }

				}
				// print_r($GB);die;

			}
		}

		$keywords = preg_split("/[\s,]+/", $cleanCommand);
		$result =  false;
		$phpCommand = str_replace('THEN$', '&&', $phpCommand);
		//print_r($phpCommand);echo '<br/>';

		if (!empty($phpCommand)) {
			eval('$result = ' . "$phpCommand;");
			//var_dump($thengroup);
			// var_dump($result);

			if ($result) {
				//var_dump($thengroup);die;
				if ($thengroup) {
					return true;
				} else {
					return false;
				}
			} else {

				if ($thengroup) {
					return false;
				} else {

					return true;
				}
			}
			// return $result;
		} else {
			return false;
		}

		// var_dump ($result);die;




	}

	public function validateapproval($policy, $list, $bgnumb)
	{
		//die('a');
		$command = ' ' . $policy . ' ';
		$command = strtoupper($command);

		$cleanCommand = trim(preg_replace('/\s+/', ' ', $command));
		//var_dump($cleanCommand);
		$commandspli = explode('THEN', $cleanCommand);
		$commandnew = '';
		foreach ($commandspli as $ky => $valky) {
			if ($commandspli[$ky + 1] != '') {
				$commandnew .= '(' . $valky . ') THEN ';
			} else {
				$commandnew .= '(' . $valky . ')';
			}
		}

		//transform to php logical operator syntak
		$translate = array(
			'AND' => '&&',
			'OR' => '||',
			'THEN' => '&&',
			'A' => '$A',
			'B' => '$B',
			'C' => '$C',
			'D' => '$D',
			'E' => '$E',
			'F' => '$F',
			'G' => '$G',
			'H' => '$H',
			'I' => '$I',
			'J' => '$J',
			'K' => '$K',
			'L' => '$L',
			'M' => '$M',
			'N' => '$N',
			'O' => '$O',
			'P' => '$P',
			'Q' => '$Q',
			'R' => '$R',
			// 'S' => '$S',
			'T' => '$T',
			'U' => '$U',
			'V' => '$V',
			'W' => '$W',
			'X' => '$X',
			'Y' => '$Y',
			'Z' => '$Z',
			'SG' => '$SG',
		);

		$phpCommand =  strtr($commandnew, $translate);

		$param = array(
			'0' => '$A',
			'1' => '$B',
			'2' => '$C',
			'3' => '$D',
			'4' => '$E',
			'5' => '$F',
			'6' => '$G',
			'7' => '$H',
			'8' => '$I',
			'9' => '$J',
			'10' => '$K',
			'11' => '$L',
			'12' => '$M',
			'13' => '$N',
			'14' => '$O',
			'15' => '$P',
			'16' => '$Q',
			'17' => '$R',
			// '18' => '$S',
			'19' => '$T',
			'20' => '$U',
			'21' => '$V',
			'22' => '$W',
			'23' => '$X',
			'24' => '$Y',
			'25' => '$Z',
			'26' => '$SG',
		);
		// print_r($phpCommand);die;
		// function str_replace_first($from, $to, $content,$row)
		// {
		//     $from = '/'.preg_quote($from, '/').'/';
		//     return preg_replace($from, $to, $content, $row);
		// }

		$command = str_replace('(', '', $policy);
		$command = str_replace(')', '', $command);
		$list = explode(' ', $command);

		$approver = array();
		foreach ($list as $key => $value) {
			if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
				$selectapprover	= $this->_db->select()
					->from(array('C' => 'T_APPROVAL'), array(
						'USER_ID'
					))

					->where("C.PS_NUMBER = ?", (string) $bgnumb)
					->where("C.GROUP = ?", (string) $value);
				// echo $selectapprover;die;
				$usergroup = $this->_db->fetchAll($selectapprover);

				$approver[$value] = $usergroup;
			}
		}

		foreach ($param as $url) {
			if (strpos($phpCommand, $url) !== FALSE) {
				$ta = substr_count($phpCommand, $url);

				if (!empty($approver)) {
					foreach ($list as $key => $value) {
						if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {

							foreach ($approver[$value] as $row => $val) {
								if (!empty($val)) {
									$values = 'G' . $value;
									${$values}[$row + 1] = true;
								}
							}
						}
					}
				}

				for ($i = 1; $i <= $ta; $i++) {
					foreach ($list as $key => $value) {
						if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {

							$values = 'G' . $value;

							if (empty(${$values}[$i])) {
								${$values}[$i] = false;
							}
						}
					}

					$BG_NUMBER = $i;
					$label = str_replace('$', '$G', $url);

					$replace = $label . '[' . $BG_NUMBER . ']';

					$alf = str_replace('$', '', $url);
					$values = 'G' . $alf;

					if (${$values}[$i] == $replace) {
						$phpCommand = $this->str_replace_first($url, 'true', $phpCommand, 1);
						//	 print_r($phpCommand);
					} else {
						$phpCommand = $this->str_replace_first($url, 'false', $phpCommand, 1);
						//	 print_r($phpCommand);
					}
				}
			}
		}
		$keywords = preg_split("/[\s,]+/", $cleanCommand);
		$result =  false;

		eval('$result = ' . "$phpCommand;");
		//var_dump ($result);die;
		return $result;
	}

	function str_replace_first($from, $to, $content, $row)
	{
		$from = '/' . preg_quote($from, '/') . '/';
		return preg_replace($from, $to, $content, $row);
	}

	public function headerhandleAction()
	{
		$bg_reg_number = $this->_getParam('bgregnumber');
		$header_name = $this->_getParam('header_name');
		$header_lists = [
			'TypeofBankGuarantee',
			'BankGuaranteeNominalandValidityPeriod',
			'BankGuaranteeRecipient',
			'GuaranteeTransaction',
			'CounterWarranty',
			'ProvisionFeeAdministrationandOtherExpenses',
			'FormatandLanguage',
			'OthersAttachmentDocuments',
		];

		$temp = $this->_db->select()
			->from(["A" => "TEMP_BANK_GUARANTEE"], ["*"])
			->joinLeft(array('B' => 'M_CUSTOMER'), 'A.BG_INSURANCE_CODE = B.CUST_ID', array('B.CUST_NAME'))
			->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
			->where('A.BG_REG_NUMBER = ?', $bg_reg_number)
			->where('A.BG_STATUS IN (2, 22)')
			->query()->fetchAll();

		$tbg = $this->_db->select()
			->from(["A" => "T_BANK_GUARANTEE"], ["*"])
			->joinLeft(array('B' => 'M_CUSTOMER'), 'A.BG_INSURANCE_CODE = B.CUST_ID', array('B.CUST_NAME'))
			->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
			->where('A.BG_REG_NUMBER = ?', $bg_reg_number)
			->where('A.BG_STATUS IN (2, 22)')
			->query()->fetchAll();

		if ($temp[0]['CHANGE_TYPE'] != '0' && !empty($tbg)) {
			$diff = array_diff($temp[0], $tbg[0]);
			if (!empty($diff)) {
				if (in_array($header_name, $header_lists)) {
					echo '&nbsp; <span style="font-size: 0.8em;" class="bg-white px-2 border border-danger rounded-circle"><i class="text-danger">i</i></span>';
				}
			}
		}
	}

	public function inquirymarginaldepositAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$regNumber = $this->_getParam('reqNumber');

		$bgdata = $this->_db->select()
			->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
			->where('A.BG_REG_NUMBER = ?', $regNumber)
			->query()->fetchAll();

		$optHtml = '<table class="table table-sm mb-0" id="guarantedTransactions">
		<thead>
                                <th style="padding-left: 2%;">Hold Seq</th>
                                <th>Nomor Rekening</th>
                                <th>Nama Rekening</th>
                                <th>Tipe</th>
                                <th>Valuta</th>
								<th>Nominal</th>
                            </thead>
                            <tbody>';

		$bgdatasplit = $this->_db->select()
			->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
			->joinLeft(['B' => 'M_CUSTOMER_ACCT'], 'A.ACCT = B.ACCT_NO AND B.CUST_ID = ' . $this->_db->quote($bgdata[0]['CUST_ID']) . '', ["M_ACCT_DESC" => "B.ACCT_DESC", "M_ACCT_TYPE" => "B.ACCT_TYPE", 'CUST_ID', 'CCY_ID'])
			->where('A.BG_REG_NUMBER = ?', $regNumber)
			->query()->fetchAll();

		if (!empty($bgdatasplit)) {
			foreach ($bgdatasplit as $key => $row) {
				if (strtolower($row["M_ACCT_DESC"]) == "giro" || $row["M_ACCT_TYPE"] == "D") continue;
				// GET SERVICE 
				$serviceAccount = new Service_Account($row['ACCT'], null);
				$accountInfo = $serviceAccount->inquiryAccontInfo();
				$serviceCif = new Service_Account(null, null, null, null, null, $accountInfo["cif"]);
				$saveResult = $serviceCif->inquiryCIFAccount()["accounts"];
				$listAccount = array_search($row['ACCT'], array_column($saveResult, "account_number"));

				$row['HOLD_SEQUENCE'] ? $holdSequence = $row['HOLD_SEQUENCE'] : $holdSequence = '-';
				if ($saveResult[$listAccount]["product_type"] == 'J1') {
					continue;
				}
				$optHtml .= '<tr>
						<td style="padding-left: 2%;">' . $holdSequence . '</td>
						<td>' . $row['ACCT'] . '</td>
						<td>' . $row['NAME'] . '</td>
						<td>' . $saveResult[$listAccount]["type_desc"] . " " . $saveResult[$listAccount]["product_type"] . '</td>
						<td>' . $saveResult[$listAccount]["currency"] . '</td>
						<td>' . Application_Helper_General::displayMoney($row['AMOUNT']) . '</td>
					</tr>
					';
			}
		} else {
			$optHtml .= '<tr><td colspan="7" class="tbl-evencontent" align="center">--- ' . $this->language->_('No Data') . ' ---</td></tr>';
		}

		$optHtml .= '</tbody></table><div class="col-sm-12 mb-3 pl-0"><strong class="formreq"><font color="red">&nbsp;*</font></strong> <label for="" class="textGray col-form-label">' . $this->language->_('Rekening Tipe Deposito dan Saving akan dilakukan pelepasan dana ditahan (tanpa pemindahan dana)') . '</label></div>';

		echo $optHtml;
	}

	public function checkvalidasiAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();
		$bgNumber = $this->_getParam('bgNumber');

		$data = $this->_db->select()
			->from('TEMP_BANK_GUARANTEE_CLOSE')
			->where('BG_NUMBER = ?', $bgNumber)
			->where('CHANGE_TYPE = 3')
			->where('SUGGESTION_STATUS NOT IN (7,11)')
			->query()->fetch();

		if (!empty($data)) {
			$return['status'] = true;
		} else {
			$return['status'] = false;
		}

		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($return);
	}

	public function inquiryaccountinfoAction()
	{

		$app = Zend_Registry::get('config');
		$app = $app['app']['bankcode'];

		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$acct_no = $this->_getParam('acct_no');
		//echo $acct_no;

		$svcAccount = new Service_Account($acct_no, Application_Helper_General::getCurrNum('IDR'), $app, '1');
		$result = $svcAccount->inquiryAccontInfo('AI', TRUE);
		//var_dump($result);
		//die();
		$data['data'] = false;
		is_array($result) ? $result :  $result = $data;
		echo json_encode($result);
	}

	public function inquiryrekeningklaimAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();
		$acct_no = $this->_getParam('acct_no');
		$svcAccount = new Service_Account($acct_no, null);
		$result = $svcAccount->inquiryAccontInfo('AI', TRUE);

		$swiftcode = $this->_request->getParam('swiftcode');
		$acct_name = $this->_request->getParam('acct_name');

		if (strtolower($swiftcode) == 'btanidja') {
			if ($result['response_desc'] == 'Success') //00 = success
			{
				$svcAccountCIF = new Service_Account($result['account_number'], null, null, null, null, $result['cif']);
				$result2 = $svcAccountCIF->inquiryCIFAccount();

				$filterBy = $result['account_number']; // or Finance etc.
				$new = array_filter($result2['accounts'], function ($var) use ($filterBy) {
					return ($var['account_number'] == $filterBy);
				});

				$singleArr = array();
				foreach ($new as $key => $val) {
					$singleArr = $val;
				}
			} else {
				header('Content-Type: application/json; charset=utf-8');
				echo json_encode($result);
				return 0;
			}

			if ($singleArr['type_desc'] == 'Deposito') {
				$svcAccountDeposito = new Service_Account($result['account_number'], null, null, null, null, null);
				$result3 = $svcAccountDeposito->inquiryDeposito();

				if ($result3["response_code"] != "0000") {
					header('Content-Type: application/json; charset=utf-8');
					echo json_encode(array_merge($result3, $singleArr, $result));
					return 0;
				}

				if ($result3["status"] != 1 && $result3["status"] != 4) {
					header('Content-Type: application/json; charset=utf-8');
					echo json_encode(array_merge($result3, $singleArr, $result));
					return 0;
				}

				$sqlRekeningJaminanExist = $this->_db->select()
					->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
					->where('A.ACCT = ?', $result['account_number'])
					->query()->fetchAll();

				$result3["check_exist_split"] = (!empty($sqlRekeningJaminanExist)) ? 1 : 0;

				$get_product_type = current($new)["type"];

				$check_prod_type = $this->_db->select()
					->from("M_PRODUCT_TYPE")
					->where("PRODUCT_CODE = ?", $result3["account_type"])
					->query()->fetch();

				$result3["check_product_type"] = 0;
				if (!empty($check_prod_type)) {
					$result3["check_product_type"] = 1;
				}
			} else {
				$svcAccountBalance = new Service_Account($result['account_number'], null, null, null, null, null);
				$result3 = $svcAccountBalance->inquiryAccountBalance();

				if ($result3["status"] != 1 && $result3["status"] != 4) {
					header('Content-Type: application/json; charset=utf-8');
					echo json_encode($result3);
					return 0;
				}

				$get_product_type = current($new)["type"];
				$check_prod_type = $this->_db->select()
					->from("M_PRODUCT_TYPE")
					->where("PRODUCT_CODE = ?", $result3["account_type"])
					->query()->fetch();

				$result3["check_product_type"] = 0;
				if (!empty($check_prod_type)) {
					$result3["check_product_type"] = 1;
				}
			}

			$return = array_merge($result, $singleArr, $result3);

			$data['data'] = false;
			is_array($return) ? $return :  $return = $data;

			header('Content-Type: application/json; charset=utf-8');
			echo json_encode($return);
		} else {
			$clientUser  =  new SGO_Soap_ClientUser();

			$getInfoBank = $this->_db->select()
				->from('M_DOMESTIC_BANK_TABLE')
				->where('SWIFT_CODE = ?', $swiftcode)
				->query()->fetch();

			$request = [
				'account_number' => $acct_no,
				'bank_code' => $getInfoBank['BANK_CODE']
			];

			$clientUser->callapi('otherbank', NULL, $request);
			$responseService = $clientUser->getResult();

			$result = [
				'valid' => true,
				'account_name' => $responseService['account_name']
			];

			if ($responseService['response_code'] != '0000') {
				$result = [
					'valid' => false,
					'message' => $this->language->_('Rekening tidak ditemukan')
				];
			}

			if (strtolower($responseService['account_name']) !== strtolower($acct_name)) {
				$result = [
					'valid' => false,
					'message' => $this->language->_('Nama dan Nomor Rekening tidak sesuai')
				];
			}

			$result = array_merge($result, array_intersect_key($responseService, ['response_code' => '']));
			echo json_encode($result);
			return;
		}
	}


	public function downloadlampiranAction()
	{
		// decrypt numb
		$setting = new Settings();
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$pw_hash = md5($enc_salt . $enc_pass);
		$rand = $this->_userIdLogin . date('dHis') . $pw_hash;
		$sessionNamespace->token     = $rand;
		$this->view->token = $sessionNamespace->token;

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$this->view->token = $sessionNamespace->token;


		$AESMYSQL = new Crypt_AESMYSQL();

		$bgfile     = urldecode($this->_getParam('bgfile'));

		$attahmentDestination = UPLOAD_PATH . '/document/closing/';
		return $this->_helper->download->file($bgfile, $attahmentDestination . $bgfile);
	}

	public function downloadAction()
	{
		// decrypt numb

		$setting = new Settings();
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$pw_hash = md5($enc_salt . $enc_pass);
		$rand = $this->_userIdLogin . date('dHis') . $pw_hash;
		$sessionNamespace->token     = $rand;
		$this->view->token = $sessionNamespace->token;

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$this->view->token = $sessionNamespace->token;


		$AESMYSQL = new Crypt_AESMYSQL();

		$BG_NUMBER     = urldecode($this->_getParam('bgnumb'));

		//$BG_NUMBER = $AESMYSQL->decrypt($BG_NUMBER, $password);
		//echo $BG_NUMBER;die;
		$bgdata = $this->_db->select()
			->from("T_BANK_GUARANTEE", ["DOCUMENT_ID"])
			->where("BG_NUMBER = ?", $BG_NUMBER)
			->query()->fetch();

		//var_dump($bgdata);die;
		$attahmentDestination = UPLOAD_PATH . '/document/submit/';
		return $this->_helper->download->file($bgdata["DOCUMENT_ID"] . ".pdf", $attahmentDestination . $bgdata["DOCUMENT_ID"] . ".pdf");
	}


	public function showfinaldocAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		// decrypt numb

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$this->view->token = $sessionNamespace->token;

		$AESMYSQL = new Crypt_AESMYSQL();

		$BG_NUMBER     = urldecode($this->_getParam('bgnumb'));

		//$BG_NUMBER = $AESMYSQL->decrypt($BG_NUMBER, $password);

		$file = $this->_request->getParam("file");

		$get_document_id = $this->_db->select()
			->from("T_BANK_GUARANTEE", ["DOCUMENT_ID"])
			->where("BG_NUMBER = ?", $BG_NUMBER)
			->query()->fetch();

		$get_file_name = $get_document_id["DOCUMENT_ID"] . ".pdf";

		header("Content-type: application/pdf");
		header("Content-Disposition: inline; filename=" . $get_file_name);
		// @readfile('/app/bg/library/data/uploads/document/submit/' . $get_file_name);
		@readfile(UPLOAD_PATH . '/document/submit/' . $get_file_name);
	}

	public function calculate_time_span($date)
	{
		// $seconds  = strtotime(date('Y-m-d H:i:s')) - strtotime($date);

		// $months = floor($seconds / (3600 * 24 * 30));
		// $day = floor($seconds / (3600 * 24));
		// $hours = floor($seconds / 3600);
		// $mins = floor(($seconds - ($hours * 3600)) / 60);
		// $secs = floor($seconds % 60);

		// if ($seconds < 60)
		// 	$time = $secs . " seconds ago";
		// else if ($seconds < 60 * 60)
		// 	$time = $mins . " min ago";
		// else if ($seconds < 24 * 60 * 60)
		// 	$time = $hours . " hours ago";
		// else if ($seconds < 24 * 60 * 60)
		// 	$time = $day . " day ago";
		// else
		// 	$time = $months . " month ago";

		// return $day . " hari " . $hours . " jam " . $mins . " menit ";
		$now = new DateTime();
		$dateObj = new DateTime($date);
		$interval = $now->diff($dateObj);

		$days = $interval->days;
		$hours = $interval->h;
		$minutes = $interval->i;

		if ($days > 0) {
			$time = $days . " hari " . $hours . " jam " . $minutes . " menit";
		} else if ($hours > 0) {
			$time = $hours . " jam " . $minutes . " menit";
		} else {
			$time = $minutes . " menit";
		}

		return $time;
	}
}
