<?php
require_once 'Zend/Controller/Action.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'SGO/Helper/Encryption.php';

class bgclosingworkflow_releasedetailController extends Application_Main
{

	protected $_moduleDB = 'RTF'; // masih harus diganti

	public function indexAction()
	{

		$this->_helper->_layout->setLayout('newlayout');

		$settings = new Settings();
		$system_type = $settings->getSetting('system_type');

		$this->view->userId = $this->_userIdLogin;

		// BEGIN GLOBAL VARIABEL
		$config = Zend_Registry::get('config');
		$bgpublishType  = $config["bgpublish"]["type"]["desc"];
		$bgpublishCode  = $config["bgpublish"]["type"]["code"];
		$arrbgpublish   = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

		$bgcgType   = $config["bgcg"]["type"]["desc"];
		$bgcgCode   = $config["bgcg"]["type"]["code"];
		$arrbgcg 	= array_combine(array_values($bgcgCode), array_values($bgcgType));

		$bgType   = $config["bg"]["type"]["desc"];
		$bgCode   = $config["bg"]["type"]["code"];
		$arrbg 	= array_combine(array_values($bgCode), array_values($bgType));
		$this->view->arrbg = $arrbg;

		$bgclosingType   = $config["bgclosing"]["changetype"]["desc"];
		$bgclosingCode   = $config["bgclosing"]["changetype"]["code"];
		$arrbgclosingType 	= array_combine(array_values($bgclosingCode), array_values($bgclosingType));
		$this->view->arrbgclosingType = $arrbgclosingType;

		$bgclosingStatus   = $config["bgclosing"]["status"]["desc"];
		$bgclosingstatusCode   = $config["bgclosing"]["status"]["code"];
		$arrbgclosingStatus 	= array_combine(array_values($bgclosingstatusCode), array_values($bgclosingStatus));
		$this->view->arrbgclosingStatus = $arrbgclosingStatus;

		$historyStatusCode = $config["bgclosinghistory"]["status"]["code"];
		$historyStatusDesc = $config["bgclosinghistory"]["status"]["desc"];
		$historyStatusArr  = array_combine(array_values($historyStatusCode), array_values($historyStatusDesc));
		$this->view->historyStatusArr   = $historyStatusArr;

		$arrStatusPenutupan = [
			1 => 'Menunggu Konfirmasi Pembayaran',
			2 => 'Permintaan Perbaikan Konfirmasi Penyelesaian',
			3 => 'Menunggu Persetujuan Pembayaran',
			4 => 'Service dalam proses',
			5 => 'Proses Selesai',
			6 => 'Processing'
		];

		$this->view->arrStatusPenutupan = $arrStatusPenutupan;

		$config = Zend_Registry::get('config');

		$transCode = $config['transaction']['list']['code'];
		$transDesc = $config['transaction']['list']['desc'];

		$arrListService = array_combine($transCode, $transDesc);

		$this->view->arrListService = $arrListService;

		$arrStatusTransaction = [
			1 => 'Sukses',
			2 => 'Sukses dengan Pembaharuan Status',
			3 => 'Time Out',
			4 => 'Gagal',
			5 => 'Menunggu Pembayaran',
			6 => 'Pending Service'
		];

		$this->view->arrStatusTransaction = $arrStatusTransaction;

		$arrType = [
			1 => 'Penutupan Permintaan Principal',
			2 => 'Penutupan Tanpa Klaim',
			3 => 'Klaim Obligee'
		];

		$this->view->arrType = $arrType;

		// END GLOBAL VARIABEL

		// BEGIN PENUTUPAN BANK GARANSI
		// BEGIN DETAIL BANK GARANSI

		$bgparam = $this->_getParam('bgnumb');
		$this->view->bgnumb_enc = $bgparam;
		$AESMYSQL = new Crypt_AESMYSQL();
		$decryption = urldecode($bgparam);
		$numb = $AESMYSQL->decrypt($decryption, $password);

		if (!empty($numb)) {
			$bgdata = $this->_db->select()
				->from(array('A' => 'T_BANK_GUARANTEE'), array(
					'*', 'CUST_EMAIL_BG' => 'A.CUST_EMAIL', 'BG_REG_NUMBER_SPESIAL' => 'A.BG_REG_NUMBER',
					'INS_NAME' => new Zend_Db_Expr('(SELECT CI.CUST_NAME FROM M_CUSTOMER CI WHERE CI.CUST_ID = A.BG_INSURANCE_CODE LIMIT 1)'),
					'INS_BRANCH_NAME' => new Zend_Db_Expr('(SELECT INS_BRANCH_NAME FROM M_INS_BRANCH WHERE INS_BRANCH_CODE = (SELECT PS_FIELDVALUE FROM T_BANK_GUARANTEE_DETAIL tbgd WHERE tbgd.BG_REG_NUMBER = A.BG_REG_NUMBER AND LOWER(tbgd.PS_FIELDNAME) = \'insurance branch\' LIMIT 1) LIMIT 1)'),
				))
				->joinleft(array('B' => 'M_CUSTOMER'), 'A.CUST_ID = B.CUST_ID', array('B.*'))
				->joinleft(array('C' => 'M_BRANCH'), 'A.BG_BRANCH = C.BRANCH_CODE', array('C.BRANCH_NAME', 'C.BRANCH_EMAIL'))
				->joinleft(array('D' => 'TEMP_BANK_GUARANTEE_CLOSE'), 'A.BG_NUMBER = D.BG_NUMBER', array('D.CHANGE_TYPE', 'D.SUGGESTION_STATUS', 'D.CLAIM_FIRST_VERIFIED'))
				->joinleft(array('E' => 'M_CITYLIST'), 'B.CUST_CITY = E.CITY_CODE', array('E.*'))
				->joinleft(array('F' => 'T_BG_PSLIP'), 'F.BG_NUMBER = A.BG_NUMBER', array('F.PS_STATUS', 'F.TYPE', 'F.CLOSE_REF_NUMBER', 'F.PS_DONE', 'F.CLOSE_REF_NUMBER'))
				//->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
				->where('A.BG_NUMBER = ?', $numb)
				->query()->fetchAll();

			$bgdata[0]['CLOSE_REF_NUMBER'] = $bgdata[0]['CLOSE_REF_NUMBER'] ?: $bgdata[0]['CLOSE_REF_NUMBER_T'];

			if (!empty($bgdata)) {
				$getBG = $bgdata['0'];

				// Get data T_BG_TRANSACTION
				$selectTrans = $this->_db->select()
					->from(
						array('A' => 'T_BG_TRANSACTION'),
						array('*')
					)
					->where('A.CLOSE_REF_NUMBER = ?', $getBG['CLOSE_REF_NUMBER'])
					->order('A.TRANSACTION_ID ASC');
				$dataHistoryTransaction = $this->_db->fetchAll($selectTrans);

				$hasTimeoutTrans = array_shift(array_filter($dataHistoryTransaction, function ($item) {
					return $item['TRA_STATUS'] == 3;
				}));

				$hasFailedTrans = array_shift(array_filter($dataHistoryTransaction, function ($item) {
					return $item['TRA_STATUS'] == 4;
				}));

				$this->view->hasTimeoutTrans = $hasTimeoutTrans;
				$this->view->hasFailedTrans = $hasFailedTrans;

				if ($getBG['BG_NUMBER'] == '') {
					$getBG['BG_NUMBER'] = '-';
				}

				if ($getBG['BG_SUBJECT'] == '') {
					$getBG['BG_SUBJECT'] = '- no subject -';
				}

				$this->view->getBG = $getBG;
				//$this->view->time_calculation = $this->calculate_time_span($getBG['CLAIM_FIRST_VERIFIED']);


				$this->view->publishDesc = $arrbgpublish[$getBG['BG_PUBLISH']];
				if ($getBG['BG_PUBLISH'] == 1) {
					$getPaperPrint = $this->_db->select()
						->from('M_PAPER')
						->where('notes = ?', $getBG['BG_NUMBER'])
						->where('STATUS = 1')
						->query()->fetchAll();

					$totalKertas = count($getPaperPrint);

					$this->view->nomorRefKertas = ' - Nomor Ref Kertas : ' . $getPaperPrint[0]['PAPER_ID'] . ' - ' . $getPaperPrint[$totalKertas - 1]['PAPER_ID'] . ' (' . $totalKertas . ' lembar)';
				}

				// BEGIN NILAI BANK GARANSI

				$bgdetaildata = $this->_db->select()
					->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
					->where('A.BG_REG_NUMBER = ?', $getBG["BG_REG_NUMBER"])
					->query()->fetchAll();


				if (!empty($bgdetaildata)) {
					foreach ($bgdetaildata as $key => $value) {
						if ($value['PS_FIELDNAME'] == 'Currency') {
							$this->view->bgCurrency =   $value['PS_FIELDVALUE'];
						}

						if ($getBG['COUNTER_WARRANTY_TYPE'] == '3') {

							if ($value['PS_FIELDNAME'] == 'Insurance Name') {

								$cekInsuranceDetail = $this->_db->select()
									->from('M_CUSTOMER')
									->where('CUST_ID = ?', $value['PS_FIELDVALUE'])
									->query()->fetch();

								$this->view->insuraceName =   $cekInsuranceDetail['CUST_NAME'];
							}

							if ($value['PS_FIELDNAME'] == 'Insurance Branch') {

								$insuranceBranch = $this->_db->select()
									->from("M_INS_BRANCH")
									->where("INS_BRANCH_CODE = ?", $value['PS_FIELDVALUE'])
									->query()->fetchAll();

								$this->view->insuranceBranch = $insuranceBranch[0]["INS_BRANCH_NAME"];
							}
						}
					}
				}

				$this->view->BG_AMOUNT  = $getBG['BG_AMOUNT'];
				$this->view->updateStart = Application_Helper_General::convertDate($getBG['TIME_PERIOD_START'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->updateEnd = Application_Helper_General::convertDate($getBG['TIME_PERIOD_END'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->bgClaimDate = Application_Helper_General::convertDate($getBG['BG_CLAIM_DATE'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->bgIssuedDate = Application_Helper_General::convertDate($getBG['BG_ISSUED'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->psDone = Application_Helper_General::convertDate($getBG['PS_DONE'], $this->view->viewDateFormat, $this->view->defaultDateFormat);

				// END NILAI BANK GARANSI

				// BEGIN KONTRA GARANSI
				$this->view->counterWarrantyType = $getBG['COUNTER_WARRANTY_TYPE'];
				$this->view->counterWarrantyDesc = $arrbgcg[$getBG['COUNTER_WARRANTY_TYPE']];
				$this->view->changeType = $getBG['CHANGE_TYPE'];

				// END KONTRA GARANSI

				// BEGIN LAMPIRAN DOKUMEN

				if (!empty($getBG['CLOSE_REF_NUMBER'])) {
					$bgdatafile = $this->_db->select()
						->from(array('A' => 'TEMP_BANK_GUARANTEE_FILE_CLOSE'), array('*'))
						->where('A.CLOSE_REF_NUMBER = ?', $getBG['CLOSE_REF_NUMBER'])
						->query()->fetchAll();
					$this->view->lampiranDokumen = $bgdatafile;
				}
				// END LAMPIRAN DOKUMEN

				// BEGIN MARGINAL DEPOSIT PRINCIPAL
				$saveHoldAmount = null;

				$bgdatasplit = $this->_db->select()
					->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
					->joinLeft(['B' => 'M_CUSTOMER_ACCT'], 'A.ACCT = B.ACCT_NO AND B.CUST_ID = ' . $this->_db->quote($getBG['CUST_ID']) . '', ["M_ACCT_DESC" => "B.ACCT_DESC", "M_ACCT_TYPE" => "B.ACCT_TYPE", 'CUST_ID', 'CCY_ID'])
					->where('A.BG_REG_NUMBER = ?', $getBG['BG_REG_NUMBER'])
					->query()->fetchAll();


				$this->view->bgdatasplit = $bgdatasplit;

				$checkEscrow = array_filter($bgdatasplit, function ($item) {
					return strtolower($item['ACCT_DESC']) == 'escrow';
				});

				$this->view->haveEscrow = ($checkEscrow) ? true : false;

				$saveEscrow = 0;

				foreach ($bgdatasplit as $key => $value) {
					// if ($value['CUST_ID'] != $data['CUST_ID']) continue;
					// if ($value["ACCT_DESC"]) continue;
					// $mdeksisting += intval($value["AMOUNT"]);
					if (strtolower($value["M_ACCT_DESC"]) == "giro" || $value["M_ACCT_TYPE"] == "D" || strtolower($value["ACCT_DESC"]) == "giro" || strtolower($value["ACCT_TYPE"]) == "giro") continue;
					$saveHoldAmount += floatval($value["AMOUNT"]);
					if (strtolower($value["ACCT_DESC"] == 'Escrow')) $saveEscrow += floatval($value["AMOUNT"]);
				}
				// foreach ($bgdatasplit as $key => $value) {
				// 	if ($value['CUST_ID'] != $getBG['CUST_ID']) continue;
				// 	if ($value["ACCT_DESC"]) continue;

				// 	$saveHoldAmount += intval($value["AMOUNT"]);
				// }

				$this->view->holdAmount = $saveHoldAmount;
				// END MARGINAL DEPOSIT PRINCIPAL

				// BEGIN PELEPASAN DANA
				// BEGIN LIST ACCOUNT PELEPASAN DANA
				if (!empty($getBG['CLOSE_REF_NUMBER'])) {

					$dataListAccount = $this->_db->select()
						->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL_CLOSE'), array('*'))
						->where('A.CLOSE_REF_NUMBER = ?', $getBG['CLOSE_REF_NUMBER'])
						->query()->fetchAll();

					$jmlListAccount = $this->_db->select()
						->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL_CLOSE'), array('*'))
						->where('A.CLOSE_REF_NUMBER = ?', $getBG['CLOSE_REF_NUMBER'])
						->group(array("ACCT_INDEX"))
						->query()->fetchAll();

					if (!empty($jmlListAccount)) {
						$jumlahAccount = count($jmlListAccount);
					}

					$htmlListAccount .= '<table id="guarantedTransactions">
													<thead>
														<tr>
															<th>Own Account </th>
															<th>Account Number</th>
															<th></th>
															<th>Account Name</th>
															<th>Currency</th>
															<th>Nominal</th>
															
														</tr>
													</thead>
												<tbody id="tbody">';
					$htmlListAccountPDF .= '<table id="guarantedTransactions">
													<thead>
														<tr>
															<th>Own Account </th>
															<th>Account Number</th>
															<th>Account Name</th>
															<th>Currency</th>
															<th>Nominal</th>
															
														</tr>
													</thead>
												<tbody id="tbody">';
					foreach ($dataListAccount as $key => $val) {
						$data[$val['PS_FIELDNAME']][$val['ACCT_INDEX']] = $val['PS_FIELDVALUE'];
					}

					$arrOwn = array("0" => "T", "1" => "Y");
					for ($i = 1; $i <= $jumlahAccount; $i++) {

						$htmlListAccount .= '<tr>
											<td>' . $data['Beneficiary is own account'][$i] . '</td>
											<td class="listrekeningMD">' . $data['Beneficiary Account Number'][$i] . '</td>
											<td><div class="hidden ml-2 border border-danger rounded-circle position-relative" style="width: 20px; height: 20px;" id="iconErrorMD' . $data['Beneficiary Account Number'][$i] . '"><div class="d-flex justify-content-center text-danger">i</div>
												<div class="position-absolute border border-danger text-danger hidden p-2" style="background-color: white; width: 214px;z-index: 9999; top: 0; left: 30px"><span id="errorMD' . $data['Beneficiary Account Number'][$i] . '">Status rekening tidak aktif</span></div>
											</div></td>
											<td>' . $data['Beneficiary Account Name'][$i] . '</td>
											<td>' . $data['Beneficiary Account CCY'][$i] . '</td>
											<td>' . Application_Helper_General::displayMoney($data['Transaction Amount'][$i]) . '</td>
											
										</tr>
										';
						$htmlListAccountPDF .= '<tr>
											<td>' . $data['Beneficiary is own account'][$i] . '</td>
											<td class="listrekeningMD">' . $data['Beneficiary Account Number'][$i] . '</td>
											<td>' . $data['Beneficiary Account Name'][$i] . '</td>
											<td>' . $data['Beneficiary Account CCY'][$i] . '</td>
											<td>' . Application_Helper_General::displayMoney($data['Transaction Amount'][$i]) . '</td>
											
										</tr>
										';
						$totalAmount += intval(str_replace(',', '', $data['Transaction Amount'][$i]));
					}

					$htmlListAccount .= '</tbody></table>';
					$htmlListAccountPDF .= '</tbody></table>';

					$this->view->listAccount = $htmlListAccount;
					$this->view->listAccountPDF = $htmlListAccountPDF;
					$this->view->transferAmount = $totalAmount;
					$this->view->totalPelepasanDana = Application_Helper_General::displayMoney($totalAmount);
				}
				// END LIST ACCOUNT PELEPASAN DANA
				// END PELEPASAN DANA

			}
		}
		// END DETAIL BANK GARANSI

		// END PENUTUPAN BANK GARANSI

		// Get data T_BANK_GUARANTEE_HISTORY
		if (!empty($getBG['CLOSE_REF_NUMBER'])) {
			$select = $this->_db->select()
				->from(
					array('A' => 'T_BANK_GUARANTEE_HISTORY_CLOSE'),
					array('*')
				)
				->where('A.CLOSE_REF_NUMBER = ?', $getBG['CLOSE_REF_NUMBER'])
				->where('A.BG_NUMBER = ?', $getBG['BG_NUMBER']);

			$dataHistory = $this->_db->fetchAll($select);
			$this->view->dataHistory = $dataHistory;
		}

		// Get data T_BG_PSLIP
		if (!empty($getBG['CLOSE_REF_NUMBER'])) {
			$selectPslip = $this->_db->select()
				->from(
					array('A' => 'T_BG_PSLIP'),
					array('*')
				)
				->where('A.CLOSE_REF_NUMBER = ?', $getBG['CLOSE_REF_NUMBER']);
			$dataHistoryPslip = $this->_db->fetchAll($selectPslip);

			foreach ($dataHistoryPslip as $row) {

				if ($row['DISPLAY_FLAG'] == 1) {
					$flagTransaction = true;
				} else {
					$flagTransaction = false;
				}
			}

			$this->view->flagTransaction = $flagTransaction;
		}

		// Get data T_BG_TRANSACTION
		if (!empty($getBG['CLOSE_REF_NUMBER'])) {
			$selectTrans = $this->_db->select()
				->from(
					array('A' => 'T_BG_TRANSACTION'),
					array('*')
				)
				->where('A.CLOSE_REF_NUMBER = ?', $getBG['CLOSE_REF_NUMBER'])
				->order('A.TRANSACTION_ID ASC');
			$dataHistoryTransaction = $this->_db->fetchAll($selectTrans);

			$this->view->dataHistoryTransaction = $dataHistoryTransaction;
		}

		// Get data T_BG_TRANSACTION
		$selectListBank = $this->_db->select()
			->from(
				array('A' => 'M_BANK_TABLE'),
				array('*')
			);
		$dataBank = $this->_db->fetchAll($selectListBank);
		$listBank = array();
		foreach ($dataBank as $row) {
			$listBank[$row['BANK_CODE']] = $row['BANK_NAME'];
		}
		$this->view->listBank = $listBank;


		if ($this->_request->isPost()) {

			$datas = $this->_request->getParams();
			//Zend_Debug::dump($datas);die;

			$kirim = $this->_getParam('kirim');

			if ($this->_getParam('timeout')) {
				// $this->cekTimeout($getBG['CLOSE_REF_NUMBER']);
				// return;

				$dataPslip = array(
					'PS_STATUS' => 6,
				);

				$where['CLOSE_REF_NUMBER = ?'] = $getBG['CLOSE_REF_NUMBER'];
				$this->_db->update('T_BG_PSLIP', $dataPslip, $where);

				Application_Helper_Email::runTransaction($getBG['CLOSE_REF_NUMBER'], '3');

				$this->setbackURL('/' . $this->_request->getModuleName() . '/release/');
				$this->_redirect('/notification/success/index');
			}

			if ($this->_getParam('gagal')) {
				// $this->cekTimeout($getBG['CLOSE_REF_NUMBER']);
				// return;

				$dataPslip = array(
					'PS_STATUS' => 6,
				);

				$where['CLOSE_REF_NUMBER = ?'] = $getBG['CLOSE_REF_NUMBER'];
				$this->_db->update('T_BG_PSLIP', $dataPslip, $where);

				Application_Helper_Email::runTransaction($getBG['CLOSE_REF_NUMBER'], '4');

				$this->setbackURL('/' . $this->_request->getModuleName() . '/release/');
				$this->_redirect('/notification/success/index');
			}

			if ($kirim) {
				$data = array(
					'PS_CONFIRMED' => new Zend_Db_Expr("now()"),
					'PS_CONFIRMEDBY' => $this->_userIdLogin,
					'PS_STATUS' => 3,
				);

				$where['CLOSE_REF_NUMBER= ?'] = $getBG['CLOSE_REF_NUMBER'];
				$this->_db->update('T_BG_PSLIP', $data, $where);

				$historyInsert = array(
					'DATE_TIME'         => new Zend_Db_Expr("now()"),
					'CLOSE_REF_NUMBER'         => $getBG['CLOSE_REF_NUMBER'],
					'BG_NUMBER'         => $getBG['BG_NUMBER'],
					'CUST_ID'           => "-",
					'USER_FROM'        => 'BANK',
					'USER_LOGIN'        => $this->_userIdLogin,
					'BG_REASON'        => 'NoRef Pengajuan : ' . $getBG['CLOSE_REF_NUMBER'],
					'HISTORY_STATUS'        => 16,
				);

				$this->_db->insert('T_BANK_GUARANTEE_HISTORY_CLOSE', $historyInsert);


				// SEND EMAIL
				$settings = new Settings();
				$allSetting = $settings->getAllSetting();

				$config = Zend_Registry::get('config');

				$bgcgType   = $config["bgcg"]["type"]["desc"];
				$bgcgCode   = $config["bgcg"]["type"]["code"];
				$arrbgcg 	= $this->combineCodeAndDesc($bgcgCode, $bgcgType);

				$bgclosingType   = $config["bgclosing"]["changetype"]["desc"];
				$bgclosingCode   = $config["bgclosing"]["changetype"]["code"];
				$arrbgclosingType 	= $this->combineCodeAndDesc($bgclosingCode, $bgclosingType);

				$bgclosingStatus   = $config["bgclosinghistory"]["status"]["desc"];
				$bgclosingstatusCode   = $config["bgclosinghistory"]["status"]["code"];
				$arrbgclosingStatus 	= $this->combineCodeAndDesc($bgclosingstatusCode, $bgclosingStatus);

				// $dataEmail = [
				// 	'[[close_ref_number]]' => $getBG['CLOSE_REF_NUMBER'],
				// 	'[[bg_number]]' => $getBG['BG_NUMBER'],
				// 	'[[bg_subject]]' => $getBG["BG_SUBJECT"],
				// 	'[[cust_name]]' => $getBG["CUST_NAME"],
				// 	'[[recipient_name]]' => $getBG['RECIPIENT_NAME'],
				// 	'[[counter_warranty_type]]' => $arrbgcg[$getBG['COUNTER_WARRANTY_TYPE']],
				// 	'[[change_type]]' => $arrbgclosingType[$getBG['CHANGE_TYPE_CLOSE']],
				// 	'[[suggestion_status]]' => $arrbgclosingStatus[15],
				// 	'[[repair_notes]]' => $historyInsert['BG_REASON'],
				// 	'[[recipient_name]]' => $getBG['RECIPIENT_NAME'],
				// ];

				// $getEmailTemplate = $allSetting['bemailtemplate_proses_penutupan_klaim'];
				// $getEmailTemplate = strtr($getEmailTemplate, $dataEmail);

				// $privi = $getBG['COUNTER_WARRANTY_TYPE'] == '1' ? ['PPCC'] : ['PPNC'];
				// $emailAddresses = $this->getBuserEmailWithPrivi($privi, $getBG['BG_BRANCH']);

				// // SEND EMAIL KE SME/BTS
				// foreach ($emailAddresses as $email) {
				// 	$this->sendEmail($email['BUSER_EMAIL'], $this->language->_('Notifikasi Proses Penutupan / Klaim'), $getEmailTemplate);
				// }


				if ($getBG['COUNTER_WARRANTY_TYPE'] == '1') {
					Application_Helper_General::writeLog('KPCC', 'Konfirmasi Penyelesaian Pelepasan Jaminan Principal untuk BG No : ' . $getBG['BG_NUMBER'] . ', Principal : ' . $getBG['CUST_ID'] . ', NoRef Penyelesaian : ' . $getBG['CLOSE_REF_NUMBER'] . '');
				} else {
					Application_Helper_General::writeLog('KPNC', 'Konfirmasi Penyelesaian Pelepasan MD Principal untuk BG No : ' . $getBG['BG_NUMBER'] . ', Principal : ' . $getBG['CUST_ID'] . ', NoRef Penyelesaian : ' . $getBG['CLOSE_REF_NUMBER'] . '');
				}

				$this->setbackURL('/' . $this->_request->getModuleName() . '/release/');
				$this->_redirect('/notification/success/index');
			}


			$persetujuan = $this->_getParam('persetujuan');
			if ($persetujuan) {

				$authAdapter = new SGO_Auth_Adapter_Ldap($this->_userIdLogin, $this->_request->getParam("password_to_ldap"));

				$checkLDAP = $authAdapter->verifyldap();

				$checkLDAP = json_decode($checkLDAP, true);

				//Zend_Debug::dump($checkLDAP);die;

				if ($checkLDAP['status'] === '0') {
					$error_LDAP = $this->language->_("Password Salah");
					$this->view->error = true;
					$this->view->errorMsg = $error_LDAP;
				}

				if ($checkLDAP['status'] === '1') {
					$data = array(
						'PS_CONFIRMED' => new Zend_Db_Expr("now()"),
						'PS_CONFIRMEDBY' => $this->_userIdLogin,
						'PS_STATUS' => 6,
						'DISPLAY_FLAG' => 1,
					);

					$where['CLOSE_REF_NUMBER= ?'] = $getBG['CLOSE_REF_NUMBER'];
					$this->_db->update('T_BG_PSLIP', $data, $where);

					$historyInsert = array(
						'DATE_TIME'         => new Zend_Db_Expr("now()"),
						'CLOSE_REF_NUMBER'         => $getBG['CLOSE_REF_NUMBER'],
						'BG_NUMBER'         => $getBG['BG_NUMBER'],
						'CUST_ID'           => "-",
						'USER_FROM'        => 'BANK',
						'USER_LOGIN'        => $this->_userIdLogin,
						'BG_REASON'        => null,
						'HISTORY_STATUS'        => 20,
					);

					$bgdatasplit = $this->_db->select()
						->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'))
						->where('A.BG_REG_NUMBER = ?', $getBG['BG_REG_NUMBER'])
						->query()->fetchAll();


					$no = 1;

					foreach ($bgdatasplit as $key => $value) {

						if (strtolower($value["ACCT_DESC"] == 'Escrow')) {

							$TRANSACTION_ID = $this->_db->select()
								->from(
									array('A' => 'T_BG_TRANSACTION'),
									array('*')
								)
								->where('A.CLOSE_REF_NUMBER = ?', $getBG['CLOSE_REF_NUMBER'])
								->order('A.TRANSACTION_ID DESC')
								->query()->fetchAll();
							// $no = count($TRANSACTION_ID);

							$dataListAccount = $this->_db->select()
								->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL_CLOSE'), array('*'))
								->where('A.CLOSE_REF_NUMBER = ?', $getBG['CLOSE_REF_NUMBER'])
								->query()->fetchAll();

							$jmlListAccount = $this->_db->select()
								->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL_CLOSE'), array('*'))
								->where('A.CLOSE_REF_NUMBER = ?', $getBG['CLOSE_REF_NUMBER'])
								->group(array("ACCT_INDEX"))
								->query()->fetchAll();

							if (!empty($jmlListAccount)) {
								$jumlahAccount = count($jmlListAccount);
							}

							foreach ($dataListAccount as $key => $val) {
								$data[$val['PS_FIELDNAME']][$val['ACCT_INDEX']] = $val['PS_FIELDVALUE'];
							}

							for ($i = 1; $i <= $jumlahAccount; $i++) {

								// INSERT TO T BG TRANSACTION

								$insertData = [
									'TRANSACTION_ID' => $this->generateTransactionID($getBG['CLOSE_REF_NUMBER'], $no++),
									'CLOSE_REF_NUMBER' => $getBG['CLOSE_REF_NUMBER'],
									'BG_NUMBER' => $value['BG_NUMBER'],
									'SERVICE' => 1, // TRANSFER BTN
									'SOURCE_ACCT' => $value['ACCT'],
									'SOURCE_ACCT_CCY' => 'IDR',
									"SOURCE_ACCT_NAME" => $value['NAME'],
									'CHARGE_ACCT' => '',
									'BENEF_SWIFT_CODE' => '',
									'BENEF_ACCT' => $data['Beneficiary Account Number'][$i],
									'BENEF_ACCT_NAME' => $data['Beneficiary Account Name'][$i],
									'TRA_CCY' => $data['Beneficiary Account CCY'][$i],
									'TRA_AMOUNT' => $data['Transaction Amount'][$i],
									'CHARGE_AMOUNT' => '',
									'REMARKS1' => strval('Pelepasan ' . ($getBG['COUNTER_WARRANTY_TYPE'] == 1 ? 'Jaminan' : 'MD') . ' BG ' . $getBG['BG_NUMBER']),
									'REMARKS2' => '',
									'SOURCE_ADDRESS' => '',
									'SOURCE_PHONE' => '000',
									'BENEF_ADDRESS' => '',
									'BENEF_PHONE' => '000',
									'TRA_TIME' => null,
									'TRA_STATUS' => null,
									'UPDATE_APPROVED' => null,
									'UPDATE_APPROVEDBY' => null,
									'UUID' => null
								];

								$this->_db->insert('T_BG_TRANSACTION', $insertData);
							}
						} else {

							$callService = new Service_Account($value['ACCT'], '');
							$acctInfo = $callService->inquiryAccountBalance();

							// TABUNGAN
							if (strtolower($acctInfo['account_type']) == 's') {
								// INSERT TO T BG TRANSACTION
								$insertData = [
									'TRANSACTION_ID' => $this->generateTransactionID($getBG['CLOSE_REF_NUMBER'], $no++),
									'CLOSE_REF_NUMBER' => $getBG['CLOSE_REF_NUMBER'],
									'BG_NUMBER' => $value['BG_NUMBER'],
									'SERVICE' => 3, // UNLOCK SAVING
									'SOURCE_ACCT' => $value['ACCT'],
									'SOURCE_ACCT_CCY' => $value['CCY_ID'],
									'SOURCE_ACCT_NAME' => $value['NAME'],
									'CHARGE_ACCT' => '',
									'BENEF_SWIFT_CODE' => '',
									'BENEF_ACCT' => '',
									'HOLD_BY_BRANCH' => $value['HOLD_BY_BRANCH'],
									'HOLD_SEQUENCE' => $value['HOLD_SEQUENCE'],
									'TRA_CCY' => $value['CCY_ID'],
									'TRA_AMOUNT' => $value['AMOUNT'],
									'CHARGE_AMOUNT' => '',
									'REMARKS1' => '',
									'REMARKS2' => '',
									'SOURCE_ADDRESS' => '',
									'SOURCE_PHONE' => '',
									'BENEF_ADDRESS' => '',
									'TRA_TIME' => null,
									'TRA_STATUS' => null,
									'UPDATE_APPROVED' => null,
									'UPDATE_APPROVEDBY' => null,
									'UUID' => null
								];

								$this->_db->insert('T_BG_TRANSACTION', $insertData);
							}

							// DEPOSITO
							$acctInfo = $callService->inquiryDeposito();
							if (strtolower($acctInfo['account_type']) == 't') {

								// INSERT TO T BG TRANSACTION
								$insertData = [
									'TRANSACTION_ID' => $this->generateTransactionID($getBG['CLOSE_REF_NUMBER'], $no++),
									'CLOSE_REF_NUMBER' => $getBG['CLOSE_REF_NUMBER'],
									'BG_NUMBER' => $value['BG_NUMBER'],
									'SERVICE' => 2, // UNLOCK DEPOSITO
									'SOURCE_ACCT' => $value['ACCT'],
									'SOURCE_ACCT_CCY' => $value['CCY_ID'],
									'SOURCE_ACCT_NAME' => $value['NAME'],
									'CHARGE_ACCT' => '',
									'BENEF_SWIFT_CODE' => '',
									'BENEF_ACCT' => '',
									'HOLD_BY_BRANCH' => $value['HOLD_BY_BRANCH'],
									'HOLD_SEQUENCE' => $value['HOLD_SEQUENCE'],
									'TRA_CCY' => $value['CCY_ID'],
									'TRA_AMOUNT' => $value['AMOUNT'],
									'CHARGE_AMOUNT' => '',
									'REMARKS1' => '',
									'REMARKS2' => '',
									'SOURCE_ADDRESS' => '',
									'SOURCE_PHONE' => '',
									'BENEF_ADDRESS' => '',
									'TRA_TIME' => null,
									'TRA_STATUS' => null,
									'UPDATE_APPROVED' => null,
									'UPDATE_APPROVEDBY' => null,
									'UUID' => null
								];

								$this->_db->insert('T_BG_TRANSACTION', $insertData);
							}
						}
					}

					$selectTrans = $this->_db->select()
						->from(
							array('A' => 'T_BG_TRANSACTION'),
							array('*')
						)
						->where('A.CLOSE_REF_NUMBER = ?', $getBG['CLOSE_REF_NUMBER'])
						->order('A.SERVICE ASC');
					$dataTransaction = $this->_db->fetchAll($selectTrans);

					foreach ($dataTransaction as $row) {

						/*$arrListService = [
							1 => 'Transfer Inhouse',
							2 => 'Unlock Deposito',
							3 => 'Unlock Saving',
							4 => 'Open Account',
							5 => 'Deposito Disbursement',
							6 => 'Loan Disbursement',
							7 => 'SKN',
							8 => 'RTGS'
						  ];*/
						$statusService = array();

						if ($row['SERVICE'] == 1 && false) {

							if ($row['TRA_STATUS'] != 1) {
								$param = [
									"SOURCE_ACCOUNT_NUMBER" => $row["SOURCE_ACCT"],
									"SOURCE_ACCOUNT_NAME" => $row["SOURCE_ACCT_NAME"],
									"BENEFICIARY_ACCOUNT_NUMBER" => $row['BENEF_ACCT'],
									"BENEFICIARY_ACCOUNT_NAME" => $row["BENEF_ACCT_NAME"],
									"PHONE_NUMBER_FROM" =>  $row["SOURCE_PHONE"],
									"AMOUNT" => $row["TRA_AMOUNT"],
									"PHONE_NUMBER_TO" =>  $row["BENEF_ACCT_PHONE"],
									"RATE1" => 0,
									"RATE2" => 0,
									"RATE3" => 0,
									"RATE4" => 0,
									// "DESCRIPTION" => $row['TRANSACTION_ID'],
									"DESCRIPTION" => strval('Pelepasan ' . ($getBG['COUNTER_WARRANTY_TYPE'] == 1 ? 'Jaminan' : 'MD') . ' BG ' . $row['BG_NUMBER']),
									"DESCRIPTION_DETAIL" => '',
								];
								$service =  new Service_TransferWithin($param);
								$response = $service->sendTransfer();

								$parseDateTime = DateTime::createFromFormat('Y-m-d\TH:i:s+', $response['RawResult']['datetime']);
								$traTime = $parseDateTime->format('Y-m-d H:i:s');

								if ($response["RawResult"]["response_code"] == "0000") {
									$this->_db->update("T_BG_TRANSACTION", [
										"BANK_RESPONSE" => $response["RawResult"]["response_code"] . " - " . $response["RawResult"]["response_desc"],
										"UUID" => $response["RawResult"]["uuid"],
										'UPDATE_APPROVED'  => new Zend_Db_Expr("now()"),
										'UPDATE_APPROVEDBY'  => $this->_userIdLogin,
										"TRA_STATUS" => 1,
										'TRA_TIME' => $traTime,
										'TRA_REF' => $response["RawResult"]["ref"],
									], [
										"TRANSACTION_ID = ?" => $row["TRANSACTION_ID"]
									]);
									$statusService = 1;
								} else if ($response["RawResult"]["response_code"] == "9999") {
									$this->_db->update("T_BG_TRANSACTION", [
										"BANK_RESPONSE" => $response["RawResult"]["response_code"] . " - " . $response["RawResult"]["response_desc"],
										"UUID" => $response["RawResult"]["uuid"],
										'UPDATE_APPROVED'  => new Zend_Db_Expr("now()"),
										'UPDATE_APPROVEDBY'  => $this->_userIdLogin,
										"TRA_STATUS" => 3,
										'TRA_TIME' => $traTime,
										'TRA_REF' => $response["RawResult"]["ref"],
									], [
										"TRANSACTION_ID = ?" => $row["TRANSACTION_ID"]
									]);
								} else {
									$this->_db->update("T_BG_TRANSACTION", [
										"BANK_RESPONSE" => $response["RawResult"]["response_code"] . " - " . $response["RawResult"]["response_desc"],
										"UUID" => $response["RawResult"]["uuid"],
										'UPDATE_APPROVED'  => new Zend_Db_Expr("now()"),
										'UPDATE_APPROVEDBY'  => $this->_userIdLogin,
										"TRA_STATUS" => 4,
										'TRA_TIME' => $traTime,
										'TRA_REF' => $response["RawResult"]["ref"],
									], [
										"TRANSACTION_ID = ?" => $row["TRANSACTION_ID"]
									]);
								}
							}
						}

						if ($row['SERVICE'] == 2 && false) {

							if ($row['TRA_STATUS'] != 1) {
								$svcAccount = new Service_Account($row["SOURCE_ACCT"], null);
								$unlockDeposito = $svcAccount->unlockDeposito('T', $row['HOLD_BY_BRANCH'], $row['HOLD_SEQUENCE']);

								$parseDateTime = DateTime::createFromFormat('Y-m-d\TH:i:s+', $unlockDeposito['datetime']);
								$traTime = $parseDateTime->format('Y-m-d H:i:s');

								if ($unlockDeposito['response_code'] == "0000") {
									$this->_db->update("T_BG_TRANSACTION", [
										"BANK_RESPONSE" => $unlockDeposito['response_code'] . " - " . $unlockDeposito['response_desc'],
										"UUID" => $unlockDeposito["uuid"],
										'UPDATE_APPROVED'  => new Zend_Db_Expr("now()"),
										'UPDATE_APPROVEDBY'  => $this->_userIdLogin,
										"TRA_STATUS" => 1,
										'TRA_TIME' => $traTime,
										"TRA_REF" => $unlockDeposito['ref']
									], [
										"TRANSACTION_ID = ?" => $row["TRANSACTION_ID"]
									]);
									$statusService++;
								} else if ($unlockDeposito['response_code'] == "9999") {
									$this->_db->update("T_BG_TRANSACTION", [
										"BANK_RESPONSE" => $unlockDeposito['response_code'] . " - " . $unlockDeposito['response_desc'],
										"UUID" => $unlockDeposito["uuid"],
										'UPDATE_APPROVED'  => new Zend_Db_Expr("now()"),
										'UPDATE_APPROVEDBY'  => $this->_userIdLogin,
										"TRA_STATUS" => 3,
										'TRA_TIME' => $traTime,
										"TRA_REF" => $unlockDeposito['ref']
									], [
										"TRANSACTION_ID = ?" => $row["TRANSACTION_ID"]
									]);
								} else {
									$this->_db->update("T_BG_TRANSACTION", [
										"BANK_RESPONSE" => $unlockDeposito['response_code'] . " - " . $unlockDeposito['response_desc'],
										"UUID" => $unlockDeposito["uuid"],
										'UPDATE_APPROVED'  => new Zend_Db_Expr("now()"),
										'UPDATE_APPROVEDBY'  => $this->_userIdLogin,
										"TRA_STATUS" => 4,
										'TRA_TIME' => $traTime,
										"TRA_REF" => $unlockDeposito['ref']
									], [
										"TRANSACTION_ID = ?" => $row["TRANSACTION_ID"]
									]);
								}
							}
						}

						if ($row['SERVICE'] == 3 && false) {

							if ($row['TRA_STATUS'] != 1) {
								$svcAccount = new Service_Account($row["SOURCE_ACCT"], null);
								$unlockSaving = $svcAccount->unlockSaving([
									'branch_code' => $row['HOLD_BY_BRANCH'],
									'sequence_number' => $row['HOLD_SEQUENCE']
								]);

								$parseDateTime = DateTime::createFromFormat('Y-m-d\TH:i:s+', $unlockSaving['datetime']);
								$traTime = $parseDateTime->format('Y-m-d H:i:s');

								if ($unlockSaving['response_code'] == "0000") {
									$this->_db->update("T_BG_TRANSACTION", [
										"BANK_RESPONSE" => $unlockSaving['response_code'] . " - " . $unlockSaving['response_desc'],
										"UUID" => $unlockSaving["uuid"],
										'UPDATE_APPROVED'  => new Zend_Db_Expr("now()"),
										'UPDATE_APPROVEDBY'  => $this->_userIdLogin,
										"TRA_STATUS" => 1,
										'TRA_TIME' => $traTime,
										"TRA_REF" => $unlockSaving['ref']
									], [
										"TRANSACTION_ID = ?" => $row["TRANSACTION_ID"]
									]);
									$statusService++;
								} else if ($unlockSaving['response_code'] == "9999") {
									$this->_db->update("T_BG_TRANSACTION", [
										"BANK_RESPONSE" => $unlockSaving['response_code'] . " - " . $unlockSaving['response_desc'],
										"UUID" => $unlockSaving["uuid"],
										'UPDATE_APPROVED'  => new Zend_Db_Expr("now()"),
										'UPDATE_APPROVEDBY'  => $this->_userIdLogin,
										"TRA_STATUS" => 3,
										'TRA_TIME' => $traTime,
										"TRA_REF" => $unlockSaving['ref']
									], [
										"TRANSACTION_ID = ?" => $row["TRANSACTION_ID"]
									]);
								} else {
									$this->_db->update("T_BG_TRANSACTION", [
										"BANK_RESPONSE" => $unlockSaving['response_code'] . " - " . $unlockSaving['response_desc'],
										"UUID" => $unlockSaving["uuid"],
										'UPDATE_APPROVED'  => new Zend_Db_Expr("now()"),
										'UPDATE_APPROVEDBY'  => $this->_userIdLogin,
										"TRA_STATUS" => 4,
										'TRA_TIME' => $traTime,
										"TRA_REF" => $unlockSaving['ref']
									], [
										"TRANSACTION_ID = ?" => $row["TRANSACTION_ID"]
									]);
								}
							}
						}
					}

					$selectTrans = $this->_db->select()
						->from(
							array('A' => 'T_BG_TRANSACTION'),
							array('*')
						)
						->where('A.CLOSE_REF_NUMBER = ?', $getBG['CLOSE_REF_NUMBER'])
						->where('A.BG_NUMBER = ?', $getBG['BG_NUMBER'])
						->order('A.SERVICE ASC');
					$dataTransaction = $this->_db->fetchAll($selectTrans);

					// $ps_status = true;
					// if ($dataTransaction) {
					// 	foreach ($dataTransaction as $row) {
					// 		$tra_status  = $row['TRA_STATUS'];
					// 		$ps_status = ($tra_status != 1) ? false : $ps_status;
					// 	}
					// }

					// if ($ps_status == true) {
					// 	$data = array(
					// 		'PS_APPROVED' => new Zend_Db_Expr("now()"),
					// 		'PS_APPROVEDBY' => $this->_userIdLogin,
					// 		'PS_DONE' => new Zend_Db_Expr("now()"),
					// 		'PS_STATUS' => 5,
					// 	);

					// 	$where['CLOSE_REF_NUMBER= ?'] = $getBG['CLOSE_REF_NUMBER'];
					// 	$this->_db->update('T_BG_PSLIP', $data, $where);
					// } else {
					// 	$data = array(
					// 		'PS_APPROVED' => new Zend_Db_Expr("now()"),
					// 		'PS_APPROVEDBY' => $this->_userIdLogin,
					// 		'PS_STATUS' => 4,
					// 	);

					// 	$where['CLOSE_REF_NUMBER= ?'] = $getBG['CLOSE_REF_NUMBER'];
					// 	$this->_db->update('T_BG_PSLIP', $data, $where);
					// }


					$historyInsert = array(
						'DATE_TIME'         => new Zend_Db_Expr("now()"),
						'CLOSE_REF_NUMBER'         => $getBG['CLOSE_REF_NUMBER'],
						'BG_NUMBER'         => $getBG['BG_NUMBER'],
						'CUST_ID'           => "-",
						'USER_FROM'        => 'BANK',
						'USER_LOGIN'        => $this->_userIdLogin,
						'BG_REASON'        => 'NoRef Pengajuan : ' . $getBG['CLOSE_REF_NUMBER'],
						'HISTORY_STATUS'        => 20,
					);

					$this->_db->insert('T_BANK_GUARANTEE_HISTORY_CLOSE', $historyInsert);


					// CHECK TRANSACTION
					$checkTrx = $this->_db->select()
						->from('T_BG_TRANSACTION')
						->where('CLOSE_REF_NUMBER = ?', $getBG['CLOSE_REF_NUMBER'])
						->where('TRA_STATUS != ?', '1')
						->query()->fetch();

					$checkExistTrx = $this->_db->select()
						->from('T_BG_TRANSACTION')
						->where('CLOSE_REF_NUMBER = ?', $getBG['CLOSE_REF_NUMBER'])
						->query()->fetch();

					// JIKA SEMUA TRANSAKSI BERHASIL MAKA KIRIM EMAIL
					if (!$checkTrx && $checkExistTrx && false) {
						// SEND EMAIL

						//$data['historydata'] = $historyData;

						$getIndexChargeAcctNumber = $this->_db->select()
							->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
							->where('CLOSE_REF_NUMBER = ?', $getBG['CLOSE_REF_NUMBER'])
							->where('LOWER(PS_FIELDNAME) = ?', 'beneficiary account number')
							->query()->fetch();

						$getAcctChargeDetail = $this->_db->select()
							->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
							->where('CLOSE_REF_NUMBER = ?', $getBG['CLOSE_REF_NUMBER'])
							->where('ACCT_INDEX = ?', $getIndexChargeAcctNumber['ACCT_INDEX'])
							->query()->fetchAll();

						$nominal = number_format($getAcctChargeDetail["Transaction Amount"], 2);

						// SEND EMAIL TO PRINCIPAL
						$getEmailTemplate = $this->prepareForSendEmail($getBG, 'bemailtemplate_2A', $nominal);
						if ($getBG['CUST_EMAIL']) $this->sendEmail($getBG['CUST_EMAIL_BG'], $this->language->_('Pemberitahuan Proses Penyelesaian BG No. ' . $bgdata[0]['BG_NUMBER']), $getEmailTemplate);

						// SEND EMAIL TO OBLIGEE
						// $getEmailTemplate = $this->prepareForSendEmail($getBG, 'bemailtemplate_2A', $nominal);
						// if ($getBG['RECIPIENT_EMAIL']) $this->sendEmail($getBG['RECIPIENT_EMAIL'], $this->language->_('Pemberitahuan Proses Penyelesaian BG No. ' . $bgdata[0]['BG_NUMBER']), $getEmailTemplate);

						// SEND EMAIL TO KANTOR CABANG PENERBIT
						$getEmailTemplate = $this->prepareForSendEmail($getBG, 'bemailtemplate_2C', $nominal);
						if ($getBG['BRANCH_EMAIL']) $this->sendEmail($getBG['BRANCH_EMAIL'], $this->language->_('Pemberitahuan Proses Penyelesaian BG No. ' . $bgdata[0]['BG_NUMBER']), $getEmailTemplate);

						// SEND EMAIL TO GROUP CASH COL / NON CASH COL

						if ($getBG['COUNTER_WARRANTY_TYPE'] == 1) {
							// FULL COVER
							$getGroupFC = $settings->getSettingNew('email_group_cashcoll');
							$getGroupFC = explode(',', $getGroupFC);
							$getGroupFC = array_map('trim', $getGroupFC);
							if ($getGroupFC) {
								foreach ($getGroupFC as $emailGroup) {
									$getEmailTemplate = $this->prepareForSendEmail($getBG, 'bemailtemplate_2C', $nominal);
									if ($emailGroup) $this->sendEmail($emailGroup, $this->language->_('Pemberitahuan Proses Penyelesaian BG No. ' . $bgdata[0]['BG_NUMBER']), $getEmailTemplate);
								}
							}
						}

						if ($getBG['COUNTER_WARRANTY_TYPE'] == 2) {
							// LINE FACILITY
							$getGroupFC = $settings->getSettingNew('email_group_linefacility');
							$getGroupFC = explode(',', $getGroupFC);
							$getGroupFC = array_map('trim', $getGroupFC);
							if ($getGroupFC) {
								foreach ($getGroupFC as $emailGroup) {
									$getEmailTemplate = $this->prepareForSendEmail($getBG, 'bemailtemplate_2C', $nominal);
									if ($emailGroup) $this->sendEmail($emailGroup, $this->language->_('Pemberitahuan Proses Penyelesaian BG No. ' . $bgdata[0]['BG_NUMBER']), $getEmailTemplate);
								}
							}
						}

						if ($getBG['COUNTER_WARRANTY_TYPE'] == 3) {
							// LINE FACILITY
							$getGroupFC = $settings->getSettingNew('email_group_insurance');
							$getGroupFC = explode(',', $getGroupFC);
							$getGroupFC = array_map('trim', $getGroupFC);
							if ($getGroupFC) {
								foreach ($getGroupFC as $emailGroup) {
									$getEmailTemplate = $this->prepareForSendEmail($getBG, 'bemailtemplate_2C', $nominal);
									if ($emailGroup) $this->sendEmail($emailGroup, $this->language->_('Pemberitahuan Proses Penyelesaian BG No. ' . $bgdata[0]['BG_NUMBER']), $getEmailTemplate);
								}
							}
						}

						// SEND EMAIL TO KANTOR CABANG PENERBIT
						$getInsBranch = $this->_db->select()
							->from('T_BANK_GUARANTEE_DETAIL', ['PS_FIELDVALUE'])
							->where('BG_REG_NUMBER = ?', $getBG['BG_REG_NUMBER'])
							->where('LOWER(PS_FIELDNAME) = ?', 'insurance branch')
							->query()->fetch();

						$getInsBranchEmail = $this->_db->select()
							->from('M_INS_BRANCH')
							->where('INS_BRANCH_CODE = ?', $getInsBranch['PS_FIELDVALUE'] ?: 'XXX')
							->query()->fetch();

						$getEmailTemplate = $this->prepareForSendEmail($getBG, 'bemailtemplate_2B', $nominal);
						if ($getInsBranchEmail['INS_BRANCH_EMAIL'] && $bgdatasplit) $this->sendEmail($getInsBranchEmail['INS_BRANCH_EMAIL'], $this->language->_('Pemberitahuan Proses Penyelesaian BG No. ' . $bgdata[0]['BG_NUMBER']), $getEmailTemplate);
					}

					Application_Helper_Email::runTransaction($getBG['CLOSE_REF_NUMBER']);


					if ($getBG['COUNTER_WARRANTY_TYPE'] == '1') {
						Application_Helper_General::writeLog('PPCC', 'Persetujuan Penyelesaian Pelepasan Jaminan untuk BG No : ' . $getBG['BG_NUMBER'] . ', Principal : ' . $getBG['CUST_ID'] . ', NoRef Penyelesaian : ' . $getBG['CLOSE_REF_NUMBER'] . '');
					} else {
						Application_Helper_General::writeLog('PPNC', 'Persetujuan Penyelesaian Pelepasan MD untuk BG No : ' . $getBG['BG_NUMBER'] . ', Principal : ' . $getBG['CUST_ID'] . ', NoRef Penyelesaian : ' . $getBG['CLOSE_REF_NUMBER'] . '');
					}

					$this->setbackURL('/' . $this->_request->getModuleName() . '/release/');
					$this->_redirect('/notification/success/index');
				}
			}

			$back = $this->_getParam('back');
			if ($back) {
				$this->_redirect('/bgclosingworkflow/release');
			}

			$pdf = $this->_getParam('pdf');
			if ($pdf) {
				$bgdata = $this->_db->select()
					->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
					->where('A.BG_REG_NUMBER = ?', $getBG['BG_REG_NUMBER'])
					->query()->fetchAll();

				$listMarginalDeposit = $this->_db->select()
					->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
					->joinLeft(['B' => 'M_CUSTOMER_ACCT'], 'A.ACCT = B.ACCT_NO AND B.CUST_ID = ' . $this->_db->quote($bgdata[0]['CUST_ID']) . '', ["M_ACCT_DESC" => "B.ACCT_DESC", "M_ACCT_TYPE" => "B.ACCT_TYPE", 'CUST_ID', 'CCY_ID'])
					->where('A.BG_REG_NUMBER = ?', $getBG['BG_REG_NUMBER'])
					->query()->fetchAll();

				$this->view->listMarginalDeposit = $listMarginalDeposit;
				$viewHtml = $this->view->render('releasedetail/pdf.phtml');

				$outputHTML = "<tr><td>" . $viewHtml . "</td></tr>";
				//echo $outputHTML;die;

				$this->_db->insert('T_BACTIVITY', array(
					'LOG_DATE'         => new Zend_Db_Expr('now()'),
					'USER_ID'           => $this->_userIdLogin,
					'USER_NAME'           => $this->_userNameLogin,
					'ACTION_DESC'       => 'RPCS',
					'ACTION_FULLDESC'   => 'Download PDF Penutupan Bank Garansi . BG Number : ( ' . $getBG['BG_NUMBER'] . ' )',
				));

				// Application_Helper_General::writeLog('RPCS','Download PDF Customer Detail. Cust id : ( '.$cust_id.' )');

				$this->_helper->download->pdfModif(null, null, null, 'Penutupan Bank Garansi Nomor BG ( ' . $getBG['BG_NUMBER'] . ' )', $outputHTML);
			}
		}

		Application_Helper_General::writeLog('RCCS', 'Lihat Detail Penyelesaian ' . $arrStatusPenutupan[$getBG['PS_STATUS']] . ' untuk BG No : ' . $getBG['BG_NUMBER'] . ', Principal : ' . $getBG['CUST_ID'] . ', NoRef Penyelesaian : ' . $getBG['CLOSE_REF_NUMBER'] . '');
	}

	private function prepareForSendEmail($data, $emailTemplate, $nominal)
	{
		$getBG = $data;
		$settings = new Settings();
		$allSetting = $settings->getAllSetting();

		$config = Zend_Registry::get('config');

		$bgcgType   = $config["bgcg"]["type"]["desc"];
		$bgcgCode   = $config["bgcg"]["type"]["code"];
		$arrbgcg 	= $this->combineCodeAndDesc($bgcgCode, $bgcgType);

		$bgclosingType   = $config["bgclosing"]["changetype"]["desc"];
		$bgclosingCode   = $config["bgclosing"]["changetype"]["code"];
		$arrbgclosingType 	= $this->combineCodeAndDesc($bgclosingCode, $bgclosingType);

		$bgclosingStatus   = $config["bgclosinghistory"]["status"]["desc"];
		$bgclosingstatusCode   = $config["bgclosinghistory"]["status"]["code"];
		$arrbgclosingStatus 	= $this->combineCodeAndDesc($bgclosingstatusCode, $bgclosingStatus);

		$getEscrowRelease = $this->_db->select()
			->from('TEMP_BANK_GUARANTEE_DETAIL_CLOSE')
			->where('CLOSE_REF_NUMBER = ?', $getBG['CLOSE_REF_NUMBER'])
			->query()->fetchAll();

		$getBgCurrency = $this->_db->select()
			->from('T_BANK_GUARANTEE_DETAIL')
			->where('BG_REG_NUMBER = ?', $getBG['BG_REG_NUMBER_SPESIAL'])
			->where('LOWER(PS_FIELDNAME) = ?', 'currency')
			->query()->fetch();

		$getBgCurrency = $getBgCurrency ? $getBgCurrency['PS_FIELDVALUE'] : '-';

		$benefAcctNumber = array_shift(array_filter($getEscrowRelease, function ($item) {
			return strtolower($item['PS_FIELDNAME']) == 'beneficiary account number';
		}))['PS_FIELDVALUE'];

		$benefAcctName = array_shift(array_filter($getEscrowRelease, function ($item) {
			return strtolower($item['PS_FIELDNAME']) == 'beneficiary account name';
		}))['PS_FIELDVALUE'];

		$getBankName = $this->_db->select()
			->from(['mdbt' => 'M_DOMESTIC_BANK_TABLE'], ['mdbt.BANK_NAME'])
			->where('mdbt.SWIFT_CODE = ?', new Zend_Db_Expr('(SELECT PS_FIELDVALUE FROM TEMP_BANK_GUARANTEE_DETAIL_CLOSE tbgdc WHERE tbgdc.CLOSE_REF_NUMBER = ' . $this->_db->quote($getBG['CLOSE_REF_NUMBER']) . ' AND LOWER(tbgdc.PS_FIELDNAME) = \'swift code\')'))
			->query()->fetch();

		// T BANK GUARANTEE UNDERLYING
		$getAllUnderlying = $this->_db->select()
			->from('T_BANK_GUARANTEE_UNDERLYING')
			->where('BG_REG_NUMBER = ?', $getBG['BG_REG_NUMBER_SPESIAL'])
			->query()->fetchAll();

		$underlyingText = implode(', ', array_column($getAllUnderlying, 'DOC_NAME'));

		$underlyingForEmail = '
			<div style="display:inline-block;max-width: 394px;overflow:hidden;text-overflow: ellipsis;white-space: nowrap;">' . $underlyingText . '</div>
				<br>
			<div style="display:inline-block; margin-top: 5px">(Total : ' . count($getAllUnderlying) . ' Dokumen)</div>
			';

		// GET SPLIT
		$getSplit = $this->_db->select()
			->from(['A' => 'T_BANK_GUARANTEE_SPLIT'])
			->where('A.BG_NUMBER = ?', $getBG['BG_NUMBER'])
			->query()->fetchAll();

		$filterWihtoutEscrow = array_filter($getSplit, function ($item) {
			return strtolower($item['ACCT_DESC']) != 'escrow';
		});

		$filterWihtoutEscrow = $filterWihtoutEscrow ?: [];

		$sumAll = array_sum(array_column($filterWihtoutEscrow, 'AMOUNT'));
		$sumAllNumberFormat = number_format($sumAll, 2);

		$dataEmail = [
			'[[close_ref_number]]' => $getBG['CLOSE_REF_NUMBER'],
			'[[bg_number]]' => $getBG['BG_NUMBER'],
			'[[bg_amount]]' => $getBgCurrency . ' ' . number_format($getBG['BG_AMOUNT'], 2),
			'[[bg_subject]]' => $getBG["BG_SUBJECT"],
			'[[ps_status]]' => 'Proses Selesai',
			'[[ps_done]]' => date('d-m-Y'),
			'[[usage_purpose]]' => ucwords(strtolower($getBG['USAGE_PURPOSE_DESC'])),
			'[[cust_name]]' => $getBG["CUST_NAME"],
			'[[cust_cp]]' => $getBG["CUST_CP"],
			'[[contract_name]]' => $underlyingForEmail,
			'[[title_counter_type]]' => $getBG['COUNTER_WARRANTY_TYPE'] == 1 ? 'Jaminan' : 'MD',
			'[[transaction_amount]]' => '',
			'[[recipient_name]]' => $getBG['RECIPIENT_NAME'],
			'[[recipient_cp]]' => $getBG['RECIPIENT_CP'],
			'[[counter_warranty_type]]' => $arrbgcg[$getBG['COUNTER_WARRANTY_TYPE']],
			'[[change_type]]' => $arrbgclosingType[$getBG['CHANGE_TYPE_CLOSE']],
			'[[suggestion_status]]' => $arrbgclosingStatus[15],
			'[[repair_notes]]' => $data['historydata']['BG_REASON'],
			'[[recipient_name]]' => $getBG['RECIPIENT_NAME'],
			'[[beneficiary_account_number]]' => $benefAcctNumber,
			'[[time_period_start]]' => date('d M Y', strtotime($getBG['TIME_PERIOD_START'])),
			'[[time_period_end]]' => date('d M Y', strtotime($getBG['TIME_PERIOD_END'])),
			'[[bankname]]' => $getBankName['BANK_NAME'],
			'[[beneficiary_account_name]]' => $benefAcctName,
			'[[insurance_name]]' => $getBG['INS_NAME'] ?: '-',
			'[[insurance_branch_name]]' => $getBG['INS_BRANCH_NAME'],
			'[[md_principal]]' => implode(' ', [$getBgCurrency, $sumAllNumberFormat]),
		];

		$getEmailTemplate = $allSetting[$emailTemplate];
		$getEmailTemplate = strtr($getEmailTemplate, $dataEmail);

		return $getEmailTemplate;
	}


	private function sendEmail($emailAddress, $subjectEmail, $emailTemplate)
	{
		$setting = new Settings();
		$allSetting = $setting->getAllSetting();

		$data = [
			'[[master_bank_name]]' => $allSetting["master_bank_name"],
			'[[master_bank_app_name]]' => $allSetting["master_bank_app_name"],
			'[[master_app_name]]' => $allSetting["master_bank_app_name"],
			'[[master_bank_email]]' => $allSetting["master_bank_email"],
			'[[master_bank_telp]]' => $allSetting["master_bank_telp"],
		];

		$getEmailTemplate = strtr($emailTemplate, $data);
		Application_Helper_Email::sendEmail($emailAddress, $subjectEmail, $getEmailTemplate);
	}

	private function combineCodeAndDesc($arrayCode, $arrayDesc)
	{
		return array_combine($arrayCode, array_map(function ($arrayMap) use ($arrayDesc, $arrayCode) {
			return $arrayDesc[$arrayMap];
		}, array_keys($arrayCode)));
	}



	function generateTransactionID($paymentRef, $counter = 0)
	{
		//		$sql_countTransaction = dbQuery("select count(transaction_id) as countTransaction from sb_transaction where ps_number = '{$paymentID}'");
		//		$rs_countTransaction = tep_db_fetch_array($sql_countTransaction);

		// $countTransaction = ($counter < 1) ? 1 : $counter + 1;
		$countTransaction = $counter;
		$seqNumberTransaction = str_pad($countTransaction, ((strlen($countTransaction) == 1) ? 2 : ((0 + strlen($countTransaction)) - strlen($countTransaction))), "0", STR_PAD_LEFT);
		$transactionID = $paymentRef . $seqNumberTransaction;

		return $transactionID;
	}

	public function detailAction()
	{

		$this->_helper->layout()->setLayout('popup');

		$settings = new Settings();
		$system_type = $settings->getSetting('system_type');

		// BEGIN GLOBAL VARIABEL
		$config = Zend_Registry::get('config');
		$bgpublishType  = $config["bgpublish"]["type"]["desc"];
		$bgpublishCode  = $config["bgpublish"]["type"]["code"];
		$arrbgpublish   = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

		$bgcgType   = $config["bgcg"]["type"]["desc"];
		$bgcgCode   = $config["bgcg"]["type"]["code"];
		$arrbgcg 	= array_combine(array_values($bgcgCode), array_values($bgcgType));

		$bgType   = $config["bg"]["type"]["desc"];
		$bgCode   = $config["bg"]["type"]["code"];
		$arrbg 	= array_combine(array_values($bgCode), array_values($bgType));
		$this->view->arrbg = $arrbg;

		$bgclosingType   = $config["bgclosing"]["changetype"]["desc"];
		$bgclosingCode   = $config["bgclosing"]["changetype"]["code"];
		$arrbgclosingType 	= array_combine(array_values($bgclosingCode), array_values($bgclosingType));
		$this->view->arrbgclosingType = $arrbgclosingType;

		$bgclosingStatus   = $config["bgclosing"]["status"]["desc"];
		$bgclosingstatusCode   = $config["bgclosing"]["status"]["code"];
		$arrbgclosingStatus 	= array_combine(array_values($bgclosingstatusCode), array_values($bgclosingStatus));
		$this->view->arrbgclosingStatus = $arrbgclosingStatus;

		$historyStatusCode = $config["bgclosinghistory"]["status"]["code"];
		$historyStatusDesc = $config["bgclosinghistory"]["status"]["desc"];
		$historyStatusArr  = array_combine(array_values($historyStatusCode), array_values($historyStatusDesc));
		$this->view->historyStatusArr   = $historyStatusArr;

		$arrStatusPenutupan = [
			1 => 'Menunggu Konfirmasi Pembayaran',
			2 => 'Permintaan Perbaikan Konfirmasi Penyelesaian',
			3 => 'Menunggu Persetujuan Pembayaran',
			4 => 'Service dalam proses',
			5 => 'Proses Selesai',
			6 => 'Processing'
		];

		$this->view->arrStatusPenutupan = $arrStatusPenutupan;

		$config = Zend_Registry::get('config');

		$transCode = $config['transaction']['list']['code'];
		$transDesc = $config['transaction']['list']['desc'];

		$arrListService = array_combine($transCode, $transDesc);

		$this->view->arrListService = $arrListService;

		$arrStatusTransaction = [
			1 => 'Sukses',
			2 => 'Sukses dengan Pembaharuan Status',
			3 => 'Time Out',
			4 => 'Gagal',
			5 => 'Menunggu Pembayaran',
			6 => 'Pending Service'
		];

		$this->view->arrStatusTransaction = $arrStatusTransaction;

		// END GLOBAL VARIABEL

		// BEGIN PENUTUPAN BANK GARANSI
		// BEGIN DETAIL BANK GARANSI

		$numb = $this->_getParam('bgnumb');


		if (!empty($numb)) {
			$bgdata = $this->_db->select()
				->from(array('A' => 'T_BANK_GUARANTEE'), array('*', 'BG_REG_NUMBER_SPESIAL' => 'A.BG_REG_NUMBER'))
				->joinleft(array('B' => 'M_CUSTOMER'), 'A.CUST_ID = B.CUST_ID', array('B.*'))
				->joinleft(array('C' => 'M_BRANCH'), 'A.BG_BRANCH = C.BRANCH_CODE', array('C.BRANCH_NAME'))
				->joinleft(array('D' => 'TEMP_BANK_GUARANTEE_CLOSE'), 'A.BG_NUMBER = D.BG_NUMBER', array('D.CHANGE_TYPE', 'D.SUGGESTION_STATUS', 'D.CLOSE_REF_NUMBER', 'D.CLAIM_FIRST_VERIFIED'))
				->joinleft(array('E' => 'M_CITYLIST'), 'B.CUST_CITY = E.CITY_CODE', array('E.*'))
				->joinleft(array('F' => 'T_BG_PSLIP'), 'F.CLOSE_REF_NUMBER = D.CLOSE_REF_NUMBER', array('F.PS_STATUS'))
				//->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
				->where('A.BG_NUMBER = ?', $numb)
				->query()->fetchAll();
			if (!empty($bgdata)) {
				$getBG = $bgdata['0'];

				if ($getBG['BG_NUMBER'] == '') {
					$getBG['BG_NUMBER'] = '-';
				}

				if ($getBG['BG_SUBJECT'] == '') {
					$getBG['BG_SUBJECT'] = '- no subject -';
				}

				$this->view->getBG = $getBG;
				//$this->view->time_calculation = $this->calculate_time_span($getBG['CLAIM_FIRST_VERIFIED']);


				$this->view->publishDesc = $arrbgpublish[$getBG['BG_PUBLISH']];
				if ($getBG['BG_PUBLISH'] == 1) {
					$getPaperPrint = $this->_db->select()
						->from('M_PAPER')
						->where('notes = ?', $getBG['BG_NUMBER'])
						->where('STATUS = 1')
						->query()->fetchAll();

					$totalKertas = count($getPaperPrint);

					$this->view->nomorRefKertas = ' - Nomor Ref Kertas : ' . $getPaperPrint[0]['PAPER_ID'] . ' - ' . $getPaperPrint[$totalKertas - 1]['PAPER_ID'] . ' (' . $totalKertas . ' lembar)';
				}

				// BEGIN NILAI BANK GARANSI

				$bgdetaildata = $this->_db->select()
					->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
					->where('A.BG_REG_NUMBER = ?', $getBG["BG_REG_NUMBER"])
					->query()->fetchAll();


				if (!empty($bgdetaildata)) {
					foreach ($bgdetaildata as $key => $value) {
						if ($value['PS_FIELDNAME'] == 'Currency') {
							$this->view->bgCurrency =   $value['PS_FIELDVALUE'];
						}

						if ($getBG['COUNTER_WARRANTY_TYPE'] == '3') {

							if ($value['PS_FIELDNAME'] == 'Insurance Name') {
								$this->view->insuraceName =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Insurance Branch') {

								$insuranceBranch = $this->_db->select()
									->from("M_INS_BRANCH")
									->where("INS_BRANCH_CODE = ?", $value['PS_FIELDVALUE'])
									->query()->fetchAll();

								$this->view->insuranceBranch = $insuranceBranch[0]["INS_BRANCH_NAME"];
							}
						}
					}
				}

				$this->view->BG_AMOUNT  = $getBG['BG_AMOUNT'];
				$this->view->updateStart = Application_Helper_General::convertDate($getBG['TIME_PERIOD_START'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->updateEnd = Application_Helper_General::convertDate($getBG['TIME_PERIOD_END'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->bgClaimDate = Application_Helper_General::convertDate($getBG['BG_CLAIM_DATE'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->bgIssuedDate = Application_Helper_General::convertDate($getBG['BG_ISSUED'], $this->view->viewDateFormat, $this->view->defaultDateFormat);


				// END NILAI BANK GARANSI

				// BEGIN KONTRA GARANSI
				$this->view->counterWarrantyType = $getBG['COUNTER_WARRANTY_TYPE'];
				$this->view->counterWarrantyDesc = $arrbgcg[$getBG['COUNTER_WARRANTY_TYPE']];
				$this->view->changeType = $getBG['CHANGE_TYPE'];

				// END KONTRA GARANSI


			}
		}
		// END DETAIL BANK GARANSI

		// END PENUTUPAN BANK GARANSI

		// Get data T_BG_TRANSACTION
		$transid = $this->_getParam('transid');

		$select = $this->_db->select()
			->from(
				array('A' => 'T_BG_TRANSACTION'),
				array('*')
			)
			->where('A.BG_NUMBER = ?', $getBG['BG_NUMBER'])
			->where('A.TRANSACTION_ID = ?', $transid)
			->order('A.SERVICE ASC');
		$dataHistoryTransaction = $this->_db->fetchAll($select);
		$this->view->dataHistoryTransaction = $dataHistoryTransaction[0];

		if ($this->_request->isPost()) {
			$pdf = $this->_getParam('pdf');
			if ($pdf) {

				$viewHtml = $this->view->render('releasedetail/pdfdetail.phtml');
				$outputHTML = "<tr><td>" . $viewHtml . "</td></tr>";

				$this->_helper->download->pdfModif(null, null, null, 'Detail Transaksi Nomor BG ( ' . $getBG['BG_NUMBER'] . ' )', $outputHTML);
			}
		}
	}

	public function inquirymarginaldepositAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$regNumber = $this->_getParam('reqNumber');

		$bgdata = $this->_db->select()
			->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
			->where('A.BG_REG_NUMBER = ?', $regNumber)
			->query()->fetchAll();

		$optHtml = '<table id="guarantedTransactions">
		<thead>
                                <th>Hold Seq</th>
                                <th>Nomor Rekening</th>
                                <th>Nama Rekening</th>
                                <th>Tipe</th>
                                <th>Valuta</th>
								<th>Nominal</th>
                            </thead>
                            <tbody>';

		$bgdatasplit = $this->_db->select()
			->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
			->joinLeft(['B' => 'M_CUSTOMER_ACCT'], 'A.ACCT = B.ACCT_NO AND B.CUST_ID = ' . $this->_db->quote($bgdata[0]['CUST_ID']) . '', ["M_ACCT_DESC" => "B.ACCT_DESC", "M_ACCT_TYPE" => "B.ACCT_TYPE", 'CUST_ID', 'CCY_ID'])
			->where('A.BG_REG_NUMBER = ?', $regNumber)
			->query()->fetchAll();
		//var_dump($bgdatasplit);
		if (!empty($bgdatasplit)) {
			foreach ($bgdatasplit as $key => $row) {
				// if (strtolower($row["M_ACCT_DESC"]) == "giro" || $row["M_ACCT_TYPE"] == "D" || strtolower($row["ACCT_DESC"]) == "giro" || strtolower($row["ACCT_TYPE"]) == "giro") continue;
				if (strtolower($row["M_ACCT_DESC"]) == "giro" || strtolower($row["ACCT_DESC"]) == "giro" || $row["M_ACCT_TYPE"] == "D") continue;
				// GET SERVICE 
				$serviceAccount = new Service_Account($row['ACCT'], null);
				$accountInfo = $serviceAccount->inquiryAccontInfo();
				$serviceCif = new Service_Account(null, null, null, null, null, $accountInfo["cif"]);
				$saveResult = $serviceCif->inquiryCIFAccount()["accounts"];
				$listAccount = array_search($row['ACCT'], array_column($saveResult, "account_number"));

				$somethingError = false;

				// CHECK HOLD SAVING
				if (strtolower($saveResult[$listAccount]["type"]) == 's') {
					$inqlocksaving = $serviceAccount->inqLockSaving();

					if ($inqlocksaving['lock']) {
						$findSequence = array_filter($inqlocksaving['lock'], function ($item) use ($row) {
							return $item['sequence_number'] == $row['HOLD_SEQUENCE'];
						});

						$somethingError = (empty($findSequence)) ? true : $somethingError;
					}
				}

				// CHECK LOCK DEPO
				if (strtolower($saveResult[$listAccount]["type"]) == 't') {

					$inqlockdepo = $serviceAccount->inquiryLockDeposito();

					if ($inqlockdepo['response_code'] != '0000') $somethingError = true;
					else {
						if ($inqlockdepo['sequence_number'] != $row['HOLD_SEQUENCE']) $somethingError = true;
					}
				}

				$optHtml .= '<tr>
						<td>' . $row['HOLD_SEQUENCE'] . '</td>
						<td style="width: 1%;">' . $row['ACCT'] . '</td>
						<td>' . $row['NAME'] . '</td>
						<td>' . $saveResult[$listAccount]["type_desc"] . " " . $saveResult[$listAccount]["product_type"] . '</td>
						<td>' . $saveResult[$listAccount]["currency"] . '</td>
						<td>' . number_format($row['AMOUNT'], 2) . ' ' . ($somethingError ? '<div class="text-danger d-inline-block ml-2" data-toggle="tooltip" data-placement="top" title="Data tidak ditemukan"><div class="d-flex align-items-center justify-content-center inforek" style="border: 1px solid red; width: 20px; height: 20px; border-radius: 9999px"></div></div>' : '') . '</td>
					</tr>
					';
			}
		} else {
			$optHtml .= '<tr><td colspan="7" class="tbl-evencontent" align="center">--- ' . $this->language->_('No Data') . ' ---</td></tr>';
		}

		$optHtml .= '</tbody></table>';

		echo $optHtml;
	}

	public function trxdetailAction()
	{
		$this->_helper->layout()->setLayout('popup');

		$AESMYSQL = new Crypt_AESMYSQL();

		$trxId = $this->_request->getParam('trxid');
		$trxId = $AESMYSQL->decrypt(urldecode($trxId), uniqid());

		$closeRefNumber = substr($trxId, 0, -2);

		$bgdata = $this->_db->select()
			->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
			->joinleft(array('B' => 'M_CUSTOMER'), 'A.CUST_ID = B.CUST_ID', array('B.*'))
			->joinleft(array('C' => 'M_BRANCH'), 'A.BG_BRANCH = C.BRANCH_CODE', array('C.BRANCH_NAME'))
			->joinleft(array('D' => 'TEMP_BANK_GUARANTEE_CLOSE'), 'A.BG_NUMBER = D.BG_NUMBER', array('D.CHANGE_TYPE', 'D.SUGGESTION_STATUS', 'D.CLAIM_FIRST_VERIFIED'))
			->joinleft(array('E' => 'M_CITYLIST'), 'B.CUST_CITY = E.CITY_CODE', array('E.*'))
			->joinleft(array('F' => 'T_BG_PSLIP'), 'F.BG_NUMBER = A.BG_NUMBER', array('F.PS_STATUS', 'F.TYPE'))
			// ->where('A.CLOSE_REF_NUMBER = ?', $closeRefNumber)
			->where('F.CLOSE_REF_NUMBER = ?', $closeRefNumber)
			->query()->fetch();

		$this->view->getBG = $bgdata;

		$detailTrx = $this->_db->select()
			->from('T_BG_TRANSACTION')
			->where('TRANSACTION_ID = ?', $trxId)
			->query()->fetch();

		$this->view->detailTrx = $detailTrx;

		$sourceAcctInfo = $this->getDetailInfoAcct($detailTrx['SOURCE_ACCT']);

		$this->view->sourceAcctInfo = $sourceAcctInfo;

		$config = Zend_Registry::get('config');

		$transCode = $config['transaction']['list']['code'];
		$transDesc = $config['transaction']['list']['desc'];

		$arrListService = array_combine($transCode, $transDesc);

		$this->view->arrListService = $arrListService;

		$arrStatusTransaction = [
			1 => 'Sukses',
			2 => 'Sukses dengan Pembaharuan Status',
			3 => 'Time Out',
			4 => 'Gagal',
			5 => 'Menunggu Pembayaran',
			6 => 'Pending Service'
		];

		if ($detailTrx['SEVICE'] == '7' || $detailTrx['SEVICE'] == '8') {
			$feeAcctDetail = $this->getDetailInfoAcct($detailTrx['CHARGE_ACCT']);

			$this->view->feeAcctDetail = $feeAcctDetail;
		}

		$this->view->arrStatusTransaction = $arrStatusTransaction;

		$this->view->getBG = $bgdata;
	}

	private function getDetailInfoAcct($acctnumber)
	{
		$callService = new Service_Account($acctnumber, '');
		$resultAcctInfo = $callService->inquiryAccontInfo();

		$getCif = $resultAcctInfo['cif'];

		$callServiceCif = new Service_Account('', '', '', '', '', $getCif);
		$resultCifAccount = $callServiceCif->inquiryCIFAccount();

		$filterAcct = array_shift(array_filter($resultCifAccount['accounts'], function ($item) use ($acctnumber) {
			return ltrim($item['account_number'], '0') == ltrim($acctnumber, '0');
		}));

		return array_merge($resultAcctInfo, $filterAcct);
	}

	/**
	 * @param array $privi
	 * @param string $bgBranch
	 * 
	 * @return array
	 */
	public function getBuserEmailWithPrivi(array $privi, string $bgBranch): array
	{
		$priviJoin = array_map(function ($item) {
			return "'$item'";
		}, $privi);
		$priviJoin = join(',', $priviJoin);

		$result = $this->_db->select()
			->from(['MB' => 'M_BUSER'], ['MB.BUSER_EMAIL'])
			->where('MB.BGROUP_ID IN (?)', new Zend_Db_Expr('(SELECT BGROUP_ID FROM M_BPRIVI_GROUP mbg WHERE mbg.BPRIVI_ID IN(' . $priviJoin . ') GROUP BY BGROUP_ID)'))
			->where('MB.BUSER_EMAIL != ?', '');

		if ($privi != 'PPNC') $result->where('BUSER_BRANCH IN (?)', new Zend_Db_Expr('(SELECT ID FROM M_BRANCH WHERE BRANCH_CODE = ' . $this->_db->quote('161') . ')'));

		$result = $result->group('MB.BUSER_EMAIL')
			->query()->fetchAll();

		return $result;
	}

	public function cekTimeout($closeRefNumber)
	{
		$getAllTimeoutTrans = $this->_db->select()
			->from('T_BG_TRANSACTION')
			->where('CLOSE_REF_NUMBER = ?', $closeRefNumber)
			->where('TRA_STATUS = ?', 3)
			->query()->fetchAll();

		$transCodeMapping = [
			1 => '8766',
			5 => '8771',
			6 => '8772',
			7 => '8768',
			8 => '8769',
		];

		$svcCekStatus = new SGO_Soap_ClientUser();

		foreach ($getAllTimeoutTrans as $trans) {

			if (in_array($trans['SERVICE'], [2, 3])) {
				if ($trans['SERVICE'] == 2) {
					$inqLockSaving = new Service_Account($trans['SOURCE_ACCT'], '');
					$resultInqLockSaving = $inqLockSaving->inquiryLockDeposito();

					if ($resultInqLockSaving['response_code'] == '0404') {
						$this->_db->update('T_BG_TRANSACTION', ['TRA_STATUS' => 1], [
							'CLOSE_REF_NUMBER = ?' => $closeRefNumber
						]);
					}
				} else {
					$inqLockSaving = new Service_Account($trans['SOURCE_ACCT'], '');
					$resultInqLockSaving = $inqLockSaving->inqLockSaving();

					if ($resultInqLockSaving['response_code'] == '0000') {
						$cekSeqNumber = array_filter($resultInqLockSaving['lock'], function ($item) use ($trans) {
							return $item['sequence_number'] == $trans['HOLD_SEQUENCE'];
						});

						if (empty($cekSeqNumber)) {
							$this->_db->update('T_BG_TRANSACTION', ['TRA_STATUS' => 1], [
								'CLOSE_REF_NUMBER = ?' => $closeRefNumber
							]);
						}
					}
				}
			} else {
				$svcCekStatus->callapi("inqtransfer", null, [
					"ref_trace" => $trans['TRA_REF'],
					"trans_code" => $transCodeMapping[$trans['SERVICE']]
				]);

				$result = $svcCekStatus->getResult();

				if ($result['response_code'] == '0000') {

					$this->_db->update('T_BG_TRANSACTION', ['TRA_STATUS' => 1], [
						'TRANSACTION_ID = ?' => $trans['TRANSACTION_ID']
					]);
				} else {
					$this->_db->update('T_BG_TRANSACTION', ['TRA_STATUS' => 4], [
						'TRANSACTION_ID = ?' => $trans['TRANSACTION_ID']
					]);
				}
			}
		}

		$getAllTrans = $this->_db->select()
			->from('T_BG_TRANSACTION')
			->where('CLOSE_REF_NUMBER = ?', $closeRefNumber)
			->where('TRA_STATUS != ?', 1)
			->query()->fetchAll();

		if (!$getAllTrans) {
			$this->_db->update('T_BG_PSLIP', ['PS_STATUS' => 5], [
				'CLOSE_REF_NUMBER = ?' => $closeRefNumber
			]);
		}

		$this->setbackURL('/' . $this->_request->getModuleName() . '/release/');
		$this->_redirect('/notification/success/index');
	}
}
