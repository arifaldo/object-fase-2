<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';


class bgclosingworkflow_ApproveController extends Application_Main
{

  protected $_moduleDB = 'RTF'; // masih harus diganti

  public function indexAction()
  {
    $this->_helper->_layout->setLayout('newlayout');

    $settings = new Settings();
    $system_type = $settings->getSetting('system_type');
    $this->view->systemType = $system_type;

    $conf = Zend_Registry::get('config');
    $this->_bankName = $conf['app']['bankname'];
    $this->view->masterbankname = $this->_bankName;

    $this->CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

    $fields = array(
      'regno'             => array(
        'field'    => 'BG_NUMBER',
        'label'    => $this->language->_('Nomor BG / Subjek'),
      ),
      'principal'              => array(
        'field'    => 'CUST_ID',
        'label'   => $this->language->_('Principal'),
      ),
      'obligee'              => array(
        'field'    => 'RECIPIENT_NAME',
        'label'   => $this->language->_('Obligee'),
      ),
      'bank_branch'       => array(
        'field'    => 'BG_BRANCH',
        'label'    => $this->language->_('Bank Cabang'),
      ),
      'bg_amount'         => array(
        'field'    => 'BG_AMOUNT',
        'label'    => $this->language->_('Nominal BG')
      ),
      'change_type'     => array(
        'field'    => 'CHANGE_TYPE',
        'label'    => $this->language->_('Tipe Usulan'),
      ),
    );

    $this->view->fields = $fields;

    // change type
    $bgChangeTypeCode = $conf['bgclosing']['changetype']['code'];
    $bgChangeTypeDesc = $conf['bgclosing']['changetype']['desc'];
    $bgChangeType = $this->combineCodeAndDesc($bgChangeTypeCode, $bgChangeTypeDesc);

    $this->view->bgChangeType = $bgChangeType;

    // filter ----------------------------------------------------------

    $filterlist = array_combine(array_column($fields, 'field'), array_column($fields, 'label'));
    $filterlist['BG_NUMBER'] = $this->language->_('Nomor BG');
    $filterlist['BG_SUBJECT'] = $this->language->_('Subjek');

    $this->view->filterlist = $filterlist;

    // filter 
    $filter = [];
    if ($this->_getParam('filter')) {
      foreach ($this->_getParam('wherecol') as $key => $value) {
        $filter[$value] = trim($this->_getParam('whereval')[$key]);
        if (strtolower($key) == 'bg_amount') $filter[$value] = str_replace([','], '', $filter[$value]);
      }

      $this->view->filterCol = $filter;
    }

    $getBranch = $this->_db->select()
      ->from(['MBSR' => 'M_BUSER'])
      ->joinLeft(['MBRN' => 'M_BRANCH'], 'MBSR.BUSER_BRANCH = MBRN.ID')
      ->where('MBSR.BUSER_ID = ?', $this->_userIdLogin)
      ->query()->fetch();

    $model = new bgclosingworkflow_Model_Approveclosing();
    $getALlClosingBg = $model->getAllClosingBg($filter, 6, $getBranch['BRANCH_CODE']);

    $result = [];
    foreach ($getALlClosingBg as $key => $item) {
		if ($item['COUNTER_WARRANTY_TYPE'] == '1') {
			$boundary = $this->validatebtn('50', $item['BG_AMOUNT'], 'IDR', $item['CLOSE_REF_NUMBER']);
		} else {
			$boundary = $this->validatebtn('51', $item['BG_AMOUNT'], 'IDR', $value['CLOSE_REF_NUMBER']);
		}
	  
		if (!$boundary) continue;
      	$result[$key] = $item;
    }

    $this->paging($result);

    $conf = Zend_Registry::get('config');

    $this->view->bankname = $conf['app']['bankname'];

    $config        = Zend_Registry::get('config');
    $BgType     = $config["bg"]["status"]["desc"];
    $BgCode     = $config["bg"]["status"]["code"];

    $arrStatus = array_combine(array_values($BgCode), array_values($BgType));

    $this->view->arrStatus = $arrStatus;

    $arrType = array(
      1 => 'Standart',
      2 => 'Custom'
    );

    $arrLang = array(
      1 => 'Indonesian',
      2 => 'English',
      3 => 'Bilingual'
    );
    $this->view->langArr = $arrLang;
    $this->view->formatArr = $arrType;

    if (!empty($dataParamValue)) {

      $this->view->efdateStart = $dataParamValue['BG_PERIOD'];
      $this->view->efdateEnd = $dataParamValue['BG_PERIOD_END'];

      foreach ($dataParamValue as $key => $value) {
        $duparr = explode(',', $value);
        if (!empty($duparr)) {

          foreach ($duparr as $ss => $vs) {
            $wherecol[]  = $key;
            $whereval[] = $vs;
          }
        } else {
          $wherecol[]  = $key;
          $whereval[] = $value;
        }
      }
      $this->view->wherecol     = $wherecol;
      $this->view->whereval     = $whereval;
    }

    Application_Helper_General::writeLog('ACCS', $this->language->_('Lihat Daftar Pengajuan Penutupan dan Klaim Menunggu Persetujuan Bank'));
  }

  public function getcountertypeAction()
  {
    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout()->disableLayout();

    $tblName = $this->_getParam('id');


    $optHtml = "<option value=''>-- " . $this->language->_('Please Select') . " --</option>";

    $arrWarrantyType = array(
      '1' => 'FC',
      '2' => 'LF',
      '3' => 'INS'
    );

    foreach ($arrWarrantyType as $key => $row) {
      if ($tblName == $key) {
        $select = 'selected';
      } else {
        $select = '';
      }
      $optHtml .= "<option value='" . $key . "' " . $select . ">" . $row . "</option>";
    }

    echo $optHtml;
  }

  public function filterAction()
  {
    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout()->disableLayout();

    $param = $this->_getParam('type');

    if ($param == 'principal') {
      $tblName = $this->_getParam('id');
      $optHtml = "<option value=''>-- " . $this->language->_('Please Select') . " --</option>";
      $select_principal = $this->_db->select()
        ->from(array('A' => 'M_CUST_LINEFACILITY'), array("A.CUST_ID", "B.CUST_NAME"))
        ->join(['B' => 'M_CUSTOMER'], 'A.CUST_ID = B.CUST_ID')
        // ->where('A.CUST_SEGMENT = 4')
        ->query()->fetchAll();
      $save_principal = [];

      foreach ($select_principal as $key => $value) {
        $save_principal[$value['CUST_ID']] = $value["CUST_NAME"];
      }

      if (!empty($tblName)) {
        foreach ($save_principal as $key => $row) {
          if ($tblName == $row) {
            $select = 'selected';
          } else {
            $select = '';
          }
          $optHtml .= "<option value='" . $row . "' " . $select . ">" . $row . "</option>";
        }
      } else {
        foreach ($save_principal as $key => $row) {
          $optHtml .= "<option value='" . $key . "'>" . $row . "</option>";
        }
      }
      echo $optHtml;
    }

    if ($param == 'spobligee') {
      $tblName = $this->_getParam('id');
      $optHtml = "<option value=''>-- " . $this->language->_('Please Select') . " --</option>";
      $spobligee = [
        1 => 'Y',
        2 => 'T',
      ];
      if (!empty($tblName)) {
        foreach ($spobligee as $key => $row) {
          if ($key == 1 || $key == 2) {
            if ($tblName == $key) {
              $select = 'selected';
            } else {
              $select = '';
            }
            $optHtml .= "<option value='" . $key . "' " . $select . ">" . $row . "</option>";
          }
        }
      } else {
        foreach ($spobligee as $key => $row) {
          $optHtml .= "<option value='" . $key . "'>" . $row . "</option>";
        }
      }
      echo $optHtml;
    }

    if ($param == 'branch') {
      $tblName = $this->_getParam('id');
      $optHtml = "<option value=''>-- " . $this->language->_('Please Select') . " --</option>";
      $select_branch = $this->_db->select()
        ->from(array('A' => 'M_BRANCH'), array("BRANCH_CODE", "BRANCH_NAME"))
        ->order('A.BRANCH_NAME ASC')
        ->query()->fetchAll();
      $save_branch = [];

      foreach ($select_branch as $key => $value) {
        $save_branch[$value['BRANCH_CODE']] = $value["BRANCH_NAME"];
      }

      if (!empty($tblName)) {
        foreach ($save_branch as $key => $row) {
          if ($tblName == $row) {
            $select = 'selected';
          } else {
            $select = '';
          }
          $optHtml .= "<option value='" . $row . "' " . $select . ">" . $row . "</option>";
        }
      } else {
        foreach ($save_branch as $key => $row) {
          $optHtml .= "<option value='" . $key . "'>" . $row . "</option>";
        }
      }
      echo $optHtml;
    }

    if ($param == 'change_type') {
      $tblName = $this->_getParam('id');
      $optHtml = "<option value=''>-- " . $this->language->_('Please Select') . " --</option>";

      $conf = Zend_Registry::get('config');

      // change type
      $bgChangeTypeCode = $conf['bgclosing']['changetype']['code'];
      $bgChangeTypeDesc = $conf['bgclosing']['changetype']['desc'];
      $bgChangeType = $this->combineCodeAndDesc($bgChangeTypeCode, $bgChangeTypeDesc);

      $change_type = array_diff_key($bgChangeType, [4 => '']);

      foreach ($change_type as $key => $row) {
        if ($key == 1 || $key == 2 || $key == 3) {
          if ($tblName == $key) {
            $select = 'selected';
          } else {
            $select = '';
          }
          $optHtml .= "<option value='" . $key . "' " . $select . ">" . $row . "</option>";
        }
      }
      echo $optHtml;
    }
  }

  private function combineCodeAndDesc($arrayCode, $arrayDesc)
  {
    return array_combine($arrayCode, array_map(function ($arrayMap) use ($arrayDesc, $arrayCode) {
      return $arrayDesc[$arrayMap];
    }, array_keys($arrayCode)));
  }

  public function validatebtn($transfertype, $amount, $ccy, $psnumb)
	{
		//die;


		$selectuser	= $this->_db->select()
			->from(array('C' => 'M_APP_BGBOUNDARY'), array(
				'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
				'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
				'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
				'C.TRANSFER_TYPE',
				'C.POLICY'
			))

			->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
			->where("C.BOUNDARY_MIN 	<= ?", $amount)
			->where("C.BOUNDARY_MAX 	>= ?", $amount)
			->limit(1);


		//echo $selectuser;die;

		$datauser = $this->_db->fetchAll($selectuser);

		if (empty($datauser)) {

			return true;
		}

		$selectusergroup	= $this->_db->select()
			->from(array('C' => 'M_APP_GROUP_BUSER'), array(
				'*'
			))

			->where("C.BUSER_ID 	= ?", (string) $this->_userIdLogin);

		$usergroup = $this->_db->fetchAll($selectusergroup);

		$this->view->boundarydata = $datauser;
		//var_dump($usergroup);die;
		// print_r($this->view->boundarydata);die;
		if (!empty($usergroup)) {
			$cek = false;

			foreach ($usergroup as $key => $value) {

				$group = explode('_', $value['GROUP_BUSER_ID']);
				$alphabet = array(1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'E', 6 => 'F', 7 => 'G', 8 => 'H', 9 => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z');
				$groupalfa = $alphabet[(int) $group[2]];
				//print_r($groupalfa);echo '-';
				$usergroup[$key]['GROUP'] = $groupalfa;
				//var_dump($usergroup);
				// print_r($datauser);die;
				foreach ($datauser as $nub => $val) {
					$command = str_replace('(', '', $val['POLICY']);
					$command = str_replace(')', '', $command);
					$list = explode(' ', $command);

					//var_dump($list);die;
					foreach ($list as $row => $data) {

						if ($data == $groupalfa) {
							$cek = true;
							//die('ter');
							break;
						}
					}
				}
			}



			if ($group[0] == 'S') {
				return true;
			}
			// echo $cek;
			// print_r($cek);die;
			if (!$cek) {
				// die('here');
				return false;
			}
		} else {
			return false;
		}


		$tempusergroup = $usergroup;
		//echo $cek;die;
		if ($cek) {

			$command = ' ' . $datauser['0']['POLICY'] . ' ';
			$command = strtoupper($command);

			$cleanCommand = trim(preg_replace('/\s+/', ' ', $command));

			//transform to php logical operator syntak
			$translate = array(
				'AND' => '&&',
				'OR' => '||',
				'THEN' => 'THEN$',
				'A' => '$A',
				'B' => '$B',
				'C' => '$C',
				'D' => '$D',
				'E' => '$E',
				'F' => '$F',
				'G' => '$G',
				'H' => '$H',
				'I' => '$I',
				'J' => '$J',
				'K' => '$K',
				'L' => '$L',
				'M' => '$M',
				'N' => '$N',
				'O' => '$O',
				'P' => '$P',
				'Q' => '$Q',
				'R' => '$R',
				// 'S' => '$S',
				'T' => '$T',
				'U' => '$U',
				'V' => '$V',
				'W' => '$W',
				'X' => '$X',
				'Y' => '$Y',
				'Z' => '$Z',
				'SG' => '$SG',
			);

			$phpCommand =  strtr($cleanCommand, $translate);
			//var_dump($phpCommand);die;
			$param = array(
				'0' => '$A',
				'1' => '$B',
				'2' => '$C',
				'3' => '$D',
				'4' => '$E',
				'5' => '$F',
				'6' => '$G',
				'7' => '$H',
				'8' => '$I',
				'9' => '$J',
				'10' => '$K',
				'11' => '$L',
				'12' => '$M',
				'13' => '$N',
				'14' => '$O',
				'15' => '$P',
				'16' => '$Q',
				'17' => '$R',
				// '18' => '$S',
				'19' => '$T',
				'20' => '$U',
				'21' => '$V',
				'22' => '$W',
				'23' => '$X',
				'24' => '$Y',
				'25' => '$Z',
				'26' => '$SG',
			);
			// print_r($phpCommand);die;
			/*function str_replace_first($from, $to, $content, $row)
			{
				$from = '/' . preg_quote($from, '/') . '/';
				return preg_replace($from, $to, $content, $row);
			}*/

			$command = str_replace('(', ' ', $val['POLICY']);
			$command = str_replace(')', ' ', $command);
			$list = explode(' ', $command);

			// print_r($list);die;
			//var_dump($command);die;

			$thendata = explode('THEN', $command);

			//print_r($thendata);echo '<br>';die;
			$cthen = count($thendata);
			// print_r($thendata);die;
			$secondcommand = str_replace('(', '', trim($thendata[0]));
			$secondcommand = str_replace(')', '', $secondcommand);
			$secondcommand = str_replace('AND', '', $secondcommand);
			$secondcommand = str_replace('OR', '', $secondcommand);
			$secondlist = explode(' ', $secondcommand);

			// print_r($secondlist);die;
			// print_r($grouplist);die;
			// print_r($thendata[$i]);die;
			// return true;
			if (!empty($secondlist)) {
				foreach ($usergroup as $key => $value) {

					foreach ($secondlist as $row => $thenval) {


						// print_r($value['GROUP']);echo ' == ';echo $thenval;echo '<br/>';
						if (trim($value['GROUP']) == trim($thenval)) {
							$thengroup = true;
							$grouplist[] = trim($thenval);
							//die('here');
						}
					}
				}
			}

			//var_dump($cthen);die;
			if ($cthen >= 2) {
				foreach ($usergroup as $key => $value) {
					// print_r($value);
					foreach ($thendata as $row => $thenval) {
						// echo '|';print_r($thenval);echo '==';
						// print_r($value['GROUP']);echo '|';echo '<br/>';
						// $thengroup = true;
						$newsecondcommand = str_replace('(', '', trim($thenval));
						$newsecondcommand = str_replace(')', '', $newsecondcommand);
						$newsecondcommand = str_replace('AND', '', $newsecondcommand);
						$newsecondcommand = str_replace('OR', '', $newsecondcommand);
						$newsecondlist = explode(' ', $newsecondcommand);
						//var_dump($newsecondcommand);
						if (in_array(trim($value['GROUP']), $newsecondlist)) {
							//if (trim($value['GROUP']) == trim($thenval)) {
							$thengroup = true;
							$grouplist[] = trim($thenval);
							//die('here');
						}
					}
				}
			}

			//var_dump($grouplist);die;
			//var_dump($thengroup);die;
			// var_dump($thengroup);die;
			// // print_r($group);die;
			// // echo $thengroup;die;
			//echo '<pre>';
			//var_dump($thengroup);
			//var_dump($thendata);
			//print_r($thendata);echo '<br/>';die('here');


			if ($thengroup == true) {


				for ($i = 1; $i <= $cthen; ++$i) {
					$oriCommand = $phpCommand;
					//echo $oriCommand;die;
					$indno = $i;
					//echo $oriCommand;echo '<br>';

					for ($a = $cthen - $indno; $a >= 1; --$a) {


						if ($i > 1) {
							$replace = 'THEN$ $' . trim($thendata[$a + 1]);
						} else {
							$replace = 'THEN$ $' . trim($thendata[$a]);
						}

						$oriCommand = str_replace($replace, "", $oriCommand);
					}

					//print_r($thendata);echo '<br>';die();

					//die;
					// if($i == 3){
					// echo 'command : ';echo $oriCommand;echo '<br/>';
					// }
					//print_r($oriCommand);echo '<br>';
					//print_r($list);echo '<br>';die;


					$result = $this->generate($oriCommand, $list, $param, $psnumb, $groupalfa, $thengroup);

					// print_r($i);
					//var_dump($result);
					//echo 'result-';var_dump($result);echo '-';die;
					if ($result) {
						// die;
						// echo $thendata[$i+1];die('eere');
						// print_r($i);
						// print_r($thendata);die;



						$replace = 'THEN$ $' . trim($thendata[$i + 1]);
						// var_dump($replace);die;
						// print_r($i);
						if (!empty($thendata[$i + 1])) {
							//die;
							$oriCommand = str_replace($replace, "", $phpCommand);
						} else {
							// die;
							$thirdcommand = str_replace('(', '', trim($thendata[$i - 1]));
							$thirdcommand = str_replace(')', '', $thirdcommand);
							$thirdcommand = str_replace('AND', '', $thirdcommand);
							$thirdcommand = str_replace('OR', '', $thirdcommand);
							$thirdlist = explode(' ', $thirdcommand);
							//						var_dump($secondlist);
							//						var_dump($grouplist);
							//							die;

							if (!empty($secondlist)) {
								foreach ($grouplist as $key => $valg) {
									foreach ($secondlist as $row => $value) {
										if ($value == $valg) {
											//echo 'sini';

											return false;
										}
									}
								}
							}
							$oriCommand = $phpCommand;
						}
						// print_r($thendata[$i]);die;
						// if($i == 3){
						// echo $oriCommand;die;
						// echo '<br/>';	
						// }
						// print_r($i);
						//echo $oriCommand;
						$result = $this->generate($oriCommand, $list, $param, $psnumb, $groupalfa, $thengroup);
						//var_dump($result);echo '<br/>';
						if (!$result) {
							// die;
							//print_r($groupalfa);
							//print_r($thendata[$i]);

							if ($groupalfa == trim($thendata[$i])) {

								return true;
							} else {

								//return true;
							}
							/*
							foreach ($grouplist as $key => $valg) {
								 
								if (trim($valg) == trim($thendata[$i])) {
									// die;
									// print_r($i);
									// print_r($thendata);
									// print_r($thendata[$i]);die;
									if ($thendata[$i + 1] == $valg) {
										return true;
									} else {
										// die('here');
										return false;
									}
								}
							} */
						} else {

							//return false;

							// $result = $this->generate($phpCommand,$list,$param,$psnumb);
							// print_r($phpCommand);
							// if($result){}
							// die('here');
						}
						// var_dump($result);
						// die;


					} else {
						//die('here');
						$secondcommand = str_replace('(', '', trim($thendata[$i - 1]));
						$secondcommand = str_replace(')', '', $secondcommand);
						$secondcommand = str_replace('AND', '', $secondcommand);
						$secondcommand = str_replace('OR', '', $secondcommand);
						$secondlist = explode(' ', $secondcommand);

						//var_dump($i);
						// var_dump($thendata);
						//print_r($grouplist);
						//print_r($list);
						//die;
						//if($groupalfa == $gro)
						$approver = array();
						$countlist = array_count_values($list);

						foreach ($list as $key => $value) {

							if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {

								$selectapprover	= $this->_db->select()
									->from(array('C' => 'T_BGAPPROVAL'), array(
										'USER_ID'
									))

									// ->where("C.USER_ID 	= ?" , (string)$this->_userIdLogin)
									->where("C.REG_NUMBER = ?", (string) $psnumb)
									->where("C.GROUP = ?", (string) $value);
								//echo $selectapprover;die;
								$usergroup = $this->_db->fetchAll($selectapprover);

								// echo '<pre>';
								// var_dump($psnumb);
								// print_r($usergroup);
								$approver[$value] = $usergroup;

								if ($countlist[$value] == count($approver[$value]) && $tempusergroup['0']['GROUP'] == $value) {
									//var_dump($countlist[$value]);
									//var_dump($tempusergroup['0']['GROUP']);  
									//die('gere');
									return false;
								}
							}
						}
						// die;


						//$array = array(1, "hello", 1, "world", "hello");


						//echo '<pre>';
						//var_dump($list);
						//var_dump($approver);
						//die;
						//var_dump($secondlist[0]);die;
						//foreach($approver as $app => $valpp){


						if (!empty($thendata[$i])) {

							if (empty($approver[$secondlist[0]]) && $groupalfa != $secondlist['0']) {
								//die('gere');

								return false;
							}
						}

						//echo '<pre>';
						//var_dump($thendata);
						//var_dump($grouplist);
						//var_dump($approver);
						//var_dump($groupalfa);
						//die;
						foreach ($grouplist as $key => $vg) {
							$newgroupalpa = str_replace('AND', '', $vg);
							$newgroupalpa = str_replace('OR', '', $newgroupalpa);
							$groupsecondlist = explode(' ', $newgroupalpa);

							//var_dump($groupsecondlist);
							//var_dump($groupalfa);die;
							//&& empty($approver[$groupalfa])
							if (in_array($groupalfa, $groupsecondlist)) {
								//echo 'gere';die;
								return true;
								//die('ge');
							}

							if ($vg == $groupalfa  && count($approver[$groupalfa]) > 0) {
								// echo 'gere';die;
								return true;
								//die('ge');
							}
						}

						// var_dump($approver[$groupalfa]);
						// var_dump($grouplist);
						//var_dump($groupalfa);die;

						//echo 'ger';die;
						//print_r($secondlist);die;
						//	 return true;
						if (!empty($secondlist)) {
							foreach ($grouplist as $key => $valg) {
								foreach ($secondlist as $row => $value) {
									if ($value == $valg) {

										if (empty($thendata[1])) {
											//die;
											return true;
										}
										//else{
										//	return false;
										//	}
									}
								}
							}
						}

						//	echo 'here';die;
						$secondresult = $this->generate($thendata[$i - 1], $list, $param, $psnumb, $groupalfa, $thengroup);
						// var_dump($secondresult);
						//print_r($thendata[$i-1]);
						//die;
						//if()
						//	 echo '<pre>';
						//var_dump($grouplist);die;
						foreach ($grouplist as $key => $valgroup) {
							//print_r($valgroup);
							//var_dump($thendata[$i]); 


							if (trim($valg) == trim($thendata[$i])) {
								$cekgroup = false;
								//die('here');
								if ($secondresult) {

									return false;
								} else {
									//die;
									return true;
								}

								//break;
							}

							//else if (trim($valg) == trim($thendata[$i - 1])) {
							//		$cekgroup = false;
							// die('here');
							//	return false;
							//	}
						}
						//die;
						//if (!$cekgroup) {
						// die('here');
						//return false;
						//}
					}

					//		    echo '<br/>';
					//	${$command} = $oriCommand;
				}
			} else if (!empty($thendata) && $thengroup == false) {

				//var_dump($thendata);
				foreach ($thendata as $ky => $vlue) {
					$newsecondcommand = str_replace('(', '', trim($vlue));
					$newsecondcommand = str_replace(')', '', $newsecondcommand);
					$newsecondcommand = str_replace('AND', '', $newsecondcommand);
					$newsecondcommand = str_replace('OR', '', $newsecondcommand);
					$newsecondlist = explode(' ', $newsecondcommand);
					//echo $newsecondlist['0'];
					//echo $groupalfa;die;
					//var_dump($newsecondlist);
					if ($newsecondlist['0'] == $groupalfa) {
						return true;
					}
				}
				return false;
			}




			$approver = array();
			// print_r($list);die;  	
			foreach ($list as $key => $value) {
				if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
					$selectapprover	= $this->_db->select()
						->from(array('C' => 'T_BGAPPROVAL'), array(
							'USER_ID'
						))

						// ->where("C.USER_ID 	= ?" , (string)$this->_userIdLogin)
						->where("C.REG_NUMBER = ?", (string) $psnumb)
						->where("C.GROUP = ?", (string) $value);
					//	 echo $selectapprover;
					$usergroup = $this->_db->fetchAll($selectapprover);
					// print_r($usergroup);
					$approver[$value] = $usergroup;
				}
			}
			//die;




			// print_r($phpCommand);die;
			foreach ($param as $url) {
				if (strpos($phpCommand, $url) !== FALSE) {
					$ta = substr_count($phpCommand, $url);
					// print_r($list);die;

					if (!empty($approver)) {
						// print_r($approver);die;
						foreach ($list as $key => $value) {
							if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
								foreach ($approver[$value] as $row => $val) {
									// print_r($approver);die;
									if (!empty($val)) {
										$values = 'G' . $value;
										${$values}[$row + 1] = true;
										// print_r($B);
									}

									// print_r($val);
								}
							}
						}
					}

					for ($i = 1; $i <= $ta; $i++) {
						// print_r($list);die;
						foreach ($list as $key => $value) {
							if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
								$values = 'G' . $value;
								// print_r(${$values});
								if (empty(${$values}[$i])) {
									${$values}[$i] = false;
								}
								// if(${$value}[$i])
							}
						}
						// print_r($phpCommand);die;
						$BG_NUMBER = $i;
						$label = str_replace('$', '$G', $url);

						$replace = $label . '[' . $BG_NUMBER . ']';

						$alf = str_replace('$', '', $url);
						$values = 'G' . $alf;
						// print_r($values);die;
						if (${$values}[$i] == $replace) {
							$phpCommand = $this->str_replace_first($url, 'true', $phpCommand, 1);
							// print_r($phpCommand);
						} else {
							$phpCommand = $this->str_replace_first($url, 'false', $phpCommand, 1);
							// print_r($phpCommand);die;
						}
						// }
						// }

					}
					// print_r($GB);die;

				}
			}

			$keywords = preg_split("/[\s,]+/", $cleanCommand);
			$result =  false;
			$thendata = explode('THEN$', $phpCommand);
			//var_dump($thendata);die;
			if (!empty($thendata['1'])) {
				$phpCommand = '';
				foreach ($thendata as $tkey => $tval) {
					$phpCommand .= '(';
					$phpCommand .= $tval . ')';
					if (!empty($thendata[$tkey + 1])) {
						$phpCommand .= ' && ';
					}
				}
			} else {

				$phpCommand = str_replace('THEN$', '&&', $phpCommand);
			}
			//var_dump($phpCommand);
			if (!empty($phpCommand)) {
				eval('$result = ' . "$phpCommand;");
			} else {
				return false;
			}
			//var_dump($result);die;
			if (!$result) {

				return true;
			}
			//die('here2');
			//var_dump ($result);die;
			//return $result;
		} else {
			//die('here');
			return true;
		}
	}

  private function str_replace_first($from, $to, $content, $row)
	{
		$from = '/' . preg_quote($from, '/') . '/';
		return preg_replace($from, $to, $content, $row);
	}

  public function generate($command, $list, $param, $psnumb, $group, $thengroup)
	{

		$phpCommand = $command;

		// echo $command;die;

		$approver = array();

		$count_list = array_count_values($list);
		//print_r($count_list);
		foreach ($list as $key => $value) {
			if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
				$selectapprover	= $this->_db->select()
					->from(array('C' => 'T_BGAPPROVAL'), array(
						'USER_ID'
					))

					// ->where("C.USER_ID 	= ?" , (string)$this->_userIdLogin)
					->where("C.REG_NUMBER = ?", (string) $psnumb)
					->where("C.GROUP = ?", (string) $value);
				// echo $selectapprover;
				$usergroup = $this->_db->fetchAll($selectapprover);
				// print_r($usergroup);
				$approver[$value] = $usergroup;
			}
		}
		//var_dump($param);die;
		//var_dump($group);
		foreach ($approver as $appval) {
			$totaldata = count($approver[$group]);
			$totalgroup = $count_list[$group];
			//var_dump($totaldata);
			//var_dump($totalgroup);
			if ($totalgroup == $totaldata && $totalgroup != 0) {

				return false;
			}
		} //die;
		//die;





		foreach ($param as $url) {

			if (strpos($phpCommand, $url) !== FALSE) {
				$ta = substr_count($phpCommand, $url);
				// print_r($list);die;

				if (!empty($approver)) {
					// print_r($approver);die;
					foreach ($list as $key => $value) {
						if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
							foreach ($approver[$value] as $row => $val) {
								// print_r($approver);die;
								if (!empty($val)) {
									$values = 'G' . $value;
									${$values}[$row + 1] = true;
									// print_r($B);
								}

								// print_r($val);
							}
						}
					}
				}


				// print_r($approver);die;

				for ($i = 1; $i <= $ta; $i++) {

					foreach ($list as $key => $value) {
						if (!empty($value)) {
							if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
								$values = 'G' . $value;
								// print_r(${$values});
								if (empty(${$values}[$i])) {
									${$values}[$i] = false;
								}
								// if(${$value}[$i])
							}
						}
					}


					$BG_NUMBER = $i;
					$label = str_replace('$', '$G', $url);
					// print_r($phpCommand);die('here');
					$replace = $label . '[' . $BG_NUMBER . ']';

					$alf = str_replace('$', '', $url);
					$values = 'G' . $alf;

					if (${$values}[$i] == $replace) {
						$phpCommand = $this->str_replace_first($url, 'true', $phpCommand, 1);
						// print_r($phpCommand);
					} else {
						$phpCommand = $this->str_replace_first($url, 'false', $phpCommand, 1);
						// print_r($phpCommand);die;
					}
					// }
					// }

				}
				// print_r($GB);die;

			}
		}

		$keywords = preg_split("/[\s,]+/", $cleanCommand);
		$result =  false;
		$phpCommand = str_replace('THEN$', '&&', $phpCommand);
		//print_r($phpCommand);echo '<br/>';

		if (!empty($phpCommand)) {
			eval('$result = ' . "$phpCommand;");
			//var_dump($thengroup);
			// var_dump($result);

			if ($result) {
				//var_dump($thengroup);die;
				if ($thengroup) {
					return true;
				} else {
					return false;
				}
			} else {

				if ($thengroup) {
					return false;
				} else {

					return true;
				}
			}
			// return $result;
		} else {
			return false;
		}

		// var_dump ($result);die;




	}
}
