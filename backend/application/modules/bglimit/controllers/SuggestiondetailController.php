<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class bglimit_SuggestiondetailController extends customer_Model_Customer
{
  	protected $_moduleDB = 'CST';

	public function indexAction()
  	{
		
  		$this->_helper->layout()->setLayout('popup');
  		
  		$params = $this->_request->getParams();
  		$keyValue = null;
  		$excludeChangeId  = 'CHANGES_STATUS IN ('.$this->_db->quote($this->_changeStatus['code']['waitingApproval']).','.$this->_db->quote($this->_changeStatus['code']['repairRequested']).')';
  		$excludeChangeId .= ' AND '.$this->_db->quoteInto('CHANGES_FLAG = ?', $this->_applicationFlag);

  		if(is_array($params) && count($params)) $fullDesc = SGO_Helper_GeneralFunction::displayFullDesc($params);
  		else $fullDesc = null;

  	    $this->view->suggestionType = $this->_suggestType;
  		
  	    
  	    
  	    //cek apakah data changes id benar" WA atau RR, jika bukan maka lgsg redirect notification invalid
  	    $changes_id = strip_tags( trim($this->_getParam('changes_id')) );
  	      
	    $select = $this->_db->select()
		                     ->from(array('A' => 'T_GLOBAL_CHANGES'),array('*'))
		                     ->where("A.CHANGES_ID = ?",$changes_id)
		                     ->where("A.CHANGES_STATUS = 'WA' OR A.CHANGES_STATUS = 'RR'");
		$global_changes = $this->_db->fetchRow($select);


	
		 
		if(empty($global_changes))  $this->_redirect('/notification/invalid/index');
		//END cek apakah data changes id benar" WA atau RR, jika bukan maka lgsg redirect notification invalid
  	    
  	    
  		if(array_key_exists('changes_id', $params))
  		{  
	
  			$filters = array('changes_id' => array('StripTags', 'StringTrim'));
  			$validators = array(
  				'changes_id' => array(
  					'NotEmpty',
  					'Digits',
  					array('Db_RecordExists', array('table' => 'T_GLOBAL_CHANGES', 'field' => 'CHANGES_ID', 'exclude' => $excludeChangeId)),
					//array('Db_RecordExists', array('table' => 'TEMP_BANK_GUARANTEE', 'field' => 'CHANGES_ID')),
					'messages' => array(
						'Can not be empty',
						'Invalid format',
						'Change id is not already exists in Master Global Changes',
//						'Change id is not already exists in Temp Global Changes',
					                   ),
  				                    ),
  			                     );

  			
  			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $params, $this->_optionsValidator);
	  		if($zf_filter_input->isValid())
	  		{
				//die;
	  		    //$this->view->countryArr = Application_Helper_Array::listArray($this->getCountry(),'COUNTRY_CODE','COUNTRY_NAME');
	  		    $this->view->status_type = $this->_masterglobalstatus;
	  		    
	  			$changeId  = $zf_filter_input->changes_id;
	  			//$resultdata = $this->getTempCustomer($changeId);
				$resultdata  = $this->_db->fetchRow(
                     $this->_db->select()
                               ->from(array('T' => 'TEMP_CUSTOMER'))
                               ->join(array('G' => 'T_GLOBAL_CHANGES'), 'G.CHANGES_ID = T.CHANGES_ID', array('CHANGES_ID','CHANGES_TYPE','CREATED','CREATED_BY','CHANGES_STATUS','READ_STATUS'))
                               ->where('T.CHANGES_ID = ?', $changeId)
                               );
				
	  			$keyValue  = $resultdata['CUST_ID'];
	  			

				
				//var_dump($resultdata);die;
	  			//suggest data
	  			$this->view->changes_id     = $resultdata['CHANGES_ID'];
	  			$this->view->changes_type   = $resultdata['CHANGES_TYPE'];
	  			$this->view->changes_status = $resultdata['CHANGES_STATUS'];
	  			$this->view->read_status    = $resultdata['READ_STATUS'];
	  			$this->view->created        = $resultdata['CREATED'];
	  			$this->view->created_by     = $resultdata['CREATED_BY'];

	  			if($resultdata['CHANGES_TYPE'] == 'S'){
					$this->view->changes_name  = 'Suspend';
				 }elseif($resultdata['CHANGES_TYPE'] == 'U'){
					 $this->view->changes_name  = 'Unsuspend';
				 }elseif($resultdata['CHANGES_TYPE'] == 'L'){
					 $this->view->changes_name  = 'Delete';
				 }elseif($resultdata['CHANGES_TYPE'] == 'E'){
					$this->view->changes_name  = 'Edit';
				 }
	  			
 				//temp bank garante data
	  			$this->view->cust_id      = strtoupper($resultdata['CUST_ID']);
       			$this->view->cust_name    = $resultdata['CUST_NAME'];
       			$this->view->cust_bg_limit     = $resultdata['CUST_BG_LIMIT'];
				

				   $config    		= Zend_Registry::get('config');
				   $BgType 		= $config["bg"]["status"]["desc"];
				   $BgCode 		= $config["bg"]["status"]["code"];
		   
				   $arrStatus = array_combine(array_values($BgCode),array_values($BgType));

				
				$this->view->status  = $arrStatus[$resultdata['BG_STATUS']];
				


				$data = $resultdata;
					
				$this->view->data = $data;
				
				
				//$resultdata['CUST_FINANCE'];
            
       			
              	
				
				//echo $selectglobal;die;
				
				

				$setting = new Settings();
				$system_type = $setting->getSetting('system_type');
				$this->view->system_type = $system_type;

				$tblName = $resultdata['CUST_CIF'];
				$app = Zend_Registry::get('config');
				$app = $app['app']['bankcode'];
			// return $app;

			
				$m_resultdata  = $this->_db->fetchRow(
							$this->_db->select()
									->from(array('T' => 'M_CUSTOMER'))
									//->join(array('G' => 'T_GLOBAL_CHANGES'), 'G.CHANGES_ID = T.CHANGES_ID', array('CHANGES_ID','CHANGES_TYPE','CREATED','CREATED_BY','CHANGES_STATUS','READ_STATUS'))
									->where('T.CUST_ID = ?', $resultdata['CUST_ID'])
									);

				$keyValue  = $m_resultdata['CUST_ID'];

		
				//$this->view->cust_id      = strtoupper($m_resultdata['CUST_ID']);
       			$this->view->m_cust_name   = $m_resultdata['CUST_NAME'];
       			$this->view->m_cust_bg_limit     = $m_resultdata['CUST_BG_LIMIT'];

				   $config    		= Zend_Registry::get('config');
				   $BgType 		= $config["bg"]["status"]["desc"];
				   $BgCode 		= $config["bg"]["status"]["code"];
		   
				   $m_arrStatus = array_combine(array_values($BgCode),array_values($BgType));

				
   
				   
				$this->view->m_status  = $m_arrStatus[$m_resultdata['BG_STATUS']];

			    
      		    //master customer data
      		    $m_resultdata = $this->getCustomer(strtoupper($resultdata['CUST_ID']));
      		    
      		    $this->view->m_cust_id      = strtoupper($m_resultdata['CUST_ID']);
       			$this->view->m_cust_name    = $m_resultdata['CUST_NAME'];
       			
			
      			// Application_Helper_General::writeLog('CCCL','View customer changes list');
	  		}
	  		else
	  		{
	  			$errors = $zf_filter_input->getMessages();
	  			$errorRemark = SGO_Helper_GeneralFunction::displayErrorRemark($errors);
	  			Application_Helper_General::writeLog('CCCL','View customer changes list');

	  			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
	  			$this->_helper->getHelper('FlashMessenger')->addMessage($errors['changes_id']);
      			$this->_redirect('/popuperror/index/index');
	  		}
  		}
  		else
  		{
  			$errorRemark = 'Changes Id not found';
			//Application_Helper_General::writeLog('CCCL','');

	  		$this->_helper->getHelper('FlashMessenger')->addMessage('F');
	  		$this->_helper->getHelper('FlashMessenger')->addMessage($errorRemark);
      		$this->_redirect('/popuperror/index/index');
  		}
  		if(!$this->_request->isPost()){
  		Application_Helper_General::writeLog('CCCL','View customer changes list');
  		}
  		
  		
	}
}



