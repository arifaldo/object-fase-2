<?php
require_once 'Zend/Controller/Action.php';

class Activationcard_DebitaddController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$attahmentDestination 	= UPLOAD_PATH.'/document/temp';
		$adapter 				= new Zend_File_Transfer_Adapter_Http();
		
		$setting = new Settings();			  	
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
		$pw_hash = md5($enc_salt.$enc_pass);
		$rand = $this->_userIdLogin.date('dHis').$pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token; 
		$this->setbackURL('/'.$this->_request->getModuleName().'/index/index/');
		

		if($this->_request->isPost())
		{
			$params = $this->_request->getParams();
			//var_dump($attahmentDestination);die;
			$adapter->setDestination ($attahmentDestination);

			$extensionValidator = new Zend_Validate_File_Extension(array(false, 'txt'));
			$extensionValidator->setMessage(
				'Error: Extension file must be *.txt'
			);

			$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
			$sizeValidator->setMessage(
				'Error: File siz must not more than '.$this->getSetting('Fe_attachment_maxbyte')
			);
			
			
			
			//var_dump($params);
			$adapter->setValidators(array($extensionValidator,$sizeValidator));

			if($adapter->isValid())
			{

				$success = 1;
				$sourceFileName = $adapter->getFileName();
				$newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';
				$adapter->addFilter ( 'Rename',$newFileName);
				$adapter->receive();
				//Zend_Debug::dump($newFileName);die;
				$myFile = file_get_contents($newFileName);
				//var_dump(htmlspecialchars($myFile));
				$arry_myFile = explode("\n", $myFile);
				@unlink($newFileName);
				//Zend_Debug::dump($arry_myFile);die;
				if($adapter->receive())
				{
					
					
					unset($arry_myFile[0]);
					
					foreach ($arry_myFile as $ky => $vl)
					{
						$master = $this->_db->FetchRow(
						$this->_db->select()
							->from('T_DEBITCARD')
							->where('DEBIT_NUMBER = ?',trim($vl))
							
						);
						//var_dump($master);
						if(!empty($master)){
							$success = 0;
							$err = true;
							$errmsg = 'Debit Card already registered';
						}
						$arry_myFile[$ky] = trim($vl);
					}
					
				//	var_dump($err);die;
					if($err){
						$err = true;
						$errmsg = 'Debit Card already registered';
						$success = 0;
						$this->view->err = $err;
						$this->view->errmsg = $errmsg;
						//die;
						//$this->_redirect('/corporatedebitcard/debitadd/index/cust/'.$params['cust'].'/regid/'.$params['regid']);
					}else if(count(array_unique($arry_myFile))<count($arry_myFile))
						{
							$errmsg = 'Duplicate Debit Card';
							$err = true;
							$success = 0;
							$this->view->err = $err;
						$this->view->errmsg = $errmsg;
						//die;
						//	$this->_redirect('/corporatedebitcard/debitadd/index/cust/'.$params['cust'].'/regid/'.$params['regid']);
							
							// Array has duplicates
						}else{
						
					$this->_db->beginTransaction();
					$change_id = $this->suggestionWaitingApproval('Debit Card Activation',$info,$this->_changeType['code']['new'],null,'T_DEBITCARD','TEMP_DEBITCARD','-','-','-','-');
					//var_dump($arry_myFile);die;
					foreach ($arry_myFile as $row)
					{

						

						//$var['created'] = date('Y-m-d H:i:s');
						//$var['createdby'] = $this->_userIdLogin;
						/*
						try
						{
							$var = array();
							
								$checkno = false;
								  $acct_no = mt_rand(10000000,99999999);
								  $master = $this->_db->FetchRow(
									$this->_db->select()
										->from('T_DEBITCARD')
										->where('ACCT_NO = ?',trim($acct_no))	
									);
								  $tempmaster = $this->_db->FetchRow(
									$this->_db->select()
										->from('TEMP_DEBITCARD')
										->where('ACCT_NO = ?',trim($acct_no))	
									);
									//var_dump($master);
									//var_dump($tempmaster);die;
									if(empty($master) && empty($tempmaster)){
										$checkno = true;
									}
								  //echo "The number is: $x <br>";
								  //$x++;
								
							
							if( !empty($row) && $checkno)
							{
								
								
							
								$queueService = SGO_Extendedmodule_Queueservice_Service::getInstance();
                    
								$emailQueueProducer = $queueService->getQueueByProfileName("DEBITCARD_ACTIVATION");
								$insArr['card_no'] = $row;
								$insArr['acct_no'] = $acct_no;
								$insArr['user_id'] = $this->_userIdLogin;
								$datarab = json_encode($insArr);
								//var_dump($datarab);die;
								$emailQueueProducer->insertQueueItem($datarab);
							
//var_dump($row);die;		
/*					
								$request = array();
								$cust_type = 'CORPORATE';
								$request['header']['sender_id'] = 'D360';
								$request['header']['signature'] = '';
								$request['data']['customer_type'] = $cust_type;
								$request['data']['bank_code'] = '999';
								$request['data']['cif'] = trim($row);
								$request['data']['account_type'] = '10';
								$request['data']['account_number'] = trim($row);
								$request['data']['account_currency'] = '000';
								$request['data']['account_name'] = 'ESPAY';
								$request['data']['email'] = NULL;
								$request['data']['phone_number'] = NULL;
								$request['data']['birthdate'] = NULL;
								$request['data']['address'] = NULL;
								$request['data']['city'] = NULL;
								$request['data']['state'] = NULL;
								$clientUser  =  new SGO_Soap_ClientUser();
								
								$success = $clientUser->callapi('registeraccount',$request);
								$result  = $clientUser->getResult();
								
								//$var['CHANGES_ID'] = $change_id;
								//$var['CUST_ID'] = $cust_id;
								//$var['REG_NUMBER'] = $REG_NUMBER;
								$var['DEBIT_NUMBER'] = trim($row);
								$var['DEBIT_STATUS'] = '3';
								
								//$var['DEBIT_SUGGESTEDBY'] = $this->_userIdLogin;
								//$var['DEBIT_SUGGESTED'] = new Zend_Db_Expr('now()');
								$var['DEBIT_ACTIVATEDBY'] = $this->_userIdLogin;
								$var['DEBIT_ACTIVATED'] = new Zend_Db_Expr('now()');
								$var['IS_ACTIVATED'] = 1;
								
 								// echo "<pre>";
								// print_r($var);die;
								$this->_db->insert("T_DEBITCARD",$var);
								
								
								
								$requestva = array(
									'sender_id' => 'D360',
									'order_id' => trim($row),
									'remark1' => null,
									'remark2' => 'ESPAY',
									'remark3' => null
									
								);
								$success = $clientUser->callapi('generateva',$requestva);
								$result  = $clientUser->getResult();
								$resultArr = json_decode(json_encode($result), true);
								$vanumb = '';
								if(!empty($resultArr['va_list'])){
									foreach($resultArr['va_list'] as $ky => $vl){
										$vanumb = $vl['va_number'];
										$inpArr = array(
											'REG_NUMBER' => trim($row),
											'VA_NUMBER' => $vl['va_number'],
											'BANK_CODE' => $vl['bank_code'],
											'FEE' => $vl['fee']
										);
										$customerins = $this->_db->insert('T_VA_DEBIT',$inpArr);
									}
									
								}
								
								$requestactivation = array();
								$requestactivation['card_no'] = trim($row);
								$requestactivation['acct_no'] = $vanumb;
								$number = rand(0,99999999);
								$trace = str_pad($number,8,0,STR_PAD_LEFT);
								$requestactivation['trace'] = trim($trace);
								
								
								$success = $clientUser->callapi('activationcard',$requestactivation);
								$result  = $clientUser->getResult();
								
								$requestpin = array();
								$requestpin['card_no'] = trim($row);
								$first_pin = $setting->getSetting('first_pin');
								$requestpin['first_pin'] = trim($first_pin);
								$number = rand(0,99999999);
								$trace = str_pad($number,8,0,STR_PAD_LEFT);
								$requestpin['trace'] = trim($trace);
								
								
								$success = $clientUser->callapi('firstpin',$requestpin);
								$result  = $clientUser->getResult();
								*/
					/*			$success = 1;
							}
						}
						catch(Exception $e)
						{
							//var_dump($e);die;
							$success = 0;
						}*/

					

						try
						{
							$var = array();
							if( !empty($row) )
							{

								//$var['CHANGES_ID'] = $change_id;
								/*$var['CUST_ID'] =  $this->getCustId($this->_userIdLogin);
								$var['REG_NUMBER'] = $this->generatePaymentID(1);
								$var['DEBIT_NUMBER'] = trim($row);
								$var['DEBIT_STATUS'] = '3';
								
								//$var['DEBIT_SUGGESTEDBY'] = $this->_userIdLogin;
								//$var['DEBIT_SUGGESTED'] = new Zend_Db_Expr('now()');
								$var['DEBIT_ACTIVATEDBY'] = $this->_userIdLogin;
								$var['DEBIT_ACTIVATED'] = new Zend_Db_Expr('now()');
								$var['IS_ACTIVATED'] = 1;
								
 								// echo "<pre>";
								// print_r($var);die;
								$this->_db->insert("T_DEBITCARD",$var);
								//$this->_db->commit();

								$master = $this->_db->FetchRow(
								$this->_db->select()
									->from('T_DEBITCARD')
									->where('IS_ACTIVATED = ?','1')
									//->where('REG_NUMBER IS NULL')
									->where('DEBIT_NUMBER = ?',$var['DEBIT_NUMBER'])
									
								);*/
								$checkno = false;
								  $acct_no = mt_rand(10000000,99999999);
								  $master = $this->_db->FetchRow(
									$this->_db->select()
										->from('T_DEBITCARD')
										->where('ACCT_NO = ?',trim($acct_no))	
									);
								  $tempmaster = $this->_db->FetchRow(
									$this->_db->select()
										->from('TEMP_DEBITCARD')
										->where('ACCT_NO = ?',trim($acct_no))	
									);
									//var_dump($master);
									//var_dump($tempmaster);die;
									if(empty($master) && empty($tempmaster)){
										$checkno = true;
									}
								
								$var['CHANGES_ID'] = $change_id;
								$var['DEBIT_NUMBER'] = trim($row);
								$var['ACCT_NO'] = trim($acct_no);
								$var['DEBIT_STATUS'] = '3';
								$var['DEBIT_SUGGESTEDBY'] = $this->_userIdLogin;
								$var['DEBIT_SUGGESTED'] = new Zend_Db_Expr('now()');
								
 								 //echo "<pre>";
								 //print_r($var);die;
								 if($checkno){
								$this->_db->insert("TEMP_DEBITCARD",$var);
								 }
							}
						}
						catch(Exception $e)
						{
							//var_dump($e);die;
							$success = 0;
						}

					}
					if($success == 1)
						{
							$this->_db->commit();
							Application_Helper_General::writeLog('COIM','Import Debit Card List');
							$this->setbackURL('/'.$this->_request->getModuleName().'/index/index/');
							$this->_redirect('/notification/submited/index');
						}else{
							$this->_db->rollBack();
						}
						}
						
				}
				
			}
			else
			{
				$this->view->errorMsg = $adapter->getMessages();
				Zend_Debug::dump($adapter->getMessages());die;
			}
		}
		Application_Helper_General::writeLog('COIM','Import Debit Card List');
	}
	
	 public function downloadtrx2Action()
    {
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
		
      
      $changeId = $this->_getParam('reg_id');
      
      $datakey = array(
                  "0" => 'Debit Card Number'
                );
      $headerData[] = $datakey;
      //var_dump($changeId);
     $resultdata  = $this->_db->fetchAll(
                     $this->_db->select()
                               ->from(array('T' => 'TEMP_DEBITCARD'))
                               ->joinleft(array('C' => 'M_CUSTOMER'),'C.CUST_ID=T.CUST_ID',array('CUST_NAME'))
							   ->joinleft(array('D' => 'T_CUST_DEBIT'),'D.REG_NUMBER=T.REG_NUMBER',array('REG_NUMBER'))
                               ->join(array('G' => 'T_GLOBAL_CHANGES'), 'G.CHANGES_ID = T.CHANGES_ID', array('CHANGES_ID','CHANGES_TYPE','CREATED','CREATED_BY','CHANGES_STATUS','READ_STATUS'))
                               ->where('T.CHANGES_ID = ?', $changeId)
                               );

     
      foreach ($resultdata as $p => $pTrx)
      {

        

        $paramTrx = array(  
                  "0" => $pTrx['DEBIT_NUMBER']
                );

        $newData[] = $paramTrx;
        
      }
	//var_dump($newData);die;
      $this->_helper->download->txtBatch($headerData,$newData,null,$resultdata['0']['REG_NUMBER']);
      Application_Helper_General::writeLog('RPPY','Download TXT Document Upload Detail');  
      
    }

	public function generatePaymentID($forTransaction, $PS_PERIODIC = null)
	{
		$currentDate = date("Ymd");
		// $seqNumber	 = $this->getPaymentCounter($forTransaction);
		$seqNumber	 = strtoupper(Application_Helper_General::str_rand(9));
		$checkDigit  = '';
			$paymentID   = "CDC".$currentDate.$seqNumber.$checkDigit;
		
			
		return $paymentID;
	}

	public function getCustId($userID)
	{
		
		$resultdata  = $this->_db->fetchAll(
			$this->_db->select()
					  ->from(array('M' => 'M_USER'))
					  ->where('M.USER_ID = ?', $userID)
					  );
		//echo $userID;
		//echo "<pre>";
		//var_dump($resultdata);
		//die();
		/*foreach ($resultdata as $key => $row)
		{
			$custID = $row['CUST_ID'];
		}*/
		
		return $resultdata[0]['CUST_ID'];
	}
	
}