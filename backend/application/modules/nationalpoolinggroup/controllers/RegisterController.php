<?php
require_once 'Zend/Controller/Action.php';

require_once "Service/Account.php";
require_once 'General/Account.php';
class nationalpoolinggroup_RegisterController extends Application_Main 
{

    protected $_moduleDB = 'RTF'; // masih harus diganti
    
    public function indexAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->systemType = $system_type;

		$selectcomp = $this->_db->select()
                            ->from('M_CUSTOMER', array('value' => 'CUST_NAME','CUST_ID','CUST_TYPE'))
                            // ->where('CUST_NAME = ? ',$tblName)
                            ->where('CUST_STATUS = 1 ');

        $tempColumn = $this->_db->fetchAll($selectcomp);

        $this->view->custarr = json_encode($tempColumn); 

        if($this->_request->isPost())
		{	

			$params = $this->_request->getParams();
			// print_r($params);

			$selectcomp = $this->_db->select()
                             ->from(array('A' => 'M_CUSTOMER'),array('*'))
                           //  ->joinLeft(array('B' => 'M_COUNTRY'),'A.COUNTRY_CODE = B.COUNTRY_CODE',array('COUNTRY_NAME'))
                             ->where('A.CUST_ID = ?', $params['COMPANY_ID'])
							 
                             ->query()->fetchAll();

			//echo "<pre>";
			//var_dump($selectcomp[0]);die;
/*
			$adapter = new Zend_File_Transfer_Adapter_Http();
			$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';

			$adapter->setDestination ( $this->_destinationUploadDir );
			$extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv', 'pdf','docx'));
			$extensionValidator->setMessage(
				$this->language->_('Error').': '.$this->language->_('Extension file must be').' *.csv and *.pdf'
			);

			$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
			$sizeValidator->setMessage(
				'Error: File exceeds maximum size'
			);

			$adapter->setValidators ( array (
				$extensionValidator,
				$sizeValidator,
			));
			
			*/
			$attahmentDestination 	= UPLOAD_PATH.'/document/temp';		
				$adapternew 				= new Zend_File_Transfer_Adapter_Http();
				$adapternew->setDestination ($attahmentDestination);
				$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
				$sizeValidator->setMessage(
											$this->language->_('Error: File size must not more than ').$this->getSetting('Fe_attachment_maxbyte')
											);

				$adapternew->setValidators(array($sizeValidator));	
				$files = $adapternew->getFileInfo();
				
			$adapternew->setOptions(array('ignoreNoFile'=>true));
			
			$files = $adapternew->getFileInfo();
			// echo "test";die;
			if ($adapternew->isValid ())
			{
				// echo "valid";die; 
				//$sourceFileName = $adapter->getFileName ();
				if(!empty($adapternew->getFileName ()['uploadSource'])){
					$newFileNamedoc =  $adapternew->getFileName ()['uploadSource'];
				}else{
					$newFileNamedoc =  $adapternew->getFileName ();
				}
				
				//var_dump($adapternew->getFileName ());
			//	$newFileNamedoc = $adapter->getFileName ()['uploadDoc'] . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';
				 
			//	$adapter->addFilter ( 'Rename',$newFileName  );
				//var_dump($adapternew->getFileName ());
				
			//	$adapternew->isValid ();
				
				foreach($files as $key => $val){
				$i = 0;	
			//	echo '<pre>';
			//	var_dump($val);die;
				$sourceFileName = $val['name'];
				$newFileName =  $val['name'];
			//	$adapternew->addFilter ( 'Rename',$newFileName); 
				try{
				$resust = $adapternew->receive($val['name']);
				//Zend_Debug::dump($adapternew->getMessages());
			//	var_dump($resust);
				}catch(Exception $e) 
									{
													var_dump($e);die;
													
									}
				//die;
				//Zend_Debug::dump($val);die;
			//	$myFile = file_get_contents($newFileName);
			//	$csvData = $this->parseCSV($newFileName);
			//	 echo "<pre>";
			//		 var_dump($csvData);
				} 
				//die;
				if (true)
				{
				//var_dump($newFileNamedoc);
					$newFileName = file_get_contents($newFileNamedoc);
					// var_dump($newFileName);die;
					//PARSING CSV HERE
					$csvData = $this->parseCSV($newFileNamedoc);
					$csvData2 = $this->parseCSV($newFileNamedoc);
					 
					@unlink($newFileName);
					//end

					$totalRecords2 = count($csvData2);
					$totaldata = 0;
					if($totalRecords2)
						{
							for ($a= 3; $a<$totalRecords2; $a++ ){
							$totaldata++;
						}
					}
					unset($csvData[0]);
				//	 echo "<pre>";
					//var_dump($csvData);die; 
					
					if(!empty($csvData)){
						$credit = 0;
						$saving = 0;
						foreach($csvData as $key => $val){
							if(trim($val['1'])=='Y'){
								$credit++;
							}
							if(trim($val['1'])=='Y'){
								$saving++;
							}
						}
						
						if($saving > 1){
								$error = 'Saving Account cannot more than one account';
								$sessionNamespace = new Zend_Session_Namespace('nationalpooling');
								$sessionNamespace->error = $error;
								$this->_redirect('/nationalpoolinggroup/register/index/error/1');
							}
						
						if($creditAcct > 1){
								$error = 'Credit Account cannot more than one account';
								$sessionNamespace = new Zend_Session_Namespace('nationalpooling');
								$sessionNamespace->error = $error;
								$this->_redirect('/nationalpoolinggroup/register/index/error/1');
						}
						
						if($saving == 0){
							$error = 'Please select your Saving Account';
							$sessionNamespace = new Zend_Session_Namespace('nationalpooling');
							$sessionNamespace->error = $error;
							$this->_redirect('/nationalpoolinggroup/register/index/error/1');
						}
						
						if($credit == 0){
							$error = 'Please select your Credit Account';
							$sessionNamespace = new Zend_Session_Namespace('nationalpooling');
							$sessionNamespace->error = $error;
							$this->_redirect('/nationalpoolinggroup/register/index/error/1');
						}
						
						

						if(!empty($csvData)){
						
						
							$app = Zend_Registry::get('config');
							$app = $app['app']['bankcode'];
						   
						/*
							$core = array();
							$svcAccount = new Service_Account($selectcomp[0]['CUST_CIF'], Application_Helper_General::getCurrNum('IDR'), $app);
							$result   = $svcAccount->cifaccountInquiry($core);
						
						// echo '<pre>';
						// print_r($result);
						// print_r($core);die;
							if($core->responseCode == '0000' || $result['ResponseCode']=='0000' ){
							  $arrResult = array();
							  $err = array();

							  if(count($core->accountList) > 1){
					//            print_r($core->accountList);
								//array data lebih dari 1
								foreach ($core->accountList as $key => $val){
								  if($val->status == '0'){
									$arrResult = array_merge($arrResult, array($val->accountNo => $val));
									array_push($err, $val->accountNo);
								  }
					//              echo 'here';
								}
							  }
							}
							$error = false;
						foreach($csvData as $key => $val){	
							if (in_array($val['0'], $err)) {
								
							}else{
								$error = true;
								$error = 'Account is not registered in CIF account';
								$sessionNamespace = new Zend_Session_Namespace('nationalpooling');
								$sessionNamespace->error = $error;
								$this->_redirect('/nationalpoolinggroup/register/index/error/1');
							
							}
						}
						
						*/
						$error = false;
						//$result = array_unique($csvData);
				/*		if(!empty($result)){
								$error = true;
								$error = 'Cannot Duplicate Account';
								$sessionNamespace = new Zend_Session_Namespace('nationalpooling');
								$sessionNamespace->error = $error;
								$this->_redirect('/nationalpoolinggroup/register/index/error/1');
						}
					*/	
						foreach($csvData as $key => $val){
							 $ccy_id_num = Application_Helper_General::getCurrNum('IDR');
							 $account    = new Service_Account($val['0'],$ccy_id_num);
							 $result     = $account->accountInquiry();
							 
							 if($result['ResponseCode'] == '00'){
								$csvData[$key]['cif'] = $result['Cif'];
								$csvData[$key]['acctname'] = $result['AccountName'];
								$csvData[$key]['type'] = $result['ProductType'];
								$csvData[$key]['productPlan'] = $result['productPlan'];
								$csvData[$key]['planCode'] = $result['planCode'];
								
								
							 }else{
								$error = true;
								$error = 'Invalid Account';
								$sessionNamespace = new Zend_Session_Namespace('nationalpooling');
								$sessionNamespace->error = $error;
								$this->_redirect('/nationalpoolinggroup/register/index/error/1');
							 }
							 
						// var_dump($result);
						}
						// echo '<pre>';
					//	var_dump($csvData);die;
						if(!$error){
							
								if(!empty($files['uploadDoc'])){
										$insertArr = array(
												'CUST_ID'				=> 'BANK',
												'FILE_UPLOADEDBY'		=> $this->_userIdLogin,
												'FILE_TYPE'				=> '2',
												'FILE_NAME' 			=> $files['uploadDoc']['name'],
												'FILE_DESCRIPTION'		=> $params['doc_desc'],
												'FILE_UPLOADED_TIME' 	=> new Zend_Db_Expr('now()'),
												'FILE_DOWNLOADED'		=> '0',
												'FILE_DELETED'			=> '0',
												'FILE_DOWNLOADEDBY'		=> '',
												'FILE_DELETEDBY'		=> '',
												'FILE_ISEMAILED'		=> '0',
												'FILE_SYSNAME'			=> $files['uploadDoc']['name'],
												'FILE_MODULE'			=> NULL,
												'DOCUMENT_NUMBER'			=> $this->generateDocID()
										);
										//echo '<pre>';
										//var_dump($files);
										//var_dump($adapternew->getFileName ());
									//Zend_Debug::dump($insertArr);die;
										try{
										$this->_db->insert('T_FILE_SUBMIT', $insertArr);
										$selectid  = $this->_db->select()
											  ->from(array('T_FILE_SUBMIT'),array('FILE_ID'))
											  ->order('FILE_ID DESC')
											  ->limit(1)
											  ;
								
										
										$datalast = $this->_db->fetchAll($selectid);
										$params['doc_id'] = $datalast[0]['FILE_ID'];
										//var_dump($lastid);die;
										}
												catch(Exception $e)
												{
													var_dump($e);die;
												}
										}
							
						//	$change_id = $this->suggestionWaitingApproval('Notional Pooling',$info,$this->_changeType['code']['new'],null,'T_NOTIONAL_POOLING','TEMP_NOTIONAL_POOLING',$params['COMPANY_ID'],$params['COMPANY_NAME'],$params['COMPANY_ID'],$params['COMPANY_NAME']);
								$data = array(
											//	'CHANGES_ID' => $change_id,
											//	'N_NUMBER' => $this->generateTransactionID(),
												'N_CUST_ID' => $params['COMPANY_ID'],
												'N_CIF' => $selectcomp[0]['CUST_CIF'],
												'N_STATUS' => 1,
												'N_DESC' => $params['doc_desc'],
												'N_UD_FILE' => $files['uploadDoc']['name'],
												'N_UD_ID'	=> $params['doc_id'],
												
												'N_CREATED' => new Zend_Db_Expr('now()'),
												'N_CREATEDBY' => $this->_userIdLogin
								);
								//$insert = $this->_db->insert('TEMP_NOTIONAL_POOLING',$data);
								
								
								$csvDatanew = array();
								//var_dump($err);die;
								foreach($csvData as $key => $val){
									try 
									{	
										
										if($val[1]=='Y'){
											$cr = '1';
										}else{
											$cr = '0';
										}
										if($val[2]=='Y'){
											$sv = '1';
										}else{
											$sv =  '0';
										}
				
										 $select_product_type  = $this->_db->select()
											  ->from(array('M_PRODUCT_TYPE'),array('PRODUCT_NAME'))
											  ->where("PRODUCT_CODE = ?", $val['type'])
											  ->where("PRODUCT_PLAN = ?", $val['planCode']);
								
								
										$userData_product_type = $this->_db->fetchAll($select_product_type);

										$dataAcct = array(
														//'CHANGES_ID' => $change_id,
														//'N_NUMBER' => $data['N_NUMBER'],
														'N_SOURCE_ACCOUNT' => $val['0'],
														'N_CIF' => $val['cif'],
														'PRODUCT_TYPE' => $val['type'],
														'PRODUCT_PLAN' => $val['planCode'],
														'ACCT_DESC' => $userData_product_type[0]['PRODUCT_NAME'],
														'ACCT_NAME' => $val['acctname'],
														'N_IS_SAVING' => $sv,
														'N_IS_CREDIT' => $cr
												
										
										
										);
										$csvDatanew[$key][] = $dataAcct;
										//$insert = $this->_db->insert('TEMP_NOTIONAL_DETAIL',$dataAcct);
										// echo "<pre>";
										//var_dump($data);die;


									}
									catch(Exception $e) 
									{
													var_dump($e);die;
													
									}
								}
/*
								$rowUpdated = $this->_db->update(
									'T_GLOBAL_CHANGES',
									array(
										'CHANGES_STATUS' => 'WA',
										'CHANGES_INFORMATION' => 'Unread Suggestion',
										'LASTUPDATED' => new Zend_Db_Expr("now()"),
									),
									$this->_db->quoteInto('CHANGES_ID = ?', $data['CHANGES_ID'])
                    			);
								*/
								$sessionNameConfrim = new Zend_Session_Namespace('nationalpooling');
								$sessionNameConfrim->data = $params;
								$sessionNameConfrim->csvData = $csvDatanew;
								$sessionNameConfrim->datapool = $data;
								
								$this->setbackURL('/'.$this->_request->getModuleName().'/index');
								$this->_redirect('/nationalpoolinggroup/register/confirm');
						}
					}

					}else{
						$error = 'empty data';
						$sessionNamespace = new Zend_Session_Namespace('nationalpooling');
						$sessionNamespace->error = $error;
						$this->_redirect('/nationalpoolinggroup/register/index/error/1');

					}

						
				}else{
				$this->view->errorMsg = $adapter->getMessages();
				Zend_Debug::dump($adapter->getMessages());die;
				}
			}else{
				$this->view->errorMsg = $adapter->getMessages();
				Zend_Debug::dump($adapter->getMessages());die;
			}
			      

		}

    }
	
	 public function confirmAction()
    {
        $this->_helper->_layout->setLayout('newlayout');
 
        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->systemType = $system_type;
		$sessionNameConfrim = new Zend_Session_Namespace('nationalpooling');
		$this->view->data = $sessionNameConfrim->data;
		$this->view->csvData = $sessionNameConfrim->csvData;
		$this->view->datapool = $sessionNameConfrim->datapool;
		//echo '<pre>';
		//var_dump($sessionNameConfrim->csvData);die;
		if($this->_request->isPost()) {
						$change_id = $this->suggestionWaitingApproval('Notional Pooling',$info,$this->_changeType['code']['new'],null,'T_NOTIONAL_POOLING','TEMP_NOTIONAL_POOLING',$params['COMPANY_ID'],$params['COMPANY_NAME'],$params['COMPANY_ID'],$params['COMPANY_NAME']);
								$data = $sessionNameConfrim->datapool;
								$data['CHANGES_ID'] = $change_id;
								$data['N_NUMBER']  = $this->generateTransactionID();
								$insert = $this->_db->insert('TEMP_NOTIONAL_POOLING',$data);
								$csvData = $sessionNameConfrim->csvData;
								//echo '<pre>';
								foreach($csvData as $key => $val){
										//var_dump($val);
										$dataAcct = $val[0];
										$dataAcct['CHANGES_ID'] = $change_id;
										$dataAcct['N_NUMBER']  = $data['N_NUMBER'];
										//var_dump($dataAcct);die;
										$insert = $this->_db->insert('TEMP_NOTIONAL_DETAIL',$dataAcct);
								}
								$rowUpdated = $this->_db->update(
									'T_GLOBAL_CHANGES',
									array(
										'CHANGES_STATUS' => 'WA',
										'CHANGES_INFORMATION' => 'Unread Suggestion',
										'LASTUPDATED' => new Zend_Db_Expr("now()"),
									),
									$this->_db->quoteInto('CHANGES_ID = ?', $data['CHANGES_ID'])
                    			);
								$this->setbackURL('/'.$this->_request->getModuleName().'/index');
								$this->_redirect('/notification/submited/index');
								
		}		
												
	}

    private function parseCSV($fileName){
		$csvData = false;
		try {
				$Csv = new Application_Csv (  $fileName, $separator = "," );
				$csvData = $Csv->readAll ();
			} catch ( Exception $e ) {
				echo nl2br ( $e->getTraceAsString () );
			}
			return $csvData;
	}

	public function generateTransactionID(){

        $currentDate = date("Ymd");
        $seqNumber   = strtoupper(Application_Helper_General::str_rand(6));
        $trxId = 'NP'.$currentDate.$seqNumber;

        return $trxId;
    }
	
	public function generateDocID(){

        $currentDate = date("Ymd");
        $seqNumber   = strtoupper(Application_Helper_General::str_rand(6));
        $trxId = 'DOC'.$seqNumber;

        return $trxId;
    }
}