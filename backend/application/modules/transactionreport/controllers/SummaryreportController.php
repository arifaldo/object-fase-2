<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';

class transactionreport_SummaryreportController extends Application_Main 
{
	protected $_moduleDB = 'RTF';
	
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		
		try {
			$conf 		= Zend_Registry::get('config');
			// 		$host 		= $conf['db2']['params']['host'];
			// 		$username	= $conf['db2']['params']['username'];
			// 		$pass		= $conf['db2']['params']['password'];
			// 		$db			= $conf['db2']['params']['dbname'];
			// 		$port		= $conf['db2']['params']['port'];
		
			//stag config
			$db = Zend_Db_Table::getDefaultAdapter();
			$config = Zend_Registry::get('config');
		
			//protected $_db;
		$config = array(
							       'host'      => '192.168.86.23',
							       'username'  => 'ibsgolive',
							       'password'  => 'F9t%<HCj3L9tPZf?mv)]C#K',
							       'dbname'    => 'sgolivebankdev',
							       'port'  => '3306'
							     );
			$dbCon = new Zend_Db_Adapter_Pdo_Mysql($config);
		
			// 		$key = $conf['enc']['dec']['key'];
			// 		$iv = $conf['enc']['dec']['iv'];
		
			// 		$usernameval = Application_Helper_General::decrypt($key,$iv,$username);
			// 		$passval = Application_Helper_General::decrypt($key,$iv,$pass);
		
			// 		$config = array(
			// 				'host'      	=> $host,
			// 				'username'  	=> $usernameval,
			// 				'password'  	=> $passval,
			// 				'dbname'    	=> $db,
			// 				'port'  		=> $port
			// 		);
		
			// 		$dbCon = new Zend_Db_Adapter_Pdo_Mysql($config);
		}
		catch(Zend_Db_Exception $e) {
			echo $e->getMessage();
		}
		
		$arrPayType 	= Application_Helper_General::filterPaymentTypeRekon($this->_paymenttype, $this->_transfertype);
		$arrTraType 	= array_combine($this->_transfertype["code"],$this->_transfertype["desc"]);
		$arrTraStatus	= array_combine($this->_transferstatus["code"],$this->_transferstatus["desc"]);
		//Zend_Debug::dump($arrPayType);
		
		
		unset($arrPayType["6,4"]);
		unset($arrPayType["7,5"]);
		unset($arrPayType['8']);
		unset($arrPayType['1']);
		unset($arrPayType['2']);
// 		
		unset ($arrTraStatus['2']);
		unset ($arrTraStatus['0']);
		
		// 6,4 THEN 'Multi Credit'
		// 7,5 THEN 'Multi Debet'
		$paytypeall		= array_combine($this->_paymenttype["code"],$this->_paymenttype["desc"]);
		foreach($paytypeall as $key => $value){
			
			if($key != 3 && $key != 8 && $key != 9 && $key != 10){
			
				if($key == 4 
				// OR $key == 6
				) {
					$value = $paytypeall[6];
					$optpaytypeAll[$key] = $value;
				}
				else if($key == 5 
				// OR $key == 7
				) {
					$value = $paytypeall[7];
					$optpaytypeAll[$key] = $value;
				}
				else $optpaytypeAll[$key] = $value;
			}
			
		}
		
		
		foreach($arrPayType as $key => $value){
			if($key != 3){
				$optpaytypeRaw[$key] = $value;
			}
		}
		
		foreach($arrTraType as $key => $value){
			if($key <= 2 ){
				$arrTraTypeRaw[$key] = $value;
			}
		}
		
		/*
			ChargedStatus :
				Not Charged if tra_status = '4' -> (failed)
				Charged if tra_status = '3' -> (success)
		*/
		
		$arrChargedStatus = array('0'=>'Not Charged','1'=>'Charged');
		
		$listId = $this->_db->select()
						->from(array('M_CUSTOMER'),
							   array('CUST_ID','CUST_NAME'))
						->order('CUST_ID ASC')
						->query()->fetchAll();
		
       	$list = array(""=>'-- '.$this->language->_('Please Select'). " --");
		$list += Application_Helper_Array::listArray($listId,'CUST_ID','CUST_ID');
		
		$this->view->arrPayType 		= $optpaytypeRaw;
		$this->view->arrTraType 		= $arrTraTypeRaw;
		$this->view->arrTraStatus 		= $arrTraStatus;
		$this->view->arrChargedStatus 	= $arrChargedStatus;		
		$this->view->listCustId 		= $list;
		
		
		$amount = $this->language->_('Qty');
		$total = $this->language->_('Amount');
		
		$fields = array	(
							'providerName'  		=> array(
															'field' => 'PROVIDER_NAME',//'PS_BILLER_ID',
															'label' => $this->language->_('Product Name'),
															'sortable' => true
														),
							
							'amount'  			=> array(
															'field' => 'TOTAL',
															'label' => $amount,
															'sortable' => true
														),
							'ccy'				=> array(
															'field' => 'ccy',
															'label' => $ccy,
															'sortable' => true
														),
							'total'  			=> array(
																'field' => 'SUM_AMOUNT',
																'label' => $total,
																'sortable' => true
														),
						);
		
		$filterlist = array('PS_TRFDATE','PRODUCT_NAME');
		
		$this->view->filterlist = $filterlist;

		//validasi page, jika input page bukan angka               
		$page 			= $this->_getParam('page');
		$csv 			= $this->_getParam('csv');
		$pdf 			= $this->_getParam('pdf');
		$filter 		= $this->_getParam('filter');
		$clearfilter	= $this->_getParam('clearfilter');
		
		$this->view->filter			= $filter;
		$this->view->clearfilter	= $clearfilter;
			
		$page 	= (Zend_Validate::is($page,'Digits'))? $page : 1;
		
		//validasi sort, jika input sort bukan ASC atau DESC
		// $sortBy  = $this->_getParam('sortby');
		$sortBy  = ($this->_getParam('sortby')) ?  $this->_getParam('sortby') : ('updateddate');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'desc';
		
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$filterArr = array('COMP_ID'    	=> array('StripTags'),
	                       'COMP_NAME'  		=> array('StripTags','StringTrim','StringToUpper'),
						   
						   'PAYMENTREF' 		=> array('StripTags','StringTrim','StringToUpper'),
						   'TRANSACTIONID' 		=> array('StripTags','StringTrim','StringToUpper'),
						   'REFERENCENO' 		=> array('StripTags','StringTrim'),
						   'PS_TRFDATE' 		=> array('StripTags','StringTrim'),
						   'PS_TRFDATE_END' 		=> array('StripTags','StringTrim'),
						   
						   'UPDATEDFROM' 		=> array('StripTags','StringTrim'),
						   'UPDATEDTO' 			=> array('StripTags','StringTrim'),
						   
						   'CREATEDFROM' 		=> array('StripTags','StringTrim'),
						   'CREATEDTO' 			=> array('StripTags','StringTrim'),
						   
						   'ACCTSRC' 			=> array('StripTags','StringTrim','StringToUpper'),
						   'BENEACCT' 			=> array('StripTags','StringTrim','StringToUpper'),
						   'PRODUCT_NAME'		=> array('StripTags','StringTrim','StringToUpper'),						   
						   'PAYTYPE' 			=> array('StripTags'),
						   'TRANSFERTYPE' 		=> array('StripTags'),
						   'UUID' 				=> array('StripTags'),
						   'TRANSSTATUS' 		=> array('StripTags'),
						   'CHARGEDSTATUS' 		=> array('StripTags'),
	                      );
		
		// if POST value not null, get post, else get param
		$dataParam = array('PRODUCT_NAME');
		$dataParamValue = array();
		
		$clean2 = array_diff( $this->_request->getParam('wherecol'),$dataParam); 
		$dataParam = array_diff( $this->_request->getParam('wherecol'),$clean2); 
		// print_r($dataParam);die;

		// print_r($output);die;
		// print_r($this->_request->getParam('wherecol'));
		foreach ($dataParam as $no => $dtParam)
		{
		
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
				// print_r($dataval);
				$order = 0;
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($value == "PS_TRFDATE"){
								$order--;
							}
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$order];
						}
						$order++;
					}
				
			}
		}
		
		//echo "<pre>";
		//print_r ($dataParamValue);

		if(!empty($this->_request->getParam('trfdate'))){
				$transferarr = $this->_request->getParam('trfdate');
					$dataParamValue['PS_TRFDATE'] = $transferarr[0];
					$dataParamValue['PS_TRFDATE_END'] = $transferarr[1];
			}
		// print_r($dataParamValue);
		// The default is set so all fields allow an empty string		
		$options = array('allowEmpty' => true);
		$validators = array(
		
						'COMP_ID' 	=> array(array('InArray', array('haystack' => array_keys($list)))),
						'COMP_NAME' 	=> array(),	
						
						'PAYMENTREF' 	=> array(),
						'REFERENCENO' 	=> array(),
						'TRANSACTIONID' => array(),	
												
						'PS_TRFDATE' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'PS_TRFDATE_END'	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						
						'UPDATEDFROM' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'UPDATEDTO'		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						
						'CREATEDFROM' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'CREATEDTO'		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						
						'ACCTSRC' 		=> array(),	
						'BENEACCT' 		=> array(),	
						
						'PAYTYPE' 		=> array(array('InArray', array('haystack' => array_keys($optpaytypeRaw)))),							
						'TRANSFERTYPE' 	=> array(array('InArray', array('haystack' => array_keys($arrTraTypeRaw)))),
						'UUID' 			=> array(),
						'TRANSSTATUS' 	=> array(array('InArray', array('haystack' => array_keys($arrTraStatus)))),
						
						'CHARGEDSTATUS' => array(array('InArray', array('haystack' => array_keys($arrChargedStatus)))),
						'PRODUCT_NAME'	=> array(),	
						
						);
		
		$zf_filter  		= new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);
		
		$billerCharges = new systemlog_Model_Transactionreport();
		$serviceTypeArr 	 = $billerCharges->getServiceType();
		$this->view->serviceTypeArr = $serviceTypeArr;
		
		
		
		foreach($arrPayType as $key => $value){
			if($key != 3){
				$optpaytypeRaw[$key] = $this->language->_($value);
			}
		}
		
		$this->view->arrPayType 	= $optpaytypeRaw;
		
		$fCOMPANYCODE 		= html_entity_decode($zf_filter->getEscaped('COMP_ID'));
		$fCOMPANYNAME 		= html_entity_decode($zf_filter->getEscaped('COMP_NAME'));
		$fPAYMENTREF 		= html_entity_decode($zf_filter->getEscaped('PAYMENTREF'));
		$fREFERENCENO 		= html_entity_decode($zf_filter->getEscaped('REFERENCENO'));
		
		$fTRANSACTIONID		= html_entity_decode($zf_filter->getEscaped('TRANSACTIONID'));
		
		$fCREATEDFROM 		= html_entity_decode($zf_filter->getEscaped('CREATEDFROM'));
		$fCREATEDTO 		= html_entity_decode($zf_filter->getEscaped('CREATEDTO'));
		
		$fTRANSFERFROM 		= html_entity_decode($zf_filter->getEscaped('PS_TRFDATE'));
		$fTRANSFERTO 		= html_entity_decode($zf_filter->getEscaped('PS_TRFDATE_END'));
		
		$fACCTSRC 			= html_entity_decode($zf_filter->getEscaped('ACCTSRC'));
		$fBENEACCT 			= html_entity_decode($zf_filter->getEscaped('BENEACCT'));
		
		$fPAYTYPE 			= html_entity_decode($zf_filter->getEscaped('PAYTYPE'));
		$fTRANSFERTYPE 		= html_entity_decode($zf_filter->getEscaped('TRANSFERTYPE'));
		$fUUID 		= html_entity_decode($zf_filter->getEscaped('UUID'));
		$fTRANSSTATUS 		= html_entity_decode($zf_filter->getEscaped('TRANSSTATUS'));
		$fCHARGEDSTATUS 	= html_entity_decode($zf_filter->getEscaped('CHARGEDSTATUS'));
		$fPROVIDERNAME = html_entity_decode($zf_filter->getEscaped('PRODUCT_NAME'));
		
		
		if($filter == NULL && $clearfilter != 1){
			$fTRANSFERFROM 	= date("d/m/Y");
			$fTRANSFERTO 	= date("d/m/Y");
		}
		else{
		
			if($filter != NULL){
				$fTRANSFERFROM 		= html_entity_decode($zf_filter->getEscaped('PS_TRFDATE'));
				$fTRANSFERTO 		= html_entity_decode($zf_filter->getEscaped('PS_TRFDATE_END'));
			}
			else if($clearfilter == 1){
				$fTRANSFERFROM 	= "";
				$fTRANSFERTO 	= "";
			}
		}
		// print_r($fTRANSFERFROM);die;
		/*
			CONCAT(sb_pslip.acctsrc,' (',sb_pslip.accsrc_ccy,')',' / ', sb_pslip.accsrc_bankname,' / ',sb_pslip.accsrc_alias) AS fullaccfrom,
			CONCAT(sb_transaction.tra_accttgt,' (', sb_transaction.tra_accttgtcurr,')',' / ', sb_transaction.tra_accttgtbankname) AS fullaccto,
				
			IF (tra_alreadycharged=1,'Y','N') AS tra_statuscharged,
		
		*/
		
		$casePayType = "(CASE P.PS_TYPE ";
  		foreach($optpaytypeAll as $key => $val)	{ $casePayType .= " WHEN ".$key." THEN '".$val."'"; }
  		$casePayType .= " END)";
		
		$caseTraType = "(CASE T.TRANSFER_TYPE ";
  		foreach($arrTraTypeRaw as $key => $val)	{ $caseTraType .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseTraType .= " END)";
		
		$caseTraStatus = "(CASE T.TRA_STATUS ";
  		foreach($arrTraStatus as $key => $val)	{ $caseTraStatus .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseTraStatus .= " END)";
		
		$select = $this->_db->select()
							->from(		array(	'T'=>'T_TRANSACTION'),array())
							
							->joinLeft(	array(	'P'=>'T_PSLIP'),'T.PS_NUMBER = P.PS_NUMBER',
										array(	
												'ccy'			=>'P.PS_CCY',
												))
							->joinLeft(array(	'C'=>'M_CUSTOMER'),'P.CUST_ID = C.CUST_ID',array())
							->join(	array('B' => 'M_SERVICE_PROVIDER'), 'P.PS_BILLER_ID = B.PROVIDER_ID', array('B.PROVIDER_NAME','P.PS_BILLER_ID',
												'TOTAL' =>'COUNT(P.PS_NUMBER)',
												'SUM_AMOUNT' =>'SUM(T.TRA_AMOUNT)'
												
												))
							->group('P.PS_BILLER_ID');
							//->where(" T.TRA_STATUS = 3 oR T.TRA_STATUS = 4");
	
		if($fCOMPANYCODE)	{ $select->where("UPPER(C.CUST_ID) = ".$this->_db->quote($fCOMPANYCODE)); }
		if($fCOMPANYNAME)	{ $select->where("UPPER(C.CUST_NAME) LIKE ".$this->_db->quote('%'.$fCOMPANYNAME.'%')); }
		if($fPAYMENTREF) 	{ $select->where("UPPER(P.PS_NUMBER) LIKE ".$this->_db->quote('%'.$fPAYMENTREF.'%')); }
		if($fTRANSACTIONID) { $select->where("UPPER(T.TRANSACTION_ID) LIKE ".$this->_db->quote('%'.$fTRANSACTIONID.'%')); }
		if($fPROVIDERNAME) { $select->where("UPPER(B.PROVIDER_NAME) LIKE ".$this->_db->quote('%'.$fPROVIDERNAME.'%')); }


		if($filter == NULL && $clearfilter != 1){
			$fTRANSFERFROM 	= date("d/m/Y");
			$fTRANSFERTO 	= date("d/m/Y");
			$select->where('MONTH(P.PS_EFDATE) = MONTH(NOW()) AND YEAR(P.PS_EFDATE) = YEAR(NOW())');
			// $select->where('MONTH(P.PS_EFDATE) <= MONTH(NOW())');
		}else{
			if($fTRANSFERFROM){
			$FormatDate 	= new Zend_Date($fTRANSFERFROM, $this->_dateDisplayFormat);
			$transferfrom  	= $FormatDate->toString($this->_dateDBFormat);	
			$select->where('DATE(P.PS_EFDATE) >= ?', $transferfrom);
			}
			// echo $select;
			if($fTRANSFERTO){
				$FormatDate 	= new Zend_Date($fTRANSFERTO, $this->_dateDisplayFormat);
				$transferto  	= $FormatDate->toString($this->_dateDBFormat);	
				$select->where('DATE(P.PS_EFDATE) <= ?', $transferto);
			}	
		}
		// echo $select;
		
		
		if($fCREATEDFROM){
			$FormatDate 	= new Zend_Date($fCREATEDFROM, $this->_dateDisplayFormat);
			$createdfrom  	= $FormatDate->toString($this->_dateDBFormat);	
			$select->where('DATE(P.PS_CREATED) >= ?', $createdfrom);
		}
		if($fCREATEDTO){
			$FormatDate 	= new Zend_Date($fCREATEDTO, $this->_dateDisplayFormat);
			$createdto  	= $FormatDate->toString($this->_dateDBFormat);	
			$select->where('DATE(P.PS_CREATED) <= ?', $createdto);
		}
		
		if($fUPDATEDFROM){
			$FormatDate 	= new Zend_Date($fUPDATEDFROM, $this->_dateDisplayFormat);
			$updatedfrom  	= $FormatDate->toString($this->_dateDBFormat);	
			$select->where('DATE(P.PS_UPDATED) >= ?', $updatedfrom);
		}
		if($fUPDATEDTO){
			$FormatDate 	= new Zend_Date($fUPDATEDTO, $this->_dateDisplayFormat);
			$updatedto  	= $FormatDate->toString($this->_dateDBFormat);	
			$select->where('DATE(P.PS_UPDATED) <= ?', $updatedto);
		}
		
		if($fACCTSRC)	{ $select->where("T.SOURCE_ACCOUNT LIKE ".$this->_db->quote('%'.$fACCTSRC.'%')); }
		if($fBENEACCT)	{ $select->where("T.BENEFICIARY_ACCOUNT LIKE ".$this->_db->quote('%'.$fBENEACCT.'%')); }
		
		if($fPAYTYPE){ 
			$fPayType 	 	= explode(",", $fPAYTYPE);
			$select->where("P.PS_TYPE in (?) ", $fPayType);		
		}
		
		if($fTRANSFERTYPE != null)
		{
			$select->where("T.TRANSFER_TYPE = ".$this->_db->quote($fTRANSFERTYPE));
		}
		else if($this->_getParam('TRANSFERTYPE') != null)
		{ 
			if($this->_getParam('TRANSFERTYPE') == 0)
			{
				$fTRANSFERTYPE = 0;
				$select->where("T.TRANSFER_TYPE = ".$this->_db->quote($fTRANSFERTYPE));
			}
		}
		
		if($fTRANSSTATUS){ 
			$select->where("T.TRA_STATUS = ".$this->_db->quote($fTRANSSTATUS));
		}
		if($fCHARGEDSTATUS!=''){ 
			$select->where("T.TRANSFER_FEE_STATUS = ".$this->_db->quote($fCHARGEDSTATUS));
		}
		


		$select->order($sortBy.' '.$sortDir);
		// echo $select;
		$dataSQL 	= $this->_db->fetchAll($select);
		
// 		
		if(!empty($dataSQL)){
		foreach($dataSQL as $key =>$dt)
			{
				$biller = (!empty($dt["PS_BILLER_ID"])?$dt["PS_BILLER_ID"]:'');
					
					$select2 = $this->_db->select()
					->FROM(array('A' => 'M_SERVICE_PROVIDER'),
							array('A.PROVIDER_ID','A.PROVIDER_NAME','A.SERVICE_TYPE'))
							->JOIN (array('B' => 'M_SERVICES_TYPE'), 'B.SERVICE_ID = A.SERVICE_TYPE', array('B.SERVICE_NAME'));
					//						->WHERE('A.PROVIDER_ID = ? ', $provId);
					$select2->where('A.PROVIDER_ID = ? ', $biller);
						
					$resultTes = $this->_db->fetchAll($select2);
					
					
					
				$totalnumbertx = $totalnumbertx + $dt["SUM_AMOUNT"];
				$totalvaluetx  = $totalvaluetx + $dt["TRANSFER_FEE"];
			
			}
		}
		
// 		echo "<pre>";
// 				print_r($dataSQL);die;
		
		
		$dataCount 	= count($dataSQL); // utk validasi max row csv

		if($csv || $pdf || $this->_request->getParam('print')){	
		
			$header  = Application_Helper_Array::simpleArray($fields, "label"); 
			
			
			if(is_array($dataSQL) && count($dataSQL) > 0){
				foreach($dataSQL as $key => $val){
					
					if($val["ccy"] || $val["traamount"]){
						$dataSQL[$key]["ccy"] = $val["ccy"].' '.Application_Helper_General::displayMoney($val["traamount"]);
						// $dataSQL[$key]["ccy"] = "test";
					}
					if($val["beneacct"] || $val["benebankname"]){
						$dataSQL[$key]["beneacct"] = $val["beneacct"].' / '.$val["benebankname"];
					}
					
					unset($dataSQL[$key]["PS_BILLER_ID"]);
					// if($val["charged"] == 1){
						
						// $dataSQL[$key]["charged"] = 'Y';
						
						// if($val["charged"] == '1') 		$dataSQL[$key]["charged"] = 'Y';
						// else if($val["charged"] == '0') 	$dataSQL[$key]["charged"] = 'N';
						// else if($val["charged"] == NULL)	$dataSQL[$key]["charged"] = '-';
					
					// }
					// else if($val["charged"] == '0') 	$dataSQL[$key]["charged"] = 'N';
					// else if($val["charged"] == NULL)	$dataSQL[$key]["charged"] = '-';
					
					unset($dataSQL[$key]["traamount"]);	
					unset($dataSQL[$key]["benebankname"]);	
					
				}
			
			}
			else  $dataSQL = array();

			
			if($csv)
			{		
				$key_index = sizeof($dataSQL);
					$dataSQL[$key_index+1][0]	= 'Total';
					$dataSQL[$key_index+1][1]	= '';
					$dataSQL[$key_index+1][2]	= $totalnumbertx;
					
				
				$this->_helper->download->csv($header,$dataSQL,null,'Summary Report');  
				Application_Helper_General::writeLog('RPTX','Download CSV Summary report');
			}
			elseif($pdf)
			{
				$key_index = sizeof($dataSQL);
				$dataSQL[$key_index+1]['number'] = false;
				$dataSQL[$key_index+1][0]	= 'Total';
				$dataSQL[$key_index+1][1]	= '';
				$dataSQL[$key_index+1][2]	= $totalnumbertx;
				$this->_helper->download->pdf($header,$dataSQL,null,'Summary Report');  
				Application_Helper_General::writeLog('RPTX','Download PDF Summary report');
			}
			else if($this->_request->getParam('print') == 1){
				$data = $this->_db->fetchAll($select);

				if(is_array($data) && count($data) > 0){
					foreach($data as $key => $val){
						
						// if($val["ccy"] || $val["traamount"]){
							$data[$key]["SUM_AMOUNT"] = Application_Helper_General::displayMoney($val["SUM_AMOUNT"]);
						// }
						
						$key_index = $key;
					}
					
				}
				$data[$key_index+1]['PROVIDER_NAME']	= 'Total';
				$data[$key_index+1][1]	= '';
				$data[$key_index+1]['SUM_AMOUNT']	= $totalnumbertx;

				unset($fields['paymenttype']);
				unset($fields['transfertype']);
				unset($fields['transferstatus']);
				unset($fields['charged']);
				$this->_forward('print', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'Summary Report', 'data_header' => $fields));
			}
		}	
		else
		{		
			$this->paging($dataSQL);
// 			echo "<pre>";
// 			var_dump($dataSQL);die;
			$stringParam = array(
									'COMPANYCODE'	=>$fCOMPANYCODE,
									'COMPANYNAME'	=>$fCOMPANYNAME,
									'PAYMENTREF'	=>$fPAYMENTREF,
									'REFERENCENO'	=>$fREFERENCENO,
									'TRANSACTIONID'	=>$fTRANSACTIONID,
									
									'TRANSFERFROM'	=>$fTRANSFERFROM,
									'TRANSFERTO'	=>$fTRANSFERTO,
									
									'CREATEDFROM'	=>$fCREATEDFROM,
									'CREATEDTO'		=>$fCREATEDTO,
									
									'UPDATEDFROM'	=>$fUPDATEDFROM,
									'UPDATEDTO'		=>$fUPDATEDTO,
									
									'ACCTSRC'		=>$fACCTSRC,
									'BENEACCT'		=>$fBENEACCT,
									
									'PAYTYPE'		=>$fPAYTYPE,
									'TRANSFERTYPE'	=>$fTRANSFERTYPE,
									'UUID'			=>$fUUID,
									'TRANSSTATUS'	=>$fTRANSSTATUS,
									'CHARGEDSTATUS'	=>$fCHARGEDSTATUS,
									'clearfilter'	=> $clearfilter,
									'filter'		=> $filter,
								
								);
			
			$this->view->COMPANYCODE 	= $fCOMPANYCODE;
			$this->view->COMPANYNAME 	= $fCOMPANYNAME;
			$this->view->PAYMENTREF 	= $fPAYMENTREF;
			$this->view->REFERENCENO 	= $fREFERENCENO;
			$this->view->TRANSACTIONID 	= $fTRANSACTIONID;
			
			if($filter == NULL && $clearfilter != 1){

				$fTRANSFERFROM 	= date("1/m/Y");
				$fTRANSFERTO 	= date("d/m/Y");
				
					// echo $this->view->displayDateTimeFormat;die;
			$this->view->LTRANSFERFROM 	= Application_Helper_General::convertDate($fTRANSFERFROM,'dd MMM yyyy',$this->view->defaultDateFormat);
			$this->view->LTRANSFERTO 	= Application_Helper_General::convertDate($fTRANSFERTO,'dd MMM yyyy',$this->view->defaultDateFormat);
			
			}else{
	
				$this->view->LTRANSFERFROM 	= Application_Helper_General::convertDate($fTRANSFERFROM,'dd MMM yyyy',$this->view->defaultDateFormat);
				$this->view->LTRANSFERTO 	= Application_Helper_General::convertDate($fTRANSFERTO,'dd MMM yyyy',$this->view->defaultDateFormat);

			}

			$this->view->TRANSFERFROM 	= $fTRANSFERFROM;
			$this->view->TRANSFERTO 	= $fTRANSFERTO;
			
			$this->view->CREATEDFROM 	= $fCREATEDFROM;
			$this->view->CREATEDTO 		= $fCREATEDTO;
			
			$this->view->UPDATEDFROM 	= $fUPDATEDFROM;
			$this->view->UPDATEDTO 		= $fUPDATEDTO;
			
			$this->view->ACCTSRC 		= $fACCTSRC;
			$this->view->BENEACCT 		= $fBENEACCT;
			
			$this->view->PAYTYPE 		= $fPAYTYPE;
			$this->view->TRANSFERTYPE 	= $fTRANSFERTYPE;
			$this->view->UUID 			= $fUUID;
			$this->view->TRANSSTATUS 	= $fTRANSSTATUS;
			$this->view->CHARGEDSTATUS 	= $fCHARGEDSTATUS;
			
			unset($fields['total']);
			$this->view->fields = $fields;
			$this->view->filter = $filter;
			
			$this->view->arrPayType 		= $optpaytypeRaw;
			$this->view->arrTraType 		= $arrTraTypeRaw;
			$this->view->arrTraStatus 		= $arrTraStatus;
			$this->view->arrChargedStatus 	= $arrChargedStatus;		
			$this->view->listCustId 		= $list;
			$this->view->dataCount 			= $dataCount;
			$this->view->totalnumbertx 			= $totalnumbertx;
			
			
			Application_Helper_General::writeLog('RPTX','View Summary Report');

			if(!empty($dataParamValue)){
	    		$this->view->trfdateStart = $dataParamValue['PS_TRFDATE'];
	    		$this->view->trfdateEnd = $dataParamValue['PS_TRFDATE_END'];

			    unset($dataParamValue['PS_TRFDATE_END']);
	
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
		        $this->view->wherecol     = $wherecol;
		        $this->view->whereval     = $whereval;
		     // print_r($whereval);die;
		     }
			
		}		
		
	}
}
