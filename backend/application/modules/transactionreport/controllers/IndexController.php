<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';

class transactionreport_IndexController extends Application_Main 
{
	protected $_moduleDB = 'RTF';
	
	public function indexAction()
	{

		$setting = new Settings();			  	
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
		$pw_hash = md5($enc_salt.$enc_pass);
		$rand = $this->_userIdLogin.date('dHis').$pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token; 
		
		$this->_helper->layout()->setLayout('newlayout');
		$arrPayType 	= Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
		$arrTraType 	= array_combine($this->_transfertype["code"],$this->_transfertype["desc"]);
		$arrTraStatus	= array_combine($this->_transferstatus["code"],$this->_transferstatus["desc"]);
		
		unset ($arrTraStatus['2']);
		unset ($arrTraStatus['0']);
		
		// 6,4 THEN 'Multi Credit'
		// 7,5 THEN 'Multi Debet'
		$paytypeall		= array_combine($this->_paymenttype["code"],$this->_paymenttype["desc"]);
		foreach($paytypeall as $key => $value){
			
			if($key != 8 && $key != 9 && $key != 10){
			
				if($key == 4 
				// OR $key == 6
				) {
					$value = $paytypeall[6];
					$optpaytypeAll[$key] = $value;
				}
				else if($key == 5 
				// OR $key == 7
				) {
					$value = $paytypeall[7];
					$optpaytypeAll[$key] = $value;
				}
				else $optpaytypeAll[$key] = $value;
			}
			
		}
		
		
		foreach($arrPayType as $key => $value){
			//if($key != 3){
				$optpaytypeRaw[$key] = $value;
			//}
		}
		
		foreach($arrTraType as $key => $value){
			//if($key <= 2 ){
				$arrTraTypeRaw[$key] = $value;
			//}
		}
		
		/*
			ChargedStatus :
				Not Charged if tra_status = '4' -> (failed)
				Charged if tra_status = '3' -> (success)
		*/
		
		$arrChargedStatus = array('0'=>'Not Charged','1'=>'Charged');
		
		$listId = $this->_db->select()
						->from(array('M_CUSTOMER'),
							   array('CUST_ID','CUST_NAME'))
						->order('CUST_ID ASC')
						->query()->fetchAll();
		
       	$list = array(""=>'-- '.$this->language->_('Please Select'). " --");
		$list += Application_Helper_Array::listArray($listId,'CUST_ID','CUST_ID');
		
		$this->view->arrPayType 		= $optpaytypeRaw;
		$this->view->arrTraType 		= $arrTraTypeRaw;
		$this->view->arrTraStatus 		= $arrTraStatus;
		$this->view->arrChargedStatus 	= $arrChargedStatus;		
		$this->view->listCustId 		= $list;
		
		$fields = array	(
							'companycode'  		=> array(
															'field' => 'companycode',
															'label' => $this->language->_('Company'),
															'sortable' => true
														),
							'transactionid' 	=> array(
															'field' => 'transactionid',
															'label' => $this->language->_('Transaction Ref#').' / '.$this->language->_('Trace No#'),
															'sortable' => true
														),
							/*'companyname'  		=> array(
															'field' => 'companyname',
															'label' => $this->language->_('Company Name'),
															'sortable' => true
														),*/
							/*'payref'  			=> array(
															'field' => 'payref',
															'label' => $this->language->_('Payment Ref#'),
															'sortable' => true
														),*/
														
							/*'traceNo'  			=> array(
															'field' => 'traceNo',
															'label' => $this->language->_('Trace No'),
															'sortable' => true
														),*/
														
							/*'bankResponse'  			=> array(
															'field' => 'bankResponse',
															'label' => $this->language->_('Response'),
															'sortable' => true
														),
														
							'pssubject'  		=> array(
															'field' => 'pssubject',
															'label' => $this->language->_('Payment Subject'),
															'sortable' => true
														),
							'createddate'  		=> array(
															'field' => 'createddate',
															'label' => $this->language->_('Created Date'),
															'sortable' => true
														),	
							'updateddate'  		=> array(
															'field' => 'updateddate',
															'label' => $this->language->_('Updated Date'),
															'sortable' => true
														),	*/
							'transferdate'  	=> array(
															'field' => 'transferdate',
															'label' => $this->language->_('Payment Date'),
															'sortable' => true
														),
							'updateddate'  	=> array(
															'field' => 'updateddate',
															'label' => $this->language->_('Last Updated'),
															'sortable' => true
														),
							'acctsrc'  			=> array(
															'field' => 'acctsrc',
															'label' => $this->language->_('Source Account'),
															'sortable' => true
														),
							'beneacct'  			=> array(
															'field' => 'beneacct',
															'label' => $this->language->_('Beneficiary Account'),
															'sortable' => true
														),
							/*'bookrate'  			=> array(
															'field' => 'bookrate',
															'label' => $this->language->_('Book Rate'),
															'sortable' => true
														),*/
							'traamount'  		=> array(
															'field' => 'traamount',
															'label' => $this->language->_('Amount'),
															'sortable' => true
														),
							/*'tramessage'  		=> array(
															'field' => 'tramessage',
															'label' => $this->language->_('Message'),
															'sortable' => true
														),
							'addmessage'  		=> array(
															'field' => 'addmessage',
															'label' => $this->language->_('Additional Message'),
															'sortable' => true
														),*/
							'paymenttype'  		=> array(
															'field' => 'paymenttype',
															'label' => $this->language->_('Payment Type'),
															'sortable' => true
														),
							/*'transfertype'  	=> array(
															'field' => 'transfertype',
															'label' => $this->language->_('Transfer Type'),
															'sortable' => true
														),*/
							'transferstatus'  	=> array(
															'field' => 'transferstatus',
															'label' => $this->language->_('Payment Status'),
															'sortable' => true
														),
						);

		$filterlist = array('PS_TRFDATE','COMP_ID','PAYTYPE','TRFTYPE','TRANSSTATUS','COMP_NAME','PAY_REF','SOURCE_ACCOUNT','BENEFICIARY_ACCOUNT');
		
		$this->view->filterlist = $filterlist;
		
		//validasi page, jika input page bukan angka               
		$page 			= $this->_getParam('page');
		$csv 			= $this->_getParam('csv');
		$pdf 			= $this->_getParam('pdf');
		$filter 		= $this->_getParam('filter');
		$clearfilter	= $this->_getParam('clearfilter');
		
		$this->view->filter			= $filter;
		$this->view->clearfilter	= $clearfilter;
			
		$page 	= (Zend_Validate::is($page,'Digits'))? $page : 1;
		
		//validasi sort, jika input sort bukan ASC atau DESC
		// $sortBy  = $this->_getParam('sortby');
		$sortBy  = ($this->_getParam('sortby')) ?  $this->_getParam('sortby') : ('updateddate');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'desc';
		
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$filterArr = array('COMP_ID'    	=> array('StripTags'),
	                       'COMP_NAME'  		=> array('StripTags','StringTrim','StringToUpper'),
						   
						   'PAY_REF' 		=> array('StripTags','StringTrim','StringToUpper'),
						   'TRANSACTIONID' 		=> array('StripTags','StringTrim','StringToUpper'),
						   						   
						   'PS_TRFDATE' 		=> array('StripTags','StringTrim'),
						   'PS_TRFDATE_END' 		=> array('StripTags','StringTrim'),
						   
						   'PS_UPDATED' 		=> array('StripTags','StringTrim'),
						   'PS_UPDATED_END' 			=> array('StripTags','StringTrim'),
						   
						   'PS_CREATED' 		=> array('StripTags','StringTrim'),
						   'PS_CREATED_END' 			=> array('StripTags','StringTrim'),
						   
						   'SOURCE_ACCOUNT' 			=> array('StripTags','StringTrim','StringToUpper'),
						   'BENEFICIARY_ACCOUNT' 			=> array('StripTags','StringTrim','StringToUpper'),
						  						   
						   'PAYTYPE' 			=> array('StripTags'),
						   'TRFTYPE' 		=> array('StripTags'),
						   'TRANSSTATUS' 		=> array('StripTags'),
						   'CHARGEDSTATUS' 		=> array('StripTags'),
	                      );
		
		// if POST value not null, get post, else get param
		$dataParam = array('COMP_ID','COMP_NAME','PAY_REF','TRANSSTATUS','PAYTYPE','TRFTYPE','SOURCE_ACCOUNT','BENEFICIARY_ACCOUNT');
		$dataParamValue = array();
		
		$clean2 = array_diff( $this->_request->getParam('wherecol'),$dataParam); 
		$dataParam = array_diff( $this->_request->getParam('wherecol'),$clean2); 
		// print_r($dataParam);die;

		// print_r($output);die;
		// print_r($this->_request->getParam('wherecol'));
		foreach ($dataParam as $no => $dtParam)
		{
		
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
				// print_r($dataval);
				$order = 0;
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($value == "PS_CREATED"){
								$order--;
							}
						if($value == "PS_UPDATED"){
								$order--;
							}
						if($value == "PS_TRFDATE"){
								$order--;
							}
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$order];
						}
						$order++;
					}
				
			}
		}
		
		//echo "<pre>";
		//print_r ($dataParamValue);

		if(!empty($this->_request->getParam('createdate'))){
				$createarr = $this->_request->getParam('createndate');
					$dataParamValue['PS_CREATED'] = $createarr[0];
					$dataParamValue['PS_CREATED_END'] = $createarr[1];
			}

		if(!empty($this->_request->getParam('updatedate'))){
				$updatearr = $this->_request->getParam('updatedate');
					$dataParamValue['PS_UPDATED'] = $updatearr[0];
					$dataParamValue['PS_UPDATED_END'] = $updatearr[1];
			}

		if(!empty($this->_request->getParam('trfdate'))){
				$transferarr = $this->_request->getParam('trfdate');
					$dataParamValue['PS_TRFDATE'] = $transferarr[0];
					$dataParamValue['PS_TRFDATE_END'] = $transferarr[1];
			}
		
		// The default is set so all fields allow an empty string		
		$options = array('allowEmpty' => true);
		$validators = array(
		
						'COMP_ID' 	=> array(array('InArray', array('haystack' => array_keys($list)))),
						'COMP_NAME' 	=> array(),	
						
						'PAY_REF' 	=> array(),	
						'TRANSACTIONID' => array(),	
												
						'PS_TRFDATE' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'PS_TRFDATE_END'	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						
						'PS_UPDATED' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'PS_UPDATED_END'		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						
						'PS_CREATED' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'PS_CREATED_END'		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						
						'SOURCE_ACCOUNT' 		=> array(),	
						'BENEFICIARY_ACCOUNT' 		=> array(),	
						
						'PAYTYPE' 		=> array(array('InArray', array('haystack' => array_keys($optpaytypeRaw)))),							
						'TRFTYPE'=> array(array('InArray', array('haystack' => array_keys($arrTraTypeRaw)))),	
						'TRANSSTATUS' 	=> array(array('InArray', array('haystack' => array_keys($arrTraStatus)))),	
						'CHARGEDSTATUS' => array(array('InArray', array('haystack' => array_keys($arrChargedStatus)))),	
						
						);
		
		$zf_filter  		= new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);
		
		$fCOMPANYCODE 		= html_entity_decode($zf_filter->getEscaped('COMP_ID'));
		$fCOMPANYNAME 		= html_entity_decode($zf_filter->getEscaped('COMP_NAME'));
		$fPAYMENTREF 		= html_entity_decode($zf_filter->getEscaped('PAY_REF'));
		$fTRANSACTIONID		= html_entity_decode($zf_filter->getEscaped('TRANSACTIONID'));
		
		$fCREATEDFROM 		= html_entity_decode($zf_filter->getEscaped('PS_CREATED'));
		$fCREATEDTO 		= html_entity_decode($zf_filter->getEscaped('PS_CREATED_END'));
		
		$fTRANSFERFROM 		= html_entity_decode($zf_filter->getEscaped('PS_TRFDATE'));
		$fTRANSFERTO 		= html_entity_decode($zf_filter->getEscaped('PS_TRFDATE'));
		
		$fACCTSRC 			= html_entity_decode($zf_filter->getEscaped('SOURCE_ACCOUNT'));
		$fBENEACCT 			= html_entity_decode($zf_filter->getEscaped('BENEFICIARY_ACCOUNT'));
		
		$fPAYTYPE 			= html_entity_decode($zf_filter->getEscaped('PAYTYPE'));
		$fTRANSFERTYPE 		= html_entity_decode($zf_filter->getEscaped('TRFTYPE'));
		$fTRANSSTATUS 		= html_entity_decode($zf_filter->getEscaped('TRANSSTATUS'));
		$fCHARGEDSTATUS 	= html_entity_decode($zf_filter->getEscaped('CHARGEDSTATUS'));
		
		
		if($filter == NULL && $clearfilter != 1){
			$fUPDATEDFROM 	= date("d/m/Y");
			$fUPDATEDTO 	= date("d/m/Y");
		}
		else{
		
			if($filter != NULL){
				$fUPDATEDFROM 		= html_entity_decode($zf_filter->getEscaped('PS_UPDATED'));
				$fUPDATEDTO 		= html_entity_decode($zf_filter->getEscaped('PS_UPDATED_END'));
			}
			else if($clearfilter == 1){
				$fUPDATEDFROM 	= "";
				$fUPDATEDTO 	= "";
			}
		}
		
		/*
			CONCAT(sb_pslip.acctsrc,' (',sb_pslip.accsrc_ccy,')',' / ', sb_pslip.accsrc_bankname,' / ',sb_pslip.accsrc_alias) AS fullaccfrom,
			CONCAT(sb_transaction.tra_accttgt,' (', sb_transaction.tra_accttgtcurr,')',' / ', sb_transaction.tra_accttgtbankname) AS fullaccto,
				
			IF (tra_alreadycharged=1,'Y','N') AS tra_statuscharged,
		
		*/
		
		$casePayType = "(CASE P.PS_TYPE ";
  		foreach($optpaytypeAll as $key => $val)	{ $casePayType .= " WHEN ".$key." THEN '".$val."'"; }
  		$casePayType .= " END)";
		
		$caseTraType = "(CASE T.TRANSFER_TYPE ";
  		foreach($arrTraTypeRaw as $key => $val)	{ $caseTraType .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseTraType .= " END)";
		
		$caseTraStatus = "(CASE T.TRA_STATUS ";
  		foreach($arrTraStatus as $key => $val)	{ $caseTraStatus .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseTraStatus .= " END)";
		
		$select = $this->_db->select()
							->from(		array(	'T'=>'T_TRANSACTION'),array())
							->joinLeft(	array(	'P'=>'T_PSLIP'),'T.PS_NUMBER = P.PS_NUMBER',
										array(	
												'companycode'	=>'C.CUST_ID',
												'companyname'	=>'C.CUST_NAME',	
												'payref'		=>'P.PS_NUMBER',	
												'traceNo'		=>'T.TRACE_NO',	
												'bankResponse'	=>'T.BANK_RESPONSE',	
												'pssubject'		=>'P.PS_SUBJECT',	
												'createddate'	=>'P.PS_CREATED',	
												'updateddate'	=>'P.PS_UPDATED',	
												'transferdate'	=>'P.PS_EFDATE',
												'acctsrc'		=> new Zend_Db_Expr("CONCAT(
																				T.SOURCE_ACCOUNT , ' - ' , T.SOURCE_ACCOUNT_NAME )"),
												'beneacct'		=> new Zend_Db_Expr("CONCAT(
																				T.BENEFICIARY_ACCOUNT , ' - ' , T.BENEFICIARY_ACCOUNT_NAME )"),
												'benebankname'	=>'T.BENEFICIARY_ALIAS_NAME',
												'PSTYPE'		=>'P.PS_TYPE',
												'TRA_TYPE'		=>'T.TRANSFER_TYPE',
												'ccy'			=>'P.PS_CCY',	
												'traamount'		=>'T.TRA_AMOUNT',
												'sourceccy'		=>'T.SOURCE_ACCOUNT_CCY',
												'transactionid'	=>'T.TRANSACTION_ID',	
												'addmessage'	=>'T.TRA_ADDITIONAL_MESSAGE',
												'tramessage'	=>'T.TRA_MESSAGE',	
												'trarefno'		=>'T.TRA_REFNO',	
												'bookrate'		=>'T.BOOK_RATE',
												'bookbuy'		=>'T.BOOK_RATE_BUY',
												'transferfee'	=>'T.TRANSFER_FEE',
												'fullamountfee'	=>'T.FULL_AMOUNT_FEE',
												'provisionfee'	=>'T.PROVISION_FEE',
												'T.EQUIVALENT_AMOUNT_IDR',													
												'T.EQUIVALENT_AMOUNT_USD',
												'paymenttype'	=>$casePayType,	
												'transfertype'	=>$caseTraType,	
												'transferstatus'=>$caseTraStatus,
												 'charged'		=>'T.TRANSFER_FEE_STATUS',
												'T.TRA_REMAIN',
												
												))
							->joinLeft(array(	'C'=>'M_CUSTOMER'),'P.CUST_ID = C.CUST_ID',array())
							->order('P.PS_UPDATED DESC');
							//->where("T.TRA_STATUS IN (?)",array('1','3','4','9','13'));
	
		if($fCOMPANYCODE)	{ $select->where("UPPER(C.CUST_ID) = ".$this->_db->quote($fCOMPANYCODE)); }
		if($fCOMPANYNAME)	{ $select->where("UPPER(C.CUST_NAME) LIKE ".$this->_db->quote('%'.$fCOMPANYNAME.'%')); }
		if($fPAYMENTREF) 	{ $select->where("UPPER(P.PS_NUMBER) LIKE ".$this->_db->quote('%'.$fPAYMENTREF.'%')); }
		if($fTRANSACTIONID) { $select->where("UPPER(T.TRANSACTION_ID) LIKE ".$this->_db->quote('%'.$fTRANSACTIONID.'%')); }
		
		if($fTRANSFERFROM){
			$FormatDate 	= new Zend_Date($fTRANSFERFROM, $this->_dateDisplayFormat);
			$transferfrom  	= $FormatDate->toString($this->_dateDBFormat);	
			$select->where('DATE(P.PS_EFDATE) >= ?', $transferfrom);
		}
		if($fTRANSFERTO){
			$FormatDate 	= new Zend_Date($fTRANSFERTO, $this->_dateDisplayFormat);
			$transferto  	= $FormatDate->toString($this->_dateDBFormat);	
			$select->where('DATE(P.PS_EFDATE) <= ?', $transferto);
		}
		
		if($fCREATEDFROM){
			$FormatDate 	= new Zend_Date($fCREATEDFROM, $this->_dateDisplayFormat);
			$createdfrom  	= $FormatDate->toString($this->_dateDBFormat);	
			$select->where('DATE(P.PS_CREATED) >= ?', $createdfrom);
		}
		if($fCREATEDTO){
			$FormatDate 	= new Zend_Date($fCREATEDTO, $this->_dateDisplayFormat);
			$createdto  	= $FormatDate->toString($this->_dateDBFormat);	
			$select->where('DATE(P.PS_CREATED) <= ?', $createdto);
		}
		
		if($fUPDATEDFROM){
			$FormatDate 	= new Zend_Date($fUPDATEDFROM, $this->_dateDisplayFormat);
			$updatedfrom  	= $FormatDate->toString($this->_dateDBFormat);	
			$select->where('DATE(P.PS_UPDATED) >= ?', $updatedfrom);
		}
		if($fUPDATEDTO){
			$FormatDate 	= new Zend_Date($fUPDATEDTO, $this->_dateDisplayFormat);
			$updatedto  	= $FormatDate->toString($this->_dateDBFormat);	
			$select->where('DATE(P.PS_UPDATED) <= ?', $updatedto);
		}
		
		if($fACCTSRC)	{ $select->where("T.SOURCE_ACCOUNT LIKE ".$this->_db->quote('%'.$fACCTSRC.'%')); }
		if($fBENEACCT)	{ $select->where("T.BENEFICIARY_ACCOUNT LIKE ".$this->_db->quote('%'.$fBENEACCT.'%')); }
		
		if($fPAYTYPE){ 
			$fPayType 	 	= explode(",", $fPAYTYPE);
			$select->where("P.PS_TYPE in (?) ", $fPayType);		
		}
		
		if($fTRANSFERTYPE != null)
		{
			$select->where("T.TRANSFER_TYPE = ".$this->_db->quote($fTRANSFERTYPE));
		}
		else if($this->_getParam('TRANSFERTYPE') != null)
		{ 
			if($this->_getParam('TRANSFERTYPE') == 0)
			{
				$fTRANSFERTYPE = 0;
				$select->where("T.TRANSFER_TYPE = ".$this->_db->quote($fTRANSFERTYPE));
			}
		}
		
		if($fTRANSSTATUS){ 
			$select->where("T.TRA_STATUS = ".$this->_db->quote($fTRANSSTATUS));
		}
		else{
			$select->where("T.TRA_STATUS IN (3,4,9,13)");
		}
		
		
		if($fCHARGEDSTATUS!=''){ 
			$select->where("T.TRANSFER_FEE_STATUS = ".$this->_db->quote($fCHARGEDSTATUS));
		}
		
		$select->order($sortBy.' '.$sortDir);
		$select->limit(100);
		// echo $select;die;
		$dataSQL 	= $this->_db->fetchAll($select);
		$dataCount 	= count($dataSQL); // utk validasi max row csv

		if($csv || $pdf || $this->_request->getParam('print')){	
		
			$header  = Application_Helper_Array::simpleArray($fields, "label"); 
			
			
			if(is_array($dataSQL) && count($dataSQL) > 0){
				foreach($dataSQL as $key => $val){
					
					if($val["ccy"] || $val["traamount"]){
						$dataSQL[$key]["ccy"] = $val["ccy"].' '.Application_Helper_General::displayMoney($val["traamount"]);
						// $dataSQL[$key]["ccy"] = "test";
					}
					if($val["beneacct"] || $val["benebankname"]){
						$dataSQL[$key]["beneacct"] = $val["beneacct"].' / '.$val["benebankname"];
					}

					if($val['bookrate']){
						$dataSQL[$key]['bookrate'] = 'IDR '.Application_Helper_General::displayMoney($val['bookrate']);
					}
					// if($val["charged"] == 1){
						
						// $dataSQL[$key]["charged"] = 'Y';
						
						// if($val["charged"] == '1') 		$dataSQL[$key]["charged"] = 'Y';
						// else if($val["charged"] == '0') 	$dataSQL[$key]["charged"] = 'N';
						// else if($val["charged"] == NULL)	$dataSQL[$key]["charged"] = '-';
					
					// }
					// else if($val["charged"] == '0') 	$dataSQL[$key]["charged"] = 'N';
					// else if($val["charged"] == NULL)	$dataSQL[$key]["charged"] = '-';
					
					unset($dataSQL[$key]["traamount"]);	
					unset($dataSQL[$key]["benebankname"]);	
					
				}
			
			}
			else  $dataSQL = array();

			
			if($csv)
			{			
				// echo "<pre>";
				// print_r($dataSQL);
				// die;
				
				$this->_helper->download->csv($header,$dataSQL,null,'Transaction Report');  
				Application_Helper_General::writeLog('RPTX','Download CSV transaction report');
			}
			elseif($pdf)
			{
				$this->_helper->download->pdf($header,$dataSQL,null,'Transaction Report');  
				Application_Helper_General::writeLog('RPTX','Download PDF transaction report');
			}
			else if($this->_request->getParam('print') == 1){
				$data = $this->_db->fetchAll($select);
				if(is_array($data) && count($data) > 0){
					foreach($data as $key => $val){
						
						if($val["ccy"] || $val["traamount"]){
							$data[$key]["traamount"] = $val["ccy"].' '.Application_Helper_General::displayMoney($val["traamount"]);
						}
						if($val['bookrate']){
							$data[$key]['bookrate'] = 'IDR '.Application_Helper_General::displayMoney($val['bookrate']);
						}
					}
				}

				unset($fields['paymenttype']);
				unset($fields['transfertype']);
				unset($fields['transferstatus']);
				unset($fields['charged']);
				$this->_forward('print', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'Transaction Report', 'data_header' => $fields));
			}
		}	
		else
		{		
			//print_r($dataSQL);die;
			$this->paging($dataSQL);
			
			$stringParam = array(
									'COMP_ID'	=>$fCOMPANYCODE,
									'COMP_NAME'	=>$fCOMPANYNAME,
									'PAY_REF'	=>$fPAYMENTREF,
									'TRANSACTIONID'	=>$fTRANSACTIONID,
									
									'PS_TRFDATE'	=>$fTRANSFERFROM,
									'PS_TRFDATE_END'	=>$fTRANSFERTO,
									
									'PS_CREATED'	=>$fCREATEDFROM,
									'PS_CREATED_END'		=>$fCREATEDTO,
									
									'PS_UPDATED'	=>$fUPDATEDFROM,
									'PS_UPDATED_END'		=>$fUPDATEDTO,
									
									'SOURCE_ACCOUNT'		=>$fACCTSRC,
									'BENEFICIARY_ACCOUNT'		=>$fBENEACCT,
									
									'PAYTYPE'		=>$fPAYTYPE,
									'TRFTYPE'	=>$fTRANSFERTYPE,
									'TRANSSTATUS'	=>$fTRANSSTATUS,
									'CHARGEDSTATUS'	=>$fCHARGEDSTATUS,
									'clearfilter'	=> $clearfilter,
									'filter'		=> $filter,
								
								);
			
			$this->view->COMPANYCODE 	= $fCOMPANYCODE;
			$this->view->COMPANYNAME 	= $fCOMPANYNAME;
			$this->view->PAYMENTREF 	= $fPAYMENTREF;
			$this->view->TRANSACTIONID 	= $fTRANSACTIONID;
			
			$this->view->TRANSFERFROM 	= $fTRANSFERFROM;
			$this->view->TRANSFERTO 	= $fTRANSFERTO;
			
			$this->view->CREATEDFROM 	= $fCREATEDFROM;
			$this->view->CREATEDTO 		= $fCREATEDTO;
			
			$this->view->UPDATEDFROM 	= $fUPDATEDFROM;
			$this->view->UPDATEDTO 		= $fUPDATEDTO;
			
			$this->view->ACCTSRC 		= $fACCTSRC;
			$this->view->BENEACCT 		= $fBENEACCT;
			
			$this->view->PAYTYPE 		= $fPAYTYPE;
			$this->view->TRANSFERTYPE 	= $fTRANSFERTYPE;
			$this->view->TRANSSTATUS 	= $fTRANSSTATUS;
			$this->view->CHARGEDSTATUS 	= $fCHARGEDSTATUS;
			
			$this->view->fields = $fields;
			$this->view->filter = $filter;
			
			$this->view->arrPayType 		= $optpaytypeRaw;
			$this->view->arrTraType 		= $arrTraTypeRaw;
			$this->view->arrTraStatus 		= $arrTraStatus;
			$this->view->arrChargedStatus 	= $arrChargedStatus;		
			$this->view->listCustId 		= $list;
			$this->view->dataCount 			= $dataCount;
			
			Application_Helper_General::writeLog('RPTX','View Transaction Report');
			
		}		

		if(!empty($dataParamValue)){
	    		$this->view->createdStart = $dataParamValue['PS_CREATED'];
	    		$this->view->createdEnd = $dataParamValue['PS_CREATED_END'];
	    		$this->view->updatedStart = $dataParamValue['PS_UPDATED'];
	    		$this->view->updateEnd = $dataParamValue['PS_UPDATED_END'];
	    		$this->view->trfdateStart = $dataParamValue['PS_TRFDATE'];
	    		$this->view->trfdateEnd = $dataParamValue['PS_TRFDATE_END'];

	    	  	unset($dataParamValue['PS_CREATED_END']);
			    unset($dataParamValue['PS_TRFDATE_END']);
			    unset($dataParamValue['PS_UPDATED_END']);
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     // print_r($whereval);die;
      }
		
	}
}
