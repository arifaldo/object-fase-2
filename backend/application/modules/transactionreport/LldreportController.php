<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';

class transactionreport_LldreportController extends Application_Main 
{	
	public function indexAction()
	{
		
//		$data = $this->_db->select()->distinct()
//					->from(array('T_TRANSACTION'),array('SERVICE_NAME'))
////					->order('SERVICE_NAME ASC')
//					->where("SERVICE_NAME != ''")
//					->query()->fetchAll();
//
//    	$this->view->serviceTypeArr = $data;

		$model = new bankadminreport_Model_Bankadminreport();
		$arrCcyId    = $model->getCCYId();
		$this->view->arrCcyId 		= $arrCcyId;
		
		$billerCharges = new setbillercharges_Model_Setbillercharges();
    	$serviceTypeArr 	 = $billerCharges->getServiceType();
    	$this->view->serviceTypeArr = $serviceTypeArr;

		$getPayment = new transactionreport_Model_Transactionreport();
		$filterSend = array();
		
		//$arrPayType 	= array_combine($this->_paymenttype["code"],$this->_paymenttype["desc"]);
		$arrPayType 	= Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
		//Zend_Debug::dump($arrPayType);
		unset($arrPayType['8']);
		
		$arrPayStatus 	= array_combine($this->_paymentstatus["code"],$this->_paymentstatus["desc"]);
		$arrTraTypeRaw 	= array_combine($this->_transfertype["code"],$this->_transfertype["desc"]);
		
		$listUserArr = $this->getUserID();
		$listUser = array(''=>'--- '.$this->language->_('Any Value').' ---');
		$listUser = array_merge($listUser,Application_Helper_Array::listArray($listUserArr,'USER_ID','USER_ID'));
// 		var_dump($arrPayStatus);die;
// 		Zend_Debug::dump($arrPayStatus);
		unset($arrPayStatus[10]);
		unset($arrPayStatus[15]);
		unset($arrPayStatus[16]);
		unset($arrPayStatus[1]);
		unset($arrPayStatus[2]);
		unset($arrPayStatus[3]);
		
		
		foreach($arrPayType as $key => $value){
			if($key != 3){
				$optpaytypeRaw[$key] = $this->language->_($value);
			}
		}
		
		$this->view->arrPayType 	= $optpaytypeRaw;
		$this->view->arrPayStatus 	= $arrPayStatus;
		$this->view->arrTraType 	= $arrTraTypeRaw;
		$this->view->listUser 		= $listUser;
		
		$filterSend = array();
		
		$userid = $this->language->_('User ID');
		$username = $this->language->_('User Name');
		$paymentref = $this->language->_('Payment Ref');
		$beneficiaryaccount = $this->language->_('Beneficiary Account');
		$paymentref = $this->language->_('Payment Ref#');
		$trxid = $this->language->_('TRX ID');		
		$traceNo = $this->language->_('Trace No');
		$createddate = $this->language->_('Created Date');
		$beneficiarytype = $this->language->_('Beneficiary Type');
		$updateddate = $this->language->_('Updated Date');
		$paymentdate = $this->language->_('Payment Date');
		$sourceaccount = $this->language->_('Source Account');
		$paymenttype = $this->language->_('Payment Type');
		$paymentstatus = $this->language->_('Payment Status');
		$amount = $this->language->_('Currency / Amount');
		$fee = $this->language->_('Fee');
	
		$fields = array	(	
							'userlogin'  		=> array(
															'field' => 'U.USER_ID',
															'label' => $userid,
															'sortable' => true
														),
							'username'  		=> array(
															'field' => 'USER_FULLNAME',
															'label' => $username,
															'sortable' => true
														),
							'payref'  			=> array(
															'field' => 'PS_NUMBER',
															'label' => $paymentref,
															'sortable' => true
														),							
							'bankname'  		=> array(
															'field' => 'BANK_NAME',
															'label' => 'Bank Name',
															'sortable' => true
							),
							/*'nostrobank'  		=> array(
															'field' => 'NOSTRO_NAME',
															'label' => 'Nostro Bank',
															'sortable' => true
													),*/
								
							'transferdate'  	=> array(
															'field' => 'PS_EFDATE',
															'label' => $paymentdate,
															'sortable' => true
														),
							'acctsource'  			=> array(
															'field' => 'ACCT_SOURCE',
															'label' => $sourceaccount,
															'sortable' => true
														),
							'beneaccount'  			=> array(
															'field' => 'ACCT_BENE',
															'label' => $beneficiaryaccount,
															'sortable' => true
														),
							'amount'  			=> array(
														'field' => 'TRA_AMOUNTS',
														'label' => $amount,
														'sortable' => true
							),
						

							'bankResponse'  			=> array(
															'field' => 'TRA_MESSAGE',
															'label' => $this->language->_('Message'),
															'sortable' => true
														),
// 							'lldcon'  			=> array(
// 															'field' => 'LLD_CONTENT',
// 															'label' => $this->language->_('Message'),
// 															'sortable' => true
// 													),
						
																					
						);
		
		//validasi page, jika input page bukan angka               
		$page 			= $this->_getParam('page');
		$csv 			= $this->_getParam('csv');
		$pdf 			= $this->_getParam('pdf');
		$filter 		= $this->_getParam('filter');
		$filter_clear 		= $this->_getParam('filter_clear');
			
		$page 	= (Zend_Validate::is($page,'Digits'))? $page : 1;
		
		//validasi sort, jika input sort bukan ASC atau DESC
		// $sortBy  = $this->_getParam('sortby');
		$sortBy  = ($this->_getParam('sortby')) ?  $this->_getParam('sortby') : ('updateddate');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		if($sortBy=='U.USER_ID'){
			$sortBy = 'PS_CREATED';
		}
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'desc';
		
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$data = array();
		
		$filterArr = array('userid'    			=> array('StripTags'),
	                       'username'  			=> array('StripTags','StringTrim','StringToUpper'),
						   
						   'paymentreff' 		=> array('StripTags','StringTrim','StringToUpper'),
						   						   
						   'transferfrom' 		=> array('StripTags','StringTrim'),
						   'transferto' 		=> array('StripTags','StringTrim'),
						   'createdfrom' 		=> array('StripTags','StringTrim'),
						   'createdto' 			=> array('StripTags','StringTrim'),
						   'updatedfrom' 		=> array('StripTags','StringTrim'),
						   'updatedto' 			=> array('StripTags','StringTrim'),
						   
						   'acctsource' 		=> array('StripTags','StringTrim','StringToUpper'),
						   'beneaccount' 		=> array('StripTags','StringTrim','StringToUpper'),
						  						   
						   'paymentstatus' 		=> array('StripTags'),
						   'paymenttype' 		=> array('StripTags'),
						   'transfertype' 		=> array('StripTags'),
						   'providerName' 		=> array('StripTags','StringTrim','StringToUpper'),
				   		   'serviceCategory' 	=> array('StripTags'),
				   		   'traceNo' 	=> array('StripTags','StringTrim','StringToUpper'),
						   'CCYID' 	=> array('StripTags','StringTrim','StringToUpper'),
	                      );
		
		// if POST value not null, get post, else get param
		$dataParam = array(	"userid","username","paymentreff","transferfrom","transferto","createdfrom","createdto","updatedfrom",
							"updatedto","acctsource","beneaccount","paymentstatus","paymenttype","transfertype","providerName","serviceCategory","traceNo","CCYID");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{
			$dataPost = $this->_request->getPost($dtParam);
			$dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}
		
		// The default is set so all fields allow an empty string		
		$options = array('allowEmpty' => true);
		$validators = array(
		
						'userid' 		=> array(array('InArray', array('haystack' => array_keys($listUserArr)))),
						'username' 		=> array(),	
						
						'paymentreff' 	=> array(),	
												
						'transferfrom' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'transferto'	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						'createdfrom' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'createdto'		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						'updatedfrom' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'updatedto'		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						
						'acctsource' 	=> array(),	
						'beneaccount' 	=> array(),	
						
						'paymentstatus' => array(array('InArray', array('haystack' => array_keys($arrPayStatus)))),							
						'paymenttype' 	=> array(array('InArray', array('haystack' => array_keys($optpaytypeRaw)))),	
						'transfertype' 	=> array(array('InArray', array('haystack' => array_keys($arrTraTypeRaw)))),	
						'providerName' => array(),	
						'serviceCategory' 	=>array(),	
						'traceNo' 	=>array(),	
						'CCYID' 	=>array(),	
						);

		
		$zf_filter  	= new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);
		
		$fuserid 			= html_entity_decode($zf_filter->getEscaped('userid'));
		$fusername			= html_entity_decode($zf_filter->getEscaped('username'));
		$fpaymentreff 		= html_entity_decode($zf_filter->getEscaped('paymentreff'));
		
		$fcreatedfrom 		= html_entity_decode($zf_filter->getEscaped('createdfrom'));
		$fcreatedto 		= html_entity_decode($zf_filter->getEscaped('createdto'));
		
		$ftransferfrom 		= html_entity_decode($zf_filter->getEscaped('transferfrom'));
		$ftransferto 		= html_entity_decode($zf_filter->getEscaped('transferto'));
		
		$fupdatedfrom		= html_entity_decode($zf_filter->getEscaped('updatedfrom'));
		$fupdatedto			= html_entity_decode($zf_filter->getEscaped('updatedto'));
		
		$facctsource 		= html_entity_decode($zf_filter->getEscaped('acctsource'));
		$fbeneaccount 		= html_entity_decode($zf_filter->getEscaped('beneaccount'));
		$fpaymentstatus 	= html_entity_decode($zf_filter->getEscaped('paymentstatus'));
		$fpaymenttype 		= html_entity_decode($zf_filter->getEscaped('paymenttype'));
		$ftransfertype 		= html_entity_decode($zf_filter->getEscaped('transfertype'));
		$PROVIDER_NAME		= html_entity_decode($zf_filter->getEscaped('providerName'));
		$serviceCategory		= html_entity_decode($zf_filter->getEscaped('serviceCategory'));
		$traceNo			= html_entity_decode($zf_filter->getEscaped('traceNo'));
		$CCYID				= html_entity_decode($zf_filter->getEscaped('CCYID'));
		
		/*if($filter != 'Set Filter')
		{	
			$ftransferfrom = (date("d/m/Y"));
			$ftransferto = (date("d/m/Y"));
			$this->view->transferfrom  = $ftransferfrom;
			$this->view->transferto  = $ftransferto;
			
			$FormatDate 	= new Zend_Date($ftransferfrom, $this->_dateDisplayFormat);
			$transferfrom  	= $FormatDate->toString($this->_dateDBFormat);
			$filterSend['transferfrom'] = $transferfrom;
			
			$FormatDate 	= new Zend_Date($ftransferto, $this->_dateDisplayFormat);
			$transferto 	= $FormatDate->toString($this->_dateDBFormat);
			$filterSend['transferto'] = $transferto;
		}*/
		
		if($filter == null)
		{	
			$ftransferfrom = (date("d/m/Y"));
			$ftransferto = (date("d/m/Y"));
			$this->view->transferfrom  = $ftransferfrom;
			$this->view->transferto  = $ftransferto;
			
			$FormatDate 	= new Zend_Date($ftransferfrom, $this->_dateDisplayFormat);
			$transferfrom  	= $FormatDate->toString($this->_dateDBFormat);
			$filterSend['transferfrom'] = $transferfrom;
			
			$FormatDate 	= new Zend_Date($ftransferto, $this->_dateDisplayFormat);
			$transferto 	= $FormatDate->toString($this->_dateDBFormat);
			$filterSend['transferto'] = $transferto;
		} 
		
		//if($filter == 'Set Filter')
		if($filter == True)
		{
			if($fuserid!=null)
			{ 
				$filterSend['userid'] = $fuserid;
				$this->view->userid = $fuserid;
			}
			
			if($fusername!=null)
			{ 
				$filterSend['username'] = $fusername;
				$this->view->username = $fusername;
			}
			
			if($fpaymentreff!=null)
			{ 
				$filterSend['paymentreff'] = $fpaymentreff;
				$this->view->paymentreff = $fpaymentreff;
			}
			
			if($ftransferfrom!=null)
			{
				$this->view->transferfrom = $ftransferfrom;
				$FormatDate 	= new Zend_Date($ftransferfrom, $this->_dateDisplayFormat);
				$transferfrom  	= $FormatDate->toString($this->_dateDBFormat);	
				$filterSend['transferfrom'] = $transferfrom;
			}
			
			if($ftransferto!=null)
			{
				$this->view->transferto = $ftransferto;
				$FormatDate 	= new Zend_Date($ftransferto, $this->_dateDisplayFormat);
				$transferto  	= $FormatDate->toString($this->_dateDBFormat);	
				$filterSend['transferto'] = $transferto;
			}
			
			if($fcreatedfrom!=null)
			{
				$this->view->createdfrom = $fcreatedfrom;
				$FormatDate 	= new Zend_Date($fcreatedfrom, $this->_dateDisplayFormat);
				$fcreatedfrom  	= $FormatDate->toString($this->_dateDBFormat);
				$filterSend['createdfrom'] = $fcreatedfrom;
				
			}
			
			if($fcreatedto!=null)
			{
				$this->view->createdto = $fcreatedto;
				$FormatDate 	= new Zend_Date($fcreatedto, $this->_dateDisplayFormat);
				$fcreatedto  	= $FormatDate->toString($this->_dateDBFormat);
				$filterSend['createdto'] = $fcreatedto;
				
			}
			
			if($fupdatedfrom!=null)
			{
				$this->view->updatedfrom = $fupdatedfrom;
				$FormatDate 	= new Zend_Date($fupdatedfrom, $this->_dateDisplayFormat);
				$fupdatedfrom  	= $FormatDate->toString($this->_dateDBFormat);
				$filterSend['updatedfrom'] = $fupdatedfrom;
				
			}
			
			if($fupdatedto!=null)
			{
				$this->view->updatedto = $fupdatedto;
				$FormatDate = new Zend_Date($fupdatedto, $this->_dateDisplayFormat);
				$fupdatedto  = $FormatDate->toString($this->_dateDBFormat);
				$filterSend['updatedto'] = $fupdatedto;
				
			}
			
			if($facctsource!=null)
			{ 
				$filterSend['acctsource'] = $facctsource; 
				$this->view->acctsource = $facctsource;
			}
			
			if($fbeneaccount!=null)	
			{ 
				$filterSend['beneaccount'] = $fbeneaccount;
				$this->view->beneaccount = $fbeneaccount;
			}
			
			if($fpaymentstatus!=null)
			{ 
				$filterSend['paymentstatus'] = $fpaymentstatus;
				$this->view->paymentstatus = $fpaymentstatus;		
			}
			
			if($fpaymenttype!=null)
			{ 
				$filterSend['paymenttype'] = $fpaymenttype;
				$this->view->paymenttype = $fpaymenttype;
			}
			
			if($ftransfertype != null)
			{
				$filterSend['transfertype'] = $ftransfertype;
				$this->view->transfertype = $ftransfertype;
			}
			
			if($PROVIDER_NAME!=null)
			{ 
				$filterSend['providerName'] = $PROVIDER_NAME;
				$this->view->providerName = $PROVIDER_NAME;
			}
			if($serviceCategory!=null)
			{ 
				$filterSend['serviceCategory'] = $serviceCategory;
				$this->view->serviceCategory = $serviceCategory;
			}
			if($traceNo!=null)
			{ 
				$filterSend['traceNo'] = $traceNo;
				$this->view->traceNo = $traceNo;
			}
			if($CCYID!=null)
			{ 
				$filterSend['CCYID'] = $CCYID;
				$this->view->CCYID = $CCYID;
			}
		}
		
		
		else if($filter_clear == TRUE){
			// $ftransferfrom = '00/00/0000';
			// $ftransferto = (date("d/m/Y"));
			// $this->view->transferfrom  = '';
			// $this->view->transferto  = '';
			
			// $FormatDate 	= new Zend_Date($ftransferfrom, $this->_dateDisplayFormat);
			// $transferfrom  	= $FormatDate->toString($this->_dateDBFormat);
			// $filterSend['transferfrom'] = $transferfrom;
			
			// $FormatDate 	= new Zend_Date($ftransferto, $this->_dateDisplayFormat);
			// $transferto 	= $FormatDate->toString($this->_dateDBFormat);
			// $filterSend['transferto'] = $transferto;
			
			$ftransferfrom = (date("d/m/Y"));
			$ftransferto = (date("d/m/Y"));
			$this->view->transferfrom  = $ftransferfrom;
			$this->view->transferto  = $ftransferto;
			
			$FormatDate 	= new Zend_Date($ftransferfrom, $this->_dateDisplayFormat);
			$transferfrom  	= $FormatDate->toString($this->_dateDBFormat);
			$filterSend['transferfrom'] = $transferfrom;
			
			$FormatDate 	= new Zend_Date($ftransferto, $this->_dateDisplayFormat);
			$transferto 	= $FormatDate->toString($this->_dateDBFormat);
			$filterSend['transferto'] = $transferto;
			
		}
		
		$sorting = $sortBy.' '.$sortDir;
		$result = $getPayment -> getPayment($filterSend,$sorting);


		//summary total
		$totalnumbertx = 0;
		$totalvaluetx = 0;
		foreach($result as $key =>$dt)
		{

			$totalnumbertx = $totalnumbertx + $dt["TRA_AMOUNT"];
			$totalvaluetx  = $totalvaluetx + $dt["TRANSFER_FEE"];

		}

		@$avgnumbertx 	= round($totalnumbertx / count($result),2);
		@$avgvaluetx 	= round($totalvaluetx / count($result),2);
		
		
		//count totalhit and average:
		$totalvaluetx 	= Application_Helper_General::displayMoney($totalvaluetx);
		$totalnumbertx 	= Application_Helper_General::displayMoney($totalnumbertx);
		
		$this->view->totalnumbertx 	= $totalnumbertx;
		$this->view->totalvaluetx 	= $totalvaluetx;
		
		foreach($result as $key=>$value)
		{
			if($value['TRANSFER_FEE']==''){
				$result[$key]["TRANSFER_FEE"] = '0.00';
			}
			$result[$key]["LOG_DATE"] = Application_Helper_General::convertDate($value["LOG_DATE"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
		}
			
		if ($csv || $pdf) 
		{
				$arr_print = $result;

				$sort_cetak = $this->_getParam('sortby');
				$sortDir_cetak = $this->_getParam('sortdir');
				
				$sortDir_cetak = (Zend_Validate::is($sortDir_cetak,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir_cetak : 'desc';
				$sorting_cetak = $sort_cetak.' '.$sortDir_cetak;
				$arr_cetak = $getPayment ->getPayment($filterSend,$sorting_cetak);
				$arr = $arr_cetak;
			$i = 0;
			foreach($result as $key => $val)
			{
				$biller = (!empty($val["PS_BILLER_ID"])?$val["PS_BILLER_ID"]:'');
				$resultTes = $getPayment->getProvider($biller);
				
				unset($arr[$key]['TRA_STATUS']);
// 				
					
// 				$arr[$key]["PS_CREATED"] = Application_Helper_General::convertDate($val["PS_CREATED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
// 				$arr[$key]["PS_UPDATED"] = Application_Helper_General::convertDate($val["PS_UPDATED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
//				$arr[$key]["PS_EFDATE"] = Application_Helper_General::convertDate($val["PS_EFDATE"],$this->view->_dateViewFormat);
// 				$arr[$key]["BANK"] = "";
// 				$arr[$key]["NOSTRO_NAME"] = "";
				$arr[$key]["PS_EFDATE"] = Application_Helper_General::convertDate($val["PS_EFDATE"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
				$crdate = $arr[$key]["PS_EFDATE"];
				unset($arr[$key]['TRX_ID']);
				unset($arr[$key]['PS_CREATED']);
				unset($arr[$key]['PS_UPDATED']);
				$crdate_clean = str_replace('00:00:00','',$crdate);
				
				
				unset($arr[$key]['TRACE_NO']);
				unset($arr[$key]['BANK_RESPONSE']);
				unset($arr[$key]['TRA_AMOUNT']);
				unset($arr[$key]['TRANSFER_FEE']);
				unset($arr[$key]['BILLER_ORDER_ID']);
				unset($arr[$key]['PS_BILLER_ID']);
				unset($arr[$key]['PS_CCY']);
				
				unset($arr[$key]['LLD_TRANSACTION_PURPOSE']);
				unset($arr[$key]['BENEFICIARY_CATEGORY']);
				unset($arr[$key]['TRA_AMOUNTS']);
				unset($arr[$key]['PROVIDER_NAME']);
			
				
				
				if($val["BANK_NAME"] == NULL){
					$arr[$key]["BANK_NAME"] = $val["BENEFICIARY_BANK_NAME"];
				}
				else{
					$arr[$key]["BANK_NAME"] = $val["BANK_NAME"];
				}

				//$arr[$key]["NOSBANK"] = $val["NOSTRO_NAME"];
				unset($arr[$key]['NOSBANK']);
				unset($arr[$key]["NOSTRO_NAME"]);

				unset($arr[$key]["BENEFICIARY_BANK_NAME"]);
				$arr[$key]["PS_EFDATE"] = $crdate_clean;
// 				unset($arr[$key]['PS_EFDATE']);
				$arr[$key]["TRA_AMOUNT"] = Application_Helper_General::displayMoney($val["TRA_AMOUNT"]);
				$arr[$key]["TRA_AMOUNTS"] = Application_Helper_General::displayMoney($val["TRA_AMOUNT"]);
				$arr[$key]["TRANSFER_FEE"] = Application_Helper_General::displayMoney($val["TRANSFER_FEE"]);
				$arr[$key]["PS_BILLER_ID"] = $resultTes[0]["SERVICE_NAME"];
				$arr[$i++]['PROVIDER_NAME'] = $resultTes[0]["PROVIDER_NAME"];
				unset($arr[$key]['PS_TYPE']);
				//unset($arr[$key]['NOSTRO_NAME']);
				unset($arr[$key]['TRA_AMOUNT']);
				unset($arr[$key]['TRA_AMOUNTS']);
				unset($arr[$key]['TRANSFER_FEE']);
				unset($arr[$key]['PS_BILLER_ID']);
				unset($arr[$key]['PROVIDER_NAME']);
				unset($arr[$key]['PS_STATUS']);
				//unset($arr[$key]['CURR_AMOUNT']);
//				unset($arr[$key]['USER_FULLNAME']);
//				unset($arr[$key]['PS_UPDATED']);
			}
	
//				unset($fields['username']);
//				unset($fields['updateddate']);
			$tempfields = $fields;
			//unset($tempfields['amount']);
			$header  = Application_Helper_Array::simpleArray($tempfields, "label");
			
			if($csv)
			{
// 				print_r($arr);die;
// 				$arr[$key+1]['number']	= false;
// 				$arr[$key+1][0]	= '';
// 				$arr[$key+1][1]	= '';
// 				$arr[$key+1][2]	= '';
// 				$arr[$key+1][4]	= '';
// 				$arr[$key+1][5]	= '';
// 				$arr[$key+1][6]	= '';
				
// 				$arr[$key+1][7]	= 'Total';
// 				$arr[$key+1][8]	= $totalnumbertx;
				
// 				echo "<pre>";print_r($arr);die;
				$this->_helper->download->csv($header,$arr,null,$this->language->_('LLD Report'));  
				Application_Helper_General::writeLog('RPPY','Export CSV Payment Report');
			}
			if($pdf)
			{
// 				$arr[$key+1]['number']	= false;
// 				$arr[$key+1][0]	= '';
// 				$arr[$key+1][1]	= '';
// 				$arr[$key+1][2]	= '';
// 				$arr[$key+1][4]	= '';
// 				$arr[$key+1][5]	= '';
// 				$arr[$key+1][6]	= '';
				
// 				$arr[$key+1][7]	= 'Total';
// 				$arr[$key+1][8]	= $totalnumbertx;
				
				//$arr[$key+1][18]	= '';
				$this->_helper->download->pdf($header,$arr,null,$this->language->_('LLD Report'),'landscape');  
				Application_Helper_General::writeLog('RPPY','Export PDF LLD Report');
			}
			
			//Zend_Debug::dump($dataSQL);die;
		}else if($this->_request->getParam('print')){
				
				$arr_print = $result;

				$i = 0;
				foreach($result as $key => $val)
				{
					$biller = (!empty($val["PS_BILLER_ID"])?$val["PS_BILLER_ID"]:'');
					$resultTes = $getPayment->getProvider($biller);
					
					unset($arr_print[$key]['TRA_STATUS']);
					$crdate = $arr_print[$key]["PS_EFDATE"];
					unset($arr_print[$key]["PS_EFDATE"]);
					$crdate_clean = str_replace('00:00:00','',$crdate);
					$arr_print[$key]["PS_EFDATE"] = $crdate_clean;
					
// 					$arr_print[$key]["PS_CREATED"] = Application_Helper_General::convertDate($val["PS_CREATED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
// 					$arr_print[$key]["PS_UPDATED"] = Application_Helper_General::convertDate($val["PS_UPDATED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
	//				$arr[$key]["PS_EFDATE"] = Application_Helper_General::convertDate($val["PS_EFDATE"],$this->view->_dateViewFormat);
// 					$arr_print[$key]["PS_EFDATE"] = Application_Helper_General::convertDate($val["PS_EFDATE"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
					$arr_print[$key]["TRA_AMOUNTS"] = $val["PS_CCY"].' '.Application_Helper_General::displayMoney($val["TRA_AMOUNT"]);
					$arr_print[$key]["TRANSFER_FEE"] = Application_Helper_General::displayMoney($val["TRANSFER_FEE"]);
					$arr_print[$key]["PS_BILLER_ID"] = $resultTes[0]["SERVICE_NAME"];
					$arr_print[$i++]['PROVIDER_NAME'] = $resultTes[0]["PROVIDER_NAME"];

					if($val["BANK_NAME"] == NULL){
						$arr_print[$key]["BANK_NAME"] = $val["BENEFICIARY_BANK_NAME"];
					}
					else{
						$arr_print[$key]["BANK_NAME"] = $val["BANK_NAME"];
					}
					
	//				unset($arr[$key]['USER_FULLNAME']);
	//				unset($arr[$key]['PS_UPDATED']);
				}

				$tempfields = $fields;
				//unset($tempfields['amount']);

				if($this->_request->getParam('print') == 1){
//				unset($fields['username']);
//				unset($fields['updateddate']);
// 				$arr_print[$key+1][0]	= '';
// 				$arr_print[$key+1][1]	= '';
// 				$arr_print[$key+1][2]	= '';
// 				$arr_print[$key+1][4]	= '';
// 				$arr_print[$key+1][5]	= '';
// 				$arr_print[$key+1][6]	= '';
// 				$arr_print[$key+1][7]	= '';
// 				$arr_print[$key+1][8]	= '';
// 				$arr_print[$key+1][9]	= '';
// 				$arr_print[$key+1][10]	= '';
// 				$arr_print[$key+1][11]	= '';
// 				$arr_print[$key+1]['USER_ID']	= 'Total';
// 				$arr_print[$key+1]['TRA_AMOUNT']	= $totalnumbertx;
// 				$arr_print[$key+1]['TRANSFER_FEE']	= $totalvaluetx;
				
// 				print_r($arr_print);die;
	            $this->_forward('print', 'index', 'widget', array('data_content' => $arr_print, 'data_caption' => 'LLD Report', 'data_header' => $tempfields));
	       	}

		}
		else
		{
			Application_Helper_General::writeLog('RPPY','View Payment Report');
		}
		
		$this->paging($result);
		$this->view->fields = $fields;
		$this->view->filter = $filter;
	}
}
