<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class bgrequest_IndexController extends Application_Main 
{

    protected $_moduleDB = 'RTF'; // masih harus diganti
    
    public function indexAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->systemType = $system_type;

        $conf = Zend_Registry::get('config');
        $this->_bankName = $conf['app']['bankname'];
        $this->view->masterbankname = $this->_bankName;

        $filter_clear     = $this->_getParam('clearfilter');
		
		$selectbranch = $this->_db->select()
                      ->from(array('BG' => 'M_BUSER'))
					  ->join(array('C' => 'M_BRANCH'), 'C.ID = BG.BUSER_BRANCH', array('HEADQUARTER' ,'BRANCH_CODE'))
					  ->where('BG.BUSER_ID = ?',$this->_userIdLogin);
		$branch = $this->_db->fetchRow($selectbranch);
        
        $fields = array(            
            'regno'     => array('field'   => 'BG_REG_NUMBER',
                                 'label'   => $this->language->_('Reg No#'),
                                ),
            'applicant' => array('field'  => 'CUST_ID',
                                  'label'  => $this->language->_('Applicant'),
                                ),
            'subject'   => array('field'    => 'BG_SUBJECT',
                                'label'    => $this->language->_('Subject'),
                                ),
            'idccy'     => array('field'    => 'CCYID',
                                  'label'    => $this->language->_('CCY '),
                                ),
            'bgamount'  => array('field'    => 'BG_AMOUNT',
                                'label'    => $this->language->_('BG Amount'),
                                ),
            'datefrom'  => array('field'  => 'DATE_FROM',
                                  'label'  => $this->language->_('Date From'),
                                ),
            'dateto'    => array('field'      => 'DATE_TO',
                                'label'      => $this->language->_('Date To'),  
                                )
        );

        $filterlist = array('BG_REG_NUMBER','BG_SUBJECT','BG_PERIOD');

        $this->view->filterlist = $filterlist;
                  
        $page    = $this->_getParam('page');        
        
        $sortBy  = ($this->_getParam('sortby')) ?  $this->_getParam('sortby') : ('BG_UPDATED');
        $sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
        
        $sortDir = $this->_getParam('sortdir');
        $sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'desc';
        
        $this->view->currentPage = $page;
        $this->view->sortBy      = $sortBy;
        $this->view->sortDir     = $sortDir;
        
        $select2 = $this->_db->select()
                      ->from(array('BG' => 'T_BANK_GUARANTEE'))
                      ->where('BG.BG_STATUS = ?', '4')
                      ->join(array('C' => 'M_CUSTOMER'), 'BG.CUST_ID = C.CUST_ID', array('CUST_NAME' => 'C.CUST_NAME'))
                      ->order('BG_UPDATED DESC');
        
        $filterArr = array(
                      'BG_REG_NUMBER'   => array('StripTags','StringTrim','StringToUpper'),                                               
                      'BG_SUBJECT'      => array('StripTags')
                          );
                            
        $dataParam = array("BG_REG_NUMBER","BG_SUBJECT");
        $dataParamValue = array();

        foreach ($dataParam as $dtParam)
        {          
          if(!empty($this->_request->getParam('wherecol'))){
            $dataval = $this->_request->getParam('whereval');
              foreach ($this->_request->getParam('wherecol') as $key => $value) {
                if($dtParam==$value){
                  $dataParamValue[$dtParam] = $dataval[$key];
                }
              }

          }
        }

        if(!empty($this->_request->getParam('efdate'))){
          $efdatearr = $this->_request->getParam('efdate');
          $dataParamValue['TIME_PERIOD_START'] = $efdatearr[0];
          $dataParamValue['TIME_PERIOD_END'] = $efdatearr[1];
        }
                          
        $validator = array(                  
         'BG_REG_NUMBER'  => array(),  
         'BG_SUBJECT'      => array(),              
         'TIME_PERIOD_START'    => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
         'TIME_PERIOD_END'    => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
          );
       
        $zf_filter   = new Zend_Filter_Input($filterArr,$validator,$dataParamValue);

        if ($zf_filter->isValid()) {
          $filter     = TRUE;
        }
        
         
        $bgRegNubmer  = html_entity_decode($zf_filter->getEscaped('BG_REG_NUMBER'));   
        $bgSubject  = html_entity_decode($zf_filter->getEscaped('BG_SUBJECT'));     
        $datefrom    = html_entity_decode($zf_filter->getEscaped('TIME_PERIOD_START'));
        $dateto    = html_entity_decode($zf_filter->getEscaped('TIME_PERIOD_END'));
       
       
        if($filter == null)
        { 
          $datefrom = (date("d/m/Y"));
          $dateto = (date("d/m/Y"));
          $this->view->fDateFrom  = (date("d/m/Y"));
          $this->view->fDateTo  = (date("d/m/Y"));
        }
        
        if($filter_clear == '1'){
          $this->view->fDateFrom  = '';
          $this->view->fDateTo  = '';
          $datefrom = '';
          $dateto = '';
          
        }
      
        if($filter == null || $filter ==TRUE)
        {
          $this->view->fDateFrom = $datefrom;
          $this->view->fDateTo = $dateto;  
          if(!empty($datefrom))
          {
            $FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
            $datefrom  = $FormatDate->toString($this->_dateDBFormat); 
          }
                
          if(!empty($dateto))
          {
              $FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
              $dateto    = $FormatDate->toString($this->_dateDBFormat);
          }
      
          if(!empty($datefrom) && empty($dateto))
              $select2->where("BG.TIME_PERIOD_START >= ".$this->_db->quote($datefrom));
                
          if(empty($datefrom) && !empty($dateto))
              $select2->where("BG.TIME_PERIOD_END <= ".$this->_db->quote($dateto));
                  
          if(!empty($datefrom) && !empty($dateto))
              $select2->where("BG.TIME_PERIOD_START >= ".$this->_db->quote($datefrom)." and BG.TIME_PERIOD_END <= ".$this->_db->quote($dateto));
        }

      
        if($filter == TRUE)
        {   
          
          if($bgRegNubmer!=null)
          {         
            $this->view->bgRegNubmer = $bgRegNubmer;
            $select2->where("BG.BG_REG_NUMBER LIKE ".$this->_db->quote('%'.$bgRegNubmer.'%'));
          }
          
          if($bgSubject != null)
          {
             $this->view->bgSubject = $bgSubject;
               $select2->where("BG.SUBJECT LIKE ".$this->_db->quote('%'.$bgSubject.'%'));
          }
        }
		if($branch['HEADQUARTER'] != 'YES'){
			$select2->where('BG.BG_BRANCH = ?',$branch['BRANCH_CODE']);
		}

        $select2->order($sortBy.' '.$sortDir);
      
        // if($this->_request->isPost() )
        // {
          
        //   $bg_id = $this->_getParam('change_id');
          
        //   if(count($bg_id) > 0)
        //   {     
          
        //     if($this->_getParam('submit')=='Approve')
        //     { 
                      
        //       foreach($bg_id as $key=>$row){            
        //         try 
        //         {
        //           $this->_db->beginTransaction();   
        
        //           $dataapprove =  array(          
        //                       'BG_STATUS' => '5', 
        //                       'BG_APPROVED' => date('Y-m-d H:i:s'), 
        //                       'BG_APPROVEDBY' => $this->_userIdLogin,
        //                       'BG_UPDATED' => date('Y-m-d H:i:s'), 
        //                       'BG_UPDATEDBY' => $this->_userIdLogin
        //                     );              
                  
        //           $where = array('BG_REG_NUMBER = ?' => $row);
        //           $result = $this->_db->update('T_BANK_GUARANTEE',$dataapprove,$where);
        //           Application_Helper_General::writeLog('APLO','Approve Bank Guarantee '.$row);  
                  
        //           $this->_db->commit();             
                  
        //         }
        //         catch(Exception $e) 
        //         {
        //           $this->_db->rollBack();
                  
        //         }
        //         }     
        //         $this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
        //       $this->_redirect('/notification/success');          
              
              
        //     }elseif($this->_getParam('submit')=='Reject')
        //     {
              
        //       foreach($BG_id as $key=>$row){            
        //         try 
        //         {
        //           $this->_db->beginTransaction();   
        
        //           $dataapprove =  array(          
        //                       'BG_STATUS' => '6',
        //                       'BG_UPDATED' => date('Y-m-d H:i:s'), 
        //                       'BG_UPDATEDBY' => $this->_userIdLogin
        //                     );              
                  
        //           $where = array('BG_REG_NUMBER = ?' => $row);
        //           $result = $this->_db->update('T_BANK_GUARANTEE',$dataapprove,$where);
        //           Application_Helper_General::writeLog('RJLO','Reject Bank Guarantee '.$row); 
                  
        //           $this->_db->commit();             
                  
        //         }
        //         catch(Exception $e) 
        //         {
        //           $this->_db->rollBack();
                  
        //         }
        //         }     
        //         $this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
        //       $this->_redirect('/notification/success');    
              
        //     }
          
        //   }else{
        //     $this->view->error = TRUE;
        //     $this->view->report_msg = $this->language->_('Error: Please select a Bank Guarantee Request');
        //   }
          
        // }
      
        if($csv || $pdf || $this->_request->getParam('print'))
        {
          $arr = $this->_db->fetchAll($select2);
          foreach ($arr as $key => $value)
          {
            unset($arr[$key]['CUST_ID']);
            //echo $key;
            $arr[$key]["PS_REQUESTED"] = Application_Helper_General::convertDate($value["PS_REQUESTED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);

            $arr[$key]["SOURCE_ACCOUNT"] = $arr[$key]["SOURCE_ACCOUNT"]." [".$arr[$key]["SOURCE_ACCOUNT_CCY"]."] / ".$arr[$key]["SOURCE_ACCOUNT_NAME"];
            unset($arr[$key]["SOURCE_ACCOUNT_CCY"]);
            unset($arr[$key]["SOURCE_ACCOUNT_NAME"]);

            $arrProductType = array('1'=>'Cheque','2'=>'Bilyet Giro');
            $arr[$key]["PRODUCT_TYPE"] = $arrProductType[$arr[$key]["PRODUCT_TYPE"]];

            unset($arr[$key]['PS_APPROVEBY']);

            $requestby = $arr[$key]['PS_REQUESTBY'];
            unset($arr[$key]['PS_REQUESTBY']);

            $arr[$key]["ADMIN_FEE"] = "IDR ".Application_Helper_General::displayMoney($arr[$key]["ADMIN_FEE"]);
            unset($arr[$key]["SUGGEST_STATUS"]);

            $arr[$key]["PS_REQUESTBY"] = $requestby;
            
          }
    
          $header = Application_Helper_Array::simpleArray($fields, 'label');
          if($csv)
          {
            Application_Helper_General::writeLog('DTRX','Download CSV Transaction Report');
            $this->_helper->download->csv($header,$arr,null,'Executed Transaction');
          }
          
          if($pdf)
          {
            Application_Helper_General::writeLog('DTRX','Download PDF Transaction Report');
            $this->_helper->download->pdf($header,$arr,null,'Executed Transaction');
          }
          if($this->_request->getParam('print') == 1){
            
                      $this->_forward('print', 'index', 'widget', array('data_content' => $arr, 'data_caption' => 'Executed Transaction', 'data_header' => $fields));
              }
      }
      else
      {
        Application_Helper_General::writeLog('DTRX','View Transaction Report');
      }
  
        $this->view->fields = $fields;
        $this->view->filter = $filter;
        $this->paging($select2);

        if(!empty($dataParamValue)){
          foreach ($dataParamValue as $key => $value) {
            $wherecol[] = $key;
            $whereval[] = $value;
          }
            $this->view->wherecol     = $wherecol;
            $this->view->whereval     = $whereval;
          }

    }

}