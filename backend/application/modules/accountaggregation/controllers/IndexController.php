<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';
require_once 'General/CustomerUser.php';
require_once 'CMD/Validate/ValidateAccountBeneficiary.php';
require_once 'Service/Token.php'; //added new
require_once 'CMD/Validate/Validate.php';

class Accountaggregation_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$selectbank = $this->_db->select()
					 ->from(array('C' => 'M_BANKTABLE'));
		// $select3->where("USER_ID = ".$this->_db->quote($this->_userIdLogin));
		$databank 					= $this->_db->fetchAll($selectbank);
		// print_r($databank);die;
		$bankArr = array();
		foreach ($databank as $key => $value) {
			// print_r($value);die;
			$bankArr[$value['BANK_CODE']] = $value['BANK_NAME'];
		}
		// print_r($bankArr);die;
		$this->view->DEBIT_BANKarr  = $bankArr;
  		// print_r($this->_request->getParams());die;

		if($this->_getParam('error') == '1'){
			$sessionNamespace = new Zend_Session_Namespace('directdebit');
			$error = $sessionNamespace->error;
			// print_r($sessionNamespace);die;
			$this->view->error = $error;
		}

		if($this->_getParam('confirm') == '1'){
			$sessionNamespace = new Zend_Session_Namespace('beneficiaryAccountAddEdit');
			$content = $sessionNamespace->content;
			$this->view->confirmPage = 1;
			$this->view->COMPANY_ID = $content['COMP_ID'];
			$this->view->COMPANY_NAME = $content['COMP_NAME'];
			$this->view->COMPANY_TYPE = $content['COMP_TYPE'];
			
			$bankdata = $this->_db->select()
					 ->from(array('C' => 'M_BANKTABLE'));
			$bankdata->where("BANK_CODE = ".$this->_db->quote($content['DEBIT_BANK']));
			$databank 					= $this->_db->fetchAll($bankdata);
			
			$this->view->DEBIT_BANK_NAME = $databank['0']['BANK_NAME'];
			$this->view->DEBIT_BANK = $content['DEBIT_BANK'];
			$this->view->DEBIT_ACCOUNT = $content['DEBIT_ACCT'];
			$this->view->ACCT_NAME = $content['ACCT_NAME'];
		}

	   $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		 if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}
    	}

	    $fields = array(
						'clearing_code'  => array('field' => 'CLR_CODE',
											      'label' => $this->language->_('Company'),
											      'sortable' => true),
						'swift_code'     => array('field' => 'SWIFT_CODE',
											      'label' => $this->language->_('Company Type'),
											      'sortable' => true),
						'bank_name'      => array('field' => 'BANK_NAME',
											      'label' => $this->language->_('Bank'),
											      'sortable' => true),
						'bank_account'      => array('field' => 'BANK_NAME',
											      'label' => $this->language->_('Bank Account'),
											      'sortable' => true),
						'ccy'      => array('field' => 'BANK_NAME',
											      'label' => $this->language->_('CCY'),
											      'sortable' => true),
						'status'      => array('field' => 'BANK_NAME',
											      'label' => $this->language->_('Status'),
											      'sortable' => true),
						'last_suggested'      => array('field' => 'BANK_NAME',
											      'label' => $this->language->_('Last Suggested'),
											      'sortable' => true),
						'last_approved'      => array('field' => 'BANK_NAME',
											      'label' => $this->language->_('Last Approved'),
											      'sortable' => true),
						/*'Workfield_code'           => array('field' => 'WORKFIELD_CODE',
											      'label' => 'Workfield Code',
											      'sortable' => true),
						 'bank_office_name'      => array('field' => 'BANK_OFFICE_NAME',
											      'label' => 'Bank Office Name',
											      'sortable' => true),
						'bank_address'      => array('field' => 'BANK_ADDRESS',
											      'label' => 'Bank Address',
											      'sortable' => true),
						// 'city'           => array('field' => 'CITY',
											      // 'label' => 'City',
											      // 'sortable' => true),
						 'CITY'           => array('field' => 'CITY_CODE',
											      'label' => 'City Code',
											      'sortable' => true),
						'province_code'           => array('field' => 'PROVINCE_CODE',
											      'label' => 'Province Code',
											      'sortable' => true),
						'branch_code'           => array('field' => 'BRANCH_CODE',
											      'label' => 'Branch Code',
											      'sortable' => true),
						'branch_name'           => array('field' => 'BRANCH_NAME',
											      'label' => 'Branch Name',
											      'sortable' => true),
												  /*
						'kbi_code'           => array('field' => 'KBI_CODE',
											      'label' => 'KBI Code',
											      'sortable' => true),

						'pop_status_code'           => array('field' => 'POP_STATUS_CODE',
											      'label' => 'Population Status',
											      'sortable' => true),
						'res_status_code'           => array('field' => 'RES_STATUS_CODE',
											      'label' => 'Resident Status',
											      'sortable' => true), */
					/* 	'clr_status_code'           => array('field' => 'CLR_STATUS',
											      'label' => 'Clearing Status',
											      'sortable' => true),
						'cord_status_code'           => array('field' => 'CORD_STATUS_CODE',
											      'label' => 'Cord Status',
											      'sortable' => true),
						'bank_inst_code'           => array('field' => 'BANK_INST_CODE',
											      'label' => 'Bank Inst Code',
											      'sortable' => true),
						'active_date'           => array('field' => 'ACTIVE_DATE',
											      'label' => 'Active Date',
											      'sortable' => true),
						'bi_account'           => array('field' => 'BI_ACCOUNT',
											      'label' => 'BI Account',
											      'sortable' => true), */
				      );


	    $filterlist = array("CLR_CODE","SWIFT_CODE","BANK_NAME");

	    $selectcomp = $this->_db->select()
                            ->from('M_CUSTOMER', array('value' => 'CUST_ID','CUST_NAME','CUST_TYPE'))
                            // ->where('CUST_NAME = ? ',$tblName)
                            ->where('CUST_STATUS = 1 ');

        $tempColumn = $this->_db->fetchAll($selectcomp);

        $this->view->custarr = json_encode($tempColumn);

		$this->view->filterlist = $filterlist;

		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','BANK_NAME');
		$sortDir = $this->_getParam('sortdir','asc');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;

		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';

		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	'filter' 	  	 => array('StringTrim','StripTags'),
							'CLR_CODE'  => array('StringTrim','StripTags'),
							'SWIFT_CODE'      => array('StringTrim','StripTags'),
							'BANK_NAME'      => array('StringTrim','StripTags')
							// 'swift_code'     => array('StringTrim','StripTags')
		);

		$dataParam = array("CLR_CODE","SWIFT_CODE","BANK_NAME");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{

			// print_r($dtParam);die;
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}

			}

			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}

		$zf_filter = new Zend_Filter_Input($filterArr,array(),$dataParamValue);
		// $filter = $zf_filter->getEscaped('filter');
		$filter 		= $this->_getParam('filter');

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;

		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');

		$getData = Application_Helper_Array::SimpleArray($fields,'field');

		$select = $this->_db->select()
					        ->from(array('A' => 'M_DOMESTIC_BANK_TABLE'),array_merge($getData,array('BANK_ID')))
							->where('BANK_ISMASTER <> 1');

		if($filter == TRUE || $csv || $pdf || $this->_request->getParam('print'))
		{
		    $header = Application_Helper_Array::simpleArray($fields,'label');

			$fclearing_code = $zf_filter->getEscaped('CLR_CODE');
			$fbank_name     = $zf_filter->getEscaped('BANK_NAME');
			$fswift_code    = $zf_filter->getEscaped('SWIFT_CODE');
			$fcity_code     = $zf_filter->getEscaped('CITY');

	        if($fclearing_code) $select->where('UPPER(CLR_CODE) LIKE '.$this->_db->quote('%'.strtoupper($fclearing_code).'%'));
	        if($fbank_name)     $select->where('BANK_NAME LIKE '.$this->_db->quote('%'.strtoupper($fbank_name).'%'));
	        if($fswift_code)    $select->where('UPPER(SWIFT_CODE) LIKE '.$this->_db->quote('%'.strtoupper($fswift_code).'%'));
	        if($fcity_code)          $select->where('UPPER(CITY_CODE) LIKE '.$this->_db->quote('%'.strtoupper($fcity_code).'%'));

			$this->view->clearing_code = $fclearing_code;
			$this->view->bank_name     = $fbank_name;
			$this->view->swift_code    = $fswift_code;
			$this->view->CITY     = $fcity_code;
		}

		//$this->view->success = true;
		$select->order($sortBy.' '.$sortDir);

		$select = $this->_db->fetchall($select);

		//menghilangkan index/key BANK_ID utk kepentingan CSV dan PDF
		$selectPdfCsv = $select;
		foreach($selectPdfCsv as $key => $row)
		{
		    unset($selectPdfCsv[$key]['BANK_ID']);
		}


		//--------konfigurasicsv dan pdf---------
	    if($csv)
	    {
				$this->_helper->download->csv($header,$selectPdfCsv,null,'domestic_bank_list');
				Application_Helper_General::writeLog('DBLS','Download CSV Domestic Bank');
		}
		else if($pdf)
		{
				$this->_helper->download->pdf($header,$selectPdfCsv,null,'domestic_bank_list');
				Application_Helper_General::writeLog('DBLS','Download PDF Domestic Bank');
		}
		else if($this->_request->getParam('print') == 1){
			$this->_forward('print', 'index', 'widget', array('data_content' => $selectPdfCsv, 'data_caption' => 'Domestic Bank', 'data_header' => $fields));
		}
		else
		{
				Application_Helper_General::writeLog('DBLS','View Domestic Bank');
		}
		//-------END konfigurasicsv dan pdf------------

		$this->paging($select);
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;

      }

	}

	public function addAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$this->view->bankpop = 'disabled';
		
//		$payment					= new payment_Model_Payment();
		// $modelCity = new predefinedbeneficiary_Model_Predefinedbeneficiary();
		
		$attahmentDestination 	= UPLOAD_PATH . '/document/help/';		
		$errorRemark 			= null;
		$adapter 				= new Zend_File_Transfer_Adapter_Http();

		$settings 			= new Application_Settings();
		$privibenelinkage = $this->view->hasPrivilege('BLBA');
		$Beneficiary = new Beneficiary();
		$filter  = new Application_Filtering();
		$settings = new Application_Settings();

		//ambil data m_user ->ambil sourceAccount, acctType, statusnya aktif=1
		
		$selectcomp = $this->_db->select()
                            ->from('M_CUSTOMER', array('value' => 'CUST_ID','CUST_NAME','CUST_TYPE'))
                            // ->where('CUST_NAME = ? ',$tblName)
                            ->where('CUST_STATUS = 1 ');

        $tempColumn = $this->_db->fetchAll($selectcomp);

        $this->view->custarr = json_encode($tempColumn);
        // $data = 
        // echo $data;die;

		//validasi token -- begin
		// $select3 = $this->_db->select()
		// 			 ->from(array('C' => 'M_USER'));
		// $select3->where("USER_ID = ".$this->_db->quote($this->_userIdLogin));
		// $data2 					= $this->_db->fetchRow($select3);
		

		$selectbank = $this->_db->select()
					 ->from(array('C' => 'M_BANKTABLE'));
		// $select3->where("USER_ID = ".$this->_db->quote($this->_userIdLogin));
		$databank 					= $this->_db->fetchAll($selectbank);
		// print_r($databank);die;
		$bankArr = array();
		foreach ($databank as $key => $value) {
			// print_r($value);die;
			$bankArr[$value['BANK_CODE']] = $value['BANK_NAME'];
		}

		$this->view->DEBIT_BANKarr  = $bankArr;

		if($this->_request->isPost() )
		{	

				$params = $this->_request->getParams();
				// print_r($params);die;

				if($params['report_radio'] == '1'){
				$adapter = new Zend_File_Transfer_Adapter_Http();
				$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';

                $adapter->setDestination ( $this->_destinationUploadDir );
                $extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv'));
                $extensionValidator->setMessage(
                    $this->language->_('Error').': '.$this->language->_('Extension file must be').' *.csv'
                );

                $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
                $sizeValidator->setMessage(
                    'Error: File exceeds maximum size'
                );

                $adapter->setValidators ( array (
                    $extensionValidator,
                    $sizeValidator,
                ));
                // print_r($adapter->getFileName ());
// die;
                if ($adapter->isValid ())
                {
       
                    $sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
                    $newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

                    $adapter->addFilter ( 'Rename',$newFileName  );

                    if ($adapter->receive ())
                    {
                        //PARSING CSV HERE
                        $csvData = $this->parseCSV($newFileName);
                        $csvData2 = $this->parseCSV($newFileName);
                        
                        @unlink($newFileName);
                        //end

                        $totalRecords2 = count($csvData2);
                        $totaldata = 0;
                        if($totalRecords2)
                            {
                                for ($a= 3; $a<$totalRecords2; $a++ ){
                                $totaldata++;
                            }
                        }
                        if(!empty($csvData['3'])){
                        	$totalrow = 3+$totaldata;

                        	try 
							{
											
											
											
                        	for ($i=3; $i < $totalrow; $i++) { 
                        		if(!empty($csvData[$i]['0']) && !empty($csvData[$i]['1']) && !empty($csvData[$i]['2'])){
                        			$this->_db->beginTransaction();
                        			if($i=='3'){
                        				$info = $this->language->_('accountaggregation');
										$info2 = $this->language->_('Import new Aggregation data');
										$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['new'],null,'T_AGREGATION','TEMP_AGREGATION',$csvData['3']['1'],'',$csvData['3']['1']);
                        			}

                        			$compdata = $this->_db->select()
									->from(array('C' => 'M_CUSTOMER'));
									$compdata->where("CUST_ID = ?",$csvData[$i]['0']);
									$compdata 					= $this->_db->fetchRow($compdata);
									// print_r($compdata);die;
									if(empty($compdata)){
										print_r($csvData[$i]['0']);die;
										$error = 'invalid customer code';
										$this->_db->rollBack();
										$sessionNamespace = new Zend_Session_Namespace('accountaggregation');
										$sessionNamespace->error = $error;
										$this->_redirect('/accountaggregation/index/index/error/1');
										break;

									}

									$bankdata = $this->_db->select()
									->from(array('C' => 'M_BANKTABLE'));
									$bankdata->where("BANK_CODE = ?",(int)$csvData[$i]['1']);
									$bankdata 					= $this->_db->fetchRow($bankdata);
									// print_r($bankdata);die;
									if(empty($bankdata)){
										$error = 'invalid bank code';
										$this->_db->rollBack();
										$sessionNamespace = new Zend_Session_Namespace('accountaggregation');
										$sessionNamespace->error = $error;
										$this->_redirect('/accountaggregation/index/index/error/1');
										break;
									}

									$inputarr = array(
										'COMP_ID' => $compdata['CUST_ID'],
										'COMP_NAME' => $compdata['CUST_NAME'],
										'COMP_TYPE' => $compdata['COMP_TYPE'],
										'BANK_ID' => sprintf("%03s", $csvData[$i]['1']),
										'BANK_ACCT' => $csvData[$i]['2']

									);
									$inputarr['AG_SUGESTED'] = new Zend_Db_Expr('now()');
									$inputarr['AG_SUGESTEDBY'] = $this->_userIdLogin;
									$inputarr['CHANGES_ID'] = $change_id;
									try{
										$this->_db->insert('TEMP_AGREGATION',$inputarr);	
									}catch(Exception $e) 
									{
													$this->_db->rollBack();
									}
									
										Application_Helper_General::writeLog('BADA','Add Direct Debit '.$compdata['CUST_ID']);
									$this->_db->commit();


                        		}else{
                        			$error = 'invalid data';
                        			$this->_db->rollBack();
                        			$sessionNamespace = new Zend_Session_Namespace('accountaggregation');
									$sessionNamespace->error = $error;
									$this->_redirect('/accountaggregation/index/index/error/1');
                        			break;

                        		}
                        	}
                        	}
							catch(Exception $e) 
							{
											$this->_db->rollBack();
							}

                        }else{
                        	$error = 'empty data';
                        	$sessionNamespace = new Zend_Session_Namespace('accountaggregation');
							$sessionNamespace->error = $error;
							$this->_redirect('/accountaggregation/index/index/error/1');

                        }

  	                     	$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index');
    						$this->_redirect('/notification/success');	
                    }
                }      


            }




			
			$fileExt 				= "pdf";
			
			$sourceFileName = $adapter->getFileName();

			if($sourceFileName == null)
			{
				$sourceFileName = null;
				$fileType = null;
			}
			else
			{
				$sourceFileName = substr(basename($adapter->getFileName()), 0);
				if($_FILES["document"]["type"])
				{
					$adapter->setDestination($attahmentDestination);
					$maxFileSize 			= $this->getSetting('Fe_attachment_maxbyte');
					$fileType = $adapter->getMimeType();
					$size = $_FILES["document"]["size"];
				}
				else
				{
					$fileType = null;
					$size = null;
				}
			}
			$paramsName['sourceFileName'] 	= $sourceFileName;
			
			
		    $filters = array(
		        'COMPANY_ID' => array('StringTrim','StripTags'),
		        'COMPANY_NAME' => array('StringTrim','StripTags'),
		        // 'auto_monthlyfee' => array('StringTrim','StripTags'),
		        'COMPANY_TYPE' => array('StringTrim','StripTags'),
		        'DEBIT_BANK' => array('StringTrim','StripTags'),
		        'DEBIT_ACCOUNT' => array('StringTrim','StripTags'),
		        'ACCT_NAME' => array('StringTrim','StripTags'),
		        'sourceFileName'		=> array('StringTrim')
		        );
		    
		    $validators = array(
		        
		        'COMPANY_ID'      => array('NotEmpty',
		            'messages' => array(
		                $this->language->_('Can not be empty')
		            )
		        ),
		        'COMPANY_NAME'      => array('NotEmpty',
		            'messages' => array(
		                $this->language->_('Can not be empty')
		            )
		        ),
		        'COMPANY_TYPE'      => array('allowEmpty' => TRUE),
		        'DEBIT_BANK'      => array('NotEmpty',
		            'messages' => array(
		                $this->language->_('Can not be empty')
		            )
		        ),
		        'DEBIT_ACCOUNT'      => array('NotEmpty',
		            'messages' => array(
		                $this->language->_('Can not be empty')
		            )
		        ),
		        'ACCT_NAME'      => array('NotEmpty',
		            'messages' => array(
		                $this->language->_('Can not be empty')
		            )
		        ),
		        'sourceFileName' => array('allowEmpty' => TRUE)
		        
		    );
		    
		    
		   
		    
			//$benefType = $this->_getParam('benefType');
			//setelah disamakan dgn FSD
			// $benefType = '4';//$this->_getParam('benefType'); //kalo dua ONLINE harus panggil fundtransferinquiry, kalo 1 skn rtgs langsung lewat
		
				$COMPANY_ID   = $this->_request->getParam('COMPANY_ID');
				$COMPANY_NAME   = $this->_request->getParam('COMPANY_NAME');
				$COMPANY_TYPE = $this->_request->getParam('COMPANY_TYPE');
				$DEBIT_BANK   = $this->_request->getParam('DEBIT_BANK');
				$DEBIT_ACCOUNT   = $this->_request->getParam('DEBIT_ACCOUNT');
				$ACCT_NAME = $this->_request->getParam('ACCT_NAME');
		
// 			$this->view->CURR_CODE = $CURR_CODE;

                
	//-------------------------------------------------------------START VALIDATION---------------------------------------------------------------------//	
			
				$fCOM_ID   = $filter->filter($COMPANY_ID  , "COMPANY_ID");
				$fCOM_NAME = $filter->filter($COMPANY_NAME , "COMPANY_NAME");
				$fCOM_TYPE = $filter->filter($COMPANY_TYPE, "COMPANY_TYPE");
				$fDEBIT_BANK   = $filter->filter($DEBIT_BANK  , "DEBIT_BANK");
				$fDEBIT_ACCT = $filter->filter($DEBIT_ACCOUNT , "DEBIT_ACCOUNT");
				$fACCT_NAME = $filter->filter($ACCT_NAME, "ACCT_NAME");
				
				
				$selectcheck = $this->_db->select()
				->from(array('C' => 'T_AGREGATION'));
				$selectcheck->where("COMP_ID = ?",$fCOM_ID);
				$selectcheck->where("BANK_ID = ?",$fDEBIT_BANK);
				$selectcheck->where("BANK_ACCT = ?",$fDEBIT_ACCT);
// 				 echo $selectcheck;die; 
				$checkbene 					= $this->_db->fetchRow($selectcheck);
			//validasi token -- end
     			// $zf_filter_input =/ array();
			// if(empty($checkbene)){
				$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);	
			// }
			$fileTypeMessage = explode('/',$fileType);
				$fileType =  $fileTypeMessage[1];
				$extensionValidator = new Zend_Validate_File_Extension(array(false, $fileExt));
				$extensionValidator->setMessage("Extension file must be *.pdf");

				$maxFileSize = "1024000";
				$size = number_format($size);

				$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $maxFileSize));
				$sizeValidator->setMessage("File size is exceeding 1,024,000 byte(s), uploaded file is $size byte(s)");
				
				$adapter->setValidators(array($extensionValidator, $sizeValidator));
					$validfile = true;
				if($adapter->isValid()){
					$validfile = true;
				}

			if($zf_filter_input->isValid() && $validfile)
			{
			    // die;
// 				$content = array(
// 								'BRANCH_NAME' 	 => $zf_filter_input->branch_name,
// 								'BANK_ADDRESS' 	 => $zf_filter_input->branch_address,
//             				    'CITY_NAME' 	 => $zf_filter_input->city_name,
//             				    'REGION_NAME' 	 => $zf_filter_input->region_name,
//             				    'CONTACT' 	 => $zf_filter_input->contact
// 						       );
				
				if(empty($checkbene)){



				// $privibenelinkage = $this->view->hasPrivilege('BLBA');

					$newFileName = 'submit-' . strtr(microtime(), array("." => "-", " " => "-")) . $sourceFileName;
					$adapter->addFilter('Rename', $newFileName);
					//$fileType = $adapter->getMimeType();

					$adapter->receive();
					
					$content = array(
											'COMP_ID' 			=> $zf_filter_input->COMPANY_ID,
											'COMP_NAME' 			=> $zf_filter_input->COMPANY_NAME,
											'COMP_TYPE' 		=> $zf_filter_input->COMPANY_TYPE,
											'BANK_ID' 		=> $zf_filter_input->DEBIT_BANK,
											'BANK_ACCT' 			=> $zf_filter_input->DEBIT_ACCOUNT,
											'ACCT_NAME'				=> $zf_filter_input->ACCT_NAME,
											'DIR_FILENAME' 		=> $sourceFileName,
											'DIR_SYS_FILENAME'		=> $newFileName,
									);

								
					
						
							// $content['BENEFICIARY_TYPE'] = 4; // 2 = domestic ( SKN/RTGS )
							//$content['CLR_CODE'] = $CLR_CODE;
						// print_r($content);die;
							$sessionNamespace = new Zend_Session_Namespace('beneficiaryAccountAddEdit');
							$sessionNamespace->mode = 'Add';
							$sessionNamespace->content = $content;
							// $this->_redirect('/directdebit/index/confirm');
							$this->_redirect('/accountaggregation/index/index/confirm/1');

					
				}else{
					$this->view->succes = false;
					$this->view->error = true;
               		$this->view->report_msg = $this->language->_('Aggregation data already exist');	
				}
						
			}
			else
			{

				$this->view->error = true;
				foreach(array_keys($filters) as $field)
						$this->view->$field = ($zf_filter_input->isValid($field))? $zf_filter_input->getEscaped($field) : $this->_getParam($field);

				$error = $zf_filter_input->getMessages();
				
				//format error utk ditampilkan di view html 
                $errorArray = null;
		        foreach($error as $keyRoot => $rowError)
		        {
		           foreach($rowError as $errorString)
		           {
		              $errorArray[$keyRoot] = $errorString;
		           }
		        }
        // print_r($errorArray);die;
		        $this->view->succes = false;
                $this->view->report_msg = $errorArray;			
			}
		}	
		
		$this->view->COMPANY_ID = $COMPANY_ID;
		$this->view->COMPANY_NAME = $COMPANY_NAME;
		$this->view->COMPANY_TYPE = $COMPANY_TYPE;
		$this->view->DEBIT_BANK = $DEBIT_BANK;
		$this->view->DEBIT_ACCOUNT = $DEBIT_ACCOUNT;
		$this->view->ACCT_NAME = $ACCT_NAME;
		
		Application_Helper_General::writeLog('BADA','Add Direct debit data');
					
	}

	public function confirmAction()
	{

		$this->_helper->layout()->setLayout('newlayout');

		$sessionNamespace = new Zend_Session_Namespace('beneficiaryAccountAddEdit');
		$content = $sessionNamespace->content;
		$this->view->COMPANY_ID = $content['COMP_ID'];
		$this->view->COMPANY_NAME = $content['COMP_NAME'];
		$this->view->COMPANY_TYPE = $content['COMP_TYPE'];
		$this->view->DEBIT_BANK = $content['BANK_ID'];
		$this->view->DEBIT_ACCOUNT = $content['BANK_ACCT'];
		$this->view->ACCT_NAME = $content['ACCT_NAME'];
		// print_r($content);
		// $mode = $sessionNamespace->mode;
		// $this->view->mode = $mode;
		

// 		die;
//  		$this->fillParams(null,$content['PHONE'],$content['ACBENEF'],$content['NRC']);
		if($this->_request->isPost() )
		{	
			// die;
			if($this->_getParam('submit1')==$this->language->_('Back'))
			{
				if($mode=='Add'){
					$this->_redirect('/accountaggregation/index/index/isback/1');
				}elseif ($mode=='Edit'){
					$this->_redirect('/accountaggregation/index/index/benef_id/'.$content['BENEFICIARY_ID'].'/isback/1');	
				}
			}
			else
			{
// die;
				try 
				{
					//-----insert benef--------------
					$this->_db->beginTransaction();
					// $Beneficiary = new Beneficiary();
					// $add = $Beneficiary->add($content);
					$info = $this->language->_('accountaggregation');
					$info2 = $this->language->_('Set new Aggregation data');
					
					$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['new'],null,'T_AGREGATION','TEMP_AGREGATION',$content['COMP_ID'],$content['COMP_NAME'],$content['COMP_ID']);
							$content['CHANGES_ID'] = $change_id;
							$content['DIR_SUGESTED'] = new Zend_Db_Expr('now()');
							$content['DIR_SUGESTEDBY'] = $this->_userIdLogin;
							// print_r($content);die;
					$this->_db->insert('TEMP_AGREGATION',$content);
					Application_Helper_General::writeLog('BADA','Add Aggregation '.$content['COMPANY_ID']);
					$this->_db->commit();
					unset($_SESSION['beneficiaryAccountAddEdit']);
					$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
					$this->_redirect('/notification/success');
				}
				catch(Exception $e) 
				{
					// print_r($e);die;
					//rollback changes
					$this->_db->rollBack();
				}
			}
		}
		Application_Helper_General::writeLog('BADA','Confirm Aggregation');
	}

}
