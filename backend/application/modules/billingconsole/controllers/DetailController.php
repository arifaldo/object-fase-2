<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Account.php';
require_once 'General/CustomerUser.php';
require_once 'Crypt/AESMYSQL.php';

class billingconsole_DetailController extends Application_Main
{
	protected $_moduleDB = 'RTF'; // masih harus diganti

	// public function __construct()
	// {
	// 	$this->_db = Zend_Db_Table::getDefaultAdapter();
	// }


	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
	    $password = $sessionNamespace->token; 
	    $this->view->token = $sessionNamespace->token;


	    $AESMYSQL = new Crypt_AESMYSQL();
	    $cust_id = $AESMYSQL->decrypt($this->_getParam('cust_id'), $password);
	    $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;

		$selectbank = $this->_db->select()
					 ->from(array('C' => 'M_BANK_TABLE'),array('*'));
		
	
		$databank 					= $this->_db->fetchAll($selectbank);

		$app = Zend_Registry::get('config');
		// charges.type.code.api/
		$status = $app['charges']['type']['code']['api'];
		$desc = $app['charges']['type']['desc']['api'];
		// var_dump($status);die;
		$paramservice = array();
		foreach ($status as $key => $value) {
			$paramservice[$value] = $desc[$key]; 
		}

		$this->view->paramservice = $paramservice;
	
		// $bankArr = array(''=>'-- '.$this->language->_('Please Select').' --');
		foreach ($databank as $key => $value) {
			// print_r($value);die;
			$bankArr[$value['BANK_CODE']] = $value['BANK_NAME'];
		}
		// print_r($bankArr);die;
		$this->view->DEBIT_BANKarr  = $bankArr;

		$optMonth = '';
		$min1month = date("F Y",strtotime("-1 month"));
		$min1monthval = date("Y-m",strtotime("-1 month"));
		$min2month = date("F Y",strtotime("-2 month"));
		$min2monthval = date("Y-m",strtotime("-2 month"));
		$min3month = date("F Y",strtotime("-3 month"));
		$min3monthval = date("Y-m",strtotime("-3 month"));

		$currentMonthVal = date("Y-m");
		$currentMonth = date("F Y");

		$optMonth .= '<option value="'.$min3monthval.'">'.$min3month.'</option>';
		$optMonth .= '<option value="'.$min2monthval.'">'.$min2month.'</option>';
		$optMonth .= '<option value="'.$min1monthval.'">'.$min1month.'</option>';
		$optMonth .= '<option value="'.$currentMonthVal.'" selected>'.$currentMonth.' (Ongoing)</option>';
		
		$this->view->optMonth = $optMonth;	

		$this->view->cust_id = $cust_id;

	
		if($this->_request->isPost()){
			$month = $this->_getParam('month');

			$hitlistbank = $this->_db->fetchAll(
	                    $this->_db->select()
	                         ->from(array('A' => 'T_DIGI_LOG'),array('TOTALHIT'=>'COUNT(DIGI_ID)','DIGI_BANK','DIGI_SERVICE'))
	                         ->JOIN (array('B' => 'M_BANK_TABLE'),'A.DIGI_BANK = B.BANK_CODE',array('B.BANK_NAME'))
	                         ->where("A.DIGI_CUST = ? ", $cust_id)
	                         ->where("A.DIGI_SERVICE != 0")
	                         // ->where("A.DIGI_BANK != NULL")
	                         // ->where("A.DIGI_SERVICE != NULL")
	                         ->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m')= '".$month."'")
	                         ->group('A.DIGI_BANK')     
	               );	

			$hitlist = $this->_db->fetchAll(
	                    $this->_db->select()
	                         ->from(array('A' => 'T_DIGI_LOG'),array('TOTALHIT'=>'COUNT(DIGI_ID)','DIGI_BANK','DIGI_SERVICE'))
	                         ->where("A.DIGI_CUST = ? ", $cust_id)
	                          ->where("A.DIGI_SERVICE != 0")
	                         ->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m')= '".$month."'")
	                         ->group('A.DIGI_BANK')
	                         ->group('A.DIGI_SERVICE')
	               );
		}
		else{
			$whereCurDate = date('Y-m');

			$hitlistbank = $this->_db->fetchAll(
	                    $this->_db->select()
	                         ->from(array('A' => 'T_DIGI_LOG'),array('TOTALHIT'=>'COUNT(DIGI_ID)','DIGI_BANK','DIGI_SERVICE'))
	                         ->JOIN (array('B' => 'M_BANK_TABLE'),'A.DIGI_BANK = B.BANK_CODE',array('B.BANK_NAME')
								 		)
	                         ->where("A.DIGI_CUST = ? ", $cust_id)
	                          ->where("A.DIGI_SERVICE != 0")
	                         ->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m')= '".$whereCurDate."'")
	                         ->group('A.DIGI_BANK')     
	               );	

			$hitlist = $this->_db->fetchAll(
	                    $this->_db->select()
	                         ->from(array('A' => 'T_DIGI_LOG'),array('TOTALHIT'=>'COUNT(DIGI_ID)','DIGI_BANK','DIGI_SERVICE'))
	                         ->where("A.DIGI_CUST = ? ", $cust_id)
	                          ->where("A.DIGI_SERVICE != 0")
	                         ->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m')= '".$whereCurDate."'")
	                         ->group('A.DIGI_BANK')
	                         ->group('A.DIGI_SERVICE')
	               );	
		}

		// echo $complist;	
		// echo "<pre>";
		// print_r($hitlist);die;

		//get data from globalvar.ini
		$serviceType = array_flip($this->_chargestype['code']['api']);
		$serviceDesc =  $this->_chargestype['desc']['api'];
		$serviceColumnName =  $this->_chargestype['dbcolumn']['api'];

		$this->view->serviceType = $serviceType;
		$this->view->serviceDesc = $serviceDesc;

		//get charges data from M_CHARGES_API
		$custIdList = "'".$cust_id."','GLOBAL'";
		$chargesFee = $this->_db->fetchAll(
            	$this->_db->select()
                 ->from(array('A' => 'M_CHARGES_API'),array('*'))
                 ->where("A.CUST_ID IN (".$custIdList.")")
       	);	

		$chargesFee = $chargesFee[0];

		$total = 0;
		if(!empty($hitlist)){
			foreach ($hitlist as $key => $value) {
				$price = $chargesFee[$serviceColumnName[$serviceType[$value['DIGI_SERVICE']]]];
				$totalsrv = (int)$value['TOTALHIT']*$price;
				$hitlist[$key]['TOTALPRICE'] = $totalsrv;
				
				$total = $total + $totalsrv;
			}
			$this->view->hitlistbank = $hitlistbank;
			// print_r($hitlist);die();
			$this->view->servicelist = $hitlist;
			// echo "<pre>";
			// var_dump($hitlistbank);
			// var_dump($hitlist);
			$this->view->totalprice = $total;
		}

		// var_dump($account);die;

		$pdf = $this->_getParam('pdf');

		if($pdf)
		{
			$base64 = $this->_getParam('assetsChartBase64');

			$hitlistbank = $this->_db->fetchAll(
	                    $this->_db->select()
	                         ->from(array('A' => 'T_DIGI_LOG'),array('TOTALHIT'=>'COUNT(DIGI_ID)','DIGI_BANK','DIGI_SERVICE'))
	                         ->JOIN (array('B' => 'M_BANK_TABLE'),'A.DIGI_BANK = B.BANK_CODE',array('B.BANK_NAME'))
	                         ->where("A.DIGI_CUST = ? ", $cust_id)
	                         ->where("A.DIGI_SERVICE != 0")
	                         // ->where("A.DIGI_BANK != NULL")
	                         // ->where("A.DIGI_SERVICE != NULL")
	                         ->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m')= '".$month."'")
	                         ->group('A.DIGI_BANK')     
	               );	

			$hitlist = $this->_db->fetchAll(
	                    $this->_db->select()
	                         ->from(array('A' => 'T_DIGI_LOG'),array('TOTALHIT'=>'COUNT(DIGI_ID)','DIGI_BANK','DIGI_SERVICE'))
	                         ->where("A.DIGI_CUST = ? ", $cust_id)
	                          ->where("A.DIGI_SERVICE != 0")
	                         ->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m')= '".$month."'")
	                         ->group('A.DIGI_BANK')
	                         ->group('A.DIGI_SERVICE')
	               );

			$total = 0;
			if(!empty($hitlist)){
				foreach ($hitlist as $key => $value) {
					$price = $chargesFee[$serviceColumnName[$serviceType[$value['DIGI_SERVICE']]]];
					$totalsrv = (int)$value['TOTALHIT']*$price;
					$hitlist[$key]['TOTALPRICE'] = $totalsrv;
					
					$total = $total + $totalsrv;
				}
				$this->view->pdfHitlistbank = $hitlistbank;
				$this->view->pdfServicelist = $hitlist;
				$this->view->pdfTotalprice = $total;
			}

			$formatedMonth = DateTime::createFromFormat('Y-m', $month);
			$formMonth = $formatedMonth->format('F Y');


			$curMonth = strtotime(date("Y-m"));

			$datediff = strtotime($month) - $curMonth;
			$difference = floor($datediff/(60*60*24));

			$title = '';
			if($difference==0)
			{
			   $title = 'Billing Console_'.$formMonth.' (Ongoing)';
			   $this->view->formatedMonth = $formMonth.' (Ongoing)';
			}
			else{
			   $title = 'Billing Console_'.$formMonth;
			   $this->view->formatedMonth = $formMonth;
			}

			$custData = $this->_db->fetchRow(
                $this->_db->select()
                     ->from(array('A' => 'M_CUSTOMER'),array('A.CUST_NAME'))
                     ->where("A.CUST_ID = ? ", $cust_id));

			Application_Helper_General::writeLog('BAIQ','Print PDF');
			// $HTMLchart = $this->view->render($this->view->controllername.'/chart.phtml');
			$HTMLtable = $this->view->render($this->view->controllername.'/pdf.phtml');
			$this->_helper->download->pdfWithChart(null,null,null,$title,$custData['CUST_NAME'],'BILLING CONSOLE',$base64,'280px',$HTMLtable);
		}
		
	}

	public function changemonthAction(){
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $cust_id = $this->_getParam('cust_id');

	    $value = $this->_getParam('value');

	    $hitlistbank = $this->_db->fetchAll(
	                    $this->_db->select()
	                         ->from(array('A' => 'T_DIGI_LOG'),array('TOTALHIT'=>'COUNT(DIGI_ID)','DIGI_BANK','DIGI_SERVICE'))
	                         ->JOIN (array('B' => 'M_BANK_TABLE'),'A.DIGI_BANK = B.BANK_CODE',array('B.BANK_NAME'))
	                         ->where("A.DIGI_CUST = ? ", $cust_id)
	                         ->where("A.DIGI_SERVICE != 0")
	                         // ->where("A.DIGI_BANK != NULL")
	                         // ->where("A.DIGI_SERVICE != NULL")
	                         ->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m')= '".$value."'")
	                         ->group('A.DIGI_BANK')     
	               );	

			$hitlist = $this->_db->fetchAll(
	                    $this->_db->select()
	                         ->from(array('A' => 'T_DIGI_LOG'),array('TOTALHIT'=>'COUNT(DIGI_ID)','DIGI_BANK','DIGI_SERVICE'))
	                         ->where("A.DIGI_CUST = ? ", $cust_id)
	                          ->where("A.DIGI_SERVICE != 0")
	                         ->where("DATE_FORMAT(A.DIGI_TIMESTAMP, '%Y-%m')= '".$value."'")
	                         ->group('A.DIGI_BANK')
	                         ->group('A.DIGI_SERVICE')
	               );	

		//get data from globalvar.ini
		$serviceType = array_flip($this->_chargestype['code']['api']);
		$serviceDesc =  $this->_chargestype['desc']['api'];
		$serviceColumnName =  $this->_chargestype['dbcolumn']['api'];

		$total = 0;
		$custIdList = "'".$cust_id."','GLOBAL'";
		$chargesFee = $this->_db->fetchAll(
            	$this->_db->select()
                 ->from(array('A' => 'M_CHARGES_API'),array('*'))
                 ->where("A.CUST_ID IN (".$custIdList.")")
       	);	

		$chargesFee = $chargesFee[0];
		// echo "<pre>";
		// var_dump($hitlist);die;
		if(!empty($hitlist)){
			foreach ($hitlist as $key => $value) {
				$price = $chargesFee[$serviceColumnName[$serviceType[$value['DIGI_SERVICE']]]];
				$totalsrv = (int)$value['TOTALHIT']*$price;
				$hitlist[$key]['TOTALPRICE'] = $totalsrv;
				
				$total = $total + $totalsrv;
			}
		}

		$result['hitlistbank'] = $hitlistbank;
		$result['servicelist'] = $hitlist;
		$result['totalprice'] = 'Total: IDR '.Application_Helper_General::displayMoney($total);

		if(!empty($hitlistbank)){
	      $datagraph = array();
	      $totalhit = 0;
		  foreach ($hitlistbank as $key => $value) {
		      $totalhit += $value['TOTALHIT'];
		  }
	      foreach ($hitlistbank as $key => $value) {
	          $datagraph[$key]['y'] = (int)$value['TOTALHIT'];
      		  $datagraph[$key]['legendText'] = $value['BANK_NAME'].' : '.number_format($value['TOTALHIT']).' ('.number_format(((int)$value['TOTALHIT']/(int)$totalhit)*100,2).'%)';
	          $datagraph[$key]['name'] = $value['BANK_NAME'];
	          $datagraph[$key]['color'] =sprintf('#%06X', mt_rand(0, 0xFFFFFF));
	      }
	    }

	    // $result['datapointstr'] = $str;

	    $visitorsData['Service'][] = array(
	    	'click' => 'visitorsChartDrilldownHandler',
	    	'cursor' => 'pointer',
	    	'explodeOnClick' => false,
	    	'name' => "New vs Returning Visitors",
	    	'showInLegend' => true,
	    	// 'indexLabelPlacement' => "outside",
	    	'type' => "doughnut",
	    	'dataPoints' => $datagraph
	    );

	    if(!empty($hitlistbank)){


	        $str = '';
	        foreach ($hitlistbank as $key => $value) {

		    $datagraphdetail = array();
		      
		    $strdata = array();
		    foreach ($hitlist as $k => $val) {
		        if($value['DIGI_BANK'] == $val['DIGI_BANK']){
		          	$datagraphdetail['y'] = (int)$val['TOTALHIT'];
		           	$service = array();
		            $service[1] = 'Balance Inquiry';
		            $service[2] = 'Register ';
		            $service[NULL] = 'Transfer Fee';
			        $datagraphdetail['name'] = $service[$val['DIGI_SERVICE']];
			        $datagraphdetail['indexLabel'] = $service[$val['DIGI_SERVICE']].' : '.$val['TOTALHIT'];
			        $datagraphdetail['color'] =sprintf('#%06X', mt_rand(0, 0xFFFFFF));

			        array_push($strdata, $datagraphdetail);
			        unset($datagraphdetail);
			    }
		      }

		      if(!empty($hitlistbank[$key+1])){

		          $visitorsData[$value['BANK_NAME']][] = array(
		          	'color' => sprintf('#%06X', mt_rand(0, 0xFFFFFF)),
		          	'name' => $value['BANK_NAME'],
		          	'type' => 'doughnut',
		          	'showInLegend' => 'true',
		          	'dataPoints' => $strdata,
		          );

		      }else{

		      	  $visitorsData[$value['BANK_NAME']][] = array(
		          	'color' => sprintf('#%06X', mt_rand(0, 0xFFFFFF)),
		          	'name' => $value['BANK_NAME'],
		          	'type' => 'doughnut',
		          	// 'indexLabelPlacement' => 'outside',
		          	'showInLegend' => 'true',
		          	'dataPoints' => $strdata,
		          );
		      }
		    }
		}

		$result['visitorsdata'] = json_encode($visitorsData);


		$selectbank = $this->_db->select()
					 ->from(array('C' => 'M_BANK_TABLE'),array('*'));
		
	
		$databank 					= $this->_db->fetchAll($selectbank);
	
		foreach ($databank as $key => $value) {
			$bankArr[$value['BANK_CODE']] = $value['BANK_NAME'];
		}


	    $i = 0;
	    $tableData = '';

	    $serviceDesc =  $this->_chargestype['desc']['api'];
	    $serviceType = array_flip($this->_chargestype['code']['api']);
	    // echo "<pre>";
	    // var_dump($serviceDesc);die;
	    if(!empty($hitlist)){
	      foreach ($hitlist as $key => $value) {
	        $td_css = ($i%2==0)?'tbl-evencontent':'tbl-oddcontent';
	          $tableData .= '<tr>  
                    <td class="'.$td_css.'">'.$bankArr[$value['DIGI_BANK']].'</td>
                    <td class="'.$td_css.'">'.$serviceDesc[$serviceType[$value['DIGI_SERVICE']]].'</td>
                    <td class="'.$td_css.'" align="right">'.number_format($value['TOTALHIT']).'</td>
                    <td class="'.$td_css.'" style="width: 30%;" ><label class="col-md-3" style="text-align:left">IDR</label><label class="col-md-9" style="text-align:right">'.Application_Helper_General::displayMoney($value['TOTALPRICE']).'</label></td>
                  </tr>';
            $i++;

        	}
        }
        else{
        	$tableData .= '<tr><td colspan="5" align="center">---- No Data ----</td></tr>';
        }
              

        $result['tableData'] = $tableData;

		echo json_encode($result);
	}

}
