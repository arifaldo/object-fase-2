<?php


require_once 'Zend/Controller/Action.php';


class billingconsole_IndexController extends Application_Main
{
	public function indexAction()
	{

		$setting = new Settings();			  	
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
		$pw_hash = md5($enc_salt.$enc_pass);
		$rand = $this->_userIdLogin.date('dHis').$pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;
	
		$this->_helper->layout()->setLayout('newlayout');


		$optMonth = '';
		$min1month = date("F Y",strtotime("-1 month"));
		$min1monthval = date("Y-m",strtotime("-1 month"));
		$min2month = date("F Y",strtotime("-2 month"));
		$min2monthval = date("Y-m",strtotime("-2 month"));
		$min3month = date("F Y",strtotime("-3 month"));
		$min3monthval = date("Y-m",strtotime("-3 month"));

		$currentMonthVal = date("Y-m");
		$currentMonth = date("F Y");

		$optMonth .= '<option value="'.$min3monthval.'">'.$min3month.'</option>';
		$optMonth .= '<option value="'.$min2monthval.'">'.$min2month.'</option>';
		$optMonth .= '<option value="'.$min1monthval.'">'.$min1month.'</option>';
		$optMonth .= '<option value="'.$currentMonthVal.'" selected>'.$currentMonth.' (Ongoing)</option>';
		
		$this->view->optMonth = $optMonth;	



		$companyCode = $this->language->_('Company Code');
		$companyName = $this->language->_('Company');
		$balanceInquiry = $this->language->_('Balance Inquiry');
		$accountStatement = $this->language->_('Account Statement');
		$inhouse = $this->language->_('Inhouse Transfer');
		$skn = $this->language->_('SKN Transfer');
		$rtgs = $this->language->_('RTGS Transfer');
		$online = $this->language->_('ONLINE Transfer');
		$inhouseBenefInquiry = $this->language->_('Inhouse Beneficiary Inquiry');
		$interBenefInquiry = $this->language->_('Interbank Beneficiary Inquiry');
		$kursRate = $this->language->_('Kurs Rate Inquiry');
		$total = $this->language->_('Total');

		$fields = array	(
							// 'Company Code'  			=> array	(
							// 										'field' => 'B.CUST_ID',
							// 										'label' => $companyCode,
							// 										'sortable' => true
							// 									),
							'Company'  				=> array	(
																	'field' => 'CUST_ID',
																	'label' => $companyName,
																	'type' => null,
																	'sortable' => true
																),
							'Balance Inquiry'  			=> array	(
																	'field' => 'BALANCE_INQUIRY',
																	'label' => $balanceInquiry,
																	'type' => 'number',
																	'sortable' => true
																),
							'Account Statement'  			=> array	(
																	'field' => 'ACCOUNT_STATEMENT',
																	'label' => $accountStatement,
																	'type' => 'number',
																	'sortable' => true
																),
							'Inhouse Transfer'  			=> array	(
																	'field' => 'INHOUSE_TRANSFER',
																	'label' => $inhouse,
																	'type' => 'number',
																	'sortable' => true
																),
							'SKN Transfer'  			=> array	(
																	'field' => 'SKN_TRANSFER',
																	'label' => $skn,
																	'type' => 'number',
																	'sortable' => true
																),
							'RTGS Transfer'  			=> array	(
																	'field' => 'RTGS_TRANSFER',
																	'label' => $rtgs,
																	'type' => 'number',
																	'sortable' => true
																),
							'ONLINE Transfer'  			=> array	(
																	'field' => 'ONLINE_TRANSFER',
																	'label' => $online,
																	'type' => 'number',
																	'sortable' => true
																),
							'Inhouse Beneficiary Inquiry'  			=> array	(
																	'field' => 'INHOUSE_BENEF_INQUIRY',
																	'label' => $inhouseBenefInquiry,
																	'type' => 'number',
																	'sortable' => true
																),
							'Interbank Beneficiary Inquiry'  			=> array	(
																	'field' => 'INTERBANK_BENEF_INQUIRY',
																	'label' => $interBenefInquiry,
																	'type' => 'number',
																	'sortable' => true
																),
							'Kurs Rate Inquiry'  			=> array	(
																	'field' => 'KURS_RATE_INQUIRY',
																	'label' => $kursRate,
																	'type' => 'number',
																	'sortable' => true
																),
							'Total'  		=> array	(
																	'field' => 'TOTAL',
																	'label' => $total,
																	'type' => 'money',
																	'sortable' => true
																)
						);

		$filterlist = array("CUST_ID","SUGGESTOR","SUGGESTED_DATE");
		$this->view->filterlist = $filterlist;

		$filterArr = array('filter' 			=> array('StripTags','StringTrim'),
	                       'custid'    			=> array('StripTags','StringTrim'),
	                       'custname'    			=> array('StripTags','StringTrim'),
	                      );

	    $validator = array('filter' 			=> array(),
	                       'custid'    			=> array(),
	                       'custname'    			=> array(),
	                      );

	    $dataParam = array("CUST_ID","CUST_NAME");
		$dataParamValue = array();
	    foreach ($dataParam as $dtParam)
		{
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if($dtParam==$value){
						$dataParamValue[$dtParam] = $dataval[$key];
					}
				}
			}
		}


	    $zf_filter = new Zend_Filter_Input($filterArr,$validator,$this->_request->getParams());
	    $filter = $zf_filter->getEscaped('filter');
	    $custid = html_entity_decode($zf_filter->getEscaped('custid'));;
	    $custid = html_entity_decode($zf_filter->getEscaped('custname'));;
		//Zend_Debug::dump($this->_masterglobalstatus); die;

		$page = $this->_getParam('page');

		$page = (Zend_Validate::is($page,'Digits'))? $page : 1;
		$sortBy  = $this->_getParam('sortby');
   		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
   		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;

		$whereCurDate = date('Y-m');

		//select from log
		$selectLog = $this->_db->select()
	         ->from(array('A' => 'M_CUSTOMER'), array('A.CUST_ID','A.CUST_NAME'))
	         ->joinLeft(array('B' => 'T_DIGI_LOG'),'B.DIGI_CUST = A.CUST_ID',
	         				array(
	         									'coalesce(B.DIGI_SERVICE," - ") AS SERVICE',
	         									'COUNT(B.DIGI_ID) AS TOTALHIT'
	         				))
	         ->where("B.DIGI_SERVICE != 0")
	         ->where("DATE_FORMAT(B.DIGI_TIMESTAMP, '%Y-%m')= '".$whereCurDate."'")
	         ->group("A.CUST_ID")
	         ->group("B.DIGI_SERVICE");


		if($filter == true)
		{

			if($custid)
		    {
	       		$this->view->cekcustid = $custid;
	       		$selectLog->where("B.CUST_ID LIKE ".$this->_db->quote($custid));
		    }

			if($custname)
		    {
	       		$this->view->custname = $custname;
	       		$selectLog->where("B.CUST_NAME LIKE ".$this->_db->quote('%'.$custname.'%'));
		    }
		}

		$selectLog->order($sortBy.' '.$sortDir);

		$selectLog = $this->_db->fetchAll($selectLog);

		//select all company
       	$selectCompany = $this->_db->fetchAll(
            $this->_db->select()
                 ->from(array('A' => 'M_CUSTOMER'), array('A.CUST_ID','A.CUST_NAME'))
       	);


		//select m charges api
		$selectCharges = $this->_db->select()->distinct()
						->from(array('B' => 'M_CUSTOMER'),array('B.CUST_ID',
																'B.CUST_NAME',
																'B.CUST_SUGGESTED',
																'B.CUST_SUGGESTEDBY'))
						->joinLeft(array('C' => 'M_CHARGES_API'),'B.CUST_ID = C.CUST_ID',
								array(
										'coalesce(C.BALANCE_INQUIRY," - ") AS BALANCE_INQUIRY', 
										'coalesce(C.ACCOUNT_STATEMENT," - ") AS ACCOUNT_STATEMENT', 
										'coalesce(C.INHOUSE_TRANSFER," - ") AS INHOUSE_TRANSFER',
										'coalesce(C.SKN_TRANSFER," - ") AS SKN_TRANSFER',
										'coalesce(C.RTGS_TRANSFER," - ") AS RTGS_TRANSFER',
										'coalesce(C.ONLINE_TRANSFER," - ") AS ONLINE_TRANSFER',
										'coalesce(C.INHOUSE_BENEF_INQUIRY," - ") AS INHOUSE_BENEF_INQUIRY',
										'coalesce(C.INTERBANK_BENEF_INQUIRY," - ") AS INTERBANK_BENEF_INQUIRY',
										'coalesce(C.KURS_RATE_INQUIRY," - ") AS KURS_RATE_INQUIRY',
										'coalesce(C.CHARGES_SUGGESTED," - ") AS CHARGES_SUGGESTED',
										'coalesce(C.CHARGES_SUGGESTEDBY," - ") AS CHARGES_SUGGESTEDBY',
										'coalesce(C.CHARGES_APPROVED," - ") AS CHARGES_APPROVED',
										'coalesce(C.CHARGES_APPROVEDBY," - ") AS CHARGES_APPROVEDBY'
								))
						->where("B.CUST_STATUS != '3'")
						->order('B.CUST_ID ASC');
						
		$arr = $this->_db->fetchAll($selectCharges);

		//select global charges
       	$selectGlobal = $this->_db->fetchRow(
            $this->_db->select()
                 ->from(array('A' => 'M_CHARGES_API'))
                 ->where('A.CUST_ID = "GLOBAL"')
       	);


       	$serviceType = $this->_chargestype['code']['api'];
       	$serviceTypeFlip = array_flip($this->_chargestype['code']['api']);
       	$serviceTypeDesc = $this->_chargestype['code']['desc']['api'];
		$serviceColumnName =  $this->_chargestype['dbcolumn']['api'];

		// echo '<pre

		$tempCust = '';
		foreach ($selectLog as $key => $value) {

			foreach ($arr as $key2=> $value2) {
				if ($value['CUST_ID'] == $value2['CUST_ID']) {
					if ($value2[$serviceColumnName[$serviceTypeFlip[$value['SERVICE']]]] != ' - ') {
						$value['TOTAL'] = $value['TOTALHIT'] * $value2[$serviceColumnName[$serviceTypeFlip[$value['SERVICE']]]];
					}
					else{
						$value['TOTAL'] = $value['TOTALHIT'] * $selectGlobal[$serviceColumnName[$serviceTypeFlip[$value['SERVICE']]]];
					}
				}
			}
			$logData[$value['CUST_ID']][] = $value;
		}

		foreach ($selectCompany as $key => $value) {

			if (!empty($logData)) {
				foreach ($logData as $key2 => $value2) {
					$total = 0;
					if ($value['CUST_ID'] == $key2) {

						$newValue['CUST_ID'] = $value['CUST_ID'];					
						$newValue['CUST_NAME'] = $value2[0]['CUST_NAME'];					
						foreach ($value2 as $keys => $values) {

							$newValue[$serviceColumnName[$serviceTypeFlip[$values['SERVICE']]]] = $values['TOTALHIT'];

							$total += $values['TOTAL'];
						}
						$newValue['TOTAL'] = $total;
						break;
					}
					else{
						$newValue = array(
								'CUST_ID' => $value['CUST_ID'],
								'CUST_NAME' => $value['CUST_NAME'],
								'BALANCE_INQUIRY' => ' - ',
								'ACCOUNT_STATEMENT' => ' - ',
								'INHOUSE_TRANSFER' => ' - ',
								'SKN_TRANSFER' => ' - ',
								'RTGS_TRANSFER' => ' - ',
								'ONLINE_TRANSFER' => ' - ',
								'INHOUSE_BENEF_INQUIRY' => ' - ',
								'INTERBANK_BENEF_INQUIRY' => ' - ',
								'KURS_RATE_INQUIRY' => ' - '
							);
						$newValue['TOTAL'] = $total;
					}
				}
			}
			else{
				$newValue = array(
								'CUST_ID' => $value['CUST_ID'],
								'CUST_NAME' => $value['CUST_NAME'],
								'BALANCE_INQUIRY' => ' - ',
								'ACCOUNT_STATEMENT' => ' - ',
								'INHOUSE_TRANSFER' => ' - ',
								'SKN_TRANSFER' => ' - ',
								'RTGS_TRANSFER' => ' - ',
								'ONLINE_TRANSFER' => ' - ',
								'INHOUSE_BENEF_INQUIRY' => ' - ',
								'INTERBANK_BENEF_INQUIRY' => ' - ',
								'KURS_RATE_INQUIRY' => ' - '
							);
				$newValue['TOTAL'] = 0;
			}
			

			$logAllData[$value['CUST_ID']] = $newValue;
				
		}

		if($this->_request->getParam('print') == 1){

			foreach($logAllData as $key=>$row)
			{
				$logAllData[$key]['TOTAL'] = 'IDR '.Application_Helper_General::displayMoney($row['TOTAL']);
				$logAllData[$key]['CUST_ID'] = $row['CUST_NAME'].' ('.$row['CUST_ID'].')';

				
			}

			// echo "<pre>";
			// var_dump($logAllData);
			// die();

			$this->_forward('print', 'index', 'widget', array('data_content' => $logAllData, 'data_caption' => 'Billing Console', 'data_header' => $fields));
		}

		// echo "<pre>";
		// print_r($logAllData);die();

    	// $this->paging($logAllData,30);
    	$this->view->logAllData = $logAllData;
    	$this->view->fields = $fields;
    	$this->view->filter = $filter;

    
    	Application_Helper_General::writeLog('CHLS','View Company Charges List');
	}

	public function changemonthAction(){
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

	    $date = $this->_getParam('value');


		$selectLog = $this->_db->select()
	         ->from(array('A' => 'M_CUSTOMER'), array('A.CUST_ID','A.CUST_NAME'))
	         ->joinLeft(array('B' => 'T_DIGI_LOG'),'B.DIGI_CUST = A.CUST_ID',
	         				array(
	         									'coalesce(B.DIGI_SERVICE," - ") AS SERVICE',
	         									'COUNT(B.DIGI_ID) AS TOTALHIT'
	         				))
	         ->where("B.DIGI_SERVICE != 0")
	         ->where("DATE_FORMAT(B.DIGI_TIMESTAMP, '%Y-%m')= '".$date."'")
	         ->group("A.CUST_ID")
	         ->group("B.DIGI_SERVICE");

	    $selectLog = $this->_db->fetchAll($selectLog);

	    
	    	//select all company
	       	$selectCompany = $this->_db->fetchAll(
	            $this->_db->select()
	                 ->from(array('A' => 'M_CUSTOMER'), array('A.CUST_ID','A.CUST_NAME'))
	       	);


			//select m charges api
			$selectCharges = $this->_db->select()->distinct()
							->from(array('B' => 'M_CUSTOMER'),array('B.CUST_ID',
																	'B.CUST_NAME',
																	'B.CUST_SUGGESTED',
																	'B.CUST_SUGGESTEDBY'))
							->joinLeft(array('C' => 'M_CHARGES_API'),'B.CUST_ID = C.CUST_ID',
									array(
											'coalesce(C.BALANCE_INQUIRY," - ") AS BALANCE_INQUIRY', 
											'coalesce(C.ACCOUNT_STATEMENT," - ") AS ACCOUNT_STATEMENT', 
											'coalesce(C.INHOUSE_TRANSFER," - ") AS INHOUSE_TRANSFER',
											'coalesce(C.SKN_TRANSFER," - ") AS SKN_TRANSFER',
											'coalesce(C.RTGS_TRANSFER," - ") AS RTGS_TRANSFER',
											'coalesce(C.ONLINE_TRANSFER," - ") AS ONLINE_TRANSFER',
											'coalesce(C.INHOUSE_BENEF_INQUIRY," - ") AS INHOUSE_BENEF_INQUIRY',
											'coalesce(C.INTERBANK_BENEF_INQUIRY," - ") AS INTERBANK_BENEF_INQUIRY',
											'coalesce(C.KURS_RATE_INQUIRY," - ") AS KURS_RATE_INQUIRY',
											'coalesce(C.CHARGES_SUGGESTED," - ") AS CHARGES_SUGGESTED',
											'coalesce(C.CHARGES_SUGGESTEDBY," - ") AS CHARGES_SUGGESTEDBY',
											'coalesce(C.CHARGES_APPROVED," - ") AS CHARGES_APPROVED',
											'coalesce(C.CHARGES_APPROVEDBY," - ") AS CHARGES_APPROVEDBY'
									))
							->where("B.CUST_STATUS != '3'")
							->order('B.CUST_ID ASC');
							
			$arr = $this->_db->fetchAll($selectCharges);

			//select global charges
	       	$selectGlobal = $this->_db->fetchRow(
	            $this->_db->select()
	                 ->from(array('A' => 'M_CHARGES_API'))
	                 ->where('A.CUST_ID = "GLOBAL"')
	       	);


	       	$serviceType = $this->_chargestype['code']['api'];
	       	$serviceTypeFlip = array_flip($this->_chargestype['code']['api']);
	       	$serviceTypeDesc = $this->_chargestype['code']['desc']['api'];
			$serviceColumnName =  $this->_chargestype['dbcolumn']['api'];

			$tempCust = '';
			foreach ($selectLog as $key => $value) {

				foreach ($arr as $key2=> $value2) {
					if ($value['CUST_ID'] == $value2['CUST_ID']) {
						if ($value2[$serviceColumnName[$serviceTypeFlip[$value['SERVICE']]]] != ' - ') {
							$value['TOTAL'] = $value['TOTALHIT'] * $value2[$serviceColumnName[$serviceTypeFlip[$value['SERVICE']]]];
						}
						else{
							$value['TOTAL'] = $value['TOTALHIT'] * $selectGlobal[$serviceColumnName[$serviceTypeFlip[$value['SERVICE']]]];
						}
					}
				}
				$logData[$value['CUST_ID']][] = $value;
			}

			// echo '<pre>';
			// print_r($logData);die();

			foreach ($selectCompany as $key => $value) {

				if (!empty($logData)) {
					foreach ($logData as $key2 => $value2) {
						$total = 0;
						if ($value['CUST_ID'] == $key2) {

							$newValue['CUST_ID'] = $value['CUST_ID'];					
							$newValue['CUST_NAME'] = $value2[0]['CUST_NAME'];					
							foreach ($value2 as $keys => $values) {

								$newValue[$serviceColumnName[$serviceTypeFlip[$values['SERVICE']]]] = $values['TOTALHIT'];

								$total += $values['TOTAL'];
							}
							$newValue['TOTAL'] = $total;
							break;
						}
						else{
							$newValue = array(
									'CUST_ID' => $value['CUST_ID'],
									'CUST_NAME' => $value['CUST_NAME'],
									'BALANCE_INQUIRY' => ' - ',
									'ACCOUNT_STATEMENT' => ' - ',
									'INHOUSE_TRANSFER' => ' - ',
									'SKN_TRANSFER' => ' - ',
									'RTGS_TRANSFER' => ' - ',
									'ONLINE_TRANSFER' => ' - ',
									'INHOUSE_BENEF_INQUIRY' => ' - ',
									'INTERBANK_BENEF_INQUIRY' => ' - ',
									'KURS_RATE_INQUIRY' => ' - '
								);
							$newValue['TOTAL'] = $total;
						}
					}	
				}
				else{
					$newValue = array(
									'CUST_ID' => $value['CUST_ID'],
									'CUST_NAME' => $value['CUST_NAME'],
									'BALANCE_INQUIRY' => ' - ',
									'ACCOUNT_STATEMENT' => ' - ',
									'INHOUSE_TRANSFER' => ' - ',
									'SKN_TRANSFER' => ' - ',
									'RTGS_TRANSFER' => ' - ',
									'ONLINE_TRANSFER' => ' - ',
									'INHOUSE_BENEF_INQUIRY' => ' - ',
									'INTERBANK_BENEF_INQUIRY' => ' - ',
									'KURS_RATE_INQUIRY' => ' - '
								);
					$newValue['TOTAL'] = 0;
				}

				$logAllData[$value['CUST_ID']] = $newValue;
			}

			$tableData = '';

		    if(count($logAllData) > 0)
		    {
		      $totalbilling = 0;
		      $counter = 1;
		      foreach($logAllData as $key => $value)
		      {   
		        $td_css = ($i%2==0)?'tbl-evencontent':'tbl-oddcontent';

		        // $link = $this->url(array('module'=>$this->modulename,'controller'=>'detail','action'=>'index','cust_id'=>$value['CUST_ID']),null,true);
		        $compName = $value['CUST_NAME'].' (<a href="billingconsole/detail/index/cust_id/'.$value['CUST_ID'].'">'.$value['CUST_ID'].'</a>)';

		        $tableData .= '<tr>
		                <td class="'.$td_css.'">'.$counter.'</td>
		                <td class="'.$td_css.'">'.$compName.'</td>
		                <td class="'.$td_css.'">'.($value['BALANCE_INQUIRY'] != ' - ' ? number_format($value['BALANCE_INQUIRY']) : $value['BALANCE_INQUIRY']).'</td>
		               <td class="'.$td_css.'">'.($value['ACCOUNT_STATEMENT'] != ' - ' ? number_format($value['ACCOUNT_STATEMENT']) : $value['ACCOUNT_STATEMENT']).'</td>
		               <td class="'.$td_css.'">'.($value['INHOUSE_TRANSFER'] != ' - ' ? number_format($value['INHOUSE_TRANSFER']) : $value['INHOUSE_TRANSFER']).'</td>
		               <td class="'.$td_css.'">'.($value['SKN_TRANSFER'] != ' - ' ? number_format($value['SKN_TRANSFER']) : $value['SKN_TRANSFER']).'</td>
		               <td class="'.$td_css.'">'.($value['RTGS_TRANSFER'] != ' - ' ? number_format($value['RTGS_TRANSFER']) : $value['RTGS_TRANSFER']).'</td>
		               <td class="'.$td_css.'">'.($value['ONLINE_TRANSFER'] != ' - ' ? number_format($value['ONLINE_TRANSFER']) : $value['ONLINE_TRANSFER']).'</td>
		               <td class="'.$td_css.'">'.($value['INHOUSE_BENEF_INQUIRY'] != ' - ' ? number_format($value['INHOUSE_BENEF_INQUIRY']) : $value['INHOUSE_BENEF_INQUIRY']).'</td>
		               <td class="'.$td_css.'">'.($value['INTERBANK_BENEF_INQUIRY'] != ' - ' ? number_format($value['INTERBANK_BENEF_INQUIRY']) : $value['INTERBANK_BENEF_INQUIRY']).'</td>
		               <td class="'.$td_css.'">'.($value['KURS_RATE_INQUIRY'] != ' - ' ? number_format($value['KURS_RATE_INQUIRY']) : $value['KURS_RATE_INQUIRY']).'</td>
		                <td align="right" width="20%" class="'.$td_css.'">IDR '.Application_Helper_General::displayMoney($value['TOTAL']).'</td>';

		        $tableData .= '</tr>';
		        $counter++;
		        $totalbilling += $value['TOTAL'];
		    }
		}
	    else
	    {
		  	$totalbilling = 0;
		    $tableData .= '<tr><td colspan=11 class="tbl-evencontent" align="center"> ----- '.$this->language->_('No Data').' ----- </td></tr>';
	    }	
              

        $result['tableData'] = $tableData;
        $result['totalbilling'] = Application_Helper_General::displayMoney($totalbilling);

		echo json_encode($result);
	}
}
