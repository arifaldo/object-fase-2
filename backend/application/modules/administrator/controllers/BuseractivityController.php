<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';
class administrator_BuseractivityController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$select = $this->_db->select()->distinct()
			->from(
				array('A' => 'M_BGROUP'),
				array('BGROUP_ID', 'BGROUP_DESC')
			)
			->order('BGROUP_DESC ASC')
			->query()->fetchAll();

		$this->view->var = $select;

		$activity = $this->_db->select()->distinct()
			->from(array('A' => 'M_BPRIVILEGE'), array('BPRIVI_ID', 'BPRIVI_DESC'))
			->order('BPRIVI_DESC ASC')
			->query()->fetchAll();

		$login = array('BPRIVI_ID' => 'BLGN', 'BPRIVI_DESC' => 'Login');
		$logout = array('BPRIVI_ID' => 'BLGT', 'BPRIVI_DESC' => 'Logout');
		$changepass = array('BPRIVI_ID' => 'CHOP', 'BPRIVI_DESC' => 'Change My Password');
		array_unshift($activity, $changepass);
		array_unshift($activity, $logout);
		array_unshift($activity, $login);

		$activityarr = Application_Helper_Array::listArray($activity, 'BPRIVI_ID', 'BPRIVI_DESC');
		asort($activityarr);
		//Zend_Debug::dump($activityarr);die;

		$this->view->activity = $activityarr;

		//Application_Helper_General::writeLog('ADBU','View Backend User Activity');

		$fields = array(

			'BUSER_NAME'        => array(
				'field' => 'BUSER_NAME',
				'label' => $this->language->_('User Name'),
				'sortable' => true
			),
			'B.LOG_DATE'     => array(
				'field' => 'B.LOG_DATE',
				'label' => $this->language->_('Activity Date'),
				'sortable' => true
			),
			'BPRIVI_DESC'  		=> array(
				'field' => 'BPRIVI_DESC',
				'label' => $this->language->_('Activity Type'),
				'sortable' => true
			),
			'ACTION_FULLDESC'   => array(
				'field' => 'ACTION_FULLDESC',
				'label' => $this->language->_('Description'),
				'sortable' => true
			)
		);
		// PS_ACTIVITY',
		$filterlist = array('User ID' => 'USER_ID', 'User Name' => 'USER_NAME', 'Activity Type' => 'ACTIVITY_TYPE', 'Activity Date' => 'ACTIVITY_DATE');

		$this->view->filterlist = $filterlist;

		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby', 'B.LOG_DATE');
		$sortDir = $this->_getParam('sortdir', 'DESC');

		$page = (Zend_Validate::is($page, 'Digits') && ($page > 0)) ? $page : 1;

		$sortBy = (Zend_Validate::is(
			$sortBy,
			'InArray',
			array(array_keys($fields))
		)) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is(
			$sortDir,
			'InArray',
			array('haystack' => array('asc', 'desc'))
		)) ? $sortDir : 'asc';

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;

		$filter = $this->_getParam('filter');
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');

		$select2 = $this->_db->select()
			->from(array('B' => 'T_BACTIVITY'), array())
			->joinleft(array('C' => 'M_BUSER'), 'B.USER_ID = C.BUSER_ID', array())
			->joinleft(array('D' => 'M_BPRIVILEGE'), 'B.ACTION_DESC = D.BPRIVI_ID', array(
				'C.BUSER_ID',
				'C.BUSER_NAME',
				'B.LOG_DATE',
				'BPRIVI_DESC' 	=> new Zend_Db_Expr("(CASE B.ACTION_DESC 
				WHEN 'BLGN' THEN 'Login' 
				WHEN 'BLGT' THEN 'Logout' 
				WHEN 'CHOP' THEN 'Change My Password' 
				ELSE D.BPRIVI_DESC 
				END)"),
				'B.ACTION_FULLDESC'
			));
		//echo ($select2);die;
		$filterArr = array(
			'filter' => array('StripTags', 'StringTrim'),
			'GROUP'    => array('StripTags', 'StringTrim', 'StringToUpper'),
			'USER_ID'  => array('StripTags', 'StringTrim', 'StringToUpper'),
			'USER_NAME'  => array('StripTags', 'StringTrim', 'StringToUpper'),
			'description'  => array('StripTags', 'StringTrim', 'StringToUpper'),
			'ACTIVITY_TYPE' => array('StripTags', 'StringToUpper'),
			'PS_ACTIVITY' => array('StripTags', 'StringTrim'),
			'PS_ACTIVITY_END' => array('StripTags', 'StringTrim'),
		);

		$validators = array(
			'filter'			 	=> array(),
			'GROUP'   	 		=> array(),
			'USER_ID'  			=> array(),
			'USER_NAME'  			=> array(),
			'description'  			=> array(),
			'ACTIVITY_TYPE' 	=> array(),
			'PS_ACTIVITY' 			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
			'PS_ACTIVITY_END' 			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
		);

		$dataParam = array('USER_ID', 'USER_NAME', 'GROUP', 'ACTIVITY_TYPE');
		$dataParamValue = array();

		$clean2 = array_diff($this->_request->getParam('wherecol'), $dataParam);
		$dataParam = array_diff($this->_request->getParam('wherecol'), $clean2);
		// print_r($dataParam);die;

		// print_r($output);die;
		// print_r($this->_request->getParam('wherecol'));
		foreach ($dataParam as $no => $dtParam) {

			if (!empty($this->_request->getParam('wherecol'))) {
				$dataval = $this->_request->getParam('whereval');
				// print_r($dataval);
				$order = 0;
				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if ($value == "PS_ACTIVITY") {
						$order--;
					}
					if ($dtParam == $value) {
						$dataParamValue[$dtParam] = $dataval[$order];
					}
					$order++;
				}
			}
		}

		if ($this->_request->getParam('createdate')) {
			// $this->view->createdStart = $this->_request->getParam('createdate')[0];
			// $this->view->createdEnd = $this->_request->getParam('createdate')[1];
			$dataParamValue['LAST_SUGGESTED_START'] = $this->_request->getParam('createdate')[0];
			$dataParamValue['LAST_SUGGESTED_END'] = $this->_request->getParam('createdate')[1];
		}

		$zf_filter = new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $this->_optionsValidator);
		//echo "<pre>";
		//print_r ($dataParamValue);
		$filter 		= $this->_getParam('filter');
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');

		if (!empty($this->_request->getParam('actdate'))) {
			$actarr = $this->_request->getParam('actdate');
			$dataParamValue['PS_ACTIVITY'] = $actarr[0];
			$dataParamValue['PS_ACTIVITY_END'] = $actarr[1];
		}

		$validator = array(
			'filter'			 	=> array(),
			'GROUP'   	 		=> array(),
			'USER_ID'  			=> array(),
			'USER_NAME'  			=> array(),
			'description'  			=> array(),
			'ACTIVITY_TYPE' 	=> array(),
			'PS_ACTIVITY' 			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
			'PS_ACTIVITY_END' 			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
		);

		// $zf_filter  	= new Zend_Filter_Input($filterArr, $validators, $dataParamValue);
		// $filter = $this->_request->getParam('filter');
		$group = html_entity_decode($zf_filter->getEscaped('GROUP'));
		$userid = html_entity_decode($zf_filter->getEscaped('USER_ID'));
		$username = html_entity_decode($zf_filter->getEscaped('USER_NAME'));
		$description = html_entity_decode($zf_filter->getEscaped('description'));
		$active = html_entity_decode($zf_filter->getEscaped('ACTIVITY_TYPE'));
		$datefrom = html_entity_decode($zf_filter->getEscaped('PS_ACTIVITY'));
		$dateto = html_entity_decode($zf_filter->getEscaped('PS_ACTIVITY_END'));
		$clearfilter	= $this->_getParam('clearfilter');

		if ($filter == null && $clearfilter != 1) {
			$datefrom = (date("d/m/Y"));
			$dateto = (date("d/m/Y"));
			$this->view->fDateFrom  = (date("d/m/Y"));
			$this->view->fDateTo  = (date("d/m/Y"));
			//Zend_Debug::dump($this->view->fDateFrom); die;
		} else {

			if ($filter != NULL) {
				$datefrom 		= html_entity_decode($zf_filter->getEscaped('PS_ACTIVITY'));
				$dateto 		= html_entity_decode($zf_filter->getEscaped('PS_ACTIVITY_END'));
			} else if ($clearfilter == 1) {
				$datefrom 	= "";
				$dateto 	= "";
			}
		}

		if ($filter == null || $filter == TRUE) {
			$this->view->fDateTo    = $dateto;
			$this->view->fDateFrom  = $datefrom;
			//Zend_Debug::dump($custid); die;

			if (!empty($datefrom)) {
				$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
				$datefrom  = $FormatDate->toString($this->_dateDBFormat);
			}

			if (!empty($dateto)) {
				$FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
				$dateto    = $FormatDate->toString($this->_dateDBFormat);
				//Zend_Debug::dump($dateto); die;	
			}


			if (!empty($datefrom) && empty($dateto))
				$select2->where("DATE(B.LOG_DATE) >= " . $this->_db->quote($datefrom));

			if (empty($datefrom) && !empty($dateto))
				$select2->where("DATE(B.LOG_DATE) <= " . $this->_db->quote($dateto));

			if (!empty($datefrom) && !empty($dateto))
				$select2->where("DATE(B.LOG_DATE) between " . $this->_db->quote($datefrom) . " and " . $this->_db->quote($dateto));
		}

		if ($filter == TRUE || $csv || $pdf || $this->_request->getParam('print')) {
			//if($active) $select2->where("ACTION_DESC LIKE ".$active);
			if ($active) {
				$this->view->active = $active;
				$select2->where("B.ACTION_DESC LIKE " . $this->_db->quote($active));
			}

			if ($group) {
				$this->view->group = $group;
				$select2->where("C.BGROUP_ID LIKE " . $this->_db->quote('%' . $group . '%'));
			}

			if ($userid) {
				$this->view->userid = $userid;
				$select2->where("C.BUSER_ID LIKE " . $this->_db->quote('%' . $userid . '%'));
			}

			if ($username) {
				$this->view->username = $username;
				$select2->where("C.BUSER_NAME LIKE " . $this->_db->quote('%' . $username . '%'));
			}
			if ($description) {
				$this->view->description = $description;
				$select2->where("B.ACTION_FULLDESC LIKE " . $this->_db->quote('%' . $description . '%'));
			}

			if ($dataParamValue['LAST_SUGGESTED_START']) {
				$select2->where('DATE(B.LOG_DATE) >= DATE(' . $this->_db->quote($dataParamValue['LAST_SUGGESTED_START']) . ')');
				$select2->where('DATE(B.LOG_DATE) <= DATE(' . $this->_db->quote($dataParamValue['LAST_SUGGESTED_END']) . ')');
			}
		}


		// $select2->order($sortBy . ' ' . $sortDir);
		$select2->order('B.LOG_DATE DESC');
		$this->paging($select2);

		$this->view->fields = $fields;
		$this->view->filter = $filter;

		if (!empty($dataParamValue)) {
			$filterlistdata = array("Filter");
			foreach ($dataParamValue as $fil => $val) {
				$paramTrx = $fil . " - " . $val;
				array_push($filterlistdata, $paramTrx);
			}
		} else {
			$filterlistdata = array("Filter - None");
		}

		$this->view->data_filter = $filterlistdata;

		// echo $select2;die;
		if ($csv || $pdf || $this->_request->getParam('print')) {
			$result = $select2->query()->FetchAll();

			foreach ($result as $key => $value) {
				unset($select[$key]['ID']);

				$result[$key]["LOG_DATE"] = Application_Helper_General::convertDate($value["LOG_DATE"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
			}

			if ($csv) {

				// Application_Helper_General::writeLog('ADBU','Download CSV Backend User Activity Report');

				$this->_db->insert('T_BACTIVITY', array(
					'LOG_DATE'         => new Zend_Db_Expr('now()'),
					'USER_ID'          => $this->_userIdLogin,
					'USER_NAME'        => $this->_userNameLogin,
					'ACTION_DESC'      => 'ADBU',
					'ACTION_FULLDESC'  => 'Download CSV Bank User Activity Report',
				));

				$newArrLabel = [
					'No',
					'User Name',
					'Activity Date',
					'Activity Type',
					'Deskripsi'
				];

				$newArr = [];

				foreach ($result as $key => $value) {
					$newArr[] = [
						$key + 1,
						$value['BUSER_NAME'] . ' (' . $value['BUSER_ID'] . ')',
						$value['LOG_DATE'],
						$value['BPRIVI_DESC'],
						$value['ACTION_FULLDESC']
					];
				}

				// $this->_helper->download->csv(array($this->language->_('User ID'),$this->language->_('User Name'),$this->language->_('Activity Date'),$this->language->_('Activity Type'), $this->language->_('Description')),$result,null,$this->language->_('Backend User Activity Log'));
				$this->_helper->download->csv($newArrLabel, $newArr, null, $this->language->_('Bank User Activity Log'));
			} else if ($pdf) {
				Application_Helper_General::writeLog('ADBU', 'Download PDF Backend User Activity Report');
				// $this->_helper->download->pdf(array($this->language->_('User ID'),$this->language->_('User Name'),$this->language->_('Activity Date'),$this->language->_('Activity Type'), $this->language->_('Description')),$result,null,$this->language->_('Backend User Activity Log'));
				$this->_helper->download->pdf($fields, $result, null, $this->language->_('Backend User Activity Log'));
			} else if ($this->_request->getParam('print') == 1) {

				$zf_filter = new Zend_Filter_Input($filters, $validators, $dataParamValue, $this->_optionsValidator);
				// $filter 	= $zf_filter->getEscaped('filter');
				$filter 		= $this->_getParam('filter');
				$filterlistdatax = $this->_request->getParam('data_filter');


				if ($filterlistdatax != "Filter - None") {
					foreach ($filterlistdatax as $key => $val) {
						//echo $val;
						$arr = explode("-", $val);
						$colmn = ltrim(rtrim($arr[0]));
						$value = ltrim(rtrim($arr[1]));

						$arrFilter[$colmn] = $value;
					}

					$cgroup = html_entity_decode($arrFilter['GROUP']);
					$cuserid = html_entity_decode($arrFilter['USER_ID']);
					$cusername = html_entity_decode($arrFilter['USER_NAME']);
					$cdescription = html_entity_decode($arrFilter['description']);
					$cactive = html_entity_decode($arrFilter['ACTIVITY_TYPE']);
					$cdatefrom = html_entity_decode($arrFilter['PS_ACTIVITY']);
					$cdateto = html_entity_decode($arrFilter['PS_ACTIVITY_END']);

					$cdatefrom   = (Zend_Date::isDate($cdatefrom, $this->_dateDisplayFormat)) ?
						new Zend_Date($cdatefrom, $this->_dateDisplayFormat) :
						false;

					$cdateto     = (Zend_Date::isDate($cdateto, $this->_dateDisplayFormat)) ?
						new Zend_Date($cdateto, $this->_dateDisplayFormat) :
						false;

					if ($cactive) {
						$this->view->active = $cactive;
						$select2->where("B.ACTION_DESC LIKE " . $this->_db->quote($cactive));
					}

					if ($cgroup) {
						$this->view->group = $cgroup;
						$select2->where("C.BGROUP_ID LIKE " . $this->_db->quote('%' . $cgroup . '%'));
					}

					if ($cuserid) {
						$this->view->userid = $cuserid;
						$select2->where("C.BUSER_ID LIKE " . $this->_db->quote('%' . $cuserid . '%'));
					}

					if ($cusername) {
						$this->view->username = $cusername;
						$select2->where("C.BUSER_NAME LIKE " . $this->_db->quote('%' . $cusername . '%'));
					}
					if ($cdescription) {
						$this->view->description = $cdescription;
						$select2->where("B.ACTION_FULLDESC LIKE " . $this->_db->quote('%' . $cdescription . '%'));
					}
					$data = $this->_db->fetchall($select2);

					$field_export = array(

						'BUSER_NAME'        => array(
							'field' => 'BUSER_NAME',
							'label' => $this->language->_('User Name'),
							'sortable' => true
						),
						'B.LOG_DATE'     => array(
							'field' => 'B.LOG_DATE',
							'label' => $this->language->_('Activity Date'),
							'sortable' => true
						),
						'BPRIVI_DESC'  		=> array(
							'field' => 'BPRIVI_DESC',
							'label' => $this->language->_('Activity Type'),
							'sortable' => true
						),
						'ACTION_FULLDESC'   => array(
							'field' => 'ACTION_FULLDESC',
							'label' => $this->language->_('Description'),
							'sortable' => true
						)
					);
					foreach ($data as $pTrx) {
						$paramTrx = array(

							"BUSER_NAME"  				=> $pTrx['BUSER_NAME'],
							"BUSER_ID" 			=> $pTrx['BUSER_ID'],
							"LOG_DATE"  			=> $pTrx['LOG_DATE'],
							"BPRIVI_DESC"  			=> $pTrx['BPRIVI_DESC'],
							"ACTION_FULLDESC"  		=> $pTrx['ACTION_FULLDESC'],

						);
						$newData[] = $paramTrx;
					}

					$this->_forward('print', 'index', 'widget', array('data_content' => $newData, 'data_caption' => 'Customer List Report', 'data_header' => $field_export, 'data_filter' => $filterlistdatax));

					// $this->_forward('print', 'index', 'widget', array('data_content' => $result, 'data_caption' => 'Backend User Activity Log', 'data_header' => $fields));
					Application_Helper_General::writeLog('ADBU', 'Print Backend User 	Activity Report');
				}
			}
			// else
			// {
			// 	Application_Helper_General::writeLog('ADBU','View Backend User Activity Report');
			// }
		}

		$this->view->fields = $fields;
		$this->view->filter = $filter;

		$this->view->modulename = $this->_request->getModuleName();
		Application_Helper_General::writeLog('ADBU', 'View Backend User Activity Report');

		if (!empty($dataParamValue)) {
			$this->view->createdStart = $dataParamValue['LAST_SUGGESTED_START'];
			$this->view->createdEnd = $dataParamValue['LAST_SUGGESTED_END'];
			$this->view->actdateStart = $dataParamValue['PS_ACTIVITY'];
			$this->view->actdateEnd = $dataParamValue['PS_ACTIVITY_END'];

			unset($dataParamValue['PS_ACTIVITY_END']);
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
			// print_r($whereval);die;
		} else {
			$wherecol = array();
			$whereval = array();
		}

		$this->view->wherecol     = $wherecol;
		$this->view->whereval     = $whereval;
	}
}
