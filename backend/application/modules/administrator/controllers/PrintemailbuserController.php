<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Settings.php';
class Administrator_PrintemailbuserController extends Application_Main
{
	public function indexAction()
	{	

		$setting = new Settings();			  	
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
		$pw_hash = md5($enc_salt.$enc_pass);
		$rand = $this->_userIdLogin.date('dHis').$pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;
	
		$this->_helper->layout()->setLayout('newlayout');
		$fields = array(
						'BUSER_ID'      	=> array('field' => 'A.BUSER_ID',
											      'label' => $this->language->_('User'),
											      'sortable' => true),
						// 'BUSER_NAME'      	=> array('field' => 'BUSER_NAME',
						// 					      'label' => $this->language->_('Name'),
						// 					      'sortable' => true),
						'BUSER_ISNEW'           => array('field' => 'BUSER_ISNEW',
											      'label' => $this->language->_('New User'),
											      'sortable' => true),
						/*'methodemail'      	=> array('field' => 'methodemail',
											      'label' => $this->language->_('Password Emailed'),
											      'sortable' => true),
						'methodpostmail'  	=> array('field' => 'methodpostmail',
											      'label' => $this->language->_('Password Printed'),
											      'sortable' => true),*/
						'BUSER_ISEMAILPWD'     => array('field' => 'methodrecommend',
											      'label' => $this->language->_('Send Method'),
											      'sortable' => true));

	$filterlist = array("USER_ID","IS_NEW");
		
	$this->view->filterlist = $filterlist;
				      
		
				      
	$page = $this->_getParam('page');	
	$page = (Zend_Validate::is($page,'Digits'))? $page : 1;
    
    //validasi sort, jika input sort bukan ASC atau DESC
    $sortBy  = $this->_getParam('sortby');
    $sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
    $sortDir = $this->_getParam('sortdir');
	$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
		;
		
    $this->view->currentPage = $page;
    $this->view->sortBy = $sortBy;
    $this->view->sortDir = $sortDir;
		
		$filter = $this->_getParam('filter');
		$pdf = $this->_getParam('pdf');
		
		$select2 = $this->_db->select()
							->from(array('A' => 'M_BUSER'),array(	'*',
								'user'	=> new Zend_Db_Expr("CONCAT(A.BUSER_NAME , ' (' , A.BUSER_ID , ')  ' )"),
																	'methodemail' => new Zend_Db_Expr("(CASE A.BUSER_RPASSWORD_ISEMAILED 
																        	  								   	WHEN '1' THEN 'Yes' 
																        	  									WHEN '0' THEN 'No'  
																        	  									END)"),
																	'methodrecommend' => new Zend_Db_Expr("(CASE A.BUSER_ISEMAILPWD
																        	  								   	WHEN '1' THEN 'Email' 
																        	  									ELSE 'Posted Mail'
																        	  									END)"),
																	'methodpostmail' => new Zend_Db_Expr("(CASE A.BUSER_RPASSWORD_ISPRINTED 
																        	  								   	WHEN '1' THEN 'Yes' 
																        	  									WHEN '0' THEN 'No'  
																        	  									END)"),))
							->where("BUSER_RCHANGE = 1")
							->where("BUSER_RRESET = 0")
							->where("BUSER_STATUS NOT LIKE '3'");
		//Zend_Debug::dump($select2);die;
					        
		$filterArr = array(	'filter' => array('StripTags','StringTrim'),
	                       	'USER_ID'    => array('StripTags','StringTrim','StringToUpper'),
	                       	'IS_PRINT'  => array('StripTags','StringTrim'),
							'IS_MAIL'  => array('StripTags','StringTrim'),
							'IS_NEW'  => array('StripTags','StringTrim','StringToUpper'),
	                      );

		$dataParam = array("USER_ID","IS_PRINT","IS_MAIL","IS_NEW");
		$dataParamValue = array();
		
		$clean2 = array_diff( $this->_request->getParam('wherecol'),$dataParam); 
		$dataParam = array_diff( $this->_request->getParam('wherecol'),$clean2); 
// print_r($this->_request->getParam('wherecol'));
		foreach ($dataParam as $no => $dtParam)
		{
		
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
				// print_r($dataval);
				$order = 0;
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($value == "UPDATE_DATE"){
								$order--;
							}
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$order];
						}
						$order++;
					}
				
			}
		}


	    $zf_filter = new Zend_Filter_Input($filterArr,array(),$dataParamValue);
	    // $filter = $zf_filter->getEscaped('filter');
	    $filter 		= $this->_getParam('filter');
	    $userid = html_entity_decode($zf_filter->getEscaped('USER_ID'));
		$printed = html_entity_decode($zf_filter->getEscaped('IS_PRINT'));
		$emailed = html_entity_decode($zf_filter->getEscaped('IS_MAIL'));
		$new = html_entity_decode($zf_filter->getEscaped('IS_NEW'));

	    //Zend_Debug::dump($userid); die;
		if($filter == TRUE)
		{
		 
		//Zend_Debug::dump($select2); die;
	       
	    if($userid){
	       $this->view->userid = $userid;
	       $select2->where("BUSER_ID LIKE ".$this->_db->quote('%'.$userid.'%'));}
	    
	    if($printed != null || $printed != ""){
	       $this->view->printed = $printed;
	       $select2->where("A.BUSER_RPASSWORD_ISPRINTED LIKE ".$this->_db->quote($printed));}
	       
		if($emailed != null || $emailed != ""){
	       	$this->view->emailed = $emailed;
			$select2->where("A.BUSER_RPASSWORD_ISEMAILED LIKE ".$this->_db->quote($emailed));}
	       
		if($new){
	       $this->view->new = $new;
			if($new == "YES")
	       {
	       	$newcek = "1";
	       }
	       elseif($new == "NO")
	       {
	       	$newcek = "0";
	       }
	       $select2->where("BUSER_ISNEW LIKE ".$this->_db->quote($newcek));}

		}
		$select2->order($sortBy.' '.$sortDir);
		// echo $select2;die;
		$result=$select2->query()->fetchAll();
		
			
		if($pdf)
		{
			//die;	
			  $useridpdf = $this->_getParam('useridpdf');
			  $filename = $useridpdf.'_PDF';
	  			$select = $this->_db->select()
							->from(array('A' => 'M_BUSER'),array('BUSER_NAME','BUSER_ID','BUSER_CLEARTEXT_PASSWORD','BUSER_EMAIL','BUSER_ISNEW'));
				$select->where("BUSER_ID LIKE ".$this->_db->quote($useridpdf));
	 			$result = $this->_db->fetchRow($select);
	 			
			  	$setting = new Settings();
			  	
			  	$templateEmailMasterBankAddress = $setting->getSetting('master_bank_address');
				$templateEmailMasterBankAppName = $setting->getSetting('master_bank_app_name');
				$templateEmailMasterBankAppUrl = $setting->getSetting('master_bank_app_url');
				$templateEmailMasterBankCity = $setting->getSetting('master_bank_city');
				$templateEmailMasterBankCountry = $setting->getSetting('master_bank_country');
				$templateEmailMasterBankEmail = $setting->getSetting('master_bank_email');
				$templateEmailMasterBankEmail1 = $setting->getSetting('master_bank_email1');
				$templateEmailMasterBankFax = $setting->getSetting('master_bank_fax');
				$templateEmailMasterBankName = $setting->getSetting('master_bank_name');
				$templateEmailMasterBankProvince = $setting->getSetting('master_bank_province');
				$templateEmailMasterBankTelp = $setting->getSetting('master_bank_telp');
				$templateEmailMasterBankWebsite = $setting->getSetting('master_bank_website');
				$templateEmailMasterBankFullname = $result['BUSER_NAME'];
			  	
//				$password = strtoupper(substr(base64_decode($result['BUSER_CLEARTEXT_PASSWORD']),4, -4));
				$password 	= substr(base64_decode($result['BUSER_CLEARTEXT_PASSWORD']),4, -4) ;
			  	
				if($result['BUSER_ISNEW'] == 1)
				{
					$template = $setting->getSetting('btemplate_newuser');
				}
				else
				{
					$template = $setting->getSetting('btemplate_resetpwd');
				}
				
				$template = str_ireplace('[[buser_name]]',$result['BUSER_NAME'],$template);
	 			$template = str_ireplace('[[buser_login]]',$result['BUSER_ID'],$template);
	 			$template = str_ireplace('[[buser_cleartext_password]]',$password,$template);
	 			$template = str_ireplace('[[master_bank_name]]',$templateEmailMasterBankName,$template);
	 			$template = str_ireplace('[[USER_FULLNAME]]',$templateEmailMasterBankFullname,$template);
				$this->view->template = $template;
			  //Zend_Debug::dump($template); die;
			  $template = "<tr><td>".$template."</td></tr>";
   			  $this->_helper->download->pdf(null,null,null,$filename,$template); 
   			  Application_Helper_General::writeLog('ADRL','Downloading Print Pdf : User Id ('.$filename.')');     
		}
	//Zend_Debug::dump($result);die;
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		$this->paging($result);
		
		if(!$this->_request->isPost()){
				Application_Helper_General::writeLog('ADRL','Administrator > Print/email Password Backend User');
		}

		if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     
      }
	}
}
?>
