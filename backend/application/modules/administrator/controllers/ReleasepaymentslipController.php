<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Payment.php';
require_once 'Crypt/AESMYSQL.php';

class Administrator_ReleasepaymentslipController extends Application_Main
{
  public function indexAction() 
  { 

  	$setting = new Settings();			  	
	$enc_pass = $setting->getSetting('enc_pass');
	$enc_salt = $setting->getSetting('enc_salt');
	$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
	$pw_hash = md5($enc_salt.$enc_pass);
	$rand = $this->_userIdLogin.date('dHis').$pw_hash;
	$sessionNamespace->token 	= $rand;
	$password = $sessionNamespace->token; 
	$this->view->token = $sessionNamespace->token;
	
	$this->_helper->layout()->setLayout('newlayout');
		$select = $this->_db->select()
			->from(array('A' => 'M_CUSTOMER'),
				array('CUST_NAME', 'CUST_ID'))
				->order('CUST_NAME ASC')
				 -> query() ->fetchAll();
		$this->view->var=$select;
		
		$selectcur = $this->_db->select()
			->from(array('A' => 'M_CURRENCY'),
				array('CCY_ID'))
				->order('CCY_ID ASC')
				 -> query() ->fetchAll();
		$this->view->cur=$selectcur;

	//proses release
  	$release = $this->_getParam('release');
	
   	if($release)
    {
    	Application_Helper_General::writeLog('RLSP','Release Pending Payment Slip');
    	$date = (date("Y-m-d"));
    	$qrelease = $this->_db->select()
						->from(array('A' => 'T_PSLIP'),array('A.PS_NUMBER','A.CUST_ID','A.PS_CATEGORY', 'PS_TXCOUNT'))
						->where("A.PS_STATUS = '7'")
						->where("DATE(A.PS_EFDATE) = ".$this->_db->quote($date))
						->query()->fetchAll();
		$payslip = 0;
		$traslip = 0;
		$paysuccess = 0;
		$payfailed = 0;
		$invalidpayslip = 0;
		$trasuccess = 0;
		$trafailed = 0;
		$failedattempt = 0;
		
		$profileid = $this->_getParam('profileid');

		
		

		foreach($profileid as $psnumber)
		{
			// $AESMYSQL = new Crypt_AESMYSQL();
      		// $releaseid = $AESMYSQL->decrypt($this->_getParam($releaselist['PS_NUMBER']), $password);
      		
			// if($releaseid == '1')
			// {
			$releaselist = array();
				foreach ($qrelease as $key => $value) {
					if($value['PS_NUMBER'] == $ps_number){
						$releaselist = $qrelease[$key];
					}
				}

				$payslip++;
				$Payment = new Payment($psnumber, $releaselist['CUST_ID'], 'BACKEND');
				
//				$cekpayment = $this->_db->select()
//										->from(array('A' => 'T_PSLIP'),array())
//										->join(array('B' => 'T_TRANSACTION'), 'A.PS_NUMBER = B.PS_NUMBER',array('A.PS_NUMBER','tracount' => "COUNT('B.TRANSACTION_ID')"))
//										->where("PS_STATUS = '7'")
//										->where("DATE(A.PS_EFDATE) = ".$this->_db->quote($date))
//										->where("A.PS_NUMBER = '".($releaselist['PS_NUMBER']."'"))
//										->group('A.PS_NUMBER');
//				$cekpayment = $this->_db->fetchRow($cekpayment);
				
				$traslip = $traslip + $releaselist['PS_TXCOUNT'];
				
//				if(!$cekpayment)
//				{
//					$invalidpayslip++;
//				}
//				else
//				{
	
					$resultRelease = $Payment->releasePayment();
					
					$cektrans = $this->_db->select()
										->from(array('t' => 'T_TRANSACTION'),array( "trasuccess"   => "sum(if(t.TRA_STATUS = 3, 1, 0))", 
																					"trafailed"    => "sum(if(t.TRA_STATUS = 4, 1, 0))",
																					))
										->where("t.PS_NUMBER = '".($psnumber."'"));
					$cektrans = $this->_db->fetchRow($cektrans);
				
					
					if ($resultRelease['status'] == '00')
					{
						$paysuccess++;
						$trasuccess = $trasuccess + $cektrans['trasuccess'];
						$trafailed  = $trafailed  + $cektrans['trafailed'];
					}
					if ($resultRelease['status'] != '00' && $resultRelease['status'] != 'XT')
					{
						$payfailed++;
						$trasuccess = $trasuccess + $cektrans['trasuccess'];
						$trafailed  = $trafailed  + $cektrans['trafailed'];
					}
					if ($resultRelease['status'] == 'XT')
					{
						$failedattempt++;
					}
//				}
				
			// }
		}
		
		$this->view->success = 'success';
		$this->view->payslip = $payslip;
		$this->view->traslip = $traslip;
		$this->view->paysuccess = $paysuccess;
		$this->view->payfailed = $payfailed;
		$this->view->invalidpayslip = $invalidpayslip;
		$this->view->trasuccess = $trasuccess;
		$this->view->trafailed = $trafailed;
		$this->view->failedattempt = $failedattempt;

    }
	//-end-

    //proses select, filtering, paging
    $paymentSlip = $this->language->_('Payment Slip Number');
    $subject = $this->language->_('Subject');
    $company = $this->language->_('Company');
    $sourceAccount = $this->language->_('Source Account');
    $currency = $this->language->_('Currency');
    $totalAmount = $this->language->_('Total Amount');
    $paymentDate = $this->language->_('Payment Date');
    $createDaate = $this->language->_('Created Date');
    $lastUpdate = $this->language->_('Updated Date');
    
    $fields = array('slipnumb'   => array('field'    => 'B.PS_NUMBER',
                                        'label'    => $paymentSlip,
                                        'sortable' => true),
    
                    'slipsubject' => array('field'    => 'PS_SUBJECT',
                                        'label'    => $subject,
                                        'sortable' => true),
    
                    'custname'     => array('field'    => 'CUST_NAME',
                                        'label'    => $company,
                                        'sortable' => true),
    
    				'source'     => array('field'  => 'SOURCE_ACCOUNT',
                                        'label'    => $sourceAccount,
                                        'sortable' => true),
    
                    // 'currency'     => array('field'  => 'PS_CCY',
                    //                     'label'    => $currency,
                    //                     'sortable' => true),
    
    				'amount'     => array('field'  => 'PS_TOTAL_AMOUNT',
                                        'label'    => $totalAmount,
                                        'sortable' => true),
    
    				'paydate'     => array('field'  => 'PS_EFDATE',
                                        'label'    => $paymentDate,
                                        'sortable' => true),
    
    				'crtdate'     => array('field'  => 'PS_CREATED',
                                        'label'    => $createDaate,
                                        'sortable' => true),
    
                    'lstupdate'   => array('field'    => 'PS_UPDATED',
                                        'label'    => $lastUpdate,
                                        'sortable' => true),
                   );
	$filterlist = array('PS_CREATED','PS_EFDATE','PS_UPDATED','PS_SUBJECT','PS_NUMBER','SOURCE_ACCOUNT','COMP_ID','CCY');
		
	$this->view->filterlist = $filterlist;

    
    $page = $this->_getParam('page');
    $page = (Zend_Validate::is($page,'Digits'))? $page : 1;
    
    $pdf = $this->_getParam('pdf');
    $csv = $this->_getParam('csv');
    
    //validasi sort, jika input sort bukan ASC atau DESC
    $sortBy  = $this->_getParam('sortby');
    $sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
    $sortDir = $this->_getParam('sortdir');
	$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';

    $filterArr = array(	'filter' 		=> array('StripTags','StringTrim'),
    					'PS_SUBJECT'  	=> array('StripTags','StringTrim','StringToUpper'),
    					'PS_NUMBER'  	=> array('StripTags','StringTrim','StringToUpper'),
                       	'COMP_ID'  	=> array('StripTags','StringTrim','StringToUpper'),
    					'SOURCE_ACCOUNT'  		=> array('StripTags','StringTrim','StringToUpper'),
    					'CCY'  			=> array('StripTags','StringTrim','StringToUpper'),
    					'PS_CREATED' 	=> array('StripTags','StringTrim'),
    					'PS_CREATED_END' 		=> array('StripTags','StringTrim'),
    					'PS_EFDATE' 	=> array('StripTags','StringTrim'),
    					'PS_EFDATE_END' 		=> array('StripTags','StringTrim'),
    					'PS_UPDATED' 	=> array('StripTags','StringTrim'),
    					'PS_UPDATED_END' 		=> array('StripTags','StringTrim'),
                      );
                      
    $validator = array(	'filter' 		=> array(),
    					'PS_SUBJECT'  	=> array(),
    					'PS_NUMBER'  	=> array(),
                       	'COMP_ID'  		=> array(),
    					'SOURCE_ACCOUNT'		=> array(),
    					'CCY'  			=> array(),
    					'PS_CREATED' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
    					'PS_CREATED_END' 		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
    					'PS_EFDATE' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
    					'PS_EFDATE_END' 		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
    					'PS_UPDATED' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
    					'PS_UPDATED_END' 		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
                      );

    $dataParam = array('PS_SUBJECT','PS_NUMBER','COMP_ID','SOURCE_ACCOUNT','CCY');
		$dataParamValue = array();
		
$clean2 = array_diff( $this->_request->getParam('wherecol'),$dataParam); 
$dataParam = array_diff( $this->_request->getParam('wherecol'),$clean2); 
// print_r($dataParam);die;

		// print_r($output);die;
		// print_r($this->_request->getParam('wherecol'));
		foreach ($dataParam as $no => $dtParam)
		{
		
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
				// print_r($dataval);
				$order = 0;
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($value == "PS_CREATED" || $value == "PS_UPDATED" || $value == "PS_EFDATE"){
								$order--;
							}
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$order];
						}
						$order++;
					}
				
			}
		}
		// print_r($dataParamValue);
		// die;	
		// print_r($this->_request->getParam('whereval'));die;
			if(!empty($this->_request->getParam('createdate'))){
				$createarr = $this->_request->getParam('createdate');
					$dataParamValue['PS_CREATED'] = $createarr[0];
					$dataParamValue['PS_CREATED_END'] = $createarr[1];
			}
			if(!empty($this->_request->getParam('updatedate'))){
				$updatearr = $this->_request->getParam('updatedate');
					$dataParamValue['PS_UPDATED'] = $updatearr[0];
					$dataParamValue['PS_UPDATED_END'] = $updatearr[1];
			}

			if(!empty($this->_request->getParam('efdate'))){
				$efdatearr = $this->_request->getParam('efdate');
					$dataParamValue['PS_EFDATE'] = $efdatearr[0];
					$dataParamValue['PS_EFDATE_END'] = $efdatearr[1];
			}
                    // print_r($dataParamValue);  
    $zf_filter = new Zend_Filter_Input($filterArr,$validator,$dataParamValue);
    // $filter 		= $zf_filter->getEscaped('filter');
 	$filter 		= $this->_getParam('filter');

    $clearfilter 		= $this->_getParam('clearfilter');
    
    $slipnumb		= html_entity_decode($zf_filter->getEscaped('PS_NUMBER'));
    $slipsubject    = html_entity_decode($zf_filter->getEscaped('PS_SUBJECT'));
	$custname   	= html_entity_decode($zf_filter->getEscaped('CUST_ID'));
	$source     	= html_entity_decode($zf_filter->getEscaped('SOURCE_ACCOUNT'));
	$scur   		= html_entity_decode($zf_filter->getEscaped('CCY'));
	$datefrom   	= html_entity_decode($zf_filter->getEscaped('PS_CREATED'));
	$dateto   		= html_entity_decode($zf_filter->getEscaped('PS_CREATED_END'));
	$datefrom2   	= html_entity_decode($zf_filter->getEscaped('PS_UPDATED'));
	$dateto2   		= html_entity_decode($zf_filter->getEscaped('PS_UPDATED_END'));
	$datefrom3   	= html_entity_decode($zf_filter->getEscaped('PS_EFDATE'));
	$dateto3   		= html_entity_decode($zf_filter->getEscaped('PS_EFDATE_END'));
    
    $this->view->currentPage = $page;
    $this->view->sortBy = $sortBy;
    $this->view->sortDir = $sortDir;
    
   		$date = (date("Y-m-d"));
        $select2 = $this->_db->select()->distinct()
					        ->from(array('B' => 'T_PSLIP'),array())
					        ->join(array('C' => 'M_CUSTOMER'), 'B.CUST_ID = C.CUST_ID',array())
					        ->join(array('D' => 'T_TRANSACTION'), 'B.PS_NUMBER = D.PS_NUMBER',array('B.PS_NUMBER',
					        																		'B.PS_SUBJECT',
					        																		'C.CUST_NAME',
					        																		'D.SOURCE_ACCOUNT',
					        																		'B.PS_CCY',
					        																		'B.PS_TOTAL_AMOUNT',
					        																		'PS_EFDATE'=>'B.PS_EFDATE',
					        																		'B.PS_CREATED',
					        																		'B.PS_UPDATED',));
					        
		$select2 -> where("(PS_STATUS = '7' AND DATE(PS_EFDATE) = ".$this->_db->quote($date).")");
//Zend_Debug::dump($source);die;
		if($clearfilter == TRUE){
			$this->view->fDateTo3    = '';
			$this->view->fDateFrom3  = '';
		}
	//	else{
	  		if($filter == null)
			{	$datefrom3 = (date("d/m/Y"));
				$dateto3 = (date("d/m/Y"));
				$this->view->fDateFrom3  = (date("d/m/Y"));
				$this->view->fDateTo3  = (date("d/m/Y"));
			//Zend_Debug::dump($this->view->fDateFrom); die;
			}
			
			if($filter == null || $filter ==TRUE)
			{
				$this->view->fDateTo3    = $dateto3;
				$this->view->fDateFrom3  = $datefrom3;
			if(!empty($datefrom3))
		            {
		            	$FormatDate = new Zend_Date($datefrom3, $this->_dateDisplayFormat);
						$datefrom3  = $FormatDate->toString($this->_dateDBFormat);	
		            }
		            
		    if(!empty($dateto3))
		            {
		            	$FormatDate = new Zend_Date($dateto3, $this->_dateDisplayFormat);
						$dateto3    = $FormatDate->toString($this->_dateDBFormat);
						//Zend_Debug::dump($dateto); die;	
		            }
			
			if(!empty($datefrom3) && empty($dateto3))
		            $select2->where("DATE(PS_EFDATE) >= ".$this->_db->quote($datefrom3));
		            
		   	if(empty($datefrom3) && !empty($dateto3))
		            $select2->where("DATE(PS_EFDATE) <= ".$this->_db->quote($dateto3));
		            
		    if(!empty($datefrom3) && !empty($dateto3))
		            $select2->where("DATE(PS_EFDATE) between ".$this->_db->quote($datefrom3)." and ".$this->_db->quote($dateto3));
			}
			//print_r($filter);die;
			if($filter == TRUE)
			{
			if(!empty($datefrom))
		            {	            	
						$this->view->fDateFrom  = $datefrom;
		            	$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
						$datefrom  = $FormatDate->toString($this->_dateDBFormat);	
		            }
		            
		    if(!empty($dateto))
		            {
		            	$this->view->fDateTo    = $dateto;
		            	$FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
						$dateto    = $FormatDate->toString($this->_dateDBFormat);
						//Zend_Debug::dump($dateto); die;	
		            }
		     
			if(!empty($datefrom2))
		            {
		            	$this->view->fDateFrom2  = $datefrom2;
		            	$FormatDate = new Zend_Date($datefrom2, $this->_dateDisplayFormat);
						$datefrom2  = $FormatDate->toString($this->_dateDBFormat);	
		            }
		            
		    if(!empty($dateto2))
		            {
		            	$this->view->fDateTo2    = $dateto2;
		            	$FormatDate = new Zend_Date($dateto2, $this->_dateDisplayFormat);
						$dateto2    = $FormatDate->toString($this->_dateDBFormat);
						//Zend_Debug::dump($dateto); die;	
		            }
			
			if(!empty($datefrom) && empty($dateto))
		            $select2->where("DATE(PS_CREATED) >= ".$this->_db->quote($datefrom));
		            
		   	if(empty($datefrom) && !empty($dateto))
		            $select2->where("DATE(PS_CREATED) <= ".$this->_db->quote($dateto));
		            
		    if(!empty($datefrom) && !empty($dateto))
		            $select2->where("DATE(PS_CREATED) between ".$this->_db->quote($datefrom)." and ".$this->_db->quote($dateto));			        
	
	        if(!empty($datefrom2) && empty($dateto2))
		            $select2->where("DATE(PS_UPDATED) >= ".$this->_db->quote($datefrom2));
		            
		   	if(empty($datefrom2) && !empty($dateto2))
		            $select2->where("DATE(PS_UPDATED) <= ".$this->_db->quote($dateto2));
		            
		    if(!empty($datefrom2) && !empty($dateto2))
		            $select2->where("DATE(PS_UPDATED) between ".$this->_db->quote($datefrom2)." and ".$this->_db->quote($dateto2));
	
				if($slipnumb)$select2->where('UPPER(B.PS_NUMBER) LIKE '.$this->_db->quote('%'.strtoupper($slipnumb).'%'));
				if($slipsubject)$select2->where('UPPER(PS_SUBJECT) LIKE '.$this->_db->quote('%'.strtoupper($slipsubject).'%'));
				if($custname)$select2->where('UPPER(C.CUST_ID) LIKE '.$this->_db->quote('%'.strtoupper($custname).'%'));
				if($source)$select2->where('UPPER(SOURCE_ACCOUNT) LIKE '.$this->_db->quote('%'.strtoupper($source).'%'));
				if($scur)$select2->where('UPPER(PS_CCY) LIKE '.$this->_db->quote('%'.strtoupper($scur).'%'));
				
	
				$this->view->custname = $custname;
				$this->view->slipnumb = $slipnumb;
				$this->view->slipsubject = $slipsubject;
				$this->view->source = $source;
				$this->view->scur = $scur;
	        }
        
	//	}
        
        
    
		$select2->order($sortBy.' '.$sortDir);
	    // END proses pengambilan data filter,display all,sorting
			// echo $select2; die;
	    $result = $select2->query()->FetchAll();
	    //Zend_Debug::dump($result);die;
	    $this->paging($result);
	    // print_r($filter);die;
	    $this->view->fields = $fields;
	    $this->view->filter = $filter;
	    $this->view->statusCode = array_flip($this->_masterglobalstatus['code']);
	    $this->view->statusDesc = $this->_masterglobalstatus['desc'];
	    $this->view->modulename = $this->_request->getModuleName();

  		if($pdf || $csv || $this->_request->getParam('print'))
	    {
	    	foreach($result as $key=>$value)
			{
				$result[$key]["PS_CREATED"] = Application_Helper_General::convertDate($value["PS_CREATED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
				$result[$key]["PS_EFDATE"]  = Application_Helper_General::convertDate($value["PS_EFDATE"],"dd MMM YYYY",$this->defaultDateFormat);
				$result[$key]["PS_UPDATED"] = Application_Helper_General::convertDate($value["PS_UPDATED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
				$result[$key]["PS_TOTAL_AMOUNT"] = $value["PS_CCY"].' '.Application_Helper_General::displayMoney($value["PS_TOTAL_AMOUNT"]);
				unset($result[$key]["PS_CCY"]);

			}	

			// echo "<pre>";
			// var_dump($result);
			// die();

		    if($csv)
			{
				Application_Helper_General::writeLog('RPFL','Download CSV Release Pending Payment Slip');
				$header  = Application_Helper_Array::simpleArray($fields, "label");
				$this->_helper->download->csv($header,$result,null,'Release Payment Slip');
			}
			
			if($pdf)
			{
				Application_Helper_General::writeLog('RPFL','Download PDF Release Pending Payment Slip');
				$header  = Application_Helper_Array::simpleArray($fields, "label");
				$this->_helper->download->pdf($header,$result,null,'Release Payment Slip');
			}
	    	if($this->_request->getParam('print') == 1){
				$this->_forward('print', 'index', 'widget', array('data_content' => $result, 'data_caption' => 'Release Payment Slip', 'data_header' => $fields));
				Application_Helper_General::writeLog('RPFL','Print Release Pending Payment Slip');
			}
	    }
	    else
	    {
	    	if(!$release)
		  	{
		  		//Application_Helper_General::writeLog('RPFL','Viewing Release Pending Payment FSCM');
		  		Application_Helper_General::writeLog('RLLS','Viewing Release Pending Payment Slip');
		  	}
	    }

	    // print_r($dataParamValue);die;
	  
	    if(!empty($dataParamValue)){
	    		$this->view->createStart = $dataParamValue['PS_CREATED'];
	    		$this->view->createEnd = $dataParamValue['PS_CREATED_END'];
	    		$this->view->updateStart = $dataParamValue['PS_UPDATED'];
	    		$this->view->updateEnd = $dataParamValue['PS_UPDATED_END'];
	    		$this->view->efdateStart = $dataParamValue['PS_EFDATE'];
	    		$this->view->efdateEnd = $dataParamValue['PS_EFDATE_END'];

	    	  	unset($dataParamValue['PS_CREATED_END']);
			    unset($dataParamValue['PS_EFDATE_END']);
			    unset($dataParamValue['PS_UPDATED_END']);
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     // print_r($whereval);die;
      }
	}

	public function notifikasiAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		
		//proses release
	  	$release = $this->_getParam('release');
		
	   	if($release)
	    {
	    	Application_Helper_General::writeLog('RLSP','Release Pending Payment Slip');
	    	$date = (date("Y-m-d"));
	    	$qrelease = $this->_db->select()
							->from(array('A' => 'T_PSLIP'),array('A.PS_NUMBER','A.CUST_ID','A.PS_CATEGORY', 'PS_TXCOUNT'))
							->where("A.PS_STATUS = '7'")
							->where("DATE(A.PS_EFDATE) = ".$this->_db->quote($date))
							->query()->fetchAll();
			$payslip = 0;
			$traslip = 0;
			$paysuccess = 0;
			$payfailed = 0;
			$invalidpayslip = 0;
			$trasuccess = 0;
			$trafailed = 0;
			$failedattempt = 0;
			
			$profileid = $this->_getParam('profileid');

		
		

		foreach($profileid as $psnumber)
		{
			// $AESMYSQL = new Crypt_AESMYSQL();
      		// $releaseid = $AESMYSQL->decrypt($this->_getParam($releaselist['PS_NUMBER']), $password);
      		
			// if($releaseid == '1')
			// {
				$releaselist = array();
					foreach ($qrelease as $key => $value) {
						if($value['PS_NUMBER'] == $psnumber){
							$releaselist = $qrelease[$key];
						}
					}
				// $AESMYSQL = new Crypt_AESMYSQL();
	      		// $releaseid = $AESMYSQL->decrypt($this->_getParam($releaselist['PS_NUMBER']), $password);
	      		
				// if($releaseid == '1')
				// {
					$payslip++;
					//var_dump($releaselist);die;
					$Payment = new Payment($psnumber, $releaselist['CUST_ID'], 'BACKEND');
					
	//				$cekpayment = $this->_db->select()
	//										->from(array('A' => 'T_PSLIP'),array())
	//										->join(array('B' => 'T_TRANSACTION'), 'A.PS_NUMBER = B.PS_NUMBER',array('A.PS_NUMBER','tracount' => "COUNT('B.TRANSACTION_ID')"))
	//										->where("PS_STATUS = '7'")
	//										->where("DATE(A.PS_EFDATE) = ".$this->_db->quote($date))
	//										->where("A.PS_NUMBER = '".($releaselist['PS_NUMBER']."'"))
	//										->group('A.PS_NUMBER');
	//				$cekpayment = $this->_db->fetchRow($cekpayment);
					
					$traslip = $traslip + $releaselist['PS_TXCOUNT'];
					
	//				if(!$cekpayment)
	//				{
	//					$invalidpayslip++;
	//				}
	//				else
	//				{
		//die('here');
		
						$resultRelease = $Payment->releasePayment();
						
						$cektrans = $this->_db->select()
											->from(array('t' => 'T_TRANSACTION'),array( "trasuccess"   => "sum(if(t.TRA_STATUS = 3, 1, 0))", 
																						"trafailed"    => "sum(if(t.TRA_STATUS = 4, 1, 0))",
																						))
											->where("t.PS_NUMBER = '".($psnumber."'"));
						$cektrans = $this->_db->fetchRow($cektrans);
					
						
						if ($resultRelease['status'] == '00')
						{
							$paysuccess++;
							$trasuccess = $trasuccess + $cektrans['trasuccess'];
							$trafailed  = $trafailed  + $cektrans['trafailed'];
						}
						if ($resultRelease['status'] != '00' && $resultRelease['status'] != 'XT')
						{
							$payfailed++;
							$trasuccess = $trasuccess + $cektrans['trasuccess'];
							$trafailed  = $trafailed  + $cektrans['trafailed'];
						}
						if ($resultRelease['status'] == 'XT')
						{
							$failedattempt++;
						}
	//				}
					
				// }
			}
			
			$this->view->success = 'success';
			$this->view->payslip = $payslip;
			$this->view->traslip = $traslip;
			$this->view->paysuccess = $paysuccess;
			$this->view->payfailed = $payfailed;
			$this->view->invalidpayslip = $invalidpayslip;
			$this->view->trasuccess = $trasuccess;
			$this->view->trafailed = $trafailed;
			$this->view->failedattempt = $failedattempt;

	    }
		//-end-
	}
}


