<?php


require_once 'Zend/Controller/Action.php';


class biller_DetailController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');   
		$providerID 	= $this->_getParam('providerid');
		if(empty($providerID))
		{
			header('location: /biller');exit();
		}
		$model 			 = new biller_Model_Biller();
		$this->view->providerData 	 = $model->getDetailProvider($providerID);
		$this->view->changestatus 	 = $model->isRequestChange($providerID);
		$param['PROVIDER_ID'] = $providerID;
		$this->view->billerAcc 	 = $model->getBillerAcc($param);
		$this->setbackURL('/'.$this->_request->getModuleName().'/detail/index/providerid/'.$providerID);
		Application_Helper_General::writeLog('VBIL','View Detail Biller');
	}

	public function activedeactiveAction()
	{


		$providerID 	= $this->_getParam('providerid');
		$status 			= $this->_getParam('status');

		if(empty($providerID) || !in_array($status,array(1,2)))
		{
			header('location: /biller');exit();
		}
		$model 			 = new biller_Model_Biller();
		$providerData 	 = $model->getDetailProvider($providerID);
		if(empty($providerData))
		{
			header('location: /biller');exit();
		}
		else
		{
			if($status == 2)
			{
				$code = strtoupper($this->_changeType['code']['suspend']);
				$info2 = 'Suspend Biller';
			}
			elseif($status == 1)
			{
				$code = strtoupper($this->_changeType['code']['unsuspend']);
				$info2 = 'Unsuspend Biller';
			}
			$this->_db->beginTransaction();
			try
			{
				$providerID 	 = $this->_getParam('providerID');
				$providerName 	 = $this->_getParam('providerName');
				$info = 'Biller List';
				$change_id = $this->suggestionWaitingApproval($info,$info2,$code,null,'M_SERVICE_PROVIDER','TEMP_SERVICE_PROVIDER',$providerData['PROVIDER_CODE'],$providerData['PROVIDER_NAME'],$providerData['PROVIDER_CODE']);

				unset($providerData['CHARGES']);
				$data= $providerData+array(
																	'CHANGES_ID' 			=> $change_id,
																	'PROVIDER_SUGGESTED' 	=> new Zend_Db_Expr('GETDATE()'),
																	'PROVIDER_SUGGESTEDBY' 	=> $this->_userIdLogin
																);

				$getServiceType 	 = $model->getServiceType();
				if(!empty($getServiceType))
				{
					foreach($getServiceType as $value)
					{
						$arrServiceType[trim($value['SERVICE_NAME'])] = $value['SERVICE_ID'];
					}
				}

				$arrProviderType = array('Payment' => 1,'Purchase' =>2);
				$data['PROVIDER_TYPE']	= $arrProviderType[$providerData['PROVIDER_TYPE']];
				$data['SERVICE_TYPE']		= $arrServiceType[trim($providerData['SERVICE_TYPE'])];
				$data['PROVIDER_STATUS'] = $status;

				$model->insertTemp($data);
				Application_Helper_General::writeLog('UBIL',''.$info2. ' '. $providerData['PROVIDER_CODE']);

				if($error == 0)
				{
					$this->_db->commit();
					$this->setbackURL('/'.$this->_request->getModuleName());
					$this->_redirect('/notification/submited/index');
				}
			}
			catch(Exception $e)
			{
				$this->_db->rollBack();
			}
		}
	}

}
