<?php


require_once 'Zend/Controller/Action.php';


class inhousecharge_MonthlyfeeController extends Application_Main
{
	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');
		$custid = $this->_getParam('custid');
		
		$select = $this->_db->select()
			->from(array('M_CUSTOMER'));
		
		$select -> where("CUST_ID LIKE ".$this->_db->quote($custid));
		$result = $this->_db->FetchRow($select);
		$this->view->result = $result;

		$monthlyfeestatus = $result["CUST_MONTHLYFEE_STATUS"];
		
		if($monthlyfeestatus == 0)
		{
			$this->view->monthlyfeestatus = 'Disabled';
		}
		if($monthlyfeestatus == 1)
		{
			$this->view->monthlyfeestatus = 'Enabled';
		}
		
		
		$select2 = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER'),array('*'))
					        ->joinleft(array('B' => 'M_CUSTOMER_ACCT'), 'A.CUST_ID = B.CUST_ID',array('*'))
					        ->joinleft(array('C' => 'M_CHARGES_WITHIN'), 'B.ACCT_NO = C.ACCT_NO',array('A.CUST_ID', 'A.CUST_NAME', 'B.ACCT_NO', 'C.AMOUNT'));
		$result2 = $select2->query()->FetchAll();
		$this->view->result2 = $result2;
		
		$select3 = $this->_db->select()
							->from('T_GLOBAL_CHANGES');
		$select3 -> where("KEY_FIELD LIKE ".$this->_db->quote($custid));
		$select3 -> where("DISPLAY_TABLENAME = 'Company Charges Setting'");
		$select3 -> where("CHANGES_STATUS = 'WA'");
		$cek = $select3->query()->FetchAll();

		//Zend_Debug::dump($cek); die;	
		if($cek)
		{
			$docErr = "*No changes allowed for this record while awaiting approval for previous change";	
			$this->view->error = $docErr;
			$this->view->changestatus = "disabled";
		}
		
		Application_Helper_General::writeLog('CHLS','View Company charges detail (choose monthly fee charges) ('.$custid.')');
	}
}
