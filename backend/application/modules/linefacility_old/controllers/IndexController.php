<?php
require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';
require_once("Service/Account.php");

class linefacility_IndexController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$model = new linefacility_Model_Linefacility();

		$setting 	= new Settings();
		$enc_pass 	= $setting->getSetting('enc_pass');
		$enc_salt 	= $setting->getSetting('enc_salt');

		$sessToken 			= new Zend_Session_Namespace('Tokenenc');
		$password_hash 		= md5($enc_salt . $enc_pass);
		$rand 				= $this->_userIdLogin . date('YmdHis') . $password_hash;
		$sessToken->token 	= $rand;
		$this->view->token 	= $sessToken->token;

		$filter 		= $this->_getParam('filter');
		$filter_clear 	= $this->_getParam('filter_clear');
		$filterlist = array("COMP_CODE", "COMP_NAME", "COLLECTIBILITY_CODE");

		$this->view->filterlist = $filterlist;

		$headers = array(
			'CUST_ID'  => array(
				'field' => 'CUST_ID',
				'label' => 'Company ID'
			),
			'CUST_NAME'  => array(
				'field' => 'CUST_NAME',
				'label' => 'Company Name'
			),
			'CUST_SEGMENT'  => array(
				'field' => 'CUST_SEGMENT',
				'label' => 'Segment'
			),
			'COLLECTIBILITY_CODE'     => array(
				'field' => 'COLLECTIBILITY_CODE',
				'label' => 'Collectibility'
			),
			'PKS_EXP_DATE'     => array(
				'field' => 'PKS_EXP_DATE',
				'label' => 'PKS Exp Date'
			),
			'LAST_APPROVED'     => array(
				'field' => 'LAST_APPROVED',
				'label' => 'Last Approved'
			),
			'PLAFOND_LIMIT'     => array(
				'field' => 'PLAFOND_LIMIT',
				'label' => 'Plafond Limit'
			),
			'LIMIT_USED'     => array(
				'field' => 'LIMIT_USED',
				'label' => 'Limit Used'
			),
			'STATUS'     => array(
				'field' => 'STATUS',
				'label' => 'Status'
			)

		);
		$dataParam = array("COMP_CODE", "COMP_NAME", "COLLECTIBILITY_CODE");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam) {

			// print_r($dtParam);die;
			if (!empty($this->_request->getParam('wherecol'))) {
				$dataval = $this->_request->getParam('whereval');
				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if ($dtParam == $value) {
						$dataParamValue[$dtParam] = $dataval[$key];
					}
				}
			}

			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}

		//get filtering param
		$filters = array('*' => array('StringTrim', 'StripTags'));

		$validators = array('*' => array('allowEmpty' => true));

		$optionValidators = array('breakChainOnFailure' => false);

		$zf_filter = new Zend_Filter_Input($filters, $validators, $dataParamValue,  $optionValidators);

		$filter = ($zf_filter->filter) ? $zf_filter->filter : $this->_request->getParam('filter');
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');

		$CompanyCode 	= $filterParam['fCompanyCode'] = $this->_request->getParam('fCompanyCode');
		$CompanyName 	= $filterParam['fCompanyName'] = $this->_request->getParam('fCompanyName');
		$CollectCode 	= $filterParam['fCollectCode'] = $this->_request->getParam('fCollectCode');

		$segmentationArr = [
			1 => 'CORPORATE',
			2 => 'COMMERCIAL',
			3 => 'SME',
			4 => 'FINANCIAL INSTITUTION'
		];

		$statusArr = [
			1 => 'Approved',
			2 => 'Terminated',
			3 => 'Expired',
			4 => 'Freeze Submission'
		];
		$this->view->collectCodeArr = Application_Helper_Array::listArray($model->getCreditQuality(), 'CODE', 'DESCRIPTION');


		// $data = $model->getData($filterParam, $filter);
		$select = $this->_db->select()
			->from(
				array('A' => 'M_CUST_LINEFACILITY'),
				array('ID', 'CUST_ID', 'CUST_SEGMENT', 'PKS_EXP_DATE', 'LAST_APPROVED', 'PLAFOND_LIMIT', 'STATUS')
			)
			->joinLeft(
				array('B' => 'M_CUSTOMER'),
				'B.CUST_ID = A.CUST_ID',
				array('CUST_NAME', 'COLLECTIBILITY_CODE')
			);



		if ($filter == TRUE) {
			if ($filterParam['fCompanyCode']) {
				$select->where('A.CUST_ID LIKE ' . $this->_db->quote('%' . $filterParam['fCompanyCode'] . '%'));
			}
			if ($filterParam['fCompanyName']) {
				$select->where('B.CUST_NAME LIKE ' . $this->_db->quote('%' . $filterParam['fCompanyName'] . '%'));
			}
			if ($filterParam['fCollectCode']) {
				$select->where('B.COLLECTIBILITY_CODE = ?', $filterParam['fCollectCode']);
			}
		}

		if ($filter == TRUE && $filter_clear == NULL) {
			$fCompanyCode 	= $filterParam['fCompanyCode'] = $this->_request->getParam('fCompanyCode');
			$fCompanyName 	= $filterParam['fCompanyName'] = $this->_request->getParam('fCompanyName');
			$fCollectCode 	= $filterParam['fCollectCode'] = $this->_request->getParam('fCollectCode');
			// $fUploadDateFrom 	= html_entity_decode($zf_filter->getEscaped('fUploadDateFrom'));
			// $fUploadDateTo 		= html_entity_decode($zf_filter->getEscaped('fUploadDateTo'));
		}
		if ($filter == TRUE || $csv || $pdf || $this->_request->getParam('print')) {
			$header = Application_Helper_Array::simpleArray($headers, 'label');
			if ($CompanyCode) {
				$select->where("UPPER(A.CUST_ID) LIKE " . $this->_db->quote($CompanyCode));
			}

			if ($CompanyName) {
				$select->where("UPPER(B.CUST_NAME) LIKE " . $this->_db->quote('%' . $CompanyName . '%'));
			}

			if ($CollectCode) {
				$select->where("UPPER(B.COLLECTIBILITY_CODE) LIKE " . $this->_db->quote('%' . $CollectCode . '%'));
			}
		}
		// if($fUploadDateFrom){
		// 	$FormatDate = new Zend_Date($fUploadDateFrom, $this->_dateDisplayFormat);
		// 	$fUploadDateFrom = $FormatDate->toString($this->_dateDBFormat);	
		// }

		// if($fUploadDateTo){
		// 	$FormatDate = new Zend_Date($fUploadDateTo, $this->_dateDisplayFormat);
		// 	$fUploadDateTo = $FormatDate->toString($this->_dateDBFormat);	
		// }
		$this->paging($select);
		$this->view->filter = $filter;

		if (!empty($dataParamValue)) {
			$filterlistdata = array("Filter");
			foreach ($dataParamValue as $fil => $val) {
				$paramTrx = $fil . " - " . $val;
				array_push($filterlistdata, $paramTrx);
			}
		} else {
			$filterlistdata = array("Filter - None");
		}

		$this->view->data_filter = $filterlistdata;
		// echo $select;die;
		if ($csv || $pdf || $this->_request->getParam('print')) {
			$arr = $this->_db->fetchAll($select);
			foreach ($select as $key => $value) {
				unset($select[$key]['ID']);
			}

			if ($csv) {
				$field_export = array(
					'CUST_ID'  => array(
						'field' => 'CUST_ID',
						'label' => 'Company ID'
					),
					'CUST_NAME'  => array(
						'field' => 'CUST_NAME',
						'label' => 'Company Name'
					),
					'CUST_SEGMENT'  => array(
						'field' => 'CUST_SEGMENT',
						'label' => 'Segment'
					),
					'COLLECTIBILITY_CODE'     => array(
						'field' => 'COLLECTIBILITY_CODE',
						'label' => 'Collectibility'
					),
					'PKS_EXP_DATE'     => array(
						'field' => 'PKS_EXP_DATE',
						'label' => 'PKS Exp Date'
					),
					'LAST_APPROVED'     => array(
						'field' => 'LAST_APPROVED',
						'label' => 'Last Approved'
					),
					'PLAFOND_LIMIT'     => array(
						'field' => 'PLAFOND_LIMIT',
						'label' => 'Plafond Limit'
					),
					'LIMIT_USED'     => array(
						'field' => 'LIMIT_USED',
						'label' => 'Limit Used'
					),
					'STATUS'     => array(
						'field' => 'STATUS',
						'label' => 'Status'
					)



				);
				foreach ($arr as $pTrx) {
					$paramTrx = array(

						"CUST_ID"  				=> $pTrx['CUST_ID'],
						"CUST_NAME" 			=> $pTrx['CUST_NAME'],
						"CUST_SEGMENT"  			=> $pTrx['CUST_SEGMENT'],
						"COLLECTIBILITY_CODE"  			=> $pTrx['COLLECTIBILITY_CODE'],
						"PKS_EXP_DATE"  		=> $pTrx['PKS_EXP_DATE'],
						"LAST_APPROVED"  		=> $pTrx['LAST_APPROVED'],
						"PLAFOND_LIMIT"  		=> $pTrx['PLAFOND_LIMIT'],
						"LIMIT_USED"  		=> $pTrx['LIMIT_USED'],
						"STATUS"  		=> $pTrx['STATUS'],
					);
					$newData[] = $paramTrx;
				}
				$this->_helper->download->csv($header, $newData, null, $this->language->_('Line Facility List Report'));
			} else if ($pdf) {
				$this->_helper->download->pdf($header, $arr, null, $this->language->_('Line Facility List Report'));
			} else if ($this->_request->getParam('print') == 1) {
				$zf_filter = new Zend_Filter_Input($filters, $validators, $dataParamValue,  $optionValidators);

				$filter 		= $this->_getParam('filter');
				$filterlistdatax = $this->_request->getParam('data_filter');

				if ($filterlistdatax != "Filter - None") {
					foreach ($filterlistdatax as $key => $val) {
						//echo $val;
						$arr = explode("-", $val);
						$colmn = ltrim(rtrim($arr[0]));
						$value = ltrim(rtrim($arr[1]));

						$arrFilter[$colmn] = $value;
					}

					$cCompanyCode 	= $filterParam['fCompanyCode'] = $this->_request->getParam('fCompanyCode');
					$cCompanyName 	= $filterParam['fCompanyName'] = $this->_request->getParam('fCompanyName');
					$cCollectCode 	= $filterParam['fCollectCode'] = $this->_request->getParam('fCollectCode');

					if ($cCompanyCode) {
						$select->where('UPPER(A.CUST_ID)=' . $this->_db->quote(strtoupper($cCompanyCode)));
					}
					if ($cCompanyName) {
						$select->where('UPPER(B.CUST_NAME) LIKE ' . $this->_db->quote('%' . strtoupper($cCompanyName) . '%'));
					}
					if ($select) {
						$datas->where('UPPER(B.COLLECTIBILITY_CODE) LIKE ' . $this->_db->quote('%' . strtoupper($cCollectCode) . '%'));
					}
				}

				$data = $this->_db->fetchall($select);



				$field_export = array(
					'CUST_ID'  => array(
						'field' => 'CUST_ID',
						'label' => 'Company ID'
					),
					'CUST_NAME'  => array(
						'field' => 'CUST_NAME',
						'label' => 'Company Name'
					),
					'CUST_SEGMENT'  => array(
						'field' => 'CUST_SEGMENT',
						'label' => 'Segment'
					),
					'COLLECTIBILITY_CODE'     => array(
						'field' => 'COLLECTIBILITY_CODE',
						'label' => 'Collectibility'
					),
					'PKS_EXP_DATE'     => array(
						'field' => 'PKS_EXP_DATE',
						'label' => 'PKS Exp Date'
					),
					'LAST_APPROVED'     => array(
						'field' => 'LAST_APPROVED',
						'label' => 'Last Approved'
					),
					'PLAFOND_LIMIT'     => array(
						'field' => 'PLAFOND_LIMIT',
						'label' => 'Plafond Limit'
					),
					'LIMIT_USED'     => array(
						'field' => 'LIMIT_USED',
						'label' => 'Limit Used'
					),
					'STATUS'     => array(
						'field' => 'STATUS',
						'label' => 'Status'
					)



				);


				foreach ($data as $pTrx) {
					$paramTrx = array(

						"CUST_ID"  				=> $pTrx['CUST_ID'],
						"CUST_NAME" 			=> $pTrx['CUST_NAME'],
						"CUST_SEGMENT"  			=> $pTrx['CUST_SEGMENT'],
						"COLLECTIBILITY_CODE"  			=> $pTrx['COLLECTIBILITY_CODE'],
						"PKS_EXP_DATE"  		=> $pTrx['PKS_EXP_DATE'],
						"LAST_APPROVED"  		=> $pTrx['LAST_APPROVED'],
						"PLAFOND_LIMIT"  		=> $pTrx['PLAFOND_LIMIT'],
						"LIMIT_USED"  		=> $pTrx['LIMIT_USED'],
						"STATUS"  		=> $pTrx['STATUS'],
					);
					$newData[] = $paramTrx;
				}

				$this->_forward('print', 'index', 'widget', array('data_content' => $newData, 'data_caption' => 'Customer List Report', 'data_header' => $field_export, 'data_filter' => $filterlistdatax));
			}
		}
		// echo '<pre>';
		// print_r($data);
		// echo '</pre><br>';
		// die;


		$model_linefacility = new linefacility_Model_Linefacility();

		$data_linefacility = $model_linefacility->getData();
		$curdate = date('Y-m-d');

		foreach ($data_linefacility as $row) {
			if ($row['STATUS'] != 3) {
				if (strtotime($row["PKS_EXP_DATE"]) < strtotime($curdate)) {
					$dataUpdate = ['STATUS' => 3];
					$dataWhere  = ['ID = ?' => $row['ID']];
					$this->_db->update('M_CUST_LINEFACILITY', $dataUpdate, $dataWhere);
				}
			}
		}

		$this->view->filter 			= $filter;
		$this->view->statusArr 			= $statusArr;
		$this->view->segmentationArr 	= $segmentationArr;
		// $this->view->collectCodeArr 	= $model->getCreditQuality();


		$this->view->fCompanyCode 		= $fCompanyCode;
		$this->view->fCompanyName 		= $fCompanyName;
		$this->view->fCollectCode 		= $fCollectCode;
		// $this->view->fUploadDateFrom 	= $fUploadDateFrom;
		// $this->view->fUploadDateTo 		= $fUploadDateTo;

		Application_Helper_General::writeLog('VFBO', 'View Line Facility Customer List');
	}

	public function onboardingAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		if (!$this->view->hasPrivilege("OFBO")) {
			return $this->_redirect("/home/dashboard");
		}

		$model = new linefacility_Model_Linefacility();

		$this->view->escrowArr = array('' => '-- Pilih Rekening --');

		$getCustomer = $model->getCustomer();
		$custIdArr = Application_Helper_Array::listArray($getCustomer, 'CUST_ID', 'CUST_NAME');
		//echo '<pre>';
		//print_r($custIdArr);die;
		$this->view->custIdArr = $custIdArr;

		$getAgreement = $model->getAgreement();
		$agreementArr = Application_Helper_Array::listArray($getAgreement, 'AGREEMENT_CODE', 'AGREEMENT_DESC');
		$this->view->agreementArr = $agreementArr;

		$segmentationArr = [
			1 => 'CORPORATE',
			2 => 'COMMERCIAL',
			3 => 'SME',
			4 => 'FINANCIAL INSTITUTION'
		];
		$this->view->segmentationArr = $segmentationArr;

		if ($this->_request->isPost()) {

			$filters = array('*' => array('StringTrim', 'StripTags'));

			$zf_filter_input = new Zend_Filter_Input($filters, null, $this->_request->getPost());

			$cust_id 			= $zf_filter_input->cust_id;
			$cust_segment 		= $zf_filter_input->cust_segment;
			$agreement_type 	= $zf_filter_input->agreement_type;
			$collect_code 		= $zf_filter_input->collect_code;
			$pks_number 		= $zf_filter_input->pks_number;
			$pks_file 			= $_FILES['pks_file'];
			$pks_start_date 	= $zf_filter_input->pks_start_date;
			$pks_exp_date 		= $zf_filter_input->pks_exp_date;
			$claim_deadline 	= $zf_filter_input->claim_deadline;
			$grace_period	 	= $zf_filter_input->grace_period;
			$doc_deadline 		= $zf_filter_input->doc_deadline;
			$fee_debited 		= $zf_filter_input->fee_debited;
			$debited_acct 		= $zf_filter_input->debited_acct;
			$escrow_acct 		= $zf_filter_input->escrow_acct;
			$plafond_limit 		= Application_Helper_General::convertDisplayMoney($zf_filter_input->plafond_limit);
			$ticket_size 		= Application_Helper_General::convertDisplayMoney($zf_filter_input->ticket_size);
			$ccy 				= $zf_filter_input->ccy;
			$marginal_deposit 	= Application_Helper_General::convertDisplayMoney($zf_filter_input->marginal_deposit);
			$fee_provision 		= $zf_filter_input->fee_provision;
			$fee_admin 			= Application_Helper_General::convertDisplayMoney($zf_filter_input->fee_admin);
			$fee_stamp 			= Application_Helper_General::convertDisplayMoney($zf_filter_input->fee_stamp);
			$percentplafond 	= $zf_filter_input->percentplafond;

			$this->_db->delete("TEMP_CUST_LINEFACILITY_DETAIL", [
				'CUST_ID = ? ' => $cust_id
			]);

			// insert ke M_CUST_LINEFACILITY_DETAIL -----------------------------------------------------------

			$all_jaminan = [
				"JAMINAN PEMBAYARAN",
				"JAMINAN PENAWARAN",
				"JAMINAN PELAKSANAAN",
				"JAMINAN UANG MUKA",
				"JAMINAN PEMELIHARAAN",
				"JAMINAN LAINNYA"
			];

			$get_cust_model = $this->_db->fetchOne(
				$this->_db->select()
					->from("M_CUSTOMER", ["CUST_MODEL"])
					->where("CUST_ID = ?", $cust_id)
			);

			$get_all_jaminan = $this->_db->fetchAll(
				$this->_db->select()
					->from("M_CHARGES_BG")
					->where("CHARGES_ID LIKE ?", ($get_cust_model == 1) ? "%LF%" : "%INS%")
			);

			$hitung_bumn = 0;
			$hitung_nonbumn = 6;

			foreach ($all_jaminan as $jaminan) {
				if ($this->_request->getParam(str_replace(" ", "_", $jaminan))) {
					$flag = 1;
				} else {
					$flag = 0;
				}

				switch ($jaminan) {
					case 'JAMINAN PEMBAYARAN':
						$percentage_bumn = $this->_request->getPost(strtolower(str_replace(" ", "_", $jaminan) . "_bumn"));
						$percentage_nonbumn = $this->_request->getPost(strtolower(str_replace(" ", "_", $jaminan) . "_nonbumn"));
						break;

					case 'JAMINAN PENAWARAN':
						$percentage_bumn = $this->_request->getPost(strtolower(str_replace(" ", "_", $jaminan) . "_bumn"));
						$percentage_nonbumn = $this->_request->getPost(strtolower(str_replace(" ", "_", $jaminan) . "_nonbumn"));
						break;

					case 'JAMINAN PELAKSANAAN':
						$percentage_bumn = $this->_request->getPost(strtolower(str_replace(" ", "_", $jaminan) . "_bumn"));
						$percentage_nonbumn = $this->_request->getPost(strtolower(str_replace(" ", "_", $jaminan) . "_nonbumn"));
						break;

					case 'JAMINAN UANG MUKA':
						$percentage_bumn = $this->_request->getPost(strtolower(str_replace(" ", "_", $jaminan) . "_bumn"));
						$percentage_nonbumn = $this->_request->getPost(strtolower(str_replace(" ", "_", $jaminan) . "_nonbumn"));
						break;

					case 'JAMINAN PEMELIHARAAN':
						$percentage_bumn = $this->_request->getPost(strtolower(str_replace(" ", "_", $jaminan) . "_bumn"));
						$percentage_nonbumn = $this->_request->getPost(strtolower(str_replace(" ", "_", $jaminan) . "_nonbumn"));
						break;

					case 'JAMINAN LAINNYA':
						$percentage_bumn = $this->_request->getPost(strtolower(str_replace(" ", "_", $jaminan) . "_bumn"));
						$percentage_nonbumn = $this->_request->getPost(strtolower(str_replace(" ", "_", $jaminan) . "_nonbumn"));
						break;

					default:
						$percentage_bumn = 0;
						$percentage_nonbumn = 0;
						break;
				}

				if ($jaminan == "JAMINAN LAINNYA") {
					$jaminan = "JAMINAN LAINNYA (SP2D)";
				}

				$this->_db->insert("TEMP_CUST_LINEFACILITY_DETAIL", [
					"CUST_ID" => $cust_id,
					"OFFER_TYPE" => $jaminan,
					"GRUP_BUMN" => 1,
					"FEE_PROVISION" => $percentage_bumn,
					"FLAG" => $flag,
					"LAST_SUGGESTED" => new Zend_Db_Expr('now()')
				]);

				$this->_db->insert("TEMP_CUST_LINEFACILITY_DETAIL", [
					"CUST_ID" => $cust_id,
					"OFFER_TYPE" => $jaminan,
					"GRUP_BUMN" => 0,
					"FEE_PROVISION" => $percentage_nonbumn,
					"FLAG" => $flag,
					"LAST_SUGGESTED" => new Zend_Db_Expr('now()')
				]);
			}

			// ------------------------------------------------------------------------------------------------

			// check rekening pembayaran klaim ---------------------------------------------------------------

			if (!empty($escrow_acct)) {
				$svcRekeningPembayaranKlaim = new Service_Account($escrow_acct, "");
				if ($svcRekeningPembayaranKlaim->inquiryAccontInfo()["response_code"] == "0000") {
					if ($svcRekeningPembayaranKlaim->inquiryAccountBalance()["response_code"] == "0000") {

						if ($svcRekeningPembayaranKlaim->inquiryAccountBalance()["status"] != 1 && $svcRekeningPembayaranKlaim->inquiryAccountBalance()["status"] != 4) {
							$errDesc['escrow_acct'] = $this->language->_('Status rekening tidak aktif');
						}

						if ($svcRekeningPembayaranKlaim->inquiryAccountBalance()["account_type"] != "D") {
							$errDesc['escrow_acct'] = $this->language->_('Tipe rekening bukan giro / tabungan');
						}

						$check_prod_type = $this->_db->select()
							->from("M_PRODUCT_TYPE")
							->where("PRODUCT_CODE = ?", $svcRekeningPembayaranKlaim->inquiryAccountBalance()["account_type"])
							->query()->fetch();

						if (empty($check_prod_type)) {
							$errDesc['escrow_acct'] = $this->language->_('Tipe rekening tidak ada di tipe produk');
						}
					} else {
						if ($svcRekeningPembayaranKlaim->inquiryDeposito()["status"] != 1 && $svcRekeningPembayaranKlaim->inquiryDeposito()["status"] != 4) {
							$errDesc['escrow_acct'] = $this->language->_('Status rekening tidak aktif');
						}

						if ($svcRekeningPembayaranKlaim->inquiryDeposito()["account_type"] != "D") {
							$errDesc['escrow_acct'] = $this->language->_('Tipe rekening bukan giro / tabungan');
						}

						$check_prod_type = $this->_db->select()
							->from("M_PRODUCT_TYPE")
							->where("PRODUCT_CODE = ?", $svcRekeningPembayaranKlaim->inquiryDeposito()["account_type"])
							->query()->fetch();

						if (empty($check_prod_type)) {
							$errDesc['escrow_acct'] = $this->language->_('Tipe rekening tidak ada di tipe produk');
						}
					}
					$escrow_acct_name = $svcRekeningPembayaranKlaim->inquiryAccontInfo()["account_name"];
					$escrow_ccy = ($svcRekeningPembayaranKlaim->inquiryAccontInfo()["currency"]) ?: 'IDR';
				} else {
					$errDesc['escrow_acct'] = $this->language->_('Rekening tidak ditemukan');
				}

				$check_db_escrow = $this->_db->select()
					->from("M_CUSTOMER_ACCT")
					->where("ACCT_NO = ?", $escrow_acct)
					->where("CUST_ID = ?", $cust_id)
					->query()->fetch();

				if (empty($check_db_escrow)) {
					$errDesc['escrow_acct'] = $this->language->_('Rekening tidak ditemukan');
				} else {
					if ($check_db_escrow["ACCT_STATUS"] != 1 || $check_db_escrow["ACCT_STATUS"] != '1') $errDesc['escrow_acct'] = $this->language->_('Status rekening tidak aktif');
				}
			} else {
				$escrow_acct = "";
			}

			// -----------------------------------------------------------------------------------------------

			// check rekening pembayaran klaim ---------------------------------------------------------------

			if (!empty($debited_acct)) {
				$svcCekRekeningPembebananBiaya = new Service_Account($debited_acct, "");
				if ($svcCekRekeningPembebananBiaya->inquiryAccontInfo()["response_code"] == "0000") {
					if ($svcCekRekeningPembebananBiaya->inquiryAccountBalance()["response_code"] == "0000") {
						if ($svcCekRekeningPembebananBiaya->inquiryAccountBalance()["status"] != 1 && $svcCekRekeningPembebananBiaya->inquiryAccountBalance()["status"] != 4) {
							$errDesc['debited_acct'] = $this->language->_('Status rekening tidak aktif');
						}

						if ($svcCekRekeningPembebananBiaya->inquiryAccountBalance()["account_type"] != "D" && $svcCekRekeningPembebananBiaya->inquiryAccountBalance()["account_type"] != "S") {
							$errDesc['debited_acct'] = $this->language->_('Tipe rekening bukan giro / tabungan');
						}

						$check_prod_type = $this->_db->select()
							->from("M_PRODUCT_TYPE")
							->where("PRODUCT_CODE = ?", $svcCekRekeningPembebananBiaya->inquiryAccountBalance()["account_type"])
							->query()->fetch();

						if (empty($check_prod_type)) {
							$errDesc['debited_acct'] = $this->language->_('Tipe rekening tidak ada di tipe produk');
						}
					} else {
						if ($svcCekRekeningPembebananBiaya->inquiryDeposito()["status"] != 1 && $svcCekRekeningPembebananBiaya->inquiryDeposito()["status"] != 4) {
							$errDesc['debited_acct'] = $this->language->_('Status rekening tidak aktif');
						}

						if ($svcCekRekeningPembebananBiaya->inquiryDeposito()["account_type"] != "D" && $svcCekRekeningPembebananBiaya->inquiryDeposito()["account_type"] != "S") {
							$errDesc['debited_acct'] = $this->language->_('Tipe rekening bukan giro / tabungan');
						}

						$check_prod_type = $this->_db->select()
							->from("M_PRODUCT_TYPE")
							->where("PRODUCT_CODE = ?", $svcCekRekeningPembebananBiaya->inquiryDeposito()["account_type"])
							->query()->fetch();

						if (empty($check_prod_type)) {
							$errDesc['debited_acct'] = $this->language->_('Tipe rekening tidak ada di tipe produk');
						}
					}
					$debited_acct_name = $svcCekRekeningPembebananBiaya->inquiryAccontInfo()["account_name"];
					$escrow_ccy = ($svcCekRekeningPembebananBiaya->inquiryAccontInfo()["currency"]) ?: 'IDR';
				} else {
					$errDesc['debited_acct'] = $this->language->_('Rekening tidak ditemukan');
				}

				$check_db_escrow = $this->_db->select()
					->from("M_CUSTOMER_ACCT")
					->where("ACCT_NO = ?", $debited_acct)
					->where("CUST_ID = ?", $cust_id)
					->query()->fetch();

				if (empty($check_db_escrow)) {
					$errDesc['escrow_acct'] = $this->language->_('Rekening tidak ditemukan');
				} else {
					if ($check_db_escrow["ACCT_STATUS"] != 1 || $check_db_escrow["ACCT_STATUS"] != '1') $errDesc['escrow_acct'] = $this->language->_('Status rekening tidak aktif');
				}
			}

			// -----------------------------------------------------------------------------------------------


			if (empty($cust_id)) {
				$errDesc['cust_id'] = $this->language->_('Customer cannot be empty');
			}
			if (empty($cust_segment)) {
				$errDesc['cust_segment'] = $this->language->_('Segmentation cannot be empty');
			}
			if (empty($agreement_type)) {
				$errDesc['agreement_type'] = $this->language->_('Jenis Pengikatan cannot be empty');
			}
			if (empty($pks_number)) {
				$errDesc['pks_number'] = $this->language->_('Underlying Document cannot be empty');
			}
			if (empty($pks_file['name'])) {
				$errDesc['pks_file'] = $this->language->_('Upload Document cannot be empty');
			}
			if (empty($pks_start_date)) {
				$errDesc['pks_start_date'] = $this->language->_('Facility Start Date cannot be empty');
			}
			if (empty($pks_exp_date)) {
				$errDesc['pks_exp_date'] = $this->language->_('Facility Expired Date cannot be empty');
			}
			if (empty($claim_deadline)) {
				$errDesc['claim_deadline'] = $this->language->_('Claim Payment Deadline cannot be empty');
			}
			if ($cust_segment == 4) {
				if (empty($grace_period)) {
					$errDesc['grace_period'] = $this->language->_('Grace Period cannot be empty');
				}
			}
			if (empty($plafond_limit) || $plafond_limit == 0) {
				$errDesc['plafond_limit'] = $this->language->_('Facility Plafond Limit cannot be empty');
			}

			$get_min_amt = $this->_db->select()
				->from("M_MINAMT_CCY")
				->where("CCY_ID = ?", "IDR")
				->query()->fetch();

			if (round(floatval($plafond_limit), 2) < $get_min_amt["MIN_TX_AMT"]) {
				$errDesc['plafond_limit'] = $this->language->_('Tidak boleh lebih kecil dari ' . number_format($get_min_amt["MIN_TX_AMT"], 2));
			}

			if (round(floatval($plafond_limit), 2) > round(floatval(9999999999999.99), 2)) {
				$errDesc['plafond_limit'] = $this->language->_('Tidak boleh lebih besar dari ' . number_format(9999999999999.99, 2));
			}

			if (empty($ticket_size) || $ticket_size == 0) {
				$errDesc['ticket_size'] = $this->language->_('Ticket Size cannot be empty');
			}
			if ($fee_provision > 100) {
				$errDesc['fee_provision'] = $this->language->_('Bank Provision Fee maximal 100%');
			}
			if ($fee_admin > 9999999999999) {
				$errDesc['fee_admin'] = $this->language->_('Bank Administration Fee cannot more then 9,999,999,999,999');
			}
			if ($fee_stamp > 9999999999999) {
				$errDesc['fee_stamp'] = $this->language->_('Bank Stamp Fee cannot more then 9,999,999,999,999');
			}
			if ($cust_segment == 4) {
				if (empty($doc_deadline)) {
					$errDesc['doc_deadline'] = $this->language->_('Document Submission Deadline cannot be empty');
				}
				if (empty($fee_debited)) {
					$errDesc['fee_debited'] = $this->language->_('Fee Will be Debited from cannot be empty');
				} else {
					if ($fee_debited == 2) {
						if (empty($debited_acct)) {
							$errDesc['debited_acct'] = $this->language->_('Debited Account cannot be empty');
						}
					}
				}
				if ($ticket_size == '1') {
					if (empty($percentplafond)) {
						$errDesc['ticket_size'] = $this->language->_('Ticket Size is more than 10% plafond limit');
					}
				}
				$percent10 = (10 / 100) * (float)$plafond_limit;
				if ($ticket_size > $percent10) {
					$errDesc['ticket_size'] = $this->language->_('Ticket Size is more than 10% plafond limit');
				}
				if (empty($marginal_deposit) || $marginal_deposit == 0) {
					$errDesc['marginal_deposit'] = $this->language->_('Marginal Deposit cannot be empty');
				}
			}

			$this->view->errDesc 			= $errDesc;
			$this->view->cust_id 			= $cust_id;
			$this->view->cust_segment 		= $cust_segment;
			$this->view->agreement_type 	= $agreement_type;
			$this->view->collect_code 		= $collect_code;
			$this->view->pks_number 		= $pks_number;
			$this->view->pks_start_date 	= $pks_start_date;
			$this->view->pks_exp_date 		= $pks_exp_date;
			$this->view->claim_deadline 	= $claim_deadline;
			$this->view->doc_deadline 		= $doc_deadline;
			$this->view->grace_period 		= $grace_period;
			$this->view->fee_debited 		= $fee_debited;
			$this->view->debited_acct 		= $debited_acct;
			$this->view->escrow_acct 		= $escrow_acct;
			$this->view->plafond_limit 		= $plafond_limit;
			$this->view->ticket_size 		= $ticket_size;
			$this->view->ccy 				= $ccy;
			$this->view->marginal_deposit 	= $marginal_deposit;
			$this->view->fee_provision 		= $fee_provision;
			$this->view->fee_admin 			= $fee_admin;
			$this->view->fee_stamp 			= $fee_stamp;
			$this->view->percentplafond		= $percentplafond;

			if (empty($errDesc)) {
				if ($ccy != "IDR") {
					$marginal_deposit = round(floatval($marginal_deposit / 100) * $plafond_limit, 2);
				}
				//die;
				$path = UPLOAD_PATH . '/document/submit/';

				$filename_pks = strtolower(uniqid()) . '_' . strtolower(str_replace(' ', '_', $pks_file['name']));
				$path_old_pks = $pks_file['tmp_name'];
				$path_new_pks = $path . $filename_pks;
				move_uploaded_file($path_old_pks, $path_new_pks);

				if ($cust_segment == 4) {
					$content = [
						'CUST_ID'			=> $cust_id,
						'CUST_SEGMENT'		=> 4,
						'AGREEMENT_TYPE'	=> $agreement_type,
						'PKS_NUMBER'		=> $pks_number,
						'PKS_FILE'			=> $filename_pks,
						'PKS_START_DATE'	=> $pks_start_date,
						'PKS_EXP_DATE'		=> $pks_exp_date,
						'CLAIM_DEADLINE'	=> $claim_deadline,
						'GRACE_PERIOD'		=> $grace_period,
						'DOC_DEADLINE'		=> $doc_deadline,
						'FEE_DEBITED'		=> $fee_debited,
						'DEBITED_ACCOUNT'	=> ($fee_debited == 2) ? $debited_acct : null,
						'ESCROW_ACCT'		=> $escrow_acct ?: null,
						'ESCROW_CCY'		=> $escrow_ccy ?: null,
						'ESCROW_ACCT_NAME'	=> $escrow_acct_name ?: null,
						'PLAFOND_LIMIT'		=> $plafond_limit,
						'TICKET_SIZE'		=> $ticket_size,
						'MARGINAL_CCY'		=> $ccy,
						'MARGINAL_DEPOSIT'	=> $marginal_deposit,
						'FEE_PROVISION'		=> $fee_provision ?: 0,
						'FEE_ADMIN'			=> $fee_admin ?: null,
						'FEE_STAMP'			=> $fee_stamp ?: null,
						'FLAG'				=> 1,
						'TYPE'				=> 'Onboarding',
						'PERCENT_PLAFOND'	=> $percentplafond,
						"PROVISION"			=> [
							strtoupper("jaminan_pembayaran_bumn") => $this->_request->getPost("jaminan_pembayaran_bumn"),
							strtoupper("jaminan_penawaran_bumn") => $this->_request->getPost("jaminan_penawaran_bumn"),
							strtoupper("jaminan_pelaksanaan_bumn") => $this->_request->getPost("jaminan_pelaksanaan_bumn"),
							strtoupper("jaminan_uang_muka_bumn") => $this->_request->getPost("jaminan_uangmuka_bumn"),
							strtoupper("jaminan_pemeliharaan_bumn") => $this->_request->getPost("jaminan_pemeliharaan_bumn"),
							strtoupper("jaminan_lainnya_bumn") => $this->_request->getPost("jaminan_lainnya_bumn"),
							strtoupper("jaminan_pembayaran_nonbumn") => $this->_request->getPost("jaminan_pembayaran_nonbumn"),
							strtoupper("jaminan_penawaran_nonbumn") => $this->_request->getPost("jaminan_penawaran_nonbumn"),
							strtoupper("jaminan_pelaksanaan_nonbumn") => $this->_request->getPost("jaminan_pelaksanaan_nonbumn"),
							strtoupper("jaminan_uang_muka_nonbumn") => $this->_request->getPost("jaminan_uangmuka_nonbumn"),
							strtoupper("jaminan_pemeliharaan_nonbumn") => $this->_request->getPost("jaminan_pemeliharaan_nonbumn"),
							strtoupper("jaminan_lainnya_nonbumn") => $this->_request->getPost("jaminan_lainnya_nonbumn"),
						]
					];
				} else {
					$content = [
						'CUST_ID'			=> $cust_id,
						'CUST_SEGMENT'		=> $cust_segment,
						'AGREEMENT_TYPE'	=> $agreement_type,
						'PKS_NUMBER'		=> $pks_number,
						'PKS_FILE'			=> $filename_pks,
						'PKS_START_DATE'	=> $pks_start_date,
						'PKS_EXP_DATE'		=> $pks_exp_date,
						'CLAIM_DEADLINE'	=> $claim_deadline,
						'GRACE_PERIOD'		=> 0,
						'ESCROW_ACCT'		=> $escrow_acct ?: null,
						'ESCROW_CCY'		=> $escrow_ccy ?: null,
						'ESCROW_ACCT_NAME'	=> $escrow_acct_name ?: null,
						'PLAFOND_LIMIT'		=> $plafond_limit,
						'TICKET_SIZE'		=> $plafond_limit,
						'FEE_PROVISION'		=> $fee_provision ?: 0,
						'FEE_ADMIN'			=> $fee_admin ?: null,
						'FEE_STAMP'			=> $fee_stamp ?: null,
						'FLAG'				=> 1,
						'TYPE'				=> 'Onboarding',
						'PERCENT_PLAFOND'	=> $percentplafond,
						"PROVISION"			=> [
							strtoupper("jaminan_pembayaran_bumn") => $this->_request->getPost("jaminan_pembayaran_bumn"),
							strtoupper("jaminan_penawaran_bumn") => $this->_request->getPost("jaminan_penawaran_bumn"),
							strtoupper("jaminan_pelaksanaan_bumn") => $this->_request->getPost("jaminan_pelaksanaan_bumn"),
							strtoupper("jaminan_uang_muka_bumn") => $this->_request->getPost("jaminan_uangmuka_bumn"),
							strtoupper("jaminan_pemeliharaan_bumn") => $this->_request->getPost("jaminan_pemeliharaan_bumn"),
							strtoupper("jaminan_lainnya_bumn") => $this->_request->getPost("jaminan_lainnya_bumn"),
							strtoupper("jaminan_pembayaran_nonbumn") => $this->_request->getPost("jaminan_pembayaran_nonbumn"),
							strtoupper("jaminan_penawaran_nonbumn") => $this->_request->getPost("jaminan_penawaran_nonbumn"),
							strtoupper("jaminan_pelaksanaan_nonbumn") => $this->_request->getPost("jaminan_pelaksanaan_nonbumn"),
							strtoupper("jaminan_uang_muka_nonbumn") => $this->_request->getPost("jaminan_uangmuka_nonbumn"),
							strtoupper("jaminan_pemeliharaan_nonbumn") => $this->_request->getPost("jaminan_pemeliharaan_nonbumn"),
							strtoupper("jaminan_lainnya_nonbumn") => $this->_request->getPost("jaminan_lainnya_nonbumn"),
						]
					];
				}

				$tempData = new Zend_Session_Namespace('tempData');
				$tempData->content = $content;


				$this->_redirect('linefacility/index/confirm');
				Application_Helper_General::writeLog('OFBO', 'Onboarding Line Facility');
			}
		} else {
			Application_Helper_General::writeLog('OFBO', 'View Customer Line Facility Onboarding');
		}
	}

	public function confirmAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$model = new linefacility_Model_Linefacility();

		$tempData = new Zend_Session_Namespace('tempData');
		$content  = $tempData->content;

		$segmentationArr = [
			1 => 'CORPORATE',
			2 => 'COMMERCIAL',
			3 => 'SME',
			4 => 'FINANCIAL INSTITUTION'
		];
		$this->view->segmentationArr = $segmentationArr;

		$cust_id    = $content['CUST_ID'];

		$getCustomer  = $model->getCustomerById($cust_id);
		$cust_name    = $getCustomer['CUST_NAME'];
		$cust_model   = $getCustomer['CUST_MODEL'];

		$this->view->checkBumn = $getCustomer["GRUP_BUMN"];

		$this->view->cust_model = $cust_model;

		$get_all_provision = $this->_db->fetchAll(
			$this->_db->select()
				->from("TEMP_CUST_LINEFACILITY_DETAIL")
				->where("CUST_ID = ?", $cust_id)
		);

		$this->view->get_all_provision = $get_all_provision;

		$getCustAcctById = $model->getCustAcctById($cust_id);
		$debitedAcctArr  = [];
		foreach ($getCustAcctById as $row) {
			$debitedAcctArr[$row['ACCT_NO']] = $row['ACCT_NO'] . ' (' . $row['CCY_ID'] . ') /' . $row['ACCT_NAME'];
		}
		$this->view->debitedAcctArr = $debitedAcctArr;

		$getCreditQuality = $model->getCreditQuality();
		$creditArr = Application_Helper_Array::listArray($getCreditQuality, 'CODE', 'DESCRIPTION');
		$this->view->creditArr = $creditArr;

		$getAgreement = $model->getAgreement();
		$agreementArr = Application_Helper_Array::listArray($getAgreement, 'AGREEMENT_CODE', 'AGREEMENT_DESC');
		$this->view->agreementArr = $agreementArr;

		$content['CUST_NAME']			= $cust_name;
		$content['COLLECTIBILITY_CODE']	= $getCustomer['COLLECTIBILITY_CODE'];

		if (!empty($content["ESCROW_ACCT"])) {
			$get_escrow_detail = $this->_db->select()
				->from(["MCA" => "M_CUSTOMER_ACCT"])
				->where("MCA.ACCT_NO = ?", $content["ESCROW_ACCT"]);

			//echo $get_escrow_detail;die;
			$get_escrow_detail = $this->_db->fetchRow($get_escrow_detail);
		}
		$this->view->data = $content;

		if ($this->_request->isPost()) {

			if ($content['TYPE'] == 'UPDATE') {

				$this->_db->insert('T_BACTIVITY', array(
					'LOG_DATE'         => new Zend_Db_Expr('now()'),
					'USER_ID'           => $this->_userIdLogin,
					'USER_NAME'           => $this->_userNameLogin,
					'ACTION_DESC'       => 'UFBO',
					'ACTION_FULLDESC'   => 'Update Line Fasilitas Nasabah ' . $getCustomer['CUST_NAME'] . ' (' . $data['CUST_ID'] . ')',
				));
			}

			$changes_type = ($content['TYPE'] == 'Onboarding') ? $this->_changeType['code']['new'] : $this->_changeType['code']['edit'];
			$info         = 'Customer ID = ' . $cust_id . ', Customer Name = ' . $cust_name;
			$change_id    = $this->suggestionWaitingApproval('Line Facility', $info, $changes_type, null, 'M_CUST_LINEFACILITY', 'TEMP_CUST_LINEFACILITY', $cust_id, $cust_name, $cust_id, $cust_name);

			try {
				$this->_db->beginTransaction();

				if ($content['CUST_SEGMENT'] == 4) {
					$dataInsert = [
						'CHANGES_ID'		=> $change_id,
						'CUST_ID'			=> $content['CUST_ID'],
						'CUST_SEGMENT'		=> $content['CUST_SEGMENT'],
						'AGREEMENT_TYPE'	=> $content['AGREEMENT_TYPE'],
						'PKS_NUMBER'		=> $content['PKS_NUMBER'],
						'PKS_FILE'			=> $content['PKS_FILE'],
						'PKS_START_DATE'	=> $content['PKS_START_DATE'],
						'PKS_EXP_DATE'		=> $content['PKS_EXP_DATE'],
						'CLAIM_DEADLINE'	=> $content['CLAIM_DEADLINE'],
						'GRACE_PERIOD'		=> $content['GRACE_PERIOD'],
						'DOC_DEADLINE'		=> $content['DOC_DEADLINE'],
						'FEE_DEBITED'		=> $content['FEE_DEBITED'],
						'DEBITED_ACCOUNT'	=> $content['DEBITED_ACCOUNT'],
						'ESCROW_ACCT'		=> $content['ESCROW_ACCT'],
						// 'ESCROW_CCY'		=> $content['ESCROW_CCY'],
						// 'ESCROW_ACCT_NAME'	=> $get_escrow_detail['ACCT_NAME'],
						'PLAFOND_LIMIT'		=> $content['PLAFOND_LIMIT'],
						'TICKET_SIZE'		=> $content['TICKET_SIZE'],
						'MARGINAL_CCY'		=> $content['MARGINAL_CCY'],
						'MARGINAL_DEPOSIT'	=> $content['MARGINAL_DEPOSIT'],
						'FEE_PROVISION'		=> $content['FEE_PROVISION'],
						'FEE_ADMIN'			=> $content['FEE_ADMIN'],
						'FEE_STAMP'			=> $content['FEE_STAMP'],
						'STATUS'			=> $content['STATUS'] ?: '1',
						'FLAG'				=> $content['FLAG'],
						'ENDED_REASON'		=> $content['ENDED_REASON'],
						'TERMINATED_FILE'	=> $content['TERMINATED_FILE'],
						'PERCENT_PLAFOND'	=> $content['PERCENT_PLAFOND'],
						'LAST_SUGGESTED'	=> new Zend_Db_Expr('now()'),
						'LAST_SUGGESTEDBY'	=> $this->_userIdLogin
					];
				} else {
					$dataInsert = [
						'CHANGES_ID'		=> $change_id,
						'CUST_ID'			=> $content['CUST_ID'],
						'CUST_SEGMENT'		=> $content['CUST_SEGMENT'],
						'AGREEMENT_TYPE'	=> $content['AGREEMENT_TYPE'],
						'PKS_NUMBER'		=> $content['PKS_NUMBER'],
						'PKS_FILE'			=> $content['PKS_FILE'],
						'PKS_START_DATE'	=> $content['PKS_START_DATE'],
						'PKS_EXP_DATE'		=> $content['PKS_EXP_DATE'],
						'CLAIM_DEADLINE'	=> $content['CLAIM_DEADLINE'],
						'GRACE_PERIOD'		=> 0,
						'PAYMENT_ACCT'		=> $content['ESCROW_ACCT'],
						// 'ESCROW_CCY'		=> $content['ESCROW_CCY'],
						// 'ESCROW_ACCT_NAME'	=> $get_escrow_detail['ACCT_NAME'],
						'PLAFOND_LIMIT'		=> $content['PLAFOND_LIMIT'],
						'TICKET_SIZE'		=> $content['TICKET_SIZE'],
						'FEE_PROVISION'		=> $content['FEE_PROVISION'],
						'FEE_ADMIN'			=> $content['FEE_ADMIN'],
						'FEE_STAMP'			=> $content['FEE_STAMP'],
						'STATUS'			=> $content['STATUS'] ?: '1',
						'FLAG'				=> $content['FLAG'],
						'ENDED_REASON'		=> $content['ENDED_REASON'],
						'TERMINATED_FILE'	=> $content['TERMINATED_FILE'],
						'LAST_SUGGESTED'	=> new Zend_Db_Expr('now()'),
						'PERCENT_PLAFOND'	=> $content['PERCENT_PLAFOND'],
						'LAST_SUGGESTEDBY'	=> $this->_userIdLogin
					];
				}

				$svcRekeningPembayaranKlaim = new Service_Account($content['ESCROW_ACCT'], "");

				if ($svcRekeningPembayaranKlaim->inquiryAccontInfo()["response_code"] == "0000") {
					if ($svcRekeningPembayaranKlaim->inquiryAccountBalance()["response_code"] == "0000") {
						if ($svcRekeningPembayaranKlaim->inquiryAccountBalance()["status"] != 1 && $svcRekeningPembayaranKlaim->inquiryAccountBalance()["status"] != 4) {
							$errDesc['escrow_acct'] = $this->language->_('Status rekening tidak aktif');
						}
					} else {
						if ($svcRekeningPembayaranKlaim->inquiryDeposito()["status"] != 1 && $svcRekeningPembayaranKlaim->inquiryDeposito()["status"] != 4) {
							$errDesc['escrow_acct'] = $this->language->_('Status rekening tidak aktif');
						}
					}
					$escrow_acct_name = $svcRekeningPembayaranKlaim->inquiryAccontInfo()["account_name"];
					$escrow_ccy = ($svcRekeningPembayaranKlaim->inquiryAccontInfo()["currency"]) ?: 'IDR';
				} else {
					$errDesc['escrow_acct'] = $this->language->_('Rekening tidak ditemukan');
				}




				// if (!empty($content['ESCROW_ACCT'])) {
				// 	$svcAccount = new Service_Account($content['ESCROW_ACCT'], null);
				// 	$result = $svcAccount->inquiryAccontInfo('AI', TRUE);
				// }

				// if (!empty($content['DEBITED_ACCOUNT'])) {
				// 	$svcAccount = new Service_Account($content['DEBITED_ACCOUNT'], null);
				// 	$result = $svcAccount->inquiryAccontInfo('AI', TRUE);
				// 	//	var_dump($result);
				// }
				// var_dump($result);die;


				// if ($svcRekeningPembayaranKlaim['response_desc'] == 'Success' || $svcRekeningPembayaranKlaim['response_code'] == '0907') //00 = success
				// {

				// print_r($dataInsert);
				// die();

				$this->_db->insert('TEMP_CUST_LINEFACILITY', $dataInsert);
				// $this->_db->commit();

				if (strtolower($content["TYPE"]) != "update") {
					Application_Helper_General::writeLog('OFBO', 'Customer Line Facility Onboarding CUST_ID: ' . $cust_id);
				} else {
					Application_Helper_General::writeLog('UFBO', 'Customer Line Facility Update CUST_ID: ' . $cust_id);
				}
				$this->setbackURL('/linefacility');
				$this->_redirect('/notification/submited/index');
				// } else {
				// 	$this->view->error = true;
				// 	$this->view->errorMsg = $errDesc;
				// }
			} catch (Exception $error) {


				// var_dump($error);
				// die();
				$this->_db->rollBack();
				echo '<pre>';
				print_r($error->getMessage());
				echo '</pre><br>';
				die;
			}
		}
	}

	public function detailAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$flashMessanger = $this->_helper->getHelper('FlashMessenger');

		if ($flashMessanger->getMessages()) {
			$get_error = $flashMessanger->getMessages()[0]["error"];
		}

		$this->view->error_unfreeze = $get_error;

		$model = new linefacility_Model_Linefacility();

		$id = $this->_getParam('id');
		$download = $this->_getParam('download');
		$submit = $this->_getParam('submit');
		$submit_freeze = $this->_getParam('submit_freeze');

		$sessToken 	= new Zend_Session_Namespace('Tokenenc');
		$password 	= $sessToken->token;

		$AESMYSQL = new Crypt_AESMYSQL();
		$decryption = urldecode($id);
		$decryption_id = $AESMYSQL->decrypt($decryption, $password);

		$data = $model->getDataById($decryption_id);

		$getCustomerName = $this->_db->select()
			->from("M_CUSTOMER", ["CUST_NAME", "GRUP_BUMN"])
			->where("CUST_ID = ?", $data['CUST_ID'])
			->query()->fetchAll();

		$this->_db->insert('T_BACTIVITY', array(
			'LOG_DATE'         => new Zend_Db_Expr('now()'),
			'USER_ID'           => $this->_userIdLogin,
			'USER_NAME'           => $this->_userNameLogin,
			'ACTION_DESC'       => 'VFBO',
			'ACTION_FULLDESC'   => 'Melihat Detail Line Fasilitas Nasabah ' . $getCustomerName[0]['CUST_NAME'] . ' (' . $data['CUST_ID'] . ')',
		));

		$data["GROUP_BUMN"] = $getCustomerName[0]["GRUP_BUMN"];
		$this->view->data = $data;

		// $sql = $this->_db->select()
		// ->from(["MCA" => "TEMP_BANK_GUARANTEE"])
		// ->where("MCA.CUST_ID = ?", "ASKRINDO01")
		// ->order("BG_CREATED DESC")
		// ->query()->fetchAll();

		// var_dump($sql[0]['COUNTER_WARRANTY_CODE']);die;
		// $this->view->selecbut = $sql;


		$limit_available = (float)$data['PLAFOND_LIMIT'] - (float)$data['LIMIT_USED'];
		$this->view->limit_available = $limit_available;

		$currentUrl = $this->getRequest()->getRequestUri();
		$this->view->currentUrl = $currentUrl;

		$params = $this->getRequest()->getParams();
		$this->view->params = $params;

		$cust_id = $data['CUST_ID'];

		$get_all_provision = $this->_db->fetchAll(
			$this->_db->select()
				->from("M_CUST_LINEFACILITY_DETAIL")
				->where("CUST_ID = ?", $cust_id)
		);

		$this->view->get_all_provision = $get_all_provision;

		$getCustomer  = $model->getCustomerById($cust_id);
		$cust_model   = $getCustomer['CUST_MODEL'];
		$this->view->cust_model = $cust_model;

		$getCustAcctById = $model->getCustAcctById($cust_id);
		$debitedAcctArr = [];
		foreach ($getCustAcctById as $row) {
			$debitedAcctArr[$row['ACCT_NO']] = $row['ACCT_NO'] . ' (' . $row['CCY_ID'] . ') /' . $row['ACCT_NAME'];
		}
		$this->view->debitedAcctArr = $debitedAcctArr;

		$segmentationArr = [
			1 => 'CORPORATE',
			2 => 'COMMERCIAL',
			3 => 'SME',
			4 => 'FINANCIAL INSTITUTION'
		];
		$this->view->segmentationArr = $segmentationArr;

		$statusArr = [
			'1'	=> 'Approved',
			'2'	=> 'Terminated',
			'3'	=> 'Expired',
			'4'	=> 'Freeze Submission',
			'5'	=> 'Unfreeze Submission'
		];
		$this->view->statusArr = $statusArr;

		$getCreditQuality = $model->getCreditQuality();
		$creditArr = Application_Helper_Array::listArray($getCreditQuality, 'CODE', 'DESCRIPTION');
		$this->view->creditArr = $creditArr;

		$getAgreement = $model->getAgreement();
		$agreementArr = Application_Helper_Array::listArray($getAgreement, 'AGREEMENT_CODE', 'AGREEMENT_DESC');
		$this->view->agreementArr = $agreementArr;

		$get_linefacility = $this->_db->select()
			->from("M_CUST_LINEFACILITY", ["CUST_ID", "PLAFOND_LIMIT", "FEE_PROVISION", "FEE_ADMIN", "FEE_STAMP", "CUST_SEGMENT"])
			->where("ID = ?", $decryption_id)
			->query()->fetchAll();

		if ($get_linefacility[0]["CUST_SEGMENT"] == "4") {
			$check_all_detail = $this->_db->select()
				->from("T_BANK_GUARANTEE_DETAIL")
				->where("PS_FIELDNAME = ?", "Insurance Name")
				->where("PS_FIELDVALUE = ?", $get_linefacility[0]["CUST_ID"])
				->query()->fetchAll();

			$total_bgamount_on_risk = 0;

			if (count($check_all_detail) > 0) {
				$save_bg_reg_number = [];
				foreach ($check_all_detail as $value) {
					array_push($save_bg_reg_number, $value["BG_REG_NUMBER"]);
				}

				$get_bgamount_on_risks = $this->_db->select()
					->from("T_BANK_GUARANTEE", ["BG_AMOUNT"])
					->where("(BG_STATUS = 15) AND COUNTER_WARRANTY_TYPE = '3'")
					->where("BG_REG_NUMBER IN (?)", $save_bg_reg_number)
					->query()->fetchAll();

				foreach ($get_bgamount_on_risks as $get_bgamount_on_risk) {
					$total_bgamount_on_risk += $get_bgamount_on_risk["BG_AMOUNT"];
				}
			}

			$check_all_detail = $this->_db->select()
				->from("TEMP_BANK_GUARANTEE_DETAIL")
				->where("PS_FIELDNAME = ?", "Insurance Name")
				->where("PS_FIELDVALUE = ?", $get_linefacility[0]["CUST_ID"])
				->query()->fetchAll();

			$total_bgamount_on_temp = 0;

			if (count($check_all_detail) > 0) {

				$save_bg_reg_number = [];
				foreach ($check_all_detail as $value) {
					array_push($save_bg_reg_number, $value["BG_REG_NUMBER"]);
				}

				$get_bgamount_on_temps = $this->_db->select()
					->from("TEMP_BANK_GUARANTEE", ["BG_AMOUNT"])
					->where("COUNTER_WARRANTY_TYPE = '3'")
					->where("BG_REG_NUMBER IN (?)", $save_bg_reg_number)
					->where("BG_STATUS IN (5,6,7,10,14,17,20)")
					->query()->fetchAll();

				foreach ($get_bgamount_on_temps as $key => $get_bgamount_on_temp) {
					$total_bgamount_on_temp += $get_bgamount_on_temp["BG_AMOUNT"];
				}
			}

			$this->view->current_limit = $get_linefacility[0]["PLAFOND_LIMIT"] - $total_bgamount_on_risk - $total_bgamount_on_temp;
			$this->view->limit_used = $total_bgamount_on_risk + $total_bgamount_on_temp;
		} else {
			$get_bgamount_on_risks = $this->_db->select()
				->from("T_BANK_GUARANTEE", ["BG_AMOUNT"])
				->where("(BG_STATUS = 15) AND COUNTER_WARRANTY_TYPE = '2' AND CUST_ID = " . $this->_db->quote($get_linefacility[0]["CUST_ID"]))
				->query()->fetchAll();


			$total_bgamount_on_risk = 0;

			foreach ($get_bgamount_on_risks as $key => $get_bgamount_on_risk) {
				$total_bgamount_on_risk += $get_bgamount_on_risk["BG_AMOUNT"];
			}

			$total_bgamount_on_temp = 0;

			$get_bgamount_on_temps = $this->_db->select()
				->from("TEMP_BANK_GUARANTEE", ["BG_AMOUNT"])
				->where("COUNTER_WARRANTY_TYPE = '2' AND CUST_ID = " . $this->_db->quote($get_linefacility[0]["CUST_ID"]))
				->where("BG_STATUS IN (1,2,3,5,6,7,14,17,20)")
				->query()->fetchAll();

			foreach ($get_bgamount_on_temps as $key => $get_bgamount_on_temp) {
				$total_bgamount_on_temp += $get_bgamount_on_temp["BG_AMOUNT"];
			}


			$this->view->current_limit = $get_linefacility[0]["PLAFOND_LIMIT"] - $total_bgamount_on_risk - $total_bgamount_on_temp;
			$this->view->limit_used = $total_bgamount_on_risk + $total_bgamount_on_temp;
		}

		$getTempLinefacility = $model->getTempLinefacility($cust_id);
		if ($getTempLinefacility) {
			$this->view->is_update = true;
		}

		if ($download) {
			$path = UPLOAD_PATH . '/document/submit/';
			$file = $data['PKS_FILE'];
			$this->_helper->download->file($file, $path . $file);
		}

		if ($submit) {
			$doc_file = $_FILES['underlying_doc'];
			$reason = $this->_getParam('reason');

			$path = UPLOAD_PATH . '/document/submit/';
			$filename_doc = strtolower(uniqid()) . '_' . strtolower(str_replace(' ', '_', $doc_file['name']));
			$path_old_doc = $doc_file['tmp_name'];
			$path_new_doc = $path . $filename_doc;
			move_uploaded_file($path_old_doc, $path_new_doc);

			$getCustomer  = $model->getCustomerById($cust_id);
			$cust_name    = $getCustomer['CUST_NAME'];
			$info         = 'Customer ID = ' . $cust_id . ', Customer Name = ' . $cust_name;
			$change_id    = $this->suggestionWaitingApproval('Line Facility', $info, $this->_changeType['code']['edit'], null, 'M_CUST_LINEFACILITY', 'TEMP_CUST_LINEFACILITY', $cust_id, $cust_name, $cust_id, $cust_name);

			try {
				$this->_db->beginTransaction();

				if ($data['CUST_SEGMENT'] == 4) {
					$dataInsert = [
						'CHANGES_ID'		=> $change_id,
						'CUST_ID'			=> $cust_id,
						'CUST_SEGMENT'		=> 4,
						'AGREEMENT_TYPE'	=> $data['AGREEMENT_TYPE'],
						'PKS_NUMBER'		=> $data['PKS_NUMBER'],
						'PKS_FILE'			=> $data['PKS_FILE'],
						'PKS_START_DATE'	=> $data['PKS_START_DATE'],
						'PKS_EXP_DATE'		=> $data['PKS_EXP_DATE'],
						'CLAIM_DEADLINE'	=> $data['CLAIM_DEADLINE'],
						'DOC_DEADLINE'		=> $data['DOC_DEADLINE'],
						'FEE_DEBITED'		=> $data['FEE_DEBITED'],
						// 'PAYMENT_ACCT'		=> $data['PAYMENT_ACCT'],
						'DEBITED_ACCOUNT'	=> ($data['FEE_DEBITED'] == 2) ? $data['DEBITED_ACCOUNT'] : null,
						'PLAFOND_LIMIT'		=> $data['PLAFOND_LIMIT'],
						'TICKET_SIZE'		=> $data['TICKET_SIZE'],
						'MARGINAL_CCY'		=> $data['MARGINAL_CCY'],
						'MARGINAL_DEPOSIT'	=> $data['MARGINAL_DEPOSIT'],
						'FEE_PROVISION'		=> $data['FEE_PROVISION'] ?: 0,
						'FEE_ADMIN'			=> $data['FEE_ADMIN'] ?: null,
						'FEE_STAMP'			=> $data['FEE_STAMP'] ?: null,
						'STATUS'			=> 2, // Terminate
						'FLAG'				=> 1, // Waiting Review
						'ENDED_REASON'		=> $reason,
						'TERMINATED_FILE'	=> $filename_doc,
						'LAST_SUGGESTED'	=> new Zend_Db_Expr('now()'),
						'LAST_SUGGESTEDBY'	=> $this->_userIdLogin
					];
				} else {
					$dataInsert = [
						'CHANGES_ID'		=> $change_id,
						'CUST_ID'			=> $cust_id,
						'CUST_SEGMENT'		=> $data['CUST_SEGMENT'],
						'AGREEMENT_TYPE'	=> $data['AGREEMENT_TYPE'],
						'PKS_NUMBER'		=> $data['PKS_NUMBER'],
						'PKS_FILE'			=> $data['PKS_FILE'],
						'PKS_START_DATE'	=> $data['PKS_START_DATE'],
						'PKS_EXP_DATE'		=> $data['PKS_EXP_DATE'],
						'CLAIM_DEADLINE'	=> $data['CLAIM_DEADLINE'],
						'PLAFOND_LIMIT'		=> $data['PLAFOND_LIMIT'],
						// 'PAYMENT_ACCT'		=> $data['PAYMENT_ACCT'],
						'TICKET_SIZE'		=> $data['TICKET_SIZE'],
						'FEE_PROVISION'		=> $data['FEE_PROVISION'],
						'FEE_ADMIN'			=> $data['FEE_ADMIN'],
						'FEE_STAMP'			=> $data['FEE_STAMP'] ?: null,
						'STATUS'			=> 2, // Terminate
						'FLAG'				=> 1, // Waiting Review
						'ENDED_REASON'		=> $reason,
						'TERMINATED_FILE'	=> $filename_doc,
						'LAST_SUGGESTED'	=> new Zend_Db_Expr('now()'),
						'LAST_SUGGESTEDBY'	=> $this->_userIdLogin
					];
				}
				$this->_db->insert('TEMP_CUST_LINEFACILITY', $dataInsert);

				$this->_db->commit();

				Application_Helper_General::writeLog('OFBO', 'Customer Line Facility Terminate CUST_ID: ' . $cust_id);
				$this->setbackURL('/linefacility');
				$this->_redirect('/notification/submited/index');
			} catch (Exception $error) {
				$this->_db->rollBack();
				echo '<pre>';
				print_r($error->getMessage());
				echo '</pre><br>';
				die;
			}
		}

		if ($submit_freeze) {
			$reason = $this->_getParam('reason');

			$getCustomer  = $model->getCustomerById($cust_id);
			$cust_name    = $getCustomer['CUST_NAME'];
			$info         = 'Customer ID = ' . $cust_id . ', Customer Name = ' . $cust_name;
			$change_id    = $this->suggestionWaitingApproval('Line Facility', $info, $this->_changeType['code']['edit'], null, 'M_CUST_LINEFACILITY', 'TEMP_CUST_LINEFACILITY', $cust_id, $cust_name, $cust_id, $cust_name);

			try {
				$this->_db->beginTransaction();

				if ($data['CUST_SEGMENT'] == 4) {
					$dataInsert = [
						'CHANGES_ID'		=> $change_id,
						'CUST_ID'			=> $cust_id,
						'CUST_SEGMENT'		=> 4,
						'AGREEMENT_TYPE'	=> $data['AGREEMENT_TYPE'],
						'PKS_NUMBER'		=> $data['PKS_NUMBER'],
						'PKS_FILE'			=> $data['PKS_FILE'],
						'PKS_START_DATE'	=> $data['PKS_START_DATE'],
						'PKS_EXP_DATE'		=> $data['PKS_EXP_DATE'],
						'CLAIM_DEADLINE'	=> $data['CLAIM_DEADLINE'],
						'DOC_DEADLINE'		=> $data['DOC_DEADLINE'],
						'FEE_DEBITED'		=> $data['FEE_DEBITED'],
						'PAYMENT_ACCT'		=> $data['PAYMENT_ACCT'],
						'ESCROW_ACCT'		=> $data['ESCROW_ACCT'],
						'DEBITED_ACCOUNT'	=> ($data['FEE_DEBITED'] == 2) ? $data['DEBITED_ACCOUNT'] : null,
						'PLAFOND_LIMIT'		=> $data['PLAFOND_LIMIT'],
						'TICKET_SIZE'		=> $data['TICKET_SIZE'],
						'PERCENT_PLAFOND'	=> $data['PERCENT_PLAFOND'],
						"GRACE_PERIOD" 		=> $data["GRACE_PERIOD"],
						'MARGINAL_CCY'		=> $data['MARGINAL_CCY'],
						'MARGINAL_DEPOSIT'	=> $data['MARGINAL_DEPOSIT'],
						'FEE_PROVISION'		=> $data['FEE_PROVISION'] ?: 0,
						'FEE_ADMIN'			=> $data['FEE_ADMIN'] ?: null,
						'FEE_STAMP'			=> $data['FEE_STAMP'] ?: null,
						'STATUS'			=> 4, // Freeze
						"FREEZE_MANUAL"		=> 1,
						'FLAG'				=> 1, // Waiting Review
						'ENDED_REASON'		=> $reason,
						'LAST_SUGGESTED'	=> new Zend_Db_Expr('now()'),
						'LAST_SUGGESTEDBY'	=> $this->_userIdLogin
					];
				} else {
					$dataInsert = [
						'CHANGES_ID'		=> $change_id,
						'CUST_ID'			=> $cust_id,
						'CUST_SEGMENT'		=> $data['CUST_SEGMENT'],
						'AGREEMENT_TYPE'	=> $data['AGREEMENT_TYPE'],
						'PKS_NUMBER'		=> $data['PKS_NUMBER'],
						'PKS_FILE'			=> $data['PKS_FILE'],
						'PKS_START_DATE'	=> $data['PKS_START_DATE'],
						'PKS_EXP_DATE'		=> $data['PKS_EXP_DATE'],
						'CLAIM_DEADLINE'	=> $data['CLAIM_DEADLINE'],
						'PAYMENT_ACCT'		=> $data['PAYMENT_ACCT'],
						'PLAFOND_LIMIT'		=> $data['PLAFOND_LIMIT'],
						'TICKET_SIZE'		=> $data['TICKET_SIZE'],
						'FEE_PROVISION'		=> $data['FEE_PROVISION'],
						'FEE_ADMIN'			=> $data['FEE_ADMIN'],
						'FEE_STAMP'			=> $data['FEE_STAMP'] ?: null,
						'STATUS'			=> 4, // Freeze
						"FREEZE_MANUAL"		=> 1,
						'FLAG'				=> 1, // Waiting Review
						'ENDED_REASON'		=> $reason,
						'LAST_SUGGESTED'	=> new Zend_Db_Expr('now()'),
						'LAST_SUGGESTEDBY'	=> $this->_userIdLogin
					];
				}

				// echo "<pre>";
				// var_dump($dataInsert);
				// echo "</pre>";
				// die();
				$this->_db->insert('TEMP_CUST_LINEFACILITY', $dataInsert);

				$this->_db->delete("TEMP_CUST_LINEFACILITY_DETAIL", [
					'CUST_ID = ? ' => $cust_id
				]);

				foreach ($get_all_provision as $provision) {
					unset($provision["ID"]);
					$this->_db->insert("TEMP_CUST_LINEFACILITY_DETAIL", $provision);
				}

				$this->_db->commit();

				Application_Helper_General::writeLog('OFBO', 'Customer Line Facility Terminate CUST_ID: ' . $cust_id);
				$this->setbackURL('/linefacility');
				$this->_redirect('/notification/submited/index');
			} catch (Exception $error) {
				$this->_db->rollBack();
				echo '<pre>';
				print_r($error->getMessage());
				echo '</pre><br>';
				die;
			}
		}
	}

	public function updateAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		if (!$this->view->hasPrivilege("UFBO")) {
			return $this->_redirect("/home/dashboard");
		}

		$model = new linefacility_Model_Linefacility();

		$id = $this->_getParam('id');

		$sessToken 	= new Zend_Session_Namespace('Tokenenc');
		$password 	= $sessToken->token;

		$AESMYSQL = new Crypt_AESMYSQL();
		$decryption = urldecode($id);
		$decryption_id = $AESMYSQL->decrypt($decryption, $password);

		$data = $model->getDataById($decryption_id);
		$this->view->data = $data;

		$currentUrl = $this->getRequest()->getRequestUri();
		$this->view->currentUrl = $currentUrl;

		$params = $this->getRequest()->getParams();
		$this->view->params = $params;

		$segmentationArr = [
			1 => 'CORPORATE',
			2 => 'COMMERCIAL',
			3 => 'SME'
		];
		$this->view->segmentationArr = $segmentationArr;

		$custId = $data['CUST_ID'];

		$getCustomer  = $model->getCustomerById($custId);
		$cust_name    = $getCustomer['CUST_NAME'];
		$cust_model   = $getCustomer['CUST_MODEL'];
		$this->view->cust_model = $cust_model;


		$get_all_provision = $this->_db->fetchAll(
			$this->_db->select()
				->from("M_CUST_LINEFACILITY_DETAIL")
				->where("CUST_ID = ?", $data["CUST_ID"])
		);

		$this->view->get_all_provision = $get_all_provision;

		$getCreditQuality = $model->getCreditQuality();
		$creditArr = Application_Helper_Array::listArray($getCreditQuality, 'CODE', 'DESCRIPTION');
		$this->view->creditArr = $creditArr;

		$getAgreement = $model->getAgreement();
		$agreementArr = Application_Helper_Array::listArray($getAgreement, 'AGREEMENT_CODE', 'AGREEMENT_DESC');
		$this->view->agreementArr = $agreementArr;

		$debitedAcctArr = $model->getCustAcctById($custId);
		$this->view->debitedAcctArr = $debitedAcctArr;

		if ($this->_request->isPost()) {

			$filters = array('*' => array('StringTrim', 'StripTags'));

			$zf_filter_input = new Zend_Filter_Input($filters, null, $this->_request->getPost());

			$cust_segment 		= $zf_filter_input->cust_segment;
			$grace_period 		= $zf_filter_input->grace_period;
			$agreement_type 	= $zf_filter_input->agreement_type;
			$pks_number 		= $zf_filter_input->pks_number;
			$pks_file 			= $_FILES['pks_file'];
			$pks_start_date 	= $zf_filter_input->pks_start_date;
			$pks_exp_date 		= $zf_filter_input->pks_exp_date;
			$claim_deadline 	= $zf_filter_input->claim_deadline;
			$doc_deadline 		= $zf_filter_input->doc_deadline;
			$fee_debited 		= $zf_filter_input->fee_debited;
			$debited_acct 		= $zf_filter_input->debited_acct;
			$escrow_acct 		= $zf_filter_input->escrow_acct;
			$percentplafond 	= $zf_filter_input->percentplafond;
			$plafond_limit 		= Application_Helper_General::convertDisplayMoney($zf_filter_input->plafond_limit);
			$ticket_size 		= Application_Helper_General::convertDisplayMoney($zf_filter_input->ticket_size);
			$ccy 				= $zf_filter_input->ccy;
			$marginal_deposit 	= Application_Helper_General::convertDisplayMoney($zf_filter_input->marginal_deposit);
			$fee_provision 		= $zf_filter_input->fee_provision;
			$fee_admin 			= Application_Helper_General::convertDisplayMoney($zf_filter_input->fee_admin);
			$fee_stamp 			= Application_Helper_General::convertDisplayMoney($zf_filter_input->fee_stamp);

			$this->_db->delete("TEMP_CUST_LINEFACILITY_DETAIL", [
				'CUST_ID = ? ' => $data["CUST_ID"]
			]);

			// insert ke M_CUST_LINEFACILITY_DETAIL -----------------------------------------------------------

			$all_jaminan = [
				"JAMINAN PEMBAYARAN",
				"JAMINAN PENAWARAN",
				"JAMINAN PELAKSANAAN",
				"JAMINAN UANG MUKA",
				"JAMINAN PEMELIHARAAN",
				"JAMINAN LAINNYA"
			];

			$get_cust_model = $this->_db->fetchOne(
				$this->_db->select()
					->from("M_CUSTOMER", ["CUST_MODEL"])
					->where("CUST_ID = ?", $data["CUST_ID"])
			);

			$get_all_jaminan = $this->_db->fetchAll(
				$this->_db->select()
					->from("M_CHARGES_BG")
					->where("CHARGES_ID LIKE ?", ($get_cust_model == 1) ? "%LF%" : "%INS%")
			);

			$hitung_bumn = 0;
			$hitung_nonbumn = 6;

			foreach ($all_jaminan as $jaminan) {
				if ($this->_request->getParam(str_replace(" ", "_", $jaminan))) {
					$flag = 1;
				} else {
					$flag = 0;
				}

				switch ($jaminan) {
					case 'JAMINAN PEMBAYARAN':
						$percentage_bumn = $this->_request->getPost(strtolower(str_replace(" ", "_", $jaminan) . "_bumn"));
						$percentage_nonbumn = $this->_request->getPost(strtolower(str_replace(" ", "_", $jaminan) . "_nonbumn"));
						break;

					case 'JAMINAN PENAWARAN':
						$percentage_bumn = $this->_request->getPost(strtolower(str_replace(" ", "_", $jaminan) . "_bumn"));
						$percentage_nonbumn = $this->_request->getPost(strtolower(str_replace(" ", "_", $jaminan) . "_nonbumn"));
						break;

					case 'JAMINAN PELAKSANAAN':
						$percentage_bumn = $this->_request->getPost(strtolower(str_replace(" ", "_", $jaminan) . "_bumn"));
						$percentage_nonbumn = $this->_request->getPost(strtolower(str_replace(" ", "_", $jaminan) . "_nonbumn"));
						break;

					case 'JAMINAN UANG MUKA':
						$percentage_bumn = $this->_request->getPost(strtolower(str_replace(" ", "_", $jaminan) . "_bumn"));
						$percentage_nonbumn = $this->_request->getPost(strtolower(str_replace(" ", "_", $jaminan) . "_nonbumn"));
						break;

					case 'JAMINAN PEMELIHARAAN':
						$percentage_bumn = $this->_request->getPost(strtolower(str_replace(" ", "_", $jaminan) . "_bumn"));
						$percentage_nonbumn = $this->_request->getPost(strtolower(str_replace(" ", "_", $jaminan) . "_nonbumn"));
						break;

					case 'JAMINAN LAINNYA':
						$percentage_bumn = $this->_request->getPost(strtolower(str_replace(" ", "_", $jaminan) . "_bumn"));
						$percentage_nonbumn = $this->_request->getPost(strtolower(str_replace(" ", "_", $jaminan) . "_nonbumn"));
						break;

					default:
						$percentage_bumn = 0;
						$percentage_nonbumn = 0;
						break;
				}

				if ($jaminan == "JAMINAN LAINNYA") {
					$jaminan = "JAMINAN LAINNYA (SP2D)";
				}

				$this->_db->insert("TEMP_CUST_LINEFACILITY_DETAIL", [
					"CUST_ID" => $data["CUST_ID"],
					"OFFER_TYPE" => $jaminan,
					"GRUP_BUMN" => 1,
					"FEE_PROVISION" => $percentage_bumn,
					"FLAG" => $flag,
					"LAST_SUGGESTED" => new Zend_Db_Expr('now()')
				]);

				$this->_db->insert("TEMP_CUST_LINEFACILITY_DETAIL", [
					"CUST_ID" => $data["CUST_ID"],
					"OFFER_TYPE" => $jaminan,
					"GRUP_BUMN" => 0,
					"FEE_PROVISION" => $percentage_nonbumn,
					"FLAG" => $flag,
					"LAST_SUGGESTED" => new Zend_Db_Expr('now()')
				]);
			}

			// ------------------------------------------------------------------------------------------------

			$valid = true;
			if (!empty($escrow_acct)) {
				$svcAccount = new Service_Account($escrow_acct, 'IDR');
				$result = $svcAccount->inquiryAccountBalance('AB', FALSE);
				//var_dump($result);
				if ($result['response_code'] == '00' || $result['response_code'] == '0000') {
					$err = false;
					$valid = true;
				} else {
					$err = true;
					$errmsg = 'Service not available';
					$errDesc['cust_segment'] = $errmsg;
					$valid = false;
				}
			}

			if (!empty($debited_acct)) {
				$svcAccount = new Service_Account($debited_acct, 'IDR');
				$result = $svcAccount->inquiryAccountBalance('AB', FALSE);
				//var_dump($result);
				if ($result['response_code'] == '00' || $result['response_code'] == '0000') {
					$err = false;
					$valid = true;
				} else {
					$err = true;
					$errmsg = 'Service not available';
					$valid = false;
				}
			}

			if (empty($cust_segment)) {
				$errDesc['cust_segment'] = $this->language->_('Segmentation cannot be empty');
			}

			if ($cust_segment == 4) {
				if (empty($grace_period)) {
					$errDesc['grace_period'] = $this->language->_('Grace Period cannot be empty');
				}
			}
			if (empty($agreement_type)) {
				$errDesc['agreement_type'] = $this->language->_('Jenis Pengikatan cannot be empty');
			}
			if (empty($pks_number)) {
				$errDesc['pks_number'] = $this->language->_('Underlying Document cannot be empty');
			}
			// if (empty($pks_file['name'])) {
			// 	$errDesc['pks_file'] = $this->language->_('Upload Document cannot be empty');
			// }
			if (empty($pks_start_date)) {
				$errDesc['pks_start_date'] = $this->language->_('Facility Start Date cannot be empty');
			}
			if (empty($pks_exp_date)) {
				$errDesc['pks_exp_date'] = $this->language->_('Facility Expired Date cannot be empty');
			}
			if (empty($claim_deadline)) {
				$errDesc['claim_deadline'] = $this->language->_('Claim Payment Deadline cannot be empty');
			}
			if (empty($plafond_limit) || $plafond_limit == 0) {
				$errDesc['plafond_limit'] = $this->language->_('Facility Plafond Limit cannot be empty');
			}

			$get_min_amt = $this->_db->select()
				->from("M_MINAMT_CCY")
				->where("CCY_ID = ?", "IDR")
				->query()->fetch();

			if (round(floatval($plafond_limit), 2) < $get_min_amt["MIN_TX_AMT"]) {
				$errDesc['plafond_limit'] = $this->language->_('Tidak boleh lebih kecil dari ' . number_format($get_min_amt["MIN_TX_AMT"], 2));
			}

			if (round(floatval($plafond_limit), 2) > round(floatval(9999999999999.99), 2)) {
				$errDesc['plafond_limit'] = $this->language->_('Tidak boleh lebih besar dari ' . number_format(9999999999999.99, 2));
			}

			if (empty($ticket_size) || $ticket_size == 0) {
				$errDesc['ticket_size'] = $this->language->_('Ticket Size cannot be empty');
			}
			if ($fee_provision > 100) {
				$errDesc['fee_provision'] = $this->language->_(' Bank Provision Fee maximal 100%');
			}
			if ($fee_admin > 9999999999999) {
				$errDesc['fee_admin'] = $this->language->_(' Bank Administration Fee cannot more then 9,999,999,999,999');
			}
			if ($fee_stamp > 9999999999999) {
				$errDesc['fee_stamp'] = $this->language->_(' Bank Stamp Fee cannot more then 9,999,999,999,999');
			}
			if ($cust_segment == 4) {
				if (empty($doc_deadline)) {
					$errDesc['doc_deadline'] = $this->language->_('Document Submission Deadline cannot be empty');
				}
				if (empty($fee_debited)) {
					$errDesc['fee_debited'] = $this->language->_('Fee Will be Debited from cannot be empty');
				} else {
					if ($fee_debited == 2) {
						if (empty($debited_acct)) {
							$errDesc['debited_acct'] = $this->language->_('Debited Account cannot be empty');
						}
					}
				}
				$percent10 = (10 / 100) * (float)$plafond_limit;
				if ($ticket_size > $percent10) {
					$errDesc['ticket_size'] = $this->language->_('Ticket Size is more than 10% plafond limit');
				}
				if (empty($marginal_deposit) || $marginal_deposit == 0) {
					$errDesc['marginal_deposit'] = $this->language->_('Marginal Deposit cannot be empty');
				}
			}

			// check rekening pembayaran klaim ---------------------------------------------------------------

			if (!empty($escrow_acct)) {
				$svcRekeningPembayaranKlaim = new Service_Account($escrow_acct, "");
				if ($svcRekeningPembayaranKlaim->inquiryAccontInfo()["response_code"] == "0000") {
					if ($svcRekeningPembayaranKlaim->inquiryAccountBalance()["response_code"] == "0000") {
						if ($svcRekeningPembayaranKlaim->inquiryAccountBalance()["status"] != 1 && $svcRekeningPembayaranKlaim->inquiryAccountBalance()["status"] != 4) {
							$errDesc['escrow_acct'] = $this->language->_('Status rekening tidak aktif');
						}

						if ($svcRekeningPembayaranKlaim->inquiryAccountBalance()["account_type"] != "D") {
							$errDesc['escrow_acct'] = $this->language->_('Tipe rekening bukan giro / tabungan');
						}

						$check_prod_type = $this->_db->select()
							->from("M_PRODUCT_TYPE")
							->where("PRODUCT_CODE = ?", $svcRekeningPembayaranKlaim->inquiryAccountBalance()["account_type"])
							->query()->fetch();

						if (empty($check_prod_type)) {
							$errDesc['escrow_acct'] = $this->language->_('Tipe rekening tidak ada di tipe produk');
						}
					} else {
						if ($svcRekeningPembayaranKlaim->inquiryDeposito()["status"] != 1 && $svcRekeningPembayaranKlaim->inquiryDeposito()["status"] != 4) {
							$errDesc['escrow_acct'] = $this->language->_('Status rekening tidak aktif');
						}

						if ($svcRekeningPembayaranKlaim->inquiryDeposito()["account_type"] != "D") {
							$errDesc['escrow_acct'] = $this->language->_('Tipe rekening bukan giro / tabungan');
						}

						$check_prod_type = $this->_db->select()
							->from("M_PRODUCT_TYPE")
							->where("PRODUCT_CODE = ?", $svcRekeningPembayaranKlaim->inquiryDeposito()["account_type"])
							->query()->fetch();

						if (empty($check_prod_type)) {
							$errDesc['escrow_acct'] = $this->language->_('Tipe rekening tidak ada di tipe produk');
						}
					}
					$escrow_acct_name = $svcRekeningPembayaranKlaim->inquiryAccontInfo()["account_name"];
					$escrow_ccy = ($svcRekeningPembayaranKlaim->inquiryAccontInfo()["currency"]) ?: 'IDR';
				} else {
					$errDesc['escrow_acct'] = $this->language->_('Rekening tidak ditemukan');
				}

				$check_db_escrow = $this->_db->select()
					->from("M_CUSTOMER_ACCT")
					->where("ACCT_NO = ?", $escrow_acct)
					->where("CUST_ID = ?", $data["CUST_ID"])
					->query()->fetch();

				if (empty($check_db_escrow)) {
					$errDesc['escrow_acct'] = $this->language->_('Rekening tidak ditemukan');
				} else {
					if ($check_db_escrow["ACCT_STATUS"] != 1 || $check_db_escrow["ACCT_STATUS"] != '1') $errDesc['escrow_acct'] = $this->language->_('Status rekening tidak aktif');
				}
			} else {
				$escrow_acct = "";
			}

			// -----------------------------------------------------------------------------------------------

			// check rekening pembayaran klaim ---------------------------------------------------------------

			if (!empty($debited_acct)) {
				$svcCekRekeningPembebananBiaya = new Service_Account($debited_acct, "");
				if ($svcCekRekeningPembebananBiaya->inquiryAccontInfo()["response_code"] == "0000") {
					if ($svcCekRekeningPembebananBiaya->inquiryAccountBalance()["response_code"] == "0000") {
						if ($svcCekRekeningPembebananBiaya->inquiryAccountBalance()["status"] != 1 && $svcCekRekeningPembebananBiaya->inquiryAccountBalance()["status"] != 4) {
							$errDesc['debited_acct'] = $this->language->_('Status rekening tidak aktif');
						}

						if ($svcCekRekeningPembebananBiaya->inquiryAccountBalance()["account_type"] != "D" && $svcCekRekeningPembebananBiaya->inquiryAccountBalance()["account_type"] != "S") {
							$errDesc['debited_acct'] = $this->language->_('Tipe rekening bukan giro / tabungan');
						}

						$check_prod_type = $this->_db->select()
							->from("M_PRODUCT_TYPE")
							->where("PRODUCT_CODE = ?", $svcCekRekeningPembebananBiaya->inquiryAccountBalance()["account_type"])
							->query()->fetch();

						if (empty($check_prod_type)) {
							$errDesc['debited_acct'] = $this->language->_('Tipe rekening tidak ada di tipe produk');
						}
					} else {
						if ($svcCekRekeningPembebananBiaya->inquiryDeposito()["status"] != 1 && $svcCekRekeningPembebananBiaya->inquiryDeposito()["status"] != 4) {
							$errDesc['debited_acct'] = $this->language->_('Status rekening tidak aktif');
						}

						if ($svcCekRekeningPembebananBiaya->inquiryDeposito()["account_type"] != "D" && $svcCekRekeningPembebananBiaya->inquiryDeposito()["account_type"] != "S") {
							$errDesc['debited_acct'] = $this->language->_('Tipe rekening bukan giro / tabungan');
						}

						$check_prod_type = $this->_db->select()
							->from("M_PRODUCT_TYPE")
							->where("PRODUCT_CODE = ?", $svcCekRekeningPembebananBiaya->inquiryDeposito()["account_type"])
							->query()->fetch();

						if (empty($check_prod_type)) {
							$errDesc['debited_acct'] = $this->language->_('Tipe rekening tidak ada di tipe produk');
						}
					}
					$debited_acct_name = $svcCekRekeningPembebananBiaya->inquiryAccontInfo()["account_name"];
					$escrow_ccy = ($svcCekRekeningPembebananBiaya->inquiryAccontInfo()["currency"]) ?: 'IDR';
				} else {
					$errDesc['debited_acct'] = $this->language->_('Rekening tidak ditemukan');
				}

				$check_db_escrow = $this->_db->select()
					->from("M_CUSTOMER_ACCT")
					->where("ACCT_NO = ?", $escrow_acct)
					->where("CUST_ID = ?", $data["CUST_ID"])
					->query()->fetch();

				if (empty($check_db_escrow)) {
					$errDesc['escrow_acct'] = $this->language->_('Rekening tidak ditemukan');
				} else {
					if ($check_db_escrow["ACCT_STATUS"] != 1 || $check_db_escrow["ACCT_STATUS"] != '1') $errDesc['escrow_acct'] = $this->language->_('Status rekening tidak aktif');
				}
			}

			// -----------------------------------------------------------------------------------------------

			$this->view->errDesc 			= $errDesc;
			$this->view->cust_segment 		= $cust_segment;
			$this->view->agreement_type 	= $agreement_type;
			$this->view->pks_number 		= $pks_number;
			$this->view->pks_start_date 	= $pks_start_date;
			$this->view->pks_exp_date 		= $pks_exp_date;
			$this->view->claim_deadline 	= $claim_deadline;
			$this->view->doc_deadline 		= $doc_deadline;
			$this->view->fee_debited 		= $fee_debited;
			$this->view->debited_acct 		= $debited_acct;
			$this->view->escrow_acct 		= $escrow_acct;
			$this->view->plafond_limit 		= $plafond_limit;
			$this->view->ticket_size 		= $ticket_size;
			$this->view->percentplafond 	= $percentplafond;
			$this->view->ccy 				= $ccy;
			$this->view->marginal_deposit 	= $marginal_deposit;
			$this->view->fee_provision 		= $fee_provision;
			$this->view->fee_admin 			= $fee_admin;
			$this->view->fee_stamp 			= $fee_stamp;

			if (empty($pks_file['name'])) {
				$valid = true;
			}

			// if (empty($errDesc) && $valid) {
			if (empty($errDesc)) {
				if ($ccy != "IDR") {
					$marginal_deposit = round(floatval($marginal_deposit / 100) * $plafond_limit, 2);
				}
				$path = UPLOAD_PATH . '/document/submit/';

				if (!empty($pks_file['name'])) {
					$filename_pks = strtolower(uniqid()) . '_' . strtolower(str_replace(' ', '_', $pks_file['name']));
					$path_old_pks = $pks_file['tmp_name'];
					$path_new_pks = $path . $filename_pks;
					move_uploaded_file($path_old_pks, $path_new_pks);
				} else {
					$filename_pks = $data["PKS_FILE"];
				}

				// $dir = $path.$data['PKS_FILE'];
				// if(is_file($dir)){
				// 	unlink($dir);
				// }

				if ($cust_segment == 4) {
					$content = [
						'CHANGES_ID'		=> $change_id,
						'CUST_ID'			=> $custId,
						'CUST_SEGMENT'		=> 4,
						'AGREEMENT_TYPE'	=> $agreement_type,
						'PKS_NUMBER'		=> $pks_number,
						'PKS_FILE'			=> $filename_pks,
						'PKS_START_DATE'	=> $pks_start_date,
						'PKS_EXP_DATE'		=> $pks_exp_date,
						'CLAIM_DEADLINE'	=> $claim_deadline,
						'DOC_DEADLINE'		=> $doc_deadline,
						'GRACE_PERIOD'		=> $grace_period,
						'FEE_DEBITED'		=> $fee_debited,
						'DEBITED_ACCOUNT'	=> ($fee_debited == 2) ? $debited_acct : null,
						'ESCROW_ACCT'		=> $escrow_acct ?: null,
						'ESCROW_CCY'		=> $escrow_ccy ?: null,
						'ESCROW_ACCT_NAME'	=> $escrow_acct_name ?: null,
						'PLAFOND_LIMIT'		=> $plafond_limit,
						'PERCENT_PLAFOND'	=> $percentplafond ?: null,
						'TICKET_SIZE'		=> $ticket_size,
						'MARGINAL_CCY'		=> $ccy,
						'MARGINAL_DEPOSIT'	=> $marginal_deposit,
						'FEE_PROVISION'		=> $fee_provision ?: 0,
						'FEE_ADMIN'			=> $fee_admin ?: null,
						'FEE_STAMP'			=> $fee_stamp ?: null,
						'STATUS'			=> ($data['STATUS'] == 5) ? 1 : $data['STATUS'],
						'FLAG'				=> 1,
						'ENDED_REASON'		=> $data['ENDED_REASON'],
						'TERMINATED_FILE'	=> $data['TERMINATED_FILE'],
						'TYPE'				=> 'Update'
					];
				} else {
					$content = [
						'CHANGES_ID'		=> $change_id,
						'CUST_ID'			=> $custId,
						'CUST_SEGMENT'		=> $cust_segment,
						'AGREEMENT_TYPE'	=> $agreement_type,
						'PKS_NUMBER'		=> $pks_number,
						'PKS_FILE'			=> $filename_pks,
						'PKS_START_DATE'	=> $pks_start_date,
						'PKS_EXP_DATE'		=> $pks_exp_date,
						'CLAIM_DEADLINE'	=> $claim_deadline,
						'GRACE_PERIOD'		=> $grace_period,
						'ESCROW_ACCT'		=> $escrow_acct ?: null,
						'ESCROW_CCY'		=> $escrow_ccy ?: null,
						'ESCROW_ACCT_NAME'	=> $escrow_acct_name ?: null,
						'PLAFOND_LIMIT'		=> $plafond_limit,
						'PERCENT_PLAFOND'	=> $percentplafond ?: null,
						'TICKET_SIZE'		=> $plafond_limit,
						'FEE_PROVISION'		=> $fee_provision ?: 0,
						'FEE_ADMIN'			=> $fee_admin ?: null,
						'FEE_STAMP'			=> $fee_stamp ?: null,
						'STATUS'			=> ($data['STATUS'] == 5) ? 1 : $data['STATUS'],
						'FLAG'				=> 1,
						'ENDED_REASON'		=> $data['ENDED_REASON'],
						'TERMINATED_FILE'	=> $data['TERMINATED_FILE'],
						'TYPE'				=> 'Update'
					];
				}

				$tempData = new Zend_Session_Namespace('tempData');
				$tempData->content = $content;
				$this->_redirect('linefacility/index/confirm');
			}

			if ($err) {
				//$err = true;
				//$errmsg = 'Service not available';

				$this->view->err = true;
				$this->view->errmsg = $errmsg;
			}
		} else {
			// Application_Helper_General::writeLog('UFBO', 'View Update Customer Line Facility');
		}
	}

	public function unfreezeAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();

		$model = new linefacility_Model_Linefacility();

		$id = $this->_getParam('id');

		$sessToken 	= new Zend_Session_Namespace('Tokenenc');
		$password 	= $sessToken->token;

		$AESMYSQL = new Crypt_AESMYSQL();
		$decryption = urldecode($id);
		$decryption_id = $AESMYSQL->decrypt($decryption, $password);

		$data = $model->getDataById($decryption_id);

		$cust_id = $data['CUST_ID'];

		$getCustomer  = $model->getCustomerById($cust_id);
		$cust_name    = $getCustomer['CUST_NAME'];
		$info         = 'Customer ID = ' . $cust_id . ', Customer Name = ' . $cust_name;
		$change_id    = $this->suggestionWaitingApproval('Line Facility', $info, $this->_changeType['code']['edit'], null, 'M_CUST_LINEFACILITY', 'TEMP_CUST_LINEFACILITY', $cust_id, $cust_name, $cust_id, $cust_name);

		$check_marginal = $this->_db->select()
			->from("M_MARGINALDEPOSIT")
			->where("CUST_ID = ?", $cust_id)
			->query()->fetch();

		$flashMessanger = $this->_helper->getHelper('FlashMessenger');
		$save_error["error"] = [];

		if ($getCustomer['CUST_MODEL'] == 2) {
			if ($check_marginal["CG_COMPLETENESS"] == 0) {
				array_push($save_error["error"], "Terdapat kekurangan Dokumen Kontra Garansi");
			}

			if ($check_marginal["CG_COMPLETENESS"] == 0) {
				array_push($save_error["error"], "MD saat ini kurang dari yang dipersyaratkan");
			}

			if ($save_error) {
				$flashMessanger->addMessage($save_error);
			}

			if ($check_marginal["CG_COMPLETENESS"] == 0 || $check_marginal["CG_COMPLETENESS"] == 0) {
				return $this->_redirect('/linefacility/index/detail/id/' . urldecode($id));
			}
		}
		try {
			$this->_db->beginTransaction();

			if ($data['CUST_SEGMENT'] == 4) {
				$dataInsert = [
					'CHANGES_ID'		=> $change_id,
					'CUST_ID'			=> $cust_id,
					'CUST_SEGMENT'		=> 4,
					'AGREEMENT_TYPE'	=> $data['AGREEMENT_TYPE'],
					'PKS_NUMBER'		=> $data['PKS_NUMBER'],
					'PKS_FILE'			=> $data['PKS_FILE'],
					'PKS_START_DATE'	=> $data['PKS_START_DATE'],
					'PKS_EXP_DATE'		=> $data['PKS_EXP_DATE'],
					'CLAIM_DEADLINE'	=> $data['CLAIM_DEADLINE'],
					'DOC_DEADLINE'		=> $data['DOC_DEADLINE'],
					'FEE_DEBITED'		=> $data['FEE_DEBITED'],
					'PAYMENT_ACCT'		=> $data['PAYMENT_ACCT'],
					'ESCROW_ACCT'		=> $data['ESCROW_ACCT'],
					'DEBITED_ACCOUNT'	=> ($data['FEE_DEBITED'] == 2) ? $data['DEBITED_ACCOUNT'] : null,
					'PLAFOND_LIMIT'		=> $data['PLAFOND_LIMIT'],
					'TICKET_SIZE'		=> $data['TICKET_SIZE'],
					'PERCENT_PLAFOND'	=> $data['PERCENT_PLAFOND'],
					"GRACE_PERIOD" 		=> $data["GRACE_PERIOD"],
					'MARGINAL_CCY'		=> $data['MARGINAL_CCY'],
					'MARGINAL_DEPOSIT'	=> $data['MARGINAL_DEPOSIT'],
					'FEE_PROVISION'		=> $data['FEE_PROVISION'] ?: 0,
					'FEE_ADMIN'			=> $data['FEE_ADMIN'] ?: null,
					'FEE_STAMP'			=> $data['FEE_STAMP'] ?: null,
					'STATUS'			=> 5, // Freeze
					'FLAG'				=> 1, // Waiting Review
					'ENDED_REASON'		=> $reason,
					'LAST_SUGGESTED'	=> new Zend_Db_Expr('now()'),
					'LAST_SUGGESTEDBY'	=> $this->_userIdLogin
				];
			} else {
				$dataInsert = [
					'CHANGES_ID'		=> $change_id,
					'CUST_ID'			=> $cust_id,
					'AGREEMENT_TYPE'	=> $data['AGREEMENT_TYPE'],
					'CUST_SEGMENT'		=> $data['CUST_SEGMENT'],
					'PKS_NUMBER'		=> $data['PKS_NUMBER'],
					'PKS_FILE'			=> $data['PKS_FILE'],
					'PKS_START_DATE'	=> $data['PKS_START_DATE'],
					'PKS_EXP_DATE'		=> $data['PKS_EXP_DATE'],
					'CLAIM_DEADLINE'	=> $data['CLAIM_DEADLINE'],
					'PAYMENT_ACCT'		=> $data['PAYMENT_ACCT'],
					'PLAFOND_LIMIT'		=> $data['PLAFOND_LIMIT'],
					'TICKET_SIZE'		=> $data['TICKET_SIZE'],
					'FEE_PROVISION'		=> $data['FEE_PROVISION'],
					'FEE_ADMIN'			=> $data['FEE_ADMIN'],
					'FEE_STAMP'			=> $data['FEE_STAMP'] ?: null,
					'STATUS'			=> 5, // Unfreeze
					'FLAG'				=> 1,
					'LAST_SUGGESTED'	=> new Zend_Db_Expr('now()'),
					'LAST_SUGGESTEDBY'	=> $this->_userIdLogin
				];
			}
			$this->_db->insert('TEMP_CUST_LINEFACILITY', $dataInsert);

			$this->_db->delete("TEMP_CUST_LINEFACILITY_DETAIL", [
				'CUST_ID = ? ' => $cust_id
			]);

			$get_all_provision = $this->_db->fetchAll(
				$this->_db->select()
					->from("M_CUST_LINEFACILITY_DETAIL")
					->where("CUST_ID = ?", $cust_id)
			);

			foreach ($get_all_provision as $provision) {
				$this->_db->insert("TEMP_CUST_LINEFACILITY_DETAIL", $provision);
			}

			$this->_db->commit();

			Application_Helper_General::writeLog('OFBO', 'Customer Line Facility Unfreeze Submission CUST_ID: ' . $cust_id);
			$this->setbackURL('/linefacility');
			$this->_redirect('/notification/submited/index');
		} catch (Exception $error) {
			$this->_db->rollBack();
			echo '<pre>';
			print_r($error->getMessage());
			echo '</pre><br>';
			die;
		}
	}

	public function checkcustomerAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();

		$request = $this->getRequest();

		$cust_id = $request->cust_id;

		$model = new linefacility_Model_Linefacility();

		$getCustomer = $model->getCustomerById($cust_id);

		$cust_model = $getCustomer['CUST_MODEL'];

		$getCreditQuality = $model->getCreditQuality();
		$creditArr = Application_Helper_Array::listArray($getCreditQuality, 'CODE', 'DESCRIPTION');

		if ($getCustomer['COLLECTIBILITY_CODE'] == null) {
			$collect_code = '-';
		} else {
			$collect_code = $getCustomer['COLLECTIBILITY_CODE'] . ' - ' . $creditArr[$getCustomer['COLLECTIBILITY_CODE']];
		}

		if ($this->_request->getParam("onboarding")) {
			$getCustAcctById = $model->getCustAcctById($cust_id, "Giro", ["ACCT_STATUS" => "1"]);
		} else {
			$getCustAcctById = $model->getCustAcctById($cust_id, "Giro");
		}

		$getTempLinefacility = $model->getTempLinefacility($cust_id);
		if ($getTempLinefacility) {
			$is_onboarding = true;
		} else {
			$is_onboarding = false;
		}

		echo json_encode(array('cust_model' => $cust_model, 'collect_code' => $collect_code, 'debited_acct' => $getCustAcctById, 'is_onboarding' => $is_onboarding, 'bumn' => $getCustomer['GRUP_BUMN']));
	}

	public function checkescrowacctAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();

		$request = $this->getRequest();

		$cust_id = $request->cust_id;
		$payment_acct = (empty($request->payment_acct)) ? "" : $request->payment_acct;

		$get_acct = $this->_db->select()
			->from("M_CUSTOMER_ACCT")
			->where("CUST_ID = ?", $cust_id)
			->where("ACCT_TYPE = ?", "20")
			->query()->fetchAll();

		// echo json_encode($get_acct);


		if (count($get_acct) > 0) {
			echo '<option value="">-- Pilih Rekening --</option>';
			foreach ($get_acct as $key => $value) {
				$selected = ($payment_acct == $value["ACCT_NO"]) ? "selected" : "";
				echo '<option value="' . $value["ACCT_NO"] . '" ' . $selected . '>' . $value["ACCT_NO"] .  '-' . $value["ACCT_NAME"] . '</option>';
			}
		} else {
			echo '<option value="">-- Pilih Rekening --</option>';
		}
	}

	public function checkexpiredAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();

		$model = new linefacility_Model_Linefacility();

		$data = $model->getData();
		$curdate = date('Y-m-d');

		$expired = 0;
		foreach ($data as $row) {
			if ($row['STATUS'] != 3) {
				if (strtotime($row["PKS_EXP_DATE"]) < strtotime($curdate)) {
					$dataUpdate = ['STATUS' => 3];
					$dataWhere  = ['ID = ?' => $row['ID']];
					$this->_db->update('M_CUST_LINEFACILITY', $dataUpdate, $dataWhere);
					$expired++;
				}
			}
		}
		if ($expired > 0) {
			echo json_encode(array('expired' => TRUE));
		} else {
			echo json_encode(array('expired' => FALSE));
		}
	}

	public function checkfreezeAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();

		$model = new linefacility_Model_Linefacility();

		$data = $model->getData();
		$curdate = date('Y-m-d');

		$freeze = 0;
		foreach ($data as $row) {
			if ($row['STATUS'] != 4) {
				$startDate = $row['PKS_START_DATE'];
				$expDate = $row["PKS_EXP_DATE"];
				$day = $row['CLAIM_DEADLINE'];
				$deadlineDate = date('Y-m-d', strtotime("+$day day", strtotime($startDate)));
				// if ($deadlineDate < $curdate) {
				if (date('Y-m-d', strtotime($expDate)) < $curdate) {
					$dataUpdate = ['STATUS' => 4];
					$dataWhere  = ['ID = ?' => $row['ID']];
					$this->_db->update('M_CUST_LINEFACILITY', $dataUpdate, $dataWhere);
					$freeze++;
				}
			}
		}
		if ($freeze > 0) {
			echo json_encode(array('freeze' => TRUE));
		} else {
			echo json_encode(array('freeze' => FALSE));
		}
	}

	public function downloadAction()
	{
		$change_id = $this->_request->getParam("changeid");
		$underlying = $this->_request->getParam("underlying");
		$master = $this->_request->getParam("master");

		$temp_cust_lf = $this->_db->select()
			->from("TEMP_CUST_LINEFACILITY")
			->where("CHANGES_ID = ?", $change_id)
			->query()->fetch();

		if ($master) {
			$get_master = $this->_db->select()
				->from("M_CUST_LINEFACILITY")
				->where("CUST_ID = ?", $temp_cust_lf["CUST_ID"])
				->query()->fetch();
			$temp_cust_lf = $get_master;
		}

		if ($underlying) {
			if (file_exists("/app/bg/library/data/uploads/document/submit/" . $temp_cust_lf["PKS_FILE"])) {
				$file_url = "/app/bg/library/data/uploads/document/submit/" . $temp_cust_lf["PKS_FILE"];
				header('Content-Description: File Transfer');
				header('Content-Type: application/octet-stream');
				header('Content-Disposition: attachment; filename="' . $temp_cust_lf["PKS_FILE"] . '"');
				header('Content-Transfer-Encoding: binary');
				header('Expires: 0');
				header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
				header('Pragma: public');
				header('Content-Length: ' . filesize($file_url)); //Absolute URL
				ob_clean();
				flush();
				readfile($file_url); //Absolute URL
				exit();
			}
		}
	}

	public function getprovisionAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();

		$cust_model = $this->_request->getParam("cust_model");

		$get_all_provision = $this->_db->fetchAll(
			$this->_db->select()
				->from("M_CHARGES_BG")
				->where("CHARGES_ID LIKE ?", ($cust_model == 1) ? "%LF%" : "%INS%")
				->order("CHARGES_ID ASC")
		);

		$get_others = $this->_db->select()
			->from("M_CHARGES_BG")
			->where("CHARGES_ID IN (?)", ["METERAI01", "ADM01"])
			->order("CHARGES_ID ASC")
			->query()->fetchAll();

		header("Content-Type: application/json");
		echo json_encode([
			"provision" => $get_all_provision,
			"others" => $get_others
		]);
	}
}
