<?php
require_once 'Zend/Controller/Action.php';
require_once("Service/Account.php");
class linefacility_SuggestiondetailController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newpopup');

		$model = new linefacility_Model_Linefacility();

		$change_id = $this->_getParam('changes_id');
		$change_id = (Zend_Validate::is($change_id, 'Digits')) ? $change_id : 0;
		$this->view->change_id = $change_id;

		$this->view->suggestionType = $this->_suggestType;

		if ($change_id) {
			$select = $this->_db->select()
				->from('T_GLOBAL_CHANGES', array('CHANGES_ID'))
				->where('CHANGES_ID = ' . $this->_db->quote($change_id))
				->where("CHANGES_STATUS = 'WA' OR CHANGES_STATUS = 'RR'");
			$result = $this->_db->fetchOne($select);

			if (empty($result)) {
				$this->_redirect('/notification/invalid/index');
			} else {

				$checkChangesId = $this->_db->select()
					->from(["A" => "TEMP_CHARGES_BG"], ["*"])
					// ->join(
					// 	array('G' => 'T_GLOBAL_CHANGES'),
					// 	'G.CHANGES_ID = A.CHANGES_ID',
					// 	array('G.CHANGES_ID', 'CHANGES_TYPE', 'CREATED', 'CREATED_BY', 'CHANGES_STATUS', 'READ_STATUS', 'MODULE')
					// )
					// ->where("CHARGES_TYPE = 10")
					// ->where("A.CHANGES_ID = ?", $change_id)
					->query()->fetchAll();

				if (count($checkChangesId) > 0) {

					$newCekCash = [];
					foreach ($checkChangesId as $value) {
						$newCekCash[$value["CHARGES_ID"]] = $value;
					}

					$this->view->cashCollateral = 1;
					$this->view->changes_id = $change_id;
					$this->view->all_data = $newCekCash;

					// $getCash = $this->_db->select()
					// 	->from("M_CHARGES_OTHER")
					// 	->where("CUST_ID = ? ", "GLOBAL")
					// 	->where("CHARGES_TYPE = ?", "10")
					// 	->query()->fetchAll();

					// $tempCash = $this->_db->select()
					// 	->from("TEMP_CHARGES_OTHER")
					// 	->where("CUST_ID = ? ", "GLOBAL")
					// 	->where("CHARGES_TYPE = ?", "10")
					// 	->query()->fetchAll();

					// if (count($getCash) > 0 && count($tempCash) > 0) {
					// 	$this->view->updateData = 1;
					// 	$this->view->oldData = $getCash[0];
					// }

					$submit_review = $this->_getParam('submit_review');

					if ($submit_review) {

						if (!$this->view->hasPrivilege("RCCC")) {
							return 0;
						}

						$cekChangesInformation = $this->_db->select()
							->from('T_GLOBAL_CHANGES')
							->where('CHANGES_ID = ?', $change_id)
							->query()->fetchAll();

						if (count($cekChangesInformation) > 0) {
							$this->_db->update("TEMP_CHARGES_BG", [
								"FLAG" => "1",
							], ["CHARGES_ID = ?" => "ADM01"]);
						}

						Application_Helper_General::writeLog('RCCC', 'Review BG Charges');

						header("Refresh:0");
					}

					return 0;
				}

				$checkRebateRequest = $this->_db->select()
					->from(["TRR" => "TEMP_REQUEST_REBATE"], ["*"])
					->joinleft(
						array('G' => 'T_GLOBAL_CHANGES'),
						'G.CHANGES_ID = TRR.CHANGES_ID',
						array('G.CHANGES_ID', 'CHANGES_TYPE', 'CREATED', 'CREATED_BY', 'CHANGES_STATUS', 'READ_STATUS', 'MODULE')
					)
					->where("TRR.CHANGES_ID = ?", $change_id)
					->query()->fetchAll();

				if (count($checkRebateRequest) > 0) {
					$getData = $checkRebateRequest[0];

					$this->view->requestRebate = 1;
					$this->view->changes_id = $change_id;
					$this->view->data = $getData;

					$getOldProvisionFee = $this->_db->select()
						->from("M_CHARGES_OTHER")
						->where("CUST_ID = ? ", "GLOBAL")
						->where("CHARGES_TYPE = ?", "10")
						->query()->fetchAll();

					$this->view->getOldProvisionFee = $getOldProvisionFee[0];

					$submit_review = $this->_getParam('submit_review');

					if ($submit_review) {

						if (!$this->view->hasPrivilege("RVRC")) {
							return 0;
						}

						$cekChangesInformation = $this->_db->select()
							->from('T_GLOBAL_CHANGES')
							->where('CHANGES_ID = ?', $change_id)
							->where('CHANGES_INFORMATION = ?', 'Request Rebate')
							->query()->fetchAll();


						if (count($cekChangesInformation) > 0) {
							$this->_db->update("TEMP_REQUEST_REBATE", [
								"FLAG" => "1",
								"REBATE_REVIEWED" => new Zend_Db_Expr('now()'),
								"REBATE_REVIEWEDBY" => $this->_userIdLogin,
							], ["CHANGES_ID = ?" => $change_id]);
						}

						header("Refresh:0");
					}

					return 0;
				}

				if (!$this->view->hasPrivilege("VLFC")) {
					return $this->_redirect("/home/dashboard");
				}

				$select = $this->_db->select()
					->from(array('LF' => 'TEMP_CUST_LINEFACILITY'))
					->joinLeft(
						array('B' => 'M_CUSTOMER'),
						'B.CUST_ID = LF.CUST_ID',
						array('CUST_NAME', 'COLLECTIBILITY_CODE', "GRUP_BUMN")
					)
					->join(
						array('G' => 'T_GLOBAL_CHANGES'),
						'G.CHANGES_ID = LF.CHANGES_ID',
						array('CHANGES_ID', 'CHANGES_TYPE', 'CREATED', 'CREATED_BY', 'CHANGES_STATUS', 'READ_STATUS', 'MODULE')
					)
					->where('LF.CHANGES_ID = ?', $change_id);
				$resultData = $this->_db->fetchRow($select);

				// check payment acct --------------------------------------------------------------------

				$svcPaymentAcct = new Service_Account($resultData["PAYMENT_ACCT"], "");
				$this->view->paymentAcctName = $svcPaymentAcct->inquiryAccontInfo()["account_name"];

				// ---------------------------------------------------------------------------------------


				if ($resultData) {
					$this->view->cust_id 		= $resultData['CUST_ID'];
					$this->view->changes_id 	= $resultData['CHANGES_ID'];
					$this->view->changes_type 	= $resultData['CHANGES_TYPE'];
					$this->view->changes_status = $resultData['CHANGES_STATUS'];
					$this->view->read_status 	= $resultData['READ_STATUS'];
					$this->view->flag 			= $resultData['FLAG'];
					$this->view->checkBumn 		= $resultData['GRUP_BUMN'];

					$statusArr = [
						'1'	=> 'Approved',
						'2'	=> 'Terminated',
						'3'	=> 'Expired',
						'4'	=> 'Freeze Submission',
						'5'	=> 'Unfreeze Submission'
					];
					$this->view->statusArr = $statusArr;

					$segmentationArr = [
						1 => 'CORPORATE',
						2 => 'COMMERCIAL',
						3 => 'SME',
						4 => 'FINANCIAL INSTITUTION'
					];

					$getCreditQuality = $model->getCreditQuality();
					$collectCodeArr = Application_Helper_Array::listArray($getCreditQuality, 'CODE', 'DESCRIPTION');

					$getAgreement = $model->getAgreement();
					$agreementArr = Application_Helper_Array::listArray($getAgreement, 'AGREEMENT_CODE', 'AGREEMENT_DESC');


					// DATA TEMP ////////////////////////////////////////////////////////
					$getCustomerById = $model->getCustomerById($resultData['CUST_ID']);

					$getDataByCustId = $this->_db->select()
						->from(['A' => "TEMP_CUST_LINEFACILITY"])
						->joinLeft(
							array('B' => 'M_CUSTOMER'),
							'B.CUST_ID = A.CUST_ID',
							array('CUST_NAME', 'COLLECTIBILITY_CODE', "GRUP_BUMN")
						)
						->where("A.CUST_ID = ?", $resultData["CUST_ID"])
						->query()->fetch();

					$this->view->getDataLf = $getDataByCustId;

					if ($resultData['CUST_SEGMENT'] == 4) {
						if ($resultData['FEE_DEBITED'] == 2) {
							$getCustAcctByAcct = $model->getCustAcctByAcct($resultData['DEBITED_ACCOUNT']);
							$this->view->debited_account = $getCustAcctByAcct['ACCT_NO'] . ' (' . $getCustAcctByAcct['CCY_ID'] . ')' . ' / ' . $getCustAcctByAcct['ACCT_NAME'] . ' / ' . $getCustAcctByAcct['ACCT_DESC'];
						}
					}

					$percent10 = ($resultData["PERCENT_PLAFOND"] / 100) * (float)$resultData['PLAFOND_LIMIT'];

					if ($resultData['TICKET_SIZE'] == 1) {
						if (empty($resultData['PERCENT_PLAFOND'])) {
							$resultData['PERCENT_PLAFOND'] = "-";
						}
						$ticket_size = $resultData['PERCENT_PLAFOND'] . '% (IDR ' . number_format($percent10, 2) . ')';
					} elseif ($resultData['TICKET_SIZE'] == 2) {
						$ticket_size = 'Allowed to optimized Plafond Limit';
					} else {
						$ticket_size = 'IDR ' . Application_Helper_General::displayMoneyplain($resultData['TICKET_SIZE']);
					}
					if (!empty($resultData['ESCROW_ACCT'])) {
						$resultData['PAYMENT_ACCT'] = $resultData['ESCROW_ACCT'];
					}

					$get_info_payment_acct = new Service_Account($resultData['PAYMENT_ACCT'], "");
					$get_info_payment_result = $get_info_payment_acct->inquiryAccontInfo();

					$this->view->customer 			= $getCustomerById['CUST_NAME'] . ' (' . $getCustomerById['CUST_ID'] . ')';
					$this->view->customerModel 		= $getCustomerById['CUST_MODEL'];
					$this->view->collect_code 		= $resultData['COLLECTIBILITY_CODE'] . ' - ' . $collectCodeArr[$resultData['COLLECTIBILITY_CODE']];
					$this->view->cust_segment 		= $segmentationArr[$resultData['CUST_SEGMENT']];
					$this->view->cust_segment1		= $resultData['CUST_SEGMENT'];
					$this->view->agreement_type 	= $agreementArr[$resultData['AGREEMENT_TYPE']];
					$this->view->pks_number 		= $resultData['PKS_NUMBER'];
					$this->view->pks_start_date 	= date('d M Y', strtotime($resultData['PKS_START_DATE']));
					$this->view->pks_exp_date 		= date('d M Y', strtotime($resultData['PKS_EXP_DATE']));
					$this->view->claim_deadline 	= $resultData['CLAIM_DEADLINE'];
					$this->view->grace_period	 	= $resultData['GRACE_PERIOD'];
					$this->view->doc_deadline 		= $resultData['DOC_DEADLINE'];
					// $this->view->fee_debited 		= ($resultData['FEE_DEBITED'] == 1) ? 'APPLICANT' : 'FINANCIAL INSTITUTION';
					$this->view->fee_debited 		= ($resultData['FEE_DEBITED'] == 1) ? 'Pemohon' : 'Asuransi';
					$this->view->payment_acct 		= $resultData['PAYMENT_ACCT'];
					$this->view->detail_payment_acct = ($get_info_payment_result["response_code"] == "0000") ? $get_info_payment_result["account_name"] : "()";
					// $this->view->plafond_limit 		= 'IDR ' . Application_Helper_General::displayMoneyplain($resultData['PLAFOND_LIMIT']);
					$this->view->plafond_limit 		= 'IDR ' . number_format($resultData['PLAFOND_LIMIT'], 2);
					$this->view->ticket_size 		= $ticket_size;
					// $this->view->marginal_deposit 	= 'IDR ' . Application_Helper_General::displayMoneyplain($resultData['MARGINAL_DEPOSIT']);
					$this->view->marginal_deposit 	= 'IDR ' . number_format($resultData['MARGINAL_DEPOSIT'], 2);
					$this->view->fee_provision 		= $resultData['FEE_PROVISION'];
					$this->view->fee_admin 			= 'IDR ' . Application_Helper_General::displayMoneyplain($resultData['FEE_ADMIN']);
					$this->view->fee_stamp 			= 'IDR ' . Application_Helper_General::displayMoneyplain($resultData['FEE_STAMP']);
					$this->view->status 			= $statusArr[$resultData['STATUS']];
					$this->view->reason 			= $resultData['ENDED_REASON'];

					$get_all_provision = $this->_db->fetchAll(
						$this->_db->select()
							->from("TEMP_CUST_LINEFACILITY_DETAIL")
							->where("CUST_ID = ?", $getCustomerById['CUST_ID'])
					);

					$this->view->get_all_provision = $get_all_provision;

					// DATA MASTER ////////////////////////////////////////////////////
					if ($getDataByCustId) {
						if ($getDataByCustId['CUST_SEGMENT'] == 4) {
							if ($getDataByCustId['FEE_DEBITED'] == 2) {
								$getCustAcctByAcct = $model->getCustAcctByAcct($getDataByCustId['DEBITED_ACCOUNT']);
								$this->view->m_debited_account = $getCustAcctByAcct['ACCT_NO'] . ' (' . $getCustAcctByAcct['CCY_ID'] . ')' . ' / ' . $getCustAcctByAcct['ACCT_NAME'] . ' / ' . $getCustAcctByAcct['ACCT_DESC'];
							}
						}

						$m_percent10 = ($getDataByCustId["PERCENT_PLAFOND"] / 100) * (float)$getDataByCustId['PLAFOND_LIMIT'];

						if ($getDataByCustId['TICKET_SIZE'] == 1) {
							$ticket_size = $getDataByCustId['PERCENT_PLAFOND'] . '% (IDR ' . number_format($m_percent10, 2) . ')';
						} elseif ($getDataByCustId['TICKET_SIZE'] == 2) {
							$ticket_size = 'Allowed to optimized Plafond Limit';
						} else {
							$ticket_size = 'IDR ' . Application_Helper_General::displayMoneyplain($getDataByCustId['TICKET_SIZE']);
						}
						if (!empty($getDataByCustId['ESCROW_ACCT'])) {
							$getDataByCustId['PAYMENT_ACCT'] = $getDataByCustId['ESCROW_ACCT'];
						}

						$get_info_m_payment_acct = new Service_Account($getDataByCustId['PAYMENT_ACCT'], "");
						$get_info_m_payment_result = $get_info_m_payment_acct->inquiryAccontInfo();

						$this->view->m_customer 		= $getCustomerById['CUST_NAME'] . ' (' . $getCustomerById['CUST_ID'] . ')';
						$this->view->m_collect_code 	= $getDataByCustId['COLLECTIBILITY_CODE'] . ' - ' . $collectCodeArr[$getDataByCustId['COLLECTIBILITY_CODE']];
						$this->view->m_cust_segment 	= $segmentationArr[$getDataByCustId['CUST_SEGMENT']];
						$this->view->m_agreement_type 	= $agreementArr[$getDataByCustId['AGREEMENT_TYPE']];
						$this->view->m_pks_number 		= $getDataByCustId['PKS_NUMBER'];
						$this->view->m_pks_start_date 	= date('d M Y', strtotime($getDataByCustId['PKS_START_DATE']));
						$this->view->m_pks_exp_date 	= date('d M Y', strtotime($getDataByCustId['PKS_EXP_DATE']));
						$this->view->m_claim_deadline 	= $getDataByCustId['CLAIM_DEADLINE'];
						$this->view->m_grace_period 	= $getDataByCustId['GRACE_PERIOD'];
						$this->view->m_doc_deadline 	= $getDataByCustId['DOC_DEADLINE'];
						$this->view->m_fee_debited 		= ($getDataByCustId['FEE_DEBITED'] == 1) ? 'Pemohon' : 'Asuransi';
						$this->view->m_payment_acct 	= $getDataByCustId['PAYMENT_ACCT'];
						$this->view->m_detail_payment_acct 	= ($get_info_m_payment_result["response_code"] == "0000") ? $get_info_m_payment_result["account_name"] : "()";
						// $this->view->m_plafond_limit 	= 'IDR ' . Application_Helper_General::displayMoneyplain($getDataByCustId['PLAFOND_LIMIT']);
						$this->view->m_plafond_limit 	= 'IDR ' . number_format($getDataByCustId['PLAFOND_LIMIT'], 2);
						$this->view->m_ticket_size 		= $ticket_size;
						// $this->view->m_marginal_deposit = 'IDR ' . Application_Helper_General::displayMoneyplain($getDataByCustId['MARGINAL_DEPOSIT']);
						$this->view->m_marginal_deposit = 'IDR ' . number_format($getDataByCustId['MARGINAL_DEPOSIT'], 2);
						$this->view->m_fee_provision 	= $getDataByCustId['FEE_PROVISION'];
						$this->view->m_fee_admin 		= 'IDR ' . Application_Helper_General::displayMoneyplain($getDataByCustId['FEE_ADMIN']);
						$this->view->m_fee_stamp 		= 'IDR ' . Application_Helper_General::displayMoneyplain($getDataByCustId['FEE_STAMP']);
						$this->view->m_status 			= $statusArr[$getDataByCustId['STATUS']];
						$this->view->m_reason 			= $getDataByCustId['ENDED_REASON'];

						$m_get_all_provision = $this->_db->fetchAll(
							$this->_db->select()
								->from("M_CUST_LINEFACILITY_DETAIL")
								->where("CUST_ID = ?", $getCustomerById['CUST_ID'])
						);

						$this->view->m_get_all_provision = $m_get_all_provision;
					}

					$submit_review = $this->_getParam('submit_review');
					$submit_request_repair = $this->_getParam('submit_request_repair');

					if ($submit_review) {

						if (!$this->view->hasPrivilege("RLFC")) {
							return $this->_redirect("/home/dashboard");
						}

						try {
							//$this->_db->beginTransaction();
							//echo '<pre>';
							//var_dump($resultData);die;
							/*
							if (!empty($resultData['ESCROW_ACCT'])) {
								$svcAccount = new Service_Account($resultData['ESCROW_ACCT'], null);
							} else if (!empty($resultData['PAYMENT_ACCT'])) {
								$svcAccount = new Service_Account($resultData['PAYMENT_ACCT'], null);
							}
							*/

							if (!empty($resultData['PAYMENT_ACCT'])) {

								$svcAccount = new Service_Account($resultData['PAYMENT_ACCT'], '');

								$resultSvc = $svcAccount->inquiryAccontInfo('AI', TRUE);

								if ($resultSvc["response_code"] == "0000") {
									if ($svcAccount->inquiryAccountBalance()["response_code"] == "0000") {
										if ($svcAccount->inquiryAccountBalance()["status"] != 1 && $svcAccount->inquiryAccountBalance()["status"] != 4) {
											$this->view->err_rekening_pemabayaran = $this->language->_('Status rekening tidak aktif');
											$resultSvc['response_desc'] = 'Gagal';
										} else {
											$resultSvc['response_desc'] = 'Success';
										}
									} else {
										if ($svcAccount->inquiryDeposito()["status"] != 1 && $svcAccount->inquiryDeposito()["status"] != 4) {
											$this->view->err_rekening_pemabayaran = $this->language->_('Status rekening tidak aktif');
											$resultSvc['response_desc'] = 'Gagal';
										} else {
											$resultSvc['response_desc'] = 'Success';
										}
									}
								} else {
									$this->view->err_rekening_pemabayaran = "Rekening tidak ditemukan";
									$resultSvc['response_desc'] = 'Gagal';
								}
							} else if (!empty($resultData['ESCROW_ACCT'])) {
								$svcAccount = new Service_Account($resultData['ESCROW_ACCT'], 'IDR');
								$resultSvc = $svcAccount->inquiryAccontInfo('AI', TRUE);
							} else {
								$resultSvc['response_desc'] = 'Success';
							}

							if ($resultSvc['response_desc'] == 'Success') //00 = success
							{
								//die('here');
								$data = [
									'FLAG'				=> 2,
									'LAST_REVIEWED'		=> new Zend_Db_Expr('now()'),
									'LAST_REVIEWEDBY'	=> $this->_userIdLogin
								];
								$where = ['CUST_ID = ?' => $resultData['CUST_ID']];
								$this->_db->update('TEMP_CUST_LINEFACILITY', $data, $where);
							} else {
								//die('here1');
								$this->view->error = true;
								$this->view->errorMsg = $this->view->err_rekening_pemabayaran;
							}
							//$this->_db->commit();	
							$this->view->is_review = true;
							Application_Helper_General::writeLog('RLFC', 'Review Line Facility Changes | CUST_ID : ' . $getCustomerById['CUST_ID']);
						} catch (Exception $error) {
							echo '<pre>';
							var_dump($error);
							die;
							//$this->_db->rollBack();
							echo '<pre>';
							print_r($error->getMessage());
							echo '</pre><br>';
							die;
						}

						if (!$this->view->error) {
							header("Refresh:0");
						}
					}

					if ($submit_request_repair) {
						try {
							$this->_db->beginTransaction();

							$data2  = [
								'CHANGES_STATUS'	=> 'RR',
								'LASTUPDATED'		=> new Zend_Db_Expr('now()')
							];
							$where2 = ['CHANGES_ID = ?' => $change_id];
							$this->_db->update('T_GLOBAL_CHANGES', $data2, $where2);

							$this->_db->commit();

							$this->view->is_request_repair = true;
						} catch (Exception $error) {
							$this->_db->rollBack();
							echo '<pre>';
							print_r($error->getMessage());
							echo '</pre><br>';
							die;
						}

						header("Refresh:0");
					}
				}
			}
		}
	}

	public function repairAction()
	{
		$this->_helper->layout()->setLayout('newpopup');

		$model = new linefacility_Model_Linefacility();

		$change_id = $this->_getParam('changes_id');
		$change_id = (Zend_Validate::is($change_id, 'Digits')) ? $change_id : 0;

		if (!$this->view->hasPrivilege("RPFC")) {
			return $this->_redirect("/home/dashboard");
		}

		if ($change_id) {
			$select = $this->_db->select()
				->from('T_GLOBAL_CHANGES', array('CHANGES_ID'))
				->where('CHANGES_ID = ' . $this->_db->quote($change_id))
				->where("CHANGES_STATUS = 'WA' OR CHANGES_STATUS = 'RR'");
			$result = $this->_db->fetchOne($select);

			if (empty($result)) {
				$this->_redirect('/notification/invalid/index');
			} else {
				$select = $this->_db->select()
					->from(array('LF' => 'TEMP_CUST_LINEFACILITY'))
					->joinLeft(
						array('B' => 'M_CUSTOMER'),
						'B.CUST_ID = LF.CUST_ID',
						array('CUST_NAME', 'CUST_MODEL', 'COLLECTIBILITY_CODE')
					)
					->join(
						array('G' => 'T_GLOBAL_CHANGES'),
						'G.CHANGES_ID = LF.CHANGES_ID',
						array('CHANGES_ID', 'CHANGES_TYPE', 'CREATED', 'CREATED_BY', 'CHANGES_STATUS', 'READ_STATUS', 'MODULE')
					)
					->where('LF.CHANGES_ID = ?', $change_id);
				$resultData = $this->_db->fetchRow($select);
				$this->view->data = $resultData;

				if ($resultData) {
					$custId = $resultData['CUST_ID'];
					$changes_type = $resultData['CHANGES_TYPE'];

					$segmentationArr = [
						1 => 'CORPORATE',
						2 => 'COMMERCIAL',
						3 => 'SME'
					];
					$this->view->segmentationArr = $segmentationArr;

					$getCreditQuality = $model->getCreditQuality();
					$creditArr = Application_Helper_Array::listArray($getCreditQuality, 'CODE', 'DESCRIPTION');
					$this->view->creditArr = $creditArr;

					$getAgreement = $model->getAgreement();
					$agreementArr = Application_Helper_Array::listArray($getAgreement, 'AGREEMENT_CODE', 'AGREEMENT_DESC');
					$this->view->agreementArr = $agreementArr;

					$debitedAcctArr = $model->getCustAcctById($custId);
					$this->view->debitedAcctArr = $debitedAcctArr;

					if ($this->_request->isPost()) {
						$filters = array('*' => array('StringTrim', 'StripTags'));

						$zf_filter_input = new Zend_Filter_Input($filters, null, $this->_request->getPost());

						$cust_segment 		= $zf_filter_input->cust_segment;
						$agreement_type 	= $zf_filter_input->agreement_type;
						$pks_number 		= $zf_filter_input->pks_number;
						$pks_file 			= $_FILES['pks_file'];
						$pks_start_date 	= $zf_filter_input->pks_start_date;
						$pks_exp_date 		= $zf_filter_input->pks_exp_date;
						$claim_deadline 	= $zf_filter_input->claim_deadline;
						$doc_deadline 		= $zf_filter_input->doc_deadline;
						$fee_debited 		= $zf_filter_input->fee_debited;
						$debited_acct 		= $zf_filter_input->debited_acct;
						$plafond_limit 		= Application_Helper_General::convertDisplayMoney($zf_filter_input->plafond_limit);
						$ticket_size 		= Application_Helper_General::convertDisplayMoney($zf_filter_input->ticket_size);
						$ccy 				= $zf_filter_input->ccy;
						$marginal_deposit 	= Application_Helper_General::convertDisplayMoney($zf_filter_input->marginal_deposit);
						$fee_provision 		= $zf_filter_input->fee_provision;
						$fee_admin 			= Application_Helper_General::convertDisplayMoney($zf_filter_input->fee_admin);
						$fee_stamp 			= Application_Helper_General::convertDisplayMoney($zf_filter_input->fee_stamp);

						if (empty($cust_segment)) {
							$errDesc['cust_segment'] = $this->language->_('Segmentation cannot be empty');
						}
						if (empty($agreement_type)) {
							$errDesc['agreement_type'] = $this->language->_('Jenis Pengikatan cannot be empty');
						}
						if (empty($pks_number)) {
							$errDesc['pks_number'] = $this->language->_('Underlying Document cannot be empty');
						}
						if (empty($pks_file['name'])) {
							$errDesc['pks_file'] = $this->language->_('Upload Document cannot be empty');
						}
						if (empty($pks_start_date)) {
							$errDesc['pks_start_date'] = $this->language->_('Facility Start Date cannot be empty');
						}
						if (empty($pks_exp_date)) {
							$errDesc['pks_exp_date'] = $this->language->_('Facility Expired Date cannot be empty');
						}
						if (empty($claim_deadline)) {
							$errDesc['claim_deadline'] = $this->language->_('Claim Payment Deadline cannot be empty');
						}
						if (empty($plafond_limit) || $plafond_limit == 0) {
							$errDesc['plafond_limit'] = $this->language->_('Facility Plafond Limit cannot be empty');
						}
						if (empty($ticket_size) || $ticket_size == 0) {
							$errDesc['ticket_size'] = $this->language->_('Ticket Size cannot be empty');
						}
						if ($fee_provision > 100) {
							$errDesc['fee_provision'] = $this->language->_(' Bank Provision Fee maximal 100%');
						}
						if ($fee_admin > 9999999999999) {
							$errDesc['fee_admin'] = $this->language->_(' Bank Administration Fee cannot more then 9,999,999,999,999');
						}
						if ($fee_stamp > 9999999999999) {
							$errDesc['fee_stamp'] = $this->language->_(' Bank Stamp Fee cannot more then 9,999,999,999,999');
						}
						if ($cust_segment == 4) {
							if (empty($doc_deadline)) {
								$errDesc['doc_deadline'] = $this->language->_('Document Submission Deadline cannot be empty');
							}
							if (empty($fee_debited)) {
								$errDesc['fee_debited'] = $this->language->_('Fee Will be Debited from cannot be empty');
							} else {
								if ($fee_debited == 2) {
									if (empty($debited_acct)) {
										$errDesc['debited_acct'] = $this->language->_('Debited Account cannot be empty');
									}
								}
							}
							$percent10 = (10 / 100) * (float)$plafond_limit;
							if ($ticket_size > $percent10) {
								$errDesc['ticket_size'] = $this->language->_('Ticket Size is more than 10% plafond limit');
							}
							if (empty($marginal_deposit) || $marginal_deposit == 0) {
								$errDesc['marginal_deposit'] = $this->language->_('Marginal Deposit cannot be empty');
							}
						}

						$this->view->errDesc 			= $errDesc;
						$this->view->cust_segment 		= $cust_segment;
						$this->view->agreement_type 	= $agreement_type;
						$this->view->pks_number 		= $pks_number;
						$this->view->pks_start_date 	= $pks_start_date;
						$this->view->pks_exp_date 		= $pks_exp_date;
						$this->view->claim_deadline 	= $claim_deadline;
						$this->view->doc_deadline 		= $doc_deadline;
						$this->view->fee_debited 		= $fee_debited;
						$this->view->debited_acct 		= $debited_acct;
						$this->view->plafond_limit 		= $plafond_limit;
						$this->view->ticket_size 		= $ticket_size;
						$this->view->ccy 				= $ccy;
						$this->view->marginal_deposit 	= $marginal_deposit;
						$this->view->fee_provision 		= $fee_provision;
						$this->view->fee_admin 			= $fee_admin;
						$this->view->fee_stamp 			= $fee_stamp;

						if (empty($errDesc)) {
							$path = UPLOAD_PATH . '/document/submit/';

							$filename_pks = strtolower(uniqid()) . '_' . strtolower(str_replace(' ', '_', $pks_file['name']));
							$path_old_pks = $pks_file['tmp_name'];
							$path_new_pks = $path . $filename_pks;
							move_uploaded_file($path_old_pks, $path_new_pks);

							$dir = $path . $data['PKS_FILE'];
							if (is_file($dir)) {
								unlink($dir);
							}

							try {
								$this->_db->beginTransaction();

								if ($cust_segment == 4) {
									$dataInsert = [
										'CUST_ID'			=> $custId,
										'CUST_SEGMENT'		=> $cust_segment,
										'AGREEMENT_TYPE'	=> $agreement_type,
										'PKS_NUMBER'		=> $pks_number,
										'PKS_FILE'			=> $filename_pks,
										'PKS_START_DATE'	=> $pks_start_date,
										'PKS_EXP_DATE'		=> $pks_exp_date,
										'CLAIM_DEADLINE'	=> $claim_deadline,
										'DOC_DEADLINE'		=> $doc_deadline,
										'FEE_DEBITED'		=> $fee_debited,
										'DEBITED_ACCOUNT'	=> ($fee_debited == 2) ? $debited_acct : null,
										'PLAFOND_LIMIT'		=> $plafond_limit,
										'TICKET_SIZE'		=> $ticket_size,
										'MARGINAL_CCY'		=> $ccy,
										'MARGINAL_DEPOSIT'	=> $marginal_deposit,
										'FEE_PROVISION'		=> $fee_provision ?: 0,
										'FEE_ADMIN'			=> $fee_admin ?: null,
										'FEE_STAMP'			=> $fee_stamp ?: null,
										'STATUS'			=> ($resultData['STATUS'] == 5) ? 1 : $resultData['STATUS'],
										'FLAG'				=> 1,
										'ENDED_REASON'		=> $resultData['ENDED_REASON'],
										'TERMINATED_FILE'	=> $resultData['TERMINATED_FILE'],
										'LAST_SUGGESTED'	=> new Zend_Db_Expr('now()'),
										'LAST_SUGGESTEDBY'	=> $this->_userIdLogin
									];
								} else {
									$dataInsert = [
										'CUST_ID'			=> $custId,
										'CUST_SEGMENT'		=> $cust_segment,
										'AGREEMENT_TYPE'	=> $agreement_type,
										'PKS_NUMBER'		=> $pks_number,
										'PKS_FILE'			=> $filename_pks,
										'PKS_START_DATE'	=> $pks_start_date,
										'PKS_EXP_DATE'		=> $pks_exp_date,
										'CLAIM_DEADLINE'	=> $claim_deadline,
										'PLAFOND_LIMIT'		=> $plafond_limit,
										'TICKET_SIZE'		=> $plafond_limit,
										'FEE_PROVISION'		=> $fee_provision ?: 0,
										'FEE_ADMIN'			=> $fee_admin ?: null,
										'FEE_STAMP'			=> $fee_stamp ?: null,
										'STATUS'			=> ($resultData['STATUS'] == 5) ? 1 : $resultData['STATUS'],
										'FLAG'				=> 1,
										'ENDED_REASON'		=> $resultData['ENDED_REASON'],
										'TERMINATED_FILE'	=> $resultData['TERMINATED_FILE'],
										'LAST_SUGGESTED'	=> new Zend_Db_Expr('now()'),
										'LAST_SUGGESTEDBY'	=> $this->_userIdLogin
									];
								}
								$where1 = ['CUST_ID = ?' => $custId];
								$this->_db->update('TEMP_CUST_LINEFACILITY', $dataInsert, $where1);

								$data2  = [
									'CHANGES_STATUS' => 'WA',
									'READ_STATUS'	=> 2,
									'LASTUPDATED'	=> new Zend_Db_Expr('now()')
								];
								$where2 = ['CHANGES_ID = ?' => $change_id];
								$this->_db->update('T_GLOBAL_CHANGES', $data2, $where2);

								$this->_db->commit();

								Application_Helper_General::writeLog('VLFC', 'View Line Facility Changes CUST_ID: ' . $custId);
								$this->_redirect('/popup/successpopup/index');
							} catch (Exception $error) {
								$this->_db->rollBack();
								echo '<pre>';
								print_r($error->getMessage());
								echo '</pre><br>';
								die;
							}
						}
					} else {
						Application_Helper_General::writeLog('VLFC', 'View Line Facility Changes');
					}
				}
			}
		}
	}

	public function approveAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();

		$changes_id = $this->_getParam('changes_id');
		$cust_id   = $this->_getParam('cust_id');

		$select = $this->_db->select()
			->from(array('LF' => 'TEMP_CUST_LINEFACILITY'))
			->joinLeft(
				array('B' => 'M_CUSTOMER'),
				'B.CUST_ID = LF.CUST_ID',
				array('CUST_NAME', 'COLLECTIBILITY_CODE')
			)
			->join(
				array('G' => 'T_GLOBAL_CHANGES'),
				'G.CHANGES_ID = LF.CHANGES_ID',
				array('CHANGES_ID', 'CHANGES_TYPE', 'CREATED', 'CREATED_BY', 'CHANGES_STATUS', 'READ_STATUS', 'MODULE', "DISPLAY_TABLENAME")
			)
			->where('LF.CHANGES_ID = ?', $changes_id);
		$result = $this->_db->fetchRow($select);

		if ($result["DISPLAY_TABLENAME"] == "Line Facility") {
			if (!$this->view->hasPrivilege("ALFC")) {
				echo json_encode(array('status' => 'failed', "message" => "you don't have privilege"));
				return 0;
			}
		}




		try {
			//$this->_db->beginTransaction();

			// if ($result['CHANGES_TYPE'] == 'E') {
			// 	$m_select = $this->_db->select()
			// 	->from(array('LF' => 'M_CUST_LINEFACILITY'), array('STATUS'))
			// 	->where('LF.CUST_ID = ?', $cust_id);
			// 	$m_result = $this->_db->fetchRow($m_select);

			// 	if ($m_result['STATUS'] == $result['STATUS']) {
			// 		$status = $m_result['STATUS'];
			// 	} else {
			// 		$status = ($result['STATUS'] == 5) ? 1 : $result['STATUS'];
			// 	}
			// } else {
			// 	$status = 1;
			// }

			$status = $result['STATUS'];

			//var_dump($result['CUST_SEGMENT']);die;

			if ($status == 5 || $status == '5') {
				$status = 1;
			}

			//if ($status == 3 || $status == '3' || $status == 2 || $status == '2') {
			if ($status == 3 || $status == '3') {
				$today = strtotime(date("d M Y"));
				$exp_date = strtotime($result['PKS_EXP_DATE']);

				if ($today <= $exp_date) $status = 1;
			}

			if ($result['CUST_SEGMENT'] == 4) {
				if (empty($result['PAYMENT_ACCT'])) {
					$result['PAYMENT_ACCT'] = $result['ESCROW_ACCT'];
				}
				$data = [
					'CUST_ID'			=> $cust_id,
					'CUST_SEGMENT'		=> 4,
					'AGREEMENT_TYPE'	=> $result['AGREEMENT_TYPE'],
					'PKS_NUMBER'		=> $result['PKS_NUMBER'],
					'PKS_FILE'			=> $result['PKS_FILE'],
					'PKS_START_DATE'	=> $result['PKS_START_DATE'],
					'PKS_EXP_DATE'		=> $result['PKS_EXP_DATE'],
					'CLAIM_DEADLINE'	=> $result['CLAIM_DEADLINE'],
					'GRACE_PERIOD'		=> $result['GRACE_PERIOD'],
					'DOC_DEADLINE'		=> $result['DOC_DEADLINE'],
					'FEE_DEBITED'		=> $result['FEE_DEBITED'],
					'ESCROW_ACCT'		=> $result['ESCROW_ACCT'],
					'PAYMENT_ACCT'		=> $result['PAYMENT_ACCT'],
					'DEBITED_ACCOUNT'	=> ($result['FEE_DEBITED'] == 2) ? $result['DEBITED_ACCOUNT'] : null,
					'PLAFOND_LIMIT'		=> $result['PLAFOND_LIMIT'],
					'TICKET_SIZE'		=> $result['TICKET_SIZE'],
					'MARGINAL_CCY'		=> $result['MARGINAL_CCY'],
					'PERCENT_PLAFOND'	=> $result['PERCENT_PLAFOND'],
					'MARGINAL_DEPOSIT'	=> $result['MARGINAL_DEPOSIT'],
					'FEE_PROVISION'		=> $result['FEE_PROVISION'] ?: 0,
					'FEE_ADMIN'			=> $result['FEE_ADMIN'] ?: null,
					'FEE_STAMP'			=> $result['FEE_STAMP'] ?: null,
					'STATUS'			=> $status,
					"FREEZE_MANUAL"		=> $result['FREEZE_MANUAL'],
					'ENDED_REASON'		=> $result['ENDED_REASON'],
					'TERMINATED_FILE'	=> $result['TERMINATED_FILE'],
					'LAST_SUGGESTED'	=> $result['LAST_SUGGESTED'],
					'LAST_SUGGESTEDBY'	=> $result['LAST_SUGGESTEDBY'],
					'LAST_REVIEWED'		=> $result['LAST_REVIEWED'],
					'LAST_REVIEWEDBY'	=> $result['LAST_REVIEWEDBY'],
					'LAST_APPROVED'		=> new Zend_Db_Expr('now()'),
					'LAST_APPROVEDBY'	=> $this->_userIdLogin
				];
			} else {
				$data = [
					'CUST_ID'			=> $cust_id,
					'CUST_SEGMENT'		=> $result['CUST_SEGMENT'],
					'AGREEMENT_TYPE'	=> $result['AGREEMENT_TYPE'],
					'PKS_NUMBER'		=> $result['PKS_NUMBER'],
					'PKS_FILE'			=> $result['PKS_FILE'],
					'PKS_START_DATE'	=> $result['PKS_START_DATE'],
					'PKS_EXP_DATE'		=> $result['PKS_EXP_DATE'],
					'CLAIM_DEADLINE'	=> $result['CLAIM_DEADLINE'],
					'PAYMENT_ACCT'		=> $result['PAYMENT_ACCT'],
					'GRACE_PERIOD'		=> 0,
					'PLAFOND_LIMIT'		=> $result['PLAFOND_LIMIT'],
					'PERCENT_PLAFOND'	=> $result['PERCENT_PLAFOND'],
					'TICKET_SIZE'		=> $result['TICKET_SIZE'],
					'FEE_PROVISION'		=> $result['FEE_PROVISION'] ?: 0,
					'FEE_ADMIN'			=> $result['FEE_ADMIN'] ?: null,
					'FEE_STAMP'			=> $result['FEE_STAMP'] ?: null,
					'STATUS'			=> $status,
					"FREEZE_MANUAL"		=> $result['FREEZE_MANUAL'],
					'ENDED_REASON'		=> $result['ENDED_REASON'],
					'TERMINATED_FILE'	=> $result['TERMINATED_FILE'],
					'LAST_SUGGESTED'	=> $result['LAST_SUGGESTED'],
					'LAST_SUGGESTEDBY'	=> $result['LAST_SUGGESTEDBY'],
					'LAST_REVIEWED'		=> $result['LAST_REVIEWED'],
					'LAST_REVIEWEDBY'	=> $result['LAST_REVIEWEDBY'],
					'LAST_APPROVED'		=> new Zend_Db_Expr('now()'),
					'LAST_APPROVEDBY'	=> $this->_userIdLogin
				];
			}

			$where1 = ['CUST_ID = ?' => $result['CUST_ID']];

			/*
			if (!empty($content['ESCROW_ACCT'])) {
				$svcAccount = new Service_Account($content['ESCROW_ACCT'], null);
				$resultacct = $svcAccount->inquiryAccontInfo('AI', TRUE);
			} else if (!empty($content['PAYMENT_ACCT'])) {
				$svcAccount = new Service_Account($content['PAYMENT_ACCT'], null);
				$resultacct = $svcAccount->inquiryAccontInfo('AI', TRUE);
			} else {
				$resultacct['response_desc'] = 'Success';
			}
			*/
			if (!empty($resultacct['PAYMENT_ACCT'])) {
				$svcAccount = new Service_Account($resultacct['PAYMENT_ACCT'], null);
				$resultacctacct = $svcAccount->inquiryAccontInfo('AI', TRUE);

				if ($resultacct["response_code"] == "0000") {
					if ($svcAccount->inquiryAccountBalance()["response_code"] == "0000") {
						if ($svcAccount->inquiryAccountBalance()["status"] != 1 && $svcAccount->inquiryAccountBalance()["status"] != 4) {
							$this->view->err_rekening_pemabayaran = $this->language->_('Status rekening tidak aktif');
							$resultacct['response_desc'] = 'Gagal';
						} else {
							$resultacct['response_desc'] = 'Success';
						}
					} else {
						if ($svcAccount->inquiryDeposito()["status"] != 1 && $svcAccount->inquiryDeposito()["status"] != 4) {
							$this->view->err_rekening_pemabayaran = $this->language->_('Status rekening tidak aktif');
							$resultacct['response_desc'] = 'Gagal';
						} else {
							$resultacct['response_desc'] = 'Success';
						}
					}
				} else {
					$this->view->err_rekening_pemabayaran = "Rekening tidak ditemukan";
					$resultacct['response_desc'] = 'Gagal';
				}
			} else {
				$resultacct['response_desc'] = 'Success';
			}
			//var_dump($content['ESCROW_ACCT']);die;


			// $resultacct['response_desc'] = 'Gagal';
			if ($resultacct['response_desc'] == 'Success') //00 = success
			{

				if ($result['CHANGES_TYPE'] == 'E' || $result['CHANGES_TYPE'] == 'F' || $result['CHANGES_TYPE'] == 'K' || $result['CHANGES_TYPE'] == 'T') {
					//var_dump($where1);
					//		var_dump($result);die;
					$this->_db->update('M_CUST_LINEFACILITY', $data, $where1);
				} else {
					$getCashCollateral = $this->_db->select()
						->from("T_GLOBAL_CHANGES")
						->where("CHANGES_ID = ?", $changes_id)
						->where("CHANGES_INFORMATION IN (?)", ["Cash Collateral", "Request Rebate"])
						->query()->fetchAll();

					if (count($getCashCollateral) == 0) {
						$this->_db->insert('M_CUST_LINEFACILITY', $data);
					}
				}

				$this->_db->delete("M_CUST_LINEFACILITY_DETAIL", [
					'CUST_ID = ? ' => $cust_id
				]);

				$get_provision = $this->_db->select()
					->from("TEMP_CUST_LINEFACILITY_DETAIL")
					->where("CUST_ID = ?", $cust_id)
					->query()->fetchAll();

				foreach ($get_provision as $provision) {
					$this->_db->insert("M_CUST_LINEFACILITY_DETAIL", [
						"CUST_ID" => $cust_id,
						"OFFER_TYPE" => $provision["OFFER_TYPE"],
						"GRUP_BUMN" => $provision["GRUP_BUMN"],
						"FEE_PROVISION" => $provision["FEE_PROVISION"],
						"FLAG" => $provision["FLAG"],
						"LAST_SUGGESTED" => new Zend_Db_Expr('now()')
					]);
				}

				$this->_db->delete("TEMP_CUST_LINEFACILITY_DETAIL", [
					'CUST_ID = ? ' => $cust_id
				]);

				$this->_db->delete('TEMP_CUST_LINEFACILITY', $where1);

				$data2  = [
					'CHANGES_STATUS'	=> 'AP',
					'LASTUPDATED'		=> new Zend_Db_Expr('now()')
				];
				$where2 = ['CHANGES_ID = ?' => $changes_id];

				$cekChangesInformation = $this->_db->select()
					->from('T_GLOBAL_CHANGES')
					->where('CHANGES_ID = ?', $changes_id)
					->where('CHANGES_INFORMATION = ?', 'Cash Collateral')
					->query()
					->fetchAll();

				if (count($cekChangesInformation) > 0) {
					$getRecord = $this->_db->select()
						->from("TEMP_CHARGES_OTHER")
						->where("CHANGES_ID = ?", $changes_id)
						->query()
						->fetchAll();

					$getRecord = $getRecord[0];

					if (!$this->view->hasPrivilege("ACCC")) {
						echo json_encode(array('status' => 'failed', "message" => "you don't have privilege"));
						return 0;
					}

					$dataArr = [
						"CUST_ID" => $getRecord["CUST_ID"],
						"CHARGES_TYPE" => $getRecord["CHARGES_TYPE"],
						"CHARGES_CCY" => $getRecord["CHARGES_CCY"],
						"CHARGES_PCT" => $getRecord["CHARGES_PCT"],
						"CHARGES_ADM" => $getRecord["CHARGES_ADM"],
						"CHARGES_STAMP" => $getRecord["CHARGES_STAMP"],
						"CHARGES_SUGGESTED" => $getRecord["CHARGES_SUGGESTED"],
						"CHARGES_SUGGESTEDBY" => $getRecord["CHARGES_SUGGESTEDBY"],
						"CHARGES_APPROVED" => new Zend_Db_Expr('now()'),
						"CHARGES_APPROVEDBY" => $this->_userIdLogin,
						"CHARGES_REVIEWED" => $getRecord["CHARGES_REVIEWED"],
						"CHARGES_REVIEWEDBY" => $getRecord["CHARGES_REVIEWEDBY"],
					];
					$this->_db->delete("M_CHARGES_OTHER", [
						"CUST_ID = ?" => "GLOBAL",
						"CHARGES_TYPE" => "10"
					]);
					$this->_db->insert("M_CHARGES_OTHER", $dataArr);
					$this->_db->delete("TEMP_CHARGES_OTHER", [
						"CHANGES_ID = ? " => $changes_id
					]);
				}

				$cekRequestRebate = $this->_db->select()
					->from('T_GLOBAL_CHANGES')
					->where('CHANGES_ID = ?', $changes_id)
					->where('CHANGES_INFORMATION = ?', 'Request Rebate')
					->query()->fetchAll();

				if (count($cekRequestRebate) > 0) {
					$getRecord = $this->_db->select()
						->from("TEMP_REQUEST_REBATE")
						->where("CHANGES_ID = ?", $changes_id)
						->query()->fetchAll();

					$getRecord = $getRecord[0];

					$getDataBG = $this->_db->select()
						->from("TEMP_BANK_GUARANTEE")
						->where("BG_REG_NUMBER = ?", $getRecord["BG_REG_NUMBER"])
						->query()->fetchAll();

					$getDataBG = $getDataBG[0];

					if (!$this->view->hasPrivilege("ARRC")) {
						echo json_encode(array('status' => 'failed', "message" => "you don't have privilege"));
						return 0;
					}

					// $dataArr = [
					// 	"REDEBATE_STATUS" => "1",
					// 	"REDEBATE_TYPE" => "1",
					// 	"REDEBATE_AMOUNT" => $getRecord["PROVISION_FEE"],
					// 	"CHARGES_CCY" => $getRecord["CHARGES_CCY"],
					// 	"CHARGES_PCT" => $getRecord["CHARGES_PCT"],
					// 	"CHARGES_ADM" => $getRecord["CHARGES_ADM"],
					// 	"CHARGES_STAMP" => $getRecord["CHARGES_STAMP"],
					// 	"CHARGES_SUGGESTED" => $getRecord["CHARGES_SUGGESTED"],
					// 	"CHARGES_SUGGESTEDBY" => $getRecord["CHARGES_SUGGESTEDBY"],
					// 	"CHARGES_APPROVED" => new Zend_Db_Expr('now()'),
					// 	"CHARGES_APPROVEDBY" => $this->_userIdLogin,
					// 	"CHARGES_REVIEWED" => $getRecord["CHARGES_REVIEWED"],
					// 	"CHARGES_REVIEWEDBY" => $getRecord["CHARGES_REVIEWEDBY"],
					// ];

					$tgl1 = new DateTime($getDataBG["TIME_PERIOD_START"]);
					$tgl2 = new DateTime($getDataBG["TIME_PERIOD_END"]);
					$jarak = $tgl2->diff($tgl1);

					$awal = new DateTime(date("Y") . "-01-01");
					$akhir = new DateTime(date("Y") . "-12-31");
					$daysInYear = $akhir->diff($awal);

					$total_provision_fee = floatval($getDataBG["BG_AMOUNT"]) * ($getRecord["PROVISION_FEE"] / 100) * (($jarak->days + 1) / ($daysInYear->days + 1));

					$dataArr = [
						"REDEBATE_STATUS" => "1",
						"REDEBATE_TYPE" => "1",
						"REDEBATE_AMOUNT" => $getRecord["PROVISION_FEE"],
						"PROVISION_FEE" => floatval(str_replace(",", "", number_format($total_provision_fee, 2))),
					];

					$this->_db->update("TEMP_BANK_GUARANTEE", $dataArr, ["BG_REG_NUMBER" => $getRecord["BG_REG_NUMBER"]]);
				}

				$this->_db->update('T_GLOBAL_CHANGES', $data2, $where2);

				//$this->_db->commit();

				if ($result['CHANGES_TYPE'] == 'N') {
					if ($result['CUST_SEGMENT'] == 4) {
						$this->_db->insert("M_MARGINALDEPOSIT", [
							"CUST_ID" => $data["CUST_ID"],
							"CG_COMPLETENESS" => 1,
							"CURRENT_GUARANTEE" => 0,
							"LAST_CHECK" => new Zend_Db_Expr('now()'),
							"PKS_STATUS" => 0,
							"LAST_SUGGESTED" => new Zend_Db_Expr('now()'),
							"LAST_SUGGESTEDBY" => $this->_userIdLogin,
							"LAST_REVIEWED" => new Zend_Db_Expr('now()'),
							"LAST_REVIEWEDBY" => $this->_userIdLogin,
							"LAST_APPROVED" => new Zend_Db_Expr('now()'),
							"LAST_APPROVEDBY" => $this->_userIdLogin,
						]);
					}
				}

				Application_Helper_General::writeLog('ALFC', 'Approve Line Facility Changes | CUST_ID : ' . $data["CUST_ID"]);
				echo json_encode(array('status' => 'success'));
			} else {

				$this->view->error = true;
				$this->view->errorMsg = $result['response_desc'];
				header("Content-Type : application/json");
				echo json_encode(array('status' => 'failed', "message" => $this->view->err_rekening_pemabayaran));
			}
		} catch (Exception $error) {
			//var_dump($error);die;
			$message = $error->getMessage();
			echo json_encode(array('status' => 'failed', 'message' => $message));
		}
	}

	public function rejectAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();

		$changes_id = $this->_getParam('changes_id');
		$cust_id   = $this->_getParam('cust_id');
		$reject_note   = $this->_getParam('reject_note');

		try {
			$this->_db->beginTransaction();

			$cekChangesInformation = $this->_db->select()
				->from('T_GLOBAL_CHANGES')
				->where('CHANGES_ID = ?', $changes_id)
				->where('DISPLAY_TABLENAME = ?', 'BG Charges')
				->query()
				->fetchAll();

			if (count($cekChangesInformation) > 0) {

				if (!$this->view->hasPrivilege("ACCC") && !$this->view->hasPrivilege("RCCC")) {
					echo json_encode(array('status' => 'failed', "message" => "you don't have privilege"));
					return 0;
				}

				$this->_db->delete("TEMP_CHARGES_BG");

				$codeLog = "RCCC";
				$isian = "Reject BG Charges | Note : " . $reject_note;
			}

			$cekRequestRebate = $this->_db->select()
				->from('T_GLOBAL_CHANGES')
				->where('CHANGES_ID = ?', $changes_id)
				->where('CHANGES_INFORMATION = ?', 'Request Rebate')
				->query()->fetchAll();

			if (count($cekRequestRebate) > 0) {
				if (!$this->view->hasPrivilege("RVRC") && !$this->view->hasPrivilege("ARRC")) {
					echo json_encode(array('status' => 'failed', "message" => "you don't have privilege"));
					return 0;
				}

				$getRecord = $this->_db->select()
					->from("TEMP_REQUEST_REBATE")
					->where("CHANGES_ID = ?", $changes_id)
					->query()->fetchAll();

				$getRecord = $getRecord[0];

				$this->_db->update("TEMP_BANK_GUARANTEE", [
					"REDEBATE_STATUS" => "2"
				], [
					"BG_REG_NUMBER" => $getRecord["BG_REG_NUMBER"]
				]);
			}

			$cekker = $this->_db->select()
				->from('T_GLOBAL_CHANGES')
				->where('CHANGES_ID = ?', $changes_id)
				->query()
				->fetchAll();

			if ($cekker[0]["DISPLAY_TABLENAME" == "Line Facility"]) {
				if (!$this->view->hasPrrivilege("RLFC") && !$this->view->hasPrrivilege("ALFC")) {
					echo json_encode(array('status' => 'failed', "message" => "you don't have privilege"));
					return 0;
				}
			}

			$where1 = ['CUST_ID = ?' => $cust_id];
			$this->_db->delete('TEMP_CUST_LINEFACILITY', $where1);

			$data2  = [
				'CHANGES_STATUS'	=> 'RJ',
				'CHANGES_REASON'	=> $reject_note,
				'LASTUPDATED'		=> new Zend_Db_Expr('now()')
			];
			$where2 = ['CHANGES_ID = ?' => $changes_id];
			$this->_db->update('T_GLOBAL_CHANGES', $data2, $where2);

			$this->_db->commit();

			echo json_encode(array('status' => 'success'));
			Application_Helper_General::writeLog(($codeLog) ?: 'RLFC', ($isian) ?: 'Reject Customer Line Facility Changes CUST_ID : ' . $cekker[0]["COMPANY_CODE"]);
		} catch (Exception $error) {
			$message = $error->getMessage();
			echo json_encode(array('status' => 'failed', 'message' => $message));
		}
	}
}
