<?php

require_once 'Zend/Controller/Action.php';

class Transpurpose_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
	   $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		 if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}

    	$code = $this->language->_('Code');
		$category = $this->language->_('Category');
		$description = $this->language->_('Description');
		
	    $fields = array(
						'CODE'  => array('field' => 'CODE',
											      'label' => $code,
											      'sortable' => true),
						'CATEGORY'     => array('field' => 'CATEGORY',
											      'label' => $category,
											      'sortable' => true),
			    		'DESCRIPTION'     => array('field' => 'DESCRIPTION',
							    				'label' => $description,
							    				'sortable' => true)
				      );

		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','CODE');
		$sortDir = $this->_getParam('sortdir','asc');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;

		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';

		//get filtering param
		/*$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	'filter' 	  	 => array('StringTrim','StripTags'),
							'clearing_code'  => array('StringTrim','StripTags'),
							'CITY'      => array('StringTrim','StripTags'),
							'bank_name'      => array('StringTrim','StripTags'),
							'swift_code'     => array('StringTrim','StripTags')
		);

		$zf_filter = new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		$filter = $zf_filter->getEscaped('filter');*/

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');	
		//$filter_lg = $this->_getParam('filter');	
		
		$getData = Application_Helper_Array::SimpleArray($fields,'field');

		$select = $this->_db->select()
					        ->from(array('A' => 'M_TRANSACTION_PURPOSE'),$getData);


		//if($filter == 'Set Filter' || $filter == 'Mengatur Filter' || $csv || $pdf) 
		if($csv || $pdf)
		{  
		    $header = Application_Helper_Array::simpleArray($fields,'label');

			/*$fclearing_code = $zf_filter->getEscaped('clearing_code');
			$fbank_name     = $zf_filter->getEscaped('bank_name');
			$fswift_code    = $zf_filter->getEscaped('swift_code');
			$fcity_code     = $zf_filter->getEscaped('CITY');

	        if($fclearing_code) $select->where('UPPER(CLR_CODE) LIKE '.$this->_db->quote('%'.strtoupper($fclearing_code).'%'));
	        if($fbank_name)     $select->where('BANK_NAME LIKE '.$this->_db->quote('%'.strtoupper($fbank_name).'%'));
	        if($fswift_code)    $select->where('UPPER(SWIFT_CODE) LIKE '.$this->_db->quote('%'.strtoupper($fswift_code).'%'));
	        if($fcity_code)          $select->where('UPPER(CITY_CODE) LIKE '.$this->_db->quote('%'.strtoupper($fcity_code).'%'));
			
			$this->view->clearing_code = $fclearing_code;
			$this->view->bank_name     = $fbank_name;
			$this->view->swift_code    = $fswift_code;
			$this->view->CITY     = $fcity_code;*/
		}

		//$this->view->success = true;
		$select->order($sortBy.' '.$sortDir);  
		
		$select = $this->_db->fetchall($select);
		
		//menghilangkan index/key BANK_ID utk kepentingan CSV dan PDF
		/*$selectPdfCsv = $select;
		foreach($selectPdfCsv as $key => $row)
		{
		    unset($selectPdfCsv[$key]['BANK_ID']);
		}*/
		

		//--------konfigurasicsv dan pdf---------
	    if($csv)
	    {		
				$this->_helper->download->csv($header,$select,null,'Transaction Purpose');
				Application_Helper_General::writeLog('TPLS','Download CSV Transaction Purpose');
		}
		else if($pdf)
		{
				$this->_helper->download->pdf($header,$select,null,'Transaction Purpose');
				Application_Helper_General::writeLog('TPLS','Download PDF Transaction Purpose');
		}
		else if($this->_request->getParam('print') == 1){
             $this->_forward('print', 'index', 'widget', array('data_content' => $select, 'data_caption' => 'Transaction Purpose', 'data_header' => $fields));
       	}
		else
		{		
				Application_Helper_General::writeLog('TPLS','View Transaction Purpose');
		}	
		//-------END konfigurasicsv dan pdf------------

		$this->paging($select);
		$this->view->fields = $fields;
		$this->view->filter = $filter;

	}

}