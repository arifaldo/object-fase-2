<?php
/**
 * Tempregisterechannel model
 *
 * @author Novita
 * @version 1.0
 */

require_once 'Service/Token.php';
require_once 'General/CustomerUser.php';
require_once 'General/Settings.php';

class Echanrequest_Model_Tempregisterechannel extends Echanrequest_Model_Tempchanges
{
	protected $_moduleId = 'REC';
	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function approveNew($actor = null,$additional = array()){

		// echo "<pre>";
		// var_dump($additional);die;
		$reg_type = $additional['reg_type'];
		$tableName = 'TEMP_MYONLINE_'.$reg_type;

		$regis = $this->dbObj->select()
							->from($tableName)
				  			->where('CHANGES_ID = ?',$this->_changeId)
				  			->query()
				  			->fetch(Zend_Db::FETCH_ASSOC);

		if(!count($regis)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(e-Registration)';
			return false;
        }

        if($reg_type == 'BIS'){

        	//--------------------- CHECK ACCOUNT -----------------

        	// $acctList = $additional['acctlist'];
        	$tempcust = $regis['CUST_ID'];
        	$finalAcct = array();
        	$acctctr = 0;

	   //      foreach($acctList as $key => $val){
	   //      	$acct = $this->dbObj->select()
    //     							->from('T_MYONLINE_BIS_ACC')
    //     							->where('ACCT_NO = ?', $val)
    //     							->where('CUST_ID = ?', $tempcust)
    //     							->query()
    //     							->fetch(Zend_Db::FETCH_ASSOC);

    //     		$app = Zend_Registry::get('config');
				// $app = $app['app']['bankcode'];

				// $acct_no = $acct['ACCT_NO'];
				// $ccy_code = Application_Helper_General::getCurrNum($acct['ACCT_CCY']);

    //     		$svcAccount = new Service_Account($acct_no,$ccy_code,$app);
    //     		$accinq = $svcAccount->accountInquiry();

    //     		$responseCode = $accinq['ResponseCode'];
    //     		$responseDesc = $accinq['ResponseDesc'];

    //     		if($responseCode == '00'){
    //     			$custCif = $accinq['Cif'];

    //     			$productCode = $accinq['ProductType'];
    //     			$productPlan = $accinq['productPlan'];

    //     			$checkIb = $this->dbObj->select()
    //     									->from('M_PRODUCT_TYPE')
    //     									->where('PRODUCT_CODE = ?', $productCode)
    //     									->where('PRODUCT_PLAN = ?', $productPlan)
    //     									->query()
    //     									->fetchAll();

    //     			if(empty($checkIb)){
    //     				$this->_errorCode = '';
				// 		$this->_errorMsg = 'Account Type of : '.$acct_no.' are forbidden to use e-channel facilities';
				// 		return false;
    //     			}
    //     			else{
    //     				$finalAcct[$acctctr]['ACCT_NO'] = $acct_no;
    //     				$finalAcct[$acctctr]['CCY_ID'] = $accinq['Currency'];
    //     				$finalAcct[$acctctr]['ACCT_NAME'] = $accinq['AccountName'];
    //     				$finalAcct[$acctctr]['ACCT_TYPE'] = $accinq['ProductType'];
    //     				$finalAcct[$acctctr]['ACCT_DESC'] = $checkIb[0]['PRODUCT_NAME'];
    //     				$finalAcct[$acctctr]['ACCT_ALIAS_NAME'] = $acct['ACCT_NAME'];
    //     				$finalAcct[$acctctr]['ACCT_EMAIL'] = $accinq['Email'];
    //     				$finalAcct[$acctctr]['ACCT_STATUS'] = 1;

				// 		if($accinq['ProductType'] == "10")
				// 			$source = 3;
				// 		elseif($accinq['ProductType'] == "20")
				// 			$source = 2;
				// 		else
				// 			$source = 0;

				// 		$finalAcct[$acctctr]['ACCT_SOURCE'] = $source;
				// 		$finalAcct[$acctctr]['ACCT_OWNERSHIP'] = $acct['ACCT_OWNERSHIP'];

				// 		$acctctr++;
    //     			}
    //     		}
    //     		else{
    //     			$this->_errorCode = '';
				// 	$this->_errorMsg = 'Failed to inquriy account';
				// 	return false;
    //     		}
	   //      }

        	//---------------------INSERT TO CMS M_CUSTOMER--------------------

        	$custCif = 'CIF'.$tempcust;

        	$checkCif = $this->dbObj->select()
        							->from('M_CUSTOMER')
        							->where('CUST_CIF = ?', $custCif)
        							->query()
        							->fetch(Zend_Db::FETCH_ASSOC);


        	$checkCifTemp = $this->dbObj->select()
        							->from('TEMP_CUSTOMER')
        							->where('CUST_CIF = ?', $custCif)
        							->query()
        							->fetch(Zend_Db::FETCH_ASSOC);

        	if(empty($checkCif) && empty($checkCifTemp)){
        		$custData = array_diff_key($regis,array('TEMP_ID'=>'','CHANGES_ID'=>'','REG_STATUS'=>'','CREATED'=>'','CREATEDBY'=>'','UPDATED'=>'','UPDATEDBY'=>'','AKTA_PP'=>'','AKTA_PT'=>'','ID_PIHAK'=>'','SIUP_TDP'=>'','OTHER_DOC'=>'','ADART'=>''));

        		//generate CUST ID

        		$getSetting = $this->dbObj->select()
	        							->from('M_SETTING')
	        							->where('SETTING_ID = ?', 'company_code_len')
	        							->query()
	        							->fetchAll();

	        	$maxlen = $getSetting[0]['SETTING_VALUE'];
	        	$namelen = $maxlen-2;

	        	$namePart = trim(substr(preg_replace('/[^a-zA-Z0-9]/', '', $custData['CUST_NAME']),0,$namelen));

        		//$namePart = trim(substr(preg_replace('/[^a-zA-Z0-9]/', '', $custData['CUST_NAME']),0,4));
        		$tempID = $tempcust;

        		$selectCust = $this->dbObj->select()
		        							->from('M_CUSTOMER')
		        							->where('CUST_ID = ?', $tempID)
		        							->query()
		        							->fetch(Zend_Db::FETCH_ASSOC);

		        $selectCustTemp = $this->dbObj->select()
			        							->from('TEMP_CUSTOMER')
			        							->where('CUST_ID = ?', $tempID)
			        							->query()
			        							->fetch(Zend_Db::FETCH_ASSOC);

			    if(empty($selectCust) && empty($selectCustTemp))
			    	$checkName = true;
			    else
			    	$checkName = false;


        		while(!$checkName){
        			$numberPart = mt_rand(11,99);
        			//$numberPart = mt_rand(1111,9999);
	        		$tempID = $namePart.$numberPart;
	        		// if(strlen($tempID) > 6)
	        		// 	$tempID = trim(substr($tempID, 0, 6));

        			$selectCust = $this->dbObj->select()
			        							->from('M_CUSTOMER')
			        							->where('CUST_ID = ?', $tempID)
			        							->query()
			        							->fetch(Zend_Db::FETCH_ASSOC);

			        $selectCustTemp = $this->dbObj->select()
				        							->from('TEMP_CUSTOMER')
				        							->where('CUST_ID = ?', $tempID)
				        							->query()
				        							->fetch(Zend_Db::FETCH_ASSOC);

				    if(empty($selectCust) && empty($selectCustTemp))
				    	$checkName = true;
				    else
				    	$checkName = false;
	        	}

	        	$cust_id = $tempID;

	        	$custData['CUST_ID'] = $cust_id;
	        	$custData['CUST_CIF'] = $custCif;

        		$custData['CUST_CREATED'] = new Zend_Db_Expr('GETDATE()');
        		$custData['CUST_CREATEDBY'] = $actor;
        		$custData['CUST_UPDATED'] = new Zend_Db_Expr('now()');
				$custData['CUST_UPDATEDBY'] = $actor;
				$custData['CUST_STATUS'] = 1;

				$customerins = $this->dbObj->insert('M_CUSTOMER',$custData);

				if(!(boolean)$customerins) 
				{
					$this->_errorCode = '82';
					$this->_errorMsg = 'Query failed(Customer)';
					return false;
				}

				//ambil dari m_setting SKN RTGS
				$global_charges_skn_ori = 0; // $set->getSettingNew('global_charges_skn');
				$global_charges_rtgs_ori = 0; // $set->getSettingNew('global_charges_rtgs');
				
				//ambil dari m_setting FEE CHARGE
				$admin_fee_account_ori = 0; // $set->getSettingNew('admin_fee_account');
				$admin_fee_company_ori = 0; //$set->getSettingNew('admin_fee_company');


				//insert CHARGE SKN - M_CHARGES_OTHER
			 	$insertChargeSkn['CUST_ID']     	= $cust_id;
		 		$insertChargeSkn['CHARGES_TYPE']	= '2';
		 		$insertChargeSkn['CHARGES_NO']  	= '1';
		 		$insertChargeSkn['CHARGES_CCY'] 	= 'IDR';
		 		$insertChargeSkn['CHARGES_AMT'] 	= $global_charges_skn_ori;

		 		$ChargeSknInsert = $this->dbObj->insert('M_CHARGES_OTHER',$insertChargeSkn);

				if(!(boolean)$ChargeSknInsert){
					$this->_errorCode = '82';
					$this->_errorMsg = 'Query failed(Charges SKN)';
					return false;
				}
				
				//insert CHARGE SKN - M_CHARGES_OTHER
			 	$insertChargeRtgs['CUST_ID']     	= $cust_id;
		 		$insertChargeRtgs['CHARGES_TYPE']	= '1';
		 		$insertChargeRtgs['CHARGES_NO']  	= '1';
		 		$insertChargeRtgs['CHARGES_CCY'] 	= 'IDR';
		 		$insertChargeRtgs['CHARGES_AMT'] 	= $global_charges_rtgs_ori;

		 		$ChargeRtgsInsert = $this->dbObj->insert('M_CHARGES_OTHER',$insertChargeRtgs);

		 		if(!(boolean)$ChargeRtgsInsert){
					$this->_errorCode = '82';
					$this->_errorMsg = 'Query failed(Charges RTGS)';
					return false;
				}
		 		
		 		
		 		$remittancechar = $this->dbObj->select()
		 										->from(array('M_CHARGES_REMITTANCE'),array('*'))
		 										->where("CUST_ID =?",'GLOBAL')
		 		    							->order('CUST_ID ASC')
		 		    							->query()->fetchAll();
		 		
		 		$insertremittande = array();
		 		foreach ($remittancechar as $key=>$val){
		 		    $insertremittande = array(
		 		        'CUST_ID' 				=> $cust_id,
		 		        'CHARGE_TYPE'    		=> $val['CHARGE_TYPE'],
		 		        'CHARGE_CCY'     		=> $val['CHARGE_CCY'],
		 		        'CHARGE_AMOUNT_CCY'     => $val['CHARGE_AMOUNT_CCY'],
		 		        'CHARGE_AMT'            => $val['CHARGE_AMT'],
		 		        'CHARGE_PROV_MIN_AMT'   => $val['CHARGE_PROV_MIN_AMT'],
		 		        'CHARGE_PROV_MAX_AMT'   => $val['CHARGE_PROV_MAX_AMT'],
		 		        'CHARGE_PCT'            => $val['CHARGE_PCT']
		 		    );
		 		    

		 		    $ChargeRemittanceInsert = $this->dbObj->insert('M_CHARGES_REMITTANCE',$insertremittande);

		 		    if(!(boolean)$ChargeRemittanceInsert){
						$this->_errorCode = '82';
						$this->_errorMsg = 'Query failed(Charges Remittance)';
						return false;
					}
		 		}


		        //--------------------------------INSERT TO TABLE M_USER---------------------------------------

		        $userList = $additional['userlist'];

		        $emailArr = array();
		        $emailCtr = 0;
		        // var_dump($userlist);
		        foreach($userList as $key => $val){
		        	$user = $this->dbObj->select()
	        							->from('T_MYONLINE_BIS_USER')
	        							->where('USER_ID = ?', $val)
	        							->where('CUST_ID = ?', $tempcust)
	        							->query()
	        							->fetch(Zend_Db::FETCH_ASSOC);


	        		$ftempName = $user['USER_PRIVI'];

		        	$insertArr = array_diff_key($user,array('ID'=>'','USER_PRIVI'=>''));
		        	$str=rand(); 
					$rand = md5($str); 
		        	$insertArr['CUST_ID'] = $cust_id;
					$insertArr['USER_CREATED']     = new Zend_Db_Expr('now()');
					$insertArr['USER_CREATEDBY']   = $actor;
					$insertArr['USER_UPDATED']     = new Zend_Db_Expr('now()');
					$insertArr['USER_UPDATEDBY']   = $actor;
					$insertArr['USER_ISNEW']       = 1;
					$insertArr['USER_STATUS']	= 1;
					$insertArr['USER_ISEMAIL'] = 1;
					$insertArr['USER_CODE'] = $rand;
					$insertArr['USER_DATEPASS']     = new Zend_Db_Expr('now() + INTERVAL 1 DAY');

					$tokentype = $this->_getSettingBis('token_type_layout');
					$insertArr['TOKEN_TYPE'] = $tokentype;
					

					// var_dump($insertArr);
					$userins = $this->dbObj->insert('M_USER',$insertArr);
					
					if(!(boolean)$userins) 
					{
						$this->_errorCode = '82';
						$this->_errorMsg = 'Query failed(User)';
						return false;
					}	
					//-------------------------------END INSERT TO TABLE M_USER----------------------------------
					
					//----------------jika new user pertama x, maka ada tambahan yaitu : request change password dan update requeset unlock user----------------------
					// $customerUser = new CustomerUser($cust_id,$insertArr['USER_ID']);
				    // $customerUser->requestUnlockResetPassword(2);    //2 = print
					// $customerUser->approveRequestResetPassword();
					//-----------END jika new user pertama x, maka ada tambahan yaitu : request change password dan update requeset unlock user-----------------------

			        // $FResetPass = $this->_getSettingBis('femailtemplate_newuser');

			        // $datacreated = $this->dbObj->select()
										 //        ->from('M_USER')
										 //        ->where('USER_ID = ?',$insertArr['USER_ID'])
										 //        ->where('CUST_ID = ?',$cust_id)
										 //        ->query()
										 //        ->fetch(Zend_Db::FETCH_ASSOC);

					$set = new Settings();
					$templateEmailMasterBankAddress = $set->getSetting('master_bank_address');
					$templateEmailMasterBankAppName = $set->getSetting('master_bank_app_name');
					$templateEmailMasterBankAppUrl = $set->getSetting('master_bank_app_url');
					$templateEmailMasterBankCity = $set->getSetting('master_bank_city');
					$templateEmailMasterBankCountry = $set->getSetting('master_bank_country');
					$templateEmailMasterBankEmail = $set->getSetting('master_bank_email');
					$templateEmailMasterBankEmail1 = $set->getSetting('master_bank_email1');
					$templateEmailMasterBankFax = $set->getSetting('master_bank_fax');
					$templateEmailMasterBankName = $set->getSetting('master_bank_name');
					$templateEmailMasterBankProvince = $set->getSetting('master_bank_province');
					$templateEmailMasterBankTelp = $set->getSetting('master_bank_telp');
					$templateEmailMasterBankWebsite = $set->getSetting('master_bank_website');
					$templateEmailMasterBankWapp = $set->getSetting('master_bank_wapp');
					// $templateEmailMasterBankAppname = $set->getSetting('master_bank_app_name');
					$url_fo = $set->getSetting('url_fo');


					$FEmailResetPass = $set->getSetting('femailtemplate_newuser');

					// print_r($isi);die;
					// $actual_link = $_SERVER['SERVER_NAME'];
					// $key = md5 ('permataNet92');
					// $encrypt = new Crypt_AES ();
					// $user = ( $encrypt->encrypt ( $user_id ) );
					// $cust = ( $encrypt->encrypt ( $isi['CUST_ID'] ) );

			 	// 	$newPassword = '<a href="'.$actual_link.':9996/pass/index?safetycheck=&cust_id='.urlencode($cust).'&user_id='.urlencode($user).'" >Confirm</a>';
			 		$actual_link = $_SERVER['SERVER_NAME'];
					$key = md5 ('permataNet92');
					// $encrypt = new Crypt_AES ();
					// $user = ( $encrypt->encrypt ( $user_id ) );
					// $user = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $user_id, MCRYPT_MODE_ECB);
					// $cust = ( $encrypt->encrypt ( $isi['CUST_ID'] ) );
					$cust = ( $this->sslEnc(  $insertArr['CUST_ID'] ) );
					$user = ( $this->sslEnc(  $insertArr['USER_ID'] ) );
					$date = date('Y-m-d h:i:s', strtotime("+1 days"));

			 		$newPassword = $url_fo.'/pass/index?safetycheck=&cust_id='.urlencode($cust).'&user_id='.urlencode($user);
			 		// $template = str_ireplace('[[exp_date]]',Application_Helper_General::convertDate($date,'d-MMM-yyyy HH:mm:ss','yyyy-MM-dd HH:mm:ss'),$template);
			 		// print_r($newPassword);die;
			 		$datenow = date('d-M-Y H:i:s', strtotime("+1 days"));
					$data = array( '[[user_fullname]]' => $insertArr['USER_FULLNAME'],
											'[[comp_accid]]' => $insertArr['CUST_ID'],
											'[[user_login]]' => $insertArr['USER_ID'],
											'[[user_email]]' => $insertArr['USER_EMAIL'],
											'[[confirm_link]]' => $newPassword,
											'[[master_bank_name]]' => $templateEmailMasterBankName,
											'[[master_bank_telp]]' => $templateEmailMasterBankTelp,
											'[[master_bank_email]]' => $templateEmailMasterBankEmail,
											'[[master_bank_wapp]]' => $templateEmailMasterBankWapp,
											'[[master_bank_app_name]]' => '360 Corporate Digital Banking',
											'[[exp_date]]' => $datenow

											);

					$FEmailResetPass = strtr($FEmailResetPass,$data);
					// echo $FEmailResetPass;
					$status = Application_Helper_Email::sendEmail($insertArr['USER_EMAIL'],'New User Account Information',$FEmailResetPass);

			        
			  //       $newPassword = substr(base64_decode($datacreated['USER_CLEARTEXT_PWD']),4, -4);
			        
			  //       $FResetPass = str_ireplace('[[user_fullname]]',$datacreated['USER_FULLNAME'],$FResetPass);
					// $FResetPass = str_ireplace('[[comp_accid]]',$datacreated['CUST_ID'],$FResetPass);
					// $FResetPass = str_ireplace('[[user_login]]',$datacreated['USER_ID'],$FResetPass);
					// $FResetPass = str_ireplace('[[user_email]]',$datacreated['USER_EMAIL'],$FResetPass);
					// $FResetPass = str_ireplace('[[user_cleartext_password]]',$newPassword,$FResetPass);
					// $FResetPass = str_ireplace('[[master_bank_name]]',$templateEmailMasterBankName,$FResetPass);
					// $FResetPass = str_ireplace('[[master_bank_email]]',$templateEmailMasterBankEmail,$FResetPass);
					// $FResetPass = str_ireplace('[[master_bank_telp]]',$templateEmailMasterBankTelp,$FResetPass);
					// //$FResetPass = str_ireplace('[[user_role]]',$user_role,$FResetPass);

					// $emailArr[$emailCtr]['EMAIL'] = $datacreated['USER_EMAIL'];
					// $emailArr[$emailCtr]['CONTENT'] = $FResetPass;
					// $emailCtr++;

					//Application_Helper_Email::queueEmail($datacreated['USER_EMAIL'],'New Password Information',$FResetPass);
					//Application_Helper_Email::sendEmail($datacreated['USER_EMAIL'],'New Password Information',$FResetPass);

					$updData = array('USER_RPWD_ISEMAILED'=> 1);
					$where =  array();
					$where['CUST_ID = ?'] 	= $cust_id;
					$where['USER_ID = ?'] 	= $insertArr['USER_ID'];
					$this->dbObj->update('M_USER',$updData,$where);
					
					
					//--------------------------------INSERT TO TABLE M_FPRIVI_USER----------------------------

					$getTemplateID = $this->dbObj->select()
									          ->from('M_FTEMPLATE')
									          ->where('LOWER(FTEMPLATE_DESC) = ?', strtolower($ftempName))
									          ->query()->fetchAll();

					if(empty($getTemplateID)){
						$this->_errorCode = '';
						$this->_errorMsg = 'Privilege template not found';
						return false;
					}

					$ftemplateID = $getTemplateID[0]['FTEMPLATE_ID'];

				    $privilege = $this->dbObj->select()
									          ->from('M_FPRIVILEGE_TEMPLATE')
									          ->where('FTEMPLATE_ID = ?', $ftemplateID)
									          ->query()->fetchAll();
					if(count($privilege) > 0)
					{
					   foreach($privilege as $privi)
					   {

					      $fuserid = $cust_id.$insertArr['USER_ID'];
					      $insertPrivi = array('FPRIVI_ID' => $privi['FPRIVI_ID'], 'FUSER_ID' => $fuserid);
			              $priviins = $this->dbObj->insert('M_FPRIVI_USER',$insertPrivi);
					   }

					   	if(!(boolean)$priviins) 
						{
							$this->_errorCode = '82';
							$this->_errorMsg = 'Query failed(Privilege)';
							return false;
						}
			        }
					//-------------------------------END INSERT TO TABLE M_FPRIVI_USER----------------
				}

				//-------------------------------INSERT TO M_CUST_ADAPTER_PROFILE----------------------------

					// $arrTrfType = array(
				 //        'payroll',
				 //        'manytomany',
				 //        'emoney',
				 //        'onetomany',
				 //        'manytoone'
			  //       );

				 //    foreach($arrTrfType as $trfType){

				 //        $content =  array(
				 //        				'CUST_ID' => $cust_id,
				 //                        'TRF_TYPE'  => $trfType,
				 //                        'ADAPTER_PROFILE_ID' => 'defaults'
				 //                    );
				         
				 //        $adapterins = $this->dbObj->insert('M_CUST_ADAPTER_PROFILE', $content);

				 //        if(!(boolean)$adapterins) 
					// 	{
					// 		$this->_errorCode = '82';
					// 		$this->_errorMsg = 'Query failed(Adapter)';
					// 		return false;
					// 	}
				 //    }

				//-------------------------------END INSERT TO M_CUST_ADAPTER_PROFILE----------------------------------


				// die;

				//----------------- INSERT ACCOUNT --------------------

				// foreach($finalAcct as $key => $val){
				// 	$insertAcct = $finalAcct[$key];
				// 	$insertAcct['CUST_ID'] = $cust_id;
				// 	$insertAcct['ACCT_CREATED']     = new Zend_Db_Expr('now()');
				// 	$insertAcct['ACCT_CREATEDBY']   = $actor;
				// 	$insertAcct['ACCT_UPDATED']     = new Zend_Db_Expr('now()');
				// 	$insertAcct['ACCT_UPDATEDBY']   = $actor;

				// 	$acctins = $this->dbObj->insert('M_CUSTOMER_ACCT',$insertAcct);
					
				// 	if(!(boolean)$acctins) 
				// 	{
				// 		$this->_errorCode = '82';
				// 		$this->_errorMsg = 'Query failed(Account)';
				// 		return false;
				// 	}
				// }
		    }
		    else{
		    	$this->_errorCode = '';
				$this->_errorMsg = 'Cif has been suggested / registered';
				return false;
		    }
		}


  		$deleteChanges = $this->deleteNew(1);
		if(!$deleteChanges) return false;

		//email disetujui
		if($reg_type == 'BIS'){
			$master_bank_app_name = $this->_getSettingBis('master_bank_app_name');
			$echanName = $master_bank_app_name;
			$email = $regis['CUST_EMAIL'];
			$sendsms = explode(";", $regis['CUST_PHONE']);
		}
		else{
			$echanName = 'Permata Online (Individual)';
			$email = $regis['USER_EMAIL'];
			$sendsms = $regis['USER_MOBILE_PHONE'];
		}

		$msg = "Selamat, pendaftaran e-Channel ".$echanName." Anda disetujui.";

		//$setting = new Settings();
		$template = $this->_getSettingBis('bemailtemplate_customer_eregis');
		$templateEmailMasterBankName = $this->_getSettingBis('master_bank_name');

		$templateEmailMasterBankAppName = $this->_getSettingBis('master_bank_app_name');

		$bank_email = $this->_getSettingBis('master_bank_email');
		$bank_telp = $this->_getSettingBis('master_bank_telp');
		$bank_wapp = $this->_getSettingBis('master_bank_wapp');

		$greeting = "Yth ".$resultdata['CUST_NAME'];

					

		$template = str_ireplace('[[master_bank_app_name]]',$templateEmailMasterBankAppName,$template);
		$template = str_ireplace('[[master_bank_name]]',$templateEmailMasterBankName,$template);

		$template = str_ireplace('[[contact_name]]',$regis['CUST_CONTACT'],$template);
		$template = str_ireplace('[[comp_name]]',$regis['CUST_NAME'],$template);
		$template = str_ireplace('[[comp_accid]]',$regis['CUST_ID'],$template);
		

					
		$template = str_ireplace('[[master_bank_email]]',$bank_email,$template);
		$template = str_ireplace('[[master_bank_telp]]',$bank_telp,$template);
		$template = str_ireplace('[[master_bank_wapp]]',$bank_wapp,$template);



		if($reg_type == 'BIS'){
			$greeting = "Yth ".$regis['CUST_NAME'];
		}
		else{
			$greeting = "Yth ".$regis['USER_FULLNAME'];
		}

		// $template = str_ireplace('[[greeting]]',$greeting,$template);
		// $template = str_ireplace('[[eregistration_message]]',$msg,$template);
	 // 	$template = str_ireplace('[[master_bank_name]]',$templateEmailMasterBankName,$template);

		$subject = "Customer Registration Approved";
		//Application_Helper_Email::queueEmail($email,$subject,$template,__FILE__.__LINE__);
		Application_Helper_Email::sendEmail($email,$subject,$template);


		//email user pass
		// if(!empty($emailArr)){
		// 	foreach($emailArr as $row){
		// 		Application_Helper_Email::sendEmail($row["EMAIL"],"New Password Information",$row["CONTENT"]);
		// 	}
		// }

		$smsmsg = $greeting.",\n".$msg;

		if(count($sendsms) > 1){
			for($i=0;$i<count($sendsms);$i++){
				$sms = new Service_SMS($sendsms[$i],$smsmsg);
				//$sms->OutgoingSMS();
			}
		}
		else{
			$sms = new Service_SMS($regis['CUST_PHONE'],$smsmsg);
			//$sms->OutgoingSMS();
		}

		return true;
	}

	public function sslPrm()
	{
	return array("6a1f325be4c0492063e83a8cb2cb9ae7","IV (optional)","aes-128-cbc");
	}

	public function sslEnc($msg)
	{
	  list ($pass, $iv, $method)=$this->sslPrm();
		 return urlencode(openssl_encrypt(urlencode($msg), $method, $pass, false, $iv));
	}


	public function deleteNew($new = null) {
		$type = $this->_changesInfo['KEY_FIELD'];

		if($type == "IND")
			$tableName = "TEMP_MYONLINE_IND";
		else
			$tableName = "TEMP_MYONLINE_BIS";

				  			

		if($type == "IND"){
			$this->dbObj->delete('T_MYONLINE_IND_ACC',$this->dbObj->quoteInto('REG_ID = ?',$tempData['TEMP_ID']));
		}
		else{
			$this->dbObj->delete('T_MYONLINE_BIS_ACC',$this->dbObj->quoteInto('CUST_ID = ?',$tempData['CUST_ID']));
			$this->dbObj->delete('T_MYONLINE_BIS_USER',$this->dbObj->quoteInto('CUST_ID = ?',$tempData['CUST_ID']));
		}

		$this->dbObj->delete($tableName,$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		return true;
	}
	
	public function approveEdit(){

	}
	
	public function approveActivate(){

	}
	
	public function approveDeactivate(){

	}
	
	public function approveDelete(){

	}
	
	public function deleteEdit(){

	}
	
	public function deleteActivate(){

	}
	
	public function deleteDeactivate(){

	}
	
	public function deleteDelete(){

	}

	private function _getSetting($setting_id){
		$select = $this->dbObj->select()
								->from(array('M_SETTING'), array('SETTING_VALUE'))
								->where('SETTING_ID = ?', $setting_id);

		return $this->dbObj->fetchOne($select);
	}

	private function _getSettingBis($setting_id){
		$select = $this->dbObj->select()
								->from(array('M_SETTING'), array('SETTING_VALUE'))
								->where('SETTING_ID = ?', $setting_id);

		return $this->dbObj->fetchOne($select);
	}
}