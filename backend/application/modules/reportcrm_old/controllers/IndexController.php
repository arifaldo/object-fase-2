<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';

require_once 'General/Customer.php';
require_once 'General/CustomerUser.php';
class reportcrm_IndexController extends Application_Main {


	
	public function indexAction()
    { 
    	$this->_helper->_layout->setLayout('newlayout');

    	$select = $this->_db->select()
    						->from('information_schema.tables', array('table_name'))
    						->where('table_schema = ?', 'bank_mayapada_cm_demo');

    	$tableList = $this->_db->fetchAll($select);

        $report =  $this->_db->fetchAll(
                        $this->_db->select()->distinct()
                             ->from(array('A' => 'T_REPORT_GENERATOR'),array('A.*'))
                             ->where("A.REPORT_CUST = ".$this->_db->quote($this->_custIdLogin))
                             ->where("A.REPORT_CREATEDBY = ".$this->_db->quote($this->_userIdLogin))
                     );
        foreach ($report as $key => $value) {
            $report[$key]['REPORT_CREATED'] = Application_Helper_General::convertDate($value['REPORT_CREATED'],$this->displayDateTimeFormat,$this->defaultDateFormat);
        }
        $this->view->report = $report;

         $select = $this->_db->select()
                                    ->from('information_schema.columns', array('DATA_TYPE','COLUMN_NAME'))
                                    ->where('table_schema = ?', 'bank_mayapada_cm_demo')
                                    ->where('table_name in ("T_PSLIP","T_TRANSACTION")')
                                    ->where('COLUMN_NAME NOT in ("T_PSLIP","T_TRANSACTION","ESCROW_ACC",
                                    "ESCROW_ACC_TYPE",
                                    "HOST_RESPONSE",
                                    "REVERSAL_DESC",
                                    "REVERSAL_STATUS",
                                    "UUID",
                                    "LOG",
                                    "REFF_ID",
                                    "BENEFICIARY_ID",
                                    "BENEF_ACCT_BANK_CODE",
                                    "BENEFICIARY_DATA",
                                    "TRA_CHARGE_TO",
                                    "SENDFILE_sTATUS",
                                    "RELEASE_TYPE",
                                    "EFT_STATUS",
                                    "EFT_BANKCODE",
                                    "BANK_RESPONSE",
                                    "DATE_UPDATE",
                                    "BENEFICIARY_ADDRESS2",
                                    "BENEFICIARY_ADDRESS3",
                                    "PROVIDER_CHARGES",
                                    "LLD_CODE",
                                    "TRA_REFNO",
                                    "TRX_ID",
                                    "TRA_REMAIN",
                                    "TRANSFER_FEE_STATUS",
                                    "RELEASE_TYPE",
                                    "PS_BILLER_ID",
                                    "PS_PERIODIC",
                                    "PS_REMAIN",
                                    "FEATURE_ID",
                                    "PS_TXCOUNT",
                                    "PS_RELEASER_CHALLENGE",
                                    "PS_RELEASER_USER_LOGIN",
                                    "DISPLAY_FLAG",
                                    "RAW_REQUEST",
                                    "REVERSAL_DESC",
                                    "TRACE_NO",
                                    "TX_FEE_SCM_CHARGE_TO",
                                    "DISKONTO_AMOUNT",
                                    "BILLER_ORDER_ID",
                                    "EFT_BANKRESPONSE",
                                    "SKN_TRANSACTION_TYPE",
                                    "ORG_DIR",
                                    "SWIFT_CODE",
                                    "NOSTRO_CODE",
                                    "CLR_CODE",
                                    "BANK_CODE",
                                    "POB_NUMBER",
                                    "BENEFICIARY_BI_ACCOUNT",
                                    "BENEFICIARY_BANK_ADDRESS2",
                                    "BENEFICIARY_BANK_ADDRESS3" )');
            // echo $select;die;    
        $tempColumn = $this->_db->fetchAll($select);
        foreach ($tempColumn as $key => $value) {
            if($value['COLUMN_NAME'] == 'PS_NUMBER'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Payment Ref');
            }else if($value['COLUMN_NAME'] == 'PS_SUBJECT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Subject');
            }else if($value['COLUMN_NAME'] == 'PS_CREATED'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Create Date');
            }else if($value['COLUMN_NAME'] == 'PS_UPDATED'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Update Date');
            }else if($value['COLUMN_NAME'] == 'PS_EFDATE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transaction Date');
            }else if($value['COLUMN_NAME'] == 'PS_STATUS'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Payment Status');
            }else if($value['COLUMN_NAME'] == 'CUST_ID'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Company ID');
            }else if($value['COLUMN_NAME'] == 'PS_TOTAL_AMOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Total Amount');
            }else if($value['COLUMN_NAME'] == 'PS_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Payment Type');
            }else if($value['COLUMN_NAME'] == 'PS_CATEGORY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Category');
            }else if($value['COLUMN_NAME'] == 'PS_CCY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Curency');
            }else if($value['COLUMN_NAME'] == 'PS_CREATEDBY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Create By');
            }else if($value['COLUMN_NAME'] == 'EQUIVALENT_AMOUNT_IDR'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Equivalent Amount');
            }else if($value['COLUMN_NAME'] == 'TRANSACTION_ID'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transaction Id');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ACCOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Account');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_EMAIL'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Email');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CITIZENSHIP'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Citizenship');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_RESIDENT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Resident');
            }else if($value['COLUMN_NAME'] == 'TRA_AMOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Amount');
            }else if($value['COLUMN_NAME'] == 'TRANSFER_FEE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Fee');
            }else if($value['COLUMN_NAME'] == 'TRANSFER_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Type');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ALIAS_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Alias');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ACCOUNT_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Name');
            }else if($value['COLUMN_NAME'] == 'RATE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Rate Sell');
            }else if($value['COLUMN_NAME'] == 'RATE_BUY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Rate Buy');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ALIAS_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Type');
            }else if($value['COLUMN_NAME'] == 'BOOK_RATE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Book Rate Sell');
            }else if($value['COLUMN_NAME'] == 'BOOK_RATE_BUY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Book Rate Buy');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCT_BANK_CODE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Bank');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_CCY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account CCY');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Name');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_ALIAS_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Alias');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Type');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ACCOUNT_CCY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination CCY');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_MOBILE_PHONE_NUMBER'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Phone');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ADDRESS'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Address');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Name');
            }else if($value['COLUMN_NAME'] == 'TOTAL_CHARGES'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Total Charges');
            }else if($value['COLUMN_NAME'] == 'FULL_AMOUNT_FEE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Full Amount Fee');
            }else if($value['COLUMN_NAME'] == 'PROVISION_FEE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Provision Fee');
            }else if($value['COLUMN_NAME'] == 'EQUIVALENT_AMOUNT_USD'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Total USD');
            }else if($value['COLUMN_NAME'] == 'TRA_MESSAGE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Message');
            }else if($value['COLUMN_NAME'] == 'TRA_ADDITIONAL_MESSAGE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Additional Message');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Bank');
            }else if($value['COLUMN_NAME'] == 'TRA_STATUS'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Status');
            }else if($value['COLUMN_NAME'] == 'LLD_TRANSACTOR_RELATIONSHIP'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Transactor Relationship');
            }else if($value['COLUMN_NAME'] == 'LLD_TRANSACTION_PURPOSE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Transaction Purpose');
            }else if($value['COLUMN_NAME'] == 'LLD_IDENTITY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Identity');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ID_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Id Type');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ID_NUMBER'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Id Number');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CITY_CODE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination City');
            }else if($value['COLUMN_NAME'] == 'NOSTRO_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Nostro Name');
            }else if($value['COLUMN_NAME'] == 'LLD_DESC'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Description');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CITIZENSHIP_COUNTRY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Country');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_ADDRESS1'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Address');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_BRANCH'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Branch');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_COUNTRY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Bank Country');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CATEGORY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Category');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_CITY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Bank City');
            }
            else{
                $tempColumn[$key]['COLOMN'] = $value['COLUMN_NAME'];
            }
            
        }
        // echo '<pre>';
        // print_r($tempColumn);die;
        $this->view->colomndata = $tempColumn;

    	$this->view->tableList = $tableList;

        if($this->_request->isPost()){

            $params     = $this->_request->getParams();
            // print_r($params);die;
            $filters    = array('tablename' => array('StringTrim','StripTags','HtmlEntities'),
                                'tablecols' => array('StringTrim','StripTags','HtmlEntities'),
                                'sortasc' => array('StringTrim','StripTags','HtmlEntities'),
                                'sortdesc' => array('StringTrim','StripTags','HtmlEntities'),
                                'datalimit' => array('StringTrim','StripTags','HtmlEntities'),
                                'wherecol' => array('StringTrim','StripTags','HtmlEntities'),
                                'whereopt' => array('StringTrim','StripTags','HtmlEntities'),
                                'whereval' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_name' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_email' => array('StringTrim','StripTags','HtmlEntities'),
                                'label' => array('StringTrim','StripTags','HtmlEntities'),
                                'colomn' => array('StringTrim','StripTags','HtmlEntities'),
                                'type' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_schedule' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_date' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_day' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_time' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_data' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_month' => array('StringTrim','StripTags','HtmlEntities'),
                                'sortby' => array('StringTrim','StripTags','HtmlEntities'),
                                'sortasc' => array('StringTrim','StripTags','HtmlEntities'),
                                'sortdesc' => array('StringTrim','StripTags','HtmlEntities')
                                
                                
            );

            $validators =  array('tablename'     => array(),
                                'tablecols'      => array(),
                                'sortasc'        => array(),
                                'sortdesc'       => array(),
                                'wherecol'       => array(),
                                'whereopt'       => array(),
                                'whereval'          => array('allowEmpty'=>true
                                                            // new Zend_Validate_Regex(array('pattern' => '/^[0-9A-Za-z\\s-_.]+$/')),
                                                            // 'messages' => array('Invalid report condition')
                                                    ),
                                'datalimit'         => array('allowEmpty' => true,
                                                            'Digits',
                                                            'messages' => array('Invalid data limit format')
                                                    ),
                                'report_name'       => array('NotEmpty',
                                                            array('StringLength',array('max'=>200)),
                                                            'messages' => array('Can not be empty',
                                                                            'Report name length cannot be more than 200',
                                                                        )
                                                    ),
                                'label'       => array('NotEmpty',
                                                            'messages' => array('Can not be empty',
                                                                        )
                                                    ),

                                'colomn'       => array('NotEmpty',
                                                            'messages' => array('Can not be empty',
                                                                        )
                                                    ),
                                'type'       => array('NotEmpty',
                                                            'messages' => array('Can not be empty',
                                                                        )
                                                    ),
                                'report_email'      => array('allowEmpty'=>true,
                                                            array('StringLength',array('max'=>128)),
                                                            'messages' => array(//'Can not be empty',
                                                                            'Email length cannot be more than 128',
                                                                        )
                                                    ),
                                'report_schedule'   => array('allowEmpty'=>true,
                                                            'Alpha',
                                                            'messages' => array(//'Can not be empty',
                                                                                'Invalid schedule'
                                                                        )
                                                    ),
                                'report_date'       => array('allowEmpty'=>true),
                                'report_day'       => array('allowEmpty'=>true),
                                'report_time'       => array('allowEmpty'=>true),
                                'report_data'       => array('allowEmpty'=>true),
                                'report_month'       => array('allowEmpty'=>true),
                                'sortby'       => array('allowEmpty'=>true),
                                'sortasc'       => array('allowEmpty'=>true),
                                'sortdesc'       => array('allowEmpty'=>true)
                                
                                
            );

            $zf_filter = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

            $cek_multiple_email = true;

            if($zf_filter->report_email)
            {
                $validate = new Validate();
                $cek_multiple_email = $validate->isValidEmailMultiple($zf_filter->report_email);
            }
            // echo 'here';die;
            if($zf_filter->isValid() && $cek_multiple_email == true)
            {
                // print_r($zf_filter);die;
                // print_r($zf_filter->sortasc);die;
                $selectedCols = implode(",", $zf_filter->tablecols);
                // $sortAsc = implode(",", $zf_filter->sortasc);
                // $sortDesc = implode(",", $zf_filter->sortdesc);

                $whereCols = $zf_filter->wherecol;
                $whereOpts = $zf_filter->whereopt;
                $whereVals = $zf_filter->whereval;

                $optsArr = array("EQUAL" => "=",
                                "NOT EQUAL" => "<>",
                                "LESS THAN" => "<",
                                "GREATER THAN" => ">",
                                "LESS THAN OR EQUAL TO" => "<=",
                                "GREATER THAN OR EQUAL TO" => ">="
                );

                
                // print_r($whereCols);
                // print_r($whereVals);
                
                if(!empty($whereCols)){
                    $tempWhere = array();
                    $index = 0;
                    // $withoutDuplicates = array_unique(array_map("strtoupper", $whereCols));
                    // print_r($withoutDuplicates);
                    $row = 0;
                    // print_r($whereVals);
                    // print_r($whereCols);
                    foreach($whereCols as $key => $val){
                        $tempName = explode("-", $val);
                        $colName = $tempName[0];
                        if($key%2==0){

                            
                            // print_r($test);
                        if($whereOpts[$key] != "LIKE"){
                            $opt = $optsArr[$whereOpts[$index]];
                            // print_r($whereVals);
                            $whereval = $whereCols[$key+1];
                        }
                        else{
                            $opt = $whereOpts[$index];
                            $whereval = "%".$whereCols[$key+1]."%";
                        }
                        // if($whereVals==''){
                        //     $whereval = 0;
                        // }
                        $duplicate = in_array($val, $whereCols);
                        if($duplicate){
                            // print_r($whereval);
                            if(empty($whereVals[$row])){
                                $whereVals[$row] = 0;
                            }
                            if($row == '0'){
                                $tempWhere[$row] = $colName." ".$opt." ".$whereVals[$row];  
                            }else{
                                $tempWhere[$row-1] .= " OR ".$colName." ".$opt." ".$whereVals[$row];  
                            }
                            
                            $row++;
                        }else{

                            $tempWhere[$row] = $colName." ".$opt." ".$whereval;      
                            $row++;
                        }
                        
                        $index++;
                        }
                        // $index++;
                    }

                    // print_r($tempWhere);

                    $wheres = implode(";",$tempWhere);
                }
                // print_r($zf_filter->report_data);
               

               
            
                // print_r($wheres);die;
                

                if(empty($zf_filter->report_email)){
                    $schedule = null;
                }
                else{
                    if(empty($zf_filter->report_schedule))
                        $schedule = "weekly";
                    else
                        $schedule = strtolower($zf_filter->report_schedule);
                }

                if(empty($zf_filter->datalimit))
                    $limit = 0;
                else
                    $limit = $zf_filter->datalimit;
                
                $insArr = array("REPORT_NAME" => $zf_filter->report_name,
                                "REPORT_TABLE" => $zf_filter->tablename,
                                "REPORT_COLUMNS" => $selectedCols,
                                "REPORT_WHERE" => $wheres,
                                // "REPORT_SORT_ASC" => $sortAsc,
                                // "REPORT_SORT_DESC" => $sortDesc,
                                "REPORT_LIMIT" => $limit,
                                "REPORT_EMAIL" => $zf_filter->report_email,
                                "REPORT_SCHEDULE" => $schedule,
                                "REPORT_CUST" => $this->_custIdLogin,
                                "REPORT_CREATED" => new Zend_Db_Expr("GETDATE()"),
                                "REPORT_CREATEDBY" => $this->_userIdLogin,
                                "REPORT_DATA" => $zf_filter->report_data

                );

                if($zf_filter->sortby == '1'){
// "REPORT_SORT_ASC" => $sortAsc,
                    $insArr['REPORT_SORT_ASC'] =  $zf_filter->sortasc;
                }else if($zf_filter->sortby == '2'){
                    $insArr['REPORT_SORT_DESC'] =  $zf_filter->sortdesc;
                }


                if($schedule == 'monthly'){
                    $insArr['REPORT_DATE'] =  $zf_filter->report_date;
                }else if($schedule == 'weekly'){
                    $insArr['REPORT_DAY'] =  $zf_filter->report_day;
                    $insArr['REPORT_TIME'] =  $zf_filter->report_time;
                }else if($schedule == 'daily'){
                    // $insArr['REPORT_DAY'] =  $zf_filter->report_day;
                    $insArr['REPORT_TIME'] =  $zf_filter->report_time;
                }else if($schedule == 'yearly'){
                    $insArr['REPORT_MONTH'] =  $zf_filter->report_month;
                    $insArr['REPORT_DAY'] =  $zf_filter->report_day;
                }

                try{
                    $this->_db->beginTransaction();

                    $this->_db->insert('T_REPORT_GENERATOR',$insArr);

                    $lastId = $this->_db->lastInsertId();
                    // print_r($zf_filter->label);die;
                    if(!empty($zf_filter->label)){
                        foreach ($zf_filter->label as $key => $value) {
                                    // print_r($value);
                                if($zf_filter->type[$key]=='datetime'){
                                    $type = 4;
                                }elseif($zf_filter->type[$key]=='date'){
                                    $type = 3;
                                }elseif($zf_filter->type[$key]=='varchar'){
                                    $type = 1;
                                }elseif($zf_filter->type[$key]=='text'){
                                    $type = 4;
                                }elseif($zf_filter->type[$key]=='decimal' || $zf_filter->type[$key]=='int'){
                                    $type = 2;
                                }else{
                                    $type = 1;
                                }

                                $colmnArr = array("COLM_NAME" => $value,
                                    "COLM_FIELD" => $zf_filter->colomn[$key],
                                    "COLM_REPORD_ID" => $lastId,
                                    "COLM_INDEX" => $key,
                                    "COLM_TYPE" => $type
                                );
                                // print_r($colmnArr);die;
                                $this->_db->insert('T_REPORT_COLOMN',$colmnArr);
                        }

                    }

                    Application_Helper_General::writeLog('ADRG','New report has been added, Report Name : '.$zf_filter->report_name. ' Creator : '.$this->_custIdLogin." | ".$this->_userIdLogin);

                    $this->_db->commit();
                    $this->setbackURL('/reportcrm');
                    $this->_redirect('/notification/success/index');
                }
                catch(Exception $e){
                    // print_r($e);die;
                    $this->_db->rollBack();
                    $error_remark = $this->language->_('An Error Occured. Please Try Again');
                }
            // die;
                if(isset($error_remark))
                {
                    // die;
                    Application_Helper_General::writeLog('ADRG','Add Report');
                    $this->_helper->getHelper('FlashMessenger')->addMessage('F');
                    $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
                    $this->_redirect('/reportcrm');
                }
            }
            else{
                $this->view->tablename = ($zf_filter->isValid('tablename'))? $zf_filter->tablename : $this->_getParam('tablename');
                $this->view->tablecols = ($zf_filter->isValid('tablecols'))? $zf_filter->tablecols : $this->_getParam('tablecols');
                $this->view->sortasc = ($zf_filter->isValid('sortasc'))? $zf_filter->sortasc : $this->_getParam('sortasc');
                $this->view->sortdesc = ($zf_filter->isValid('sortdesc'))? $zf_filter->sortdesc : $this->_getParam('sortdesc');
                $this->view->datalimit = ($zf_filter->isValid('datalimit'))? $zf_filter->datalimit : $this->_getParam('datalimit');
                $this->view->wherecol = ($zf_filter->isValid('wherecol'))? $zf_filter->wherecol : $this->_getParam('wherecol');
                $this->view->whereopt = ($zf_filter->isValid('whereopt'))? $zf_filter->whereopt : $this->_getParam('whereopt'); 
                $this->view->whereval = ($zf_filter->isValid('whereval'))? $zf_filter->whereval : $this->_getParam('whereval');
                $this->view->report_name = ($zf_filter->isValid('report_name'))? $zf_filter->report_name : $this->_getParam('report_name');
                $this->view->report_email = ($zf_filter->isValid('report_email'))? $zf_filter->report_email : $this->_getParam('report_email');
                $this->view->report_schedule = ($zf_filter->isValid('report_schedule'))? $zf_filter->report_schedule : $this->_getParam('report_schedule');

                $error = $zf_filter->getMessages();
           // print_r($error);die;
                $errorArray = null;
                foreach($error as $keyRoot => $rowError)
                {
                   foreach($rowError as $errorString)
                   {
                      $errorArray[$keyRoot] = $errorString;
                   }
                }
        
                if(isSet($cek_multiple_email) && $cek_multiple_email == false) $errorArray['report_email'] = 'Invalid email format';
        
                $this->view->error_msg = $errorArray;

                $tblName = ($zf_filter->isValid('tablename'))? $zf_filter->tablename : $this->_getParam('tablename');
                $select = $this->_db->select()
                                    ->from('information_schema.columns', array('DATA_TYPE','COLUMN_NAME'))
                                    ->where('table_schema = ?', 'bank_mayapada_cm_demo')
                                    ->where('table_name in ("T_PSLIP","T_TRANSACTION")');
                
                $tempColumn = $this->_db->fetchAll($select);

                $this->view->columnList = $tempColumn;

                $tblCols = ($zf_filter->isValid('tablecols'))? $zf_filter->tablecols : $this->_getParam('tablecols');
                $tblAsc = ($zf_filter->isValid('sortasc'))? $zf_filter->sortasc : $this->_getParam('sortasc');
                $tblDesc = ($zf_filter->isValid('sortdesc'))? $zf_filter->sortdesc : $this->_getParam('sortdesc');

                $leftCols = array();
                $leftAsc = array();
                $leftDesc = array();
                foreach($tempColumn as $row){
                    if(!in_array($row['COLUMN_NAME'], $tblCols))
                        $leftCols[] = $row['COLUMN_NAME'];

                    if(!in_array($row['COLUMN_NAME'], $tblAsc))
                        $leftAsc[] = $row['COLUMN_NAME'];

                    if(!in_array($row['COLUMN_NAME'], $tblDesc))
                        $leftDesc[] = $row['COLUMN_NAME'];
                }

                $this->view->leftcols = $leftCols;
                $this->view->leftasc = $leftAsc;
                $this->view->leftdesc = $leftDesc;

                $whereCols = ($zf_filter->isValid('wherecol'))? $zf_filter->wherecol : $this->_getParam('wherecol');
                if(!empty($whereCols)){
                    $wherectr = count($whereCols)+1;
                }
                else{
                    $wherectr = 1;
                }

                $this->view->wherectr = $wherectr;
            }
        }
        else{
            $this->view->wherectr = 1;
        }
    }


    public function columnAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $select = $this->_db->select()
                            ->from('information_schema.columns', array('COLUMN_NAME'))
                            ->where('table_schema = ?', 'bank_mayapada_cm_demo')
                            ->where('table_name in ("T_PSLIP","T_TRANSACTION")');
                            // echo $select;die;
        $data = $this->_db->fetchAll($select);
        foreach($data as $row){
            $optHtml.="<option value='".$row['COLUMN_NAME']."'>".$row['COLUMN_NAME']."</option>";
        }

        echo $optHtml;
    }


        public function showAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');
         $arr = array(    '1'=>$this->language->_('Daily'),
                        '2'=>$this->language->_('Weekly'),
                        '3'=>$this->language->_('Monthly')
                    );

         $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
       
        foreach($arr as $key => $row){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$key."' ".$select.">".$row."</option>";
        }

        echo $optHtml;
    }



        public function actionAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');
    $select = $this->_db->select()
                           ->from(  array(  'M'         =>'M_FPRIVILEGE'),
                                    array(  'privicode' =>'FPRIVI_ID',
                                            'prividesc' =>'FPRIVI_DESC') )
                           ->query()->fetchAll();

         $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
       
        foreach($select as $key => $row){
            if($tblName==$row['privicode']){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$row['privicode']."' ".$select.">".$row['prividesc']."</option>";
        }

        echo $optHtml;
    }

        

    public function wherecolumnAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $select = $this->_db->select()
                            ->from('information_schema.columns', array('DATA_TYPE','COLUMN_NAME'))
                            ->where('table_schema = ?', 'bank_mayapada_cm_demo')
                            ->where('table_name in ("T_PSLIP","T_TRANSACTION")')
                            ->where('COLUMN_NAME NOT in ("T_PSLIP","T_TRANSACTION","ESCROW_ACC",
                                    "ESCROW_ACC_TYPE",
                                    "HOST_RESPONSE",
                                    "REVERSAL_DESC",
                                    "REVERSAL_STATUS",
                                    "UUID",
                                    "PS_NUMBER",
                                    "LLD_DESC",
                                    "TRANSACTION_ID",
                                    "PS_SUBJECT",
                                    "LOG",
                                    "REFF_ID",
                                    "BENEFICIARY_ID",
                                    "BENEF_ACCT_BANK_CODE",
                                    "BENEFICIARY_DATA",
                                    "TRA_CHARGE_TO",
                                    "SENDFILE_sTATUS",
                                    "RELEASE_TYPE",
                                    "EFT_STATUS",
                                    "EFT_BANKCODE",
                                    "BANK_RESPONSE",
                                    "DATE_UPDATE",
                                    "BENEFICIARY_ADDRESS2",
                                    "BENEFICIARY_ADDRESS3",
                                    "PROVIDER_CHARGES",
                                    "LLD_CODE",
                                    "TRA_REFNO",
                                    "TRX_ID",
                                    "TRA_REMAIN",
                                    "TRANSFER_FEE_STATUS",
                                    "RELEASE_TYPE",
                                    "PS_BILLER_ID",
                                    "TRA_MESSAGE",
                                    "TRA_ADDITIONAL_MESSAGE",
                                    "PS_PERIODIC",
                                    "CUST_ID",
                                    "RATE",
                                    "RATE_BUY",
                                    "BOOK_RATE",
                                    "BOOK_RATE_BUY",
                                    "PROVISION_FEE",
                                    "FULL_AMOUNT_FEE",
                                    "TOTAL_CHARGES",
                                    "RATE_BUY",
                                    "PS_REMAIN",
                                    "FEATURE_ID",
                                    "PS_TXCOUNT",
                                    "PS_RELEASER_CHALLENGE",
                                    "PS_RELEASER_USER_LOGIN",
                                    "DISPLAY_FLAG",
                                    "RAW_REQUEST",
                                    "REVERSAL_DESC",
                                    "TRACE_NO",
                                    "TX_FEE_SCM_CHARGE_TO",
                                    "DISKONTO_AMOUNT",
                                    "BILLER_ORDER_ID",
                                    "EFT_BANKRESPONSE",
                                    "SKN_TRANSACTION_TYPE",
                                    "ORG_DIR",
                                    "SWIFT_CODE",
                                    "TRANSFER_FEE",
                                    "NOSTRO_CODE",
                                    "CLR_CODE",
                                    "BANK_CODE",
                                    "POB_NUMBER",
                                    "BENEFICIARY_BI_ACCOUNT",
                                    "BENEFICIARY_BANK_ADDRESS2",
                                    "BENEFICIARY_BANK_ADDRESS3" )');
                            // ->where('DATA_TYPE != ? ','datetime')
                            // ->where('DATA_TYPE != ? ','date');

        $tempColumn = $this->_db->fetchAll($select);
        $optHtml = "<option value=''>-- ".$this->language->_('Any Value')." --</option>";
        foreach ($tempColumn as $key => $value) {
            if($value['COLUMN_NAME'] == 'PS_NUMBER'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Payment Ref');
            }else if($value['COLUMN_NAME'] == 'PS_SUBJECT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Subject');
            }else if($value['COLUMN_NAME'] == 'PS_CREATED'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Create Date');
            }else if($value['COLUMN_NAME'] == 'PS_UPDATED'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Update Date');
            }else if($value['COLUMN_NAME'] == 'PS_EFDATE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transaction Date');
            }else if($value['COLUMN_NAME'] == 'PS_STATUS'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Payment Status');
            }else if($value['COLUMN_NAME'] == 'CUST_ID'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Company ID');
            }else if($value['COLUMN_NAME'] == 'PS_TOTAL_AMOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Total Amount');
            }else if($value['COLUMN_NAME'] == 'PS_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Payment Type');
            }else if($value['COLUMN_NAME'] == 'PS_CATEGORY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Category');
            }else if($value['COLUMN_NAME'] == 'PS_CCY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Curency');
            }else if($value['COLUMN_NAME'] == 'PS_CREATEDBY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Create By');
            }else if($value['COLUMN_NAME'] == 'EQUIVALENT_AMOUNT_IDR'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Equivalent Amount');
            }else if($value['COLUMN_NAME'] == 'TRANSACTION_ID'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transaction Id');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ACCOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Account');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_EMAIL'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Email');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CITIZENSHIP'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Citizenship');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_RESIDENT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Resident');
            }else if($value['COLUMN_NAME'] == 'TRA_AMOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Amount');
            }else if($value['COLUMN_NAME'] == 'TRANSFER_FEE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Fee');
            }else if($value['COLUMN_NAME'] == 'TRANSFER_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Type');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ALIAS_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Alias');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ACCOUNT_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Name');
            }else if($value['COLUMN_NAME'] == 'RATE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Rate Sell');
            }else if($value['COLUMN_NAME'] == 'RATE_BUY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Rate Buy');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ALIAS_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Type');
            }else if($value['COLUMN_NAME'] == 'BOOK_RATE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Book Rate Sell');
            }else if($value['COLUMN_NAME'] == 'BOOK_RATE_BUY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Book Rate Buy');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCT_BANK_CODE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Bank');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_CCY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account CCY');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Name');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_ALIAS_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Alias');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Type');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ACCOUNT_CCY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination CCY');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_MOBILE_PHONE_NUMBER'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Phone');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ADDRESS'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Address');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Name');
            }else if($value['COLUMN_NAME'] == 'TOTAL_CHARGES'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Total Charges');
            }else if($value['COLUMN_NAME'] == 'FULL_AMOUNT_FEE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Full Amount Fee');
            }else if($value['COLUMN_NAME'] == 'PROVISION_FEE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Provision Fee');
            }else if($value['COLUMN_NAME'] == 'EQUIVALENT_AMOUNT_USD'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Total USD');
            }else if($value['COLUMN_NAME'] == 'TRA_MESSAGE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Message');
            }else if($value['COLUMN_NAME'] == 'TRA_ADDITIONAL_MESSAGE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Additional Message');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Bank');
            }else if($value['COLUMN_NAME'] == 'TRA_STATUS'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Status');
            }else if($value['COLUMN_NAME'] == 'LLD_TRANSACTOR_RELATIONSHIP'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Transactor Relationship');
            }else if($value['COLUMN_NAME'] == 'LLD_TRANSACTION_PURPOSE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Transaction Purpose');
            }else if($value['COLUMN_NAME'] == 'LLD_IDENTITY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Identity');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ID_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Id Type');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ID_NUMBER'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Id Number');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CITY_CODE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination City');
            }else if($value['COLUMN_NAME'] == 'NOSTRO_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Nostro Name');
            }else if($value['COLUMN_NAME'] == 'LLD_DESC'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Description');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CITIZENSHIP_COUNTRY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Country');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_ADDRESS1'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Address');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_BRANCH'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Branch');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_COUNTRY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Bank Country');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CATEGORY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Category');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_CITY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Bank City');
            }
            else{
                $tempColumn[$key]['COLOMN'] = $value['COLUMN_NAME'];
            }
            
        }
        foreach($tempColumn as $row){
            $optHtml.="<option value='".$row['COLUMN_NAME']."-".$row['DATA_TYPE']."'>".$row['COLOMN']."</option>";
        }

        echo $optHtml;
    }


     public function wherecolumnnewAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $select = $this->_db->select()
                            ->from('information_schema.columns', array('DATA_TYPE','COLUMN_NAME'))
                            ->where('table_schema = ?', 'bank_mayapada_cm_demo')
                            ->where('table_name in ("T_PSLIP","T_TRANSACTION")')
                            ->where('COLUMN_NAME NOT in ("T_PSLIP","T_TRANSACTION","ESCROW_ACC",
                                    "ESCROW_ACC_TYPE",
                                    "HOST_RESPONSE",
                                    "REVERSAL_DESC",
                                    "REVERSAL_STATUS",
                                    "UUID",
                                    "PS_NUMBER",
                                    "LLD_DESC",
                                    "TRANSACTION_ID",
                                    "PS_SUBJECT",
                                    "LOG",
                                    "REFF_ID",
                                    "BENEFICIARY_ID",
                                    "BENEF_ACCT_BANK_CODE",
                                    "BENEFICIARY_DATA",
                                    "TRA_CHARGE_TO",
                                    "SENDFILE_sTATUS",
                                    "RELEASE_TYPE",
                                    "EFT_STATUS",
                                    "EFT_BANKCODE",
                                    "BANK_RESPONSE",
                                    "DATE_UPDATE",
                                    "BENEFICIARY_ADDRESS2",
                                    "BENEFICIARY_ADDRESS3",
                                    "PROVIDER_CHARGES",
                                    "LLD_CODE",
                                    "TRA_REFNO",
                                    "TRX_ID",
                                    "TRA_REMAIN",
                                    "TRANSFER_FEE_STATUS",
                                    "RELEASE_TYPE",
                                    "PS_BILLER_ID",
                                    "TRA_MESSAGE",
                                    "TRA_ADDITIONAL_MESSAGE",
                                    "PS_PERIODIC",
                                    "CUST_ID",
                                    "RATE",
                                    "RATE_BUY",
                                    "BOOK_RATE",
                                    "BOOK_RATE_BUY",
                                    "PROVISION_FEE",
                                    "FULL_AMOUNT_FEE",
                                    "TOTAL_CHARGES",
                                    "RATE_BUY",
                                    "PS_REMAIN",
                                    "FEATURE_ID",
                                    "PS_TXCOUNT",
                                    "PS_RELEASER_CHALLENGE",
                                    "PS_RELEASER_USER_LOGIN",
                                    "DISPLAY_FLAG",
                                    "RAW_REQUEST",
                                    "REVERSAL_DESC",
                                    "TRACE_NO",
                                    "TX_FEE_SCM_CHARGE_TO",
                                    "DISKONTO_AMOUNT",
                                    "BILLER_ORDER_ID",
                                    "EFT_BANKRESPONSE",
                                    "SKN_TRANSACTION_TYPE",
                                    "ORG_DIR",
                                    "SWIFT_CODE",
                                    "TRANSFER_FEE",
                                    "NOSTRO_CODE",
                                    "CLR_CODE",
                                    "BANK_CODE",
                                    "POB_NUMBER",
                                    "BENEFICIARY_BI_ACCOUNT",
                                    "BENEFICIARY_BANK_ADDRESS2",
                                    "BENEFICIARY_BANK_ADDRESS3" )')
                            ->where('DATA_TYPE != ? ','datetime')
                            ->where('DATA_TYPE != ? ','date');

        $tempColumn = $this->_db->fetchAll($select);
        $optHtml = "<option value=''>-- ".$this->language->_('Any Value')." --</option>";
        foreach ($tempColumn as $key => $value) {
            if($value['COLUMN_NAME'] == 'PS_NUMBER'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Payment Ref');
            }else if($value['COLUMN_NAME'] == 'PS_SUBJECT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Subject');
            }else if($value['COLUMN_NAME'] == 'PS_CREATED'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Create Date');
            }else if($value['COLUMN_NAME'] == 'PS_UPDATED'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Update Date');
            }else if($value['COLUMN_NAME'] == 'PS_EFDATE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transaction Date');
            }else if($value['COLUMN_NAME'] == 'PS_STATUS'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Payment Status');
            }else if($value['COLUMN_NAME'] == 'CUST_ID'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Company ID');
            }else if($value['COLUMN_NAME'] == 'PS_TOTAL_AMOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Total Amount');
            }else if($value['COLUMN_NAME'] == 'PS_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Payment Type');
            }else if($value['COLUMN_NAME'] == 'PS_CATEGORY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Category');
            }else if($value['COLUMN_NAME'] == 'PS_CCY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Curency');
            }else if($value['COLUMN_NAME'] == 'PS_CREATEDBY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Create By');
            }else if($value['COLUMN_NAME'] == 'EQUIVALENT_AMOUNT_IDR'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Equivalent Amount');
            }else if($value['COLUMN_NAME'] == 'TRANSACTION_ID'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transaction Id');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ACCOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Account');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_EMAIL'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Email');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CITIZENSHIP'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Citizenship');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_RESIDENT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Resident');
            }else if($value['COLUMN_NAME'] == 'TRA_AMOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Amount');
            }else if($value['COLUMN_NAME'] == 'TRANSFER_FEE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Fee');
            }else if($value['COLUMN_NAME'] == 'TRANSFER_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Type');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ALIAS_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Alias');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ACCOUNT_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Name');
            }else if($value['COLUMN_NAME'] == 'RATE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Rate Sell');
            }else if($value['COLUMN_NAME'] == 'RATE_BUY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Rate Buy');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ALIAS_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Type');
            }else if($value['COLUMN_NAME'] == 'BOOK_RATE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Book Rate Sell');
            }else if($value['COLUMN_NAME'] == 'BOOK_RATE_BUY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Book Rate Buy');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCT_BANK_CODE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Bank');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_CCY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account CCY');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Name');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_ALIAS_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Alias');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Type');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ACCOUNT_CCY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination CCY');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_MOBILE_PHONE_NUMBER'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Phone');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ADDRESS'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Address');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Name');
            }else if($value['COLUMN_NAME'] == 'TOTAL_CHARGES'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Total Charges');
            }else if($value['COLUMN_NAME'] == 'FULL_AMOUNT_FEE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Full Amount Fee');
            }else if($value['COLUMN_NAME'] == 'PROVISION_FEE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Provision Fee');
            }else if($value['COLUMN_NAME'] == 'EQUIVALENT_AMOUNT_USD'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Total USD');
            }else if($value['COLUMN_NAME'] == 'TRA_MESSAGE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Message');
            }else if($value['COLUMN_NAME'] == 'TRA_ADDITIONAL_MESSAGE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Additional Message');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Bank');
            }else if($value['COLUMN_NAME'] == 'TRA_STATUS'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Status');
            }else if($value['COLUMN_NAME'] == 'LLD_TRANSACTOR_RELATIONSHIP'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Transactor Relationship');
            }else if($value['COLUMN_NAME'] == 'LLD_TRANSACTION_PURPOSE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Transaction Purpose');
            }else if($value['COLUMN_NAME'] == 'LLD_IDENTITY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Identity');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ID_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Id Type');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ID_NUMBER'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Id Number');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CITY_CODE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination City');
            }else if($value['COLUMN_NAME'] == 'NOSTRO_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Nostro Name');
            }else if($value['COLUMN_NAME'] == 'LLD_DESC'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Description');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CITIZENSHIP_COUNTRY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Country');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_ADDRESS1'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Address');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_BRANCH'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Branch');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_COUNTRY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Bank Country');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CATEGORY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Category');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_CITY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Bank City');
            }
            else{
                $tempColumn[$key]['COLOMN'] = $value['COLUMN_NAME'];
            }
            
        }
        foreach($tempColumn as $row){
            $optHtml.="<option value='".$row['COLUMN_NAME']."-".$row['DATA_TYPE']."'>".$row['COLOMN']."</option>";
        }

        echo $optHtml;
    }




    public function wherecolumnpsstatusAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');
        $arrPayStatus   = array_combine($this->_paymentstatus["code"], $this->_paymentstatus["desc"]);
        foreach($arrPayStatus as $key => $value){ if($key != 3) $optpayStatusRaw[$key] = $this->language->_($value); }
        

        // $tempColumn = $this->_db->fetchAll($select);
        $opt[""] = "-- " .$this->language->_('Please Select'). " --";
        $optPayStatus   = $opt + $optpayStatusRaw;
        // print_r($optpayStatusRaw);die;
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
       
        foreach($optpayStatusRaw as $key => $row){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$key."' ".$select.">".$row."</option>";
        }

        echo $optHtml;
    }

     public function wherecolumnpstypeAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');
//         $payType    = Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
//         foreach($payType as $key => $value){
// //          if($key != 3) $optpaytypeRaw[$key] = $this->language->_($value);

//              $optpaytypeRaw[$key] = $this->language->_($value);
//         }


//         // $tempColumn = $this->_db->fetchAll($select);
//         // $opt[""] = "-- " .$this->language->_('Please Select'). " --";
//         // $optPayStatus   = $opt + $optpayStatusRaw;
//         // print_r($optpayStatusRaw);die;
//         $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";

//         foreach($optpaytypeRaw as $key => $row){
//             if($tblName==$key){
//                 $select = 'selected';
//             }else{
//                 $select = '';
//             }
//             $optHtml.="<option value='".$key."' ".$select.">".$row."</option>";
//         }

        $conf = Zend_Registry::get('config');
        $paymentType = $conf['payment']['type'];

        $setting = new Settings();
        $system_type    = $setting->getSetting('system_type');

        $paymentTypeFlip = array_flip($paymentType['code']);
        //$system_type = 2;
        if ($system_type == 2) {
        $selectTrx  = $this->_db->select()
            ->distinct()
            ->from( array('T_PSLIP'),array('PS_TYPE'))
            ->where('PS_TYPE IN (18,19,20,21,23)')
            ->group('PS_TYPE')
            ->order("PS_TYPE ASC");
        }else if($system_type == 1){
            $selectTrx  = $this->_db->select()
            ->distinct()
            ->from( array('T_PSLIP'),array('PS_TYPE'))
            ->where('PS_TYPE NOT IN (18,19,20,21,23)')
            ->group('PS_TYPE')
            ->order("PS_TYPE ASC");
        }else{
        $selectTrx  = $this->_db->select()
            ->distinct()
            ->from( array('T_PSLIP'),array('PS_TYPE'))
            ->group('PS_TYPE')
            ->order("PS_TYPE ASC");
        }
            
        $ACBENEFArr = $this->_db->fetchAll($selectTrx);

        if ($system_type == 1) {
            unset($paymentType['code']['opentransfer']);
            unset($paymentType['code']['sweeptransfer']);
            unset($paymentType['code']['opensweep']);
            unset($paymentType['code']['opensweepother']);

            unset($ACBENEFArr[14]);
            unset($ACBENEFArr[15]);
            unset($ACBENEFArr[16]);
            unset($ACBENEFArr[17]);

        }
		
		unset($ACBENEFArr[28]);
        unset($ACBENEFArr[27]);
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        //echo '<pre>';
		//var_dump($ACBENEFArr);
        foreach ($ACBENEFArr as $key => $value) {

            if ($value['PS_TYPE'] != NULL && $value['PS_TYPE'] != 27 && $value['PS_TYPE'] != 28) {

                if ($system_type == 2) {
                    
                    if ($value['PS_TYPE'] == '19') {
                     $payType = 'CP Same Bank Remains';
                     $pstype = $value['PS_TYPE'];
                    }else if ($value['PS_TYPE'] == '20') {
                         $payType = 'CP Same Bank Maintains';
                         $pstype = $value['PS_TYPE'];
                    }else if ($value['PS_TYPE'] == '23') {
                         $payType = 'CP Others Remains';
                         $pstype = $value['PS_TYPE']; 
                    }else if ($value['PS_TYPE'] == '21') {
                         $payType = 'Money Movement';
                         $pstype = $value['PS_TYPE'];
                    }
                        
                }else if($system_type == 1){

                    $payType = $paymentType['desc'][$paymentTypeFlip[$value['PS_TYPE']]];
                    $pstype = $value['PS_TYPE'];
                    
                }else{

                    $payType = $paymentType['desc'][$paymentTypeFlip[$value['PS_TYPE']]];
                    $pstype = $value['PS_TYPE'];

                }
                
                if($tblName==$pstype){
                    $select = 'selected';
                }else{
                    $select = '';
                }

                if ($pstype) {
                    $optHtml.="<option value='".$pstype."' ".$select.">".$payType."</option>";
                }
                
            }
            
        }
        
        if ($pstype) {
            echo $optHtml;
        }
    }

    public function wheresourceaccountAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');
        $CustomerUser   = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
        
        $arrAccount     = $CustomerUser->getAccounts();
        
        if(is_array($arrAccount) && count($arrAccount) > 0){
            foreach($arrAccount as $key => $value){
                
                $val        = $arrAccount[$key]["ACCT_NO"];
                $ccy        = $arrAccount[$key]["CCY_ID"];
                $acctname   = $arrAccount[$key]["ACCT_NAME"];
                //$acctalias    = $arrAccount[$key]["ACCT_ALIAS_NAME"];
                $accttype   = ($arrAccount[$key]["ACCT_TYPE"] == '10') ? 'SAVING': 'GIRO';  // 10 : saving, 20 : giro;
                
                $arrAccountRaw[$val] = $val.' ['.$ccy.'] '.$acctname.' ('.$accttype.')';
                
            }
        }
        else { $arrAccountRaw = array();}

        // $tempColumn = $this->_db->fetchAll($select);
        // $opt[""] = "-- " .$this->language->_('Please Select'). " --";
        // $optPayStatus   = $opt + $optpayStatusRaw;
        // print_r($arrAccountRaw);die;
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
       
        foreach($arrAccountRaw as $key => $row){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$key."' ".$select.">".$row."</option>";
        }

        echo $optHtml;
    }




     public function whereuserAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        $cust_id = (string)$this->_custIdLogin;
        $cust = new Customer($cust_id);
        $userList = $cust->getUserList();
        $tblName = $this->_getParam('id');
        // $tempColumn = $this->_db->fetchAll($select);
        // $opt[""] = "-- " .$this->language->_('Please Select'). " --";
        // $optPayStatus   = $opt + $optpayStatusRaw;
        // print_r($arrAccountRaw);die;
        // alert($tblName);
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        if(!empty($userList)){
        foreach($userList as $key => $row){
            if($tblName==$row['USER_ID']){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$row['USER_ID']."' ".$select.">".$row['USER_ID']."</option>";
        }
        }

        echo $optHtml;
    }


    public function approvestatusAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        
        $tblName = $this->_getParam('id');
        $approvalstatus = $this->_aprovalstatus;
        $statusarr = array_combine(array_values($approvalstatus['code']),array_values($approvalstatus['desc']));
        // print_r($statusarr);
        unset($statusarr['2']);
        unset($statusarr['3']);
        unset($statusarr['4']);
        unset($statusarr['5']);
        unset($statusarr['6']);
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        if(!empty($statusarr)){
        foreach($statusarr as $key => $row){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$key."' ".$select.">".$row."</option>";
        }
        }

        echo $optHtml;
    }


      public function serviceareaAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        $tblName = $this->_getParam('id');
        $select = $this->_db->select()
                           ->distinct()
                           ->from('M_SERVICE_AREA',array('AREA_NAME'))
                           ->query()->fetchAll();
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        if(!empty($select)){
        foreach($select as $key => $row){
            if($tblName==$row['AREA_NAME']){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$row['AREA_NAME']."' ".$select.">".$row['AREA_NAME']."</option>";
        }
        }

        echo $optHtml;

    
    }

     public function cifaccountAction(){


     $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        
        $tblName = $this->_getParam('id');
             $app = Zend_Registry::get('config');
        $app = $app['app']['bankcode'];
       // return $app;
    
        $core = array();
    $svcAccount = new Service_Account($result['CUST_CIF'], Application_Helper_General::getCurrNum('IDR'), $app);
    $result   = $svcAccount->cifaccountInquiry($core);
    
    if($core->responseCode == '0000' || $result['ResponseCode']=='0000' ){
          $arrResult = array();
          $err = array();
          
          if(count($core->accountList) > 1){
//            print_r($core->accountList);
            //array data lebih dari 1
            foreach ($core->accountList as $key => $val){
              if($val->status == '0'){  
                $arrResult = array_merge($arrResult, array($val->accountNo => $val));
                array_push($err, $val->accountNo);
              }
//              echo 'here';
            }
          }
          else{
            //array kurang dari 2
            foreach ($core as $data){
              if($data->status == '0'){ 
                $arrResult = array_merge($arrResult, array($data->accountNo => $data));
                array_push($err, $data->accountNo);
              }
//              echo 'here1';
//            Zend_Debug::dump($data);
            }
          }
          
        $final = $arrResult;
        if(count($final) < 1){
          $this->view->data1 = '0';
        }
        else{
          $this->view->data = $arrResult;
        }
        
//        $dataPro = array();
        $dataProPlan = array('xxxxx');
          foreach ($final as $dataProductPlan){
            // print_r($dataProductPlan);die;
              if($dataProductPlan->status == '0'){  
                if (isset($dataProductPlan->productType))
                {
                $select_product_type  = $this->_db->select()
                      ->from(array('M_PRODUCT_TYPE'),array('PRODUCT_NAME'))
                      ->where("PRODUCT_CODE = ?", $dataProductPlan->productType);
        //              ->where('PRODUCT_PLAN = ?','62');
                      // echo $select_product_type;
                $userData_product_type = $this->_db->fetchAll($select_product_type);
  //              
                // print_r($userData_product_type);
                $dataProductPlan->planName = $userData_product_type[0]['PRODUCT_NAME'];
                // $dataProductPlanOri = (($dataProductPlan->planName == null)?'':$dataProductPlan);
                }else{
                  $dataProductPlan = $dataProductPlan; 
                }             
              
              // print_r($dataProductPlan);die;
             $dataUser[] = $dataProductPlan;
              }
          }

     }

        $optHtml = "<table cellpadding='3' cellspacing='0' border='0' class='table table-sm table-striped'><tr class='headercolor'><th width='3%' valign='top' style='background-color: #0a3918; color:white'></th><th  valign='top'  style='background-color: #0a3918; color:white'>Account Number</th><th  valign='top'  style='background-color: #0a3918; color:white'>Account Name</th><th  valign='top'  style='background-color: #0a3918; color:white'>Currency</th><th  valign='top'  style='background-color: #0a3918; color:white'>Type</th></tr>";


        if(!empty($dataUser)){
        foreach($dataUser as $key => $row){
             $i = $i + 1;    
            $acct_source = 3;
            $acct_desc   = $row->planName;

            $button_checkbox = "<input type='checkbox' name='req_id[]' id='".$row->accountNo."' value='".$row->accountNo."'>";
            // $button_checkbox = $this->formCheckbox("req_id[{$row->accountNo}]",null);
            $td_css = ($i%2==0)?'tbl-evencontent':'tbl-oddcontent';
            if ($this->data[$key] != ""){
                if($row->accountNo == ''){
                }
                else{
                    $optHtml .= '<tr>td class="'.$td_css.'"><input type="checkbox" name="req_id[]" id="'.$row->accountNo.'" onclick="check()" value="'.$row->accountNo.'"></td><td class="'.$td_css.'">'.$row->accountNo.'</td><td class="'.$td_css.'"><input type="hidden" name="accountName'.$row->accountNo.'" value="'.$row->accountName.'">'.$row->accountName.'</td>';
                    $optHtml .= '<td class="'.$td_css.'"><input type="hidden" name="ccy'.$row->accountNo.'" value="'.Application_Helper_General::getCurrCode($row->ccy).'">'.Application_Helper_General::getCurrCode($row->ccy).'</td><td class="'.$td_css.'"><input type="hidden" name="acct_desc'.$row->accountNo.'" value="'.$acct_desc.'"><input type="hidden" name="productType'.$row->accountNo.'" value="'.$row->productType.'">'.$acct_desc.'<input type="hidden" name="planCode'.$row->accountNo.'" value="'.$row->planCode.'"><input type="hidden" name="productName'.$row->accountNo.'" value="'.$acct_desc.'"></td></tr>';    
                        
                }
            }
        }
        }else{
            // if($this->data == ''){
        $optHtml .= '<tr><td colspan="5" class="tbl-evencontent" align="center">'.$this->language->_('No Data').'</td></tr>';
            
        }
        $optHtml .= '</table>';
        echo $optHtml;
  }


    public function nostroAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        $tblName = $this->_getParam('id');
        // $select = ( array(''=>'-- '.$this->language->_('Please Select').' --')+Application_Helper_Array::listArray($model->getNostroNames(),'NOSTRO_NAME','NOSTRO_NAME'));
        $select = $this->_db->select()
                            ->from(array('A' => 'M_NOSTRO_BANK'),array('NOSTRO_NAME'))
                            ->group('NOSTRO_NAME');
        $select = $this->_db->fetchall($select);
        // print_r($select);die;
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        if(!empty($select)){
        foreach($select as $key => $row){
            if($tblName==$row['NOSTRO_NAME']){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$row['NOSTRO_NAME']."' ".$select.">".$row['NOSTRO_NAME']."</option>";
        }
        }

        echo $optHtml;

    
    }


    public function custbinAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        $tblName = $this->_getParam('id');
    
            $select = $this->_db->select()
                                ->from(array('B'            => 'M_CUSTOMER_BIN'),
                                       array('B.CUST_ID')
                                       )
                                ->join(array('A'=>'M_CUSTOMER'), 'A.CUST_ID = B.CUST_ID',array('A.CUST_NAME'))
                                ->query()->fetchAll();

        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        if(!empty($select)){
        foreach($select as $key => $row){
            if($tblName==$row['NOSTRO_NAME']){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$row['CUST_ID']."' ".$select.">".$row['CUST_NAME']."</option>";
        }
        }

        echo $optHtml;
    }


     public function countryAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        $tblName = $this->_getParam('id');
     

        $selectcountry = $this->_db->select()
        ->from('M_COUNTRY')
        ->query()->fetchAll();
        // print_r($selectcountry);die;

        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        if(!empty($selectcountry)){
        foreach($selectcountry as $key => $row){
            if($tblName==$row['COUNTRY_CODE']){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$row['COUNTRY_CODE']."' ".$select.">".$row['COUNTRY_NAME']."</option>";
        }
        }

        echo $optHtml;

    
    }


    
     public function tratypereportAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        // $cust_id = (string)$this->_custIdLogin;
        // $cust = new Customer($cust_id);
        // $userList = $cust->getUserList();
        $tblName = $this->_getParam('id');
        $arrTraType     = array_combine($this->_transfertype["code"],$this->_transfertype["desc"]);
        // $tempColumn = $this->_db->fetchAll($select);
        // $opt[""] = "-- " .$this->language->_('Please Select'). " --";
        // $optPayStatus   = $opt + $optpayStatusRaw;
        // print_r($arrAccountRaw);die;
        // alert($tblName);
        // print_r($arrTraType);die;
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        if(!empty($arrTraType)){
            unset($arrTraType[0]);
            unset($arrTraType[1]);
            unset($arrTraType[2]);
            unset($arrTraType[5]);
            unset($arrTraType[6]);
            unset($arrTraType[9]);
            unset($arrTraType[10]);
        foreach($arrTraType as $key => $row){
            // print_r($key);
            if($key=='3'){
                $row = 'Remittance (OUR)';
            }else if($key=='4'){
                $row = 'Remittance (SHA)';
            }else if($key=='7'){
                $row = 'Mayapada (Sell)';
            }else if($key=='8'){
                $row = 'Mayapada (Buy)';
            }
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$key."' ".$select.">".$row."</option>";
        }
        }

        echo $optHtml;
    }



      public function tratypeAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        // $cust_id = (string)$this->_custIdLogin;
        // $cust = new Customer($cust_id);
        // $userList = $cust->getUserList();
        $tblName = $this->_getParam('id');
        $arrTraType     = array_combine($this->_transfertype["code"],$this->_transfertype["desc"]);
        // $tempColumn = $this->_db->fetchAll($select);
        // $opt[""] = "-- " .$this->language->_('Please Select'). " --";
        // $optPayStatus   = $opt + $optpayStatusRaw;
        // print_r($arrAccountRaw);die;
        // alert($tblName);
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        if(!empty($arrTraType)){
        foreach($arrTraType as $key => $row){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$key."' ".$select.">".$row."</option>";
        }
        }

        echo $optHtml;
    }


     public function trastatusAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        // $cust_id = (string)$this->_custIdLogin;
        // $cust = new Customer($cust_id);
        // $userList = $cust->getUserList();
        $tblName = $this->_getParam('id');
         
        $arrTraStatus   = array_combine($this->_transferstatus["code"],$this->_transferstatus["desc"]);
        
        unset ($arrTraStatus['2']);
        unset ($arrTraStatus['0']);
        unset ($arrTraStatus['1']);

        // $tempColumn = $this->_db->fetchAll($select);
        // $opt[""] = "-- " .$this->language->_('Please Select'). " --";
        // $optPayStatus   = $opt + $optpayStatusRaw;
        // print_r($arrAccountRaw);die;
        // alert($tblName);
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        if(!empty($arrTraStatus)){
        foreach($arrTraStatus as $key => $row){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$key."' ".$select.">".$row."</option>";
        }
        }

        echo $optHtml;
    }


       
    

       public function paytypeAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        // $cust_id = (string)$this->_custIdLogin;
        // $cust = new Customer($cust_id);
        // $userList = $cust->getUserList();
        $tblName = $this->_getParam('id');
        $arrPayType     = Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
        // $tempColumn = $this->_db->fetchAll($select);
        // $opt[""] = "-- " .$this->language->_('Please Select'). " --";
        // $optPayStatus   = $opt + $optpayStatusRaw;
        // print_r($arrAccountRaw);die;
        // alert($tblName);
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        if(!empty($arrPayType)){
        foreach($arrPayType as $key => $row){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$key."' ".$select.">".$row."</option>";
        }
        }

        echo $optHtml;
    }
    


     public function approvestatAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        $aprovalstatus = $this->_aprovalstatus;
        $aprovalstatus = array(''=>'-- '. $this->language->_('All') .' --');
        $aprovalstatus += Application_Helper_Array::globalvarArray($this->_aprovalstatus);
        foreach($aprovalstatus as $key => $value){ if($key != 3) $optpayStatusRaw[$key] = $this->language->_($value); }
        
        unset($aprovalstatus[2]);
        unset($aprovalstatus[3]);
        unset($aprovalstatus[6]);
        
        unset($optpayStatusRaw[2]);
        unset($optpayStatusRaw[3]);
        unset($optpayStatusRaw[6]);

         foreach ($optpayStatusRaw as $key => $value) {
            // print_r($key);die;\
                if(!empty($tblName)){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }

            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;

    }

   public function categoryAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
         $tblName = $this->_getParam('id');
        
    $arrquestioncategory    = array_combine($this->_questioncategory["code"],$this->_questioncategory["desc"]);
        $questioncategoryArray  = array( '' => '--- '.$this->language->_('Please Select').' --- ');
        $questioncategoryArray += array_combine(array_values($this->_questioncategory['code']),array_values($this->_questioncategory['desc']));             
        //$this->view->questioncategoryArray        = $questioncategoryArray;
        
        foreach($questioncategoryArray as $key => $value){ if($key != 5) $optpayStatusRaw[$key] = $this->language->_($value); }
    
        $optPayType = $optpayStatusRaw;
        // print_r($optPayType);die;
        foreach ($optPayType as $key => $value) {
            // print_r($key);die;\
                if(!empty($tblName)){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }

            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;
        // $this->view->questioncategoryArray  = $optPayType;
        

    }

    public function ccyAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');
        
        // $tempColumn = $this->_db->fetchAll($select);
        // $opt[""] = "-- " .$this->language->_('Please Select'). " --";
        // $optPayStatus   = $opt + $optpayStatusRaw;
        // print_r($arrAccountRaw);die;
        $select = $this->_db->select()
                            ->from(array('A' => 'M_MINAMT_CCY'),array('CCY_ID'));
        
        $select = $this->_db->fetchall($select);
        // print_r($select);die;
       $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        if(!empty($select)){
        foreach($select as $key => $row){
            if($tblName==$row['CCY_ID']){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$row['CCY_ID']."' ".$select.">".$row['CCY_ID']."</option>";
        }
        }
       
        // $optHtml.="<option value='".$key."' ".$select.">".$row."</option>";
        // $optHtml.="<option value='IDR' ".$selectidr.">IDR</option>";
        // $optHtml.="<option value='USD' ".$selectusd.">USD</option>";
       

        echo $optHtml;
    }



     public function providerAction(){
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $data = $this->_db->select()->distinct()
                    ->from(array('M_SERVICE_PROVIDER'),array('PROVIDER_CODE'))
                    ->order('PROVIDER_CODE ASC')
                    -> query() ->fetchAll();

        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        foreach ($data as $key => $value) {
            if($tblName==$value['PROVIDER_CODE']){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$value['PROVIDER_CODE']."' ".$select.">".$value['PROVIDER_CODE']."</option>";
        }

        echo $optHtml;
    
    }


    public function providerstatusAction(){
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');
    $options = array_combine(array_values($this->_masterglobalstatus['code']),array_values($this->_masterglobalstatus['desc']));
        unset($options[3]);
        array_unshift($options,'---'.$this->language->_('Any Value').'---');
        foreach($options as $key => $value){ if($key != 3) $optpayStatusRaw[$key] = $this->language->_($value); }
         foreach ($optpayStatusRaw as $key => $value) {
            // print_r($key);die;\
                if(!empty($tblName)){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }

            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;
    }

     public function providercategoryAction(){
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

       $data = $this->_db->select()->distinct()
                    ->from(array('M_SERVICES_TYPE'))
                    ->order('SERVICE_OF ASC')
                    ->order('SERVICE_NAME ASC')
                    ->query()->fetchAll();

        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        foreach ($data as $key => $value) {
            if($tblName==$value['SERVICE_ID']){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$value['SERVICE_ID']."' ".$select.">".$value['SERVICE_NAME']."</option>";
        }

        echo $optHtml;
    
    }

    public function bgroupAction(){
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

         $selectGroup =  $this->_db->select()
                                    ->from( 'M_BGROUP',array('BGROUP_ID','BGROUP_DESC'))
                                    ->where("BGROUP_STATUS = '1'");         
        $arrGroup = $this->_db->fetchAll($selectGroup);
         $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        foreach ($arrGroup as $key => $value) {
            if($tblName==$value['BGROUP_ID']){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$value['BGROUP_ID']."' ".$select.">".$value['BGROUP_DESC']."</option>";
        }

        echo $optHtml;

    }

    public function bbranchAction(){
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

         $selectGroup =  $this->_db->select()
                                    ->from( 'M_BRANCH',array('ID','BRANCH_NAME'));
                                    // ->where("BGROUP_STATUS = '1'");         
        $arrGroup = $this->_db->fetchAll($selectGroup);
         $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        foreach ($arrGroup as $key => $value) {
            if($tblName==$value['ID']){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$value['ID']."' ".$select.">".$value['BRANCH_NAME']."</option>";
        }

        echo $optHtml;

    }

    public function bislockedAction(){
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');
			if($tblName== '1'){
                $selectYes = 'selected';
            }else{
                $selectYes = '';
            }
			
			if($tblName== '0'){
                $selectNo = 'selected';
            }else{
                $selectNo = '';
            }
			
         $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
      
            $optHtml.="<option value='1' ".$selectYes.">Yes</option>";
            $optHtml.="<option value='0' ".$selectNo.">No</option>";

        echo $optHtml;

    }

    public function bstatusAction(){
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');


    $statusarr = array_combine(array_values($this->_masterglobalstatus['code']),array_values($this->_masterglobalstatus['desc']));

      $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        foreach ($statusarr as $key => $value) {
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;

    }

        public function bactAction(){
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');


        $activity = $this->_db->select()->distinct()
                                ->from(array('A' => 'M_BPRIVILEGE'),array('BPRIVI_ID', 'BPRIVI_DESC'))
                                ->order('BPRIVI_DESC ASC')
                                ->query()->fetchAll();
                                
        $activityarr = Application_Helper_Array::listArray($activity,'BPRIVI_ID','BPRIVI_DESC');

        asort($activityarr);
        // print_r($activityarr);
         $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        foreach ($activityarr as $key => $value) {
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;

    }


        public function blockAction(){
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";

        if($tblName == 'yes'){
            $selecty= 'selected';
        }else if($tblName == 'no'){
            $selectn= 'selected';
        }

        $optHtml.="<option value='yes' ".$selecty.">".$this->language->_('Locked')."</option>";
        $optHtml.="<option value='no' ".$selectn.">".$this->language->_('Not Locked')."</option>";
        
        echo $optHtml;


        }



        public function changesugesttypeAction(){
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        unset($this->_suggestType['code']['activate']);
        unset($this->_suggestType['desc']['activate']);
        unset($this->_suggestType['code']['deactivate']);
        unset($this->_suggestType['desc']['deactivate']);
        // $this->view->suggestionType   =  $this->_suggestType;
        $sugest = $this->_suggestType;
        $changesTypeDescArr = $sugest['desc']; 
        $changesTypeCodeArr = $sugest['code'];
        $changesFilterArr = array_flip($changesTypeCodeArr);
        foreach($changesFilterArr as $key=>$val)
            $changesFilterArr[$key] = $this->language->_($changesTypeDescArr[$val]);
        //Zend_Debug::dump($changesTypeCodeArr);
        $changesFilterArr = array_merge(array('' => '-- '.$this->language->_('Any Value').' --'),$changesFilterArr);    

        foreach ($changesFilterArr as $key => $value) {
            // print_r($key);die;\
                if(!empty($tblName)){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }

            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;

        }



        public function echanelsugesttypeAction(){
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');
        $echannelArr = array('Permata Online (Bisnis)' => 'Permata Online (Bisnis)');

        $echannelArr = array_merge(array(''=>'--- '.$this->language->_('Any Value').' ---'),$echannelArr);

         foreach ($echannelArr as $key => $value) {
            // print_r($key);die;\
                if(!empty($tblName)){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }

            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;

        }

        public function byesnoAction(){
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";

        if($tblName == '1'){
            $selecty= 'selected';
        }else if($tblName == '0'){
            $selectn= 'selected';
        }

        $optHtml.="<option value='1' ".$selecty.">".$this->language->_('Yes')."</option>";
        $optHtml.="<option value='0' ".$selectn.">".$this->language->_('No')."</option>";
        
        echo $optHtml;


        }


     public function statusAction(){
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $customerstatus = $this->_customerstatus;

        $options = array_combine(array_values($customerstatus['code']),array_values($customerstatus['desc']));
        // print_r($options);die;
        unset($options[0]);
        unset($options[4]);
        unset($options[5]);
        foreach ($options as $key => $value) {
            // print_r($key);die;\
                if(!empty($tblName)){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }

            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;


    }





    public function actlistAction(){

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $activity = $this->_db->select()->distinct()
                                ->from(array('A' => 'M_FPRIVILEGE'),array('FPRIVI_ID', 'FPRIVI_DESC'))
                                ->order('FPRIVI_DESC ASC')
                                ->query()->fetchAll();
        
        $login = array('FPRIVI_ID'=>'FLGN','FPRIVI_DESC'=>'Login');
        $logout = array('FPRIVI_ID'=>'FLGT','FPRIVI_DESC'=>'Logout');
        $changepass = array('FPRIVI_ID'=>'CHMP','FPRIVI_DESC'=>'Change My Password');
        $resetpass = array('FPRIVI_ID'=>'RFPW','FPRIVI_DESC'=>'Reset Forgot Password');
        array_unshift($activity,$changepass);
        array_unshift($activity,$logout);
        array_unshift($activity,$login);
        array_unshift($activity,$resetpass);
                                
        $activityarr = Application_Helper_Array::listArray($activity,'FPRIVI_ID','FPRIVI_DESC');
        asort($activityarr);
        // print_r($activityarr);
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";

        foreach ($activityarr as $key => $value) {
            // print_r($key);die;\
                if(!empty($tblName)){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }

            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;

    }

    public function sugesttypeAction(){
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $changesTypeCodeArr   = $this->_suggestType['code'];
        
        unset($this->_suggestType['code']['activate']);
        unset($this->_suggestType['desc']['activate']);
        unset($this->_suggestType['code']['deactivate']);
        unset($this->_suggestType['desc']['deactivate']);
        $options = array_combine(array_values($this->_suggestType['code']),array_values($this->_suggestType['desc']));
        
        // print_r($options);die;
        foreach ($options as $key => $value) {
            // print_r($key);die;\
                if(!empty($tblName)){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }

            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;


    }

    public function sugeststatusAction(){

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');


    $suggestionStatusCP = array( "" => '--'.$this->language->_('Any Value')."--",
                                            "UR"=> $this->language->_('Unread Suggestion') ,
                                            "RS"=> $this->language->_('Read Suggestion'), 
                                            "RR"=> $this->language->_('Request Repaired'),
                                            "RP"=> $this->language->_('Repaired Suggestion'),
        ); 
    $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
     foreach ($suggestionStatusCP as $key => $value) {
            // print_r($key);die;\
                if(!empty($tblName)){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }

            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;


    }


        public function chargesstatusAction(){

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

    $arrChargeStatus = array_combine($this->_chargestatus["code"],$this->_chargestatus["desc"]);
        foreach($arrChargeStatus as $key => $value){
            if($key != '13'){ $arrStatusK[$key] = $this->language->_($value); }
        
        }


         $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
     foreach ($arrStatusK as $key => $value) {
            // print_r($key);die;\
                if(!empty($tblName)){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }

            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;


    }

         public function chargestypeAction(){

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

                    $arrtype    = array_combine($this->_chargestype["code"], $this->_chargestype["desc"]);
                    unset($arrtypee[3]);
                    unset($arrtype[4]);
                    unset($arrtype[5]);
                    unset($arrtype[6]);
                    unset($arrtype[7]);
                    foreach($arrtype as $key => $value){ if($key != 3) $optpayStatusRaw[$key] = $this->language->_($value); }


         $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
     foreach ($optpayStatusRaw as $key => $value) {
            // print_r($key);die;\
                if(!empty($tblName)){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }

            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;


    }


    public function areaAction(){

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');


    $areaArr = $select = $this->_db->select()
                           ->distinct()
                           ->from('M_SERVICE_AREA',array('AREA_NAME'))
                           ->query()->fetchAll();
        //$this->view->branchArr = array(''=>'-- '.$this->language->_('Any Value').' --')+Application_Helper_Array::listArray($model->getBranch(),"CODE","NAME");
        $area = array(''=>'-- '.$this->language->_('Any Value').' --')+Application_Helper_Array::listArray($areaArr,"AREA_NAME","AREA_NAME");
        // print_r($area);die;
        foreach ($area as $key => $value) {
            // print_r($key);die;\
                if(!empty($tblName)){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }

            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;
        
    }

    public function sugestdataAction(){

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');


    $listSuggestData  = array(
        'General Setting'=>$this->language->_('General Setting'),
        'Minimum Amount and Currency Available'=>$this->language->_('Minimum Amount and Currency Available'),
         'Holiday Setting'=>$this->language->_('Holiday Setting'),
         'Customer'=>$this->language->_('Customer'),
         'Bank Account'=>$this->language->_('Bank Account'),
         'User Account'=>$this->language->_('User List'),
        // 'User Limit'=>$this->language->_('User Limit'),
        // 'User Daily Limit'=>$this->language->_('User Daily Limit'),
        // 'Approver Group'=>$this->language->_('Approver Group'),
        // 'Approver Group Boundary'=>$this->language->_('Approver Group Boundary'),
        'Backend User'=>$this->language->_('Backend User'),
        'Backend Group'=>$this->language->_('Backend Group'),
        'Backend End Global Setting'=>$this->language->_('Backend End Global Setting'),
        'Notional Pooling'=>$this->language->_('Notional Pooling'),
        'Approver Matrix'=>$this->language->_('Approver Matrix'),
        'Auto Debit'=>$this->language->_('Auto Debit'),
        'directdebit'=>$this->language->_('Direct Debit'),
        
        
         'Charges'=>$this->language->_('Charges Setting'),
         'Global Charges'=>$this->language->_('Global Charges Setting'),
        // 'Charges Template'=>$this->language->_('Charges Template'),
        'User Account' => $this->language->_('User Account'),
        'User List' => $this->language->_('User List'),
        'User Limit'=>$this->language->_('User Limit'),
        'User Daily Limit'=>$this->language->_('User Daily Limit'),
        'Approver Group'=>$this->language->_('Approver Group'),
        'Approver Group Boundary'=>$this->language->_('Approver Group Boundary'),
        // 'COA Account'=>'COA Account',
        // 'Charges Template'=>'Charges Template',
        // 'Charges'=>'Charges',
        // 'System Balance'=>'System Balance',
        // 'Global Scheme Parameter'=>'Global Scheme Parameter',
        // 'Scheme Configuration'=>'Scheme Configuration',
        // 'Physical Document Configuration'=>'Physical Document Configuration',
        // 'Root Community'=>'Root Community',
        // 'Principal'=>'Principal',
        // 'Community'=>'Community',
        // 'Member'=>'Member',
        // 'Assign User to Community'=>'Assign User to Community',
    );
    ksort($listSuggestData);
    $listSuggestData = array('all'=>'--'.$this->language->_('Any Value').'--')+$listSuggestData;

     // $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";

        foreach ($listSuggestData as $key => $value) {
            // print_r($key);die;\
                if(!empty($tblName)){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }

            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;


    }

    public function benetypeAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');
        
        // $tempColumn = $this->_db->fetchAll($select);
        // $opt[""] = "-- " .$this->language->_('Please Select'). " --";
        // $optPayStatus   = $opt + $optpayStatusRaw;
        // print_r($arrAccountRaw);die;
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
       $selectidr = '';
       $selectusd = '';
       if(!empty($tblName)){
            if($tblName=='IDR'){
                $selectidr = 'selected';
            }else{
                $selectusd = '';
            }

       }
       if($tblName == '1'){$select =  'selected';}
        $type =$this->language->_('In House');
        $optHtml.="<option value = '1' ".$select.">".$type."</option>";
        if($tblName == '2'){$select =  'selected';}
        $type =$this->language->_('Domestic');
        $optHtml.="<option value = '2' ".$select.">".$type."</option>";
        if($tblName == '3'){$select =  'selected';}
        $type =$this->language->_('Remittance');
        $optHtml.="<option value = '3' ".$select.">".$type."</option>";
        if($tblName == '4'){$select =  'selected';}
        $type =$this->language->_('Local Remittance');
        $optHtml.="<option value = '4' ".$select.">".$type."</option>";
                

        echo $optHtml;
    }


      public function benefAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $ACBENEFArr = array();

//      if ($priviBeneLinkage == true)
//      {
            $select = $this->_db->select()
                                ->from(array('B'            => 'M_BENEFICIARY_USER'),
                                       array('ACBENEF_ID'   => 'B.BENEFICIARY_ID')
                                       )
                                ->join(array('A'=>'M_BENEFICIARY'), 'A.BENEFICIARY_ID = B.BENEFICIARY_ID',array('A.BENEFICIARY_ACCOUNT'))
                                ->where("B.CUST_ID  = ?" , (string)$this->_custIdLogin)
                                ->where("B.USER_ID  = ?" , (string)$this->_userIdLogin);
         // echo "<pre>";
     // echo $select->__toString();
//          die;
            $ACBENEFArr = $this->_db->fetchAll($select);
            // $ACBENEFArr = Application_Helper_Array::simpleArray($ACBENEFArr, "ACBENEF_ID");
            // print_r($ACBENEFArr);
            // bila empty, user bene linkage cannot see all payment
            if (empty($ACBENEFArr))
            {   $ACBENEFArr[] = "0";        }
                    
        // $tempColumn = $this->_db->fetchAll($select);
        // $opt[""] = "-- " .$this->language->_('Please Select'). " --";
        // $optPayStatus   = $opt + $optpayStatusRaw;
        // print_r($arrAccountRaw);die;
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        foreach ($ACBENEFArr as $key => $value) {
            if($tblName==$value['ACBENEF_ID']){
                $select = 'selected';
            }else{
                $select = '';
            }
            // $optHtml.="<option value='".$key."' ".$select.">".$row."</option>";
            $optHtml.="<option value='".$value['ACBENEF_ID']."' ".$select.">".$value['BENEFICIARY_ACCOUNT']."</option>";
        }
       
        
        // $optHtml.="<option value='USD'>USD</option>";
       

        echo $optHtml;
    }



    public function custAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $ACBENEFArr = array();

//      if ($priviBeneLinkage == true)
//      {
            $select = $this->_db->select()
                                ->from(array('B'            => 'M_CUSTOMER'),
                                       array('CUST_ID'   => 'B.CUST_ID')
                                       );

            $ACBENEFArr = $this->_db->fetchAll($select);
            
                    
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        foreach ($ACBENEFArr as $key => $value) {
            if($tblName==$value['CUST_ID']){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$value['CUST_ID']."' ".$select.">".$value['CUST_ID']."</option>";
        }

        echo $optHtml;
    }

    public function loantypeAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $loantypeArr = array(
            '1' => 'Term Loan',
            '2' => 'Demand Loan'
        );
            
                    
        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";
        foreach ($loantypeArr as $key => $value) {
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$key."' ".$select.">".$value."</option>";
        }

        echo $optHtml;
    }


    public function binstatusAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";

        // $ongoingStatusArr = array(
        //     1 => 'Ongoing',
        //     2 => 'Canceled'
        // );
 
        $arrStatus = array('1' => 'Active',
                            '2' => 'Closed',
                            '3' => 'Deleted'
                          );

        foreach($arrStatus as $key => $row){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$key."' ".$select.">".$row."</option>";
        }

        echo $optHtml;
    }

    public function countertypeAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";

        // $ongoingStatusArr = array(
        //     1 => 'Ongoing',
        //     2 => 'Canceled'
        // );

        $arrWaranty = array(
            1 => 'FC',
            2 => 'LF',
            3 => 'Insurance'
          );
 
        

        foreach($arrWaranty as $key => $row){
            if($tblName==$key){
                $select = 'selected';
            }else{
                $select = '';
            }
            $optHtml.="<option value='".$key."' ".$select.">".$row."</option>";
        }

        echo $optHtml;
    }

    
}
