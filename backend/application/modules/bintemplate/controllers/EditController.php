<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';
require_once 'Crypt/AESMYSQL.php';

class bintemplate_EditController extends bintemplate_Model_Bintemplate
{

    private $_binType;

    public function initController()
    {       
        $this->_binType = $this->_bin['type']['code'];
    }

    public function indexAction()
    {
        //pengaturan url untuk button back
        $this->setbackURL('/' . $this->_request->getModuleName() . '/index');
        $this->_helper->layout()->setLayout('newlayout');

        $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
        $password = $sessionNamespace->token; 
        $this->view->token = $sessionNamespace->token;


        $AESMYSQL = new Crypt_AESMYSQL();
        $temp_code = $AESMYSQL->decrypt($this->_getParam('temp_code'), $password);

        if ($this->_request->isPost() && $this->_getParam('submit') == $this->language->_('Submit')) {
            $temp_code = $this->_getParam('temp_code');
        }

        $this->view->temp_code = $temp_code;
        
        $resultdata = $this->getBinTemplate($temp_code);

        if (!empty($resultdata)) {

            $this->view->template_code   = $resultdata['TEMP_CODE'];
            $this->view->template_name = $resultdata['TEMP_NAME'];
            $this->view->template_description = $resultdata['TEMP_DESC'];


            $this->view->bin_type = $resultdata['BIN_TYPE'];
            $this->view->ex_time_type = $resultdata['EX_TIME_TYPE'];
            
            
            if($resultdata['BIN_TYPE'] == 1){
            $this->view->bin_type_text = 'eCollection';
            }else{
            $this->view->bin_type_text = 'VA Debit';
            }
            //ecollection
            $this->view->vptype = $resultdata['VA_TYPE'];
            $this->view->expiredtime = $resultdata['EXP_TIME'];
            $this->view->ex_time = $resultdata['EX_TIME'];
            $this->view->allowskip = $resultdata['SKIP_ERROR'];
            $this->view->partial_payment = $resultdata['PARTIAL_TYPE'];
            $this->view->billing_method = $resultdata['BILLING_TYPE'];
            $this->view->buyer_fee = $resultdata['BUYER_FEE'];
            $this->view->seller_fee = $resultdata['SELLER_FEE'];
            $this->view->vp_min = $resultdata['VP_MIN'];
            $this->view->vp_max = $resultdata['VP_MAX'];

            //vadebit
            $this->view->allowskip_debit = $resultdata['SKIP_ERROR'];
            $this->view->va_credit_max = $resultdata['VA_CREDIT_MAX'];
            $this->view->va_daily_limit = $resultdata['VA_DAILY_LIMIT'];
            $this->view->remaining_available_credit = $resultdata['REMAINING_AVAILABLE_CREDIT'];

            //vadebit
            if ($resultdata['BIN_TYPE'] == $this->_binType['vadebit'] && $resultdata['REMAINING_AVAILABLE_CREDIT'] == '1') {
                if ($resultdata['EX_TIME_TYPE'] == 2) {
                    $this->view->ex_time_weekly = $resultdata['EX_TIME'];
                }
                else if ($resultdata['EX_TIME_TYPE'] == 3) {
                    $this->view->ex_time_monthly = $resultdata['EX_TIME'];
                }
                else if ($resultdata['EX_TIME_TYPE'] == 4) {
                    $exTime = explode('_', $resultdata['EX_TIME']); 
                    $this->view->ex_time_yearly_date = $exTime[0];
                    $this->view->ex_time_yearly_month = $exTime[1];
                }
            }
        }

        $tempData = $this->_db->select()
            ->from(array('A' => 'TEMP_BIN_TEMPLATE'), array('*'))
            ->where('UPPER(A.TEMP_CODE)=' . $this->_db->quote((string) $temp_code))
            ->query()->fetch();

        $this->view->in_temp = false;
        if (!empty($tempData)) {
            $this->view->in_temp = true;
        }

        if ($this->_request->isPost() && $this->_getParam('submit') == $this->language->_('Submit')) {
            $this->view->inputParam = $this->getRequest()->getParams();
            $bin_type = $this->_request->getParams()['bin_type'];

            $filters = array(
                'template_code'           => array('StripTags', 'StringTrim', 'StringToUpper'),
                'template_name'         => array('StripTags', 'StringTrim'),
                'template_description'        => array('StripTags', 'StringTrim'),
                'bin_type'        => array('StripTags', 'StringTrim'),
                'ex_time'        => array('StripTags', 'StringTrim'),
            );

            if ($bin_type == $this->_binType['ecollection']) {
                $filters['expiredtime'] = array('StripTags', 'StringTrim');
                $filters['ex_time_type'] = array('StripTags', 'StringTrim');
                $filters['vptype'] = array('StripTags', 'StringTrim');
                $filters['partial_payment'] = array('StripTags', 'StringTrim');
                $filters['billing_method'] = array('StripTags', 'StringTrim');
                $filters['buyer_fee'] = array('StripTags', 'StringTrim');
                $filters['seller_fee'] = array('StripTags', 'StringTrim');
                $filters['vp_min'] = array('StripTags', 'StringTrim');
                $filters['vp_max'] = array('StripTags', 'StringTrim');
                $filters['allowskip'] = array('StripTags', 'StringTrim');
            }
            else if($bin_type == $this->_binType['vadebit']){
                $filters['allowskip_debit'] = array('StripTags', 'StringTrim');
                $filters['va_credit_max'] = array('StripTags', 'StringTrim');
                $filters['va_daily_limit'] = array('StripTags', 'StringTrim');
                $filters['remaining_available_credit'] = array('StripTags', 'StringTrim');
                $filters['ex_time_type_debit'] = array('StripTags', 'StringTrim');
            }

            if ($bin_type == $this->_binType['vadebit']) {
                $validators =  array(
                    'template_name'        => array(
                        'NotEmpty', 'messages' => array($this->language->_('Can not be empty'))
                    ),
                    'template_description'        => array(
                        'allowEmpty' => true,

                        'messages' => array()
                    ),
                    'va_credit_max'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
                    'va_daily_limit'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
                    'remaining_available_credit'                => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
                    'bin_type'        => array(
                        'allowEmpty' => true,

                        'messages' => array()
                    ),
                    'allowskip_debit' => array(
                        'allowEmpty' => true,

                        'messages' => array()
                    ),
                    'ex_time'        => array(
                        'allowEmpty' => true,

                        'messages' => array()
                    ),
                    'ex_time_type_debit'        => array(
                        'allowEmpty' => true,

                        'messages' => array()
                    ),
                );

            } else {
                $validators =  array(    
                    'template_name'        => array(
                        'NotEmpty', 'messages' => array($this->language->_('Can not be empty'))
                    ),
                    'template_description'        => array(
                        'allowEmpty' => true,

                        'messages' => array()
                    ),
                    'buyer_fee'        => array(
                        'allowEmpty' => true,

                        'messages' => array()
                    ),
                    'seller_fee'        => array(
                        'allowEmpty' => true,

                        'messages' => array()
                    ),

                    'ex_time'        => array(
                        'allowEmpty' => true,

                        'messages' => array()
                    ),
                    'ex_time_type'        => array(
                        'allowEmpty' => true,

                        'messages' => array()
                    ),

                    'vp_max'        => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
                    'vp_min'        => array('NotEmpty', 'messages' => array($this->language->_('Can not be empty'))),
                    'bin_type'        => array(),
                    'expiredtime'        => array(),
                    'vptype'             => array(),
                    'partial_payment'    => array(),

                    'billing_method'     => array(),
                    'allowskip'          => array(),
                );
            }

            $zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);

            if ($zf_filter_input->isValid()) {
                
                $info = "Edit BIN Template";
                $temp_data = $this->_tempData;
                foreach ($validators as $key => $value) {
                    if ($zf_filter_input->$key) $temp_data[strtoupper($key)] = $zf_filter_input->$key;
                }

                try {
                    $this->_db->beginTransaction();

                    $temp_data['TEMP_NAME']   = $zf_filter_input->template_name;
                    $temp_data['TEMP_DESC']   = $zf_filter_input->template_description;

                    //eCollection
                    if ($bin_type == $this->_binType['ecollection']) {
                        $temp_data['VA_TYPE']   = $zf_filter_input->vptype;

                        $temp_data['BIN_TYPE']   = $zf_filter_input->bin_type;

                        $temp_data['EXP_TIME']   = $zf_filter_input->expiredtime;
                        $temp_data['EX_TIME']   = $zf_filter_input->ex_time;
                        $temp_data['EX_TIME_TYPE']   = $zf_filter_input->ex_time_type;

                        $temp_data['PARTIAL_TYPE']   = $zf_filter_input->partial_payment;
                        $temp_data['BILLING_TYPE']   = $zf_filter_input->billing_method;

                        $buyer_fee =    Application_Helper_General::convertDisplayMoney($zf_filter_input->buyer_fee);
                        $seller_fee =   Application_Helper_General::convertDisplayMoney($zf_filter_input->seller_fee);
                        $vp_min =   Application_Helper_General::convertDisplayMoney($zf_filter_input->vp_min);
                        $vp_max =   Application_Helper_General::convertDisplayMoney($zf_filter_input->vp_max);
                        $temp_data['BUYER_FEE']   = $buyer_fee;
                        $temp_data['SELLER_FEE']   = $seller_fee;
                        if ($bin_type == $this->_binType['vadebit']) {
                            $temp_data['VP_MIN']   = 0;
                            $temp_data['VP_MAX']   = 0;
                        } else {
                            $temp_data['VP_MIN']   = $vp_min;
                            $temp_data['VP_MAX']   = $vp_max;
                        }

                        $temp_data['SKIP_ERROR']   = $zf_filter_input->allowskip;
                    }
                    // VA Debit
                    else {
                        $temp_data['SKIP_ERROR']   = $zf_filter_input->allowskip_debit;
                        $vaCreditMax =  Application_Helper_General::convertDisplayMoney($zf_filter_input->va_credit_max);
                        $vaDailyLimit = Application_Helper_General::convertDisplayMoney($zf_filter_input->va_daily_limit);
                        $temp_data['VA_CREDIT_MAX'] = $vaCreditMax;
                        $temp_data['VA_DAILY_LIMIT'] = $vaDailyLimit;
                        $temp_data['REMAINING_AVAILABLE_CREDIT'] = $zf_filter_input->remaining_available_credit;
                        $temp_data['EX_TIME_TYPE']   = $zf_filter_input->ex_time_type_debit;

                        // weekly
                        if ($zf_filter_input->ex_time_type_debit == '2') {
                            $ex_time = $this->_getParam('ex_time_weekly');
                        }
                        //monthly
                        else if($zf_filter_input->ex_time_type_debit == '3') {
                            $ex_time = $this->_getParam('ex_time_monthly');

                        }
                        //yearly
                        else if($zf_filter_input->ex_time_type_debit == '4'){
                            $ex_time = $this->_getParam('ex_time_yearly_date').'_'.$this->_getParam('ex_time_yearly_month');
                        }
                        //daily
                        else{
                            $ex_time = '';
                        }

                        $temp_data['EX_TIME']   = $ex_time;
                    }

                    $temp_data['TEMP_CREATED']    = $resultdata['TEMP_CREATED'];
                    $temp_data['TEMP_CREATEDBY']  = $resultdata['TEMP_CREATEDBY'];
                    $temp_data['TEMP_SUGGESTED']    = $resultdata['TEMP_SUGGESTED'];
                    $temp_data['TEMP_SUGGESTEDBY']  = $resultdata['TEMP_SUGGESTEDBY'];


                    $temp_data['TEMP_UPDATED']    = new Zend_Db_Expr('now()');
                    $temp_data['TEMP_UPDATEDBY']  = $this->_userIdLogin;

                    $temp_data = $this->resetData($temp_data);

                    // insert ke T_GLOBAL_CHANGES
                    $changeid = $this->suggestionWaitingApproval('Bin Template', $info, $this->_changeType['code']['edit'], null, 'M_CUSTOMER_BIN', 'TEMP_CUSTOMER_BIN', 'BIN TEMPLATE', $zf_filter_input->template_name);
                    $this->insertTempBinTemplateUpdate($changeid, $temp_data, $temp_code);

                    //log CRUD
                    Application_Helper_General::writeLog('BNUD', 'BIN Template has been updated (edit), Template Code : B' . str_pad($temp_code, 3, '0', STR_PAD_LEFT) . ' ,Change id : ' . $changeid);

                    $this->_db->commit();

                    $ns = new Zend_Session_Namespace('FVC');
                    $ns->backURL = '/bintemplate';

                    $this->_redirect('/notification/submited/index');
                    // var_dump("159");die;
                } catch (Exception $e) {
                    $this->_db->rollBack();
                    $error_remark = $this->language->_('Database Error');
                    // var_dump($e);die;
                }

                if (isset($error_remark)) {
                    $msg = $error_remark;
                    $class = 'F';
                } else {
                    $msg = 'Success';
                    $class = 'S';
                }

                $this->_helper->getHelper('FlashMessenger')->addMessage($class);
                $this->_helper->getHelper('FlashMessenger')->addMessage($msg);
                //           $this->_redirect($this->_backURL);

            } //END IF is VALID
            else {
                $this->view->error = 1;
                $this->view->template_code   = ($zf_filter_input->isValid('template_code')) ? $zf_filter_input->template_code : $this->_getParam('template_code');
                $this->view->template_name = ($zf_filter_input->isValid('template_name')) ? $zf_filter_input->template_name : $this->_getParam('template_name');
                $this->view->template_description = ($zf_filter_input->isValid('template_description')) ? $zf_filter_input->template_description : $this->_getParam('template_description');
                
                $this->view->vp_max   = ($zf_filter_input->isValid('vp_max')) ? $zf_filter_input->vp_max : $this->_getParam('vp_max');
                $this->view->vp_min   = ($zf_filter_input->isValid('vp_min')) ? $zf_filter_input->vp_min : $this->_getParam('vp_min');
                $this->view->seller_fee   = ($zf_filter_input->isValid('seller_fee')) ? $zf_filter_input->seller_fee : $this->_getParam('seller_fee');
                $this->view->buyer_fee   = ($zf_filter_input->isValid('buyer_fee')) ? $zf_filter_input->buyer_fee : $this->_getParam('buyer_fee');
                $this->view->partial_payment   = ($zf_filter_input->isValid('partial_payment')) ? $zf_filter_input->partial_payment : $this->_getParam('partial_payment');
                $this->view->billing_method   = ($zf_filter_input->isValid('billing_method')) ? $zf_filter_input->billing_method : $this->_getParam('billing_method');
                $this->view->vptype   = ($zf_filter_input->isValid('vptype')) ? $zf_filter_input->vptype : $this->_getParam('vptype');
                $this->view->expiredtime   = ($zf_filter_input->isValid('expiredtime')) ? $zf_filter_input->expiredtime : $this->_getParam('expiredtime');
                $this->view->ex_time   = ($zf_filter_input->isValid('ex_time')) ? $zf_filter_input->ex_time : $this->_getParam('ex_time');
                $this->view->ex_time_type   = ($zf_filter_input->isValid('ex_time_type')) ? $zf_filter_input->ex_time_type : $this->_getParam('ex_time_type');
                $this->view->allowskip   = ($zf_filter_input->isValid('allowskip')) ? $zf_filter_input->allowskip : $this->_getParam('allowskip');

                $this->view->bin_type = $bin_type;

                //va debit
                $this->view->allowskip_debit   = ($zf_filter_input->isValid('allowskip_debit')) ? $zf_filter_input->allowskip_debit : $this->_getParam('allowskip_debit');
                $this->view->va_credit_max   = ($zf_filter_input->isValid('va_credit_max')) ? $zf_filter_input->vacreditmax : $this->_getParam('va_credit_max');
                $this->view->va_daily_limit   = ($zf_filter_input->isValid('va_daily_limit')) ? $zf_filter_input->va_daily_limit : $this->_getParam('va_daily_limit');
                $this->view->remaining_available_credit   = ($zf_filter_input->isValid('remaining_available_credit')) ? $zf_filter_input->remaining_available_credit : $this->_getParam('remaining_available_credit');
                $this->view->ex_time_type_debit   = ($zf_filter_input->isValid('ex_time_type_debit')) ? $zf_filter_input->ex_time_type_debit : $this->_getParam('ex_time_type_debit');
                $this->view->ex_time_weekly   = ($zf_filter_input->isValid('ex_time_weekly')) ? $zf_filter_input->ex_time_weekly : $this->_getParam('ex_time_weekly');
                $this->view->ex_time_monthly   = ($zf_filter_input->isValid('ex_time_monthly')) ? $zf_filter_input->ex_time_monthly : $this->_getParam('ex_time_monthly');
                $this->view->ex_time_yearly_date  = ($zf_filter_input->isValid('ex_time_yearly_date')) ? $zf_filter_input->ex_time_yearly_date : $this->_getParam('ex_time_yearly_date');
                $this->view->ex_time_yearly_month  = ($zf_filter_input->isValid('ex_time_yearly_month')) ? $zf_filter_input->ex_time_yearly_month : $this->_getParam('ex_time_yearly_month');

                $error = $zf_filter_input->getMessages();
                $errorArray = null;
                foreach ($error as $keyRoot => $rowError) {
                    foreach ($rowError as $errorString) {
                        $errorArray[$keyRoot] = $errorString;
                    }
                }
                foreach ($error as $keyRoot => $rowError) {
                    foreach ($rowError as $errorString) {
                        $keyname = "error_" . $keyRoot;
                        // print_r($keyname);die;
                        $this->view->$keyname = $this->language->_($errorString);
                    }
                }

                $this->view->customer_msg  = $errorArray;
            }
        } // END if($this->_request->isPost())
    }

    public function getBinTemplate($temp_code)
    {
        $select = $this->_db->select()
            ->from(array('A' => 'BIN_TEMPLATE'), array('*'))
            ->where('UPPER(A.TEMP_CODE)=' . $this->_db->quote((string) $temp_code))
            ->query()->fetch();
        return $select;
    }

    public function resetData($cust_data){
        //ecollection
        if($cust_data['BIN_TYPE'] == $this->_binType['ecollection']){
            $cust_data['VA_CREDIT_MAX'] = '0';
            $cust_data['VA_DAILY_LIMIT'] = '0';
            $cust_data['REMAINING_AVAILABLE_CREDIT'] = '';
        }
        //vadebit
        else if($cust_data['BIN_TYPE'] == $this->_binType['vadebit']){
            $cust_data['VA_TYPE'] = '';
            $cust_data['EXP_TIME'] = '';
            $cust_data['PARTIAL_TYPE'] = '';
            $cust_data['BILLING_TYPE'] = '';
            $cust_data['BUYER_FEE'] = '0';
            $cust_data['SELLER_FEE'] = '0';
            $cust_data['VP_MIN'] = '0';
            $cust_data['VP_MAX'] = '0';
        }

        return $cust_data;
    }
}
