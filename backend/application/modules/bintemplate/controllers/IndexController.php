<?php

require_once 'Zend/Controller/Action.php';

class bintemplate_IndexController extends bintemplate_Model_Bintemplate
{

    private $_binType;

    public function initController()
    {

        //format display date
        $this->view->dateDisplayFormat = $this->_dateDisplayFormat;

        $this->view->binTypeArr = array_combine($this->_bin['type']['code'], $this->_bin['type']['desc']);
    }
    public function indexAction()
    {
        $setting = new Settings();          
        $enc_pass = $setting->getSetting('enc_pass');
        $enc_salt = $setting->getSetting('enc_salt');
        $sessionNamespace = new Zend_Session_Namespace('Tokenenc');      
        $pw_hash = md5($enc_salt.$enc_pass);
        $rand = $this->_userIdLogin.date('dHis').$pw_hash;
        $sessionNamespace->token  = $rand;
        $this->view->token = $sessionNamespace->token;
        
        $this->_helper->layout()->setLayout('newlayout'); 

        $fields = array(
            'type' => array(
                'field'    => 'BIN_TYPE',
                'label'    => $this->language->_('Type'),
                'sortable' => true
            ),
            'company' => array(
                'field'    => 'TEMP_CODE',
                'label'    => $this->language->_('Template Code'),
                'sortable' => true
            ),

            'bin'     => array(
                'field'    => 'TEMP_NAME',
                'label'    => $this->language->_('Template Name'),
                'sortable' => true
            ),

            'status'   => array(
                'field'    => 'TEMP_DESC',
                'label'    => $this->language->_('Description'),
                'sortable' => true
            ),

            'latestSuggestion'     => array(
                'field'    => 'BIN_SUGGESTED',
                'label'    => $this->language->_('Last Suggested'),
                'sortable' => true
            ),

            'latestApproval'    => array(
                'field'  => 'BIN_UPDATED',
                'label'    => $this->language->_('Last Approved'),
                'sortable' => true
            )
        );

        //validasi sort, jika input sort bukan ASC atau DESC
        $sortBy  = $this->_getParam('sortby');
        $sortBy  = (Zend_Validate::is($sortBy, 'InArray', array(array_keys($fields)))) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
        $sortDir = $this->_getParam('sortdir');
        $sortDir = (Zend_Validate::is($sortDir, 'InArray', array('haystack' => array('asc', 'desc')))) ? $sortDir : 'asc';

        $this->view->sortBy = $sortBy;
        $this->view->sortDir = $sortDir;
        $this->view->fields = $fields;

        // proses pengambilan data filter,display all,sorting
        $select = array();
        $select = $this->_db->select()
            ->from(array('a' => 'BIN_TEMPLATE'), array('BIN_TYPE','TEMP_CODE', 'TEMP_NAME', 'TEMP_DESC', 'TEMP_SUGGESTED', 'TEMP_SUGGESTEDBY', 'TEMP_UPDATED', 'TEMP_UPDATEDBY'))
            ->order(array('TEMP_CODE ASC'))
            ->query()->fetchAll();
        $this->paging($select);
    }
}
