<?php

require_once 'General/Settings.php';

Class fprivilagetemplate_Model_Fprivilagetemplate 
{

	protected $_db;
	// constructor
	public function __construct()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
	}
	
	public function getAllPrivilege()
	{
		$result = $this->_db->select()
							 ->FROM ('M_FPRIVILEGE');

		$setting = new Settings();
	$type = $setting->getSetting('system_type');
 // var_dump($type);
      if($type=='1'){
        $result->where('FPRIVI_MODE IN (0,1)');
      }else if($type=='2'){
        $result->where('FPRIVI_MODE IN (0,2)');
      }
							 
		$result->order('FPRIVI_DESC ASC');
    
  //echo $select;
      $result = $result->query()->fetchAll();
		
		return $result;
	}

	public function getTemplatedesc($ftemplateid)
	{
		$result = $this->_db->FETCHONE($this->_db->select()
					 ->FROM ('M_FTEMPLATE',array('FTEMPLATE_DESC','FTEMPLATE_GROUP','FTEMPLATE_SECURE'))
					 ->WHERE('FTEMPLATE_ID = ?',$ftemplateid));	
		return $result;
	}

	public function getTemplate($ftemplateid)
	{
		$result = $this->_db->FetchRow($this->_db->select()
					 ->FROM ('M_FTEMPLATE',array('FTEMPLATE_DESC','FTEMPLATE_GROUP','FTEMPLATE_SECURE'))
					 ->WHERE('FTEMPLATE_ID = ?',$ftemplateid));	
		return $result;
	}

	

	public function getFpriviltem($ftemplateid)
	{
		$result = $this->_db->select()
							 ->FROM (array('A' => 'M_FPRIVILEGE_TEMPLATE'), array('A.FPRIVI_ID'))
							 ->joinLeft(array('MB' => 'M_FPRIVILEGE'), 'MB.FPRIVI_ID = A.FPRIVI_ID', array('MB.FPRIVI_DESC'))
						     ->WHERE('A.FTEMPLATE_ID = ?',$ftemplateid)
						     ->query()->fetchall();
		return $result;
	}

	public function getModuleDescArr()
    {  
        $select = $this->_db->select()
  	                       ->from('M_MODULE');
  	    $setting = new Settings();
		$type = $setting->getSetting('system_type');
	 // var_dump($type);
	      if($type=='1'){
	        $select->where('MODULE_OPEN IN (0,1)');
	      }else if($type=='2'){
	        $select->where('MODULE_OPEN IN (0,2)');
	      }
								 
			//$result->order('FPRIVI_DESC ASC');
	    
	  //echo $select;
	      $select = $select->query()->fetchAll();
  	                       //->query()->fetchAll();

  	    $module_desc = array();                   
  	    foreach($select as $row)
  	    {
  	        $module_desc[$row['MODULE_ID']] = $row['MODULE_DESC'];
  	    }     
  	      
  	    return $module_desc;
    }
}

?>