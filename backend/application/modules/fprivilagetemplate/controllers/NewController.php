<?php
require_once 'Zend/Controller/Action.php';

class fprivilagetemplate_NewController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$modelPrivil = new fprivilagetemplate_Model_Fprivilagetemplate();

		if (!$this->view->hasPrivilege("PTAD")) {
			return $this->_redirect("/fprivilagetemplate");
		}

		$privil = $modelPrivil->getAllPrivilege();
		$module = $modelPrivil->getModuleDescArr();
		$this->view->sortingGroup = $module;

		$arrType = array("" => "---" . $this->language->_('Please Select') . "---", 'F' => "Frontend", 'B' => "Backend");
		$arrSecure = array("" => "---" . $this->language->_('Please Select') . "---", 0 => "No", 1 => "Yes");

		$this->view->isbybo = $arrType;
		$this->view->secure = $arrSecure;


		foreach ($privil as $row) {
			$moduleID = $module[$row['FPRIVI_MODULEID']];
			unset($row['FPRIVI_MODULEID']);
			$privilGroup[$moduleID][] = $row;
		}
		// ksort($privilGroup);
		// $privilGroup['No Group'] = $privilGroup[''];

		unset($privilGroup['']);
		$filterArr = array(
			'tmplcode'    => array('StripTags', 'StringTrim', 'StringToUpper'),
			'tmplname'  => array('StripTags', 'StringTrim'),
			// 'tmplgroup'  => array('StripTags', 'StringTrim'),
			// 'tmplsecurity'  => array('StripTags', 'StringTrim'),
		);
		$validators = array(
			'tmplcode' => array(
				'NotEmpty',
				'Alnum',
				array('StringLength', array('min' => 3, 'max' => 3)),
				array('Db_NoRecordExists', array('table' => 'M_FTEMPLATE', 'field' => 'FTEMPLATE_ID')),
				'messages' => array(
					$this->language->_('Can not be empty, Template Code length must not be more than 3'),
					$this->language->_('Must be alphabet or numeric values'),
					$this->language->_('Template Code length must be 3'),
					$this->language->_('Template Code already in use. Please use another'),
				)
			),
			'tmplname' => array(
				'NotEmpty',
				// 'Alnum',
				// array('StringLength', array('min' => 1, 'max' => 128)),
				array('Db_NoRecordExists', array('table' => 'M_FTEMPLATE', 'field' => 'FTEMPLATE_DESC')),
				'messages' => array(
					$this->language->_('Can not be empty'),
					// $this->language->_('Must be alphabet or numeric values'),
					// $this->language->_('Template Code length must not be more than 128'),
					$this->language->_('Template Name already in use. Please use another'),
				)
			),
			// 'tmplgroup' => array(
			// 	'NotEmpty',
			// 	//	array('StringLength',array('min'=>1,'max'=>128)),
			// 	'messages' => array(
			// 		$this->language->_('Can not be empty'),
			// 		//			$this->language->_('Must be alphabet or numeric values'),
			// 		//			$this->language->_('Template Code length must not be more than 128')
			// 	)
			// ),
			// 'tmplsecurity' => array(
			// 	'NotEmpty',
			// 	//	array('StringLength',array('min'=>1,'max'=>128)),
			// 	'messages' => array(
			// 		$this->language->_('Can not be empty'),
			// 		//			$this->language->_('Must be alphabet or numeric values'),
			// 		//			$this->language->_('Template Code length must not be more than 128')
			// 	)
			// )
		);

		$zf_filters = new Zend_Filter_Input($filterArr, $validators, $this->_request->getParams());

		$re = '%^[a-zA-Z0-9+\- ]*$%';
		$cekspecialchar = preg_match($re, $zf_filters->tmplname) ? 1 : 0;

		if ($this->_request->isPost()) {
			$cekcheckbox = 0;
			// var_dump($this->_request->getParams());die;

			foreach ($privil as $row) {
				if ($this->_getParam($row['FPRIVI_ID']) == 1) {
					$cekcheckbox++;
					$this->view->$row['FPRIVI_ID'] = $this->_getParam($row['FPRIVI_ID']);
				}
			}
			if ($cekcheckbox == 0) {
				$this->view->errcheckbox = $this->language->_('Please Select at least 1 Privilege');
			}

			if ($zf_filters->isValid() && $cekspecialchar && $cekcheckbox != 0) {
				$cekcheckbox = 0;
				$this->_db->beginTransaction();
				try {
					foreach ($privil as $row) {
						if ($this->_getParam($row['FPRIVI_ID']) == 1) {
							$cekcheckbox++;
							$data = array(
								'FPRIVI_ID' => $row['FPRIVI_ID'],
								'FPRIVI_DESC' => $row['FPRIVI_DESC'],
								'FTEMPLATE_ID' => $zf_filters->tmplcode
								// 'FTEMPLATE_GROUP' => 'B'
							);
							$this->_db->insert('M_FPRIVILEGE_TEMPLATE', $data);
						}
					}

					if ($cekcheckbox == 0) {
						$this->_db->rollBack();
					} else {

						$data = array(
							'FTEMPLATE_ID' => $zf_filters->tmplcode,
							'FTEMPLATE_DESC' => $zf_filters->tmplname,
							'FTEMPLATE_GROUP' => $zf_filters->tmplgroup,
							// 'FTEMPLATE_SECURE' => $zf_filters->tmplsecurity,
						);
						$this->_db->insert('M_FTEMPLATE', $data);

						$this->_db->commit();
						Application_Helper_General::writeLog('PTAD', 'Add Frontend Privilege Template. Template Code :[' . $zf_filters->tmplcode . '], Template Name : [' . $zf_filters->tmplname . ']');
						$this->setbackURL('/fprivilagetemplate');
						$this->_redirect('/notification/success/index');
					}
				} catch (Exception $e) {
					var_dump($e);
					die;
					$this->_db->rollBack();
				}
			} else {
				if (!$cekspecialchar) {
					$this->view->xtmplname = 'Must not contain special characters';
					$zf_filters->tmplname = '';
				}
				foreach ($zf_filters->getMessages() as $key => $err) {
					$xxx = 'x' . $key;
					$this->view->$xxx = $this->displayError($err);
				}
			}
			$this->view->tmplcode = ($zf_filters->isValid()) ? $zf_filters->tmplcode : $this->_getParam('tmplcode');
			$this->view->tmplname = ($zf_filters->isValid() && $cekspecialchar) ? $zf_filters->tmplname : $this->_getParam('tmplname');
			$this->view->tmplgroup = ($zf_filters->isValid()) ? $zf_filters->tmplgroup : $this->_getParam('tmplgroup');
			$this->view->tmplsecurity = ($zf_filters->isValid()) ? $zf_filters->tmplsecurity : $this->_getParam('tmplsecurity');
			foreach ($privil as $row) {
				$check = $row['FPRIVI_ID'] . "check";
				$this->view->$check = ($zf_filters->isValid()) ? $zf_filters->$row['FPRIVI_ID'] : $this->_getParam($row['FPRIVI_ID']);
			}
		}
		$this->view->privilGroup = $privilGroup;
		$this->view->modulename = $this->_request->getModuleName();
	}
}
