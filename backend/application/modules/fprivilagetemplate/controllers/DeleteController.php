<?php
require_once 'Zend/Controller/Action.php';

class fprivilagetemplate_DeleteController extends Application_Main  
{
	public function indexAction() 
	{
		$ftemplateid = $this->_getParam('ftempid');
				
		$this->_db->beginTransaction();
		try{
			//DELETE DATA BEFORE EDIT
			$where = array('FTEMPLATE_ID = ?' => $ftemplateid);
			$this->_db->delete('M_FPRIVILEGE_TEMPLATE',$where);
			
			//DELETE DATA BEFORE EDIT
			$where = array('FTEMPLATE_ID = ?' => $ftemplateid);
			$this->_db->delete('M_FTEMPLATE',$where);
	
			$this->_db->commit();
			Application_Helper_General::writeLog('PTUD','Delete Frontend Privilege Template. Template Code : ['.$ftemplateid.']');
			$this->setbackURL('/fprivilagetemplate');
			$this->_redirect('/notification/success/index');
		}
		catch(Exception $e)
		{
			$this->_db->rollBack();					
		} 			
	}
}
