<?php

require_once 'Zend/Controller/Action.php';

class binreport_StatementController extends Application_Main
{


    public function indexAction()
    {
        $this->_helper->layout()->setLayout('newlayout');
        $fields = array(
            'customer'     => array(
                'field'    => 'CUST_CITY',
                'label'    => $this->language->_('Customer'),
                'sortable' => true
            ),

            'bin_va'   => array(
                'field'    => 'CUST_STATUS',
                'label'    => $this->language->_('BIN - VA Number'),
                'sortable' => true
            ),

            'created_date'     => array(
                'field'    => 'BIN_SUGGESTED',
                'label'    => $this->language->_('Created Date'),
                'sortable' => true
            ),

            'last_uploaded'    => array(
                'field'  => 'BIN_UPDATED',
                'label'    => $this->language->_('Last Uploaded'),
                'sortable' => true
            ),
            'ccy'    => array(
                'field'  => 'BIN_UPDATED',
                'label'    => $this->language->_('CCY'),
                'sortable' => true
            ),
            'document_amount'    => array(
                'field'  => 'BIN_UPDATED',
                'label'    => $this->language->_('Document Amount'),
                'sortable' => true
            ),
            'payment_amount'    => array(
                'field'  => 'BIN_UPDATED',
                'label'    => $this->language->_('Payment Amount'),
                'sortable' => true
            ),
            'invalid_amount'    => array(
                'field'  => 'BIN_UPDATED',
                'label'    => $this->language->_('Invalid Amount'),
                'sortable' => true
            ),
            'balance'    => array(
                'field'  => 'BIN_UPDATED',
                'label'    => $this->language->_('Balance'),
                'sortable' => true
            ),
            'status'    => array(
                'field'  => 'BIN_UPDATED',
                'label'    => $this->language->_('Status'),
                'sortable' => true
            )
        );

        //validasi sort, jika input sort bukan ASC atau DESC
        $sortBy  = $this->_getParam('sortby');
        $sortBy  = (Zend_Validate::is($sortBy, 'InArray', array(array_keys($fields)))) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
        $sortDir = $this->_getParam('sortdir');
        $sortDir = (Zend_Validate::is($sortDir, 'InArray', array('transaction_time' => array('asc', 'desc')))) ? $sortDir : 'asc';

        $this->view->currentPage = $page;
        $this->view->sortBy = $sortBy;
        $this->view->sortDir = $sortDir;
        $this->view->fields = $fields;
    }
}
