<?php

require_once 'Zend/Controller/Action.php';

class binreport_SummaryController extends Application_Main
{


    public function indexAction()
    {
        $this->_helper->layout()->setLayout('newlayout');
        $fields = array(
            'summary'     => array(
                'field'    => 'CUST_CITY',
                'label'    => $this->language->_('Summary'),
                'sortable' => true
            ),

            'total_invoice'   => array(
                'field'    => 'CUST_STATUS',
                'label'    => $this->language->_('Total Invoice'),
                'sortable' => true
            ),

            'ccy'    => array(
                'field'  => 'BIN_UPDATED',
                'label'    => $this->language->_('CCY'),
                'sortable' => true
            ),
            'total_amount'   => array(
                'field'    => 'CUST_STATUS',
                'label'    => $this->language->_('Total Amount'),
                'sortable' => true
            )
        );

        //validasi sort, jika input sort bukan ASC atau DESC
        $sortBy  = $this->_getParam('sortby');
        $sortBy  = (Zend_Validate::is($sortBy, 'InArray', array(array_keys($fields)))) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
        $sortDir = $this->_getParam('sortdir');
        $sortDir = (Zend_Validate::is($sortDir, 'InArray', array('transaction_time' => array('asc', 'desc')))) ? $sortDir : 'asc';

        $this->view->currentPage = $page;
        $this->view->sortBy = $sortBy;
        $this->view->sortDir = $sortDir;
        $this->view->fields = $fields;
    }
}
