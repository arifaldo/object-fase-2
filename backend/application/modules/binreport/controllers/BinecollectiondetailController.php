<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class binreport_BinecollectiondetailController extends Application_Main
{

    public function indexAction() 
    {
      $this->_helper->layout()->setLayout('newlayout');
      // die('here');
      $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
      $password = $sessionNamespace->token; 
      $this->view->token = $sessionNamespace->token;


      $AESMYSQL = new Crypt_AESMYSQL();
      $cust_id = $AESMYSQL->decrypt($this->_getParam('cust_id'), $password);
      $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;

      $cust_bin = strtoupper($AESMYSQL->decrypt($this->_getParam('cust_bin'), $password));
      $va_number = strtoupper($AESMYSQL->decrypt($this->_getParam('va_number'), $password));

       //pengaturan url untuk button back
	    $this->setbackURL('/'.$this->_request->getModuleName().'/view/index/cust_id/'.$this->_getParam('cust_id').'/cust_bin/'.$this->_getParam('cust_bin').'/va_number/'.$this->_getParam('va_number')); 

      $vaStatusArr = array(
        '1' => 'Open',
        '2' => 'Close',
        '3' => 'Deleted'
      );

      $this->view->vaStatusArr = $vaStatusArr;
       
      if($cust_id && $cust_bin && $va_number)
      {
          //---------------------------------------va transaction data-----------------------------------------------
          $select = $this->_db->select()
            ->from(array('T' => 'T_VA_TRANSACTION'), array(
                                                          'T.*',
                                                           'DOCSTATUS' => new Zend_Db_Expr('CASE
                                                                                                WHEN DOC_STATUS = 0 THEN "In Billing"
                                                                                                WHEN DOC_STATUS = 1 THEN "Pending Future Date"
                                                                                                WHEN DOC_STATUS = 2 THEN "Settled"
                                                                                                WHEN DOC_STATUS = 3 THEN "Unsettled"
                                                                                                WHEN DOC_STATUS = 4 THEN "Cancelled"
                                                                                                WHEN EXP_DATE < "'.date('Y-m-d H:i:s').'" THEN "Unsettled"
                                                                                                WHEN VA_STATUS = 3 THEN "-"
                                                                                              END
                                                                                            ')
                                                    ))
            ->join(array('B' => 'M_CUSTOMER_BIN'), 'T.CUST_BIN = B.CUST_BIN', array('B.VA_TYPE'))
            ->join(array('C' => 'T_VA_TRANSACTION_CREDIT'), 'T.VA_NUMBER = C.VA_NUMBER', array('C.AVAILABLE_CREDIT', 'C.CREDIT_SETTLEMENT'))
            ->where('B.CUST_ID = ?', (string)$cust_id)
            ->where('T.CUST_ID = ?', (string)$cust_id)
            ->where('C.CUST_ID = ?', (string)$cust_id)
            ->where('T.CUST_BIN = ?', (string)$cust_bin)
            ->where('C.CUST_BIN = ?', (string)$cust_bin)
            ->where('T.VA_NUMBER = ?', (string)$va_number)
            ->where('C.VA_NUMBER = ?', (string)$va_number);

          $data = $this->_db->fetchAll($select);
          $this->view->data = $data;

          // echo "<pre>";
          // print_r($data);die();

          $totalBilling = 0;
          $totalPaid = 0;
          $totalSettledWRemarks = 0;
          $totalUnSettled = 0;
          $totalInBilling = 0;

          $invoiceSettled = 0;
          $invoiceSettledWRemarks = 0;
          $invoiceUnsettled = 0;
          $invoiceInbilling = 0;
          $invoiceFutureDate = 0;

          $settledWRemarksArr = array();

          foreach ($data as $key => $value) {
            $totalBilling += $value['IN_BILLING'];
            $totalPaid += $value['SETTLED_AMOUNT'];

            if ($value['DOC_STATUS'] == '2' && !empty($value['REMARKS'])) {
              array_push($settledWRemarksArr, $value);
              $totalSettledWRemarks += $value['SETTLED_AMOUNT'];
              $invoiceSettledWRemarks++;
            }
            else if ($value['DOC_STATUS'] == '2') {
              $invoiceSettled++;
            }
            else if ($value['DOC_STATUS'] == '3') {
              $invoiceUnsettled++;
            }
            else if($value['DOC_STATUS'] == '0'){
              $invoiceInbilling++;
            }
            else if($value['DOC_STATUS'] == '1'){
              $invoiceFutureDate++;
            }

            $totalUnSettled += $value['UNSETTLED_AMOUNT'];
            $totalInBilling += $value['IN_BILLING'];
          }

          $this->view->totalBilling = $totalBilling;
          $this->view->totalPaid    = $totalPaid;
          $this->view->totalSettledWRemarks = $totalSettledWRemarks;
          $this->view->totalUnSettled = $totalUnSettled;
          $this->view->totalInBilling = $totalInBilling;

          $this->view->settledWRemarksArr = $settledWRemarksArr;

          $this->view->invoiceSettled = $invoiceSettled;
          $this->view->invoiceSettledWRemarks = $invoiceSettledWRemarks;
          $this->view->invoiceUnsettled = $invoiceUnsettled;
          $this->view->invoiceInbilling = $invoiceInbilling;
          $this->view->invoiceFutureDate = $invoiceFutureDate;



          //---------------------------------------Payment History-----------------------------------------------
          $select = $this->_db->select()
            ->from(array('D' => 'T_VA_TRANSACTION_DETAIL'), array('D.*'));
          $select->join(array('T' => 'T_VA_TRANSACTION'), 'D.VA_ID = T.VA_ID', array('T.CUST_BIN', 'T.VA_NUMBER'));
          $select->where('T.CUST_ID = ?', (string)$cust_id);
          $select->where('T.CUST_BIN = ?', (string)$cust_bin);
          $select->where('T.VA_NUMBER = ?', (string)$va_number);

          $historyData = $this->_db->fetchAll($select);
          $this->view->historyData = $historyData;
      }
      else{
        $this->_helper->getHelper('FlashMessenger')->addMessage("F");
        $this->_helper->getHelper('FlashMessenger')->addMessage($this->language->_('Error') . ": " . $this->language->_('VA not valid') . ".");
        $this->_redirect('/' . $this->view->modulename . '/binecollection/index');
      }
   }	

  public function availabletosettledAction(){
    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout()->disableLayout();

    $cust_bin = $this->_getParam('cust_bin');
    $va_number = $this->_getParam('va_number');
    $availablecredit = $this->_getParam('availablecredit');
    $note = $this->_getParam('note');

    $creditData = $this->_db->fetchRow(
                      $this->_db->select()
                        ->from(array('B' => 'T_VA_TRANSACTION_CREDIT'),array('B.CREDIT_SETTLEMENT'))
                        ->where('B.CUST_ID = ?', (string)$cust_id)
                        ->where('B.CUST_BIN = ?', (string)$cust_bin)
                        ->where('B.VA_NUMBER = ?', (string)$va_number)
                );

    $creditSettlement = $creditData['CREDIT_SETTLEMENT'];
    
    $creditSettlement += $availablecredit;

    try{
      $where['CUST_ID = ?'] = $cust_id;
      $where['CUST_BIN = ?'] = $cust_bin;
      $where['VA_NUMBER = ?'] = $va_number;
      $updateArr = array('AVAILABLE_CREDIT' => 0, 'CREDIT_SETTLEMENT' => $creditSettlement);
      $this->_db->update('T_VA_TRANSACTION_CREDIT',$updateArr,$where);

      //insert history
      $historyArr = array(
        'DATE_TIME' => new Zend_Db_Expr("now()"),
        'CUST_ID' => $cust_id,
        'USER_ID' => $this->_userIdLogin,
        'CUST_BIN' => $cust_bin,
        'VA_NUMBER' => $va_number,
        'AMOUNT' => $availablecredit,
        'REMARKS' => $note
      );

      $this->_db->insert('T_VA_TRANSACTION_CREDIT_HISTORY', $historyArr);  

      echo 'success';
    }
    catch(Exception $e){
      echo 'failed'; 
    }
  }

  public function vacredithistoryAction(){
    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout()->disableLayout();

    $cust_bin = $this->_getParam('cust_bin');
    $va_number = $this->_getParam('va_number');

    $historyData = $this->_db->fetchAll(
                      $this->_db->select()
                        ->from(array('B' => 'T_VA_TRANSACTION_CREDIT_HISTORY'),array('*'))
                        ->join(array('U' => 'M_USER'), 'B.USER_ID = U.USER_ID', array('U.USER_FULLNAME'))
                        ->where('U.CUST_ID = ?', (string)$cust_id)
                        ->where('B.CUST_ID = ?', (string)$cust_id)
                        ->where('B.CUST_BIN = ?', (string)$cust_bin)
                        ->where('B.VA_NUMBER = ?', (string)$va_number)
                );

    $html = '';
    if (!empty($historyData)) {
       foreach ($historyData as $key => $value) {

        $updateTime = Application_Helper_General::convertDate($value['DATE_TIME'], $this->displayDateTimeFormat, $this->defaultDateFormat);

        $html .= '<tr>
                    <td>'.$value['USER_FULLNAME'].'</td>
                    <td>'.$updateTime.'</td>
                    <td>IDR</td>
                    <td>'.Application_Helper_General::displayMoney($value['AMOUNT']).'</td>
                    <td>'.$value['REMARKS'].'</td>
                  </tr>';
      }
    }
    else{
      $html .= '<tr>
                  <td colspan="5" align="center" class="homeCardText">------------- No Data -------------</td>
                </tr>';
    }

    echo $html;
   
  }

  public function vadetailAction()
  {
    $this->_helper->layout()->setLayout('newpopup');
    
    $cust_id = $this->_getParam('cust_id');
    $cust_bin = $this->_getParam('cust_bin');
    $va_id = $this->_getParam('va_id');
    $type = $this->_getParam('type'); //static or dynamic
    $this->view->type = $type;

    $select = $this->_db->select()
          ->from(array('T' => 'T_VA_TRANSACTION'), array(         
                                                    'T.CUST_BIN',
                                                    'T.CUST_ID',
                                                    'VA_NAME',
                                                    'VA_NUMBER'           => new Zend_Db_Expr('CONCAT(T.CUST_BIN, "-", T.VA_NUMBER)'),
                                                    'VANUMBER'            => 'T.VA_NUMBER',
                                                    'T.VA_ID',
                                                    'T.MESSAGE',
                                                    'T.EF_DATE',
                                                    'T.EXP_DATE',
                                                    'T.CCY',
                                                    'T.INVOICE_AMOUNT',
                                                    'T.SETTLED_AMOUNT',
                                                    'DOCSTATUS'            => 'T.DOC_STATUS',
                                                    'DOC_STATUS'           => new Zend_Db_Expr('CASE
                                                                              WHEN T.DOC_STATUS = 0 THEN "In Billing"
                                                                              WHEN T.DOC_STATUS = 1 THEN "Pending Future Date"
                                                                              WHEN T.DOC_STATUS = 2 THEN "Settled"
                                                                              WHEN T.DOC_STATUS = 3 THEN "Unsettled"
                                                                              WHEN T.DOC_STATUS = 4 THEN "Cancelled"
                                                                              WHEN T.EXP_DATE < "'.date('Y-m-d H:i:s').'" THEN "Unsettled"
                                                                              WHEN T.VA_STATUS = 3 THEN "-"
                                                                            END
                                                                           '),
                                                    'T.REMARKS',
                                                    'T.SETTLED_AMOUNT',
                                                    'T.UNSETTLED_AMOUNT',
                                                    'SETTLED_WITH_REMARK' => new Zend_Db_Expr('CASE
                                                                                                WHEN T.DOC_STATUS = 2 AND T.REMARKS IS NOT NULL THEN T.SETTLED_AMOUNT
                                                                                                ELSE "0"
                                                                                              END'),
                                                    'T.IN_BILLING'
                                                 ))
          ->where('T.CUST_BIN = ?', (string)$cust_bin)
          ->where('T.CUST_ID = ?', (string)$cust_id);

    if ($type == 'static') {
        $select->where('T.VA_ID = ?', (string)$va_id);
    }
    else{
      $select->where('T.VA_NUMBER = ?', (string)$va_id);
    }

    $data = $this->_db->fetchRow($select);

    $this->view->data = $data;

    //history data
    $select = $this->_db->select()
                    ->from(array('T' => 'T_VA_TRANSACTION'), array())
                    ->join(array('H' => 'T_VA_TRANSACTION_HISTORY'), 'T.VA_TRANSACTION_ID = H.VA_TRANSACTION_ID', array(
                                                                                                                      '*',
                                                                                                                      'HISTORYSTATUS' => new Zend_Db_Expr('CASE
                                                                                                                          WHEN H.HISTORY_STATUS = 1 THEN "Create"
                                                                                                                          WHEN H.HISTORY_STATUS = 2 THEN "Overwrite"
                                                                                                                          WHEN H.HISTORY_STATUS = 3 THEN "Cancel"
                                                                                                                          WHEN H.HISTORY_STATUS = 4 THEN "Expired"
                                                                                                                        END
                                                                                                                       ')
                                                                                                                  ))
                    ->join(array('U' => 'M_USER'), 'H.USER_ID = U.USER_ID', array('U.USER_FULLNAME'))
                    ->where('U.CUST_ID = ?', (string)$cust_id)
                    ->where('T.CUST_ID = ?', (string)$cust_id)
                    ->where('T.CUST_BIN = ?', (string)$cust_bin);

    if ($type == 'static') {
        $select->where('T.VA_ID = ?', (string)$va_id);
    }
    else{
      $select->where('T.VA_NUMBER = ?', (string)$va_id);
    }

    $historyData = $this->_db->fetchAll($select);

    $this->view->historyData = $historyData;

  }

  //------------------------------------------------overwrite props------------------------------------------------------------
  public function getvadataAction(){
    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout()->disableLayout();

    $cust_bin = $this->_getParam('cust_bin');
    $va_number = $this->_getParam('va_number');
    $va_id = $this->_getParam('va_id');
    $type = $this->_getParam('type');

    $select = $this->_db->select()
                    ->from(array('T' => 'T_VA_TRANSACTION'),array('*'))
                    ->where('T.CUST_ID = ?', (string)$cust_id)
                    ->where('T.CUST_BIN = ?', (string)$cust_bin);

    if ($type == 'static') {
        $select->where('T.VA_ID = ?', (string)$va_id);
    }
    else{
      $select->where('T.VA_NUMBER = ?', (string)$va_number);
    }

    $data = $this->_db->fetchRow($select);

    echo json_encode($data);
   
  }

  public function overwriteAction(){
    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout()->disableLayout();

    $cust_bin = $this->_getParam('cust_bin');
    $va_number = $this->_getParam('va_number');
    $va_id = $this->_getParam('va_id');
    $type = $this->_getParam('type');

    $message = $this->_getParam('message');
    $exp_date = $this->_getParam('exp_date');
    $exp_time = $this->_getParam('exp_time');
    $invoice_amount = $this->_getParam('invoice_amount');

    if ($type == 'static') {
      //insert history
      $transactionData = $this->_db->fetchRow(
              $this->_db->select()
                        ->from(array('B' => 'T_VA_TRANSACTION'),array('B.VA_TRANSACTION_ID'))
                        ->where('B.CUST_ID = ?', (string)$cust_id)
                        ->where('B.CUST_BIN = ?', (string)$cust_bin)
                        ->where('B.VA_ID = ?', (string)$va_id)
      );

      $where['VA_ID = ?'] = $va_id;
    }
    else{
      //insert history
      $transactionData = $this->_db->fetchRow(
            $this->_db->select()
                      ->from(array('B' => 'T_VA_TRANSACTION'),array('B.VA_TRANSACTION_ID'))
                      ->where('B.CUST_ID = ?', (string)$cust_id)
                      ->where('B.CUST_BIN = ?', (string)$cust_bin)
                      ->where('B.VA_NUMBER = ?', (string)$va_number)
      );

      $where['VA_NUMBER = ?'] = $va_number;
    }

    $vaTransactionId = $transactionData['VA_TRANSACTION_ID'];

    try{
      $where['CUST_ID = ?'] = $cust_id;
      $where['CUST_BIN = ?'] = $cust_bin;

      $updateArr = array(
                      'MESSAGE' => $message, 
                      'EF_DATE' => new Zend_Db_Expr("now()"),
                      'DOC_STATUS' => 0, //in billing
                      'EXP_DATE' => $exp_date.' '.$exp_time,
                      'UPDATED_DATE' => new Zend_Db_Expr("now()"),
                      'UPDATED_BY' => $this->_userIdLogin,
                      'INVOICE_AMOUNT' => Application_Helper_General::convertDisplayMoney($invoice_amount),
                      'IN_BILLING' => Application_Helper_General::convertDisplayMoney($invoice_amount)
                  );

      $this->_db->update('T_VA_TRANSACTION',$updateArr,$where);

      //insert history
      $historyArr = array(
        'VA_TRANSACTION_ID' => $vaTransactionId,
        'DATE_TIME' => new Zend_Db_Expr("now()"),
        'CUST_ID' => $cust_id,
        'USER_ID' => $this->_userIdLogin,
        'HISTORY_STATUS' => 2 //1=create, 2=overwrite, 3=cancel, 4=expired(system)
      );

      $this->_db->insert('T_VA_TRANSACTION_HISTORY', $historyArr);  

      echo 'success';
    }
    catch(Exception $e){
      echo 'failed'; 
    }
  }

  //------------------------------------------------end overwrite props------------------------------------------------------------

  //-----------------------------------------------------------update--------------------------------------------------------------
  public function updateAction(){
    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout()->disableLayout();

    $cust_bin = $this->_getParam('cust_bin');
    $va_number = $this->_getParam('va_number');
    $va_id = $this->_getParam('va_id');
    $type = $this->_getParam('type');

    $remarks = $this->_getParam('remarks');
    $status = $this->_getParam('status');
    $invoice_amount = $this->_getParam('invoice_amount');

    try{
      $where['CUST_ID = ?'] = $cust_id;
      $where['CUST_BIN = ?'] = $cust_bin;

      if ($type == 'static') {
        $where['VA_ID = ?'] = $va_id;
      }
      else{
        $where['VA_NUMBER = ?'] = $va_number;
      }

      $updateArr = array(
                      'REMARKS' => $remarks, 
                      'DOC_STATUS' => $status, //0 inbilling, 1 pending future date, 2 settled, 3 unsettled, 4 cancelled
                      'UPDATED_DATE' => new Zend_Db_Expr("now()"),
                      'UPDATED_BY' => $this->_userIdLogin,
                  );

      if ($status == '2') {
        $updateArr['IN_BILLING'] = 0;
        $updateArr['SETTLED_AMOUNT'] = $invoice_amount;
        $transChannel = 'Manual update to settled '.'by: '.$this->_userNameLogin.' ('.$this->_userIdLogin.')';
      }
      else{
        $transChannel = 'Manual update to cancelled '.'by: '.$this->_userIdLogin.' ('.$this->_userIdLogin.')';
      }

      $this->_db->update('T_VA_TRANSACTION',$updateArr,$where);

      //insert detail
      $detailArr = array(
        'CCY' => 'IDR',
        'AMOUNT' => 0,
        'TRANS_CHANNEL' => $transChannel,
        'PAYMENT_DATE' =>  new Zend_Db_Expr("now()"),
        'DESCRIPTION' =>  null,
        'STATUS' => 1 //1 success, 2 failed, else suspect
      );

      if ($type == 'static') {
        $detailArr['VA_ID'] = $va_id;
      }
      else{
        $detailArr['VA_ID'] = $va_number;
      }

      $this->_db->insert('T_VA_TRANSACTION_DETAIL', $detailArr);  

      echo 'success';
    }
    catch(Exception $e){
      echo $e; 
    }
  }
  //-----------------------------------------------------------end update--------------------------------------------------------------

}




