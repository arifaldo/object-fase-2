<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';

class Memberbank_DeleteController extends Application_Main 
{

    protected $_moduleDB = 'RTF'; //masih harus diganti

	public function indexAction() 
	{ 
	    //pengaturan url untuk button back
	    $this->setbackURL('/'.$this->_request->getModuleName().'/index');  
	
	    $filters = array('bccy' => array('StringTrim', 'StripTags','StringToUpper'),
	    				'bbank_code' => array('StringTrim','StripTags','StringToUpper'),
	    				'benefbank_code' => array('StringTrim','StripTags','StringToUpper'),
	    				'id' => array('StringTrim','StripTags','StringToUpper')
	    		
	    );
							 
		$validators =  array(
					    'bccy' => array(
								      'NotEmpty', 
							          array('Db_RecordExists', array('table' => 'M_MEMBER_BANK', 'field' => 'CCY')),
							       	  'messages' => array(
							   						  'Cannot be empty',
							   					      'Currency is not found',
							   						     ) 
							             ),
					    'bbank_code' => array(
								      'NotEmpty', 
							          array('Db_RecordExists', array('table' => 'M_MEMBER_BANK', 'field' => 'NOSTRO_SWIFTCODE')),
							       	  'messages' => array(
							   						  'Cannot be empty',
							   					      'Bank Code is not found',
							   						     ) 
							             ),
						'benefbank_code' => array(
									'NotEmpty',
									array('Db_RecordExists', array('table' => 'M_MEMBER_BANK', 'field' => 'BANK_CODE')),
									'messages' => array(
											'Cannot be empty',
											'Bank Code is not found',
									)
						),
						'id' => array(
									'NotEmpty',
									array('Db_RecordExists', array('table' => 'M_MEMBER_BANK', 'field' => 'ID')),
									'messages' => array(
											'Cannot be empty',
											'Bank Code is not found',
									)
						)
						
					        );
			
		if(array_key_exists('bccy',$this->_request->getParams()) && array_key_exists('bbank_code',$this->_request->getParams()) && array_key_exists('benefbank_code',$this->_request->getParams()) && array_key_exists('id',$this->_request->getParams()))
		{
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			
			if($zf_filter_input->isValid()) 
			{
				try 
				{   
				    $this->_db->beginTransaction();
				    
				    $id = $zf_filter_input->getEscaped('id');
					$ccy  = $zf_filter_input->getEscaped('bccy'); 
					$bankcode = $zf_filter_input->getEscaped('bbank_code');
					$benefbankkcode = $zf_filter_input->getEscaped('benefbank_code');
					
					$this->_db->delete('M_MEMBER_BANK',array('ID = ?'=>$id));
				   	
					
					$this->_db->commit();		
						 
					Application_Helper_General::writeLog('NBUD','Delete Member Bank. CCY : ['.$ccy.'] , Nosro Bank Code : ['.$bankcode.'], Bank Code : ['.$benefbankkcode.']');
					$this->_redirect('/notification/success/index');
						
				}
				catch(Exception $e) 
				{
					//rollback changes
					$this->_db->rollBack();
					
					$errorMsg = $this->getErrorRemark('82');
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);

					//$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/failed');
					$this->_redirect('/'.$this->_request->getModuleName().'/index');
				}
			}
			else
			{
				$this->view->error = true;
				$errors = $zf_filter_input->getMessages();
				
				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				foreach($errors as $key => $error)
				{
					foreach($error as $key2 => $errorMsg)
					{
						$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
					}					
				}

				Application_Helper_General::writeLog('NBUD','Update Member Bank');
				//$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/failed');
				$this->_redirect('/'.$this->_request->getModuleName().'/index');
			} 
			
		}
		else
		{
			$errorMsg = '';
			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
			$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
					
			Application_Helper_General::writeLog('NBUD','Update Member Bank');
			
			//$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/failed');
			$this->_redirect('/'.$this->_request->getModuleName().'/index');
		}
	}
	
	
	public function successAction()
	{
		$this->_redirect($this->_backURL);
	}
	
	public function failedAction()
	{
		$this->_redirect($this->_backURL);
	}

}
