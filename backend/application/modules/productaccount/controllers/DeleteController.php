<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';

class productaccount_DeleteController extends Application_Main 
{

    protected $_moduleDB = 'RTF'; //masih harus diganti

	public function indexAction() 
	{ 
		$model = new productaccount_Model_Productaccount();
		
	    //pengaturan url untuk button back
	    $this->setbackURL('/'.$this->view->modulename);

	    $filters = array('product_id' => array('StringTrim', 'StripTags'));
							 
		$validators =  array(
					    'product_id' => array(
								      'NotEmpty', 
							          array('Db_RecordExists', array('table' => 'M_PRODUCT_TYPE', 'field' => 'PRODUCT_ID')),
							       	  'messages' => array(
							   						  $this->language->_('Cannt be empty'),
							   					      $this->language->_('Product ID is not founded'),
												) 
							             )
					        );
			
		if(array_key_exists('product_id',$this->_request->getParams()))
		{
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			if($zf_filter_input->isValid()) 
			{
				try 
				{   
					Application_Helper_General::writeLog('AALS','Delete Product Account Type,Product ID : '.$this->_getParam('product_id'));
				    $this->_db->beginTransaction();
					$productId  = $zf_filter_input->getEscaped('product_id');
				   	$model->deleteData($productId);
				   	
				
					
					$this->_db->commit();
					$this->_redirect('/notification/success/index'); 
					
				}
				catch(Exception $e) 
				{
					//rollback changes
					$this->_db->rollBack();
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
					$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/failed');
				}
			}
			else
			{
				$this->view->error = true;
				$errors = $zf_filter_input->getMessages();
				
				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				foreach($errors as $key => $error)
				{
					foreach($error as $key2 => $errorMsg)
					{
						$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
						
						Application_Helper_General::writeLog('AALS','Update Product Type');
						
						$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/failed');
						
					}					
				}	
			}
		}
		else
		{
			$errorMsg = '';
			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
			$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
					
			Application_Helper_General::writeLog('OBDE','Delete others bank');
			
			$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/failed');
		}
	}
	
	
	public function successAction()
	{
		$this->_redirect($this->_backURL);
	}
	
	public function failedAction()
	{
		$this->_redirect($this->_backURL);
	}

}
