<?php


require_once 'Zend/Controller/Action.php';


class billeronboardaccount_DetailController extends Application_Main
{
	public function indexAction() 
	{
		$acct 	= $this->_getParam('acct');
		if(empty($acct))
		{
			header('location: /billeraccount');exit();
		}
		$biller 			 = new billeronboard_Model_Billeronboard();
		$acctData = $biller->getBillerAcc(array('ACCT_NO'=>$acct,'detail'=>true));
		$this->view->acctData 	 = $acctData[0];
		
		$this->view->changestatus = ( $biller->acctIsRequestChange($acct) || $acctData[0]['ACCT_STATUS'] == 'Deleted') ? true : false;
		$this->setbackURL('/'.$this->_request->getModuleName().'/detail/index/acct/'.$acct);
		Application_Helper_General::writeLog('VBLA','View Detail E-Commerce Account');
	}
	
	public function activedeactiveAction() 
	{
		$acct 	= $this->_getParam('acct');
		$status 			= $this->_getParam('status');

		if(empty($acct) || !in_array($status,array(1,2,3)))
		{
			header('location: /billeronboardaccount');exit();
		}
		$biller 			 = new biller_Model_Biller();
		$temp = $biller->getBillerAcc(array('ACCT_NO'=>$acct,'detail'=>true));
		$acctData = $temp[0];
		if(empty($acct))
		{
			header('location: /'.$this->_request->getModuleName());exit();
		}
		else
		{
			if($status == 2)
			{
				$code = strtoupper($this->_changeType['code']['suspend']);
				$info2 = 'Suspend E-Commerce Account : '.$acct;
			}
			elseif($status == 1)
			{
				$code = strtoupper($this->_changeType['code']['unsuspend']);
				$info2 = 'Unsuspend E-Commerce Account : '.$acct;
			}
			elseif($status == 3)
			{
				$code = strtoupper($this->_changeType['code']['delete']);
				$info2 = 'Delete E-Commerce Account : '.$acct;
			}
			$this->_db->beginTransaction();
			try
			{
				$info = 'E-Commerce Account';
				$change_id = $this->suggestionWaitingApproval($info,$info2,$code,null,'M_PROVIDER_ACCT','TEMP_PROVIDER_ACCT',$acctData['ACCT_NO'],$acctData['ACCT_NAME'],$acctData['ACCT_NO']);

				$data= $acctData+array(
																	'CHANGES_ID' 			=> $change_id,
																	'ACCT_SUGGESTED' 	=> new Zend_Db_Expr('GETDATE()'),
																	'ACCT_SUGGESTEDBY' 	=> $this->_userIdLogin
																);
				unset($data['PROVIDER_CODE']);
				unset($data['PROVIDER_NAME']);
				$data['ACCT_STATUS'] = $status;
				
				$biller->insertTempacct($data);
				Application_Helper_General::writeLog('UBAC',''.$info2. ' '. $data['PROVIDER_CODE']);
				
				
				if($error == 0)
				{
					$this->_db->commit();
					$this->setbackURL('/'.$this->_request->getModuleName());
					$this->_redirect('/notification/submited/index');
				}
			}
			catch(Exception $e)
			{
				$this->_db->rollBack();
			}
		}
	}

}