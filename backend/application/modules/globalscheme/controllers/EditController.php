<?php
require_once 'Zend/Controller/Action.php';

class Globalscheme_EditController extends Application_Main {
	protected $_ccybase;
	protected $_allparams;
	protected $_cekCcy;
	protected $_zf_filters;
	protected $_schemeCode;
	public function indexAction(){
		
	$charges_source = array('Disbursement'=>array(
													'charges_type' => 'charges_type_d',
													'charges_mod' => 'charges_mod_d',
													'ccy_interest' => 'ccy_di',
													'rate_interest' => 'rate_di',
													'ccy_percentage' => 'ccy_dtp',
													'charges_percentage' => 'rate_dtp',
													'min_percentage' => 'min_dtp',
													'max_percentage' => 'max_dtp',
													'ccy_fixamount' => 'ccy_dtf',
													'amount' => 'amount_dtf',
													'disb_full' => 'disb_full',
													'interest' => 'val_di',
													'transPer' => 'val_dtp',
													'transFix' => 'val_dtf',
												 ));

	$charges_source += array('GracePeriod'=>array(  
													'charges_type' => 'charges_type_g',
													'charges_mod' => 'charges_mod_g',
													'ccy_interest' => 'ccy_gi',
													'rate_interest' => 'rate_gi',
													'ccy_percentage' => 'ccy_gtp',
													'charges_percentage' => 'rate_gtp',
													'min_percentage' => 'min_gtp',
													'max_percentage' => 'max_gtp',
													'ccy_fixamount' => 'ccy_gtf',
													'amount' => 'amount_gtf',
													'interest' => 'val_gi',
													'transPer' => 'val_gtp',
													'transFix' => 'val_gtf',
												 ));
			  
	$charges_source += array('Addtenor'=>array( 
													'charges_type' => 'charges_type_a',
													'charges_mod' => 'charges_mod_a',
													'ccy_interest' => 'ccy_ai',
													'rate_interest' => 'rate_ai',
													'ccy_percentage' => 'ccy_atp',
													'charges_percentage' => 'rate_atp',
													'min_percentage' => 'min_atp',
													'max_percentage' => 'max_atp',
													'ccy_fixamount' => 'ccy_atf',
													'amount' => 'amount_atf',
													'interest' => 'val_ai',
													'transPer' => 'val_atp',
													'transFix' => 'val_atf',
													'charges_period' => 'label_type_a0',
											 ));
									 
	$charges_source += array('Penalty'=>array(
													'charges_type' => 'charges_type_p',
													'charges_mod' => 'charges_mod_p',
													'ccy_interest' => 'ccy_pi',
													'rate_interest' => 'rate_pi',
													'ccy_percentage' => 'ccy_ptp',
													'charges_percentage' => 'rate_ptp',
													'min_percentage' => 'min_ptp',
													'max_percentage' => 'max_ptp',
													'ccy_fixamount' => 'ccy_ptf',
													'amount' => 'amount_ptf',
													'interest' => 'val_pi',
													'transPer' => 'val_ptp',
													'transFix' => 'val_ptf',
												 ));
		
			//Zend_Debug::Dump($this->getallParams());
		$getCcy = Application_Helper_Array::listArray($this->getCcy(),'CCY_ID','CCY_ID');
		$this->view->ccyList =  array_merge(array(''=>'-- Any Value --'),$getCcy);
			
			//Zend_Debug::Dump($this->_request->getParams());
		$this->view->maxdisburst = 0; 
		$this->view->maxtransactionapp = 0; 
		$params = $this->_request->getParams();
		$scheme_codes = strtoupper($this->_getParam('code'));
		$code_scheme =  $scheme_codes;
		$valid_status = "";
		$status_err = "";
		
			$select	= $this->_db->select()
									->distinct()
									->from	(
												array('BS'=>'M_BUSINESS_SCHEME'),
												array(
												
														
														'BUSS_SCHEME_CODE'=>'BS.BUSS_SCHEME_CODE',
														'BUSS_SCHEME_NAME'=>'BS.BUSS_SCHEME_NAME',
														
														
													)
											)
									->where('BS.BUSS_SCHEME_CODE =?',$scheme_codes);	
									//->order(array('FEE_OPTION ASC'));
											
		  $result_query = $this->_db->fetchRow($select);
		  $this->view->scheme_name = $result_query['BUSS_SCHEME_NAME'];
		  $this->view->scheme_code = $scheme_codes;
		  $scheme_name = $result_query['BUSS_SCHEME_NAME'];
		 // Zend_debug::Dump($result_query);
		  $check_data = new Zend_Validate_Db_NoRecordExists(
							array(
								'table' => 'TEMP_BUSINESS_SCHEME',
								'field' => 'BUSS_SCHEME_CODE'
							));
			$this->view->status_temp = "";
			if (!$check_data->isValid($scheme_codes))
			{
				$this->view->status_temp = " No changes allowed for this record while awaiting approval for previous change.";
			}
			
		
		if(!$this->_request->isPost())
		{
			
			$global_scheme	= $this->_db->fetchRow($this->_db->select()->from(
											array('BS'=>'M_BUSINESS_SCHEME')
											)
											->where('BS.BUSS_SCHEME_CODE =?',$scheme_codes));
				  
					
				$this->view->data 	= $global_scheme;
				if(is_array($global_scheme))
				{
					
					$this->view->maxdisburst      = $global_scheme['MAX_DISB_RATE'];
					$this->view->maxtransactionapp      = $global_scheme['MAX_TX_APPROVAL_DAY'];
				
				}
				
				
				$this->_setParam('max_disburse',$global_scheme['MAX_DISB_RATE']);
    			$this->_setParam('approval_days',$global_scheme['MAX_TX_APPROVAL_DAY']);
			
			if($scheme_codes)
			{	
				$chargesDetail['Disbursement'] = $this->_db->fetchAll($this->_db->SELECT()
                     ->FROM('M_BUSINESS_SCHEME_CHARGES')
                     ->WHERE('BUSS_SCHEME_CODE = ? ',$scheme_codes)
                     ->WHERE('INT_OPTION = 1')); 
                     
			    $chargesDetail['Addtenor'] = $this->_db->fetchAll($this->_db->SELECT()
			                     ->FROM('M_BUSINESS_SCHEME_CHARGES')
			                     ->WHERE('BUSS_SCHEME_CODE = ? ',$scheme_codes)
			                     ->WHERE('INT_OPTION = 2')); 
			                     
			    $chargesDetail['GracePeriod'] = $this->_db->fetchAll($this->_db->SELECT()
			                     ->FROM('M_BUSINESS_SCHEME_CHARGES')
			                     ->WHERE('BUSS_SCHEME_CODE = ? ',$scheme_codes)
			                     ->WHERE('INT_OPTION = 3')); 
			                     
			    $chargesDetail['Penalty'] = $this->_db->fetchAll($this->_db->SELECT()
			                     ->FROM('M_BUSINESS_SCHEME_CHARGES')
			                     ->WHERE('BUSS_SCHEME_CODE = ? ',$scheme_codes)
			                     ->WHERE('INT_OPTION = 4')); 
			
			   $dbCharges_Type = array('DISB_CHARGES_TYPE','ADD_TENOR_CHARGES_TYPE','GP_CHARGES_TYPE','PEN_CHARGES_TYPE');
			   $arrCharges_Type = array('Disbursement','Addtenor','GracePeriod','Penalty');
			   $chargess = $charges_source;
			   foreach($chargesDetail as $key=>$arrChargesDetail)
			    {
			     $chargesType   = $global_scheme[str_replace($arrCharges_Type,$dbCharges_Type,$key)];
			     $chargesTypeVar  = $chargess[$key]['charges_type'];
			     $this->_setParam($chargesTypeVar,$chargesType);
			     if($chargesTypeVar == 'charges_type_d')
			     {
			      if($chargesType == 1)
			      {
			       $this->_setParam('label_type_d2',$arrChargesDetail[0]['FEE_PERIOD']);
			      }
			      else
			      {
			       $this->_setParam('label_type_d',$arrChargesDetail[0]['INT_PERIOD']);
			      }
			     }elseif($chargesTypeVar == 'charges_type_a')
			     {
			      $this->_setParam('label_type_a0',$arrChargesDetail[0]['FEE_PERIOD']);
			     }
			
			     if($chargesType == 2)
			     {
			      $chargess[$key]['charges_mod'] = 'charges_mod_d2';
			      $chargess[$key]['ccy_percentage'] = 'ccy_dittp';
			      $chargess[$key]['charges_percentage'] = 'rate_dittp';
			      $chargess[$key]['min_percentage'] = 'min_dittp';
			      $chargess[$key]['max_percentage'] = 'max_dittp';
			      $chargess[$key]['ccy_fixamount'] = 'ccy_dittf';
			      $chargess[$key]['amount'] = 'amount_dittf';        
			     }
			     
			     $ccy_int        = $chargess[$key]['ccy_interest'];
			     $ccy_rate        = $chargess[$key]['rate_interest'];
			     $ccy_percentage    = $chargess[$key]['ccy_percentage'];
			     $charges_percentage  = $chargess[$key]['charges_percentage'];
			     $min_percentage    = $chargess[$key]['min_percentage'];
			     $max_percentage    = $chargess[$key]['max_percentage'];
			     $ccy_fixamount     = $chargess[$key]['ccy_fixamount'];
			     $amount        = $chargess[$key]['amount'];
			
			     $intCcy = array();
			     $intRate = array();
			     $feeCcy = array();
			     $feeRate = array();
			     $feeMin = array();
			     $feeMax = array();
			     $feeAmt = array();
			
			     foreach($arrChargesDetail as $detail)
			     {
			      if(($chargesType == 1 || $chargesType == 2 ) && $detail['FEE_TYPE'] != null)
			      {
			       $chargesMod     = $detail['FEE_TYPE'];
			       $chargesModVar    = $chargess[$key]['charges_mod'];  
			       
			       $feeCcy[]  = $detail['FEE_CCY'];      
			       if($chargesMod == 1)
			       {
			        $feeRate[] = $detail['FEE_RATE'];
			        $feeMin[] = $detail['FEE_MIN'];
			        $feeMax[] = $detail['FEE_MAX'];       
			       }
			       else
			       {
			        $feeAmt[] = $detail['FEE_AMT'];
			       }
			      }
			      elseif($chargesType == 0 || $chargesType == 2)
			      {
			        $intCcy[] = $detail['INT_CCY'];
			        $intRate[] = $detail['INT_RATE'];  
			      }
			     }
			     if($chargesType == 1 || $chargesType == 2)
			     {
			      if($chargesMod == 1)
			      {
			       $this->_setParam($chargesModVar,$chargesMod);
			       $this->_setParam($ccy_percentage,$feeCcy);
			       $this->_setParam($charges_percentage,$feeRate);
			       $this->_setParam($min_percentage,$feeMin);
			       $this->_setParam($max_percentage,$feeMax);
			      }
			      else
			      {
			       $this->_setParam($chargesModVar,$chargesMod);
			       $this->_setParam($ccy_fixamount,$feeCcy);
			       $this->_setParam($amount,$feeAmt);
			      }     
			     }
			     if($chargesType == 0 || $chargesType == 2)
			     {
			      $this->_setParam($ccy_int,$intCcy);
			      $this->_setParam($ccy_rate,$intRate);
			     }
			    }
			    
			    unset($chargess);	
			}	
			
		}
		
		
						$this->_allparams  = $this->_getAllParams();	
						$validateDisbursement = $this->validate_facilityinterest($charges_source['Disbursement']);
						$validatePenalty = $this->validate_facilityinterest($charges_source['Penalty']);
						$validateGracePeriod = $this->validate_facilityinterest($charges_source['GracePeriod']);
						$validateAddtenor = $this->validate_facilityinterest($charges_source['Addtenor']);
						
						
						$filters = array();
						$validators = array();
						$err_msg = array();
						$filters  = array_merge($filters,$validateDisbursement[0]);
						$validators  = array_merge($validators,$validateDisbursement[1]);
						$err_msg = array_merge($err_msg,$validateDisbursement[2]);
						
						$filters  = array_merge($filters,$validateGracePeriod[0]);
						$validators  = array_merge($validators,$validateGracePeriod[1]);
						$err_msg = array_merge($err_msg,$validateGracePeriod[2]);

						$filters  = array_merge($filters,$validateAddtenor[0]);
						$validators  = array_merge($validators,$validateAddtenor[1]);
						$err_msg = array_merge($err_msg,$validateAddtenor[2]);

						$filters  = array_merge($filters,$validatePenalty[0]);
						$validators  = array_merge($validators,$validatePenalty[1]);
						$err_msg = array_merge($err_msg,$validatePenalty[2]);
						
						
						$this->view->paramsVal = $this->_getAllParams();
						
						$this->_zf_filters = new Zend_Filter_Input($filters, $validators,$this->_getAllParams());

						if($this->_zf_filters->isValid() && empty($err_msg)){
							$valid_status = '';
						}else{
							$this->return_errorMessages($err_msg);
							$valid_status = 'error';
						}
						$this->_schemeCode = $scheme_codes;
						$this->return_values($charges_source);
						
						
					if($this->_request->isPost())
					{
					
					$this->view->maxdisburst = $this->_getParam('maxdisburst'); 
					$this->view->maxtransactionapp = $this->_getParam('maxtransactionapp');	
			
						$this->view->loadstatus = "validasi";
						$filters = array(
											'maxdisburst'		=> array('StringTrim'),
											'maxtransactionapp'	=> array('StripTags', 'StringTrim'),
											
										);
						
						
							
						$zf_filter_input = new Zend_Filter_Input($filters,null,$this->_request->getPost());
			
						$maxdisburst 											 = $zf_filter_input->maxdisburst;
						$maxtransactionapp										 = $zf_filter_input->maxtransactionapp;
			
						$maxdisburst = Application_Helper_General::setEmptyDecimal($maxdisburst);
						//Application_Helper_General::convertDisplayMoney
						$page_check = "";
						$page_check = (Zend_Validate::is($maxdisburst,'Float'))? $page_check : 1;
						if ($maxdisburst === "")
						{	
							$this->view->err_maxdisburst = "Max Disbursement cannot be left blank.";
							$valid_status = "error";
						}elseif (($maxdisburst <= 0) || ($maxdisburst >= 101))
						{	
							
							$this->view->err_maxdisburst = "Max Disbursement Must be greater 0 then smaller than 100.";
							$valid_status = "error";
						}
						if(($page_check == 1) && (empty($this->view->err_maxdisburst)))
						{
							$this->view->err_maxdisburst = "Max Disbursement Must be Numeric";
							$valid_status = "error";
						}
						
						$page_check = (Zend_Validate::is($maxtransactionapp,'Digits'))? $page_check : 1;
						
						if ($maxtransactionapp == "")
						{	
							$this->view->err_maxtransactionapp = "Max Transaction Approval cannot be left blank.";
							$valid_status = "error";
						}else if (($maxtransactionapp <= 0) || ($maxtransactionapp >= 101))
						{	
							$this->view->err_maxtransactionapp = "Max Transaction Approval Must be greater 0 then smaller than 100.";
							$valid_status = "error";
						}
						if(($page_check == 1) && (!empty($maxtransactionapp)))
						{	
							$this->view->err_maxtransactionapp = "Max Transaction Approval Must be Numeric";
							$valid_status = "error";
						}
						
			//-------------------------------------------------------------- validasi	
			
									
			
			//-------------------------------------------------------			
						//Zend_debug::Dump($params);	
						if(empty($valid_status))
						{
							
							//echo "On Development";
							
							//Zend_Debug::dump($params);
							//die();
						
							try {
									
									$this->_db->beginTransaction();
										
										$displayTableName = 'Global Scheme Parameter';
										
										$masterTable = 'M_BUSINESS_SCHEME,M_BUSINESS_SCHEME_CHARGES';
										$tempTable='TEMP_BUSINESS_SCHEME,TEMP_BUSINESS_SCHEME_CHARGES';	
										$changeInfo = 'Business Scheme';
									
										$principle_id = $scheme_codes; //code supplierfinancing
										$changeType = $this->_changeType['code']['edit'];
										 //$change_id = $this->suggestionWaitingApproval('Member',$info,$this->_changeType['code']['edit'],null,'M_MEMBER,M_MEMBER_ACCT,M_MEMBER_FEE,M_MEMBER_INSTALLMENT','TEMP_MEMBER,TEMP_MEMBER_ACCT,TEMP_MEMBER_FEE,TEMP_MEMBER_INSTALLMENT','MEMBER_ID',$zf_filter_input->member_id);
										
										// insert ke T_GLOBAL_CHANGES
										$changesId = $this->suggestionWaitingApproval($displayTableName,$changeInfo, $changeType, null,$masterTable,$tempTable,$scheme_codes, $scheme_name);
										
										//INSERT INTO TEMP_ANCHOR & TEMP_ANCHOR_LIMIT	
										$update   = new Zend_Db_Expr('now()');
										$this->_db->insert('TEMP_BUSINESS_SCHEME', 
																array(
																	'CHANGES_ID'=>$changesId,
																	'BUSS_SCHEME_CODE' => $code_scheme,
																	'BUSS_SCHEME_NAME' => $scheme_name,
																	'MAX_DISB_RATE' => $params['maxdisburst'],
																	'MAX_TX_APPROVAL_DAY' => $params['maxtransactionapp'],
																	'DISB_CHARGES_TYPE' => $params['charges_type_d'],
																	'ADD_TENOR_CHARGES_TYPE' => $params['charges_type_a'],
																	'GP_CHARGES_TYPE' => $params['charges_type_g'],
																	'PEN_CHARGES_TYPE' => $params['charges_type_p'],
																	'SUGGESTED' => $update,
																	'SUGGESTEDBY' => $this->_userIdLogin
															));
															
										
										$update   = new Zend_Db_Expr('now()');
										$updateArr = array('SUGGESTED'=>$update,'SUGGESTEDBY'=>$this->_userIdLogin);
			
										$whereArr = array('BUSS_SCHEME_CODE = ?'=>$code_scheme);
										$act_update = $this->_db->update('M_BUSINESS_SCHEME',$updateArr,$whereArr);
										$this->insert_chargesDetail($charges_source, $changesId);
										
									$this->_db->commit();
									$this->setbackURL('/globalscheme');
									$this->_redirect('/notification/success');
									
							}
							catch(Exception $e){
								$this->_db->rollBack();
								$this->view->error = true;
							}
							
						}	
							
						
						
			
						
					}
	
	$this->view->loadstatus = 'validasi';
	}
	
	private function validate_facilityinterest($source)
	{
		$err_msg = array();
		$filter_facility = array();
		$validate_facility = array();
		$arrChargesType = array(0,1);
		$charges_type = $this->_allparams[$source['charges_type']];
		$arrYesNo = array('0','1');
		
		if($source['charges_type'] == 'charges_type_d')
		{
			$arrChargesType = array(0,1,2);
			$filter_facility = array_merge($filter_facility,array(
																						$source['disb_full'] => array('StripTags','StringTrim')
																					 )
													);		
			$validate_facility = array_merge($validate_facility,array($source['disb_full'] => array('NotEmpty','Digits',array('inArray',$arrYesNo),
																																					'messages' => array(
																																														'Please select from the list',	
																																														'Please select from the list',	
																																														'Please select from the list',																																
																																													),
																																					),
																	));
		}
		
		//---------------------------------------------CHARGES TYPE-----------------------------------------------------//		
		$filter_facility = array_merge($filter_facility,array(
																						$source['charges_type'] => array('StripTags','StringTrim')
																					 )
													);		
		$validate_facility = array_merge($validate_facility,array($source['charges_type'] => array('NotEmpty','Digits',array('inArray',$arrChargesType),
																																					'messages' => array(
																																														'Please select from the list',	
																																														'Please select from the list',	
																																														'Please select from the list',																																
																																													),
																																					),	
														  ));		
		//---------------------------------------------END CHARGES TYPE-----------------------------------------------------//
		if($charges_type == 0 || ( $charges_type == 2 && count($arrChargesType)==3 ))
		{
		//---------------------------------------------FILTER & VALIDATE INTEREST-----------------------------------------------------//
			$cekCcy['INTEREST'] = array();
			if(isset($this->_allparams[$source['ccy_interest']]))
			{
				foreach($this->_allparams[$source['ccy_interest']] as $key=>$value)
				{					
					$ccy_interest = $source['ccy_interest'].$key;			
					$rate_interest = $source['rate_interest'].$key;
					$value_ccy_interest = $this->_allparams[$source['ccy_interest']][$key];
					$value_rate_interest = $this->_allparams[$source['rate_interest']][$key];
					$this->_setParam($ccy_interest,$value_ccy_interest);	
					$this->_setParam($rate_interest,$value_rate_interest);					
					
						if(in_array($value_ccy_interest,$cekCcy['INTEREST']))
						{
							$err_msg = array_merge($err_msg,array($source['ccy_interest']=>'1 entry for each currency'));
						}

					$cekCcy['INTEREST'][$this->_allparams[$source['ccy_interest']][$key]] = $this->_allparams[$source['ccy_interest']][$key];
					
					$filter_facility = array_merge($filter_facility,array(
																									$ccy_interest => array('StripTags','StringTrim'),
																									$rate_interest => array('StripTags','StringTrim')
																								 )
																);		
					$validate_facility = array_merge($validate_facility,array($ccy_interest => array('NotEmpty',array('Db_RecordExists',
																																														 array('table' => 'M_MINAMT_CCY',
																																														'field' => 'CCY_ID',)),
																																				'messages' => array(
																																													'Currency : Can not be empty',
																																													'Currency : Please select from the list',																																																																									
																																												),
																																				),	
																											$rate_interest => array(array('Between', array('min'=>0.01,'max'=>100)),
																																						'NotEmpty',
																																						new Zend_Validate_Regex('/^([1-9][0-9]{0,2})?(([0-9])\\.[0-9]{0,2})?$/'),
																																						'messages' => array(																																													
																																													'Rate : Must be greater than 0 and lower than or equal to 100',
																																													'Rate : Can not be empty',
																																													'Rate : Invalid Format',
																																													),
																																				),	
																	  ));	
				}				
			}
			else
			{
				//--------------------SET MESSAGE FOR SETTING AT LEAST ONE CHARGES INTEREST-------------------------//
				$err_msg = array_merge($err_msg,array($source['ccy_interest']=>'At least 1 interest currency must be setup'));
			}
		//---------------------------------------------END FILTER & VALIDATE INTEREST-----------------------------------------------------//		
		}
		if($charges_type == 1 || ( $charges_type == 2 && count($arrChargesType)==3 ))
		{
			if($source['charges_type'] == 'charges_type_a')
			{
				$arrChargesType = array(1,2);
				$filter_facility = array_merge($filter_facility,array(
																							$source['charges_period'] => array('StripTags','StringTrim')
																						 )
														);		
				$validate_facility = array_merge($validate_facility,array($source['charges_period'] => array('NotEmpty','Digits',array('inArray',$arrChargesType),
																																						'messages' => array(
																																															'Please select from the list',	
																																															'Please select from the list',	
																																															'Please select from the list',																																
																																														),
																																						),
																		));
			}
			
			
			
			if($charges_type == 2)
			{
				$source['charges_mod'] = 'charges_mod_d2';
				$source['ccy_percentage'] = 'ccy_dittp';
				$source['charges_percentage'] = 'rate_dittp';
				$source['min_percentage'] = 'min_dittp';
				$source['max_percentage'] = 'max_dittp';
				$source['ccy_fixamount'] = 'ccy_dittf';
				$source['amount'] = 'amount_dittf';
			}
			$charges_mod = $this->_allparams[$source['charges_mod']];
			
			//---------------------------------------------CHARGES MOD-----------------------------------------------------//
			$filter_facility = array_merge($filter_facility,array(
																							$source['charges_mod'] => array('StripTags','StringTrim')
																						 )
														);
			$validate_facility = array_merge($validate_facility,array($source['charges_mod'] => array('NotEmpty',array('inArray',array('1','2')),
																																		'messages' => array(
																																											'Can not be empty',
																																											'Please select from the list',	
																																											'Please select from the list',																															
																																											),
																																		),
															  ));
			//--------------------------------------------END CHARGES MOD-----------------------------------------------------//	
			if($charges_mod == '1')
			{
				$cekCcy['PERCENTAGE'] = array();
				if(isset($this->_allparams[$source['ccy_percentage']]))
				{
					foreach($this->_allparams[$source['ccy_percentage']] as $key=>$value)
					{
						$ccy_percentage = $source['ccy_percentage'].$key;
						$charges_percentage = $source['charges_percentage'].$key;
						$min_percentage = $source['min_percentage'].$key;
						$max_percentage = $source['max_percentage'].$key;
						
						$value_ccy_percentage = $this->_allparams[$source['ccy_percentage']][$key];
						$value_charges_percentage = $this->_allparams[$source['charges_percentage']][$key];					
						$value_min_percentage = Application_Helper_General::convertDisplayMoney($this->_allparams[$source['min_percentage']][$key]);
						$value_max_percentage = Application_Helper_General::convertDisplayMoney($this->_allparams[$source['max_percentage']][$key]);
						
						$this->_setParam($ccy_percentage,$value_ccy_percentage);	
						$this->_setParam($charges_percentage,$value_charges_percentage);
						$this->_setParam($min_percentage,$value_min_percentage);	
						$this->_setParam($max_percentage,$value_max_percentage);
						
							if(in_array($value_ccy_percentage,$cekCcy['PERCENTAGE']))
							{
								$err_msg = array_merge($err_msg,array($source['ccy_percentage']=>'1 entry for each currency'));
							}						
						$cekCcy['PERCENTAGE'][$this->_allparams[$source['ccy_percentage']][$key]] = $this->_allparams[$source['ccy_percentage']][$key];
						
					//-------------------------------------------- FILTER && VALIDATE PERCENTAGE CHARGES -----------------------------------------------------//
					$filter_facility = array_merge($filter_facility,array(
																									$ccy_percentage => array('StripTags','StringTrim'),
																									$charges_percentage => array('StripTags','StringTrim'),
																									$min_percentage => array('StripTags','StringTrim'),
																									$max_percentage => array('StripTags','StringTrim'),
																								 )
																);
					$validate_facility = array_merge($validate_facility,array($ccy_percentage => array(array('NotEmpty',array('Db_RecordExists',
																																															 array('table' => 'M_MINAMT_CCY',
																																															'field' => 'CCY_ID',)),
																																											'messages' => array(
																																																				'Currency : Can not be empty',
																																																				'Currency : Please select from the list',																																																																									
																																																			),
																																					),	
																																				),
																											$charges_percentage =>  array(array('Between', array('min'=>0.01,'max'=>100)),
																																										'NotEmpty',
																																										new Zend_Validate_Regex('/^([1-9][0-9]{0,2})?(([0-9])\\.[0-9]{0,2})?$/'),
																																										'messages' => array(																																													
																																																						'Rate : Must be greater than 0 and lower than or equal 100',
																																																						'Rate : Can not be empty',
																																																						'Rate : Invalid Format',
																																																						),
																																					),
																											$min_percentage =>  array(
																																								'NotEmpty',
																																								array('Between', array('min'=>0.01,'max'=>($value_max_percentage-0.01))),
																																								array('Between', array('min'=>0.01,'max'=>9999999999999.99)),
																																								new Zend_Validate_Regex('/^([1-9][0-9]{0,12})?(([0-9])\\.[0-9]{0,2})?$/'),
																																								'messages' => array(
																																																'Minimum Rate Can not be empty',
																																																'Minimum Rate must be lower than Maximum Rate',
																																																'Minimum Rate Must be greater than 0 and lower than or equal to 9999999999999.99',
																																																'Minimum Rate Invalid Format',
																																																),
																																					),
																											$max_percentage =>  array(array('Between', array('min'=>0.01,'max'=>9999999999999.99)),
																																															'NotEmpty',
																																															new Zend_Validate_Regex('/^([1-9][0-9]{0,12})?(([0-9])\\.[0-9]{0,2})?$/'),
																																															'messages' => array(																																													
																																																						'Max : Must be greater than minimum percentage and lower than or equal to 9999999999999.99',
																																																						'Max : Can not be empty',
																																																						'Max : Invalid Format',
																																																						),
																																					),
																	  ));
					}
				}
				else
				{
					//------------------SET MESSAGE FOR SETTING AT LEAST ONE PERCENTAGE CHARGES----------------//
					$err_msg = array_merge($err_msg,array($source['ccy_percentage']=>'At least 1 Percentage currency must be setup'));
				}
				//--------------------------------------------END FILTER && VALIDATE PERCENTAGE CHARGES-----------------------------------------------------//	
			}
			elseif($charges_mod == '2')
			{
			//-------------------------------------------- FILTER && VALIDATE FIXAMOUNT-----------------------------------------------------//	
				$cekCcy['FIXAMOUNT'] = array();
				if(isset($this->_allparams[$source['ccy_fixamount']]))
				{
					foreach($this->_allparams[$source['ccy_fixamount']] as $key=>$value)
					{
						$ccy_fixamount = $source['ccy_fixamount'].$key;
						$amount = $source['amount'].$key;
						$value_ccy_fixamount = $this->_allparams[$source['ccy_fixamount']][$key];
						$value_amount = Application_Helper_General::convertDisplayMoney($this->_allparams[$source['amount']][$key]);
						$this->_setParam($ccy_fixamount,$value_ccy_fixamount);	
						$this->_setParam($amount,$value_amount);
						
							if(in_array($value_ccy_fixamount,$cekCcy['FIXAMOUNT']))
							{
								$err_msg = array_merge($err_msg,array($source['ccy_fixamount']=>'1 entry for each currency'));
							}
						$cekCcy['FIXAMOUNT'][$this->_allparams[$source['ccy_fixamount']][$key]] = $this->_allparams[$source['ccy_fixamount']][$key];

						$filter_facility = array_merge($filter_facility,array(
																										$ccy_fixamount => array('StripTags','StringTrim'),
																										$amount => array('StripTags','StringTrim')
																									 )
																	);		
						$validate_facility = array_merge($validate_facility,array($ccy_fixamount => array('NotEmpty',array('Db_RecordExists',
																																															 array('table' => 'M_MINAMT_CCY',
																																															'field' => 'CCY_ID',)),
																																					'messages' => array(
																																														'Currency : Can not be empty',
																																														'Currency : Please select from the list',																																																																									
																																													),
																																					),	
																												$amount => array(array('Between', array('min'=>0.01,'max'=>9999999999999.99)),
																																					'NotEmpty',
																																					new Zend_Validate_Regex('/^([1-9][0-9]{0,12})?(([0-9])\\.[0-9]{0,2})?$/'),
																																					'messages' => array(																																													
																																														'Amount : Must be greater than 0 and lower than or equal to 9999999999999.99',
																																														'Amount : Can not be empty',
																																														'Amount : Invalid Format',
																																														),
																																					),	
																		  ));	
					}
				}
				else
				{
				//--------------------SET MESSAGE FOR SETTING AT LEAST ONE CHARGES FIXAMOUNT-------------------------//
				$err_msg = array_merge($err_msg,array($source['ccy_fixamount']=>'At least 1 fix amount currency must be setup'));
				}
				//--------------------------------------------END FILTER && VALIDATE FIXAMOUNT-----------------------------------------------------//	
			}
		}
// CEK CROSS CURRENCY
		$crossCurr = false;
		if(isset($cekCcy['INTEREST']))
		{
			$crossCurr = $this->cek_crossCurrency($cekCcy['INTEREST']);
		}
		if(isset($cekCcy['PERCENTAGE']))
		{
			$crossCurr = $this->cek_crossCurrency($cekCcy['PERCENTAGE']);
		}
		if(isset($cekCcy['FIXAMOUNT']))
		{
			$crossCurr = $this->cek_crossCurrency($cekCcy['FIXAMOUNT']);
		}
		if($crossCurr)
		{
			$err_msg['crossCurrency'] = 'Please check the currency for each charges';
		}
		
		return array($filter_facility,$validate_facility,$err_msg);
	}
	
	function cek_crossCurrency($listCurrency)
	{
		ksort($listCurrency);
		if(!$this->_cekCcy)
			$this->_cekCcy = $listCurrency;
		else
		{
			if(serialize($this->_cekCcy) != serialize($listCurrency))
			{
				return true;
			}
		}
		return false;
	}
	
	function return_errorMessages($err_msg = null)
	{	
		//========================= RETURN FILTER & VALIDATOR ERROR MESSAGES ============================//
						foreach($this->_zf_filters->getMessages() as $key => $msg)
						{
							$errVar = 'err'.$key;						
							foreach($msg as $messages)
							{								
								$this->view->$errVar = $messages;									
							}
						}
		//========================= END FILTER & VALIDATOR RETURN ERROR MESSAGES ============================//	
						foreach($err_msg as $key => $msg)
						{
							$errVar = 'err'.$key;
							$this->view->$errVar = $msg;
						}
	}
	
	function return_facilityinterest($source,$allMessages=null)
	{
		$return_val = null;
	    $charges_type = isset($this->_allparams[$source['charges_type']]) ? $this->_allparams[$source['charges_type']] : '' ;
		$arrChargesType = array(0,1);

		if($source['charges_type'] == 'charges_type_d')
		{
			$arrChargesType = array(0,1,2);
		}

		if($charges_type == 0 || ( $charges_type == 2 && count($arrChargesType)==3 ))
		{
			$ccy_int = $source['ccy_interest'];
			$rate_int = $source['rate_interest'];	
			if(is_array($this->_getParam($ccy_int)))
			{
				foreach($this->_getParam($ccy_int) as $key => $value)
				{
					$errMsg = '';
					$ccy_val		= ($ccy_int.$key);
					$rate_val	= ($rate_int.$key);
					if($allMessages[$ccy_val])
					{
						foreach($allMessages[$ccy_val] as $key=>$value)
						{
							if($errMsg != null)$errMsg .= '<br/>';
							$errMsg 		.= $value;
						}
					}
					if($allMessages[$rate_val])
					{
						foreach($allMessages[$rate_val] as $key=>$value)
						{if($errMsg != null)$errMsg .='<br/>';
						$errMsg 		.= $value;}
					}
					$return_val['INTEREST'][] = array(
																'int_ccy' 			 => $this->_getParam($ccy_val),
																'int_rate' 		 => $this->_getParam($rate_val),
																'int_err_mess' => $errMsg,
															);
				}
			} 
		}
		if($charges_type == 1 || ( $charges_type == 2 && count($arrChargesType)==3 ))
		{
			if($charges_type == 2)
			{
				$source['charges_mod'] = 'charges_mod_d2';
				$source['ccy_percentage'] = 'ccy_dittp';
				$source['charges_percentage'] = 'rate_dittp';
				$source['min_percentage'] = 'min_dittp';
				$source['max_percentage'] = 'max_dittp';
				$source['ccy_fixamount'] = 'ccy_dittf';
				$source['amount'] = 'amount_dittf';								
			}
			$charges_mod = $this->_allparams[$source['charges_mod']];

			if($charges_mod == '1')
			{
				$ccy_percentage			     = $source['ccy_percentage'];
				$charges_percentage 	 = $source['charges_percentage'];
				$min_percentage				 = $source['min_percentage'];
				$max_percentage			 = $source['max_percentage'];
				if(is_array($this->_getParam($ccy_percentage)))
				{				
					foreach($this->_getParam($ccy_percentage) as $key => $value)
					{
						$errMsg = '';
						$ccy_val				= ($ccy_percentage.$key);
						$charges_val		= ($charges_percentage.$key);
						$min_val				= ($min_percentage.$key);
						$max_val			= ($max_percentage.$key);
						
						if(isset($allMessages[$ccy_val]))
						{
							foreach($allMessages[$ccy_val] as $key=>$value)
							{if($errMsg != null)$errMsg .= '<br/>';
							$errMsg 		.= $value;}
						}
						if(isset($allMessages[$charges_val]))
						{
							foreach($allMessages[$charges_val] as $key=>$value)
							{if($errMsg != null)$errMsg .='<br/>';
							$errMsg 		.= $value;}
						}
						if(isset($allMessages[$min_val]))
						{
							foreach($allMessages[$min_val] as $key=>$value)
							{if($errMsg != null)$errMsg .='<br/>';
							$errMsg 		.= $value;}
						}
						if(isset($allMessages[$max_val]))
						{
							foreach($allMessages[$max_val] as $key=>$value)
							{if($errMsg != null)$errMsg .='<br/>';
							$errMsg 		.= $value;}
						}
						$return_val['PERCENTAGE'][] = array(
																	'int_ccy' 			 => $this->_getParam($ccy_val),
																	'int_rate' 		 => $this->_getParam($charges_val),
																	'int_min' 			 => Application_Helper_General::displayMoney($this->_getParam($min_val)),
																	'int_max' 		 => Application_Helper_General::displayMoney($this->_getParam($max_val)),
																	'int_err_mess' => $errMsg,
																);		
					}
				} 
			}
			if($charges_mod == '2')
			{
				$ccy_fixamount = $source['ccy_fixamount'];
				$amount = $source['amount'];			
				if(is_array($this->_getParam($ccy_fixamount)))
				{				
					foreach($this->_getParam($ccy_fixamount) as $key => $value)
					{
						$errMsg = '';
						$ccy_val			= ($ccy_fixamount.$key);
						$amount_val	= ($amount.$key);
						if( isset($allMessages[$ccy_val]))
						{
							foreach($allMessages[$ccy_val] as $key=>$value)
							{if($errMsg != null)$errMsg .= '<br/>';
							$errMsg 		.= $value;}
						}
						if(isset($allMessages[$amount_val]))
						{
							foreach($allMessages[$amount_val] as $key=>$value)
							{if($errMsg != null)$errMsg .='<br/>';
							$errMsg 		.= $value;}
						}
						$return_val['FIXAMOUNT'][] = array(
																	'int_ccy' 			 => $this->_getParam($ccy_val),
																	'int_amount' 		 => Application_Helper_General::displayMoney($this->_getParam($amount_val)),
																	'int_err_mess' => $errMsg,
																);
					}									
				} 				
			}
		}

		return $return_val;
	}
	
	function return_values($charges_source)
	{	
		//========================= FACILITY INTEREST  ============================//
				$this->view->loadstatus = "validasi";
				
				$errMes = $this->_zf_filters->getMessages();
				foreach($charges_source as $source=>$var)
				{
					if(isset($this->_allparams[$charges_source[$source]['charges_type']]))
					{
						if($this->_allparams[$charges_source[$source]['charges_type']] == 2)
						{
							$var['transPer'] = 'val_dittp';
							$var['transFix'] = 'val_dittf';
						}
					}
					$facilityInterest = $this->return_facilityinterest($charges_source[$source],$errMes);
					if(isset($facilityInterest['INTEREST']))
						$this->view->$var['interest'] = $facilityInterest['INTEREST'];
					if(isset($facilityInterest['PERCENTAGE']))
						$this->view->$var['transPer'] = $facilityInterest['PERCENTAGE'];
					if(isset($facilityInterest['FIXAMOUNT']))
						$this->view->$var['transFix'] = $facilityInterest['FIXAMOUNT'];
				}
				$this->view->label_type_d = ($this->_getParam('label_type_d') != null) ? $this->_getParam('label_type_d') : 1;
  				$this->view->label_type_d2 = ($this->_getParam('label_type_d2')  != null) ? $this->_getParam('label_type_d2') : 1;
			    $this->view->valFeeAddPeriod = ($this->_getParam('label_type_a0')  != null) ? $this->_getParam('label_type_a0') : 1;
		//========================= END FACILITY INTEREST ============================//
	}
	
	
	function insert_chargesDetail($data_source,$change_id)
	{
		$dataTEMP_CHARGES_DETAIL = array();
		foreach($data_source as $option => $source)
		{		  							
			if($option == 'Disbursement')
			$type = 1;
			if($option == 'Addtenor')
			$type = 2;
			if($option == 'GracePeriod')
			$type = 3;
			if($option == 'Penalty')
			$type = 4;			
			$charges_type = $this->_allparams[$source['charges_type']];			
			if($charges_type == 0 || $charges_type == 2)
			{
			
			//--------------------------------------------- INTEREST -----------------------------------------------------//	
				$install_per = 2;
				if($option == 'Disbursement')
				{
					$install_per = 1;
				}					

				if(isset($this->_allparams[$source['ccy_interest']]))
				{
					foreach($this->_allparams[$source['ccy_interest']] as $key=>$value)
					{	
						$ccy_interest = $source['ccy_interest'].$key;
						$rate_interest = $source['rate_interest'].$key;									
						
						$dataTEMP_CHARGES_DETAIL[] = (
																				array(
																						'CHANGES_ID' => $change_id,
																						'BUSS_SCHEME_CODE' => $this->_schemeCode,
																						'INT_CCY' => $this->_zf_filters->$ccy_interest,
																						'INT_RATE' => $this->_zf_filters->$rate_interest,
																						'INT_PERIOD' => $install_per,
																						'INT_OPTION' => $type,
																						'FEE_TYPE' => null,
																						'FEE_PERIOD' => null,
																						'FEE_CCY' => null,
																						'FEE_AMT' => null,
																						'FEE_RATE' => null,
																						'FEE_MIN' => null,
																						'FEE_MAX' => null,
																						'FEE_OPTION' => $type,
																					  )
																			);																		
					}
				}
			//---------------------------------------------END INTEREST-----------------------------------------------------//		
			}
			if($charges_type == 1 || $charges_type == 2)
			{

				
				$charges_mod = $this->_allparams[$source['charges_mod']];			
				$install_per = 1;
				
				
				
				if($option == 'Disbursement')
				{
					if($charges_type == 2)
					{
						// $install_per = 2;
						$install_per = $this->_getParam('label_type_d2');
					}else{
						$install_per = $this->_getParam('label_type_d');
					}
				}elseif($option == 'Addtenor'){
					$install_per = 	$this->_allparams[$source['charges_period']];
				}
				if($charges_type == 2)
				{
					$source['charges_mod'] = 'charges_mod_d2';
					$source['ccy_percentage'] = 'ccy_dittp';
					$source['charges_percentage'] = 'rate_dittp';
					$source['min_percentage'] = 'min_dittp';
					$source['max_percentage'] = 'max_dittp';
					$source['ccy_fixamount'] = 'ccy_dittf';
					$source['amount'] = 'amount_dittf';
				}
				$charges_mod = $this->_allparams[$source['charges_mod']];
				if($charges_mod == '1')
				{
				//-------------------------------------------- PERCENTAGE CHARGES -----------------------------------------------------//	
					if(isset($this->_allparams[$source['ccy_percentage']]))
					{
						foreach($this->_allparams[$source['ccy_percentage']] as $key=>$value)
						{
							$ccy_percentage = $source['ccy_percentage'].$key;
							$charges_percentage = $source['charges_percentage'].$key;
							$min_percentage = $source['min_percentage'].$key;
							$max_percentage = $source['max_percentage'].$key;
							
							$dataTEMP_CHARGES_DETAIL[] = (
																					array(
																							'CHANGES_ID' => $change_id,
																							'BUSS_SCHEME_CODE' => $this->_schemeCode,
																							'INT_CCY' => null,
																							'INT_RATE' => null,
																							'INT_PERIOD' => null,
																							'INT_OPTION' => $type,
																							'FEE_TYPE' => $charges_mod,
																							'FEE_PERIOD' => $install_per,
																							'FEE_CCY' => $this->_zf_filters->$ccy_percentage,
																							'FEE_AMT' => null,
																							'FEE_RATE' => $this->_zf_filters->$charges_percentage,
																							'FEE_MIN' => $this->_zf_filters->$min_percentage,
																							'FEE_MAX' => $this->_zf_filters->$max_percentage,
																							'FEE_OPTION' => $type,
																						  )
																				);						
						
						}
					}
					//--------------------------------------------END PERCENTAGE CHARGES-----------------------------------------------------//	
				}
				elseif($charges_mod == '2')
				{
				//-------------------------------------------- FIXAMOUNT -----------------------------------------------------//	
					if(isset($this->_allparams[$source['ccy_fixamount']]))
					{
						foreach($this->_allparams[$source['ccy_fixamount']] as $key=>$value)
						{
							$ccy_fixamount = $source['ccy_fixamount'].$key;
							$amount = $source['amount'].$key;
					
							$dataTEMP_CHARGES_DETAIL[] = (
																				array(
																						'CHANGES_ID' => $change_id,
																						'BUSS_SCHEME_CODE' => $this->_schemeCode,
																						'INT_CCY' => null,
																						'INT_RATE' => null,
																						'INT_PERIOD' => null,
																						'INT_OPTION' => $type,
																						'FEE_TYPE' => $charges_mod,
																						'FEE_PERIOD' => $install_per,
																						'FEE_CCY' => $this->_zf_filters->$ccy_fixamount,
																						'FEE_AMT' => $this->_zf_filters->$amount,
																						'FEE_RATE' => null,
																						'FEE_MIN' => null,
																						'FEE_MAX' => null,
																						'FEE_OPTION' => $type,
																					  )
																			);	
						}
					}
				//-------------------------------------------- END FIXAMOUNT -----------------------------------------------------//	
				}
			}
		}
		foreach($dataTEMP_CHARGES_DETAIL as $data)
		{	
			$this->_db->insert('TEMP_BUSINESS_SCHEME_CHARGES',$data);	
		}		
	}
	
}

?>
