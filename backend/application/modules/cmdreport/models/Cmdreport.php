<?php
class cmdreport_Model_Cmdreport
{
	protected $_db;

	protected $_masterglobalstatus;

    // constructor
	public function __construct()
	{
		$config = Zend_Registry::get('config');
		$this->_masterglobalstatus = $config['master']['globalstatus'];
		$this->_beneftype = $config['account']['beneftype'];
		$this->_chargesto = $config['charges']['to'];
		$this->_providertype = $config['provider']['type'];
		$this->_db = Zend_Db_Table::getDefaultAdapter();
	}

    public function getFPrivilege()
    {
		$data = $this->_db->select()->distinct()
								->from(array('A' => 'M_FPRIVILEGE'),array('FPRIVI_ID', 'FPRIVI_DESC'))
								->order('FPRIVI_DESC ASC')
				 				->query()->fetchAll();
       return $data;
    }

    public function getActivityReport($fParam,$sortBy,$sortDir,$filter)
    {
		$select2 = $this->_db->select()
					        ->from(array('B' => 'T_FACTIVITY'),array())
					        ->joinleft(array('C' => 'M_CUSTOMER'), 'B.CUST_ID = C.CUST_ID',array())
					        ->joinleft(array('U' => 'M_USER'), 'B.USER_ID = U.USER_ID',array())
					        ->joinleft(array('D' => 'M_FPRIVILEGE'),'B.ACTION_DESC = D.FPRIVI_ID',array(//'C.CUST_ID',
				        																			'B.ACTION_FULLDESC'));


		if($filter == null || $filter == true)
		{
			if(!empty($fParam['datefrom']) && empty($fParam['dateto']))
					$select2->where("DATE(B.LOG_DATE) >= ".$this->_db->quote($fParam['datefrom']));

			if(empty($fParam['datefrom']) && !empty($fParam['dateto']))
					$select2->where("DATE(B.LOG_DATE) <= ".$this->_db->quote($fParam['dateto']));

			if(!empty($fParam['datefrom']) && !empty($fParam['dateto']))
					$select2->where("DATE(B.LOG_DATE) BETWEEN ".$this->_db->quote($fParam['datefrom'])." and ".$this->_db->quote($fParam['dateto']));
		}

		if($filter == TRUE)
		{
			if($fParam['active'])
			{
				if(($fParam['active'] != 'FLGN' && $fParam['active'] !='FLGT' && $fParam['active'] != 'CHMP'))
				{
				   $select2->where("D.FPRIVI_ID LIKE ".$this->_db->quote($fParam['active']));
				}
				else
				{
					$select2->where("B.ACTION_DESC LIKE ".$this->_db->quote($fParam['active']));
				}
			}

			if($fParam['userid'])
			{
			   $select2->where("B.USER_ID LIKE ".$this->_db->quote('%'.$fParam['userid'].'%'));
			}

			if($fParam['fullname'])
			{
			   $select2->where("U.USER_FULLNAME LIKE ".$this->_db->quote('%'.$fParam['fullname'].'%'));
			}

			if($fParam['description'])
			{
			   $select2->where("B.ACTION_FULLDESC LIKE ".$this->_db->quote('%'.$fParam['description'].'%'));
			}

		}

		$select2->order($sortBy.' '.$sortDir);

		return $this->_db->fetchAll($select2);
    }

    public function getHelpReport($fParam,$sortBy,$sortDir,$filter)
    {
		$select = $this->_db->select()
								->from(	array('TH' => 'T_HELP'),
										array(
												'HELP_ID'=>'TH.HELP_ID',
												'HELP_TOPIC'=>'TH.HELP_TOPIC',
												'UPLOAD_DATETIME'=>'TH.UPLOAD_DATETIME',
												'HELP_DESCRIPTION'=>'TH.HELP_DESCRIPTION',
												'HELP_FILENAME'=>'TH.HELP_FILENAME',
												'HELP_ISDELETED'=>'TH.HELP_ISDELETED',
												'EDIT_DATETIME'=>'TH.EDIT_DATETIME',
												'HELP_SYS_FILENAME'=>'TH.HELP_SYS_FILENAME'))
								->joinLeft	(
												array('MB' => 'M_BUSER'),
												'MB.BUSER_ID = TH.UPLOADED_BY',
												array('UPLOADED_BY'=>'MB.BUSER_NAME')
											)
								->joinLeft	(
												array('MB2' => 'M_BUSER'),
												'MB2.BUSER_ID = TH.HELP_EDITEDBY',
												array('HELP_EDITEDBY'=>'MB2.BUSER_NAME')
											);

		if($filter == TRUE)
		{
			if($fParam['description'])
			{
				$select->where("UPPER(TH.HELP_DESCRIPTION) LIKE ".$this->_db->quote('%'.$fParam['description'].'%'));
			}

			if($fParam['HELP_ISDELETED'] != null)
			{
				$select->where("TH.HELP_ISDELETED = ?", $fParam['HELP_ISDELETED']);
			}

			if($fParam['SEARCH_TEXT'])
			{
				$select->where("UPPER(TH.HELP_FILENAME) LIKE ".$this->_db->quote('%'.$fParam['SEARCH_TEXT'].'%'));
			}

			if($fParam['HELP_TOPIC'])
			{
				$select->where("UPPER(TH.HELP_TOPIC) LIKE ".$this->_db->quote('%'.$fParam['HELP_TOPIC'].'%'));
			}

			if($fParam['UPLOADED_BY'])
			{
				$select->where("UPPER(MB.BUSER_NAME) LIKE ".$this->_db->quote('%'.$fParam['UPLOADED_BY'].'%'));
			}
		}

		if($filter == TRUE || $filter == null)
		{

			$DATE_START = $fParam['DATE_START'];
			$DATE_END 	= $fParam['DATE_END'];
//			if($fParam['DATE_START'])
//			{
//				$select->where('DATE(TH.EDIT_DATETIME) >= '.$this->_db->quote($fParam['DATE_START']));
//			}
//
//			if($fParam['DATE_END'])
//			{
//				$select->where('DATE(TH.EDIT_DATETIME) <= '.$this->_db->quote($fParam['DATE_END']));
//			}
			
			
			if(!empty($DATE_START) && empty($DATE_END))
		            $select->where("DATE(TH.UPLOAD_DATETIME) >= ".$this->_db->quote($DATE_START));
//		            
		   	if(empty($DATE_START) && !empty($DATE_END))
		            $select->where("DATE(TH.UPLOAD_DATETIME) <= ".$this->_db->quote($DATE_END));
//		            
		    if(!empty($DATE_START) && !empty($DATE_END))
		            $select->where("DATE(TH.UPLOAD_DATETIME) between ".$this->_db->quote($DATE_START)." and ".$this->_db->quote($DATE_END));
			
		}

		$select->order($sortBy.' '.$sortDir);

		return $this->_db->fetchAll($select);
    }

    public function getHolidayListReport($fParam,$sortBy,$sortDir,$filter)
    {
		$select = $this->_db->select()
			->from(	'M_HOLIDAY',array('HOLIDAY_DATE','HOLIDAY_DESCRIPTION'))
			->order(array($sortBy.' '.$sortDir));

		if($filter == TRUE || $filter == null)
		{
			if($fParam['DATE_START'])
			{
				$select->where('DATE(HOLIDAY_DATE) >= '.$this->_db->quote($fParam['DATE_START']));
			}

			if($fParam['DATE_END'])
			{
				$select->where('DATE(HOLIDAY_DATE) <= '.$this->_db->quote($fParam['DATE_END']));
			}
		}

		if($filter == true)
		{
			if($fParam['SEARCH_TEXT'])
			{
				$select->where("UPPER(HOLIDAY_DESCRIPTION) LIKE ".$this->_db->quote('%'.$fParam['SEARCH_TEXT'].'%'));
			}
		}

		$select->order($sortBy.' '.$sortDir);

		return $this->_db->fetchAll($select);
    }

	public function getCustomerList($fParam,$sorting)
	{
    	$statusarr = array_combine(array_values($this->_masterglobalstatus['code']),array_values($this->_masterglobalstatus['desc']));

    	$caseStatus = "(CASE A.USER_STATUS ";
  		foreach($statusarr as $key=>$val)
  		{
   			$caseStatus .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseStatus .= " END)";

  		$select = $this->_db->select()
					        ->from(array('A' => 'M_USER'),array('A.USER_ID',
		        											 	'A.USER_FULLNAME',
		        												'A.TOKEN_ID',
		        											 	'A.USER_EMAIL',
		        											 	'A.USER_CIF',
		        											 	'A.USER_MOBILE_PHONE',
		        											 	'A.USER_CREATED',
		        											 	'A.USER_SUGGESTED',
		        											 	'A.USER_UPDATED',
		        											 	'A.USER_LOCKREASON','USER_STATUS'=>$caseStatus));

		if(!empty($fParam))
		{
			if(isSet($fParam['userid']))$select->where('UPPER(USER_ID) LIKE '.$this->_db->quote('%'.strtoupper($fParam['userid']).'%'));
			if(isSet($fParam['username']))$select->where('UPPER(USER_FULLNAME) LIKE '.$this->_db->quote('%'.strtoupper($fParam['username']).'%'));
			if(isSet($fParam['phone']))$select->where('USER_MOBILE_PHONE='.$fParam['phone']);
			if(isSet($fParam['usercif']))$select->where('UPPER(USER_CIF) LIKE '.$this->_db->quote('%'.strtoupper($fParam['usercif']).'%'));
			if(isSet($fParam['city']))$select->where('UPPER(USER_CITY) LIKE '.$this->_db->quote('%'.strtoupper($fParam['city']).'%'));
			if(isSet($fParam['status']))$select->where('USER_STATUS= ?',$fParam['status']);
		}

		$select->order($sorting);

		$result = $this->_db->fetchAll($select);

		return $result;
	}

	public function getCustomerDetail($userid)
	{
		$statusarr = array_combine(array_values($this->_masterglobalstatus['code']),array_values($this->_masterglobalstatus['desc']));

		$caseStatus = "(CASE A.USER_STATUS ";
  		foreach($statusarr as $key=>$val)
  		{
   			$caseStatus .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseStatus .= " END)";

    	$select = $this->_db->select()
				->from(array('A' => 'M_USER'),array('*','USER_STATUS'=>$caseStatus));
		$select->where('UPPER(USER_ID) LIKE '.$this->_db->quote(strtoupper($userid)));
		$result = $this->_db->fetchRow($select);
		//Zend_Debug::dump($result);
		return $result;
	}

	public function getDailyLimit($userid)
	{
		$statusarr = array_combine(array_values($this->_masterglobalstatus['code']),array_values($this->_masterglobalstatus['desc']));

		$caseStatus = "(CASE A.DAILYLIMIT_STATUS ";
  		foreach($statusarr as $key=>$val)
  		{
   			$caseStatus .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseStatus .= " END)";

		$select = $this->_db->select()
				->from(array('A' => 'M_DAILYLIMIT'),array('CCY_ID','DAILYLIMIT','CREATED','SUGGESTED','UPDATED','DAILYLIMIT_STATUS'=>$caseStatus));
		$select -> where("A.USER_LOGIN = ".$this->_db->quote($userid));
		$result = $select->query()->FetchAll();

		return $result;
	}

	public function getUserLimit($userid)
	{
		$statusarr = array_combine(array_values($this->_masterglobalstatus['code']),array_values($this->_masterglobalstatus['desc']));

		$caseStatus = "(CASE A.MAKERLIMIT_STATUS ";
  		foreach($statusarr as $key=>$val)
  		{
   			$caseStatus .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseStatus .= " END)";

    	$select = $this->_db->select()
				->from(array('A' => 'M_MAKERLIMIT'),array('*','MAKERLIMIT_STATUS'=>$caseStatus))
				->joinleft (array('B' => 'M_USER_ACCT'),'A.ACCT_NO = B.ACCT_NO',array('*'))
				->joinleft (array('C' => 'M_USER_ACCT'),'A.ACCT_NO = C.ACCT_NO',array('*'));
		$select -> where("A.USER_LOGIN = ".$this->_db->quote($userid));
		$result = $select->query()->FetchAll();

		return $result;
	}

	public function getLastActivity($userid)
	{
    	$lastactive = $this->_db->select()
						->from('T_FACTIVITY',array('LOG_DATE'));
		$lastactive -> where("USER_ID = ".$this->_db->quote($userid));
		$lastactive -> order('LOG_DATE DESC');
		$lastactive = $this->_db->fetchRow($lastactive);

		return $lastactive;
	}

	public function getBankAcc($userid)
	{
		$statusarr = array_combine(array_values($this->_masterglobalstatus['code']),array_values($this->_masterglobalstatus['desc']));

		$caseStatus = "(CASE A.ACCT_STATUS ";
  		foreach($statusarr as $key=>$val)
  		{
   			$caseStatus .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseStatus .= " END)";

    	$select = $this->_db->select()
				->from(array('A' => 'M_USER_ACCT',array('*','ACCT_STATUS'=>$caseStatus)));
		if($userid)
		{
			$select -> where("A.USER_ID = ".$this->_db->quote($userid));
		}
		$result = $select->query()->FetchAll();

		return $result;
	}

	public function getBankAccUser()
	{
    	$select = $this->_db->select()
				->from(array('A' => 'M_USER_ACCT'),array('USER_ID'));
		$select ->order('USER_ID ASC');
		$result = $select->query()->FetchAll();

		return $result;
	}

	public function getBankAccList($fParam,$sorting)
	{
		$statusarr = array_combine(array_values($this->_masterglobalstatus['code']),array_values($this->_masterglobalstatus['desc']));

		$caseStatus = "(CASE A.ACCT_STATUS ";
  		foreach($statusarr as $key=>$val)
  		{
   			$caseStatus .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseStatus .= " END)";

    	$select = $this->_db->select()
							 ->from (array('A' => 'M_USER_ACCT'),array())
							 ->join (array('B' => 'M_USER'),'A.USER_ID = B.USER_ID',array('B.USER_ID',
			 																			 'B.USER_FULLNAME',
			 																			 'A.ACCT_NO',
			 																			 'A.ACCT_NAME',
			 																			 'A.CCY_ID',
			 																			 'A.ACCT_DESC',
			 																			 'A.ACCT_EMAIL',
																						 'A.ACCT_CREATED',
																						 'A.ACCT_SUGGESTED',
																						 'A.ACCT_UPDATED',
																						 'ACCT_STATUS' => $caseStatus));

		if(!empty($fParam))
		{
			if(isSet($fParam['userid']))
			{
				$select->where("UPPER(B.USER_ID) LIKE ".$this->_db->quote('%'.$fParam['userid'].'%'));
			}

			if(isSet($fParam['username']))
			{
				$select->where("UPPER(B.USER_FULLNAME) LIKE ".$this->_db->quote('%'.$fParam['username'].'%'));
			}

			if(isSet($fParam['accnumber']))
			{
				$select->where("ACCT_NO LIKE ".$this->_db->quote('%'.$fParam['accnumber'].'%'));
			}

			if(isSet($fParam['accname']))
			{
				$select->where("UPPER(ACCT_NAME) LIKE ".$this->_db->quote('%'.$fParam['accname'].'%'));
			}

			if(isSet($fParam['stat']))
			{
				$select->where("ACCT_STATUS LIKE ".$this->_db->quote($fParam['stat']));
			}
		}


		$select->order($sorting);
		return $this->_db->fetchAll($select);
	}

	public function getCharges($fparam,$sorting)
	{
		$select = $this->_db->select()
				->from(array('A' => 'M_SERVICE_PROVIDER'),array())
				->join(array('B' => 'M_CHARGES_PROVIDER'),'A.PROVIDER_ID = B.PROVIDER_ID',array('CHARGES_TYPE' 	=> new Zend_Db_Expr("'Biller Charges'"),
																								'BILLER'		=> new Zend_Db_Expr("CONCAT(A.PROVIDER_CODE,' / ',A.PROVIDER_NAME)"),
																								'CCY'			=> "B.CHARGES_CCY",
																								'AMOUNT'		=> "B.CHARGES_AMT"));

		$chargesarr = array("'RTGS_charges'" 	=> 'RTGS',
							"'SKN_charges'"		=> 'SKN',
							"'ONLINE_charges'"	=> 'ONLINE');

		$casecharges = "(CASE A.SETTING_ID ";
  		foreach($chargesarr as $key=>$val)
  		{
   			$casecharges .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$casecharges .= " END)";

		$select2 = $this->_db->select()
				->from(array('A' => 'M_SETTING'),array(	'CHARGES_TYPE' 	=> $casecharges,
														'BILLER'		=> new Zend_Db_Expr("''"),
														'CCY'			=> new Zend_Db_Expr("'IDR'"),
														'AMOUNT'		=> "A.SETTING_VALUE"));
		$select2->where("A.SETTING_ID = 'RTGS_charges' OR A.SETTING_ID = 'SKN_charges' OR A.SETTING_ID = 'ONLINE_charges'");

		$selectstring = $select->__toString();
		$selectstring2 = $select2->__toString();

		$unionquery = $this->_db->select()
								->union(array($selectstring,$selectstring2));

		$selectunion = $this->_db->select()
							->from (($unionquery),array('*'));

		if(!empty($fparam))
		{
			if(isSet($fparam['chargetype']))
			{
				$selectunion->where("UPPER(CHARGES_TYPE) = ".$this->_db->quote($fparam['chargetype']));
			}

			if(isSet($fparam['billercode']))
			{
				$selectunion->where("UPPER(BILLER) LIKE ".$this->_db->quote($fparam['billercode'].' /%'));
			}

			if(isSet($fparam['billername']))
			{
				$selectunion->where("UPPER(BILLER) LIKE ".$this->_db->quote('% / %'.$fparam['billername'].'%'));
			}
		}

		$selectunion->order($sorting);
		$result = $this->_db->fetchAll($selectunion);

		return $result;
	}

	public function getBillerCode()
	{
    	$select = $this->_db->select()
				->from(array('A' => 'M_SERVICE_PROVIDER'),array())
				->join(array('B' => 'M_CHARGES_PROVIDER'),'A.PROVIDER_ID = B.PROVIDER_ID',array('A.PROVIDER_CODE'));
		$select ->order('A.PROVIDER_CODE ASC');
		$result = $select->query()->FetchAll();

		return $result;
	}

	function getCityCode($cityCode)
	{
		$select = $this->_db->select()
				->from(array('A' => 'M_CITY'),array('*'));
//		$select->joinLeft(array('MU'=>'M_PROVIDER_ACCT'), 'MU.PROVIDER_ID = A.PROVIDER_ID', array('A.PROVIDER_ID','A.PROVIDER_NAME'));		
		$select->where('A.CITY_CODE = ?',$cityCode);
//		$select ->order('CITY_NAME ASC');
		return $this->_db->fetchall($select);
	}

	public function getBeneficiary($type,$fparam,$sorting=null)
	{
		$typearr = array_combine(array_values($this->_beneftype['code']),array_values($this->_beneftype['desc']));
		$casetype = "(CASE A.BENEFICIARY_TYPE ";
  		foreach($typearr as $key=>$val)
  		{
   			$casetype .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$casetype .= " END)";

		if($type == 'list')
		{
			$select = $this->_db->select()
						->from(array('A' => 'M_BENEFICIARY'),array())
						->join(array('B' => 'M_BENEFICIARY_USER'),'A.BENEFICIARY_ID = B.BENEFICIARY_ID', array())
						->join(array('C' => 'M_USER'), 'B.USER_ID = C.USER_ID AND B.CUST_ID = C.CUST_ID',array('B.USER_ID',
																					'C.USER_FULLNAME',
																					'A.BENEFICIARY_ISAPPROVE',
																					'A.BENEFICIARY_ACCOUNT',
																					'A.BENEFICIARY_NAME',
																					'A.BENEFICIARY_ALIAS',
																					'A.CURR_CODE',
																					//'A.BENEFICIARY_BANK_CODE',
																					'A.BANK_CODE',
																					'A.BANK_ADDRESS1',
																					'A.BANK_ADDRESS2',
																					'A.BENEFICIARY_CREATEDBY',
																					'A.BENEFICIARY_CREATED',
																					'BENEFICIARY_TYPE' => $casetype,'A.BENEFICIARY_ISAPPROVE'));
																					//'A.BENEFICIARY_ISREQUEST_DELETE'));
																					//'A.BENEFICIARY_ISAPPROVE','A.BENEFICIARY_ISREQUEST_DELETE'));
																					//'BENEFICIARY_TYPE' => $casetype));
			
			if(!empty($fparam))
			{
				if(isSet($fparam['userid']))
				{
					$select->where("UPPER(B.USER_ID) LIKE ".$this->_db->quote('%'.$fparam['userid'].'%'));
				}

				if(isSet($fparam['username']))
				{
					$select->where("UPPER(USER_FULLNAME) LIKE ".$this->_db->quote('%'.$fparam['username'].'%'));
				}

				if(isSet($fparam['beneacct']))
				{
					$select->where("BENEFICIARY_ACCOUNT LIKE ".$this->_db->quote('%'.$fparam['beneacct'].'%'));
				}

				if(isSet($fparam['beneacctname']))
				{
					$select->where("UPPER(BENEFICIARY_NAME) LIKE ".$this->_db->quote('%'.$fparam['beneacctname'].'%'));
				}

				if(isSet($fparam['beneaccttype']))
				{
					$select->where("BENEFICIARY_TYPE = ".$this->_db->quote($fparam['beneaccttype']));
				}
			}

			$select->order($sorting);
			$result = $this->_db->fetchAll($select);
		}

		if($type == 'all')
		{
			$benefType = $fparam['type'];
			if($benefType == 'Others')
				{
					$beneficiary_type = '2';
				}
			elseif($benefType == 'Online')
				{
					$beneficiary_type = '8';
				}
			elseif($benefType == 'Mayapada')
				{
					$beneficiary_type = '1';
				}
			else{
					$beneficiary_type = '0';
				}

			$select = $this->_db->select()
						->from(array('A' => 'M_BENEFICIARY'),array())
						->join(array('B' => 'M_BENEFICIARY_USER'),'A.BENEFICIARY_ID = B.BENEFICIARY_ID',array())
						->join(array('C' => 'M_USER'),'B.USER_ID = C.USER_ID AND B.CUST_ID = C.CUST_ID',array('B.USER_ID',
																					'C.USER_FULLNAME',
																					//'A.BENEFICIARY_ISAPPROVE',
																					'A.BENEFICIARY_ACCOUNT',
																					'A.BENEFICIARY_NAME',
																					//'A.BENEFICIARY_ALIAS',
																					'BENEFICIARY_TYPE' => $casetype,
																					'A.CURR_CODE',
																					'A.BENEFICIARY_ADDRESS',
																					'A.BENEFICIARY_RESIDENT',
																					'A.BENEFICIARY_EMAIL',
																					'A.BANK_CODE',
																					'A.BANK_ADDRESS1',
																					'A.BANK_ADDRESS2',
																					'A.BENEFICIARY_ID_TYPE',
																					'A.BENEFICIARY_ID_NUMBER',
																					'A.BENEFICIARY_CREATEDBY',
																					'A.BENEFICIARY_CREATED',
																					'A.BENEFICIARY_CATEGORY',
																					'A.BENEFICIARY_ISAPPROVE'));
			$select->where("UPPER(B.USER_ID) = ".$this->_db->quote($fparam['userid']));
			$select->where("UPPER(A.BENEFICIARY_ACCOUNT) = ".$this->_db->quote($fparam['dataid']));
			$select->where("UPPER(BENEFICIARY_TYPE) = ".$this->_db->quote($beneficiary_type));

			$result = $this->_db->fetchRow($select);
		}

		return $result;
	}

	public function getServiceProvider()
    {
		$data = $this->_db->select()->distinct()
					->from(array('M_SERVICE_PROVIDER'),array('PROVIDER_CODE'))
					->order('PROVIDER_CODE ASC')
				 	-> query() ->fetchAll();

       return $data;
    }

	public function getBillerChargesList($fParam,$sorting)
    {
    	$providerarr = array_combine(array_values($this->_providertype['code']),array_values($this->_providertype['desc']));
		$chargestoarr = array_combine(array_values($this->_chargesto['code']),array_values($this->_chargesto['desc']));

		$caseProvider = "(CASE B.PROVIDER_TYPE ";
  		foreach($providerarr as $key=>$val)
  		{
   			$caseProvider .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseProvider .= " END)";

		$caseChargesTo = "(CASE A.CHARGES_TO ";
  		foreach($chargestoarr as $key=>$val)
  		{
   			$caseChargesTo .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseChargesTo .= " END)";

		$select = $this->_db->select()
						->from(array('B' => 'M_SERVICE_PROVIDER'),array())
						->join(array('A' => 'M_CHARGES_PROVIDER'),'A.PROVIDER_ID = B.PROVIDER_ID',array('B.PROVIDER_ID',
																										'B.PROVIDER_CODE',
																										'B.PROVIDER_NAME',
																										'A.CHARGES_CCY',
																										'A.CHARGES_AMT',
																										'CHARGES_TO' => $caseChargesTo,
																										'PROVIDER_TYPE' => $caseProvider,
																										'A.CHARGES_SUGGESTED',
																										'A.CHARGES_SUGGESTEDBY'));


		if(!empty($fParam['datefrom']))
					$select->where("DATE(A.CHARGES_SUGGESTED) >= ".$this->_db->quote($fParam['datefrom']));

		if(!empty($fParam['dateto']))
				$select->where("DATE(A.CHARGES_SUGGESTED) <= ".$this->_db->quote($fParam['dateto']));

     	if($fParam['billerCode'])
	    {
       		$select->where("B.PROVIDER_CODE = ".$this->_db->quote($fParam['billerCode']));
	    }

		if($fParam['billerName'])
		{
       		$select->where("B.PROVIDER_NAME LIKE ".$this->_db->quote('%'.$fParam['billerName'].'%'));
		}

		if($fParam['suggestor'])
	    {
       		$select->where("A.CHARGES_SUGGESTEDBY LIKE ".$this->_db->quote($fParam['suggestor']));
	    }

		$select->order($sorting);

       return $this->_db->fetchAll($select);
    }

	public function getBillerChargesDetail($providerid)
	{
		$providerarr = array_combine(array_values($this->_providertype['code']),array_values($this->_providertype['desc']));
		$chargestoarr = array_combine(array_values($this->_chargesto['code']),array_values($this->_chargesto['desc']));

		$caseProvider = "(CASE A.PROVIDER_TYPE ";
  		foreach($providerarr as $key=>$val)
  		{
   			$caseProvider .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseProvider .= " END)";

		$caseChargesTo = "(CASE B.CHARGES_TO ";
  		foreach($chargestoarr as $key=>$val)
  		{
   			$caseChargesTo .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseChargesTo .= " END)";

    	$select = $this->_db->select()
						->from(array('A' => 'M_SERVICE_PROVIDER'),array('*'))
						->join(array('B' => 'M_CHARGES_PROVIDER'),'A.PROVIDER_ID = B.PROVIDER_ID',array('*',
																										'CHARGES_TO' => $caseChargesTo,
																										'PROVIDER_TYPE' => $caseProvider));
		$select->where("A.PROVIDER_ID = ".$this->_db->quote($providerid));
		$result = $this->_db->fetchRow($select);

		return $result;
	}
}