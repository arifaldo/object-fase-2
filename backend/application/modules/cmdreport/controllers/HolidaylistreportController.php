<?php


require_once 'Zend/Controller/Action.php';

class cmdreport_HolidaylistreportController extends Application_Main {
	
	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
		
		$fields = array(
							'Holiday Date'  		=> array	(
																	'field' => 'HOLIDAY_DATE',
																	'label' => $this->language->_('Holiday Date'),
																	'sortable' => true
																),
							'Holiday Description'  	=> array	(
																	'field' => 'HOLIDAY_DESCRIPTION',
																	'label' => $this->language->_('Holiday Description'),
																	'sortable' => true
																)
						);

		$filterlist = array('PS_HOLIDAY', 'HOLIDAY_DESCRIPTION');
		
		$this->view->filterlist = $filterlist;
	
                        
        //get page, sortby, sortdir
        $page 		= $this->_getParam('page');		
        $sortBy 	= $this->_getParam('sortby');
        $sortDir 	= $this->_getParam('sortdir');
        $getCSV 	= $this->_getParam('csv');
        $csv 		= $this->_getParam('csv');	
		$pdf 		= $this->_getParam('pdf');
		
		
        //validate parameters before passing to view and query
        $page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
                    
        $sortBy = (Zend_Validate::is($sortBy,'InArray', array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
        $sortDir = (Zend_Validate::is($sortDir,'InArray', array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';

	    $this->view->currentPage = $page;
	    $this->view->sortBy = $sortBy;
	    $this->view->sortDir = $sortDir;
	    
		
		//$zf_filter 	= new Zend_Filter_Input($filters,array(),$this->_request->getParams());
		
		$filter = $this->_getParam('filter');
		$filter_clear = $this->_getParam('clearfilter');
		
				
		$select =	$this->_db->select()
						->from(	'M_HOLIDAY',array('HOLIDAY_DATE','HOLIDAY_DESCRIPTION'))
						->order(array($sortBy.' '.$sortDir));								
							
			if($filter == null || !empty($pdf) || !empty($csv)  )
			{
				$now = Zend_Date::now()->toString("dd/MM/yyyy");
				
				$date = $now;
				$endDate = $now;
				$DATE_START = $date;
				$DATE_END = $endDate;

				$this->view->DATE_START = $DATE_START;
				$this->view->DATE_END =  $DATE_END;
			}
			
			if($filter_clear == TRUE)
			{
				$now = '';
				
				$date = $now;
				$endDate = $now;
				$DATE_START = $date;
				$DATE_END = $endDate;

				$this->view->DATE_START = $DATE_START;
				$this->view->DATE_END =  $DATE_END;
			}
						
			
			if($filter == TRUE)
			{
			
				//get filtering param
			   $filterArr = array(	'HOLIDAY_DESCRIPTION'   	=> array('StringTrim','StripTags','StringToUpper'),
									'PS_HOLIDAY' 	  	=> array('StringTrim','StripTags'),
									'PS_HOLIDAY_END' 	  	=> array('StringTrim','StripTags'),
				);
				
				
				// if POST value not null, get post, else get param
				$dataParam = array('HOLIDAY_DESCRIPTION');
				$dataParamValue = array();
				
				$clean2 = array_diff( $this->_request->getParam('wherecol'),$dataParam); 
				$dataParam = array_diff( $this->_request->getParam('wherecol'),$clean2); 
				// print_r($dataParam);die;

				// print_r($output);die;
				// print_r($this->_request->getParam('wherecol'));
				foreach ($dataParam as $no => $dtParam)
				{
				
					if(!empty($this->_request->getParam('wherecol'))){
						$dataval = $this->_request->getParam('whereval');
						// print_r($dataval);
						$order = 0;
							foreach ($this->_request->getParam('wherecol') as $key => $value) {
								if($value == "PS_HOLIDAY"){
										$order--;
									}
								if($dtParam==$value){
									$dataParamValue[$dtParam] = $dataval[$order];
								}
								$order++;
							}
					}
				}
				
				//echo "<pre>";
				//print_r ($dataParamValue);

				if(!empty($this->_request->getParam('holidaydate'))){
						$holidayarr = $this->_request->getParam('holidaydate');
						$dataParamValue['PS_HOLIDAY'] = $holidayarr[0];
						$dataParamValue['PS_HOLIDAY_END'] = $holidayarr[1];
					}
				$validators = array(
									'HOLIDAY_DESCRIPTION' 	=> array(),					
									'PS_HOLIDAY' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
									'PS_HOLIDAY_END'	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
								);
				
				$options = array('allowEmpty' => true);
				
				
				$zf_filter  	= new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);
			
				$DATE_START    	= html_entity_decode($zf_filter->getEscaped('PS_HOLIDAY'));
				$DATE_END		= html_entity_decode($zf_filter->getEscaped('PS_HOLIDAY_END'));
				$SEARCH_TEXT   	= html_entity_decode($zf_filter->getEscaped('HOLIDAY_DESCRIPTION'));
				
				$this->view->DATE_START 	= $DATE_START;
				$this->view->DATE_END 		=  $DATE_END;
				$this->view->SEARCH_TEXT 	= $SEARCH_TEXT;
			
			}
	        
			if($filter == TRUE || $filter == null)
	        {			
				if(!empty($DATE_START))
				{
					$FormatDate = new Zend_Date($DATE_START, $this->_dateDisplayFormat);
					$DATE_START  = $FormatDate->toString($this->_dateDBFormat);							
				}
					
				if(!empty($DATE_END))
				{
					$FormatDate = new Zend_Date($DATE_END, $this->_dateDisplayFormat);
					$DATE_END    = $FormatDate->toString($this->_dateDBFormat);
				}
					
	        	if($DATE_START)
				{
					$select->where('DATE(HOLIDAY_DATE) >= '.$this->_db->quote($DATE_START));
				}
				
				if($DATE_END)
				{
					$select->where('DATE(HOLIDAY_DATE) <= '.$this->_db->quote($DATE_END));	
				}
	        }
	    
			if($filter == TRUE)
			{
				if($SEARCH_TEXT)
				{
					$select->where("UPPER(HOLIDAY_DESCRIPTION) LIKE ".$this->_db->quote('%'.$SEARCH_TEXT.'%'));
				}
			}
		
			
			if($csv || $pdf || $this->_request->getParam('print'))
			{
				
				$data = $this->_db->fetchAll($select);
				$header = array($this->language->_('Holiday Date'),$this->language->_('Holiday Description'));
				foreach($data as $key=>$row)
				{
					
					$data[$key]['HOLIDAY_DATE'] = Application_Helper_General::convertDate($row['HOLIDAY_DATE'],'dd MMM yyyy',$this->view->defaultDateFormat);					
				}
				
				if($csv)
				{
					$this->_helper->download->csv($header,$data,null,$this->language->_('Holiday List Report'));  
					Application_Helper_General::writeLog('RPHD','Download CSV Holiday Report');
				}
				else if($pdf)
				{
					$this->_helper->download->pdf($header,$data,null,$this->language->_('Holiday List Report'));
					Application_Helper_General::writeLog('RPHD','Download PDF Holiday Report');
				}
				elseif($this->_request->getParam('print') == 1){
					$this->_forward('print', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'Holiday List Report', 'data_header' => $fields));
				}
			}
	        else
	        {                              
		        $this->paging($select);
		        $this->view->fields 	 = $fields;
		        $this->view->filter 	 = $filter;
				Application_Helper_General::writeLog('RPHD','View Holiday List Report');
	        }

	        if(!empty($dataParamValue)){
	    		$this->view->holidayStart = $dataParamValue['PS_HOLIDAY'];
	    		$this->view->holidayEnd = $dataParamValue['PS_HOLIDAY_END'];

			    unset($dataParamValue['PS_HOLIDAY_END']);
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
	        $this->view->wherecol     = $wherecol;
	        $this->view->whereval     = $whereval;
	     // print_r($whereval);die;
	      }

 	}
}
