<?php

require_once 'Zend/Controller/Action.php';

class Ofacbank_NewController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti


    public function indexAction()
	{
	    //pengaturan url untuk button back
	    //$this->setbackURL('/'.$this->_request->getModuleName().'/index');
			$this->_helper->layout()->setLayout('newlayout');  
	    $this->setbackURL('/ofacbank/index');

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}
    	}

    	$this->view->report_msg = array();

    	$model = new ofacbank_Model_Ofacbank();
	    $this->view->countryArr = ( array(''=>'-- '.$this->language->_('Please Select').' --')+Application_Helper_Array::listArray($model->getCountry(),'COUNTRY_CODE','COUNTRY_NAME'));

		if($this->_request->isPost() )
		{
			$filters = array(
			                 'country_code'     => array('StringTrim','StripTags'),
							);

			$validators = array(
								'country_code'    => array(
															'NotEmpty',
															array('Db_NoRecordExists', array('table' => 'M_OFAC', 'field' => 'country_code')),
															'messages' => array(
																              $this->language->_('Can not be empty'),
																              $this->language->_('OFAC already exist'),
														                      )
														   ),
							   );


			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

			if($zf_filter_input->isValid())
			{
				$countrydet = $model->getCountryName($zf_filter_input->country_code);
				$country_name = $countrydet['COUNTRY_NAME'];
				$created = date('Y-m-d H:i:s');

				$content = array(
								'country_code' 	 => $zf_filter_input->country_code,
								'country_name' 	 	 => $country_name,
								'created'	 => $created,
 								'createdby' => $this->_userIdLogin,
						       );

				try
				{

					//-------- insert --------------
					$this->_db->beginTransaction();

					$this->_db->insert('M_OFAC',$content);

					$this->_db->commit();
					//$id = $this->_db->lastInsertId();
					Application_Helper_General::writeLog('OFAD','Add OFAC Bank. Country Code : ['.$zf_filter_input->country_code.'], Country Name : ['.$country_name.']');
					$this->view->success = true;
					$this->view->report_msg = array();

					// $this->_redirect('/notification/success/index');
				}
				catch(Exception $e)
				{
					//rollback changes
					$this->_db->rollBack();

					foreach(array_keys($filters) as $field)
						$this->view->$field = $this->_getParam($field);

					$errorMsg = 'exception';
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
				}
			}
			else
			{
				$this->view->error = true;
				foreach(array_keys($filters) as $field)
						$this->view->$field = $this->_getParam($field);

				$error = $zf_filter_input->getMessages();

				//format error utk ditampilkan di view html
                $errorArray = null;
		        foreach($error as $keyRoot => $rowError)
		        {
		           foreach($rowError as $errorString)
		           {
		              $errorArray[$keyRoot] = $errorString;
		           }
		        }

		        $this->view->succes = false;
                $this->view->report_msg = $errorArray;
			}
		}

		Application_Helper_General::writeLog('OFAD','Add OFAC Bank');
	}

}
