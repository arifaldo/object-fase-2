<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';
class systemlog_TransactionlogController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$model = new systemlog_Model_Systemlog();

		$activity = $model->getFPrivilege();

		$login = array('FPRIVI_ID' => 'FLGN', 'FPRIVI_DESC' => 'Login');
		$logout = array('FPRIVI_ID' => 'FLGT', 'FPRIVI_DESC' => 'Logout');
		$changepass = array('FPRIVI_ID' => 'CHMP', 'FPRIVI_DESC' => 'Change My Password');
		array_unshift($activity, $changepass);
		array_unshift($activity, $logout);
		array_unshift($activity, $login);
		//Application_Helper_General::writeLog('RPCT','View Activity Report');	

		/*$activityarr = Application_Helper_Array::listArray($activity,'FPRIVI_ID','FPRIVI_DESC');
		asort($activityarr);		
		unset($activityarr[MOFT]);
		unset($activityarr[CRDI]);
		unset($activityarr[CRIP]);
		unset($activityarr[MAKT]);*/

		// $select = $this->_db->select()->distinct()
		// 								->from(
		// 											array('T_INTERFACE_LOG'),
		// 											array(
		// 													'FUNCTION_NAME',
		// 												)
		// 										);
		// $arrfunction 				= $this->_db->fetchAll($select);
		$arrfunction = array();
		$this->view->arrfunction 	= $arrfunction;

		$function_name_lg = $this->language->_('Function Name');
		$string_lg = $this->language->_('String');
		$log_date_lg = $this->language->_('Log Date');
		$uid_lg = $this->language->_('UID');
		$action_lg = $this->language->_('Action');

		$fields = array(
			'FUNCTION_NAME'     => array(
				'field' => 'FUNCTION_NAME',
				'label' => $function_name_lg,
				'sortable' => true
			),
			'STRING'     		=> array(
				'field' => 'STRING',
				'label' => $string_lg,
				'sortable' => true
			),
			'UID'  				=> array(
				'field' => 'UID',
				'label' => $uid_lg,
				'sortable' => true
			),
			'LOG_DATE'      	=> array(
				'field' => 'LOG_DATE',
				'label' => $log_date_lg,
				'sortable' => true
			),
			'ACTION'   			=> array(
				'field' => 'ACTION',
				'label' => $action_lg,
				'sortable' => true
			)
		);

		$filterlist = array('PS_LOG_DATE', 'FUNCTION_NAME', 'STRING', 'UID');
		$this->view->filterlist = $filterlist;

		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby', 'LOG_ID');
		$sortDir = $this->_getParam('sortdir', 'desc');
		$filter_clear    = $this->_getParam('clearfilter');

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;

		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');

		$filterArr = array(
			'filter'				=> array('StripTags', 'StringTrim'),
			//'SEARCH_CUST'    	=> array('StripTags','StringTrim','StringToUpper'),
			'STRING'		  		=> array('StripTags', 'StringTrim', 'StringToUpper'),
			'UID'    			=> array('StripTags', 'StringTrim', 'StringToUpper'),
			//'FullName'    		=> array('StripTags','StringTrim','StringToUpper'),
			'FUNCTION_NAME' 	=> array('StripTags', 'StringToUpper'),
			'PS_LOG_DATE' 			=> array('StripTags', 'StringTrim'),
			'PS_LOG_DATE_END' 			=> array('StripTags', 'StringTrim'),
			//'Description'    	=> array('StripTags','StringTrim','StringToUpper'),
		);

		$validator = array(
			'filter'			 	=> array(),
			//'SEARCH_CUST'   	 	=> array(),
			'STRING'  		  	=> array(),
			'UID'    			=> array(),
			//'FullName'    		=> array(),
			'FUNCTION_NAME' 	=> array(),
			'PS_LOG_DATE' 			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
			'PS_LOG_DATE_END' 			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
			//'Description'    	=> array(),
		);

		$dataParam = array('FUNCTION_NAME', 'STRING', 'UID');
		$dataParamValue = array();

		// $clean2 = array_diff($this->_request->getParam('wherecol'), $dataParam);
		// $dataParam = array_diff($this->_request->getParam('wherecol'), $clean2);
		// print_r($dataParam);die;

		// print_r($output);die;
		// print_r($this->_request->getParam('wherecol'));
		foreach ($dataParam as $no => $dtParam) {

			if (!empty($this->_request->getParam('wherecol'))) {
				$dataval = $this->_request->getParam('whereval');
				// print_r($dataval);
				$order = 0;
				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if ($value == "PS_LOG_DATE") {
						$order--;
					}
					if ($dtParam == $value) {
						$dataParamValue[$dtParam] = $dataval[$order];
					}
					$order++;
				}
			}
		}
		// print_r($dataParamValue);
		// die;	
		// print_r($this->_request->getParam('whereval'));die;

		if (!empty($this->_request->getParam('logdate'))) {
			$logarr = $this->_request->getParam('logdate');
			$dataParamValue['PS_LOG_DATE'] = $logarr[0];
			$dataParamValue['PS_LOG_DATE_END'] = $logarr[1];
		}

		$fParam = array();
		$datefrom = "";
		$dateto = "";

		$zf_filter = new Zend_Filter_Input($filterArr, $validator, $dataParamValue);
		// $filter 		= $zf_filter->getEscaped('filter');
		$filter 		= $this->_getParam('filter');
		$string 	= $fParam['string']		= html_entity_decode($zf_filter->getEscaped('STRING'));
		$uid 		= $fParam['uid']		= html_entity_decode($zf_filter->getEscaped('UID'));
		$function 	= $fParam['function']	= html_entity_decode($zf_filter->getEscaped('FUNCTION_NAME'));
		$datefrom 	= $fParam['datefrom']	= html_entity_decode($zf_filter->getEscaped('PS_LOG_DATE'));
		$dateto	 	= $fParam['dateto']		= html_entity_decode($zf_filter->getEscaped('PS_LOG_DATE_END'));

		if ($filter == null) {
			$datefrom = (date("d/m/Y"));
			$dateto = (date("d/m/Y"));
			$this->view->fDateFrom  = (date("d/m/Y"));
			$this->view->fDateTo  = (date("d/m/Y"));
		}

		if ($filter == null || $filter == TRUE) {
			$this->view->fDateTo    = $dateto;
			$this->view->fDateFrom  = $datefrom;
		}

		if (!empty($datefrom)) {
			$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
			$fParam['datefrom']  = $FormatDate->toString($this->_dateDBFormat);
		}

		if (!empty($dateto)) {
			$FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
			$fParam['dateto']    = $FormatDate->toString($this->_dateDBFormat);
		}

		if ($filter == TRUE) {

			if ($function) {
				$this->view->function = $function;
			}

			if ($string) {
				$this->view->string = $string;
			}

			if ($uid) {
				$this->view->uid = $uid;
			}
		}
		if ($filter_clear == TRUE) {
			$datefrom = '00/00/0000';
			$dateto = (date("d/m/Y"));
			$this->view->logdateStart  = '';
			$this->view->logdateEnd  = '';

			$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
			$fParam['datefrom']  = $FormatDate->toString($this->_dateDBFormat);

			$FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
			$fParam['dateto']    = $FormatDate->toString($this->_dateDBFormat);
		}

		$data = $model->getTransactionLog($fParam, $sortBy, $sortDir, $filter);
		$data = $data;

		// foreach ($data as $key => $value) {
		// 	if ($value['ACTION'] == 'R') {
		// 		$str = '';
		// 		foreach ($value['STRING'] as $keys => $values) {
		// 			$str .= $keys.'='.$values.'&';
		// 		}

		// 		$data[$key]['STRING'] = $str;
		// 	}
		// }
		//echo '<pre>';

		$this->paging($data);


		if ($pdf || $csv) {
			$arr = $data;

			foreach ($arr as $key => $value) {
				$arr[$key]["LOG_DATE"] = Application_Helper_General::convertDate($value["LOG_DATE"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
			}

			if ($csv) {
				Application_Helper_General::writeLog('SYLG', 'Download CSV Transaction Log');
				$this->_helper->download->csv(array('Function Name', 'String', 'UID', 'Log Date', 'Action'), $arr, null, 'Transaction Log');
			}

			if ($pdf) {
				Application_Helper_General::writeLog('SYLG', 'Download PDF Transaction Log');
				$this->_helper->download->pdf(array('Function Name', 'String', 'UID', 'Log Date', 'Action'), $arr, null, 'Transaction Log');
			}
		} else {
			Application_Helper_General::writeLog('ILOG', 'View Transaction Log');
		}

		$this->view->fields = $fields;
		$this->view->filter = $filter;

		if (!empty($dataParamValue)) {
			$this->view->logdateStart = $dataParamValue['PS_LOG_DATE'];
			$this->view->logdateEnd = $dataParamValue['PS_LOG_DATE_END'];

			unset($dataParamValue['PS_LOG_DATE_END']);
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
			$this->view->wherecol     = $wherecol;
			$this->view->whereval     = $whereval;
			// print_r($whereval);die;
		}
	}
}
