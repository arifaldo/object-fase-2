<?php
require_once 'Zend/Controller/Action.php';
class setbilleronboardcharges_SuggestiondetailController extends Application_Main
{
	protected $_moduleDB = 'RCM';

  	public function indexAction()
  	{
		$model = new setbilleronboardcharges_Model_Setbilleronboardcharges();
		$modelBiller = new biller_Model_Biller();
		
  		$this->_helper->layout()->setLayout('popup');
  		$params = $this->_request->getParams();

  		if(is_array($params) && count($params))
  			$fullDesc = Application_Helper_General::displayFullDesc($params);
  		else
  			$fullDesc = null;

  		if(array_key_exists('changes_id', $params))
  		{
  			$filters = array('changes_id' => array('StripTags', 'StringTrim'));
  			$validators = array(
  				'changes_id' => array(
  					'NotEmpty',
  					'Digits',
					'messages' => array(
						'No Suggestion ID',
						'Wrong ID Format',
					),
  				),
  			);

  			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $params, $this->_optionsValidator);
	  		if($zf_filter_input->isValid())
	  		{
				$tempData = $masterData = null;
	  			$changeId = $zf_filter_input->changes_id;
				$changeInfo = array();
				$changeInfo = $this->getGlobalChanges($changeId);

				if(empty($changeInfo))
				{
					$this->_redirect('/notification/invalid/index');
				}
				else{
					if(!in_array($changeInfo['CHANGES_STATUS'],array('RR','WA')))
					{
						$this->_redirect('/notification/invalid/index');
					}
					
					$tempData = $modelBiller->getDetailProviderTemp($changeId);
					$masterData = $modelBiller->getDetailProvider($tempData['PROVIDER_ID']);
					
					$this->view->suggested_by = $changeInfo['CREATED_BY'];
					$this->view->suggestion_date = $changeInfo['CREATED'];
					$this->view->temp_data = $tempData;
					$this->view->master_data = $masterData;
					$this->view->changes_id = $changeId;
				}
	  		}
	  		else
	  		{
	  			$errors = $zf_filter_input->getMessages();
	  			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
	  			$this->_helper->getHelper('FlashMessenger')->addMessage($errors['changes_id']);
	  		}
  		}
  		else
  		{
  			$errorRemark = 'No Suggestion ID';
	  		$this->_helper->getHelper('FlashMessenger')->addMessage('F');
	  		$this->_helper->getHelper('FlashMessenger')->addMessage($errorRemark);
    	}
    	if(!$this->_request->isPost()){
    		Application_Helper_General::writeLog('VWBC','Viewing Suggestion Detail E-Commerce Charges for '.$tempData['PROVIDER_NAME']);	
    	}
		
	}
}