<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';

class bankforexlimit_DeleteController extends Application_Main 
{

    protected $_moduleDB = 'RTF'; //masih harus diganti

	public function indexAction() 
	{ 
		$model = new bankforexlimit_Model_Bankforexlimit();
		
	    //pengaturan url untuk button back
	    $this->setbackURL('/'.$this->view->modulename);

	    $filters = array('ccy_id' => array('StringTrim', 'StripTags'));
							 
		$validators =  array(
					    'ccy_id' => array(
								      'NotEmpty', 
							          array('Db_RecordExists', array('table' => 'M_FOREX', 'field' => 'CCY')),
							       	  'messages' => array(
							   						  $this->language->_('Cannt be empty'),
							   					      $this->language->_('Product ID is not founded'),
												) 
							             )
					        );
			
		if(array_key_exists('ccy_id',$this->_request->getParams()))
		{
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			$error = $zf_filter_input->getMessages();
			if(empty($error))
			{
				try 
				{   
					Application_Helper_General::writeLog('PAIM','Delete Bank Forex Limit,CCY ID : '.$this->_getParam('ccy_id'));
				    $this->_db->beginTransaction();
					$ccy_id  = $zf_filter_input->getEscaped('ccy_id');
				   	$model->deleteData($ccy_id);
				   	
				
					
					$this->_db->commit();
					$this->_redirect('/notification/success/index'); 
					
				}
				catch(Exception $e) 
				{
					//rollback changes
					$this->_db->rollBack();
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
					$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/failed');
				}
			}
			else
			{
				$this->view->error = true;
				$errors = $zf_filter_input->getMessages();
				
				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				foreach($errors as $key => $error)
				{
					foreach($error as $key2 => $errorMsg)
					{
						$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
						
						Application_Helper_General::writeLog('PAIM','Update Product Type');
						
						$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/failed');
						
					}					
				}	
			}
		}
		else
		{
			$errorMsg = '';
			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
			$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
					
			Application_Helper_General::writeLog('OBDE','Delete others bank');
			
			$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/failed');
		}
	}
	
	
	public function successAction()
	{
		$this->_redirect($this->_backURL);
	}
	
	public function failedAction()
	{
		$this->_redirect($this->_backURL);
	}

}
