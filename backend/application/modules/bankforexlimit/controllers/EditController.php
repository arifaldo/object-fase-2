<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';

class bankforexlimit_EditController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti


    public function indexAction()
	{
		$model = new bankforexlimit_Model_Bankforexlimit();

	    //pengaturan url untuk button back
	    $this->setbackURL('/'.$this->_request->getModuleName().'/index');

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}
    	}


    	$this->view->report_msg = array();

		$ccy_id = $this->_getParam('ccy_id');
		$this->view->ccy_id        = $ccy_id;
// 		$ccy_id = (Zend_Validate::is($ccy_id,'Digits'))? $ccy_id : null;

		if(!empty($ccy_id))
		{
			$temp = $model->getDetail($ccy_id);
// 			print_r($temp);die;
			if(empty($temp))
			{
				$this->_redirect('/bankforexlimit');
			}
			$resultdata = $temp;
			if($resultdata)
			{
				$this->view->ccyCode        = $resultdata['CCY'];
				$this->view->max_buy        = $resultdata['BUY_AMOUNT'];
				$this->view->max_sell      = $resultdata['SELL_AMOUNT'];


				if($this->_request->isPost())
				{
					$max_buy = $this->_getParam('max_buy');
					$max_buy_val =	Application_Helper_General::convertDisplayMoney($max_buy);
					$this->_setParam('max_buy',$max_buy_val);
						
					$max_sell = $this->_getParam('max_sell');
					$max_sell_val =	Application_Helper_General::convertDisplayMoney($max_sell);
					$this->_setParam('max_sell',$max_sell_val);
					
					$filters = array('ccyCode'       => array('StringTrim','StripTags'),
									 'max_buy'       => array('StringTrim','StripTags'),
									 'max_sell'     => array('StringTrim','StripTags'),
									);
					
// 					$PRODUCT_PLAN = "PRODUCT_PLAN <> '".$resultdata['PRODUCT_PLAN']."'";
// 					$PRODUCT_NAME = "PRODUCT_NAME <> '".$resultdata['PRODUCT_NAME']."'";
					$validators = array(						
											'max_buy'  => array('allowEmpty'=> TRUE,
															array('Between', array('min'=>1,'max'=>9999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															('Can not be empty',
															'Must be numeric values',
															'Value between 1 - 9999999999.99',
															'Value between 1 - 9999999999.99',	)),

											'max_sell' => array('allowEmpty'=> TRUE,
															array('Between', array('min'=>1,'max'=>9999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															('Can not be empty',
															'Must be numeric values',
															'Value between 1 - 9999999999.99',
															'Value between 1 - 9999999999.99',	)),
										   );

					$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
					$error = $zf_filter_input->getMessages();
// 					print_r($error);die;
					if(empty($error))
					{
						$content = array(
										
										'BUY_AMOUNT' 	=> $zf_filter_input->max_buy,
										'SELL_AMOUNT' 	=> $zf_filter_input->max_sell,
										'UPDATEDBY'  => $this->_userIdLogin,
										'UPDATED'  => new Zend_Db_Expr('GETDATE()')
										   );

						try
						{
							
							//-----insert--------------
							$this->_db->beginTransaction();

							$model->updateData($ccy_id,$content);

							$this->_db->commit();

							$this->fillParam($zf_filter_input);
							$this->_redirect('/notification/success/index');
						}
						catch(Exception $e)
						{
							//rollback changes
							$this->_db->rollBack();

							$this->fillParam($zf_filter_input);

							$errorMsg = 'exception';
							$this->_helper->getHelper('FlashMessenger')->addMessage('F');
							$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
						}
							Application_Helper_General::writeLog('BXUD','Update Bank Forex Limit,CCY ID : '.$ccy_id);
	
					}
					else
					{
						$this->view->error = true;
						$this->fillParam($zf_filter_input);
						$error = $zf_filter_input->getMessages();

						//format error utk ditampilkan di view html
						foreach($error as $keyRoot => $rowError)
						{
						   foreach($rowError as $errorString)
						   {
								$errID = 'error_'.$keyRoot;
								$this->view->$errID = $errorString;
						   }
						}
						$this->view->error = true;
					}
				}
			}
		}
		else
		{
		   $err = $this->language->_('No Data');
		}
		Application_Helper_General::writeLog('BXUD','Update Bank Forex Limit,CCY ID : '.$ccy_id);
	}


    private function fillParam($zf_filter_input)
	{
		$this->view->ccyCode = ($zf_filter_input->isValid('ccyCode')) ? $zf_filter_input->ccyCode : $this->_getParam('ccyCode');
		$this->view->max_buy = ($zf_filter_input->isValid('max_buy')) ? $zf_filter_input->max_buy : $this->_getParam('max_buy');
		$this->view->max_sell     = ($zf_filter_input->isValid('max_sell')) ? $zf_filter_input->max_sell : $this->_getParam('max_sell');
	}



}