<?php

require_once 'Zend/Controller/Action.php';

class Nostrobank_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

	   $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		 if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}

    	$ccy = 'CCY';
    	$country = $this->language->_('Country');
		//$benefbank = $this->language->_('Beneficiary Bank Name');
		$nostrobank = $this->language->_('Nostro Bank Name');
		$nostroswift = $this->language->_('Nostro Swift Code');
		$created = $this->language->_('Created Date');
		$createdby = $this->language->_('Created By');
		$updated = $this->language->_('Updated Date');
		$updatedby = $this->language->_('Updated By');
		
		$model = new nostrobank_Model_Nostrobank();
	    $this->view->ccyArr = ( array(''=>'-- '.$this->language->_('Please Select').' --')+Application_Helper_Array::listArray($model->getCCYList(),'CCY_ID','CCY_ID'));
	    $this->view->nostroArr = ( array(''=>'-- '.$this->language->_('Please Select').' --')+Application_Helper_Array::listArray($model->getNostroNames(),'NOSTRO_NAME','NOSTRO_NAME'));
		
	    $fields = array(
						'ccy'  => array('field' =>'CCY',
											      'label' => $ccy,
											      'sortable' => true),
						/*'country_name'     => array('field' => 'COUNTRY_NAME',
											      'label' => $country,
											      'sortable' => true),
						'bank_name'      => array('field' => 'BANK_NAME',
											      'label' => $benefbank,
											      'sortable' => true),*/
						'nostro_name'      => array('field' => 'NOSTRO_NAME',
											      'label' => $nostrobank,
											      'sortable' => true),
						'nostro_swift'      => array('field' => 'NOSTRO_SWIFTCODE',
											      'label' => $nostroswift,
											      'sortable' => true),
						'created'      => array('field' => 'CREATED',
											      'label' => $created,
											      'sortable' => true),
						'createdby'      => array('field' => 'CREATEDBY',
											      'label' => $createdby,
											      'sortable' => true),
						'updated'      => array('field' => 'UPDATED',
											      'label' => $updated,
											      'sortable' => true),
						'updatedby'      => array('field' => 'UPDATEDBY',
											      'label' => $updatedby,
											      'sortable' => true)
				      );


	    $filterlist = array('NOSTRO_ID','CCY');
		
		$this->view->filterlist = $filterlist;
		//get page, sortby, sortdir
		    $page = $this->_getParam('page');
    
    
    
    		$page = (Zend_Validate::is($page,'Digits'))? $page : 1;
    
    //validasi sort, jika input sort bukan ASC atau DESC
		$sortBy  = $this->_getParam('sortby');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	'filter' 	  	 => array('StringTrim','StripTags'),
							'CCY'  => array('StringTrim','StripTags'),
							//'bank_name'      => array('StringTrim','StripTags'),
							'NOSTRO_ID'      => array('StringTrim','StripTags')
		);


		 $dataParam = array("CCY","NOSTRO_ID");
		$dataParamValue = array();
		// print_r($this->_request->getParam('wherecol'));die;
		foreach ($dataParam as $dtParam)
		{
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}
				
			}

		}

		$zf_filter = new Zend_Filter_Input($filterArr,array(),$dataParamValue);
		// $filter = $zf_filter->getEscaped('filter');
		$filter 		= $this->_getParam('filter');

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');	
		//$filter_lg = $this->_getParam('filter');	

		//$getData = Application_Helper_Array::SimpleArray($fields,'field');

		$select = $this->_db->select()
					        ->from(array('A' => 'M_NOSTRO_BANK'),array('NOSTRO_ID','CCY','NOSTRO_NAME','NOSTRO_SWIFTCODE','CREATED','CREATEDBY','UPDATED','UPDATEDBY'));
					        //->join(array('B' => 'M_BANK_REMITTANCE'),'A.BANK_CODE = SUBSTR(B.bank_code,1,8)',array());


		//if($filter == 'Set Filter' || $filter == 'Mengatur Filter' || $csv || $pdf) 
		if($filter == true || $csv || $pdf || $this->_request->getParam('print')) 
		{  
		    $header = Application_Helper_Array::simpleArray($fields,'label');

			$fccy = $zf_filter->getEscaped('CCY');
			//$fbenef_bank     = $zf_filter->getEscaped('bank_name');
			$fnostro_name    = $zf_filter->getEscaped('NOSTRO_ID');

	        if($fccy) $select->where('UPPER(CCY) LIKE '.$this->_db->quote('%'.strtoupper($fccy).'%'));
	        //if($fbenef_bank)     $select->where('UPPER(A.BANK_CODE) LIKE '.$this->_db->quote('%'.strtoupper($fbenef_bank).'%'));
	        if($fnostro_name)    $select->where('UPPER(NOSTRO_NAME) LIKE '.$this->_db->quote('%'.strtoupper($fnostro_name).'%'));
			
			$this->view->ccy = $fccy;
			//$this->view->benef_bank     = $fbenef_bank;
			$this->view->nostro_name    = $fnostro_name;
		}

		//$this->view->success = true;
		$select->order($sortBy.' '.$sortDir);  
		
		$select = $this->_db->fetchall($select);

		//menghilangkan index/key BANK_CODE utk kepentingan CSV dan PDF
		$selectPdfCsv = $select;
		foreach($selectPdfCsv as $key => $row)
		{
		    unset($selectPdfCsv[$key]['NOSTRO_ID']);
		}
		

		//--------konfigurasicsv dan pdf---------
	    if($csv)
	    {		
				$this->_helper->download->csv($header,$selectPdfCsv,null,'NOSTRO BANK');
				Application_Helper_General::writeLog('NBLS','Download CSV Nostro Bank');
		}
		else if($pdf)
		{
				$this->_helper->download->pdf($header,$selectPdfCsv,null,'NOSTRO BANK');
				Application_Helper_General::writeLog('NBLS','Download PDF Nostro Bank');
		}
		else if($this->_request->getParam('print') == 1){
             $this->_forward('print', 'index', 'widget', array('data_content' => $select, 'data_caption' => 'Nostro Bank', 'data_header' => $fields));
       	}
		else
		{		
				Application_Helper_General::writeLog('NBLS','View Nostro Bank');
		}	
		//-------END konfigurasicsv dan pdf------------

		$this->paging($select);
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     
      }

	}

}