<?php

class Emailbackconf_IndexController extends Application_Main {
	
	protected $_moduleDB  = 'WLT';
	
	public function indexAction() {
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->msg = $msg;
     		}	
    	}
							
		$suggestedText = $this->_db->fetchOne(
							$this->_db->select()->from (
							 	array ('M_SETTING'), 
							 	array ('SETTING_VALUE'))
							 	->where('LOWER(SETTING_ID) = ?','welcome_text')
							);

		$isPendingAvailable = $this->_db->fetchOne(
									$this->_db->select()->from (
									 	array ('TEMP_SETTING'), 
									 	array ('SETTING_VALUE'))
									 	->where('LOWER(SETTING_ID) = ?','welcome_text')
							 );
							 
		$this->view->disableFlag = ($isPendingAvailable==true) ? true :  false;
		//Zend_Debug::dump($this->view->disableFlag);
		$this->view->suggestedText = $suggestedText;
			
		//insert log
		try {
	  		$this->_db->beginTransaction();
	  		$this->backendLog('V',$this->_moduleDB,$this->_moduleDB,null,null);
	  		$this->_db->commit();
		}
    	catch(Exception $e){
 	  		$this->_db->rollBack();
	  		SGO_Helper_GeneralLog::technicalLog($e);
		}
	}		
}

?>