<?php

class Welcome_EditController extends Application_Main {
	
	protected $_moduleDB  = 'WLT';
	
	public function indexAction() {
		$error_remark = null;
        $isPendingAvailable = $this->_db->fetchOne(
									$this->_db->select()->from (
									 	array ('TEMP_SETTING'), 
									 	array ('SETTING_VALUE'))
									 	->where('SETTING_ID = ?','welcome_text')
							 );
							 
		if($isPendingAvailable==true)
		{
			$error_remark = $this->getErrorRemark('03','Welcome Text');
			//insert log
			try {
	  			$this->_db->beginTransaction();
	  			if($this->_request->isPost()){
	    			$action = 'E'; $fulldesc = $this->displayFullDesc($this->_request->getPost());
	  			}else{ $action = 'V'; $fulldesc = null; }
	  			$this->backendLog($action,$this->_moduleDB,$this->_moduleDB,$fulldesc,$error_remark);
	  			$this->_db->commit();
			}
    		catch(Exception $e){
 	  			$this->_db->rollBack();
	  			Application_Helper_GeneralLog::technicalLog($e);
			}
			
			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
      		$this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
      		$this->_redirect($this->_backURL);
		}

		
		if ($this->_request->isPost ()) {
			$filters = array('welcometext' => array('StringTrim', 'StripTags'));			
			$validators =  array('welcometext' => array('allowEmpty'=>true, array('StringLength', array('min' => 0, 'max' => 5000)), 'messages' => $this->getErrorRemark('04','WLT')));
			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getPost(),$this->_optionsValidator);
		
			if ($zf_filter_input->isValid ()) {	
						try {
							$this->_db->beginTransaction();
        										
							$arrayTGlobalChanges = array (	'CHANGES_FLAG' => 'B', 
													'CUST_ID' => 'BANK',
													'DISPLAY_TABLENAME' => 'Welcome Text', 
													'CHANGES_INFORMATION' => '',
													'CHANGES_TYPE' => 'E',
													'CHANGES_STATUS' => 'WA', 
													'CHANGES_REASON' => '',
													'MAIN_TABLENAME' => 'M_SETTING',
													'TEMP_TABLENAME' => 'TEMP_SETTING',
													'KEY_FIELD' => 'MODULE_ID',
													'KEY_VALUE' => 'WLT' , 
													'CREATED' => new Zend_Db_Expr("now()"),
													'CREATED_BY' => $this->_userIdLogin,
													'LASTUPDATED' => new Zend_Db_Expr("now()"),
													'MODULE' => 'welcome'
												);
							$this->_db->insert( 'T_GLOBAL_CHANGES', $arrayTGlobalChanges );							
									
							$latestId = $this->_db->fetchOne('SELECT @@identity AS LASTID');									
							
							$arrayTempSetting = array (	'CHANGES_ID' => $latestId,
													'SETTING_ID' => 'welcome_text',
													'SETTING_VALUE' => $zf_filter_input->welcometext,
													'MODULE_ID' => 'WLT'										
												);
												
							$this->_db->insert( 'TEMP_SETTING', $arrayTempSetting );
							$this->_db->commit ();
						} catch ( Exception $e ) {
							$this->_db->rollBack ();
							SGO_Helper_GeneralLog::technicalLog($e);					
							$error_remark = $this->getErrorRemark('82');
						}
						
						//insert log
						try {
	  						$this->_db->beginTransaction();
	  						$fulldesc = $this->displayFullDesc($this->_request->getPost());
	  						$this->backendLog('E',$this->_moduleDB,$this->_moduleDB,$fulldesc,$error_remark);
	  						$this->_db->commit();
						}
    					catch(Exception $e){
 	  						$this->_db->rollBack();
	  						SGO_Helper_GeneralLog::technicalLog($e);
						}
						
						if($error_remark){ $class = 'F'; $msg = $error_remark; }
						else{ $class = 'S'; $msg = $this->getErrorRemark('00', 'Welcome Text'); }
						$this->_helper->getHelper('FlashMessenger')->addMessage($class);
      					$this->_helper->getHelper('FlashMessenger')->addMessage($msg);
      					$this->_redirect($this->_backURL);
				}else{
					$errors = $zf_filter_input->getMessages ();
					if(count($errors))$error_remark = SGO_Helper_GeneralFunction::displayErrorRemark($errors);
					$this->view->suggestedText = ($zf_filter_input->isValid('welcometext'))? $zf_filter_input->welcometext : $this->_getParam('welcometext');
					$this->view->wltError = (isset($errors['welcometext']))? $errors['welcometext'] : null;
					$this->view->error = 1;
				}
							
		}else{
			$suggestedText = $this->_db->fetchOne(
							$this->_db->select()->from (
							 	array ('M_SETTING'), 
							 	array ('SETTING_VALUE'))
							 	->where('SETTING_ID = ?','welcome_text')
							);
			$this->view->suggestedText = $suggestedText;
		}
			
		//insert log
		try {
	  		$this->_db->beginTransaction();
	  		if($this->_request->isPost()){
	    		$action = 'E'; $fulldesc = SGO_Helper_GeneralFunction::displayFullDesc($this->_request->getPost());
	  		}else{ $action = 'V'; $fulldesc = null; }
	  		$this->backendLog($action,$this->_moduleDB,$this->_moduleDB,$fulldesc,$error_remark);
	  		$this->_db->commit();
		}
    	catch(Exception $e){
 	  		$this->_db->rollBack();
	  		SGO_Helper_GeneralLog::technicalLog($e);
		}
	}		
}

?>