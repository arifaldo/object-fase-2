<?php

/**
 * privilege mapping for dual control
 *
 * @author
 * @version
 */
class Changebusiness_Model_Privilege
{

	protected $_privilegeMap = array(
		'VSOC' => 'specialobligee',
		'VLFC' => 'linefacility',
		'VRRC' => 'requestrebate',
		'VIBO' => 'insurancebranch',
	);


	protected $_privilegeApproveMap = array(
		'ASOC' => 'specialobligee',
		'ALFC' => 'linefacility',
		'ARRC' => 'requestrebate',
		'MIBO' => 'insurancebranch',
	);

	protected $_privilegeRepairMap = array(
		'RPOC' => 'specialobligee',
		'RPFC' => 'linefacility',
		'RRRC' => 'requestrebate',
		'MIBO' => 'insurancebranch',
	);

	protected $_privilegeRequestRepairMap = array();
	protected $_privilegeRejectMap = array();

	public function __construct()
	{
		$this->_privilegeRepairMap = $this->_privilegeRepairMap;
		$this->_privilegeRequestRepairMap = $this->_privilegeApproveMap;
		$this->_privilegeRejectMap = $this->_privilegeApproveMap;
	}

	public function getAuthorizeModule()
	{

		$myPrivilege =  array_flip(Zend_Registry::get('privilege'));
		$authorizeModuleArr = array_intersect_key($this->_privilegeMap, $myPrivilege);
		 /*echo '<pre>';
		 var_dump($this->_privilegeMap);
		 var_dump($myPrivilege);
		 print_r($authorizeModuleArr );die;*/
		return $authorizeModuleArr;
	}


	public function isAuthorizeApprove($moduleName)
	{
		$myPrivilege =  array_flip(Zend_Registry::get('privilege'));
		$authorizeModuleArr = array_intersect_key($this->_privilegeApproveMap, $myPrivilege);
		$returnValue = false;
		if (is_array($authorizeModuleArr)) {
			$newArr = array_flip($authorizeModuleArr);

			if (array_key_exists('BSBO', $authorizeModuleArr)) {
				$newArr['specialobligee'] = 'BSBO';
				
			}
			
			if (array_key_exists($moduleName, $newArr)) {
				$returnValue =  true;
			}
		}
		return $returnValue;
	}
	public function isAuthorizeReject($moduleName)
	{
		$myPrivilege =  array_flip(Zend_Registry::get('privilege'));
		$authorizeModuleArr = array_intersect_key($this->_privilegeApproveMap, $myPrivilege);
		$returnValue = false;
		if (is_array($authorizeModuleArr)) {
			$newArr = array_flip($authorizeModuleArr);
			if (array_key_exists('BSBO', $authorizeModuleArr)) {
				$newArr['specialobligee'] = 'BSBO';
				
			}

			if (array_key_exists($moduleName, $newArr)) {
				$returnValue =  true;
			}

			
		}
		return $returnValue;
	}
	public function isAuthorizeRequestRepair($moduleName)
	{
		$myPrivilege =  array_flip(Zend_Registry::get('privilege'));
		$authorizeModuleArr = array_intersect_key($this->_privilegeRequestRepairMap, $myPrivilege);
		$returnValue = false;
		if (is_array($authorizeModuleArr)) {
			$newArr = array_flip($authorizeModuleArr);
			if (array_key_exists('CHCA', $authorizeModuleArr)) {
				$newArr['adminfeeaccount'] = 'CHCA';
				$newArr['adminfeecompany'] = 'CHCA';
				$newArr['companycharges'] = 'CHCA';
				$newArr['chargesdomestic'] = 'CHCA';
				$newArr['disbursementcharges'] = 'CHCA';
				$newArr['nationalpoolinggroup'] = 'CHCA';
				$newArr['monthlyaccount'] = 'CHCA';
				$newArr['monthlycompany'] = 'CHCA';
				$newArr['realtimecharges'] = 'CHCA';
				$newArr['settlementcharges'] = 'CHCA';
				$newArr['directdebit'] = 'CHCA';
				$newArr['specialcharges'] = 'CHCA';
				$newArr['trafficapicharges'] = 'CHCA';
			}

			if (array_key_exists('ABAO', $authorizeModuleArr)) {
				$newArr['binsetup'] = 'ABAO';
				$newArr['bintemplate'] = 'ABAO';
				$newArr['corporatedcard'] = 'ABAO';
				$newArr['corporatedebitcard'] = 'ABAO';
				$newArr['activationcard'] = 'ABAO';
				$newArr['bgreport'] = 'ABAO';
				
			}
			
			if (array_key_exists('CCCA', $authorizeModuleArr)) {
				$newArr['customer'] = 'CCCA';
				$newArr['bglimit'] = 'CCCA';
				
			}
			
			if (array_key_exists('APBO', $authorizeModuleArr)) {
				$newArr['billeronboard'] = 'APBO';
				$newArr['remittancechar'] = 'APBO';
				$newArr['specialcharges'] = 'APBO';
				$newArr['localremittancechar'] = 'APBO';
			}
  
			if (array_key_exists('BMCA', $authorizeModuleArr)) {
				$newArr['boundary'] = 'BMCA';
				$newArr['authorizationmatrix'] = 'BMCA';
			}
			
			if (array_key_exists('MLCA', $authorizeModuleArr)) {
				$newArr['userlimit'] = 'MLCA';
				$newArr['useropenlimit'] = 'MLCA';
			}

			

			if (array_key_exists($moduleName, $newArr)) {
				$returnValue =  true;
			}
		}
		return $returnValue;
	}

	public function getPrivilege($moduleName, $action)
	{

		$myPrivilege =  array_flip(Zend_Registry::get('privilege'));
		if ($action == 'Approve') {
			$authorizeModuleArr = array_intersect_key($this->_privilegeApproveMap, $myPrivilege);
		} elseif ($action == 'Reject') {
			$authorizeModuleArr = array_intersect_key($this->_privilegeRejectMap, $myPrivilege);
		} elseif ($action == 'Request Repair') {
			$authorizeModuleArr = array_intersect_key($this->_privilegeRequestRepairMap, $myPrivilege);
		}

		$returnValue = "REQC";
		if (is_array($authorizeModuleArr)) {
			$newArr = array_flip($authorizeModuleArr);
			if (array_key_exists('CHCA', $authorizeModuleArr)) {
				$newArr['adminfeeaccount'] = 'CHCR';
				$newArr['adminfeecompany'] = 'CHCR';
				$newArr['companycharges'] = 'CHCR';
				$newArr['chargesdomestic'] = 'CHCR';
				$newArr['disbursementcharges'] = 'CHCR';
				$newArr['nationalpoolinggroup'] = 'CHCR';
				$newArr['monthlyaccount'] = 'CHCR';
				$newArr['monthlycompany'] = 'CHCR';
				$newArr['realtimecharges'] = 'CHCR';
				$newArr['settlementcharges'] = 'CHCR';
				$newArr['directdebit'] = 'CHCR';
				$newArr['specialcharges'] = 'CHCR';
				$newArr['trafficapicharges'] = 'CHCR';
			}

			if (array_key_exists('ABAO', $authorizeModuleArr)) {
				$newArr['binsetup'] = 'ABAO';
				$newArr['bintemplate'] = 'ABAO';
				$newArr['corporatedcard'] = 'ABAO';
				$newArr['corporatedebitcard'] = 'ABAO';
				$newArr['activationcard'] = 'ABAO';
				$newArr['bgreport'] = 'ABAO';
				
			}
			
			if (array_key_exists('CCCA', $authorizeModuleArr)) {
				$newArr['customer'] = 'CCCA';
				$newArr['bglimit'] = 'CCCA';
				
			}

			if (array_key_exists('BMCA', $authorizeModuleArr)) {
				$newArr['boundary'] = 'BMCA';
				$newArr['authorizationmatrix'] = 'BMCA';
			}
			
			if (array_key_exists('MLCA', $authorizeModuleArr)) {
				$newArr['userlimit'] = 'MLCA';
				$newArr['useropenlimit'] = 'MLCA';
			}

			

			if (array_key_exists($moduleName, $newArr)) {
				$returnValue =  $newArr[$moduleName];
			}
		}
		return $returnValue;
	}


	public function isAuthorizeRepair($moduleName)
	{
		$myPrivilege =  array_flip(Zend_Registry::get('privilege'));
		$authorizeModuleArr = array_intersect_key($this->_privilegeRepairMap, $myPrivilege);
		$returnValue = false;
		if (is_array($authorizeModuleArr)) {
			$newArr = array_flip($authorizeModuleArr);
			if (array_key_exists('CHCR', $authorizeModuleArr)) {
				$newArr['adminfeeaccount'] = 'CHCR';
				$newArr['adminfeecompany'] = 'CHCR';
				$newArr['companycharges'] = 'CHCR';
				$newArr['chargesdomestic'] = 'CHCR';
				$newArr['disbursementcharges'] = 'CHCR';
				$newArr['nationalpoolinggroup'] = 'CHCR';
				$newArr['monthlyaccount'] = 'CHCR';
				$newArr['monthlycompany'] = 'CHCR';
				$newArr['realtimecharges'] = 'CHCR';
				$newArr['settlementcharges'] = 'CHCR';
				$newArr['directdebit'] = 'CHCR';
				$newArr['specialcharges'] = 'CHCR';
				$newArr['trafficapicharges'] = 'CHCR';
			}

			if (array_key_exists('ABAO', $authorizeModuleArr)) {
				$newArr['binsetup'] = 'ABAO';
				$newArr['bintemplate'] = 'ABAO';
				$newArr['corporatedcard'] = 'ABAO';
				$newArr['corporatedebitcard'] = 'ABAO';
				$newArr['activationcard'] = 'ABAO';
				$newArr['bgreport'] = 'ABAO';
				
			}
			
			if (array_key_exists('CCCR', $authorizeModuleArr)) {
				$newArr['customer'] = 'CCCR';
				$newArr['bglimit'] = 'CCCR';
				
			}

			if (array_key_exists('BMCR', $authorizeModuleArr)) {
				$newArr['boundary'] = 'BMCR';
				$newArr['authorizationmatrix'] = 'BMCR';
			}
			
			if (array_key_exists('MLCR', $authorizeModuleArr)) {
				$newArr['userlimit'] = 'MLCR';
				$newArr['useropenlimit'] = 'MLCR';
			}

			
			

			if (array_key_exists($moduleName, $newArr)) {
				$returnValue =  true;
			}
		}
		return $returnValue;
	}
}
