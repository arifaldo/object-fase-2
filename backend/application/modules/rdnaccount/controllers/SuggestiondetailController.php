<?php
require_once 'Zend/Controller/Action.php';

class rdnaccount_SuggestiondetailController extends customeraccount_Model_Customeraccount{

  public function indexAction()
  {
    $change_id = $this->_getParam('changes_id');
    $change_id = (Zend_Validate::is($change_id,'Digits'))? $change_id : 0;

    $this->_helper->layout()->setLayout('newpopup');

    $this->view->suggestionType = $this->_suggestType;

    if($change_id)
    {
  	  $select = $this->_db->select()
                             ->from('T_GLOBAL_CHANGES',array('CHANGES_ID'))
                             ->where('CHANGES_ID='.$this->_db->quote($change_id))
                             ->where("CHANGES_FLAG='B'")
                             ->where("CHANGES_STATUS = 'WA' OR CHANGES_STATUS = 'RR'");
//                             ->where('UPPER(MODULE)='.$this->_db->quote(strtoupper($this->_request->getModuleName())));
      $result = $this->_db->fetchOne($select);

      if(empty($result))  $this->_redirect('/notification/invalid/index');

      if($result)
      {
        //content send to view
        $select = $this->_db->select()
                               ->from(array('T' =>'TEMP_CUSTOMER_ACCT'))
      	                       ->join(array('G' => 'T_GLOBAL_CHANGES'), 'G.CHANGES_ID = T.CHANGES_ID', array('CHANGES_ID','CHANGES_TYPE','CREATED','CREATED_BY','CHANGES_STATUS','READ_STATUS'))
      	                       ->joinleft(array('U'=>'M_GROUPING'),'T.GROUP_ID=U.GROUP_ID',array('GROUP_NAME'))
	  				           ->where('T.CHANGES_ID = ?', $change_id);
      	$resultdata = $this->_db->fetchRow($select);
      	//Zend_Debug::dump($resultdata);

      	if($result['TEMP_ID'])
      	{
      	   //suggest data
	  			$this->view->changes_id     = $resultdata['CHANGES_ID'];
	  			$this->view->changes_type   = $resultdata['CHANGES_TYPE'];
	  			$this->view->changes_status = $resultdata['CHANGES_STATUS'];
	  			$this->view->read_status    = $resultdata['READ_STATUS'];
	  			$this->view->created        = $resultdata['CREATED'];
	  			$this->view->created_by     = $resultdata['CREATED_BY'];


	  	   //customer account data
      	   $select = $this->_db->select()
  	                           ->from('M_CUSTOMER',array('CUST_ID','CUST_NAME'))
  	                           ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$resultdata['CUST_ID']));
           $result = $this->_db->fetchRow($select);
           $this->view->cust_name    = $result['CUST_NAME'];

      	   $this->view->cust_id      = $resultdata['CUST_ID'];
           $this->view->change_type  = $this->_changeType;
           $this->view->status_type  = $this->_masterglobalstatus;
           $this->view->login_type   = $this->_masterhasStatus;

      	   $this->view->acct_no      = $resultdata['ACCT_NO'];
           $this->view->acct_status  = $resultdata['ACCT_STATUS'];
           $this->view->acct_name    = $resultdata['ACCT_NAME'];
           $this->view->acct_desc    = $resultdata['ACCT_DESC'];

           $this->view->acct_email   = $resultdata['ACCT_EMAIL'];
           $this->view->ccy_id       = $resultdata['CCY_ID'];
           $this->view->order_no     = $resultdata['ORDER_NO'];
           $this->view->group_name   = $resultdata['GROUP_NAME'];


           $this->view->acct_created      =  $resultdata['ACCT_CREATED'];
           $this->view->acct_createdby    =  $resultdata['ACCT_CREATEDBY'];
           $this->view->acct_suggested    =  $resultdata['ACCT_SUGGESTED'];
           $this->view->acct_suggestedby  =  $resultdata['ACCT_SUGGESTEDBY'];
           $this->view->acct_updated      =  $resultdata['ACCT_UPDATED'];
           $this->view->acct_updatedby    =  $resultdata['ACCT_UPDATEDBY'];



           //master customer account
	  	   $select = $this->_db->select()
	 				         ->from(array('CA'=>'M_CUSTOMER_ACCT'),array('*'))
		  				     ->joinleft(array('g'=>'M_GROUPING'),'g.GROUP_ID=CA.GROUP_ID',array('GROUP_NAME'))
		  				     ->where('UPPER(CA.ACCT_NO)='.$this->_db->quote((string)$resultdata['ACCT_NO']))
		  				     ->order(array('ORDER_NO ASC'))	;

      	   $m_resultdata = $this->_db->fetchRow($select);
           //Zend_Debug::dump($m_resultdata);

           $this->view->m_acct_no      = $m_resultdata['ACCT_NO'];
           $this->view->m_acct_status  = $m_resultdata['ACCT_STATUS'];
           $this->view->m_acct_name    = $m_resultdata['ACCT_NAME'];
           $this->view->m_acct_desc    = $m_resultdata['ACCT_DESC'];

           $this->view->m_cust_id      = $m_resultdata['CUST_ID'];
           $this->view->m_cust_name     = $result['CUST_NAME'];

           $this->view->m_acct_email   = $m_resultdata['ACCT_EMAIL'];
           $this->view->m_ccy_id       = $m_resultdata['CCY_ID'];
           $this->view->m_order_no     = $m_resultdata['ORDER_NO'];
           $this->view->m_group_name   = $m_resultdata['GROUP_NAME'];


           $this->view->m_acct_created      =  $m_resultdata['ACCT_CREATED'];
           $this->view->m_acct_createdby    =  $m_resultdata['ACCT_CREATEDBY'];
           $this->view->m_acct_suggested    =  $m_resultdata['ACCT_SUGGESTED'];
           $this->view->m_acct_suggestedby  =  $m_resultdata['ACCT_SUGGESTEDBY'];
           $this->view->m_acct_updated      =  $m_resultdata['ACCT_UPDATED'];
           $this->view->m_acct_updatedby    =  $m_resultdata['ACCT_UPDATEDBY'];



        /*if($resultdata['USER_ISEMAIL']) $userIsEmail = 'Yes';
	    else $userIsEmail = 'No';
        $this->view->userIsEmail = $userIsEmail;*/


       	}
       	else{ $change_id = 0; }
      }
      else{ $change_id = 0; }
    }





    if(!$change_id)
    {
      // $error_remark = $this->getErrorRemark('22','Suggestion ID');
      $error_remark =$this->language->_('Invalid Suggestion ID');
      //insert log
	 /*  try {
	  	$this->_db->beginTransaction();
	    $this->backendLog(strtoupper($this->_actionID['view']),strtoupper($this->_moduleID['user']),null,null,$error_remark);
		$this->_db->commit();
	  }
      catch(Exception $e){
 		$this->_db->rollBack();
 		SGO_Helper_GeneralLog::technicalLog($e);
	  } */

      $this->_helper->getHelper('FlashMessenger')->addMessage('F');
	  $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
      //$this->_redirect($this->_backURL);
      $this->_redirect('/popuperror/index/index');
    }

    //insert log
	// try
	// {
	  // $this->_db->beginTransaction();
    if(!$this->_request->isPost()){
	  Application_Helper_General::writeLog('ACCL','View Bank Account Changes List');
    }

	  // $this->_db->commit();
	// }
    // catch(Exception $e){
 	  // $this->_db->rollBack();
 	  // SGO_Helper_GeneralLog::technicalLog($e);
	// }
    $this->view->changes_id = $change_id;
  }
}
