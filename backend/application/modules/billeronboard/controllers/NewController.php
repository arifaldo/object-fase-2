<?php


require_once 'Zend/Controller/Action.php';


class billeronboard_NewController extends Application_Main
{
	
	public function generatePaymentID($forTransaction)
	{
		$currentDate = date("Ymd");
		// $seqNumber	 = $this->getPaymentCounter($forTransaction);
		$seqNumber	 = strtoupper(Application_Helper_General::str_rand(9));
		$checkDigit  = '';
		//$checkDigit  = strtoupper(substr(MD5($currentDate.$seqNumber),-3));
		// $checkDigit  = strtoupper(substr(preg_replace("/[^a-zA-Z0-9]/", "", uniqid()),-3));
		$paymentID   = $currentDate.$seqNumber.$checkDigit;
	
		return $paymentID;
	}
	
	public function indexAction() 
	{
		// min max untuk provider Code and Name Length
  		$min = 4;
		$max = 30;
		$max_billercode = 10;
		
		// service type
		$serviceType = 1; //payment
		
		$model 			 = new billeronboard_Model_Billeronboard();
		$getServiceType 	 = $model->getServiceType($serviceType);
// 		Zend_Debug::dump($getServiceType);
// 		$getServiceType[0] = array('SERVICE_ID' => '', 'SERVICE_NAME' => 'E-Commerce Payment');
		if(!empty($getServiceType))
		{
			foreach($getServiceType as $value)
			{
				$arrProvider[$value['SERVICE_OF']] = $value['SERVICE_NAME'];
			}
		}
		
		//$arrProviderType = $arrProvider;
		$arrProviderType = array(1=>'Payment');
//		$this->view->arrProviderType = array('0'=> '--- Please Select ---')+$arrProviderType;
		$this->view->arrProviderType = $arrProviderType;
		$this->view->arrServiceType = $arrProvider;
		
		$this->view->keyId = $this->generatePaymentID(1);
		
		
		$this->view->providerType = 0;// set default
		if($this->_request->isPost())
		{
			$providerName = $this->_getParam('providerName');
			$providerCode = $this->_getParam('providerCode');
			$providerType = $this->_getParam('providerType');

			$filters = array(
							   'providerCode'    	=> array('StripTags','StringTrim','StringToUpper'),
							   'providerName'  	=> array('StripTags','StringTrim'),
							   'providerType'  	=> array('StripTags','StringTrim'),
							  );
			$validators = array(
										'providerName' 	=> array(	
																'NotEmpty',
																//'Alnum',
																array	(
																			'StringLength', 
																			array	(
																						'min' => $min,
																						'max' => $max
																					)
																		),
																array	(
																			'Db_NoRecordExists',
																			array	(
																						'table'=>'M_SERVICE_PROVIDER',
																						'field'=>'PROVIDER_NAME'
																					)
																		),
																array	(
																			'Db_NoRecordExists',
																			array	(
																						'table'=>'TEMP_SERVICE_PROVIDER',
																						'field'=>'PROVIDER_NAME'
																					)
																		),
																'messages' => array(
																						"Error: E-Commerce Name Can not be empty.",
																						//"Error: Invalid Biller Name.",
																						"Error: Invalid E-Commerce Name length( $min - $max character allowed).",
																						"Error: $providerName is already registered.",
																						"Error: $providerName is already taken."
																					)
															),
										'providerCode' 	=> array(	
																'NotEmpty',
																//'Alnum',
																array	(
																			'StringLength', 
																			array	(
																						'min' => $min,
																						'max' => $max_billercode
																					)
																		),
																array	(
																			'Db_NoRecordExists',
																			array	(
																						'table'=>'M_SERVICE_PROVIDER',
																						'field'=>'PROVIDER_CODE'
																					)
																		),
																array	(
																			'Db_NoRecordExists',
																			array	(
																						'table'=>'TEMP_SERVICE_PROVIDER',
																						'field'=>'PROVIDER_CODE'
																					)
																		),
																'messages' => array(
																						"Error: E-Commerce Code Can not be empty.",
																						//"Error: Invalid E-Commerce Code.",
																						"Error: Invalid E-Commerce Code length( $min - $max_billercode character allowed).",
																						"Error: $providerCode is already registered.",
																						"Error: $providerCode is already taken."
																					)
															),
										'domainName' 	=> array(
																'NotEmpty',																
																'messages' => array(																																								
																						'Error: Domain Can not be empty.',																						
																					)
															),
										'keyId' 	=> array(
																'NotEmpty',																
																'messages' => array(																																								
																						'Error: Key Id Can not be empty.',																						
																					)
															),																																
									);

			

			$zf_filter = new Zend_Filter_Input($filters,$validators,$this->_request->getParams());
			
			
			if($zf_filter->isValid())
			{
							
			
				$this->_db->beginTransaction();
				
				/*$select3 = $this->_db->select()
				->from(array('M_SERVICE_PROVIDER'));
				$select3 -> where("PROVIDER_CODE = ?", $providerCode);
				$result3 = $select3->query()->FetchAll();
				
				$select4 = $this->_db->select()
				->from(array('M_SERVICE_PROVIDER'));
				$select4 -> where("PROVIDER_NAME = ?", $providerName);
				$result4 = $select4->query()->FetchAll();
				
				if(empty($result3) && empty($result4)){
					$exist = true;
					$result = true;
				}else{
					$exist = false;
					$result = false;
					$this->view->ERROR_MSG_N = '';
				}
				
				
				if($result)
				{*/
					
					$param['PROVIDER_CODE'] 	= html_entity_decode($this->_getParam('providerCode'));
					$param['PROVIDER_NAME'] 	= html_entity_decode($this->_getParam('providerName'));
					$param['PROVIDER_DOMAIN'] 	= html_entity_decode($this->_getParam('domainName'));
					$param['PROVIDER_KEY_ID'] 	= html_entity_decode($this->_getParam('keyId'));
					$providerType	= $param['PROVIDER_TYPE'] 	= html_entity_decode($this->_getParam('providerType'));
					$param['SERVICE_TYPE'] 	= html_entity_decode('18');
					
					
					
					try
					{
						$providerID 	 = $param['PROVIDER_CODE'];
						$providerName 	 = $param['PROVIDER_NAME'];
						  
						$info = 'E-Commerce Payment';
						$info2 = 'Add New E-Commerce Payment';
									
						$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['new'],null,'M_SERVICE_PROVIDER','TEMP_SERVICE_PROVIDER',$param['PROVIDER_CODE'],$param['PROVIDER_NAME'],$param['PROVIDER_CODE']);
	
						$data= $param+array(
																			'CHANGES_ID' 			=> $change_id,
																			'PROVIDER_STATUS' 			=> 1,
																			'PROVIDER_SUGGESTED' 	=> new Zend_Db_Expr('GETDATE()'),
																			'PROVIDER_SUGGESTEDBY' 	=> $this->_userIdLogin,
																			'PROVIDER_DOMAIN' 		=> $param['PROVIDER_DOMAIN'],
																			'PROVIDER_KEY_ID' 		=> $param['PROVIDER_KEY_ID']
																		);
						$model->insertTemp($data);
						Application_Helper_General::writeLog('CBIL','Add New E-Commerce. E-Commerce ID : '.$providerID.'.E-Commerce Name : '.$providerName);
						
						if($error == 0)
						{
							$this->_db->commit();
							$this->setbackURL('/billeronboard');
							$this->_redirect('/notification/submited/index');
						}
					}
					catch(Exception $e)
					{
						$this->_db->rollBack();
					}
				/*}
				else
				{
					
					$this->view->providerCode = $providerCode;
					$this->view->providerName = $providerName;
					$this->view->providerType = $providerType;
					$this->view->serviceType1 = '1';
					if(!$exist){
						$this->view->ERROR_MSG_N = 'Provider name or code already exist *';
					}else{
						$this->view->ERROR_MSG_N = '';
						$listMsg = $zf_filter->getMessages();
						foreach($listMsg as $key => $arrMsg)
						{
							foreach($arrMsg as $msg)
							{
								$keyID = $key.'_errMsg';
								$this->view->$keyID = $msg;
							}
						}	
					}
					
					
				}*/
				
			}else{
				
				$this->view->providerCode = $this->_getParam('providerCode');
				$this->view->providerName = $this->_getParam('providerName');
				$this->view->domainName = $this->_getParam('domainName');
				$this->view->keyId = $this->_getParam('keyId');
				$serviceID = 'serviceType'.$providerType;
				$this->view->$serviceID = $this->_getParam($serviceID);

				$listMsg = $zf_filter->getMessages();
				foreach($listMsg as $key => $arrMsg)
				{
					foreach($arrMsg as $msg)
					{
						$keyID = $key.'_errMsg';
						$this->view->$keyID = $msg;
					}
				}
			}
		}

    	Application_Helper_General::writeLog('CBIL','View Add E-Commerce');
	}
}