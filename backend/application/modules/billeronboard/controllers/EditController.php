<?php

require_once 'Zend/Controller/Action.php';

class billeronboard_EditController extends Application_Main
{
	public function indexAction() 
	{
		// min max untuk provider Code and Name Length
  		$min = 4;
		$max = 10;
		//$model = new biller_Model_Biller();
		$providerID = $this->_getParam('providerID');		
  		$this->view->providerIdd = $providerID;
  		
		$model 			 = new billeronboard_Model_Billeronboard();
		if(empty($providerID))
		{
			header("location: {$this->view->backURL}");exit();
		}
		$providerData 	 = $model->getDetailProvider($providerID);
		if(empty($providerData))
		{
			header("location: {$this->view->backURL}");exit();
		}
		else
		{
			$this->view->providerData = $providerData;
			$this->view->providerID = $providerID;
			
			$submit = $this->_getParam('submit');
			$cek = $model->isRequestChange($providerID);
			if(!$cek)
			{
				if($submit)
				{
					
					$providerName = $this->_getParam('providerName');
					$providerCode = $this->_getParam('providerCode');
					$providerType = $this->_getParam('providerType');
		
					$filters = array(
									   'providerCode'    	=> array('StripTags','StringTrim','StringToUpper'),
									   'providerName'  	=> array('StripTags','StringTrim'),
									   'providerType'  	=> array('StripTags','StringTrim'),
									  );
					$validators = array(
												'providerName' 	=> array(	
																		'NotEmpty',
																		//'Alnum',
																		array	(
																					'StringLength', 
																					array	(
																								'min' => $min,
																								'max' => $max
																							)
																				),
																		/*array	(
																					'Db_NoRecordExists',
																					array	(
																								'table'=>'M_SERVICE_PROVIDER',
																								'field'=>'PROVIDER_NAME'
																							)
																				),
																		array	(
																					'Db_NoRecordExists',
																					array	(
																								'table'=>'TEMP_SERVICE_PROVIDER',
																								'field'=>'PROVIDER_NAME'
																							)
																				),*/
																		'messages' => array(
																								"Error: E-Commerce Name Can not be empty.",
																								//"Error: Invalid Biller Name.",
																								"Error: Invalid E-Commerce Name length( $min - $max character allowed).",
																								//"Error: $providerName is already registered.",
																								//"Error: $providerName is already taken."
																							)
																	),
												
												'domainName' 	=> array(
																		'NotEmpty',																
																		'messages' => array(																																								
																								'Error: Domain Can not be empty.',																						
																							)
																	),
												'keyId' 	=> array(
																		'NotEmpty',																
																		'messages' => array(																																								
																								'Error: Key Id Can not be empty.',																						
																							)
																	),																																
											);
		
					
		
					$zf_filter = new Zend_Filter_Input($filters,$validators,$this->_request->getParams());
					
					if($zf_filter->isValid())
					{
						$error = 0;
						if($error == 0)
						{
							$this->_db->beginTransaction();
							try
							{
								$providerID 	 = $this->_getParam('providerID');
								$providerName 	 = $this->_getParam('providerName');
								$providerDomain	 = $this->_getParam('domainName');
								$providerKeyId 	 = $this->_getParam('keyId');
								  
								$info = 'E-Commerce Payment';
								$info2 = 'Update E-commerce Payment';
								
								$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['edit'],null,'M_SERVICE_PROVIDER','TEMP_SERVICE_PROVIDER',$providerData['PROVIDER_CODE'],$providerData['PROVIDER_NAME'],$providerData['PROVIDER_CODE']);
								
								unset($providerData['CHARGES']);
								$data= $providerData+array(
																					'CHANGES_ID' 			=> $change_id,
																					'PROVIDER_SUGGESTED' 	=> new Zend_Db_Expr('GETDATE()'),
																					'PROVIDER_SUGGESTEDBY' 	=> $this->_userIdLogin
																				);
								$data['PROVIDER_NAME']		= $providerName;
								$data['PROVIDER_DOMAIN']	= $providerDomain;
								$data['PROVIDER_KEY_ID']	= $providerKeyId;
	
								$getServiceType 	 = $model->getServiceType();
								if(!empty($getServiceType))
								{
									foreach($getServiceType as $value)
									{
										$arrServiceType[trim($value['SERVICE_NAME'])] = $value['SERVICE_ID'];
									}
								}
	
								$arrProviderType = array('Payment' => 1,'Purchase' =>2);
								$data['PROVIDER_TYPE']	= $arrProviderType[$providerData['PROVIDER_TYPE']];
								$data['SERVICE_TYPE']		= $arrServiceType[trim($providerData['SERVICE_TYPE'])];
								$data['PROVIDER_STATUS']	= 1;// approved
	
								$model->insertTemp($data);
								Application_Helper_General::writeLog('UECL','Update E-Commerce Payment');
								
								if($error == 0)
								{
									$this->_db->commit();
									$this->setbackURL('/billeronboard');
									$this->_redirect('/notification/submited/index');
								}
							}
							catch(Exception $e)
							{
								$this->_db->rollBack();
							}
						}
					}else{
				
						$this->view->providerCode = $this->_getParam('providerCode');
						$this->view->providerName = $this->_getParam('providerName');
						$this->view->domainName = $this->_getParam('domainName');
						$this->view->keyId = $this->_getParam('keyId');
						$serviceID = 'serviceType'.$providerType;
						$this->view->$serviceID = $this->_getParam($serviceID);
		
						$listMsg = $zf_filter->getMessages();
						foreach($listMsg as $key => $arrMsg)
						{
							foreach($arrMsg as $msg)
							{
								$keyID = $key.'_errMsg';
								$this->view->$keyID = $msg;
							}
						}
						
					}
					
							
				}
			}
			else
			{
				$docErr = "*No changes allowed for this record while awaiting approval for previous change";	
				$this->view->error = $docErr;
				$this->view->changestatus = "disabled";
			}
		}
	}
}
