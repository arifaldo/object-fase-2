<?php

require_once 'Zend/Controller/Action.php';

class billeronboard_RepairController extends Application_Main 
{
  public function indexAction() 
  {
		$model = new billeronboard_Model_Billeronboard();
  		$this->_helper->layout()->setLayout('popup');
  		$params = $this->_request->getParams();
		// min max untuk provider Code and Name Length
		$min = 4;
		$max = 30;
		$max_billercode = 10;
  		if(array_key_exists('changes_id', $params))
  		{
  			$filters = array('changes_id' => array('StripTags', 'StringTrim'));
  			$validators = array(
  				'changes_id' => array(
  					'NotEmpty',
  					'Digits',
					'messages' => array(
						'Suggestion ID does not exist.',
						'Suggestion ID must be number.',
					),
  				),
  			);
  			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $params, $this->_optionsValidator);
	  		if($zf_filter_input->isValid())
	  		{
	  			$changeId = $zf_filter_input->changes_id;
				$changeInfo = $this->getGlobalChanges($changeId);
				$providerType = array ( 1 => 'Payment', 2 => 'Purchase');
				$getServiceType 	 = $model->getServiceType();
				if(!empty($getServiceType))
				{
					foreach($getServiceType as $value)
					{
						$arrServiceType[$value['SERVICE_OF']][$value['SERVICE_ID']] = $value['SERVICE_NAME'];
					}
				}
				$tempData = $model->getTemp($changeId);
				$tempData['SERVICE_TYPE']	=	$arrServiceType[$tempData['PROVIDER_TYPE']][$tempData['SERVICE_TYPE']];
				$tempData['PROVIDER_TYPE']	=	$providerType[$tempData['PROVIDER_TYPE']];
				$this->view->providerData = $tempData;
	  			$this->view->changes_id = $changeId;
				$this->view->modulename = $this->_request->getModuleName();
			}
	  		else
	  		{
	  			$errors = $zf_filter_input->getMessages();
	  			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
      			$this->_helper->getHelper('FlashMessenger')->addMessage($errors['changes_id']);
	  		}
		}
  		else
  		{
  			$errorRemark = 'Suggestion ID does not exist';
  			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
      		$this->_helper->getHelper('FlashMessenger')->addMessage($errorRemark);
  		}

		if($this->_request->isPost() && $this->view->hasPrivilege('RBIO'))
		{
			$providerName = $this->_getParam('providerName');
			$excludeName = "PROVIDER_NAME != ".$this->_db->quote($tempData['PROVIDER_NAME']);

			$filters = array(
							   'providerName'  	=> array('StripTags','StringTrim'),
							  );
			$validators = array(
										'providerName' 	=> array(	
																'NotEmpty',
																//'Alnum',
																array	(
																			'StringLength', 
																			array	(
																						'min' => $min,
																						'max' => $max
																					)
																		),
																array	(
																			'Db_NoRecordExists',
																			array	(
																						'table'=>'M_SERVICE_PROVIDER',
																						'field'=>'PROVIDER_NAME',
																						'exclude'=>$excludeName
																					)
																		),
																array	(
																			'Db_NoRecordExists',
																			array	(
																						'table'=>'TEMP_SERVICE_PROVIDER',
																						'field'=>'PROVIDER_NAME',
																						'exclude'=>$excludeName
																					)
																		),
																'messages' => array(
																						"Error: E-Commerce Name Can not be empty.",
																						//"Error: Invalid Provider Name.",
																						"Error: Invalid E-Commerce Name length( $min - $max character allowed).",
																						"Error: $providerName is already registered.",
																						"Error: $providerName is already taken."
																					)
															),
															
															'domainName' 	=> array(
																'NotEmpty',																
																'messages' => array(																																								
																						'Error: Domain Can not be empty.',																						
																					)
															),
															'keyId' 	=> array(
																'NotEmpty',																
																'messages' => array(																																								
																						'Error: Key Id Can not be empty.',																						
																					)
															),
									);
			if($changeInfo['CHANGES_TYPE'] == 'N')
			{
				$providerCode = $this->_getParam('providerCode');
				$excludeCode = "PROVIDER_CODE != ".$this->_db->quote($tempData['PROVIDER_CODE']);
				$filters += array('providerCode'    	=> array('StripTags','StringTrim','StringToUpper'));
				$validators += array('providerCode' 	=> array(	
																	'NotEmpty',
																	//'Alnum',
																	array	(
																				'StringLength', 
																				array	(
																							'min' => $min,
																							'max' => $max_billercode
																						)
																			),
																	array	(
																				'Db_NoRecordExists',
																				array	(
																							'table'=>'M_SERVICE_PROVIDER',
																							'field'=>'PROVIDER_CODE',
																							'exclude'=>$excludeName
																						)
																			),
																	array	(
																				'Db_NoRecordExists',
																				array	(
																							'table'=>'TEMP_SERVICE_PROVIDER',
																							'field'=>'PROVIDER_CODE',
																							'exclude'=>$excludeCode
																						)
																			),
																	'messages' => array(
																							"Error: E-Commerce Code Can not be empty.",
																							//"Error: Invalid Provider Code.",
																							"Error: Invalid E-Commerce Code length( $min - $max_billercode character allowed).",
																							"Error: $providerCode is already registered.",
																							"Error: $providerCode is already taken."
																						)
																));
			}
			$zf_filter = new Zend_Filter_Input($filters,$validators,$this->_request->getParams());

			if($zf_filter->isValid())
			{
				if($changeInfo['CHANGES_TYPE'] == 'N')
				{
					$param['PROVIDER_CODE'] 	= html_entity_decode($zf_filter->getEscaped('providerCode'));
				}
				$param['PROVIDER_NAME'] 	= html_entity_decode($zf_filter->getEscaped('providerName'));
				$param['PROVIDER_DOMAIN'] 	= html_entity_decode($this->_getParam('domainName'));
				$param['PROVIDER_KEY_ID'] 	= html_entity_decode($this->_getParam('keyId'));
				try
				{
					$this->_db->beginTransaction();
					
					$this->updateGlobalChanges($changeId);
					$model->updateTemp($changeId,$param);	
					
					//END LOOP
					Application_Helper_General::writeLog('RBIL','Submitting Repair Biller Charges for Provider : '.$tempData['PROVIDER_NAME']);
					$this->_db->commit();
					$this->_redirect('/popup/successpopup');
				}
				catch(Exception $e) 
				{
					//rollback changes
					$this->_db->rollBack();
					$errorMsg = 'Database failed.';
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
				}
			}
			else
			{
				$this->view->providerCode = $this->_getParam('providerCode');
				$this->view->providerName = $this->_getParam('providerName');
				$serviceID = 'serviceType'.$providerType;
				$this->view->$serviceID = $this->_getParam($serviceID);

				$listMsg = $zf_filter->getMessages();
				foreach($listMsg as $key => $arrMsg)
				{
					foreach($arrMsg as $msg)
					{
						$keyID = $key.'_errMsg';
						$this->view->$keyID = $msg;
					}
				}
			}
			$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
			if(count($temp)>1){
				if($temp[0]=='F' || $temp[0]=='S'){
					if($temp[0]=='F')
						$this->view->error = 1;
					else
						$this->view->success = 1;
					$msg = ''; unset($temp[0]);
					foreach($temp as $value)
					{
						if(!is_array($value))
							$value = array($value);
						$msg .= $this->view->formErrors($value);
					}
					$this->view->report_msg = $msg;
				}	
			}
		}
  }
}