	<?php


require_once 'Zend/Controller/Action.php';

class userlist_IndexController extends Application_Main 
{
	/**
	 * The default action - show the home page
	 */
	//protected $_groupStatusCode  = array();
	protected $menuTitle	       = "Cash management";
	protected $actionView	       = "V";
	
	public function indexAction() 
	{
		$this->setBackUrl();
		$this->menuTitle	       = "Cash Management";
		$this->actionView	       = "V";
		$this->actionCSV	       = "CSV";
		
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
    	
    	$selectGroup =	$this->_db->select()
									->from(	'M_BGROUP',
										array(
												'BGROUP_ID','BGROUP_DESC'));
									//->where('USER_ID = \''.$user_id.'\'')		
									//->where('PASSWORD = \''.$v_password_old.'\'');
				
				// echo $v_password;
		 $arrGroup = $this->_db->fetchAll($selectGroup);
       
		$this->view->arrgroup = $arrGroup;
	
		$this->reportTitle 			= 'Administrator';
		$this->view->menuTitle	    = $this->reportTitle;//$this->menuTitle.' &gt; '.$this->reportTitle;
		$moduleID 					= "BAL";
		
		 $fields = array('userid'   => array('field'    => 'BUSER_ID',
	                                          'label'    => 'User ID',
	                                          'sortable' => true),
						  'status'   => array('field'    => 'BUSER_STATUS',
	                                          'label'    => 'Status',
	                                          'sortable' => true),
	                      'username' => array('field'    => 'BUSER_NAME',
	                                          'label'    => 'Name',
	                                          'sortable' => true),
						   'group'   => array('field'    => 'BGROUP_ID',
	                                          'label'    => 'Group',
	                                          'sortable' => true),
	                      'islocked'   => array('field'    => 'BUSER_ISLOCKED',
	                                          'label'    => 'User Is Locked',
	                                          'sortable' => true),
	                      'lastlogin'   => array('field'    => 'BUSER_LASTLOGIN',
	                                          'label'    => 'Last Login',
	                                          'sortable' => true),
							'latestsuggestion'   => array('field'    => 'BUSER_NAME',
	                                          'label'    => 'Latest Suggestion',
	                                          'sortable' => true),
							'latestsuggestor'   => array('field'    => 'BUSER_NAME',
	                                          'label'    => 'Latest Suggestor',
	                                          'sortable' => true),
							'latestsuggestapp'   => array('field'    => 'BUSER_NAME',
	                                          'label'    => 'Latest Suggestion Approve',
	                                          'sortable' => true),
							'latestapp'   => array('field'    => 'BUSER_NAME',
	                                          'label'    => 'Latest Approver',
	                                          'sortable' => true),
							'action'   => array('field'    => '',
	                                          'label'    => 'Action',
	                                          'sortable' => FALSE)													  
	                       );
                        
        //get page, sortby, sortdir
        $page = $this->_getParam('page');		
        $sortBy = $this->_getParam('sortby');
        $sortDir = $this->_getParam('sortdir');
        $getCSV = $this->_getParam('csv');
        
        $actionID = ($getCSV == 1)? $this->actionCSV: $this->actionView;
        $this->beLog($actionID, $moduleID);

        //validate parameters before passing to view and query
        $page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
                    
        $sortBy = (Zend_Validate::is($sortBy,'InArray',
        	array(array_keys($fields))
            ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

        $sortDir = (Zend_Validate::is($sortDir,'InArray',
        	array('haystack'=>array('asc','desc'))
            ))? $sortDir : 'asc';

		//get filtering param
        $filters = array('filter' 	   	=>  array('StringTrim', 'StripTags'),
                         'userId'   	=>  array('StringTrim', 'StripTags', 'StringToUpper'),
                         'fDateFrom' 	=>  array('StringTrim', 'StripTags'),
                         'fDateTo'   	=>  array('StringTrim', 'StripTags'),
        				 'moduleDesc'   =>  array('StringTrim', 'StripTags', 'StringToUpper'),
        				 'actionDesc'   =>  array('StringTrim', 'StripTags', 'StringToUpper'),
        				 'status'   	=>  array('StringTrim', 'StripTags', 'StringToUpper'),
            );
        
        $optVldtr   = array('breakChainOnFailure' => false);
       
	
	      	$this->view->currentPage = $page;
	        $this->view->sortBy = $sortBy;
	        $this->view->sortDir = $sortDir;
	        		        
	        $filter		 = $this->_request->getParam('filter');
	        $csv = $this->_getParam('csv');
			$pdf = $this->_getParam('pdf');
	        if($filter != '' || $csv || $pdf)
			{
				
			$zf_filter = new Zend_Filter_Input($filters,array(),$this->_request->getParams());
			$filter = $zf_filter->getEscaped('filter');
	           
	            
				
				
	           
	        			
				
				$select	= $this->_db->select()
								->distinct()
								->from	(
											array('MB'=>'M_BUSER'),
											array(
													'BUSER_ID'=>'MB.BUSER_ID',
													'BUSER_STATUS'=>'MB.BUSER_STATUS',
													'BUSER_NAME'=>'MB.BUSER_NAME',
													'BGROUP_ID'=>'MB.BGROUP_ID',
													'BGROUP_DESC'=>'MG.BGROUP_DESC',
													'BUSER_ISLOCKED'=>'MB.BUSER_ISLOCKED',
													'BUSER_LASTLOGIN'=>'MB.BUSER_LASTLOGIN'
													
												)
										)
								->joinLeft	(
												array('MG' => 'M_BGROUP'),
												'MB.BGROUP_ID = MG.BGROUP_ID',
												array()
											)
						//		->where('TP.CUST_ID =?',$this->_custIdLogin);	
								->order(array($sortBy.' '.$sortDir));								
							
			}
			else{
				$select = array();
			}
			//echo $select;
			
	        
			if($filter == 'Set Filter' || $csv || $pdf)
	        {
				 $userId = html_entity_decode($zf_filter->getEscaped('userId'));
				 $groupId = html_entity_decode($zf_filter->getEscaped('groupId'));
				 $status = html_entity_decode($zf_filter->getEscaped('status'));
				 $fDateTo = $zf_filter->getEscaped('fDateTo');
				 $fDateFrom = $zf_filter->getEscaped('fDateFrom');
				
				/*$userId     = html_entity_decode(($zf_filter->userId)    ? $zf_filter->userId     : $this->_request->getParam('userId'));
				$groupId  = html_entity_decode(($zf_filter->groupId) ? $zf_filter->groupId  : $this->_request->getParam('groupId'));
				$actionDesc  = html_entity_decode(($zf_filter->actionDesc) ? $zf_filter->actionDesc  : $this->_request->getParam('actionDesc'));
				$status	 = html_entity_decode(($zf_filter->status)   ? $zf_filter->status    : $this->_request->getParam('status'));
				$fDateFrom 	 = ($zf_filter->fDateFrom)  ? $zf_filter->fDateFrom   : $this->_request->getParam('fDateFrom');
				$fDateTo     = ($zf_filter->fDateTo)    ? $zf_filter->fDateTo     : $this->_request->getParam('fDateTo');*/
				
				//where clauses        	
	            if($userId)
				{
				$this->view->userId = $userId;
	            $select->where("UPPER(MB.BUSER_ID) LIKE ".$this->_db->quote('%'.$userId.'%'));
	            
				}
				
				  if($status == 1 || $status == 2 || $status == 3)
	            $select->where("UPPER(MB.BUSER_STATUS) LIKE ".$this->_db->quote($status));
				$this->view->status = $status;
	            
				if($groupId)
				{
				
				$this->view->groupId = $groupId;
	            $select->where("UPPER(MG.BGROUP_ID) LIKE ".$this->_db->quote('%'.$groupId.'%'));
				}
				
				if(!empty($fDateFrom))
	            {
	            	$FormatDate = new Zend_Date($fDateFrom, $this->_dateDisplayFormat);
					$fDateFrom  = $FormatDate->toString($this->_dateDBFormat);
					$this->view->fDateFrom = $zf_filter->getEscaped('fDateFrom');					
	            }
	            
	            if(!empty($fDateTo))
	            {
	            	$FormatDate = new Zend_Date($fDateTo, $this->_dateDisplayFormat);
					$fDateTo    = $FormatDate->toString($this->_dateDBFormat);		
					$this->view->fDateTo = $zf_filter->getEscaped('fDateTo');
	            }
	            
	            if(!empty($fDateFrom) && empty($fDateTo))
	            $select->where("CONVERT(DATE, MB.BUSER_LASTLOGIN) >= ".$this->_db->quote($fDateFrom));
	            
	            if(empty($fDateFrom) && !empty($fDateTo))
	            $select->where("CONVERT(DATE, MB.BUSER_LASTLOGIN) <= ".$this->_db->quote($fDateTo));
	            
	            if(!empty($fDateFrom) && !empty($fDateTo))
	            $select->where("CONVERT(DATE, MB.BUSER_LASTLOGIN) between ".$this->_db->quote($fDateFrom)." and ".$this->_db->quote($fDateTo));
			}
			
			
			if($csv || $pdf)
			{
				
				$data = $this->_db->fetchAll($select);
				$header = array('User ID','Status','Name','Group','User Is Locked','Last Login','Latest Suggestion','Latest Suggestor','Latest Suggestion Approve','Latest Approver');
				//Zend_Debug::dump($this->_bankstatus['code']);die;
				foreach($data as $key=>$row)
				{
					unset($data[$key]['BGROUP_ID']);
					$arrACCT_STATUS = array(0=>"Approved",1=>"Suspended",2=>"Deleted");
					$data[$key]['BUSER_STATUS'] = in_array($row['BUSER_STATUS'], array_keys($arrACCT_STATUS)) ? $arrACCT_STATUS[$row['BUSER_STATUS']] : "N/A";
					$arrACCT_ISLOCKED = array(0=>"No",1=>"Yes");
					$data[$key]['BUSER_ISLOCKED'] = in_array($row['BUSER_ISLOCKED'], array_keys($arrACCT_ISLOCKED)) ? $arrACCT_ISLOCKED[$row['BUSER_ISLOCKED']] : "N/A";
					$data[$key]['BUSER_LASTLOGIN'] = Application_Helper_General::convertDate($row['BUSER_LASTLOGIN'],'d-MMM-yyyy HH:mm:ss','yyyy-MM-dd HH:mm:ss');					
				}
				//Zend_Debug::dump($data);die;
				//die($select);
				
				if($csv)
				{
					$this->_helper->download->csv($header,$data,null,'User List');  
					$this->backendLog('CSV', $this->_moduleDB, null,null,null);  
				}
				else if($pdf)
				{
					$this->_helper->download->pdf($header,$data,null,'User List');    
					$this->backendLog('PDF', $this->_moduleDB, null,null,null);  
				}
			}
	        else
	        {                              
		        $this->paging($select);
		        $this->view->fields 	 = $fields;
		        $this->view->filter 	 = $filter;
		        $this->view->reportTitle = $this->reportTitle;
	        } 
        	
      
 	}
 	
	// System Activity Log -> berisi daftar aktifitas yang dilakukan oleh system.
	
  public function beLog($actionID, $moduleID)
	{
		//insert log
		try 
		{
			$this->_db->beginTransaction();
		  	$this->backendLog($actionID, $moduleID, null); 
		  	$this->_db->commit();
		}
	    catch(Exception $e)
	    {
	 	  	$this->_db->rollBack();
		  	SGO_Helper_GeneralLog::technicalLog($e);
		}
	}
  protected function errorCSV()
  {
	$this->_helper->getHelper('FlashMessenger')->addMessage('F');
	$this->_helper->getHelper('FlashMessenger')->addMessage($this->getErrorRemark('08'));
	$this->_redirect($this->_backURL);
  }
}