<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'General/Charges.php';
require_once ('../../frontend/application/modules/purchasing/models/Purchasing.php');
require_once 'Crypt/AESMYSQL.php';

class suspectchangerequest_DetailController extends Application_Main
{

	// public function initController(){
	// 		  $this->_helper->layout()->setLayout('popup');
	// }

	protected $_controllerList 	= "index";

	public function indexAction()
	{
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
	    $password = $sessionNamespace->token; 
	    $this->view->token = $sessionNamespace->token;

	    $suggestid 	= $this->_getParam('suggestid');
	    $custid 	= $this->_getParam('custid');
	  	$slipnumb 	= $this->_getParam('payReff');
	  	$extype 	= $this->_getParam('extype');
	  	$approve 	= $this->_getParam('approve');
	  	$reject 	= $this->_getParam('reject');
	  	
	  	if($approve)
	  	{
	  		Application_Helper_General::writeLog('EXCA','Approve Payment Exception ('.$slipnumb.')');
	  	}
	  	if($reject)
	  	{
	  		Application_Helper_General::writeLog('EXCA','Reject Payment Exception ('.$slipnumb.')');
	  	}
	  	if(!$approve || !$reject)
	  	{
	  		//Application_Helper_General::writeLog('EXCL','Viewing Payment Exception Changes List Detail ('.$slipnumb.')');	
	  	}
	  	
	  	$selectCek = $this->_db->select()
								->from(array('A' => 'T_PSLIP_EXCEPTION_REPAIR'),array('*'))
								->joinLeft(array('B' => 'T_PSLIP_EXCEPTION_REPAIR_DETAIL'),'A.SUGGEST_ID = B.SUGGEST_ID',array('*'))
								->joinLeft(array('C' => 'T_PSLIP'),'A.PS_NUMBER = C.PS_NUMBER',array('PS_CCY','ESCROW_ACC','ESCROW_ACC_TYPE','SENDFILE_STATUS'))
								->joinLeft(array('D' => 'T_TRANSACTION'),'B.TRANSACTION_ID = D.TRANSACTION_ID',array('SOURCE_ACCOUNT'))
								->where("A.SUGGEST_ID LIKE ".$this->_db->quote($suggestid))
								->where("A.EXCEPTION_TYPE LIKE ".$this->_db->quote($extype))
								->where("A.SUGGEST_STATUS LIKE '0' OR A.SUGGEST_STATUS LIKE '1'");
		
		$cek = $this->_db->fetchAll($selectCek);

		// echo "<pre>";
		// var_dump($cek);
		// die();

  		if($reject)
  		{
  			// $updatesugReject['SUGGEST_STATUS'] = 'RJ';
  			// $updatesugReject['PS_STATUS'] = 4;
  			// $wheresugReject['SUGGEST_ID = ?'] = $suggestid;
  			// $query = $this->_db->update ( "T_PSLIP_EXCEPTION_REPAIR", $updatesugReject, $wheresugReject );

  			$insertHistory['DATE_TIME'] = new Zend_Db_Expr('now()');
			$insertHistory['PS_NUMBER'] = $slipnumb;
			$insertHistory['USER_LOGIN'] = $this->_userIdLogin;
			$insertHistory['CUST_ID'] = $cek[0]['CUST_ID'];
			$insertHistory['HISTORY_STATUS'] = '25';
			$insertHistory['PS_REASON'] = 'Request Rejected';
			$this->_db->insert('T_PSLIP_HISTORY', $insertHistory);

  			$where = array(
  							'SUGGEST_ID = ?' => $suggestid,
  							'CUST_ID 	= ?' => $custid
  						);
  			$this->_db->delete('T_PSLIP_EXCEPTION_REPAIR',$where);

  			$where = array(
  							'SUGGEST_ID = ?' => $suggestid
  						);
  			$this->_db->delete('T_PSLIP_EXCEPTION_REPAIR_DETAIL',$where);
  			
  			$ns = new Zend_Session_Namespace('FVC');
	    	$ns->backURL = $this->view->backURL;
			$this->_redirect('/notification/success');
  		}
  		
  		if($approve)
  		{
					
			$updatesugApprove['SUGGEST_STATUS'] 	= 'AP';
			$wheresugApprove['SUGGEST_ID = ?'] 		= $suggestid;
			$query = $this->_db->update ( "T_PSLIP_EXCEPTION_REPAIR", $updatesugApprove, $wheresugApprove );
			
	// 		if($cek[0]['TYPE']==4)
	// 		{
	// 			if($cek[0]['SENDFILE_STATUS']==0) // exception from source to escrow
	// 			{
	// 				if($cek[0]['PS_STATUS']==5) // if success
	// 				{
	// 					$updateApprove['PS_STATUS'] = 8; // in progress
	// 					$updateApprove['SENDFILE_STATUS'] = 1;
	// 					$updateApprove['PS_UPDATED'] = new Zend_Db_Expr('GETDATE()');
	// 					$whereApprove['PS_NUMBER = ?'] = $slipnumb;
	// 					$query = $this->_db->update ( "T_PSLIP", $updateApprove, $whereApprove );
						
	// 					foreach($cek as $row)
	// 					{
	// 						$updatedetailApprove['TRA_STATUS'] = 2; // processed
	// 						$wheredetailApprove['TRANSACTION_ID = ?'] = $row['TRANSACTION_ID'];
	// 						$this->_db->update('T_TRANSACTION', $updatedetailApprove,$wheredetailApprove);
	// 					}
						
	// 				}
	// 				else // failed
	// 				{
	// 					$updateApprove['PS_STATUS'] = 6; // failed
	// 					$updateApprove['PS_UPDATED'] = new Zend_Db_Expr('GETDATE()');
	// 					$whereApprove['PS_NUMBER = ?'] = $slipnumb;
	// 					$query = $this->_db->update ( "T_PSLIP", $updateApprove, $whereApprove );
				
	// 					foreach($cek as $row)
	// 					{
	// 						$cekDomestic = $this->_db->select()
	// 											->from(array('A' => 'T_TRANSACTION'),array('A.TRANSFER_TYPE'))
	// 											->where("A.TRANSACTION_ID LIKE ".$this->_db->quote($row['TRANSACTION_ID']))
	// 											->where("A.TRANSFER_TYPE != 0");
												
	// 						$cekDomestic = $this->_db->fetchRow($cekDomestic);
	// 						if($cekDomestic) 
	// 						{
	// 							$updatedetailApprove['EFT_STATUS'] = '2';
	// 						}
	// 						$updatedetailApprove['TRA_STATUS'] = '4';
	// 						$wheredetailApprove['TRANSACTION_ID = ?'] = $row['TRANSACTION_ID'];
	// 						$this->_db->update('T_TRANSACTION', $updatedetailApprove,$wheredetailApprove);
	// 					}
					
	// 				}
	// 			}
	// 			else if($cek[0]['SENDFILE_STATUS']==2) // exception from escrow to beneficiary
	// 			{
	// //							if($cek[0]['PS_STATUS']==5) // if success
	// //							{
	// //								$isExceptionReversal = false;
	// //								$numTrxFailed = 0;
	// //								foreach($cek as $row)
	// //								{
	// //									if($row['TRA_STATUS'] == '4') // ff failed, transfer reversal
	// //									{
	// //										$numTrxFailed += 1;
	// //									}
	// //									
	// //									$cekDomestic = $this->_db->select()
	// //														->from(array('A' => 'T_TRANSACTION'),array('A.TRANSFER_TYPE'))
	// //														->where("A.TRANSACTION_ID LIKE ".$this->_db->quote($row['TRANSACTION_ID']))
	// //														->where("A.TRANSFER_TYPE != 0");
	// //														
	// //									$cekDomestic = $this->_db->fetchRow($cekDomestic);
	// //									if($cekDomestic && $row['TRA_STATUS'] == '4') 
	// //									{
	// //										$updatedetailApprove['EFT_STATUS'] = '2';
	// //									}
	// //									
	// //									$updatedetailApprove['TRA_STATUS'] = $row['TRA_STATUS'];
	// //									$wheredetailApprove['TRANSACTION_ID = ?'] = $row['TRANSACTION_ID'];
	// //									$this->_db->update('T_TRANSACTION', $updatedetailApprove,$wheredetailApprove);
	// //								}
	// //								
	// //								if($numTrxFailed>0) // DO REVERSAL
	// //								{
	// //									$updateApprove['SENDFILE_STATUS'] = 3;
	// //									$whereApprove['PS_NUMBER = ?'] = $slipnumb;
	// //									$query = $this->_db->update ( "T_PSLIP", $updateApprove, $whereApprove );
	// //									
	// //									$pslip = array(
	// //													'ESCROW_ACC' => $row['ESCROW_ACC'],
	// //													'PS_CCY' => $row['PS_CCY'],
	// //													'ESCROW_ACC_TYPE' => $row['ESCROW_ACC_TYPE']
	// //												);
	// //									$amountRev = $this->getTotalAmountReversal($slipnumb);
	// //									
	// //									$Payment = new Payment($slipnumb);
	// //									
	// //									$resultReversal = $Payment->sendTransferSingleWithinReversal($pslip, $cek[0]['SOURCE_ACCOUNT'], $amountRev);
	// //									if($resultReversal['ResponseCode'] != '00')
	// //									{
	// //										$isExceptionReversal = true;
	// //									}
	// //								}
	// //								
	// //								if($isExceptionReversal)
	// //									$updateApprove['PS_STATUS'] = 9; // exception reversal
	// //								else
	// //								{
	// //									$updateApprove['PS_STATUS'] = 5; // complete
	// //									$updateApprove['SENDFILE_STATUS'] = 4;
	// //								}
	// //								$whereApprove['PS_NUMBER = ?'] = $slipnumb;
	// //								$query = $this->_db->update ( "T_PSLIP", $updateApprove, $whereApprove );
	// //								
	// //							}
	// //							else // failed
	// //							{
	// //								$updateApprove['PS_STATUS'] = 6; // failed
	// //								$whereApprove['PS_NUMBER = ?'] = $slipnumb;
	// //								$query = $this->_db->update ( "T_PSLIP", $updateApprove, $whereApprove );
	// //						
	// //								foreach($cek as $row)
	// //								{
	// //									$cekDomestic = $this->_db->select()
	// //														->from(array('A' => 'T_TRANSACTION'),array('A.TRANSFER_TYPE'))
	// //														->where("A.TRANSACTION_ID LIKE ".$this->_db->quote($row['TRANSACTION_ID']))
	// //														->where("A.TRANSFER_TYPE != 0");
	// //														
	// //									$cekDomestic = $this->_db->fetchRow($cekDomestic);
	// //									if($cekDomestic) 
	// //									{
	// //										$updatedetailApprove['EFT_STATUS'] = '2';
	// //									}
	// //									$updatedetailApprove['TRA_STATUS'] = '4';
	// //									$wheredetailApprove['TRANSACTION_ID = ?'] = $row['TRANSACTION_ID'];
	// //									$this->_db->update('T_TRANSACTION', $updatedetailApprove,$wheredetailApprove);
	// //								}
	// //
	// //								// DO REVERSAL
	// //								{
	// //									$updateApprove['SENDFILE_STATUS'] = 3;
	// //									$whereApprove['PS_NUMBER = ?'] = $slipnumb;
	// //									$query = $this->_db->update ( "T_PSLIP", $updateApprove, $whereApprove );
	// //									
	// //									$pslip = array(
	// //													'ESCROW_ACC' => $row['ESCROW_ACC'],
	// //													'PS_CCY' => $row['PS_CCY'],
	// //													'ESCROW_ACC_TYPE' => $row['ESCROW_ACC_TYPE']
	// //												);
	// //									$amountRev = $this->getTotalAmountReversal($slipnumb);
	// //
	// //									$Payment = new Payment($slipnumb);
	// //									$resultReversal = $Payment->sendTransferSingleWithinReversal($pslip, $cek[0]['SOURCE_ACCOUNT'], $amountRev);
	// //									if($resultReversal['ResponseCode'] != '00')
	// //									{
	// //										$isExceptionReversal = true;
	// //									}
	// //								}
	// //								
	// //								if($isExceptionReversal)
	// //									$updateApprove['PS_STATUS'] = 9; // exception reversal
	// //								else
	// //								{
	// //									$updateApprove['PS_STATUS'] = 6; // complete
	// //									$updateApprove['SENDFILE_STATUS'] = 4;
	// //								}
	// //								$whereApprove['PS_NUMBER = ?'] = $slipnumb;
	// //								$query = $this->_db->update ( "T_PSLIP", $updateApprove, $whereApprove );
	// //
	// //							}

					
	// 				if($cek[0]['PS_STATUS']==5) // if success
	// 				{
	// 					$isExceptionReversal = false;
	// 					$numTrxFailed = 0;
	// 					foreach($cek as $row)
	// 					{
	// 						if($row['TRA_STATUS'] == '4') // ff failed, transfer reversal
	// 						{
	// 							$numTrxFailed += 1;
	// 						}
							
	// 						$cekDomestic = $this->_db->select()
	// 											->from(array('A' => 'T_TRANSACTION'),array('A.TRANSFER_TYPE'))
	// 											->where("A.TRANSACTION_ID LIKE ".$this->_db->quote($row['TRANSACTION_ID']))
	// 											->where("A.TRANSFER_TYPE != 0");
												
	// 						$cekDomestic = $this->_db->fetchRow($cekDomestic);
	// 						if($cekDomestic && $row['TRA_STATUS'] == '4') 
	// 						{
	// 							$updatedetailApprove['EFT_STATUS'] = '2';
	// 						}
							
	// 						$updatedetailApprove['TRA_STATUS'] = $row['TRA_STATUS'];
	// 						$wheredetailApprove['TRANSACTION_ID = ?'] = $row['TRANSACTION_ID'];
	// 						$this->_db->update('T_TRANSACTION', $updatedetailApprove,$wheredetailApprove);
	// 					}
						
	// 					if($numTrxFailed>0) // DO REVERSAL
	// 					{
	// 						$updateApprove['SENDFILE_STATUS'] = 3;
	// 						$updateApprove['PS_UPDATED'] = new Zend_Db_Expr('GETDATE()');
	// 						$whereApprove['PS_NUMBER = ?'] = $slipnumb;
	// 						$query = $this->_db->update ( "T_PSLIP", $updateApprove, $whereApprove );
							
	// 						$pslip = array(
	// 										'ESCROW_ACC' => $row['ESCROW_ACC'],
	// 										'PS_CCY' => $row['PS_CCY'],
	// 										'ESCROW_ACC_TYPE' => $row['ESCROW_ACC_TYPE']
	// 									);
	// 						$amountRev = $this->getTotalAmountReversal($slipnumb);
							
	// 						$Payment = new Payment($slipnumb);
							
	// //									$resultReversal = $Payment->sendTransferSingleWithinReversal($pslip, $cek[0]['SOURCE_ACCOUNT'], $amountRev);
	// //									if($resultReversal['ResponseCode'] != '00')
	// //									{
	// //										$isExceptionReversal = true;
	// //									}
	// 					}
						
	// 					if($isExceptionReversal)
	// 						$updateApprove['PS_STATUS'] = 9; // exception reversal
	// 					else
	// 					{
	// 						$updateApprove['PS_STATUS'] = 5; // complete
	// 						$updateApprove['SENDFILE_STATUS'] = 4;
	// 					}
	// 					$updateApprove['PS_UPDATED'] = new Zend_Db_Expr('GETDATE()');
	// 					$whereApprove['PS_NUMBER = ?'] = $slipnumb;
	// 					$query = $this->_db->update ( "T_PSLIP", $updateApprove, $whereApprove );
						
	// 				}
	// 				else // failed
	// 				{
	// 					$updateApprove['PS_STATUS'] = 6; // failed
	// 					$updateApprove['PS_UPDATED'] = new Zend_Db_Expr('GETDATE()');
	// 					$whereApprove['PS_NUMBER = ?'] = $slipnumb;
	// 					$query = $this->_db->update ( "T_PSLIP", $updateApprove, $whereApprove );
				
	// 					foreach($cek as $row)
	// 					{
	// 						$cekDomestic = $this->_db->select()
	// 											->from(array('A' => 'T_TRANSACTION'),array('A.TRANSFER_TYPE'))
	// 											->where("A.TRANSACTION_ID LIKE ".$this->_db->quote($row['TRANSACTION_ID']))
	// 											->where("A.TRANSFER_TYPE != 0");
												
	// 						$cekDomestic = $this->_db->fetchRow($cekDomestic);
	// 						if($cekDomestic) 
	// 						{
	// 							$updatedetailApprove['EFT_STATUS'] = '2';
	// 						}
	// 						$updatedetailApprove['TRA_STATUS'] = '4';
	// 						$wheredetailApprove['TRANSACTION_ID = ?'] = $row['TRANSACTION_ID'];
	// 						$this->_db->update('T_TRANSACTION', $updatedetailApprove,$wheredetailApprove);
	// 					}

	// 					// DO REVERSAL
	// 					{
	// 						$updateApprove['SENDFILE_STATUS'] = 3;
	// 						$updateApprove['PS_UPDATED'] = new Zend_Db_Expr('GETDATE()');
	// 						$whereApprove['PS_NUMBER = ?'] = $slipnumb;
	// 						$query = $this->_db->update ( "T_PSLIP", $updateApprove, $whereApprove );
							
	// 						$pslip = array(
	// 										'ESCROW_ACC' => $row['ESCROW_ACC'],
	// 										'PS_CCY' => $row['PS_CCY'],
	// 										'ESCROW_ACC_TYPE' => $row['ESCROW_ACC_TYPE']
	// 									);
	// 						$amountRev = $this->getTotalAmountReversal($slipnumb);

	// 						$Payment = new Payment($slipnumb);
	// //									$resultReversal = $Payment->sendTransferSingleWithinReversal($pslip, $cek[0]['SOURCE_ACCOUNT'], $amountRev);
	// //									if($resultReversal['ResponseCode'] != '00')
	// //									{
	// //										$isExceptionReversal = true;
	// //									}
	// 					}
						
	// 					if($isExceptionReversal)
	// 						$updateApprove['PS_STATUS'] = 9; // exception reversal
	// 					else
	// 					{
	// 						$updateApprove['PS_STATUS'] = 6; // complete
	// 						$updateApprove['SENDFILE_STATUS'] = 4;
	// 					}
	// 					$updateApprove['PS_UPDATED'] = new Zend_Db_Expr('GETDATE()');
	// 					$whereApprove['PS_NUMBER = ?'] = $slipnumb;
	// 					$query = $this->_db->update ( "T_PSLIP", $updateApprove, $whereApprove );

	// 				}
	// 			}
	// 			else if($cek[0]['SENDFILE_STATUS']==3) // exception on reversal
	// 			{
	// 				if($cek[0]['PS_STATUS']==1) // if resend reversal
	// 				{
	// 					$pslip = array(
	// 									'ESCROW_ACC' => $cek[0]['ESCROW_ACC'],
	// 									'PS_CCY' => $cek[0]['PS_CCY'],
	// 									'ESCROW_ACC_TYPE' => $cek[0]['ESCROW_ACC_TYPE']
	// 								);
	// 					$amountRev = $this->getTotalAmountReversal($slipnumb);

	// 					$Payment = new Payment($slipnumb);
	// 					$resultReversal = $Payment->sendTransferSingleWithinReversal($pslip, $cek[0]['SOURCE_ACCOUNT'], $amountRev);
	// 					if($resultReversal['ResponseCode'] == '00')
	// 					{
	// 						$updateApprove['PS_STATUS'] = 5; // complete
	// 						$updateApprove['SENDFILE_STATUS'] = 4;
	// 					}
	// 					else{
	// 						$updateApprove['PS_STATUS'] = 9; // exception reversal
	// 					}
						
	// 					$updateApprove['PS_UPDATED'] = new Zend_Db_Expr('GETDATE()');	
	// 					$whereApprove['PS_NUMBER = ?'] = $slipnumb;
	// 					$query = $this->_db->update ( "T_PSLIP", $updateApprove, $whereApprove );
						
	// 				}
	// 				else if($cek[0]['PS_STATUS']==5) // if success
	// 				{
	// 					$updateApprove['PS_STATUS'] = 5; // complete
	// 					$updateApprove['SENDFILE_STATUS'] = 4;
	// 					$updateApprove['PS_UPDATED'] = new Zend_Db_Expr('GETDATE()');
	// 					$whereApprove['PS_NUMBER = ?'] = $slipnumb;
	// 					$query = $this->_db->update ( "T_PSLIP", $updateApprove, $whereApprove );
	// 				}
	// 			}
	// 		}
	// 		else
	// 		{
				
				// if ($cek[0]['PS_STATUS'] == 3) {
				// 	$updateApprove['PS_STATUS'] = 5;
				// }elseif ($cek[0]['PS_STATUS'] == 4) {
				// 	$updateApprove['PS_STATUS'] = 6;
				// }

				$select1 = $this->_db->select()
								->from(	array(	'T'				=>'T_PSLIP_EXCEPTION_REPAIR_DETAIL'),
										array(	'counttrx'		=>'COUNT(T.TRANSACTION_ID)',
											))
								->where('T.SUGGEST_ID =? ',$suggestid);
				$count1 = $this->_db->fetchRow($select1);

				if ($count1['counttrx'] == 1) {
					
					$select2 = $this->_db->select()
								->from(	array(	'T'				=>'T_PSLIP_EXCEPTION_REPAIR_DETAIL'),
										array(	'ps_status'		=>'TRA_STATUS',
											))
								->where('T.SUGGEST_ID =? ',$suggestid);
					$count2 = $this->_db->fetchRow($select2);

					if ($count2['ps_status'] == 3) {
						
						$updatepsstatus = 5;
					}else{
						$updatepsstatus = 6;
					}

					$updateApprove['PS_STATUS'] = $updatepsstatus;
					$updateApprove['PS_UPDATED'] = new Zend_Db_Expr('GETDATE()');
					$whereApprove['PS_NUMBER = ?'] = $slipnumb;
					$query = $this->_db->update ( "T_PSLIP", $updateApprove, $whereApprove );
					
					$cekDomestic = $this->_db->select()
										->from(array('A' => 'T_TRANSACTION'),array('A.TRANSFER_TYPE'))
										->where("A.TRANSACTION_ID LIKE ".$this->_db->quote($cek[0]['TRANSACTION_ID']))
										->where("A.TRANSFER_TYPE NOT LIKE '0'");
										
					$cekDomestic = $this->_db->fetchRow($cekDomestic);
					
					foreach($cek as $row)
					{
						if($cekDomestic && $row['TRA_STATUS'] == '4') 
						{
							$updatedetailApprove['EFT_STATUS'] = '2';
						}
						$updatedetailApprove['TRA_STATUS'] = $row['TRA_STATUS'];
						$wheredetailApprove['TRANSACTION_ID = ?'] = $row['TRANSACTION_ID'];
						$this->_db->update('T_TRANSACTION', $updatedetailApprove,$wheredetailApprove);
					}
					
					// $selectQuery	= "SELECT PS_STATUS,TRA_AMOUNT,SOURCE_ACCOUNT FROM T_PSLIP P INNER JOIN T_TRANSACTION T ON P.PS_NUMBER = T.PS_NUMBER 
					// WHERE P.CUST_ID = ".$this->_db->quote($cek[0]['CUST_ID'])." AND T.PS_NUMBER = ".$this->_db->quote($slipnumb);

					// $resultQuery =  $this->_db->fetchAll($selectQuery);
			
					// if(is_array($resultQuery)){
						// foreach ($resultQuery as $key => $data) {
							// if($data['PS_STATUS']!='3'){
								// $sourceAccount =  $data['SOURCE_ACCOUNT'];
								// $holdAmountRelease = $data['TRA_AMOUNT'];
								// $systemBalance =  new systemBalance($cek[0]['CUST_ID'], $sourceAccount);
								// $systemBalance->releaseAmountHold($holdAmountRelease,$slipnumb);
							// }
						// }
					// }

				}elseif ($count1['counttrx'] > 1) {
					
					$select2 = $this->_db->select()
								->from(	array(	'T'					=>'T_PSLIP_EXCEPTION_REPAIR_DETAIL'),
										array(	'counttrxbulk'		=>'COUNT(T.TRANSACTION_ID)',
											))
								->where('T.TRA_STATUS = ?', '3')
								->where('T.SUGGEST_ID =? ',$suggestid);
					$count2 = $this->_db->fetchRow($select2);

					if ($count2['counttrxbulk'] >= 1) {
						
						$updatepsstatus = 5;
					}else{
						$updatepsstatus = 6;
					}

					$updateApprove['PS_STATUS'] = $updatepsstatus;
					$updateApprove['PS_UPDATED'] = new Zend_Db_Expr('GETDATE()');
					$whereApprove['PS_NUMBER = ?'] = $slipnumb;
					$query = $this->_db->update ( "T_PSLIP", $updateApprove, $whereApprove );
					
					$cekDomestic = $this->_db->select()
										->from(array('A' => 'T_TRANSACTION'),array('A.TRANSFER_TYPE'))
										->where("A.TRANSACTION_ID LIKE ".$this->_db->quote($cek[0]['TRANSACTION_ID']))
										->where("A.TRANSFER_TYPE NOT LIKE '0'");
										
					$cekDomestic = $this->_db->fetchRow($cekDomestic);
					
					foreach($cek as $row)
					{
						if($cekDomestic && $row['TRA_STATUS'] == '4') 
						{
							$updatedetailApprove['EFT_STATUS'] = '2';
						}
						$updatedetailApprove['TRA_STATUS'] = $row['TRA_STATUS'];
						$wheredetailApprove['TRANSACTION_ID = ?'] = $row['TRANSACTION_ID'];
						$this->_db->update('T_TRANSACTION', $updatedetailApprove,$wheredetailApprove);
					}
					
				}
				
				$insertHistory['DATE_TIME'] = new Zend_Db_Expr('now()');
				$insertHistory['PS_NUMBER'] = $slipnumb;
				$insertHistory['USER_LOGIN'] = $this->_userIdLogin;
				$insertHistory['CUST_ID'] = $cek[0]['CUST_ID'];
				$insertHistory['HISTORY_STATUS'] = '24';
				$insertHistory['PS_REASON'] = 'Request Approved';
				$this->_db->insert('T_PSLIP_HISTORY', $insertHistory);
				
			//}
			// $this->_db->commit();
			// $this->_redirect('/suspectchangerequest/index');
			$ns = new Zend_Session_Namespace('FVC');
	    	$ns->backURL = $this->view->backURL;
			$this->_redirect('/notification/success');

  		}


		$AESMYSQL = new Crypt_AESMYSQL();
		$PS_NUMBER = $AESMYSQL->decrypt($this->_getParam('payRef'), $password);

		$suggestid = $this->_getParam('suggestid');

		$this->_helper->layout()->setLayout('newlayout');
		$filter 			= new Application_Filtering();
		$pdf 				= $filter->filter($this->_getParam('pdf')					, "BUTTON");
		$this->_paymentRef 	= $PS_NUMBER;

		$select1 = $this->_db->select()
								->from(	array(	'T'				=>'T_PSLIP_EXCEPTION_REPAIR_DETAIL'),
										array(	'counttrx'		=>'COUNT(T.TRANSACTION_ID)',
											))
								->where('T.SUGGEST_ID =? ',$suggestid);
		$count1 = $this->_db->fetchRow($select1);

		$this->view->numbertrx 			= $count1['counttrx'];

		$conf = Zend_Registry::get('config');
		$paymentStatus = $conf['payment']['status'];
		$paymentStatusFlip = array_flip($paymentStatus['code']);

		$this->view->paymentStatus = $paymentStatus;
		$this->view->paymentStatusFlip = $paymentStatusFlip;

		$sessionNamespace = new Zend_Session_Namespace('URL_CP_PR');
		$this->view->backURL = (!empty($sessionNamespace->URL)) ?
									   $sessionNamespace->URL : '/'.$this->view->modulename.'/'.$this->_controllerList.'/index';

		$arrPayStatus 	= array_combine($this->_paymentstatus["code"],$this->_paymentstatus["desc"]);
		$arrPayType 	= array_combine($this->_paymenttype["code"],$this->_paymenttype["desc"]);

		$casePayStatus = "(CASE P.PS_STATUS ";
  		foreach($arrPayStatus as $key => $val)	{ $casePayStatus .= " WHEN ".$key." THEN '".$val."'"; }
  		$casePayStatus .= " END)";

		$casePayType = "(CASE P.PS_TYPE ";
  		foreach($arrPayType as $key => $val)	{ $casePayType .= " WHEN ".$key." THEN '".$val."'"; }
  		$casePayType .= " END)";

  		$approvalstatus = $this->_aprovalstatus;
		$statusarr = array_combine(array_values($approvalstatus['code']),array_values($approvalstatus['desc']));
  		$caseStatus = "(CASE A.SUGGEST_STATUS ";
  		foreach($statusarr as $key=>$val)
  		{
   			$caseStatus .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseStatus .= " END)";

		$select	= $this->_db->select()
							->from(		array(	'P' 		=> 'T_PSLIP'),
										array(	'payStatus'	=>$casePayStatus,
												'PS_STATUS'	=>'P.PS_STATUS',
												'ps_statusrepair'	=> 'A.PS_STATUS',
												'compCode'	=>'C.CUST_ID',
												'compName'	=>'C.CUST_NAME',
												'paySubject'=>'P.PS_SUBJECT',
												'pscount'	=>'P.PS_TXCOUNT',
												'created'	=>'P.PS_CREATED',
												'createdby'	=>'P.PS_CREATEDBY',
												'updated'	=>'P.PS_UPDATED',
												'efdate'	=>'P.PS_EFDATE',
												'psperiodic'=>'P.PS_PERIODIC',
												'sumamount'	=>'P.PS_TOTAL_AMOUNT',
												'PS_CATEGORY'	=>'P.PS_CATEGORY',
												'periodicuom'	=>'TP.PS_EVERY_PERIODIC_UOM',
												'periodicnumber'=>'TP.PS_PERIODIC_NUMBER',
												'ccy'			=>'P.PS_CCY',
												'ps_file'		=>'P.PS_FILE',
												'addmessage'	=>'T.TRA_ADDITIONAL_MESSAGE',
												'transmission'	=>'',
												'reference'		=>'',
												'traceno'		=>'P.UUID',
												'releaser'		=>'P.PS_RELEASER_USER_LOGIN',
												'challengecode'	=>'P.PS_RELEASER_CHALLENGE',
												'PS_TYPE'		=>'P.PS_TYPE',
												'bookrate'		=>'T.BOOK_RATE',
												'bookbuy'		=>'T.BOOK_RATE_BUY',
												'amount'		=>'T.TRA_AMOUNT',
												'transferfee'	=>'T.TRANSFER_FEE',
												'provfee'		=>'T.PROVISION_FEE',
												'fafee'			=>'T.FULL_AMOUNT_FEE',
												'sourceccy'		=>'T.SOURCE_ACCOUNT_CCY',
												'beneccy'		=>'T.BENEFICIARY_ACCOUNT_CCY',
												'payType'		=>$casePayType,
												'status'		=>$caseStatus,
												'suggestuser'	=>'A.SUGGEST_USER',
										  		'suggestdate'	=>'A.SUGGEST_DATE',
										  		'suggestid'		=>'A.SUGGEST_ID',
										  		'extype'		=>'A.EXCEPTION_TYPE',
										  		'custid'		=>'A.CUST_ID',
												'BALANCE_TYPE'=>new Zend_Db_Expr("(SELECT BALANCE_TYPE FROM T_PERIODIC_DETAIL WHERE PS_PERIODIC = P.PS_PERIODIC limit 1)"),
												'sourceresident'=>'CA.ACCT_RESIDENT',
												'sourcecitizen'=>'CA.ACCT_CITIZENSHIP',
												'sourcecategory'=>'CA.ACCT_CATEGORY',
												// 'sourceidtype'=>'CA.ACCT_ID_TYPE',
												// 'sourceidnum'=>'CA.ACCT_ID_NUM',
												'benefresident'=>'T.BENEFICIARY_RESIDENT',
												'benefcitizen'=>'T.BENEFICIARY_CITIZENSHIP',
												'benefcategory'=>'T.BENEFICIARY_CATEGORY',
												// 'benefidtype'=>'T.BENEFICIARY_ID_TYPE',
												// 'benefidnum'=>'T.BENEFICIARY_ID_NUMBER',
												'lldidentity'=>'T.LLD_IDENTITY',
												'message'=>'T.TRA_MESSAGE',
												'lldpurpose'=>'T.LLD_TRANSACTION_PURPOSE',
												'lldrel'=>'T.LLD_TRANSACTOR_RELATIONSHIP',
												'T.EQUIVALENT_AMOUNT_IDR',
												'T.EQUIVALENT_AMOUNT_USD',
												'T.TRACE_NO',
												'PS_REMAIN'		=>'P.PS_REMAIN'

											))
							->joinLeft(	array(	'C' => 'M_CUSTOMER' ),'P.CUST_ID = C.CUST_ID',array())
							->joinLeft(	array(	'T' => 'T_TRANSACTION' ),'T.PS_NUMBER = P.PS_NUMBER',array('T.TRA_AMOUNT'))
							->joinLeft(	array(	'TP' => 'T_PERIODIC' ),'TP.PS_PERIODIC = P.PS_PERIODIC',array())
							->joinLeft( array( 'CA' => 'M_CUSTOMER_ACCT'), 'T.SOURCE_ACCOUNT = CA.ACCT_NO', array())
							->joinLeft( array( 'A' => 'T_PSLIP_EXCEPTION_REPAIR'), 'P.PS_NUMBER = A.PS_NUMBER', array())
							->where('P.PS_NUMBER =? ',$PS_NUMBER);
		$pslip = $this->_db->fetchRow($select);

		$PSSTATUS  = $pslip["PS_STATUS"];
		$payStatus = $pslip["payStatus"];
// die;
		if($PSSTATUS == 5){

			// COMPLETED WITH (_) TRANSACTION (S) FAILED
			// TRA_STATUS FAILED (4)

			$select = $this->_db->select()
								->from('T_TRANSACTION',array('countfailed'=>'count(TRANSACTION_ID)'))
								->where("TRA_STATUS = '4' AND PS_NUMBER = ?",$PS_NUMBER);
			$countFailed = $this->_db->fetchOne($select);

			if($countFailed == 0) 	$value = $payStatus;
			else 					$value = 'Completed with '.$countFailed.' Failed Transaction(s)';

		}
		else $value = $payStatus;

		$persenLabel = $pslip["BALANCE_TYPE"] == '2' ? ' %' : '';
		// View Data
		$this->_tableMst[0]["label"] = "Payment Ref#";
		$this->_tableMst[1]["label"] = "Payment Status";
		$this->_tableMst[2]["label"] = "Company Code";
		$this->_tableMst[3]["label"] = "Company Name";
		$this->_tableMst[4]["label"] = "Payment Subject";
		$this->_tableMst[5]["label"] = "Master Account";
		$this->_tableMst[6]["label"] = "Created Date";
		$this->_tableMst[7]["label"] = "Updated Date";
		$this->_tableMst[8]["label"] = "Payment Date";
		// $this->_tableMst[9]["label"] = "Transmission";
		// $this->_tableMst[10]["label"] = "User Reference";
		//$this->_tableMst[11]["label"] = "Bank Response";
		//$this->_tableMst[12]["label"] = "Bank Resp. Code";
		$this->_tableMst[11]["label"] = "Trace No.";
		$this->_tableMst[12]["label"] = "Releaser";
		$this->_tableMst[13]["label"] = "Challenge Code";
		$this->_tableMst[14]["label"] = "Total Payment";
		$this->_tableMst[15]["label"] = "Payment Type";

		$this->view->suggestuser 	= $pslip['suggestuser'];
		$this->view->suggestdate 	= $pslip['suggestdate'];
		$this->view->status 		= $pslip['status'];
		$this->view->suggestid 		= $pslip['suggestid'];
		$this->view->extype 		= $pslip['extype'];
		$this->view->custid 		= $pslip['custid'];
		$this->view->ps_statusrepair = $pslip['ps_statusrepair'];
		$this->view->ps_file 		 = $pslip['ps_file'];
		$this->view->pscount 		 = $pslip['pscount'];
		$this->view->psnumber 		 = $PS_NUMBER;
		$this->view->paymentstatus 	 = $value;
		$this->view->compCode 		 = $pslip['compCode'];
		$this->view->compname 		 = $pslip['compName'];
		$this->view->pssubject 		 = $pslip['paySubject'];
		$this->view->message 		 = $pslip['message'];
		// $this->view->updated 	     = $pslip['updated'];
		// $this->view->createdby 	     = $pslip['createdby'];
		$this->view->addmessage 	 = $pslip['addmessage'];

		$suggested = '(Last Suggested by '.$pslip['suggestuser'].' - '.$pslip['suggestdate'].') - Unread Suggestion';
		$this->view->suggested = $suggested; 

		$this->_tableMst[5]["value"] = "";
		$this->_tableMst[6]["value"] = Application_Helper_General::convertDate($pslip['created'],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
		$this->_tableMst[7]["value"] = Application_Helper_General::convertDate($pslip['updated'],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
		$this->view->paymentdate 	 = Application_Helper_General::convertDate($pslip['efdate'],$this->_dateViewFormat);
		// $this->_tableMst[9]["value"] = ($PSSTATUS==5) ? Application_Helper_General::convertDate($pslip['updated'],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat) : "";
		// $this->_tableMst[10]["value"] = (isset($pslip['reference'])) ? $pslip['reference'] : '';
		//$this->_tableMst[11]["value"] = $pslip['bankresponse'];
		//$this->_tableMst[12]["value"] = $pslip['bankrespcode'];
		$this->_tableMst[11]["value"] = $pslip['TRACE_NO'];
		$this->_tableMst[12]["value"] = $pslip['releaser'];
		$this->_tableMst[13]["value"] = $pslip['challengecode'];

		if ($pslip['psperiodic'] != NULL || !empty($pslip['psperiodic'])) {

			if ($pslip['psperiodic'] == 0) {
				$frequently = '1x';
				$this->view->frequently = $frequently;
			}else{

				if ($pslip['periodicuom'] == 1) {
				$frequently = 'Daily'.' (FDP Ref# '.$pslip['periodicnumber'].')';
				$this->view->frequently = $frequently; 
				}
				else if($pslip['periodicuom'] == 2){
					$frequently = 'Weekly'.' (FDP Ref# '.$pslip['periodicnumber'].')';
					$this->view->frequently = $frequently;
				}
				else if($pslip['periodicuom'] == 3){
					$frequently = 'Monthly'.' (FDP Ref# '.$pslip['periodicnumber'].')';
					$this->view->frequently = $frequently;
				}

			}

		}else{
			$frequently = '1x';
			$this->view->frequently = $frequently;
		}

		//print_r($pslip);die;
		if(!empty($persenLabel)){
			if($pslip['PS_TYPE']=='14' || $pslip['PS_TYPE']=='15'){
			$this->view->totalamount = $pslip['sumamount'].'% ('.$pslip['ccy'].' '.Application_Helper_General::displayMoney($pslip['PS_REMAIN']).')';
			}else{
			$this->view->totalamount = $pslip['sumamount'].'% ('.$pslip['ccy'].' '.Application_Helper_General::displayMoney($pslip['TRA_AMOUNT']).')';
			}

		}else{
			if($pslip['PS_TYPE']=='14' || $pslip['PS_TYPE']=='15'){
				$this->view->totalamount = $pslip['ccy'].' '.Application_Helper_General::displayMoney($pslip['PS_REMAIN']);
			}
			elseif($pslip['PS_TYPE'] != '3'){
				if($pslip['ccy']=='USD'){

					if($pslip['beneccy'] == 'USD' && $pslip['sourceccy']=='USD'){
						$tra_amout = $pslip['sumamount'];
						$this->view->totalamount = 'USD '.Application_Helper_General::displayMoney($tra_amout);

					}
					else if($pslip['EQUIVALENT_AMOUNT_IDR']!='0.00'){
						$tra_amout = $pslip['EQUIVALENT_AMOUNT_IDR'];
						$this->view->totalamount = $pslip['ccy'].' '.Application_Helper_General::displayMoney($pslip['sumamount']).' (IDR '.Application_Helper_General::displayMoney($pslip['EQUIVALENT_AMOUNT_IDR']).')';
					}else{
						$tra_amout = $pslip['sumamount'];
						$this->view->totalamount = $pslip['ccy'].' '.Application_Helper_General::displayMoney($pslip['sumamount']).' (IDR '.Application_Helper_General::displayMoney($pslip['EQUIVALENT_AMOUNT_IDR']).')';
					}

				}else{
					$this->view->totalamount = $pslip['ccy'].' '.Application_Helper_General::displayMoney($pslip['sumamount']);
				}

			}else{

				if($pslip['beneccy'] == 'USD' && $pslip['sourceccy']=='USD'){

					$this->view->totalamount = 'USD '.Application_Helper_General::displayMoney($pslip['EQUIVALENT_AMOUNT_IDR']);
				}else if($pslip['sourceccy'] == 'USD' && $pslip['ccy'] == 'USD'){
					$totalinvalas = $pslip['EQUIVALENT_AMOUNT_USD'];
					$totalinidr = $pslip['EQUIVALENT_AMOUNT_IDR'];
					//$totalinvalas = $pslip['amount'] + $pslip['transferfee'] + $pslip['provfee'] + $pslip['fafee'];
					//$totalinidr = $totalinvalas * $pslip['bookrate'];
						$this->view->totalamount = 'IDR '.Application_Helper_General::displayMoney($totalinidr);
				}
				else{
					//$totalinidr = (($pslip['amount'] + $pslip['fafee'])*$pslip['bookrate']) + $pslip['transferfee'];
					//$totalinvalas = $totalinidr / $pslip['bookbuy'];
					$totalinidr = $pslip['EQUIVALENT_AMOUNT_IDR'];
					$totalinvalas = $pslip['amount'];
				$this->view->totalamount = 'IDR '.Application_Helper_General::displayMoney($totalinidr);
				}


			}
		}

		//die;
		$this->view->payType = $pslip["payType"];
		$this->view->pstype = $pslip["PS_TYPE"];
		// print_r($pslip);die;
		// if ($pslip["PS_CATEGORY"] == "BULK PAYMENT")
		// {
			// download trx bulk file
			$downloadURL = $this->view->url(array('module'=>'suspectchangerequest','controller'=>'detail','action'=>'downloadtrx','csv'=>'1','payReff'=>$PS_NUMBER),null,true);
			//$this->_tableMst[18]["label"] = "Download File";
			$this->view->download = $this->view->formButton('download','download',array('class'=>'inputbtn', 'onclick'=>"window.location = ".$this->_db->quote($downloadURL).";"));

			$downloadURL2 = $this->view->url(array('module'=>'suspectchangerequest','controller'=>'detail','action'=>'downloadtrx2','csv'=>'1','payReff'=>$PS_NUMBER,'suggestid'=>$suggestid),null,true);
			//$this->_tableMst[18]["label"] = "Download File";
			$this->view->download2 = $this->view->formButton('download','download',array('class'=>'inputbtn', 'onclick'=>"window.location = ".$this->_db->quote($downloadURL2).";"));

		//}

		// separate credit and debet view
		// if pstype = multidebet, bulkdebet
		if ($pslip["PS_TYPE"] == $this->_paymenttype["code"]["multidebet"] || $pslip["PS_TYPE"] == $this->_paymenttype["code"]["bulkdebet"])
			$this->debet($pslip);
		else if ($pslip["PS_TYPE"] == $this->_paymenttype["code"]["within"])
			$this->within($pslip);
		else if ($pslip["PS_TYPE"] == $this->_paymenttype["code"]["domestic"]){
			$this->domestic($pslip);
			// $this->LLDContent($pslip);
		}
		elseif($pslip["PS_TYPE"] == $this->_paymenttype["code"]["remittance"]){
			$this->credit($pslip);
			// $this->LLDContent($pslip);
		}
		else
			$this->credit($pslip);

		$this->approvalMatrix($pslip);
		$this->userInvolved($pslip);

		$this->view->PS_NUMBER 			= $PS_NUMBER;

		$this->view->tableMst 			= $this->_tableMst;
		$this->view->totalTrx 			= (isset($pslip["numtrx"])) ? $pslip["numtrx"] : '';
		$this->view->totalAmt 			= (isset($pslip["amount"])) ? $pslip["amount"] : '';
		$this->view->pdf 				= ($pdf)? true: false;

		if($this->view->tableDtl)
		{
			$tableDtl = $this->view->tableDtl;

//			echo($PSSTATUS);die;
			if($PSSTATUS == '1' || $PSSTATUS == '2' || $PSSTATUS == '3' || $PSSTATUS == '7')
			{

				$no=0;
				foreach($tableDtl as $row)
				{
					if ($pslip["PS_TYPE"] == $this->_paymenttype["code"]["domestic"] || $pslip["PS_TYPE"] == $this->_paymenttype["code"]["multidebet"] || $pslip["PS_TYPE"] == $this->_paymenttype["code"]["bulkdebet"])
					{
						$transferType = $row['TRANS_TYPE_FULL'];
					}
					else
					{
						$transferType = $row['TRANSFER_TYPE'];
					}

					//echo($row['SOURCE_ACCOUNT']." -- ".$pslip["compCode"]." == ".$pslip["PS_TYPE"]." - - ".$pslip["compCode"]." - - ".$transferType."<br>");

					if($pslip["PS_TYPE"] !== $this->_paymenttype["code"]["remittance"]){
						$chargesObj = Charges::factory($pslip["compCode"],$transferType);
						$paramCharges = array('accsrc'=>$row['SOURCE_ACCOUNT'],'transferType'=>$transferType);
						$chargesAmt = $chargesObj->getCharges($paramCharges);
					}

					//Zend_Debug::dump($chargesAmt);
					//$this->view->tableDtl[$no]['TRANSFER_FEE']=$chargesAmt;
					//$this->view->tableDtl[$no]['TRANSFER_FEE']= Application_Helper_General::displayMoney($chargesAmt);

					$no++;
				}
			}
			/*else if($PSSTATUS == '5' || $PSSTATUS == '6' || $PSSTATUS == '9')
			{

			}*/
			else if($PSSTATUS == '4' || $PSSTATUS == '14')
			{
				$no=0;
				foreach($tableDtl as $row)
				{
					// $this->view->tableDtl[$no]['TRANSFER_FEE']= ' - ';
					$no++;
				}
			}

			$no=0;
			foreach($tableDtl as $row)
			{
									//
				if($tableDt1[$no]['TRANSFER_TYPE']=='PB'){
					$tableDt1[$no]['TRANSFER_TYPE'] = 'Mayapada';
				}
				//unset($this->view->tableDtl[$no]['SOURCE_ACCOUNT']);
					$no++;
			}
			//print_r($tableDtl);die;
			//unset($this->view->fields['SOURCE_ACCOUNT']);
			//Zend_Debug::dump($this->view->tableDtl);
		}

		if($pdf)
		{
			$outputHTML = "<tr><td>".$this->view->render('/detail/index.phtml')."</td></tr>";
			$this->_helper->download->pdf(null,null,null,'Payment Detail',$outputHTML);
			Application_Helper_General::writeLog('RPPY','Download PDF Payment Detail Report');
		}
		else
		{
			Application_Helper_General::writeLog('RPPY','View Payment Detail Report');
		}


	}

	public function downloadtrxAction()
	{
		$PS_NUMBER 			= trim(strip_tags($this->_getParam('payReff')));
		$arrPayStatus 	= array_combine($this->_paymentstatus["code"],$this->_paymentstatus["desc"]);

		$casePayStatus = "(CASE P.PS_STATUS ";
  		foreach($arrPayStatus as $key => $val)	{ $casePayStatus .= " WHEN ".$key." THEN '".$val."'"; }
  		$casePayStatus .= " END)";


		$select	= $this->_db->select()
							->from(		array(	'P' 		=> 'T_PSLIP'),
										array(	'payStatus'	=>$casePayStatus,
												'PS_STATUS'	=>'P.PS_STATUS',
												'compCode'	=>'C.CUST_ID',
												'compName'	=>'C.CUST_NAME',
												'paySubject'=>'P.PS_SUBJECT',
												'created'	=>'P.PS_CREATED',
												'updated'	=>'P.PS_UPDATED',
												'efdate'	=>'P.PS_EFDATE',
												'sumamount'	=>'P.PS_TOTAL_AMOUNT',
												'PS_CATEGORY'	=>'P.PS_CATEGORY',
												'ccy'			=>'P.PS_CCY',
												'transmission'	=>'',
												'reference'		=>'',
												'bankresponse'	=>'',
												'bankrespcode'	=>'',
												'traceno'		=>'P.UUID',
												'releaser'		=>'P.PS_RELEASER_USER_LOGIN',
												'challengecode'	=>'P.PS_RELEASER_CHALLENGE',
												'PS_TYPE'		=>'P.PS_TYPE'

											))
							->joinLeft(	array(	'C' => 'M_CUSTOMER' ),'P.CUST_ID = C.CUST_ID',array())
							->where('P.PS_NUMBER =? ',$PS_NUMBER);

		$pslip = $this->_db->fetchRow($select);

		if (!empty($pslip))
		{
			// separate credit and debet view
			$companyName = $pslip['compName'];
			$companyCode = $pslip['compCode'];
			if ($pslip["PS_TYPE"] == $this->_paymenttype["code"]["multidebet"] || $pslip["PS_TYPE"] == $this->_paymenttype["code"]["bulkdebet"])
				$this->downloadTrxDebet($PS_NUMBER,$companyName,$companyCode);
			else
				$this->downloadTrxCredit($PS_NUMBER,$companyName,$companyCode);
		}
		else
		{
			$data = array();
			$data[0][0] = "Invalid Payment Number";
			$this->_helper->download->csv(array(),$data,null,$PS_NUMBER);
			Application_Helper_General::writeLog('RPPY','Download CSV Payment Detail Report');
		}
	}

	protected function downloadTrxCredit($PS_NUMBER,$companyName,$companyCode)
	{
		$arrTransferStatus 	= array_combine($this->_transferstatus["code"],$this->_transferstatus["desc"]);

		$caseTraStatus = "(CASE TT.TRA_STATUS ";
  		foreach($arrTransferStatus as $key => $val)	{ $caseTraStatus .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseTraStatus .= " END)";

		$company = $companyName.' ('.$companyCode.')';
		$headerData[] = array($company);
		$select	= $this->_db->select()
							->from(	array(	'TT' => 'T_TRANSACTION'),
									array(
											'TRANSACTION_ID'		=> 'TT.TRANSACTION_ID',
											'BANK_CODE'				=> 'D.BANK_NAME',
											'ACCTSRC'				=> 'TT.SOURCE_ACCOUNT',
											'BENEF_BANK_CODE'		=> 'E.BANK_NAME',
											'BENEFICIARY_ACCOUNT'	=> 'TT.BENEFICIARY_ACCOUNT',
											'CCY'					=> 'C.PS_CCY',
											'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
											'BANK_RESPONSE'			=> 'TT.BANK_RESPONSE',
											'TRA_STATUS'			=> $caseTraStatus,
										  )
									)
							->joinLeft(	array(	'C' => 'T_PSLIP' ),'TT.PS_NUMBER = C.PS_NUMBER',array())
							->joinLeft(	array(	'D' => 'M_BANK_TABLE' ),'TT.SOURCE_ACCT_BANK_CODE = D.BANK_CODE',array())
							->joinLeft(	array(	'E' => 'M_BANK_TABLE' ),'TT.BENEF_ACCT_BANK_CODE = E.BANK_CODE',array())
							->where('TT.PS_NUMBER = ?', $PS_NUMBER);

		$data = $this->_db->fetchAll($select);

		$newData[] = array("No","Transaction ID","Source Bank","Source Account","Beneficiary Bank","Beneficiary Account","CCY","Amount","RC","Status","Update to"," ","Hanya isi kolom Update to menjadi 0 = Failed, 1 = Success (jangan mengubah kolom lainnya)");
		$no = 1;
		foreach ($data as $p => $pTrx)
		{

			if ($pTrx['BANK_RESPONSE'] != NULL) {
				$bankresponse = explode(':', $pTrx['BANK_RESPONSE']);
				//$status = $bankresponse[1];
				$RC 	= $bankresponse[0];
			}

			
			$paramTrx = array(	"NO"					=> $no++,
								"TRANSACTION_ID"  		=> $pTrx['TRANSACTION_ID'],
								"BANK_CODE"  			=> $pTrx['BANK_CODE'],
								"ACCTSRC"  				=> $pTrx['ACCTSRC'],
								"BENEF_BANK_CODE"  		=> $pTrx['BENEF_BANK_CODE'],
								"BENEFICIARY_ACCOUNT" 	=> $pTrx['BENEFICIARY_ACCOUNT'],
								"CCY"  					=> $pTrx['CCY'],
								"TRA_AMOUNT"  			=> $pTrx['TRA_AMOUNT'],
								"RC"					=> $RC,
								"STATUS"				=> $pTrx['TRA_STATUS'],
							);

			$newData[] = $paramTrx;
			
		}
		$this->_helper->download->csv($headerData,$newData,null,$PS_NUMBER);
		Application_Helper_General::writeLog('RPPY','Download CSV Payment Detail Report');
	}

	protected function downloadTrxDebet($PS_NUMBER,$companyName,$companyCode)
	{
		$arrTransferStatus 	= array_combine($this->_transferstatus["code"],$this->_transferstatus["desc"]);

		$caseTraStatus = "(CASE TT.TRA_STATUS ";
  		foreach($arrTransferStatus as $key => $val)	{ $caseTraStatus .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseTraStatus .= " END)";

		$company = $companyName.' ('.$companyCode.')';
		$headerData[] = array($company);
		$select	= $this->_db->select()
							->from(	array(	'TT' => 'T_TRANSACTION'),
									array(
											'TRANSACTION_ID'		=> 'TT.TRANSACTION_ID',
											'BANK_CODE'				=> 'D.BANK_NAME',
											'ACCTSRC'				=> 'TT.SOURCE_ACCOUNT',
											'BENEF_BANK_CODE'		=> 'E.BANK_NAME',
											'BENEFICIARY_ACCOUNT'	=> 'TT.BENEFICIARY_ACCOUNT',
											'CCY'					=> 'C.PS_CCY',
											'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
											'BANK_RESPONSE'			=> 'TT.BANK_RESPONSE',
											'TRA_STATUS'			=> $caseTraStatus,
										  )
									)
							->joinLeft(	array(	'C' => 'T_PSLIP' ),'TT.PS_NUMBER = C.PS_NUMBER',array())
							->joinLeft(	array(	'D' => 'M_BANK_TABLE' ),'TT.SOURCE_ACCT_BANK_CODE = D.BANK_CODE',array())
							->joinLeft(	array(	'E' => 'M_BANK_TABLE' ),'TT.BENEF_ACCT_BANK_CODE = E.BANK_CODE',array())
							->where('TT.PS_NUMBER = ?', $PS_NUMBER);

		$data = $this->_db->fetchAll($select);

		$newData[] = array("No","Transaction ID","Source Bank","Source Account","Beneficiary Bank","Beneficiary Account","CCY","Amount","RC","Status","Update to"," ","Hanya isi kolom Update to menjadi 0 = Failed, 1 = Success (jangan mengubah kolom lainnya)");
		$no = 1;
		foreach ($data as $p => $pTrx)
		{

			if ($pTrx['BANK_RESPONSE'] != NULL) {
				$bankresponse = explode(':', $pTrx['BANK_RESPONSE']);
				//$status = $bankresponse[1];
				$RC 	= $bankresponse[0];
			}

			
			$paramTrx = array(	"NO"					=> $no++,
								"TRANSACTION_ID"  		=> $pTrx['TRANSACTION_ID'],
								"BANK_CODE"  			=> $pTrx['BANK_CODE'],
								"ACCTSRC"  				=> $pTrx['ACCTSRC'],
								"BENEF_BANK_CODE"  		=> $pTrx['BENEF_BANK_CODE'],
								"BENEFICIARY_ACCOUNT" 	=> $pTrx['BENEFICIARY_ACCOUNT'],
								"CCY"  					=> $pTrx['CCY'],
								"TRA_AMOUNT"  			=> $pTrx['TRA_AMOUNT'],
								"RC"					=> $RC,
								"STATUS"				=> $pTrx['TRA_STATUS'],
							);

			
			$newData[] = $paramTrx;
			
		}

		$this->_helper->download->csv($headerData,$newData,null,$PS_NUMBER);
		Application_Helper_General::writeLog('RPPY','Download CSV Payment Detail Report');
	}

	public function downloadtrx2Action()
	{
		$PS_NUMBER 			= trim(strip_tags($this->_getParam('payReff')));
		$suggestid 			= $this->_getParam('suggestid'); 
		$arrPayStatus 	= array_combine($this->_paymentstatus["code"],$this->_paymentstatus["desc"]);

		$casePayStatus = "(CASE P.PS_STATUS ";
  		foreach($arrPayStatus as $key => $val)	{ $casePayStatus .= " WHEN ".$key." THEN '".$val."'"; }
  		$casePayStatus .= " END)";


		$select	= $this->_db->select()
							->from(		array(	'P' 		=> 'T_PSLIP'),
										array(	'payStatus'	=>$casePayStatus,
												'PS_STATUS'	=>'P.PS_STATUS',
												'compCode'	=>'C.CUST_ID',
												'compName'	=>'C.CUST_NAME',
												'paySubject'=>'P.PS_SUBJECT',
												'created'	=>'P.PS_CREATED',
												'updated'	=>'P.PS_UPDATED',
												'efdate'	=>'P.PS_EFDATE',
												'sumamount'	=>'P.PS_TOTAL_AMOUNT',
												'PS_CATEGORY'	=>'P.PS_CATEGORY',
												'ccy'			=>'P.PS_CCY',
												'transmission'	=>'',
												'reference'		=>'',
												'bankresponse'	=>'',
												'bankrespcode'	=>'',
												'traceno'		=>'P.UUID',
												'releaser'		=>'P.PS_RELEASER_USER_LOGIN',
												'challengecode'	=>'P.PS_RELEASER_CHALLENGE',
												'PS_TYPE'		=>'P.PS_TYPE'

											))
							->joinLeft(	array(	'C' => 'M_CUSTOMER' ),'P.CUST_ID = C.CUST_ID',array())
							->where('P.PS_NUMBER =? ',$PS_NUMBER);

		$pslip = $this->_db->fetchRow($select);

		if (!empty($pslip))
		{
			// separate credit and debet view
			$companyName = $pslip['compName'];
			$companyCode = $pslip['compCode'];
			if ($pslip["PS_TYPE"] == $this->_paymenttype["code"]["multidebet"] || $pslip["PS_TYPE"] == $this->_paymenttype["code"]["bulkdebet"])
				$this->downloadTrxDebet2($PS_NUMBER,$companyName,$companyCode,$suggestid);
			else
				$this->downloadTrxCredit2($PS_NUMBER,$companyName,$companyCode,$suggestid);
		}
		else
		{
			$data = array();
			$data[0][0] = "Invalid Payment Number";
			$this->_helper->download->csv(array(),$data,null,$PS_NUMBER);
			Application_Helper_General::writeLog('RPPY','Download CSV Payment Detail Report');
		}
	}

	protected function downloadTrxCredit2($PS_NUMBER,$companyName,$companyCode,$suggestid)
	{
		$arrTransferStatus 	= array_combine($this->_transferstatus["code"],$this->_transferstatus["desc"]);

		$caseTraStatus = "(CASE TT.TRA_STATUS ";
  		foreach($arrTransferStatus as $key => $val)	{ $caseTraStatus .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseTraStatus .= " END)";

		$company = $companyName.' ('.$companyCode.')';
		$headerData[] = array($company);
		$select	= $this->_db->select()
							->from(array('A' => 'T_PSLIP_EXCEPTION_REPAIR_DETAIL'),array())
							->joinLeft(	array(	'TT' => 'T_TRANSACTION'), 'A.TRANSACTION_ID = TT.TRANSACTION_ID',
									array(
											'TRANSACTION_ID'		=> 'A.TRANSACTION_ID',
											'BANK_CODE'				=> 'D.BANK_NAME',
											'ACCTSRC'				=> 'TT.SOURCE_ACCOUNT',
											'BENEF_BANK_CODE'		=> 'E.BANK_NAME',
											'BENEFICIARY_ACCOUNT'	=> 'TT.BENEFICIARY_ACCOUNT',
											'CCY'					=> 'C.PS_CCY',
											'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
											'BANK_RESPONSE'			=> 'TT.BANK_RESPONSE',
											'TRA_STATUS'			=> $caseTraStatus,
											'UPDATE_TO'				=> 'A.TRA_STATUS',
										  )
									)
							->joinLeft(	array(	'C' => 'T_PSLIP' ),'TT.PS_NUMBER = C.PS_NUMBER',array())
							->joinLeft(	array(	'D' => 'M_BANK_TABLE' ),'TT.SOURCE_ACCT_BANK_CODE = D.BANK_CODE',array())
							->joinLeft(	array(	'E' => 'M_BANK_TABLE' ),'TT.BENEF_ACCT_BANK_CODE = E.BANK_CODE',array())
							->joinLeft(	array(	'F' => 'T_PSLIP_EXCEPTION_REPAIR' ),'A.SUGGEST_ID = F.SUGGEST_ID',array())
							->where('A.SUGGEST_ID = ?', $suggestid)
							->where('F.PS_NUMBER = ?', $PS_NUMBER);

		$data = $this->_db->fetchAll($select);

		if (empty($data)) {
			
			$newData[] = array("No","Transaction ID","Source Bank","Source Account","Beneficiary Bank","Beneficiary Account","CCY","Amount","RC","Status","Update to"," ","Hanya isi kolom Update to menjadi 0 = Failed, 1 = Success (jangan mengubah kolom lainnya)");

			$this->_helper->download->csv($headerData,$newData,null,$PS_NUMBER);
			Application_Helper_General::writeLog('RPPY','Download CSV Payment Detail Report');
	
		}else{

			$newData[] = array("No","Transaction ID","Source Bank","Source Account","Beneficiary Bank","Beneficiary Account","CCY","Amount","RC","Status","Update to"," ","Hanya isi kolom Update to menjadi 0 = Failed, 1 = Success (jangan mengubah kolom lainnya)");
			$no = 1;
			foreach ($data as $p => $pTrx)
			{

				if ($pTrx['BANK_RESPONSE'] != NULL) {
					$bankresponse = explode(':', $pTrx['BANK_RESPONSE']);
					//$status = $bankresponse[1];
					$RC 	= $bankresponse[0];
				}

				if ($pTrx['UPDATE_TO'] == 3) {
					$psstatus = 1;
				}elseif ($pTrx['UPDATE_TO'] == 4) {
					$psstatus = 0;
				}

				
				$paramTrx = array(	"NO"					=> $no++,
									"TRANSACTION_ID"  		=> $pTrx['TRANSACTION_ID'],
									"BANK_CODE"  			=> $pTrx['BANK_CODE'],
									"ACCTSRC"  				=> $pTrx['ACCTSRC'],
									"BENEF_BANK_CODE"  		=> $pTrx['BENEF_BANK_CODE'],
									"BENEFICIARY_ACCOUNT" 	=> $pTrx['BENEFICIARY_ACCOUNT'],
									"CCY"  					=> $pTrx['CCY'],
									"TRA_AMOUNT"  			=> $pTrx['TRA_AMOUNT'],
									"RC"					=> $RC,
									"STATUS"				=> $pTrx['TRA_STATUS'],
									"UPDATE_TO"				=> $psstatus,
								);

				$newData[] = $paramTrx;
			}

			$this->_helper->download->csv($headerData,$newData,null,$PS_NUMBER);
			Application_Helper_General::writeLog('RPPY','Download CSV Payment Detail Report');
		}
	}

	protected function downloadTrxDebet2($PS_NUMBER,$companyName,$companyCode,$suggestid)
	{
		$arrTransferStatus 	= array_combine($this->_transferstatus["code"],$this->_transferstatus["desc"]);

		$caseTraStatus = "(CASE TT.TRA_STATUS ";
  		foreach($arrTransferStatus as $key => $val)	{ $caseTraStatus .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseTraStatus .= " END)";

		$company = $companyName.' ('.$companyCode.')';
		$headerData[] = array($company);
		$select	= $this->_db->select()
							->from(array('A' => 'T_PSLIP_EXCEPTION_REPAIR_DETAIL'),array())
							->joinLeft(	array(	'TT' => 'T_TRANSACTION'), 'A.TRANSACTION_ID = TT.TRANSACTION_ID',
									array(
											'TRANSACTION_ID'		=> 'A.TRANSACTION_ID',
											'BANK_CODE'				=> 'D.BANK_NAME',
											'ACCTSRC'				=> 'TT.SOURCE_ACCOUNT',
											'BENEF_BANK_CODE'		=> 'E.BANK_NAME',
											'BENEFICIARY_ACCOUNT'	=> 'TT.BENEFICIARY_ACCOUNT',
											'CCY'					=> 'C.PS_CCY',
											'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
											'BANK_RESPONSE'			=> 'TT.BANK_RESPONSE',
											'TRA_STATUS'			=> $caseTraStatus,
											'UPDATE_TO'				=> 'A.TRA_STATUS',
										  )
									)
							->joinLeft(	array(	'C' => 'T_PSLIP' ),'TT.PS_NUMBER = C.PS_NUMBER',array())
							->joinLeft(	array(	'D' => 'M_BANK_TABLE' ),'TT.SOURCE_ACCT_BANK_CODE = D.BANK_CODE',array())
							->joinLeft(	array(	'E' => 'M_BANK_TABLE' ),'TT.BENEF_ACCT_BANK_CODE = E.BANK_CODE',array())
							->joinLeft(	array(	'F' => 'T_PSLIP_EXCEPTION_REPAIR' ),'A.SUGGEST_ID = F.SUGGEST_ID',array())
							->where('A.SUGGEST_ID = ?', $suggestid)
							->where('F.PS_NUMBER = ?', $PS_NUMBER);

		$data = $this->_db->fetchAll($select);

		if (empty($data)) {
			
			$newData[] = array("No","Transaction ID","Source Bank","Source Account","Beneficiary Bank","Beneficiary Account","CCY","Amount","RC","Status","Update to"," ","Hanya isi kolom Update to menjadi 0 = Failed, 1 = Success (jangan mengubah kolom lainnya)");

			$this->_helper->download->csv($headerData,$newData,null,$PS_NUMBER);
			Application_Helper_General::writeLog('RPPY','Download CSV Payment Detail Report');
	
		}else{

			$newData[] = array("No","Transaction ID","Source Bank","Source Account","Beneficiary Bank","Beneficiary Account","CCY","Amount","RC","Status","Update to"," ","Hanya isi kolom Update to menjadi 0 = Failed, 1 = Success (jangan mengubah kolom lainnya)");
			$no = 1;
			foreach ($data as $p => $pTrx)
			{

				if ($pTrx['BANK_RESPONSE'] != NULL) {
					$bankresponse = explode(':', $pTrx['BANK_RESPONSE']);
					//$status = $bankresponse[1];
					$RC 	= $bankresponse[0];
				}

				if ($pTrx['UPDATE_TO'] == 3) {
					$psstatus = 1;
				}elseif ($pTrx['UPDATE_TO'] == 4) {
					$psstatus = 0;
				}

				
				$paramTrx = array(	"NO"					=> $no++,
									"TRANSACTION_ID"  		=> $pTrx['TRANSACTION_ID'],
									"BANK_CODE"  			=> $pTrx['BANK_CODE'],
									"ACCTSRC"  				=> $pTrx['ACCTSRC'],
									"BENEF_BANK_CODE"  		=> $pTrx['BENEF_BANK_CODE'],
									"BENEFICIARY_ACCOUNT" 	=> $pTrx['BENEFICIARY_ACCOUNT'],
									"CCY"  					=> $pTrx['CCY'],
									"TRA_AMOUNT"  			=> $pTrx['TRA_AMOUNT'],
									"RC"					=> $RC,
									"STATUS"				=> $pTrx['TRA_STATUS'],
									"UPDATE_TO"				=> $psstatus,
								);

				$newData[] = $paramTrx;
			}

			$this->_helper->download->csv($headerData,$newData,null,$PS_NUMBER);
			Application_Helper_General::writeLog('RPPY','Download CSV Payment Detail Report');
		}
	}

	private function LLDContent($pslip)
	{
		$PS_NUMBER 		= $this->_paymentRef;
		$arrEFTStatus 	= array_combine($this->_eftstatus["code"],$this->_eftstatus["desc"]);

		$caseEFTStatus = "(CASE TT.EFT_STATUS ";
  		foreach($arrEFTStatus as $key => $val)	{ $caseEFTStatus .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseEFTStatus .= " END)";

		// Table Detail Header
		$fields = array("TRANSANCTION_ID"	=> 'Transaction Ref#',
						// "EFT_STATUS"		=> 'EFT Status',
						"LLD_DESC" 			=> 'LLD Content',
            			);

		$select	= $this->_db->select()
							->from(	array(	'TT' => 'T_TRANSACTION'),
									array(
											'TRANSANCTION_ID'		=> 'TT.TRANSACTION_ID',
											// 'EFT_STATUS'			=> $caseEFTStatus,
											'LLD_DESC'				=> 'TT.LLD_DESC',
											'*'

										  )
									)
							->joinLeft	(   array('B' => 'M_BENEFICIARY'), 'TT.BENEFICIARY_ID = B.BENEFICIARY_ID', array() )
							->where('TT.PS_NUMBER = ?', $PS_NUMBER);

		$pslipTrx = $this->_db->fetchAll($select);

		$tableDtl = array();
		foreach ($pslipTrx as $p => $pTrx)
		{
			// create table detail data
			foreach ($fields as $key => $field)
			{
				$value = $pTrx[$key];

				if($key == 'LLD_DESC' && $pslip['PS_TYPE'] == '3'){
					if($pslip['benefresident'] == 'R')
				   	{
				   		$resident = 'Resident';
				   	}
					elseif($pslip['benefresident'] == 'NR')
				   	{
				   		$resident = 'Non Resident';
				   	}
				   	else
				   	{
				   		$resident = '';
				   	}

				   	if($pslip['sourceresident'] == 'R')
				   	{
				   		$sourceresident = 'Resident';
				   	}
					elseif($pslip['sourceresident'] == 'NR')
				   	{
				   		$sourceresident = 'Non Resident';
				   	}
				   	else
				   	{
				   		$sourceresident = '';
				   	}

				   	if($pslip['benefcitizen'] == 'W')
				   	{
				   		$benenationality = 'WNI';
				   	}
					elseif($pslip['benefcitizen'] == 'N')
				   	{
				   		$benenationality = 'WNA';
				   	}
				   	else
				   	{
				   		$benenationality = '';
				   	}

				   	if($pslip['sourcecitizen'] == 'W')
				   	{
				   		$sourcenationality = 'WNI';
				   	}
					elseif($pslip['sourcecitizen'] == 'N')
				   	{
				   		$sourcenationality = 'WNA';
				   	}
				   	else
				   	{
				   		$sourcenationality = '';
				   	}

				   	// Create LLD string
					$settings 			= new Application_Settings();
					$LLD_array 			= array();
					$LLD_DESC_arrayCat 	= array();
					$lldIdenticalArr  	= $settings->getLLDDOMIdentical();
		 	  		$lldRelationshipArr = $settings->getLLDDOMRelationship();

		 	  		$model = new purchasing_Model_Purchasing();
					$purposeArr = $model->getTranspurpose();
					$purposeList = array();
					foreach ($purposeArr as $key => $value ){
						$purposeList[$value['CODE']] = $value['DESCRIPTION'];
					}

					if (!empty($pslip['benefcategory']))
					{
						$lldCategoryArr  	= $settings->getLLDDOMCategory();
						$LLD_CATEGORY_POST = $lldCategoryArr[$pslip['benefcategory']];
						$LLD_CATEGORY_SOURCE = $lldCategoryArr[$pslip['sourcecategory']];
					}
					//print_r($pslipTrx);die;
					$SourceAcount = $this->_db->select()
			->from(
				array('M_CUSTOMER_ACCT'),
				array('*')
			)
			->where('ACCT_NO = ?',$pslipTrx['0']['SOURCE_ACCOUNT'])

		;

				$SourceAcountData = $this->_db->fetchRow($SourceAcount );
//				print_r($SourceAcountData);die;
// 			   	print_r(ucfirst($country));
// 			   	die;
				$citizenship = ($SourceAcountData['ACCT_RESIDENT'] == "R")? 'Resident' : 'Non Resident';
				$benecitizenship = ($pslipTrx['0']['BENEFICIARY_RESIDENT'] == "R")? 'Resident' : 'Non Resident';

				$country     = ($SourceAcountData['ACCT_CITIZENSHIP'] == "W")? 'WNI' : 'WNA';
				$benecountry = ($pslipTrx['0']['BENEFICIARY_CITIZENSHIP'] == "W")? 'WNI' : 'WNA';

				$settings 			= new Application_Settings();

				$lldCategoryArr  	= $settings->getLLDDOMCategory();
				$lldIdenticalArr  	= $settings->getLLDDOMIdentical();
				$lldRelationshipArr = $settings->getLLDDOMRelationship();
				$purpose = $this->_db->select()
			->from(
				array('M_TRANSACTION_PURPOSE'),
				array('*')
			)
			->where('CODE = ?',$pslipTrx['0']['LLD_TRANSACTION_PURPOSE'])

		;

				$purposeData = $this->_db->fetchRow($purpose);
				$category = $lldCategoryArr[$SourceAcountData['ACCT_CATEGORY']];

					$value = "Sender Citizenship : ".$citizenship."; Sender Nationality : ".$country."; Sender Category : ".$category."; Destination Citizenship : ".$benecitizenship."; Destination Nationality : ".$benecountry."; Destination Category : ".$lldCategoryArr[$pslipTrx['0']['BENEFICIARY_CATEGORY']]."; Identity : ".$lldIdenticalArr[$pslipTrx['0']['LLD_IDENTITY']]."; Transactor Relationship : ".$lldRelationshipArr[$pslipTrx['0']['LLD_TRANSACTOR_RELATIONSHIP']]."; Transaction Purpose : ".$purposeData['DESCRIPTION'];
				}

				$value = ($value == "")? "&nbsp;": $value;

				$tableDtl[$p][$key] = $value;	// TODO: Transfer Charge
			}
		}

		$this->view->fieldsLLDContent 	= $fields;
		$this->view->tableDtlLLDContent = $tableDtl;

	}

	private function within($pslip)
	{
		$PS_NUMBER 			= $this->_paymentRef;
		$arrTransferStatus 	= array_combine($this->_transferstatus["code"],$this->_transferstatus["desc"]);
		$arrCitizenship 	= array_combine($this->_citizenship["code"],$this->_citizenship["desc"]);

		$caseCityzenship = "(CASE TT.BENEFICIARY_CITIZENSHIP";
  		foreach($arrCitizenship as $key => $val)	{ $caseCityzenship .= " WHEN '".$key."' THEN '".$val."'"; }
  		$caseCityzenship .= " END)";


		$caseTraStatus = "(CASE TT.TRA_STATUS ";
  		foreach($arrTransferStatus as $key => $val)	{ $caseTraStatus .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseTraStatus .= " END)";

  		$caseTraStatus1 = "(CASE E.TRA_STATUS ";
  		foreach($arrTransferStatus as $key => $val)	{ $caseTraStatus1 .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseTraStatus1 .= " END)";

		//Application_Helper_General::viewAccount($pslip["accsrc"], $pslip["accsrc_ccy"], $pslip["accsrc_name"], $pslip["accsrc_alias"])

		$select = $this->_db->select()
								->from(	array(	'T'				=>'T_TRANSACTION'),
										array(	'accsrc'		=>'T.SOURCE_ACCOUNT',
												'accsrc_ccy'	=>'T.SOURCE_ACCOUNT_CCY',
												'accsrc_name'	=>'T.SOURCE_ACCOUNT_NAME',
												'accsrc_alias'	=>'T.SOURCE_ACCOUNT_ALIAS_NAME',
											))
								->where('T.PS_NUMBER =? ',$PS_NUMBER);
		$pslipaccount = $this->_db->fetchRow($select);
		$account = $pslipaccount['accsrc'].' ['.$pslipaccount['accsrc_ccy'].'] - '.$pslipaccount['accsrc_name'];

		$this->_tableMst[5]["label"] = "Source Account";
		$this->_tableMst[5]["value"] = $account;

		/*

		$fields = array("TRANSACTION_ID"	=> 'Transaction ID',
						"ACCTSRC"			=> 'Source Account',
						"ACCTSRC_NAME"		=> 'Source Account Name',
						"BANK_NAME"			=> 'Bank',
						"CITY"				=> 'City',
						"BENE_CITYZENSHIP"	=> 'Citizenship',
						"ACCTSRC_CCY"		=> 'Ccy',
						"TRA_AMOUNT"		=> 'Amount',
						"TRANSFER_FEE"		=> 'Charge',
						"TRA_MESSAGE" 		=> 'Message',
						"TRA_ADDMESSAGE"  	   	=> 'Additional Message',
						"TRANS_TYPE_FULL"	=> 'Transfer Type',
						"TRA_STATUS" 		=> 'Status',
            			);

		*/

		// Table Detail Header
		$fields = array("TRANSACTION_ID"		=> 'Transaction ID',
						"SOURCE_NAME" 	=> 'Source Account',
						"BENEF_NAME"			=> 'Beneficiary Account',
						// "ACBENEF_NAME_ALIAS"=> 'Beneficiary Name ',
						// "BANK_NAME"			=> 'Bank',
						// "CITY"				=> 'City',
						// "BENE_CITYZENSHIP"	=> 'Citizenship',
						"PS_CCY"		=> 'CCY',
						"TRA_AMOUNT"  	   		=> 'Amount',
						// "TRANSFER_FEE"		=> 'Charge',
						// "TRA_MESSAGE"  	   	=> 'Message',
						// "TRA_ADDMESSAGE"			=> 'Additional Message',
						// "TRANSFER_TYPE"		=> 'Transfer Type',
						"RC"					=> 'RC',
						"TRA_STATUS"			=> 'Status',
						"TRA_STATUS1" 			=> 'Update to',
// 						"BANK_RESPONSE" 		=> 'Response',
						
            			);

		$select	= $this->_db->select()
							->from(	array(	'TT' => 'T_TRANSACTION'),
									array(
											'TRANSACTION_ID'		=> 'TT.TRANSACTION_ID',
											//'numbertrx'				=> 'COUNT(TT.TRANSACTION_ID)',
											'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
											'ACBENEF_NAME_ALIAS'	=> 'TT.BENEFICIARY_ACCOUNT_NAME',
											'ACBENEF_NAME'			=> new Zend_Db_Expr("
																							CONCAT( TT.BENEFICIARY_ACCOUNT , ' (' , TT.BENEFICIARY_ACCOUNT_CCY , ') - ' , G.BANK_NAME ) "),
											'SOURCE_ACCOUNT_NAME'	=> new Zend_Db_Expr("
																							CONCAT( TT.SOURCE_ACCOUNT , ' (' , TT.SOURCE_ACCOUNT_CCY , ') - ' , F.BANK_NAME ) "),
											'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
											'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
											'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',
											'TRA_REFNO'				=> 'TT.TRA_REFNO',

											'TRANSFER_TYPE'			=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN 'PB'
																				 WHEN TT.TRANSFER_TYPE = '1' THEN 'RTGS'
																				 WHEN TT.TRANSFER_TYPE = '2' THEN 'SKN'
																				 WHEN TT.TRANSFER_TYPE = '3' THEN 'OUR'
																				 WHEN TT.TRANSFER_TYPE = '4' THEN 'SHA'
																				 WHEN TT.TRANSFER_TYPE = '6' THEN 'VA'
																				 WHEN TT.TRANSFER_TYPE = '7' THEN 'Mayapada (Sell)'
																				 WHEN TT.TRANSFER_TYPE = '8' THEN 'Mayapada (Buy)'
																				 ELSE 'N/A'
																			END"),
											'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
											'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
											'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
											'BANK_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN '".$this->_bankName."'
																				 ELSE TT.BENEFICIARY_BANK_NAME
																			END"),
											'TRA_STATUS' 			=> $caseTraStatus,
											'TRA_STATUS1' 			=> $caseTraStatus1,
											'CITY'					=> 'TT.BENEFICIARY_BANK_CITY',
											'BENE_CITYZENSHIP'		=> 'TT.BENEFICIARY_CITIZENSHIP',
											'C.PS_CCY',
											'RC'					=> 'TT.BANK_RESPONSE',
											'BANK_RESPONSE'			=> 'TT.BANK_RESPONSE',
											/* 'BANK_RESPONSE'		=> new Zend_Db_Expr("
																							CASE WHEN TT.BANK_RESPONSE = '' THEN 'N/A'
																								 WHEN TT.BANK_RESPONSE = null THEN 'N/A'
																								 ELSE TT.BANK_RESPONSE
																							END"), */
											'SOURCE_ACCOUNT'		=> 'TT.SOURCE_ACCOUNT',
											'SOURCE_ACCOUNT_CCY'	=> 'TT.SOURCE_ACCOUNT_CCY',
											'SOURCE_ACCOUNT_BANK'	=> 'F.BANK_NAME',
											'BENEFICIARY_ACCOUNT'		=> 'TT.BENEFICIARY_ACCOUNT',
											'BENEFICIARY_ACCOUNT_CCY'	=> 'TT.BENEFICIARY_ACCOUNT_CCY',
											'BENEFICIARY_ACCOUNT_BANK'	=> 'G.BANK_NAME',
											'PS_CCY'					=> 'C.PS_CCY',

										  )
									)
							->joinLeft	(   array('B' => 'M_BENEFICIARY'), 'TT.BENEFICIARY_ID = B.BENEFICIARY_ID', array() )
							->joinLeft	(   array('C' => 'T_PSLIP'), 'C.PS_NUMBER = TT.PS_NUMBER', array() )
							->joinLeft ( array('E' => 'T_PSLIP_EXCEPTION_REPAIR_DETAIL'), 'TT.TRANSACTION_ID = E.TRANSACTION_ID', array() )
							->joinLeft(	array(	'F' => 'M_BANK_TABLE' ),'TT.SOURCE_ACCT_BANK_CODE = F.BANK_CODE',array())
							->joinLeft(	array(	'G' => 'M_BANK_TABLE' ),'TT.BENEF_ACCT_BANK_CODE = G.BANK_CODE',array())
							->where('TT.PS_NUMBER = ?', $PS_NUMBER);

		// echo "<pre>";
		// echo $select->__toString();
		// echo "<br />";
		//die;
		$pslipTrx = $this->_db->fetchAll($select);

		$tableDtl = array();
		foreach ($pslipTrx as $p => $pTrx)
		{
			// create table detail data
			foreach ($fields as $key => $field)
			{
				$value = $pTrx[$key];
				$ccy = $pTrx["ACBENEF_CCY"];

				if ($key == "TRA_AMOUNT" || $key == "TRANSFER_FEE")
				{	$value = Application_Helper_General::displayMoney($value);	}
				elseif($key == "TRANSFER_TYPE")
				{
					if($value=='PB'){
						$value = 'Mayapada';
					}
				}

				if($key == 'BENE_CITYZENSHIP'){
					if($value=='W'){
						$value='Resident';
					}else{
						$value='Non Resident';

					}
				}

				$value = ($value == "")? "&nbsp;": $value;

				$tableDtl[$p][$key] = $value;	// TODO: Transfer Charge
			}


			if ($pTrx['BANK_RESPONSE'] != NULL || !empty($pTrx['BANK_RESPONSE'])) {
				$bankresponse = explode(':', $pTrx['BANK_RESPONSE']);
				//$tableDtl[$p]['BANK_RESPONSE'] = $bankresponse[1];

				$tableDtl[$p]['RC'] = $bankresponse[0];
			}

			if ($pTrx['SOURCE_ACCOUNT_BANK'] != NULL || !empty($pTrx['SOURCE_ACCOUNT_BANK'])) {
				$sourceaccountbank = $pTrx['SOURCE_ACCOUNT'].' ('.$pTrx['SOURCE_ACCOUNT_CCY'].') - '.$pTrx['SOURCE_ACCOUNT_BANK'];
				$tableDtl[$p]['SOURCE_NAME'] = $sourceaccountbank;
			}else{
				$setting = new Settings();
				$master_bank_name = $setting->getSetting('master_bank_name');
				// $this->view->master_bank_app_name = $setting->getSetting('master_bank_app_name');

				$sourceaccountbank = $pTrx['SOURCE_ACCOUNT'].' ('.$pTrx['SOURCE_ACCOUNT_CCY'].') - '.$master_bank_name;
				$tableDtl[$p]['SOURCE_NAME'] = $sourceaccountbank;
			}

			if ($pTrx['BENEFICIARY_ACCOUNT_BANK'] != NULL || !empty($pTrx['BENEFICIARY_ACCOUNT_BANK'])) {
				$benefaccountbank = $pTrx['BENEFICIARY_ACCOUNT'].' ('.$pTrx['BENEFICIARY_ACCOUNT_CCY'].') - '.$pTrx['BENEFICIARY_ACCOUNT_BANK'];
				$tableDtl[$p]['BENEF_NAME'] = $benefaccountbank;
			}else{
				$setting = new Settings();
				$master_bank_name = $setting->getSetting('master_bank_name');
				// $this->view->master_bank_app_name = $setting->getSetting('master_bank_app_name');

				$benefaccountbank = $pTrx['BENEFICIARY_ACCOUNT'].' ('.$pTrx['BENEFICIARY_ACCOUNT_CCY'].') - '.$master_bank_name;
				$tableDtl[$p]['BENEF_NAME'] = $benefaccountbank;
			}

			//$this->view->numbertrx 			= $pTrx['numbertrx'];
		}

		$this->view->fields 			= $fields;
		$this->view->tableDtl 			= $tableDtl;
	}

	private function domestic($pslip)
	{
		$PS_NUMBER 			= $this->_paymentRef;
		$arrTransferStatus 	= array_combine($this->_transferstatus["code"],$this->_transferstatus["desc"]);
		$arrCitizenship 	= array_combine($this->_citizenship["code"],$this->_citizenship["desc"]);

		$caseTraStatus = "(CASE TT.TRA_STATUS ";
  		foreach($arrTransferStatus as $key => $val)	{ $caseTraStatus .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseTraStatus .= " END)";

  		$caseTraStatus1 = "(CASE E.TRA_STATUS ";
  		foreach($arrTransferStatus as $key => $val)	{ $caseTraStatus1 .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseTraStatus1 .= " END)";

		$caseCityzenship = "(CASE TT.BENEFICIARY_CITIZENSHIP";
  		foreach($arrCitizenship as $key => $val)	{ $caseCityzenship .= " WHEN '".$key."' THEN '".$val."'"; }
  		$caseCityzenship .= " END)";

		//Application_Helper_General::viewAccount($pslip["accsrc"], $pslip["accsrc_ccy"], $pslip["accsrc_name"], $pslip["accsrc_alias"])

		$select = $this->_db->select()
								->from(	array(	'T'				=>'T_TRANSACTION'),
										array(	'accsrc'		=>'T.SOURCE_ACCOUNT',
												'accsrc_ccy'	=>'T.SOURCE_ACCOUNT_CCY',
												'accsrc_name'	=>'T.SOURCE_ACCOUNT_NAME',
												'accsrc_alias'	=>'T.SOURCE_ACCOUNT_ALIAS_NAME',
											))
								->where('T.PS_NUMBER =? ',$PS_NUMBER);
		$pslipaccount = $this->_db->fetchRow($select);
		$account = $pslipaccount['accsrc'].' ['.$pslipaccount['accsrc_ccy'].'] - '.$pslipaccount['accsrc_name'].' / '.$pslipaccount['accsrc_alias'];

		$this->_tableMst[5]["label"] = "Source Account";
		$this->_tableMst[5]["value"] = $account;

		/*

		$fields = array("TRANSACTION_ID"	=> 'Transaction ID',
						"ACCTSRC"			=> 'Source Account',
						"ACCTSRC_NAME"		=> 'Source Account Name',
						"BANK_NAME"			=> 'Bank',
						"CITY"				=> 'City',
						"BENE_CITYZENSHIP"	=> 'Citizenship',
						"ACCTSRC_CCY"		=> 'Ccy',
						"TRA_AMOUNT"		=> 'Amount',
						"TRANSFER_FEE"		=> 'Charge',
						"TRA_MESSAGE" 		=> 'Message',
						"TRA_REFNO"  	   	=> 'Additional Message',
						"TRANS_TYPE_FULL"	=> 'Transfer Type',
						"TRA_STATUS" 		=> 'Status',
            			);

		*/

		// Table Detail Header
		$fields = array("TRANSACTION_ID"	=> 'Transaction ID',
						"SOURCE_NAME" => 'Source Account',
						"BENEF_NAME"			=> 'Beneficiary Account',
// 						"ACBENEF_NAME_ALIAS"=> 'Beneficiary Name',
						// "BENEFICIARY_CATEGORY"			=> 'Beneficiary Category',
						// "BENEFICIARY_ID_TYPE"			=> 'Beneficiary Identification Type',
						// "BENEFICIARY_CITY_CODE"			=> 'Beneficiary Identification Number',
						// "BANK_NAME"			=> 'Beneficiary City',

						// "BANK_NAME"			=> 'Bank',
						// "CITY"				=> 'City',
						// "BENE_CITYZENSHIP"	=> 'Citizenship',
						"PS_CCY"		=> 'CCY',
						"TRA_AMOUNT"  	   	=> 'Amount',
						// "TRANSFER_FEE"		=> 'Charge',
						// "TRA_MESSAGE"		=> 'Message',
						// "TRA_REFNO"			=> 'Additional Message',
						// "TRANS_TYPE_FULL"	=> 'Transfer Type',
						"RC"					=> 'RC',
						"TRA_STATUS"			=> 'Status',
						"TRA_STATUS1" 		=> 'Update to',
// 						"BANK_RESPONSE" 		=> 'Response',
						// "SOURCE_ACCOUNT" 	=> 'SOURCE_ACCOUNT',
            			);

		$select	= $this->_db->select()
							->from(	array(	'TT' => 'T_TRANSACTION'),
									array(
											'TRANSACTION_ID'		=> 'TT.TRANSACTION_ID',
											//'numbertrx'				=> 'COUNT(TT.TRANSACTION_ID)',
											'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
											'ACBENEF_NAME_ALIAS'	=> new Zend_Db_Expr("
																			CONCAT ( TT.BENEFICIARY_ACCOUNT_NAME , ' ' ) "),
											'ACBENEF_NAME'			=> new Zend_Db_Expr("
																			CONCAT ( TT.BENEFICIARY_ACCOUNT , ' (' , TT.BENEFICIARY_ACCOUNT_CCY , ')  ' , TT.BENEFICIARY_ACCOUNT_NAME , ' / ' , TT.BENEFICIARY_BANK_NAME ) "),
											'SOURCE_ACCOUNT_NAME'	=> new Zend_Db_Expr("
																			CONCAT( TT.SOURCE_ACCOUNT , ' (' , TT.SOURCE_ACCOUNT_CCY , ') - ' , F.BANK_NAME ) "),
											'BENEFICIARY_NAME'	=> new Zend_Db_Expr("
																			CONCAT( TT.BENEFICIARY_ACCOUNT , ' (' , TT.BENEFICIARY_ACCOUNT_CCY , ') - ' , G.BANK_NAME ) "),
											'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
											'CITY'					=> 'TT.BENEFICIARY_BANK_CITY',
											'BENEFICIARY_CATEGORY'				=> 'TT.BENEFICIARY_CATEGORY',
											'BENEFICIARY_ID_TYPE'				=> 'TT.BENEFICIARY_ID_TYPE',
											'BENEFICIARY_ID_NUMBER'				=> 'TT.BENEFICIARY_ID_NUMBER',
											'BENEFICIARY_CITY_CODE'				=> 'TT.BENEFICIARY_CITY_CODE',
											'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
											'TRA_REFNO'				=> 'TT.TRA_REFNO',
											'TRANSFER_TYPE'			=> 'TT.TRANSFER_TYPE',
											'TRANS_TYPE_FULL'		=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN 'PB'
																				 WHEN TT.TRANSFER_TYPE = '1' THEN 'RTGS'
																				 WHEN TT.TRANSFER_TYPE = '2' THEN 'SKN'
																				 WHEN TT.TRANSFER_TYPE = '3' THEN 'OUR'
																				 WHEN TT.TRANSFER_TYPE = '4' THEN 'SHA'
																				 ELSE 'N/A'
																			END"),
											'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
											'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
											'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
											'BANK_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN '".$this->_bankName."'
																				 ELSE TT.BENEFICIARY_BANK_NAME
																			END"),
											'BENE_CITYZENSHIP'		=> $caseCityzenship,
											'TRA_STATUS' 			=> $caseTraStatus,
											'TRA_STATUS1' 			=> $caseTraStatus1,
											'RC'					=> 'TT.BANK_RESPONSE',
											'BANK_RESPONSE'			=> 'TT.BANK_RESPONSE',
											/* 'BANK_RESPONSE'		=> new Zend_Db_Expr("
																							CASE WHEN TT.BANK_RESPONSE = '' THEN 'N/A'
																								 WHEN TT.BANK_RESPONSE = null THEN 'N/A'
																								 ELSE TT.BANK_RESPONSE
																							END"), */
											'SOURCE_ACCOUNT'		=> 'TT.SOURCE_ACCOUNT',
											'SOURCE_ACCOUNT_CCY'	=> 'TT.SOURCE_ACCOUNT_CCY',
											'SOURCE_ACCOUNT_BANK'	=> 'F.BANK_NAME',
											'BENEFICIARY_ACCOUNT'		=> 'TT.BENEFICIARY_ACCOUNT',
											'BENEFICIARY_ACCOUNT_CCY'	=> 'TT.BENEFICIARY_ACCOUNT_CCY',
											'BENEFICIARY_ACCOUNT_BANK'	=> 'G.BANK_NAME',
											'PS_CCY'					=> 'H.PS_CCY',
										  )
									)
							->joinLeft	(   array('B' => 'M_BENEFICIARY'), 'TT.BENEFICIARY_ID = B.BENEFICIARY_ID', array() )
							->joinLeft ( array('E' => 'T_PSLIP_EXCEPTION_REPAIR_DETAIL'), 'TT.TRANSACTION_ID = E.TRANSACTION_ID', array() )
							->joinLeft(	array(	'F' => 'M_BANK_TABLE' ),'TT.SOURCE_ACCT_BANK_CODE = F.BANK_CODE',array())
							->joinLeft(	array(	'G' => 'M_BANK_TABLE' ),'TT.BENEF_ACCT_BANK_CODE = G.BANK_CODE',array())
							->joinLeft ( array('H' => 'T_PSLIP'), 'TT.PS_NUMBER = H.PS_NUMBER', array() )
							->where('TT.PS_NUMBER = ?', $PS_NUMBER);

		// echo "<pre>";
		// echo $select->__toString();
		// die;

		$pslipTrx = $this->_db->fetchAll($select);

		$tableDtl = array();
		foreach ($pslipTrx as $p => $pTrx)
		{
			// create table detail data
			foreach ($fields as $key => $field)
			{
				$value 		= $pTrx[$key];
				$CITY 		= $pTrx["CITY"];
				$BANKNAME 	= $pTrx["BANK_NAME"];
				$ccy 		= $pTrx["ACBENEF_CCY"];

				if($key == "BENEFICIARY_CATEGORY"){
					$LLD_CATEGORY = $value;
					// 9. Create LLD string
					$settings 			= new Application_Settings();
					$LLD_array 			= array();
					$LLD_DESC_arrayCat 	= array();
					$lldTypeArr  		= $settings->getLLDDOMType();

					if (!empty($LLD_CATEGORY))
					{
						$lldCategoryArr  	= $settings->getLLDDOMCategory();
						$LLD_array["CT"] 	= $LLD_CATEGORY;
						$LLD_CATEGORY_POST = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
					}
					$value = $LLD_CATEGORY_POST;
				}
				if($key == "BENEFICIARY_CITY_CODE"){
					$CITY_CODE = $value;
					$CITY_CODEGet = (!empty($CITY_CODE)?$CITY_CODE:'');
					$select = $this->_db->select()
					->from(array('A' => 'M_CITY'),array('*'));
					$select->where('A.CITY_CODE = ?',$CITY_CODEGet);
					$arr = $this->_db->fetchall($select);
					$value = $arr[0]['CITY_NAME'];
				}

				if ($key == "TRA_AMOUNT" || $key == "TRANSFER_FEE")
				{	$value = Application_Helper_General::displayMoney($value);	}

				$value = ($value == "")? "&nbsp;": $value;

				$tableDtl[$p][$key] = $value;	// TODO: Transfer Charge
			}

			if ($pTrx['BANK_RESPONSE'] != NULL || !empty($pTrx['BANK_RESPONSE'])) {
				$bankresponse = explode(':', $pTrx['BANK_RESPONSE']);
				//$tableDtl[$p]['BANK_RESPONSE'] = $bankresponse[1];

				$tableDtl[$p]['RC'] = $bankresponse[0];
			}
			if ($pTrx['SOURCE_ACCOUNT_BANK'] != NULL || !empty($pTrx['SOURCE_ACCOUNT_BANK'])) {
				$sourceaccountbank = $pTrx['SOURCE_ACCOUNT'].' ('.$pTrx['SOURCE_ACCOUNT_CCY'].') - '.$pTrx['SOURCE_ACCOUNT_BANK'];
				$tableDtl[$p]['SOURCE_NAME'] = $sourceaccountbank;
			}else{
				$setting = new Settings();
				$master_bank_name = $setting->getSetting('master_bank_name');
				// $this->view->master_bank_app_name = $setting->getSetting('master_bank_app_name');

				$sourceaccountbank = $pTrx['SOURCE_ACCOUNT'].' ('.$pTrx['SOURCE_ACCOUNT_CCY'].') - '.$master_bank_name;
				$tableDtl[$p]['SOURCE_NAME'] = $sourceaccountbank;
			}

			if ($pTrx['BENEFICIARY_ACCOUNT_BANK'] != NULL || !empty($pTrx['BENEFICIARY_ACCOUNT_BANK'])) {
				$benefaccountbank = $pTrx['BENEFICIARY_ACCOUNT'].' ('.$pTrx['BENEFICIARY_ACCOUNT_CCY'].') - '.$pTrx['BENEFICIARY_ACCOUNT_BANK'];
				$tableDtl[$p]['BENEF_NAME'] = $benefaccountbank;
			}else{
				$setting = new Settings();
				$master_bank_name = $setting->getSetting('master_bank_name');
				// $this->view->master_bank_app_name = $setting->getSetting('master_bank_app_name');

				$benefaccountbank = $pTrx['BENEFICIARY_ACCOUNT'].' ('.$pTrx['BENEFICIARY_ACCOUNT_CCY'].') - '.$master_bank_name;
				$tableDtl[$p]['BENEF_NAME'] = $benefaccountbank;
			}
			//$this->view->numbertrx 			= $pTrx['numbertrx'];
		}

		$this->view->fields 			= $fields;
		$this->view->tableDtl 			= $tableDtl;
		$this->view->TITLE_MST		 	= "Transfer From";
		$this->view->TITLE_DTL		 	= "Transfer To";
	}

	private function userInvolved($pslip)
	{
		$PS_NUMBER 				= $this->_paymentRef;
		$tableDtlUserInvolved 	= array();

		$arrHistoryStatus 		= array_combine($this->_historystatus["code"],$this->_historystatus["desc"]);

		$caseHistoryStatus = "(CASE TP.HISTORY_STATUS ";
  		foreach($arrHistoryStatus as $key => $val)	{ $caseHistoryStatus .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseHistoryStatus .= " END)";

		// Table Detail Header User Involved
		$fieldsinvolved = array("USER"		=> 'User ID',
								"ACTION"	=> 'Action',
								"DATETIME"	=> 'Date/Time',
								"REASON"	=> 'Notes',
								);

		$select = $this->_db->select()
								->from(	array(	'TP'		=>'T_PSLIP_HISTORY'),
										array(	'USER'		=>'USER_LOGIN',
												'ACTION'	=>$caseHistoryStatus,
												'DATETIME'	=>'DATE_TIME',
												'REASON'	=>'PS_REASON',
											))
								->where('TP.PS_NUMBER =? ',$PS_NUMBER)
								->order('DATE_TIME ASC');

		$pslipinvolved = $this->_db->fetchAll($select);

		foreach ($pslipinvolved as $p => $pInv)
		{
			// create table detail data
			foreach ($fieldsinvolved as $key => $field)
			{
				$value = $pInv[$key];

				if ($key == "DATETIME")
				{	$value = Application_Helper_General::convertDate($value, $this->view->displayDateTimeFormat,$this->view->defaultDateFormat);	}

				$value = ($value == "")? "&nbsp;": $value;

				$tableDtlUserInvolved[$p][$key] = $value;
			}
		}

		$this->view->fieldsinvolved	 		= $fieldsinvolved;
		$this->view->tableDtlUserInvolved 	= $tableDtlUserInvolved;
	}


	private function approvalMatrix($pslip)
	{
		$PS_NUMBER 		= $this->_paymentRef;
		$tableDtlApp 	= array();
		$payAmount 		= $pslip["sumamount"];
		$payCcy 		= $pslip["ccy"];
		$payCustId 		= $pslip["compCode"];
		$psremain 		= $pslip['PS_REMAIN'];
//		print_r($pslip['PS_REMAIN']);die;
		if($payAmount != ""){
			if($pslip['PS_TYPE'] != 14){
			$select = $this->_db->select()
								->from(		array(	'B'				=>'M_APP_BOUNDARY'),
											array(	'BOUNDARY_ID'	=>'BOUNDARY_ID',
													'BOUNDARY_MAX'	=>'BOUNDARY_MAX',
													'BOUNDARY_MIN'	=>'BOUNDARY_MIN'
										))
								->joinLeft(	array('C'		=>'M_CUSTOMER'),'B.CUST_ID = C.CUST_ID',array())
								->where("	BOUNDARY_MIN <= '$payAmount'
											AND BOUNDARY_MAX >= '$payAmount'
											AND C.CUST_ID = '$payCustId'
											AND CCY_BOUNDARY = '$payCcy'");
			}else{
						$select = $this->_db->select()
								->from(		array(	'B'				=>'M_APP_BOUNDARY'),
											array(	'BOUNDARY_ID'	=>'BOUNDARY_ID',
													'BOUNDARY_MAX'	=>'BOUNDARY_MAX',
													'BOUNDARY_MIN'	=>'BOUNDARY_MIN'
										))
								->joinLeft(	array('C'		=>'M_CUSTOMER'),'B.CUST_ID = C.CUST_ID',array())
								->where("	BOUNDARY_MIN <= '$psremain'
											AND BOUNDARY_MAX >= '$psremain'
											AND C.CUST_ID = '$payCustId'
											AND CCY_BOUNDARY = '$payCcy'");
			}
			// echo "<pre>";
			// echo $select->__toString();
			// die;

			$sel = $this->_db->fetchRow($select);
			$BOUNDARYID = $sel["BOUNDARY_ID"];

			if($BOUNDARYID){

				$BOUNDARY_MIN = Application_Helper_General::displayMoney($sel["BOUNDARY_MIN"]);
				$BOUNDARY_MAX = Application_Helper_General::displayMoney($sel["BOUNDARY_MAX"]);

				$BOUNDARYRANGE = $BOUNDARY_MIN.' - '.$BOUNDARY_MAX;

				$selectBoundGroup = $this->_db->select()
												->from(	array(	'BG'			=>'M_APP_BOUNDARY_GROUP'),
														array(	'BGID'			=>'BOUNDARY_GROUP_ID',
																'GROUPUSERID'	=>'GROUP_USER_ID',
																	)
														)
												->where("BG.BOUNDARY_ID = '$BOUNDARYID'");
				// echo "<pre>";
				// echo $selectBoundGroup->__toString();
				// die;

				$BoundGroup = $this->_db->fetchAll($selectBoundGroup);
				// echo "boundaryid: $BOUNDARYID<br />";
				// echo "<pre>";
				// print_r($BoundGroup);
				//die;

				if(!empty($BoundGroup)){
					foreach($BoundGroup as $key => $value){
						$originalGROUPUSERID = $value["GROUPUSERID"];
						$GROUPUSERID = $value["GROUPUSERID"];
						//$arrBoundGroup[$key]['GROUPUSERID'] = $GROUPUSERID;
						//echo "groupuserid: $GROUPUSERID<BR />";

						if(substr($GROUPUSERID,0,1) == 'N')
						{
							$GROUPUSERID = 'Group '.intval(substr($GROUPUSERID,-2));
						}
						if(substr($GROUPUSERID,0,1) == 'S')
						{
							$GROUPUSERID = 'Special Group';
						}

						$selGroupUser = $this->_db->select()
												->from('M_APP_GROUP_USER', array('USER_ID'=>'USER_ID'))
												->where("GROUP_USER_ID = '$originalGROUPUSERID'");
						// echo "<pre>";
						// echo $selGroupUser->__toString();
						// echo "<br />";
						// die;

						$GroupUser = $this->_db->fetchAll($selGroupUser);

						//echo "<pre>";
						//print_r($GroupUser);
						if(!empty($GroupUser)){
							foreach($GroupUser as $d => $dt){
								foreach($dt as $dd => $data){

									$value = $data;
									$arrBoundGroup[$GROUPUSERID][$d] = $value;
									//echo "d, dt,dd,data: $d, $dt, $dd, $data<br />";
								}

							}
						}
						else{ $arrBoundGroup[$GROUPUSERID] = ""; }

					}
					//echo "arrboundgroup: <br />";
					//print_r($arrBoundGroup);

				}
				else { $BOUNDARYRANGE = "No Boundary Group"; }

			}
			else $BOUNDARYRANGE = "No Record Found";
		}
		else $BOUNDARYRANGE = "No Record Found";


		// Select Detail Header Approval Matrix


		$this->view->fieldsapproval	 = (isset($fieldsapproval)) ? $fieldsapproval : '';
		$this->view->tableDtlApp 	 = $tableDtlApp;
		$this->view->BOUNDARYRANGE 	 = $BOUNDARYRANGE;
		$this->view->arrBoundGroup 	 = (isset($arrBoundGroup)) ? $arrBoundGroup : '';
	}

	private function debet($pslip)
	{

		$PS_NUMBER 			= $this->_paymentRef;
		$arrTransferStatus 	= array_combine($this->_transferstatus["code"],$this->_transferstatus["desc"]);
		$arrCitizenship 	= array_combine($this->_citizenship["code"],$this->_citizenship["desc"]);

		$caseTraStatus = "(CASE TT.TRA_STATUS ";
  		foreach($arrTransferStatus as $key => $val)	{ $caseTraStatus .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseTraStatus .= " END)";

  		$caseTraStatus1 = "(CASE E.TRA_STATUS ";
  		foreach($arrTransferStatus as $key => $val)	{ $caseTraStatus1 .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseTraStatus1 .= " END)";

		$caseCityzenship = "(CASE TT.BENEFICIARY_CITIZENSHIP";
  		foreach($arrCitizenship as $key => $val)	{ $caseCityzenship .= " WHEN '".$key."' THEN '".$val."'"; }
  		$caseCityzenship .= " END)";

		// Application_Helper_General::viewAccount($pslip["acbenef"], $pslip["acbenef_ccy"], $pslip["acbenef_name"], $pslip["acbenef_alias"])

		$select = $this->_db->select()
								->from(	array(	'T'				=>'T_TRANSACTION'),
										array(	'acbenef'		=>'T.BENEFICIARY_ACCOUNT',
												'acbenef_ccy'	=>'T.BENEFICIARY_ACCOUNT_CCY',
												'acbenef_name'	=>'T.BENEFICIARY_ACCOUNT_NAME',
												'acbenef_alias'	=>'T.BENEFICIARY_ALIAS_NAME',
											))
								->where('T.PS_NUMBER =? ',$PS_NUMBER);
		$pslipaccount = $this->_db->fetchRow($select);
		$account = $pslipaccount['acbenef'].' ['.$pslipaccount['acbenef_ccy'].'] - '.$pslipaccount['acbenef_name'].' / '.$pslipaccount['acbenef_alias'];

		$this->_tableMst[5]["label"] = "Beneficiary Account";
		$this->_tableMst[5]["value"] = $account;



		$fields = array("TRANSACTION_ID"	=> 'Transaction ID',
						//"ACCTSRC"			=> 'Source Account',
						"SOURCE_NAME"		=> 'Source Account',
						"BENEF_NAME"		=> 'Beneficiary Account',
						// "BANK_NAME"			=> 'Bank',
						// "CITY"				=> 'City',
						// "BENE_CITYZENSHIP"	=> 'Citizenship',
						"PS_CCY"		=> 'CCY',
						"TRA_AMOUNT"		=> 'Amount',
						// "TRANSFER_FEE"		=> 'Charge',
						// "TRA_MESSAGE" 		=> 'Message',
						// "TRA_REFNO"  	   	=> 'Additional Message',
						// "TRANS_TYPE_FULL"	=> 'Transfer Type',
						"RC"					=> 'RC',
						"TRA_STATUS"			=> 'Status',
						"TRA_STATUS1" 		=> 'Update to',
// 						"BANK_RESPONSE" 		=> 'Response',
						//"SOURCE_ACCOUNT" 	=> 'SOURCE_ACCOUNT',
            			);

        $select	= $this->_db->select()
							->from	(
										array(	'TT' => 'T_TRANSACTION'),
										array(
												'TRANSACTION_ID'			=> 'TT.TRANSACTION_ID',
												//'numbertrx'					=> 'COUNT(TT.TRANSACTION_ID)',
												'ACCTSRC'					=> 'TT.SOURCE_ACCOUNT',
												'ACCTSRC_CCY'				=> 'TT.SOURCE_ACCOUNT_CCY',
												'ACCTSRC_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.SOURCE_ACCOUNT_ALIAS_NAME is null THEN TT.SOURCE_ACCOUNT_NAME
																				 ELSE CONCAT ( TT.SOURCE_ACCOUNT_NAME , ' (' , TT.SOURCE_ACCOUNT_ALIAS_NAME , ') ')
																			END"),
												'SOURCE_ACCOUNT_NAME'	=> new Zend_Db_Expr("
																			CONCAT( TT.SOURCE_ACCOUNT , ' (' , TT.SOURCE_ACCOUNT_CCY , ') - ' , F.BANK_NAME ) "),
												'ACBENEF_NAME'			=> new Zend_Db_Expr("
																							CONCAT( TT.BENEFICIARY_ACCOUNT , ' (' , TT.BENEFICIARY_ACCOUNT_CCY , ') - ' , G.BANK_NAME ) "),
												'ACCTSRC_ALIAS'				=> 'TT.SOURCE_ACCOUNT_ALIAS_NAME',
												'TRA_AMOUNT'				=> 'TT.TRA_AMOUNT',
												'TRA_MESSAGE'				=> 'TT.TRA_MESSAGE',
												'TRA_REFNO'					=> 'TT.TRA_REFNO',
												'TRA_STATUS'				=> $caseTraStatus,
												'TRA_STATUS1' 			=> $caseTraStatus1,
												'CITY'					=> 'TT.BENEFICIARY_BANK_CITY',
												'TRANSFER_TYPE'			=> 'TT.TRANSFER_TYPE',
												'TRANS_TYPE_FULL'		=> new Zend_Db_Expr("
																				CASE WHEN TT.TRANSFER_TYPE = '0' THEN 'PB'
																					 WHEN TT.TRANSFER_TYPE = '1' THEN 'RTGS'
																					 WHEN TT.TRANSFER_TYPE = '2' THEN 'SKN'
																					 WHEN TT.TRANSFER_TYPE = '3' THEN 'OUR'
																					 WHEN TT.TRANSFER_TYPE = '4' THEN 'SHA'
																					 ELSE 'N/A'
																				END"),

												'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
												'BANK_NAME'				=> new Zend_Db_Expr("
																				CASE WHEN TT.TRANSFER_TYPE = '0' THEN '".$this->_bankName."'
																					 ELSE TT.BENEFICIARY_BANK_NAME
																				END"),
												'BENE_CITYZENSHIP'		=> $caseCityzenship,
												'RC'					=> 'TT.BANK_RESPONSE',
											'BANK_RESPONSE'			=> 'TT.BANK_RESPONSE',
												/* 'BANK_RESPONSE'		=> new Zend_Db_Expr("
																							CASE WHEN TT.BANK_RESPONSE = '' THEN 'N/A'
																								 WHEN TT.BANK_RESPONSE = null THEN 'N/A'
																								 ELSE TT.BANK_RESPONSE
																							END"), */
												'SOURCE_ACCOUNT'		=> 'TT.SOURCE_ACCOUNT',
												'SOURCE_ACCOUNT_CCY'	=> 'TT.SOURCE_ACCOUNT_CCY',
												'SOURCE_ACCOUNT_BANK'	=> 'F.BANK_NAME',
												'BENEFICIARY_ACCOUNT'		=> 'TT.BENEFICIARY_ACCOUNT',
												'BENEFICIARY_ACCOUNT_CCY'	=> 'TT.BENEFICIARY_ACCOUNT_CCY',
												'BENEFICIARY_ACCOUNT_BANK'	=> 'G.BANK_NAME',
												'PS_CCY'					=> 'H.PS_CCY',
											  )
									)
							->joinLeft ( array('E' => 'T_PSLIP_EXCEPTION_REPAIR_DETAIL'), 'TT.TRANSACTION_ID = E.TRANSACTION_ID', array() )
							->joinLeft(	array(	'F' => 'M_BANK_TABLE' ),'TT.SOURCE_ACCT_BANK_CODE = F.BANK_CODE',array())
							->joinLeft(	array(	'G' => 'M_BANK_TABLE' ),'TT.BENEF_ACCT_BANK_CODE = G.BANK_CODE',array())
							->joinLeft ( array('H' => 'T_PSLIP'), 'TT.PS_NUMBER = H.PS_NUMBER', array() )
							->where('TT.PS_NUMBER = ?', $PS_NUMBER);

		$pslipTrx = $this->_db->fetchAll($select);

		$tableDtl = array();
		foreach ($pslipTrx as $p => $pTrx)
		{
			// create table detail data
			foreach ($fields as $key => $field)
			{
				$value 	= $pTrx[$key];
				$ccy 	= $pTrx["ACCTSRC_CCY"];

				if ($key == "TRA_AMOUNT" || $key == "TRANSFER_FEE")
				{	$value = Application_Helper_General::displayMoney($value);	}


				$value = ($value == "")? "&nbsp;": $value;

				$tableDtl[$p][$key] = $value;
			}

			if ($pTrx['BANK_RESPONSE'] != NULL || !empty($pTrx['BANK_RESPONSE'])) {
				$bankresponse = explode(':', $pTrx['BANK_RESPONSE']);
				//$tableDtl[$p]['BANK_RESPONSE'] = $bankresponse[1];

				$tableDtl[$p]['RC'] = $bankresponse[0];
			}
			if ($pTrx['SOURCE_ACCOUNT_BANK'] != NULL || !empty($pTrx['SOURCE_ACCOUNT_BANK'])) {
				$sourceaccountbank = $pTrx['SOURCE_ACCOUNT'].' ('.$pTrx['SOURCE_ACCOUNT_CCY'].') - '.$pTrx['SOURCE_ACCOUNT_BANK'];
				$tableDtl[$p]['SOURCE_NAME'] = $sourceaccountbank;
			}else{
				$setting = new Settings();
				$master_bank_name = $setting->getSetting('master_bank_name');
				// $this->view->master_bank_app_name = $setting->getSetting('master_bank_app_name');

				$sourceaccountbank = $pTrx['SOURCE_ACCOUNT'].' ('.$pTrx['SOURCE_ACCOUNT_CCY'].') - '.$master_bank_name;
				$tableDtl[$p]['SOURCE_NAME'] = $sourceaccountbank;
			}

			if ($pTrx['BENEFICIARY_ACCOUNT_BANK'] != NULL || !empty($pTrx['BENEFICIARY_ACCOUNT_BANK'])) {
				$benefaccountbank = $pTrx['BENEFICIARY_ACCOUNT'].' ('.$pTrx['BENEFICIARY_ACCOUNT_CCY'].') - '.$pTrx['BENEFICIARY_ACCOUNT_BANK'];
				$tableDtl[$p]['BENEF_NAME'] = $benefaccountbank;
			}else{
				$setting = new Settings();
				$master_bank_name = $setting->getSetting('master_bank_name');
				// $this->view->master_bank_app_name = $setting->getSetting('master_bank_app_name');

				$benefaccountbank = $pTrx['BENEFICIARY_ACCOUNT'].' ('.$pTrx['BENEFICIARY_ACCOUNT_CCY'].') - '.$master_bank_name;
				$tableDtl[$p]['BENEF_NAME'] = $benefaccountbank;
			}
			//$this->view->numbertrx 			= $pTrx['numbertrx'];
		}

		$this->view->fields			 = $fields;
		$this->view->tableDtl 		 = $tableDtl;
		$this->view->TITLE_MST		 = "Transfer To";
		$this->view->TITLE_DTL		 = "Transfer From";
	}

	private function credit($pslip)
	{
		$PS_NUMBER 			= $this->_paymentRef;
		$arrTransferStatus 	= array_combine($this->_transferstatus["code"],$this->_transferstatus["desc"]);
		$arrCitizenship 	= array_combine($this->_citizenship["code"],$this->_citizenship["desc"]);
		$arrEFTStatus 		= array_combine($this->_eftstatus["code"],$this->_eftstatus["desc"]);

		$caseEFTStatus = "(CASE TT.EFT_STATUS ";
  		foreach($arrEFTStatus as $key => $val)	{ $caseEFTStatus .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseEFTStatus .= " END)";

		$caseTraStatus = "(CASE TT.TRA_STATUS ";
  		foreach($arrTransferStatus as $key => $val)	{ $caseTraStatus .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseTraStatus .= " END)";

  		$caseTraStatus1 = "(CASE E.TRA_STATUS ";
  		foreach($arrTransferStatus as $key => $val)	{ $caseTraStatus1 .= " WHEN ".$key." THEN '".$val."'"; }
  		$caseTraStatus1 .= " END)";

		$caseCityzenship = "(CASE TT.BENEFICIARY_CITIZENSHIP";
  		foreach($arrCitizenship as $key => $val)	{ $caseCityzenship .= " WHEN '".$key."' THEN '".$val."'"; }
  		$caseCityzenship .= " END)";

  		$config    		= Zend_Registry::get('config');
		$paymentStatus 	= $config["payment"]["status"]; 
		$casePaymentStatus = Application_Helper_General::casePaymentStatus($paymentStatus);

		//Application_Helper_General::viewAccount($pslip["accsrc"], $pslip["accsrc_ccy"], $pslip["accsrc_name"], $pslip["accsrc_alias"])

		$select = $this->_db->select()
								->from(	array(	'T'				=>'T_TRANSACTION'),
										array(	'accsrc'		=>'T.SOURCE_ACCOUNT',
												'accsrc_ccy'	=>'T.SOURCE_ACCOUNT_CCY',
												'accsrc_name'	=>'T.SOURCE_ACCOUNT_NAME',
												'accsrc_alias'	=>'T.SOURCE_ACCOUNT_ALIAS_NAME',
											))
								->where('T.PS_NUMBER =? ',$PS_NUMBER);
		$pslipaccount = $this->_db->fetchRow($select);
		$account = $pslipaccount['accsrc'].' ['.$pslipaccount['accsrc_ccy'].'] - '.$pslipaccount['accsrc_name'].' / '.$pslipaccount['accsrc_alias'];

		$this->_tableMst[5]["label"] = "Source Account";
		$this->_tableMst[5]["value"] = $account;

		/*

		$fields = array("TRANSACTION_ID"	=> 'Transaction ID',
						"ACCTSRC"			=> 'Source Account',
						"ACCTSRC_NAME"		=> 'Source Account Name',
						"BANK_NAME"			=> 'Bank',
						"CITY"				=> 'City',
						"BENE_CITYZENSHIP"	=> 'Citizenship',
						"ACCTSRC_CCY"		=> 'Ccy',
						"TRA_AMOUNT"		=> 'Amount',
						"TRANSFER_FEE"		=> 'Charge',
						"TRA_MESSAGE" 		=> 'Message',
						"TRA_ADDMESSAGE"  	   	=> 'Additional Message',
						"TRANS_TYPE_FULL"	=> 'Transfer Type',
						"TRA_STATUS" 		=> 'Status',
            			);

		*/

		if($pslip['PS_TYPE'] == '3'){
			$fields = array("TRANSACTION_ID"	=>'Transaction ID',
							"SOURCE_NAME" 	=> 'Source Account',
							//"ACBENEF"			=> 'Beneficiary Account',
							"BENEF_NAME"		=> 'Beneficiary Account',
							// "BANK_NAME"			=> 'Beneficiary Bank',
							// "NOSTRO_NAME"		=> 'Nostro Bank',
							// "COUNTRY_NAME"		=> 'Country',
							"PS_CCY"  	   	=> 'CCY',
							// "RATE"			=> 'Rate',
							"TRA_AMOUNT"  	   	=> 'Amount',
							// "TRANSFER_FEE"		=> 'Transfer Fee',
							// "FULL_AMOUNT_FEE"	=> 'Full Amount Fee',
							// "PROVISION_FEE"		=> 'Provision Fee',
							// "TOTAL_AMOUNT"		=> 'Total Amount',
							// "BOOK_RATE"		=> 'Book Rate',
							// "TRA_MESSAGE" 		=> 'Message',
							// "TRA_ADDMESSAGE"  	   	=> 'Additional Message',
							// "TRANSFER_TYPE"	   	=> 'Transfer Type',
							"RC"					=> 'RC',
							"TRA_STATUS"			=> 'Status',
							"TRA_STATUS1" 		=> 'Update to',
	            			);
		}else if($pslip['PS_TYPE'] == '12'){
			// Table Detail Header
			$fields = array("TRANSACTION_ID"	=>'Transaction ID',
							"SOURCE_NAME" 	=> 'Source Account',
							//"ACBENEF"			=> 'Beneficiary Account',
							"BENEF_NAME"		=> 'Beneficiary Account',
							// "BENEFICIARY_ID_NUMBER"		=> 'Beneficiary NRC',
							// "BENEFICIARY_MOBILE_PHONE_NUMBER"		=> 'Beneficiary Phone',

							// "BANK_NAME"			=> 'Beneficiary Bank',
							// "BENEFICIARY_BANK_ADDRESS1"				=> 'Beneficiary Bank Address',
							// "CITY"				=> 'Beneficiary Bank City',
							// "NOSTRO_NAME"	=> 'Nostro Bank',
							// "COUNTRY_NAME"	=> 'Country',

							"PS_CCY"  	   	=> 'CCY',
							// "RATE"  	   	=> 'Rate',
							"TRA_AMOUNT"  	   	=> 'Amount',
							// "BOOK_RATE"		=> 'Book Rate',
							// "TRANSFER_FEE"		=> 'Transfer Fee',
							// "FULL_AMOUNT_FEE"		=> 'Full Amount Fee',
							// "PROVISION_FEE"		=> 'Provision Fee',
							// "TOTAL"		=> 'Total',
							// "TRA_MESSAGE" 		=> 'Message',
							// "TRA_ADDMESSAGE"  	   	=> 'Additional Message',
							// "TRANSFER_TYPE"	   	=> 'Transfer Type',
							"RC"					=> 'RC',
							"TRA_STATUS"			=> 'Status',
							"TRA_STATUS1" 		=> 'Update to',
							// "EFT_STATUS" 		=> 'EFT Status',
	// 						"BANK_RESPONSE" 		=> 'Response',
							// "SOURCE_ACCOUNT" 	=> 'SOURCE_ACCOUNT',
	            			);
		}
		else{
			// Table Detail Header
			$fields = array("TRANSACTION_ID"	=>'Transaction ID',
							"SOURCE_NAME" 	=> 'Source Account',
							"BENEF_NAME"			=> 'Beneficiary Account',
							// "ACBENEF_NAME"		=> 'Beneficiary Name',
							// "BANK_NAME"			=> 'Bank',
							// "CITY"				=> 'City',
							// "BENE_CITYZENSHIP"	=> 'Citizenship',
							"PS_CCY"  	   	=> 'CCY',
							"TRA_AMOUNT"  	   	=> 'Amount',
							// "TRANSFER_FEE"		=> 'Charge',

							// "TRA_MESSAGE" 		=> 'Message',
							// "TRA_ADDMESSAGE"  	   	=> 'Additional Message',
							// "TRANSFER_TYPE"	   	=> 'Transfer Type',
							"RC"					=> 'RC',
							"TRA_STATUS"			=> 'Status',
							"TRA_STATUS1" 			=> 'Update to',
							// "EFT_STATUS" 		=> 'EFT Status',
	// 						"BANK_RESPONSE" 		=> 'Response',
							
	            			);
		}

		$select	= $this->_db->select()
							->from(	array(	'TT' => 'T_TRANSACTION'),
									array(
											'TRANSACTION_ID'		=> 'TT.TRANSACTION_ID',
											//'numbertrx'				=> 'COUNT(TT.TRANSACTION_ID)',
											'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
											// 'ACBENEF_NAME'			=> new Zend_Db_Expr("
											// 								CONCAT ( TT.BENEFICIARY_ACCOUNT_NAME , ' ' ) "),
											'ACBENEF_NAME'			=> new Zend_Db_Expr("
																							CONCAT( TT.BENEFICIARY_ACCOUNT , ' (' , TT.BENEFICIARY_ACCOUNT_CCY , ') - ' , G.BANK_NAME ) "),
											'SOURCE_ACCOUNT_NAME'	=> new Zend_Db_Expr("
																							CONCAT( TT.SOURCE_ACCOUNT , ' (' , TT.SOURCE_ACCOUNT_CCY , ') - ' , F.BANK_NAME ) "),
											'TRA_AMOUNT_IDR'	=> new Zend_Db_Expr("
																							CONCAT( D.PS_CCY , ' - ' , TT.TRA_AMOUNT ) "),
											'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
											'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
											'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',
											'TT.BENEFICIARY_MOBILE_PHONE_NUMBER',
											'TT.BENEFICIARY_ID_NUMBER',
											'TT.BENEFICIARY_BANK_ADDRESS1',
											'TT.RATE',
											'TRA_REFNO'				=> 'TT.TRA_REFNO',
											'NOSTRO_NAME'			=> 'TT.NOSTRO_NAME',
											'BOOK_RATE'				=> 'TT.BOOK_RATE',
											'TT.RATE',
											'BOOK_RATE_BUY'			=> 'TT.BOOK_RATE_BUY',
											'FULL_AMOUNT_FEE'		=> 'TT.FULL_AMOUNT_FEE',
											'PROVISION_FEE'			=> 'TT.PROVISION_FEE',
											'TRANSFER_TYPE'			=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN 'PB'
																				 WHEN TT.TRANSFER_TYPE = '1' THEN 'RTGS'
																				 WHEN TT.TRANSFER_TYPE = '2' THEN 'SKN'
																				 WHEN TT.TRANSFER_TYPE = '3' THEN 'OUR'
																				 WHEN TT.TRANSFER_TYPE = '4' THEN 'SHA'
																				 WHEN TT.TRANSFER_TYPE = '9' THEN 'FA'
																				 WHEN TT.TRANSFER_TYPE = '10' THEN 'No FA'
																				 ELSE 'N/A'
																			END"),
											'STATUS'			=> new Zend_Db_Expr("
																			CASE WHEN TT.TRA_STATUS = '3' THEN 'Success'
																				 WHEN TT.TRA_STATUS = '4' THEN 'Failed'
																			END"),
											'PAYSTATUS'				=> new Zend_Db_Expr("CASE D.PS_STATUS $casePaymentStatus ELSE 'N/A' END"),
											'SOURCE_CCY'			=> 'TT.SOURCE_ACCOUNT_CCY',
											'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
											'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
											'TRA_REMAIN'			=> 'TT.TRA_REMAIN',
											'COUNTRY_NAME'			=> 'C.COUNTRY_NAME',
											'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
											'BANK_NAME'				=> new Zend_Db_Expr("
																			CASE WHEN TT.TRANSFER_TYPE = '0' THEN '".$this->_bankName."'
																				 ELSE TT.BENEFICIARY_BANK_NAME
																			END"),
											'TRA_STATUS' 			=> $caseTraStatus,
											'TRA_STATUS1' 			=> $caseTraStatus1,
											'CITY'					=> 'TT.BENEFICIARY_BANK_CITY',
											'BENE_CITYZENSHIP'		=> $caseCityzenship,
											'RC'					=> 'TT.BANK_RESPONSE',
											'BANK_RESPONSE'			=> 'TT.BANK_RESPONSE',
											// 'EFT_STATUS'			=> new Zend_Db_Expr("
																			// CASE WHEN TT.TRANSFER_TYPE = '0' THEN '-'
																				 // ELSE ".$caseEFTStatus."
																			// END"),
											/* 'BANK_RESPONSE'		=> new Zend_Db_Expr("
																							CASE WHEN TT.BANK_RESPONSE = '' THEN 'N/A'
																								 WHEN TT.BANK_RESPONSE = null THEN 'N/A'
																								 ELSE TT.BANK_RESPONSE
																							END"), */
											'SOURCE_ACCOUNT'		=> 'TT.SOURCE_ACCOUNT',
											'SOURCE_ACCOUNT_CCY'	=> 'TT.SOURCE_ACCOUNT_CCY',
											'SOURCE_ACCOUNT_BANK'	=> 'F.BANK_NAME',
											'BENEFICIARY_ACCOUNT'		=> 'TT.BENEFICIARY_ACCOUNT',
											'BENEFICIARY_ACCOUNT_CCY'	=> 'TT.BENEFICIARY_ACCOUNT_CCY',
											'BENEFICIARY_ACCOUNT_BANK'	=> 'G.BANK_NAME',
											'D.PS_CCY','D.CUST_ID',
											'TT.EQUIVALENT_AMOUNT_IDR',
											'TT.EQUIVALENT_AMOUNT_USD',
											'PS_TYPE'				=> 'D.PS_TYPE',
											'PS_STATUS'				=> 'D.PS_STATUS',
											'PS_CCY'				=> 'D.PS_CCY',
										  )
									)
							->joinLeft	(   array('B' => 'M_BENEFICIARY'), 'TT.BENEFICIARY_ID = B.BENEFICIARY_ID', array() )
							->joinLeft ( array('C' => 'M_COUNTRY'), 'TT.BENEFICIARY_BANK_COUNTRY = C.COUNTRY_CODE', array() )
							->joinLeft ( array('D' => 'T_PSLIP'), 'TT.PS_NUMBER = D.PS_NUMBER', array() )
							->joinLeft ( array('E' => 'T_PSLIP_EXCEPTION_REPAIR_DETAIL'), 'TT.TRANSACTION_ID = E.TRANSACTION_ID', array() )
							->joinLeft(	array(	'F' => 'M_BANK_TABLE' ),'TT.SOURCE_ACCT_BANK_CODE = F.BANK_CODE',array())
							->joinLeft(	array(	'G' => 'M_BANK_TABLE' ),'TT.BENEF_ACCT_BANK_CODE = G.BANK_CODE',array())
							->where('TT.PS_NUMBER = ?', $PS_NUMBER);

		$pslipTrx = $this->_db->fetchAll($select);
		// print_r($pslipTrx);die;
		$tableDtl = array();
		foreach ($pslipTrx as $p => $pTrx)
		{
			// create table detail data
			foreach ($fields as $key => $field)
			{
				if($key == "TOTAL_AMOUNT"){
					if($pTrx['PS_CCY'] == 'USD'){
						$totalinvalas = $pTrx['EQUIVALENT_AMOUNT_USD'];
						$totalinidr = $pTrx['EQUIVALENT_AMOUNT_IDR'];
						//print_r($pTrx);die;
						if($pTrx['SOURCE_CCY']=='USD' && $pTrx['ACBENEF_CCY']=='USD' && $pslip['PS_TYPE']=='1'){
								$value = 'USD '.Application_Helper_General::displayMoney($pTrx['TRA_AMOUNT']);

						}else if($pTrx['SOURCE_CCY']=='USD' && $pTrx['ACBENEF_CCY']=='USD'){
								$value = 'USD '.Application_Helper_General::displayMoney($pTrx['EQUIVALENT_AMOUNT_IDR']);

							}else if($pTrx['PS_TYPE']!='3'){
						$value = $pTrx['ACBENEF_CCY'].' '.Application_Helper_General::displayMoney($totalinvalas).' (IDR '.Application_Helper_General::displayMoney($totalinidr).')';
						}else{
						$value = 'IDR '.Application_Helper_General::displayMoney($totalinidr);

						}

					}
					else{
						if($pTrx['EQUIVALENT_AMOUNT_IDR'] != '0.00' || $pTrx['EQUIVALENT_AMOUNT_IDR'] != ''){
							$pTrx['EQUIVALENT_AMOUNT_IDR'] = $pTrx['EQUIVALENT_AMOUNT_IDR'];
						}else{
							$pTrx['EQUIVALENT_AMOUNT_IDR'] = $pTrx['TRA_AMOUNT'];
						}

						$totalinidr = $pTrx['EQUIVALENT_AMOUNT_IDR'];
						$value = 'IDR '.Application_Helper_General::displayMoney($totalinidr);

					}


				}
				else{
					$value = $pTrx[$key];
				}
				if($key == 'ACBENEF' && ($pTrx['TRANSFER_TYPE']=='FA' || $pTrx['TRANSFER_TYPE']=='No FA')){
					// $app = Zend_Registry::get('config');
					// $appBankname = $app['app']['bankname'];
					$value = '';
				}
				if($key == 'FULL_AMOUNT_FEE' && ($pTrx['TRANSFER_TYPE']=='No FA')){
					// $app = Zend_Registry::get('config');
					// $appBankname = $app['app']['bankname'];
					$value = '0';
				}


				if($key == 'BANK_NAME' && ($pTrx['TRANSFER_TYPE']=='FA' || $pTrx['TRANSFER_TYPE']=='No FA')){
					$app = Zend_Registry::get('config');
					$appBankname = $app['app']['bankname'];
					$value = $appBankname.' - '.$pTrx['BANK_NAME'];
				}

				if($key == 'TOTAL'){
					if($pTrx['TRANSFER_TYPE']=='No FA'){
						$total = $pTrx['TRA_AMOUNT'];
					}else{
						$total = $pTrx['TRA_AMOUNT'] + $pTrx['FULL_AMOUNT_FEE'];
					}

					$value = $pTrx['SOURCE_CCY'].' '.Application_Helper_General::displayMoney($total);
				}
				if ($key == "TRA_AMOUNT")
				{
					if($pTrx['PS_TYPE']=='15' || $pTrx['PS_TYPE']=='14'){
						if($pTrx['PS_STATUS'] == '5' || $pTrx['PS_STATUS'] == '1' || $pTrx['PS_STATUS'] == '2'){
						$value = Application_Helper_General::displayMoney($pTrx['TRA_REMAIN']);
						}else{
						$value = '-';
						}

					}else{
					$value = Application_Helper_General::displayMoney($value);
					}
				}

				if($key == 'BOOK_RATE'){

					$value = Application_Helper_General::displayMoney($value);
					// print_r($value);die;
				}

				if($key == "TRANSFER_FEE"){

					$selecttrffee = $this->_db->select()->from(array('T'=>'M_CHARGES_REMITTANCE'),array('*'))
					->where("T.CHARGE_TYPE =?",'3')
			->where("T.CUST_ID =?",$pTrx['CUST_ID'])
			->where("T.CHARGE_CCY =? ", $pTrx['SOURCE_CCY'])
			;
			//echo $selecttrffee;
			$trffee = $this->_db->fetchRow($selecttrffee);
					$value = $trffee['CHARGE_AMOUNT_CCY'].' '.Application_Helper_General::displayMoney($value);
				}elseif($key == "FULL_AMOUNT_FEE"){
								$selecttrfFA = $this->_db->select()
			->from(		array(	'T'=>'M_CHARGES_REMITTANCE'),array('*'))
			->where("T.CHARGE_TYPE =?",'4')
			->where("T.CUST_ID =?",$pTrx['CUST_ID'])
			->where("T.CHARGE_CCY =?",$pTrx['ACBENEF_CCY'])
			;
			$trfFA = $this->_db->fetchRow($selecttrfFA);

					$value = $trfFA['CHARGE_AMOUNT_CCY'].' '.Application_Helper_General::displayMoney($value);
				}elseif($key == "TRANSFER_TYPE")
				{
					if($value=='PB'){
						$value = 'Mayapada';
					}
				}
				if($key == 'RATE'){
				if($pTrx['SOURCE_CCY']=='USD' && $pTrx['ACBENEF_CCY']=='USD'){
					$value = '0.00';
				}
					$value = 'IDR '.Application_Helper_General::displayMoney($value);
				}
				if($key == 'PROVISION_FEE'){




			$selecttrfpro = $this->_db->select()
			->from(		array(	'T'=>'M_CHARGES_REMITTANCE'),array('*'))
			->where("T.CHARGE_TYPE =?",'5')
			->where("T.CUST_ID =?",$pTrx['CUST_ID'])
			->where("T.CHARGE_CCY =?",$pTrx['SOURCE_CCY'])
			;
			$trfpro = $this->_db->fetchRow($selecttrfpro);
				$value = $trfpro['CHARGE_AMOUNT_CCY'].' '.Application_Helper_General::displayMoney($value);
				//print_r($value);die;
				}

				$value = ($value == "")? "&nbsp;": $value;

				$tableDtl[$p][$key] = $value;	// TODO: Transfer Charge
			}

			if ($pTrx['BANK_RESPONSE'] != NULL || !empty($pTrx['BANK_RESPONSE'])) {
				$bankresponse = explode(':', $pTrx['BANK_RESPONSE']);
				//$tableDtl[$p]['BANK_RESPONSE'] = $bankresponse[1];

				$tableDtl[$p]['RC'] = $bankresponse[0];
			}
			if ($pTrx['SOURCE_ACCOUNT_BANK'] != NULL || !empty($pTrx['SOURCE_ACCOUNT_BANK'])) {
				$sourceaccountbank = $pTrx['SOURCE_ACCOUNT'].' ('.$pTrx['SOURCE_ACCOUNT_CCY'].') - '.$pTrx['SOURCE_ACCOUNT_BANK'];
				$tableDtl[$p]['SOURCE_NAME'] = $sourceaccountbank;
			}else{
				$setting = new Settings();
				$master_bank_name = $setting->getSetting('master_bank_name');
				// $this->view->master_bank_app_name = $setting->getSetting('master_bank_app_name');

				$sourceaccountbank = $pTrx['SOURCE_ACCOUNT'].' ('.$pTrx['SOURCE_ACCOUNT_CCY'].') - '.$master_bank_name;
				$tableDtl[$p]['SOURCE_NAME'] = $sourceaccountbank;
			}

			if ($pTrx['BENEFICIARY_ACCOUNT_BANK'] != NULL || !empty($pTrx['BENEFICIARY_ACCOUNT_BANK'])) {
				$benefaccountbank = $pTrx['BENEFICIARY_ACCOUNT'].' ('.$pTrx['BENEFICIARY_ACCOUNT_CCY'].') - '.$pTrx['BENEFICIARY_ACCOUNT_BANK'];
				$tableDtl[$p]['BENEF_NAME'] = $benefaccountbank;
			}else{
				$setting = new Settings();
				$master_bank_name = $setting->getSetting('master_bank_name');
				// $this->view->master_bank_app_name = $setting->getSetting('master_bank_app_name');

				$benefaccountbank = $pTrx['BENEFICIARY_ACCOUNT'].' ('.$pTrx['BENEFICIARY_ACCOUNT_CCY'].') - '.$master_bank_name;
				$tableDtl[$p]['BENEF_NAME'] = $benefaccountbank;
			}
			//$this->view->numbertrx 			= $pTrx['numbertrx'];
		}

		$this->view->fields 			= $fields;
		$this->view->tableDtl 			= $tableDtl;
	}

}