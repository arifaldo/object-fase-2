<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';

require_once 'General/Customer.php';
require_once 'General/CustomerUser.php';

require_once 'SGO/Extendedmodule/PHPExcel.php';
require_once 'SGO/Extendedmodule/PHPExcel/IOFactory.php';

class businessadapter_ViewController extends Application_Main {

    public function indexAction()
    { 
        $this->_helper->_layout->setLayout('newlayout');

        $profileid = $this->_request->getParam('profileid');
        //var_dump($profileid);
        $this->view->profileId = $profileid;

		$checktempData = $this->_db->select()
                     ->from(array('H' => 'TEMP_ADAPTER_PROFILE'), '*')
                     //->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'H.PROFILE_ID = D.PROFILE_ID', '*')
                     ->where('H.PROFILE_ID = '.$this->_db->quote($profileid));

        $tempAdapter = $this->_db->fetchAll($checktempData);
		if(!empty($tempAdapter)){
			$this->view->hidebutton = true;
		}

        $selectData = $this->_db->select()
                     ->from(array('H' => 'M_ADAPTER_PROFILE'), '*')
                     ->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'H.PROFILE_ID = D.PROFILE_ID', '*')
                     ->where('H.PROFILE_ID = '.$this->_db->quote($profileid));

        $dataAdapter = $this->_db->fetchAll($selectData);

        $fileName = $dataAdapter[0]['FILE_PATH'];
        $fixLength = $dataAdapter[0]['FIXLENGTH'];
        $fixLengthType = $dataAdapter[0]['FIXLENGTH_TYPE'];
        $fixLengthHeader = $dataAdapter[0]['FIXLENGTH_HEADER_ORDER'];
        $fixLengthHeaderName = $dataAdapter[0]['FIXLENGTH_HEADER_NAME'];
        $fixLengthContent = $dataAdapter[0]['FIXLENGTH_CONTENT_ORDER'];
        $delimitedWith = $dataAdapter[0]['DELIMITED_WITH'];

        $file_contents = file_get_contents($fileName);
        
        $delete     = $this->_getParam('delete');
        $params = $this->_request->getParams();
        
        $select = $this->_db->select()
                                    ->from(array('H' => 'M_CUST_ADAPTER_PROFILE'),array('*'));

             $select->where('H.ADAPTER_PROFILE_ID = '. $this->_db->quote($profileid));
    
             $profileCust = $this->_db->fetchAll($select);
             $this->view->custlist = $profileCust;
        
         if($delete)
        {
            $profileid = $this->_request->getParam('profileid');
			//var_dump($profileid);die;
            $validators = array (
                                    'profileid' => array   (
                                                            'NotEmpty',
                                                            'messages' => array (
                                                                                    'Error File ID Submitted',
                                                                                )
                                                        ),
                                );

            $filtersVal = array ( 'profileid' => array('StringTrim','StripTags'));

            $zf_filter_input = new Zend_Filter_Input($filtersVal,$validators,$params,$this->_optionsValidator);
                $success = false;
            //foreach ($profileid as $key => $value) {
              //  if(!empty($value)){
             //       $success = true;
              //  }
            //}
            //var_dump($params);die; 
             $select = $this->_db->select()
                                    ->from(array('H' => 'M_CUST_ADAPTER_PROFILE'),array('*'));

             $select->where('H.ADAPTER_PROFILE_ID = '. $this->_db->quote($profileid));
			//echo $select;
             $profileCust = $this->_db->fetchAll($select);
			 $success = true;
             if(!empty($profileCust)){
                 $success = false;
                 $msg = 'Adapter Profile is being used by customers';
             }
            //var_dump($profileid);die;
            //if(){
                
            //}
			//var_dump($success);die;
            if($zf_filter_input->isValid() && $success)
            {
			
                try
                {
                   // foreach ($profileid as $key => $value)
                   // {

                        $id = $profileid;

                        //unlink files
                        $select = $this->_db->select()
                                    ->from(array('H' => 'M_ADAPTER_PROFILE'),array('*'));

                        $select->where('H.PROFILE_ID = '. $this->_db->quote($id));
						//echo $select;die;
                        $profileData = $this->_db->fetchAll($select);

                        $file_path = $profileData[0]['FILE_PATH'];
						
						
						$selectdetail = $this->_db->select()
                                    ->from(array('H' => 'M_ADAPTER_PROFILE_DETAIL'),array('*'));

                        $selectdetail->where('H.PROFILE_ID = '. $this->_db->quote($id));
						//echo $select;die;
                        $profileDetail = $this->_db->fetchAll($selectdetail);
						
                        //@unlink($file_path);

                        $this->_db->beginTransaction();

                        //$where['PROFILE_ID = ?'] = $id;
                        //$result =  $this->_db->delete('M_ADAPTER_PROFILE',$where);
                        $info = $this->language->_('businessadapter');
						$info2 = $this->language->_('Business Profile Adapter');
						$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['delete'],null,'M_ADAPTER_PROFILE','TEMP_ADAPTER_PROFILE',$profileData[0]['PROFILE_NAME'],'','');
												
						$tableName = 'TEMP_ADAPTER_PROFILE';
						$arrData = $profileData[0];
						$arrData['CHANGES_ID'] = $change_id;
						

						
						
						
                        //$tableName = 'M_ADAPTER_PROFILE';
                        $arrData['STATUS'] = 3;
						$arrData['SUGGESTED_BY'] = $this->_userIdLogin;
						$arrData['SUGGESTED_DATE'] = date('Y-m-d H:i:s');
						$this->_db->insert($tableName, $arrData);
						$tableDetail = 'TEMP_ADAPTER_PROFILE_DETAIL';
						if(!empty($profileDetail)){
						foreach($profileDetail as $val){
						$arrDetail = $val;
						$arrDetail['CHANGES_ID'] = $change_id;
						$this->_db->insert($tableDetail, $arrDetail);
						}
						}

                        
                        
                        //$wheredelete['PROFILE_ID = ?'] = $id;
                        //$result = $this->_db->delete('M_ADAPTER_PROFILE_DETAIL',$wheredelete);

                        $profilename = $profileData[0]['PROFILE_NAME'];
                        $profilid    = $profileData[0]['PROFILE_ID'];

                        Application_Helper_General::writeLog('PFAD','Delete Business Profile Adapter. Profile Name : '.$profilename.' (ID: '.$profilid.')');

                        $this->_db->commit();
                    //}

                }
                catch(Exception $e)
                {
					//var_dump($e);die;
                    $this->_db->rollBack();
                }
                $this->setbackURL('/businessadapter/');
                $this->_redirect('/notification/success/index');
            }
            else
            {
                
                $error          = true;
                $errors         = $zf_filter_input->getMessages();
                if(!empty($msg)){
                    $errors = $msg;
                }
				//var_dump($errors);die;
                $profileiderr   = (isset($errors['profileiderr']))? $errors['profileiderr'] : null;
            }

        }
        
        
        $suspend     = $this->_getParam('suspend');
        
         if($suspend)
        {
            $profileid = $this->_request->getParam('profileid');

            $validators = array (
                                    'profileid' => array   (
                                                            'NotEmpty',
                                                            'messages' => array (
                                                                                    'Error File ID Submitted',
                                                                                )
                                                        ),
                                );

            $filtersVal = array ( 'profileid' => array('StringTrim','StripTags'));

            $zf_filter_input = new Zend_Filter_Input($filtersVal,$validators,$params,$this->_optionsValidator);
                $success = false;
            //foreach ($profileid as $key => $value) {
             //   if(!empty($value)){
                    $success = true;
             //   }
            //}


            if($zf_filter_input->isValid() && $success)
            {

                try
                {
                    //foreach ($profileid as $key => $value)
                   // {

                        $id = $profileid;

                        //unlink files
                        $select = $this->_db->select()
                                    ->from(array('H' => 'M_ADAPTER_PROFILE'),array('*'));

                        $select->where('H.PROFILE_ID = '. $this->_db->quote($id));

                        $profileData = $this->_db->fetchAll($select);

                        $file_path = $profileData[0]['FILE_PATH'];
                        //@unlink($file_path);

                        $this->_db->beginTransaction();

                        //$where['PROFILE_ID = ?'] = $id;
                        //$result =  $this->_db->delete('M_ADAPTER_PROFILE',$where);
                        
                        $info = $this->language->_('businessadapter');
						$info2 = $this->language->_('Business Profile Adapter');
						$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['suspend'],null,'M_ADAPTER_PROFILE','TEMP_ADAPTER_PROFILE',$profileData[0]['PROFILE_NAME'],'','');
												
						$tableName = 'TEMP_ADAPTER_PROFILE';
						$arrData = $profileData[0];
						$arrData['CHANGES_ID'] = $change_id;
						

						$selectdetail = $this->_db->select()
                                    ->from(array('H' => 'M_ADAPTER_PROFILE_DETAIL'),array('*'));

                        $selectdetail->where('H.PROFILE_ID = '. $this->_db->quote($id));
						//echo $select;die;
                        $profileDetail = $this->_db->fetchAll($selectdetail);
						
						
                        //$tableName = 'M_ADAPTER_PROFILE';
                        $arrData['STATUS'] = 2;
						$arrData['SUGGESTED_BY'] = $this->_userIdLogin;
						$arrData['SUGGESTED_DATE'] = date('Y-m-d H:i:s');
						$this->_db->insert($tableName, $arrData);
						$tableDetail = 'TEMP_ADAPTER_PROFILE_DETAIL';
						if(!empty($profileDetail)){
						foreach($profileDetail as $val){
						$arrDetail = $val;
						$arrDetail['CHANGES_ID'] = $change_id;
						$this->_db->insert($tableDetail, $arrDetail);
						}
						}
                        //$wheredelete['PROFILE_ID = ?'] = $id;
                        //$result = $this->_db->delete('M_ADAPTER_PROFILE_DETAIL',$wheredelete);

                        $profilename = $profileData[0]['PROFILE_NAME'];
                        $profilid    = $profileData[0]['PROFILE_ID'];

                        Application_Helper_General::writeLog('PFAD','Suspend Business Profile Adapter. Profile Name : '.$profilename.' (ID: '.$profilid.')');

                        $this->_db->commit();
                    //}

                }
                catch(Exception $e)
                {
                    $this->_db->rollBack();
                }
                $this->setbackURL('/businessadapter/');
                $this->_redirect('/notification/success/index');
            }
            else
            {
                $error          = true;
                $errors         = $zf_filter_input->getMessages();
                $profileiderr   = (isset($errors['profileiderr']))? $errors['profileiderr'] : null;
            }

        }
        
        $unsuspend     = $this->_getParam('unsuspend');
        
         if($unsuspend)
        {
            $profileid = $this->_request->getParam('profileid');

            $validators = array (
                                    'profileid' => array   (
                                                            'NotEmpty',
                                                            'messages' => array (
                                                                                    'Error File ID Submitted',
                                                                                )
                                                        ),
                                );

            $filtersVal = array ( 'profileid' => array('StringTrim','StripTags'));

            $zf_filter_input = new Zend_Filter_Input($filtersVal,$validators,$params,$this->_optionsValidator);
                $success = false;
            //foreach ($profileid as $key => $value) {
                //if(!empty($value)){
                    $success = true;
              //  }
           // }


            if($zf_filter_input->isValid() && $success)
            {

                try
                {
                    //foreach ($profileid as $key => $value)
                    //{

                        $id = $profileid;

                        //unlink files
                        $select = $this->_db->select()
                                    ->from(array('H' => 'M_ADAPTER_PROFILE'),array('*'));

                        $select->where('H.PROFILE_ID = '. $this->_db->quote($id));

                        $profileData = $this->_db->fetchAll($select);

                        $file_path = $profileData[0]['FILE_PATH'];
                        //@unlink($file_path);

                        $this->_db->beginTransaction();

                        //$where['PROFILE_ID = ?'] = $id;
                        //$result =  $this->_db->delete('M_ADAPTER_PROFILE',$where);
                        
                        $info = $this->language->_('businessadapter');
						$info2 = $this->language->_('Business Profile Adapter');
						$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['unsuspend'],null,'M_ADAPTER_PROFILE','TEMP_ADAPTER_PROFILE',$profileData[0]['PROFILE_NAME'],'','');
												
						$tableName = 'TEMP_ADAPTER_PROFILE';
						$arrData = $profileData[0];
						$arrData['CHANGES_ID'] = $change_id;
						

						$selectdetail = $this->_db->select()
                                    ->from(array('H' => 'M_ADAPTER_PROFILE_DETAIL'),array('*'));

                        $selectdetail->where('H.PROFILE_ID = '. $this->_db->quote($id));
						//echo $select;die;
                        $profileDetail = $this->_db->fetchAll($selectdetail);
						
						
                        //$tableName = 'M_ADAPTER_PROFILE';
                        $arrData['STATUS'] = 1;
						$arrData['SUGGESTED_BY'] = $this->_userIdLogin;
						$arrData['SUGGESTED_DATE'] = date('Y-m-d H:i:s');
						$this->_db->insert($tableName, $arrData);
						$tableDetail = 'TEMP_ADAPTER_PROFILE_DETAIL';
						if(!empty($profileDetail)){
						foreach($profileDetail as $val){
						$arrDetail = $val;
						$arrDetail['CHANGES_ID'] = $change_id;
						$this->_db->insert($tableDetail, $arrDetail);
						}
						}
                        
                        //$wheredelete['PROFILE_ID = ?'] = $id;
                        //$result = $this->_db->delete('M_ADAPTER_PROFILE_DETAIL',$wheredelete);

                        $profilename = $profileData[0]['PROFILE_NAME'];
                        $profilid    = $profileData[0]['PROFILE_ID'];

                        Application_Helper_General::writeLog('PFAD','Unsuspend Business Profile Adapter. Profile Name : '.$profilename.' (ID: '.$profilid.')');

                        $this->_db->commit();
                    //}

                }
                catch(Exception $e)
                {
                    $this->_db->rollBack();
                }
                $this->setbackURL('/businessadapter/');
                $this->_redirect('/notification/success/index');
            }
            else
            {
                $error          = true;
                $errors         = $zf_filter_input->getMessages();
                $profileiderr   = (isset($errors['profileiderr']))? $errors['profileiderr'] : null;
            }

        }


        if (strpos($fileName, '.csv') !== false) {
            $extensionName = 'csv';
        }
        else if (strpos($fileName, '.json') !== false) {
            $extensionName = 'json';
        }
        else if (strpos($fileName, '.xml') !== false) {
            $extensionName = 'xml';
        }
        else if (strpos($fileName, '.txt') !== false) {
            $extensionName = 'txt';
        }
        else if (strpos($fileName, '.xls') !== false) {
            $extensionName = 'xls';
        }
        else if (strpos($fileName, '.xlsx') !== false) {
            $extensionName = 'xlsx';
        }
        
        //if csv occured
        if ($extensionName === 'csv') {

            if (!empty($delimitedWith)) {
                $data = $this->_helper->parser->parseCSV($fileName, $delimitedWith);
            }
            //if fix length
            else if ($fixLength == 1) {

                $data = file($fileName); 
            }
            else{
                $data = $this->_helper->parser->parseCSV($fileName);
            }
        }
        //if txt occured
        else if ($extensionName === 'txt') {

           $lines = file($fileName);

           $checkMt940 = false;
           $checkMt101 = false;
           foreach ($lines as $line) {
               if (strpos($line, '{1:') !== false) {
                   $checkMt940 = true;
               }
               else if (strpos($line, ':20:') !== false) {
                   $checkMt101 = true;
               }
           }

           //if mt940 format
           if ($checkMt940) {
               
                $data = $this->_helper->parser->mt940($fileName);
                $mtFile = true;
           }
           else if($checkMt101){
                $data = $this->_helper->parser->mt101($fileName);
                $mtFile = true;
           }
           else{

                if (!empty($delimitedWith)) {
                    //parse csv jg bisa utk txt
                    $data = $this->_helper->parser->parseCSV($fileName, $delimitedWith);
                }
                //if fix length
                else if ($fixLength == 1) {

                    $data = file($fileName);                   
                }
                else{
                    //parse csv jg bisa utk txt
                    $data = $this->_helper->parser->parseCSV($fileName, ',');
                }
           }
        }
        //if json occured
        else if ($extensionName === 'json') {

            $datajson = json_decode($file_contents, 1);
            $i = 0;
            foreach ($datajson as $key => $value) {
                if ($i == 0) {
                    $data[$i] = array_keys($value);
                    $data[$i + 1] = array_values($value);
                }
                else{
                    $data[$i+1] = array_values($value);
                }

                $i++;
            }
        }
        //if xml occured
        else if($extensionName === 'xml'){

            $xml = (array) simplexml_load_string($file_contents);

             $i = 0;
            foreach ($xml as $key => $value) {
                foreach ($value as $key2 => $value2) {
                    if ($i == 0) {
                        $data[$i] = array_keys((array)$value2);
                        $data[$i + 1] = array_values((array)$value2);
                    }
                    else{
                        $data[$i+1] = array_values((array)$value2);
                    }

                    $i++;
                }
            }

            for($i=0; $i<count($data); $i++){
                if ($i > 0) {
                    foreach ($data[$i] as $key => $value) {
                        if (empty($value)) {
                            $data[$i][$key] = null;
                        }
                    }
                }
            }
        }
        else if($extensionName === 'xls' || $extensionName === 'xlsx'){
            try {
                $inputFileType = IOFactory::identify($fileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($fileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($fileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
             
            for ($row = 1; $row <= $highestRow; $row++){                        
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row);

                $data[$row - 1] = $rowData[0];
            }
        }

        if(!empty($data))
        {    

            if ($fixLength == 1) {   

                $fixLengthHeaderNameArr = explode(',', $fixLengthHeaderName);

                if (count($fixLengthHeaderNameArr) != 0) {

                    foreach ($fixLengthHeaderNameArr as $key => $value) {
                        //first order, startIndex from 0
                       $newdata[0][] = $value;
                    }
                }
                else{
                    $newdata[0][] = $fixLengthHeaderName;
                }

                $startArrIndex = 1;

                 // if with header
                if ($fixLengthType == 1) {
                    $surplusIndex = 0;
                }
                else if ($fixLengthType == 3) {
                    $surplusIndex = 1;
                }

                $contentOrderArr = explode(',', $fixLengthContent);

                if (count($contentOrderArr) > 1) {
                    foreach ($data as $key => $value) {
                        if ($key >= $startArrIndex) {
                            foreach ($contentOrderArr as $key2 => $value2) {
                                //first order, startIndex from 0
                                if ($key2 == 0) {
                                    $startIndex = 0;
                                    $endIndex = (int) $value2 + 1;

                                    $newdata[$key + $surplusIndex][] = substr($value,$startIndex,$endIndex);
                                }
                                else{
                                    $startIndex = (int) ($contentOrderArr[$key2 - 1] + 1);
                                    $endIndex = (int) ($value2 - ($startIndex - 1));

                                    $newdata[$key + $surplusIndex][] = substr($value,$startIndex,$endIndex);
                                }
                            }       
                        }   
                    }
                }

                $this->view->headerData = $newdata[0];
                unset($newdata[0]);
                $this->view->valueData  = $newdata;
            }
            else{
 
                //---------get rid of empty spaces---------
                $emptySpaces = array();
                foreach ($data[0] as $key => $value) {
                    if (empty($value)) {
                        array_push($emptySpaces, $key);
                        unset($data[0][$key]);
                    }
                }
                foreach ($data as $key => $value) {
                    foreach ($emptySpaces as $spaces) {
                        unset($data[$key][$spaces]);
                    }
                }

                $this->view->headerData = $data[0];

                unset($data[0]);
                $this->view->valueData  = $data;
            }

            $this->view->dataAdapter = $dataAdapter;
        }
        else //kalo total record = 0
        {
            $error_msg[0] = $this->language->_('Error').': '.$this->language->_('Wrong File Formatss').'.';
            $this->view->error      = true;
            $this->view->report_msg = $this->displayError($error_msg);
        }
    }  
}
