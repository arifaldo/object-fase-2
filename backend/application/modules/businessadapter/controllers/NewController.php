<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';

require_once 'General/Customer.php';
require_once 'General/CustomerUser.php';

require_once 'SGO/Extendedmodule/PHPExcel.php';
require_once 'SGO/Extendedmodule/PHPExcel/IOFactory.php';


class businessadapter_NewController extends Application_Main {

    protected $_destinationUploadDir = '';
    protected $_maxRow = '';

    public function initController()
    {
        $this->_destinationUploadDir = UPLOAD_PATH . '/document/adapterProfile/';

        $setting = new Settings();
        $this->_maxRow = $setting->getSetting('max_import_bulk');
    }

    public function indexAction()
    { 
        $this->_helper->_layout->setLayout('newlayout');

        $selectTag = $this->_db->select()
                     ->distinct()
                     ->from(array('T' => 'M_MAPPING'), 'T.TAG');

        $dataTag = $this->_db->fetchAll($selectTag);

        $opt = '<option value="autonumber">Auto Numbering</option>
                <option value="uid">Unique ID</option>';

        foreach ($dataTag as $key => $value) {
            $opt .= '<option value="'.$value['TAG'].'_mapping">'.$value['TAG'].' Auto Mapping</option>'; 
        }

        $this->view->functionOpt = $opt;

        if($this->_request->isPost())
        {

            $param = $this->_request->getParams();

            if ($param['fixLengthSubmit']) {

                $sessionNamespace = new Zend_Session_Namespace('fixLengthData');
                $sourceFileName = $sessionNamespace->sourceFileName;
                $extension = $sessionNamespace->extension;
                $newFileName = $sessionNamespace->newFileName;
                $fixLengthType = $sessionNamespace->fixLengthType;
                $fileContents = $sessionNamespace->fileContents;

                $label = $param['label'];

                $headerOrder = null;

                // if with header
                if ($fixLengthType == 1) {
                    //karena yg diambil hanya order dri row ke 2 saja
                    $headerOrder = $param['row1'];
                    $contentOrder = $param['row2'];
                    $startArrIndex = 1;
                    $surplusIndex = 0;
                }
                else if ($fixLengthType == 3) {
                    //karena yg diambil hanya order dri row ke 1 saja
                    $contentOrder = $param['row1'];
                    $startArrIndex = 0;
                    $surplusIndex = 1;
                }

                $contentOrderArr = explode(',', $contentOrder);

                if (count($contentOrderArr) > 1) {
                    foreach ($fileContents as $key => $value) {
                        if ($key >= $startArrIndex) {
                            foreach ($contentOrderArr as $key2 => $value2) {
                                //first order, startIndex from 0
                                if ($key2 == 0) {
                                    $startIndex = 0;
                                    $endIndex = (int) $value2 + 1;

                                    $data[$key + $surplusIndex][] = substr($value,$startIndex,$endIndex);
                                }
                                else{
                                    $startIndex = (int) ($contentOrderArr[$key2 - 1] + 1);
                                    $endIndex = (int) ($value2 - ($startIndex - 1));

                                    $data[$key + $surplusIndex][] = substr($value,$startIndex,$endIndex);
                                }
                            }       
                        }   
                    }
                }

                foreach ($label as $key => $labels) {
                    $data[0][] = $labels;
                } 

                if(!empty($data))
                {   
                    $this->view->fileName = $sourceFileName; 
                    $headerData = $data[0]; 
                    $this->view->headerData = $headerData;

                    $this->view->headerCount = count($data[0]);

                    $halfHeaderCount = (int) (count($data[0]) / 2);

                    $this->view->halfHeaderCount = $halfHeaderCount;

                    unset($data[0]);
                    $this->view->valueData  = $data;
                                                     
                    $this->view->uploadFile = true;

                    $sessionNamespace = new Zend_Session_Namespace('businessAdapter');
                    $sessionNamespace->headerData = $headerData;
                    $sessionNamespace->filePath = $newFileName;
                    $sessionNamespace->extension = $extension;
                    $sessionNamespace->fixLength = true;
                    $sessionNamespace->fixLengthType = $fixLengthType;
                    $sessionNamespace->fixLengthHeader = $headerOrder;
                    $sessionNamespace->fixLengthContent = $contentOrder;
                    $sessionNamespace->delimitedWith = null;
                }
                else //kalo total record = 0
                {
                    @unlink($newFileName);

                    $error_msg[0] = $this->language->_('Error').': '.$this->language->_('Wrong File Formatss').'.';
                    $this->view->error      = true;
                    $this->view->report_msg = $this->displayError($error_msg);
                }
            }
            else{

                $adapter = new Zend_File_Transfer_Adapter_Http();

                $adapter->setDestination ( $this->_destinationUploadDir );
                $extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv', 'txt', 'json', 'xml', 'xls', 'xlsx'));
                $extensionValidator->setMessage(
                    $this->language->_('Error').': '.$this->language->_('Extension file must be').' *.csv, *.json, *.xml, *.txt, *.xls, *.xlsx'
                );

                $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
                $sizeValidator->setMessage(
                    'Error: File exceeds maximum size'
                );

                $adapter->setValidators ( array (
                    $extensionValidator,
                    $sizeValidator,
                ));

                $sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);

                $extension = explode('.', $sourceFileName);

                $extensionName = $extension[1];

                $newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

                $adapter->addFilter ( 'Rename',$newFileName  );

                if ($adapter->isValid ())
                {
                    if ($adapter->receive ())
                    {
                       $file_contents = file_get_contents($newFileName);
                       $delimitedWith = null;

                        //if csv occured
                        if ($extensionName === 'csv') {

                            //PARSING CSV HERE
                            //if advanced option
                            if ($param['advancedOption'] == 1 && $param['radType'] == 1) {
                                $delimitedWith = $param['delimitedWith'];
                                $data = $this->_helper->parser->parseCSV($newFileName, $delimitedWith);
                            }
                            //if fix length
                            else if ($param['advancedOption'] == 1 && $param['radType'] == 2) {

                                $file_contents = file($newFileName);

                                if (!empty($file_contents)) {
                                    $fixLength = true;
                                    $this->view->fixLength = true;

                                    //delete space in file
                                    foreach ($file_contents as $contents) {
                                        $newFileContents[] = str_replace(' ', '_', $contents);
                                    }
                                    $this->view->fileContents = $newFileContents;

                                    $this->view->fixLengthType = $param['fixLengthType'];

                                    $sessionNamespace = new Zend_Session_Namespace('fixLengthData');
                                    $sessionNamespace->sourceFileName = $sourceFileName;
                                    $sessionNamespace->newFileName = $newFileName;
                                    $sessionNamespace->extension = $extensionName;
                                    $sessionNamespace->fixLengthType = $param['fixLengthType'];
                                    $sessionNamespace->fileContents = $file_contents;
                                    $sessionNamespace->startRow = $param['startRow'];
                                        $sessionNamespace->startColomn = $param['startColomn'];
                                }
                                else{
                                    $error_msg[0] = $this->language->_('Error').': '.$this->language->_('File are empty').'.';
                                    $this->view->error      = true;
                                    $this->view->report_msg = $this->displayError($error_msg);
                                }                                
                            }
                            else{
                                $data = $this->_helper->parser->parseCSV($newFileName);
                            }
                        }
                        //if txt occured
                        else if ($extensionName === 'txt') {

                           $lines = file($newFileName);

                           $checkMt940 = false;
                           $checkMt101 = false;
                           foreach ($lines as $line) {
                               if (strpos($line, '{1:') !== false) {
                                   $checkMt940 = true;
                               }
                               else if (strpos($line, ':20:') !== false) {
                                   $checkMt101 = true;
                               }
                           }

                           //if mt940 format
                           if ($checkMt940) {
                               
                                $data = $this->_helper->parser->mt940($newFileName);
                                $mtFile = true;
                           }
                           else if($checkMt101){
                                $data = $this->_helper->parser->mt101($newFileName);
                                $mtFile = true;
                           }
                           else{

                                if ($param['advancedOption'] == 1 && $param['radType'] == 1) {
                                    $delimitedWith = $param['delimitedWith'];
                                    //parse csv jg bisa utk txt
                                    $data = $this->_helper->parser->parseCSV($newFileName, $delimitedWith);
                                }
                                //if fix length
                                else if ($param['advancedOption'] == 1 && $param['radType'] == 2) {

                                    if (!empty($lines)) {
                                        $fixLength = true;
                                        $this->view->fixLength = true;

                                        //delete space in file
                                        foreach ($lines as $contents) {
                                            $newFileContents[] = str_replace(' ', '_', $contents);
                                        }
                                        $this->view->fileContents = $newFileContents;

                                        $this->view->fixLengthType = $param['fixLengthType'];

                                        $sessionNamespace = new Zend_Session_Namespace('fixLengthData');
                                        $sessionNamespace->sourceFileName = $sourceFileName;
                                        $sessionNamespace->newFileName = $newFileName;
                                        $sessionNamespace->extension = $extensionName;
                                        $sessionNamespace->fixLengthType = $param['fixLengthType'];
                                        $sessionNamespace->fileContents = $file_contents;
                                        $sessionNamespace->startRow = $param['startRow'];
                                        $sessionNamespace->startColomn = $param['startColomn'];
                                        
                                    }
                                    else{
                                        $error_msg[0] = $this->language->_('Error').': '.$this->language->_('File are empty').'.';
                                        $this->view->error      = true;
                                        $this->view->report_msg = $this->displayError($error_msg);
                                    }                                
                                }
                                else{
                                    //parse csv jg bisa utk txt, defaultnya koma
                                    $data = $this->_helper->parser->parseCSV($newFileName, ',');
                                }
                           }
                        }
                        //if json occured
                        else if ($extensionName === 'json') {

                            $datajson = json_decode($file_contents, 1);
                            $i = 0;
                            foreach ($datajson as $key => $value) {
                                if ($i == 0) {
                                    $data[$i] = array_keys($value);
                                    $data[$i + 1] = array_values($value);
                                }
                                else{
                                    $data[$i+1] = array_values($value);
                                }

                                $i++;
                            }
                        }
                        //if xml occured
                        else if($extensionName === 'xml'){

                            $file_contents = file_get_contents($newFileName);

                            $xml = (array) simplexml_load_string($file_contents);

                             $i = 0;
                            foreach ($xml as $key => $value) {
                                foreach ($value as $key2 => $value2) {
                                    if ($i == 0) {
                                        $data[$i] = array_keys((array)$value2);
                                        $data[$i + 1] = array_values((array)$value2);
                                    }
                                    else{
                                        $data[$i+1] = array_values((array)$value2);
                                    }

                                    $i++;
                                }
                            }

                            for($i=0; $i<count($data); $i++){
                                if ($i > 0) {
                                    foreach ($data[$i] as $key => $value) {
                                        if (empty($value)) {
                                            $data[$i][$key] = null;
                                        }
                                    }
                                }
                            }
                        }
                        else if($extensionName === 'xls' || $extensionName === 'xlsx'){
                            try {
                                $inputFileType = IOFactory::identify($newFileName);
                                $objReader = IOFactory::createReader($inputFileType);
                                $objPHPExcel = $objReader->load($newFileName);
                            } catch(Exception $e) {
                                die('Error loading file "'.pathinfo($newFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
                            }

                            $sheet = $objPHPExcel->getSheet(0);
                            $highestRow = $sheet->getHighestRow();
                            $highestColumn = $sheet->getHighestColumn();
                             
                            for ($row = 1; $row <= $highestRow; $row++){                        
                                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, null, true, false, false);

                                $data[$row - 1] = $rowData[0];
                            }
                        }

                        if(!empty($data))
                        {   

                            if ($mtFile) {
                                $newData[0] = array_keys($data[0]);

                                $i = 1;
                                foreach ($data as $key => $value) {
                                    foreach ($value as $key2 => $value2) {
                                        $newData[$i][] = $value2;
                                    }
                                    $i++;
                                }
                                unset($data);
                                $data = $newData;
                            }

                            //if ($param['advancedOption'] == 1 && $param['checkStartRow'] == 1) {
                            if ($param['advancedOption'] == 1 && $param['startRow'] ) {
                                //echo 'param = '.$param['startRow'];
                                if (empty($param['startRow'])) {
                                    $startFrom = 0;   
                                }
                                else{   
                                    for($i=0; $i<$param['startRow']-1; $i++){
                                        unset($data[$i]);
                                    }
                                }
                                $startFrom = $param['startRow']-1;
                            }
                            else{
                                $startFrom = 0;
                            }


                             //---------get rid of empty spaces---------
                            $emptySpaces = array();
                            foreach ($data[$startFrom] as $key => $value) {
                                if (empty($value)) {
                                    array_push($emptySpaces, $key);
                                    unset($data[$startFrom][$key]);
                                }
                            }
                            foreach ($data as $key => $value) {
                                foreach ($emptySpaces as $spaces) {
                                    unset($data[$key][$spaces]);
                                }
                            }


                            $this->view->fileName = $sourceFileName;
                            $headerData = $data[$startFrom];  
                            $this->view->headerData = $headerData;

                            $this->view->headerCount = count($data[$startFrom]);

                            $halfHeaderCount = (int) (count($data[$startFrom]) / 2);

                            $this->view->halfHeaderCount = $halfHeaderCount;

                            unset($data[$startFrom]);
                            $this->view->valueData  = $data;
                                                             
                            $this->view->uploadFile = true;

                            $sessionNamespace = new Zend_Session_Namespace('businessAdapter');
                            $sessionNamespace->headerData = $headerData;
                            $sessionNamespace->filePath = $newFileName;
                            $sessionNamespace->extension = $extensionName;
                            $sessionNamespace->fixLength = false;
                            $sessionNamespace->delimitedWith = $delimitedWith;
                            $sessionNamespace->startRow = $param['startRow'];
                            $sessionNamespace->startColomn = $param['startColomn'];
                        }
                        else //kalo total record = 0
                        {
                            if (!$fixLength) {
                                @unlink($newFileName);   

                                $error_msg[0] = $this->language->_('Error').': '.$this->language->_('Wrong File Formatss').'.';
                                $this->view->error      = true;
                                $this->view->report_msg = $this->displayError($error_msg);
                            }
                        }
                    }
                }
                else
                {
                    $this->view->error = true;
                    foreach($adapter->getMessages() as $key=>$val)
                    {
                        if($key=='fileUploadErrorNoFile')
                            $error_msg[0] = $this->language->_('Error').': '.$this->language->_('File cannot be left blank. Please correct it').'.';
                        else
                            $error_msg[0] = $val;
                        break;
                    }
                    $errors = $this->displayError($error_msg);
                    $this->view->report_msg = $errors;
                }   

            }
        }
        $sessionNamespace = new Zend_Session_Namespace('businessAdapter');
        $startRow = $sessionNamespace->startRow;
        $startColomn = $sessionNamespace->startColomn;
         
        $this->view->startRow = $startRow;
        $this->view->startColomn = $startColomn;
        
        
    }  

    public function confirmAction(){

        $sessionNamespace = new Zend_Session_Namespace('businessAdapter');
        $extension = $sessionNamespace->extension;
        $headerData = $sessionNamespace->headerData;
        $filePath = $sessionNamespace->filePath;
        $fixLength = $sessionNamespace->fixLength;
        $fixLengthType = $sessionNamespace->fixLengthType;
        $fixLengthHeader = $sessionNamespace->fixLengthHeader;
        $fixLengthContent = $sessionNamespace->fixLengthContent;
        $delimitedWith = $sessionNamespace->delimitedWith;
        $startRow = $sessionNamespace->startRow;
        $startColomn = $sessionNamespace->startColomn;


        $params = $this->_request->getParams();

        //start m_adapter_profile
        $filter = array(
           'profileName' => array('StripTags','StringTrim'),
           'fileType' => array('StripTags','StringTrim')
        );
        
        $options = array('allowEmpty' => FALSE);
        
        $validators = array(
            'profileName'   => array(),
            'fileType'      => array()
        );

        $headerNames = null;
        if ($fixLength) {
            $headerNames = implode(',',$headerData);
        }

        $filter  = new Zend_Filter_Input($filter, $validators, $params, $options);

        if ($filter->isValid()){

            $this->_db->beginTransaction();
            try{
                if(empty($startRow)){
                    $startRow = 1;
                }
                if(empty($startColomn)){
                    $startColomn = 0;
                }
                
				
				$info = $this->language->_('businessadapter');
				$info2 = $this->language->_('Business Profile Adapter');
				$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['new'],null,'M_ADAPTER_PROFILE','TEMP_ADAPTER_PROFILE',$filter->getEscaped('profileName'),'','');
										
                $tableName = 'TEMP_ADAPTER_PROFILE';
                $arrData = array(
					'CHANGES_ID'	=> $change_id,
                    'PROFILE_NAME' => $filter->getEscaped('profileName'),
                    'FILE_FORMAT' => $extension,
                    'SAVE_AS_FORMAT' => $filter->getEscaped('fileType'),
                    // 'HEADER_ORDER' => $filter->getEscaped('headerOrder'),
                    'SUGGESTED_BY' => $this->_userIdLogin,
                    'SUGGESTED_DATE' => date('Y-m-d H:i:s'),
                    'FILE_PATH' => $filePath,
                    'FIXLENGTH' => $fixLength,
                    'FIXLENGTH_TYPE' => $fixLengthType,
                    'FIXLENGTH_HEADER_ORDER' => $fixLengthHeader,
                    'FIXLENGTH_CONTENT_ORDER' => $fixLengthContent,
                    'FIXLENGTH_HEADER_NAME' => $headerNames,
                    'DELIMITED_WITH' => $delimitedWith,
                    'ROW_START'     => $startRow,
                    'COLUMN_START'      => $startColomn,
                    'TEMPLATE_TYPE'     => 1
                );

                $this->_db->insert($tableName, $arrData);

                $lastId = $this->_db->fetchOne('select @@identity');
                
                $this->_db->commit();

            }catch(Exception $e){
                $success = false;
                $this->_db->rollBack();
                print_r($e);die();
                $error[] = $e->getMessage();
            }

             //finish m_adapter profile


            //start m_adapter_profile_detail

            $label = $params['label'];
            $content = $params['content'];
            $function = $params['function'];
            $chkMandatory = $params['chkMandatory'];

            $i = 0;
            foreach ($label as $key => $value) {

                $mandatory = 0;
                if (!empty($chkMandatory[$key])) {
                    $mandatory = 1;
                }

                $functions = null;
                if (!empty($function[$key])) {
                    $functions = $function[$key];
                }

                $data2 = array(
				    'CHANGES_ID'	=> $change_id,
                    'PROFILE_ID' => $lastId,
                    'HEADER_NAME' => $label[$key],
                    'HEADER_INDEX' => $i,
                    'MANDATORY' => $mandatory,
                    'HEADER_FUNCTION' => $functions,
                    'HEADER_CONTENT' => $content[$key]
                );

                $this->_db->beginTransaction();

                try{
                    $tableName = 'TEMP_ADAPTER_PROFILE_DETAIL';
                    $this->_db->insert($tableName, $data2);
                    
                    $this->_db->commit();

                    $success = true;

                }catch(Exception $e){
                    $success = false;
                    $this->_db->rollBack();
                    print_r($e);die();
                    $error[] = $e->getMessage();
                }
                $i++;
            }

            if ($success) {
                Zend_Session::namespaceUnset('businessAdapter');
                Zend_Session::namespaceUnset('fixLengthData');
                $this->setbackURL('/'.$this->_request->getModuleName().'/index/');
                $this->_redirect('/notification/success/index');
            }
            else{
                $this->_db->rollBack();
                $this->view->error = true;
                $error_msg = $error;
                $errors = $this->displayError($error_msg);
                $this->view->report_msg = $errors;

            } 
        }else{
            $error = $filter->getMessages();

            $this->view->error = true;
            $error_msg = $error;
            $errors = $this->displayError($error_msg);
            $this->view->report_msg = $errors;
        }

        // $headerCounter = $params['headerCounter'];

        // for($i=0; $i<=$headerCounter; $i++){
 
        //     $mandatory = 0;
        //     if (!empty($params['chkMandatory'.$i])) {
        //         $mandatory = 1;
        //     }

        //     $function = null;
        //     if (!empty($params['headerFunction'.$i])) {
        //         $function = $params['headerFunction'.$i];
        //     }

        //     $data2 = array(
        //         'PROFILE_ID' => $lastId,
        //         'HEADER_NAME' => $params['headerName'.$i],
        //         'HEADER_INDEX' => $params['headerIndex'.$i],
        //         'MANDATORY' => $mandatory,
        //         'HEADER_FUNCTION' => $function,
        //         'HEADER_CONTENT' => $params['headerContent'.$i]
        //     );

        //     $this->_db->beginTransaction();

        //     try{
        //         $tableName = 'M_ADAPTER_PROFILE_DETAIL';
        //         $this->_db->insert($tableName, $data2);
                
        //         $this->_db->commit();

        //         $success = true;

        //     }catch(Exception $e){
        //         $success = false;
        //         $this->_db->rollBack();
        //         print_r($e);die();
        //         $error[] = $e->getMessage();
        //     }
        // }  
    }
}
