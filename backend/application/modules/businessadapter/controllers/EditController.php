<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';

require_once 'General/Customer.php';
require_once 'General/CustomerUser.php';

require_once 'SGO/Extendedmodule/PHPExcel.php';
require_once 'SGO/Extendedmodule/PHPExcel/IOFactory.php';

class businessadapter_EditController extends Application_Main {

    public function indexAction()
    { 
        $this->_helper->_layout->setLayout('newlayout');

        $profileid = $this->_request->getParam('profileid');

        $selectData = $this->_db->select()
                     ->from(array('H' => 'M_ADAPTER_PROFILE'), '*')
                     ->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'H.PROFILE_ID = D.PROFILE_ID', '*')
                     ->where('H.PROFILE_ID = '.$this->_db->quote($profileid));

        $dataAdapter = $this->_db->fetchAll($selectData);

        $selectTag = $this->_db->select()
                     ->distinct()
                     ->from(array('T' => 'M_MAPPING'), 'T.TAG');

        $dataTag = $this->_db->fetchAll($selectTag);

        $opt = '<option value="autonumber">Auto Numbering</option>
                <option value="uid">Unique ID</option>';

        foreach ($dataTag as $key => $value) {
            $opt .= '<option value="'.$value['TAG'].'_mapping">'.$value['TAG'].' Auto Mapping</option>'; 
        }

        $this->view->functionOpt = $opt;

        $fileName = $dataAdapter[0]['FILE_PATH'];
        $fixLength = $dataAdapter[0]['FIXLENGTH'];
        $fixLengthType = $dataAdapter[0]['FIXLENGTH_TYPE'];
        $fixLengthHeader = $dataAdapter[0]['FIXLENGTH_HEADER_ORDER'];
        $fixLengthHeaderName = $dataAdapter[0]['FIXLENGTH_HEADER_NAME'];
        $fixLengthContent = $dataAdapter[0]['FIXLENGTH_CONTENT_ORDER'];
        $delimitedWith = $dataAdapter[0]['DELIMITED_WITH'];
        $templateType = $dataAdapter[0]['TEMPLATE_TYPE'];
        

        $file_contents = file_get_contents($fileName);


        if (strpos($fileName, '.csv') !== false) {
            $extensionName = 'csv';
        }
        else if (strpos($fileName, '.json') !== false) {
            $extensionName = 'json';
        }
        else if (strpos($fileName, '.xml') !== false) {
            $extensionName = 'xml';
        }
        else if (strpos($fileName, '.txt') !== false) {
            $extensionName = 'txt';
        }
        else if (strpos($fileName, '.xls') !== false) {
            $extensionName = 'xls';
        }
        else if (strpos($fileName, '.xlsx') !== false) {
            $extensionName = 'xlsx';
        }
        
        //if csv occured
        if ($extensionName === 'csv') {

            if (!empty($delimitedWith)) {
                $data = $this->_helper->parser->parseCSV($fileName, $delimitedWith);
            }
            //if fix length
            else if ($fixLength == 1) {

                $data = file($fileName); 
            }
            else{
                $data = $this->_helper->parser->parseCSV($fileName);
            }
        }
        //if txt occured
        else if ($extensionName === 'txt') {

           $lines = file($fileName);

           $checkMt940 = false;
           $checkMt101 = false;
           foreach ($lines as $line) {
               if (strpos($line, '{1:') !== false) {
                   $checkMt940 = true;
               }
               else if (strpos($line, ':20:') !== false) {
                   $checkMt101 = true;
               }
           }

           //if mt940 format
           if ($checkMt940) {
               
                $data = $this->_helper->parser->mt940($fileName);
                $mtFile = true;
           }
           else if($checkMt101){
                $data = $this->_helper->parser->mt101($fileName);
                $mtFile = true;
           }
           else{

                if (!empty($delimitedWith)) {
                    //parse csv jg bisa utk txt
                    $data = $this->_helper->parser->parseCSV($fileName, $delimitedWith);
                }
                //if fix length
                else if ($fixLength == 1) {

                    $data = file($fileName);                   
                }
                else{
                    //parse csv jg bisa utk txt, defaultnya pakai koma
                    $data = $this->_helper->parser->parseCSV($fileName, ',');
                }
           }
        }
        //if json occured
        else if ($extensionName === 'json') {

            $datajson = json_decode($file_contents, 1);
            $i = 0;
            foreach ($datajson as $key => $value) {
                if ($i == 0) {
                    $data[$i] = array_keys($value);
                    $data[$i + 1] = array_values($value);
                }
                else{
                    $data[$i+1] = array_values($value);
                }

                $i++;
            }
        }
        //if xml occured
        else if($extensionName === 'xml'){

            $xml = (array) simplexml_load_string($file_contents);

             $i = 0;
            foreach ($xml as $key => $value) {
                foreach ($value as $key2 => $value2) {
                    if ($i == 0) {
                        $data[$i] = array_keys((array)$value2);
                        $data[$i + 1] = array_values((array)$value2);
                    }
                    else{
                        $data[$i+1] = array_values((array)$value2);
                    }

                    $i++;
                }
            }

            for($i=0; $i<count($data); $i++){
                if ($i > 0) {
                    foreach ($data[$i] as $key => $value) {
                        if (empty($value)) {
                            $data[$i][$key] = null;
                        }
                    }
                }
            }
        }
        else if($extensionName === 'xls' || $extensionName === 'xlsx'){
            try {
                $inputFileType = IOFactory::identify($fileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($fileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($fileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
             
            for ($row = 1; $row <= $highestRow; $row++){                        
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row);

                $data[$row - 1] = $rowData[0];
            }
        }

        if(!empty($data))
        {    
            $this->view->profileId = $profileid;

            if ($fixLength == 1) {   

                $fixLengthHeaderNameArr = explode(',', $fixLengthHeaderName);

                if (count($fixLengthHeaderNameArr) != 0) {

                    foreach ($fixLengthHeaderNameArr as $key => $value) {
                        //first order, startIndex from 0
                       $newdata[0][] = $value;
                    }
                }
                else{
                    $newdata[0][] = $fixLengthHeaderName;
                }

                $startArrIndex = 1;

                 // if with header
                if ($fixLengthType == 1) {
                    $surplusIndex = 0;
                }
                else if ($fixLengthType == 3) {
                    $surplusIndex = 1;
                }

                $contentOrderArr = explode(',', $fixLengthContent);

                if (count($contentOrderArr) > 1) {
                    foreach ($data as $key => $value) {
                        if ($key >= $startArrIndex) {
                            foreach ($contentOrderArr as $key2 => $value2) {
                                //first order, startIndex from 0
                                if ($key2 == 0) {
                                    $startIndex = 0;
                                    $endIndex = (int) $value2 + 1;

                                    $newdata[$key + $surplusIndex][] = substr($value,$startIndex,$endIndex);
                                }
                                else{
                                    $startIndex = (int) ($contentOrderArr[$key2 - 1] + 1);
                                    $endIndex = (int) ($value2 - ($startIndex - 1));

                                    $newdata[$key + $surplusIndex][] = substr($value,$startIndex,$endIndex);
                                }
                            }       
                        }   
                    }
                }

                //---------get rid of empty spaces---------
                $emptySpaces = array();
                foreach ($newdata[0] as $key => $value) {
                    if (empty($value)) {
                        array_push($emptySpaces, $key);
                        unset($newdata[0][$key]);
                    }
                }
                foreach ($newdata as $key => $value) {
                    foreach ($emptySpaces as $spaces) {
                        unset($newdata[$key][$spaces]);
                    }
                }
				
				
                $this->view->headerData = $newdata[0];

                $this->view->headerCount = count($newdata[0]);

                $halfHeaderCount = (int) (count($newdata[0]) / 2);

                $this->view->halfHeaderCount = $halfHeaderCount;

                unset($newdata[0]);
                $this->view->valueData  = $newdata;
            }
            else{

                 //---------get rid of empty spaces---------
                $emptySpaces = array();
                foreach ($data[0] as $key => $value) {
                    if (empty($value)) {
                        array_push($emptySpaces, $key);
                        unset($data[0][$key]);
                    }
                }
                foreach ($data as $key => $value) {
                    foreach ($emptySpaces as $spaces) {
                        unset($data[$key][$spaces]);
                    }
                }

				//echo '<pre>';
				//var_dump($data[0]);die;
                $this->view->headerData = $data[0];

                $this->view->headerCount = count($data[0]);

                $halfHeaderCount = (int) (count($data[0]) / 2);

                $this->view->halfHeaderCount = $halfHeaderCount;

                unset($data[0]);
                $this->view->valueData  = $data;
            }

            $this->view->dataAdapter = $dataAdapter;
        }
        else //kalo total record = 0
        {
            $error_msg[0] = $this->language->_('Error').': '.$this->language->_('Wrong File Formatss').'.';
            $this->view->error      = true;
            $this->view->report_msg = $this->displayError($error_msg);
        }

        if($this->_request->isPost())
        {
            $params = $this->_request->getParams();

            $profileid = $params['profileId'];

            //start m_adapter_profile
            $filter = array(
               'profileName' => array('StripTags','StringTrim'),
               'fileType' => array('StripTags','StringTrim'),
               // 'headerOrder' => array('StripTags','StringTrim'),
            );
            
            $options = array('allowEmpty' => FALSE);
            
            $validators = array(
                'profileName'   => array(),
                'fileType'      => array(),
                // 'headerOrder'   => array()
            );

            $filter  = new Zend_Filter_Input($filter, $validators, $params, $options);

            if ($filter->isValid()){

                $this->_db->beginTransaction();
				$info = $this->language->_('businessadapter');
				$info2 = $this->language->_('Business Profile Adapter');
				$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['edit'],null,'M_ADAPTER_PROFILE','TEMP_ADAPTER_PROFILE',$filter->getEscaped('profileName'),'','');
				
				$selectData = $this->_db->select()
                     ->from(array('H' => 'M_ADAPTER_PROFILE'), '*')
                    
                     ->where('H.PROFILE_ID = '.$this->_db->quote($profileid));

				$dataAdapter = $this->_db->fetchAll($selectData);
				
                try{
                    $tableName = 'TEMP_ADAPTER_PROFILE';
					$arrData = $dataAdapter[0];
                    $arrData['CHANGES_ID'] = $change_id;
					$arrData['PROFILE_NAME'] = $filter->getEscaped('profileName');
					$arrData['FILE_FORMAT'] = $filter->getEscaped('fileType');
					$arrData['SUGGESTED_BY'] = $this->_userIdLogin;
					$arrData['SUGGESTED_DATE'] = date('Y-m-d H:i:s');
					$arrData['TEMPLATE_TYPE'] = $params['templatetemp'];
					$arrData['PROFILE_ID'] = $profileid;
					

					
                    $this->_db->insert($tableName,$arrData);
                    
                    $this->_db->commit();

                }catch(Exception $e){
                    $success = false;
                    $this->_db->rollBack();
                    print_r($e);die();
                    $error[] = $e->getMessage();
                }

                //finish m_adapter profile

                //start m_adapter_profile_detail

                //delete the existing data first & insert the new one
              //  $this->_db->delete('T_ADAPTER_PROFILE_DETAIL','PROFILE_ID = '.$this->_db->quote($profileid));

                $label = $params['label'];
                $content = $params['content'];
                $function = $params['function'];
                $chkMandatory = $params['chkMandatory'];

                $i = 0;
                foreach ($label as $key => $value) {

                    $mandatory = 0;
                    if (!empty($chkMandatory[$key])) {
                        $mandatory = 1;
                    }

                    $functions = null;
                    if (!empty($function[$key])) {
                        $functions = $function[$key];
                    }

                    $data2 = array(
						'CHANGES_ID'	=> $change_id,
                        'PROFILE_ID' => $profileid,
                        'HEADER_NAME' => $label[$key],
                        'HEADER_INDEX' => $i,
                        'MANDATORY' => $mandatory,
                        'HEADER_FUNCTION' => $functions,
                        'HEADER_CONTENT' => $content[$key]
                    );

                    $this->_db->beginTransaction();

                    try{
                        $tableName = 'TEMP_ADAPTER_PROFILE_DETAIL';
                        $this->_db->insert($tableName, $data2);
                        
                        $this->_db->commit();

                        $success = true;
 
                    }catch(Exception $e){
                        $success = false;
                        $this->_db->rollBack();
                        print_r($e);die();
                        $error[] = $e->getMessage();
                    }
                    $i++;
                }

                if ($success) {
                    $this->setbackURL('/'.$this->_request->getModuleName().'/index/');
                    $this->_redirect('/notification/success/index');
                }
                else{
                    $this->_db->rollBack();
                    $this->view->error = true;
                    $error_msg = $error;
                    $errors = $this->displayError($error_msg);
                    $this->view->report_msg = $errors;

                }
            }else{
                $error = $filter->getMessages();
                $this->view->error = true;
                $error_msg = $error;
                $errors = $this->displayError($error_msg);
                $this->view->report_msg = $errors;
            }      
        }
    }  
}
