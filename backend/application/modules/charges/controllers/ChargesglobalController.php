<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Settings.php';

class charges_ChargesglobalController extends Application_Main{

	public function indexAction()
	{

        $setting= new Settings();
		$this->_helper->layout()->setLayout('newlayout');
		$settingValue = $setting->getSetting('autocharges_isenabled');
		//Zend_Debug::dump($settingValue); die;
		$value = $this->_getParam('s');
		$save = $this->_getParam('m');
		$system_type = $setting->getSetting('system_type');
		if($system_type == '2' || $system_type == '3'){
		$this->view->showdiv = true;
		}else{ 
		$this->view->showdiv = false;	
		}
		
		$cekmonthlyglobal = $this->_db->select()
					        ->from(array('A' => 'TEMP_CHARGES_MONTHLY'),array('*'));
		$cekmonthlyglobal ->where("A.CUST_ID = 'GLOBAL'");
		$cektempmonthlyglobal = $this->_db->fetchAll($cekmonthlyglobal);
		if(!empty($cektempmonthlyglobal)){
			$docErr = "*".$this->language->_('No changes allowed for this record while awaiting approval for previous change')."";
			$this->view->errormnth = $docErr;
			$this->view->changestatusmnth = "disabled";
		}
		
		
		$cekmonthlyremit = $this->_db->select()
					        ->from(array('A' => 'TEMP_CHARGES_REMITTANCE'),array('*'));
		$cekmonthlyremit ->where("A.CUST_ID = 'GLOBAL'");
		$cektempmonthlyremit = $this->_db->fetchAll($cekmonthlyremit);
		if(!empty($cektempmonthlyremit)){
			$docErr = "*".$this->language->_('No changes allowed for this record while awaiting approval for previous change')."";
			$this->view->errorremit = $docErr;
			$this->view->changestatusremit = "disabled";
		}
		
		$selectmonthlyglobal = $this->_db->select()
					        ->from(array('A' => 'M_CHARGES_MONTHLY'),array('*'));
		$selectmonthlyglobal ->where("A.CUST_ID = 'GLOBAL'");
		$monthlyglobal = $this->_db->fetchAll($selectmonthlyglobal);
		 
		$this->view->monthlyglobal = $monthlyglobal;
		
		$pstypeArr = array(1 => 'RTGS',2 => 'SKN',8 => 'Domestic Online',0 => 'Inhouse');
		
		$this->view->pstype = $pstypeArr;
		
		$selectpbglobal = $this->_db->select()
					        ->from(array('A' => 'M_CHARGES_WITHIN'),array('*'));
		$selectpbglobal ->where("A.CUST_ID = 'GLOBAL'");
		$pbglobal = $this->_db->fetchAll($selectpbglobal);
		 if(!empty($pbglobal)){
			 $pbamount = $pbglobal['0']['AMOUNT'];
		 }else{
			 $pbamount = 0;
		 }
		 //var_dump($pbamount);
		$this->view->pbglobal = $pbamount;
		
		$selectdomglobal = $this->_db->select()
					        ->from(array('A' => 'M_CHARGES_OTHER'),array('*'));
		$selectdomglobal ->where("A.CUST_ID = 'GLOBAL'");
		$domglobal = $this->_db->fetchAll($selectdomglobal);
		 
		$this->view->domglobal = $domglobal;
		
		$cekpbglobal = $this->_db->select()
					        ->from(array('A' => 'TEMP_CHARGES_WITHIN'),array('*'));
		$cekpbglobal ->where("A.CUST_ID = 'GLOBAL'");
		$cektemppbglobal = $this->_db->fetchAll($cekpbglobal);
		 
		$cekothglobal = $this->_db->select()
					        ->from(array('A' => 'TEMP_CHARGES_OTHER'),array('*'));
		$cekothglobal ->where("A.CUST_ID = 'GLOBAL'");
		$cektempotherglobal = $this->_db->fetchAll($cekothglobal);
	if(!empty($cektemppbglobal) || !empty($cektempotherglobal) ){
			$docErr = "*".$this->language->_('No changes allowed for this record while awaiting approval for previous change')."";
			$this->view->errorpb = $docErr;
			$this->view->changestatuspb = "disabled";
		}
 
		$selectglobal = $this->_db->select()
					        ->from(array('A' => 'M_CHARGES_API'),array('*'));
		$selectglobal ->where("A.CUST_ID = 'GLOBAL'");
		$arrglobal = $this->_db->fetchAll($selectglobal);
		$this->view->balance_inquiry = $arrglobal[0]['BALANCE_INQUIRY'];
	        	$this->view->account_statement = $arrglobal[0]['ACCOUNT_STATEMENT'];
	        	$this->view->inhouse_transfer = $arrglobal[0]['INHOUSE_TRANSFER'];
	        	$this->view->skn_transfer = $arrglobal[0]['SKN_TRANSFER'];
	        	$this->view->rtgs_transfer = $arrglobal[0]['RTGS_TRANSFER'];
	        	$this->view->online_transfer = $arrglobal[0]['ONLINE_TRANSFER'];
	        	$this->view->inhouse_benef_inquiry = $arrglobal[0]['INHOUSE_BENEF_INQUIRY'];
	        	$this->view->inter_benef_inquiry = $arrglobal[0]['INTERBANK_BENEF_INQUIRY'];
	        	$this->view->kurs_rate = $arrglobal[0]['KURS_RATE_INQUIRY'];
		$this->view->globalcharge = $arrglobal;

		if(isset($value))
		{
			if($value == "1")
			{
				$settingValue = "0";
			}
			else
			{
				$settingValue = "1";
			}
			$setting->setSetting('autocharges_isenabled',$settingValue);
		}
			$this->view->value = $settingValue;
			$this->view->save = $save;
		
		
		$cust_id = 'GLOBAL';
		$this->view->custid = $cust_id;
		
		$selectccy = $this->_db->select()
             ->from(array('A' => 'M_MINAMT_CCY'), array('CCY_ID'));
            
	    $listccy = $selectccy->query()->fetchAll();
		$this->view->listccy = $listccy;
//var_dump($listccy);die;		
		
			
			
		$sqlQueryHoliday = "
			SELECT 
				CHARGE_ID,
				CCY_ID,
				CHARGE_AMT,
				CHARGE_AMOUNT_CCY,
				CHARGE_PROV_MIN_AMT,
				CHARGE_TYPE,
				CHARGE_PROV_MAX_AMT,
				CHARGE_PCT 
			FROM `M_MINAMT_CCY` A
			LEFT JOIN
			(SELECT * 
				FROM M_CHARGES_REMITTANCE  
				WHERE CHARGE_TYPE = ?
				AND CUST_ID = ?) B
			ON A.`CCY_ID` = B.`CHARGE_CCY`";
			//ORDER BY CHARGE_ID DESC";
		
		   $transfee = $this->_db->fetchAll($sqlQueryHoliday,array('3',$cust_id));

		   $fullfee = $this->_db->fetchAll($sqlQueryHoliday,array('4',$cust_id));

			$prfee = $this->_db->fetchAll($sqlQueryHoliday,array('5',$cust_id));

			foreach($prfee as $key=>$value)
			{
				// if(!empty($key)){
					$keyw = 'prccy'.$value['CCY_ID'];
					$this->view->$keyw = $value['CHARGE_AMOUNT_CCY'];

					$keymin = 'prmin'.$value['CCY_ID'];
					// print_r($keyamt);
					// echo "<br>";
					$this->view->$keymin = $value['CHARGE_PROV_MIN_AMT'];

					$keymax = 'prmax'.$value['CCY_ID'];
					// print_r($keyamt);
					// echo "<br>";
					$this->view->$keymax = $value['CHARGE_PROV_MAX_AMT'];

					$keypct = 'prpct'.$value['CCY_ID'];
					// print_r($keyamt);
					// echo "<br>";
					$this->view->$keypct = $value['CHARGE_PCT'];
				// }
			}

		   foreach($fullfee as $key=>$value)
			{
				// if(!empty($key)){
					$keyw = 'faccy'.$value['CCY_ID'];
					$this->view->$keyw = $value['CHARGE_AMOUNT_CCY'];

					$keyamt = 'faamt'.$value['CCY_ID'];
					// print_r($keyamt);
					// echo "<br>";
					$this->view->$keyamt = $value['CHARGE_AMT'];
				// }
			}
		   // echo "<pre>";
		   // print_r($transfee);
		   foreach($transfee as $key=>$value)
			{
				// if(!empty($key)){
					$keyw = 'tfccy'.$value['CCY_ID'];
					$this->view->$keyw = $value['CHARGE_AMOUNT_CCY'];

					$keyamt = 'tfamt'.$value['CCY_ID'];
					// print_r($keyamt);
					// echo "<br>";
					$this->view->$keyamt = $value['CHARGE_AMT'];
				// }
			}

		   $this->view->transfee = $transfee;


	}



}
