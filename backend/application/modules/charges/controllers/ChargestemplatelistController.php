<?php


require_once 'Zend/Controller/Action.php';


class charges_ChargestemplatelistController extends Application_Main
{
	public function indexAction() 
	{
	    
		$fields = array	(
		                    'TemplateName'         => array     ( 'field' => 'TEMPLATE_NAME',
							                                       'label' => 'Template Name',
																   'sortable' => true
							                                      ),
							'CreateDate'  			=> array	(
																	'field' => 'CREATED',
																	'label' => 'Create Date',
																	'sortable' => true
																),
							'Creator'  			=> array	(
																	'field' => 'CREATEDBY',
																	'label' => 'Creator',
																	'sortable' => true
																),
							'LatestSuggestion'  			=> array	(
																	'field' => 'SUGGESTED',
																	'label' => 'Latest Suggestion',
																	'sortable' => true
																),
							'LatestSuggestor'  			=> array	(
																	'field' => 'SUGGESTEDBY',
																	'label' => 'Latest Suggestor',
																	'sortable' => true
																),
							'Type'  			=> array	(
																	'field' => 'CHARGES_TYPE',
																	'label' => 'Type',
																	'sortable' => false
																),
						);
						
		//get page, sortby, sortdir
        $page    = $this->_getParam('page');		
        $sortBy  = $this->_getParam('sortby');
        $sortDir = $this->_getParam('sortdir');
		$service = $this->_getParam('service');
	    if ($service)
		{
		    //echo "redirect";
			$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/new');
			die();
		}
		
		//validate parameters before passing to view and query
        $page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
        	array(array_keys($fields))
            ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
			
		$sortDir = (Zend_Validate::is($sortDir,'InArray',
        	array('haystack'=>array('asc','desc'))
            ))? $sortDir : 'asc';
			
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;	
		
		$this->view->fields = $fields;
		
		//get filtering param
        $filters = array('filter' 	   	=>  array('StringTrim', 'StripTags'),
                         'TemplateName2'      =>  array('StringTrim', 'StripTags', 'StringToUpper'),
        				 'Type'     	=>  array('StringTrim', 'StripTags', 'StringToUpper'),

                        );
        
        $validators = array(
											'filter'	  => array('allowEmpty' => true),
											'TemplateName2'	  => array('allowEmpty' => true),
											'Type'		  => array('allowEmpty' => true), 
										);
		
		$zf_filter = new Zend_Filter_Input($filters, $validators, $this->getRequest()->getParams(), $this->_optionsValidator);
        $filter 	= $zf_filter->getEscaped('filter');
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');
		$templateName2   = html_entity_decode(($zf_filter->TemplateName2)  ? $zf_filter->TemplateName2   : strtoupper($this->_request->getParam('TemplateName2')));
		
		
		
		//data
		$mData = $this->_db->select()
			->from('M_TEMPLATE_CHARGES_OTHER', array('*'));
			//->join(array('TCO' => 'M_TEMPLATE_CHARGES_OTHER_DETAIL'),'A.TEMPLATE_ID = TCO.TEMPLATE_ID' , array('CHARGES_TYPE','CHARGES_CCY','CHARGES_AMT'))
		    
	    //echo $mData;exit;
			//->query();
		if($filter == 'Set Filter' || $csv || $pdf)
		{
		    if($templateName2)
	        {
			    $mData->where("UPPER(TEMPLATE_NAME) LIKE ".$this->_db->quote('%'.$templateName2.'%'));
		    }
		}
		$mData->order($sortBy.' '.$sortDir); 
		//$mData->order('TEMPLATE_ID ASC');
		if($csv || $pdf)
		{
			$arr = $this->_db->fetchAll($mData);
		}
		if($csv)
		{	
			$this->_helper->download->csv(array('TEMPLATE NAME','Create Date','Creator','Latest Suggestion','Latest Suggestor'),$arr,null,'Charge Template List');  
		}
		else if($pdf)
		{
			$this->_helper->download->pdf(array('TEMPLATE NAME','Create Date','Creator','Latest Suggestion','Latest Suggestor'),$arr,null,'Charge Template List');  
		}
			
		//$res =	$this->_db  ->fetchAll($mData);
		$this->paging($mData);
		//$this->view->mData = $res;
		
		
				 
	}
	public function newAction()
	{
	    $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	} else {
    	
    	    $this->view->report_msg = array();
		}
		
    	
		if($this->_request->isPost() )
		{
		    //process post
			$this->_addSave();
		} else {
		    //form add
			$this->_formAdd();
		}
	}
	
	public function editAction()
	{
	    $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	} else {
    	
    	    $this->view->report_msg = array();
		}
		
    	
		if($this->_request->isPost() )
		{
		    //process post
			$this->_addSave(1);
		} else {
		    //form edit
			$this->_formEdit();
			
		}
	}
	
	private function _formEdit()
	{
	    
			  $this->_helper->layout()->setLayout('popup');
	
	    $template_id = $this->_getParam('template_id');
		$mData = $this->_db->select()
			->from(array('A' => 'M_TEMPLATE_CHARGES_OTHER'), array('*'))
			->join(array('TCO' => 'M_TEMPLATE_CHARGES_OTHER_DETAIL'),'A.TEMPLATE_ID = TCO.TEMPLATE_ID' , array('CHARGES_TYPE','CHARGES_CCY','CHARGES_AMT'))
			->where("A.TEMPLATE_ID = ".$this->_db->quote($template_id));
		$arr = $this->_db->fetchAll($mData);
		$ccyArr = $this->_db->select()
						->from(array('M_MINAMT_CCY'), array('CCY_ID'))
						->order('CCY_ID ASC')
						->query()
						->fetchAll();
		$this->view->ccyArr = Application_Helper_Array::listArray($ccyArr,'CCY_ID','CCY_ID');
		if(is_array($ccyArr)){
	        	$ccyArrValidate = Application_Helper_Array::simpleArray($ccyArr,'CCY_ID');
	    }
		$this->view->templatedetail = $arr;
	}
	public function deleteAction()
	{
	    $template_id = $this->_getParam('template_id');
		$mData = $this->_db->select()
			->from(array('A' => 'M_TEMPLATE_CHARGES_OTHER'), array('*'))
			->join(array('TCO' => 'M_TEMPLATE_CHARGES_OTHER_DETAIL'),'A.TEMPLATE_ID = TCO.TEMPLATE_ID' , array('CHARGES_TYPE','CHARGES_CCY','CHARGES_AMT'))
			->where("A.TEMPLATE_ID = ".$this->_db->quote($template_id));
		
		$arr = $this->_db->fetchAll($mData);
		foreach($arr as $datadetail)
		{
		    $TEMPLATE_ID =  $datadetail['TEMPLATE_ID'];
		    $TEMPLATE_NAME =  $datadetail['TEMPLATE_NAME'];
		    $TCREATED =  $datadetail['CREATED'];
			$TCREATOR =  $datadetail['CREATEDBY'];
			$TSUGGESTED =  $datadetail['SUGGESTED'];
			$TSUGGESTOR =  $datadetail['SUGGESTEDBY'];
			$TAPPROVED =  $datadetail['UPDATED'];
			$TAPPROVER =  $datadetail['UPDATEDBY'];
			if ($datadetail['CHARGES_TYPE'] == 1)
			{
			    $RTGSAMT = $datadetail['CHARGES_AMT'];
				$RTGSCCY = $datadetail['CHARGES_CCY'];
			}
			
			if ($datadetail['CHARGES_TYPE'] == 2)
			{
			    $SKNAMT = $datadetail['CHARGES_AMT'];
				$SKNCCY = $datadetail['CHARGES_CCY'];
			}
		}
			
			
				    $changeInfo= "Delete Template Charge Other";
				    $changeType = $this->_changeType['code']['delete'];
				
				$masterTable = 'M_TEMPLATE_CHARGES_OTHER,M_TEMPLATE_CHARGES_OTHER_DETAIL';
				$tempTable =  'TEMP_TEMPLATE_CHARGES_OTHER,TEMP_TEMPLATE_CHARGES_OTHER_DETAIL';
				$keyField = 'TEMPLATE_ID';
				$keyValue = $TEMPLATE_ID;
				$displayTableName = 'Charges Template List';
				//var_dump($dataforinsert);exit;					
				$changesId = $this->suggestionWaitingApproval($displayTableName, $changeInfo, $changeType,'',$masterTable,$tempTable,$keyField,$keyValue);
			$content = array(
							'TEMPLATE_NAME'  => $TEMPLATE_NAME,
							'CREATED' 		 => $TCREATED,
							'CREATEDBY' 	 => $TCREATOR,
							'SUGGESTED' => date("Y-m-d H:i:s"),
							'SUGGESTEDBY' => $this->_userIdLogin,
							'CHANGES_ID' => $changesId
						    );
			
				   
		    $content['TEMPLATE_ID'] = $TEMPLATE_ID;
			
			//insert into temp
				$this->_db->insert('TEMP_TEMPLATE_CHARGES_OTHER',$content);
				
			    $lastIdTemp = $this->_db->fetchOne('select @@identity');
				
				$content2 = array(
				            'TEMP_ID' => $lastIdTemp,
							
							'TEMPLATE_NAME'  => $TEMPLATE_NAME,
							'CHARGES_TYPE' => 1,
							'CHARGES_CCY' =>$RTGSCCY,
							'CHARGES_AMT' =>  $RTGSAMT,
							'CHANGES_ID' => $changesId
							
					
						    );
				
				$content2['TEMPLATE_ID'] = $TEMPLATE_ID;
				
				$this->_db->insert('TEMP_TEMPLATE_CHARGES_OTHER_DETAIL',$content2);	

                $content3 = array(
				            'TEMP_ID' => $lastIdTemp,
							
							'TEMPLATE_NAME'  => $TEMPLATE_NAME,
							'CHARGES_TYPE' => 2,
							'CHARGES_CCY' =>$SKNCCY,
							'CHARGES_AMT' => $SKNAMT,
							'CHANGES_ID' => $changesId
							
					
						    );
				
				$content3['TEMPLATE_ID'] = $TEMPLATE_ID;
				
				$this->_db->insert('TEMP_TEMPLATE_CHARGES_OTHER_DETAIL',$content3);
				$this->_redirect('/notification/success');

            				
		
	}
	private function _formAdd()
	{
	    //echo "sini";exit;
			
		$ccyArr = $this->_db->select()
						->from(array('M_MINAMT_CCY'), array('CCY_ID'))
						->order('CCY_ID ASC')
						->query()
						->fetchAll();
		$this->view->ccyArr = Application_Helper_Array::listArray($ccyArr,'CCY_ID','CCY_ID');
		if(is_array($ccyArr)){
	        	$ccyArrValidate = Application_Helper_Array::simpleArray($ccyArr,'CCY_ID');
	    }
		$this->view->myccyrtgs='IDR';
		$this->view->myccyskn='IDR';
	}
	
	private function _addSave($isEdit=0)
	{
	    $filters = array(
			                 'template_name'     => array('StringTrim','StripTags'),
							 'city'          => array('StringTrim','StripTags'),
							 'charge_skn_curr' => array('StringTrim','StripTags'),
							 'charge_rtgs_curr'    => array('StringTrim','StripTags'),
							 'charge_rtgs'    => array('StringTrim','StripTags'),
							 'charge_skn'    => array('StringTrim','StripTags')
							);
		 		
			$validators = array(
			                    'template_name' => array('NotEmpty',
			                                         new Zend_Validate_StringLength(array('max'=>35)),
													 'messages' => array(
																         'Can not be empty',
																         'Data too long (max 35 chars)',
			                                                             )
													),
								'charge_skn_curr' => array('NotEmpty',
			                                         new Zend_Validate_StringLength(array('max'=>35)),
													 'messages' => array(
																         'Can not be empty',
																         'Data too long (max 35 chars)',
			                                                             )
													),					
								'charge_rtgs_curr'      => array('NotEmpty',
													 
													 new Zend_Validate_StringLength(array('max'=>15)),
													 'messages' => array(
																         'Data too long (max 15 chars)',
																         )
														),
								'charge_rtgs'      => array('NotEmpty',
													 
													 new Zend_Validate_StringLength(array('max'=>15)),
													 'messages' => array(
																         'Data too long (max 15 chars)',
																         )
														),
								'charge_skn'      => array('NotEmpty',
													 
													 new Zend_Validate_StringLength(array('max'=>15)),
													 'messages' => array(
																         'Data too long (max 15 chars)',
																         )
														),
														
								
								
							   );
		
							   
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			if($zf_filter_input->isValid())
			{
			    if ($isEdit)
				{
			        $changeInfo= "Edit Template Charge Other";
				    $changeType = $this->_changeType['code']['edit'];
					$keyField = 'TEMPLATE_ID';
				    $keyValue = $this->_getParam('template_id');
				} else {
				    $changeInfo= "New Template Charge Other";
				    $changeType = $this->_changeType['code']['new'];
					$keyField = 'CREATEDBY';
				    $keyValue = $this->_userIdLogin;
				}
				$masterTable = 'M_TEMPLATE_CHARGES_OTHER,M_TEMPLATE_CHARGES_OTHER_DETAIL';
				$tempTable =  'TEMP_TEMPLATE_CHARGES_OTHER,TEMP_TEMPLATE_CHARGES_OTHER_DETAIL';
				
				$displayTableName = 'Charges Template List';
				//var_dump($dataforinsert);exit;					
				$changesId = $this->suggestionWaitingApproval($displayTableName, $changeInfo, $changeType,'',$masterTable,$tempTable,$keyField,$keyValue);
				
				
			    $content = array(
							'TEMPLATE_NAME'  => $zf_filter_input->template_name,
							'CREATED' 		 => date("Y-m-d H:i:s"),
							'CREATEDBY' 	 => $this->_userIdLogin,
							'SUGGESTED' => date("Y-m-d H:i:s"),
							'SUGGESTEDBY' => $this->_userIdLogin,
							'CHANGES_ID' => $changesId
						    );
				if ($isEdit)
				{
				   
				    $content['TEMPLATE_ID'] = $this->_getParam('template_id');
				}
				//insert into temp
				$this->_db->insert('TEMP_TEMPLATE_CHARGES_OTHER',$content);
				
			    $lastIdTemp = $this->_db->fetchOne('select @@identity');
				
				$content2 = array(
				            'TEMP_ID' => $lastIdTemp,
							
							'TEMPLATE_NAME'  => $zf_filter_input->template_name,
							'CHARGES_TYPE' => 1,
							'CHARGES_CCY' =>$zf_filter_input->charge_rtgs_curr,
							'CHARGES_AMT' => $zf_filter_input->charge_rtgs,
							'CHANGES_ID' => $changesId
							
					
						    );
				if ($isEdit)
				{
				    $content2['TEMPLATE_ID'] = $this->_getParam('template_id');
				}
				$this->_db->insert('TEMP_TEMPLATE_CHARGES_OTHER_DETAIL',$content2);	

                $content3 = array(
				            'TEMP_ID' => $lastIdTemp,
							
							'TEMPLATE_NAME'  => $zf_filter_input->template_name,
							'CHARGES_TYPE' => 2,
							'CHARGES_CCY' =>$zf_filter_input->charge_skn_curr,
							'CHARGES_AMT' => $zf_filter_input->charge_skn,
							'CHANGES_ID' => $changesId
							
					
						    );
				if ($isEdit)
				{
				    $content3['TEMPLATE_ID'] = $this->_getParam('template_id');
				}
				$this->_db->insert('TEMP_TEMPLATE_CHARGES_OTHER_DETAIL',$content3);
				$this->_redirect('/notification/success');
				
				//var_dump($lastIdTemp);exit;
				//get last id
				//insert into temp detail using last id
					
			}
	}
}
