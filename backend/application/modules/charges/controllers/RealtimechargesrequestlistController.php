<?php


require_once 'Zend/Controller/Action.php';


class charges_RealtimechargesrequestlistController extends Application_Main
{
	public function indexAction() 
	{
		$fields = array	(
							'Company Code'  			=> array	(
																	'field' => 'CUST_ID',
																	'label' => 'Company Code',
																	'sortable' => false
																),
							'Company Name'  			=> array	(
																	'field' => 'CUST_NAME',
																	'label' => 'Company Name',
																	'sortable' => false
																),
							'Suggest Date'  			=> array	(
																	'field' => 'CUST_NAME',
																	'label' => 'Suggest Date',
																	'sortable' => false
																),
							'Suggestor'  			=> array	(
																	'field' => 'CUST_NAME',
																	'label' => 'Suggestor',
																	'sortable' => false
																),
							'Suggestion Status'  			=> array	(
																	'field' => 'CUST_NAME',
																	'label' => 'Suggestion Status',
																	'sortable' => false
																),
						);
		$this->view->fields = $fields;
	}
}
