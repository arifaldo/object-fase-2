<?php
require_once 'Zend/Controller/Action.php';

class charges_ChargesummaryreportController extends Application_Main
{	
	protected $_moduleDB = 'RTF';
	
	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');
	
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
			if($temp[0]=='F' || $temp[0]=='S'){
				if($temp[0]=='F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = ''; unset($temp[0]);
				foreach($temp as $value)
				{
					if(!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->customer_msg = $msg;
			}	
		}
		
		$param = $this->_request->getParams();
		// var_dump($param);
		
		$monthlist = array(
						'1'		=>	'January',
						'2' 	=> 	'February',
						'3'		=>	'March',
						'4'		=>	'April',
						'5'		=>	'May',
						'6'		=>	'June',
						'7'		=>	'July',
						'8'		=>	'August',
						'9'		=>	'September',
						'10'	=>	'October',
						'11'	=>	'November',
						'12'	=>	'December');
		$this->view->monthlist = $monthlist;
		
		$thisyear = date("Y");
		$yearlist = array(
						$thisyear-3	=> 	$thisyear-3,
						$thisyear-2	=> 	$thisyear-2,
						$thisyear-1	=> 	$thisyear-1,
						$thisyear	=> 	$thisyear);
		$this->view->yearlist = $yearlist;
		
		$fields = array	(
							'chargestype'  		=> array(	
															'field' => 'chargestype',
															'label' => $this->language->_('Charge Type'),
															'sortable' => false
														),
							'ccy'  				=> array(
															'field' => 'ccy',
															'label' => $this->language->_('Currency'),
															'sortable' => false
														),
							'numbertransaction' => array(
															'field' => 'numbertransaction',
															'label' => $this->language->_('Number of Transaction'),
															'sortable' => false
														),
							'totalcharges'  	=> array(
															'field' => 'totalcharges',
															'label' => $this->language->_('Total Charges'),
															'sortable' => false
														),
						);
		
		//validasi page, jika input page bukan angka               
		$page 	= $this->_getParam('page');
		$page 	= (Zend_Validate::is($page,'Digits'))? $page : 1;
		$this->view->currentPage = $page;
		
		//button csv, pdf & filter
		$csv 	= $this->_getParam('csv');
		$print  = $this->_getParam('print');
		$pdf 	= $this->_getParam('pdf');
		$filter 	= $this->_getParam('filter');
		
		//validasi sort, jika input sort bukan ASC atau DESC
		$sortBy  = $this->_getParam('sortby');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
		
		$this->view->currentPage 	= $page;
		$this->view->sortBy 		= $sortBy;
		$this->view->sortDir 		= $sortDir;
		
		//Param date
		$datetype 	= $this->_getParam('datetype');
		$month 		= $this->_getParam('month');
		$year 		= $this->_getParam('year');
		
		$validateDateFormat = new Zend_Validate_Date(array('format' => $this->_dateDisplayFormat));
		
		$dataSQL = array();
		
		//Set templates
		$temp = array();
		$temp [0] = 'Monthly Charge'; 
		$temp [1] = 'Realtime Charge'; 
		//$temp [2] = 'Supply Chain Charge'; 
		$this->view->temp = $temp;
		
		$select = array();
		
		$select = $this->_db->select()
							->from(	array('B' => 'T_CHARGES_LOG'),
									array(	'chargestype'=>'CHARGES_TYPE',
											'paymenttype'=>'PAYMENT_TYPE',
											'ccy'=>'CCY',
											'numbertransaction'=>'COUNT(*)',
											'totalcharges'=>'SUM(AMOUNT)')
										)
							->where("B.STATUS = '1'")
							->group(array('B.CHARGES_TYPE','B.PAYMENT_TYPE','B.CCY'))
							->order("B.PAYMENT_TYPE desc")
							->order("B.CHARGES_TYPE desc");
		
		if($datetype == null || $datetype == 'date1'){ 
			
			if($validateDateFormat->isValid($this->_getParam('date')))	$date = $this->_getParam('date');
			else														$date = (date("d/m/Y"));
			
			
			$FormatDate = new Zend_Date($date, $this->_dateDisplayFormat);
			$dateselect   = $FormatDate->toString($this->_dateDBFormat);
			
			
			$this->view->datemsg =  Application_Helper_General::convertDate($date,$this->_dateViewFormat);
			$datemsg = Application_Helper_General::convertDate($date,$this->_dateViewFormat);
			$select->where("DATE(B.LOG_DATETIME) = ".$this->_db->quote($dateselect));
			
		}
		if($datetype == 'date2'){ 
			
			$this->view->datemsg = $monthlist[$month].'&nbsp;'.$year;
			$datemsg = $monthlist[$month].'&nbsp;'.$year;
			$this->view->month = $month;
			$this->view->year = $year;
			
			$select->where("MONTH(B.LOG_DATETIME) = ".$this->_db->quote($month));
			$select->where("YEAR(B.LOG_DATETIME) = ".$this->_db->quote($year));
		}
		
		// echo "<pre>";
		// echo $select->__toString(); //die();
		
		$dataD = $this->_db->fetchAll($select);

		$data1 = array(); //monthly charge
		$data2 = array(); //realtime charge
		$data3 = array(); //supplay chain charge
				
		$tot1 = NULL;
		$tot2 = NULL;
		$tot3 = NULL;
	
		
		/*
			1. monthly charge: admfeecompany, admfeeaccount, monthlycompany, monthlyaccount
			2. realtime charge: payment type (1 - 9) + charges type (PB, SKN, RTGS), partners
			3. supplay chain charge: payment type (8 - 9) + charges type (Interest fee + transaction fee)
		
		*/
		
		$mf = $this->getMonthlyfeeChargeRaw();
		//$fea = $this->getFeatureIdRaw();
		$pt = $this->getPaymentTypeRaw();		
	
		//$paydis = $pt[8];
		//$payset = $pt[9];
		
		// suply chain charge
		//$fd [8] = $paydis;
		//$fd [9] = $payset;
		
		$chargesraw = $this->getChargesTypeRaw();
		$c1 = $chargesraw[6];
		$c2 = $chargesraw[7];
		
		$chargesup [6] = $c1;
		$chargesup [7] = $c2;
		
		array_pop($chargesraw);
		array_pop($chargesraw);
		
		$total['monthly'] 	= array();
		$total['realtime'] 	= array();
		$total['scm'] 		= array();
		
		$arrPayTypeCode 	= $this->getPaymentTypeCode();
		$arrChargesTypeCode = $this->getChargesTypeCode();
		//$arrFeatureIdCode 	= $this->getFeatureIdCode();
		$arrMonthlyFeeCode 	= $this->getMonthlyfeeChargeCode();
		
		// echo "<pre>";
		// var_dump($arrMonthlyFeeCode);
		
		$arrSupplyChainChargesType[$arrChargesTypeCode[6]] = $arrChargesTypeCode[6];
		$arrSupplyChainChargesType[$arrChargesTypeCode[7]] = $arrChargesTypeCode[7];
		
		unset($arrChargesTypeCode["6"]);
		unset($arrChargesTypeCode["7"]);
		
		//$arrSupplyChainPayType[$arrPayTypeCode[8]] = $arrPayTypeCode[8];
		//$arrSupplyChainPayType[$arrPayTypeCode[9]] = $arrPayTypeCode[9];
		
		// var_dump($dataD); die;
		
		if(is_array($dataD) && count($dataD) > 0){
			foreach ($dataD as $d => $dt)
			{	
				
				$paytype = $dt["paymenttype"];
				
				// echo "$paytype<br />";
				
				$chargestype = $dt["chargestype"];
				$ccy = $dt["ccy"];
				$tot = $dt["totalcharges"];
				
				foreach ($fields as $key => $field)
				{
					
					$value = $dt[$key];					
					
					$paytypename = $this->getPaymentTypeCharges($paytype);
					$chargestypename = $this->getChargesType($chargestype,$paytype);
					
					if ($key == "totalcharges" ){	$value = Application_Helper_General::displayMoney($value); }
					else if ($key == "chargestype" ){ $value = $paytypename.''.$chargestypename;	}	
					
					$value = ($value == "" )? "&nbsp;": $value;
					if($csv || $print){ $data[$d][$key] = $value; }
					
					/*chargesallraw = 	[0] => Inhouse 
										[1] => RTGS 
										[2] => SKN 
										[3] => Transfer Fee 
										[4] => Full Amount Fee 
										[5] => Provision Fee 
										[6] => Interest Fee 
										[7] => Transaction Fee 
					*/
					
					/*payallraw = 	[1] => Inhouse 
									[2] => Domestic 
									[3] => Remittance 
									[6] => Multi Credit 
									[7] => Multi Debet 
									[4] => Multi Credit - Import 
									[5] => Multi Debet - Import 
									[8] => Disbursement 
									[9] => Settlement
					*/
					
					
					//---------------- ADM FEE ----------------------------
					
					if(in_array($paytype,$arrMonthlyFeeCode)){
						$data1[$d][$key] = $value;
					}
					
					//---------------- END OF ADM FEE ---------------------
					
					//---------------- REAL TIME ----------------------------
					
					//real time payment type (1-9) & charges type (0-5)
					if (in_array($paytype, $arrPayTypeCode)) {
						if (in_array($chargestype, $arrChargesTypeCode)) {
								
							$data2[$d][$key] = $value;
						}
					}
					
					
					//featurename
					/*if(in_array($paytype,$arrFeatureIdCode)){
						$data2[$d][$key] = $value;
					}*/
										
					//---------------- END OF REAL TIME -------------------
					
					//---------------- SUPPLY CHAIN CHARGE ----------------------------
					
					//payment type disbursment, settlement ( interest fee [6] + transaction fee [7] )
					/*if (in_array($paytype, $arrSupplyChainPayType)) {
						if (in_array($chargestype, $arrSupplyChainChargesType)) {
								
							$data3[$d][$key] = $value;
						}
					}*/
					
					//---------------- END OF SUPPLY CHAIN CHARGE ---------------------
					
				}
				
				// ---------------- ADMIN FEE ----------------
				
				if(in_array($paytype,$arrMonthlyFeeCode)){
					$total['monthly'][$ccy] += $tot;
				}
				
				// ---------------- END OF ADMIN FEE ----------------
				
				// ---------------- REAL TIME ----------------
				
				if (in_array($paytype, $arrPayTypeCode)) {
// 					if (in_array($chargestype, $arrChargesTypeCode)) {
// 						$total['realtime'][$ccy] += $tot;
// 					}
				}
				
				/*if(in_array($paytype,$arrFeatureIdCode)){
					$total['realtime'][$ccy] += $tot;
				}*/
				
				// ---------------- END OF REAL TIME ----------------
				
				// ---------------- MONTHLY FEE ----------------
				
				/*if (in_array($paytype, $arrSupplyChainPayType)) {
					if (in_array($chargestype, $arrSupplyChainChargesType)) {
								
						$total['scm'][$ccy] += $tot;
						
					}
				}*/
				
				// ---------------- END OF ADMIN FEE ----------------
			
			}
		}
		else {
			
			$data1 = array();
			$data2 = array();
			$data3 = array();
		}
		
		
		// Zend_Debug::Dump($total);
		
		// echo "<pre>";
		// echo "data1: ";
		// print_r($data1);
		// echo "data2: ";
		// print_r($data2);
		// echo "data3: ";
		// print_r($data);
		// die;
		if(isset($data))
		{
			if(is_array($data) && count ($data) > 0)
				$dataExport = array_merge($data1,$data2,$data3);
		}		
		else 
			$dataExport = array();

		

		$countData = count($dataExport); // menghitung jumlah row data u/ validasi export csv
		
		$this->view->countData 	= $countData; 
		$this->view->total 		= $total; 
		$this->view->pdf 		= ($pdf)? true: false;
		$this->view->data1 		= $data1;
		$this->view->data2 		= $data2;
		$this->view->data3 		= $data3;
	//	echo '<pre>';
		// var_dump($data1);
		// var_dump($data2);
		// var_dump($data3);//die;
		$this->view->fields 		= $fields;
		$this->view->filter 		= $filter;
	    $this->view->sortDir 		= $sortDir;
			
		$this->view->datetype 		= $datetype;
		$this->view->datefull 		= (isset($date)) ? $date: '';
		$this->view->month 			= $month;
		$this->view->year 			= $year;
		
		$this->view->datechoosen 	= (isset($datechoosen)) ? $datechoosen : '';
		$this->view->monthlist 		= $monthlist;
		$this->view->yearlist 		= $yearlist;
			
		$this->view->temp 			= $temp;

		if ($csv || $pdf || $print){	
			$header  = Application_Helper_Array::simpleArray($fields, "label"); 
// 			$header[5] = 'Periode';
		}

		

		if($csv){

			if($datetype == 'date1'){
				$date 		= $this->_getParam('date');
			}
			else if($datetype == 'date2'){ 
				$date = $monthlist[$month].' '.$year;
			}
			
			$dataExportCSV = array();
			if(is_array($dataExport) && count($dataExport) > 0){
				foreach($dataExport as $key => $value){
					$dataExportCSV[$key]['periode'] = $date;
				}
			}

			$this->_helper->download->csv($header,$dataExport,null,$this->language->_('Charges Summary Report'));  
			Application_Helper_General::writeLog('CSMR','Export CSV Customer Summary Report');
			
		}elseif($print == 1){

			// echo $print;
			 //die('here');
			
			$paramsdata= array(
				'datemsg' 	=> $datemsg, 
				'temp' 		=> $temp, 
				'total' 	=> $total,
				'fields'	=> $fields,
				'data1'     => $data1,
				'data2'     => $data2,
				'data3'     => $data3
				
			);
			
		//	echo '<pre>';
		//	var_dump($paramsdata);die;
			$this->_forward('printchangesummary', 'index', 'widget', $paramsdata);
		}
		elseif($pdf)
		{	
			$outputHTML = "<tr><td>".$this->view->render('/chargesummaryreport/index.phtml')."</td></tr>";
			$this->_helper->download->pdf(null,null,null,'Charges Summary Report',$outputHTML);
			Application_Helper_General::writeLog('CSMR','Export PDF Customer Summary Report');
			
		}
		else
		{							
			Application_Helper_General::writeLog('CSMR','View Charge Summary Report');
		}

		
	}
	
	function getChargesTypeRaw(){
		
		//charges.type.code
		//charges.type.desc
		
		$config = Zend_Registry::get('config');
		
		$ctype 	= $config["charges"]["type"]["code"];
		$cdesc 	= $config["charges"]["type"]["desc"];
		$payall = array_combine($ctype, $cdesc);
		
		return $payall;
	
	}
		
	function getChargesType($chargestype,$paytype){
		
		$chargesallraw = $this->getChargesTypeRaw();
		$payallraw = $this->getPaymentTypeRaw();
		
		/*chargesallraw = 	[0] => Inhouse 
							[1] => RTGS 
							[2] => SKN 
							[3] => Transfer Fee 
							[4] => Full Amount Fee 
							[5] => Provision Fee 
							[6] => Interest Fee 
							[7] => Transaction Fee 
		*/
		
		/*payallraw = 	[1] => Inhouse 
						[2] => Domestic 
						[3] => Remittance 
						[6] => Multi Credit 
						[7] => Multi Debet 
						[4] => Multi Credit - Import 
						[5] => Multi Debet - Import 
						[8] => Disbursement 
						[9] => Settlement
		*/
		
		$paytypenoempty = array(2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8);
		$emptyok = array(1=>1,9=>9); // Payment type: Within
		
		foreach($payallraw as $pp =>$val){
			
			foreach($chargesallraw as $ch => $valch){
			
				if($pp == 1 || $pp == 9){
					if($chargestype == 0){
						$result = " ";
					}
					else{
						if($ch == $chargestype){
							$result = ' ( '.$valch.' )';
						}
						
					}
				}
				else if($pp >= 2 && $pp<= 8){ 
					if($ch == $chargestype){
						$result = ' ( '.$valch.' )';
					}
				}
				else {}
				
			}
		}
			
		return $result;
	
	}
	
	function getMonthlyfeeChargeRaw(){
	
		//monthlyfee.type.desc.
		$config    		= Zend_Registry::get('config');
		$monthlyfee 	= $config["monthlyfee"]["type"]["desc"];
		
		return $monthlyfee;
		
	}
	
	function getFeatureIdRaw(){
	
		//featureid
		$select = $this->_db->select()
							->from(array('T'=>'M_BILLER'),array('FEATURE_ID','FEATURE_NAME'));
		$featuredata = $this->_db->fetchAll($select);
		
		return $featuredata;
		
	}
	
	function getPaymentTypeRaw(){
		
		$config = Zend_Registry::get('config');
		
		//paymenttype: payment.type.code
		$paycode 	= $config["payment"]["type"]["code"];
		$paydesc 	= $config["payment"]["type"]["desc"];
		$payall = array_combine($paycode, $paydesc);
		
		unset($payall[10]);
		
		return $payall;
	}
		
	function getPaymentTypeCharges($value){
		/*
		$monthlyfee = $this->getMonthlyfeeChargeRaw();
		//$featuredata = $this->getFeatureIdRaw();
		$payall = $this->getPaymentTypeRaw();

		//$arrayall = array_merge($monthlyfee,$featuredata,$payall);
		
		$arrayall = $monthlyfee + $featuredata + $payall;
		foreach($arrayall as $key => $val){
			if($key == $value){	$paytype = $val; }
			
		}
		
		//print_r($arrayall);
		return $paytype;	
		*/
		
		return NULL;
	}

	function getMonthlyfeeChargeCode(){
	
		//monthlyfee.type.desc.
		// $config    		= Zend_Registry::get('config');
		// $monthlyfee 	= $config["monthlyfee"]["type"]["desc"];
		// $monthlyfeearr 	= array_combine($monthlyfee, $monthlyfee);
		
		$monthlyfeearr['admfeeaccount']  = 'admfeeaccount';
		$monthlyfeearr['admfeecompany']  = 'admfeecompany';
		$monthlyfeearr['monthlyaccount'] = 'monthlyaccount';
		$monthlyfeearr['monthlycompany'] = 'monthlycompany';
		
		return $monthlyfeearr;
		
	}
	
	/*function getFeatureIdCode(){
	
		//featureid
		$select = $this->_db->select()
							->from(array('T'=>'M_BILLER'),array('FEATURE_ID','FEATURE_NAME'));
		$featuredata = $this->_db->fetchAll($select);
		
		if(!empty($select))  $result = Application_Helper_Array::listArray($select,'FEATURE_ID','FEATURE_ID'); 
	  	else 				 $result = array();
		
		return $result;
		
	}*/
	
	function getPaymentTypeCode(){
		
		$config = Zend_Registry::get('config');
		
		//paymenttype: payment.type.code
		$paycode 	= $config["payment"]["type"]["code"];
		$paydesc 	= $config["payment"]["type"]["desc"];
		$payall = array_combine($paycode, $paycode);
		
		unset($payall[10]);
		
		return $payall;
	}
	
	function getChargesTypeCode(){
		
		//charges.type.code
		//charges.type.desc
		
		$config = Zend_Registry::get('config');
		
		$ctype 	= $config["charges"]["type"]["code"];
		$cdesc 	= $config["charges"]["type"]["desc"];
		$payall = array_combine($ctype, $ctype);
		
		return $payall;
	
	}
}
