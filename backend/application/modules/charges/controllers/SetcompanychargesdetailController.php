<?php


require_once 'Zend/Controller/Action.php';


class charges_SetcompanychargesdetailController extends Application_Main
{
	public function indexAction() 
	{
		$custid = $this->_getParam('custid');
		
		$select = $this->_db->select()
			->from(array('M_CUSTOMER'));
		
		$select -> where("CUST_ID LIKE ".$this->_db->quote($custid));
		$result = $select->query()->FetchAll();
		$this->view->result = $result;	
		
		//Zend_Debug::dump($custid); die;
		
		
		/*$select3 = $this->_db->select()
				->from(array('B' => 'M_CHARGES_WITHIN'),array('*'));
		$coba = $this->_db->fetchAll($select3);
		Zend_Debug::dump($select3); die;		
		
		$list = array(	''				=>	'--- Any Value---',
						'Enabled'		=>	'Enabled',
						'Disabled' 		=> 	'Disabled');
		$this->view->optionlist = $list;
		
		$ceklist = array(	'0'		=>	'Disabled',
							'1' 	=> 	'Enabled');
		
		$fields = array	(
							'Company Code'  			=> array	(
																	'field' => 'B.CUST_ID',
																	'label' => 'Company Code',
																	'sortable' => true
																),
							'Company Name'  			=> array	(
																	'field' => 'CUST_NAME',
																	'label' => 'Company Name',
																	'sortable' => true
																),
							'Charges'  					=> array	(
																	'field' => 'CUST_CHARGES_STATUS',
																	'label' => 'Charges',
																	'sortable' => true
																),
							'Administration Fee'  		=> array	(
																	'field' => 'CUST_ADMFEE_STATUS',
																	'label' => 'Administration Fee',
																	'sortable' => true
																),
						);
		$page    = $this->_getParam('page');
		
		$page = (Zend_Validate::is($page,'Digits'))? $page : 1;
		$sortBy  = $this->_getParam('sortby');
   		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
   		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
		
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;

		$filter = $this->_getParam('filter');
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');
		$chargeStatus = $this->_getParam('chargeStatus');
		$chargeStatus = $this->_getParam('chargeStatus');
		$chargeStatus = $this->_getParam('chargeStatus');
		
		
		$filterArr = array('filter' 		=> array('StripTags','StringTrim'),
	                       'SEARCH_CUST'    => array('StripTags','StringTrim'),
	                       'CompanyName'  	=> array('StripTags','StringTrim','StringToUpper'),
						   'chargeStatus'  	=> array('StripTags','StringTrim'),
						   'adminfeeStatus'  	=> array('StripTags','StringTrim'),
	                      );
	    $zf_filter = new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
	    $filter = $zf_filter->getEscaped('filter');
	    $custid = html_entity_decode($zf_filter->getEscaped('SEARCH_CUST'));
		$custname = html_entity_decode($zf_filter->getEscaped('CompanyName'));
		$chargeStatus = html_entity_decode($zf_filter->getEscaped('chargeStatus'));
		$adminfeeStatus = html_entity_decode($zf_filter->getEscaped('adminfeeStatus'));
		
		$chargeStatusarr = "(CASE B.CUST_CHARGES_STATUS ";
  		foreach($ceklist as $key=>$val)
  		{
   			$chargeStatusarr .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$chargeStatusarr .= " END)";
		
  		$adminfeeStatusarr = "(CASE B.CUST_ADMFEE_STATUS ";
  		foreach($ceklist as $key=>$val)
  		{
   			$adminfeeStatusarr .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$adminfeeStatusarr .= " END)";
  			
		$select2 = $this->_db->select()
						->from(array('B' => 'M_CUSTOMER')
						,array('B.CUST_ID','B.CUST_NAME','CUST_CHARGES_STATUS' => $chargeStatusarr, 'CUST_ADMFEE_STATUS' => $adminfeeStatusarr));
		//echo $select2;die;
		if($filter == 'Set Filter')
		{
			if($chargeStatus)
			{
				$this->view->chargeStatus = $chargeStatus;
				if($chargeStatus == "Enabled")
				{
					$cekchargeStatus = "1";
				}
				if($chargeStatus == "Disabled")
				{
					$cekchargeStatus = "0";
				}
				$select2->where("B.CUST_CHARGES_STATUS LIKE ".$this->_db->quote($cekchargeStatus));
			}
			
			if($adminfeeStatus)
			{
				$this->view->adminfeeStatus = $adminfeeStatus;
				if($adminfeeStatus == "Enabled")
				{
					$cekadminfeeStatus = "1";
				}
				if($adminfeeStatus == "Disabled")
				{
					$cekadminfeeStatus = "0";
				}
				$select2->where("B.CUST_ADMFEE_STATUS LIKE ".$this->_db->quote($cekadminfeeStatus));
			}

		    if($custid)
		    {
	       		$this->view->cekcustid = $custid;
	       		$select2->where("B.CUST_ID LIKE ".$this->_db->quote($custid));
		    }
		    
			if($custname)
			{
	       		$this->view->custname = $custname;
	       		$select2->where("B.CUST_NAME LIKE ".$this->_db->quote('%'.$custname.'%'));
			}
		}
		$arr = $this->_db->fetchAll($select2);
		//Zend_Debug::dump($chargeStatus); die;
		//echo $select2; die;
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		$this->paging($arr);*/
	}
}
