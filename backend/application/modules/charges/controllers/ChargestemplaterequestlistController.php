<?php


require_once 'Zend/Controller/Action.php';


class charges_ChargestemplaterequestlistController extends Application_Main
{
	public function indexAction() 
	{
		$fields = array	(
							'Request#'  			=> array	(
																	'field' => 'CUST_ID',
																	'label' => 'Request#',
																	'sortable' => false
																),
							'Template Name'  			=> array	(
																	'field' => 'CUST_NAME',
																	'label' => 'Template Name',
																	'sortable' => false
																),
							'Suggest Date'  			=> array	(
																	'field' => 'CUST_NAME',
																	'label' => 'Suggest Date',
																	'sortable' => false
																),
							'Suggestor'  			=> array	(
																	'field' => 'CUST_NAME',
																	'label' => 'Suggestor',
																	'sortable' => false
																),
							'Suggestion Type'  			=> array	(
																	'field' => 'CUST_NAME',
																	'label' => 'Suggestion Type',
																	'sortable' => false
																),
							'Suggestion Status'  			=> array	(
																	'field' => 'CUST_NAME',
																	'label' => 'Suggestion Status',
																	'sortable' => false
																),
							'type'  			=> array	(
																	'field' => 'CUST_NAME',
																	'label' => 'Type',
																	'sortable' => false
																),
						);
		$this->view->fields = $fields;
	}
}
