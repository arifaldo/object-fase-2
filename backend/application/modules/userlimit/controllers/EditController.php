<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class userlimit_EditController extends Application_Main 
{

	public function initController()
	{       
		//$this->_helper->layout()->setLayout('popup');
		$listCcy = array(''=>'-- All --');
		$listCcy = array_merge($listCcy,Application_Helper_Array::listArray($this->getCcy(),'CCY_ID','CCY_ID'));
		$this->view->listCcy = $listCcy;
	}
	
  public function indexAction() 
  {
  	$this->_helper->layout()->setLayout('newlayout');

  	$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
    $password = $sessionNamespace->token; 
    $this->view->token = $sessionNamespace->token;


    $AESMYSQL = new Crypt_AESMYSQL();
    $cust_id = $AESMYSQL->decrypt($this->_getParam('cust_id'), $password);
    $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;
    $user_id = $AESMYSQL->decrypt($this->_getParam('user_id'), $password);
    $acct_no = $AESMYSQL->decrypt($this->_getParam('acct_no'), $password);

  	$this->view->cust_id = $cust_id;
  	
  	$acct_no = (Zend_Validate::is($acct_no,'Alnum'))? $acct_no : null;
  	$this->view->acct_no = $acct_no;
  	
  	$type = $this->_getParam('type');
    $this->view->type = $type;
				
	$select	= $this->_db->select()
					->from(array('M_USER'),
						   array('USER_FULLNAME')
						   )
					->where("CUST_ID = ?", $cust_id)
					->where("USER_ID = ?", $user_id);
	$userName = $this->_db->fetchOne($select);
  	
    
    if($cust_id && $user_id)
  	{
		$select = $this->_db->select()
  	                         ->from('M_CUSTOMER',array('CUST_ID','CUST_NAME','CUST_LIMIT_IDR'))
  	                         ->where('UPPER(CUST_ID)='.$this->_db->quote(strtoupper($cust_id)));
		$result = $this->_db->fetchRow($select);
		// var_dump($result);
		if($result['CUST_ID'])
		{
			$this->view->cust_name = $result['CUST_NAME'];
		}
		
		//check apakah company-nya masih ada approval
		$validator = new Zend_Validate_Db_NoRecordExists(
						array(
							'table' => 'TEMP_MAKERLIMIT',
							'field' => 'USER_LOGIN'
						));
		
		if (!$validator->isValid($user_id))
		{	
			$messages = array($this->language->_('No changes allowed for this record while awaiting approval for previous change.'));
			$this->view->isapproval = $this->displayError($messages);
		}
								 
		$fields = array('acct_no'       => array('field'    => 'A.ACCT_NO',
											   'label'    => $this->language->_('Account Number'),
											   'sortable' => true),
		
						'acct_name'    => array('field'   => 'A.ACCT_NAME',
											  'label'    => $this->language->_('Account Name'),
											  'sortable' => true),
		
						'ccy'   	 => array('field'   => 'A.CCY_ID',
											  'label'    => $this->language->_('Currency'),
											  'sortable' => true),
						);
		$sortBy = $this->_getParam('sortby','ACCT_NO');
		$sortBy = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir = $this->_getParam('sortdir','asc');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
		
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		
		$select = $this->_db->select()
							 ->from(array('A' => 'M_CUSTOMER_ACCT'),array('ACCT_NO','ACCT_NAME','CCY_ID','MAXLIMIT' => '(SELECT MAXLIMIT FROM M_MAKERLIMIT WHERE USER_LOGIN=\''.$user_id.'\' AND CUST_ID=\''.$cust_id.'\' AND ACCT_NO=A.ACCT_NO AND MAKERLIMIT_STATUS = 1)'))
							 //->joinLeft(array('B' => 'M_MAKERLIMIT'),'A.ACCT_NO = B.ACCT_NO',array('MAXLIMIT'))
							 ->where('UPPER(A.CUST_ID)='.$this->_db->quote(strtoupper($cust_id)))
							 ->where('A.ACCT_STATUS <> 3')
							 ->order($sortBy.' '.$sortDir)
							 // echo $select;die;
							 ->query()->fetchAll();	
		
		$this->view->data = $select;
		$this->view->fields = $fields;
		
		$this->view->status_type = $this->_masterglobalstatus;
		$this->view->cust_id = $cust_id;
		$this->view->user_id = $user_id;
		$this->view->acct_no = $acct_no;
		$this->view->modulename = $this->_request->getModuleName();
  	}
  	else
  	{
  	  $error_remark = $this->language->_('Customer ID does not exist.');
      //insert log
      // try 
      // {
	    // $this->_db->beginTransaction();
	    // $this->backendLog(strtoupper($this->_actionID['view']),strtoupper($this->_moduleID['user']),null,null,$error_remark);
        // $this->_db->commit();
	  // }
	  // catch(Exception $e) 
	  // {
	    // $this->_db->rollBack();
  	    // SGO_Helper_GeneralLog::technicalLog($e);
	  // }
	    
		$this->view->error = true;
		$this->view->report_msg = $error_remark;
  	}
  	
  	if($this->_request->isPost() && $this->view->hasPrivilege('MLUD'))
	{
		$maxlimit = $this->_request->getParam('maxlimit');
		$error = false;
		$errorMsg = array();
		
		foreach($maxlimit as $key=>$value)
		{
			$maxlimit[$key] = $maxlimit[$key]?Application_Helper_General::convertDisplayMoney($value):null;
			if(!$maxlimit[$key]) 
			{	
				unset($maxlimit[$key]);
				continue;
			}
			
			if($maxlimit[$key] < 0 || $maxlimit[$key] > 9999999999999)
			{
				$error = true;
				$errorMsg[$key] = $this->language->_('Too many significant digits. Maximum digit allowed : 13 digit(s)');
			}	
			// echo "string";
			// var_dump($maxlimit[$key]);
			// var_dump($result['CUST_LIMIT_IDR']);
			// if((int)$maxlimit[$key] > (int)$result['CUST_LIMIT_IDR']){
			// 	$error = true;
			// 	// echo "string";
			// 	$errorMsg[$key] = $this->language->_('Maximum Amount of Daily Limit in IDR is ').$value;
			// }		
		}
		// var_dump($result['CUST_LIMIT_IDR']);
		// die;

		// 

		if($error)
		{
			$this->view->error = true;
			$this->view->errorMsg = $errorMsg;
			$this->view->maxlimit = $maxlimit;
		}
		else
		{
			try
			{
				$info = 'User Limit, ID = '.$user_id;	
				$this->_db->beginTransaction();
		
				// insert ke T_GLOBAL_CHANGES
				$change_id = $this->suggestionWaitingApproval('User Limit',$info,$this->_changeType['code']['edit'],null,'M_MAKERLIMIT','TEMP_MAKERLIMIT',$user_id,$userName,$cust_id);
				
				//LOOPING HERE JIKA ACCOUNT YANG DI LIMIT BANYAK
				foreach($maxlimit as $key=>$value)
				{
					$data = array(	'CHANGES_ID' => $change_id,
									'USER_LOGIN' => $user_id,
									'ACCT_NO' => (string)$key,
									'CUST_ID' => $cust_id,
									'MAXLIMIT' => $maxlimit[$key],
									'SUGGESTED' => new Zend_Db_Expr('now()'),	
									'SUGGESTEDBY' => $this->_userIdLogin,	
									'MAKERLIMIT_STATUS' => 1
								);
					$this->_db->insert('TEMP_MAKERLIMIT',$data);
				}
				//END LOOP
				
				
				Application_Helper_General::writeLog('MLUD','Customer ID : '.$cust_id.', User ID : '.$user_id);
				$this->_db->commit();
				
				//$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/filter/Filter/acct_no/'.$acct_no.'/user_login/'.$user_id.'/cust_id/'.$cust_id);
				// $this->_redirect('/notification/success');
				$this->_redirect('/notification/submited/index');
			}
			catch(Exception $e) 
			{
				//rollback changes
				$this->_db->rollBack();	
			}
		}
		
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
	}
	if(!$this->_request->isPost())
		Application_Helper_General::writeLog('MLUD','Customer ID : '.$cust_id.', User ID : '.$user_id);
	}
}