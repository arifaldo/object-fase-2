<?php

class Specialcharges_RepairController extends Application_Main {
	
	protected $_moduleDB  = 'HTB';
	
	public function indexAction()
	{
		$params = $this->_request->getParams();
		$keyValue = null;
  		$excludeChangeId  = 'CHANGES_STATUS IN ('.$this->_db->quote($this->_changeStatus['code']['waitingApproval']).','.$this->_db->quote($this->_changeStatus['code']['repairRequested']).')';
  		$excludeChangeId .= ' AND '.$this->_db->quoteInto('CHANGES_FLAG = ?', $this->_applicationFlag);

  		if(is_array($params) && count($params))
  			$fullDesc = Application_Helper_General::displayFullDesc($params);
  		else
  			$fullDesc = null;

  		if(array_key_exists('changes_id', $params))
  		{
  			$filters = array('changes_id' => array('StripTags', 'StringTrim'));
  			$validators = array(
  				'changes_id' => array(
  					'NotEmpty',
  					'Digits',
  					array('Db_RecordExists', array('table' => 'T_GLOBAL_CHANGES', 'field' => 'CHANGES_ID', 'exclude' => $excludeChangeId)),
				//	array('Db_RecordExists', array('table' => 'TEMP_HOLIDAY', 'field' => 'CHANGES_ID')),
					'messages' => array(
						$this->language->_('Cannot be empty'),
						$this->language->_('Invalid format'),
						$this->language->_('Changes ID not found'),
					//	$this->language->_('Changes ID not found')
					),
  				),
  			);
			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $params, $this->_optionsValidator);
	  		if($zf_filter_input->isValid())
	  		{
	  			$year = null;
	  			$changes_id = $zf_filter_input->changes_id;
				$this->view->changes_id = $changes_id;
				$pstypeArr = array(1 => 'RTGS',2 => 'SKN',8 => 'Domestic Online',0 => 'Inhouse');
		
				$this->view->pstype = $pstypeArr;

				$selectpbglobal = $this->_db->select()
					        ->from(array('A' => 'TEMP_CHARGES_WITHIN'),array('*'));
				$selectpbglobal ->where("A.CUST_ID = 'GLOBAL'");
				$selectpbglobal ->where("A.CHANGES_ID = ? ",$changes_id);
				$pbglobal = $this->_db->fetchAll($selectpbglobal);
				 if(!empty($pbglobal)){
					 $pbamount = $pbglobal['0']['AMOUNT'];
				 }else{
					 $pbamount = 0;
				 }
				 //var_dump($pbamount);
				$this->view->pbglobal = $pbamount;
				
				
				$selectpbspecial = $this->_db->select()
							        ->from(array('A' => 'TEMP_CHARGES_WITHIN'),array('*'));
				$selectpbspecial ->where("A.CUST_ID = 'SPECIAL'");
				$selectpbspecial ->where("A.CHANGES_ID = ? ",$changes_id);
				$pbspecial = $this->_db->fetchAll($selectpbspecial);
				 if(!empty($pbspecial)){
					 $pbspecialamount = $pbspecial['0']['AMOUNT'];
				 }else{
					 $pbspecialamount = 0;
				 }
				 //var_dump($pbamount); 
				$this->view->pbspecial = $pbspecialamount;
				
				$selectdomglobal = $this->_db->select()
							        ->from(array('A' => 'TEMP_CHARGES_OTHER'),array('*'));
				$selectdomglobal ->where("A.CUST_ID = 'GLOBAL'");
				$selectdomglobal ->where("A.CHANGES_ID = ? ",$changes_id);
				
				$domglobal = $this->_db->fetchAll($selectdomglobal);
				 
				$this->view->domglobal = $domglobal;
				
				$selectdomspecial = $this->_db->select()
							        ->from(array('A' => 'TEMP_CHARGES_OTHER'),array('*'));
				$selectdomspecial ->where("A.CUST_ID = 'SPECIAL'");
				$selectdomspecial ->where("A.CHANGES_ID = ? ",$changes_id);
				$domspecial = $this->_db->fetchAll($selectdomspecial);
				 
				$this->view->domspecial = $domspecial;
				
				if($this->_request->isPost())
				{
					try{
					$where = array('CHANGES_ID = ?' => $changes_id);
										$this->_db->delete('TEMP_CHARGES_WITHIN',$where);
					$where = array('CHANGES_ID = ?' => $changes_id);
										$this->_db->delete('TEMP_CHARGES_OTHER',$where);
					$resultunion= array('1','2','8');

					$idamt = 'pbspecial';
							$cekamt = $this->_getParam($idamt);
							$amt = Application_Helper_General::convertDisplayMoney($cekamt);
					$cust_id = 'SPECIAL';
					$data= array(
												'CHANGES_ID' 		=> $changes_id,
												'CUST_ID' 			=> $cust_id,
												'ACCT_NO' 			=> '-',
												'AMOUNT' 			=> $amt,
												'BUSINESS_TYPE' 	=> '1',
												'CHARGES_ACCT_NO'	=> NULL,
												'CCY'				=> 'IDR',
												'CHARGES_SUGGESTEDBY'=> $this->_userIdLogin,
												'CHARGES_SUGGESTED'	=> new Zend_Db_Expr('now()')
											);
								// print_r($data);die;
								$this->_db->insert('TEMP_CHARGES_WITHIN',$data);	

					$idamt = 'pbglobal';
							$cekamt = $this->_getParam($idamt);
							$amt = Application_Helper_General::convertDisplayMoney($cekamt);

					$data= array(
												'CHANGES_ID' 		=> $changes_id,
												'CUST_ID' 			=> 'GLOBAL',
												'ACCT_NO' 			=> '-',
												'AMOUNT' 			=> $amt,
												'BUSINESS_TYPE' 	=> '1',
												'CHARGES_ACCT_NO'	=> NULL,
												'CCY'				=> 'IDR',
												'CHARGES_SUGGESTEDBY'=> $this->_userIdLogin,
												'CHARGES_SUGGESTED'	=> new Zend_Db_Expr('now()')
											);
								// print_r($data);die;
								$this->_db->insert('TEMP_CHARGES_WITHIN',$data);	

					$resultunion= array('1','2','8');
					foreach($resultunion as $row)
					{
						
							$idamt = 'typespecial'.$row;
							$cekamt = $this->_getParam($idamt);
							$amt = Application_Helper_General::convertDisplayMoney($cekamt);
							
								$data1 = array(
											'CHANGES_ID'	=> $changes_id,
											'CUST_ID' 		=> $cust_id,
											'CHARGES_NO'	=> NULL,
											'CHARGES_TYPE'	=> $row,
											'CHARGES_CCY' 	=> 'IDR',
											'CHARGES_AMT' 	=> $amt,
											'CHARGES_SUGGESTEDBY'=> $this->_userIdLogin,
											'CHARGES_SUGGESTED'	=> new Zend_Db_Expr('now()')
											);

							$this->_db->insert('TEMP_CHARGES_OTHER',$data1);									
								//echo $row['ACCT_NO'].' aaa<br />';
								//Zend_Debug::dump($data);
									
					}

					foreach($resultunion as $row)
					{
						
							$idamt = 'pstype'.$row;
							$cekamt = $this->_getParam($idamt);
							$amt = Application_Helper_General::convertDisplayMoney($cekamt);
							
								$data1 = array(
											'CHANGES_ID'	=> $changes_id,
											'CUST_ID' 		=> 'GLOBAL',
											'CHARGES_NO'	=> NULL,
											'CHARGES_TYPE'	=> $row,
											'CHARGES_CCY' 	=> 'IDR',
											'CHARGES_AMT' 	=> $amt,
											'CHARGES_SUGGESTEDBY'=> $this->_userIdLogin,
											'CHARGES_SUGGESTED'	=> new Zend_Db_Expr('now()')
											);

							$this->_db->insert('TEMP_CHARGES_OTHER',$data1);									
								//echo $row['ACCT_NO'].' aaa<br />';
								//Zend_Debug::dump($data);
									
					}
					}catch ( Exception $e ) {
					 print_r($e);die;
				
					}


						
					
				}
				
				
			}
		}
		if(!$this->_request->isPost())
		Application_Helper_General::writeLog('HDCR','Viewing Repair Special Charge');
	}

}

?>
