<?php

class marginaldeposit_Model_Marginaldeposit
{
	protected $_db;

	public function __construct()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
	}

	public function getData($filterParam = null, $filter = null)
	{
		$select = $this->_db->select()
			->from(
				array('A' => 'M_MARGINALDEPOSIT'),
				array('*')
			)
			->joinLeft(
				array('B' => 'M_MARGINALDEPOSIT_DETAIL'),
				'B.CUST_ID = A.CUST_ID',
				array('*')
			)
			->joinLeft(
				array('C' => 'M_CUSTOMER'),
				'C.CUST_ID = A.CUST_ID',
				array('CUST_ID', 'CUST_NAME')
			)
			->joinLeft(
				array('D' => 'M_CUST_LINEFACILITY'),
				'D.CUST_ID = A.CUST_ID',
				array('PLAFOND_LIMIT', 'MARGINAL_DEPOSIT')
			);
		// ->joinLeft(
		// 	array('D' => 'T_BANK_GUARANTEE'),'D.CUST_ID = A.CUST_ID',
		// 	array('PLAFOND_LIMIT')
		// );

		if ($filter == TRUE) {
			if ($filterParam['fCustID']) {
				$select->where('A.CUST_ID LIKE ' . $this->_db->quote('%' . $filterParam['fCustID'] . '%'));
			}
			if ($filterParam['fStatus']) {
				$select->where('A.PKS_STATUS LIKE ' . $this->_db->quote('%' . $filterParam['fStatus'] . '%'));
			}
		}

		return $this->_db->fetchAll($select);
	}

	public function getDataDetail($custID)
	{
		$select = $this->_db->select()
			->from(
				array('M_MARGINALDEPOSIT_DETAIL'),
				array('*')
			)
			->where('CUST_ID = ?', $custID);
		return $this->_db->fetchAll($select);
	}

	public function getDataTemp($filterParam = null, $filter = null)
	{
		$select = $this->_db->select()
			->from(
				array('A' => 'TEMP_MARGINALDEPOSIT'),
				array('*')
			)
			->joinLeft(
				array('B' => 'TEMP_MARGINALDEPOSIT_DETAIL'),
				'B.CUST_ID = A.CUST_ID',
				array('*')
			);

		if ($filter == TRUE) {
			if ($filterParam['fCustID']) {
				$select->where('A.CUST_ID LIKE ' . $this->_db->quote('%' . $filterParam['fCustID'] . '%'));
			}
			if ($filterParam['fStatus']) {
				$select->where('A.PKS_STATUS LIKE ' . $this->_db->quote('%' . $filterParam['fStatus'] . '%'));
			}
		}

		return $this->_db->fetchAll($select);
	}

	public function getDataBGAcct($custID)
	{

		$select = $this->_db->select()
			->from(array('A' => 'T_BANK_GUARANTEE'))
			->joinLeft(array('B' => 'T_BANK_GUARANTEE_SPLIT'), 'A.BG_REG_NUMBER = B.BG_REG_NUMBER', array('ACCT'));
		//->where("(BG_STATUS = 15)")
		//->where('CUST_ID = ?', $custID);
		//echo $select;die;

		return $this->_db->fetchAll($select);
	}

	public function getCustAcctById($custID, $list)
	{
		if (empty($list)) $list = [''];
		$select = $this->_db->select()
			->from(
				array('M_CUSTOMER_ACCT'),
				array('ACCT_NO', 'ACCT_NAME', 'CCY_ID', 'ACCT_DESC', 'ACCT_TYPE')
			)
			->where('CUST_ID = ?', $custID)
			->where('ACCT_NO NOT IN (?)', $list);
		return $this->_db->fetchAll($select);
	}

	public function getCustAcctByAcctNo($acctNo)
	{
		$select = $this->_db->select()
			->from(
				array('M_CUSTOMER_ACCT'),
				array('ACCT_NO', 'ACCT_NAME', 'CCY_ID', 'ACCT_TYPE', 'ACCT_DESC', 'ACCT_TYPE')
			)
			->where('ACCT_NO = ?', $acctNo);
		return $this->_db->fetchRow($select);
	}

	public function getBuserById($buserID)
	{
		$select = $this->_db->select()
			->from(
				array('M_BUSER'),
				array('BUSER_ID', 'BUSER_NAME')
			)
			->where('BUSER_ID = ?', $buserID);
		return $this->_db->fetchRow($select);
	}

	public function getDataCharges($CHANGES_ID)
	{

		$select = $this->_db->select()
			->from(array('A' => 'T_GLOBAL_CHANGES'))
			->where('CHANGES_ID = ?', $CHANGES_ID);
		//echo $select;die;

		return $this->_db->fetchAll($select);
	}
}
