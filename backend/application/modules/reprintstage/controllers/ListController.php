<?php
require_once 'General/BankUser.php';
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class Reprintstage_ListController extends Application_Main
{
    public function indexAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->systemType = $system_type;

        $conf = Zend_Registry::get('config');
        $this->_bankName = $conf['app']['bankname'];
        $this->view->masterbankname = $this->_bankName;

        $this->CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

        $fields = array(
            'regno'     => array(
                'field'    => 'BG_REG_NUMBER',
                'label'    => $this->language->_('BG Number') . ' / ' . $this->language->_('Subject'),
            ),
            'app' => array(
                'field' => 'CUST_ID',
                'label' => $this->language->_('Prinsipal'),
            ),

            'obligee' => array(
                'field' => 'RECIPIENT_NAME',
                'label' => $this->language->_('Obligee'),
            ),
            'branch_releaser'  => array(
                'field'    => 'BRANCH_RELEASE',
                'label'    => $this->language->_('Cabang Penerbit')
            ),
            'branch_publisher' => array(
                'field' => 'BRANCH_PRINT',
                'label' => $this->language->_('Cabang Pencetak'),
            ),
            'status' => array(
                'field' => 'BRANCH_PRINT',
                'label' => $this->language->_('Status'),
            ),
        );

        //add reza
        $filterlist = array(
            "BG_NUMBER" => 'BG NUMBER',
            'BG_SUBJECT' => "SUBJECT",
            // 'BG_PERIOD' => "PERIOD TIME",
            "APPLICANT" => "APPLICANT",
            "OBLIGEE_NAME" => "OBLIGEE NAME",
            // "BG_AMOUNT" => "AMOUNT",
            "BRANCH" => "CABANG PENERBIT",
            "CABANG_PENCETAK" => "CABANG PENCETAK",
            // "COUNTER_TYPE" => "COUNTER TYPE"
        );

        $this->view->filterlist = $filterlist;

        $page    = $this->_getParam('page');

        $sortBy  = ($this->_getParam('sortby')) ?  $this->_getParam('sortby') : ('BG_UPDATED');
        $sortBy  = (Zend_Validate::is($sortBy, 'InArray', array(array_keys($fields)))) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

        $sortDir = $this->_getParam('sortdir');
        $sortDir = (Zend_Validate::is($sortDir, 'InArray', array('haystack' => array('asc', 'desc')))) ? $sortDir : 'desc';

        $this->view->currentPage = $page;
        $this->view->sortBy      = $sortBy;
        $this->view->sortDir     = $sortDir;

        //end reza

        $warantyIn = ['1', '2', '3'];
        // Add Bahri
        // if ($this->view->hasPrivilege("ACCS")) { // Cash Collateral
        //     $warantyIn = ['1'];
        // }
        // if ($this->view->hasPrivilege("ANCS")) { // Non-Cash Collateral
        //     $warantyIn = ['2', '3'];
        // }
        // if ($this->view->hasPrivilege("ACCS") && $this->view->hasPrivilege("ANCS")) { // All
        //     $warantyIn = ['1', '2', '3'];
        // }
        $select = $this->_db->select()
            ->from(array('A' => 'M_BUSER'), array('BUSER_BRANCH', "BUSER_ID"))
            ->joinLeft(["BR" => "M_BRANCH"], "A.BUSER_BRANCH = BR.ID")
            ->where('A.BUSER_ID = ?', $this->_userIdLogin);
        $buser = $this->_db->fetchRow($select);
        $branchUser = $buser['BUSER_BRANCH'];
        // END Bahri

        $selectbg = $this->_db->select()
            ->from(array('A' => 'T_BANK_GUARANTEE'), array(
                "BG_NUMBER",
                'REG_NUMBER' => 'BG_REG_NUMBER',
                'SUBJECT' => 'BG_SUBJECT',
                'CREATED' => 'BG_CREATED',
                //'TYPE' => (string)'TYPE',
                'TIME_PERIOD_START',
                'TIME_PERIOD_END',
                'CREATEDBY' => 'BG_CREATEDBY',
                'AMOUNT' => 'BG_AMOUNT',
                'COUNTER_WARRANTY_TYPE',
                'BG_INSURANCE_CODE',
                'FULLNAME' => 'T.USER_FULLNAME',
                'SP_OBLIGEE_CODE',
                'RECIPIENT_NAME',
                'B.BRANCH_NAME',
                'C.CUST_NAME',
                'IS_AMENDMENT' => 'A.CHANGE_TYPE',
                "BG_BRANCH_PUBLISHER",
                "REPRINT"

            ))
            ->join(array('T' => 'M_USER'), 'A.BG_CREATEDBY = T.USER_ID AND A.CUST_ID = T.CUST_ID')
            ->join(array('C' => 'M_CUSTOMER'), 'A.CUST_ID = C.CUST_ID')
            ->join(array('B' => 'M_BRANCH'), 'A.BG_BRANCH = B.BRANCH_CODE')
            ->joinLeft(array('TU' => 'M_BRANCH'), 'A.BG_BRANCH = TU.BRANCH_CODE', ['*', "BRANCH_RELEASE" => "TU.BRANCH_NAME"])
            // ->joinLeft(array('BR' => 'M_BRANCH'), 'BR.ID = TU.ID', [
            //     "BRANCH_RELEASE" => "BR.BRANCH_NAME"
            // ])
            ->joinLeft(array('BP' => 'M_BRANCH'), 'A.BG_BRANCH_PUBLISHER = BP.BRANCH_CODE', [
                "BRANCH_PUBLISHER" => "BP.BRANCH_NAME"
            ])

            //->where('A.CUST_ID ='.$this->_db->quote((string)$this->_custIdLogin))
            ->where('A.BG_STATUS = 15')
            ->where("A.BG_BRANCH_PUBLISHER = '" . $buser["BRANCH_CODE"] . "' OR TU.ID = '" . $buser["BUSER_BRANCH"] . "'")
            ->where("A.SIGNING_STATUS = 2")
            ->where("A.REPRINT IN (1, 2, 3, 4)")
            ->where('A.COUNTER_WARRANTY_TYPE IN (?)', $warantyIn) // Add Bahri
            ->order('A.BG_CREATED DESC');

        //->query()->fetchAll();
        // $auth = Zend_Auth::getInstance()->getIdentity();
        // if ($auth->userHeadQuarter == "NO") {
        //     $selectbg->where('B.ID = ?', $branchUser); // Add Bahri
        // }

        // select branch ------------------------------------------------------------
        $select_branch = $this->_db->select()
            ->from(array('A' => 'M_BRANCH'), array("BRANCH_CODE", "BRANCH_NAME"))
            ->query()->fetchAll();

        $save_branch = [];

        foreach ($select_branch as $key => $value) {
            $save_branch[$value["BRANCH_CODE"]] = $value["BRANCH_NAME"];
        }

        $this->view->sel_branch = $save_branch;
        //  --------------------------------------------------------------------------

        // select counter type ------------------------------------------------------------
        $save_counter_type = [
            1 => 'FC',
            2 => 'LF',
            3 => 'Insurance'
        ];

        $this->view->counter_type = $save_counter_type;
        //  -------------------------------------------------------------------------- 

        $selectlc = $this->_db->select()
            ->from(array('A' => 'T_LC'), array(
                'REG_NUMBER' => 'LC_REG_NUMBER',
                'SUBJECT' => 'LC_CREDIT_TYPE',
                'CREATED' => 'LC_CREATED',
                'CCYID' => 'LC_CCY',
                //'TYPE' => (string)'TYPE',
                'TIME_PERIOD_END' => 'LC_EXPDATE',
                'CREATEDBY' => 'LC_CREATEDBY',
                'AMOUNT' => 'LC_AMOUNT',

                'FULLNAME' => 'T.USER_FULLNAME'
            ))
            ->join(array('T' => 'M_USER'), 'A.LC_CREATEDBY = T.USER_ID')
            //->where('A.LC_CUST ='.$this->_db->quote((string)$this->_custIdLogin))
            ->where('A.LC_STATUS = 1')
            ->order('A.LC_CREATED DESC');
        // ->query()->fetchAll();


        //$result = array_merge($selectbg, $selectlc);

        //$this->paging($result);

        $conf = Zend_Registry::get('config');


        $this->view->bankname = $conf['app']['bankname'];


        $config     = Zend_Registry::get('config');
        $BgType     = $config["bg"]["status"]["desc"];
        $BgCode     = $config["bg"]["status"]["code"];

        $arrStatus = array_combine(array_values($BgCode), array_values($BgType));

        $this->view->arrStatus = $arrStatus;

        $arrWaranty = array(
            1 => 'FC',
            2 => 'LF',
            3 => 'INS'
        );
        $this->view->arrWaranty = $arrWaranty;

        $arrType = array(
            1 => 'Standart',
            2 => 'Custom'
        );

        $arrLang = array(
            1 => 'Indonesian',
            2 => 'English',
            3 => 'Bilingual'
        );
        $this->view->langArr = $arrLang;
        $this->view->formatArr = $arrType;
        $this->view->fields = $fields;


        $filterArr = array(
            'BG_NUMBER'   => array('StripTags', 'StringTrim', 'StringToUpper'),
            'BG_SUBJECT'      => array('StripTags', 'StringTrim', 'StringToUpper')
        );

        $dataParam = array("BG_NUMBER", "BG_SUBJECT", "BG_PERIOD", "APPLICANT", "OBLIGEE_NAME", "BG_AMOUNT", "BRANCH", "COUNTER_TYPE", "CABANG_PENCETAK");
        $dataParamValue = array();

        foreach ($dataParam as $dtParam) {
            if (!empty($this->_request->getParam('wherecol'))) {
                $dataval = $this->_request->getParam('whereval');
                foreach ($this->_request->getParam('wherecol') as $key => $value) {
                    if ($dtParam == $value) {

                        if (empty($dataParamValue[$dtParam])) {
                            $dataParamValue[$dtParam] = [];
                        }
                        array_push($dataParamValue[$dtParam], $dataval[$key]);

                        // $dataParamValue[$dtParam] = $dataval[$key];
                    }
                }
            }
        }

        if (!empty($this->_request->getParam('efdate'))) {
            $efdatearr = $this->_request->getParam('efdate');
            $dataParamValue['BG_PERIOD'] = $efdatearr[0];
            $dataParamValue['BG_PERIOD_END'] = $efdatearr[1];
        }

        // var_dump($dataParamValue);
        // die();

        $validator = array(
            // 'BG_NUMBER'  => array(),
            // 'BG_SUBJECT'      => array(),
            'BG_PERIOD'    => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
            'BG_PERIOD_END'    => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
        );

        // echo "<pre>";
        // print_r($dataParamValue);die;


        $zf_filter   = new Zend_Filter_Input($filterArr, $validator, $dataParamValue);

        if ($zf_filter->isValid()) {
            $filter     = TRUE;
        }

        $bgNubmer  = $dataParamValue["BG_NUMBER"];
        $applicant = $dataParamValue["APPLICANT"];
        $OBLIGEE_NAME = $dataParamValue["OBLIGEE_NAME"];

        $amount = $dataParamValue["BG_AMOUNT"];
        $branch = $dataParamValue["BRANCH"];
        $counttype = $dataParamValue["COUNTER_TYPE"];
        // $bgNubmer  = html_entity_decode($zf_filter->getEscaped('BG_NUMBER'));
        $bgSubject  = $dataParamValue["BG_SUBJECT"];
        $cabangPencetak  = $dataParamValue["CABANG_PENCETAK"];
        // $bgSubject  = html_entity_decode($zf_filter->getEscaped('BG_SUBJECT'));
        $datefrom    = html_entity_decode($zf_filter->getEscaped('BG_PERIOD'));
        $dateto    = html_entity_decode($zf_filter->getEscaped('BG_PERIOD_END'));


        // if ($filter == null) {
        //   $datefrom = (date("d/m/Y"));
        //   $dateto = (date("d/m/Y"));
        //   $this->view->fDateFrom  = (date("d/m/Y"));
        //   $this->view->fDateTo  = (date("d/m/Y"));
        // }

        // if ($filter_clear == '1') {
        //   $this->view->fDateFrom  = '';
        //   $this->view->fDateTo  = '';
        //   $datefrom = '';
        //   $dateto = '';
        // }

        if ($filter == null || $filter == TRUE) {
            $append_query = "";

            $this->view->fDateFrom = $datefrom;
            $this->view->fDateTo = $dateto;

            if (!empty($datefrom)) {
                $FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
                $datefrom  = $FormatDate->toString($this->_dateDBFormat);
            }
            if (!empty($dateto)) {
                $FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
                $dateto    = $FormatDate->toString($this->_dateDBFormat);
            }
            if (!empty($datefrom) && empty($dateto)) {
                if ($append_query != "") {
                    $append_query .= " AND ";
                }
                $append_query .= "A.TIME_PERIOD_START >= " . $this->_db->quote($datefrom);
                // $selectbg->where("A.TIME_PERIOD_START >= " . $this->_db->quote($datefrom));
            }

            if (empty($datefrom) && !empty($dateto)) {
                if ($append_query != "") {
                    $append_query .= " AND ";
                }
                $append_query .= "A.TIME_PERIOD_END <= " . $this->_db->quote($dateto);
                // $selectbg->where("A.TIME_PERIOD_END <= " . $this->_db->quote($dateto));
            }

            if (!empty($datefrom) && !empty($dateto)) {
                if ($append_query != "") {
                    $append_query .= " AND ";
                }
                $append_query .= "A.TIME_PERIOD_START >= " . $this->_db->quote($datefrom) . " and A.TIME_PERIOD_END <= " . $this->_db->quote($dateto);
                // $selectbg->where("A.TIME_PERIOD_START >= " . $this->_db->quote($datefrom) . " and A.TIME_PERIOD_END <= " . $this->_db->quote($dateto));
            }

            // if ($bgNubmer != null) {
            if (count($bgNubmer) > 0) {
                foreach ($bgNubmer as $key => $value) {
                    if ($append_query != "") {
                        $append_query .= " AND ";
                    }
                    $append_query .= "A.BG_NUMBER LIKE " . $this->_db->quote('%' . $value . '%');
                    // $this->view->bgNubmer = $bgNubmer;
                }
            }

            // if ($bgSubject != null) {
            // $this->view->bgSubject = $bgSubject;
            if (count($bgSubject) > 0) {
                foreach ($bgSubject as $key => $value) {
                    if ($append_query != "") {
                        $append_query .= " AND ";
                    }
                    $append_query .= "A.BG_SUBJECT LIKE " . $this->_db->quote('%' . $value . '%');
                }
            }

            if (count($applicant) > 0) {
                foreach ($applicant as $key => $value) {
                    if ($append_query != "") {
                        $append_query .= " AND ";
                    }
                    $append_query .= "C.CUST_NAME LIKE " . $this->_db->quote('%' . $value . '%');
                }
            }

            if (count($OBLIGEE_NAME) > 0) {
                foreach ($OBLIGEE_NAME as $key => $value) {
                    if ($append_query != "") {
                        $append_query .= " AND ";
                    }
                    $append_query .= "A.RECIPIENT_NAME LIKE " . $this->_db->quote('%' . $value . '%');
                }
            }

            if (count($amount) > 0) {
                foreach ($amount as $key => $value) {
                    if ($append_query != "") {
                        $append_query .= " AND ";
                    }
                    $append_query .= "A.BG_AMOUNT LIKE " . $this->_db->quote('%' . $value . '%');
                }
            }

            if (count($branch) > 0) {
                foreach ($branch as $key => $value) {
                    if ($append_query != "") {
                        $append_query .= " AND ";
                    }
                    $append_query .= "TU.BRANCH_CODE LIKE " . $this->_db->quote('%' . $value . '%');
                }
            }

            if (count($cabangPencetak) > 0) {
                foreach ($cabangPencetak as $key => $value) {
                    if ($append_query != "") {
                        $append_query .= " AND ";
                    }
                    $append_query .= "BP.BRANCH_CODE LIKE " . $this->_db->quote('%' . $value . '%');
                }
            }

            if (count($counttype) > 0) {
                foreach ($counttype as $key => $value) {
                    if ($append_query != "") {
                        $append_query .= " AND ";
                    }
                    $append_query .= "A.COUNTER_WARRANTY_TYPE LIKE " . $this->_db->quote('%' . $value . '%');
                }
            }

            if ($append_query != "") {
                $selectbg->where($append_query);
            }
        }

        // $selectbg->order($sortBy.' '.$sortDir);


        $this->view->fields = $fields;
        $this->view->filter = $filter;

        $selectbg = $this->_db->fetchAll($selectbg);
        $selectlc = $this->_db->fetchAll($selectlc);
        $result = array_merge($selectbg, $selectlc);

        $setting = new Settings();
        $enc_pass = $setting->getSetting('enc_pass');
        $enc_salt = $setting->getSetting('enc_salt');
        $sessionNamespace = new Zend_Session_Namespace('Tokenenc');
        $pw_hash = md5($enc_salt . $enc_pass);
        $rand = $this->_userIdLogin . date('dHis') . $pw_hash;
        $sessionNamespace->token   = $rand;
        $this->view->token = $sessionNamespace->token;

        foreach ($result as $key => $value) {
            $get_reg_number = $value["REG_NUMBER"];

            $AESMYSQL = new Crypt_AESMYSQL();
            $rand = $this->token;

            $encrypted_payreff = $AESMYSQL->encrypt($get_reg_number, $rand);
            $encpayreff = urlencode($encrypted_payreff);

            $result[$key]["REG_NUMBER_ENCRYPTED"] = $encpayreff;
        }

        //Zend_Debug::dump($result);

        $this->paging($result);

        $csv = $this->_getParam('csv');
        if ($this->_request->isPost()) {
            if ($csv) {

                $array_header = [
                    $this->language->_('No'),
                    $this->language->_('BG Number / Subject'),
                    $this->language->_('Prinsipal'),
                    $this->language->_('Obligee'),
                    $this->language->_('Cabang Penerbit'),
                    $this->language->_('Cacbang Pencetak'),
                    $this->language->_('Status'),
                ];

                $csv_data = [];
                foreach ($result as $key => $value) {

                    if ($value['IS_AMENDMENT'] == 1) {
                        $type = 'Amendment Changes';
                    } else if ($value['IS_AMENDMENT'] == 0) {
                        $type = "New";
                    } else {
                        $type = "Amendment Draft";
                    }

                    $status = $this->view->arrStatus[$value["BG_STATUS"]];

                    if ($value["BG_STATUS"] == 17) {
                        $signing_status = (empty($value["SIGNING_STATUS"])) ? 0 : $value["SIGNING_STATUS"];
                        $status = $this->view->arrStatus[$value["BG_STATUS"]] . " (" . strval($signing_status) . "/2)";
                    }

                    switch ($value["REPRINT"]) {
                        case '1':
                            $status = "Menunggu persetujuan cetak ulang";
                            break;

                        case '2':
                            $status = "Siap Cetak";
                            break;

                        case '3':
                            $status = "Pencetakan ulang ditolak";
                            break;

                        case '4':
                            $status = "Telah tercetak";
                            break;

                        default:
                            $status = "-";
                            break;
                    }

                    $temp = [
                        $key + 1,
                        $value["BG_NUMBER"] . " / " . $value["SUBJECT"],
                        $value['CUST_NAME'] . "(" . $value['CUST_ID'] . ")",
                        $value['RECIPIENT_NAME'],
                        $value["BRANCH_RELEASE"],
                        $value["BRANCH_PUBLISHER"],
                        $status
                    ];

                    array_push($csv_data, $temp);
                }

                Application_Helper_General::writeLog('RSBG', 'Download CSV Reprinting Stage List');
                $this->_helper->download->csv($array_header, $csv_data, null, $this->language->_('Reprinting Stage List') . " - " . date("dMYHis"));
            }
        }

        if (!empty($dataParamValue)) {

            $this->view->efdateStart = $dataParamValue['BG_PERIOD'];
            $this->view->efdateEnd = $dataParamValue['BG_PERIOD_END'];

            foreach ($dataParamValue as $key => $value) {
                $duparr = explode(',', $value);
                if (!empty($duparr)) {

                    foreach ($duparr as $ss => $vs) {
                        $wherecol[]  = $key;
                        $whereval[] = $vs;
                    }
                } else {
                    if (count($value) > 1) {
                        foreach ($value as $k) {
                            $wherecol[]  = $key;
                            $whereval[] = $k;
                        }
                    } else {
                        $wherecol[]  = $key;
                        $whereval[] = $value;
                    }
                }
            }
            $this->view->wherecol     = $wherecol;
            $this->view->whereval     = $whereval;
        }
    }
}
