<?php

Class businessoperations_Model_Businessoperations
{
	protected $_db;

	public function __construct()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
	}

	public function getData($filterParam,$filter)
	{
		$select = $this->_db->select()
			->from(
				array('A' => 'M_CUST_LINEFACILITY'),
				array('*')
			)
			->joinLeft(
				array('B' => 'M_CUSTOMER'),'B.CUST_ID = A.CUST_ID',
				array('CUST_NAME','COLLECTIBILITY_CODE')
			);

		if($filter == TRUE){
			if($filterParam['fCompanyCode']){
				$select->where('A.CUST_ID LIKE '.$this->_db->quote('%'.$filterParam['fCompanyCode'].'%'));
			}
			if($filterParam['fCompanyName']){
				$select->where('B.CUST_NAME LIKE '.$this->_db->quote('%'.$filterParam['fCompanyName'].'%'));
			}
			if($filterParam['fCollectCode']){
				$select->where('B.COLLECTIBILITY_CODE = ?', $filterParam['fCollectCode']);
			}
		}
		return $this->_db->fetchAll($select);
	}

	public function getDataById($id)
	{
		$data = $this->_db->select()
						  ->from(array('A' => 'M_CUST_LINEFACILITY'),array('*'))
						  ->joinLeft(
						  		array('B' => 'M_CUSTOMER'),'B.CUST_ID = A.CUST_ID',
						  		array('CUST_NAME','COLLECTIBILITY_CODE')
							)
						  ->where('A.ID = ?', $id);
		return $this->_db->fetchRow($data);
	}

	public function getCreditQuality()
	{
	$select = $this->_db->select()
						->from('M_CREDIT_QUALITY')
						->query()->fetchAll();
	return $select;
	}

	public function getCustomer()
	{
	$cust_id = $this->_db->select()
						->from(array('M_CUST_LINEFACILITY'),array('CUST_ID'))
						->query()->fetchAll();
	$select = $this->_db->select()
						->from('M_CUSTOMER')
						->where('CUST_ID NOT IN (?)', $cust_id)
						->query()->fetchAll();
	return $select;
	}
}