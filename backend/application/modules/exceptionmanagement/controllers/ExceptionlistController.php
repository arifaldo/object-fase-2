<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';

/*
*	T_PSLIP
*	PS_STATUS : 9 / SENDFILE_STATUS : 0 => Exception Source -> Escrow
*	PS_STATUS : 9 / SENDFILE_STATUS : 2 => Escrow -> Bene
*	PS_STATUS : 9 / SENDFILE_STATUS : 3 => Reversal
*/
class exceptionmanagement_ExceptionlistController extends Application_Main
{
	public function indexAction()
	{

		$setting = new Settings();			  	
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
		$pw_hash = md5($enc_salt.$enc_pass);
		$rand = $this->_userIdLogin.date('dHis').$pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;
	
		$this->_helper->layout()->setLayout('newlayout');
		$arrPayType 	= Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
		// print_r($arrPayType);die;
		$this->view->arrPayType = $arrPayType;
		// $select = $this->_db->select()->distinct()
		// 					->from(array('A' => 'M_CUSTOMER'),array('CUST_ID'))
		// 					->order('CUST_ID ASC')
		// 		 			-> query()->fetchAll();
		$typearr = array_combine(array_values($this->_paymenttype['code']),array_values($this->_paymenttype['desc']));
		// $this->view->var=$select;
		
		$releaseDate = $this->language->_('Release Date');
		$paymentRef = $this->language->_('Payment Ref').'#';
		$companyCode = $this->language->_('Company Code');
		$companyName = $this->language->_('Company');
		$releaseDesc = $this->language->_('Release Description');
		$paymentType = $this->language->_('Payment Type');
		
		
		$fields = array(
						'release'      		=> array('field' => 'release',
											     'label' => $releaseDate,
											     'sortable' => true),
						'payref'      		=> array('field' => 'payref',
											      'label' => $paymentRef,
											      'sortable' => true),
						// 'custid'            => array('field' => 'custid',
						// 					      'label' => $companyCode,
						// 					      'sortable' => true),
						'custname'      	=> array('field' => 'custname',
											      'label' => $companyName,
											      'sortable' => true),
						'description'    	=> array('field' => 'description',
											      'label' => $releaseDesc,
											      'sortable' => true),
						'type'     			=> array('field' => 'type',
											      'label' => $paymentType,
											      'sortable' => true),);

		$filterlist = array('SUGEST_DATE','COMP_CODE','COMP_NAME','PS_NUM','PS_TYPE');
		
		$this->view->filterlist = $filterlist;
	      
		
		$page    = $this->_getParam('page');
		
		$page = (Zend_Validate::is($page,'Digits'))? $page : 1;
		$sortBy  = $this->_getParam('sortby');
   		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
   		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
		
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;

		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');
		
		$caseType = "(CASE A.PS_TYPE ";
  		foreach($typearr as $key=>$val)
  		{
   			$caseType .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseType .= " END)";

  		$select2 = $this->_db->select()->distinct()
					        ->from(array('A' => 'T_PSLIP'),array())
					        ->join(array('B' => 'T_TRANSACTION'), 'A.PS_NUMBER = B.PS_NUMBER',array())
					        ->join(array('C' => 'M_CUSTOMER'), 'A.CUST_ID = C.CUST_ID',array('release'			=>	'A.PS_EFDATE',
					        																 'releaseview'		=>	'A.PS_EFDATE',
					        																 'payref'			=>	'A.PS_NUMBER',
					        																 'custid'			=>	'C.CUST_ID',
					        																 'custname'			=>	'C.CUST_NAME',
					        																 'company'	=> new Zend_Db_Expr("CONCAT(C.CUST_NAME , ' (' , C.CUST_ID , ')  ' )"),
					        																 'description' 		=>	new Zend_Db_Expr("'PAYMENT EXCEPTION'"),
					        																 'type'				=>	$caseType,));
		$select2 -> where("A.PS_STATUS = '9'");

		$filterArr = array(	'filter' 			=> array('StripTags','StringTrim'),
						   	'PS_TYPE'  			=> array('StripTags'),
						   	'PS_NUM' 		=> array('StripTags','StringTrim','StringToUpper'),
	                       	'COMP_CODE'    		=> array('StripTags','StringTrim','StringToUpper'),
	                       	'COMP_NAME'  		=> array('StripTags','StringToUpper'),
							'status'  			=> array('StripTags'),
							'SUGEST_DATE' 		=> array('StripTags','StringTrim'),
							'SUGEST_DATE_END' 			=> array('StripTags','StringTrim'),
	                      );
	                      
	    $validator = array(	'filter' 			=> array(),
						   	'PS_TYPE'  			=> array(),
						   	'PS_NUM' 		=> array(),
	                       	'COMP_CODE'    		=> array(),
	                       	'COMP_NAME'  		=> array(),
							'status'  			=> array(),
							'SUGEST_DATE' 		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
							'SUGEST_DATE_END' 			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
	                      );

	    $dataParam = array("PS_TYPE","PS_NUM","COMP_CODE","COMP_NAME");
		$dataParamValue = array();
		// print_r($this->_request->getParam('wherecol'));die;
		foreach ($dataParam as $dtParam)
		{
			
			// print_r($dtParam);die;
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}
				
			}
				
			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}	
			if(!empty($this->_request->getParam('sugestdate'))){
				$createarr = $this->_request->getParam('sugestdate');
					$dataParamValue['SUGEST_DATE'] = $createarr[0];
					$dataParamValue['SUGEST_DATE_END'] = $createarr[1];
			}
	                      
	    $zf_filter 		= new Zend_Filter_Input($filterArr,$validator,$dataParamValue);
	    // $filter 		= $zf_filter->getEscaped('filter');
	    $filter 		= $this->_getParam('filter');
	    $paytype 		= html_entity_decode($zf_filter->getEscaped('PS_TYPE'));
	    $paymenref 		= html_entity_decode($zf_filter->getEscaped('PS_NUM'));
	    $custid 		= html_entity_decode($zf_filter->getEscaped('COMP_CODE'));
		$custname 		= html_entity_decode($zf_filter->getEscaped('COMP_NAME'));
	    $status 		= html_entity_decode($zf_filter->getEscaped('status'));
	    $datefrom 		= html_entity_decode($zf_filter->getEscaped('SUGEST_DATE'));
	    $dateto 		= html_entity_decode($zf_filter->getEscaped('SUGEST_DATE_END'));

		if($filter == null)
		{	$datefrom = (date("d/m/Y"));
			$dateto = (date("d/m/Y"));
			$this->view->fDateFrom  = (date("d/m/Y"));
			$this->view->fDateTo  = (date("d/m/Y"));
		}
		
	    
	    if($filter == null || $filter =='Set Filter')
		{
			$this->view->fDateFrom = $datefrom;
			$this->view->fDateTo = $dateto;
		if(!empty($datefrom))
	            {
	            	$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
					$datefrom  = $FormatDate->toString($this->_dateDBFormat);	
	            }
		if(!empty($datefrom))
	            {
	            	$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
					$datefrom  = $FormatDate->toString($this->_dateDBFormat);	
	            }
	            
	    if(!empty($dateto))
	            {
	            	$FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
					$dateto    = $FormatDate->toString($this->_dateDBFormat);
	            }
		
		
		if(!empty($datefrom) && empty($dateto))
	            $select2->where("DATE(PS_EFDATE) >= ".$this->_db->quote($datefrom));
	            
	   	if(empty($datefrom) && !empty($dateto))
	            $select2->where("DATE(PS_EFDATE) <= ".$this->_db->quote($dateto));
	            
	    if(!empty($datefrom) && !empty($dateto))
	            $select2->where("DATE(PS_EFDATE) between ".$this->_db->quote($datefrom)." and ".$this->_db->quote($dateto));
		}
		
		if($filter == 'Set Filter')
		{
	    	if($paytype){
				$this->view->paytype = $paytype;
				$paytype 	 	= explode(",", $paytype);
				$select2->where("A.PS_TYPE in (?) ", $paytype);		
			}
	       
			if($paymenref){
	       		$this->view->paymenref = $paymenref;
				$paymenref = '%'.$paymenref.'%';
	       		$select2->where("A.PS_NUMBER LIKE ".$this->_db->quote($paymenref));}
	       
			if($custid){
	       		$this->view->custid = $custid;
	       		$select2->where("C.CUST_ID LIKE ".$this->_db->quote($custid));}
	     
			if($custname){
	       		$this->view->custname = $custname;
	       		$select2->where("C.CUST_NAME LIKE ".$this->_db->quote('%'.$custname.'%'));}
		}

		$select2->order($sortBy.' '.$sortDir);
		if($csv || $pdf || $this->_request->getParam('print'))
		{
			$arr = $this->_db->fetchAll($select2);
			foreach ($arr as $key => $value)
			{
				$arr[$key]["releaseview"]= Application_Helper_General::convertDate($value["releaseview"],$this->view->viewDateFormat,$this->view->defaultDateFormat);

				unset($arr[$key]["release"]);
			}
			
			if($csv)
			{
				Application_Helper_General::writeLog('EXLS','Download CSV Payment Exception List');
				$this->_helper->download->csv(array($this->language->_('Release Date'),$this->language->_('Payment Ref#'),$this->language->_('Company Code'),$this->language->_('Company Name'), $this->language->_('Release Description'), $this->language->_('Payment Type')),$arr,null,$this->language->_('Payment Exception List'));
			}
			
			if($pdf)
			{
				Application_Helper_General::writeLog('EXLS','Download PDF Payment Exception List');
				$this->_helper->download->pdf(array($this->language->_('Release Date'),$this->language->_('Payment Ref#'),$this->language->_('Company Code'),$this->language->_('Company Name'), $this->language->_('Release Description'), $this->language->_('Payment Type')),$arr,null,$this->language->_('Payment Exception List'));
			}
			
			if($this->_request->getParam('print') == 1){
				$data = $this->_db->fetchAll($select2);
				foreach ($data as $key => $value)
				{
					$data[$key]["release"]= Application_Helper_General::convertDate($value["releaseview"],$this->view->viewDateFormat,$this->view->defaultDateFormat);
	
//					unset($arr[$key]["release"]);
				}
				unset($fields['action']);
				$this->_forward('print', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'Payment Exception List', 'data_header' => $fields));
			}
		}
		else
		{
			Application_Helper_General::writeLog('EXLS','Viewing Payment Exception List');
		}

		$this->view->fields = $fields;
		$this->view->filter = $filter;
		$this->paging($select2);
		// print_r($dataParamValue);die;
		unset($dataParamValue['SUGEST_DATE_END']);
		if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     
      }
      // print_r($wherecol);
	}
}
?>
