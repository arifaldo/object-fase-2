<?php
require_once 'CMD/Validate/Validate.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'General/Settings.php';

class user_NewController extends user_Model_User
{

  public function indexAction()
  {
    $this->_helper->layout()->setLayout('newlayout');

    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');
    $password = $sessionNamespace->token;
    $this->view->token = $sessionNamespace->token;

    $AESMYSQL = new Crypt_AESMYSQL();
    $cust_id = $AESMYSQL->decrypt($this->_getParam('cust_id'), $password);
    $cust_id = (Zend_Validate::is($cust_id, 'Alnum') && Zend_Validate::is($cust_id, 'StringLength', array('min' => 1, 'max' => $this->_custIdLength))) ? $cust_id : null;
    //  var_dump($this->_request->getParams());die;
    //var_dump($cust_id);
    $this->view->enccust_id = $this->_getParam('cust_id');
    $user_id = strtoupper($this->_getParam('user_id'));


    if ($this->_getParam('req_id')) {
      $this->view->req_idlist = $this->_getParam('req_id');
    }
    $error_remark = null;
    $this->view->button = true;
    $this->view->user_msg  = array();

    $this->view->priviView = array();

    $setting = new Settings();
    $minUserID = $setting->getSetting('min_length_userid');
    $maxUserID = $setting->getSetting('max_length_userid');
    $system_type = $setting->getSetting('system_type');
    $this->view->minUserID = $minUserID;
    $this->view->maxUserID = $maxUserID;

    // $resultTmpChange = $this->getTempCustomerId($cust_id);
    // // var_dump($resultTmpChange);die;
    //   if($resultTmpChange)  $temp = 0; 
    //   else  $temp = 1; 

    $getprivilege = $this->getPrivilege($system_type);
    if ($cust_id) {

      if ($system_type != '2') {
        $selectccy = $this->_db->select()
          ->from('M_MINAMT_CCY', array('CCY_ID', 'CCY_NUM'))
          ->where('CCY_ID IN ("IDR","USD")');

        $ccylist = $this->_db->fetchAll($selectccy);
      } else {
        $selectccy = $this->_db->select()
          ->from('M_MINAMT_CCY', array('CCY_ID', 'CCY_NUM'))
          // ->where('CCY_ID IN ("IDR","USD")');
          ->where('CCY_ID= ? ', 'IDR');

        $ccylist = $this->_db->fetchAll($selectccy);
      }

      $this->view->ccylist = $ccylist;

      // ->where('UPPER(CHANGES_TYPE)= ? ','N')
      // ->where('UPPER(CHANGES_STATUS) IN ("WA","RJ")')
      // ->where('UPPER(COMPANY_CODE)='.$this->_db->quote((string)$cust_id));


      $result = $this->_db->select()
        ->from('M_CUSTOMER', array('CUST_ID', 'CUST_STATUS', 'CUST_CIF', 'CUST_LIMIT_USD', 'CUST_LIMIT_IDR'))
        ->where('UPPER(CUST_ID)=' . $this->_db->quote((string)$cust_id))
        //->where('CUST_STATUS=1');
        ->where('CUST_STATUS!=3')
        ->query()->fetch();

      if (!$result['CUST_ID']) $cust_id = null;


      if (!$result) $cust_id = null;
      else {
        $companyLim = array();
        $companyLim['IDR'] = Application_Helper_General::convertDisplayMoney($result['CUST_LIMIT_IDR']);
        $companyLim['USD'] = Application_Helper_General::convertDisplayMoney($result['CUST_LIMIT_USD']);
        $this->view->limitidr = $result['CUST_LIMIT_IDR'];
        $this->view->limitusd = $result['CUST_LIMIT_USD'];
      }


      $cust_status = $result['CUST_STATUS'];
    } else {

      $this->_redirect('/authorizationacl/index/forbidden');
    }



    //var_dump($cust_id);
    //var_dump($result);die;

    $app = Zend_Registry::get('config');
    $app = $app['app']['bankcode'];

    $core = array();
    if (substr($result['CUST_CIF'], 0, 3) != 'CIF') {
      $svcAccount = new Service_Account($result['CUST_CIF'], Application_Helper_General::getCurrNum('IDR'), $app);
      $result   = $svcAccount->cifaccountInquiry($core);
    } else {
      $result = array();
    }
    // print_r($result);die();

    if ($core->responseCode == '0000' || $result['ResponseCode'] == '0000') {
      $arrResult = array();
      $err = array();

      if (count($core->accountList) > 1) {
        // print_r($core->accountList);die();
        //array data lebih dari 1
        foreach ($core->accountList as $key => $val) {
          if ($val->status == '0') {
            $arrResult = array_merge($arrResult, array($val->accountNo => $val));
            array_push($err, $val->accountNo);
          }
          //              echo 'here';
        }
      } else {
        //array kurang dari 2
        foreach ($core as $data) {
          if ($data->status == '0') {
            $arrResult = array_merge($arrResult, array($data->accountNo => $data));
            array_push($err, $data->accountNo);
          }
        }
      }

      $final = $arrResult;
      if (count($final) < 1) {
        $this->view->data1 = '0';
      } else {
        $this->view->data = $arrResult;
      }

      $dataProPlan = array('xxxxx');
      foreach ($final as $dataProductPlan) {
        if ($dataProductPlan->status == '0') {
          if (isset($dataProductPlan->productType)) {
            $select_product_type  = $this->_db->select()
              ->from(array('M_PRODUCT_TYPE'), array('PRODUCT_NAME'))
              ->where("PRODUCT_CODE = ?", $dataProductPlan->productType);
            $userData_product_type = $this->_db->fetchAll($select_product_type);
            //              
            // print_r($userData_product_type);
            $dataProductPlan->planName = $userData_product_type[0]['PRODUCT_NAME'];
          } else {
            $dataProductPlan = $dataProductPlan;
          }

          $dataUser[] = $dataProductPlan;
        }
      }
    }
    //echo '<pre>';

    //     var_dump($active);die();

    $this->view->data = $dataUser;

    // var_dump($cust_id);
    if (!$cust_id) {
      $error_remark = 'Invalid Customer ID';

      $this->_helper->getHelper('FlashMessenger')->addMessage('F');
      $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
      // $this->_redirect($this->_backURL);
    }

    $setting  = new Settings();
    $tokenType = $setting->getSetting('tokentype', '');
    $this->view->tokenType = $tokenType;

    $hardTokenTypeCode = $this->_tokenType['code']['hardtoken'];
    $this->view->hardTokenTypeCode = $hardTokenTypeCode;

    // echo $temp;die();

    // if($this->_request->isPost() && $temp == 1)
    if ($this->_request->isPost()) {

      //if($this->_getParam('submit') == 'Submit')
      if ($this->_getParam('submit') == TRUE) {

        // echo $cust_id;die();
        $userfullnameGet = $this->_getParam('user_fullname');
        $userfullnameGetSub = preg_replace("/[^0-9a-zA-Z]/", "", $userfullnameGet);
        $userfullname = strtoupper(substr($userfullnameGetSub, 0, 9));

        $generate_userid = $this->_getParam('generate_userid');
        $squenceNumberUser = $this->_getParam('squenceNumberUser');

        // $hasil_userid = $generate_userid.$userfullname.$squenceNumberUser;
        $hasil_userid = $user_id;

        $exclude_fgroup_id = '(UPPER(CUST_ID)=' . $this->_db->quote((string)$cust_id);
        $exclude_fgroup_id .= ' OR UPPER(CUST_ID)=' . $this->_db->quote('BANK');
        $exclude_fgroup_id .= ') AND UPPER(FGROUP_STATUS)=' . $this->_db->quote(strtoupper($this->_masterStatus['code']['active']));

        //get privilege id from html
        $priviId    = array();
        $filterPrivilege    = array();
        $validatorPrivilege = array();
        $privcheck = false;
        foreach ($getprivilege as $privi) {
          $priviParam = $this->_getParam($privi['FPRIVI_ID']);

          if ($priviParam == 'yes') {
            $privcheck = true;
            $priviId[] = strip_tags(trim($privi['FPRIVI_ID']));
            $filterPrivilege[$privi['FPRIVI_ID']]    = array('StripTags', 'StringTrim');
            $validatorPrivilege[$privi['FPRIVI_ID']] = array(
              'NotEmpty',
              array('StringLength', array('min' => 1, 'max' => 3)),
              'messages' => array(
                $this->language->_('invalid'),
                $this->language->_('invalid')
              )
            );
          }
        }


        $filters = array( //'user_id'      => array('StripTags','StringTrim','StringToUpper'),
          'user_id'        => array('StripTags', 'StringToUpper', 'StringTrim'),
          'user_email'     => array('StripTags', 'StringTrim'),
          'user_fullname'  => array('StripTags', 'StringTrim'),
          'user_phone'     => array('StripTags', 'StringTrim'),
          'user_ext'             => array('StripTags', 'StringTrim'),
          'user_debitnumber'             => array('StripTags', 'StringTrim'),
          'token_id'             => array('StripTags', 'StringTrim'),
          'user_isemail'         => array('StripTags', 'StringTrim'),

          'cust_id'        => array('StripTags', 'StringTrim', 'StringToUpper'),
          //'fgroup_id'      => array('StripTags','StringTrim'),
          //'token_serialno' => array('StripTags','StringTrim','Alnum')

        );


        //merge filter data user with privilege
        $filters = array_merge($filters, $filterPrivilege);

        // $TokenError = array();
        //   $cust_id = strtoupper($this->_getParam('cust_id'));

        $get_user_length = $this->_db->select()
          ->from("M_SETTING")
          ->where("SETTING_ID IN (?)", ["min_length_userid", "max_length_userid"])
          ->order("SETTING_ID ASC")
          ->query()->fetchAll();

        $maxUserID = intval($get_user_length[0]["SETTING_VALUE"]);
        $minUserID = intval($get_user_length[1]["SETTING_VALUE"]);

        $validators =  array(
          'cust_id'       => array(),

          'user_id'       => array(
            'NotEmpty',
            'Alnum',
            array('StringLength', array('min' => $minUserID, 'max' => $maxUserID)),
            // array('Db_NoRecordExists',array('table'=>'M_USER','field'=>'USER_ID','exclude'=>'USER_STATUS!=3 AND UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))),
            array('Db_NoRecordExists', array('table' => 'M_USER', 'field' => 'USER_ID', 'exclude' => 'UPPER(CUST_ID)=' . $this->_db->quote((string)$cust_id))),
            array('Db_NoRecordExists', array('table' => 'TEMP_USER', 'field' => 'USER_ID', 'exclude' => 'UPPER(CUST_ID)=' . $this->_db->quote((string)$cust_id))),
            'messages' => array(
              $this->language->_('Can not be empty'),
              $this->language->_('Invalid user id format'),
              $this->language->_('Minimal ' . $minUserID . " karakter dan maksimal " . $maxUserID . " karakter"),
              $this->language->_('User id for this company is already used. Please use another'),
              $this->language->_('User id for this company is already suggested. Please use another')
            )
          ),

          'user_email'     => array( //new Application_Validate_EmailAddress(),
            "EmailAddress",
            array('StringLength', array('max' => 128)),
            //array('Db_NoRecordExists',array('table'=>'M_USER','field'=>'USER_EMAIL','exclude'=>'UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))),
            array('Db_NoRecordExists', array('table' => 'TEMP_USER', 'field' => 'USER_EMAIL', 'exclude' => 'UPPER(CUST_ID)=' . $this->_db->quote((string)$cust_id))),
            'NotEmpty',
            'messages' => array(
              "Invalid email address",
              //'Invalid email format',
              $this->language->_('Email lenght cannot be more than') . ' 128',
              //                                         $this->language->_('Email is already used in this company. Please use another'),
              $this->language->_('Email is already suggested for this company. Please use another'),
              $this->language->_('Can not be empty'),
            )
          ),

          'user_fullname'      => array(
            'NotEmpty',
            new Zend_Validate_Alpha(array('allowWhiteSpace' => true)),
            array('StringLength', array('min' => 2, 'max' => 50)),
            'messages' => array(
              $this->language->_('Can not be empty'),
              $this->language->_('Invalid User Name Format'),
              $this->language->_('invalid. Min 2 character || Max 50 character')
            )

          ),

          'user_phone'     => array( //array('StringLength',array('min'=>1,'max'=>20)),
            'Digits',
            'allowEmpty' => true,
            'messages' => array(
              $this->language->_('Invalid phone number format'),
            )
          ),

          'user_ext'         => array(
            'Digits',
            array('StringLength', array('min' => 1, 'max' => 20)),
            'allowEmpty' => true,
            'messages' => array(
              $this->language->_('Must be numeric values'),
            )
          ),
          'user_debitnumber'     => array( //new Application_Validate_EmailAddress(),
            array('StringLength', array('max' => 128)),
            //array('Db_NoRecordExists',array('table'=>'M_USER','field'=>'USER_DEBITNUMBER','exclude'=>'UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))),
            //array('Db_NoRecordExists',array('table'=>'TEMP_USER','field'=>'USER_DEBITNUMBER','exclude'=>'UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))),
            //'NotEmpty',
            'allowEmpty' => true,
            'messages' => array(
              //'Invalid email format',
              $this->language->_('Email lenght cannot be more than') . ' 128',
              //$this->language->_('Debit Card Number is already used in this company. Please use another'),
              //$this->language->_('Debit Card Number is already suggested for this company. Please use another'),
              //$this->language->_('Can not be empty'),
            )
          ),
          'token_id'        => array(
            'allowEmpty' => true,
            'Alnum',
            array('StringLength', array('min' => 1, 'max' => 12)),
            array('Db_NoRecordExists', array('table' => 'M_USER', 'field' => 'TOKEN_ID')),
            array('Db_NoRecordExists', array('table' => 'TEMP_USER', 'field' => 'TOKEN_ID')),
            new Zend_Validate_Callback(
              array(
                'callback'  => array('SGO_Validate_Callback', 'checkTokenSerial'),
                'options' => array($cust_id, $user_id)
              )
            ),
            'messages' => array(
              $this->language->_('Invalid Token Id Format'),
              $this->language->_('Invalid Token Id Format'),
              $this->language->_('Token Id already in use. Please use another'),
              $this->language->_('Token Id already suggested. Please use another'),
              $this->language->_('Token Id is invalid')
            )
          ),

          'user_isemail'   => array(
            array('StringLength', array('min' => 1, 'max' => 25)),
            'allowEmpty' => true,
            'messages' => array($this->language->_('invalid'))
          ),
        );

        //merge validator data user with privilege
        $validators = array_merge($validators, $validatorPrivilege);
        $zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);

        //var_dump($this->_request->getParams());die;

        //validasi user_id : paling sedikit harus punya 1 huruf dan 1 angka
        //      if($zf_filter_input->user_id)
        //      {
        //         $user_id =  $zf_filter_input->user_id;
        //         $check_alpha  = 0;
        //         $check_digits = 0;
        //         $strlen_user_id = strlen($user_id);
        //
        //         for($i=0; $i<$strlen_user_id; $i++)
        //         {
        //            if(Zend_Validate::is($user_id[$i],'Alpha'))  $check_alpha  = 1;
        //            if(Zend_Validate::is($user_id[$i],'Digits')) $check_digits = 1;
        //         }
        //
        //         if($check_alpha == 1 && $check_digits == 1) $flag_user_id = 'T';
        //         else $flag_user_id = 'F';
        //      }
        //      else
        //      {
        //         $flag_user_id = 'T';
        //      }
        //END validasi user_id : paling sedikit harus punya 1 huruf dan 1 angka


        // $dailylim = $zf_filter_input->dailylimit;

        $errorLim = false;
        foreach ($ccylist as $key => $value) {

          $limittag = 'daily' . (string)$value['CCY_ID'];
          $dailylimLeft = $companyLim[$value['CCY_ID']];
          if (empty($dailylimLeft)) {
            $dailylimLeft = 0;
          }
          //echo $limittag;echo ' ';
          $dailylimit = $this->_getParam($limittag);
          $dailylimit = preg_replace("/([^0-9\\.])/i", "", $dailylimit);
          if ($dailylimit != '0.00' && $dailylimit != '') {
            //var_dump($dailylimit);
            //var_dump($dailylimLeft);
            if ($dailylimit > (int)$dailylimLeft) {
              //  echo 'hree';  

              $errorLim = true;
              $error_remark = "Maximum Amount of Daily Limit in " . $value['CCY_ID'] . " is " . Application_Helper_General::displayMoney($dailylimLeft);
            }
          }
        }

        //die;
        //echo '<pre>';
        //var_dump($dailylimit);
        //var_dump($dailylimLeft);die;

        //validasi status customer jika status deleted
        if ($cust_status != 3) $cust_id_flag = true;
        else $cust_id_flag = false;
        //END validasi status customer jika didelete

        //validasi multiple email
        if ($this->_getParam('user_email')) {
          $validate = new validate;
          $cek_email = $validate->isValidEmail($this->_getParam('user_email'));

          $select = $this->_db->select()
            ->from('M_USER', array('USER_EMAIL', 'USER_ID'))
            ->where('UPPER(CUST_ID)=' . $this->_db->quote((string)$cust_id))
            ->where('USER_EMAIL = ?', $this->_getParam('user_email'))
            ->where('USER_STATUS != 3');

          $data = $this->_db->fetchRow($select);
          //var_dump($data);die;
          $checkDuplicateEmail = false;
          if (empty($data)) {
            $checkDuplicateEmail = true;
          } else {
            if ($data['USER_ID'] == $user_id) {
              $checkDuplicateEmail = true;
            }
          }
        }

        $errorLim = false;
        if (!empty($this->_getParam('user_debitnumber'))) {
          $debitarr = explode(';', $this->_getParam('user_debitnumber'));
          foreach ($debitarr as $dval) {
            $select = $this->_db->select()
              ->from('TEMP_USER_DEBIT', array('USER_DEBITNUMBER'))
              ->where('USER_DEBITNUMBER = ?', $dval);

            $tempcekdebit = $this->_db->fetchOne($select);

            $select = $this->_db->select()
              ->from('M_USER_DEBIT', array('USER_DEBITNUMBER'))
              ->where('USER_DEBITNUMBER = ?', $dval);

            $cekdebit = $this->_db->fetchOne($select);

            $select = $this->_db->select()
              ->from('T_DEBITCARD', array('DEBIT_NUMBER'))
              ->where('DEBIT_NUMBER = ?', $dval)
              ->where('CUST_ID = ?', $this->_custIdLogin)
              ->where('DEBIT_STATUS = ?', '1');

            //echo $select;
            $cekdebitlist = $this->_db->fetchOne($select);

            if ($cekdebitlist) {
              //var_dump($cekdebit);
              //var_dump($tempcekdebit);
              //die();
              if (!empty($cekdebit) || !empty($tempcekdebit)) {
                $errorLim = true;
                $error_remark = "Duplicate Debet Card Number " . $dval;
                break;
              }
            }
          }
        }

        if ($zf_filter_input->isValid() && $cust_id_flag == true && $cek_email && $privcheck  && !$errorLim && $checkDuplicateEmail) {

          //        $info = 'User ID = '.$zf_filter_input->user_id.', User Name = '.$zf_filter_input->user_name;
          $info = 'User ID = ' . $hasil_userid . ', User Name = ' . $zf_filter_input->user_fullname;

          $user_data = array(
            'CUST_ID'        => null,
            //                           'USER_ID'        => null,
            'USER_FULLNAME'  => null,
            'USER_PASSWORD'  => null,
            'USER_EMAIL'     => null,
            'USER_PHONE'     => null,
            'USER_STATUS'    => null,
            'USER_EXT'       => null,
            'TOKEN_ID'       => null,
            'USER_ISWEBSERVICES' => null,
            'USER_ISEMAIL'       => null,
            //'FGROUP_ID'      => null,
            //'USER_HASTOKEN'  => null
            'USER_SUGGESTED'   => null,
            'USER_SUGGESTEDBY' => null,
          );

          foreach ($validators as $key => $value) {
            if ($zf_filter_input->$key)  $user_data[strtoupper($key)] = $zf_filter_input->$key;
          }

          if (is_null($user_data['USER_ISEMAIL']) || $user_data['USER_ISEMAIL'] == '') {
            $user_data['USER_ISEMAIL'] = 0;
          }

          /*Zend_Debug::dump($user_data);
      die;*/

          /*if($zf_filter_input->token_serialno)
      {
         $user_data['USER_HASTOKEN'] = strtoupper($this->_masterhasStatus['code']['yes']);
      }
      else
      {
         $user_data['USER_HASTOKEN'] = strtoupper($this->_masterhasStatus['code']['no']);
      }*/

          $user_data['USER_ID'] = $hasil_userid;
          $user_data['USER_STATUS'] = 2;
          $user_data['CUST_ID']     = $cust_id;
          $user_data['USER_CREATED']     = null;
          $user_data['USER_CREATEDBY']   = null;
          $user_data['USER_UPDATED']     = null;
          $user_data['USER_UPDATEDBY']   = null;

          $user_data['USER_SUGGESTED']    = new Zend_Db_Expr('now()');
          $user_data['USER_SUGGESTEDBY']  = $this->_userIdLogin;
          $date = date('Y-m-d h:i:s', strtotime("+1 days"));
          $str = rand();

          $rand = md5($str);
          $user_data['USER_DATEPASS']  = $date;
          $user_data['USER_CODE']  = $rand;
          unset($user_data['USER_DEBITNUMBER']);

          try {
            $this->_db->beginTransaction();
            $change_id = $this->suggestionWaitingApproval('User Account', $info, $this->_changeType['code']['new'], null, 'M_USER', 'TEMP_USER', $hasil_userid, $user_data['USER_FULLNAME'], $cust_id);

            $count_privi = count($priviId);
            //echo '<pre>';
            //var_dump($user_data);die;
            if ($count_privi > 0)  $this->insertTempPrivilege($change_id, $priviId, $user_data);

            $this->insertTempUser($change_id, $user_data);

            //log CRUD
            //      Application_Helper_General::writeLog('CSAD','User has been Added, User ID : '.$zf_filter_input->user_id. ' User Name : '.$user_data['USER_FULLNAME'].' Change id : '.$change_id);
            Application_Helper_General::writeLog('CSAD', 'User has been Added, User ID : ' . $hasil_userid . ' User Name : ' . $user_data['USER_FULLNAME'] . ' Change id : ' . $change_id);

            //  $active = $this->_request->getParam('req_id');
            //echo '<pre>';
            //var_dump($active);die;     
            $active = json_decode(json_encode($dataUser), true);

            $b = 0;
            $final1 = array();
            foreach ($active as $arr => $value) {
              $final1[$b++] =
                array(
                  'accountNo' => $value['accountNo'],
                  'accountName' => $this->_request->getParam('accountName' . $value['accountNo']),
                  // 'email'=>$this->_request->getParam('email'.$value),
                  'ccy' => $this->_request->getParam('ccy' . $value['accountNo']),
                  'productType' => $this->_request->getParam('productType' . $value['accountNo']),
                  // 'planCode'=>$this->_request->getParam('planCode'.$value),
                  'productName' => $this->_request->getParam('productName' . $value['accountNo']),
                  'acct_desc' => $this->_request->getParam('acct_desc' . $value['accountNo'])
                );
            }

            // print_r($final1);die();

            // for($i=1;$i<=$total;$i++){
            foreach ($final1 as $key => $value) {
              # code...
              // }
              $accountNo = $value['accountNo'];
              $accountName = $value['accountName'];
              $email = '';
              $ccy = $value['ccy'];
              $productType = $value['productType'];
              $acct_desc = $value['acct_desc'];


              $acct_source = 3;     //1 = prk scm 
              //$acct_desc      = $acct_desc;

              $info = 'Account No = ' . $accountNo;
              $acct_data['ACCT_STATUS']      = 1;     //1 = unsuspend
              $acct_data['ACCT_SUGGESTED']   = new Zend_Db_Expr('GETDATE()');
              $acct_data['ACCT_SUGGESTEDBY'] = $this->_userIdLogin;
              //data from core
              $acct_data['ACCT_NAME']   = $accountName;
              $acct_data['ACCT_ALIAS_NAME'] = $accountName;
              $acct_data['ACCT_NO'] = $accountNo;
              $acct_data['CUST_ID'] = $cust_id;
              $acct_data['CCY_ID'] = $ccy;
              $acct_data['ACCT_DESC'] = $acct_desc;
              $acct_data['ACCT_TYPE'] = $productType;
              $acct_data['ACCT_EMAIL'] = $email;
              $acct_data['ACCT_SOURCE'] = $acct_source;

              try {
                $acct_data['CHANGES_ID'] = $change_id;
                $insert = $this->_db->insert('TEMP_CUSTOMER_ACCT', $acct_data);
                $limittag = 'limit' . (string)$accountNo;
                $maxlimit = $this->_getParam($limittag);
                if ($maxlimit != '0.00' && $maxlimit != '') {
                  $data = array(
                    'CHANGES_ID' => $change_id,
                    'USER_LOGIN' => $hasil_userid,
                    'ACCT_NO' => (string)$accountNo,
                    'CUST_ID' => $cust_id,
                    'MAXLIMIT' => Application_Helper_General::convertDisplayMoney($maxlimit),
                    'SUGGESTED' => new Zend_Db_Expr('now()'),
                    'SUGGESTEDBY' => $this->_userIdLogin,
                    'MAKERLIMIT_STATUS' => 1
                  );
                  $this->_db->insert('TEMP_MAKERLIMIT', $data);
                }
              } catch (Exception $e) {
                //  echo '<pre>';
                //var_dump($e);die;
              }
            }

            foreach ($ccylist as $key => $value) {
              $dailydata = array();
              $dailydata['CHANGES_ID'] = $change_id;
              $dailydata['USER_LOGIN'] = $hasil_userid;
              $dailydata['CCY_ID'] = $value['CCY_ID'];
              $dailydata['CUST_ID'] = $cust_id;

              $limittag = 'daily' . (string)$value['CCY_ID'];
              $dailylimit = $this->_getParam($limittag);
              if ($dailylimit != '0.00' && $dailylimit != '') {
                $dailydata['DAILYLIMIT'] = Application_Helper_General::convertDisplayMoney($dailylimit);
                $dailydata['DAILYLIMIT_STATUS'] = '1';

                $dailydata['SUGGESTED'] = new Zend_Db_Expr('now()');
                $dailydata['SUGGESTEDBY'] = $this->_userIdLogin;
                $this->_db->insert('TEMP_DAILYLIMIT', $dailydata);
              }
            }


            if (!empty($debitarr)) {
              $insetdebit = array();
              foreach ($debitarr as $val) {
                $insetdebit['CHANGES_ID'] = $change_id;
                $insetdebit['USER_ID'] = $hasil_userid;
                $insetdebit['CUST_ID'] = $cust_id;
                $insetdebit['USER_DEBITNUMBER'] = $val;
                $insert = $this->_db->insert('TEMP_USER_DEBIT', $insetdebit);
              }
            }



            $this->_db->commit();

            $this->setbackURL('/customer/view/index/cust_id/' . $this->_getParam('cust_id'));

            $this->_redirect('/notification/submited/index');
            //$this->_redirect('/notification/success/index');
          } catch (Exception $e) {
            //  var_dump($e);die;
            $this->_db->rollBack();
            $error_remark = $this->language->_('An Error Occured. Please Try Again');
          }


          if (isset($error_remark)) {
            //insert log
            //print_r($fulldesc);
            // print_r($error_remark);die;
            try {
              $this->_db->beginTransaction();
              $fulldesc = Application_Helper_General::displayFullDesc($_POST);
              $this->backendLog(strtoupper($this->_changeType['code']['new']), strtoupper($this->_moduleID['user']), null, $fulldesc, $error_remark);
              $this->_db->commit();
            } catch (Exception $e) {
              $this->_db->rollBack();
            }

            $this->_helper->getHelper('FlashMessenger')->addMessage('F');
            $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
            // $this->_redirect($this->_backURL);
          } else {
            $this->view->success = 1;
            $msg = 'Invalid User ID';
            $this->view->user_msg = $msg;
          }
        } else {

          $active = json_decode(json_encode($dataUser), true);
          foreach ($active as $arr => $value) {
            $limit = 'limit' . $value['accountNo'];
            $this->view->$limit =  Application_Helper_General::displayMoney($this->_request->getParam($limit));
          }


          foreach ($ccylist as $key => $value) {
            $daily = 'daily' . $value['CCY_ID'];
            $this->view->$daily =  Application_Helper_General::displayMoney($this->_request->getParam($daily));
          }

          $this->view->error = 1;
          $this->view->user_id = ($zf_filter_input->isValid('user_id')) ? $zf_filter_input->user_id : $this->_getParam('user_id');
          $this->view->user_fullname = ($zf_filter_input->isValid('user_fullname')) ? $zf_filter_input->user_fullname : $this->_getParam('user_fullname');
          $this->view->user_email = ($zf_filter_input->isValid('user_email')) ? $zf_filter_input->user_email : $this->_getParam('user_email');
          $this->view->user_phone = ($zf_filter_input->isValid('user_phone')) ? $zf_filter_input->user_phone : $this->_getParam('user_phone');
          $this->view->user_ext = ($zf_filter_input->isValid('user_ext')) ? $zf_filter_input->user_ext : $this->_getParam('user_ext');
          $this->view->token_id = ($zf_filter_input->isValid('token_id')) ? $zf_filter_input->token_id : $this->_getParam('token_id');

          $this->view->user_isemail = ($zf_filter_input->isValid('user_isemail')) ? $zf_filter_input->user_isemail : $this->_getParam('user_isemail');
          $this->view->user_debitnumber = ($zf_filter_input->isValid('user_debitnumber')) ? $zf_filter_input->user_debitnumber : $this->_getParam('user_debitnumber');
          $this->view->generate_userid   = $this->_getParam('generate_userid');
          $this->view->squenceNumberUser = $this->_getParam('squenceNumberUser');

          $this->view->priviView = $priviId;

          /*$error = $zf_filter_input->getMessages();
      if(count($error))$error_remark = $this->displayErrorRemark($error);
      $this->view->user_msg = $this->displayError($error);*/
          if (!$privcheck) {
            $this->view->errcheckbox = $this->language->_('Please Select at least 1 Privilege');
          }
          $error = $zf_filter_input->getMessages();
          // print_r($error);
          //format error utk ditampilkan di view html
          $errorArray = null;
          foreach ($error as $keyRoot => $rowError) {
            foreach ($rowError as $errorString) {
              $errorArray[$keyRoot] = $errorString;
            }
          }

          //        if($flag_user_id == 'F')  $errorArray['user_id'] = $this->language->_('Invalid user id format');

          //konfigurasi error cust id jika didelete
          if ($cust_id_flag == false) $errorArray['user_id'] = $this->language->_('Cannot add record, company already deleted');

          if (isset($cek_email) && $cek_email == false) $errorArray['user_email'] = $this->language->_('Invalid format');

          if (!$checkDuplicateEmail) $errorArray['user_email'] = $this->language->_('Email is already used in this company. Please use another');

          $this->view->error_msg = $this->language->_('Error in processing form values. Please correct values and re-submit.');
          if (!empty($error_remark)) {
            $errorArray[] = $error_remark;
          }
          $this->view->error_msg = $errorArray;
        }
      }
    } else {
      $this->view->user_iswebservices = 0;
      $this->view->user_isemail = 1;

      // if (!$temp) {
      //    $this->view->error = true;
      //    $this->view->error_msg = 'Cannot add record before previous user is approved';
      // }
    }


    //--------query privilege-------------
    //modifikasi privilege agar bisa ditampilkan sesuai golongan
    $this->view->getModuleDescArr = $this->getModuleDescArr();

    $privilege_final = array();
    $privilege_final_modif = array();
    foreach ($getprivilege as $row) {
      $fprivi_moduleid = trim($row['FPRIVI_MODULEID']);
      $privilege_final[$fprivi_moduleid][] = $row;
      $privilege_final_modif = $this->modifPrivi($privilege_final);
    }

    $this->view->fprivilege  = $privilege_final_modif;
    $this->view->template    = Application_Helper_Array::listArray($this->getTemplate(), 'FTEMPLATE_ID', 'FTEMPLATE_DESC');


    $priviTemplate = array();
    foreach ($this->getPriviTemplate() as $row) {
      $priviTemplate[$row['FTEMPLATE_ID']][$row['FPRIVI_ID']] = $row['FPRIVI_DESC'];
    }

    $this->view->priviTemplate = $priviTemplate;
    //-------END query privilege--------


    /*Zend_Debug::dump($this->view->priviTemplate);
    die;*/


    $select = $this->_db->select()
      ->from('M_CUSTOMER', array('CUST_NAME'))
      ->where('UPPER(CUST_ID)=' . $this->_db->quote((string)$cust_id));
    //    $this->view->cust_name = $this->_db->fetchOne($select);

    $data = $this->_db->fetchOne($select);
    $this->view->cust_name = $data;



    /////////////////////////////////////////////////////////////////////////////

    $selectCountUserGlobal = $this->_db->select()
      ->from('T_GLOBAL_CHANGES', array('CHANGES_ID'))
      ->where('UPPER(CHANGES_TYPE)= ? ', 'N')
      ->where('UPPER(CHANGES_STATUS) IN ("WA","RJ")')
      ->where('UPPER(COMPANY_CODE)=' . $this->_db->quote((string)$cust_id));

    $dataCountUserGlobal = $this->_db->fetchAll($selectCountUserGlobal);

    $jumUserGlobal = count($dataCountUserGlobal);


    $selectCountUser = $this->_db->select()
      ->from('M_USER', array('CUST_ID'))
      ->where('UPPER(CUST_ID)=' . $this->_db->quote((string)$cust_id));
    $dataCountUser = $this->_db->fetchAll($selectCountUser);

    $jumUser = count($dataCountUser);

    if ($jumUser >= 0 && $jumUser <= 10) {
      $totalUser = $jumUserGlobal + $jumUser + 1;
      $squenceNumberUser =  '0' . $totalUser;
    } else {
      $squenceNumberUser = $jumUserGlobal + $jumUser + 1;
    }
    $this->view->squenceNumberUser = $squenceNumberUser;

    $comp_nameOriSub = preg_replace("/[^0-9a-zA-Z]/", "", $data);
    $this->view->generate_userid = strtoupper(substr($comp_nameOriSub, 0, 4)) . '_';

    //    $resultTmpChange = $this->getTempCustomerId($cust_id);
    //    if($resultTmpChange)  $temp = 0; 
    //    else  $temp = 1; 

    // $this->view->cust_temp = $temp; 

    $this->view->cust_id = $cust_id;
    $this->view->modulename = $this->_request->getModuleName();
    if ($this->_getParam('pdf') == 'PDF') {
      Application_Helper_General::writeLog('CSAD', 'Download PDF Create Customer User [BO]');
      $this->view->button = false;
      $outputHTML = "<tr><td>" . $this->view->render($this->view->controllername . '/index.phtml') . "</td></tr>";
      $this->_helper->download->pdf(null, null, null, 'Create Customer User', $outputHTML);
      $this->view->button = true;
    }


    //insert log
    try {
      $this->_db->beginTransaction();
      if (!$this->_request->isPost()) {
        Application_Helper_General::writeLog('CSAD', 'View Customer > Customer List > Customer Detail > Add Customer User');
      }
      $this->_db->commit();
    } catch (Exception $e) {
      $this->_db->rollBack();
    }
  }

  public function checkduplicateemailAction()
  {
    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout()->disableLayout();

    $email = $this->_getParam('email');
    $cust_id = $this->_getParam('cust_id');

    $select = $this->_db->select()
      ->from('M_USER', array('USER_EMAIL', 'USER_ID'))
      ->where('UPPER(CUST_ID)=' . $this->_db->quote((string)$cust_id))
      ->where('USER_EMAIL = ?', $email)
      ->where('USER_STATUS != 3');

    $data = $this->_db->fetchRow($select);

    if (empty($data)) {
      $result = 'false';
    } else {
      $result = 'true';
    }

    echo $result;
  }

  public function checkduplicateuseridAction()
  {
    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout()->disableLayout();

    $user_id = $this->_getParam('userid');
    $cust_id = $this->_getParam('cust_id');

    $select = $this->_db->select()
      ->from('M_USER', array('USER_EMAIL', 'USER_ID'))
      ->where('UPPER(CUST_ID)=' . $this->_db->quote((string)$cust_id))
      ->where('USER_ID = ?', $user_id)
      ->where('USER_STATUS != 3')
      ->query()->fetch();

    if ($select) {
      $exist = true;
    } else {
      $exist = false;
    }

    header("Content-Type : application/json");
    echo json_encode([
      "exist" => $exist
    ]);
  }
}
