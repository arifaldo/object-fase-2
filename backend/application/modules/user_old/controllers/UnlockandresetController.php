<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'Crypt/AESMYSQL.php';

class user_UnlockandresetController extends Application_Main
{

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
	    $password = $sessionNamespace->token; 
	    $this->view->token = $sessionNamespace->token;

	    $AESMYSQL = new Crypt_AESMYSQL();
	    $cust_id = $AESMYSQL->decrypt($this->_getParam('cust_id'), $password);
	    $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;

	    $user_id = strtoupper($AESMYSQL->decrypt($this->_getParam('user_id'), $password)); 

		$this->view->user_id = $user_id;
		$this->view->cust_id = $cust_id;

	   	$select =	$this->_db->SELECT()
									->FROM(array('U' => 'M_USER'), array('USER_FULLNAME','USER_EMAIL','USER_RRESET','USER_RCHANGE','USER_ISEMAIL'))
									->JOINLEFT(array('C' => 'M_CUSTOMER'), 'C.CUST_ID = U.CUST_ID', array('CUST_NAME'))
									->WHERE('USER_ID = ? ', $user_id)
									->WHERE('C.CUST_ID = ? ', $cust_id);

		$result = $this->_db->FETCHROW($select);
		$this->view->email = (!empty($result['USER_EMAIL'])) ? $result['USER_EMAIL'] : 'N/A';
		$this->view->user_isemail = $result['USER_ISEMAIL'];
		$this->view->username = $result['USER_FULLNAME'];
		$this->view->custname = $result['CUST_NAME'];

		$this->view->statusreset=1;
		if($this->_request->isPost())
		{
			if($this->_getParam('isEmail')==1 || $this->_getParam('isEmail')==2)
			{
				$customerUser = new customerUser($cust_id,$user_id);
				if($this->_getParam('isEmail')==1)
				{
					$customerUser->requestUnlockResetPassword(1,1,$this->_userIdLogin);
				}
				else if($this->_getParam('isEmail')==2){
					$customerUser->requestUnlockResetPassword(0,1,$this->_userIdLogin);
				}
				//$this->setbackURL('/'.$this->_request->getModuleName().'/view/index/cust_id/'.$cust_id.'/user_id/'.$user_id);
				Application_Helper_General::writeLog('CSRP','Requesting Unlock and Reset Password : Cust Id ( '.$cust_id.' ) ,User Id ( '.$user_id.' )');
				$this->setbackURL('/customer/view/index/cust_id/'.$this->_getParam('cust_id'));
			    $this->_redirect('/notification/submited/index');
			}
		}
	}
}
