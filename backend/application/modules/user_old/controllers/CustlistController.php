<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';

class user_CustlistController extends Application_Main
{
	protected $_moduleDB = 'RTF';


	public function indexAction()
	{

		$setting = new Settings();
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$pw_hash = md5($enc_salt . $enc_pass);
		$rand = $this->_userIdLogin . date('dHis') . $pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;

		$this->_helper->layout()->setLayout('newlayout');

		$auth = Zend_Auth::getInstance()->getIdentity();
		//pengaturan url untuk button back
		$this->setbackURL('/' . $this->_request->getModuleName() . '/custlist/');

		$status_type = $this->_masterglobalstatus;
		$this->view->status_type = $status_type;
		$listStatus =  array_combine(array_values($status_type['code']), array_values($status_type['desc']));
		$this->view->listStatus = array('' => '--' . $this->language->_('Any Value') . ' --');
		$this->view->listStatus += $listStatus;

		$listId = $this->_db->select()->distinct()
			->from(
				array('M_CUSTOMER'),
				array('CUST_ID')
			)
			->order('CUST_ID ASC')
			->query()->fetchAll();
		$this->view->listCustId = Application_Helper_Array::listArray($listId, 'CUST_ID', 'CUST_ID');

		$fields = array(
			'companyID'  			=> array(
				'field' => 'C.CUST_ID',
				'label' => $this->language->_('Company'),
				'sortable' => true
			),
			// 'companyname'  			=> array	(
			// 										'field' => 'CUST_NAME',
			// 										'label' => $this->language->_('Company Name'),
			// 										'sortable' => true
			// 									),
			'user_id'  					=> array(
				'field' => 'USER_ID',
				'label' => $this->language->_('User'),
				'sortable' => true
			),
			// 'name'  					=> array	(
			// 											'field' => 'USER_FULLNAME',
			// 											'label' => $this->language->_('User Name'),
			// 											'sortable' => true
			// 									),
			'status'  					=> array(
				'field' => 'USER_STATUS',
				'label' => $this->language->_('Status'),
				'sortable' => true
			),
			'userislocked' 					=> array(
				'field' => 'USER_ISLOCKED',
				'label' => $this->language->_('User is Locked'),
				'sortable' => true
			),
			'userisemail'  					=> array(
				'field' => 'USER_ISEMAIL',
				'label' => $this->language->_('User is Email'),
				'sortable' => true
			),
			'latestSuggestion'     => array(
				'field'    => 'USER_SUGGESTED',
				'label'    => $this->language->_('Latest Suggested'),
				'sortable' => true
			),
			// 'latestSuggestor'   => array('field'  => 'USER_SUGGESTEDBY',
			// 										   'label'    => $this->language->_('Latest Suggester'),
			// 										   'sortable' => true),
			'latestApproval'    => array(
				'field'  => 'USER_UPDATED',
				'label'    => $this->language->_('Latest Approved'),
				'sortable' => true
			),
			// 'latestApprover'  => array('field'   => 'USER_UPDATEDBY',
			// 										   'label'    => $this->language->_('Latest Approver'),
			// 										   'sortable' => true),
			// 'action'   => array('field'    => '',
			// 							  'label'    => $this->language->_('Action'),
			// 							  'sortable' => false)													  
		);

		// $filterlist = array('PS_LATEST_SUGGESTION','PS_LATEST_APPROVAL','LATEST_SUGGESTER','LATEST_APPROVER','COMP_ID','COMP_NAME','USER_ID','STATUS');
		$filterlist = array('LATEST_SUGGESTER', 'LATEST_APPROVER', 'COMP_ID', 'COMP_NAME', 'USER_ID', 'STATUS');

		$this->view->filterlist = $filterlist;

		$page = $this->_getParam('page');
		$page = (Zend_Validate::is($page, 'Digits')) ? $page : 1;
		$sortBy = $this->_getParam('sortby');
		$sortBy = (Zend_Validate::is($sortBy, 'InArray', array(array_keys($fields)))) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir, 'InArray', array('haystack' => array('asc', 'desc')))) ? $sortDir : 'asc';

		$filterArr = array(
			'filter' => array('StripTags', 'StringTrim'),
			'COMP_ID'    => array('StripTags', 'StringTrim', 'StringToUpper'),
			'COMP_NAME'  => array('StripTags', 'StringTrim', 'StringToUpper'),
			'USER_ID'    => array('StripTags', 'StringTrim', 'StringToUpper'),
			'STATUS'  => array('StripTags', 'StringTrim'),
			'PS_LATEST_SUGGESTION' => array('StripTags', 'StringTrim', 'StringToUpper'),
			'PS_LATEST_SUGGESTION_END'     => array('StripTags', 'StringTrim'),
			'LATEST_SUGGESTER'      => array('StripTags', 'StringTrim', 'StringToUpper'),
			'PS_LATEST_APPROVAL'   => array('StripTags', 'StringTrim', 'StringToUpper'),
			'PS_LATEST_APPROVAL_END'       => array('StripTags', 'StringTrim'),
			'LATEST_APPROVER'       => array('StripTags', 'StringTrim', 'StringToUpper'),
		);

		$validator = array(
			'filter'  => array(),
			'COMP_ID'     => array(),
			'COMP_NAME'   => array(),
			'STATUS'  => array(),
			'USER_ID' => array(),

			'PS_LATEST_SUGGESTION' => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
			'PS_LATEST_SUGGESTION_END'     => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
			'LATEST_SUGGESTER'      => array(),
			'PS_LATEST_APPROVAL'   => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
			'PS_LATEST_APPROVAL_END'       => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
			'LATEST_APPROVER'       => array(),
		);

		$dataParam = array('LATEST_SUGGESTER', 'LATEST_APPROVER', 'COMP_ID', 'COMP_NAME', 'USER_ID', 'STATUS');
		$dataParamValue = array();

		$clean2 = array_diff($this->_request->getParam('wherecol'), $dataParam);
		$dataParam = array_diff($this->_request->getParam('wherecol'), $clean2);
		// print_r($dataParam);die;

		// print_r($output);die;
		// print_r($this->_request->getParam('wherecol'));
		foreach ($dataParam as $no => $dtParam) {

			if (!empty($this->_request->getParam('wherecol'))) {
				$dataval = $this->_request->getParam('whereval');
				// print_r($dataval);
				$order = 0;
				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if ($value == "PS_LATEST_APPROVAL" || $value == "PS_LATEST_SUGGESTION") {
						$order--;
					}
					if ($dtParam == $value) {
						$dataParamValue[$dtParam] = $dataval[$order];
					}
					$order++;
				}
			}
		}
		// print_r($dataParamValue);
		// die;	
		// print_r($this->_request->getParam('whereval'));die;
		if (!empty($this->_request->getParam('lsdate'))) {
			$lsarr = $this->_request->getParam('lsdate');
			$dataParamValue['PS_LATEST_SUGGESTION'] = $lsarr[0];
			$dataParamValue['PS_LATEST_SUGGESTION_END'] = $lsarr[1];
		}
		if (!empty($this->_request->getParam('ladate'))) {
			$laarr = $this->_request->getParam('ladate');
			$dataParamValue['PS_LATEST_APPROVAL'] = $laarr[0];
			$dataParamValue['PS_LATEST_APPROVAL_END'] = $laarr[1];
		}


		$zf_filter = new Zend_Filter_Input($filterArr, $validator, $dataParamValue);
		$filter 		= $this->_getParam('filter');

		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');

		$listYesNo = array(
			0 => 'No',
			1 => 'Yes',
		);
		$caseStatus = "(CASE MU.USER_STATUS ";
		foreach ($listStatus as $key => $val) {
			$caseStatus .= " WHEN " . $key . " THEN '" . $val . "'";
		}
		$caseStatus .= " END)";

		$caseUSER_ISLOCKED = "(CASE MU.USER_ISLOCKED ";
		foreach ($listYesNo as $key => $val) {
			$caseUSER_ISLOCKED .= " WHEN " . $key . " THEN '" . $val . "'";
		}
		$caseUSER_ISLOCKED .= " END)";

		$caseUSER_ISEMAIL = "(CASE MU.USER_ISEMAIL ";
		foreach ($listYesNo as $key => $val) {
			$caseUSER_ISEMAIL .= " WHEN " . $key . " THEN '" . $val . "'";
		}
		$caseUSER_ISEMAIL .= " END)";

		$select = $this->_db->select()
			->FROM(array('C' => 'M_CUSTOMER'), array('C.CUST_ID', 'C.CUST_NAME'))
			->join(array('MU' => 'M_USER'), 'C.CUST_ID = MU.CUST_ID', array(
				'company'	=> new Zend_Db_Expr("CONCAT(C.CUST_NAME , ' (' , C.CUST_ID , ')  ' )"),
				'user'	=> new Zend_Db_Expr("CONCAT(MU.USER_FULLNAME , ' (' , MU.USER_ID , ')  ' )"), 'MU.USER_FULLNAME',
				'USER_STATUS' => $caseStatus, 'USER_ISEMAIL' => $caseUSER_ISEMAIL, 'USER_ISLOCKED' => $caseUSER_ISLOCKED, 'USER_ID', 'USER_SUGGESTED', 'USER_SUGGESTEDBY', 'USER_UPDATED', 'USER_UPDATEDBY'
			))
			->joinLeft(array('B' => 'M_BUSER'), 'C.CUST_CREATEDBY = B.BUSER_ID', array('B.BUSER_BRANCH'))
			->joinLeft(array('CB' => 'M_BRANCH'), 'CB.ID = B.BUSER_BRANCH', array('CB.BRANCH_NAME', 'CB.STATUS'));
		if ($auth->userHeadQuarter == "NO") {
			$select->where('CB.ID="' . $auth->userBranchId . '"');
		}



		if ($filter == TRUE) {
			$companyid = html_entity_decode($zf_filter->getEscaped('COMP_ID'));
			$companyname = html_entity_decode($zf_filter->getEscaped('COMP_NAME'));
			$userid = html_entity_decode($zf_filter->getEscaped('USER_ID'));
			$stat = $zf_filter->getEscaped('STATUS');
			$latestSuggestionFrom   = html_entity_decode($zf_filter->getEscaped('PS_LATEST_SUGGESTION'));
			$latestSuggestionTo     = html_entity_decode($zf_filter->getEscaped('PS_LATEST_SUGGESTION_END'));
			$latestSuggestor        = html_entity_decode($zf_filter->getEscaped('LATEST_SUGGESTER'));
			$latestApprovalFrom     = html_entity_decode($zf_filter->getEscaped('PS_LATEST_APPROVAL'));
			$latestApprovalTo       = html_entity_decode($zf_filter->getEscaped('PS_LATEST_APPROVAL_END'));
			$latestApprover         = html_entity_decode($zf_filter->getEscaped('LATEST_APPROVER'));

			if ($companyid) {
				$this->view->companyid = $companyid;
				$select->where("UPPER(C.CUST_ID) LIKE " . $this->_db->quote('%' . $companyid . '%'));
			}

			if ($companyname) {
				$this->view->companyname = $companyname;
				$select->where("UPPER(C.CUST_NAME) LIKE " . $this->_db->quote('%' . $companyname . '%'));
			}

			if ($userid) {
				$this->view->userid = $userid;
				$select->where("MU.USER_ID LIKE " . $this->_db->quote('%' . $userid . '%'));
			}

			if ($stat) {
				$this->view->stat = $stat;
				$select->where("USER_STATUS LIKE " . $this->_db->quote($stat));
			}

			//konversi date agar dapat dibandingkan
			$latestSuggestionFrom   = (Zend_Date::isDate($latestSuggestionFrom, $this->_dateDisplayFormat)) ?
				new Zend_Date($latestSuggestionFrom, $this->_dateDisplayFormat) :
				false;

			$latestSuggestionTo     = (Zend_Date::isDate($latestSuggestionTo, $this->_dateDisplayFormat)) ?
				new Zend_Date($latestSuggestionTo, $this->_dateDisplayFormat) :
				false;

			$latestApprovalFrom     = (Zend_Date::isDate($latestApprovalFrom, $this->_dateDisplayFormat)) ?
				new Zend_Date($latestApprovalFrom, $this->_dateDisplayFormat) :
				false;

			$latestApprovalTo       = (Zend_Date::isDate($latestApprovalTo, $this->_dateDisplayFormat)) ?
				new Zend_Date($latestApprovalTo, $this->_dateDisplayFormat) :
				false;
			if ($latestSuggestor)  $select->where('UPPER(USER_SUGGESTEDBY) LIKE ' . $this->_db->quote('%' . strtoupper($latestSuggestor) . '%'));
			if ($latestApprover)   $select->where('UPPER(USER_UPDATEDBY) LIKE ' . $this->_db->quote('%' . strtoupper($latestApprover) . '%'));
			if ($latestSuggestionFrom)  $select->where("DATE(USER_SUGGESTED) >= DATE(" . $this->_db->quote($latestSuggestionFrom->toString($this->_dateDBFormat)) . ")");
			if ($latestSuggestionTo)    $select->where("DATE(USER_SUGGESTED) <= DATE(" . $this->_db->quote($latestSuggestionTo->toString($this->_dateDBFormat)) . ")");
			if ($latestApprovalFrom)    $select->where("DATE(USER_UPDATED) >= DATE(" . $this->_db->quote($latestApprovalFrom->toString($this->_dateDBFormat)) . ")");
			if ($latestApprovalTo)      $select->where("DATE(USER_UPDATED) <= DATE(" . $this->_db->quote($latestApprovalTo->toString($this->_dateDBFormat)) . ")");
			$this->view->latestSuggestor  = $latestSuggestor;
			$this->view->latestApprover   = $latestApprover;

			if ($latestSuggestionFrom)  $this->view->latestSuggestionFrom = $latestSuggestionFrom->toString($this->_dateDisplayFormat);
			if ($latestSuggestionTo)    $this->view->latestSuggestionTo   = $latestSuggestionTo->toString($this->_dateDisplayFormat);
			if ($latestApprovalFrom)    $this->view->latestApprovalFrom   = $latestApprovalFrom->toString($this->_dateDisplayFormat);
			if ($latestApprovalTo)      $this->view->latestApprovalTo     = $latestApprovalTo->toString($this->_dateDisplayFormat);
		}
		$select->order($sortBy . ' ' . $sortDir);
		// echo $select;die;
		if ($csv || $pdf || $this->_request->getParam('print')) {
			$data = $this->_db->fetchall($select);
			// Zend_Debug::dump($data[1]);
			foreach ($data as $rowNum => $key) {
				$data[$rowNum]['USER_SUGGESTED'] = Application_Helper_General::convertDate($key['USER_SUGGESTED'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
				$data[$rowNum]['USER_UPDATED'] = Application_Helper_General::convertDate($key['USER_UPDATED'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
			}
			// Zend_Debug::dump($data[1]);die;
		}

		$tableHeader = array($this->language->_('Company Code'), $this->language->_('Company Name'), $this->language->_('User ID'), $this->language->_('User Name'), $this->language->_('Status'), $this->language->_('User is Locked'), $this->language->_('User is Email'), $this->language->_('Latest Suggestion'), $this->language->_('Latest Suggester'), $this->language->_('Latest Approval'), $this->language->_('Latest Approver'));

		if ($csv) {
			$this->_helper->download->csv($tableHeader, $data, null, $this->language->_('Customer User List Report'));
			Application_Helper_General::writeLog('ROVW', 'Importing CSV Customer > Customer User List');
		} else if ($pdf) {
			$this->_helper->download->pdf($tableHeader, $data, null, $this->language->_('Customer User List Report'));
			Application_Helper_General::writeLog('ROVW', 'Importing PDF Customer > Customer User List');
		} elseif ($this->_request->getParam('print') == 1) {
			unset($fields['action']);
			$this->_forward('print', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'Customer User List Report', 'data_header' => $fields));
		} else {
			Application_Helper_General::writeLog('CSLS', 'Viewing Customer > Customer User List');
		}



		$select->order($sortBy . ' ' . $sortDir);

		$this->paging($select);
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		$this->view->modulename = $this->_request->getModuleName();


		if (!empty($dataParamValue)) {
			$this->view->lsdateStart = $dataParamValue['PS_LATEST_SUGGESTION'];
			$this->view->lsdateEnd = $dataParamValue['PS_LATEST_SUGGESTION_END'];
			$this->view->ladateStart = $dataParamValue['PS_LATEST_APPROVAL'];
			$this->view->ladateEnd = $dataParamValue['PS_LATEST_APPROVAL_END'];

			unset($dataParamValue['PS_LATEST_SUGGESTION_END']);
			unset($dataParamValue['PS_LATEST_APPROVAL_END']);
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
			$this->view->wherecol     = $wherecol;
			$this->view->whereval     = $whereval;
			// print_r($whereval);die;
		}
	}
}
