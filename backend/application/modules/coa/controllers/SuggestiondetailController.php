<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Settings.php';

class coa_SuggestionDetailController extends Application_Main
{
	 public function initController(){
			  $this->_helper->layout()->setLayout('popup');
	}
	public function indexAction() 
	{
		$set = new Settings();
		$coa = $set->getSetting('charges_acct_IDR');                	
		
		$status = $this->_changeStatus;
		$options = array_combine(array_values($status['code']),array_values($status['desc']));
		
		$changesid = $this->_getparam('changes_id');
		$cek = $this->_db->FetchOne("SELECT SETTING_VALUE FROM TEMP_SETTING WHERE MODULE_ID ='COA'");
//		select()
//							->FROM (array('A' => 'TEMP_SETTING'),array('SETTING_VALUE'))
//							->WHERE ('MODULE_ID = ?','COA')
//							);
		if($cek == null){
			$this->view->cek = 0;		
		}
		else{
			$select	= $this->_db->FetchRow("SELECT CHANGES_STATUS , CREATED , CREATED_BY  FROM T_GLOBAL_CHANGES WHERE CHANGES_ID = '$changesid' ");

			$this->view->created = $select['CREATED'];
			$this->view->createdby = $select['CREATED_BY'];
			foreach($options as $codestat=>$descstat)
			{
				if($codestat == $select['CHANGES_STATUS'])
				{
					$this->view->status = $descstat;
				}
			}
						
			$this->view->cek = 1;
			$this->view->oldCOAAccount = $coa;
			$this->view->newCOAAccount = $cek;
		}
	}
}
