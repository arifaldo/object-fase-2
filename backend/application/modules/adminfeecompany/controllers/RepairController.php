<?php


require_once 'Zend/Controller/Action.php';


class adminfeecompany_RepairController extends Application_Main
{
	public function indexAction() 
	{	
		$changeid = $this->_getParam('changes_id');
		
		$select2 = $this->_db->select()->distinct()
					        ->from(array('A' => 'T_GLOBAL_CHANGES'),array('*'));
		$select2 -> where("A.CHANGES_ID LIKE ".$this->_db->quote($changeid));
		$result2 = $this->_db->fetchRow($select2);

		$custid = $result2['KEY_FIELD'];
		
		$select = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER'),array('*'));
		$select -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		$result = $select->query()->FetchAll();
		$this->view->result = $result;
		
		$monthlyfeestatus = $result[0]["CUST_MONTHLYFEE_STATUS"];
		
		if($monthlyfeestatus == 0)
		{
			$this->view->monthlyfeestatus = $this->language->_('Disabled');
		}
		if($monthlyfeestatus == 1)
		{
			$this->view->monthlyfeestatus = $this->language->_('Enabled');
		}
			
		$custname = $result[0]['CUST_NAME'];
		
		$selectprk = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER_ACCT'),array('A.ACCT_NO', 'A.CCY_ID', 'A.ACCT_NAME'));
		$selectprk -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		
		$selectnoprk = $this->_db->select()
					        ->from(array('A' => 'M_BENEFICIARY'),array('ACCT_NO' => 'A.BENEFICIARY_ACCOUNT', 'CCY_ID' => 'A.CURR_CODE', 'ACCT_NAME' => 'A.BENEFICIARY_NAME'));
		$selectnoprk -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		
		$selectnoprk2 = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER_ACCT'),array('A.ACCT_NO', 'A.CCY_ID', 'A.ACCT_NAME'));
		$selectnoprk2 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		$selectnoprk2 -> where("A.ACCT_SOURCE NOT LIKE '1'");
		
		//$selectprk = $selectprk->__toString();
		//$selectnoprk = $selectnoprk->__toString();
		//$selectnoprk2 = $selectnoprk2->__toString();
		
		//$unionquery = $this->_db->select()
								//->union(array($selectprk,$selectnoprk));
		
		$resultunion = $selectprk->query()->FetchAll();
		$this->view->resultaccount = $resultunion;
								
		//$selectunion = $this->_db->select()
		//					->from (($unionquery),array('*'));
		//$resultunion = $selectunion->query()->FetchAll();
		//$this->view->resultaccount = $resultunion;
		//Zend_Debug::dump($resultunion);die;
		
		/*$chargeaccount = $this->_db->select()
								->union(array($selectnoprk2,$selectnoprk));*/
		$resultlist = $selectnoprk2->query()->FetchAll();
		$this->view->resultlist = $resultlist;     
		
		$select4 = $this->_db->select()->distinct()
			        	->from(array('A' => 'TEMP_ADMFEE_MONTHLY'),array('*'));
		$select4 -> where("A.CHANGES_ID LIKE ".$this->_db->quote($changeid));
		$select4 -> where("MONTHLYFEE_TYPE = '2'");
		$result4 = $this->_db->FetchRow($select4);

   		$idamt = 'amount';
		$amt = Application_Helper_General::convertDisplayMoney($result4['AMOUNT']);
		$cekacct = 'cekacct'; 
		$this->view->$idamt = $amt;
		$this->view->$cekacct = $result4['CHARGES_ACCT_NO'];
		
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		

		$submit = $this->_getParam('submit');
		$reset = $this->_getParam('reset');
		
		if($submit)
		{
			Application_Helper_General::writeLog('CHCR','Repairing Administration fee company charges ('.$changeid.')');
			
			$error = 0;
			$cekacct = 'cekacct';
			$idamt = 'amount';
				
   				//Zend_Debug::dump($idamt); die;
				$cekamt = $this->_getParam($idamt);
				$amt = Application_Helper_General::convertDisplayMoney($cekamt);
				$temp_amt = str_replace('.','',$amt);
				$cek_angka = (Zend_Validate::is($temp_amt,'Digits')) ? true : false;
				$this->view->$idamt = $cekamt;
				
				$cekcekacct = $this->_getParam('acct');
   				$cek_angka2 = (Zend_Validate::is($cekcekacct,'Digits')) ? true : false;
   				$this->view->$cekacct = $cekcekacct;
   				//Zend_Debug::dump($cekfrom); die;
   				
				if($cek_angka == false)
					{
						$errid = 'erramount';
						$err = $this->language->_('Amount value has to be numeric');
						$this->view->$errid = $err;
						$error++;
						//die;
					}
					
				if($cekcekacct != null && $cekcekacct != "")
					{
						if($cek_angka2 == false)
						{
							$errid = 'erracct';
							$err = $this->language->_('Account value has to be numeric');
							$this->view->$errid = $err;
							$error++;
							//die;
						}
					}
				
				if(!$cekcekacct)
				{
					$errid = 'erracct';
					$err = $this->language->_('Charges Account must be choosen');
					$this->view->$errid = $err;
					$error++;
				}
   				
				//Zend_Debug::dump($error); die;
					
				if($error == 0)
				{
					//die;
					$this->_db->beginTransaction();
					try
					{
					$info = 'Charges';
					$info2 = $this->language->_('Set Charges Administration fee per Company');
					
					$this->updateGlobalChanges($changeid,$info);
				
					$where = array('CHANGES_ID = ?' => $changeid);
					$this->_db->delete('TEMP_ADMFEE_MONTHLY',$where);

					$cekacct = 'acct';
					$idamt = 'amount';
						
					$cekamt = $this->_getParam($idamt);
					$amt = Application_Helper_General::convertDisplayMoney($cekamt);
						
					$cekcekacct = $this->_getParam('acct');
						
					if($cekamt && $cekcekacct)
					{
						$data= array(
										'CHANGES_ID' 		=> $changeid,
										'CUST_ID' 			=> $custid,
										'ACCT_NO' 			=> $cekcekacct,
										'AMOUNT' 			=> $amt,
										'MONTHLYFEE_TYPE' 	=> '2',
										'CHARGES_ACCT_NO'	=> $cekcekacct
									);
						$this->_db->insert('TEMP_ADMFEE_MONTHLY',$data);										
						//echo 'aaa';die;
					}
					
					if($error == 0)
					{	
						//die;
						//Zend_Debug::dump('commit'); die;
						$this->_db->commit();
						$this->_redirect('/popup/successpopup/index');
					}
				}
				
				catch(Exception $e)
				{
					//Zend_Debug::dump('rollback'); die;
					$this->_db->rollBack();
				}
				
			}		
		}
		else
		{
			Application_Helper_General::writeLog('CHCR','View Repair Administration fee company charges page ('.$changeid.')');
		}
	}
}