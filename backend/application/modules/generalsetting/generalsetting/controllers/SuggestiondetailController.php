<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Settings.php';

class generalsetting_SuggestionDetailController extends Application_Main
{
	public function initController(){
		$this->_helper->layout()->setLayout('newpopup');
	}
	public function indexAction()
	{
		$status = $this->_changeStatus;
		$options = array_combine(array_values($status['code']),array_values($status['desc']));

		$changesid = $this->_getparam('changes_id');

		$select = $this->_db->SELECT()
		->FROM(array('A' => 'T_GLOBAL_CHANGES'),array('*'))
		->WHERE("A.CHANGES_ID = ?",$changesid)
		->WHERE("A.CHANGES_STATUS = 'WA' OR A.CHANGES_STATUS = 'RR'");
		$result = $this->_db->fetchRow($select);
		if(!$result)
		{
			$this->_redirect('/notification/invalid/index');
		}

		$cek = $this->_db->fetchone($this->_db->select()
			->from(('T_GLOBAL_CHANGES'),array('MODULE'))
			->where('MODULE = ?' , 'generalsetting')
			->where('CHANGES_ID= ?' , $changesid)
			->where("CHANGES_STATUS IN ('WA','RR')"));

		$selectnew = $this->_db->select()
		->from(array('B' => 'TEMP_SETTING'),array('SETTING_ID','SETTING_VALUE'))
					// ->where('MODULE_ID = ?' , 'GNS')
		->where('CHANGES_ID= ?' , $changesid)
		->query()->FetchAll();
					// echo "<pre>";
					// print_r($selectnew);die;
		
		if(!$cek){
			$this->view->cek = 0;
			$this->view->changeId = $changesid;
		}
		else{
			$select	= $this->_db->FetchRow("SELECT CHANGES_STATUS , CREATED , CREATED_BY  FROM T_GLOBAL_CHANGES WHERE CHANGES_ID = ".$this->_db->quote($changesid));
			$this->view->created = $select['CREATED'];
			$this->view->createdby = $select['CREATED_BY'];
			foreach($options as $codestat=>$descstat)
			{
				if($codestat == $select['CHANGES_STATUS'])
				{
					$this->view->status = $descstat;
				}
			}

			$this->view->cek = 1;

//			DATA LAMA
			$selectold = $this->_db->select()
			->from(array('A' => 'M_SETTING'),array('SETTING_ID','SETTING_VALUE'))
					// ->where('MODULE_ID = ?' , 'GNS')
			->query()->FetchAll();

			foreach($selectold as $row)
			{
				$key = $row['SETTING_ID'];
				$this->view->$key = $row['SETTING_VALUE'];
			}

//			DATA BARU
			foreach($selectnew as $row)
			{
				$new = "new".$row['SETTING_ID'];
				$this->view->$new = $row['SETTING_VALUE'];
			}

			if (isset($this->view->tokentype)) {
				$tokenTypeCode = $this->_tokenType['code'];
				$tokenTypeDesc = $this->_tokenType['desc'];

				unset($tokenTypeCode['notoken']); 
				unset($tokenTypeCode['smstoken']);
				unset($tokenTypeCode['mobiletoken']);

				$tokenTypeCodeFlip = array_flip($tokenTypeCode);

				$this->view->tokentype = $tokenTypeDesc[$tokenTypeCodeFlip[$this->view->tokentype]];
				$this->view->newtokentype = $tokenTypeDesc[$tokenTypeCodeFlip[$this->view->newtokentype]];
			}

				// echo "<pre>";
				// print_r($this->view);die();
				// echo '<pre>';
				// print_r($selectnew);die;
			$this->view->colomnlist = $selectnew;
			

			$this->view->changes_id = $this->_getParam('changes_id');
			if(!$this->_request->isPost()){
				Application_Helper_General::writeLog('STCL','View Suggestion Detail General Setting');
			}
		}
	}
}
