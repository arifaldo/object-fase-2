<?php

require_once 'Zend/Controller/Action.php';

class Generalsetting_FousersettingController extends Application_Main
{

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$select = $this->_db->select()
		->from(array('A' => 'M_SETTING'),array('SETTING_ID','SETTING_VALUE'))
		->where('MODULE_ID = ?' , 'GNS')
		->query()->FetchAll();
		$setting = Application_Helper_Array::listArray($select,'SETTING_ID','SETTING_VALUE');

		foreach($setting as $key=>$value)
		{
			if(!empty($key))
				$this->view->$key = $value;
		}

		// convert to validate
//			$threshold_rtgs = $this->_getParam('threshold_rtgs');		
//			$threshold_rtgs_val =	Application_Helper_General::convertDisplayMoney($threshold_rtgs);
//			$this->_setParam('threshold_rtgs',$threshold_rtgs_val);

		$threshold_lld = $this->_getParam('threshold_lld');		
		$threshold_lld_val =	Application_Helper_General::convertDisplayMoney($threshold_lld);
		$this->_setParam('threshold_lld',$threshold_lld_val);

		$admin_fee_company = $this->_getParam('admin_fee_company');		
		$admin_fee_company_val =	Application_Helper_General::convertDisplayMoney($admin_fee_company);
		$this->_setParam('admin_fee_company',$admin_fee_company_val);

		$admin_fee_account = $this->_getParam('admin_fee_account');		
		$admin_fee_account_val =	Application_Helper_General::convertDisplayMoney($admin_fee_account);
		$this->_setParam('admin_fee_account',$admin_fee_account_val);

		$global_charges_skn = $this->_getParam('global_charges_skn');		
		$global_charges_skn_val =	Application_Helper_General::convertDisplayMoney($global_charges_skn);
		$this->_setParam('global_charges_skn',$global_charges_skn_val);

		$global_charges_rtgs = $this->_getParam('global_charges_rtgs');		
		$global_charges_rtgs_val =	Application_Helper_General::convertDisplayMoney($global_charges_rtgs);
		$this->_setParam('global_charges_rtgs',$global_charges_rtgs_val);

		$threshold_lld_remittance = $this->_getParam('threshold_lld_remittance');
		$threshold_lld_remittance_val =	Application_Helper_General::convertDisplayMoney($threshold_lld_remittance);
		$this->_setParam('threshold_lld_remittance',$threshold_lld_remittance_val);

		$remittance_limit_permonthly = $this->_getParam('remittance_limit_permonthly');		
		$remittance_limit_permonthly_val =	Application_Helper_General::convertDisplayMoney($remittance_limit_permonthly);
		$this->_setParam('remittance_limit_permonthly',$remittance_limit_permonthly_val);


		$min_amount_skn = $this->_getParam('min_amount_skn');		
		$min_amount_skn_val =	Application_Helper_General::convertDisplayMoney($min_amount_skn);
		$this->_setParam('min_amount_skn',$min_amount_skn_val);

		$max_amount_skn = $this->_getParam('max_amount_skn');		
		$max_amount_skn_val =	Application_Helper_General::convertDisplayMoney($max_amount_skn);
		$this->_setParam('max_amount_skn',$max_amount_skn_val);

		$min_amount_rtgs = $this->_getParam('min_amount_rtgs');		
		$min_amount_rtgs_val =	Application_Helper_General::convertDisplayMoney($min_amount_rtgs);
		$this->_setParam('min_amount_rtgs',$min_amount_rtgs_val);

		$max_amount_rtgs = $this->_getParam('max_amount_rtgs');		
		$max_amount_rtgs_val =	Application_Helper_General::convertDisplayMoney($max_amount_rtgs);
		$this->_setParam('max_amount_rtgs',$max_amount_rtgs_val);

		$escrow_acct_idr = $this->_getParam('escrow_acct_idr');
			// $escrow_acct_idr_val =	Application_Helper_General::convertDisplayMoney($escrow_acct_idr);
		$this->_setParam('escrow_acct_idr',$escrow_acct_idr);

		$escrow_acct_usd = $this->_getParam('escrow_acct_usd');
			// $escrow_acct_usd_val =	Application_Helper_General::convertDisplayMoney($escrow_acct_usd);
		$this->_setParam('escrow_acct_usd',$escrow_acct_usd);

		$remittance_limit_per_month = $this->_getParam('remittance_limit_per_month');
		$remittance_limit_per_month_val =	Application_Helper_General::convertDisplayMoney($remittance_limit_per_month);
		$this->_setParam('remittance_limit_per_month',$remittance_limit_per_month_val);

			/* $SKN_cost = $this->_getParam('SKN_cost');		
			$SKN_cost_val =	Application_Helper_General::convertDisplayMoney($SKN_cost);
			$this->_setParam('SKN_cost',$SKN_cost_val);

			$RTGS_cost = $this->_getParam('RTGS_cost');		
			$RTGS_cost_val =	Application_Helper_General::convertDisplayMoney($RTGS_cost);
			$this->_setParam('RTGS_cost',$RTGS_cost_val); */

			/* $SKN_charge = $this->_getParam('SKN_charge');		
			$SKN_charge_val =	Application_Helper_General::convertDisplayMoney($SKN_charge);
			$this->_setParam('SKN_charge',$SKN_charge_val);

			$RTGS_charge = $this->_getParam('RTGS_charge');		
			$RTGS_charge_val =	Application_Helper_General::convertDisplayMoney($RTGS_cost);
			$this->_setParam('RTGS_charge',$RTGS_charge_val); */

			//FILTER
			{
				$filters = array(
					'timeoutafter' => array('StringTrim','StripTags'),
					'locknologinafter' => array('StringTrim','StripTags'),
					'lockfailloginafter' => array('StringTrim','StripTags'),
					'lockfailtokenafter' => array('StringTrim','StripTags'),
					'password_history' => array('StringTrim','StripTags'),
					'min_length_userid' => array('StringTrim','StripTags'),
					'max_length_userid' => array('StringTrim','StripTags'),
					'minfpassword' => array('StringTrim','StripTags'),
					'maxfpassword' => array('StringTrim','StripTags'),
					'b_lockfailloginafter' => array('StringTrim','StripTags'),

				);
			}

			//VALIDATE
			{
				$validators = array(

					'timeoutafter' => array('NotEmpty',
						'Digits',
						array('Between', array('min'=>1,'max'=>10000)),
						'messages' => array
						('Inputan tidak boleh kosong',
							'Inputan harus numeric',
							'Inputan minimal 1 sampai dengan'." ".number_format(10000))),
					'locknologinafter' => array('NotEmpty',
						'Digits',
						array('Between', array('min'=>1,'max'=>365)),
						'messages' => array
						('Inputan tidak boleh kosong',
							'Inputan harus numeric',
							'Inputan minimal 1 sampai dengan 365')),
					'lockfailloginafter' => array('NotEmpty',
						'Digits',
						array('Between',array('min'=>1,'max'=>10)),
						'messages' => array
						('Inputan tidak boleh kosong',
							'Inputan harus numeric',
							'Inputan minimal 1 sampai dengan 10')),
					'lockfailtokenafter' => array('NotEmpty',
						'Digits',
						array('Between', array('min'=>1,'max'=>10)),
						'messages' => array
						('Inputan tidak boleh kosong',
							'Inputan harus numeric',
							'Inputan minimal 1 sampai dengan 10')),
					'password_history' => array('NotEmpty',
						'Digits',
						array('Between', array('min'=>1,'max'=>10)),
						'messages' => array
						('Inputan tidak boleh kosong',
							'Inputan harus numeric',
							'Inputan minimal 1 sampai dengan 10')),
					'min_length_userid' => array('NotEmpty',
						'Digits',
						array('Between', array('min'=>1,'max'=>$this->_getParam('max_length_userid'))),
						'messages' => array
						('Inputan tidak boleh kosong',
							'Inputan harus numeric',
							'Inputan awal User ID minimal 1 sampai dengan kurang dari inputan akhir User ID')),
					'max_length_userid' => array('NotEmpty',
						'Digits',
						array('Between', array('min'=>$this->_getParam('min_length_userid'),'max'=>16)),
						'messages' => array
						('Inputan tidak boleh kosong',
							'Inputan harus numeric',
							'inputan maksimal User ID harus lebih dari inputan awal User ID dan sampai dengan 16 karakter')),

					'minfpassword' => array('NotEmpty',
						'Digits',
						array('Between', array('min'=>1,'max'=>$this->_getParam('max_length_userid'))),
						'messages' => array
						('Inputan tidak boleh kosong',
							'Inputan harus numeric',
							'Inputan panjang password minimal 1 sampai dengan kurang dari inputan akhir password')),
					'maxfpassword' => array('NotEmpty',
						'Digits',
						array('Between', array('min'=>$this->_getParam('min_length_userid'),'max'=>13)),
						'messages' => array
						('Inputan tidak boleh kosong',
							'Inputan harus numeric',
							'inputan maksimal password harus lebih dari inputan awal password dan sampai dengan 13 karakter')),

					'pwd_expired_day' => array('NotEmpty',
						'Digits',
						array('Between', array('min'=>1,'max'=>90)),
						'messages' => array
						('Inputan tidak boleh kosong',
							'Inputan harus numeric',
							'Minimal angka 1 sampai dengan angka 90')),

					'b_lockfailloginafter' => array('NotEmpty',
						'Digits',
						array('StringLength',array('min'=>1,'max'=>13)),
						'messages' => array
						('Can not be empty',
							'Must be numeric values',
							'Too many significant digits. Maximum digit allowed 13 digit(s)')),
				);
			}

			$zf_filter = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

			$arrfck = array('locknologinafter','lockfailloginafter','lockfailtokenafter','password_history','min_length_userid','max_length_userid','minfpassword','maxfpassword','pwd_expired_day','timeoutafter');

			$cek = $this->_db->select()
			->FROM (array('B' => 'TEMP_SETTING'),array('MODULE_ID'))
			->WHERE ('MODULE_ID = ?','GNS')
			->query()->FetchAll();
			if($cek == null)
			{
				if($this->_request->isPost())
				{


					if($zf_filter->isValid())
					{
						$this->_db->beginTransaction();
						try{

							$info = "GENERAL SETTING";
							$change_id = $this->suggestionWaitingApproval('Pengaturan Nasabah',$info,$this->_changeType['code']['edit'],null,'M_SETTING','TEMP_SETTING','','');
								//$change_id = 1;

							foreach($setting as $key=>$value){
								if (in_array($key, $arrfck)) {
									$valuee = $this->_getParam2($key);
									$data = array(	'CHANGES_ID' => $change_id,
										'SETTING_ID' => $key,
										'SETTING_VALUE' => htmlspecialchars_decode($valuee),
										'MODULE_ID' => 'GNS',
									);
									$this->_db->insert('TEMP_SETTING',$data);	
								}


							}

							Application_Helper_General::writeLog('STUD','Update General Setting');
							$this->_db->commit();
							$this->view->success = true;
							$msg = "Settings Change Request Saved";
							$this->view->report_msg = $msg;
						}
						catch(Exception $e)
						{
							var_dump($e);die;
							$this->_db->rollBack();
						}
					}
					else{
						$docErr = 'Error in processing form values. Please correct values and re-submit.';
						$this->view->report_msg = $docErr;

						foreach($zf_filter->getMessages() as $key=>$err){
							$xxx = 'x'.$key;
							$this->view->$xxx = $this->displayError($err);
						}
					}
				}
			}else{
				$this->view->error = true;
					//$this->fillParam($zf_filter_input);
				$this->view->report_msg = 'No changes allowed for this record while awaiting approval for previous change.';
			}


			if($this->_request->isPost()){

				foreach($setting as $key=>$value)
				{
					if (in_array($key, $arrfck)){
						$this->view->$key =   $this->_getParam($key);
					}
					else{
						$this->view->$key = (!empty($zf_filter->$key)) ? $zf_filter->$key : $this->_getParam($key);
					}
				}

				$disable_note = $zf_filter->disable_note;
				$note = $zf_filter->note;
			}
			else{
				$disable_note = $setting['disable_note'];
				$note = $setting['note'];
				Application_Helper_General::writeLog('STLS','View General Setting');
			}

			$disable_note_len 	 = (isset($disable_note))  ? strlen($disable_note)  : 0;
			$note_len 	 = (isset($note))  ? strlen($note)  : 0;

			$disable_note_len 	 = 200 - $disable_note_len;
			$note_len 	 = 200 - $note_len;

			$this->view->disable_note_len		= $disable_note_len;
			$this->view->note_len		= $note_len;


		}
	}