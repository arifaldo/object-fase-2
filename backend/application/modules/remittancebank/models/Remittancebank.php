<?php
Class remittancebank_Model_Remittancebank {
	
	protected $_db;

    // constructor
	public function __construct()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
	}
  
    public function getCountry()
    { 
       $select = $this->_db->select()
                           ->from('M_COUNTRY')
                           ->query()->fetchAll();
        return $select;
    }
}