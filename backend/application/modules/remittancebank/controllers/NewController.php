<?php

require_once 'Zend/Controller/Action.php';

class Remittancebank_NewController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti


    public function indexAction()
	{
	    //pengaturan url untuk button back
	    //$this->setbackURL('/'.$this->_request->getModuleName().'/index');
			$this->_helper->layout()->setLayout('newlayout');    
	    $this->setbackURL('/remittancebank/index');

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}
    	}

    	$this->view->report_msg = array();

    	$selectcountry = $this->_db->select()
		->from('M_COUNTRY')
		->query()->fetchAll();
// 	    $this->view->countryArr = ( array(''=>'-- '.$this->language->_('Please Select').' --')+Application_Helper_Array::listArray($model->getCountry(),'COUNTRY_CODE','COUNTRY_NAME'));
	    $this->view->countryArr = $selectcountry;

		if($this->_request->isPost() && $this->_getParam('submit'))
		{
			$filters = array(
			                 'bank_code' => array('StringTrim','StripTags','StringToUpper'),
							 'bank_name' => array('StringTrim','StripTags','StringToUpper'),
							 'bank_address1' => array('StringTrim','StripTags','StringToUpper'),
							 'bank_address2' => array('StringTrim','StripTags','StringToUpper'),
							 'city_name'=> array('StringTrim','StripTags','StringToUpper'),
							 'country_code'=> array('StringTrim','StripTags','StringToUpper'),
							 'pob_number' => array('StringTrim','StripTags','StringToUpper')
							);

			$validators = array(
			                    'bank_code' => array('NotEmpty',
			                                         new Zend_Validate_StringLength(array('max'=>12)),
													 'messages' => array(
																         $this->language->_('Can not be empty'),
																         $this->language->_('Data too long (max 12 chars)'),
			                                                             )
													),
								'bank_name'      => array('NotEmpty',
													 new Zend_Validate_StringLength(array('max'=>105)),
													 'messages' => array(
																		$this->language->_('Can not be empty'),
																         $this->language->_('Data too long (max 105 chars)'),
																         )
														),
								'bank_address1'      => array('NotEmpty',//'allowEmpty' => true,
													 new Zend_Validate_StringLength(array('max'=>140)),
													 'messages' => array(
													 					 $this->language->_('Can not be empty'),
																         $this->language->_('Data too long (max 140 chars)'),
																         )
														),
								'bank_address2'      => array('allowEmpty' => true,//'NotEmpty',
													 new Zend_Validate_StringLength(array('max'=>140)),
													 'messages' => array(
																		 //$this->language->_('Can not be empty'),
																         $this->language->_('Data too long (max 140 chars)'),
																         )
														),
								'city_name'      => array('NotEmpty',
													 new Zend_Validate_StringLength(array('max'=>35)),
													 'messages' => array(
																		$this->language->_('Can not be empty'),
																         $this->language->_('Data too long (max 35 chars)'),
																         )
														),
								'country_code'      => array('NotEmpty',
													 new Zend_Validate_StringLength(array('max'=>3)),
													 'messages' => array(
																		$this->language->_('Can not be empty'),
																         $this->language->_('Data too long (max 3 chars)'),
																         )
														),
								'pob_number'      => array('allowEmpty' => true, //'NotEmpty',
													 new Zend_Validate_StringLength(array('max'=>35)),
													 'messages' => array(
																		//$this->language->_('Can not be empty'),
																         $this->language->_('Data too long (max 35 chars)'),
																         )
														)
							   );


			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

			if($zf_filter_input->isValid())
			{
				//$countrycode = substr($zf_filter_input->bank_code,4,2);
				$content = array(
								'bank_code' 	 => $zf_filter_input->bank_code,
								'bank_name' 	 => $zf_filter_input->bank_name,
								'bank_address1' => $zf_filter_input->bank_address1,
								'bank_address2'	 => $zf_filter_input->bank_address2,
 								'city_name' 	 => $zf_filter_input->city_name,
 								'pob_number' => $zf_filter_input->pob_number,
 								'country_code' => $zf_filter_input->country_code,
 								'created'	=> date('Y-m-d H:i:s'),
 								'createdby' => $this->_userIdLogin
						       );

				try
				{

					//-------- insert --------------
					$this->_db->beginTransaction();

					$this->_db->insert('M_BANK_REMITTANCE',$content);

					// $this->_db->commit();
					//$id = $this->_db->lastInsertId();
					Application_Helper_General::writeLog('RBAD','Add Remittance Bank. Bank Code : ['.$zf_filter_input->bank_code.'], Bank Name : ['.$zf_filter_input->bank_name.']');
					$this->view->success = true;
					$this->view->report_msg = array();

					// $this->_redirect('/notification/success/index');
				}
				catch(Exception $e)
				{
					//rollback changes
					$this->_db->rollBack();

					foreach(array_keys($filters) as $field)
						$this->view->$field = $zf_filter_input->getEscaped($field);

					$errorMsg = 'exception';
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
				}
			}
			else
			{
				$this->view->error = true;
				foreach(array_keys($filters) as $field)
						$this->view->$field = ($zf_filter_input->isValid($field))? $zf_filter_input->getEscaped($field) : $this->_getParam($field);

				$error = $zf_filter_input->getMessages();

				//format error utk ditampilkan di view html
                $errorArray = null;
		        foreach($error as $keyRoot => $rowError)
		        {
		           foreach($rowError as $errorString)
		           {
		              $errorArray[$keyRoot] = $errorString;
		           }
		        }

		        $this->view->succes = false;
                $this->view->report_msg = $errorArray;
			}
		}

		Application_Helper_General::writeLog('RBAD','Add Remittance Bank');
	}

}
