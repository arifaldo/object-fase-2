<?php

require_once 'Zend/Controller/Action.php';

class Remittancebank_DeleteController extends Application_Main 
{

    protected $_moduleDB = 'RTF'; //masih harus diganti

	public function indexAction() 
	{ 
	    //pengaturan url untuk button back
	    $this->setbackURL('/'.$this->_request->getModuleName().'/index');  
	
	    $filters = array('bank_code' => array('StringTrim', 'StripTags','StringToUpper'));
							 
		$validators =  array(
					    'bank_code' => array(
								      'NotEmpty', 
							          array('Db_RecordExists', array('table' => 'M_BANK_REMITTANCE', 'field' => 'bank_code')),
							       	  'messages' => array(
							   						  $this->language->_('Cannot be empty'),
							   					      $this->language->_('Bank Code is not found'),
							   						     ) 
							             )
					        );
			
		if(array_key_exists('bank_code',$this->_request->getParams()))
		{
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			
			if($zf_filter_input->isValid()) 
			{
				try 
				{   
				    $this->_db->beginTransaction();
				    
					$bankCode  = $zf_filter_input->getEscaped('bank_code'); 
					
					
					$this->_db->delete('M_BANK_REMITTANCE','bank_code = '.$this->_db->quote($bankCode));
				   	
					
					$this->_db->commit();		
						 
					Application_Helper_General::writeLog('RBUD','Delete Remittance Bank. Bank Code : ['.$bankCode.']');
					$this->_redirect('/notification/success/index');
				    
						
				}
				catch(Exception $e) 
				{
					//rollback changes
					$this->_db->rollBack();
					
					$errorMsg = $this->getErrorRemark('82');
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);

					//$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/failed');
					$this->_redirect('/'.$this->_request->getModuleName().'/index');
				}
			}
			else
			{
				$this->view->error = true;
				$errors = $zf_filter_input->getMessages();
				
				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				foreach($errors as $key => $error)
				{
					foreach($error as $key2 => $errorMsg)
					{
						$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
						
						Application_Helper_General::writeLog('RBUD','Update Remittance Bank');
						
						//$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/failed');
						$this->_redirect('/'.$this->_request->getModuleName().'/index');
					}					
				}	
			} 
			
		}
		else
		{
			$errorMsg = '';
			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
			$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
					
			Application_Helper_General::writeLog('RBUD','Update Remittance Bank');
			
			//$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/failed');
			$this->_redirect('/'.$this->_request->getModuleName().'/index');
		}
	}
	
	
	public function successAction()
	{
		$this->_redirect($this->_backURL);
	}
	
	public function failedAction()
	{
		$this->_redirect($this->_backURL);
	}

}
