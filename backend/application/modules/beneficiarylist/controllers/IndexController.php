<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';
class Beneficiarylist_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti
	protected $_bankstatuscode = array();
	protected $_bankstatusdesc = array();
	protected $_beneftypecode = array();
	protected $_beneftypedesc = array();

	public function initController()
	{
		$companyCode = $this->_db->fetchAll(
					$this->_db->select()
					   ->from('M_CUSTOMER',array('CUST_ID'))
					   );
		$listCompCode = array(''=>'--- '.$this->language->_('Any Value').' ---');
		$listCompCode += Application_Helper_Array::listArray($companyCode,'CUST_ID','CUST_ID');
		$this->view->listcompcode = $listCompCode;

		$bankstatuscode = $this->_bankstatus['code'];
		$bankstatusdesc = $this->_bankstatus['desc'];

		$bankStatus = array_combine(array_values($bankstatuscode),array_values($bankstatusdesc));
		$listStatus = array(''=>'-- '.$this->language->_('Any Value').' --');
		$listStatus += $bankStatus;
		$this->view->liststatus = $listStatus;
		$this->view->bankstatuscode = $this->_bankstatuscode = array_flip($bankstatuscode);
		$this->view->bankstatusdesc = $this->_bankstatusdesc = $bankstatusdesc;


		//$beneftypecode = $this->_paymenttype['code'];
		//$beneftypedesc = $this->_paymenttype['desc'];

		$beneftypecode = $this->_beneftype['code'];
		$beneftypedesc = $this->_beneftype['desc'];
		$benefType = array (
					  1 => "Mayapada",
					  2 => "Others",
					  8 => "Online",
					  3 => "Remittance"
					);
		$listType = array(''=>'-- '.$this->language->_('Any Value').' --');
		$listType += $benefType;

		$this->view->listtype = $listType;
		$this->view->beneftypecode = $this->_beneftypecode = array_flip($beneftypecode);
		$this->view->beneftypedesc = $this->_beneftypedesc = $beneftypedesc;
	}

	public function indexAction()
	{

		$setting = new Settings();			  	
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
		$pw_hash = md5($enc_salt.$enc_pass);
		$rand = $this->_userIdLogin.date('dHis').$pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;
		
		$this->_helper->layout()->setLayout('newlayout');

		$fields = array(

						'company'  => array('field' => 'CUST_NAME',
											   'label' => $this->language->_('Company'),
											   'sortable' => true),
						'benef_acct'  => array('field' => 'BENEFICIARY_ACCOUNT',
											   'label' => $this->language->_('Beneficiary Account'),
											   'sortable' => true),
						'benef_bank'  => array('field' => 'BANK_NAME',
						 					   'label' => $this->language->_('Bank Name'),
						 					   'sortable' => true),
// 						'alias'  => array('field' => 'BENEFICIARY_ALIAS',
// 											   'label' => $this->language->_('Beneficiary Alias'),
// 											   'sortable' => true),
						'ccy'   => array('field'    => 'CURR_CODE',
											  'label'    => $this->language->_('CCY'),
											  'sortable' => true),
						'createdby'   => array('field'    => 'BENEFICIARY_CREATEDBY',
											  'label'    => $this->language->_('Created By'),
											  'sortable' => true),
						'created'   => array('field'    => 'BENEFICIARY_CREATED',
											  'label'    => $this->language->_('Created Date'),
											  'sortable' => true),
						'type'   => array('field'    => 'BENEFICIARY_TYPE',
											  'label'    => $this->language->_('Type'),
											  'sortable' => true),
						// 'bankstatus' => array('field'    => 'BENEFICIARY_BANKSTATUS',
											  // 'label'    => 'Bank Status',
											  // 'sortable' => true),
						'approve'   => array('field'    => 'BENEFICIARY_ISAPPROVE',
											  'label'    => $this->language->_('Approved Status'),
											  'sortable' => true),
// 						'delete'   => array('field'    => 'BENEFICIARY_ISREQUEST_DELETE',
// 											  'label'    => $this->language->_('Request to Delete'),
// 											  'sortable' => true)
				);

		$filterlist = array("COMP_CODE","COMP_NAME","BENEF_ACCT","BENEF_NAME","BENEF_TYPE","CURR_CODE","BENEFICIARY_CREATEDBY","BENEFICIARY_CREATED","BENEFICIARY_ISAPPROVE");

		$this->view->filterlist = $filterlist;

		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','doc_no');
		$sortDir = $this->_getParam('sortdir','asc');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;

		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';


		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	'filter' 	  	=> array('StringTrim','StripTags'),
							'COMP_CODE' 	=> array('StringTrim','StripTags','StringToUpper'),
							'COMP_NAME'    	=> array('StringTrim','StripTags','StringToUpper'),
							'BENEF_ACCT'    => array('StringTrim','StripTags'),
							'BENEF_NAME'    => array('StringTrim','StripTags','StringToUpper'),
							'CURR_CODE'    => array('StringTrim','StripTags','StringToUpper'),
							'BENEFICIARY_CREATEDBY'    => array('StringTrim','StripTags','StringToUpper'),
							'BENEFICIARY_CREATED'    => array('StringTrim','StripTags','StringToUpper'),
							'BENEFICIARY_ISAPPROVE'    => array('StringTrim','StripTags','StringToUpper'),
							// 'status'    	=> array('StringTrim','StripTags'),
							'BENEF_TYPE'    		=> array('StringTrim','StripTags'),
		);

		 $dataParam = array("COMP_CODE","COMP_NAME","BENEF_ACCT","BENEF_NAME","BENEF_TYPE","CURR_CODE","BENEFICIARY_CREATEDBY","BENEFICIARY_CREATED","BENEFICIARY_ISAPPROVE");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{

			// print_r($dtParam);die;
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}

			}

			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}

		$zf_filter = new Zend_Filter_Input($filterArr,array(),$dataParamValue);
		// $filter = $zf_filter->getEscaped('filter');
		$filter 		= $this->_getParam('filter');
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');

		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;

		//beneficiary type
		$caseBenefType = "(CASE B.BENEFICIARY_TYPE ";
		foreach($this->_beneftypecode as $key=>$val)
		{
			$caseBenefType .= " WHEN ".$key." THEN '".$this->_beneftypedesc[$val]."' ";
		}
		$caseBenefType .= " END)";

		//bank status
		// $caseBankStatus = "(CASE B.BENEFICIARY_BANKSTATUS ";
		// foreach($this->_bankstatuscode as $key=>$val)
		// {
			// $caseBankStatus .= " WHEN ".$key." THEN '".$this->_bankstatusdesc[$val]."' ";
		// }
		// $caseBankStatus .= " END)";

		$select = $this->_db->select()
					   ->from(array('B' => 'M_BENEFICIARY'),array('CUST_ID','C.CUST_NAME','BENEFICIARY_ACCOUNT','BENEFICIARY_NAME','BANK_NAME','BENEFICIARY_ALIAS','CURR_CODE','BENEFICIARY_CREATEDBY','BENEFICIARY_CREATED','BENEFICIARY_TYPE' => $caseBenefType, // 'BENEFICIARY_BANKSTATUS' => $caseBankStatus,
								'BENEFICIARY_ISAPPROVE','BENEFICIARY_ISREQUEST_DELETE'))
					   ->joinLeft(array('C' => 'M_CUSTOMER'),'B.CUST_ID = C.CUST_ID',array());

	    $select->order($sortBy.' '.$sortDir);

		if($filter == TRUE)
		{
			$fCompCode = $zf_filter->getEscaped('COMP_CODE');
			$fCompName = $zf_filter->getEscaped('COMP_NAME');
			$fAcct = $zf_filter->getEscaped('BENEF_ACCT');
			$fName = $zf_filter->getEscaped('BENEF_NAME');
			$ccyid = $zf_filter->getEscaped('CURR_CODE');
			$bycreate = $zf_filter->getEscaped('BENEFICIARY_CREATEDBY');
			$datecreate = $zf_filter->getEscaped('BENEFICIARY_CREATED');
			$approvestatus = $zf_filter->getEscaped('BENEFICIARY_ISAPPROVE');
			// $fStatus = $zf_filter->getEscaped('status');
			$fType = $zf_filter->getEscaped('BENEF_TYPE');

	        if($fCompCode)$select->where('UPPER(B.CUST_ID) = '.$this->_db->quote(strtoupper($fCompCode)));
	        if($fCompName)$select->where('UPPER(CUST_NAME) LIKE '.$this->_db->quote('%'.strtoupper($fCompName).'%'));
	        if($fAcct)$select->where('BENEFICIARY_ACCOUNT LIKE '.$this->_db->quote('%'.strtoupper($fAcct).'%'));
	        if($fName)$select->where('UPPER(BENEFICIARY_NAME) LIKE '.$this->_db->quote('%'.strtoupper($fName).'%'));
	        if($ccyid)$select->where('UPPER(CURR_CODE) LIKE '.$this->_db->quote('%'.strtoupper($ccyid).'%'));
	        if($bycreate)$select->where('UPPER(BENEFICIARY_CREATEDBY) LIKE '.$this->_db->quote('%'.strtoupper($bycreate).'%'));
	        if($datecreate)$select->where('UPPER(BENEFICIARY_CREATED) LIKE '.$this->_db->quote('%'.strtoupper($datecreate).'%'));
	        if($approvestatus)$select->where('UPPER(BENEFICIARY_ISAPPROVE) LIKE '.$this->_db->quote('%'.strtoupper($approvestatus).'%'));
	        // if($fStatus!=''||$fStatus!=null)$select->where('BENEFICIARY_BANKSTATUS = '.$fStatus);
	        if($fType)$select->where('BENEFICIARY_TYPE = '.$this->_db->quote($fType));

			$this->view->comp_code = $fCompCode;
			$this->view->comp_name = $fCompName;
			$this->view->benef_acct = $fAcct;
			$this->view->benef_name = $fName;
			$this->view->ccyid = $ccyid;
			$this->view->bycreate = $bycreate;
			$this->view->datecreate = $datecreate;
			$this->view->approvestatus = $approvestatus;
			// $this->view->status = $fStatus;
			$this->view->type = $fType;

		}
		
		if($csv || $pdf || $this->_request->getParam('print'))
		{
			$arrLabel = Application_Helper_Array::simpleArray($fields, "label");

			$data = $this->_db->fetchAll($select);

			foreach($data as $key=>$row)
			{
				$data[$key]['BENEFICIARY_CREATEDBY'] = $row['BENEFICIARY_CREATEDBY'];
				$data[$key]['BENEFICIARY_CREATED'] = Application_Helper_General::convertDate($row['BENEFICIARY_CREATED'],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
				$data[$key]['BENEFICIARY_TYPE'] = $row['BENEFICIARY_TYPE'];
				unset($data[$key]['BENEFICIARY_ALIAS']);
				unset($data[$key]['BENEFICIARY_ISREQUEST_DELETE']);
// 				print_r($data[$key]['BENEFICIARY_ISREQUEST_DELETE'])
				// $data[$key]['BENEFICIARY_BANKSTATUS'] = $row['BENEFICIARY_BANKSTATUS'];
				$data[$key]['BENEFICIARY_ISAPPROVE'] = $row['BENEFICIARY_ISAPPROVE']?'Yes':'No';
// 				$data[$key]['BENEFICIARY_ISREQUEST_DELETE'] = $row['BENEFICIARY_ISREQUEST_DELETE']?'Yes':'No';
			}
// 			echo "<pre>";
// 			print_r($data);die;
			if($csv)
			{

				Application_Helper_General::writeLog('RPBN','Download CSV Beneficiary List Report');
				$this->_helper->download->csv($arrLabel,$data,null,'Beneficiary List');
			}
			else if($pdf)
			{
				Application_Helper_General::writeLog('RPBN','Download PDF Beneficiary List Report');
				$this->_helper->download->pdf($arrLabel,$data,null,'Beneficiary List');
			}
			else if($this->_request->getParam('print') == 1){

				foreach($data as $key=>$row)
				{
					$data[$key]['CUST_NAME'] = $row['CUST_NAME'].' ('.$row['CUST_ID'].')';
					$data[$key]['BENEFICIARY_ACCOUNT'] = $row['BENEFICIARY_ACCOUNT'].' - '.$row['BENEFICIARY_NAME'];
					
				}

				// echo "<pre>";
				// var_dump($data);
				// die();

				$this->_forward('print', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'Beneficiary List', 'data_header' => $fields));
			}
		}
		$this->paging($select);
		
		$setting = new Settings();
	    $MasterBankName = $setting->getSetting('master_bank_name');
		$this->view->bankname = $MasterBankName;

		$this->view->fields = $fields;
		$this->view->filter = $filter;

		Application_Helper_General::writeLog('RPBN','View Beneficiary List Report');

		if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;

      }

	}
}
