<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class Beneficiarylist_DetailController extends Application_Main
{

   public function indexAction()
   {

   	$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
    $password = $sessionNamespace->token; 
    $this->view->token = $sessionNamespace->token;


	$this->_helper->layout()->setLayout('newpopup');
	$AESMYSQL = new Crypt_AESMYSQL();
	$acct = $AESMYSQL->decrypt($this->_getParam('acct'), $password);
	$cust_id = $AESMYSQL->decrypt($this->_getParam('cust_id'), $password);


		$bankstatuscode = array_flip($this->_bankstatus['code']);
		$bankstatusdesc = $this->_bankstatus['desc'];

		//$beneftypecode = array_flip($this->_paymenttype['code']);
		//$beneftypedesc = $this->_paymenttype['desc'];

		$beneftypecode = array_flip($this->_beneftype['code']);
		$beneftypedesc = $this->_beneftype['desc'];

    if($acct)
    {
         $select = $this->_db->select()
                             ->from(array('B' => 'M_BENEFICIARY'))
							 ->joinLeft(array('C' => 'M_CUSTOMER'),'B.CUST_ID = C.CUST_ID',array('CUST_NAME'))
							 ->joinLeft(array('CTR'=> 'M_COUNTRY'),'B.BANK_COUNTRY = CTR.COUNTRY_CODE',array('COUNTRY_NAME'))
                             ->where('B.BENEFICIARY_ACCOUNT='.$this->_db->quote($acct))
                             ->where('B.CUST_ID='.$this->_db->quote($cust_id));
        $resultdata = $this->_db->fetchRow($select);

        if($resultdata)
        {
		  $type = (array_key_exists($resultdata['BENEFICIARY_TYPE'],$beneftypecode))? $beneftypedesc[$beneftypecode[$resultdata['BENEFICIARY_TYPE']]] : '';
		  $status = (array_key_exists($resultdata['BENEFICIARY_BANKSTATUS'],$bankstatuscode))? $bankstatusdesc[$bankstatuscode[$resultdata['BENEFICIARY_BANKSTATUS']]] : '';

		  $citizenship = '';
		  if($resultdata['BENEFICIARY_CITIZENSHIP']=='W') $citizenship = 'WNI';
		  else if($resultdata['BENEFICIARY_CITIZENSHIP']=='N') $citizenship = 'WNA';

		  $residentStatus = '';
		  if($resultdata['BENEFICIARY_RESIDENT']=='NR') $residentStatus = 'Non Resident';
		  else if($resultdata['BENEFICIARY_RESIDENT']=='R') $residentStatus = 'Resident';

		  $approve = $resultdata['BENEFICIARY_ISAPPROVE']?'Yes':'No';
		  $delete = $resultdata['BENEFICIARY_ISREQUEST_DELETE']?'Yes':'No';

          $bank = $this->language->_('Bank Name');

          $settings 			= new Application_Settings();
          $LLD_array 			= array();
          $LLD_DESC_arrayCat 	= array();
          $lldTypeArr  		= $settings->getLLDDOMType();


          if (!empty($resultdata['BENEFICIARY_CATEGORY']))
          {
          	$lldCategoryArr  	= $settings->getLLDDOMCategory();
          	$LLD_array["CT"] 	= $resultdata['BENEFICIARY_CATEGORY'];
          	$LLD_CATEGORY_POST = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$resultdata['BENEFICIARY_CATEGORY']];
          }

          if (!empty($resultdata['BENEFICIARY_ID_TYPE']))
          {
          	$lldBeneIdentifArr  	= $settings->getLLDDOMBeneIdentification();
          	$LLD_array["CT"] 	= $resultdata['BENEFICIARY_ID_TYPE'];
          	$LLD_BENEIDENTIF_POST = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldBeneIdentifArr[$resultdata['BENEFICIARY_ID_TYPE']];
          }

          $this->view->comp_code   = $resultdata['CUST_ID'];
      	  $this->view->comp_name  = $resultdata['CUST_NAME'];
      	  $this->view->account     = $resultdata['BENEFICIARY_ACCOUNT'];
      	  $this->view->account_name   = $resultdata['BENEFICIARY_NAME'];
      	  $this->view->account_alias   = $resultdata['BENEFICIARY_ALIAS'];
          $this->view->benef_type = $type;
          $this->view->ccy   = $resultdata['CURR_CODE'];
          $this->view->benef_address   = $resultdata['BENEFICIARY_ADDRESS'];
          $this->view->citizenship   = $citizenship;
          $this->view->residentStatus   = $residentStatus;
          $this->view->bank_name   = $resultdata['BANK_NAME'];
          $this->view->city   = $resultdata['BANK_CITY'];
          $this->view->email   = $resultdata['BENEFICIARY_EMAIL'];
          $this->view->created_date   = $resultdata['BENEFICIARY_CREATED'];
          $this->view->created_by   = $resultdata['BENEFICIARY_CREATEDBY'];
          $this->view->status   = $status;
          $this->view->approve   = $approve;
          $this->view->delete   = $delete;
          $this->view->bank_code = $resultdata['SWIFT_CODE'];
          $this->view->bank_address1 = $resultdata['BANK_ADDRESS1'];
          $this->view->bank_address2 = $resultdata['BANK_ADDRESS2'];
          //$this->view->identical = "";
          $this->view->benef_citizenship = $resultdata['COUNTRY_NAME'];
          $this->view->benef_category = $LLD_CATEGORY_POST;
          $this->view->benef_id_type = $LLD_BENEIDENTIF_POST;
          $this->view->benef_id_no = $resultdata['BENEFICIARY_ID_NUMBER'];
          //$this->view->transactor_rel = "";

			$pdf = $this->_getParam('pdf');
			if($pdf)
			{
				$htmldata = '
				<tr><th colspan="3"><strong>Beneficiary Detail</strong></th></tr>
				<tr>
				  <td class="tbl-evencontent" width="150px">'.$this->language->_('Company Code').'</td>
				  <td class="tbl-evencontent" width="5px">:</td>
				  <td class="tbl-evencontent">'.$resultdata['CUST_ID'].'</td>
				</tr>
				<tr>
				  <td class="tbl-evencontent">'.$this->language->_('Company Name').'</td>
				  <td class="tbl-evencontent">:</td>
				  <td class="tbl-evencontent">'.$resultdata['CUST_NAME'].'</td>
				</tr>
				<tr>
				  <td class="tbl-evencontent">'.$this->language->_('Beneficiary Account').'</td>
				  <td class="tbl-evencontent">:</td>
				  <td class="tbl-evencontent">'.$resultdata['BENEFICIARY_ACCOUNT'].'</td>
				</tr>
				<tr>
				  <td class="tbl-evencontent">'.$this->language->_('Beneficiary Name').'</td>
				  <td class="tbl-evencontent">:</td>
				  <td class="tbl-evencontent">'.$resultdata['BENEFICIARY_NAME'].'</td>
				</tr>
				<tr>
				  <td class="tbl-evencontent">'.$this->language->_('Type').'</td>
				  <td class="tbl-evencontent">:</td>
				  <td class="tbl-evencontent">'.$type.'</td>
				</tr>
				<tr>
				  <td class="tbl-evencontent">'.$this->language->_('Currency').'</td>
				  <td class="tbl-evencontent">:</td>
				  <td class="tbl-evencontent">'.$resultdata['CURR_CODE'].'</td>
				</tr>
				<tr>
				  <td class="tbl-evencontent">'.$this->language->_('Beneficiary Address').'</td>
				  <td class="tbl-evencontent">:</td>
				  <td class="tbl-evencontent">'.$resultdata['BENEFICIARY_ADDRESS'].'</td>
				</tr>
				<tr>
				  <td class="tbl-evencontent">'.$this->language->_('Citizenship').'</td>
				  <td class="tbl-evencontent">:</td>
				  <td class="tbl-evencontent">'.$citizenship.'</td>
				</tr>

				<tr>
				  <td class="tbl-evencontent">'.$this->language->_('Email').'</td>
				  <td class="tbl-evencontent">:</td>
				  <td class="tbl-evencontent">'.$resultdata['BENEFICIARY_EMAIL'].'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; Bank Code</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$resultdata['SWIFT_CODE'].'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; '.$bank.'</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$resultdata['BANK_NAME'].'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; Bank Address 1</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$resultdata['BANK_ADDRESS1'].'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; Bank Address 2</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$resultdata['BANK_ADDRESS2'].'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; '.$this->language->_('Beneficiary Citizenship').'</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent"></td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; '.$this->language->_('Beneficiary Category').'</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$LLD_CATEGORY_POST.'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; '.$this->language->_('Beneficiary Identification Type').'</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$LLD_BENEIDENTIF_POST.'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; '.$this->language->_('Beneficiary Identification Number').'</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$resultdata['BENEFICIARY_ID_NUMBER'].'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; '.$this->language->_('Created Date').'</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.Application_Helper_General::convertDate($resultdata['BENEFICIARY_CREATED'],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat).'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; '.$this->language->_('Created By').'</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$resultdata['BENEFICIARY_CREATEDBY'].'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; '.$this->language->_('Approved Status').'</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$resultdata['BENEFICIARY_CREATEDBY'].'</td>
				</tr>
				  		';
				$dump = '<tr>
				  <td class="tbl-evencontent">'.$this->language->_('Bank').'</td>
				  <td class="tbl-evencontent">:</td>
				  <td class="tbl-evencontent">'.$resultdata['BANK_NAME'].'</td>
				</tr>
				<tr>
				  <td class="tbl-evencontent">'.$this->language->_('City').'</td>
				  <td class="tbl-evencontent">:</td>
				  <td class="tbl-evencontent">'.$resultdata['BANK_CITY'].'</td>
				</tr>
				<tr>
				  <td class="tbl-evencontent">'.$this->language->_('Created Date').'</td>
				  <td class="tbl-evencontent">:</td>
				  <td class="tbl-evencontent">'.Application_Helper_General::convertDate($resultdata['BENEFICIARY_CREATED'],'d-MMM-yyyy HH:mm:ss','yyyy-MM-dd HH:mm:ss').'</td>
				</tr>
				<tr>
				  <td class="tbl-evencontent">'.$this->language->_('Created By').'</td>
				  <td class="tbl-evencontent">:</td>
				  <td class="tbl-evencontent">'.$resultdata['BENEFICIARY_CREATEDBY'].'</td>
				</tr>
				<!-- tr>
				  <td class="tbl-evencontent">'.$this->language->_('Bank Status').'</td>
				  <td class="tbl-evencontent">:</td>
				  <td class="tbl-evencontent">'.$status.'</td>
				</tr -->
				<tr>
				  <td class="tbl-evencontent">'.$this->language->_('Approved Status').'</td>
				  <td class="tbl-evencontent">:</td>
				  <td class="tbl-evencontent">'.$this->language->_($approve).'</td>
				</tr>
				<tr>
				  <td class="tbl-evencontent">'.$this->language->_('Request to Delete').'</td>
				  <td class="tbl-evencontent">:</td>
				  <td class="tbl-evencontent">'.$this->language->_($delete).'</td>
				</tr>
				';

				$this->_helper->download->pdf(null,null,null,'Beneficiary Detail',$htmldata);
				Application_Helper_General::writeLog('RPBN','Download PDF Beneficiary Detail');
			}

      }
      else
      {
          $acct = 0;
      }
    }

    if(!$acct)
    {
      $error_remark = 'Account No does not exist';
      $this->_helper->getHelper('FlashMessenger')->addMessage('F');
      $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
      $this->_redirect($this->_backURL);
    }

    $this->view->acct   = $acct;
    $this->view->modulename  = $this->_request->getModuleName();

    //insert log
	Application_Helper_General::writeLog('RPBN','View Beneficiary Detail. Beneficiary Account : ( '.$acct.' ). Beneficiary Name : ( '.$resultdata['BENEFICIARY_NAME'].' )');

  }

}
