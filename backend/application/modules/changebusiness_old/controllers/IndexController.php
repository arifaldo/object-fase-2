<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class Changebusiness_IndexController extends Application_Main
{

	//	private $_suggestionStatusCP = array(	"" => "--Any Value--",
	//											"UR"=> "Unread Suggestion" ,
	//											"RS"=> "Read Suggestion",
	//											"RR"=> "Request Repaired",
	//											"RP"=> "Repaired Suggestion",
	//	);

	public function initController()
	{
		$this->_suggestionStatusCP = array(
			"" => '--' . $this->language->_('Any Value') . "--",
			"UR" => $this->language->_('Unread Suggestion'),
			"RS" => $this->language->_('Read Suggestion'),
			"RR" => $this->language->_('Request Repaired'),
			"RP" => $this->language->_('Repaired Suggestion'),
		);
	}

	/**
	 * The default action - show the home page
	 */
	//===============================================================================================================
	protected $_moduleDB  = 'WFA';

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$auth = Zend_Auth::getInstance()->getIdentity();
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if (count($temp) > 1) {
			if ($temp[0] == 'F' || $temp[0] == 'S') {
				if ($temp[0] == 'F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = '';
				unset($temp[0]);
				foreach ($temp as $value) {
					if (!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				// $this->view->report_msg = $msg;
			}
		}

		// $userId = $this->_custIdLogin;
		// var_dump($userId); die;

		$select = array();
		$displayDateFormat  			= $this->_dateDisplayFormat;
		$databaseSaveFormat 			= $this->_dateDBFormat;
		$this->view->dateJqueryFormat  	= $this->_dateJqueryFormat;

		$changesTypeCodeArr   = $this->_suggestType['code'];
		$changesStatusCodeArr = array_flip($this->_suggestionStatusCP);
		$changesStatusDescArr = $this->_suggestStatus['desc'];

		unset($this->_suggestType['code']['activate']);
		unset($this->_suggestType['desc']['activate']);
		unset($this->_suggestType['code']['deactivate']);
		unset($this->_suggestType['desc']['deactivate']);
		$this->view->suggestionType   =  $this->_suggestType;
		$this->view->suggestionStatus =  $this->_suggestionStatusCP;
		//companyCode
		$listId = $this->_db->select()
			->from(
				array('M_CUSTOMER'),
				array('CUST_ID', 'CUST_NAME')
			)
			->order('CUST_ID ASC')
			->query()->fetchAll();

		$this->view->listCustId 	= array_merge(array('' => '-- ' . $this->language->_('Any Value') . ' --'), Application_Helper_Array::listArray($listId, 'CUST_ID', 'CUST_ID'));
		$this->view->listCustName 	= Application_Helper_Array::listArray($listId, 'CUST_NAME', 'CUST_NAME');

		//$privi = Zend_Registry::get('privilege');	// get privi ID
		$privi = $this->_priviId;

		//get filtering param
		$filterArr = array(
			'filter'			=> array('StringTrim', 'StripTags'),
			'SUGGEST_DATA' 	  	=> array('StripTags'),
			'COMP_ID' 	  		=> array('StringTrim', 'StripTags'),
			'COMPANY_NAME' 	  	=> array('StripTags'),
			'DATA_ID' 			=> array('StringTrim', 'StripTags'),
			'DATA_NAME' 		=> array('StripTags'),
			'SUGGEST_TYPE' 	=> array('StringTrim', 'StripTags'),
			'SUGGEST_STATUS' 	=> array('StringTrim', 'StripTags'),
			'SUGGESTED_BY' 		=> array('StringTrim', 'StripTags'),
			'PS_SUGGESTED' => array('StringTrim', 'StripTags'),
			'STATUS_SPOBLIGEE' => array('StringTrim', 'StripTags'),
			'PS_SUGGESTED_END'	=> array('StringTrim', 'StripTags'),
		);

		$validator = array(
			'filter' 		=> array(),
			'SUGGEST_DATA'  	=> array(),
			'PS_NUMBER'  	=> array(),
			'COMP_ID'  		=> array(),
			'COMPANY_NAME'		=> array(),
			'DATA_ID'  			=> array(),
			'DATA_NAME'  			=> array(),
			'SUGGEST_TYPE'  			=> array(),
			'SUGGEST_STATUS'  			=> array(),
			'SUGGESTED_BY'  			=> array(),
			'STATUS_SPOBLIGEE'  			=> array(),
			'PS_UPDATED' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
			'PS_UPDATED_END' 		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
		);

		$dataParam = array('SUGGEST_DATA', 'COMP_ID', 'SUGGEST_TYPE', 'SUGGEST_STATUS', 'COMP_NAME', 'DATA_ID', 'DATA_NAME', 'SUGGESTED_BY', 'STATUS_SPOBLIGEE');
		$dataParamValue = array();

		if ($this->_request->getParam('wherecol')) {
			$clean2 = array_diff($this->_request->getParam('wherecol'), $dataParam);
			$dataParam = array_diff($this->_request->getParam('wherecol'), $clean2);
			// print_r($dataParam);die;

			// print_r($output);die;
			// print_r($this->_request->getParam('wherecol'));
			foreach ($dataParam as $no => $dtParam) {

				if (!empty($this->_request->getParam('wherecol'))) {
					$dataval = $this->_request->getParam('whereval');
					// print_r($dataval);
					$order = 0;
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if ($value == "PS_SUGGESTED") {
							$order--;
						}
						if ($dtParam == $value) {
							$dataParamValue[$dtParam] = $dataval[$order];
						}
						$order++;
					}
				}
			}
		}
		// print_r($dataParamValue);
		// die;
		// print_r($this->_request->getParam('whereval'));die;
		if (!empty($this->_request->getParam('sgdate'))) {
			$sgarr = $this->_request->getParam('sgdate');
			$dataParamValue['PS_SUGGESTED'] = $sgarr[0];
			$dataParamValue['PS_SUGGESTED_END'] = $sgarr[1];
		}

		$zf_filter = new Zend_Filter_Input($filterArr, $validator, $dataParamValue);
		$filter 		= $this->_getParam('filter');

		$this->setbackURL();

		$fields = array(
			'tableName'    => array(
				'field' => 'DISPLAY_TABLENAME',
				'label' => $this->language->_('Suggest Data'),
				'sortable' => true
			),
			'companyCode'  => array(
				'field' => 'COMPANY_CODE',
				'label' => $this->language->_('Company'),
				'sortable' => true
			),
			'dataId'  => array(
				'field' => 'KEY_FIELD',
				'label' => $this->language->_('Data ID'),
				'sortable' => true
			),
			'dataName'  => array(
				'field' => 'KEY_VALUE',
				'label' => $this->language->_('Data Name'),
				'sortable' => true
			),
			'suggestionDate' => array(
				'field' => 'G.CREATED',
				'label' => $this->language->_('Suggestion Date'),
				'sortable' => true
			),
			'suggestionBy' => array(
				'field' => 'CHANGES_FLAG',
				'label' => $this->language->_('Suggested By'),
				'sortable' => true
			),
			'suggestionType'  => array(
				'field' => 'CHANGES_TYPE',
				'label' => $this->language->_('Suggestion Type'),
				'sortable' => true
			),
			'suggestionInfo' => array(
				'field' => 'suggestion_status',
				'label' => $this->language->_('Suggestion Status'),
				'sortable' => true
			),
			'status' => array(
				'field' => 'STATUS_SPOBLIGEE',
				'label' => $this->language->_('Approval Stage'),
				'sortable' => true
			),
		);

		$filterlist = array('PS_SUGGESTED', 'SUGGEST_DATA', 'COMP_ID', 'SUGGEST_TYPE', 'SUGGEST_STATUS', 'COMP_NAME', 'DATA_ID', 'DATA_NAME', 'SUGGESTED_BY', 'STATUS_SPOBLIGEE');

		$this->view->filterlist = $filterlist;

		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby', 'suggestionDate');
		$sortDir = $this->_getParam('sortdir', 'desc');

		//validate parameters before passing to view and query
		$page   = (Zend_Validate::is($page, 'Digits') && ($page > 0)) ? $page : 1;
		$sortBy = (Zend_Validate::is(
			$sortBy,
			'InArray',
			array(array_keys($fields))
		)) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		//Zend_Debug::dump($sortBy);die;
		$sortDir = (Zend_Validate::is(
			$sortDir,
			'InArray',
			array('haystack' => array('asc', 'desc'))
		)) ? $sortDir : 'asc';
		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;

		if ($filter == TRUE) {
			$suggestdata 	= html_entity_decode($zf_filter->getEscaped('SUGGEST_DATA'));
			$cust_id 		= html_entity_decode($zf_filter->getEscaped('COMP_ID'));
			$cust_name 		= html_entity_decode($zf_filter->getEscaped('COMP_NAME'));
			$data_id 		= html_entity_decode($zf_filter->getEscaped('DATA_ID'));
			$data_name 		= html_entity_decode($zf_filter->getEscaped('DATA_NAME'));
			$suggestor		= html_entity_decode($zf_filter->getEscaped('SUGGESTED_BY'));
			$status		= html_entity_decode($zf_filter->getEscaped('STATUS_SPOBLIGEE'));

			// print_r($cust_name);
			//       	die('here');
			$changesCreatedFrom = (Zend_Date::isDate($dataParamValue['PS_SUGGESTED'], $displayDateFormat)) ?
				new Zend_Date($dataParamValue['PS_SUGGESTED'], $displayDateFormat) :
				false;
			$changesCreatedTo 	= (Zend_Date::isDate($dataParamValue['PS_SUGGESTED_END'], $displayDateFormat)) ?
				new Zend_Date($dataParamValue['PS_SUGGESTED_END'], $displayDateFormat) :
				false;
			$suggestionType 	= (Zend_Validate::is($zf_filter->getEscaped('SUGGEST_TYPE'), 'InArray', array('haystack' => $changesTypeCodeArr))) ?
				$zf_filter->getEscaped('SUGGEST_TYPE') :
				false;
			$suggestionStatus 	= (Zend_Validate::is($zf_filter->getEscaped('SUGGEST_STATUS'), 'InArray', array('haystack' => $changesStatusCodeArr))) ?
				$zf_filter->getEscaped('SUGGEST_STATUS') :
				false;

			$this->view->suggest_data 	= $suggestdata;
			$this->view->cust_id 		= $cust_id;
			$this->view->cust_name 		= $cust_name;
			$this->view->data_id 		= $data_id;
			$this->view->data_name 		= $data_name;
			$this->view->suggestor 		= $suggestor;
			$this->view->status 		= $status;



			if ($changesCreatedFrom) {

				$this->view->changesCreatedFrom = $changesCreatedFrom->toString($displayDateFormat);
			}
			if ($changesCreatedTo)
				$this->view->changesCreatedTo   = $changesCreatedTo->toString($displayDateFormat);
			if ($suggestionType)
				$this->view->suggestionTypeValue = $suggestionType;
			if ($suggestionStatus)
				$this->view->suggestionStatusValue = $suggestionStatus;
		}


		if (!empty($privi))		// kalo privilege is empty, gak bisa generate report, for all data
		{
			$changeModulePrivilegeObj  	= new Changebusiness_Model_Privilege();
			$listAutModuleArr 			= $changeModulePrivilegeObj->getAuthorizeModule();
			$whPriviID 					= "'" . implode("','", $listAutModuleArr) . "'";
			//echo '<pre>';print_r($whPriviID);die;
			$caseWhenRequestChangeList = " CASE WHEN (G.CHANGES_STATUS = 'RR') THEN 'Request Repaired'
				 WHEN (READ_STATUS = 0 AND CHANGES_STATUS = 'WA') THEN 'Unread Suggestion'
				 WHEN (READ_STATUS = 1 AND CHANGES_STATUS = 'WA') THEN 'Read Suggestion'
				 WHEN (READ_STATUS = 2 AND CHANGES_STATUS = 'WA') THEN 'Repaired Suggestion'
				 END
				";

			//compose query (use filtering and sorting)
			// $select = $this->_db->select()
			// 	->from(array('G' => 'T_GLOBAL_CHANGES'),
			// 	array( '*',
			// 	'suggestion_status'		=> new Zend_Db_Expr($caseWhenRequestChangeList)))
			// 	// ->where('CHANGES_FLAG = '.$this->_db->quote('B')) // B : Backend / F : Frontend. ############# REMOVE KARENA ADA CREATE USER DARI FO !??!###############
			// 	->where("G.MODULE IN (".$whPriviID.")");

			$selectbo = $this->_db->select()
				->from(
					array('G' => 'T_GLOBAL_CHANGES'),
					array(
						'G.*',
						'suggestion_status'		=> new Zend_Db_Expr($caseWhenRequestChangeList),
						'USER_ID' => 'G.CREATED_BY',
						'CUST_CREATEDBY' => 'G.CREATED_BY'
					)
				)
				->joinleft(array('B' => 'M_BUSER'), 'G.CREATED_BY = B.BUSER_ID', array('B.BUSER_BRANCH'))
				->joinleft(array('C' => 'M_BRANCH'), 'C.ID = B.BUSER_BRANCH', array('C.BRANCH_NAME', 'C.STATUS'))
				->joinleft(array('BG' => 'M_BGROUP'), 'BG.BGROUP_ID = B.BGROUP_ID', array('BG.BGROUP_CALL'))
				->joinleft(array('D' => 'TEMP_CUST_SPOBLIGEE'), 'D.CHANGES_ID = G.CHANGES_ID', array('SPOBLIGEE_FLAG' => 'D.FLAG'))
				->joinleft(array('LF' => 'TEMP_CUST_LINEFACILITY'), 'LF.CHANGES_ID = G.CHANGES_ID', array('LINEFACILITY_FLAG' => 'LF.FLAG'))
				->joinleft(array('IB' => 'TEMP_INS_BRANCH'), 'IB.CHANGES_ID = G.CHANGES_ID', array('INSBRANCH_FLAG' => 'IB.FLAG'))
				->joinleft(array('MD' => 'TEMP_MARGINALDEPOSIT'), 'MD.CHANGES_ID = G.CHANGES_ID', array('MD_FLAG' => 'MD.FLAG'))
				->where('CHANGES_FLAG = ' . $this->_db->quote('B'))
				->where("G.MODULE IN ('insurancebranch','linefacility','specialobligee','marginaldeposit','requestrebate')");
			//->where('C.ID="' . $auth->userBranchId . '"');

			$selectfo = $this->_db->select()
				->distinct()
				->from(
					array('G' => 'T_GLOBAL_CHANGES'),
					array(
						'G.*',
						'suggestion_status'		=> new Zend_Db_Expr($caseWhenRequestChangeList)
					)
				)
				->joinleft(array('U' => 'M_USER'), 'G.CREATED_BY = U.USER_ID', array('U.CUST_ID'))
				->joinleft(array('K' => 'M_CUSTOMER'), 'K.CUST_ID = U.CUST_ID', array('K.CUST_CREATEDBY'))
				->joinleft(array('B' => 'M_BUSER'), 'B.BUSER_ID = K.CUST_CREATEDBY', array('B.BUSER_BRANCH'))
				->joinleft(array('C' => 'M_BRANCH'), 'C.ID = B.BUSER_BRANCH', array('C.BRANCH_NAME', 'C.STATUS'))
				->joinleft(array('BG' => 'M_BGROUP'), 'BG.BGROUP_ID = B.BGROUP_ID', array('BG.BGROUP_CALL'))
				->joinleft(array('D' => 'TEMP_CUST_SPOBLIGEE'), 'D.CHANGES_ID = G.CHANGES_ID', array('SPOBLIGEE_FLAG' => 'D.FLAG'))
				->joinleft(array('LF' => 'TEMP_CUST_LINEFACILITY'), 'LF.CHANGES_ID = G.CHANGES_ID', array('LINEFACILITY_FLAG' => 'LF.FLAG'))
				->joinleft(array('IB' => 'TEMP_INS_BRANCH'), 'IB.CHANGES_ID = G.CHANGES_ID', array('INSBRANCH_FLAG' => 'IB.FLAG'))
				->joinleft(array('MD' => 'TEMP_MARGINALDEPOSIT'), 'MD.CHANGES_ID = G.CHANGES_ID', array('MD_FLAG' => 'MD.FLAG'))
				//->where('CHANGES_FLAG = ' . $this->_db->quote('F'))
				->where("G.MODULE IN ('insurancebranch','linefacility','specialobligee','marginaldeposit','requestrebate')");
			//->where('C.ID="' . $auth->userBranchId . '"');

			if ($auth->userHeadQuarter == "NO") {
				$selectfo->where('C.ID = ?', $auth->userBranchId); // Add Bahri
				$selectbo->where('C.ID = ?', $auth->userBranchId); // Add Bahri
			}





			//	        				->where('UPPER(CREATED_BY) <> '.$this->_db->quote(strtoupper($this->_userIdLogin)))
			//->order('CREATED DESC');
			//					}	// if (!empty($priviMod))

		}



		if ($filter == TRUE) {
			//print_r($changesCreatedFrom);die;
			//where clauses
			if ($suggestdata && $suggestdata != 'all') {
				$selectbo->where("UPPER(DISPLAY_TABLENAME) LIKE " . $this->_db->quote('%' . $suggestdata . '%'));
				$selectfo->where("UPPER(DISPLAY_TABLENAME) LIKE " . $this->_db->quote('%' . $suggestdata . '%'));
			}

			if ($cust_id) {
				$selectbo->where("UPPER(COMPANY_CODE) LIKE " . $this->_db->quote('%' . $cust_id . '%'));
				$selectfo->where("UPPER(COMPANY_CODE) LIKE " . $this->_db->quote('%' . $cust_id . '%'));
			}

			if ($cust_name) {
				$selectbo->where("UPPER(COMPANY_NAME) LIKE " . $this->_db->quote('%' . $cust_name . '%'));
				$selectfo->where("UPPER(COMPANY_NAME) LIKE " . $this->_db->quote('%' . $cust_name . '%'));
			}

			if ($data_id) {
				$selectbo->where("UPPER(KEY_FIELD) LIKE " . $this->_db->quote('%' . $data_id . '%'));
				$selectfo->where("UPPER(KEY_FIELD) LIKE " . $this->_db->quote('%' . $data_id . '%'));
			}

			if ($data_name) {
				$selectbo->where("UPPER(KEY_VALUE) LIKE " . $this->_db->quote('%' . $data_name . '%'));
				$selectfo->where("UPPER(KEY_VALUE) LIKE " . $this->_db->quote('%' . $data_name . '%'));
			}

			if ($suggestor) {
				$selectbo->where("UPPER(CREATED_BY) LIKE " . $this->_db->quote('%' . $suggestor . '%'));
				$selectfo->where("UPPER(CREATED_BY) LIKE " . $this->_db->quote('%' . $suggestor . '%'));
			}

			if ($status) {
				$selectbo->where("UPPER(STATUS_SPOBLIGEE) LIKE " . $this->_db->quote('%' . $status . '%'));
				$selectfo->where("UPPER(STATUS_SPOBLIGEE) LIKE " . $this->_db->quote('%' . $status . '%'));
			}

			if ($changesCreatedFrom) {
				$selectbo->where("DATE(CREATED) >= DATE(" . $this->_db->quote($changesCreatedFrom->toString($databaseSaveFormat)) . ")");
				$selectfo->where("DATE(CREATED) >= DATE(" . $this->_db->quote($changesCreatedFrom->toString($databaseSaveFormat)) . ")");
			}

			if ($changesCreatedTo) {
				$selectbo->where("DATE(CREATED) <= DATE(" . $this->_db->quote($changesCreatedTo->toString($databaseSaveFormat)) . ")");
				$selectfo->where("DATE(CREATED) <= DATE(" . $this->_db->quote($changesCreatedTo->toString($databaseSaveFormat)) . ")");
			}

			if ($suggestionType) {
				$selectbo->where("CHANGES_TYPE = " . $this->_db->quote($suggestionType));
				$selectfo->where("CHANGES_TYPE = " . $this->_db->quote($suggestionType));
			}

			if ($suggestionStatus) {
				if ($suggestionStatus == "RR") {
					$suggestionStatus = 'Request Repaired';
					$selectbo->where("CHANGES_STATUS = " . $this->_db->quote('RR'));
					$selectfo->where("CHANGES_STATUS = " . $this->_db->quote('RR'));
				} else {
					if ($suggestionStatus == "UR") {
						$selectbo->where('READ_STATUS = 0 AND CHANGES_STATUS = ' . $this->_db->quote('WA'));
						$selectfo->where('READ_STATUS = 0 AND CHANGES_STATUS = ' . $this->_db->quote('WA'));
					} else if ($suggestionStatus == "RS") {
						$selectbo->where('READ_STATUS = 1 AND CHANGES_STATUS = ' . $this->_db->quote('WA'));
						$selectfo->where('READ_STATUS = 1 AND CHANGES_STATUS = ' . $this->_db->quote('WA'));
					} else if ($suggestionStatus == "RP") {
						$selectbo->where('READ_STATUS = 2 AND CHANGES_STATUS = ' . $this->_db->quote('WA'));
						$selectfo->where('READ_STATUS = 2 AND CHANGES_STATUS = ' . $this->_db->quote('WA'));
					}
				}
			} else {
				$selectbo->where('(CHANGES_STATUS = ' . $this->_db->quote('WA') . ' OR CHANGES_STATUS = ' . $this->_db->quote('RR') . ')');
				$selectfo->where('(CHANGES_STATUS = ' . $this->_db->quote('WA') . ' OR CHANGES_STATUS = ' . $this->_db->quote('RR') . ')');
			}
		} else {
			// var_dump($selectbo->query()->fetchAll());
			// die();
			$selectbo->where('(CHANGES_STATUS = ' . $this->_db->quote('WA') . ' OR CHANGES_STATUS = ' . $this->_db->quote('RR') . ')');
			$selectfo->where('(CHANGES_STATUS = ' . $this->_db->quote('WA') . ' OR CHANGES_STATUS = ' . $this->_db->quote('RR') . ')');
		}
		//    $select->where('(CHANGES_FLAG = '.$this->_db->quote('B').')');
		$selectbo->group('CHANGES_ID');
		$selectfo->group('CHANGES_ID');
		$select = $this->_db->select()->distinct()->union(array($selectbo, $selectfo));
		// echo "<pre>";
		//$select->group('CHANGES_ID');
		//$select->order('CREATED DESC');
		//echo $select;
		$selectfix = $this->_db->select()
			->distinct()
			->from($select)->group('CHANGES_ID')->order('CREATED DESC');
		//echo '<pre>';
		// echo $selectfix;die;
		$result = $selectfix->query()->fetchall();


		// 	        print_r($select->query());die;
		//echo $select;die();

		// 			Zend_Debug::dump($select->__toString());die;



		$this->view->fields 		= $fields;
		$this->view->paginatorParam = $result;

		$statusArr = [
			'1'	=> 'Waiting Review',
			'2'	=> 'Waiting Approve',
			''	=> 'Waiting Approve'
		];
		$this->view->statusArr = $statusArr;

		//process posted data
		if ($this->_request->isPost()) {
			$filterArr = array(
				'change_id' => 'Digits',
				'change_act' => 'Alpha',
				'change_note' => 'StringTrim',
				'action' => 'StringTrim'
			);

			$save_request = $this->_request->getPost();

			// decrypt -------------------------------------------------------
			$setting = new Settings();
			$enc_pass = $setting->getSetting('enc_pass');
			$enc_salt = $setting->getSetting('enc_salt');
			$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
			$pw_hash = md5($enc_salt . $enc_pass);
			$rand = $this->_userIdLogin . date('dHis') . $pw_hash;
			$sessionNamespace->token 	= $rand;
			$this->view->token = $sessionNamespace->token;

			$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
			$password = $sessionNamespace->token;
			$this->view->token = $sessionNamespace->token;

			foreach ($save_request["change_id"] as $key => $value) {
				$AESMYSQL = new Crypt_AESMYSQL();

				$BG_NUMBER 	= urldecode($save_request["change_id"][$key]);

				$BG_NUMBER = $AESMYSQL->decrypt($BG_NUMBER, $password);

				$save_request["change_id"][$key] = $BG_NUMBER;
			}

			// ------------------------------------------------------------------------

			$zf_filter = new Zend_Filter_Input($filterArr, array(), $save_request);

			$updated_id 	= array();
			$failed_id 		= array();
			$updated_status = array();
			$failed_status 	= array();
			$failed_message = array();
			$success_remark = array();
			$clauseChanges 	= "";

			if (is_array($zf_filter->change_id)) {
				$content['changesId'] = $payment;

				$rand = null;
				for ($i = 0; $i < 4; $i++) {
					$rand =  $rand . rand(0, 9);
				}

				$sessionNamespace = new Zend_Session_Namespace('confirmRequestChangeList');
				$sessionNamespace->content 	= $zf_filter->change_id;
				$sessionNamespace->action 	= $zf_filter->action;
				$sessionNamespace->note 	= $zf_filter->change_note;
				$sessionNamespace->token 	= $rand;
				// 					print_r($zf_filter);die;
				$this->_redirect('/changemanagement/index/confirm');
				/*
						$note = array();
						for($i=0;$i<count($zf_filter->change_id);$i++)
						{
								if(Zend_Validate::is($zf_filter->change_id[$i],'Db_RecordExists',(array('table'=>'T_GLOBAL_CHANGES','field'=>'CHANGES_ID'))))
								{
									$gcmodel = new Changebusiness_Model_Globalchanges($zf_filter->change_id[$i]);
									$changesInfo  = $gcmodel->getChangesInfo();
									if($changesInfo['CHANGES_STATUS'] =='WA' ){
										if($zf_filter->action =='Approve'){
											$updated = $gcmodel->approve($this->_userIdLogin,$zf_filter->change_note);
											$message = 'Suggesth on Granted';
										}elseif($zf_filter->action =='Reject'){
											$updated = $gcmodel->reject($this->_userIdLogin,$zf_filter->change_note);
											$message = 'Suggestion Rejected';
										}elseif($zf_filter->action =='Request Repair'){
											$updated = $gcmodel->requestRepair($this->_userIdLogin,$zf_filter->change_note);
											$message = 'Suggestion Repair Requested';
										//else update status not recognized/no change,
										}else{
											continue;
										}

										if($updated) {
											$updated_id[$i] = $gcmodel->getSuggestData();
											$pos = array_search($zf_filter->change_act[$i],$changesStatusCodeArr);
											$updated_status[$i] = $message;
											$this->view->msgSuccess = $message;
											if($gcmodel->getErrorCode())
												$success_remark[$i] = $message;
												Application_Helper_General::writeLog('','Updating suggestion id ' .$zf_filter->change_id[$i].$gcmodel->getErrorMessage());

										}
										else
										{
											//utk keperluan error server token jika terjadi error / server mati

												$error = null;
												$note[$zf_filter->change_id[$i]] = $zf_filter->change_note;
												$failed_id[$i] = $gcmodel->getSuggestData();
												$pos = array_search($zf_filter->change_act[$i],$changesStatusCodeArr);
												$error .= ' : '.$gcmodel->getErrorMessage();
												$failed_message[$i] = $gcmodel->getErrorMessage();;

												Application_Helper_General::writeLog('','Failed Updating suggestion id ' .$zf_filter->change_id[$i].$gcmodel->getErrorMessage());
											//}

										}
									}else{
										$failed_id[$i] = $gcmodel->getSuggestData();
										$pos = array_search($zf_filter->change_act[$i],$changesStatusCodeArr);
										$error .= ' : '.$gcmodel->getErrorMessage();
										$failed_message[$i] = 'Changes could not be processed due to current status are prohibited';

										Application_Helper_General::writeLog('','Failed Updating suggestion id ' .$zf_filter->change_id[$i].$gcmodel->getErrorMessage());
										$this->view->error = true;
									}
							}
						}
						$this->view->note = $note;
					*/
			} else {
				$this->view->error = true;
				$this->view->report_msg = 'Please Checked Selection';
			}
			$this->view->updatedId = $updated_id;
			$this->view->updatedStatus = $updated_status;
			$this->view->failedId = $failed_id;
			$this->view->failedStatus = $failed_status;
			$this->view->failedMessage = $failed_message;
			$this->view->successRemark = $success_remark;

			$this->view->updated = true;
		} else {
		}

		$setting = new Settings();
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$pw_hash = md5($enc_salt . $enc_pass);
		$rand = $this->_userIdLogin . date('dHis') . $pw_hash;
		$sessionNamespace->token   = $rand;
		$this->view->token = $sessionNamespace->token;

		foreach ($result as $key => $value) {

			$AESMYSQL = new Crypt_AESMYSQL();
			$rand = $this->token;

			$encrypted_payreff = $AESMYSQL->encrypt($get_changes_id, $rand);
			$encpayreff = urlencode($encrypted_payreff);

			$result[$key]["CHANGES_ID_ENCRYPTED"] = $encpayreff;

			$get_changes_id = $value["CHANGES_ID"];

			$cekCash = $this->_db->select()
				->from("TEMP_CHARGES_OTHER")
				->where("CHANGES_ID = ?", $get_changes_id)
				->query()->fetchAll();

			if (count($cekCash) > 0) {
				$result[$key]["FLAG_CASH"] = $cekCash[0]["FLAG"];
				continue;
			} else {
				$result[$key]["FLAG_CASH"] = "kosong";
			}

			$cekRebate = $this->_db->select()
				->from("TEMP_REQUEST_REBATE")
				->where("CHANGES_ID = ?", $get_changes_id)
				->query()->fetchAll();

			if (count($cekRebate) > 0) {
				$result[$key]["FLAG_CASH"] = $cekRebate[0]["FLAG"];
			} else {
				$result[$key]["FLAG_CASH"] = "kosong";
			}
		}


		$this->paging($result);

		$URL = $this->view->url(array(
			'module'	 => $this->view->modulename,
			'controller' => $this->view->controllername,
			'action'	 => 'index',
			'page'		 => $page,
			'sortBy' 	 => $this->view->sortBy,
			'sortDir' 	 => $this->view->sortDir
		), null, true) . $this->view->qstring;

		$sessionNamespace = new Zend_Session_Namespace('URL_CHG_CONFIRM');
		$sessionNamespace->URL = $URL;

		if (!empty($dataParamValue)) {
			$this->view->sgdateStart = $dataParamValue['PS_SUGGESTED'];
			$this->view->sgdateEnd = $dataParamValue['PS_SUGGESTED_END'];

			unset($dataParamValue['PS_SUGGESTED_END']);
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
			//var_dump($wherecol);die('here');

			// print_r($whereval);die;
		} else {
			$wherecol = array();
			$whereval = array();
		}

		$this->view->wherecol     = $wherecol;
		$this->view->whereval     = $whereval;
	}

	public function confirmAction()
	{

		$this->_helper->layout()->setLayout('newlayout');
		$sessionNamespace 	= new Zend_Session_Namespace('confirmRequestChangeList');
		$dataChangesId 		= $sessionNamespace->content;
		$action 			= $sessionNamespace->action;
		$note 				= $sessionNamespace->note;
		$token 				= $sessionNamespace->token;

		$this->view->action = $action;
		$this->view->note 	= $action;
		$this->view->token 	= $token;

		$sessionNamespace = new Zend_Session_Namespace('URL_CHG_CONFIRM');
		$this->view->backURL = (!empty($sessionNamespace->URL)) ?
			$sessionNamespace->URL : '/' . $this->view->modulename . '/' . $this->_controllerList . '/index';

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if (count($temp) > 1) {
			if ($temp[0] == 'F' || $temp[0] == 'S') {
				if ($temp[0] == 'F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = '';
				unset($temp[0]);
				foreach ($temp as $value) {
					if (!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}
		}

		$select = array();
		$displayDateFormat  = $this->_dateDisplayFormat;
		$databaseSaveFormat = $this->_dateDBFormat;
		$this->view->dateJqueryFormat  = $this->_dateJqueryFormat;

		$changesTypeCodeArr   = $this->_suggestType['code'];
		$changesStatusCodeArr = array_flip($this->_suggestionStatusCP);
		$changesStatusDescArr = $this->_suggestStatus['desc'];

		unset($this->_suggestType['code']['activate']);
		unset($this->_suggestType['desc']['activate']);
		unset($this->_suggestType['code']['deactivate']);
		unset($this->_suggestType['desc']['deactivate']);
		$this->view->suggestionType   =  $this->_suggestType;
		$this->view->suggestionStatus =  $this->_suggestionStatusCP;
		//companyCode
		$listId = $this->_db->select()
			->from(
				array('M_CUSTOMER'),
				array('CUST_ID', 'CUST_NAME')
			)
			->order('CUST_ID ASC')
			->query()->fetchAll();

		$this->view->listCustId 	= array_merge(array('' => '-- ' . $this->language->_('Any Value') . ' --'), Application_Helper_Array::listArray($listId, 'CUST_ID', 'CUST_ID'));
		$this->view->listCustName 	= Application_Helper_Array::listArray($listId, 'CUST_NAME', 'CUST_NAME');

		//$privi = Zend_Registry::get('privilege');	// get privi ID
		$privi 						= $this->_priviId;

		//get filtering param
		$filterArr = array(
			'filter'			=> array('StringTrim', 'StripTags'),
			'suggest_data' 	  	=> array('StripTags'),
			'cust_id' 	  		=> array('StringTrim', 'StripTags'),
			'cust_name' 	  	=> array('StripTags'),
			'data_id' 			=> array('StringTrim', 'StripTags'),
			'data_name' 		=> array('StripTags'),
			'suggestionType' 	=> array('StringTrim', 'StripTags'),
			'suggestionStatus' 	=> array('StringTrim', 'StripTags'),
			'suggestor' 		=> array('StringTrim', 'StripTags'),
			'changesCreatedFrom' => array('StringTrim', 'StripTags'),
			'changesCreatedTo'	=> array('StringTrim', 'StripTags'),
		);

		$zf_filter = new Zend_Filter_Input($filterArr, array(), $this->_request->getParams());
		$filter = $zf_filter->getEscaped('filter');

		$fields = array(
			'tableName'    => array(
				'field' => 'DISPLAY_TABLENAME',
				'label' => 'Suggest Data',
				'sortable' => false
			),
			'companyCode'  => array(
				'field' => 'COMPANY_CODE',
				'label' => 'Company Code',
				'sortable' => false
			),
			'companyName'  => array(
				'field' => 'COMPANY_NAME',
				'label' => 'Company Name',
				'sortable' => false
			),
			'dataId'  => array(
				'field' => 'KEY_FIELD',
				'label' => 'Data ID',
				'sortable' => false
			),
			'dataName'  => array(
				'field' => 'KEY_VALUE',
				'label' => 'Data Name',
				'sortable' => false
			),
			'suggestionDate' => array(
				'field' => 'G.CREATED',
				'label' => 'Suggested Date',
				'sortable' => false
			),
			'suggestionBy' => array(
				'field' => 'CREATED_BY',
				'label' => 'Suggested By',
				'sortable' => false
			),
			'suggestionType'  => array(
				'field' => 'CHANGES_TYPE',
				'label' => 'Suggestion Type',
				'sortable' => false
			),
			'suggestionInfo' => array(
				'field' => 'suggestion_status',
				'label' => 'Suggestion Status',
				'sortable' => false
			),
		);

		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby', 'suggestionDate');
		$sortDir = $this->_getParam('sortdir', 'desc');

		//validate parameters before passing to view and query
		$page   = (Zend_Validate::is($page, 'Digits') && ($page > 0)) ? $page : 1;
		$sortBy = (Zend_Validate::is(
			$sortBy,
			'InArray',
			array(array_keys($fields))
		)) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		//Zend_Debug::dump($sortBy);die;
		$sortDir = (Zend_Validate::is(
			$sortDir,
			'InArray',
			array('haystack' => array('asc', 'desc'))
		)) ? $sortDir : 'asc';
		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;

		if (!empty($privi))		// kalo privilege is empty, gak bisa generate report, for all data
		{
			$changeModulePrivilegeObj  = new Changebusiness_Model_Privilege();
			$listAutModuleArr = $changeModulePrivilegeObj->getAuthorizeModule();

			$whPriviID = "'" . implode("','", $listAutModuleArr) . "'";

			$caseWhenRequestChangeList = " CASE WHEN (G.CHANGES_STATUS = 'RR') THEN 'Request Repaired'
											 WHEN (READ_STATUS = 0 AND CHANGES_STATUS = 'WA') THEN 'Unread Suggestion'
											 WHEN (READ_STATUS = 1 AND CHANGES_STATUS = 'WA') THEN 'Read Suggestion'
											 WHEN (READ_STATUS = 2 AND CHANGES_STATUS = 'WA') THEN 'Repaired Suggestion'
											 END
											";

			//compose query (use filtering and sorting)
			$select = $this->_db->select()
				->from(
					array('G' => 'T_GLOBAL_CHANGES'),
					array(
						'*',
						'suggestion_status'		=> new Zend_Db_Expr($caseWhenRequestChangeList)
					)
				)
				// ->where('CHANGES_FLAG = '.$this->_db->quote('B'))
				->where("G.MODULE IN (" . $whPriviID . ")");
		}

		//where clauses
		$changesId = "'" . implode("','", $dataChangesId) . "'";
		$select->where("G.CHANGES_ID IN (" . $changesId . ")");
		//order clause

		$select->order(array($sortBy . ' ' . $sortDir));
		$dataSelectedChanges 		= $this->_db->fetchAll($select);

		// encrypt -------------------------------------------------------------
		$setting = new Settings();
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$pw_hash = md5($enc_salt . $enc_pass);
		$rand = $this->_userIdLogin . date('dHis') . $pw_hash;
		$sessionNamespace->token   = $rand;
		$this->view->token = $sessionNamespace->token;

		foreach ($dataSelectedChanges as $key => $value) {
			$get_changes_id = $value["CHANGES_ID"];

			$AESMYSQL = new Crypt_AESMYSQL();
			$rand = $this->token;

			$encrypted_payreff = $AESMYSQL->encrypt($get_changes_id, $rand);
			$encpayreff = urlencode($encrypted_payreff);

			$dataSelectedChanges[$key]["CHANGES_ID_ENCRYPTED"] = $encpayreff;
		}

		// ---------------------------------------------------------------------------

		$this->view->paginator 		= $dataSelectedChanges;
		$arrayChangesError 			= array();
		$this->view->fields 		= $fields;

		$select = $select->query()->fetchAll();

		$this->view->paginatorParam = $select;

		//process posted data
		if ($this->_request->isPost()) {
			// die('aprove');
			$filterArr = array(
				'change_id' => 'Digits',
				'change_act' => 'Alpha',
				'change_note' => 'StringTrim',
				'action' => 'StringTrim',
				'token' => 'StringTrim'
			);

			$save_request = $this->_request->getPost();

			// decrypt -------------------------------------------------------
			$setting = new Settings();
			$enc_pass = $setting->getSetting('enc_pass');
			$enc_salt = $setting->getSetting('enc_salt');
			$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
			$pw_hash = md5($enc_salt . $enc_pass);
			$rand = $this->_userIdLogin . date('dHis') . $pw_hash;
			$sessionNamespace->token 	= $rand;
			$this->view->token = $sessionNamespace->token;

			$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
			$password = $sessionNamespace->token;
			$this->view->token = $sessionNamespace->token;

			foreach ($save_request["change_id"] as $key => $value) {
				$AESMYSQL = new Crypt_AESMYSQL();

				$BG_NUMBER 	= urldecode($save_request["change_id"][$key]);

				$BG_NUMBER = $AESMYSQL->decrypt($BG_NUMBER, $password);

				$save_request["change_id"][$key] = $BG_NUMBER;
			}

			// ------------------------------------------------------------------------


			$zf_filter 		= new Zend_Filter_Input($filterArr, array(), $save_request);
			$updated_id 	= array();
			$failed_id 		= array();
			$updated_status = array();
			$failed_status 	= array();
			$failed_message = array();
			$success_remark = array();

			$clauseChanges = "";


			if (is_array($dataChangesId)) {
				// die('hahahha');
				for ($i = 0; $i < count($dataChangesId); $i++) {
					if (Zend_Validate::is($dataChangesId[$i], 'Db_RecordExists', (array('table' => 'T_GLOBAL_CHANGES', 'field' => 'CHANGES_ID')))) {

						$gcmodel = new Changebusiness_Model_Globalchanges($dataChangesId[$i]);

						$changesInfo  = $gcmodel->getChangesInfo();
						// 							print_r('here');
						// 							print_r($note);die;
						if ($changesInfo['CHANGES_STATUS'] == 'WA') {

							if ($zf_filter->action == $this->language->_('Approve')) {
								$updated = $gcmodel->approve($this->_userIdLogin, $note);
								$message = $this->language->_('Suggestion Granted');
							} elseif ($zf_filter->action == $this->language->_('Reject')) {
								$updated = $gcmodel->reject($this->_userIdLogin, $note);
								$message = $this->language->_('Suggestion Rejected');
							} elseif ($zf_filter->action == $this->language->_('Request Repair')) {
								$updated = $gcmodel->requestRepair($this->_userIdLogin, $note);
								$message = $this->language->_('Suggestion Repair Requested');
								//else update status not recognized/no change,
							} else {
								continue;
							}

							if ($updated) {
								$updated_id[$i] = $gcmodel->getSuggestData();
								$pos = array_search($zf_filter->change_act[$i], $changesStatusCodeArr);
								$updated_status[$i] = $message;
								$this->view->msgSuccess = $message;
								$privilgeDualControl = $changeModulePrivilegeObj->getPrivilege($changesInfo['MODULE'], $zf_filter->action);
								Application_Helper_General::writeLog($privilgeDualControl, 'Updating suggestion id ' . $dataChangesId[$i] . $gcmodel->getErrorMessage());
								$arrayChangesError[$changesInfo['CHANGES_ID']] = 'Succes';
							} else {
								$error = null;
								$failed_id[$i] = $gcmodel->getSuggestData();
								$error .= ' : ' . $gcmodel->getErrorMessage();
								$failed_message[$i] = $gcmodel->getErrorMessage();
								$privilgeDualControl = $changeModulePrivilegeObj->getPrivilege($changesInfo['MODULE'], $zf_filter->action);
								Application_Helper_General::writeLog($privilgeDualControl, 'Failed Updating suggestion id ' . $dataChangesId[$i] . $gcmodel->getErrorMessage());
								$arrayChangesError[$changesInfo['CHANGES_ID']] = $gcmodel->getErrorMessage();
							}
						} else {
							$failed_id[$i] = $gcmodel->getSuggestData();
							$error .= ' : ' . $gcmodel->getErrorMessage();
							$failed_message[$i] = 'Changes could not be processed due to current status are prohibited';
							$privilgeDualControl = $changeModulePrivilegeObj->getPrivilege($changesInfo['MODULE'], $zf_filter->action);
							Application_Helper_General::writeLog($privilgeDualControl, 'Failed Updating suggestion id ' . $dataChangesId[$i] . $gcmodel->getErrorMessage());
							$arrayChangesError[$changesInfo['CHANGES_ID']] = 'Changes could not be processed due to current status are prohibited';
							$this->view->error = true;
						}
					}
				}
				$this->view->note = $note;

				$conten 	= $zf_filter->change_id;
				$error 		= $zf_filter->change_id;
				$action 	= $zf_filter->action;
				$note 		= $zf_filter->change_note;
				if ($zf_filter->token == $token) {
					$tokenerr 	= null;
				} else {
					$tokenerr = 1;
				}
			}

			$sessionNamespace 				= new Zend_Session_Namespace('notificationRequestChangeList');
			$sessionNamespace->content 		= $conten;
			$sessionNamespace->changeserror = $error;
			$sessionNamespace->action 		= $action;
			$sessionNamespace->note 		= $note;
			$sessionNamespace->description 	= $arrayChangesError;
			$sessionNamespace->tokenerr 	= $tokenerr;
			$this->_redirect('/changemanagement/index/notification');
		} else {
			if (is_array($dataSelectedChanges)) {
				foreach ($dataSelectedChanges as  $changesInfo) {
					if ($changesInfo['CHANGES_FLAG'] == 'F' && $changesInfo['CHANGES_TYPE'] == 'N' && $changesInfo['MAIN_TABLENAME'] == 'M_USER' && $action == $this->language->_('Request Repair')) {
						$arrayChangesError[$changesInfo['CHANGES_ID']] = 'Changes could not be processed due to current status are prohibited';
					}
					if ($changesInfo['CHANGES_STATUS'] == 'WA') {
						if ($action == $this->language->_('Approve')) {
							$changeModulePrivilegeObj  	= new Changebusiness_Model_Privilege();
							$isAuthorizeAppove 			= $changeModulePrivilegeObj->isAuthorizeApprove($changesInfo['MODULE']);
							// var_dump($changesInfo['MODULE']); die;
							if (!$isAuthorizeAppove) {
								$arrayChangesError[$changesInfo['CHANGES_ID']] = 'You have no privilege for approve specified changes';
							}
						} else if ($action == $this->language->_('Reject')) {
							$changeModulePrivilegeObj  	= new Changebusiness_Model_Privilege();
							$isAuthorizeReject 			= $changeModulePrivilegeObj->isAuthorizeReject($changesInfo['MODULE']);
							if (!$isAuthorizeReject) {
								$arrayChangesError[$changesInfo['CHANGES_ID']] = 'You have no privilege for reject specified changes';
							}
						} else if ($action == $this->language->_('Request Repair')) {
							if ($changesInfo['MODULE'] == 'customeraccount' || ($changesInfo['CHANGES_TYPE'] != 'N' && $changesInfo['CHANGES_TYPE'] != 'E')) {
								$arrayChangesError[$changesInfo['CHANGES_ID']] = 'Changes could not be processed to request repair due to suggest type status are prohibited';
							} else {
								$changeModulePrivilegeObj  	= new Changebusiness_Model_Privilege();
								$isAuthorizeRequestRepair 	= $changeModulePrivilegeObj->isAuthorizeRequestRepair($changesInfo['MODULE']);
								if (!$isAuthorizeRequestRepair) {
									$arrayChangesError[$changesInfo['CHANGES_ID']] = 'You have no privilege for request repair specified changes';
								}
							}
						}
					} else {
						$arrayChangesError[$changesInfo['CHANGES_ID']] = 'Changes could not be processed due to current status are prohibited';
					};
				}
			}
			$this->view->arrayChangesError  = $arrayChangesError;
		}
	}


	public function notificationAction()
	{

		$this->_helper->layout()->setLayout('newlayout');
		$sessionNamespace 	= new Zend_Session_Namespace('notificationRequestChangeList');
		$dataChangesId 		= $sessionNamespace->content;
		$action 			= $sessionNamespace->action;
		$note 				= $sessionNamespace->note;
		$tokenerr 			= $sessionNamespace->tokenerr;
		$description 		= $sessionNamespace->description;
		$this->view->action				= $action;
		$this->view->note 				= $action;
		$this->view->tokenerr 			= $tokenerr;

		$select 						= array();
		$displayDateFormat  			= $this->_dateDisplayFormat;
		$databaseSaveFormat 			= $this->_dateDBFormat;
		$this->view->dateJqueryFormat  	= $this->_dateJqueryFormat;

		$changesTypeCodeArr   = $this->_suggestType['code'];
		$changesStatusCodeArr = array_flip($this->_suggestionStatusCP);
		$changesStatusDescArr = $this->_suggestStatus['desc'];

		unset($this->_suggestType['code']['activate']);
		unset($this->_suggestType['desc']['activate']);
		unset($this->_suggestType['code']['deactivate']);
		unset($this->_suggestType['desc']['deactivate']);
		$this->view->suggestionType   =  $this->_suggestType;
		$this->view->suggestionStatus =  $this->_suggestionStatusCP;
		//companyCode
		$listId = $this->_db->select()
			->from(
				array('M_CUSTOMER'),
				array('CUST_ID', 'CUST_NAME')
			)
			->order('CUST_ID ASC')
			->query()->fetchAll();

		$this->view->listCustId 	= array_merge(array('' => '-- ' . $this->language->_('Any Value') . ' --'), Application_Helper_Array::listArray($listId, 'CUST_ID', 'CUST_ID'));
		$this->view->listCustName 	= Application_Helper_Array::listArray($listId, 'CUST_NAME', 'CUST_NAME');

		//$privi = Zend_Registry::get('privilege');	// get privi ID
		$privi 						= $this->_priviId;

		//get filtering param
		$filterArr = array(
			'filter'			=> array('StringTrim', 'StripTags'),
			'suggest_data' 	  	=> array('StripTags'),
			'cust_id' 	  		=> array('StringTrim', 'StripTags'),
			'cust_name' 	  	=> array('StripTags'),
			'data_id' 			=> array('StringTrim', 'StripTags'),
			'data_name' 		=> array('StripTags'),
			'suggestionType' 	=> array('StringTrim', 'StripTags'),
			'suggestionStatus' 	=> array('StringTrim', 'StripTags'),
			'suggestor' 		=> array('StringTrim', 'StripTags'),
			'changesCreatedFrom' => array('StringTrim', 'StripTags'),
			'changesCreatedTo'	=> array('StringTrim', 'StripTags'),
		);

		$zf_filter 	= new Zend_Filter_Input($filterArr, array(), $this->_request->getParams());
		$filter 	= $zf_filter->getEscaped('filter');

		$fields = array(
			'tableName'    => array(
				'field' => 'DISPLAY_TABLENAME',
				'label' => 'Suggest Data',
				'sortable' => false
			),
			'companyCode'  => array(
				'field' => 'COMPANY_CODE',
				'label' => 'Company Code',
				'sortable' => false
			),
			'companyName'  => array(
				'field' => 'COMPANY_NAME',
				'label' => 'Company Name',
				'sortable' => false
			),
			'dataId'  => array(
				'field' => 'KEY_FIELD',
				'label' => 'Data ID',
				'sortable' => false
			),
			'dataName'  => array(
				'field' => 'KEY_VALUE',
				'label' => 'Data Name',
				'sortable' => false
			),
			'suggestionDate' => array(
				'field' => 'G.CREATED',
				'label' => 'Suggested Date',
				'sortable' => false
			),
			'suggestionBy' => array(
				'field' => 'CREATED_BY',
				'label' => 'Suggested By',
				'sortable' => false
			),
			'suggestionType'  => array(
				'field' => 'CHANGES_TYPE',
				'label' => 'Suggestion Type',
				'sortable' => false
			),
			'suggestionInfo' => array(
				'field' => 'suggestion_status',
				'label' => 'Suggestion Status',
				'sortable' => false
			),
		);

		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby', 'suggestionDate');
		$sortDir = $this->_getParam('sortdir', 'desc');

		//validate parameters before passing to view and query
		$page   = (Zend_Validate::is($page, 'Digits') && ($page > 0)) ? $page : 1;
		$sortBy = (Zend_Validate::is(
			$sortBy,
			'InArray',
			array(array_keys($fields))
		)) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		//Zend_Debug::dump($sortBy);die;
		$sortDir = (Zend_Validate::is(
			$sortDir,
			'InArray',
			array('haystack' => array('asc', 'desc'))
		)) ? $sortDir : 'asc';
		$this->view->currentPage 	= $page;
		$this->view->sortBy 		= $sortBy;
		$this->view->sortDir 		= $sortDir;

		if (!empty($privi))		// kalo privilege is empty, gak bisa generate report, for all data
		{
			$changeModulePrivilegeObj  	= new Changebusiness_Model_Privilege();
			$listAutModuleArr 			= $changeModulePrivilegeObj->getAuthorizeModule();
			$whPriviID 					= "'" . implode("','", $listAutModuleArr) . "'";

			$caseWhenRequestChangeList = " 	CASE WHEN (G.CHANGES_STATUS = 'RR') THEN 'Request Repaired'
											WHEN (READ_STATUS = 0 AND CHANGES_STATUS = 'WA') THEN 'Unread Suggestion'
											WHEN (READ_STATUS = 1 AND CHANGES_STATUS = 'WA') THEN 'Read Suggestion'
											WHEN (READ_STATUS = 2 AND CHANGES_STATUS = 'WA') THEN 'Repaired Suggestion'
											END
										";

			//compose query (use filtering and sorting)
			$select = $this->_db->select()
				->from(
					array('G' => 'T_GLOBAL_CHANGES'),
					array(
						'*',
						'suggestion_status'		=> new Zend_Db_Expr($caseWhenRequestChangeList)
					)
				)
				// ->where('CHANGES_FLAG = '.$this->_db->quote('B'))
				->where("G.MODULE IN (" . $whPriviID . ")");
		}

		//where clauses
		$changesId = "'" . implode("','", $dataChangesId) . "'";
		$select->where("G.CHANGES_ID IN (" . $changesId . ")");

		//order clause

		$select->order(array($sortBy . ' ' . $sortDir));

		$dataSelectedChanges 	= $this->_db->fetchAll($select);
		$this->view->paginator 	= $dataSelectedChanges;
		$arrayChangesError 		= array();
		if (is_array($dataSelectedChanges)) {
			foreach ($dataSelectedChanges as  $changesInfo) {
			}
		}

		$this->view->arrayChangesError  = $description;
		$this->view->fields 			= $fields;
		$this->view->paginatorParam 	= $select;
	}
}
