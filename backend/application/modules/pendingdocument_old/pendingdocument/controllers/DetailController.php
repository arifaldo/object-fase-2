<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'SGO/Extendedmodule/phpqrcode/qrlib.php';

class pendingdocument_DetailController extends Application_Main
{

  protected $_moduleDB = 'RTF'; // masih harus diganti

  public function indexAction()
  {
    $this->_helper->_layout->setLayout('newlayout');

    $bg = $this->_getParam('bg');

	$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
	$password = $sessionNamespace->token;
	$this->view->token = $sessionNamespace->token;
	
    $AESMYSQL = new Crypt_AESMYSQL();
    $decryption = urldecode($bg);
    $decryption_bg = $AESMYSQL->decrypt($decryption, $password);
	//echo $decryption_bg;die;

    $conf = Zend_Registry::get('config');

    $select = $this->_db->select()
      ->from(
        array('TBG' => 'T_BANK_GUARANTEE'),
        array('*')
      )
      ->joinLeft(
        array('MB' => 'M_BRANCH'),
        'MB.BRANCH_CODE = TBG.BG_BRANCH',
        array('BRANCH_NAME')
      )
      ->joinLeft(
        array('MC' => 'M_CUSTOMER'),
        'MC.CUST_ID = TBG.CUST_ID',
        array(
          'CUST_ID',
          'CUST_NAME',
          'CUST_NPWP',
          'CUST_ADDRESS',
          'CUST_CITY',
          'CUST_FAX',
          'CUST_CONTACT',
          'CUST_PHONE',
        )
      )
      ->joinLeft(
        array('MCST' => 'M_CUSTOMER'),
        'MCST.CUST_ID = TBG.SP_OBLIGEE_CODE',
        array(
          "SP_OBLIGEE_NAME" => 'MCST.CUST_NAME',
        )
      )
      ->joinLeft(
        array('INSURANCE' => 'M_CUSTOMER'),
        'INSURANCE.CUST_ID = TBG.BG_INSURANCE_CODE',
        array(
          "INSURANCE_NAME" => 'INSURANCE.CUST_NAME',
          "INSURANCE_BRANCH" => 'INSURANCE.CUST_PROVINCE',
        )
      )
      ->joinLeft(
        array('MCL' => 'M_CITYLIST'),
        'MCL.CITY_CODE = MC.CUST_CITY',
        array('CITY_NAME')
      )
      ->joinLeft(
        array('TBGD' => 'T_BANK_GUARANTEE_DETAIL'),
        'TBGD.BG_REG_NUMBER = TBG.BG_REG_NUMBER',
        array('TBGD.USER_ID', 'TBGD.PS_FIELDNAME')
      )
      ->joinLeft(
        array('TBGS' => 'T_BANK_GUARANTEE_SPLIT'),
        'TBGS.BG_NUMBER = TBG.BG_NUMBER',
        [
          "ACCT", "BANK_CODE", "NAME", "AMOUNT", "FLAG"
        ]
      )
      ->where('TBG.BG_NUMBER = ?', $decryption_bg);
    //->where('TBG.CUST_ID = ?', $this->_custIdLogin);
    //die();
    $data = $this->_db->fetchRow($select);
//	var_dump($data);die;
    $get_cg_number = $this->_db->select()
      ->from("T_BANK_GUARANTEE_DETAIL")
      ->where("BG_REG_NUMBER = ?", $data["BG_REG_NUMBER"])
      ->where("PS_FIELDNAME = ?", "Counter Guarantee Number")
      ->query()->fetchAll();
	 
	
    $this->view->need_cg_num = false;
    if (empty($get_cg_number)) {
	
      $this->view->need_cg_num = true;
    }

    // var_dump($data);die;

    if ($this->_request->isPost()) {
		
      $params = $this->_request->getParams();

      $bgnumber = $this->_getParam('bgnumber');

      $conf = Zend_Registry::get('config');

      $selectbgnumber = $this->_db->select()
        ->from(
          array('TBG' => 'T_BANK_GUARANTEE'),
          array('*')
        )
        ->joinLeft(
          array('MB' => 'M_BRANCH'),
          'MB.BRANCH_CODE = TBG.BG_BRANCH',
          array('BRANCH_NAME')
        )
        ->joinLeft(
          array('MC' => 'M_CUSTOMER'),
          'MC.CUST_ID = TBG.CUST_ID',
          array(
            'CUST_ID',
            'CUST_NAME',
            'CUST_NPWP',
            'CUST_ADDRESS',
            'CUST_CITY',
            'CUST_FAX',
            'CUST_CONTACT',
            'CUST_PHONE',
          )
        )
        ->joinLeft(
          array('MCST' => 'M_CUSTOMER'),
          'MCST.CUST_ID = TBG.SP_OBLIGEE_CODE',
          array(
            "SP_OBLIGEE_NAME" => 'MCST.CUST_NAME',
          )
        )
        ->joinLeft(
          array('INSURANCE' => 'M_CUSTOMER'),
          'INSURANCE.CUST_ID = TBG.BG_INSURANCE_CODE',
          array(
            "INSURANCE_NAME" => 'INSURANCE.CUST_NAME',
            "INSURANCE_BRANCH" => 'INSURANCE.CUST_PROVINCE',
          )
        )
        ->joinLeft(
          array('MCL' => 'M_CITYLIST'),
          'MCL.CITY_CODE = MC.CUST_CITY',
          array('CITY_NAME')
        )
        ->joinLeft(
          array('TBGD' => 'T_BANK_GUARANTEE_DETAIL'),
          'TBGD.BG_REG_NUMBER = TBG.BG_NUMBER',
          array('TBGD.USER_ID', 'TBGD.PS_FIELDNAME')
        )
        ->joinLeft(
          array('TBGS' => 'T_BANK_GUARANTEE_SPLIT'),
          'TBGS.BG_NUMBER = TBG.BG_NUMBER',
          [
            "ACCT", "BANK_CODE", "NAME", "AMOUNT", "FLAG"
          ]
        )
        ->where('TBG.BG_NUMBER = ?', $bgnumber);
      //->where('TBG.CUST_ID = ?', $this->_custIdLogin);
      //die();
	  //echo $selectbgnumber; die;
      $databgnumber = $this->_db->fetchRow($selectbgnumber);


      $datatbank  = [
        'CGINS_STATUS' => 1,
        'BG_UPDATED'   => new Zend_Db_Expr('now()'),
        'BG_UPDATEDBY'   => $this->_userIdLogin
      ];

      $wheretbank = ['BG_NUMBER = ?' => $bgnumber];
      $this->_db->update('T_BANK_GUARANTEE', $datatbank, $wheretbank);

      Application_Helper_General::writeLog('PDBG', 'Update Completed Insurance Counter Guarantee ' . $databgnumber["CUST_NAME"] . ' For BG Number ' . $bgnumber . ' Principal ' . $databgnumber["CUST_ID"]);


      $attahmentDestination   = UPLOAD_PATH . '/document/submit/';
      $adapter         = new Zend_File_Transfer_Adapter_Http();

      $cg_document_file = $_FILES["chooseFile"];

      $cg_document_name = strtolower(uniqid()) . '_' . strtolower(str_replace(' ', '_', basename($adapter->getFileName())));
      $path_old_cg_doc = $cg_document_file['tmp_name'];
      $path_new_cg_doc = $attahmentDestination . $cg_document_name;
      move_uploaded_file($path_old_cg_doc, $path_new_cg_doc);

      if ($this->view->need_cg_num) {
        $insertGuaranteeNumber = array(
          'BG_REG_NUMBER'         => $databgnumber['BG_REG_NUMBER'],
          'CUST_ID'           => 'BANK',
          'USER_ID'           => $this->_userIdLogin,
          'PS_FIELDNAME'        => 'Counter Guarantee Number',
          'PS_FIELDTYPE'    => 1,
          'PS_FIELDVALUE'    => $this->_request->getParam("cg_number"),
        );

        $this->_db->insert('T_BANK_GUARANTEE_DETAIL', $insertGuaranteeNumber);
      }

      $insertDocGuarantee = array(
        'BG_REG_NUMBER'         => $databgnumber['BG_REG_NUMBER'],
        // 'BG_NUMBER'         => $bgnumber,
        'CUST_ID'           => 'BANK',
        'USER_ID'           => $this->_userIdLogin,
        'PS_FIELDNAME'        => 'Counter Guarantee Document',
        'PS_FIELDTYPE'    => 1,
        'PS_FIELDVALUE'    => $cg_document_name,
      );

      $this->_db->insert('T_BANK_GUARANTEE_DETAIL', $insertDocGuarantee);

      $insertDateGuarantee = array(
        'BG_REG_NUMBER'         => $databgnumber['BG_REG_NUMBER'],
        'CUST_ID'           => 'BANK',
        // 'BG_NUMBER'         => $bgnumber,
        'USER_ID'           => $this->_userIdLogin,
        'PS_FIELDNAME'        => 'Counter Guarantee Granted Date',
        'PS_FIELDTYPE'    => 1,
        'PS_FIELDVALUE'    => date('Y-m-d'),
      );

      $this->_db->insert('T_BANK_GUARANTEE_DETAIL', $insertDateGuarantee);


      $this->setbackURL('/' . $this->_request->getModuleName());
      $this->_redirect('/notification/success');
    }

    switch ($data["CHANGE_TYPE"]) {
      case '0':
        $this->view->suggestion_type = "New";
        break;
      case '1':
        $this->view->suggestion_type = "Amendment Changes";
        break;
      case '2':
        $this->view->suggestion_type = "Amendment Draft";
        break;
    }

    //echo '<pre>';print_r($data);
    $conf = Zend_Registry::get('config');
    // BG TYPE
    $bgType         = $conf["bg"]["type"]["desc"];
    $bgCode         = $conf["bg"]["type"]["code"];

    $arrbgType = array_combine(array_values($bgCode), array_values($bgType));

    $this->view->arrbgType = $arrbgType;

    //BG Counter Guarantee Type
    $bgcgType         = $conf["bgcg"]["type"]["desc"];
    $bgcgCode         = $conf["bgcg"]["type"]["code"];

    $arrbgcg = array_combine(array_values($bgcgCode), array_values($bgcgType));

    $this->view->warranty_type_text_new = $arrbgcg[$data['COUNTER_WARRANTY_TYPE']];

    if ($data['COUNTER_WARRANTY_TYPE'] == '1') {

      $bgdatasplit = $this->_db->select()
        ->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
        ->where('A.BG_REG_NUMBER = ?', $data["BG_REG_NUMBER"])
        ->query()->fetchAll();

      $this->view->fullmember = $bgdatasplit;
    }


    $sqlbgdetail = $this->_db->select()
      ->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
      //->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
      ->where('A.BG_REG_NUMBER = ?', $data["BG_REG_NUMBER"]);

    //  die('a');
    $bgdatadetail = $sqlbgdetail
      ->query()->fetchAll();



    if (!empty($bgdatadetail)) {
      foreach ($bgdatadetail as $key => $value) {

        if ($value['PS_FIELDNAME'] == 'Principle Agreement Number') {
          $this->view->principleAgreementNumber =   $value['PS_FIELDVALUE'];
        }
        if ($value['PS_FIELDNAME'] == 'Principle Agreement Granted Date') {
          $this->view->principleAgreementDate =   $value['PS_FIELDVALUE'];
        }

        if ($value['PS_FIELDNAME'] == 'Principle Insurance Premium') {
          $this->view->principleInsurancePremium =   $value['PS_FIELDVALUE'];
        }

        if ($value['PS_FIELDNAME'] == 'Principle Insurance Administration') {
          $this->view->principleInsuranceAdm =   $value['PS_FIELDVALUE'];
        }

        if ($value['PS_FIELDNAME'] == 'Principle Insurance Stamp') {
          $this->view->principleInsuranceStamp =   $value['PS_FIELDVALUE'];
        }

        if ($value['PS_FIELDNAME'] == 'Counter Guarantee Number') {
          $this->view->counterGuaranteeNumber =   $value['PS_FIELDVALUE'];
        }

        if ($value['PS_FIELDNAME'] == 'Insurance Branch') {
          $ins_branch_code =   $value['PS_FIELDVALUE'];

          $sqlinsurancebranch = $this->_db->select()
            ->from("M_INS_BRANCH", ["INS_BRANCH_ACCT"])
            ->where("INS_BRANCH_CODE = ?", $ins_branch_code);

          $get_norekinsurance = ($sqlinsurancebranch)
            ->query()->fetchAll();

          $this->view->norekCabang =   $get_norekinsurance[0]['INS_BRANCH_ACCT'];
        }
      }
    }

    $get_linefacility = $this->_db->select()
      ->from("M_CUST_LINEFACILITY", ["PLAFOND_LIMIT", "FEE_PROVISION", "FEE_ADMIN", "FEE_STAMP"])
      ->where("CUST_ID = ?", $data["CUST_ID"])
      ->query()->fetchAll();

    $this->view->current_limit = $get_linefacility[0]["PLAFOND_LIMIT"] - $total_bgamount_on_risk - $total_bgamount_on_temp;
    $this->view->max_limit = $get_linefacility[0]["PLAFOND_LIMIT"];

    $this->view->linefacility = $get_linefacility[0];

    $this->view->acct = $data['FEE_CHARGE_TO'];

    // nomor rekening insurance

    $norekinsurance = $this->_db->select()
      ->from(array('A' => 'M_INS_BRANCH'), array('*'))
      ->where('A.CUST_ID = ?', $data["CUST_ID"])
      ->query()->fetchAll();

    $this->view->fullmember = $bgdatasplit;


    $conf = Zend_Registry::get('config');
    $this->view->bankname = $conf['app']['bankname'];

    $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
    $param = array('CCY_IN' => 'IDR', 'ACCT_NO' => $data['FEE_CHARGE_TO']);
    $AccArr = $CustomerUser->getAccountsBG($param);
    //var_dump($AccArr);die;

    if (!empty($AccArr)) {
      $this->view->src_name = $AccArr['0']['ACCT_NAME'];
    }

    $get_cash_collateral = $this->_db->select()
      ->from("M_CHARGES_OTHER", ["CHARGES_PCT", "CHARGES_ADM", "CHARGES_STAMP"])
      ->where("CUST_ID = ?", "GLOBAL")
      ->where("CHARGES_TYPE = ?", "10")
      ->query()->fetchAll();

    $this->view->cash_collateral = $get_cash_collateral[0];

    $bgpublishType     = $conf["bgpublish"]["type"]["desc"];
    $bgpublishCode     = $conf["bgpublish"]["type"]["code"];

    $arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

    $this->view->publishForm = $arrbgpublish[$data['BG_PUBLISH']];

    $arrBankFormat = array(
      1 => 'Bank Standard',
      2 => 'Special Format (with bank approval)'
    );

    $this->view->bankFormat = $arrBankFormat[$data['BG_FORMAT']];
    $this->view->bankFormatNumber = $data['BG_FORMAT'];

    $arrLang = array(
      1 => 'Indonesian',
      2 => 'English',
      3 => 'Billingual',
    );

    $this->view->languagetext = $arrLang[$data['BG_LANGUAGE']];

    $checkOthersAttachment = $this->_db->select()
      ->from(["A" => "T_BANK_GUARANTEE_FILE"], ["*"])
      // ->where("BG_NUMBER = '268312345601F4201'")
      ->where('A.BG_REG_NUMBER = ?', $data["BG_REG_NUMBER"])

      ->order('A.INDEX ASC')
      ->query()->fetchAll();

    if (count($checkOthersAttachment) > 0) {
      $this->view->othersAttachment = $checkOthersAttachment;
    }

    // Get data T_BANK_GUARANTEE_HISTORY
    $select = $this->_db->select()
      ->from(
        array('TBGH' => 'T_BANK_GUARANTEE_HISTORY'),
        array('*')
      )
      ->where('TBGH.BG_REG_NUMBER = ?', $data["BG_REG_NUMBER"])
      ->where('TBGH.CUST_ID = ?', $this->_custIdLogin);
    $dataHistory = $this->_db->fetchAll($select);

    // Get data TEMP_BANK_GUARANTEE_SPLIT
    $select = $this->_db->select()
      ->from(
        array('TBGS' => 'T_BANK_GUARANTEE_SPLIT'),
        array('*')
      )
      ->where('TBGS.BG_REG_NUMBER = ?', $data["BG_REG_NUMBER"]);
    $dataAccSplit = $this->_db->fetchAll($select);

    $config = Zend_Registry::get('config');

    $docTypeCode = $config["bgdoc"]["type"]["code"];
    $docTypeDesc = $config["bgdoc"]["type"]["desc"];
    $docTypeArr  = array_combine(array_values($docTypeCode), array_values($docTypeDesc));

    $statusCode = $config["bg"]["status"]["code"];
    $statusDesc = $config["bg"]["status"]["desc"];
    $statusArr  = array_combine(array_values($statusCode), array_values($statusDesc));

    $historyStatusCode = $config["history"]["status"]["code"];
    $historyStatusDesc = $config["history"]["status"]["desc"];
    $historyStatusArr  = array_combine(array_values($historyStatusCode), array_values($historyStatusDesc));

    $counterTypeCode = $config["bgcg"]["type"]["code"];
    $counterTypeDesc = $config["bgcg"]["type"]["desc"];
    $counterTypeArr  = array_combine(array_values($counterTypeCode), array_values($counterTypeDesc));

    //$config    		= Zend_Registry::get('config');
    $BgType     = $config["bg"]["status"]["desc"];
    $BgCode     = $config["bg"]["status"]["code"];

    $arrStatus = array_combine(array_values($BgCode), array_values($BgType));

    // $arrStatus = array(
    //   '7'  => 'Canceled',
    //   '20' => 'On Risk',
    //   '21' => 'Off Risk',
    //   '22' => 'Claimed On Process',
    //   '23' => 'Claimed'
    // );

    $this->view->arrStatus = $arrStatus;





    $this->view->data               = $data;
    //Zend_Debug::dump($data);
    $this->view->dataHistory        = $dataHistory;
    $this->view->dataAccSplit       = $dataAccSplit;
    $this->view->docTypeArr         = $docTypeArr;
    $this->view->statusArr          = $statusArr;
    $this->view->historyStatusArr   = $historyStatusArr;
    $this->view->counterTypeArr     = $counterTypeArr;

    $download = $this->_getParam('download');
    if ($download == 1) {
      $attahmentDestination = UPLOAD_PATH . '/document/submit/';
      $this->_helper->download->file($data['FILE'], $attahmentDestination . $data['FILE']);
    }
  }

  public function Terbilang($nilai)
  {
    $huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
    if ($nilai == 0) {
      return "";
    } elseif ($nilai < 12 & $nilai != 0) {
      return "" . $huruf[$nilai];
    } elseif ($nilai < 20) {
      return $this->Terbilang($nilai - 10) . " Belas ";
    } elseif ($nilai < 100) {
      return $this->Terbilang($nilai / 10) . " Puluh " . $this->Terbilang($nilai % 10);
    } elseif ($nilai < 200) {
      return " Seratus " . $this->Terbilang($nilai - 100);
    } elseif ($nilai < 1000) {
      return $this->Terbilang($nilai / 100) . " Ratus " . $this->Terbilang($nilai % 100);
    } elseif ($nilai < 2000) {
      return " Seribu " . $this->Terbilang($nilai - 1000);
    } elseif ($nilai < 1000000) {
      return $this->Terbilang($nilai / 1000) . " Ribu " . $this->Terbilang($nilai % 1000);
    } elseif ($nilai < 1000000000) {
      return $this->Terbilang($nilai / 1000000) . " Juta " . $this->Terbilang($nilai % 1000000);
    } elseif ($nilai < 1000000000000) {
      return $this->Terbilang($nilai / 1000000000) . " Milyar " . $this->Terbilang($nilai % 1000000000);
    } elseif ($nilai < 100000000000000) {
      return $this->Terbilang($nilai / 1000000000000) . " Trilyun " . $this->Terbilang($nilai % 1000000000000);
    } elseif ($nilai <= 100000000000000) {
      return "Maaf Tidak Dapat di Prose Karena Jumlah nilai Terlalu Besar ";
    }
  }

  public function Terbilangen($nilai)
  {
    $huruf = array("", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", 'Nineteen');
    if ($nilai == 0) {
      return "";
    } elseif ($nilai < 20 & $nilai != 0) {
      return "" . $huruf[$nilai];
    } elseif ($nilai < 100) {
      return $this->Terbilangen($nilai / 10) . "ty " . $this->Terbilangen($nilai % 10);
    } elseif ($nilai < 200) {
      return " Seratus " . $this->Terbilangen($nilai - 100);
    } elseif ($nilai < 1000) {
      return $this->Terbilangen($nilai / 100) . " hundred " . $this->Terbilangen($nilai % 100);
    } elseif ($nilai < 2000) {
      return " Seribu " . $this->Terbilangen($nilai - 1000);
    } elseif ($nilai < 1000000) {
      return $this->Terbilangen($nilai / 1000) . " thausand " . $this->Terbilangen($nilai % 1000);
    } elseif ($nilai < 1000000000) {
      return $this->Terbilangen($nilai / 1000000) . " million " . $this->Terbilangen($nilai % 1000000);
    } elseif ($nilai < 1000000000000) {
      return $this->Terbilangen($nilai / 1000000000) . " Bilion " . $this->Terbilangen($nilai % 1000000000);
    } elseif ($nilai < 100000000000000) {
      return $this->Terbilangen($nilai / 1000000000000) . " Trillion " . $this->Terbilangen($nilai % 1000000000000);
    } elseif ($nilai <= 100000000000000) {
      return "Maaf Tidak Dapat di Prose Karena Jumlah nilai Terlalu Besar ";
    }
  }

  public function indodate($tanggal)
  {
    $bulan = array(
      1 =>   'Januari',
      'Februari',
      'Maret',
      'April',
      'Mei',
      'Juni',
      'Juli',
      'Agustus',
      'September',
      'Oktober',
      'November',
      'Desember'
    );
    $pecahkan = explode('-', $tanggal);

    // variabel pecahkan 0 = tanggal
    // variabel pecahkan 1 = bulan
    // variabel pecahkan 2 = tahun

    return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
  }
}
