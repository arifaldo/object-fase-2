<?php
require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';
require_once("Service/Account.php");

class specialobligee_IndexController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$model = new specialobligee_Model_Specialobligee();

		$data_spobligee = $model->getData();
		$curdate = date('Y-m-d');

		foreach ($data_spobligee as $row) {
			if ($row['STATUS'] != 3) {
				$expDate = $row["PKS_EXP_DATE"];
				if (strtotime($expDate) < strtotime($curdate)) {
					$dataUpdate = ['STATUS' => 3];
					$dataWhere  = ['ID = ?' => $row['ID']];
					$this->_db->update('M_CUST_SPOBLIGEE', $dataUpdate, $dataWhere);
				}
			}
		}

		$setting                            = new Settings();
		$enc_pass                           = $setting->getSetting('enc_pass');
		$enc_salt                           = $setting->getSetting('enc_salt');

		$sessToken                          = new Zend_Session_Namespace('Tokenenc');
		$password_hash                      = md5($enc_salt . $enc_pass);
		$rand                               = $this->_userIdLogin . date('YmdHis') . $password_hash;
		$sessToken->token                   = $rand;
		$this->view->token                  = $sessToken->token;

		$filter                             = $this->_getParam('filter');
		$filter_clear                       = $this->_getParam('filter_clear');

		$filterlist = array(
			"companyName" => 'Company Name',
			"pksStatus" => 'Status',
			"pksEndDate" => 'PKS End Date',
		);

		$this->view->filterlist = $filterlist;

		//get filtering param
		$filters                            = array('*' => array('StringTrim', 'StripTags'));

		$validators                         = array('*' => array('allowEmpty' => true));

		$optionValidators                   = array('breakChainOnFailure' => false);

		$zf_filter                          = new Zend_Filter_Input($filters, $validators, $this->getRequest()->getParams(), $optionValidators);

		$filter                             = ($zf_filter->filter) ? $zf_filter->filter : $this->_request->getParam('filter');

		if ($filter == TRUE && $filter_clear == NULL) {
			// $fCompanyCode                      = $filterParam['fCompanyCode'] = $this->_request->getParam('fCompanyCode');
			$fCompanyName                      = $filterParam['fCompanyName'] = $this->_request->getParam('fCompanyName');
			$pksstatus                     = $filterParam['pksstatus'] = $this->_request->getParam('pksstatus');
			$datefrom                     = $filterParam['datefrom'] = $this->_request->getParam('datefrom');
			$dateto                     = $filterParam['dateto'] = $this->_request->getParam('dateto');
			// $fCollectCode                      = $filterParam['fCollectCode'] = $this->_request->getParam('fCollectCode');
			// $fUploadDateFrom 	= html_entity_decode($zf_filter->getEscaped('fUploadDateFrom'));
			// $fUploadDateTo 		= html_entity_decode($zf_filter->getEscaped('fUploadDateTo'));
		}

		$this->view->saveFilter = $filterParam;

		// if($fUploadDateFrom){
		// 	$FormatDate = new Zend_Date($fUploadDateFrom, $this->_dateDisplayFormat);
		// 	$fUploadDateFrom = $FormatDate->toString($this->_dateDBFormat);	
		// }

		// if($fUploadDateTo){
		// 	$FormatDate = new Zend_Date($fUploadDateTo, $this->_dateDisplayFormat);
		// 	$fUploadDateTo = $FormatDate->toString($this->_dateDBFormat);	
		// }

		$data                               = $model->getData($filterParam, $filter);
		$this->paging($data);
		// echo '<pre>';
		// print_r($data);
		// echo '</pre><br>';
		// die;

		$statusArr = [
			0 => 'Waiting Approval',
			1 => 'Approved',
			2 => 'Terminated',
			3 => 'Expired'
		];

		$getCity = $model->getCity();
		$cityArr = Application_Helper_Array::listArray($getCity, 'CITY_CODE', 'CITY_NAME');

		$this->view->filter                 = $filter;
		$this->view->statusArr              = $statusArr;
		$this->view->cityArr              	= $cityArr;
		// $this->view->fCompanyCode           = $fCompanyCode;
		$this->view->fCompanyName           = $fCompanyName;
		$this->view->pksstatus           = $pksstatus;
		$this->view->datefrom           = $datefrom;
		$this->view->dateto           = $dateto;

		if ($this->_request->getParam("csv")) {
			$field_export = array(
				'CUST_ID'  => array(
					// 'field' => 'CUST_ID',
					'label' => 'Customer ID'
				),
				'CUST_NAME'  => array(
					// 'field' => 'CUST_NAME',
					'label' => 'Customer Name'
				),
				// 'CUST_SEGMENT'  => array(
				// 	'field' => 'CUST_SEGMENT',
				// 	'label' => 'Segment'
				// ),
				'CITY'  => array(
					// 'field' => 'CITY',
					'label' => 'Kab / Kota'
				),
				'COUNTRY'  => array(
					// 'field' => 'COUNTRY',
					'label' => 'Negara'
				),
				'PKS_END_DATE'     => array(
					// 'field' => 'PKS_END_DATE',
					'label' => 'PKS End Date'
				),
				'LAST_APPROVED'     => array(
					// 'field' => 'LAST_APPROVED',
					'label' => 'Last Approved'
				),
				'STATUS'     => array(
					// 'field' => 'STATUS',
					'label' => 'Status'
				)

			);
			foreach ($data as $pTrx) {
				$paramTrx = array(
					"CUST_ID"  				=> $pTrx['CUST_ID'],
					"CUST_NAME" 			=> $pTrx['CUST_NAME'],
					"CITY" 					=> $cityArr[$pTrx['CUST_CITY']],
					"COUNTRY" 				=> $pTrx['COUNTRY_NAME'],
					"PKS_EXP_DATE"  		=> $pTrx['PKS_EXP_DATE'],
					"LAST_APPROVED"  		=> $pTrx['LAST_APPROVED'],
					"STATUS"  				=> $statusArr[$pTrx['STATUS']],
				);
				$newData[] = $paramTrx;
			}

			$this->_helper->download->csv($field_export, $newData, null, $this->language->_('Special Obligee Report'));
		}
		// $this->view->fUploadDateFrom 	= $fUploadDateFrom;
		// $this->view->fUploadDateTo 		= $fUploadDateTo;

		Application_Helper_General::writeLog('VSBO', 'View Special Obligee Customer List');
	}

	public function onboardingAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		if (!$this->view->hasPrivilege("OSBO")) {
			return $this->_redirect("/home/dashboard");
		}

		$model                              = new specialobligee_Model_Specialobligee();

		$getCustomer                        = $model->getCustomer();
		$custIdArr                          = Application_Helper_Array::listArray($getCustomer, 'CUST_ID', 'CUST_NAME');
		$this->view->custIdArr              = $custIdArr;

		$statusArrArr = [
			1 => 'Approved',
			2 => 'Terminated',
			3 => 'Expired'
		];

		$this->view->statusArrArr           = $statusArrArr;
		$this->view->sharing_fee           = "0.00";

		if ($this->_request->isPost()) {
			$filters                           = array('*' => array('StringTrim', 'StripTags'));

			$zf_filter_input                   = new Zend_Filter_Input($filters, null, $this->_request->getPost());

			$cust_id                           = $zf_filter_input->cust_id;
			$pks_number                        = $zf_filter_input->pks_number;
			$pks_file                          = $_FILES['pks_file'];
			$pks_start_date                    = $zf_filter_input->pks_start_date;
			$pks_exp_date                      = $zf_filter_input->pks_exp_date;
			$debited_acct                      = $zf_filter_input->debited_acct;
			$sharing_fee                       = $zf_filter_input->sharing_fee;

			//$fee_stamp 			= Application_Helper_General::convertDisplayMoney($zf_filter_input->fee_stamp);

			// check debited_acct ----------------------------------------------------------------------

			$scvDebited = new Service_Account($debited_acct, "");
			$checkDebited = $scvDebited->inquiryAccontInfo();

			if ($checkDebited["response_code"] == "0000") {
				if ($scvDebited->inquiryAccountBalance()["response_code"] == "0000") {
					if ($scvDebited->inquiryAccountBalance()["status"] != '1' && $scvDebited->inquiryAccountBalance()["status"] != '4') {
						$errDesc['debited_acct'] = "Status rekening tidak aktif";
					}
				} else {
					if ($scvDebited->inquiryDeposito()["status"] != '1' && $scvDebited->inquiryDeposito()["status"] != '4') {
						$errDesc['debited_acct'] = "Status rekening tidak aktif";
					}
				}
			} else {
				$errDesc['debited_acct'] = "Rekening tidak ditemukan";
			}

			$check_acct_on_db = $this->_db->select()
				->from("M_CUSTOMER_ACCT")
				->where("ACCT_NO = ?", $debited_acct)
				->where("CUST_ID = ?", $cust_id)
				->query()->fetch();

			if ($check_acct_on_db["ACCT_STATUS"] != '1') {
				$errDesc['debited_acct'] = "Rekening Nasabah pada sistem bukan status approved";
			}

			// -----------------------------------------------------------------------------------------

			if (empty($cust_id)) {
				$errDesc['cust_id']               = $this->language->_('Kolom wajib di isi');
			}
			if (empty($debited_acct)) {
				$errDesc['debited_acct'] = $this->language->_('Kolom wajib di isi');
			}
			if (empty($pks_number)) {
				$errDesc['pks_number']            = $this->language->_('Kolom wajib di isi');
			}
			if (empty($pks_file['name'])) {
				$errDesc['pks_file']              = $this->language->_('Kolom wajib di isi');
			}
			if (empty($pks_start_date)) {
				$errDesc['pks_start_date']        = $this->language->_('Kolom wajib di isi');
			}
			if (empty($pks_exp_date)) {
				$errDesc['pks_exp_date']          = $this->language->_('Kolom wajib di isi');
			}

			if (empty($sharing_fee)) {
				$sharing_fee = 0;
			}

			if ($sharing_fee > 100) {
				$errDesc['sharing_fee']           = $this->language->_('Sharing Fee maximal 100%');
			}


			$this->view->errDesc               = $errDesc;
			$this->view->cust_id               = $cust_id;

			$this->view->pks_number            = $pks_number;
			$this->view->pks_start_date        = $pks_start_date;
			$this->view->pks_exp_date          = $pks_exp_date;
			$this->view->debited_acct          = $debited_acct;
			$this->view->sharing_fee           = $sharing_fee;

			$valid = true;

			$svcAccount = new Service_Account($debited_acct, null);
			$resultBalance = $svcAccount->inquiryAccountBalance('AB', TRUE);

			//var_dump($result);die;
			if ($resultBalance['response_desc'] == 'Success') //00 = success
			{
				$valid = true;
			} else {
				$valid = false;
				$err = true;
				$errmsg = 'Service error';
			}


			if (empty($errDesc) && $valid) {
				$path                             = UPLOAD_PATH . '/document/submit/';

				$filename_pks                     = strtolower(uniqid()) . '_' . strtolower(str_replace(' ', '_', $pks_file['name']));
				$path_old_pks                     = $pks_file['tmp_name'];
				$path_new_pks                     = $path . $filename_pks;
				move_uploaded_file($path_old_pks, $path_new_pks);

				$model                            = new specialobligee_Model_Specialobligee();
				$getCustomer                      = $model->getCustomerById($cust_id);
				$cust_name                        = $getCustomer['CUST_NAME'];
				$info                             = 'Customer ID = ' . $cust_id . ', Customer Name = ' . $cust_name;
				$change_id                        = $this->suggestionWaitingApproval('Special Obligee', $info, $this->_changeType['code']['new'], null, 'M_CUST_SPOBLIGEE', 'TEMP_CUST_SPOBLIGEE', $cust_id, $cust_name, $cust_id, $cust_name);
				$success                          = 1;

				try {

					$var                             = array();
					$var['CHANGES_ID']               = $change_id;
					$var['CUST_ID']                  = $cust_id;
					$var['PKS_NUMBER']               = $pks_number;
					$var['PKS_FILE']                 = $filename_pks;
					$var['PKS_START_DATE']           = $pks_start_date;
					$var['PKS_EXP_DATE']             = $pks_exp_date;
					$var['SHARING_ACCT']             = $debited_acct;
					$var['SHARING_FEE']              = $sharing_fee ?: 0;
					$var['STATUS']                   = 0;
					$var['FLAG']                     = 1;
					$var['LAST_SUGGESTEDBY']         = $this->_userIdLogin;
					$var['LAST_SUGGESTED']           = new Zend_Db_Expr('now()');
					//echo "<pre>";
					//print_r($var);die;
					$this->_db->insert("TEMP_CUST_SPOBLIGEE", $var);
				} catch (Exception $e) {
					//var_dump($e);die;
					$success                         = 0;
				}

				if ($success == 1) {
					Application_Helper_General::writeLog('OSBO', 'Customer Special Obligee Business Onboarding for Company : [' . $cust_name . '] (' . $cust_id . ')');
					$this->setbackURL('/specialobligee');
					$this->_redirect('/notification/submited/index');
				} else {
					$this->_db->rollBack();
				}
			}

			if ($err) {
				$errmsg = 'Kolom wajib diisi';
				$this->view->err = true;
				$this->view->errmsg = $errmsg;
			}
		} else {
			Application_Helper_General::writeLog('VSBO', 'View Special Obligee List');
		}
	}

	public function detailAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$model                              = new specialobligee_Model_Specialobligee();

		$id                                 = $this->_getParam('id');
		$download                           = $this->_getParam('download');
		$downloadTerminate                  = $this->_getParam('downloadTerminate');
		$submit 							= $this->_getParam('submit');

		$sessToken                          = new Zend_Session_Namespace('Tokenenc');
		$password                           = $sessToken->token;

		$AESMYSQL                           = new Crypt_AESMYSQL();
		$decryption                         = urldecode($id);
		$decryption_id                      = $AESMYSQL->decrypt($decryption, $password);

		$data                               = $model->getDataById($decryption_id);
		$this->view->data                   = $data;

		$statusArr = [
			1 => 'Approved',
			2 => 'Terminated',
			3 => 'Expired'
		];

		$this->view->statusArr           	= $statusArr;

		$currentUrl                         = $this->getRequest()->getRequestUri();
		$this->view->currentUrl             = $currentUrl;

		$params                             = $this->getRequest()->getParams();
		$this->view->params                 = $params;

		$cust_id                            = $data['CUST_ID'];
		$getCustAcctById                    = $model->getCustAcctById($cust_id);
		$debitedAcctArr                     = [];
		foreach ($getCustAcctById as $row) {
			$debitedAcctArr[$row['ACCT_NO']]   = $row['ACCT_NO'] . ' (' . $row['CCY_ID'] . ')' . ' / ' . $row['ACCT_NAME'] . ' / ' . $row['ACCT_DESC'];
		}
		$this->view->debitedAcctArr         = $debitedAcctArr;

		$getTempSpecialObligee = $model->getTempSpecialObligee($cust_id);
		if ($getTempSpecialObligee) {
			$this->view->is_update = true;
		}

		if ($download) {
			$path                              = UPLOAD_PATH . '/document/submit/';
			$file                              = $data['PKS_FILE'];
			$this->_helper->download->file($file, $path . $file);
		}

		if ($downloadTerminate) {
			$path                              = UPLOAD_PATH . '/document/submit/';
			$file                              = $data['TERMINATED_FILE'];
			$this->_helper->download->file($file, $path . $file);
		}

		if ($submit) {
			$doc_file = $_FILES['underlying_doc'];
			$reason = $this->_getParam('reason');

			$path = UPLOAD_PATH . '/document/submit/';
			$filename_doc = strtolower(uniqid()) . '_' . strtolower(str_replace(' ', '_', $doc_file['name']));
			$path_old_doc = $doc_file['tmp_name'];
			$path_new_doc = $path . $filename_doc;
			move_uploaded_file($path_old_doc, $path_new_doc);

			$getCustomer  = $model->getCustomerById($cust_id);
			$cust_name    = $getCustomer['CUST_NAME'];
			$info         = 'Customer ID = ' . $cust_id . ', Customer Name = ' . $cust_name;
			$change_id    = $this->suggestionWaitingApproval('Special Obligee', $info, $this->_changeType['code']['edit'], null, 'M_CUST_SPOBLIGEE', 'TEMP_CUST_SPOBLIGEE', $cust_id, $cust_name, $cust_id, $cust_name);

			try {
				$this->_db->beginTransaction();

				$dataInsert = [
					'CHANGES_ID'		=> $change_id,
					'CUST_ID'			=> $cust_id,
					'SHARING_ACCT'		=> $data['SHARING_ACCT'],
					'PKS_NUMBER'		=> $data['PKS_NUMBER'],
					'PKS_FILE'			=> $data['PKS_FILE'],
					'PKS_START_DATE'	=> $data['PKS_START_DATE'],
					'PKS_EXP_DATE'		=> $data['PKS_EXP_DATE'],
					'SHARING_FEE'		=> $data['SHARING_FEE'],
					'STATUS'			=> 2, // Terminate
					'FLAG'				=> 1, // Waiting Review
					'ENDED_REASON'		=> $reason,
					'TERMINATED_FILE'	=> $filename_doc,
					'LAST_SUGGESTED'	=> new Zend_Db_Expr('now()'),
					'LAST_SUGGESTEDBY'	=> $this->_userIdLogin
				];
				$this->_db->insert('TEMP_CUST_SPOBLIGEE', $dataInsert);

				$this->_db->commit();

				Application_Helper_General::writeLog('OSBO', 'Special Obligee Business Terminate CUST_ID: ' . $cust_id);
				$this->setbackURL('/specialobligee');
				$this->_redirect('/notification/submited/index');
			} catch (Exception $error) {
				$this->_db->rollBack();
				echo '<pre>';
				print_r($error->getMessage());
				echo '</pre><br>';
				die;
			}

			$data = [
				'STATUS'			=> 2,
				'ENDED_REASON'		=> $reason,
				'TERMINATED_FILE'	=> $filename_doc
			];
			$where = ['ID = ?' => $decryption_id];
			$this->_db->update('M_CUST_SPOBLIGEE', $data, $where);

			Application_Helper_General::writeLog('OSBO', 'Terminate Special Obligee Agreement for Company : [customer name] ([customer code]). Reasons : [Ended Reason].');
			$this->setbackURL('/specialobligee');
			$this->_redirect('/notification/success');
		}
	}

	public function updateAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		if (!$this->view->hasPrivilege("BSBO")) {
			return $this->_redirect("/home/dashboard");
		}

		$model                        = new specialobligee_Model_Specialobligee();

		$id                           = $this->_getParam('id');

		$sessToken                    = new Zend_Session_Namespace('Tokenenc');
		$password                     = $sessToken->token;

		$AESMYSQL                     = new Crypt_AESMYSQL();
		$decryption                   = urldecode($id);
		$decryption_id                = $AESMYSQL->decrypt($decryption, $password);

		$data                         = $model->getDataById($decryption_id);
		$this->view->data             = $data;

		$currentUrl                   = $this->getRequest()->getRequestUri();
		$this->view->currentUrl       = $currentUrl;

		$params                       = $this->getRequest()->getParams();
		$this->view->params           = $params;

		$cust_id = $data['CUST_ID'];

		$debitedAcctArr               = $model->getCustAcctById($cust_id);

		$getAllProduct = $this->_db->select()
			->from("M_PRODUCT")
			->query()->fetchAll();

		$newArr2 = [];
		foreach ($debitedAcctArr as $keyArr => $arrAcct) {

			$accountTypeCheck = false;
			$productTypeCheck = false;

			$svcAccount = new Service_Account($arrAcct['ACCT_NO'], null, null, null, null, null);
			$result = $svcAccount->inquiryAccountBalance();

			if ($result['response_code'] == '0000') {
				$accountType = $result['account_type'];
				$productType = $result['product_type'];
			} else {
				$result = $svcAccount->inquiryDeposito();

				$accountType = $result['account_type'];
				$productType = $result['product_type'];
			}

			foreach ($getAllProduct as $key => $value) {
				# code...
				if ($value['PRODUCT_CODE'] == $accountType) {
					$accountTypeCheck = true;
				};

				if ($value['PRODUCT_PLAN'] == $productType) {
					$productTypeCheck = true;
				};
			}

			if ($accountTypeCheck && $productTypeCheck) $newArr2[] = $arrAcct;
		}

		$this->view->debitedAcctArr   = $newArr2;

		if ($this->_request->isPost()) {
			$filters                           = array('*' => array('StringTrim', 'StripTags'));

			$zf_filter_input                   = new Zend_Filter_Input($filters, null, $this->_request->getPost());

			$pks_number                        = $zf_filter_input->pks_number;
			$pks_file                          = $_FILES['pks_file'];
			$pks_start_date                    = $zf_filter_input->pks_start_date;
			$pks_exp_date                      = $zf_filter_input->pks_exp_date;
			$debited_acct                      = $zf_filter_input->debited_acct;
			$sharing_fee                       = $zf_filter_input->sharing_fee;

			// check debited_acct ----------------------------------------------------------------------

			$scvDebited = new Service_Account($debited_acct, "");
			$checkDebited = $scvDebited->inquiryAccontInfo();

			if ($checkDebited["response_code"] == "0000") {
				if ($scvDebited->inquiryAccountBalance()["response_code"] == "0000") {
					if ($scvDebited->inquiryAccountBalance()["status"] != '1' && $scvDebited->inquiryAccountBalance()["status"] != '4') {
						$errDesc['debited_acct'] = "Status rekening tidak aktif";
					}
				} else {
					if ($scvDebited->inquiryDeposito()["status"] != '1' && $scvDebited->inquiryDeposito()["status"] != '4') {
						$errDesc['debited_acct'] = "Status rekening tidak aktif";
					}
				}
			} else {
				$errDesc['debited_acct'] = "Rekening tidak ditemukan";
			}

			// -----------------------------------------------------------------------------------------

			//$fee_stamp 			= Application_Helper_General::convertDisplayMoney($zf_filter_input->fee_stamp);

			if (empty($debited_acct)) {
				$errDesc['debited_acct'] = $this->language->_('Sharing Fee Account cannot be empty');
			}

			if (empty($pks_number)) {
				$errDesc['pks_number']            = $this->language->_('Underlying Document cannot be empty');
			}
			// if (empty($pks_file['name'])) {
			// 	$errDesc['pks_file']              = $this->language->_('Upload Document cannot be empty');
			// }
			if (empty($pks_start_date)) {
				$errDesc['pks_start_date']        = $this->language->_('Facility Start Date cannot be empty');
			}
			if (empty($pks_exp_date)) {
				$errDesc['pks_exp_date']          = $this->language->_('Facility Expired Date cannot be empty');
			}

			if ($sharing_fee > 100) {
				$errDesc['sharing_fee']           = $this->language->_('Sharing Fee maximal 100%');
			}
			$valid = true;

			$svcAccount = new Service_Account($debited_acct, null);
			$resultBalance = $svcAccount->inquiryAccountBalance('AB', TRUE);

			//var_dump($result);die;
			if ($resultBalance['response_desc'] == 'Success') //00 = success
			{
				$valid = true;
			} else {
				$valid = false;
				$err = true;
				$errmsg = 'Service error';
			}



			$this->view->errDesc               = $errDesc;
			$this->view->cust_id               = $cust_id;
			$this->view->pks_number            = $pks_number;
			$this->view->pks_start_date        = $pks_start_date;
			$this->view->pks_exp_date          = $pks_exp_date;
			$this->view->debited_acct          = $debited_acct;
			$this->view->sharing_fee           = $sharing_fee;


			if (empty($errDesc) && $valid) {
				$path                             = UPLOAD_PATH . '/document/submit/';

				if ($pks_file["size"] != 0) {
					$filename_pks                     = strtolower(uniqid()) . '_' . strtolower(str_replace(' ', '_', $pks_file['name']));
					$path_old_pks                     = $pks_file['tmp_name'];
					$path_new_pks                     = $path . $filename_pks;
					move_uploaded_file($path_old_pks, $path_new_pks);
				} else {
					$filename_pks = $data["PKS_FILE"];
				}

				$getCustomer                      = $model->getCustomerById($cust_id);
				$cust_name                        = $getCustomer['CUST_NAME'];
				$info                             = 'Customer ID = ' . $cust_id . ', Customer Name = ' . $cust_name;
				$change_id    					  = $this->suggestionWaitingApproval('Special Obligee', $info, $this->_changeType['code']['edit'], null, 'M_CUST_SPOBLIGEE', 'TEMP_CUST_SPOBLIGEE', $cust_id, $cust_name, $cust_id, $cust_name);
				$success                          = 1;

				try {
					$this->_db->beginTransaction();

					$dataInsert = [
						'CHANGES_ID'		=> $change_id,
						'CUST_ID'			=> $cust_id,
						'SHARING_ACCT'		=> $debited_acct,
						'PKS_NUMBER'		=> $pks_number,
						'PKS_FILE'			=> $filename_pks,
						'PKS_START_DATE'	=> $pks_start_date,
						'PKS_EXP_DATE'		=> $pks_exp_date,
						'SHARING_FEE'		=> $sharing_fee,
						'STATUS'			=> $data['STATUS'],
						'FLAG'				=> 1,
						'ENDED_REASON'		=> $data['ENDED_REASON'],
						'TERMINATED_FILE'	=> $data['TERMINATED_FILE'],
						'LAST_SUGGESTED'	=> new Zend_Db_Expr('now()'),
						'LAST_SUGGESTEDBY'	=> $this->_userIdLogin
					];
					$this->_db->insert('TEMP_CUST_SPOBLIGEE', $dataInsert);

					$this->_db->commit();

					Application_Helper_General::writeLog('BSBO', 'Update Special Obligee Agreement CUST_ID: ' . $cust_id);
					$this->setbackURL('/specialobligee');
					$this->_redirect('/notification/submited/index');
				} catch (Exception $error) {
					$this->_db->rollBack();
					echo '<pre>';
					print_r($error->getMessage());
					echo '</pre><br>';
					die;
				}
			}

			if ($err) {
				$this->view->err = true;
				$this->view->errmsg = $errmsg;
			}
		} else {
			Application_Helper_General::writeLog('VSBO', 'View Special Obligee List');
		}
	}

	public function checkcustomerAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();

		$model = new specialobligee_Model_Specialobligee();

		$request = $this->getRequest();

		$cust_id = $request->cust_id;

		$getCustAcctById = $model->getCustAcctById($cust_id);

		$getAllProduct = $this->_db->select()
			->from("M_PRODUCT")
			->query()->fetchAll();

		$newArr2 = [];
		foreach ($getCustAcctById as $keyArr => $arrAcct) {

			$accountTypeCheck = false;
			$productTypeCheck = false;

			$svcAccount = new Service_Account($arrAcct['ACCT_NO'], null, null, null, null, null);
			$result = $svcAccount->inquiryAccountBalance();

			if ($result['response_code'] == '0000') {
				$accountType = $result['account_type'];
				$productType = $result['product_type'];
			} else {
				$result = $svcAccount->inquiryDeposito();

				$accountType = $result['account_type'];
				$productType = $result['product_type'];
			}

			foreach ($getAllProduct as $key => $value) {
				# code...
				if ($value['PRODUCT_CODE'] == $accountType) {
					$accountTypeCheck = true;
				};

				if ($value['PRODUCT_PLAN'] == $productType) {
					$productTypeCheck = true;
				};
			}

			if ($accountTypeCheck && $productTypeCheck) $newArr2[] = $arrAcct;
		}

		// print_r($getCustAcctById);
		// die();

		$getTempSpecialObligee = $model->getTempSpecialObligee($cust_id);
		if ($getTempSpecialObligee) {
			$is_onboarding = true;
		} else {
			$is_onboarding = false;
		}

		echo json_encode(array('debited_acct' => $newArr2, 'is_onboarding' => $is_onboarding));
	}

	public function checkterminateAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();


		$model = new specialobligee_Model_Specialobligee();

		$data = $model->getData();
		$curdate = date('Y-m-d');

		$freeze = 0;
		foreach ($data as $row) {
			if ($row['STATUS'] != 2) {
				$startDate = $row['PKS_START_DATE'];
				$expDate = $row["PKS_EXP_DATE"];
				$day = $row['CLAIM_DEADLINE'];
				$deadlineDate = date('Y-m-d', strtotime("+$day day", strtotime($startDate)));
				// if ($deadlineDate < $curdate) {
				if (date('Y-m-d', strtotime($expDate)) < $curdate) {
					$dataUpdate = ['STATUS' => 2];
					$dataWhere  = ['ID = ?' => $row['ID']];
					$this->_db->update('M_CUST_SPOBLIGEE', $dataUpdate, $dataWhere);
					$freeze++;
				}
			}
		}
		if ($freeze > 0) {
			echo json_encode(array('freeze' => TRUE));
		} else {
			echo json_encode(array('freeze' => FALSE));
		}
	}

	public function checkexpireAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();


		$model = new specialobligee_Model_Specialobligee();

		$data = $model->getData();
		$curdate = date('Y-m-d');

		$expire = 0;
		foreach ($data as $row) {
			if ($row['STATUS'] != 3) {
				$expDate = $row["PKS_EXP_DATE"];
				if (strtotime($expDate) < strtotime($curdate)) {
					$dataUpdate = ['STATUS' => 3];
					$dataWhere  = ['ID = ?' => $row['ID']];
					$this->_db->update('M_CUST_SPOBLIGEE', $dataUpdate, $dataWhere);
					$expire++;
				}
			}
		}
		if ($freeze > 0) {
			echo json_encode(array('freeze' => TRUE));
		} else {
			echo json_encode(array('freeze' => FALSE));
		}
	}

	public function downloadAction()
	{
		$change_id = $this->_request->getParam("changeid");
		$underlying = $this->_request->getParam("underlying");
		$terminatedFile = $this->_request->getParam('terminated');
		$master = $this->_request->getParam("master");

		$temp_cust_so = $this->_db->select()
			->from("TEMP_CUST_SPOBLIGEE")
			->where("CHANGES_ID = ?", $change_id)
			->query()->fetch();

		if ($master) {
			$get_master = $this->_db->select()
				->from("M_CUST_SPOBLIGEE")
				->where("CUST_ID = ?", $temp_cust_so["CUST_ID"])
				->query()->fetch();
			$temp_cust_so = $get_master;
		}

		if ($underlying) {

			if (file_exists(UPLOAD_PATH . "/document/submit/" . $temp_cust_so["PKS_FILE"])) {
				$file_url = UPLOAD_PATH . "/document/submit/" . $temp_cust_so["PKS_FILE"];
				header('Content-Description: File Transfer');
				header('Content-Type: application/octet-stream');
				header('Content-Disposition: attachment; filename="' . $temp_cust_so["PKS_FILE"] . '"');
				header('Content-Transfer-Encoding: binary');
				header('Expires: 0');
				header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
				header('Pragma: public');
				header('Content-Length: ' . filesize($file_url)); //Absolute URL
				ob_clean();
				flush();
				readfile($file_url); //Absolute URL
				exit();
			} else {
				return $this->_helper->json([
					'message' => 'file tidak ditemukan'
				]);
			}
		}

		if ($terminatedFile) {
			if (file_exists(UPLOAD_PATH . "/document/submit/" . $temp_cust_so["TERMINATED_FILE"])) {
				$file_url = UPLOAD_PATH . "/document/submit/" . $temp_cust_so["TERMINATED_FILE"];
				header('Content-Description: File Transfer');
				header('Content-Type: application/octet-stream');
				header('Content-Disposition: attachment; filename="' . $temp_cust_so["TERMINATED_FILE"] . '"');
				header('Content-Transfer-Encoding: binary');
				header('Expires: 0');
				header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
				header('Pragma: public');
				header('Content-Length: ' . filesize($file_url)); //Absolute URL
				ob_clean();
				flush();
				readfile($file_url); //Absolute URL
				exit();
			}
		}
	}
}
