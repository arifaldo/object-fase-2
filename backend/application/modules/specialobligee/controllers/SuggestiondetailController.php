<?php
require_once 'Zend/Controller/Action.php';
require_once("Service/Account.php");

class specialobligee_SuggestiondetailController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newpopup');

		$model = new specialobligee_Model_Specialobligee();

		$change_id = $this->_getParam('changes_id');
		$change_id = (Zend_Validate::is($change_id, 'Digits')) ? $change_id : 0;

		$this->view->change_id = $change_id;

		$this->view->suggestionType = $this->_suggestType;

		if (!$this->view->hasPrivilege("VSOC")) {
			return $this->_redirect("/home/dashboard");
		}

		if ($change_id) {

			$select = $this->_db->select()
				->from('T_GLOBAL_CHANGES', array('CHANGES_ID'))
				->where('CHANGES_ID = ' . $this->_db->quote($change_id))
				->where("CHANGES_STATUS = 'WA' OR CHANGES_STATUS = 'RR'");
			$result = $this->_db->fetchOne($select);

			if (empty($result)) {
				$this->_redirect('/notification/invalid/index');
			} else {
				$select = $this->_db->select()
					->from(array('SP' => 'TEMP_CUST_SPOBLIGEE'))
					->joinLeft(
						array('B' => 'M_CUSTOMER'),
						'B.CUST_ID = SP.CUST_ID',
						array('CUST_NAME', 'COLLECTIBILITY_CODE')
					)
					->join(
						array('G' => 'T_GLOBAL_CHANGES'),
						'G.CHANGES_ID = SP.CHANGES_ID',
						array('CHANGES_ID', 'CHANGES_TYPE', 'CREATED', 'CREATED_BY', 'CHANGES_STATUS', 'READ_STATUS', 'MODULE')
					)
					->where('SP.CHANGES_ID = ?', $change_id);
				$resultData = $this->_db->fetchRow($select);

				if ($resultData) {
					$this->view->cust_id 		= $resultData['CUST_ID'];
					$this->view->changes_id 	= $resultData['CHANGES_ID'];
					$this->view->changes_type 	= $resultData['CHANGES_TYPE'];
					$this->view->changes_status = $resultData['CHANGES_STATUS'];
					$this->view->read_status 	= $resultData['READ_STATUS'];
					$this->view->flag 			= $resultData['FLAG'];

					$statusArr = [
						'1'	=> 'Approved',
						'2'	=> 'Terminated',
						'3'	=> 'Expired'
					];
					$this->view->statusArr = $statusArr;


					// DATA TEMP ////////////////////////////////////////////////////////
					$getCustomer = $model->getCustomerById($resultData['CUST_ID']);

					$tempAcct = $model->getCustAcctByAcct($resultData['SHARING_ACCT']);
					$this->view->debited_account = $tempAcct['ACCT_NO'] . ' (' . $tempAcct['CCY_ID'] . ')' . ' / ' . $tempAcct['ACCT_NAME'] . ' / ' . $tempAcct['ACCT_DESC'];

					$this->view->company 			= $getCustomer['CUST_NAME'] . ' (' . $getCustomer['CUST_ID'] . ')';
					$this->view->pks_number 		= $resultData['PKS_NUMBER'];
					$this->view->pks_start_date 	= date('d M Y', strtotime($resultData['PKS_START_DATE']));
					$this->view->pks_exp_date 		= date('d M Y', strtotime($resultData['PKS_EXP_DATE']));
					$this->view->sharing_fee 		= $resultData['SHARING_FEE'];
					$this->view->status 			= $statusArr[$resultData['STATUS']];
					$this->view->reason 			= $resultData['ENDED_REASON'];

					// DATA MASTER ////////////////////////////////////////////////////

					$dataMaster = $model->getDataByCustId($resultData['CUST_ID']);

					if ($dataMaster) {

						$masterAcct = $model->getCustAcctByAcct($dataMaster['SHARING_ACCT']);

						$this->view->m_debited_account = $masterAcct['ACCT_NO'] . ' (' . $masterAcct['CCY_ID'] . ')' . ' / ' . $masterAcct['ACCT_NAME'] . ' / ' . $masterAcct['ACCT_DESC'];

						$this->view->m_company 			= $dataMaster['CUST_NAME'] . ' (' . $dataMaster['CUST_ID'] . ')';
						$this->view->m_pks_number 		= $dataMaster['PKS_NUMBER'];
						$this->view->m_pks_file			= $dataMaster['PKS_FILE'];
						$this->view->m_pks_start_date 	= date('d M Y', strtotime($dataMaster['PKS_START_DATE']));
						$this->view->m_pks_exp_date 	= date('d M Y', strtotime($dataMaster['PKS_EXP_DATE']));
						$this->view->m_sharing_fee 		= $dataMaster['SHARING_FEE'];
						$this->view->m_status 			= $statusArr[$dataMaster['STATUS']];
						$this->view->m_reason 			= $dataMaster['ENDED_REASON'];
					}

					$submit_review = $this->_getParam('submit_review');
					$submit_request_repair = $this->_getParam('submit_request_repair');

					if ($submit_review) {

						if (!$this->view->hasPrivilege("RSOC")) {
							return $this->_redirect("/home/dashboard");
						}

						$svcAccount = new Service_Account($tempAcct['ACCT_NO'], null);
						$resultBalance = $svcAccount->inquiryAccountBalance('AB', TRUE);

						//var_dump($result);die;
						if ($resultBalance['response_desc'] == 'Success') //00 = success
						{
							$valid = true;
						} else {
							$valid = false;
							$err = true;
							$errmsg = 'Service error';
						}

						//var_dump($valid);die;
						if ($valid) {
							try {
								$this->_db->beginTransaction();

								$data = [
									'FLAG'				=> 2,
									'LAST_REVIEWED'		=> new Zend_Db_Expr('now()'),
									'LAST_REVIEWEDBY'	=> $this->_userIdLogin
								];
								$where = ['CUST_ID = ?' => $resultData['CUST_ID']];
								$this->_db->update('TEMP_CUST_SPOBLIGEE', $data, $where);

								$this->_db->commit();

								$this->view->is_review = true;
								// Application_Helper_General::writeLog('RSOC', 'Review Special Obligee : ' . $dataMaster['CUST_ID']);
								Application_Helper_General::writeLog('RSOC', 'Review Special Obligee : ' . $resultData['CUST_ID']);
							} catch (Exception $error) {
								$this->_db->rollBack();
								echo '<pre>';
								print_r($error->getMessage());
								echo '</pre><br>';
								die;
							}
							header("Refresh:0");
						}
					}

					if ($submit_request_repair) {
						try {
							$this->_db->beginTransaction();

							$data2  = [
								'CHANGES_STATUS'	=> 'RR',
								'LASTUPDATED'		=> new Zend_Db_Expr('now()')
							];
							$where2 = ['CHANGES_ID = ?' => $change_id];
							$this->_db->update('T_GLOBAL_CHANGES', $data2, $where2);

							$this->_db->commit();

							$this->view->is_request_repair = true;
						} catch (Exception $error) {
							$this->_db->rollBack();
							echo '<pre>';
							print_r($error->getMessage());
							echo '</pre><br>';
							die;
						}

						header("Refresh:0");
					}

					if ($err) {
						//$err = true;
						//$errmsg = 'Service not available';

						$this->view->err = true;
						$this->view->errmsg = $errmsg;
					}
				}
			}
		}
	}

	public function repairAction()
	{
		$this->_helper->layout()->setLayout('newpopup');

		$model = new specialobligee_Model_Specialobligee();

		if (!$this->view->hasPrivilege("RPOC")) {
			return $this->_redirect("/home/dashboard");
		}

		$change_id = $this->_getParam('changes_id');
		$change_id = (Zend_Validate::is($change_id, 'Digits')) ? $change_id : 0;

		if ($change_id) {
			$select = $this->_db->select()
				->from('T_GLOBAL_CHANGES', array('CHANGES_ID'))
				->where('CHANGES_ID = ' . $this->_db->quote($change_id))
				->where("CHANGES_STATUS = 'WA' OR CHANGES_STATUS = 'RR'");
			$result = $this->_db->fetchOne($select);

			if (empty($result)) {
				$this->_redirect('/notification/invalid/index');
			} else {
				$select = $this->_db->select()
					->from(array('SP' => 'TEMP_CUST_SPOBLIGEE'))
					->joinLeft(
						array('B' => 'M_CUSTOMER'),
						'B.CUST_ID = SP.CUST_ID',
						array('CUST_NAME')
					)
					->join(
						array('G' => 'T_GLOBAL_CHANGES'),
						'G.CHANGES_ID = SP.CHANGES_ID',
						array('CHANGES_ID', 'CHANGES_TYPE', 'CREATED', 'CREATED_BY', 'CHANGES_STATUS', 'READ_STATUS', 'MODULE')
					)
					->where('SP.CHANGES_ID = ?', $change_id);
				$resultData = $this->_db->fetchRow($select);
				$this->view->data = $resultData;

				if ($resultData) {
					$cust_id = $resultData['CUST_ID'];
					$changes_type = $resultData['CHANGES_TYPE'];

					$debitedAcctArr = $model->getCustAcctById($cust_id);
					$this->view->debitedAcctArr = $debitedAcctArr;

					if ($this->_request->isPost()) {
						$filters = array('*' => array('StringTrim', 'StripTags'));

						$zf_filter_input = new Zend_Filter_Input($filters, null, $this->_request->getPost());

						$debited_acct 		= $zf_filter_input->debited_acct;
						$pks_number 		= $zf_filter_input->pks_number;
						$pks_file 			= $_FILES['pks_file'];
						$pks_start_date 	= $zf_filter_input->pks_start_date;
						$pks_exp_date 		= $zf_filter_input->pks_exp_date;
						$sharing_fee 		= $zf_filter_input->sharing_fee;

						if (empty($debited_acct)) {
							$errDesc['debited_acct'] = $this->language->_('Fee Sharing Account cannot be empty');
						}
						if (empty($pks_number)) {
							$errDesc['pks_number'] = $this->language->_('Underlying Document cannot be empty');
						}
						if (empty($pks_file['name'])) {
							$errDesc['pks_file'] = $this->language->_('Upload Document cannot be empty');
						}
						if (empty($pks_start_date)) {
							$errDesc['pks_start_date'] = $this->language->_('Facility Start Date cannot be empty');
						}
						if (empty($pks_exp_date)) {
							$errDesc['pks_exp_date'] = $this->language->_('Facility Expired Date cannot be empty');
						}
						if ($sharing_fee > 100) {
							$errDesc['sharing_fee'] = $this->language->_('Fee Sharing maximal 100%');
						}

						$this->view->errDesc 			= $errDesc;
						$this->view->debited_acct 		= $debited_acct;
						$this->view->pks_number 		= $pks_number;
						$this->view->pks_start_date 	= $pks_start_date;
						$this->view->pks_exp_date 		= $pks_exp_date;
						$this->view->sharing_fee 		= $sharing_fee;

						if (empty($errDesc)) {
							$path = UPLOAD_PATH . '/document/submit/';

							$filename_pks = strtolower(uniqid()) . '_' . strtolower(str_replace(' ', '_', $pks_file['name']));
							$path_old_pks = $pks_file['tmp_name'];
							$path_new_pks = $path . $filename_pks;
							move_uploaded_file($path_old_pks, $path_new_pks);

							try {
								$this->_db->beginTransaction();

								$dataInsert = [
									'CUST_ID'			=> $cust_id,
									'SHARING_ACCT'		=> $debited_acct,
									'PKS_NUMBER'		=> $pks_number,
									'PKS_FILE'			=> $filename_pks,
									'PKS_START_DATE'	=> $pks_start_date,
									'PKS_EXP_DATE'		=> $pks_exp_date,
									'SHARING_FEE'		=> $sharing_fee,
									'STATUS'			=> $resultData['STATUS'],
									'FLAG'				=> 1,
									'ENDED_REASON'		=> $resultData['ENDED_REASON'],
									'TERMINATED_FILE'	=> $resultData['TERMINATED_FILE'],
									'LAST_SUGGESTED'	=> new Zend_Db_Expr('now()'),
									'LAST_SUGGESTEDBY'	=> $this->_userIdLogin
								];
								$where1 = ['CUST_ID = ?' => $cust_id];
								$this->_db->update('TEMP_CUST_SPOBLIGEE', $dataInsert, $where1);

								$data2  = [
									'CHANGES_STATUS' => 'WA',
									'READ_STATUS'	=> 2,
									'LASTUPDATED'	=> new Zend_Db_Expr('now()')
								];
								$where2 = ['CHANGES_ID = ?' => $change_id];
								$this->_db->update('T_GLOBAL_CHANGES', $data2, $where2);

								$this->_db->commit();

								Application_Helper_General::writeLog('VSOC', 'View Special Obligee Changes CUST_ID: ' . $cust_id);
								$this->_redirect('/popup/successpopup/index');
							} catch (Exception $error) {
								$this->_db->rollBack();
								echo '<pre>';
								print_r($error->getMessage());
								echo '</pre><br>';
								die;
							}
						}
					} else {
						Application_Helper_General::writeLog('VSOC', 'View Special Obligee Changes');
					}
				}
			}
		}
	}

	public function approveAction()
	{

		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();

		$changes_id = $this->_getParam('changes_id');
		$cust_id   = $this->_getParam('cust_id');

		$select = $this->_db->select()
			->from(array('SP' => 'TEMP_CUST_SPOBLIGEE'))
			->join(
				array('G' => 'T_GLOBAL_CHANGES'),
				'G.CHANGES_ID = SP.CHANGES_ID',
				array('CHANGES_ID', 'CHANGES_TYPE', 'CREATED', 'CREATED_BY', 'CHANGES_STATUS', 'READ_STATUS', 'MODULE')
			)
			->where('SP.CHANGES_ID = ?', $changes_id);
		$result = $this->_db->fetchRow($select);

		if (!$this->view->hasPrivilege("ASOC")) {
			echo json_encode(array('status' => 'failed', "message" => "you don't have privilege"));
			return 0;
		}

		$svcAccount = new Service_Account($result['SHARING_ACCT'], null);
		$resultBalance = $svcAccount->inquiryAccountBalance('AB', TRUE);

		//var_dump($result);die;
		if ($resultBalance['response_desc'] == 'Success') //00 = success
		{
			$valid = true;
		} else {
			$valid = false;
			$err = true;
			$errmsg = 'Service error';
		}

		if ($valid) {
			try {
				$this->_db->beginTransaction();

				if ($result['CHANGES_TYPE'] == 'E') {
					$m_select = $this->_db->select()
						->from(array('SP' => 'M_CUST_SPOBLIGEE'), array('STATUS'))
						->where('SP.CUST_ID = ?', $cust_id);
					$m_result = $this->_db->fetchRow($m_select);

					if ($m_result['STATUS'] == $result['STATUS']) {
						$status = 1;
					} else {
						$status = $result['STATUS'];
					}
				} else {
					$status = 1;
				}

				$data = [
					'CUST_ID'			=> $cust_id,
					'SHARING_ACCT'		=> $result['SHARING_ACCT'],
					'PKS_NUMBER'		=> $result['PKS_NUMBER'],
					'PKS_FILE'			=> $result['PKS_FILE'],
					'PKS_START_DATE'	=> $result['PKS_START_DATE'],
					'PKS_EXP_DATE'		=> $result['PKS_EXP_DATE'],
					'SHARING_FEE'		=> $result['SHARING_FEE'],
					'STATUS'			=> $status,
					'ENDED_REASON'		=> $result['ENDED_REASON'],
					'TERMINATED_FILE'	=> $result['TERMINATED_FILE'],
					'LAST_SUGGESTED'	=> $result['LAST_SUGGESTED'],
					'LAST_SUGGESTEDBY'	=> $result['LAST_SUGGESTEDBY'],
					'LAST_REVIEWED'		=> $result['LAST_REVIEWED'],
					'LAST_REVIEWEDBY'	=> $result['LAST_REVIEWEDBY'],
					'LAST_APPROVED'		=> new Zend_Db_Expr('now()'),
					'LAST_APPROVEDBY'	=> $this->_userIdLogin
				];
				$where1 = ['CUST_ID = ?' => $cust_id];
				// echo '<pre>';
				// print_r($data);
				// echo '</pre><br>';
				// die;

				if ($result['CHANGES_TYPE'] == 'E') {
					$this->_db->update('M_CUST_SPOBLIGEE', $data, $where1);
				} else {
					$this->_db->insert('M_CUST_SPOBLIGEE', $data);
				}

				$this->_db->delete('TEMP_CUST_SPOBLIGEE', $where1);

				$data2  = [
					'CHANGES_STATUS'	=> 'AP',
					'LASTUPDATED'		=> new Zend_Db_Expr('now()')
				];
				$where2 = ['CHANGES_ID = ?' => $changes_id];
				$this->_db->update('T_GLOBAL_CHANGES', $data2, $where2);

				$this->_db->commit();

				Application_Helper_General::writeLog('ASOC', 'Approve Special Obligee : ' . $cust_id);

				echo json_encode(array('status' => 'success'));
			} catch (Exception $error) {
				$message = $error->getMessage();
				echo json_encode(array('status' => 'failed', 'message' => $message));
			}
		}
		if ($err) {
			//$err = true;
			//$errmsg = 'Service not available';

			$this->view->err = true;
			$this->view->errmsg = $errmsg;
		}
	}

	public function rejectAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();

		$changes_id = $this->_getParam('changes_id');
		$cust_id   = $this->_getParam('cust_id');

		try {
			$this->_db->beginTransaction();

			$where1 = ['CUST_ID = ?' => $cust_id];
			$this->_db->delete('TEMP_CUST_SPOBLIGEE', $where1);

			$data2  = [
				'CHANGES_STATUS'	=> 'RJ',
				'LASTUPDATED'		=> new Zend_Db_Expr('now()')
			];
			$where2 = ['CHANGES_ID = ?' => $changes_id];
			$this->_db->update('T_GLOBAL_CHANGES', $data2, $where2);

			$this->_db->commit();

			echo json_encode(array('status' => 'success'));
		} catch (Exception $error) {
			$message = $error->getMessage();
			echo json_encode(array('status' => 'failed', 'message' => $message));
		}
	}
}
