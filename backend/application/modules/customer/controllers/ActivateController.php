<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class customer_ActivateController extends customer_Model_Customer 
{

     public function indexAction() 
     {
  	   $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
        $password = $sessionNamespace->token; 
        $this->view->token = $sessionNamespace->token;


        $AESMYSQL = new Crypt_AESMYSQL();
        $cust_id = strtoupper($AESMYSQL->decrypt($this->_getParam('cust_id'), $password));
        $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;

         $error_remark = null;
    
         if($cust_id)
         {
             $cust_data = $this->getCustomerActIn($cust_id,2);
      
             if($cust_data['CUST_ID'])
             {
               $tempCustomerId = $this->getTempCustomerId($cust_id);
		
               if(!$tempCustomerId)
               {
                  $info = 'Customer ID = '.$cust_data['CUST_ID'].', Customer Name = '.$cust_data['CUST_NAME'];
                  
                  
                  try 
                  {
			         $this->_db->beginTransaction();
			         
			         $cust_data['CUST_STATUS'] = 1;
                     $cust_data['CUST_SUGGESTED']    = new Zend_Db_Expr('now()');
		             $cust_data['CUST_SUGGESTEDBY']  = $this->_userIdLogin;
			        
			         // insert ke T_GLOBAL_CHANGES
			         $change_id = $this->suggestionWaitingApproval('Customer',$info,$this->_changeType['code']['unsuspend'],null,'M_CUSTOMER','TEMP_CUSTOMER',$cust_data['CUST_ID'],$cust_data['CUST_NAME'],$cust_id);
			         $this->insertTempCustomer($change_id,$cust_data);
			        
			         //log CRUD
			         Application_Helper_General::writeLog('CCSP','customer has been Unsuspended, Cust ID : '.$cust_id. ' Cust Name : '.$cust_data['CUST_NAME'].' Change id : '.$change_id);
			         
			         $this->_db->commit();
			         
			         $this->_redirect('/notification/submited/index');
			         //$this->_redirect('/notification/success/index');
		          }
		          catch(Exception $e) 
		          {
				    $this->_db->rollBack();
					$error_remark = $this->getErrorRemark('82');
					Application_Log_GeneralLog::technicalLog($e);
		          }
               }
               else $error_remark = $this->getErrorRemark('03','Customer ID'); 
             }
             else $cust_id = null; 
         }
    
        if(!$cust_id) $error_remark = $this->getErrorRemark('22','Customer ID');
        if($error_remark)
        { 
           $msg = $error_remark; 
           $class = 'F'; 
        }
        else
        {
           $class = 'S';
           $msg = $this->getErrorRemark('00','Customer ID',$cust_id);
        }
    
        
        
        //insert log
        try 
        {
	       $this->_db->beginTransaction();
	       
	       Application_Helper_General::writeLog('CCSP','view Unsuspend customer');
	        
           $this->_db->commit();
	    }
	    catch(Exception $e)
	    {
	       $this->_db->rollBack();
  	       Application_Log_GeneralLog::technicalLog($e);
	    }
    
         $this->_helper->getHelper('FlashMessenger')->addMessage($class);
         $this->_helper->getHelper('FlashMessenger')->addMessage($msg);
         $this->_redirect($this->_backURL);
         
   }
  
  
}








