<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';

class Transactiondailylimit_RepairController extends Application_Main
{
	
    public function indexAction()
	{
		$this->view->nocd = false;
		$changes_id = $this->_getParam('changes_id');
		
		$model = new transactiondailylimit_Model_Transactiondailylimitsetting();
		$select = $model->getTempSetting($changes_id);

		if($select)
		{
			$setting = Application_Helper_Array::listArray($select,'SETTING_ID','SETTING_VALUE');
			foreach($setting as $key=>$value)
			{			
				$this->view->$key = $value;
			}

			// convert to validate
			/*$threshold_rtgs = $this->_getParam('threshold_rtgs');		
			$threshold_rtgs_val =	Application_Helper_General::convertDisplayMoney($threshold_rtgs);
			$this->_setParam('threshold_rtgs',$threshold_rtgs_val);*/

			$purchase_limit_per_trx = $this->_getParam('purchase_limit_per_trx');		
			$purchase_limit_per_trx_val =	Application_Helper_General::convertDisplayMoney($purchase_limit_per_trx);
			$this->_setParam('purchase_limit_per_trx',$purchase_limit_per_trx_val);
			
			$purchase_limit_per_day = $this->_getParam('purchase_limit_per_day');		
			$purchase_limit_per_day_val =	Application_Helper_General::convertDisplayMoney($purchase_limit_per_day);
			$this->_setParam('purchase_limit_per_day',$purchase_limit_per_day_val);
			
			$pb_limit_per_trx = $this->_getParam('pb_limit_per_trx');		
			$pb_limit_per_trx_val =	Application_Helper_General::convertDisplayMoney($pb_limit_per_trx);
			$this->_setParam('pb_limit_per_trx',$pb_limit_per_trx_val);
			
			$pb_limit_per_day = $this->_getParam('pb_limit_per_day');		
			$pb_limit_per_day_val =	Application_Helper_General::convertDisplayMoney($pb_limit_per_day);
			$this->_setParam('pb_limit_per_day',$pb_limit_per_day_val);
			
//			
			$pbusd_limit_per_trx = $this->_getParam('pbusd_limit_per_trx');		
			$pbusd_limit_per_trx_val =	Application_Helper_General::convertDisplayMoney($pbusd_limit_per_trx);
			$this->_setParam('pbusd_limit_per_trx',$pbusd_limit_per_trx_val);
			
			$pbusd_limit_per_day = $this->_getParam('pbusd_limit_per_day');		
			$pbusd_limit_per_day_val =	Application_Helper_General::convertDisplayMoney($pbusd_limit_per_day);
			$this->_setParam('pbusd_limit_per_day_usd',$pbusd_limit_per_day_val);
			
			
			$rtgs_limit_per_trx = $this->_getParam('rtgs_limit_per_trx');		
			$rtgs_limit_per_trx_val =	Application_Helper_General::convertDisplayMoney($rtgs_limit_per_trx);
			$this->_setParam('rtgs_limit_per_trx',$rtgs_limit_per_trx_val);
			
			$rtgs_limit_per_day = $this->_getParam('rtgs_limit_per_day');		
			$rtgs_limit_per_day_val =	Application_Helper_General::convertDisplayMoney($rtgs_limit_per_day);
			$this->_setParam('rtgs_limit_per_day',$rtgs_limit_per_day_val);
			
			$skn_limit_per_trx = $this->_getParam('skn_limit_per_trx');		
			$skn_limit_per_trx_val =	Application_Helper_General::convertDisplayMoney($skn_limit_per_trx);
			$this->_setParam('skn_limit_per_trx',$skn_limit_per_trx_val);
			
			
			$skn_limit_per_day = $this->_getParam('skn_limit_per_day');		
			$skn_limit_per_day_val =	Application_Helper_General::convertDisplayMoney($skn_limit_per_day);
			$this->_setParam('skn_limit_per_day',$skn_limit_per_day_val);
			
			$payment_limit_per_trx = $this->_getParam('payment_limit_per_trx');		
			$payment_limit_per_trx_val =	Application_Helper_General::convertDisplayMoney($payment_limit_per_trx);
			$this->_setParam('payment_limit_per_trx',$payment_limit_per_trx_val);
			
			$payment_limit_per_day = $this->_getParam('payment_limit_per_day');		
			$payment_limit_per_day_val =	Application_Helper_General::convertDisplayMoney($payment_limit_per_day);
			$this->_setParam('payment_limit_per_day',$payment_limit_per_day_val);
			
// 			$remittance_limit_per_month = $this->_getParam('remittance_limit_per_month');
// 			$remittance_limit_per_month_val =	Application_Helper_General::convertDisplayMoney($remittance_limit_per_month);
// 			$this->_setParam('remittance_limit_per_month',$remittance_limit_per_month_val);
			
			
			$domonline_limit_per_trx = $this->_getParam('domonline_limit_per_trx');		
			$domonline_limit_per_trx_val =	Application_Helper_General::convertDisplayMoney($domonline_limit_per_trx);
			$this->_setParam('domonline_limit_per_trx',$domonline_limit_per_trx_val);
			
			$domonline_limit_per_day = $this->_getParam('domonline_limit_per_day');		
			$domonline_limit_per_day_val =	Application_Helper_General::convertDisplayMoney($domonline_limit_per_day);
			$this->_setParam('domonline_limit_per_day',$domonline_limit_per_day_val);
			
			//FILTER
			{
			$filters = array(
							'purchase_limit_per_trx' => array('StringTrim','StripTags'),
							'purchase_limit_per_day' => array('StringTrim','StripTags'),
							'pb_limit_per_trx' => array('StringTrim','StripTags'),
							'pb_limit_per_day' => array('StringTrim','StripTags'),
// 							'remittance_limit_per_month' => array('StringTrim','StripTags'),
							'pbusd_limit_per_trx' => array('StringTrim','StripTags'),
							'pbusd_limit_per_day' => array('StringTrim','StripTags'),
							'rtgs_limit_per_trx' => array('StringTrim','StripTags'),
							'rtgs_limit_per_day' => array('StringTrim','StripTags'),
							'skn_limit_per_trx' => array('StringTrim','StripTags'),
							'skn_limit_per_day' => array('StringTrim','StripTags'),
							'payment_limit_per_trx' => array('StringTrim','StripTags'),
							'payment_limit_per_day' => array('StringTrim','StripTags'),
							'domonline_limit_per_trx' => array('StringTrim','StripTags'),
							'domonline_limit_per_day' => array('StringTrim','StripTags'),
							);
			}
			//VALIDATE
			{
			$validators = array(
									
									'purchase_limit_per_trx' => array('NotEmpty',														
															array('Between', array('min'=>1,'max'=>9999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															($this->language->_('Can not be empty'),
//															$this->language->_('Amount must be greater than 0.00'),
															$this->language->_('Value between 1 - 9999999999.99'),
															$this->language->_('Value between 1 - 9999999999.99'),)),
									'purchase_limit_per_day' => array('NotEmpty',														
															array('Between', array('min'=>1,'max'=>9999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															($this->language->_('Can not be empty'),
//															$this->language->_('Amount must be greater than 0.00'),
															$this->language->_('Value between 1 - 9999999999.99'),
															$this->language->_('Value between 1 - 9999999999.99'),)),
									'remittance_limit_per_month' => array('NotEmpty',
															array('Between', array('min'=>1,'max'=>9999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															($this->language->_('Can not be empty'),
															//$this->language->_('Amount must be greater than 0.00'),
															$this->language->_('Value between 1 - 9999999999.99'),
															$this->language->_('Value between 1 - 9999999999.99'),)),
														
									'pb_limit_per_trx' => array('NotEmpty',														
															array('Between', array('min'=>1,'max'=>9999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															($this->language->_('Can not be empty'),
//															$this->language->_('Amount must be greater than 0.00'),
															$this->language->_('Value between 1 - 9999999999.99'),
															$this->language->_('Value between 1 - 9999999999.99'),)),	
									'pb_limit_per_day' => array('NotEmpty',														
															array('Between', array('min'=>1,'max'=>9999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															($this->language->_('Can not be empty'),
//															$this->language->_('Amount must be greater than 0.00'),
															$this->language->_('Value between 1 - 9999999999.99'),
															$this->language->_('Value between 1 - 9999999999.99'),)),	
									'pbusd_limit_per_trx' => array('NotEmpty',														
															array('Between', array('min'=>1,'max'=>9999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															($this->language->_('Can not be empty'),
//															$this->language->_('Amount must be greater than 0.00'),
															$this->language->_('Value between 1 - 9999999999.99'),
															$this->language->_('Value between 1 - 9999999999.99'),)),	
									'pbusd_limit_per_day' => array('NotEmpty',														
															array('Between', array('min'=>1,'max'=>9999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															($this->language->_('Can not be empty'),
//															$this->language->_('Amount must be greater than 0.00'),
															$this->language->_('Value between 1 - 9999999999.99'),
															$this->language->_('Value between 1 - 9999999999.99'),)),	
																	
																	
									'rtgs_limit_per_trx' => array('NotEmpty',														
															array('Between', array('min'=>1,'max'=>9999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															($this->language->_('Can not be empty'),
//															$this->language->_('Amount must be greater than 0.00'),
															$this->language->_('Value between 1 - 9999999999.99'),
															$this->language->_('Value between 1 - 9999999999.99'),)),	
									'rtgs_limit_per_day' => array('NotEmpty',														
															array('Between', array('min'=>1,'max'=>9999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															($this->language->_('Can not be empty'),
//															$this->language->_('Amount must be greater than 0.00'),
															$this->language->_('Value between 1 - 9999999999.99'),
															$this->language->_('Value between 1 - 9999999999.99'),)),	
																					
									'skn_limit_per_trx' => array('NotEmpty',														
															array('Between', array('min'=>1,'max'=>9999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															($this->language->_('Can not be empty'),
//															$this->language->_('Amount must be greater than 0.00'),
															$this->language->_('Value between 1 - 9999999999.99'),
															$this->language->_('Value between 1 - 9999999999.99'),)),
									'skn_limit_per_day' => array('NotEmpty',														
															array('Between', array('min'=>1,'max'=>9999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															($this->language->_('Can not be empty'),
//															$this->language->_('Amount must be greater than 0.00'),
															$this->language->_('Value between 1 - 9999999999.99'),
															$this->language->_('Value between 1 - 9999999999.99'),)),	
																
									'payment_limit_per_trx' => array('NotEmpty',														
															array('Between', array('min'=>1,'max'=>9999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															($this->language->_('Can not be empty'),
//															$this->language->_('Amount must be greater than 0.00'),
															$this->language->_('Value between 1 - 9999999999.99'),
															$this->language->_('Value between 1 - 9999999999.99'),)),	
																
									'payment_limit_per_day' => array('NotEmpty',														
															array('Between', array('min'=>1,'max'=>9999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															($this->language->_('Can not be empty'),
//															$this->language->_('Amount must be greater than 0.00'),
															$this->language->_('Value between 1 - 9999999999.99'),
															$this->language->_('Value between 1 - 9999999999.99'),)),	
															
									'domonline_limit_per_trx' => array('NotEmpty',														
															array('Between', array('min'=>1,'max'=>9999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															($this->language->_('Can not be empty'),
//															$this->language->_('Amount must be greater than 0.00'),
															$this->language->_('Value between 1 - 9999999999.99'),
															$this->language->_('Value between 1 - 9999999999.99'),)),	
																					
									'domonline_limit_per_day' => array('NotEmpty',														
															array('Between', array('min'=>1,'max'=>9999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															($this->language->_('Can not be empty'),
//															$this->language->_('Amount must be greater than 0.00'),
															$this->language->_('Value between 1 - 9999999999.99'),
															$this->language->_('Value between 1 - 9999999999.99'),)),	
															);
			}
			
			$zf_filter = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

			//validasi multiple email
		     if($this->_getParam('email_exception'))
		     {
				$validate = new validate;
		      	$cek_multiple_email = $validate->isValidEmailMultiple($this->_getParam('email_exception'));
		     }
		     else
		     {
		      	$cek_multiple_email = true;
		     }

			$cek = $model->cekTemp();
			if($cek != null)
			{
				if($this->_request->isPost() && $this->view->hasPrivilege('STTR'))
				{
					$zf_filter = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
					if($zf_filter->isValid() && $cek_multiple_email == true)
					{
						
							$this->_db->beginTransaction();
							try{

								$info = "TRANSACTION DAILY LIMIT";
								//$change_id = $this->suggestionWaitingApproval('Change General Setting',$info,$this->_changeType['code']['edit'],null,'M_SETTING','TEMP_SETTING','MODULE_ID','GNS');
								//$change_id = 1;
								$arrfck = array('isenabled','auto_release_payment','auto_monthlyfee','ftemplate_newuser','femailtemplate_newuser','ftemplate_resetpwd','femailtemplate_resetpwd','btemplate_newuser','bemailtemplate_newuser','btemplate_resetpwd','bemailtemplate_resetpwd');
								//DELETE DATA TEMP
								$model->deleteTemp($changes_id);
										
								foreach($setting as $key=>$value){
									if (in_array($key, $arrfck)) {
										$valuee = $this->_getParam($key);									
										$data = array(	'CHANGES_ID' => $changes_id,
														'SETTING_ID' => $key,
														'SETTING_VALUE' => ($valuee),
														'MODULE_ID' => 'TRX',
														);
									}									
									else
									{
										$data = array(	'CHANGES_ID' => $changes_id,
												'SETTING_ID' => $key,
												'SETTING_VALUE' => ($zf_filter->$key),
												'MODULE_ID' => 'TRX',
												);							
									}
									
									$model->insertTemp($data);						
								}

									$this->_db->commit();
									
									//UPDATE STATUS GLOBAL CHANGES		
									$this->updateGlobalChanges($changes_id,$info);	
									Application_Helper_General::writeLog('STTR','Submiting Repair Transaction Daily Limit Setting');
									$this->view->success = true;						    
									$msg = "Settings Change Request Saved";
									$this->view->report_msg = $msg;						    	    	
							}
							catch(Exception $e)
							{
									$this->_db->rollBack();					
							}
					}
					else{
						$docErr = 'Error in processing form values. Please correct values and re-submit.';
						$this->view->report_msg = $docErr;

						foreach($zf_filter->getMessages() as $key=>$err){
							$xxx = 'x'.$key;
							$this->view->$xxx = $this->displayError($err);
						}
						
						if (isSet($cek_multiple_email) && $cek_multiple_email == false){
							$this->view->xemail_exception = $this->language->_('Invalid email format');							
						}else {
							$this->view->xemail_exception = null;
						}
					}
				}
			}
			else{
				$this->view->error = true;
				//$this->fillParam($zf_filter_input);
				$this->view->report_msg = 'Changes not found';
			}
			
			if($this->_request->isPost()){
				$this->view->isenabled = $this->_getParam('isenabled');
				$this->view->disable_note = $zf_filter->disable_note;
				$this->view->note = $zf_filter->note;
				$this->view->paging_frontend = $zf_filter->paging_frontend;
				$this->view->max_import_bene = $zf_filter->max_import_bene;
				$this->view->max_import_single_payment = $zf_filter->max_import_single_payment;
				$this->view->range_futuredate = $zf_filter->range_futuredate;
				$this->view->archiveafter = $zf_filter->archiveafter;
				
				$this->view->purchase_limit_per_trx = $zf_filter->purchase_limit_per_trx;
				$this->view->pb_limit_per_trx = $zf_filter->pb_limit_per_trx;
				$this->view->rtgs_limit_per_trx = $zf_filter->rtgs_limit_per_trx;
				$this->view->skn_limit_per_trx = $zf_filter->skn_limit_per_trx;
				$this->view->payment_limit_per_trx = $zf_filter->payment_limit_per_trx;
				$this->view->domonline_limit_per_trx = $zf_filter->domonline_limit_per_trx;
			
				$disable_note = $zf_filter->disable_note;
				$note = $zf_filter->note;
			}
			else{
				$disable_note = $setting['disable_note'];
				$note = $setting['note'];
			}
			
			$disable_note_len 	 = (isset($disable_note))  ? strlen($disable_note)  : 0;	
			$note_len 	 = (isset($note))  ? strlen($note)  : 0;			
			
			$disable_note_len 	 = 200 - $disable_note_len;		
			$note_len 	 = 200 - $note_len;	

			$this->view->disable_note_len		= $disable_note_len;
			$this->view->note_len		= $note_len;
		}
		else
		{
			$this->view->error = true;
			//$this->fillParam($zf_filter_input);
			$this->view->report_msg = 'Changes not found';
		}
		Application_Helper_General::writeLog('STTR','Viewing Repair Transaction Daily Limit Setting');
	}
}