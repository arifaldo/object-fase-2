<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/BankUser.php';

class Dashboard_IndexController extends Application_Main {
	/**
	 * The default action - show the home page
	 */
	
	function toIndonesianBulan($tanggal){
		$this->arrBulan = array("January",
				"February",
				"March",
				"April",
				"May",
				"June",
				"July",
				"August",
				"September",
				"October",
				"November",
				"December");	   
		
		$arr  =  explode("-", $tanggal);
		if(is_array($arr)):
// 		print_r($tanggal);die;
		$bulan   = $this->arrBulan[$tanggal-1];
		$tanggal = $bulan;
		endif;
		return $tanggal;
	}
	
	
		
	
	public function indexAction() 
	{
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();

		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}

		$buser_id = $this->_userIdLogin;
		$this->view->buser_id 	= $buser_id;

		$settings =  new Settings();
		
		
		$this->view->month = array(
				1=>"January",
				2=>"February",
				3=>"March",
				4=>"April",
				5=>"May",
				6=>"June",
				7=>"July",
				8=>"August",
				9=>"September",
				10=>"October",
				11=>"November",
				12=>"December");
		
		
		
            
            
		if($this->_request->isPost())
		{
			$errDesc = array();
			
			$filters = array(
					'paymonth'     => array('StripTags','StringTrim'),
								 'year'     => array('StripTags','StringTrim'),
// 								 'confirm_password' => array('StripTags','StringTrim'),
								);

			$zf_filter_input = new Zend_Filter_Input($filters,null,$this->_request->getPost());

			$month 											 = $this->_request->getPost()['paymonth'];
			$year											 = $this->_request->getPost()['YEAR'];
// 			$CON_NEWBUSER_PASSWORD		 = $zf_filter_input->confirm_password;
// 			print_r($this->_request->getPost());
// 			print_r($year);die;	
			
			$errDesc = array();
			
			if(!empty($month) && !empty($year)){
				$this->view->paymonth = $month;
				$this->view->paymonthtext = $this->toIndonesianBulan($month);
				$this->view->YEAR = $year;
				$dailytrx = $this->_db->select()
				->from(array('a'=>'T_TRANSACTION'),array())
				->joinleft(array('b'=>'T_PSLIP'),'b.PS_NUMBER = a.PS_NUMBER',array(
						'TANGGAL'=>'DATE_FORMAT(b.PS_CREATED,"%e")',
						'TOTAL'=>'COUNT(a.`TRANSACTION_ID`)'))
				
						//->join(array('g'=>'M_FGROUP'),'g.FGROUP_ID=u.FGROUP_ID',array('FGROUP_NAME'))
				->where('MONTH(b.PS_CREATED) = ? ',$month)
				->where('YEAR(b.PS_CREATED) = ? ',$year)
				->group('CAST(b.`PS_CREATED` AS DATE)')
				//->where('USER_STATUS!=3');
				->query()->fetchAll();
				
				
				
				$totalvalumetrx = $this->_db->select()
				->from(array('a'=>'T_TRANSACTION'),array())
				->joinleft(array('b'=>'T_PSLIP'),'b.PS_NUMBER = a.PS_NUMBER',array(
						
						'TOTAL'=>'SUM(b.PS_TOTAL_AMOUNT)'))
				
						//->join(array('g'=>'M_FGROUP'),'g.FGROUP_ID=u.FGROUP_ID',array('FGROUP_NAME'))
				->where('MONTH(b.PS_CREATED) = ? ',$month)
				->where('YEAR(b.PS_CREATED) = ? ',$year)
				
				//->where('USER_STATUS!=3');
				->query()->fetchAll();
// 				print_r($totalvalumetrx['0']['TOTAL']);die;
				$this->view->totalvolume = $totalvalumetrx['0']['TOTAL'];
				
				$totaltrx = $this->_db->select()
				->from(array('a'=>'T_TRANSACTION'),array())
				->joinleft(array('b'=>'T_PSLIP'),'b.PS_NUMBER = a.PS_NUMBER',array(
				
						'TOTAL'=>'COUNT(b.PS_TOTAL_AMOUNT)'))
				
						//->join(array('g'=>'M_FGROUP'),'g.FGROUP_ID=u.FGROUP_ID',array('FGROUP_NAME'))
				->where('MONTH(b.PS_CREATED) = ? ',$month)
				->where('YEAR(b.PS_CREATED) = ? ',$year)
				
				//->where('USER_STATUS!=3');
				->query()->fetchAll();
				// 				print_r($totalvalumetrx['0']['TOTAL']);die;
				$this->view->totalcount = $totaltrx['0']['TOTAL'];
				
// 				 ->query();
// 				print_r($dailytrx);die;
				$custtrx = $this->_db->select()
				->from(array('a'=>'T_TRANSACTION'),array())
				->joinleft(array('b'=>'T_PSLIP'),'b.PS_NUMBER = a.PS_NUMBER',array(
						'CUST'=>'b.CUST_ID',
						'TOTAL'=>'COUNT(a.`TRANSACTION_ID`)'))
						 
						//->join(array('g'=>'M_FGROUP'),'g.FGROUP_ID=u.FGROUP_ID',array('FGROUP_NAME'))
				->where('MONTH(b.PS_CREATED) = ? ',$month)
				->where('YEAR(b.PS_CREATED) = ? ',$year)
				->group('b.CUST_ID')
				//->where('USER_STATUS!=3');
				->query()->fetchAll();
				 
				$typetrx = $this->_db->select()
				->from(array('a'=>'T_TRANSACTION'),array())
				->joinleft(array('b'=>'T_PSLIP'),'b.PS_NUMBER = a.PS_NUMBER',array(
						'CATEGORY'=>'b.PS_CATEGORY',
						'TOTAL'=>'COUNT(a.`TRANSACTION_ID`)'))
						 
						//->join(array('g'=>'M_FGROUP'),'g.FGROUP_ID=u.FGROUP_ID',array('FGROUP_NAME'))
				->where('MONTH(b.PS_CREATED) = ? ',$month)
				->where('YEAR(b.PS_CREATED) = ? ',$year)
				->group('b.PS_CATEGORY')
				//->where('USER_STATUS!=3');
				->query()->fetchAll();
				 
				// 		print_r($select);die;
				if(!empty($dailytrx)){
					$string_data = '';
					foreach ($dailytrx as $val) {
				
				
						$temp = '';
						$randomcolor = '#' . strtoupper(dechex(rand(0,10000000)));
				
				
						$string_data .= '{
                                    "country": "'.$val['TANGGAL'].'",'
				                                    		.'"visits": "'.$val['TOTAL'].'",'
				                                    				.'"color": "'.$randomcolor.'"
                                     },';
				
				
					}
				
					$this->view->graphdata = $string_data;
				}
				
				if(!empty($custtrx)){
				
					$total_other = 0;
					$string_data = '';
					foreach ($custtrx  as $key => $val) {
						 
						if($key>='9'){
							$total_other = $total_other + $val['TOTAL'];
						}else{
							$string_data .= '{"country": "'.$val['CUST'].'","litres":'.$val['TOTAL'].'},';
						}
					}
					$string_data .= '{"country": "Other","litres":'.$total_other.'},';
				
					$this->view->custtrx = $string_data;
				}
				
				if(!empty($typetrx)){
				
					$total_other = 0;
					$string_data = '';
					foreach ($typetrx  as $key => $val) {
						 
						if($key>='9'){
							$total_other = $total_other + $val['TOTAL'];
						}else{
							$string_data .= '{"country": "'.$val['CATEGORY'].'","litres":'.$val['TOTAL'].'},';
						}
					}
					$string_data .= '{"country": "Other","litres":'.$total_other.'},';
				
					$this->view->typetrx = $string_data;
				}
				
			}

			$validator = new Zend_Validate_Alnum();

			

			
		}else{
			
			$totalvalumetrx = $this->_db->select()
			->from(array('a'=>'T_TRANSACTION'),array())
			->joinleft(array('b'=>'T_PSLIP'),'b.PS_NUMBER = a.PS_NUMBER',array(
			
					'TOTAL'=>'SUM(b.PS_TOTAL_AMOUNT)'))
			
					//->join(array('g'=>'M_FGROUP'),'g.FGROUP_ID=u.FGROUP_ID',array('FGROUP_NAME'))
			->where('MONTH(b.PS_CREATED) = MONTH(NOW())')
			->where('YEAR(b.PS_CREATED) = YEAR(NOW())')
			
			//->where('USER_STATUS!=3');
			->query()->fetchAll();
			// 				print_r($totalvalumetrx['0']['TOTAL']);die;
			$this->view->totalvolume = $totalvalumetrx['0']['TOTAL'];
			
			$totaltrx = $this->_db->select()
			->from(array('a'=>'T_TRANSACTION'),array())
			->joinleft(array('b'=>'T_PSLIP'),'b.PS_NUMBER = a.PS_NUMBER',array(
			
					'TOTAL'=>'COUNT(b.PS_TOTAL_AMOUNT)'))
			
					//->join(array('g'=>'M_FGROUP'),'g.FGROUP_ID=u.FGROUP_ID',array('FGROUP_NAME'))
			->where('MONTH(b.PS_CREATED) = MONTH(NOW())')
			->where('YEAR(b.PS_CREATED) = YEAR(NOW())')
			
			//->where('USER_STATUS!=3');
			->query()->fetchAll();
			// 				print_r($totalvalumetrx['0']['TOTAL']);die;
			$this->view->totalcount = $totaltrx['0']['TOTAL'];
			
			$dailytrx = $this->_db->select()
			->from(array('a'=>'T_TRANSACTION'),array())
			->joinleft(array('b'=>'T_PSLIP'),'b.PS_NUMBER = a.PS_NUMBER',array(
					'TANGGAL'=>'DATE_FORMAT(b.PS_CREATED,"%e")',
					'TOTAL'=>'COUNT(a.`TRANSACTION_ID`)'))
			
					//->join(array('g'=>'M_FGROUP'),'g.FGROUP_ID=u.FGROUP_ID',array('FGROUP_NAME'))
			->where('MONTH(b.PS_CREATED) = MONTH(NOW())')
			->where('YEAR(b.PS_CREATED) = YEAR(NOW())')
			->group('CAST(b.`PS_CREATED` AS DATE)')
			//->where('USER_STATUS!=3');
			->query()->fetchAll();
			 
			$custtrx = $this->_db->select()
			->from(array('a'=>'T_TRANSACTION'),array())
			->joinleft(array('b'=>'T_PSLIP'),'b.PS_NUMBER = a.PS_NUMBER',array(
					'CUST'=>'b.CUST_ID',
					'TOTAL'=>'COUNT(a.`TRANSACTION_ID`)'))
					 
					//->join(array('g'=>'M_FGROUP'),'g.FGROUP_ID=u.FGROUP_ID',array('FGROUP_NAME'))
			->where('MONTH(b.PS_CREATED) = MONTH(NOW())')
			->where('YEAR(b.PS_CREATED) = YEAR(NOW())')
			->group('b.CUST_ID')
			//->where('USER_STATUS!=3');
			->query()->fetchAll();
			 
			$typetrx = $this->_db->select()
			->from(array('a'=>'T_TRANSACTION'),array())
			->joinleft(array('b'=>'T_PSLIP'),'b.PS_NUMBER = a.PS_NUMBER',array(
					'CATEGORY'=>'b.PS_CATEGORY',
					'TOTAL'=>'COUNT(a.`TRANSACTION_ID`)'))
					 
					//->join(array('g'=>'M_FGROUP'),'g.FGROUP_ID=u.FGROUP_ID',array('FGROUP_NAME'))
			->where('MONTH(b.PS_CREATED) = MONTH(NOW())')
			->where('YEAR(b.PS_CREATED) = YEAR(NOW())')
			->group('b.PS_CATEGORY')
			//->where('USER_STATUS!=3');
			->query()->fetchAll();
			 
			// 		print_r($select);die;
			if(!empty($dailytrx)){
				$string_data = '';
				foreach ($dailytrx as $val) {
			
			
					$temp = '';
					$randomcolor = '#' . strtoupper(dechex(rand(0,10000000)));
			
			
					$string_data .= '{
                                    "country": "'.$val['TANGGAL'].'",'
			                                    		.'"visits": "'.$val['TOTAL'].'",'
			                                    				.'"color": "'.$randomcolor.'"
                                     },';
			
			
				}
			
				$this->view->graphdata = $string_data;
			}
			
			if(!empty($custtrx)){
			
				$total_other = 0;
				$string_data = '';
				foreach ($custtrx  as $key => $val) {
					 
					if($key>='9'){
						$total_other = $total_other + $val['TOTAL'];
					}else{
						$string_data .= '{"country": "'.$val['CUST'].'","litres":'.$val['TOTAL'].'},';
					}
				}
				$string_data .= '{"country": "Other","litres":'.$total_other.'},';
			
				$this->view->custtrx = $string_data;
			}
			
			if(!empty($typetrx)){
			
				$total_other = 0;
				$string_data = '';
				foreach ($typetrx  as $key => $val) {
					 
					if($key>='9'){
						$total_other = $total_other + $val['TOTAL'];
					}else{
						$string_data .= '{"country": "'.$val['CATEGORY'].'","litres":'.$val['TOTAL'].'},';
					}
				}
				$string_data .= '{"country": "Other","litres":'.$total_other.'},';
			
				$this->view->typetrx = $string_data;
			}
			
			
		}
		Application_Helper_General::writeLog('CHOP','Viewing Change My Password');
	}

}

