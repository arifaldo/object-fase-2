<?php
require_once 'Zend/Controller/Action.php';

class Creditcard_IndexController extends Application_Main 
{
	protected $_moduleDB = 'RTF';
	
		
	public function indexAction()
	{
	    //pengaturan url untuk button back
	   	$this->setbackURL('/'.$this->_request->getModuleName().'/index/'); 
	//	Application_Helper_General::writeLog('CCLS','View Customer List');
		
		
		$userid = $this->language->_('User ID'); 
		$username = $this->language->_('User Name');
		$cardno = $this->language->_('Card Number');
		$name = $this->language->_('Name'); 
		$email = $this->language->_('Email Address'); 
		$latestsuggestion_lg = $this->language->_('Latest Suggestion'); 
		$latestsuggester_lg = $this->language->_('Latest Suggester'); 
		$latestapproval_lg = $this->language->_('Latest Approval'); 
		$latestapprover_lg = $this->language->_('Latest Approver'); 
		$action_lg = $this->language->_('Action'); 
		$fields = array	(
							'user_id'  					=> array	(
																		'field' => 'USER_ID',
																		'label' => $userid,
																		'sortable' => true
																	),
							'username'  					=> array	(
																		'field' => 'USER_FULLNAME',
																		'label' => $username,
																		'sortable' => true
																	),
							'card_number'  					=> array	(
																		'field' => 'ACCT_NO',
																		'label' => $cardno,
																		'sortable' => true
																	),
							'card_name' 					=> array	(
																		'field' => 'ACCT_NAME',
																		'label' => $name,
																		'sortable' => true
																	),
							'email' 					=> array	(
																		'field' => 'ACCT_EMAIL',
																		'label' => $email,
																		'sortable' => true
																	),
							'latestSuggestion'     => array('field'    => 'ACCT_SUGGESTED',
																		   'label'    => $latestsuggestion_lg,
																		   'sortable' => true),
							'latestSuggestor'   => array('field'  => 'ACCT_SUGGESTEDBY',
																	   'label'    => $latestsuggester_lg,
																	   'sortable' => true),
							'latestApproval'    => array('field'  => 'ACCT_UPDATED',
																	   'label'    => $latestapproval_lg,
																	   'sortable' => true),
							'latestApprover'  => array('field'   => 'ACCT_UPDATEDBY',
																	   'label'    => $latestapprover_lg,
																	   'sortable' => true),
							'action'   => array('field'    => '',
														  'label'    => $action_lg,
														  'sortable' => false)													  
	                    );
						
		$page = $this->_getParam('page');
	    $page = (Zend_Validate::is($page,'Digits'))? $page : 1;
	    $sortBy = $this->_getParam('sortby');
	    $sortBy = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
	    $sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
	
	    $filterArr = array('filter' => array('StripTags','StringTrim'),
	    				   'username'  => array('StripTags','StringTrim','StringToUpper'),
	                       'cardno'  => array('StripTags','StringTrim'),
	                       'name' => array('StripTags','StringTrim','StringToUpper')
	                      );
	    $zf_filter = new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
	    $filter = $zf_filter->getEscaped('filter');
	    $this->view->currentPage = $page;
	    $this->view->sortBy = $sortBy;
	    $this->view->sortDir = $sortDir;
	    $csv = $this->_getParam('csv');
	    $pdf = $this->_getParam('pdf');

		$select = $this->_db->select()
			->from(array('MUA' => 'M_USER_ACCT'),array('USER_ID' => 'MUA.USER_ID','USER_FULLNAME' => 'MU.USER_FULLNAME','ACCT_NO','ACCT_NAME','ACCT_EMAIL','ACCT_SUGGESTED','ACCT_SUGGESTEDBY','ACCT_UPDATED','ACCT_UPDATEDBY'))
			->joinleft(array('MU' => 'M_USER'), 'MUA.USER_ID = MU.USER_ID',array())
			->where('MUA.ACCT_TYPE = ?', 11);
		
		
	if($filter==TRUE)
    {
		$name = html_entity_decode($zf_filter->getEscaped('name'));
		$username = html_entity_decode($zf_filter->getEscaped('username'));
		$cardno = html_entity_decode($zf_filter->getEscaped('cardno'));
		$latestSuggestionFrom   = html_entity_decode($zf_filter->getEscaped('latestSuggestionFrom'));
		$latestSuggestionTo     = html_entity_decode($zf_filter->getEscaped('latestSuggestionTo'));
		$latestSuggestor        = html_entity_decode($zf_filter->getEscaped('latestSuggestor'));
		$latestApprovalFrom     = html_entity_decode($zf_filter->getEscaped('latestApprovalFrom'));
		$latestApprovalTo       = html_entity_decode($zf_filter->getEscaped('latestApprovalTo'));
		$latestApprover         = html_entity_decode($zf_filter->getEscaped('latestApprover'));

		if($name)
		{
		$this->view->name = $name;
		$select->where("ACCT_NAME LIKE ".$this->_db->quote('%'.$name.'%'));
		}
		if($username)
		{
		$this->view->username = $username;
		$select->where("USER_FULLNAME LIKE ".$this->_db->quote('%'.$username.'%'));
		}
    	if($cardno)
		{
		$this->view->cardno = $cardno;
		$select->where("ACCT_NO LIKE ".$this->_db->quote('%'.$cardno.'%'));
		}
		

	   //konversi date agar dapat dibandingkan
		$latestSuggestionFrom   = (Zend_Date::isDate($latestSuggestionFrom,$this->_dateDisplayFormat))?
								   new Zend_Date($latestSuggestionFrom,$this->_dateDisplayFormat):
								   false;
		
		$latestSuggestionTo     = (Zend_Date::isDate($latestSuggestionTo,$this->_dateDisplayFormat))?
								   new Zend_Date($latestSuggestionTo,$this->_dateDisplayFormat):
								   false;
														   
		$latestApprovalFrom     = (Zend_Date::isDate($latestApprovalFrom,$this->_dateDisplayFormat))?
								   new Zend_Date($latestApprovalFrom,$this->_dateDisplayFormat):
								   false;

		$latestApprovalTo       = (Zend_Date::isDate($latestApprovalTo,$this->_dateDisplayFormat))?
								   new Zend_Date($latestApprovalTo,$this->_dateDisplayFormat):
								   false;	
		if($latestSuggestor)  $select->where('UPPER(ACCT_SUGGESTEDBY) LIKE '.$this->_db->quote('%'.strtoupper($latestSuggestor).'%'));
		if($latestApprover)   $select->where('UPPER(ACCT_UPDATEDBY) LIKE '.$this->_db->quote('%'.strtoupper($latestApprover).'%'));
		if($latestSuggestionFrom)  $select->where("CONVERTSGO('DATE',ACCT_SUGGESTED) >= CONVERTSGO('DATE',".$this->_db->quote($latestSuggestionFrom->toString($this->_dateDBFormat)).")");
		if($latestSuggestionTo)    $select->where("CONVERTSGO('DATE',ACCT_SUGGESTED) <= CONVERTSGO('DATE',".$this->_db->quote($latestSuggestionTo->toString($this->_dateDBFormat)).")");
		if($latestApprovalFrom)    $select->where("CONVERTSGO('DATE',ACCT_UPDATED) >= CONVERTSGO('DATE',".$this->_db->quote($latestApprovalFrom->toString($this->_dateDBFormat)).")");
		if($latestApprovalTo)      $select->where("CONVERTSGO('DATE',ACCT_UPDATED) <= CONVERTSGO('DATE',".$this->_db->quote($latestApprovalTo->toString($this->_dateDBFormat)).")");
		$this->view->latestSuggestor  = $latestSuggestor;
		$this->view->latestApprover   = $latestApprover;
		
		if($latestSuggestionFrom)  $this->view->latestSuggestionFrom = $latestSuggestionFrom->toString($this->_dateDisplayFormat);
		if($latestSuggestionTo)    $this->view->latestSuggestionTo   = $latestSuggestionTo->toString($this->_dateDisplayFormat);
		if($latestApprovalFrom)    $this->view->latestApprovalFrom   = $latestApprovalFrom->toString($this->_dateDisplayFormat);
		if($latestApprovalTo)      $this->view->latestApprovalTo     = $latestApprovalTo->toString($this->_dateDisplayFormat);
		
    }
    $select->group($sortBy);
	$select->order($sortBy.' '.$sortDir);

	if($csv || $pdf || $this->_request->getParam('print'))
	{
		$data = $this->_db->fetchall($select);
		foreach($data as $rowNum => $key)
		{
			$length = strlen($data[$rowNum]['USER_ID']);
			$data[$rowNum]['USER_ID'] = str_repeat("*",$length);
			$data[$rowNum]['ACCT_SUGGESTED'] = Application_Helper_General::convertDate($key['ACCT_SUGGESTED'],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
			$data[$rowNum]['ACCT_UPDATED'] = Application_Helper_General::convertDate($key['ACCT_UPDATED'],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
		}
	}
	
	$header = array('User ID','User Name','Card Number','Name','Email Address','Latest Suggestion','Latest Suggestor','Latest Approval','Latest Approver');

	if($csv)
	{
		$this->_helper->download->csv($header,$data,null,'Credit Card List');  
		//Application_Helper_General::writeLog('ROVW','Importing CSV Customer > Credit Card List');
	}
	else if($pdf)
	{
		$this->_helper->download->pdf($header,$data,null,'Credit Card List');  
		//Application_Helper_General::writeLog('ROVW','Importing PDF Customer > Credit Card List');
	}
	else if($this->_request->getParam('print') == 1){
		unset($fields['action']);
        $this->_forward('print', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'Credit Card List', 'data_header' => $fields));
    }	
	else
	{		
		//Application_Helper_General::writeLog('CSLS','Viewing Customer > Credit Card List');
	}
	$select->group($sortBy);
    $select->order($sortBy.' '.$sortDir);
	
    $this->paging($select);
    $this->view->fields = $fields;
    $this->view->filter = $filter;
  	$this->view->modulename = $this->_request->getModuleName();  

	}
}