<?php
require_once 'Zend/Controller/Action.php';

class Creditcard_DetailController extends Application_Main
{

    public function indexAction() 
    {
		$model 			= new creditcard_Model_Creditcard();
		$ccno = $this->_getParam('ccno');
  	   
		if($ccno)
		{
			$filterArr = array('ccno' => array('StripTags','StringTrim'));
		    $zf_filter = new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());

		    $ccno = html_entity_decode($zf_filter->getEscaped('ccno'));
			$resultdata = $model->getCardDetail($ccno);
			
			if(!empty($resultdata))
			{
				$this->view->user_id      		= strtoupper($resultdata['USER_ID']);
				$this->view->user_fullname     	= $resultdata['USER_FULLNAME'];
				$this->view->acct_no    		= $resultdata['ACCT_NO'];
				$this->view->acct_name    		= $resultdata['ACCT_NAME'];
				$this->view->dob 				= "";
				$this->view->exp_date 			= "";
				$this->view->email    			= $resultdata['ACCT_EMAIL'];;
				$this->view->acct_created    	= Application_Helper_General::convertDate($resultdata['ACCT_CREATED'],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
				$this->view->acct_createdby      = $resultdata['ACCT_CREATEDBY'];
				$this->view->acct_suggested    = Application_Helper_General::convertDate($resultdata['ACCT_SUGGESTED'],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
				$this->view->acct_suggestedby    = $resultdata['ACCT_SUGGESTEDBY'];
				$this->view->acct_updated  		= Application_Helper_General::convertDate($resultdata['ACCT_UPDATED'],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
				$this->view->acct_updatedby  	= $resultdata['ACCT_UPDATEDBY'];
		   }
		   else{
		   	 $this->view->error = $this->language->_("Data not found");
		   }
		}
		else{
			$this->view->error = $this->language->_("Data not found");
		}
		
        //Application_Helper_General::writeLog('ADLS','View Credit Card List');
	}	
}




