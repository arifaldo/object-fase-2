<?php

Class creditcard_Model_Creditcard
{
	protected $_db;

    // constructor
	public function __construct()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
	}

    
	function getCardDetail($cardno)
	{
		$select = $this->_db->fetchRow(
					$this->_db->select()
							->from(array('MUA' => 'M_USER_ACCT'),array('USER_ID' => 'MUA.USER_ID','USER_FULLNAME' => 'MU.USER_FULLNAME','ACCT_NO','ACCT_NAME','ACCT_EMAIL','ACCT_SUGGESTED','ACCT_SUGGESTEDBY','ACCT_UPDATED','ACCT_UPDATEDBY','ACCT_CREATED','ACCT_CREATEDBY'))
							->joinleft(array('MU' => 'M_USER'), 'MUA.USER_ID = MU.USER_ID',array())
							->where('MUA.ACCT_NO = ?', (string)$cardno)
                );
		return $select;
	}
}