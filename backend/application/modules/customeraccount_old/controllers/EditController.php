<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';
require_once 'Crypt/AESMYSQL.php';

class customeraccount_EditController extends customeraccount_Model_Customeraccount 
{
	protected $_idModule = 'RPAC';
	
	public function indexAction() 
	{ 
  		$this->_helper->layout()->setLayout('newlayout');
		//$this->_moduleDB = strtoupper($this->_idModule['user']);
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
        $password = $sessionNamespace->token; 
        $this->view->token = $sessionNamespace->token; 


        $AESMYSQL = new Crypt_AESMYSQL();
        $cust_id = $AESMYSQL->decrypt($this->_getParam('cust_id'), $password);
        $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;

		$acct_no = strtoupper($AESMYSQL->decrypt($this->_getParam('acct_no'), $password));
		$acct_no = (Zend_Validate::is($acct_no,'Alnum') && Zend_Validate::is($acct_no,'StringLength',array('min'=>1,'max'=>25)))? $acct_no : null;
		$error_remark = null;
		
		$this->view->custAcct_msg = array();
		
		//data customer ACCOUNT
        //$resultdata = $this->getTempAcct($cust_id);
		
		$this->view->cust_id = $cust_id;
		$this->view->acct_no = $acct_no;
		if($cust_id)
		{
		  $select = $this->_db->select()
								 ->from('M_CUSTOMER',array('CUST_ID'))
								 ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
								 //->where('UPPER(CUST_STATUS)='.$this->_db->quote(strtoupper($this->_masterStatus['code']['active'])));
								 ->where('CUST_STATUS!=3');
		  $resultCustID = $this->_db->fetchOne($select);

		}
		if(empty($resultCustID))
		{
			  // $error_remark = $this->getErrorRemark('22','Customer ID');
			  //insert log
			  try 
			  {
				$this->_db->beginTransaction();
				if($this->_request->isPost())
				{
				  $action   = strtoupper($this->_changeType['code']['edit']);
				  $fulldesc = Application_Helper_General::displayFullDesc($_POST);
				}
				else
				{
				  $action = strtoupper($this->_actionID['view']);
				  $fulldesc = null;
				}
				// $this->backendLog($action,strtoupper($this->_idModule['user']),null,$fulldesc,$error_remark);
				$this->_db->commit();
			  }
			  catch(Exception $e) 
			  {
				$this->_db->rollBack();
				// SGO_Helper_GeneralLog::technicalLog($e);
			  }
				
			  $this->_helper->getHelper('FlashMessenger')->addMessage('F');
			  $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
			  // $this->_redirect($this->_backURL);
		}
			
			if(!empty($acct_no))
			{
			  $select = $this->_db->select()
									 ->from('M_CUSTOMER_ACCT',array('ACCT_NO'))
									 ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
									 ->where('UPPER(ACCT_NO)='.$this->_db->quote((string)$acct_no))
									 ->where('UPPER(ACCT_STATUS)!=3');
			  $result = $this->_db->fetchOne($select);
			  
				if($result)
				{
					$select = $this->_db->select()
									   ->from('TEMP_CUSTOMER_ACCT',array('TEMP_ID'))
									   ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
									   ->where('UPPER(ACCT_NO)='.$this->_db->quote((string)$acct_no));
					$result = $this->_db->fetchOne($select);
				
					if($result)
					{
						$acct_no= null;
						$error_remark = 'acct no is already suggested';
					}
				}
				else{ $acct_no = null; }
			}
			
			if(empty($acct_no))
			{
				if(!$error_remark)$error_remark = 'acct no is already suggested';
				//insert log
				try 
				{
					if($this->_request->isPost())
					{
						$action   = strtoupper($this->_changeType['code']['edit']);
						$fulldesc = Application_Helper_General::displayFullDesc($_POST);
					}
					else
					{
						$action = strtoupper($this->_actionID['view']);
						$fulldesc = 'CUST_ID:'.$cust_id;
					}
					Application_Helper_General::writeLog('RPAC','Viewing Root Community');
				}
				catch(Exception $e) 
				{
					Application_Helper_General::writeLog('RPAC','Viewing Root Community');
				}
				
			  $this->_helper->getHelper('FlashMessenger')->addMessage('F');
			  $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
			  // $this->_redirect($this->_backURL);
			}
			
			
			if($this->_request->isPost())
			{
			 
			  $filters = array(
							   'acct_alias'         => array('StripTags','StringTrim'),
							   'acct_email'     => array('StripTags','StringTrim')
							  );

			  $validators =  array(
			  						'acct_alias'       => array(),					
								   'acct_email'    => array(
								   							array('StringLength',array('max'=>128)),
								   							'NotEmpty'=> TRUE,
															//new Application_Validate_EmailAddress(),
															'messages' => array(
								   												$this->language->_('Email lenght cannot be more than 128'),
																			   	$this->language->_('Can not be empty'),
																			   //'Invalid email format',
																			   )
															), 
								 );

				  
				 
			  $zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			  
			//validasi multiple email
		      if($zf_filter_input->acct_email)
		      {
				$validate = new validate;
		      	$cek_multiple_email = $validate->isValidEmailMultiple($zf_filter_input->acct_email);
		      }
		      else
		      {
		      	$cek_multiple_email = true;
		      }
			  // print_r($cek_multiple_email);die;
			  if($zf_filter_input->isValid() && $cek_multiple_email)
			  {
				
				$info = 'Cust ID = '.$cust_id.', Customer Account = '.$acct_no;
				
				//mengambil data account dari tabel master
				$acct_data = $this->getTempAcct($acct_no);
				//$acct_data = $this->_acctData;
				
				foreach($validators as $key=>$value)
				{
				  if($zf_filter_input->$key)$acct_data[strtoupper($key)] = $zf_filter_input->$key;
				}
				
				/*if($zf_filter_input->token_serialno){ $user_data['USER_HASTOKEN'] = strtoupper($this->_masterhasStatus['code']['yes']); }
				else{ $user_data['USER_HASTOKEN'] = strtoupper($this->_masterhasStatus['code']['no']); }*/
				
				$acct_data['CUST_ID'] = $cust_id;
				$acct_data['ACCT_NO'] = $acct_no;

				try 
				{
				  $this->_db->beginTransaction();
				  
				  $acct_data['ACCT_SUGGESTED']    = new Zend_Db_Expr('now()');
				  $acct_data['ACCT_SUGGESTEDBY']  = $this->_userIdLogin;
				  $acct_data['ACCT_ALIAS_NAME']  = $zf_filter_input->acct_alias;  
				  //dual control				 

				  $change_id = $this->suggestionWaitingApproval('Bank Account',$info,strtoupper($this->_changeType['code']['edit']),null,'M_CUSTOMER_ACCT','TEMP_CUSTOMER_ACCT',$zf_filter_input->acct_no,$acct_data['ACCT_NAME'],$zf_filter_input->cust_id);
				  // print_r($acct_data);die;
				  $this->insertTempCustomerAcct($change_id,$acct_data);
				  
				  //lgsg edit master tanpa 2 control, ini khusus edit saja
				  //update record customer
			      /*$updateArr['ACCT_EMAIL']        = $acct_data['ACCT_EMAIL']; 
				  $updateArr['ACCT_SUGGESTED']    = new Zend_Db_Expr('now()');
				  $updateArr['ACCT_SUGGESTEDBY']  = $this->_userIdLogin;
				  $updateArr['ACCT_UPDATED']      = new Zend_Db_Expr('now()');
		          $updateArr['ACCT_UPDATEDBY']    = $this->_userIdLogin;
                  $whereArr = array('ACCT_NO = ?'=>(string)$acct_data['ACCT_NO']);
		          $this->_db->update('M_CUSTOMER_ACCT',$updateArr,$whereArr);*/
				  
		          //log CRUD
		          Application_Helper_General::writeLog('ACUD','Bank Account has been Updated (edit email), Acct No : '.$acct_data['ACCT_NO']. ' Acct Name : '.$acct_data['ACCT_NAME']);
				  
				  $this->_db->commit();
				  
			      //$this->setbackURL('/customer/view/index/cust_id/'.$cust_id);
			      //$this->setbackURL('/customeraccount/bankacclist');
			      
			       $this->_redirect('/notification/submited/index');
				  //$this->_redirect('/notification/success/index');
				}
				catch(Exception $e)
				{  
				  // echo $e->getMessage(); die;
				  $this->_db->rollBack();
				  // $error_remark = $this->getErrorRemark('82');
				  // SGO_Helper_GeneralLog::technicalLog($e);
				}
				  
				
				
				// if(isset($error_remark)){ $msg = $error_remark; $class = 'F'; }
				// else
				// {
				  // $class = 'S';
				  // $msg = $this->getErrorRemark('00','User ID',$zf_filter_input->user_id);
				// }
				  
				//insert log
				try 
				{
				  $this->_db->beginTransaction();
				  $fulldesc = Application_Helper_General::displayFullDesc($_POST);
				  $this->backendLog(strtoupper($this->_changeType['code']['edit']),strtoupper($this->_idModule['user']),$zf_filter_input->cust_id.','.$zf_filter_input->user_id,$fulldesc,$error_remark);
				  $this->_db->commit();
				}
				catch(Exception $e) 
				{
				  $this->_db->rollBack();
				  // SGO_Helper_GeneralLog::technicalLog($e);
				}
				  
				$this->_helper->getHelper('FlashMessenger')->addMessage($class);
				$this->_helper->getHelper('FlashMessenger')->addMessage($msg);
				// $this->_redirect($this->_backURL);
			  }
			  else
			  {
				$this->view->error = 1;
				
				$this->view->acct_email = ($zf_filter_input->isValid('acct_email'))? $zf_filter_input->acct_email : $this->_getParam('acct_email');
				$this->view->acct_alias = ($zf_filter_input->isValid('acct_alias'))? $zf_filter_input->acct_alias : $this->_getParam('acct_alias');
				$error = $zf_filter_input->getMessages();
				/*if(count($error))$error_remark = $this->displayErrorRemark($error);
				$this->view->user_msg = $this->displayError($error);*/
				
				//format error utk ditampilkan di view html 
				$errorArray = null;
				foreach($error as $keyRoot => $rowError)
				{
				   foreach($rowError as $errorString)
				   {
					  $errorArray[$keyRoot] = $errorString;
				   }
				}
				
				if(isSet($cek_multiple_email) && $cek_multiple_email == false) $errorArray['acct_email'] = $this->language->_('Invalid format');
				
				$this->view->custAcct_msg  = $errorArray;
				
			  }
			}
			else
			{
				$select = $this->_db->select()
									 ->from('M_CUSTOMER_ACCT',array('ACCT_NO','ACCT_ALIAS_NAME','ACCT_EMAIL','CCY_ID'))
									 ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
									 ->where('UPPER(ACCT_NO)='.$this->_db->quote((string)$acct_no));
				$resultdata = $this->_db->fetchRow($select);
			  
				
				$this->view->acct_email   = $resultdata['ACCT_EMAIL'];
				$this->view->acct_alias   = $resultdata['ACCT_ALIAS_NAME'];
			
				$this->_setParam('ccy_id',$resultdata['CCY_ID']);
				
			}
			
		 
			
			$select = $this->_db->select()
								   ->from('M_CUSTOMER',array('CUST_NAME'))
								   ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id));
			$this->view->cust_name = $this->_db->fetchOne($select);
			
			
			$ccy_id = strtoupper($this->_getParam('ccy_id'));
			$this->view->ccy_id  = $ccy_id;
			
			$this->view->cust_id = $cust_id;
			$this->view->acct_no = $acct_no;
			
			$this->view->modulename = $this->_request->getModuleName();
			
			//insert log
			try 
			{
			  $this->_db->beginTransaction();
			  if(!$this->_request->isPost()){
			  Application_Helper_General::writeLog('ACUD','Viewing Update Bank Account');
			  }
			  $this->_db->commit();
			}
			catch(Exception $e)
			{
			  $this->_db->rollBack();
			  // SGO_Helper_GeneralLog::technicalLog($e);
			}
	}
}