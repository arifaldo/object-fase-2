<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class customeraccount_ViewController extends customeraccount_Model_Customeraccount{

	protected $_idModule = 'ACLS';

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
	    $password = $sessionNamespace->token; 
	    $this->view->token = $sessionNamespace->token;


		$AESMYSQL = new Crypt_AESMYSQL();
		$cust_id = strtoupper($AESMYSQL->decrypt($this->_getParam('cust_id'), $password));
		$cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;
		$acct_no = strtoupper($AESMYSQL->decrypt($this->_getParam('acct_no'), $password));
		$acct_no = (Zend_Validate::is($acct_no,'Alnum') && Zend_Validate::is($acct_no,'StringLength',array('min'=>1,'max'=>25)))? $acct_no : null;

		if(!empty($cust_id) && !empty($acct_no))
		{
			$select = $this->_db->select()
								 ->from('M_CUSTOMER',array('CUST_NAME'))
								 ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id));
			$cust_name = $this->_db->fetchOne($select);
			$this->view->cust_name = $cust_name;

			//$this->view->cust_name = $result['CUST_NAME'];

			$select = $this->_db->select()
								 ->from(array('CA'=>'M_CUSTOMER_ACCT'))
								 ->joinleft(array('g'=>'M_GROUPING'),'g.GROUP_ID=CA.GROUP_ID',array('GROUP_NAME'))
								 ->where('UPPER(CA.CUST_ID)='.$this->_db->quote((string)$cust_id))
								 ->where('UPPER(CA.ACCT_NO)='.$this->_db->quote((string)$acct_no));

			$resultdata = $this->_db->fetchRow($select);

			if(is_array($resultdata))
			{
				if($this->_getParam('suspendUnsuspend') == $this->language->_('Suspend'))
				{
					$this->accountAction($resultdata,2);
				}
				elseif($this->_getParam('suspendUnsuspend') == $this->language->_('Unsuspend'))
				{
					$this->accountAction($resultdata,1);
				}
				elseif($this->_getParam('suspendUnsuspend') == $this->language->_('Delete'))
				{
					$this->accountAction($resultdata,3);
				}

				$this->view->cust_id = $cust_id;
				$this->view->acct_no      = $resultdata['ACCT_NO'];
				$this->view->acct_status  = $resultdata['ACCT_STATUS'];
				$this->view->acct_name    = $resultdata['ACCT_NAME'];
				$this->view->acct_desc    = $resultdata['ACCT_DESC'];

				$this->view->acct_email   = $resultdata['ACCT_EMAIL'];
				$this->view->ccy_id       = $resultdata['CCY_ID'];
				$this->view->order_no     = $resultdata['ORDER_NO'];
				$this->view->group_name   =  $resultdata['GROUP_NAME'];


				$this->view->acct_created      =  $resultdata['ACCT_CREATED'];
				$this->view->acct_createdby    =  $resultdata['ACCT_CREATEDBY'];
				$this->view->acct_suggested    =  $resultdata['ACCT_SUGGESTED'];
				$this->view->acct_suggestedby  =  $resultdata['ACCT_SUGGESTEDBY'];
				$this->view->acct_updated      =  $resultdata['ACCT_UPDATED'];
				$this->view->acct_updatedby    =  $resultdata['ACCT_UPDATEDBY'];

				$select = $this->_db->select()
									   ->from('TEMP_CUSTOMER_ACCT',array('TEMP_ID'))
									   ->where('UPPER(CUST_ID)='.$this->_db->quote($cust_id))
									   ->where('UPPER(ACCT_NO)='.$this->_db->quote($acct_no));

				$result = $this->_db->fetchOne($select);
				if($result){ $temp = 0; }else{ $temp = 1; }
				$this->view->acct_temp = $temp;
			}
			else{
				$this->view->error = true;
				$this->view->report_msg = ' -- No Data Found -- ';
			}
		}
		else
		{
			$this->view->error = true;
		}
		$this->view->change_type = $this->_changeType;
		$this->view->status_type = $this->_masterglobalstatus;
		$this->view->login_type = $this->_masterhasStatus;
		$this->view->modulename = $this->_request->getModuleName();

		//insert log
		//Application_Helper_General::writeLog($this->_idModule,'Viewing Customer > Bank Account List');
		if($acct_no == ''){
			Application_Helper_General::writeLog($this->_idModule,'Viewing Customer > Bank Account List');
		}
		else{
			Application_Helper_General::writeLog($this->_idModule,'View Bank Account Detail [' . $acct_no . ']');
		}


	}
	/*
	* $type => 1 Unsuspending Acc
	* $type => 2 Suspending Acc
	* $type => 3 Deleting Acc
	*/
	function accountAction($acct_data,$type)
	{
	    $cust_id =  $acct_data['CUST_ID'];
		$acct_no =  $acct_data['ACCT_NO'];
		$acct_name =  $acct_data['ACCT_NAME'];
		if(is_array($acct_data))
		{
			if(($type == 2 && $acct_data['ACCT_STATUS'] == 1 || $type == 1 && $acct_data['ACCT_STATUS'] == 2 || $type == 3) && $acct_data['ACCT_STATUS'] != 3)
			{
				$select = $this->_db->select()
											  ->from('TEMP_CUSTOMER_ACCT',array('TEMP_ID'))
											  ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
											  ->where('UPPER(ACCT_NO)='.$this->_db->quote((string)$acct_no));
				$suggestedData = $this->_db->fetchOne($select);

				if(empty($suggestedData))
				{
					$info = 'Cust ID = '.$cust_id.', Customer Account = '.$acct_no;
					$acct_data['ACCT_STATUS'] = $type;

					try
					{
						$this->_db->beginTransaction();
						$acct_data['ACCT_SUGGESTED']    = new Zend_Db_Expr('now()');
						$acct_data['ACCT_SUGGESTEDBY']  = $this->_userIdLogin;

						//insert data + insert log
						$info = "Customer Account";
						if($type == 1)
						{
							$change_id = $this->suggestionWaitingApproval('Bank Account',$info,$this->_changeType['code']['unsuspend'],null,'M_CUSTOMER_ACCT','TEMP_CUSTOMER_ACCT',$acct_no,$acct_name,$cust_id);
							Application_Helper_General::writeLog('ACSP','Unsuspending (Account Number : '.$acct_no.'; Customer ID : '.$cust_id.')');
							// $this->view->success = true;
							// $this->view->report_msg = 'Suspend Request Saved';
						}
						elseif($type == 2)
						{
							$change_id = $this->suggestionWaitingApproval('Bank Account',$info,$this->_changeType['code']['suspend'],null,'M_CUSTOMER_ACCT','TEMP_CUSTOMER_ACCT',$acct_no,$acct_name,$cust_id);
							Application_Helper_General::writeLog('ACSP','Suspending (Account Number : '.$acct_no.'; Customer ID : '.$cust_id.')');
							// $this->view->success = true;
							// $this->view->report_msg = 'Unsuspend Request Saved';
						}
						elseif($type == 3)
						{
							$change_id = $this->suggestionWaitingApproval('Bank Account',$info,$this->_changeType['code']['delete'],null,'M_CUSTOMER_ACCT','TEMP_CUSTOMER_ACCT',$acct_no,$acct_name,$cust_id);
							Application_Helper_General::writeLog('ACUD','Deleting (Account Number : '.$acct_no.'; Customer ID : '.$cust_id.')');
							// $this->view->success = true;
							// $this->view->report_msg = 'Deleting Request Saved';
						}

						//menghilangkan index group name, krn tidak perlu diinsert ke temp
	                    unset($acct_data['GROUP_NAME']);

						//Zend_Debug::dump($acct_data); die;

						$this->insertTempCustomerAcct($change_id,$acct_data);



						$this->_db->commit();
						//$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
						$this->_redirect('/notification/submited/index');
					}
					catch(Exception $e)
					{
					    echo $e->getMessage(); die;
						$this->_db->rollBack();
						// SGO_Helper_GeneralLog::technicalLog($e);
					}
				}
				else
				{
					$this->view->error = true;
					$this->view->report_msg = 'Account already suggested';
				}
			}
			else
			{
				$this->view->error = true;
				$this->view->report_msg = 'Invalid Action';
			}
		}
		else
		{
			$this->view->error = true;
			$this->view->report_msg = 'Account Not Found';
		}
	}
}
