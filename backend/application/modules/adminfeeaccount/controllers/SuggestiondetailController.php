<?php

require_once 'Zend/Controller/Action.php';

class adminfeeaccount_SuggestiondetailController extends Application_Main
{
  public function indexAction() 
  { 
  	$changeid = $this->_getParam('changes_id');
  	
	$select = $this->_db->select()
					        ->from(array('A' => 'T_GLOBAL_CHANGES'),array('*'));
	$select -> where("A.CHANGES_ID = ?", $changeid);
	$select -> where("A.CHANGES_STATUS = 'WA' OR A.CHANGES_STATUS = 'RR'");
	$result = $select->query()->FetchAll();
	$this->view->result=$result;
	$custid = $result[0]['KEY_FIELD'];
	
  	if(!$result)
	{
		$this->_redirect('/notification/invalid/index');
	}
	
	//Zend_Debug::dump($result2);die;
	
	
		$selectprk = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER_ACCT'),array('A.ACCT_NO', 'A.CCY_ID', 'A.ACCT_NAME'));
		$selectprk -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		
		$selectnoprk = $this->_db->select()
					        ->from(array('A' => 'M_BENEFICIARY'),array('ACCT_NO' => 'A.BENEFICIARY_ACCOUNT', 'CCY_ID' => 'A.CURR_CODE', 'ACCT_NAME' => 'A.BENEFICIARY_NAME'));
		$selectnoprk -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		
		$selectnoprk2 = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER_ACCT'),array('A.ACCT_NO', 'A.CCY_ID', 'A.ACCT_NAME'));
		$selectnoprk2 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		$selectnoprk2 -> where("A.ACCT_SOURCE NOT LIKE '1'");
		
		//$selectprk = $selectprk->__toString();
		//$selectnoprk = $selectnoprk->__toString();
		//$selectnoprk2 = $selectnoprk2->__toString();
		
		//$unionquery = $this->_db->select()
								//->union(array($selectprk,$selectnoprk));
		
		$resultunion = $selectprk->query()->FetchAll();
		$this->view->resultaccount = $resultunion;
								
		//$selectunion = $this->_db->select()
		//					->from (($unionquery),array('*'));
		//$resultunion = $selectunion->query()->FetchAll();
		//$this->view->resultaccount = $resultunion;
		//Zend_Debug::dump($resultunion);die;
		
		/*$chargeaccount = $this->_db->select()
								->union(array($selectnoprk2,$selectnoprk));*/
		$resultlist = $selectnoprk2->query()->FetchAll();
		$this->view->resultlist = $resultlist;

		$result2 = $selectprk->query()->FetchAll();
		//Zend_Debug::dump($selectnoprk2);die;
		$this->view->result2 = $result2;			     
		//Zend_Debug::dump($result2);die;
		foreach($result2 as $row)
		{	
			$acctno = $row['ACCT_NO'];
			$select4 = $this->_db->select()->distinct()
			        	->from(array('A' => 'M_ADMFEE_MONTHLY'),array('*'));
			$select4 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
			$select4 -> where("A.ACCT_NO LIKE ".$this->_db->quote($acctno));
			$select4 -> where("MONTHLYFEE_TYPE = '1'");
			$result4 = $this->_db->fetchAll($select4);
			$this->view->result4 = $result4;

			if($result4)
			{
					foreach($result4 as $row2)
					{
   						$idamt = $row2['ACCT_NO'].'amount'.'current';
						$amt = Application_Helper_General::convertDisplayMoney($row2['AMOUNT']);
						$select3 = $this->_db->select()
						        	->from(array('X' => $selectprk),array('*'));
						$select3 -> where("X.ACCT_NO LIKE ".$this->_db->quote($row2['CHARGES_ACCT_NO']));
						$result3 = $this->_db->fetchRow($select3);
						//Zend_Debug::dump($result3);die;
						$cekacct = 'cek'.$acctno.'current';
						$this->view->$idamt = $amt;
						
						if($result3)
						{
							$this->view->$cekacct = $result3['ACCT_NO'].' / '.$result3['ACCT_NAME'].' ('.$result3['CCY_ID'].')';
						}
						else
						{
							$this->view->$cekacct = '-';
						}
			
					}
					$this->view->enable = true;
			}
		}

  		foreach($result2 as $row3)
		{	
			$acctno = $row3['ACCT_NO'];
			$select4 = $this->_db->select()->distinct()
			        	->from(array('A' => 'TEMP_ADMFEE_MONTHLY'),array('*'));
			$select4 -> where("A.CHANGES_ID = ?", $changeid);
			$select4 -> where("A.ACCT_NO LIKE ".$this->_db->quote($acctno));
			$result4 = $this->_db->fetchAll($select4);

			if($result4)
			{
				foreach($result4 as $row5)
				{
   					$idamt = $row5['ACCT_NO'].'amount'.'suggest';
					$amt = Application_Helper_General::convertDisplayMoney($row5['AMOUNT']);
					
					$unioncharge = $this->_db->select()
											->union(array($selectnoprk2,$selectnoprk));
											
					$chargeaccount = $this->_db->select()
											->from(($unioncharge),array('*'));
						
					$select3 = $this->_db->select()
						        	->from(array('X' => $chargeaccount),array('*'));
					$select3 -> where("X.ACCT_NO LIKE ".$this->_db->quote($row5['CHARGES_ACCT_NO']));
					$result3 = $this->_db->fetchRow($select3);
					//Zend_Debug::dump($result4);die;
					
					
					$cekacct = 'cek'.$acctno.'suggest';
					$this->view->$idamt = $amt;
					
					if($result3)
					{
						$this->view->$cekacct = $result3['ACCT_NO'].' / '.$result3['ACCT_NAME'].' ('.$result3['CCY_ID'].')';
					}
					else
					{
						$this->view->$cekacct = '-';
					}
				}
			}
		}

	
    $this->view->typeCode = array_flip($this->_changeType['code']);
    $this->view->typeDesc = $this->_changeType['desc'];
    $this->view->modulename = $this->_request->getModuleName();
	//echo $select2; die;
	$this->view->changes_id = $changeid;
    Application_Helper_General::writeLog('CHCL','View Administration fee account charges changes list ('.$custid.')');
  }
}