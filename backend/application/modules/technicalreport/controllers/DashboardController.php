<?php

require_once 'Zend/Controller/Action.php';

class Technicalreport_DashboardController extends Application_Main{ 
    
    private $cpuLoad;
 
    public function indexAction(){
        $urlAjax = $this->view->url(array(
            'module' => 'financialreport',
            'controller' => 'dashboardnew',
            'action' => 'gettoplargefile',
        ));
        $this->view->urlAjax = $urlAjax;
        $cpuDetail = $this->getCPULoad(true);
        $diskTotal = disk_total_space('/');
        $diskFree = disk_free_space('/');
        $diskUsage = $diskTotal - $diskFree;

        $getDetailMemory = explode("\n", trim(shell_exec('cat /proc/meminfo')));
        
        $this->view->memoryTotal = $getDetailMemory;

        $this->view->cpuSpecDetail = $this->getCPUSpecDetail();
        $this->view->cpuDetaiPercentage = $cpuDetail;
        $diskUsagePercentage = round($diskUsage/$diskTotal*100, 0);
        $this->view->diskTopUsageDetail = shell_exec('df -h');
        
        $this->view->diskUsage = $diskUsagePercentage;
        $this->view->cpuUsage = $this->cpuUsage();
        $this->view->topCPUUsage = $this->topCPUUsage();
        $this->view->memoryUsage = $this->memoryUsage();
        $this->view->memoryUsageDetail = shell_exec('free -h');
        $this->view->topMemUsage = $this->topMemUsage();


        $this->view->diskDetail = array('diskTotal' => $this->formatSize($diskTotal). ' GB',
                                             'diskFree'  => $this->formatSize($diskFree). ' GB',
                                             'diskUsage' => $this->formatSize($diskUsage).' GB');
    }

    private function getCPUSpecDetail(){
        $shellExec = shell_exec("lscpu | egrep 'Model name|Socket|Thread|NUMA|CPU\(s\)'");
        
        return $shellExec;
    }

    public function gettoplargefileAction(){
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $data = shell_exec('du -ah / 2>/dev/null | sort -n -r | head -n 10');
        $arrayData['topLarge'] = $data;
        echo json_encode($arrayData);
    }

    private function getCPULoad($refresh = false){
        if($refresh == true || empty($this->cpuLoad)){
            $this->cpuLoad = shell_exec('top -b -n 1 | head -5');
        }

        return $this->cpuLoad;
    }

    private function cpuUsage(){
        $getLoad = explode("\n", trim($this->getCPULoad()));
        $getPercentageCPU = preg_split("/[\s]+/", $getLoad[2]);
        $cpuLoadTotal = $getPercentageCPU[1] + $getPercentageCPU[3];
        return $cpuLoadTotal;
    }
    
    private function memoryUsage(){
        $exec_free = explode("\n", trim(shell_exec('free')));
        $get_mem = preg_split("/[\s]+/", $exec_free[1]);
        $mem = round($get_mem[2]/$get_mem[1]*100, 0);
        return $mem;
    }
    private function formatSize($bytes){
        $types = array( 'B', 'KB', 'MB', 'GB', 'TB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $types ) -1 ); $bytes /= 1024, $i++ );
                return( round( $bytes, 2 ) );
    }

    private function topCPUUsage(){
        return shell_exec('ps -eo pcpu,pid,user,args | sort -k 1 -r | head -6');
    }

    private function topMemUsage(){
        return shell_exec('ps -o pid,user,%mem,command ax | sort -b -k3 -r | head -6');
    }
}