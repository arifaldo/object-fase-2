<?php

require_once 'Zend/Controller/Action.php';

class Technicalreport_LogController extends Application_Main
{
    protected $_moduleDB = 'FLG';
    protected $_sp;
    protected  $_userStatusCode= array();

    public function init()
    { 
        $this->_dbObj = Zend_Db_Table::getDefaultAdapter();

        //$master = Zend_Registry::get('master');
        //$this->_masterStatus = $master['status'];
        //$this->_userStatusCode = $master['userstatus']['code'];
    }

    
    // Log exception and soap fault here
    public function indexAction()
    {   
        
        //foreach(glob($log_directory.'/*.*') as $file) {
    //  }
    //var_dump(ROOT_PATH);die;
        $this->view->logdir = LIBRARY_PATH .'/data/logs/';
        $listPathLog = array(array('id' => 'app',
                                'name'=>LIBRARY_PATH .'/data/logs/'));
        //$listPathLog = array('app1'=>LIBRARY_PATH .'/data/logs/Backend');
        //$listPathLog = Zend_Registry::get('path')['log'];
        //$this->view->listLog = array_combine(array_values($listPathLog),array_keys($listPathLog));
        $this->view->listLog = $listPathLog;

        //$path_file = LIBRARY_PATH .'/data/logs/';     
         $dir = opendir(ROOT_PATH . '/logs/');
        $this->view->file_array = array();
      //  if($this->_request->isPost()){
            $postData = $this->_request->getParams();
            $logFolder = $postData['logFolder'];
            //var_dump($logFolder);
            //var_dump($listPathLog);
            $isValid = true;
            //$isValid = in_array($logFolder, $listPathLog);
            //var_dump($isValid);die;
            if($isValid){
                if($logFolder[0] != '/'){
                    $logFolder = '/'.$logFolder.'/';
                    $logFolder = ROOT_PATH . $logFolder;
                }
                if(file_exists($logFolder)){
                    $dir = opendir($logFolder);
                    $file_array = array();
                    while(($file = readdir($dir)) !== false){
                        if($file != "." && $file != "..") {
                            $file_array[] = $file;
                        }
                    }
                    closedir($dir);
                    arsort($file_array);
                    $this->view->file_array = $file_array;
                }
            }
            $this->view->logFolder = $postData['logFolder'];
    //    }
        
    }
    
    public function downloadfileAction(){
        $this->_helper->layout()->disableLayout();
        if(empty($this->_request->getParam('file_name'))){
            echo "file_name can't be empty";
            die;
        }

        $fileName = $this->_request->getParam('file_name');
        $fileName = str_replace("..", "", $fileName);
        $fullPath = LIBRARY_PATH .'/data/logs/';
        $fullPathFile = $fullPath . $fileName;
        echo file_get_contents($fullPathFile);
        
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="' . $fileName . '"');
        die;
    }

    
}
