<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/SystemBalance.php';

class services_AccountbalanceController extends services_Model_Services
{

	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');

		$this->view->compName = $this->_custNameLogin;

		$this->view->acct_num = '7001486335';


		if ($this->_getParam('submit')) {
					
			$filters = array(
		        'acct_num' => array('StringTrim','StripTags')
			);

			$validators = array(
	            
				'acct_num' => array(
	            				'NotEmpty',
								'messages' => array(
											    $this->language->_('Account Number Can not be empty'),
	                                          )
							)
			);

			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

			$params = $this->_request->getParams();

			if($zf_filter_input->isValid())
			{	
				$clientUser  =  new SGO_Soap_ClientUser();

				$paramObj = array(
					'account_number' => $zf_filter_input->acct_num
				);

				$success = $clientUser->callapi('pocBalanceInquiry',$paramObj,'poc/inquiry/balance');
			
				if($clientUser->isTimedOut()){
					$returnStruct = array(
							'ResponseCode'	=>'XT',
							'ResponseDesc'	=>'Service Timeout',					
							'Cif'			=>'',
							'AccountList'	=> '',
					);
				}else{
					$result  = $clientUser->getResult();

					$this->view->result = json_encode($result);

					if($result->error_code == '0000'){

						$this->view->balance = $result->account_info->available_credit;
					}
				}
			}
			else{

				$this->view->error = true;

				$error = $zf_filter_input->getMessages();

				$errorArray = null;
		        foreach($error as $keyRoot => $rowError)
		        {
		           foreach($rowError as $errorString)
		           {
		              $errorArray[$keyRoot] = $errorString;
		           }
		        }

                $this->view->error_msg = $errorArray;
			}

			foreach(array_keys($filters) as $field)
				$this->view->$field = $this->_getParam($field);

			// $this->view->source_acct_alias = $params['source_acct_alias'];
			// $this->view->source_acct = $params['source_acct'];
			// $this->view->bank_code = $params['bank_code'];
			// $this->view->travelagent = $params['travelagent'];
			// $this->view->dividedby = $params['dividedby'];
			// $this->view->dividedval = $params['dividedval'];
			// $this->view->warning1 = $params['warning1'];
			// $this->view->warning2 = $params['warning2'];
			// $this->view->remainingDivided = $params['remainingDivided'];
		}
	}

	
}
