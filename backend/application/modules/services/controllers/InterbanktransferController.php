<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/SystemBalance.php';

class services_InterbanktransferController extends services_Model_Services
{

	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');

		$this->view->compName = $this->_custNameLogin;

		$this->view->source_bank_code = '451';
		$this->view->acct_source = '7001325498';
		$this->view->acct_name_source = 'MUHAMMAD CHAEDAR';
		$this->view->bank_code = '008';
		$this->view->acct_dest = '1140005923092';
		$this->view->acct_name_dest = 'ANANDA AULIA FITRIANA';
		$this->view->amount = '10000';

		if ($this->_getParam('submit')) {
					
			$filters = array(
				'source_bank_code'   => array('StringTrim','StripTags'),
		        'acct_source' => array('StringTrim','StripTags'),
		        'acct_name_source' => array('StringTrim','StripTags'),
		        'bank_code'   => array('StringTrim','StripTags'),
		        'acct_dest'   => array('StringTrim','StripTags'),
		        'acct_name_dest' => array('StringTrim','StripTags'),		        		        
		        'amount' 	  => array('StringTrim','StripTags'),
		        'notes' 	  => array('StringTrim','StripTags')
			);

			$validators = array(
	            
				'acct_source' => array(
	            				'NotEmpty',
								'messages' => array(
											    $this->language->_('Source Account Number Can not be empty'),
	                                          )
							),
				'acct_dest'  => array(
								'NotEmpty',
								'messages' => array(
												$this->language->_('Destination Account Number')
											  )
							),
				'source_bank_code' => array(
	            				'NotEmpty',
								'messages' => array(
											    $this->language->_('Source Bank Code Can not be empty'),
	                                          )
							),
				'bank_code'  => array(
								'NotEmpty',
								'messages' => array(
												$this->language->_('Destination Bank Code Number')
											  )
							),
				'acct_name_source' => array(
	            				'NotEmpty',
								'messages' => array(
											    $this->language->_('Source Account Name Can not be empty'),
	                                          )
							),
				'acct_name_dest'  => array(
								'NotEmpty',
								'messages' => array(
												$this->language->_('Destination Account Name Can not be empty')
											  )
							),
				'amount'  	=> array(
								'NotEmpty',
								'messages' => array(
												$this->language->_('Amount Can not be empty')
											  )
							)
			);

			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

			$params = $this->_request->getParams();

			if($zf_filter_input->isValid())
			{	
				$clientUser  =  new SGO_Soap_ClientUser();

				$paramObj = array(
					'source_bank_code'				=> $zf_filter_input->source_bank_code,
					'source_account_number' 		=> $zf_filter_input->acct_source,
					'source_account_name' 			=> $zf_filter_input->acct_name_source,
					'beneficiary_bank_code'			=> $zf_filter_input->bank_code,
					'beneficiary_account_name'		=> $zf_filter_input->acct_name_dest,
					'amount'		=> $zf_filter_input->amount,					
					'description' 	=> $this->_getParam('notes')
				);
				
				$success = $clientUser->callapi('pocTransferInterbank',$paramObj,'poc/transfer/interbank');
			
				if($clientUser->isTimedOut()){
					$returnStruct = array(
							'ResponseCode'	=>'XT',
							'ResponseDesc'	=>'Service Timeout',
					);
				}else{
					$result  = $clientUser->getResult();

					$this->view->result = json_encode($result);

					if($result->error_code == '0000'){

						// $balancearr[$value['AIRLINE_CODE']] = $result->account_info->available_credit;
					}
				}
			}
			else{

				$this->view->error = true;

				$error = $zf_filter_input->getMessages();

				$errorArray = null;
		        foreach($error as $keyRoot => $rowError)
		        {
		           foreach($rowError as $errorString)
		           {
		              $errorArray[$keyRoot] = $errorString;
		           }
		        }

                $this->view->error_msg = $errorArray;
			}

			foreach(array_keys($filters) as $field)
				$this->view->$field = $this->_getParam($field);
		}
	}

	
}
