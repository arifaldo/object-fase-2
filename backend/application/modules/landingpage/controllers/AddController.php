<?php

require_once 'Zend/Controller/Action.php';

class landingpage_AddController extends Application_Main 
{
	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
		$attahmentDestination 	= UPLOAD_PATH . '/document/help/';		
		$errorRemark 			= null;
		$adapter 				= new Zend_File_Transfer_Adapter_Http();
		// print_r($adapter);die;
		if($this->_request->isPost())
		{

			$params = $this->_request->getParams();
			$fileExt 				= "png";


			$sourceFileName = $adapter->getFileName();

				if($sourceFileName == null)
				{
					$sourceFileName = null;
					$fileType = null;
				}
				else
				{
					$sourceFileName = substr(basename($adapter->getFileName()), 0);
					if($_FILES["document"]["type"])
					{
						$adapter->setDestination($attahmentDestination);
						$maxFileSize 			= $this->getSetting('Fe_attachment_maxbyte');
						$fileType = $adapter->getMimeType();
						$size = $_FILES["document"]["size"];
					}
					else
					{
						$fileType = null;
						$size = null;
					}
				}
				$paramsName['sourceFileName'] 	= $sourceFileName;

			
			$filtersName = array(
									'helpTopic'	=> array('StripTags', 'StringTrim'),
									'type'	=> array('StripTags', 'StringTrim'),
									'CROSS_CURR'	=> array('StripTags', 'StringTrim'),
									'postdatetype'	=> array('StripTags', 'StringTrim'),
									'news'	=> array('StripTags', 'StringTrim'),
								);
/*
			$validatorsName = array(					
									'helpTopic'	=> array(),
									'type'	=> array(),
									'CROSS_CURR'	=> array(),
									'postdatetype'	=> array(),
									'news'	=> array(),					
								);*/

			$validatorsName = array(
									'sourceFileName' => array(
															'NotEmpty',
															array('Db_NoRecordExists',array(
																								'table'=>'T_HELP',
																								'field'=>'HELP_FILENAME',
																								'exclude'   => array(
																														'field' => 'HELP_ISDELETED',
																														'value' => '1'
																													)
																							)
																),
															'messages' => array(
																					$this->language->_("Error : File Upload cannot be left blank or File Size too large"),
																					"Error : File ".$sourceFileName.$this->language->_(" Already Exist")
																				)
														),
									'helpTopic' => array(
																'NotEmpty',
																array('StringLength', array('max' => 50)),
																new Zend_Validate_Regex('/^[a-zA-Z 0-9]*$/'),
																array('Db_NoRecordExists',array(
																								'table'=>'M_LANDINGPAGE',
																								'field'=>'TITLE',
																								'exclude'   => array(
																														'field' => 'STATUS',
																														'value' => '4'
																													)
																							)
																		),
																'messages' => array(
																						$this->language->_("Error : Help Topic cannot be left blank"),
																						$this->language->_("Error : Help Topic size more than 50 char"),
																						$this->language->_("Error : Help Topic Format Must Alpha Numeric"),
																						$this->language->_("Error : Help Topic Already Exist")
																					)
															),
									'description' => array('allowEmpty' => TRUE),
									'type' => array('sourceFileName' => array(
															'NotEmpty',

														),
													'messages' => array(
																						$this->language->_("Error : Type cannot be left blank"),
																	)
													),
									'CROSS_CURR'	=> array(),
									'postdatetype'	=> array(),
									'news'	=> array(),	
									'target' => array(),	
								);

			$expdate = Application_Helper_General::convertDate($params['to_date'],$this->_dateDBFormat,$this->defaultDateFormat).' '.$params['to_time'];

			$curDate = date("Y-m-d H:i:s");

			if($params['postdatetype'] == 1){
				$postdate = $curDate;
			}else{
				$postdate = Application_Helper_General::convertDate($params['from_date'],$this->_dateDBFormat,$this->defaultDateFormat).' '.$params['from_time'];
			}

			if ($postdate > $expdate) {
				$status = 2;
			}
			else if ($curDate < $postdate) {
				$status = 3;
			}
			else{
				$status = 1;
			}


			// print_r($paramsName);die;
			$zf_filter_input_name = new Zend_Filter_Input($filtersName, $validatorsName, $params, $this->_optionsValidator);

			if($zf_filter_input_name->isValid())
			{
				// $name = $_FILES['document']['name'];
				// die($name);
				//$fileTypeDis = $adapter->getFileInfo();
				//die($);
				$fileTypeMessage = explode('/',$fileType);
				$fileType =  $fileTypeMessage[1];
				$extensionValidator = new Zend_Validate_File_Extension(array(false, $fileExt));
				$extensionValidator->setMessage("Extension file must be *.png");

				$maxFileSize = "2100000";
				$size = number_format($size);

				$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $maxFileSize));
				$sizeValidator->setMessage("File size is exceeding 2,100,000 byte(s), uploaded file is $size byte(s)");
				
				$adapter->setValidators(array($extensionValidator, $sizeValidator));
				
				if($adapter->isValid())
				{
					$newFileName = 'submit-' . strtr(microtime(), array("." => "-", " " => "-")) . $sourceFileName;
					$adapter->addFilter('Rename', $newFileName);
					/*$fileType = $adapter->getMimeType();*/
					if($adapter->receive())
					{
						// print_r($newFileName);
						// die;
						try
						{
							$this->_db->beginTransaction();
							// print_r($params);die;
							$insertArr = array(
								'IMAGE'		=> $newFileName,
								'TITLE' 	=> $params['helpTopic'],								
								'CREATEDBY'	=> $this->_userIdLogin, 
								'CREATED' 	=> new Zend_Db_Expr('now()'),
								'STATUS'	=> $status,
								'EFDATE'	=> $postdate,
								'EXPDATE'	=> $expdate,
								'DATE_TYPE' => $params['postdatetype'],
							);			

							$select = $this->_db->insert('M_LANDINGPAGE', $insertArr);
							//die($select);
							$this->_db->commit();
							$id = $this->_db->lastInsertId();
							Application_Helper_General::writeLog('HLAD','Add Landing Page. ID: ['.$id.'], File name : ['.$sourceFileName.'], Title :['.$params['helpTopic'].']');
							$this->setbackURL('/landingpage');
							$this->_redirect('/notification/success');

						}
						catch(Exception $e)
						{
							// print_r($e);die;
							$this->_db->rollBack();
							$this->view->errorMsg = array(array($this->language->_('An Error Occured. Please Try Again.')));
						}
					}					
				}
				else
				{
					$this->view->error = true;
					$errors = array($adapter->getMessages());
					$this->view->errorMsg = $errors;
				}
			}
			else
			{
				$this->view->error 				= true;
				$errors 						= $zf_filter_input_name->getMessages();
				
				$this->view->sourceFileNameErr 	= (isset($errors['sourceFileName']))? $errors['sourceFileName'] : null;
				$this->view->helpTopicErr 		= (isset($errors['helpTopic']))? $errors['helpTopic'] : null;
				$this->view->helpTopic 			= $params['helpTopic'];				
			}
		}
		
		
		//$this->view->max_file_size = $maxFileSize;
		//$this->view->file_extension = $fileExt;
		
		//$disable_note_len 	 = (isset($params['description']))  ? strlen($params['description'])  : 0;	
		$note_len 	 = (isset($note))  ? strlen($note)  : 0;			
		
		$disable_note_len 	 = 100; //- $disable_note_len;		
		$note_len 	 = 100 - $note_len;	

		$this->view->disable_note_len	= $disable_note_len;
		$this->view->note_len			= $note_len;
		if(!$this->_request->isPost()){
		Application_Helper_General::writeLog('HLAD','Viewing Add Landing Page');
		}
	}
}
