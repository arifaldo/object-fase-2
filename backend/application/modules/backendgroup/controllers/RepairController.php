<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Settings.php';

class backendgroup_RepairController extends Application_Main
{
	public function indexAction()
	{
		$change_id = $this->_getParam('changes_id');
		$this->view->changeid = $change_id;

		$privil = $this->_db->select()
			->FROM('M_BPRIVILEGE');

		$setting = new Settings();

		$type = $setting->getSetting('system_type');
		if ($type == '1') {
			$privil->where('BPRIVI_MODE IN (0,1)');
		} else if ($type == '2') {
			$privil->where('BPRIVI_MODE IN (0,2)');
		}
		$privil->ORDER('BPRIVI_DESC ASC');

		$privil = $privil->query()->fetchall();

		$filterArr = array(
			'groupid'    => array('StripTags', 'StringTrim', 'StringToUpper'),
			'groupname'  => array('StripTags', 'StringTrim'),
		);

		$cek = $this->_db->select()
			->from(array('TEMP_BGROUP'));
		$cek->where("CHANGES_ID = ?", $change_id);
		$cekresult = $cek->query()->FetchAll();

		$expid = "BGROUP_ID != " . $this->_db->quote($cekresult[0]['BGROUP_ID']);
		$expname = "BGROUP_DESC != " . $this->_db->quote($cekresult[0]['BGROUP_DESC']);
		$repairstatus = $cekresult[0]['BGROUP_STATUS'];
		//die;
		$validators = array(
			'groupid' => array(
				'NotEmpty',
				'Alnum',
				array('StringLength', array('min' => 3, 'max' => 6)),
				array('Db_NoRecordExists', array('table' => 'M_BGROUP', 'field' => 'BGROUP_ID', 'exclude' => $expid)),
				array('Db_NoRecordExists', array('table' => 'TEMP_BGROUP', 'field' => 'BGROUP_ID', 'exclude' => $expid)),
				'messages' => array(
					$this->language->_('Can not be empty'),
					'Must be alphabet or numeric values',
					'Group ID length minimal 3, maximal 6 character',
					'Group ID already in use. Please use another',
					'Group ID already suggested. Please use another'
				)
			),
			'groupname' => array(
				'NotEmpty',
				array('StringLength', array('min' => 1, 'max' => 32)),
				array('Db_NoRecordExists', array('table' => 'M_BGROUP', 'field' => 'BGROUP_DESC', 'exclude' => $expname)),
				array('Db_NoRecordExists', array('table' => 'TEMP_BGROUP', 'field' => 'BGROUP_DESC', 'exclude' => $expname)),
				'messages' => array(
					$this->language->_('Can not be empty'),
					'Template Code length must not be more than 32',
					'Group Name already in use. Please use another',
					'Group Name already suggested. Please use another'
				)
			)
		);

		$zf_filter = new Zend_Filter_Input($filterArr, $validators, $this->_request->getParams());
		$submit = $this->_getParam('submit');
		$reset = $this->_getParam('reset');
		$groupid = $this->_getParam('groupid');
		$groupname = $this->_getParam('groupname');
		$callcenter 	= $this->_getParam('callcenter');
		$re = '%^[a-zA-Z0-9+\- ]*$%';
		$cekspecialchar = preg_match($re, $zf_filter->groupname) ? 1 : 0;
		//Zend_Debug::dump($validators);die;

		if ($submit && $this->view->hasPrivilege('BGRP')) {
			$cekcheckbox = 0;
			foreach ($privil as $row) {
				if ($this->_getParam($row['BPRIVI_ID']) == 1) {
					$cekcheckbox++;
					$this->view->$row['BPRIVI_ID'] = $this->_getParam($row['BPRIVI_ID']);
				}
			}
			//Zend_Debug::dump($cekcheckbox);die;
			if ($cekcheckbox == 0) {
				$this->view->errcheckbox = 'Please Select at least 1 Privilege';
				//die;
			}

			if ($zf_filter->isValid() && $cekcheckbox != 0 && $groupname != $groupid && $cekspecialchar) {
				// Application_Helper_General::writeLog('BGRP', 'Repairing Backend group (' . $groupid . ')');
				$info = "Backend Group";
				$this->updateGlobalChanges($change_id, $info);
				$cekcheckbox = 0;
				$this->_db->beginTransaction();
				try {
					$info = "Backend Group";
					$where = array('CHANGES_ID = ?' => $change_id);
					$this->_db->delete('TEMP_BGROUP', $where);
					$data = array(
						'CHANGES_ID' => $change_id,
						'BGROUP_ID' => $groupid,
						'BGROUP_DESC' => $groupname,
						'BGROUP_CALL' 		=> $callcenter,
						'BGROUP_STATUS' => $repairstatus,
					);

					$this->_db->insert('TEMP_BGROUP', $data);

					foreach ($privil as $row) {
						if ($this->_getParam($row['BPRIVI_ID']) == 1) {
							$select4 = $this->_db->select()
								->from(array('TEMP_BPRIVI_GROUP'));
							$select4->where("CHANGES_ID = ?", $change_id);
							$select4->where("BPRIVI_ID LIKE " . $this->_db->quote($row['BPRIVI_ID']));
							$result4 = $this->_db->fetchRow($select4);

							if ($result4) {
								//Zend_Debug::dump($result4);die;
								$where = array('CHANGES_ID = ?' => $change_id);
								$this->_db->delete('TEMP_BPRIVI_GROUP', $where);
								$cekcheckbox++;
								$data2 = array(
									'CHANGES_ID' => $change_id,
									'BPRIVI_ID' => $row['BPRIVI_ID'],
									'BGROUP_ID' => $groupid
								);
								$this->_db->insert('TEMP_BPRIVI_GROUP', $data2);
							}
							if (!$result4) {
								//Zend_Debug::dump($this->_getParam($row['BPRIVI_ID']));die;
								$data2 = array(
									'CHANGES_ID' => $change_id,
									'BPRIVI_ID' => $row['BPRIVI_ID'],
									'BGROUP_ID' => $groupid
								);
								$this->_db->insert('TEMP_BPRIVI_GROUP', $data2);
							}
						}
					}

					if ($cekcheckbox == 0) {
						$this->_db->rollBack();
					} else {
						$this->_db->commit();
						$this->_redirect('/popup/successpopup/index');
					}
				} catch (Exception $e) {
					$this->_db->rollBack();
				}
			} else {
				if ($groupname && $groupid && $groupname == $groupid) {
					$this->view->xgroupname = "Group ID and Group name must not to be same";
				} else if (!$cekspecialchar) {
					$this->view->xgroupname = 'Must not contain special characters';
					$zf_filter->groupname = '';
				} else {
					foreach ($zf_filter->getMessages() as $key => $err) {
						$xxx = 'x' . $key;
						$this->view->$xxx = $this->displayError($err);
					}
				}
			}
			Application_Helper_General::writeLog('BGRP', 'Repair Backend group (' . $change_id . ')');
		} else {
			//Application_Helper_General::writeLog('BGRP','Viewing Repair Backend group page ('.$change_id.')');
			$select = $this->_db->select()
				->from(array('G' => 'T_GLOBAL_CHANGES'))
				->where('G.CHANGES_ID= ?', $change_id)
				->query()->fetch();

			$this->view->notes = $select["CHANGES_REASON"];

			// Application_Helper_General::writeLog('BGRP', 'Perbaikan Usulan ID : ' . $change_id . '. Suggest Data : ' . $select['DISPLAY_TABLENAME'] . ', Company : ' . $select['COMPANY_CODE'] . ', Data ID : ' . $select['KEY_FIELD'] . '');
			Application_Helper_General::writeLog('BGRP', 'Perbaikan Usulan ID : ' . $change_id . '. Suggest Data : ' . $select['DISPLAY_TABLENAME'] . ', Data ID : ' . $select['KEY_FIELD'] . '');
		}

		if (!$submit || $reset) {
			$select3 = $this->_db->select()
				->from(array('TEMP_BGROUP'));
			$select3->where("CHANGES_ID = ?", $change_id);
			$result3 = $select3->query()->FetchAll();

			$groupid = $result3[0]['BGROUP_ID'];
			$groupname = $result3[0]['BGROUP_DESC'];
			$callcenter = $result3[0]['BGROUP_CALL'];




			//Zend_Debug::dump($result4);die;
		}
		$select4 = $this->_db->select()
			->from(array('TEMP_BPRIVI_GROUP'));
		$select4->where("CHANGES_ID = ?", $change_id);
		$result4 = $select4->query()->FetchAll();

		$arrpriv = array();
		foreach ($privil as $row) {
			foreach ($result4 as $row2) {
				if ($row2['BPRIVI_ID'] == $row['BPRIVI_ID']) {
					$arrpriv[] = $row['BPRIVI_ID'];
					$this->view->$row['BPRIVI_ID'] = '1';
					break;
				}
			}
		}
		$this->view->arrpriv = $arrpriv;
		//
		$this->view->groupid = $groupid;
		$this->view->groupname = $groupname;
		$this->view->callcenter = $callcenter;
		$this->view->privil = $privil;
		$this->view->modulename = $this->_request->getModuleName();
	}
}
