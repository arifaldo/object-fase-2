<?php

require_once 'Zend/Controller/Action.php';

class Companycharges_RemitfeeController extends Application_Main
{

    public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		

		$cust_id = 'GLOBAL';
		$this->view->custid = $cust_id;
		
		$selectccy = $this->_db->select()
             ->from(array('A' => 'M_MINAMT_CCY'), array('CCY_ID'));
            
	    $listccy = $selectccy->query()->fetchAll();
	    $paramccy = array(); 
	    foreach ($listccy as $key => $value) {
	    	$paramccy[$value['CCY_ID']] = $value['CCY_ID'];
	    }
	    // echo '<pre>';
	    // print_r($paramccy);
	    $this->view->dataccy = $paramccy;
		$this->view->listccy = $listccy;

		$sqlQueryHoliday = "
			SELECT 
				CHARGE_ID,
				CCY_ID,
				CHARGE_AMT,
				CHARGE_AMOUNT_CCY,
				CHARGE_PROV_MIN_AMT,
				CHARGE_TYPE,
				CHARGE_PROV_MAX_AMT,
				CHARGE_PCT 
			FROM `M_MINAMT_CCY` A
			LEFT JOIN
			(SELECT * 
				FROM M_CHARGES_REMITTANCE  
				WHERE CHARGE_TYPE = ?
				AND CUST_ID = ?) B
			ON A.`CCY_ID` = B.`CHARGE_CCY`";
			//ORDER BY CHARGE_ID DESC";
		
       $transfee = $this->_db->fetchAll($sqlQueryHoliday,array('3',$cust_id));

       $fullfee = $this->_db->fetchAll($sqlQueryHoliday,array('4',$cust_id));

        $prfee = $this->_db->fetchAll($sqlQueryHoliday,array('5',$cust_id));

        foreach($prfee as $key=>$value)
		{
			// if(!empty($key)){
				$keyw = 'prccy'.$value['CCY_ID'];
				$this->view->$keyw = $value['CHARGE_AMOUNT_CCY'];

				$keymin = 'prmin'.$value['CCY_ID'];
				// print_r($keyamt);
				// echo "<br>";
				$this->view->$keymin = $value['CHARGE_PROV_MIN_AMT'];

				$keymax = 'prmax'.$value['CCY_ID'];
				// print_r($keyamt);
				// echo "<br>";
				$this->view->$keymax = $value['CHARGE_PROV_MAX_AMT'];

				$keypct = 'prpct'.$value['CCY_ID'];
				// print_r($keyamt);
				// echo "<br>";
				$this->view->$keypct = $value['CHARGE_PCT'];
			// }
		}

       foreach($fullfee as $key=>$value)
		{
			// if(!empty($key)){
				$keyw = 'faccy'.$value['CCY_ID'];
				$this->view->$keyw = $value['CHARGE_AMOUNT_CCY'];

				$keyamt = 'faamt'.$value['CCY_ID'];
				// print_r($keyamt);
				// echo "<br>";
				$this->view->$keyamt = $value['CHARGE_AMT'];
			// }
		}
       // echo "<pre>";
       // print_r($transfee);
       foreach($transfee as $key=>$value)
		{
			// if(!empty($key)){
				$keyw = 'tfccy'.$value['CCY_ID'];
				$this->view->$keyw = $value['CHARGE_AMOUNT_CCY'];

				$keyamt = 'tfamt'.$value['CCY_ID'];
				// print_r($keyamt);
				// echo "<br>";
				$this->view->$keyamt = $value['CHARGE_AMT'];
			// }
		}

       $this->view->transfee = $transfee;
 		$cust_name = $this->_custNameLogin;


			


		// var_dump($cust_name);exit();
		// $select = $this->_db->select()
		// 			->from(array('A' => 'M_SETTING'),array('SETTING_ID','SETTING_VALUE'))
		// 			->where('MODULE_ID = ?' , 'GNS')
		// 			->query()->FetchAll();
		// $setting = Application_Helper_Array::listArray($select,'SETTING_ID','SETTING_VALUE');

		// foreach($setting as $key=>$value)
		// {
		// 	if(!empty($key))
		// 		$this->view->$key = $value;
		// }

			//FILTER
			{
			$filters = array(
							// 'isenabled' => array('StringTrim','StripTags'),
							// 'disable_note' => array('StringTrim','StripTags'),
							// 'note' => array('StringTrim','StripTags'),

							
							);
			}

			//VALIDATE
			{
			$validators = array(
								
															);
			}

				$zf_filter = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

				$arrfck = array('isenabled','auto_release_payment','auto_monthlyfee','ftemplate_newuser','femailtemplate_newuser','ftemplate_resetpwd','femailtemplate_resetpwd','btemplate_newuser','bemailtemplate_newuser','btemplate_resetpwd','bemailtemplate_resetpwd');

				


		if($this->_request->isPost()){

			$params = $this->getRequest()->getParams();
			$ccy = $params['ccy'];

			if($zf_filter->isValid() )
			{
				
						
				$info = 'Global Charges';
					$info2 = $this->language->_('Set Charges Monthly per Account');

					$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['new'],null,'M_CHARGES_REMITTANCE','TEMP_CHARGES_REMITTANCE','-','Remittance Fee',$cust_id,$cust_name);
					
				foreach ($params['ccy'] as $val) {
					$type = 'charges_type'.$ccy;
					$type2 = $params[$type];

					$ccytf = 'ccytf'.$val;
					$ccytf2 = $params[$ccytf];

					$tfamount = 'tfamount'.$val;
					$tfamount2 = $params[$tfamount];

					$ccyfa = 'ccyfa'.$val;
					$ccyfa2 = $params[$ccyfa];

					$faamount = 'faamount'.$val;
					$faamount2 = $params[$faamount];

					$ccypf = 'ccypf'.$val;
					$ccypf2 = $params[$ccypf];

					$min = 'min'.$val;
					$min2 = $params[$min];

					$max = 'max'.$val;
					$max2 = $params[$max];

					$pcy = 'pcy'.$val;
					$pcy2 = $params[$pcy];

					

					if ($ccytf2!='-') {
						if($tfamount2 == null)
						{
							$tfamount2 = '0.00';
						}
						$charge_amt =	Application_Helper_General::convertDisplayMoney($tfamount2);
						// $ccytf3 = $ccytf2;
						$info = 'Remittance Charges';
						$info2 = 'Set Edit New Charges';

						 

						$params_insert_3 = array(
							'CHANGES_ID' => $change_id,
							'CUST_ID' => $cust_id,
							'CHARGE_TYPE' => '3',
							'CHARGE_CCY' => $val,
							'CHARGE_AMOUNT_CCY' => $ccytf2,
							'CHARGE_AMT' => $charge_amt
						
						);
						$this->_db->insert('TEMP_CHARGES_REMITTANCE',$params_insert_3);

					}

					if ($ccyfa2!='-') {
						if($faamount2 == null)
						{
							$faamount2 = '0.00';
						}
						$charge_amt =	Application_Helper_General::convertDisplayMoney($faamount2);
						// $ccytf3 = $ccytf2;
						$info = 'Remittance Charges';
						$info2 = 'Set Edit New Charges';

						// $change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['new'],null,'M_CHARGES_REMITTANCE','TEMP_CHARGES_REMITTANCE','-','Full Amount Fee',$cust_id,$cust_name);

						$params_insert_4 = array(
							'CHANGES_ID' => $change_id,
							'CUST_ID' => $cust_id,
							'CHARGE_TYPE' => '4',
							'CHARGE_CCY' => $val,
							'CHARGE_AMOUNT_CCY' => $ccytf2,
							'CHARGE_AMT' => $charge_amt
						
						);
						$this->_db->insert('TEMP_CHARGES_REMITTANCE',$params_insert_4);

					}

					if ($ccypf2!='-') {

						$charge_min =	Application_Helper_General::convertDisplayMoney($min2);
						$charge_max =	Application_Helper_General::convertDisplayMoney($max2);
						// $ccytf3 = $ccytf2;
						$info = 'Remittance Charges';
						$info2 = 'Set Edit New Charges';

						if($pcy2 == null)
						{
							$pcy2 = '0.00';
						}

						// $change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['new'],null,'M_CHARGES_REMITTANCE','TEMP_CHARGES_REMITTANCE','-','Provision Fee',$cust_id,$cust_name);

						$params_insert_5 = array(
										'CHANGES_ID' => $change_id,
										'CUST_ID' => $cust_id,
										'CHARGE_TYPE' => '5',
										'CHARGE_CCY' => $val,
										'CHARGE_AMOUNT_CCY' => $ccypf2,
										'CHARGE_PROV_MIN_AMT' => $charge_min,
										'CHARGE_PROV_MAX_AMT' => $charge_max,
										'CHARGE_PCT' => $pcy2,
						);
						$this->_db->insert('TEMP_CHARGES_REMITTANCE',$params_insert_5);

					}

				}
				
				// $this->setbackURL('/companycharges/monthlyfee/index/custid/GLOBAL');
				$this->_redirect('/notification/submited/index');
				// die('here1'); 
			}
			else{
				$docErr = 'Error in processing form values. Please correct values and re-submit.';
				$this->view->report_msg = $docErr;
				// die('here');
				foreach($zf_filter->getMessages() as $key=>$err){
				$xxx = 'x'.$key;
				// print_r($zf_filter->getMessages());die;
				$this->view->$xxx = $this->displayError($err);
				}
			}
			// var_dump($ccy); exit();

			

			$disable_note = $zf_filter->disable_note;
			$note = $zf_filter->note;
		}
		else{
			$disable_note = $setting['disable_note'];
			$note = $setting['note'];
			Application_Helper_General::writeLog('STLS','View Frontend Global Setting');
		}

		$disable_note_len 	 = (isset($disable_note))  ? strlen($disable_note)  : 0;
		$note_len 	 = (isset($note))  ? strlen($note)  : 0;

		$disable_note_len 	 = 200 - $disable_note_len;
		$note_len 	 = 200 - $note_len;

		$this->view->disable_note_len		= $disable_note_len;
		$this->view->note_len		= $note_len;


	}
}
