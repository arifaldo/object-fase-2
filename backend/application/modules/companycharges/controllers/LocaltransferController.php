<?php

require_once 'Zend/Controller/Action.php';

class Companycharges_LocaltransferController extends Application_Main
{

    public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$select = $this->_db->select()
					->from(array('A' => 'M_SETTING'),array('SETTING_ID','SETTING_VALUE'))
					->where('MODULE_ID = ?' , 'GNS')
					->query()->FetchAll();
		$setting = Application_Helper_Array::listArray($select,'SETTING_ID','SETTING_VALUE');

		foreach($setting as $key=>$value)
		{
			if(!empty($key))
				$this->view->$key = $value;
		}

			//FILTER
			{
			$filters = array(
							'isenabled' => array('StringTrim','StripTags'),
							'disable_note' => array('StringTrim','StripTags'),
							'note' => array('StringTrim','StripTags'),
							);
			}

			//VALIDATE
			{
			$validators = array(
									'isenabled' => array(	'NotEmpty',
															'messages' => array
															('Can not be empty')),

									'disable_note' => array(	'NotEmpty',
															'messages' => array
															('Can not be empty')),
									'note' => array('NotEmpty',
															'messages' => array
															('Can not be empty')),
															);
			}

				$zf_filter = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

				$arrfck = array('isenabled','auto_release_payment','auto_monthlyfee','ftemplate_newuser','femailtemplate_newuser','ftemplate_resetpwd','femailtemplate_resetpwd','btemplate_newuser','bemailtemplate_newuser','btemplate_resetpwd','bemailtemplate_resetpwd');

				$cek = $this->_db->select()
									->FROM (array('B' => 'TEMP_SETTING'),array('MODULE_ID'))
									->WHERE ('MODULE_ID = ?','GNS')
									->query()->FetchAll();
				if($cek == null)
				{
					if($this->_request->isPost() && $this->view->hasPrivilege('STUD'))
					{
						//validasi global admin fee
						if($admin_fee_company > 0 && $admin_fee_account > 0){
							//echo'ga bisa masuk';
							$result_val = false;
							$this->view->errorfee = 'Admin fee per account and admin fee per company can not be filled both, please choose one.';
						}
						else{
							//echo'bisa masuk';
							$result_val = true;
						}

						if($zf_filter->isValid() && $result_val === true)
						{
							$this->_db->beginTransaction();
							try{

								$info = "FRONT END GLOBAL SETTING";
								$change_id = $this->suggestionWaitingApproval('Front End Global Setting',$info,$this->_changeType['code']['edit'],null,'M_SETTING','TEMP_SETTING','','');
								//$change_id = 1;

								foreach($setting as $key=>$value){
										if (in_array($key, $arrfck)) {
											$valuee = $this->_getParam($key);
											$data = array(	'CHANGES_ID' => $change_id,
															'SETTING_ID' => $key,
															'SETTING_VALUE' => ($valuee),
															'MODULE_ID' => 'GNS',
															);
										}
										else
										{
											$data = array(	'CHANGES_ID' => $change_id,
													'SETTING_ID' => $key,
													'SETTING_VALUE' => ($zf_filter->$key),
													'MODULE_ID' => 'GNS',
													);
										}
											$this->_db->insert('TEMP_SETTING',$data);
								}

										Application_Helper_General::writeLog('STUD','Update Front End Global Setting');
										$this->_db->commit();
										$this->view->success = true;
										$msg = "Settings Change Request Saved";
										$this->view->report_msg = $msg;
								}
								catch(Exception $e)
								{
										$this->_db->rollBack();
								}
						}
						else{
							$docErr = 'Error in processing form values. Please correct values and re-submit.';
							$this->view->report_msg = $docErr;

							foreach($zf_filter->getMessages() as $key=>$err){
							$xxx = 'x'.$key;
							$this->view->$xxx = $this->displayError($err);
							}
						}
					}
				}else{
					$this->view->error = true;
					//$this->fillParam($zf_filter_input);
					$this->view->report_msg = 'No changes allowed for this record while awaiting approval for previous change.';
				}


		if($this->_request->isPost()){

			foreach($setting as $key=>$value)
			{
				if (in_array($key, $arrfck)){
					$this->view->$key =   $this->_getParam($key);
				}
				else{
					$this->view->$key = (!empty($zf_filter->$key)) ? $zf_filter->$key : $this->_getParam($key);
				}
			}

			$disable_note = $zf_filter->disable_note;
			$note = $zf_filter->note;
		}
		else{
			$disable_note = $setting['disable_note'];
			$note = $setting['note'];
			Application_Helper_General::writeLog('STLS','View Frontend Global Setting');
		}

		$disable_note_len 	 = (isset($disable_note))  ? strlen($disable_note)  : 0;
		$note_len 	 = (isset($note))  ? strlen($note)  : 0;

		$disable_note_len 	 = 200 - $disable_note_len;
		$note_len 	 = 200 - $note_len;

		$this->view->disable_note_len		= $disable_note_len;
		$this->view->note_len		= $note_len;


	}
}
