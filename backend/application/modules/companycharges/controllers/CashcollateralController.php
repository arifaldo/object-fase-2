<?php

require_once 'Zend/Controller/Action.php';

class Companycharges_CashcollateralController extends Application_Main
{

	public function updateAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$cekCash = $this->_db->select()
			->from("M_CHARGES_BG")
			->query()->fetchAll();

		$cekPerubahan = $this->_db->select()
			->from("TEMP_CHARGES_BG")
			->query()->fetchAll();

		if ($cekPerubahan) {
			return $this->_redirect("/companycharges/cashcollateral/detail");
		}

		$newCekCash = [];
		foreach ($cekCash as $value) {
			$newCekCash[$value["CHARGES_ID"]] = $value;
		}

		$this->view->all_data = $newCekCash;

		Application_Helper_General::writeLog('VCCC', 'View Cash Collateral Charges');

		// $getAlert = $this->_getParam("alert");

		// $this->view->errormsg = $getAlert;

		// if (count($cekCash) > 0) {
		// 	return $this->_redirect("companycharges/cashcollateral/detail");
		// }
	}

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		if (!$this->view->hasPrivilege("VCSC")) {
			return $this->_redirect("/home/dashboard");
		}

		$cekCash = $this->_db->select()
			->from("M_CHARGES_BG")
			->query()->fetchAll();

		$newCekCash = [];
		foreach ($cekCash as $value) {
			$newCekCash[$value["CHARGES_ID"]] = $value;
		}

		$this->view->all_data = $newCekCash;

		$cekPerubahan = $this->_db->select()
			->from("TEMP_CHARGES_BG")
			->query()->fetchAll();

		if ($cekPerubahan) {
			$this->view->pembaruan = 1;
		}

		$getCash = $this->_db->select()
			->from("M_CHARGES_OTHER")
			->where("CUST_ID = ? ", "GLOBAL")
			->where("CHARGES_TYPE = ?", "10")
			->query()->fetchAll();

		$tempCash = $this->_db->select()
			->from("TEMP_CHARGES_OTHER")
			->where("CUST_ID = ? ", "GLOBAL")
			->where("CHARGES_TYPE = ?", "10")
			->query()->fetchAll();

		$this->view->warning_msg = "";

		if (count($getCash) > 0 && count($tempCash) > 0) {
			$this->view->warning_msg = "No Changes allowed for this record while awaiting review and approval";
			$this->view->cannotUpdate = 1;
		}

		Application_Helper_General::writeLog('VCSC', 'View BG Charges');

		// if (count($getCash) == 0) {
		// 	return $this->_redirect("/companycharges/cashcollateral");
		// }

		$this->view->data = $getCash[0];
	}

	public function detailAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		if (!$this->view->hasPrivilege("UCSC")) {
			return $this->_redirect("/home/dashboard");
		}

		$getCash = $this->_db->select()
			->from("M_CHARGES_OTHER")
			->where("CUST_ID = ? ", "GLOBAL")
			->where("CHARGES_TYPE = ?", "10")
			->query()->fetchAll();

		$tempCash = $this->_db->select()
			->from("TEMP_CHARGES_OTHER")
			->where("CUST_ID = ? ", "GLOBAL")
			->where("CHARGES_TYPE = ?", "10")
			->query()->fetchAll();

		if (count($getCash) > 0 && count($tempCash) > 0) {
			return $this->_redirect("/companycharges/cashcollateral/detail");
		}

		if (count($getCash) == 0) {
			return $this->_redirect("/companycharges/cashcollateral");
		}

		$this->view->data = $getCash[0];
		if ($this->_request->isPost()) {
			$getAllParam = $this->_request->getParams();



			$CHARGES_ADM = Application_Helper_General::convertDisplayMoney($getAllParam["bank_administration_fee"]);
			$CHARGES_STAMP = Application_Helper_General::convertDisplayMoney($getAllParam["bank_stamp_fee"]);
			$CHARGES_PCT = Application_Helper_General::convertDisplayMoney($getAllParam["bank_provision_fee"]);
			$error = array();
			if ($CHARGES_ADM == 0) $error1 = array("bank_administration_fee" => "Can not be 0");
			if ($CHARGES_STAMP == 0) $error2 = array("bank_stamp_fee" => "Can not be 0");
			if ($CHARGES_ADM == 0) $error3 = array("bank_provision_fee" => "Can not be 0");

			$error = array_merge($error1, $error2, $error3);

			//format error utk ditampilkan di view html
			foreach ($error as $keyRoot => $rowError) {
				$errID = 'error_' . $keyRoot;
				$this->view->$errID = $rowError;
				$errorFlag = 1;
			}

			if ($errorFlag != 1) {

				$change_id = $this->suggestionWaitingApproval('Cash Collateral', "Cash Collateral", $this->_changeType['code']['new'], null, 'T_CHARGES_OTHER', 'TEMP_CHARGES_OTHER', '-', '-', '-', '-', 'linefacility');

				$insertData = [
					"CHANGES_ID" => $change_id,
					"CHARGES_SUGGESTED" => new Zend_Db_Expr('now()'),
					"CHARGES_SUGGESTEDBY" => $this->_userIdLogin,
					"CHARGES_TYPE" => 10,
					"CHARGES_CCY" => "IDR",
					"CHARGES_AMT" => null,
					"CUST_ID" => "GLOBAL",
					"CHARGES_ADM" => Application_Helper_General::convertDisplayMoney($getAllParam["bank_administration_fee"]),
					"CHARGES_STAMP" => Application_Helper_General::convertDisplayMoney($getAllParam["bank_stamp_fee"]),
					"CHARGES_PCT" => Application_Helper_General::convertDisplayMoney($getAllParam["bank_provision_fee"])
				];

				$this->_db->beginTransaction();
				$this->_db->insert("TEMP_CHARGES_OTHER", $insertData);
				$this->_db->commit();

				$this->setbackURL('/' . $this->_request->getModuleName() . '/cashcollateral/update');
				$this->_redirect('/notification/submited/index');
			}
		}
	}

	public function approveAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();

		$changes_id = $this->_getParam('changes_id');

		$get_global_changes = $this->_db->select()
			->from("T_GLOBAL_CHANGES")
			->where("CHANGES_ID = ?", $changes_id)
			->query()->fetch();

		if (!$this->view->hasPrivilege("ACCC")) {
			header("Content-Type: application/json");
			echo json_encode(array('status' => 'failed', "message" => "you don't have privilege"));
			return 0;
		}

		$this->_db->delete("M_CHARGES_BG");

		$get_temp_biaya = $this->_db->select()
			->from("TEMP_CHARGES_BG")
			->query()->fetchAll();

		foreach ($get_temp_biaya as $key => $value) {
			unset($value["FLAG"]);
			$value["CHARGES_APPROVED"] = new Zend_Db_Expr('now()');
			$value["CHARGES_APPROVEDBY"] = $get_global_changes["CREATED_BY"];
			$this->_db->insert("M_CHARGES_BG", $value);
		}

		$this->_db->delete("TEMP_CHARGES_BG");

		$this->_db->update("T_GLOBAL_CHANGES",  [
			'CHANGES_STATUS'	=> 'AP',
			'LASTUPDATED'		=> new Zend_Db_Expr('now()')
		], [
			"CHANGES_ID = ?" => $changes_id
		]);

		Application_Helper_General::writeLog('ACCC', 'Approve BG Charges');

		header("Content-Type: application/json");
		echo json_encode(array('status' => 'success'));
	}
}
