<?php

require_once 'Zend/Controller/Action.php';

class Companycharges_MonthlyfeeController extends Application_Main
{

    public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$select = $this->_db->select()
					->from(array('A' => 'M_SETTING'),array('SETTING_ID','SETTING_VALUE'))
					->where('MODULE_ID = ?' , 'CHR')
					->query()->FetchAll();
		$setting = Application_Helper_Array::listArray($select,'SETTING_ID','SETTING_VALUE');

		foreach($setting as $key=>$value)
		{
			if(!empty($key))
				$this->view->$key = $value;
		}


		 $selectccy = $this->_db->select()
             ->from(array('A' => 'M_MINAMT_CCY'), array('CCY_ID'));
            
	    $listccy = $selectccy->query()->fetchAll();
	    $paramccy = array();
	    foreach ($listccy as $key => $value) {
	    	$paramccy[$value['CCY_ID']] = $value['CCY_ID'];
	    }
	    // echo '<pre>';
	    // print_r($paramccy);
	    $this->view->dataccy = $paramccy;


		$cust_id = 'GLOBAL';
		$this->view->custid = $cust_id;

		$sqlQueryHoliday = "
			SELECT 
				CHARGE_ID,
				CCY_ID,
				CHARGE_AMT,
				CHARGE_AMOUNT_CCY,
				CHARGE_PROV_MIN_AMT,
				CHARGE_TYPE,
				CHARGE_PROV_MAX_AMT,
				CHARGE_PCT 
			FROM `M_MINAMT_CCY` A
			LEFT JOIN
			(SELECT * 
				FROM M_CHARGES_REMITTANCE  
				WHERE CHARGE_TYPE = ?
				AND CUST_ID = ?) B
			ON A.`CCY_ID` = B.`CHARGE_CCY`";
			//ORDER BY CHARGE_ID DESC";
		
       $transfee = $this->_db->fetchAll($sqlQueryHoliday,array('3',$cust_id));

       $fullfee = $this->_db->fetchAll($sqlQueryHoliday,array('4',$cust_id));

        $prfee = $this->_db->fetchAll($sqlQueryHoliday,array('5',$cust_id));

        foreach($prfee as $key=>$value)
		{
			// if(!empty($key)){
				$keyw = 'prccy'.$value['CCY_ID'];
				$this->view->$keyw = $value['CHARGE_AMOUNT_CCY'];

				$keymin = 'prmin'.$value['CCY_ID'];
				// print_r($keyamt);
				// echo "<br>";
				$this->view->$keymin = $value['CHARGE_PROV_MIN_AMT'];

				$keymax = 'prmax'.$value['CCY_ID'];
				// print_r($keyamt);
				// echo "<br>";
				$this->view->$keymax = $value['CHARGE_PROV_MAX_AMT'];

				$keypct = 'prpct'.$value['CCY_ID'];
				// print_r($keyamt);
				// echo "<br>";
				$this->view->$keypct = $value['CHARGE_PCT'];
			// }
		}

       foreach($fullfee as $key=>$value)
		{
			// if(!empty($key)){
				$keyw = 'faccy'.$value['CCY_ID'];
				$this->view->$keyw = $value['CHARGE_AMOUNT_CCY'];

				$keyamt = 'faamt'.$value['CCY_ID'];
				// print_r($keyamt);
				// echo "<br>";
				$this->view->$keyamt = $value['CHARGE_AMT'];
			// }
		}
       // echo "<pre>";
       // print_r($transfee);
       foreach($transfee as $key=>$value)
		{
			// if(!empty($key)){
				$keyw = 'tfccy'.$value['CCY_ID'];
				$this->view->$keyw = $value['CHARGE_AMOUNT_CCY'];

				$keyamt = 'tfamt'.$value['CCY_ID'];
				// print_r($keyamt);
				// echo "<br>";
				$this->view->$keyamt = $value['CHARGE_AMT'];
			// }
		}

       $this->view->transfee = $transfee;
 		$cust_name = $this->_custNameLogin;


			$charges_monthly_aud = $this->_getParam('charges_monthly_aud');
			$charges_monthly_aud_val =	Application_Helper_General::convertDisplayMoney($charges_monthly_aud);
			$this->_setParam('charges_monthly_aud',$charges_monthly_aud_val);

			$charges_monthly_eur = $this->_getParam('charges_monthly_eur');
			$charges_monthly_eur_val =	Application_Helper_General::convertDisplayMoney($charges_monthly_eur);
			$this->_setParam('charges_monthly_eur',$charges_monthly_eur_val);

			$charges_monthly_idr = $this->_getParam('charges_monthly_idr');
			$charges_monthly_idr_val =	Application_Helper_General::convertDisplayMoney($charges_monthly_idr);
			$this->_setParam('charges_monthly_idr',$charges_monthly_idr_val);

			$charges_monthly_sgd = $this->_getParam('charges_monthly_sgd');
			$charges_monthly_sgd_val =	Application_Helper_General::convertDisplayMoney($charges_monthly_sgd);
			$this->_setParam('charges_monthly_sgd',$charges_monthly_sgd_val);

			$charges_monthly_usd = $this->_getParam('charges_monthly_usd');
			$charges_monthly_usd_val =	Application_Helper_General::convertDisplayMoney($charges_monthly_usd);
			$this->_setParam('charges_monthly_usd',$charges_monthly_usd_val);

			$realtime_charges_ihhouse = $this->_getParam('realtime_charges_ihhouse');
			$realtime_charges_ihhouse_val =	Application_Helper_General::convertDisplayMoney($realtime_charges_ihhouse);
			$this->_setParam('realtime_charges_ihhouse',$realtime_charges_ihhouse_val);


			$realtime_charges_skn = $this->_getParam('realtime_charges_skn');
			$realtime_charges_skn_val =	Application_Helper_General::convertDisplayMoney($realtime_charges_skn);
			$this->_setParam('realtime_charges_skn',$realtime_charges_skn_val);

			$realtime_charges_rtgs = $this->_getParam('realtime_charges_rtgs');
			$realtime_charges_rtgs_val =	Application_Helper_General::convertDisplayMoney($realtime_charges_rtgs);
			$this->_setParam('realtime_charges_rtgs',$realtime_charges_rtgs_val);

			$realtime_charges_dom = $this->_getParam('realtime_charges_dom');
			$realtime_charges_dom_val =	Application_Helper_General::convertDisplayMoney($realtime_charges_dom);
			$this->_setParam('realtime_charges_dom',$realtime_charges_dom_val);

			$direct_charges_inhouse_idr = $this->_getParam('direct_charges_inhouse_idr');
			$direct_charges_inhouse_idr_val =	Application_Helper_General::convertDisplayMoney($direct_charges_inhouse_idr);
			$this->_setParam('direct_charges_inhouse_idr',$direct_charges_inhouse_idr_val);

			$direct_charges_dom_idr = $this->_getParam('direct_charges_dom_idr');
			$direct_charges_dom_idr_val =	Application_Helper_General::convertDisplayMoney($direct_charges_dom_idr);
			$this->_setParam('direct_charges_dom_idr',$direct_charges_dom_idr_val);


		// var_dump($cust_name);exit();
		// $select = $this->_db->select()
		// 			->from(array('A' => 'M_SETTING'),array('SETTING_ID','SETTING_VALUE'))
		// 			->where('MODULE_ID = ?' , 'GNS')
		// 			->query()->FetchAll();
		// $setting = Application_Helper_Array::listArray($select,'SETTING_ID','SETTING_VALUE');

		// foreach($setting as $key=>$value)
		// {
		// 	if(!empty($key))
		// 		$this->view->$key = $value;
		// }

			//FILTER
			{
			$filters = array(
							// 'isenabled' => array('StringTrim','StripTags'),
							// 'disable_note' => array('StringTrim','StripTags'),
							// 'note' => array('StringTrim','StripTags'),

							'charges_monthly_aud' => array('StringTrim','StripTags'),
							'charges_monthly_eur' => array('StringTrim','StripTags'),
							'charges_monthly_idr' => array('StringTrim','StripTags'),
							'charges_monthly_sgd' => array('StringTrim','StripTags'),
            			    'charges_monthly_usd' => array('StringTrim','StripTags'),
            			    'realtime_charges_ihhouse' => array('StringTrim','StripTags'),
            			    'realtime_charges_skn' => array('StringTrim','StripTags'),
							'realtime_charges_rtgs' => array('StringTrim','StripTags'),
							'realtime_charges_dom' => array('StringTrim','StripTags'),
            			    'direct_charges_inhouse_idr' => array('StringTrim','StripTags'),
            			    'direct_charges_dom_idr' => array('StringTrim','StripTags'),
							);
			}

			//VALIDATE
			{
			$validators = array(
									// 'isenabled' => array(	'NotEmpty',
									// 						'messages' => array
									// 						('Can not be empty')),

									// 'disable_note' => array(	'NotEmpty',
									// 						'messages' => array
									// 						('Can not be empty')),
									// 'note' => array('NotEmpty',
									// 						'messages' => array
									// 						('Can not be empty')),
									'charges_monthly_aud' => array('NotEmpty',
															array('Between', array('min'=>1,'max'=>9999999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															($this->language->_('Can not be empty'),
															$this->language->_('Amount must be greater than 0.00'),
															$this->language->_('Value between 1 - 9999999999999.99'),
															$this->language->_('Value between 1 - 9999999999999.99'),	)),
									'charges_monthly_eur' => array('NotEmpty',
															array('Between', array('min'=>1,'max'=>9999999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															($this->language->_('Can not be empty'),
															$this->language->_('Amount must be greater than 0.00'),
															$this->language->_('Value between 1 - 9999999999999.99'),
															$this->language->_('Value between 1 - 9999999999999.99'),	)),
									'charges_monthly_idr' => array('NotEmpty',
															array('Between', array('min'=>1,'max'=>9999999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															($this->language->_('Can not be empty'),
															$this->language->_('Amount must be greater than 0.00'),
															$this->language->_('Value between 1 - 9999999999999.99'),
															$this->language->_('Value between 1 - 9999999999999.99'),	)),
									'charges_monthly_sgd' => array('NotEmpty',
															array('Between', array('min'=>1,'max'=>9999999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															($this->language->_('Can not be empty'),
															$this->language->_('Amount must be greater than 0.00'),
															$this->language->_('Value between 1 - 9999999999999.99'),
															$this->language->_('Value between 1 - 9999999999999.99'),	)),
									'charges_monthly_usd' => array('NotEmpty',
															array('Between', array('min'=>1,'max'=>9999999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															($this->language->_('Can not be empty'),
															$this->language->_('Amount must be greater than 0.00'),
															$this->language->_('Value between 1 - 9999999999999.99'),
															$this->language->_('Value between 1 - 9999999999999.99'),	)),
									'realtime_charges_ihhouse' => array('NotEmpty',
															array('Between', array('min'=>1,'max'=>9999999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															($this->language->_('Can not be empty'),
															$this->language->_('Amount must be greater than 0.00'),
															$this->language->_('Value between 1 - 9999999999999.99'),
															$this->language->_('Value between 1 - 9999999999999.99'),	)),
									'realtime_charges_skn' => array('NotEmpty',
															array('Between', array('min'=>1,'max'=>9999999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															($this->language->_('Can not be empty'),
															$this->language->_('Amount must be greater than 0.00'),
															$this->language->_('Value between 1 - 9999999999999.99'),
															$this->language->_('Value between 1 - 9999999999999.99'),	)),
									'realtime_charges_rtgs' => array('NotEmpty',
															array('Between', array('min'=>1,'max'=>9999999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															($this->language->_('Can not be empty'),
															$this->language->_('Amount must be greater than 0.00'),
															$this->language->_('Value between 1 - 9999999999999.99'),
															$this->language->_('Value between 1 - 9999999999999.99'),	)),
									'realtime_charges_dom' => array('NotEmpty',
															array('Between', array('min'=>1,'max'=>9999999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															($this->language->_('Can not be empty'),
															$this->language->_('Amount must be greater than 0.00'),
															$this->language->_('Value between 1 - 9999999999999.99'),
															$this->language->_('Value between 1 - 9999999999999.99'),	)),
									'direct_charges_inhouse_idr' => array('NotEmpty',
															array('Between', array('min'=>1,'max'=>9999999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															($this->language->_('Can not be empty'),
															$this->language->_('Amount must be greater than 0.00'),
															$this->language->_('Value between 1 - 9999999999999.99'),
															$this->language->_('Value between 1 - 9999999999999.99'),	)),
									'direct_charges_dom_idr' => array('NotEmpty',
															array('Between', array('min'=>1,'max'=>9999999999999.99)),
															new Zend_Validate_Regex('/^([1-9]{0,1})([0-9]{1,13})(\\.[0-9]{0,2})?$/'),
															'messages' => array
															($this->language->_('Can not be empty'),
															$this->language->_('Amount must be greater than 0.00'),
															$this->language->_('Value between 1 - 9999999999999.99'),
															$this->language->_('Value between 1 - 9999999999999.99'),	)),
															);
			}

				$zf_filter = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

				$arrfck = array('isenabled','auto_release_payment','auto_monthlyfee','ftemplate_newuser','femailtemplate_newuser','ftemplate_resetpwd','femailtemplate_resetpwd','btemplate_newuser','bemailtemplate_newuser','btemplate_resetpwd','bemailtemplate_resetpwd');

				$cek = $this->_db->select()
									->FROM (array('B' => 'TEMP_SETTING'),array('MODULE_ID'))
									->WHERE ('MODULE_ID = ?','CHR')
									->query()->FetchAll();
									// print_r($cek);
				if($cek == null)
				{
					
				}else{
					$this->view->error = true;
					//$this->fillParam($zf_filter_input);
					$this->view->report_msg = 'No changes allowed for this record while awaiting approval for previous change.';
				}


		if($this->_request->isPost()){

			$params = $this->getRequest()->getParams();
			$ccy = $params['ccy'];

			if($zf_filter->isValid() )
			{
				$this->_db->beginTransaction();

				$info = "FRONT END GLOBAL CHARGES SETTING";
				$change_id = $this->suggestionWaitingApproval('Global Charges Setting',$info,$this->_changeType['code']['edit'],null,'M_SETTING','TEMP_SETTING','','');
				try{
					
					//$change_id = 1;

					foreach($setting as $key=>$value){
							if (in_array($key, $arrfck)) {
								$valuee = $this->_getParam($key);
								$data = array(	'CHANGES_ID' => $change_id,
												'SETTING_ID' => $key,
												'SETTING_VALUE' => ($valuee),
												'MODULE_ID' => 'CHR',
												);
								// print_r($data);die;
							}
							else
							{
								$data = array(	'CHANGES_ID' => $change_id,
										'SETTING_ID' => $key,
										'SETTING_VALUE' => ($zf_filter->$key),
										'MODULE_ID' => 'CHR',
										);
							}
								$this->_db->insert('TEMP_SETTING',$data);
					}

							Application_Helper_General::writeLog('STUD','Update Front End Global Setting');
							$this->_db->commit();
							$this->view->success = true;
							$msg = "Settings Change Request Saved";
							$this->view->report_msg = $msg;
				}
				catch(Exception $e)
				{
						$this->_db->rollBack();
				}
						

				foreach ($params['ccy'] as $val) {
					$type = 'charges_type'.$ccy;
					$type2 = $params[$type];

					$ccytf = 'ccytf'.$val;
					$ccytf2 = $params[$ccytf];

					$tfamount = 'tfamount'.$val;
					$tfamount2 = $params[$tfamount];

					$ccyfa = 'ccyfa'.$val;
					$ccyfa2 = $params[$ccyfa];

					$faamount = 'faamount'.$val;
					$faamount2 = $params[$faamount];

					$ccypf = 'ccypf'.$val;
					$ccypf2 = $params[$ccypf];

					$min = 'min'.$val;
					$min2 = $params[$min];

					$max = 'max'.$val;
					$max2 = $params[$max];

					$pcy = 'pcy'.$val;
					$pcy2 = $params[$pcy];

					if ($ccytf2!='-') {
						if($tfamount2 == null)
						{
							$tfamount2 = '0.00';
						}
						$charge_amt =	Application_Helper_General::convertDisplayMoney($tfamount2);
						// $ccytf3 = $ccytf2;
						$info = 'Remittance Charges';
						$info2 = 'Set Edit New Charges';

						// $change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['new'],null,'M_CHARGES_REMITTANCE','TEMP_CHARGES_REMITTANCE','-','Transfer Fee',$cust_id,$cust_name);

						$params_insert_3 = array(
							'CHANGES_ID' => $change_id,
							'CUST_ID' => $cust_id,
							'CHARGE_TYPE' => '3',
							'CHARGE_CCY' => $val,
							'CHARGE_AMOUNT_CCY' => $ccytf2,
							'CHARGE_AMT' => $charge_amt
						
						);
						$this->_db->insert('TEMP_CHARGES_REMITTANCE',$params_insert_3);

					}

					if ($ccyfa2!='-') {
						if($faamount2 == null)
						{
							$faamount2 = '0.00';
						}
						$charge_amt =	Application_Helper_General::convertDisplayMoney($faamount2);
						// $ccytf3 = $ccytf2;
						$info = 'Remittance Charges';
						$info2 = 'Set Edit New Charges';

						// $change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['new'],null,'M_CHARGES_REMITTANCE','TEMP_CHARGES_REMITTANCE','-','Full Amount Fee',$cust_id,$cust_name);

						$params_insert_4 = array(
							'CHANGES_ID' => $change_id,
							'CUST_ID' => $cust_id,
							'CHARGE_TYPE' => '4',
							'CHARGE_CCY' => $val,
							'CHARGE_AMOUNT_CCY' => $ccytf2,
							'CHARGE_AMT' => $charge_amt
						
						);
						$this->_db->insert('TEMP_CHARGES_REMITTANCE',$params_insert_4);

					}

					if ($ccypf2!='-') {

						$charge_min =	Application_Helper_General::convertDisplayMoney($min2);
						$charge_max =	Application_Helper_General::convertDisplayMoney($max2);
						// $ccytf3 = $ccytf2;
						$info = 'Remittance Charges';
						$info2 = 'Set Edit New Charges';

						if($pcy2 == null)
						{
							$pcy2 = '0.00';
						}

						// $change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['new'],null,'M_CHARGES_REMITTANCE','TEMP_CHARGES_REMITTANCE','-','Provision Fee',$cust_id,$cust_name);

						$params_insert_5 = array(
										'CHANGES_ID' => $change_id,
										'CUST_ID' => $cust_id,
										'CHARGE_TYPE' => '5',
										'CHARGE_CCY' => $val,
										'CHARGE_AMOUNT_CCY' => $ccypf2,
										'CHARGE_PROV_MIN_AMT' => $charge_min,
										'CHARGE_PROV_MAX_AMT' => $charge_max,
										'CHARGE_PCT' => $pcy2,
						);
						$this->_db->insert('TEMP_CHARGES_REMITTANCE',$params_insert_5);

					}

				}
				
				// $this->setbackURL('/companycharges/monthlyfee/index/custid/GLOBAL');
				$this->_redirect('/notification/submited/index');
				// die('here1');
			}
			else{
				$docErr = 'Error in processing form values. Please correct values and re-submit.';
				$this->view->report_msg = $docErr;
				// die('here');
				foreach($zf_filter->getMessages() as $key=>$err){
				$xxx = 'x'.$key;
				// print_r($zf_filter->getMessages());die;
				$this->view->$xxx = $this->displayError($err);
				}
			}
			// var_dump($ccy); exit();

			foreach($setting as $key=>$value)
			{
				if (in_array($key, $arrfck)){
					$this->view->$key =   $this->_getParam($key);
				}
				else{
					$this->view->$key = (!empty($zf_filter->$key)) ? $zf_filter->$key : $this->_getParam($key);
				}
			}

			$disable_note = $zf_filter->disable_note;
			$note = $zf_filter->note;
		}
		else{
			$disable_note = $setting['disable_note'];
			$note = $setting['note'];
			Application_Helper_General::writeLog('STLS','View Frontend Global Setting');
		}

		$disable_note_len 	 = (isset($disable_note))  ? strlen($disable_note)  : 0;
		$note_len 	 = (isset($note))  ? strlen($note)  : 0;

		$disable_note_len 	 = 200 - $disable_note_len;
		$note_len 	 = 200 - $note_len;

		$this->view->disable_note_len		= $disable_note_len;
		$this->view->note_len		= $note_len;


	}
}
