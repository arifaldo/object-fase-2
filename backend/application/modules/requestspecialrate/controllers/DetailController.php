<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';
require_once 'General/CustomerUser.php';
require_once 'CMD/Validate/ValidateAccountBeneficiary.php';
require_once 'Service/Token.php'; //added new
require_once 'CMD/Validate/Validate.php';

class requestspecialrate_DetailController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti
	
	protected $_destinationUploadDir = '';
	protected $_listCCYValidate = '';

	public function initController()
	{       
		$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/'; 	
		$this->_listCCYValidate = Application_Helper_Array::simpleArray($this->getCcy(),'CCY_ID');  
		$selectCurrency = '-- '.$this->language->_('Select Currency'). '--';
		$listCcy = array(''=>$selectCurrency);
		if (count($this->getCcy()) == 1){ //remove useless value
			$listCcy = array();
		}
		$listCcy = array_merge($listCcy,Application_Helper_Array::listArray($this->getCcy(),'CCY_ID','CCY_ID'));
		$this->view->ccy = $listCcy;
        		
	}
	
	public function indexAction()
	{		

		$this->_helper->layout()->setLayout('newlayout');
		
		$Settings = new Settings();
		$privibenelinkage = $this->view->hasPrivilege('BLBU');
		$filter  = new Application_Filtering();
		$settings = new Application_Settings();
		$ccyList  = $settings->setCurrencyRegistered();		
		//Zend_Debug::dump($ccyList);die;
		
		$this->view->userIdLogin  = $this->_userIdLogin;
		
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr = $CustomerUser->getAccounts();
		$this->view->AccArr = $AccArr;
        
        $getCcy = $this->getCcy();
        $this->view->CCYData = $getCcy;
        // var_dump($CCYData);

        $numreq = $this->_getParam('numreq');

        if(!empty($numreq)){
            
            $srdata = $this->_db->select()
                             ->from(array('A' => 'T_REQ_SPCRATE'),array('*'))
                             ->where('A.ID_REQUEST = ?',$numreq)
                             ->query()->fetchAll();
            
            $dataselect = $srdata['0'];
            $selectcomp = $this->_db->select()
                            ->from(array('A' => 'M_CUSTOMER'),array('*'))
                        //  ->joinLeft(array('B' => 'M_COUNTRY'),'A.COUNTRY_CODE = B.COUNTRY_CODE',array('COUNTRY_NAME'))
                            ->where('A.CUST_ID =?',$dataselect['CUST_ID'])
                            ->query()->fetchAll();

        $this->view->compinfo = $selectcomp[0];

            if($srdata){
                $data = $srdata['0'];

                $this->view->CUST_ID 	= $data['CUST_ID'];
                $this->view->TREASURY_REFF 	= $data['TREASURY_REFF'];
                $this->view->USER_ID_LOGIN 	= $data['RATE_REQUESTBY'];
                $this->view->REG_NUM 		= $data['ID_REQUEST'];
                $this->view->FROM_CCY 	= $data['FROM_CCY'];
                $this->view->TO_CCY = $data['TO_CCY'];
                $this->view->AMOUNT 		= $data['AMOUNT_TRX'];
                $this->view->CCY_AMOUNT 		= $data['CCY_AMOUNT'];
                $this->view->DEAL_DATE 		= $data['DEAL_DATE'];
                $this->view->VALID_DATE 		= $data['VALID_DATE'];
                $this->view->RATE_REQUESTED 		= $data['RATE_REQUESTED'];
                $this->view->RATE_REQUESTBY 		= $data['RATE_REQUESTBY'];
                $this->view->RATE_UPDATED 		= $data['RATE_UPDATED'];
                $this->view->RATE_UPDATEDBY 		= $data['RATE_UPDATEDBY'];
                $this->view->RATE_APPROVED 		= $data['RATE_APPROVED'];
                $this->view->RATE_APPROVEBY 		= $data['RATE_APPROVEBY'];

                $arrStatus = array('1'=>'Approve by Bank','2'=>'In Process','3'=>'Not Valid','4'=>'Rejected');
                $this->view->STATUS 		= $arrStatus[$data['STATUS']];
                $this->view->statusnum 		= $data['STATUS'];
                $this->view->actiondata     = $data['ACTION'];

                if(!empty($data['AMOUNT_RATE']))
                    $this->view->AMOUNT_RATE 		= $data['AMOUNT_RATE'];
                else
                    $this->view->AMOUNT_RATE        = "0";
                
                if(!empty($data['DEAL_RATE']))
                    $this->view->DEAL_RATE 		= $data['DEAL_RATE'];
                else
                    $this->view->DEAL_RATE        = "0";

                if($this->_request->isPost() )
                {
                    $deal = $this->_getParam('deal');
                    $reject = $this->_getParam('reject');
                    $approve = $this->_getParam('approvesr');

                    $treasure = $this->_getParam('treasuryreff');
                    $dealrate = $this->_getParam('amountoff');

                    if($dealrate == ''){
                        $dealrate = 0;
                    }

                    

                    if($deal){
                        try 
                        {
                            $data = array (
                                'DEAL_RATE' => Application_Helper_General::convertDisplayMoney($dealrate),
                                'RATE_UPDATED' => new Zend_Db_Expr("now()"),
                                'RATE_UPDATEDBY' => $this->_userIdLogin,
                                'AMOUNT_RATE' => Application_Helper_General::convertDisplayMoney($dealrate),
                                'ACTION' => 'U'
                            );
                            $where['ID_REQUEST = ?'] = $numreq;
                            $this->_db->update('T_REQ_SPCRATE',$data,$where);
                                                    
                            $historyInsert = array(
                                'ID_REQUEST'         => $numreq,
                                'CHANGES_FLAG'         => 'B',
                                'CUST_ID'           => $this->_custIdLogin,
                                'CREATEDBY'        => $this->_userIdLogin,
                                'RATE_DATETIME'    => new Zend_Db_Expr("now()"),
                                'AMOUNT_RATE'       => Application_Helper_General::convertDisplayMoney($dealrate),
                                'DEAL_RATE'       => Application_Helper_General::convertDisplayMoney($dealrate),
                                'ACTION' => 'U'
                            );
        
                            $this->_db->insert('T_HISTORY_REQRATE', $historyInsert);

                            $this->setbackURL('/'.$this->_request->getModuleName().'/index/');
                            $this->_redirect('/notification/success/index');
                        }
                        catch(Exception $e) 
                        {   
                            var_dump($e);die;
                            $this->_db->rollBack();
                        }
                    }

                    if($approve){
                        try 
                        {
                            $data = array (
                                'TREASURY_REFF' => $treasure,
                                'DEAL_RATE' => Application_Helper_General::convertDisplayMoney($data['DEAL_RATE']),
                                'RATE_UPDATED' => new Zend_Db_Expr("now()"),
                                'RATE_UPDATEDBY' => $this->_userIdLogin,
                                'RATE_APPROVED' => new Zend_Db_Expr("now()"),
                                'RATE_APPROVEBY' => $this->_userIdLogin,
                                'ACTION' => 'A',
                                'STATUS' => '1',
                            );
                            $where['ID_REQUEST = ?'] = $numreq;
                            $this->_db->update('T_REQ_SPCRATE',$data,$where);
                                                    
                            $historyInsert = array(
                                'ID_REQUEST'         => $numreq,
                                'CHANGES_FLAG'         => 'B',
                                'CUST_ID'           => $this->_custIdLogin,
                                'CREATEDBY'        => $this->_userIdLogin,
                                'RATE_DATETIME'    => new Zend_Db_Expr("now()"),
                                'AMOUNT_RATE'       => Application_Helper_General::convertDisplayMoney($data['DEAL_RATE']),
                                'DEAL_RATE'       => Application_Helper_General::convertDisplayMoney($data['DEAL_RATE']),
                                'ACTION' => 'A'
                            );
        
                            $this->_db->insert('T_HISTORY_REQRATE', $historyInsert);

                            $this->setbackURL('/'.$this->_request->getModuleName().'/index/');
                            $this->_redirect('/notification/success/index');
                        }
                        catch(Exception $e) 
                        {   
                            var_dump($e);die;
                            $this->_db->rollBack();
                        }
                    }

                    if($reject){
                        $dealrate = 0;
                        try 
                        { 
                            $data = array (
                                'STATUS' => '4',
                                'RATE_UPDATED' => new Zend_Db_Expr("now()"),
                                'RATE_UPDATEDBY' => $this->_userIdLogin,
                                'ACTION' => 'R'
                            );
                            $where['ID_REQUEST = ?'] = $numreq;
                            $this->_db->update('T_REQ_SPCRATE',$data,$where);
                            
                            $historyInsert = array(
                                'ID_REQUEST'         => $numreq,
                                'CHANGES_FLAG'         => 'B',
                                'CUST_ID'           => $selectcomp['CUST_NAME'],
                                'CREATEDBY'        => $this->_userIdLogin,
                                'RATE_DATETIME'    => new Zend_Db_Expr("now()"),
                                'AMOUNT_RATE'       => Application_Helper_General::convertDisplayMoney($dealrate),
                                'ACTION' => 'R'
                            );
        
                            $this->_db->insert('T_HISTORY_REQRATE', $historyInsert);
    
                            $this->setbackURL('/'.$this->_request->getModuleName().'/index/');
                            $this->_redirect('/notification/success/index');
                        }
                        catch(Exception $e) 
                        {   
                            var_dump($e);die;
                            $this->_db->rollBack();
                        }
                    }
                }
            }
        }
    }
}