<?php
require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'General/Account.php';

class insurancebranch_IndexController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$model = new insurancebranch_Model_Insurancebranch();

		$getData = $model->getData();
		$insBranchAcctArr = [];
		if ($getData) {
			foreach ($getData as $row) {
				$insBranchAcctArr[$row['INS_BRANCH_ACCT']] = $row['INS_BRANCH_ACCT'] . ' [' . $row['INS_BRANCH_CCY'] . '] / ' . $row['CUST_NAME'] . ' / ' . $row['ACCT_TYPE'];
			}
		}
		$this->view->insBranchAcctArr = $insBranchAcctArr;
		// echo '<pre>';print_r($insBranchAcctArr);echo '</pre>';die('insBranchAcctArr');

		$setting 	= new Settings();
		$enc_pass 	= $setting->getSetting('enc_pass');
		$enc_salt 	= $setting->getSetting('enc_salt');

		$sessToken 			= new Zend_Session_Namespace('Tokenenc');
		$password_hash 		= md5($enc_salt . $enc_pass);
		$rand 				= $this->_userIdLogin . date('YmdHis') . $password_hash;
		$sessToken->token 	= $rand;
		$this->view->token 	= $sessToken->token;
		// advance filter 

		$filterlist = array("CUST_NAME", "INS_BRANCH_NAME", "INS_BRANCH_ACCT", "LAST_SUGGESTEDBY", "LAST_APPROVEDBY");

		$this->view->filterlist = $filterlist;

		$filter = $this->_getParam('filter');
		$filter_clear = $this->_getParam('filter_clear');


		$filterArr = array(
			'filter'	=>  array('StripTags'),
			'CUST_NAME'   	=>  array('StringTrim', 'StripTags', 'StringToUpper'),
			'INS_BRANCH_NAME'   	=>  array('StringTrim', 'StripTags', 'StringToUpper'),
			'INS_BRANCH_ACCT'   	=>  array('StringTrim', 'StripTags', 'StringToUpper'),
			'LAST_SUGGESTEDBY'   	=>  array('StringTrim', 'StripTags', 'StringToUpper'),
			'LAST_APPROVEDBY'   	=>  array('StringTrim', 'StripTags', 'StringToUpper'),

		);

		$validator = array(
			'filter'			 	=> array(),
			'CUST_NAME'			 	=> array(),
			'INS_BRANCH_NAME'			 	=> array(),
			'INS_BRANCH_ACCT'			 	=> array(),
			'LAST_SUGGESTEDBY'			 	=> array(),
			'LAST_APPROVEDBY'			 	=> array(),

		);


		$dataParam = array("CUST_NAME", "INS_BRANCH_NAME", "INS_BRANCH_ACCT", "LAST_SUGGESTEDBY", "LAST_APPROVEDBY");
		$dataParamValue = array();

		$clean2 = array_diff($this->_request->getParam('wherecol'), $dataParam);
		$dataParam = array_diff($this->_request->getParam('wherecol'), $clean2);
		// print_r($this->_request->getParam('wherecol'));
		foreach ($dataParam as $no => $dtParam) {

			if (!empty($this->_request->getParam('wherecol'))) {
				$dataval = $this->_request->getParam('whereval');
				// print_r($dataval);
				$order = 0;
				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if ($dtParam == $value) {
						$dataParamValue[$dtParam] = $dataval[$order];
					}
					$order++;
				}
			}
		}

		$zf_filter = new Zend_Filter_Input($filterArr, $validator, $dataParamValue);
		// $filter 	= $zf_filter->getEscaped('filter');
		$filter 		= $this->_getParam('filter');
		$CUST_NAME		= html_entity_decode($zf_filter->getEscaped('CUST_NAME'));
		$INS_BRANCH_NAME		= html_entity_decode($zf_filter->getEscaped('INS_BRANCH_NAME'));
		$INS_BRANCH_ACCT		= html_entity_decode($zf_filter->getEscaped('INS_BRANCH_ACCT'));
		$LAST_SUGGESTEDBY		= html_entity_decode($zf_filter->getEscaped('LAST_SUGGESTEDBY'));
		$LAST_APPROVEDBY		= html_entity_decode($zf_filter->getEscaped('LAST_APPROVEDBY'));

		if ($filter_clear == true) {
		}

		if ($filter == TRUE) {
			$fInsBranchCode 	= $filterParam['fInsBranchCode'] 	= $CUST_NAME;
			$fInsBranchName 	= $filterParam['fInsBranchName'] 	= $INS_BRANCH_NAME;
			$fInsBranchAcct 	= $filterParam['fInsBranchAcct'] 	= $INS_BRANCH_ACCT;
			$fInsBranchSuggest 	= $filterParam['fInsBranchSuggest'] = $LAST_SUGGESTEDBY;
			$fInsBranchApprove 	= $filterParam['fInsBranchApprove'] = $LAST_APPROVEDBY;
		}

		if (!empty($dataParamValue)) {
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}

			$this->view->wherecol     = $wherecol;
			$this->view->whereval     = $whereval;
		}

		// END advance filter 

		$data = $model->getData($filterParam, $filter);
		$this->paging($data);

		if ($this->_request->getParam("csv")) {

			$save_temp = [];
			foreach ($data as $key => $value) {
				array_push($save_temp, [
					$key + 1,
					// ucwords(strtolower($value["CUST_NAME"])) . " (" . $value["CUST_ID"] . ")",
					$value["CUST_NAME"] . " (" . $value["CUST_ID"] . ")",
					$value["INS_BRANCH_NAME"],
					$value["INS_BRANCH_EMAIL"],
					strval("'" . $value["INS_BRANCH_ACCT"]),
					date("d M Y", strtotime($value["LAST_SUGGESTED"])) . " - " . $value["LAST_SUGGESTEDBY"],
					date("d M Y", strtotime($value["LAST_APPROVED"])) . " - " . $value["LAST_APPROVEDBY"],

				]);
			}

			$head = [
				"No",
				$this->language->_('Insurance Company'),
				$this->language->_('Branch'),
				$this->language->_('Branch Email'),
				$this->language->_('Branch Account'),
				$this->language->_('Last Suggested'),
				$this->language->_('Last Approved')
			];
			return $this->_helper->download->csv($head, $save_temp, null, $this->language->_('Insurance Branch List - ' . date("Ymd-His")));
		}

		if ($this->_request->getParam("print") == "1") {
			$field_export = array(

				'nomor' => array(
					'field'    => 'NOMOR',
					'label'    => $this->language->_('No'),
					'sortable' => true
				),
				'InsuranceComapny'     => array(
					'field'    => 'INSURANCE_COMPANY',
					'label'    => $this->language->_('Insurance Company'),
					'sortable' => true
				),

				'branch'     => array(
					'field'    => 'BRANCH',
					'label'    => $this->language->_('Branch'),
					'sortable' => true
				),

				'branchEmail'     => array(
					'field'  => 'BRANCH_EMAIL',
					'label'    => $this->language->_('Branch Email'),
					'sortable' => true
				),

				'branchAccount'   => array(
					'field'    => 'BRANCH_ACCOUNT',
					'label'    => $this->language->_('Branch Account'),
					'sortable' => true
				),

				'lastSuggested'   => array(
					'field'  => 'LAST_SUGGESTED',
					'label'    => $this->language->_('Last Suggested'),
					'sortable' => true
				),

				'Last Approved'   => array(
					'field'  => 'LAST_APPROVED',
					'label'    => $this->language->_('Last Approved'),
					'sortable' => true
				),
			);

			$dataContent = [];
			foreach ($data as $key => $value) {
				array_push($dataContent, [
					"NOMOR" => $key + 1,
					"INSURANCE_COMPANY" => $value["CUST_NAME"] . " (" . $value["CUST_ID"] . ")",
					"BRANCH" => $value["INS_BRANCH_NAME"],
					"BRANCH_EMAIL" => $value["INS_BRANCH_EMAIL"],
					"BRANCH_ACCOUNT" => $value["INS_BRANCH_ACCT"],
					"LAST_SUGGESTED" => date("d M Y", strtotime($value["LAST_SUGGESTED"])) . " - " . $value["LAST_SUGGESTEDBY"],
					"LAST_APPROVED" => date("d M Y", strtotime($value["LAST_APPROVED"])) . " - " . $value["LAST_APPROVEDBY"],

				]);
			}

			$this->_forward('print', 'index', 'widget', array('data_content' => $dataContent, 'data_caption' => 'Insurance Branch List', 'data_header' => $field_export, 'data_filter' => ""));
		}

		$this->view->filter 			= $filter;
		$this->view->fInsBranchCode 	= $fInsBranchCode;
		$this->view->fInsBranchName 	= $fInsBranchName;
		$this->view->fInsBranchAcct 	= $fInsBranchAcct;
		$this->view->fInsBranchSuggest 	= $fInsBranchSuggest;
		$this->view->fInsBranchApprove 	= $fInsBranchApprove;
		// $this->view->fDateFrom 		= $fDateFrom;
		// $this->view->fDateTo 		= $fDateTo;

		Application_Helper_General::writeLog('VIBO', 'View Insurance Branch List');
	}

	public function onboardingAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		if (!$this->view->hasPrivilege("MIBO")) {
			return $this->_redirect("/home/dashboard");
		}

		$model = new insurancebranch_Model_Insurancebranch();

		$getCustomer = $model->getCustomer();
		$custIdArr   = Application_Helper_Array::listArray($getCustomer, 'CUST_ID', 'CUST_NAME');
		$this->view->custIdArr = $custIdArr;

		$process   = $this->_getParam('process');
		$isConfirm = (empty($this->_request->getParam('isConfirm'))) ? false : true;
		$submitBtn = ($this->_request->isPost() && $process == "submit") ? true : false;

		$cust_name = "";

		if ($this->_request->isPost()) {
			$params     = $this->_request->getParams();
			$filters    = array('*' => array('StringTrim', 'StripTags'));
			$validators = array(
				'cust_id'		=> array(
					'NotEmpty',
					'messages' => array($this->language->_('Insurance Company cannot be empty')),
				),
				'cust_name'		=> array('allowEmpty' => true),
				'branch_ccy'	=> array('allowEmpty' => true),
				'acct_type'		=> array('allowEmpty' => true),
				'branch_name'	=> array(
					'NotEmpty',
					'messages' => array($this->language->_('Branch Name cannot be empty')),
				),
				'branch_email'	=> array(
					'NotEmpty',
					'messages' => array($this->language->_('Branch Email cannot be empty')),
				),
				'branch_acct'	=> array(
					'NotEmpty',
					'messages' => array($this->language->_('Branch Account cannot be empty')),
				),
			);

			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $params, $this->_optionsValidator);

			$cust_id 		= $zf_filter_input->cust_id;
			$cust_name 		= $zf_filter_input->cust_name;
			$branch_ccy 	= $zf_filter_input->branch_ccy;
			$acct_type 		= $zf_filter_input->acct_type;
			$branch_name 	= $zf_filter_input->branch_name;
			$branch_email 	= $zf_filter_input->branch_email;
			$branch_acct 	= $zf_filter_input->branch_acct;

			if ($submitBtn) {
				if ($zf_filter_input->isValid()) {
					if ($isConfirm == false) {

						$account = new Account($branch_acct, null, null, null);

						$result = $account->checkAccountInfo(false);

						// $this->dd(strtolower($result['account_type']));

						//$svcAccount = new Service_Account($branch_acct, 'IDR');

						//$result = $svcAccount->inquiryAccountBalance('AB', FALSE);
						//echo '<pre>';
						//var_dump($result);die;
						if ($result['response_code'] == '0000' || $result['response_code'] == '00') {
							$success = true;
							if ($result['type_desc'] != "Giro" && strtolower($result['account_type']) != 'd') {
								$success = false;
								$err = true;
								$errmsg = "Tipe Rekening bukan Giro";
							}

							if ($result['status'] != '1') {

								$success = false;
								$err = true;
								$errmsg = "Nomor Rekening tidak aktif";
							}

							if ($result['status'] == '4') {
								$success = false;
							}

							if ($success == true) {

								$checkAcct = $this->_db->select()
									->from('M_CUSTOMER_ACCT', ['ACCT_STATUS'])
									->where('ACCT_NO = ?', $branch_acct)
									->where('CUST_ID = ?', $cust_id)
									->query()->fetch();

								if (intval($checkAcct['ACCT_STATUS']) === 1) {
									$isConfirm = true;
								} else {
									$success = false;
									$err = true;
									switch (intval($checkAcct['ACCT_STATUS'])) {
										case 2:
											$errmsg = "Nomor Rekening sedang ditangguhkan";
											break;

										default:
											$errmsg = "Nomor Rekening tidak aktif";
											break;
									}
								}
							}
						} else {
							$isConfirm = false;
							$err = true;
							$errmsg = $result['response_desc'];
						}
					} else {
						$generateCode = $model->generateCode();
						$info         = 'Customer ID = ' . $cust_id . ', Customer Name = ' . $cust_name;
						$change_id    = $this->suggestionWaitingApproval('Insurance Branch', $info, $this->_changeType['code']['new'], null, 'M_INS_BRANCH', 'TEMP_INS_BRANCH', $generateCode, $branch_name, $cust_id, $cust_name);

						try {
							$this->_db->beginTransaction();

							$dataInsert = [
								'CHANGES_ID'		=> $change_id,
								'INS_BRANCH_CODE'	=> $generateCode,
								'CUST_ID'			=> $cust_id,
								'INS_BRANCH_NAME'	=> $branch_name,
								'INS_BRANCH_EMAIL'	=> $branch_email,
								'INS_BRANCH_ACCT'	=> $branch_acct,
								'INS_BRANCH_CCY'	=> $branch_ccy,
								'ACCT_TYPE'			=> $acct_type,
								'FLAG'				=> 2, // Waiting Approve
								'LAST_SUGGESTED'	=> new Zend_Db_Expr('now()'),
								'LAST_SUGGESTEDBY'	=> $this->_userIdLogin
							];
							$this->_db->insert('TEMP_INS_BRANCH', $dataInsert);

							$this->_db->commit();

							Application_Helper_General::writeLog('MIBO', 'Register New Insurance Branch for ' . $cust_name . ' (' . $cust_id . ')');
							$this->setbackURL('/insurancebranch');
							$this->_redirect('/notification/submited/index');
						} catch (Exception $error) {
							$this->_db->rollBack();
							echo '<pre>';
							print_r($error->getMessage());
							echo '</pre><br>';
							die;
						}
					}
				} else {
					$errors = $zf_filter_input->getMessages();
					$errDesc['cust_id']			= isset($errors['cust_id']) ? $errors['cust_id'] : null;
					$errDesc['branch_name']		= isset($errors['branch_name']) ? $errors['branch_name'] : null;
					$errDesc['branch_email']	= isset($errors['branch_email']) ? $errors['branch_email'] : null;
					$errDesc['branch_acct']		= isset($errors['branch_acct']) ? $errors['branch_acct'] : null;
				}
			} else {
				$isConfirm = false;
			}
		} else {
			Application_Helper_General::writeLog('MIBO', 'View Register New Insurance Branch');
		}

		if ($err) {
			//$err = true;
			//$errmsg = 'Service not available';

			$this->view->err = true;
			$this->view->errmsg = $errmsg;
		}

		$this->view->isConfirm 		= $isConfirm;
		$this->view->errDesc 		= $errDesc;
		$this->view->cust_id 		= $cust_id;
		$this->view->cust_name 		= $cust_name;
		$this->view->branch_ccy 	= $branch_ccy;
		$this->view->acct_type 		= $acct_type;
		$this->view->branch_name 	= $branch_name;
		$this->view->branch_email 	= $branch_email;
		$this->view->branch_acct 	= $branch_acct;
	}

	public function detailAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$model = new insurancebranch_Model_Insurancebranch();

		$id = $this->_getParam('id');

		$sessToken 	= new Zend_Session_Namespace('Tokenenc');
		$password 	= $sessToken->token;

		$AESMYSQL    = new Crypt_AESMYSQL();
		$decryption  = urldecode($id);
		$branch_code = $AESMYSQL->decrypt($decryption, $password);

		$params = $this->getRequest()->getParams();
		$this->view->params = $params;

		$data = $model->getDataById($branch_code);
		$this->view->data = $data;
		// echo '<pre>';print_r($data);echo '</pre>';die('data');
		if ($data) {
			$getTempData = $model->getTempData($branch_code, null);
			if ($getTempData) {
				$this->view->is_update = true;
			}

			Application_Helper_General::writeLog('VIBO', 'View Insurance Branch Detail of ' . $data['CUST_NAME'] . ' (' . $data['CUST_ID'] . ') – ' . $data['INS_BRANCH_NAME']);
		}
	}

	public function updateAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		if (!$this->view->hasPrivilege("MIBO")) {
			return $this->_redirect("/home/dashboard");
		}

		$model = new insurancebranch_Model_Insurancebranch();

		$id = $this->_getParam('id');

		$sessToken 	= new Zend_Session_Namespace('Tokenenc');
		$password 	= $sessToken->token;

		$AESMYSQL    = new Crypt_AESMYSQL();
		$decryption  = urldecode($id);
		$branch_code = $AESMYSQL->decrypt($decryption, $password);

		$params = $this->getRequest()->getParams();
		$this->view->params = $params;

		$data = $model->getDataById($branch_code);
		$this->view->data = $data;
		// echo '<pre>';print_r($data);echo '</pre>';die('data');
		if ($data) {
			$getCustomer = $model->getCustomer();
			$custIdArr   = Application_Helper_Array::listArray($getCustomer, 'CUST_ID', 'CUST_NAME');
			$this->view->custIdArr = $custIdArr;

			$process   = $this->_getParam('process');
			$isConfirm = (empty($this->_request->getParam('isConfirm'))) ? false : true;
			$submitBtn = ($this->_request->isPost() && $process == "submit") ? true : false;

			if ($this->_request->isPost()) {
				$params     = $this->_request->getParams();
				$filters    = array('*' => array('StringTrim', 'StripTags'));
				$validators = array(
					'cust_id'		=> array(
						'NotEmpty',
						'messages' => array($this->language->_('Insurance Company cannot be empty')),
					),
					'cust_name'		=> array('allowEmpty' => true),
					'branch_ccy'	=> array('allowEmpty' => true),
					'acct_type'		=> array('allowEmpty' => true),
					'branch_name'	=> array(
						'NotEmpty',
						'messages' => array($this->language->_('Branch Name cannot be empty')),
					),
					'branch_email'	=> array(
						'NotEmpty',
						'messages' => array($this->language->_('Branch Email cannot be empty')),
					),
					'branch_acct'	=> array(
						'NotEmpty',
						'messages' => array($this->language->_('Branch Account cannot be empty')),
					),
				);

				$zf_filter_input = new Zend_Filter_Input($filters, $validators, $params, $this->_optionsValidator);

				$cust_id 		= $zf_filter_input->cust_id;
				$cust_name 		= $zf_filter_input->cust_name;
				$branch_ccy 	= $zf_filter_input->branch_ccy;
				$acct_type 		= $zf_filter_input->acct_type;
				$branch_name 	= $zf_filter_input->branch_name;
				$branch_email 	= $zf_filter_input->branch_email;
				$branch_acct 	= $zf_filter_input->branch_acct;

				if ($submitBtn) {
					if ($zf_filter_input->isValid()) {

						if ($isConfirm == false) {
							$account = new Account($branch_acct, null, null, null);

							$result = $account->checkAccountInfo(false);


							//$svcAccount = new Service_Account($branch_acct, 'IDR');

							//$result = $svcAccount->inquiryAccountBalance('AB', FALSE);
							//echo '<pre>';
							//var_dump($result);die;
							if ($result['response_code'] == '0000' || $result['response_code'] == '00') {
								$success = true;
								if ($result['type_desc'] != "Giro" && strtolower($result['account_type']) != 'd') {
									$success = false;
									$err = true;
									$errmsg = "Tipe Rekening bukan Giro";
								}

								if ($result['status'] != '1') {

									$success = false;
									$err = true;
									$errmsg = "Nomor Rekening tidak aktif";
								}

								if ($result['status'] == '4') {
									$success = false;
								}

								if ($success == true) {
									$checkAcct = $this->_db->select()
										->from('M_CUSTOMER_ACCT', ['ACCT_STATUS'])
										->where('ACCT_NO = ?', $branch_acct)
										->query()->fetch();

									if (intval($checkAcct['ACCT_STATUS']) === 1) {
										$isConfirm = true;
									} else {
										$success = false;
										$err = true;
										switch (intval($checkAcct['ACCT_STATUS'])) {
											case 2:
												$errmsg = "Nomor Rekening sedang ditangguhkan";
												break;

											default:
												$errmsg = "Nomor Rekening tidak aktif";
												break;
										}
									}
								}
							} else {
								$isConfirm = false;
								$err = true;
								$errmsg = $result['response_desc'];
							}
						} else {




							$info      = 'Customer ID = ' . $data['CUST_ID'] . ', Customer Name = ' . $data['CUST_NAME'];
							$change_id = $this->suggestionWaitingApproval('Insurance Branch', $info, $this->_changeType['code']['edit'], null, 'M_INS_BRANCH', 'TEMP_INS_BRANCH', $branch_code, $branch_name, $data['CUST_ID'], $data['CUST_NAME']);

							try {
								$this->_db->beginTransaction();

								$dataInsert = [
									'INS_BRANCH_CODE'	=> $branch_code,
									'CHANGES_ID'		=> $change_id,
									'CUST_ID'			=> $cust_id,
									'INS_BRANCH_NAME'	=> $branch_name,
									'INS_BRANCH_EMAIL'	=> $branch_email,
									'INS_BRANCH_ACCT'	=> $branch_acct,
									'INS_BRANCH_CCY'	=> $branch_ccy,
									'ACCT_TYPE'			=> $acct_type,
									'FLAG'				=> 2, // Waiting Approve
									'LAST_SUGGESTED'	=> new Zend_Db_Expr('now()'),
									'LAST_SUGGESTEDBY'	=> $this->_userIdLogin
								];
								$this->_db->insert('TEMP_INS_BRANCH', $dataInsert);

								$this->_db->commit();

								Application_Helper_General::writeLog('MIBO', 'Update Insurance Branch for ' . $cust_name . ' (' . $cust_id . ') - ' . $branch_name);
								$this->setbackURL('/insurancebranch');
								$this->_redirect('/notification/submited/index');
							} catch (Exception $error) {
								$this->_db->rollBack();
								echo '<pre>';
								print_r($error->getMessage());
								echo '</pre><br>';
								die;
							}
						}
					} else {
						$errors = $zf_filter_input->getMessages();
						$errDesc['cust_id']			= isset($errors['cust_id']) ? $errors['cust_id'] : null;
						$errDesc['branch_name']		= isset($errors['branch_name']) ? $errors['branch_name'] : null;
						$errDesc['branch_email']	= isset($errors['branch_email']) ? $errors['branch_email'] : null;
						$errDesc['branch_acct']		= isset($errors['branch_acct']) ? $errors['branch_acct'] : null;
					}
				} else {
					$isConfirm = false;
				}
			} else {
				Application_Helper_General::writeLog('MIBO', 'View Update Insurance Branch');
			}
			if ($err) {
				//$err = true;
				//$errmsg = 'Service not available';

				$this->view->err = true;
				$this->view->errmsg = $errmsg;
			}

			$this->view->isConfirm 		= $isConfirm;
			$this->view->errDesc 		= $errDesc;
			$this->view->cust_id 		= $data['CUST_ID']; //cust_id;
			$this->view->cust_name 		= $cust_name;
			$this->view->branch_ccy 	= $branch_ccy;
			$this->view->acct_type 		= $acct_type;
			$this->view->branch_name 	= $branch_name;
			$this->view->branch_email 	= $branch_email;
			$this->view->branch_acct 	= $branch_acct;
		}
	}

	public function deleteAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();

		if (!$this->view->hasPrivilege("MIBO")) {
			return $this->_redirect("/home/dashboard");
		}

		$model = new insurancebranch_Model_Insurancebranch();

		$id = $this->_getParam('id');

		$sessToken 	= new Zend_Session_Namespace('Tokenenc');
		$password 	= $sessToken->token;

		$AESMYSQL    = new Crypt_AESMYSQL();
		$decryption  = urldecode($id);
		$branch_code = $AESMYSQL->decrypt($decryption, $password);
		// echo '<pre>';print_r($branch_code);echo '</pre>';die('branch_code');
		$data = $model->getDataById($branch_code);

		if ($data) {
			//Application_Helper_General::writeLog('MIBO', 'Delete Insurance Branch for ' . $data['CUST_NAME'] . ' (' . $data['CUST_ID'] . ') - ' . $data['INS_BRANCH_NAME']);

			$info         = 'Customer ID = ' . $data['CUST_ID'] . ', Customer Name = ' . $data['CUST_NAME'];
			//insert ke T_GLOBAL_CHANGES

			$change_id = $this->suggestionWaitingApproval('Insurance Branch', $info, $this->_changeType['code']['delete'], null, 'M_INS_BRANCH', 'TEMP_INS_BRANCH', $data['INS_BRANCH_CODE'], $data['INS_BRANCH_NAME'], $data['CUST_ID'], $data['CUST_NAME']);


			$this->_db->beginTransaction();

			$dataInsert = [
				'CHANGES_ID'		=> $change_id,
				'INS_BRANCH_CODE'	=> $data['INS_BRANCH_CODE'],
				'CUST_ID'			=> $data['CUST_ID'],
				'INS_BRANCH_NAME'	=> $data['INS_BRANCH_NAME'],
				'INS_BRANCH_EMAIL'	=> $data['INS_BRANCH_EMAIL'],
				'INS_BRANCH_ACCT'	=> $data['INS_BRANCH_ACCT'],
				'INS_BRANCH_CCY'	=> $data['INS_BRANCH_CCY'],
				'ACCT_TYPE'			=> $data['ACCT_TYPE'],
				'FLAG'				=> $data['FLAG'],
				'LAST_SUGGESTED'	=> new Zend_Db_Expr('now()'),
				'LAST_SUGGESTEDBY'	=> $this->_userIdLogin
			];
			$this->_db->insert('TEMP_INS_BRANCH', $dataInsert);

			Application_Helper_General::writeLog('MIBO', 'Insurance Branch has been updated (delete) ' . $cust_name . ' (' . $cust_id . ')');

			$this->_db->commit();



			//$where1 = ['INS_BRANCH_CODE = ?' => $branch_code];
			//$this->_db->delete('M_INS_BRANCH', $where1);

			$this->setbackURL('/insurancebranch');

			$this->_redirect('/notification/submited/index');

			//$this->_redirect("/notification/success");
		}
	}

	public function checkcustomerAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();

		$model = new insurancebranch_Model_Insurancebranch();

		$request = $this->getRequest();

		$cust_id = $request->cust_id;

		$getCustAcctById = $model->getCustAcctById($cust_id);

		$custAcctArr = [];
		if ($getCustAcctById) {

			$getAllProduct = $this->_db->select()
				->from("M_PRODUCT")
				->query()->fetchAll();

			$newArr2 = [];
			foreach ($getCustAcctById as $keyArr => $arrAcct) {

				$accountTypeCheck = false;
				$productTypeCheck = false;

				$svcAccount = new Service_Account($arrAcct['ACCT_NO'], null, null, null, null, null);
				$result = $svcAccount->inquiryAccountBalance();

				if ($result['response_code'] == '0000') {
					$accountType = $result['account_type'];
					$productType = $result['product_type'];
				} else {
					$result = $svcAccount->inquiryDeposito();

					$accountType = $result['account_type'];
					$productType = $result['product_type'];
				}

				foreach ($getAllProduct as $key => $value) {
					# code...
					if ($value['PRODUCT_CODE'] == $accountType) {
						$accountTypeCheck = true;
					};

					if ($value['PRODUCT_PLAN'] == $productType) {
						$productTypeCheck = true;
					};
				}

				if ($accountTypeCheck && $productTypeCheck) $newArr2[] = $arrAcct;
			}

			foreach ($newArr2 as $row) {

				if (strpos(strtolower($row['ACCT_DESC']), "giro") !== false) {
					$custAcctArr[$row['ACCT_NO']] = $row['ACCT_NO'] . ' [' . $row['CCY_ID'] . '] / ' . $row['ACCT_NAME'] . ' / ' . $row['ACCT_DESC'];
				}
			}
		}


		$getTempData = $model->getTempData($cust_id);
		if ($getTempData) {
			$is_onboarding = true;
		} else {
			$is_onboarding = false;
		}

		echo json_encode(array('custAcctArr' => $custAcctArr, 'is_onboarding' => $is_onboarding));
	}

	public function checkaccountAction()
	{
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout->disableLayout();

		$model = new insurancebranch_Model_Insurancebranch();

		$request = $this->getRequest();

		$acct_no = $request->acct_no;

		$getCustAcctByAcctNo = $model->getCustAcctByAcctNo($acct_no);

		echo json_encode($getCustAcctByAcctNo);
	}
}
