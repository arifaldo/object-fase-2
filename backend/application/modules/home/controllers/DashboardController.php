<?php

class Home_DashboardController extends Application_Main
{

	public function initController()
	{
		$this->_helper->layout->setLayout('newlayout');
	}

	public function indexAction()
	{

		$auth = Zend_Auth::getInstance()->getIdentity();

		$privi = $this->_priviId;

		$set = new Settings();

		$timeoutpwd = $set->getSetting('pwd_expired_day');
		$daybefore = 5;

		$modelHome = new home_Model_Home();
		$result = $modelHome->checkIsRequireChangePwd($this->_userIdLogin, $timeoutpwd, $daybefore);

		if (is_array($result) && count($result) > 0) {
			$datetime 	= $result["datetime"];
			$dateadd 	= Application_Helper_General::convertDate($result["dateadd"], $this->view->displayDateFormat, $this->view->defaultDateFormat);

			$this->view->MSGBOX = 1;
			$this->view->dateadd 		= $dateadd;
		} else $this->view->MSGBOX = 0;


		$modelHome = new home_Model_Home();

		$select = $this->_db->select()
			->from(
				'M_NOTIFICATION',
				array('*')
			);
		$select->where("DATE (EFDATE) <= DATE(NOW())");
		#$select->where("TIME (EFDATE) <= TIME(NOW())");
		$select->where("DATE (EXP_DATE) >= DATE(NOW())");
		$select->where("TARGET IN ('2','3')");
		#$select->where("TIME (EXP_TIME) >= TIME(NOW())");
		// echo $select;die;
		$notif = $this->_db->fetchAll($select);
		foreach ($notif as $key => $value) {
			$not[] = $value['ID'];
		}

		// print_r($not);die;


		$selectforce = $this->_db->select()
			->from(
				'M_BFORCE_NOTIF',
				array('NOTIF_ID')
			);
		// $select->where("DATE (EFDATE) <= DATE(NOW())");
		#$select->where("TIME (EFDATE) <= TIME(NOW())");
		$selectforce->where("USER_ID = ? ", $this->_userIdLogin);
		if (!empty($not)) {
			$selectforce->where("NOTIF_ID IN (?)", $not);
		}
		// $select->where("TARGET IN ('1','3')");
		#$select->where("TIME (EXP_TIME) >= TIME(NOW())");
		// echo $selectforce;die;
		$forcenotif = $this->_db->fetchAll($selectforce);
		// print_r($notif);die;
		if (!empty($notif)) {
			$this->view->notif = $notif;
			$this->view->forcenotif = $forcenotif;
		}


		if (!empty($privi))		// kalo privilege is empty, gak bisa generate report, for all data
		{
			$changeModulePrivilegeObj  = new Changemanagement_Model_Privilege();
			$listAutModuleArr = $changeModulePrivilegeObj->getAuthorizeModule();

			$whPriviID = "'" . implode("','", $listAutModuleArr) . "'";


			$CHANGES_STATUS = array('WA', 'RR');

			$select = $this->_db->SELECT()
				->FROM(array('G' => 'T_GLOBAL_CHANGES'), array('DISPLAY_TABLENAME' => new Zend_Db_Expr("'Data Changes'")))
				->joinleft(array('B' => 'M_BUSER'), 'G.CREATED_BY = B.BUSER_ID', array('B.BUSER_BRANCH'))
				->joinleft(array('C' => 'M_BRANCH'), 'C.ID = B.BUSER_BRANCH', array('C.BRANCH_NAME', 'C.STATUS', 'JUMLAH' => new Zend_Db_Expr("COUNT(*)")))
				// ->joinleft(array('BG' => 'M_BGROUP'), 'BG.BGROUP_ID = B.BGROUP_ID',array('BG.BGROUP_CALL'))
				//->where('CHANGES_FLAG = '.$this->_db->quote('B'))
				->where("G.MODULE IN (" . $whPriviID . ")")
				->where('CHANGES_STATUS IN (?)', $CHANGES_STATUS);

			$select2 = $this->_db->select()
				->from(array('A' => 'M_BUSER'), array('DISPLAY_TABLENAME' => new Zend_Db_Expr("'Unlock Admin User'"), 'BUSER_BRANCH' => new Zend_Db_Expr("''"), 'BRANCH_NAME' => new Zend_Db_Expr("''"), 'STATUS' => new Zend_Db_Expr("''"), 'JUMLAH' => new Zend_Db_Expr("COUNT(*)")))
				->JOIN(array('C' => 'M_BRANCH'), 'C.ID = A.BUSER_BRANCH', array())
				->where("BUSER_RCHANGE = '0'")
				->where("BUSER_RRESET = '1'")
				->where("BUSER_STATUS != 3")
				->where("BUSER_ID NOT LIKE " . $this->_db->quote($this->_userIdLogin));

			$select3 = $this->_db->SELECT()
				->FROM(array('a' => 'M_USER'), array('DISPLAY_TABLENAME' => new Zend_Db_Expr("'Customer Reset Password'"), 'BUSER_BRANCH' => new Zend_Db_Expr("''"), 'BRANCH_NAME' => new Zend_Db_Expr("''"), 'STATUS' => new Zend_Db_Expr("''"), 'JUMLAH' => new Zend_Db_Expr("COUNT(*)")))
				->JOIN(array('b' => 'M_CUSTOMER'), 'a.CUST_ID = b.CUST_ID', array())
				->WHERE('b.CUST_STATUS != 3 ')
				->WHERE('a.USER_STATUS != 3 ')
				->WHERE('a.USER_RPWD_ISBYBO = 1 ');


			$select4 = $this->_db->SELECT()
				->FROM(array('D' => 'TEMP_ONLINECUST_BIS'), array())
				->joinleft(array('G' => 'T_GLOBAL_CHANGES'), 'G.CHANGES_ID = D.CHANGES_ID', array('DISPLAY_TABLENAME'))
				->joinleft(array('B' => 'M_BUSER'), 'G.CREATED_BY = B.BUSER_ID', array('B.BUSER_BRANCH'))
				->joinleft(array('C' => 'M_BRANCH'), 'C.ID = B.BUSER_BRANCH', array('C.BRANCH_NAME', 'C.STATUS', 'JUMLAH' => new Zend_Db_Expr("COUNT(*)")))
				// ->joinleft(array('BG' => 'M_BGROUP'), 'BG.BGROUP_ID = B.BGROUP_ID',array('BG.BGROUP_CALL'))
				//->where('CHANGES_FLAG = '.$this->_db->quote('B'))
				->where("G.MODULE IN ('registerechannel')")
				->where('CHANGES_STATUS IN (?)', 'WA');
			//->where('CHANGES_STATUS IN (?)',$CHANGES_STATUS);
			$select5 = $this->_db->SELECT()
				->FROM(array('G' => 'T_GLOBAL_CHANGES'), array('DISPLAY_TABLENAME' => new Zend_Db_Expr("'Operation Changes'")))
				->joinleft(array('B' => 'M_BUSER'), 'G.CREATED_BY = B.BUSER_ID', array('B.BUSER_BRANCH'))
				->joinleft(array('C' => 'M_BRANCH'), 'C.ID = B.BUSER_BRANCH', array('C.BRANCH_NAME', 'C.STATUS', 'JUMLAH' => new Zend_Db_Expr("COUNT(*)")))
				// ->joinleft(array('BG' => 'M_BGROUP'), 'BG.BGROUP_ID = B.BGROUP_ID',array('BG.BGROUP_CALL'))
				//->where('CHANGES_FLAG = '.$this->_db->quote('B'))
				->where("G.MODULE IN ('insurancebranch','marginaldeposit','linefacility','requestrebate')")
				->where('CHANGES_STATUS IN (?)', $CHANGES_STATUS);
			//die();
			/*$selectfo = $this->_db->SELECT()
							->FROM(array('G' => 'T_GLOBAL_CHANGES'),array('DISPLAY_TABLENAME'))
							->joinleft(array('B' => 'M_BUSER'), 'G.CREATED_BY = B.BUSER_ID',array('B.BUSER_BRANCH'))
							->joinleft(array('C' => 'M_BRANCH'), 'C.ID = B.BUSER_BRANCH',array('C.BRANCH_NAME','C.STATUS'))
							// ->joinleft(array('BG' => 'M_BGROUP'), 'BG.BGROUP_ID = B.BGROUP_ID',array('BG.BGROUP_CALL'))
							->where('CHANGES_FLAG = '.$this->_db->quote('F'))
							->where("G.MODULE IN (" . $whPriviID . ")")
							->where('CHANGES_STATUS IN (?)',$CHANGES_STATUS);

			$select = $this->_db->select()->distinct()->union(array($selectbo, $selectfo));
*/
			if ($auth->userHeadQuarter == "NO") {
				$select->where('C.ID="' . $auth->userBranchId . '"');
				$select2->where('C.ID="' . $auth->userBranchId . '"');
				$select4->where('C.ID="' . $auth->userBranchId . '"');
				$select5->where('C.ID="' . $auth->userBranchId . '"');
			}
			$select = $this->_db->select()->distinct()->union(array($select, $select2, $select3, $select4, $select5));
			//$select->where('JUMLAH != 0');



			$result = $this->_db->fetchAll($select);
			// $result = $modelHome->getChangesData($whPriviID);
			//echo "<pre>";
			//var_dump($result);
			$waitingAutorization = array();
			foreach ($result as $row) {
				if ($row['DISPLAY_TABLENAME'] != "") {
					if ($row['JUMLAH'] != 0) {
						if (isset($waitingAutorization[$row['DISPLAY_TABLENAME']])) {
							$waitingAutorization[$row['DISPLAY_TABLENAME']] += $row['JUMLAH'];
						} else {
							$waitingAutorization[$row['DISPLAY_TABLENAME']] = $row['JUMLAH'];
						}
					}
				}
			}

			$this->view->waitingAutorization = $waitingAutorization;
		}
	}


	public function savenotifAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$tblName = $this->_getParam('id');
		$param = array(
			'USER_ID' => $this->_userIdLogin,
			'NOTIF_ID' => $tblName,
			'CREATED' => new Zend_Db_Expr("now()")
		);
		// try {
		$this->_db->insert('M_BFORCE_NOTIF', $param);
		// } catch (Exception $e) {
		// return $e;
		// }


		// echo $optHtml;
	}
}
