<?php
Class remittancechar_Model_Remittancechar {
	
	protected $_db;

    // constructor
	public function __construct()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
	}
  
    public function getRemittanceType($year,$custid)
    {
		// $sqlQueryHoliday = "
		// 	SELECT * FROM
		// 	(SELECT 
		// 	  CHARGE_ID,
		// 	  CCY_ID,
			  
		// 	  CHARGE_AMT,
		// 	  CHARGE_AMOUNT_CCY,
		// 	  CHARGE_PROV_MIN_AMT,
		// 	  CHARGE_TYPE,
		// 	  CHARGE_PROV_MAX_AMT,
		// 	  CHARGE_PCT 
		// 	FROM
		// 	  `M_MINAMT_CCY` A 
		// 	  LEFT JOIN `M_CHARGES_REMITTANCE` B 
		// 	    ON A.`CCY_ID` = B.`CHARGE_CCY` 
		// 	WHERE (
		// 	    CHARGE_TYPE IS NULL 
		// 	    OR CHARGE_TYPE = ?
		// 	  ) AND (CUST_ID IS NULL OR CUST_ID = ?) 
			  
		// 	ORDER BY `CHARGE_ID` DESC
		// 	) a GROUP BY `CCY_ID`";
		
  //      return $this->_db->fetchAll($sqlQueryHoliday,array($year,$custid));

    	$sqlQueryHoliday = "
			SELECT 
				CHARGE_ID,
				CCY_ID,
				CHARGE_AMT,
				CHARGE_AMOUNT_CCY,
				CHARGE_PROV_MIN_AMT,
				CHARGE_TYPE,
				CHARGE_PROV_MAX_AMT,
				CHARGE_PCT 
			FROM `M_MINAMT_CCY` A
			LEFT JOIN
			(SELECT * 
				FROM M_CHARGES_REMITTANCE  
				WHERE CHARGE_TYPE = ?
				AND CUST_ID = ?) B
			ON A.`CCY_ID` = B.`CHARGE_CCY`";
			//ORDER BY CHARGE_ID DESC";
		
       return $this->_db->fetchAll($sqlQueryHoliday,array($year,$custid));
    }
    
    public function getRemittanceTypeIndex($year,$custid=NULL)
    {
   //  	$sqlQueryHoliday = "
			// SELECT
			//   CCY_ID,
			//   CHARGE_AMOUNT_CCY,
			//   CHARGE_AMT,
			//   CHARGE_AMOUNT_CCY,
			//   CHARGE_PROV_MIN_AMT,
			//   CHARGE_TYPE,
			//   CHARGE_PROV_MAX_AMT,
			//   CHARGE_PCT
			// FROM
			//   `M_MINAMT_CCY` A
			// LEFT JOIN `M_CHARGES_REMITTANCE` B
		 //    ON A.`CCY_ID` = B.`CHARGE_CCY`
		 //    WHERE (CHARGE_AMT != '0.00' OR CHARGE_AMT IS NULL) AND (CHARGE_TYPE=?)";

		 if ($custid){
			$whereCustId = " AND CUST_ID = 'GLOBAL'";
		 }
    $sqlQueryHoliday = "
    			SELECT * FROM (		
		SELECT
			CHARGE_ID,
			  CCY_ID,
			  CHARGE_AMOUNT_CCY,
			  CHARGE_AMT,
			  
			  CHARGE_PROV_MIN_AMT,
			  CHARGE_TYPE,
			  CHARGE_PROV_MAX_AMT,
			  CHARGE_PCT
			FROM
			  `M_MINAMT_CCY` A
			LEFT JOIN `M_CHARGES_REMITTANCE` B
		    ON A.`CCY_ID` = B.`CHARGE_CCY`
		    WHERE (CHARGE_AMT != '0.00' OR CHARGE_AMT IS NULL) AND (CHARGE_TYPE=?) $whereCustId
		    ORDER BY `CHARGE_ID` DESC
	) e	    
		    GROUP BY `CCY_ID`,`CHARGE_AMOUNT_CCY`";
    	return $this->_db->fetchAll($sqlQueryHoliday,$year);
    }
    
    public function getRemittanceTypeIndexTemp($year,$idchange)
    {
//     	$sqlQueryHoliday = "
// 			SELECT
// 			  CCY_ID,
// 			  CHARGE_AMOUNT_CCY,
// 			  CHARGE_AMT,
// 			  CHARGE_AMOUNT_CCY,
// 			  CHARGE_PROV_MIN_AMT,
// 			  CHARGE_TYPE,
// 			  CHARGE_PROV_MAX_AMT,
// 			  CHARGE_PCT
// 			FROM
// 			  `M_MINAMT_CCY` A
// 			LEFT JOIN `M_CHARGES_REMITTANCE` B
// 		    ON A.`CCY_ID` = B.`CHARGE_CCY`
// 		    WHERE (CHARGE_TYPE=?)";

	$cekglobal = $this->_db->select()
    			->from(array('A' => 'M_MINAMT_CCY'),array('*'))
    			->joinleft(array('B' => 'TEMP_CHARGES_REMITTANCE'), 'A.CCY_ID = B.CHARGE_CCY',array('*'))
    			->where('B.CHANGES_ID = ?' , $idchange)
    			->where('B.CUST_ID = ?' , 'GLOBAL')

				->where('B.CHARGE_TYPE= ?' , $year)
				->order('B.TEMP_ID ASC');
    	$globaldata = $this->_db->fetchAll($cekglobal);
	if(!empty($globaldata)){
		return $globaldata;
	}else{
    	$cek = $this->_db->select()
    			->from(array('A' => 'M_MINAMT_CCY'),array('*'))
    			->joinleft(array('B' => 'TEMP_CHARGES_REMITTANCE'), 'A.CCY_ID = B.CHARGE_CCY',array('*'))
    			->where('B.CHANGES_ID = ?' , $idchange)
    			
				->where('B.CHARGE_TYPE= ?' , $year)
				->order('B.TEMP_ID ASC');
	    	return $this->_db->fetchAll($cek);
	}
    //	echo $cek;die;
//     	print_r($cek->query());die;

//     	return $this->_db->fetchAll($sqlQueryHoliday,$year);
    }
    
    public function getCheckRemittanceType($ccy,$ccyamount,$year,$custid)
    {
    	
    	$cek = $this->_db->fetchone($this->_db->select()
    			->from(array('C' => 'M_MINAMT_CCY'),array('*'))
    			->joinleft(array('D' => 'M_CHARGES_REMITTANCE'), 'C.CCY_ID = D.CHARGE_CCY',array('*'))
    			->where('CUST_ID = ?', $custid)
    			->where('CCY_ID = ?' , $ccy)
    			->where('CHARGE_AMOUNT_CCY= ?' , $ccyamount)
    			->where('CHARGE_TYPE= ?' , $year));
    	
    	return $cek;
    }
    
    public function getCCYList($year=null)
    {
    	$data = $this->_db->select()
    	->from('M_MINAMT_CCY')
    	->query()->fetchall();
    	//print_r($data);die;
    	return $data;
    }
  
    public function getTempHoliday($year)
    {
		$sqlQueryHoliday = "
			SELECT *,
			DAY(HOLIDAY_DATE) AS day,
			MONTH(HOLIDAY_DATE) AS month,
			YEAR(HOLIDAY_DATE) AS year
			FROM TEMP_HOLIDAY WHERE HOLIDAY_YEAR = ? ";
			
       return $this->_db->fetchAll($sqlQueryHoliday,$year);
    }
  
    public function getTempYear($changes_id)
    {
		$year = $this->_db->fetchOne(
	  				$this->_db->select()
	  				->from('TEMP_HOLIDAY',array('HOLIDAY_YEAR'))
					->where('CHANGES_ID = ?', $changes_id)
	  			);
		
       return $year;
    }
  
    public function getChangesReason($changes_id)
    {
		$changes_reason = $this->_db->fetchOne(
								$this->_db->select()
								->from('T_GLOBAL_CHANGES',array('CHANGES_REASON'))
								->where('CHANGES_ID = ?', $changes_id)
							);
		
       return $changes_reason;
    }
  
    public function cekTemp()
    {
		$data = $this->_db->select()
						->from('TEMP_CHARGES_REMITTANCE')
						->query()->fetchall();
		
		return $data;
    }
  
    public function cekTempId($changes_id)
    {
		$data = $this->_db->select()
							->from('TEMP_HOLIDAY')
							->where('CHANGES_ID = ?',$changes_id)
							->query()->fetchall();
		
		return $data;
    }
  
    public function insertTemp($data)
    {
		$this->_db->insert('TEMP_CHARGES_REMITTANCE',$data);	
    }
  
    public function deleteTemp($changes_id)
    {
		$where = array('CHANGES_ID = ?' => $changes_id);
		$this->_db->delete('TEMP_HOLIDAY',$where);
    }
  
    public function cekChanges($changesid)
    {
		$select = $this->_db->SELECT()
							->FROM(array('A' => 'T_GLOBAL_CHANGES'),array('*'))
							->WHERE("A.CHANGES_ID = ?",$changesid)
							->WHERE("A.CHANGES_STATUS = 'WA' OR A.CHANGES_STATUS = 'RR'");
		
		return $this->_db->fetchRow($select);
    }
  
    public function getChangesData($changesid)
    {
		$data	= $this->_db->FetchRow("SELECT CHANGES_STATUS , CREATED , CREATED_BY  FROM T_GLOBAL_CHANGES WHERE CHANGES_ID = ".$this->_db->quote($changesid));
			
		return $data;
    }
}