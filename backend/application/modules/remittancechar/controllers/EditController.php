<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/BankUser.php';

class Remittancechar_EditController extends Application_Main {
	/**
	 * The default action - show the home page
	 */
	
	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		$charge_type 	= $this->_getParam('id');
		$cust_id = $this->_getParam('custid');
		$cust_name = $this->_custNameLogin;
		
		if($charge_type=='3'){
			$this->view->title = 'Transfer Fee';
		}
		if($charge_type == '4'){
			$this->view->title = 'Full Amount Fee';
		}
		if($charge_type == '5'){
			$this->view->title = 'Provision Fee';
		}	
		
		$model = new remittancechar_Model_Remittancechar();
		
		$this->view->ccyArr = ( array(''=>'-')+Application_Helper_Array::listArray($model->getCCYList(),'CCY_ID','CCY_ID'));
// 		$this->view->paginator = $model->getCCYList();
		$buser_id = $this->_userIdLogin;
		$this->view->id 	= $charge_type;
		$this->view->custid = $cust_id;
		
		$this->view->curencylist = $model->getCCYList();
		$this->view->paginator = $model->getRemittanceType($charge_type,$cust_id);
		
		$settings =  new Settings();

		
		

		if($this->_request->isPost())
		{
			$errDesc = array();
			$ccylist = $model->getCCYList();
			$params = $this->getRequest()->getParams();

			//$params_ccy = $this->getRequest()->getParams('ccy');
// 			print_r($params_ccy);
			if($charge_type=='3'){
				foreach ($params['ccy'] as $value){
// 					print_r($value);
					$amount_data = 'amount'.$value;  
					$amount_data = $params[$amount_data];
					
					$ccy_amount = 'ccy'.$value;
					$ccy_amount_data = $params[$ccy_amount];
					
					if($params[$ccy_amount]=='-'){
						$checkdata = true;
					}else{
						$checkdata = $model->getCheckRemittanceType($value,$ccy_amount_data,$charge_type,$cust_id);
					}
					if(empty($checkdata) || $checkdata==true){
						
						
						
					}else{
						$msg = 'Error: Same currency are not allowed in full amount fee';
						break;	
					}
					
				}
			}
			if($charge_type=='4'){
				foreach ($params['ccy'] as $value){
					// 					print_r($value);
					$amount_data = 'amount'.$value;
					$amount_data = $params[$amount_data];
						
					$ccy_amount = 'ccy'.$value;
					$ccy_amount_data = $params[$ccy_amount];
						
					if($params[$ccy_amount]=='-'){
						$checkdata = true;
					}else{
						$checkdata = $model->getCheckRemittanceType($value,$ccy_amount_data,$charge_type,$cust_id);
					}
					if(empty($checkdata) || $checkdata==true){
			
			
			
					}else{
						$msg = 'Error: Same currency are not allowed in full amount fee';
						break;
					}
						
				}
			}
			if($charge_type=='5'){
				foreach ($params['ccy'] as $value){
					// 					print_r($value);
					$amount_data = 'amount'.$value;
					$amount_data = $params[$amount_data];
			
					$ccy_amount = 'ccy'.$value;
					$ccy_amount_data = $params[$ccy_amount];
			
					if($params[$ccy_amount]=='-'){
						$checkdata = true;
					}else{
						$checkdata = $model->getCheckRemittanceType($value,$ccy_amount_data,$charge_type,$cust_id);
					}
					if(empty($checkdata) || $checkdata==true){
							
							
							
					}else{
						$msg = 'Error: Same currency are not allowed in full amount fee';
						break;
					}
			
				}
			}
			
			$err = false;

			if(empty($msg))
			{
					if($charge_type=='3'){
						$info = 'Remittance Charges';
						$info2 = 'Set Edit New Charges';
						
						$change = 'true';
						foreach ($params['ccy'] as $value){
							$amount_data = 'amount'.$value;
							$ccy_amount = 'ccy'.$value;
							$charge_amt =	Application_Helper_General::convertDisplayMoney($params[$amount_data]);
								
							if(trim($params[$ccy_amount]) == '' && $charge_amt > 0){
								$change = 'err';

							}
						
						}
						// 						print_r($change);die;
						// 						print_r('here');
						if($change != 'err'){
						$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['new'],null,'M_CHARGES_REMITTANCE','TEMP_CHARGES_REMITTANCE','-','Transfer Fee',$cust_id,$cust_name);
						foreach ($params['ccy'] as $value){
							$amount_data = 'amount'.$value;  
// 							$amount_data = $this->getRequest()->getParams($amount_data);
							
							$ccy_amount = 'ccy'.$value;
// 							$ccy_amount_data = $this->getRequest()->getParams($ccy_amount);
							
							$charge_amt =	Application_Helper_General::convertDisplayMoney($params[$amount_data]);
// 							$this->_setParam('min_amount_skn',$min_amount_skn_val);

							
								
							
							
							
								$params_insert = array(
										'CHANGES_ID' => $change_id,
										'CUST_ID' => $cust_id,
										'CHARGE_TYPE' => $charge_type,
										'CHARGE_CCY' => $value,
										'CHARGE_AMOUNT_CCY' => $params[$ccy_amount],
										'CHARGE_AMT' => $charge_amt
										
								);
								if($params[$ccy_amount]=='-'){
								
								}else{
										$result = $model->insertTemp($params_insert);
								}
								if($cust_id=='GLOBAL'){
								$CustList	= $this->_db->FetchAll("SELECT `CUST_ID` FROM `M_CHARGES_REMITTANCE` WHERE CUST_ID != 'GLOBAL' GROUP BY `CUST_ID`");
								foreach ($CustList as $val){
								    $params_insert = array(
								        'CHANGES_ID' => $change_id,
								        'CUST_ID' => $val['CUST_ID'],
								        'CHARGE_TYPE' => $charge_type,
								        'CHARGE_CCY' => $value,
								        'CHARGE_AMOUNT_CCY' => $params[$ccy_amount],
								        'CHARGE_AMT' => $charge_amt
								    
								    );
								    if($params[$ccy_amount]=='-'){
								    
								    }else{
								        $result = $model->insertTemp($params_insert);
								    }
								}
								
								}
								
						}
						}
					}
					if($charge_type=='4'){
						$info = 'Remittance Charges';
						$info2 = 'Set Edit New Charges';
						
						$change = 'true';
						foreach ($params['ccy'] as $value){
							$amount_data = 'amount'.$value;
							$ccy_amount = 'ccy'.$value;
							$charge_amt =	Application_Helper_General::convertDisplayMoney($params[$amount_data]);
						
							if(trim($params[$ccy_amount]) == '' && $charge_amt > 0){
								$change = 'err';
						
							}
						
						}
						// 						print_r($change);die;
						// 						print_r('here');
						if($change != 'err'){
						$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['new'],null,'M_CHARGES_REMITTANCE','TEMP_CHARGES_REMITTANCE','-','Full Amount Fee',$cust_id,$cust_name);
						foreach ($params['ccy'] as $value){
							$amount_data = 'amount'.$value;
							// 							$amount_data = $this->getRequest()->getParams($amount_data);
								
							$ccy_amount = 'ccy'.$value;
							// 							$ccy_amount_data = $this->getRequest()->getParams($ccy_amount);
								
							$charge_amt =	Application_Helper_General::convertDisplayMoney($params[$amount_data]);
							// 							$this->_setParam('min_amount_skn',$min_amount_skn_val);
							
							
							
							
							$params_insert = array(
									'CHANGES_ID' => $change_id,
									'CUST_ID' => $cust_id,
									'CHARGE_TYPE' => $charge_type,
									'CHARGE_CCY' => $value,
									'CHARGE_AMOUNT_CCY' => $params[$ccy_amount],
									'CHARGE_AMT' => $charge_amt
					
							);
							if($params[$ccy_amount]=='-'){
					
							}else{
								$result = $model->insertTemp($params_insert);
							}
							if($cust_id=='GLOBAL'){
							$CustList	= $this->_db->FetchAll("SELECT `CUST_ID` FROM `M_CHARGES_REMITTANCE` WHERE CUST_ID != 'GLOBAL' GROUP BY `CUST_ID`");
							foreach ($CustList as $val){
							    $params_insert = array(
							        'CHANGES_ID' => $change_id,
							        'CUST_ID' => $val['CUST_ID'],
							        'CHARGE_TYPE' => $charge_type,
							        'CHARGE_CCY' => $value,
							        'CHARGE_AMOUNT_CCY' => $params[$ccy_amount],
							        'CHARGE_AMT' => $charge_amt
							        	
							    );
							    if($params[$ccy_amount]=='-'){
							        	
							    }else{
							        $result = $model->insertTemp($params_insert);
							    }
							    
							}
							
							}
					
						}
						}
					}
					if($charge_type=='5'){
						$info = 'Remittance Charges';
						$info2 = 'Set Edit New Charges';
						
						$change = 'true';
						foreach ($params['ccy'] as $value){
							$amount_min = 'min'.$value;
							$amount_max = 'max'.$value;
							$ccy_amount = 'ccy'.$value;
							$charge_min =	Application_Helper_General::convertDisplayMoney($params[$amount_min]);
							$charge_max =	Application_Helper_General::convertDisplayMoney($params[$amount_max]);
								
							if(trim($params[$ccy_amount]) == '' && ($charge_min > 0 || $charge_max > 0)){
								$change = 'err';
								// 								print_r('here');die;
							}
						
						}
						// 						print_r($params['ccy']);die;
						if($change != 'err'){
						$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['new'],null,'M_CHARGES_REMITTANCE','TEMP_CHARGES_REMITTANCE','-','Provision Fee',$cust_id,$cust_name);
						
						foreach ($params['ccy'] as $value){
							$amount_min = 'min'.$value;
							$amount_max = 'max'.$value;
							$amount_ccy = 'ccy'.$value;
							// 							$amount_data = $this->getRequest()->getParams($amount_data);
					
							$pcy_amount = 'pcy'.$value;
							// 							$ccy_amount_data = $this->getRequest()->getParams($ccy_amount);
					
							$charge_amt =	Application_Helper_General::convertDisplayMoney($params[$amount_data]);
							// 							$this->_setParam('min_amount_skn',$min_amount_skn_val);
							
								
							
								
							$params_insert = array(
									'CHANGES_ID' => $change_id,
									'CUST_ID' => $cust_id,
									'CHARGE_TYPE' => $charge_type,
									'CHARGE_CCY' => $value,
									'CHARGE_AMOUNT_CCY' => $params[$amount_ccy],
									'CHARGE_PROV_MIN_AMT' => $params[$amount_min],
									'CHARGE_PROV_MAX_AMT' => $params[$amount_max],
									'CHARGE_PCT' => $params[$pcy_amount],
									
										
							);
							
// 							print_r($params_insert);die;
							if($params[$ccy_amount]=='-'){
									
							}else{
								$result = $model->insertTemp($params_insert);
							}
							
							if($cust_id=='GLOBAL'){
							$CustList	= $this->_db->FetchAll("SELECT `CUST_ID` FROM `M_CHARGES_REMITTANCE` WHERE CUST_ID != 'GLOBAL' GROUP BY `CUST_ID`");
							foreach ($CustList as $val){
    							$params_insert = array(
    									'CHANGES_ID' => $change_id,
    									'CUST_ID' => $val['CUST_ID'],
    									'CHARGE_TYPE' => $charge_type,
    									'CHARGE_CCY' => $value,
    									'CHARGE_AMOUNT_CCY' => $params[$amount_ccy],
    									'CHARGE_PROV_MIN_AMT' => $params[$amount_min],
    									'CHARGE_PROV_MAX_AMT' => $params[$amount_max],
    									'CHARGE_PCT' => $params[$pcy_amount],
    									
    										
    							);
    							
    // 							print_r($params_insert);die;
    							if($params[$ccy_amount]=='-'){
    									
    							}else{
    								$result = $model->insertTemp($params_insert);
    							}
							    	
							}
							
							}
							
							
							}		
						}
					}
					if($cust_id=='GLOBAL'){
					$this->setbackUrl('/setglobalcompanycharges');
					}else{
					    $this->setbackUrl('/setcompanycharges');
					}
					if(!$err && $change != 'err'){
						// 						echo 'here';
						
						$this->_redirect('/notification/submited/index');
					}else{
					
						$msg = 'Error: Currency cannot be left blank.';
						$this->view->error 	= true;
						$this->view->msg_failed = $msg;
						$this->view->errDesc = $errDesc;
						// 						echo 'here2';
					}
					
					
					
					
			}
			else
			{
				$this->view->error 	= true;
				$this->view->msg_failed = 'Error in processing form values. Please correct values and re-submit';
				$this->view->errDesc = $errDesc;
			}
		}
		Application_Helper_General::writeLog('CHOP','Set Edit Remittance Charges');
	}

}

