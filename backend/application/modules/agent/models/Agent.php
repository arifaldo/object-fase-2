<?php

Class agent_Model_Agent
{
	protected $_db;

    // constructor
	public function __construct()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
	}

	public function insertTempAgent($change_id,$agent_data)
	{
		$agent_data['CHANGES_ID'] = $change_id;
		// echo "<pre>";
		// print_r($agent_data);die;
		$this->_db->insert('TEMP_USER_LKP',$agent_data);
	}

	public function forceLogout($user_id)
	{
		$where = ' UPPER(USER_ID)='.$this->_db->quote((string)$user_id);
		$this->_db->update('M_USER',array('USER_ISLOGIN'=>0),$where);
	}

	function getMaster($USER_ID=null,$selectType='fetchAll',$param=null)
	{
		$select = $this->_db->select()
		->from(array('T'=>'M_USER'));
		if(isset($param['USER_STATUS'])) $select->where('UPPER(USER_STATUS)='.$this->_db->quote((string)$param['USER_STATUS']));
		return $select->query()->$selectType();
	}

	function getMasterDetail($user_id)
	{
		$select = $this->_db->select()
		->from(array('T'=>'M_USER'))
		->where('UPPER(USER_ID) = ?', (string)$user_id);
		return $this->_db->fetchRow($select);
	}

	function getAgentData($user_id)
	{
		$select = $this->_db->select()
		->from(array('T'=>'M_USER_LKP'))
		->where('UPPER(USER_ID) = ?', (string)$user_id);
		return $this->_db->fetchRow($select);
	}

	public function getUserData($param = null)
	{
		$select = $this->_db->select()
		->from(array('L'=>'M_USER_LKP'),array())
	                           // ->join(array('MU'=>'M_USER'),'L.USER_ID = MU.USER_ID',array())
		->join(array('B'=>'M_BRANCH'),'L.USER_BRANCH_CODE = B.BRANCH_CODE',array('L.*','B.BRANCH_NAME','USER_ID' => 'L.USER_ID','AGENT_STATUS'=>'L.USER_STATUS','USER_MOBILE_PHONE'=>'L.USER_MOBILE_PHONE','USER_EMAIL'=>'L.USER_EMAIL','USER_SUGGESTED'=>'L.USER_SUGGESTED','USER_SUGGESTEDBY'=>'L.USER_SUGGESTEDBY','USER_UPDATED'=>'L.USER_UPDATED','USER_UPDATEDBY'=>'L.USER_UPDATEDBY','B.BRANCH_NAME','L.USER_CITY','L.USER_PROVINCE','L.USER_FULLNAME'));
                           // echo $select;die;
		if(!empty($param['user_id'])) $select->where('UPPER(L.USER_ID)='.$this->_db->quote((string)$param['user_id']));
		return $select->query()->fetchAll();
	}

	public function updateTempUser($changes_id,$user_data)
	{
		$user_mobile_phone = $user_data['USER_MOBILE_PHONE'];

		if(empty($user_mobile_phone)){
			$mtoken_isactivated = '0';
		}
		else{
			$mtoken_isactivated = '1';
		}

		$content = array(
			'USER_FULLNAME'  => $user_data['USER_FULLNAME'],
			'USER_EMAIL'     => $user_data['USER_EMAIL'],
			'USER_MOBILE_PHONE'     => $user_data['USER_MOBILE_PHONE'],
			'TOKEN_ID'       => $user_data['TOKEN_ID'],

			'USER_CIF'       			=> $user_data['USER_CIF'],
			'USER_ADDRESS'      	=> $user_data['USER_ADDRESS'],
			'USER_CITY'      			=> $user_data['USER_CITY'],
			'USER_ZIP'     			 	=> $user_data['USER_ZIP'],
			'USER_PROVINCE'     => $user_data['USER_PROVINCE'],
			'USER_COUNTRY'      => $user_data['USER_COUNTRY'],
			'MTOKEN_ISACTIVATED'   => $mtoken_isactivated,
			'TOKEN_TYPE'   => $user_data['TOKEN_TYPE'],
			'ADDITIONAL_DATA'   => $user_data['ADDITIONAL_DATA'],
		);

		$whereArr = array('CHANGES_ID = ?'=>$changes_id);
		$update = $this->_db->update('TEMP_USER',$content,$whereArr);

	}

	public function setUserPasswdIsEmailed($param = array())
	{
		$user 	= $param['user_id'];
		$field 	= $param['user_isemailed'];
		$field 	= (isset($field) && !(empty($field)) ? "1" : "0");
		$column = 'USER_RPWD_ISEMAILED';

		if (isset($user) && !(empty($user)) && isset($field) && !(empty($field))){
			$content[$column] = 1;
			$where = array('USER_ID = ?' => $user);
			$update = $this->_db->update('M_USER',$content,$where);
		}
	}

	public function setUserPasswdIsPrinted($param = array())
	{
		$user 	= $param['user_id'];
		$field 	= $param['user_isprinted'];
		$field 	= (isset($field) && !(empty($field)) ? "1" : "0");
		$column = 'USER_RPWD_ISPRINTED';

		if (isset($user) && !(empty($user)) && isset($field) && !(empty($field))){
			$content[$column] = 1;
			$where = array('USER_ID = ?' => $user);
			$update = $this->_db->update('M_USER',$content,$where);
		}
	}

	public function getTempAgent($changes_id)
	{
		$select = $this->_db->select()
		->from(array('T'=>'TEMP_USER_LKP'))
		->join(array('G' => 'T_GLOBAL_CHANGES'), 'G.CHANGES_ID = T.CHANGES_ID', array('CHANGES_ID','CHANGES_TYPE','CREATED','CREATED_BY','CHANGES_STATUS','READ_STATUS'))
		->where('T.CHANGES_ID = ?', $changes_id);
		$resultdata = $this->_db->fetchRow($select);
		return $resultdata;
	}

	public function getTempUserFromID($user_id)
	{
		$select = $this->_db->select()
		->from(array('T'=>'TEMP_USER_LKP'))
		->where('T.USER_ID = ?', $user_id);
		$resultdata = $this->_db->fetchRow($select);
		return $resultdata;
	}

	public function getServiceArea()
	{
		$select = $this->_db->select()
		->distinct()
		->from('M_SERVICE_AREA',array('AREA_NAME'))
		->query()->fetchAll();
		return $select;
	}

	public function getBranch()
	{
		$select = $this->_db->select()
		->from('M_BANK_BRANCH',array('CODE','NAME'))
		->query()->fetchAll();
		return $select;
	}

// 	public function getUnlockResetList($param)
// 	{
// //		$arrUSER_ISEMAILPWD = array(1=>"Email", 0=>"Postal Mail");
// 		$arrUSER_ISEMAILPWD = array(1=>"Email", 0=>"SMS");
// 		$arrUSER_ISLOCKED = array(0=>"No", 1=>"Yes");

// 		$caseUSER_ISEMAILPWD = "(CASE a.USER_ISEMAILPWD ";
// 		foreach($arrUSER_ISEMAILPWD as $key=>$val)
// 		{
// 			$caseUSER_ISEMAILPWD .= " WHEN ".$key." THEN '".$val."'";
// 		}
// 		$caseUSER_ISEMAILPWD .= " END)";

// 		$caseUSER_ISLOCKED = "(CASE a.USER_ISLOCKED ";
// 		foreach($arrUSER_ISLOCKED as $key=>$val)
// 		{
// 			$caseUSER_ISLOCKED .= " WHEN ".$key." THEN '".$val."'";
// 		}
// 		$caseUSER_ISLOCKED .= " END)";

// 		$select = $this->_db->SELECT()
// 										->FROM(array('a' => 'M_USER') ,array('a.CUST_ID','a.USER_ID','a.USER_FULLNAME','USER_ISLOCKED' => $caseUSER_ISLOCKED,'a.USER_LOCKREASON','USER_RPWD_ISBYBO','USER_RRESET','USER_ISEMAILPWD' => $caseUSER_ISEMAILPWD,'USER_RPWD_ISBYUSER','USER_MOBILE_PHONE'))
// 										->WHERE('a.USER_RPWD_ISBYBO = 1 or a.USER_RRESET = 1 ' );
// 		if(isset($param['mobile_phone'])) $select->where("USER_MOBILE_PHONE LIKE ".$this->_db->quote('%'.$param['mobile_phone'].'%'));
// 		if(isset($param['lockstatus'])) $select->where("USER_ISLOCKED LIKE ".$this->_db->quote('%'.$param['lockstatus'].'%'));
// 		if(isset($param['user_fullname'])) $select->where("USER_FULLNAME LIKE ".$this->_db->quote('%'.$param['user_fullname'].'%'));
// 		if(isset($param['sortBy']) && isset($param['sortDir'])) $select->order($param['sortBy'].' '.$param['sortDir']);
// 		return $select->Query()->FETCHALL();
// 	}

// 	public function getPrintOrEmailPwd($param)
// 	{
// //		$arrUSER_ISEMAILPWD = array(1=>"Email", 0=>"Postal Mail");
// 		$arrUSER_ISEMAILPWD = array(1=>"Email", 0=>"SMS");
// 		$arrYesNo = array(1=>"Yes", 0=>"No");

// 		$caseUSER_ISEMAILPWD = "(CASE a.USER_ISEMAILPWD ";
// 		foreach($arrUSER_ISEMAILPWD as $key=>$val)
// 		{
// 			$caseUSER_ISEMAILPWD .= " WHEN ".$key." THEN '".$val."'";
// 		}
// 		$caseUSER_ISEMAILPWD .= " END)";

// 		$caseUSER_RPWD_ISEMAILED = "(CASE a.USER_RPWD_ISEMAILED ";
// 		foreach($arrYesNo as $key=>$val)
// 		{
// 			$caseUSER_RPWD_ISEMAILED .= " WHEN ".$key." THEN '".$val."'";
// 		}
// 		$caseUSER_RPWD_ISEMAILED .= " END)";

// 		$caseUSER_RPWD_ISPRINTED = "(CASE a.USER_RPWD_ISPRINTED ";
// 		foreach($arrYesNo as $key=>$val)
// 		{
// 			$caseUSER_RPWD_ISPRINTED .= " WHEN ".$key." THEN '".$val."'";
// 		}
// 		$caseUSER_RPWD_ISPRINTED .= " END)";

// 		$caseUSER_ISNEW = "(CASE a.USER_ISNEW ";
// 		foreach($arrYesNo as $key=>$val)
// 		{
// 			$caseUSER_ISNEW .= " WHEN ".$key." THEN '".$val."'";
// 		}
// 		$caseUSER_ISNEW .= " END)";

// 		$select = $this->_db->SELECT()
// 								->FROM(array('a' => 'M_USER'),array('a.USER_ID','a.USER_MOBILE_PHONE','a.USER_FULLNAME','a.USER_EMAIL',
// 																							'USER_ISNEW'=>$caseUSER_ISNEW,
// 																							'USER_RPWD_ISEMAILED'=>$caseUSER_RPWD_ISEMAILED,
// 																							'USER_RPWD_ISPRINTED'=>$caseUSER_RPWD_ISPRINTED,
// 																							'USER_ISEMAILPWD'=>$caseUSER_ISEMAILPWD))
// 								->WHERE('a.USER_ISREQUIRE_CHANGEPWD = 1 ')
// 								->WHERE('a.USER_STATUS != 3 ')
// 								->WHERE('a.USER_ISLOCKED <> 1 ')
// 								->WHERE('a.USER_RCHANGE = 1 ');
// 		if(isset($param['mobile_phone'])) $select->where("USER_MOBILE_PHONE LIKE ".$this->_db->quote('%'.$param['mobile_phone'].'%'));
// 		if(isset($param['user_isnew'])) $select->where("USER_ISNEW LIKE ".$this->_db->quote('%'.$param['user_isnew'].'%'));
// 		if(isset($param['user_fullname'])) $select->where("USER_FULLNAME LIKE ".$this->_db->quote('%'.$param['user_fullname'].'%'));
// 		if(isset($param['user_isemailed'])) $select->where("USER_RPWD_ISEMAILED LIKE ".$this->_db->quote('%'.$param['user_isemailed'].'%'));
// 		if(isset($param['user_isprinted'])) $select->where("USER_RPWD_ISPRINTED LIKE ".$this->_db->quote('%'.$param['user_isprinted'].'%'));
// 		if(isset($param['sortBy']) && isset($param['sortDir'])) $select->order($param['sortBy'].' '.$param['sortDir']);
// 		return $select->Query()->FETCHALL();
// 	}

	public function getPassTemplate($user_id)
	{
		$select = $this->_db->SELECT ()
		->FROM ('M_USER', array('USER_ID','USER_EMAIL','USER_FULLNAME','USER_CLEARTEXT_PWD'))
		->WHERE ('USER_ID = ? ', $user_id);
		return $this->_db->fetchRow($select);
	}
	public function getUserCif($userCif){
		 //Zend_Debug::dump($userCif);die;
		$select = $this->_db->select()
		->from(array('u'=>'M_USER'),array('USER_CIF'))
		->where('UPPER(u.USER_CIF)='.$this->_db->quote((string)$userCif))
		  				     //->where('USER_STATUS!=3');
		->query()->fetchAll();

		return $select;
	}
	public function getUserCifTmp($userCif){
		 //Zend_Debug::dump($userCif);die;
		$select = $this->_db->select()
		->from(array('u'=>'TEMP_USER'),array('USER_CIF'))
		->where('UPPER(u.USER_CIF)='.$this->_db->quote((string)$userCif))
		  				     //->where('USER_STATUS!=3');
		->query()->fetchAll();

		return $select;
	}
	public function getUserID($custId)
	{
		$select = $this->_db->select()
		->from(array('u'=>'M_USER'),array('USER_ID',
			'USER_FULLNAME',
			'USER_STATUS',
			'USER_EMAIL', /*+ ADDED BY SUHANDI +*/
			'USER_PHONE',/*+ ADDED BY SUHANDI +*/
			'USER_ISLOCKED',
			'USER_ISEMAIL',
			'USER_CREATED',/*+ ADDED BY SUHANDI +*/
			'USER_UPDATED',
			'USER_UPDATEDBY',
			'USER_SUGGESTED',
			'USER_SUGGESTEDBY'))
		  				     //->join(array('g'=>'M_FGROUP'),'g.FGROUP_ID=u.FGROUP_ID',array('FGROUP_NAME'))
		->where('UPPER(u.USER_ID)='.$this->_db->quote((string)$custId))
		  				     //->where('USER_STATUS!=3');
		->query()->fetchAll();

		return $select;
	}

	public function getUserIDTmp($custId)
	{
		$select = $this->_db->select()
		->from(array('u'=>'TEMP_USER'),array('USER_ID',
			'USER_FULLNAME',
			'USER_STATUS',
			'USER_EMAIL', /*+ ADDED BY SUHANDI +*/
			'USER_PHONE',/*+ ADDED BY SUHANDI +*/
			'USER_ISLOCKED',
			'USER_ISEMAIL',
			'USER_CREATED',/*+ ADDED BY SUHANDI +*/
			'USER_UPDATED',
			'USER_UPDATEDBY',
			'USER_SUGGESTED',
			'USER_SUGGESTEDBY'))
		  				     //->join(array('g'=>'M_FGROUP'),'g.FGROUP_ID=u.FGROUP_ID',array('FGROUP_NAME'))
		->where('UPPER(u.USER_ID)='.$this->_db->quote((string)$custId))
		  				     //->where('USER_STATUS!=3');
		->query()->fetchAll();

		return $select;
	}

	public function getUserAcct($userAcct)
	{
		$select = $this->_db->select()
		->from(array('u'=>'M_USER_ACCT'),array('USER_ID','ACCT_NO','ACCT_STATUS'))
		  				     //->join(array('g'=>'M_FGROUP'),'g.FGROUP_ID=u.FGROUP_ID',array('FGROUP_NAME'))
		->where('UPPER(u.ACCT_NO)='.$this->_db->quote((string)$userAcct))
		  				     //->where('USER_STATUS!=3');
		->query()->fetchAll();

		return $select;
	}

	public function getUseridAcctPhone($userAcct, $userPhone = NULL)
	{
		$select = $this->_db->select()
		->from(array('u'=>'M_USER_ACCT'),array('USER_ID','ACCT_NO','ACCT_STATUS'))
// 	  		->join(array('g'=>'M_FGROUP'),'g.FGROUP_ID=u.FGROUP_ID',array('FGROUP_NAME'))
		->joinLeft(array('MU'=>'M_USER'), 'MU.USER_ID = u.USER_ID')
		->where('u.ACCT_NO='.$this->_db->quote((string)$userAcct))
// 	  		->where('USER_STATUS!=3');
// 	  		->query()->fetchAll()
		->where('MU.USER_MOBILE_PHONE='.$this->_db->quote((string)$userPhone))
		;
		return $this->_db->fetchAll($select);
	}

	public function getUserAcctTmp($userAcct, $userPhone = NULL)
	{
//		$select = $this->_db->select()
//	 		->from(array('u'=>'TEMP_USER_ACCT'),array('USER_ID','ACCT_NO'))
//// 		  	->join(array('g'=>'M_FGROUP'),'g.FGROUP_ID=u.FGROUP_ID',array('FGROUP_NAME'))
//		  	->joinLeft(array('MU'=>'M_USER'), 'MU.USER_ID = u.USER_ID')
//		  	->where('u.ACCT_NO='.$this->_db->quote((string)$userAcct))
//// 		  	->where('USER_STATUS!=3');
//// 		  	->query()->fetchAll()
//			->where('MU.USER_MOBILE_PHONE='.$this->_db->quote((string)$userPhone))
//		;
//			
//		return $this->_db->fetchAll($select);

		$select = $this->_db->select()
		->from(array('u'=>'TEMP_USER_ACCT'),array('USER_ID','ACCT_NO'))
		  				     //->join(array('g'=>'M_FGROUP'),'g.FGROUP_ID=u.FGROUP_ID',array('FGROUP_NAME'))
		->where('UPPER(u.ACCT_NO)='.$this->_db->quote((string)$userAcct))
		  				     //->where('USER_STATUS!=3');
		->query()->fetchAll();

		return $select;
	}
}