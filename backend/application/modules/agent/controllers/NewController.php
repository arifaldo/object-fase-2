<?php
require_once 'Zend/Controller/Action.php';

class agent_NewController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$model = new agent_Model_Agent();
		$this->_userAuth = Zend_Auth::getInstance()->getIdentity();
		$branchCode = $this->_userAuth->branchCode;
		$branchName = $this->_userAuth->branchName;

		$select = $this->_db->select()
			->from(array('L' => 'M_BUSER'), array('L.BUSER_BRANCH'))
			->joinLeft(array('B' => 'M_BRANCH'), 'B.ID = L.BUSER_BRANCH', array('B.*'))
			->where('BUSER_ID = ?', $this->_userIdLogin);
		// ->where('ACCT_STATUS = ?', '1');
		// echo $select;die;
		$branchdata = $this->_db->fetchRow($select);
		// print_r($branchdata);die;
		$this->view->branchCode = $branchdata['BRANCH_CODE'];
		$this->view->branchName = $branchdata['BRANCH_NAME'];

		$this->view->areaArr = $model->getServiceArea();
		$this->view->accArr = array();

		$this->view->error = 0;

		if ($this->_request->isPost()) {
			$filters = array(
				'user_mobile_phone'        => array('StripTags', 'StringTrim'),
				'user_id'             => array('StripTags', 'StringTrim', 'StringToUpper'),
				'user_email'     => array('StripTags', 'StringTrim'),
				'user_fullname'  => array('StripTags', 'StringTrim', 'StringToUpper'),
				'user_classification'     => array('StripTags', 'StringTrim', 'StringToUpper'),
				'user_service_area'             => array('StripTags', 'StringTrim', 'StringToUpper'),
				'user_service_address'             => array('StripTags', 'StringTrim', 'StringToUpper'),
				'user_address'   => array('StripTags', 'StringTrim', 'StringToUpper'),
				'user_city'         => array('StripTags', 'StringTrim', 'StringToUpper'),
				'user_zip'   => array('StripTags', 'StringTrim'),
				'user_province'     => array('StripTags', 'StringTrim', 'StringToUpper'),
				'user_country'     => array('StripTags', 'StringTrim', 'StringToUpper'),
				'user_acct_no'   => array('StripTags', 'StringTrim'),
				'user_acct_name'   => array('StripTags', 'StringTrim', 'StringToUpper'),
			);

			$validators =  array(
				'user_mobile_phone'	=> array(
					'NotEmpty',
					'messages' => array($this->language->_('Can not be empty'))
				),
				'user_id'       => array(
					'NotEmpty',
					array('Db_NoRecordExists', array('table' => 'M_USER_LKP', 'field' => 'USER_ID')),
					array('Db_NoRecordExists', array('table' => 'TEMP_USER_LKP', 'field' => 'USER_ID')),
					'messages' => array(
						'Can not be empty.',
						'Agent Id already exist.',
						'Agent Id already suggested'
					)
				),
				'user_fullname'	=> array(
					'NotEmpty',
					'messages' => array($this->language->_('Can not be empty'))
				),
				'user_service_area'	=> array(
					'NotEmpty',
					array('StringLength', array('max' => 15)),
					'messages' => array(
						'Can not be empty.',
						'Maksimum karakter yang diijinkan adalah 15 karakter'
					)
				),
				'user_service_address'	=> array(
					'allowEmpty' => true,
					array('StringLength', array('max' => 200)),
					'messages' => array(
						'Maksimum karakter yang diijinkan adalah 200 karakter'
					)
				),
				//           'user_acct_no'	=> array('NotEmpty',
				// 'messages' => array($this->language->_('Can not be empty'))
				//   ),
				//           'user_acct_name'	=> array('NotEmpty',
				// 'messages' => array($this->language->_('Can not be empty'))
				//   ),
				'user_email'     => array(
					'NotEmpty',
					'EmailAddress',
					'messages' => array(
						$this->language->_('Can not be empty'),
						'Format email salah'
					)
				),

				// 'user_address'	=> array('allowEmpty'=> true),
				'user_city'	=> array('allowEmpty' => true),
				// 'user_zip'	=> array('allowEmpty'=> true),
				'user_province'	=> array('allowEmpty' => true),
				// 'user_country'	=> array('allowEmpty'=> true),

				//           'user_classification' => array('NotEmpty',
				// 'messages' => array($this->language->_('Can not be empty'))
				//   ),
			);


			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);

			if ($zf_filter_input->isValid()) {
				$info = 'User ID = ' . $zf_filter_input->user_id . ', User Name = ' . $zf_filter_input->user_fullname;
				// print_r($info);die;
				$user_data = array(
					'USER_ID'	=> null,
					'USER_BRANCH_CODE' => $branchCode,
					'USER_STATUS'  => null,
					'USER_EMAIL'     => null,
					'USER_MOBILE_PHONE' => null,
					'USER_SERVICE_AREA'    => null,
					'USER_SERVICE_ADDRESS' => null,
					'USER_FULLNAME' => null,
					'USER_CITY' => null,
					'USER_PROVINCE' => null,
					'USER_ACCT_NO' => null,
				);


				foreach ($validators as $key => $value) {
					if (array_key_exists(strtoupper($key), $user_data))  $user_data[strtoupper($key)] = $zf_filter_input->$key;
				}
				$user_data['USER_BRANCH_CODE'] = $branchdata['BRANCH_CODE'];
				$user_data['USER_STATUS'] = 2;
				$user_data['USER_CREATED']     = new Zend_Db_Expr('GETDATE()');
				$user_data['USER_CREATEDBY']   = $this->_userIdLogin;
				$user_data['USER_UPDATED']     = null;
				$user_data['USER_UPDATEDBY']   = null;
				$user_data['USER_SUGGESTED']    = new Zend_Db_Expr('GETDATE()');
				$user_data['USER_SUGGESTEDBY']  = $this->_userIdLogin;

				try {
					$this->_db->beginTransaction();
					$change_id = $this->suggestionWaitingApproval('Agent', $info, $this->_changeType['code']['new'], null, 'M_USER_LKP', 'TEMP_USER_LKP', $zf_filter_input->user_id, $zf_filter_input->user_fullname, null);
					// var_dump($change_id);
					// die();
					// echo '<pre>';print_r($user_data);die;
					$model->insertTempAgent($change_id, $user_data);

					//Application_Helper_General::writeLog('CCAD','Nasabah telah ditambahkan, ID Nasabah : '.$zf_filter_input->user_id. ' Nama Nasabah : '.$user_data['USER_FULLNAME'].' Id Perubahan : '.$change_id);

					$this->_db->commit();

					$this->setbackURL('/agent/new');
					$this->_redirect('/notification/submited/index');
				} catch (Exception $e) {
					$this->_db->rollBack();
					SGO_Helper_GeneralLog::technicalLog($e);

					$this->view->error_remark = 'Gagal menambahkan agen ke database. Silahkan coba lagi.';
				}

				if ($this->view->error_remark) {
					$userid = $zf_filter_input->user_id;
					$select = $this->_db->select()
						->from(array('M_USER_ACCT'), array('ACCT_NO', 'ACCT_NAME'))
						->where('USER_ID = ?', $userid)
						->where('ACCT_STATUS = ?', '1');

					$acctData = $this->_db->fetchAll($select);
					$this->view->accArr = $acctData;

					foreach ($validators as $key => $value) {
						$this->view->$key = $zf_filter_input->$key;
					}
				}
			} else {
				$userid = $this->_getParam('user_id');
				$select = $this->_db->select()
					->from(array('M_USER_ACCT'), array('ACCT_NO', 'ACCT_NAME'))
					->where('USER_ID = ?', $userid)
					->where('ACCT_STATUS = ?', '1');

				$acctData = $this->_db->fetchAll($select);
				$this->view->accArr = $acctData;

				foreach ($validators as $key => $value) {
					$this->view->$key = $this->_getParam($key);
				}

				$error = $zf_filter_input->getMessages();

				foreach ($error as $keyRoot => $rowError) {
					foreach ($rowError as $errorString) {
						$keyname = "error_" . $keyRoot;
						$this->view->$keyname = $this->language->_($errorString);
					}
				}

				$this->view->error = 1;
			}
		}

		$this->view->modulename = $this->_request->getModuleName();

		//insert log
		try {
			$this->_db->beginTransaction();

			//Application_Helper_General::writeLog('CCAD','Add Customer');

			$this->_db->commit();
		} catch (Exception $e) {
			$this->_db->rollBack();
			SGO_Helper_GeneralLog::technicalLog($e);
		}
	}
}
