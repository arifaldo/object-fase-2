<?php

require_once 'Zend/Controller/Action.php';

class Agent_ViewaccountController extends Application_Main{ 


  public function indexAction() 
  {
    $model = new agent_Model_Agent();
	
    $user_id = strtoupper($this->_getParam('user_id'));
    $user_id = (Zend_Validate::is($user_id,'Alnum') && Zend_Validate::is($user_id,'StringLength',array('min'=>1,'max'=>25)))? $user_id : null;
    $this->view->user_id = $user_id;
    $acct_no = strtoupper($this->_getParam('acct_no'));
    $acct_no = (Zend_Validate::is($acct_no,'Digits') && Zend_Validate::is($acct_no,'StringLength',array('min'=>1,'max'=>25)))? $acct_no : null;
    //Application_Helper_General::writeLog('CCLS','View Customer List : '.$user_id);

    if($user_id)
    {
		  $resultdata = null;
		  $resultdata = $model->getUserData(array('user_id'=>$user_id));
      
      if(is_array($resultdata))
      {
        foreach($resultdata as $row){
          $this->view->user_id = $row['USER_ID'];
          $this->view->user_fullname = $row['USER_FULLNAME'];
        }

		    $modelAcc = new useraccount_Model_Useraccount();
        $accData = $modelAcc->getUserAcct(array('user_id'=>$user_id,'acct_no'=>$acct_no));

        if(is_array($accData)){
          foreach($accData as $row){
            foreach($row as $key=>$val){
              $fieldname = strtolower($key);
              $this->view->$fieldname = $val;
            }
          }
        }
        else{
          $acct_no = null;
        }
        
        
      }else{ $user_id = null; }
    }
    else{ $user_id = null; }

    if(!$user_id){
      $this->view->error_msg = "ID Agen tidak ditemukan.";
    }

    if(!$acct_no){
      $this->view->error_msg = "Rekening tidak ditemukan.";
    }
  }
}