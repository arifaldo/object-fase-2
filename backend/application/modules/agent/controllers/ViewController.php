<?php

require_once 'Zend/Controller/Action.php';

class Agent_ViewController extends Application_Main{ 


  public function indexAction() 
  {
    $this->view->report_msg = array();
    
    $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
    if(count($temp)>1){
        if($temp[0]=='F' || $temp[0]=='S'){
          if($temp[0]=='F')
            $this->view->error = 1;
          else
            $this->view->success = 1;
          $msg = ''; unset($temp[0]);
          foreach($temp as $value)
          {
            if(!is_array($value))
              $value = array($value);
            $msg .= $this->view->formErrors($value);
          }
          $this->view->report_msg = $msg;
      }
    }

    $model = new agent_Model_Agent();
	
    $user_id = strtoupper($this->_getParam('user_id'));
    $user_id = (Zend_Validate::is($user_id,'Alnum') && Zend_Validate::is($user_id,'StringLength',array('min'=>1,'max'=>25)))? $user_id : null;

    //Application_Helper_General::writeLog('CCLS','View Customer List : '.$user_id);

    if($user_id)
    {
		  $resultdata = null;
		  $resultdata = $model->getUserData(array('user_id'=>$user_id));
      
      if(is_array($resultdata))
      {
        // $modelCust = new customer_Model_Customer();
        // $this->view->countryArr = Application_Helper_Array::listArray($modelCust->getCountry(),'COUNTRY_CODE','COUNTRY_NAME');

        foreach($resultdata as $row){
          $this->view->branch_code = $row['USER_BRANCH_CODE'];
          $this->view->branch_name = $row['BRANCH_NAME'];
          $this->view->user_id = $row['USER_ID'];
          $this->view->user_fullname = $row['USER_FULLNAME'];
          $this->view->user_service_area = $row['USER_SERVICE_AREA'];
          $this->view->user_service_address = $row['USER_SERVICE_ADDRESS'];
          $this->view->agent_status = $row['AGENT_STATUS'];
          $this->view->smsb_status = $row['SMSB_STATUS'];
          $this->view->user_islocked = $row['USER_ISLOCKED'];
          $this->view->user_mobile_phone = $row['USER_MOBILE_PHONE'];
          $this->view->user_email = $row['USER_EMAIL'];
          $this->view->user_address = $row['USER_ADDRESS'];
          $this->view->user_city = $row['USER_CITY'];
          $this->view->user_zip = $row['USER_ZIP'];
          $this->view->user_province = $row['USER_PROVINCE'];
          $this->view->user_country = $row['USER_COUNTRY'];
          $this->view->user_suggested = $row['USER_SUGGESTED'];
          $this->view->user_suggestedby = $row['USER_SUGGESTEDBY'];
          $this->view->user_updated = $row['USER_UPDATED'];
          $this->view->user_updatedby = $row['USER_UPDATEDBY'];
          $this->view->user_classification = $row['USER_CLASSIFICATION'];
        }
	
		    $result = $model->getTempUserFromID($user_id);
        if($result){ $temp = 0; }else{ $temp = 1; }
        $this->view->user_temp = $temp;

		    // $modelAcc = new useraccount_Model_Useraccount();
        // $this->view->bankAccount = $modelAcc->getUserAcct(array('user_id'=>$user_id,'acct_no'=>$resultdata['USER_ACCT_NO']));
        
        
      }else{ $user_id = null; }
    }
    else{ $user_id = null; }

    if(!$user_id){
      $this->view->error_msg = "ID Agen tidak ditemukan.";
    }
  }
}