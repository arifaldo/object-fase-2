<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';

class agent_EditController extends Application_Main
{
  public function indexAction()
  {
  	$this->_helper->layout()->setLayout('newlayout');
	$model = new agent_Model_Agent();

	$this->_userAuth = Zend_Auth::getInstance()->getIdentity();
	$branchCode = $this->_userAuth->branchCode;
	$branchName = $this->_userAuth->branchName;

	$this->view->branchCode = $branchCode;
	$this->view->branchName = $branchName;

	$this->view->areaArr = $model->getServiceArea();

	$user_id = $this->_getParam('user_id');
	$this->view->user_id = $user_id;
	$getData = $model->getUserData(array('user_id'=>$user_id));

	if(is_array($getData)){
		$tempData = $getData[0];
		foreach($tempData as $key=>$val){
			$fieldname = strtolower($key);
			$this->view->$fieldname = $val;
		}

		$modelCust = new customer_Model_Customer();
        $this->view->countryArr = Application_Helper_Array::listArray($modelCust->getCountry(),'COUNTRY_CODE','COUNTRY_NAME');

        $modelAcc = new useraccount_Model_Useraccount();
        $accData = $modelAcc->getUserAcct(array('user_id'=>$user_id,'acct_no'=>$tempData['USER_ACCT_NO']));
        $this->view->user_acct_name = $accData[0]['ACCT_NAME'];
	}
	else{
		$user_id = null;
	}

	if(!$user_id){
		$this->view->user_id = $user_id;
	}

  	if($this->_request->isPost())
  	{
  		
		$filters = array(
  	   					'user_service_area' => array('StripTags','StringTrim','StringToUpper'),
                        'user_email'        => array('StripTags','StringTrim'),
                        'user_classification' => array('StripTags','StringTrim','StringToUpper'),
                        'user_service_address'=> array('StripTags','StringTrim','StringToUpper'),
                      );
	   
       	$validators =  array(
                            'user_service_area'	=> array('NotEmpty',
															array('StringLength',array('max'=>15)),
														   'messages' => array(
														   						'Area Layanan tidak boleh kosong',
																				'Maksimum karakter yang diijinkan adalah 15 karakter'
																			   )
														  ),
                           'user_service_address'	=> array('allowEmpty' => true,
															array('StringLength',array('max'=>200)),
														   'messages' => array(
																				'Maksimum karakter yang diijinkan adalah 200 karakter'
																			   )
														  ),
                           'user_email'     => array('NotEmpty',
								'EmailAddress',
								'messages' => array(
									$this->language->_('Can not be empty'),
									'Format email salah'
								)
							),
                           'user_classification' => array('NotEmpty',
												     'messages' => array($this->language->_('Can not be empty'))
											        ),
					     );

	  	$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

  		

		if($zf_filter_input->isValid())
		{
			$info = 'User ID = '.$zf_filter_input->user_id.', User Name = '.$tempData['USER_FULLNAME'];
			$user_data = array(
							   'USER_SERVICE_ADDRESS' => $zf_filter_input->user_service_address,
							   'USER_EMAIL'  => $zf_filter_input->user_email,
							   'USER_CLASSIFICATION'  => $zf_filter_input->user_classification,
							   'USER_SERVICE_AREA' => $zf_filter_input->user_service_area
							);

			$user_data['USER_SUGGESTED']    = new Zend_Db_Expr('GETDATE()');
			$user_data['USER_SUGGESTEDBY']  = $this->_userIdLogin;
  			
			try
			{
				$this->_db->beginTransaction();
				$change_id = $this->suggestionWaitingApproval('Agent',$info,$this->_changeType['code']['edit'],null,'M_USER_LKP','TEMP_USER_LKP',$zf_filter_input->user_id,$tempData['USER_FULLNAME'],null);
				
				$model->insertTempUser($change_id,$user_data);
				//log CSUD
				//Application_Helper_General::writeLog('CCUD','User has been Updated (edit), User ID : '.$user_id. ' User Name : '.$user_data['USER_FULLNAME'].' Change id : '.$change_id);

				$this->_db->commit();

				$this->setbackURL('/agent/agentlist');
				$this->_redirect('/notification/submited/index');
			}
			catch(Exception $e)
			{
				$this->_db->rollBack();
				SGO_Helper_GeneralLog::technicalLog($e);

				$this->view->error_remark = 'Gagal koneksi ke database. Silahkan coba lagi.';
			}

			if($this->view->error_remark){
				foreach($validators as $key=>$value)
			  	{
					$this->view->$key = $zf_filter_input->$key;
			  	}
			}
		}
		else
		{
			foreach($validators as $key=>$value)
			{
				$this->view->$key = $this->_getParam($key);
			}
			
			$error = $zf_filter_input->getMessages();
									
			foreach($error as $keyRoot => $rowError)
			{
			   foreach($rowError as $errorString)
			   {
			   	  $keyname = "error_" . $keyRoot;
				  $this->view->$keyname = $this->language->_($errorString);
			   }
			}

			$this->view->error = 1;
		}
  	}

  	$this->view->modulename = $this->_request->getModuleName();

    //insert log
    try
	{
	  $this->_db->beginTransaction();

	  //Application_Helper_General::writeLog('CSAD','');

	  $this->_db->commit();
	}
    catch(Exception $e)
    {
 	  $this->_db->rollBack();
	  SGO_Helper_GeneralLog::technicalLog($e);
	}
	
  }


}