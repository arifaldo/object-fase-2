<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';

class Agent_AgentlistController extends Application_Main 
{
	protected $_moduleDB = 'RTF';
	
		
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$this->view->report_msg = array();
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}
    	}

	    $this->setbackURL('/'.$this->_request->getModuleName().'/agentlist/');

	    $model = new agent_Model_Agent(); 

		$listStatus =  array('1' => 'Aktif', '2' => 'Nonaktif', '3' => 'Terhapus');
		
		$this->view->listStatus = array(''=>'-- '.$this->language->_('Any Value').' --');
		$this->view->listStatus += $listStatus;

		//$this->view->branchArr = array(''=>'-- '.$this->language->_('Any Value').' --')+Application_Helper_Array::listArray($model->getBranch(),"CODE","NAME");
		$this->view->areaArr = array(''=>'-- '.$this->language->_('Any Value').' --')+Application_Helper_Array::listArray($model->getServiceArea(),"AREA_NAME","AREA_NAME");
		
 
		$fields = array	(
							'branch_code'  					=> array	(
																		'field' => 'USER_BRANCH_CODE',
																		'label' => 'Kode Cabang',
																		'sortable' => true
																	),
							'user_id'  					=> array	(
																		'field' => 'USER_ID',
																		'label' => 'ID Agen',
																		'sortable' => true
																	),
							'name'  					=> array	(
																		'field' => 'USER_FULLNAME',
																		'label' => 'Nama Agen',
																		'sortable' => true
																	),
							'mobile_phone'  			=> array	(
																		'field' => 'USER_MOBILE_PHONE',
																		'label' => 'Nomor Ponsel',
																		'sortable' => true
																	),
							'email'  			=> array	(
																		'field' => 'USER_EMAIL',
																		'label' => 'Email',
																		'sortable' => true
																	),
							// 'agent_status'  			=> array	(
							// 											'field' => 'AGENT_STATUS',
							// 											'label' => 'Status Agen',
							// 											'sortable' => true
							// 										),
							// 'smsb_status'  					=> array	(
							// 											'field' => 'SMSB_STATUS',
							// 											'label' => 'Status SMSB',
							// 											'sortable' => true
							// 										),

							'service_area' 					=> array	(
																		'field' => 'USER_SERVICE_AREA',
																		'label' => 'Area Layanan',
																		'sortable' => true
																	),
							'latestSuggestion'     => array('field'    => 'L.USER_SUGGESTED',
																		   'label'    => 'Tanggal Disarankan',
																		   'sortable' => true),
							'latestSuggestor'   => array('field'  => 'L.USER_SUGGESTEDBY',
																	   'label'    => 'Disarankan Oleh',
																	   'sortable' => true),
							'latestApproval'    => array('field'  => 'L.USER_UPDATED',
																	   'label'    => 'Tanggal Disetujui',
																	   'sortable' => true),
							'latestApprover'  => array('field'   => 'L.USER_UPDATEDBY',
																	   'label'    => 'Disetujui Oleh',
																	   'sortable' => true)													  
	                    );
		

		$filterlist = array('AGENT_ID','AGENT_NAME','MOBILEPHONE','SERVICEAREA','SUGEST_DATE','APPROVE_DATE','SERVICEAREA','APPROVEBY','SUGESTBY');
		
		$this->view->filterlist = $filterlist;


		$page = $this->_getParam('page');
	    $page = (Zend_Validate::is($page,'Digits'))? $page : 1;
	    $sortBy = $this->_getParam('sortby');
	    $sortBy = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
	    $sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
	
	    $filterArr = array('AGENT_ID' => array('StripTags','StringTrim','StringToUpper','HtmlEntities'),
	    				   'AGENT_NAME'    => array('StripTags','StringTrim','StringToUpper','HtmlEntities'),
	                       'MOBILEPHONE'  => array('StripTags','StringTrim','StringToUpper','HtmlEntities'),
	                       // 'agent_status' => array('StripTags','StringTrim','StringToUpper','HtmlEntities'),
	                       'branch_code' => array('StripTags','StringTrim','StringToUpper','HtmlEntities'),
	                       'SERVICEAREA' => array('StripTags','StringTrim','StringToUpper','HtmlEntities'),
	                       // 'smsb_status' => array('StripTags','StringTrim','StringToUpper','HtmlEntities'),
	                       'SUGEST_DATE' => array('StripTags','StringTrim','HtmlEntities'),
	                       'SUGEST_DATE_END' => array('StripTags','StringTrim','HtmlEntities'),
	                       'SUGESTBY' => array('StripTags','StringTrim','StringToUpper','HtmlEntities'),
	                       'APPROVE_DATE' => array('StripTags','StringTrim','HtmlEntities'),
	                       'APPROVE_DATE_END' => array('StripTags','StringTrim','HtmlEntities'),
	                       'APPROVEBY' => array('StripTags','StringTrim','StringToUpper','HtmlEntities')
	                      );
	    

	    $dataParam = array("AGENT_ID","AGENT_NAME","MOBILEPHONE","SERVICEAREA","SUGESTBY","APPROVEBY");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{
			
			// print_r($dtParam);die;
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}
				
			}
				
			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}

		if(!empty($this->_request->getParam('sugestdate'))){
				$createarr = $this->_request->getParam('sugestdate');
					$dataParamValue['SUGEST_DATE'] = $createarr[0];
					$dataParamValue['SUGEST_DATE_END'] = $createarr[1];
			}
			if(!empty($this->_request->getParam('approvedate'))){
				$updatearr = $this->_request->getParam('approvedate');
					$dataParamValue['APPROVE_DATE'] = $updatearr[0];
					$dataParamValue['APPROVE_DATE_END'] = $updatearr[1];
			}

	    $zf_filter = new Zend_Filter_Input($filterArr,array(),$dataParamValue);
	    // $filter = $zf_filter->getEscaped('filter');
	    $filter 		= $this->_getParam('filter');
	    $this->view->currentPage = $page;
	    $this->view->sortBy = $sortBy;
	    $this->view->sortDir = $sortDir;
	    $csv = $this->_getParam('csv');


		$caseSmsStatus = "(CASE MU.USER_STATUS ";
		foreach($listStatus as $key=>$val)
		{
			$caseSmsStatus .= " WHEN ".$key." THEN '".$val."'";
		}
		$caseSmsStatus .= " END)";
		
		$caseAgentStatus = "(CASE L.USER_STATUS ";
		foreach($listStatus as $key=>$val)
		{
			$caseAgentStatus .= " WHEN ".$key." THEN '".$val."'";
		}
		$caseAgentStatus .= " END)";
		
		
		$select = $this->_db->select()
							->from(array('L' => 'M_USER_LKP'),array())
							->joinLeft(array('B' => 'M_BRANCH'),'B.BRANCH_CODE = L.USER_BRANCH_CODE',array('B.BRANCH_NAME'))
							->joinLeft(array('MU' => 'M_USER'),'L.USER_ID = MU.USER_ID',array(//'L.USER_BRANCH_CODE',
																						'USER_ID' => 'L.USER_ID',
																						'L.USER_FULLNAME',
																						'L.USER_EMAIL',
																						'USER_MOBILE_PHONE' => 'L.USER_MOBILE_PHONE',
																						'L.USER_SERVICE_AREA',
																						'L.USER_SUGGESTED',
																						'L.USER_SUGGESTEDBY',
																						'L.USER_UPDATED',
																						'L.USER_UPDATEDBY'
																				));
		
	
		if($filter==TRUE)
	    {
	    	$user_id = html_entity_decode($zf_filter->getEscaped('AGENT_ID'));
			$agent_name = html_entity_decode($zf_filter->getEscaped('AGENT_NAME'));
			$mobilephone = html_entity_decode($zf_filter->getEscaped('MOBILEPHONE'));
			$agent_status = html_entity_decode($zf_filter->getEscaped('agent_status'));
			$smsb_status = html_entity_decode($zf_filter->getEscaped('smsb_status'));
			$service_area = html_entity_decode($zf_filter->getEscaped('SERVICEAREA'));
			//$branch_code = html_entity_decode($zf_filter->getEscaped('branch_code'));
			$latestSuggestionFrom   = html_entity_decode($zf_filter->getEscaped('SUGEST_DATE'));
			$latestSuggestionTo     = html_entity_decode($zf_filter->getEscaped('SUGEST_DATE_END'));
			$latestSuggestor        = html_entity_decode($zf_filter->getEscaped('SUGESTBY'));
			$latestApprovalFrom     = html_entity_decode($zf_filter->getEscaped('APPROVE_DATE'));
			$latestApprovalTo       = html_entity_decode($zf_filter->getEscaped('APPROVE_DATE_END'));
			$latestApprover         = html_entity_decode($zf_filter->getEscaped('APPROVEBY'));

			if($user_id)
			{
				$this->view->user_id = $user_id;
				$select->where("L.USER_ID LIKE ".$this->_db->quote('%'.$user_id.'%'));
			}

			if($agent_name)
			{
				$this->view->agent_name = $agent_name;
				$select->where("L.USER_FULLNAME LIKE ".$this->_db->quote('%'.$agent_name.'%'));
			}

			if($mobilephone)
			{
				$this->view->mobilephone = $mobilephone;
				$select->where("L.USER_MOBILE_PHONE LIKE ".$this->_db->quote('%'.$mobilephone.'%'));
			}

			if($agent_status)
			{
				$this->view->agent_status = $agent_status;
				$select->where("L.USER_STATUS LIKE ".$this->_db->quote($agent_status));
			}

			if($smsb_status)
			{
				$this->view->smsb_status = $smsb_status;
				$select->where("MU.USER_STATUS LIKE ".$this->_db->quote($smsb_status));
			}

			// if($branch_code)
			// {
			// 	$this->view->branch_code = $branch_code;
			// 	$select->where("L.USER_BRANCH_CODE LIKE ".$this->_db->quote($branch_code));
			// }

			if($service_area)
			{
				$this->view->service_area = $service_area;
				$select->where("L.USER_SERVICE_AREA LIKE ".$this->_db->quote($service_area));
			}		

		   //konversi date agar dapat dibandingkan
			$latestSuggestionFrom   = (Zend_Date::isDate($latestSuggestionFrom,$this->_dateDisplayFormat))?
									   new Zend_Date($latestSuggestionFrom,$this->_dateDisplayFormat):
									   false;
			
			$latestSuggestionTo     = (Zend_Date::isDate($latestSuggestionTo,$this->_dateDisplayFormat))?
									   new Zend_Date($latestSuggestionTo,$this->_dateDisplayFormat):
									   false;
															   
			$latestApprovalFrom     = (Zend_Date::isDate($latestApprovalFrom,$this->_dateDisplayFormat))?
									   new Zend_Date($latestApprovalFrom,$this->_dateDisplayFormat):
									   false;

			$latestApprovalTo       = (Zend_Date::isDate($latestApprovalTo,$this->_dateDisplayFormat))?
									   new Zend_Date($latestApprovalTo,$this->_dateDisplayFormat):
									   false;

			if($latestSuggestor)  $select->where('UPPER(L.USER_SUGGESTEDBY) LIKE '.$this->_db->quote('%'.strtoupper($latestSuggestor).'%'));
			if($latestApprover)   $select->where('UPPER(L.USER_UPDATEDBY) LIKE '.$this->_db->quote('%'.strtoupper($latestApprover).'%'));
			if($latestSuggestionFrom)  $select->where("CONVERTSGO('DATE',L.USER_SUGGESTED) >= CONVERTSGO('DATE',".$this->_db->quote($latestSuggestionFrom->toString($this->_dateDBFormat)).")");
			if($latestSuggestionTo)    $select->where("CONVERTSGO('DATE',L.USER_SUGGESTED) <= CONVERTSGO('DATE',".$this->_db->quote($latestSuggestionTo->toString($this->_dateDBFormat)).")");
			if($latestApprovalFrom)    $select->where("CONVERTSGO('DATE',L.USER_UPDATED) >= CONVERTSGO('DATE',".$this->_db->quote($latestApprovalFrom->toString($this->_dateDBFormat)).")");
			if($latestApprovalTo)      $select->where("CONVERTSGO('DATE',L.USER_UPDATED) <= CONVERTSGO('DATE',".$this->_db->quote($latestApprovalTo->toString($this->_dateDBFormat)).")");
			$this->view->latestSuggestor  = $latestSuggestor;
			$this->view->latestApprover   = $latestApprover;
			
			if($latestSuggestionFrom)  $this->view->latestSuggestionFrom = $latestSuggestionFrom->toString($this->_dateDisplayFormat);
			if($latestSuggestionTo)    $this->view->latestSuggestionTo   = $latestSuggestionTo->toString($this->_dateDisplayFormat);
			if($latestApprovalFrom)    $this->view->latestApprovalFrom   = $latestApprovalFrom->toString($this->_dateDisplayFormat);
			// print_r($this->view->latestApprovalFrom);die;
			if($latestApprovalTo)      $this->view->latestApprovalTo     = $latestApprovalTo->toString($this->_dateDisplayFormat);
			
	    }
// echo $select;die;
		$select->order($sortBy.' '.$sortDir);

		if($csv)
		{
			$data = $this->_db->fetchall($select);
			foreach($data as $rowNum => $key)
			{
				$data[$rowNum]['USER_SUGGESTED'] = Application_Helper_General::convertDate($key['USER_SUGGESTED'],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
				$data[$rowNum]['USER_UPDATED'] = Application_Helper_General::convertDate($key['USER_UPDATED'],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
			}
		}
		
		$header = Application_Helper_Array::simpleArray($fields,"label");

		if($csv)
		{
			$this->_helper->download->csv($header,$data,null,'Daftar Agen Laku Pandai');   
			Application_Helper_General::writeLog('ROVW','Import CSV Nasabah > Daftar Nasabah');
		}
		else
		{		
			Application_Helper_General::writeLog('CSLS','Lihat Daftar Nasabah');
		}

		$alldata = $this->_db->fetchAll($select);
		$countdata = 0;
	    foreach($alldata as $key => $value){
	    	$countdata++;
	    }


    	$this->paging($select,$countdata);
   		$this->view->fields = $fields;
    	$this->view->filter = $filter;
		$this->view->modulename = $this->_request->getModuleName();
		$this->view->controllername = $this->_request->getControllerName();
		unset($dataParamValue['SUGEST_DATE_END']);
		unset($dataParamValue['APPROVE_DATE_END']);
		if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     
      }
	}
}