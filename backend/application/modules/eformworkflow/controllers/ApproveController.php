<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class eformworkflow_ApproveController extends Application_Main
{

  protected $_moduleDB = 'RTF'; // masih harus diganti

  public function indexAction()
  {

    $this->_helper->_layout->setLayout('newlayout');

    $settings = new Settings();
    $system_type = $settings->getSetting('system_type');
    $this->view->systemType = $system_type;

    $conf = Zend_Registry::get('config');
    $this->_bankName = $conf['app']['bankname'];
    $this->view->masterbankname = $this->_bankName;

    $this->CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

    $fields = array(
      /*'alias'  => array('field' => 'BENEFICIARY_ALIAS',
                                   'label' => $this->language->_('Alias Name'),
                                   'sortable' => true),*/
      'regno'     => array(
        'field'    => 'BG_REG_NUMBER',
        'label'    => $this->language->_('RegNumber') . ' / ' . $this->language->_('Subject'),
      ),
      'app' => array(
        'field' => 'CUST_ID',
        'label' => $this->language->_('Aplicant'),
      ),

      'obligee' => array(
        'field' => 'RECIPIENT_NAME',
        'label' => $this->language->_('Obligee'),
      ),

      'branch'     => array(
        'field'    => 'BG_BRANCH',
        'label'    => $this->language->_('Branch'),
      ),

      'countertype'     => array(
        'field'    => 'COUNTER_WARRANTY_TYPE',
        'label'    => $this->language->_('Counter Type'),
      ),

      'amount' => array(
        'field' => 'BG_AMOUNT',
        'label' => $this->language->_('BG Amount'),
      ),




      // 'startdate' => array(
      //   'field' => 'TIME_PERIOD_START',
      //   'label' => $this->language->_('Start Date'),
      // ),
      // 'enddate'   => array(
      //   'field'    => 'TIME_PERIOD_END',
      //   'label'    => $this->language->_('End Date'),
      // )
      'type'  => array(
        'field'    => 'CHANGE_TYPE',
        'label'    => $this->language->_('Type')
      )
    );

    //add reza
    $filterlist = array("BG_REG_NUMBER", "BG_SUBJECT", "APPLICANT", "BRANCH", "COUNTER_TYPE", "TYPE");

    $this->view->filterlist = $filterlist;

    $page    = $this->_getParam('page');

    $sortBy  = ($this->_getParam('sortby')) ?  $this->_getParam('sortby') : ('BG_UPDATED');
    $sortBy  = (Zend_Validate::is($sortBy, 'InArray', array(array_keys($fields)))) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

    $sortDir = $this->_getParam('sortdir');
    $sortDir = (Zend_Validate::is($sortDir, 'InArray', array('haystack' => array('asc', 'desc')))) ? $sortDir : 'desc';

    $this->view->currentPage = $page;
    $this->view->sortBy      = $sortBy;
    $this->view->sortDir     = $sortDir;

    //end reza

    // Add Bahri
    if ($this->view->hasPrivilege("ACCS")) { // Cash Collateral
      $warantyIn = ['1'];
    }
    if ($this->view->hasPrivilege("ANCS")) { // Non-Cash Collateral
      $warantyIn = ['2', '3'];
    }
    if ($this->view->hasPrivilege("ACCS") && $this->view->hasPrivilege("ANCS")) { // All
      $warantyIn = ['1', '2', '3'];
    }
    $select = $this->_db->select()
      ->from(array('A' => 'M_BUSER'), array('BUSER_BRANCH'))
      ->where('A.BUSER_ID = ?', $this->_userIdLogin);
    $buser = $this->_db->fetchRow($select);
    $branchUser = $buser['BUSER_BRANCH'];
    // END Bahri

    $selectbg = $this->_db->select()
      ->from(array('A' => 'TEMP_BANK_GUARANTEE'), array(
        'REG_NUMBER' => 'BG_REG_NUMBER',
        'SUBJECT' => 'BG_SUBJECT',
        'CREATED' => 'BG_CREATED',
        //'TYPE' => (string)'TYPE',
        'TIME_PERIOD_START',
        'TIME_PERIOD_END',
        'CREATEDBY' => 'BG_CREATEDBY',
        'AMOUNT' => 'BG_AMOUNT',
        'COUNTER_WARRANTY_TYPE',
        'BG_INSURANCE_CODE',
        'FULLNAME' => 'T.USER_FULLNAME',
        'SP_OBLIGEE_CODE',
        'RECIPIENT_NAME',
        'B.BRANCH_NAME',
        'C.CUST_NAME',
        'IS_AMENDMENT' => 'A.CHANGE_TYPE'

      ))
      ->join(array('T' => 'M_USER'), 'A.BG_CREATEDBY = T.USER_ID AND T.CUST_ID = A.CUST_ID')
      ->join(array('C' => 'M_CUSTOMER'), 'A.CUST_ID = C.CUST_ID')
      ->join(array('B' => 'M_BRANCH'), 'A.BG_BRANCH = B.BRANCH_CODE')
      //->where('A.CUST_ID ='.$this->_db->quote((string)$this->_custIdLogin))
      ->where('A.BG_STATUS = 7')
      //->where('B.ID = ?', $branchUser) // Add Bahri
      ->where('A.COUNTER_WARRANTY_TYPE IN (?)', $warantyIn) // Add Bahri
      ->order('A.BG_CREATED DESC');
    //->query()->fetchAll();
    $auth = Zend_Auth::getInstance()->getIdentity();
    if ($auth->userHeadQuarter == "NO") {
      $selectbg->where('B.ID = ?', $branchUser); // Add Bahri
    }

    // select branch ------------------------------------------------------------
    $select_branch = $this->_db->select()
      ->from(array('A' => 'M_BRANCH'), array("BRANCH_CODE", "BRANCH_NAME"))
      ->query()->fetchAll();

    $save_branch = [];

    foreach ($select_branch as $key => $value) {
      $save_branch[$value["BRANCH_CODE"]] = $value["BRANCH_NAME"];
    }

    $this->view->sel_branch = $save_branch;
    //  --------------------------------------------------------------------------

    // select APPICANT ------------------------------------------------------------
    $select_applicant = $this->_db->select()
      ->from(array('A' => 'M_CUSTOMER'), array("CUST_ID", "CUST_NAME"))
      ->query()->fetchAll();

    $save_applicant = [];

    foreach ($select_applicant as $key => $value) {
      $save_applicant[$value["CUST_ID"]] = $value["CUST_ID"];
    }

    $this->view->sel_applicant = $save_applicant;
    //  --------------------------------------------------------------------------

    // select counter type ------------------------------------------------------------
    $save_counter_type = [
      1 => 'FC',
      2 => 'LF',
      3 => 'Insurance'
    ];

    $this->view->counter_type = $save_counter_type;
    //  -------------------------------------------------------------------------- 

    // select type ------------------------------------------------------------
    $arr_type = [
      0 => 'New',
      1 => 'Amendment Changes',
      2 => 'Amendment Draft'
    ];

    $this->view->arr_type = $arr_type;
    //  -------------------------------------------------------------------------- 

    $selectlc = $this->_db->select()
      ->from(array('A' => 'T_LC'), array(
        'REG_NUMBER' => 'LC_REG_NUMBER',
        'SUBJECT' => 'LC_CREDIT_TYPE',
        'CREATED' => 'LC_CREATED',
        'CCYID' => 'LC_CCY',
        //'TYPE' => (string)'TYPE',
        'TIME_PERIOD_END' => 'LC_EXPDATE',
        'CREATEDBY' => 'LC_CREATEDBY',
        'AMOUNT' => 'LC_AMOUNT',

        'FULLNAME' => 'T.USER_FULLNAME'
      ))
      ->join(array('T' => 'M_USER'), 'A.LC_CREATEDBY = T.USER_ID')
      //->where('A.LC_CUST ='.$this->_db->quote((string)$this->_custIdLogin))
      ->where('A.LC_STATUS = 1')
      ->order('A.LC_CREATED DESC');
    // ->query()->fetchAll();


    //$result = array_merge($selectbg, $selectlc);

    //$this->paging($result);

    $conf = Zend_Registry::get('config');


    $this->view->bankname = $conf['app']['bankname'];


    $config     = Zend_Registry::get('config');
    $BgType     = $config["bg"]["status"]["desc"];
    $BgCode     = $config["bg"]["status"]["code"];

    $arrStatus = array_combine(array_values($BgCode), array_values($BgType));

    $this->view->arrStatus = $arrStatus;

    $arrWaranty = array(
      1 => 'FC',
      2 => 'LF',
      3 => 'INS'
    );
    $this->view->arrWaranty = $arrWaranty;

    $arrType = array(
      1 => 'Standart',
      2 => 'Custom'
    );

    $arrLang = array(
      1 => 'Indonesian',
      2 => 'English',
      3 => 'Bilingual'
    );
    $this->view->langArr = $arrLang;
    $this->view->formatArr = $arrType;
    $this->view->fields = $fields;


    $filterArr = array(
      'filter'  =>  array('StripTags'),
      'BG_REG_NUMBER'     =>  array('StringTrim', 'StripTags', 'StringToUpper'),
      'BG_SUBJECT'     =>  array('StringTrim', 'StripTags', 'StringToUpper'),
      'APPLICANT'     =>  array('StringTrim', 'StripTags', 'StringToUpper'),
      'BRANCH'     =>  array('StringTrim', 'StripTags', 'StringToUpper'),
      'COUNTER_TYPE'     =>  array('StringTrim', 'StripTags', 'StringToUpper'),
      'TYPE' => array('StripTags', 'StringTrim', 'StringToUpper'),
    );

    $dataParam = array("BG_REG_NUMBER", "BG_SUBJECT", "APPLICANT", "BRANCH", "COUNTER_TYPE", "TYPE");
    $dataParamValue = array();

    foreach ($dataParam as $dtParam) {
      if (!empty($this->_request->getParam('wherecol'))) {
        $dataval = $this->_request->getParam('whereval');
        foreach ($this->_request->getParam('wherecol') as $key => $value) {
          if ($dtParam == $value) {
            $dataParamValue[$dtParam] = $dataval[$key];
          }
        }
      }
    }

    // var_dump($this->_request->getParams());
    // die();

    if (!empty($this->_request->getParam('efdate'))) {
      $efdatearr = $this->_request->getParam('efdate');
      $dataParamValue['BG_PERIOD'] = $efdatearr[0];
      $dataParamValue['BG_PERIOD_END'] = $efdatearr[1];
    }

    $validator = array(
      'BG_REG_NUMBER'     =>  array(),
      'BG_SUBJECT'     =>  array(),
      'APPLICANT'     =>  array(),
      'BRANCH'     =>  array(),
      'COUNTER_TYPE'     =>  array(),
      'TYPE'     =>  array(),
    );

    // echo "<pre>";
    // print_r($dataParamValue);die;


    $zf_filter   = new Zend_Filter_Input($filterArr, $validator, $dataParamValue);

    if ($zf_filter->isValid()) {
      $filter     = TRUE;
    }

    $filter     = $this->_getParam('filter');
    $BG_REG_NUMBER    = html_entity_decode($zf_filter->getEscaped('BG_REG_NUMBER'));
    $BG_SUBJECT    = html_entity_decode($zf_filter->getEscaped('BG_SUBJECT'));
    $APPLICANT    = html_entity_decode($zf_filter->getEscaped('APPLICANT'));
    $BRANCH    = html_entity_decode($zf_filter->getEscaped('BRANCH'));
    $COUNTER_TYPE    = html_entity_decode($zf_filter->getEscaped('COUNTER_TYPE'));
    $LAST_SAVED_BY    = html_entity_decode($zf_filter->getEscaped('LAST_SAVED_BY'));
    $TYPE    = html_entity_decode($zf_filter->getEscaped('TYPE'));


    // if ($filter == null) {
    //   $datefrom = (date("d/m/Y"));
    //   $dateto = (date("d/m/Y"));
    //   $this->view->fDateFrom  = (date("d/m/Y"));
    //   $this->view->fDateTo  = (date("d/m/Y"));
    // }

    // if ($filter_clear == '1') {
    //   $this->view->fDateFrom  = '';
    //   $this->view->fDateTo  = '';
    //   $datefrom = '';
    //   $dateto = '';
    // }

    if ($filter == TRUE) {

      if ($BG_REG_NUMBER != null) {
        $selectbg->where("A.BG_REG_NUMBER LIKE " . $this->_db->quote('%' . $BG_REG_NUMBER . '%'));
      }

      if ($BG_SUBJECT != null) {
        $selectbg->where("A.BG_SUBJECT LIKE " . $this->_db->quote('%' . $BG_SUBJECT . '%'));
      }

      if ($APPLICANT   != null) {
        $selectbg->where("C.CUST_NAME LIKE " . $this->_db->quote('%' . $APPLICANT . '%'));
      }

      if ($BRANCH     != null) {
        $selectbg->where("B.BRANCH_CODE LIKE " . $this->_db->quote('%' . $BRANCH . '%'));
      }

      if ($COUNTER_TYPE     != null) {
        $selectbg->where("A.COUNTER_WARRANTY_TYPE LIKE " . $this->_db->quote('%' . $COUNTER_TYPE . '%'));
      }

      if ($TYPE     != null) {
        $selectbg->where("A.CHANGE_TYPE LIKE " . $this->_db->quote('%' . $TYPE . '%'));
      }
    }


    // $selectbg->order($sortBy.' '.$sortDir);


    $this->view->fields = $fields;
    $this->view->filter = $filter;

    $selectbg = $this->_db->fetchAll($selectbg);
    $selectlc = $this->_db->fetchAll($selectlc);
    $result = array_merge($selectbg, $selectlc);

    $setting = new Settings();
    $enc_pass = $setting->getSetting('enc_pass');
    $enc_salt = $setting->getSetting('enc_salt');
    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');
    $pw_hash = md5($enc_salt . $enc_pass);
    $rand = $this->_userIdLogin . date('dHis') . $pw_hash;
    $sessionNamespace->token   = $rand;
    $this->view->token = $sessionNamespace->token;

    $result2 = array();
    foreach ($result as $key => $value) {
      // BEGIN PERBAIKAN TEMUAN TO 13 JUNI 2023 
	 
      if ($value['COUNTER_WARRANTY_TYPE'] == '1') {
		
        $boundary = $this->validatebtn('50', $value['AMOUNT'], 'IDR', $value['REG_NUMBER']);
      } else {
        $boundary = $this->validatebtn('51', $value['AMOUNT'], 'IDR', $value['REG_NUMBER']);
      }
	  
	  if (!$boundary) continue;
   
    	  $get_reg_number = $value["REG_NUMBER"];
        $AESMYSQL = new Crypt_AESMYSQL();
        $rand = $this->token;
        $encrypted_payreff = $AESMYSQL->encrypt($get_reg_number, $rand);
        $encpayreff = urlencode($encrypted_payreff);
        $result2[$key]["REG_NUMBER_ENCRYPTED"] = $encpayreff;
 
	  	  $result2[$key]['REG_NUMBER'] = $value['REG_NUMBER'];
	  	  $result2[$key]['SUBJECT'] = $value['SUBJECT'];
	  	  $result2[$key]['CUST_ID'] = $value['CUST_ID'];
	  	  $result2[$key]['CUST_NAME'] = $value['CUST_NAME'];
        $result2[$key]['RECIPIENT_NAME'] = $value['RECIPIENT_NAME'];
        $result2[$key]['BRANCH_NAME'] = $value['BRANCH_NAME'];
        $result2[$key]['COUNTER_WARRANTY_TYPE'] = $value['COUNTER_WARRANTY_TYPE'];
        $result2[$key]['AMOUNT'] = $value['AMOUNT'];
        $result2[$key]['CCYID'] = $value['CCYID'];
        $result2[$key]['GROUP_ID'] = $value['GROUP_ID'];
        $result2[$key]['BG_INSURANCE_CODE'] = $value['BG_INSURANCE_CODE'];
        $result2[$key]['IS_AMENDMENT'] = $value['IS_AMENDMENT'];
	  
      // END PERBAIKAN TEMUAN TO 13 JUNI 2023 
	  
    }

    //Zend_Debug::dump($result2);
	  $this->paging($result2);

    if (!empty($dataParamValue)) {

      $this->view->efdateStart = $dataParamValue['BG_PERIOD'];
      $this->view->efdateEnd = $dataParamValue['BG_PERIOD_END'];

      foreach ($dataParamValue as $key => $value) {
        $duparr = explode(',', $value);
        if (!empty($duparr)) {

          foreach ($duparr as $ss => $vs) {
            $wherecol[]  = $key;
            $whereval[] = $vs;
          }
        } else {
          $wherecol[]  = $key;
          $whereval[] = $value;
        }
      }
      $this->view->wherecol     = $wherecol;
      $this->view->whereval     = $whereval;
    }


    $allData = [];

    if (!empty($this->_getParam('csv')) || $this->_request->getParam('print') == 1) {

      foreach ($result as $row) {
        $subData = [];

        if ($row['SUBJECT'] == '') {
          $row['SUBJECT'] = '- no subject -';
        } else if ($row['SUBJECT'] == '1') {
          $row['SUBJECT'] = 'Letter of Credit';
        } else if ($row['SUBJECT'] == '2') {
          $row['SUBJECT'] = 'Surat Kredit Berdokumen Dalam Negeri';
        }

        if (isset($row['CCYID'])) {
          $ccyid = $row['CCYID'];
        } else {
          $ccyid = "IDR";
        }

        $subData['REG_NUMBER'] = $row['REG_NUMBER'] . " / " . $row['SUBJECT'];
        $subData['CUSTOMER'] = $row['CUST_NAME'] . '(' . $row['CUST_ID'] . ')';
        $subData['RECIPIENT_NAME'] = $row['RECIPIENT_NAME'];
        $subData['BRANCH_NAME'] = $row['BRANCH_NAME'];
        $subData['COUNTER_WARRANTY_TYPE'] = $arrWaranty[$row['COUNTER_WARRANTY_TYPE']];
        $subData['AMOUNT'] = $ccyid . ' ' . Application_Helper_General::displayMoneyplain($row['AMOUNT']);


        if ($row['IS_AMENDMENT'] == 1) {
          $subData['IS_AMENDMENT'] = 'Amendment Changes';
        } else if ($row['IS_AMENDMENT'] == 0) {
          $subData['IS_AMENDMENT'] = "New";
        } else {
          $subData['IS_AMENDMENT'] = "Amendment Draft";
        }

        $allData[] = $subData;
      }
    }

    if ($this->_getParam('csv')) {

      $this->_helper->download->csv(array($this->language->_('RegNumber / Subject'), $this->language->_('Aplicant'), $this->language->_('Obligee'), $this->language->_('Branch'), $this->language->_('Counter Type'), $this->language->_('BG Amount'), $this->language->_('Type')), $allData, null, 'Approve Process List');
    } else if ($this->_request->getParam('print') == 1) {

      $fields = array(
        'regno'     => array(
          'field'    => 'REG_NUMBER',
          'label'    => $this->language->_('RegNumber / Subject'),
        ),
        'aplicant'     => array(
          'field'    => 'CUSTOMER',
          'label'    => $this->language->_('Aplicant'),
        ),
        'recipientname'     => array(
          'field'    => 'RECIPIENT_NAME',
          'label'    => $this->language->_('Obligee'),
        ),
        'bankbranch'     => array(
          'field'    => 'BRANCH_NAME',
          'label'    => $this->language->_('Branch'),
        ),
        'counter'     => array(
          'field'    => 'COUNTER_WARRANTY_TYPE',
          'label'    => $this->language->_('Counter Type'),
        ),
        'bgamount'  => array(
          'field'    => 'AMOUNT',
          'label'    => $this->language->_('BG Amount'),
        ),
        'type'  => array(
          'field'    => 'IS_AMENDMENT',
          'label'    => $this->language->_('Type'),
        ),
      );

      $this->_forward('print', 'index', 'widget', array('data_content' => $allData, 'data_caption' => 'Approve Process List', 'data_header' => $fields));
    }

    Application_Helper_General::writeLog('ANCS', 'Lihat Daftar BG Permintaan Persetujuan Bank');
  }


  public function custAction()
  {
    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout()->disableLayout();

    $search = $this->_getParam('search');

    $ACBENEFArr = array();

    //      if ($priviBeneLinkage == true)
    //      {
    $select = $this->_db->select()
      ->from(array('A' => 'M_CUSTOMER'));
    if ($search) {

      $select->where("A.CUST_NAME LIKE ? ", '%' . $search . '%');
    }
    //echo $select;//die;


    $result = $this->_db->fetchAll($select);

    //var_dump($result); DIE;

    if (!empty($result)) {

      $opt .= '<ul id="customer-list">';

      foreach ($result as $key => $value) {
        $custname = "'" . $value['CUST_NAME'] . "'";
        $opt .= '<li onClick="selectcustomer(' . $custname . ')">' . $value['CUST_NAME'] . '</li>';
      }

      $opt .= '</ul>';
    }
    echo $opt;
  }

  // BEGIN PERBAIKAN TEMUAN TO 13 JUNI 2023 
  public function validatebtn($transfertype, $amount, $ccy, $psnumb)
	{
		//die;


		$selectuser	= $this->_db->select()
			->from(array('C' => 'M_APP_BGBOUNDARY'), array(
				'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
				'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
				'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
				'C.TRANSFER_TYPE',
				'C.POLICY'
			))

			->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
			->where("C.BOUNDARY_MIN 	<= ?", $amount)
			->where("C.BOUNDARY_MAX 	>= ?", $amount)
			->limit(1);


		//echo $selectuser;die;

		$datauser = $this->_db->fetchAll($selectuser);

		if (empty($datauser)) {

			return true;
		}

		$selectusergroup	= $this->_db->select()
			->from(array('C' => 'M_APP_GROUP_BUSER'), array(
				'*'
			))

			->where("C.BUSER_ID 	= ?", (string) $this->_userIdLogin);

		$usergroup = $this->_db->fetchAll($selectusergroup);

		$this->view->boundarydata = $datauser;
		//var_dump($usergroup);die;
		// print_r($this->view->boundarydata);die;
		if (!empty($usergroup)) {
			$cek = false;

			foreach ($usergroup as $key => $value) {

				$group = explode('_', $value['GROUP_BUSER_ID']);
				$alphabet = array(1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'E', 6 => 'F', 7 => 'G', 8 => 'H', 9 => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z');
				$groupalfa = $alphabet[(int) $group[2]];
				//print_r($groupalfa);echo '-';
				$usergroup[$key]['GROUP'] = $groupalfa;
				//var_dump($usergroup);
				// print_r($datauser);die;
				foreach ($datauser as $nub => $val) {
					$command = str_replace('(', '', $val['POLICY']);
					$command = str_replace(')', '', $command);
					$list = explode(' ', $command);

					//var_dump($list);die;
					foreach ($list as $row => $data) {

						if ($data == $groupalfa) {
							$cek = true;
							//die('ter');
							break;
						}
					}
				}
			}



			if ($group[0] == 'S') {
				return true;
			}
			// echo $cek;
			// print_r($cek);die;
			if (!$cek) {
				// die('here');
				return false;
			}
		} else {
			return false;
		}


		$tempusergroup = $usergroup;
		//echo $cek;die;
		if ($cek) {

			$command = ' ' . $datauser['0']['POLICY'] . ' ';
			$command = strtoupper($command);

			$cleanCommand = trim(preg_replace('/\s+/', ' ', $command));

			//transform to php logical operator syntak
			$translate = array(
				'AND' => '&&',
				'OR' => '||',
				'THEN' => 'THEN$',
				'A' => '$A',
				'B' => '$B',
				'C' => '$C',
				'D' => '$D',
				'E' => '$E',
				'F' => '$F',
				'G' => '$G',
				'H' => '$H',
				'I' => '$I',
				'J' => '$J',
				'K' => '$K',
				'L' => '$L',
				'M' => '$M',
				'N' => '$N',
				'O' => '$O',
				'P' => '$P',
				'Q' => '$Q',
				'R' => '$R',
				// 'S' => '$S',
				'T' => '$T',
				'U' => '$U',
				'V' => '$V',
				'W' => '$W',
				'X' => '$X',
				'Y' => '$Y',
				'Z' => '$Z',
				'SG' => '$SG',
			);

			$phpCommand =  strtr($cleanCommand, $translate);
			//var_dump($phpCommand);die;
			$param = array(
				'0' => '$A',
				'1' => '$B',
				'2' => '$C',
				'3' => '$D',
				'4' => '$E',
				'5' => '$F',
				'6' => '$G',
				'7' => '$H',
				'8' => '$I',
				'9' => '$J',
				'10' => '$K',
				'11' => '$L',
				'12' => '$M',
				'13' => '$N',
				'14' => '$O',
				'15' => '$P',
				'16' => '$Q',
				'17' => '$R',
				// '18' => '$S',
				'19' => '$T',
				'20' => '$U',
				'21' => '$V',
				'22' => '$W',
				'23' => '$X',
				'24' => '$Y',
				'25' => '$Z',
				'26' => '$SG',
			);
			// print_r($phpCommand);die;
			/*function str_replace_first($from, $to, $content, $row)
			{
				$from = '/' . preg_quote($from, '/') . '/';
				return preg_replace($from, $to, $content, $row);
			}*/

			$command = str_replace('(', ' ', $val['POLICY']);
			$command = str_replace(')', ' ', $command);
			$list = explode(' ', $command);

			// print_r($list);die;
			//var_dump($command);die;

			$thendata = explode('THEN', $command);

			//print_r($thendata);echo '<br>';die;
			$cthen = count($thendata);
			// print_r($thendata);die;
			$secondcommand = str_replace('(', '', trim($thendata[0]));
			$secondcommand = str_replace(')', '', $secondcommand);
			$secondcommand = str_replace('AND', '', $secondcommand);
			$secondcommand = str_replace('OR', '', $secondcommand);
			$secondlist = explode(' ', $secondcommand);

			// print_r($secondlist);die;
			// print_r($grouplist);die;
			// print_r($thendata[$i]);die;
			// return true;
			if (!empty($secondlist)) {
				foreach ($usergroup as $key => $value) {

					foreach ($secondlist as $row => $thenval) {


						// print_r($value['GROUP']);echo ' == ';echo $thenval;echo '<br/>';
						if (trim($value['GROUP']) == trim($thenval)) {
							$thengroup = true;
							$grouplist[] = trim($thenval);
							//die('here');
						}
					}
				}
			}

			//var_dump($cthen);die;
			if ($cthen >= 2) {
				foreach ($usergroup as $key => $value) {
					// print_r($value);
					foreach ($thendata as $row => $thenval) {
						// echo '|';print_r($thenval);echo '==';
						// print_r($value['GROUP']);echo '|';echo '<br/>';
						// $thengroup = true;
						$newsecondcommand = str_replace('(', '', trim($thenval));
						$newsecondcommand = str_replace(')', '', $newsecondcommand);
						$newsecondcommand = str_replace('AND', '', $newsecondcommand);
						$newsecondcommand = str_replace('OR', '', $newsecondcommand);
						$newsecondlist = explode(' ', $newsecondcommand);
						//var_dump($newsecondcommand);
						if (in_array(trim($value['GROUP']), $newsecondlist)) {
							//if (trim($value['GROUP']) == trim($thenval)) {
							$thengroup = true;
							$grouplist[] = trim($thenval);
							//die('here');
						}
					}
				}
			}

			//var_dump($grouplist);die;
			//var_dump($thengroup);die;
			// var_dump($thengroup);die;
			// // print_r($group);die;
			// // echo $thengroup;die;
			//echo '<pre>';
			//var_dump($thengroup);
			//var_dump($thendata);
			//print_r($thendata);echo '<br/>';die('here');


			if ($thengroup == true) {


				for ($i = 1; $i <= $cthen; ++$i) {
					$oriCommand = $phpCommand;
					//echo $oriCommand;die;
					$indno = $i;
					//echo $oriCommand;echo '<br>';

					for ($a = $cthen - $indno; $a >= 1; --$a) {


						if ($i > 1) {
							$replace = 'THEN$ $' . trim($thendata[$a + 1]);
						} else {
							$replace = 'THEN$ $' . trim($thendata[$a]);
						}

						$oriCommand = str_replace($replace, "", $oriCommand);
					}

					//print_r($thendata);echo '<br>';die();

					//die;
					// if($i == 3){
					// echo 'command : ';echo $oriCommand;echo '<br/>';
					// }
					//print_r($oriCommand);echo '<br>';
					//print_r($list);echo '<br>';die;


					$result = $this->generate($oriCommand, $list, $param, $psnumb, $groupalfa, $thengroup);

					// print_r($i);
					//var_dump($result);
					//echo 'result-';var_dump($result);echo '-';die;
					if ($result) {
						// die;
						// echo $thendata[$i+1];die('eere');
						// print_r($i);
						// print_r($thendata);die;



						$replace = 'THEN$ $' . trim($thendata[$i + 1]);
						// var_dump($replace);die;
						// print_r($i);
						if (!empty($thendata[$i + 1])) {
							//die;
							$oriCommand = str_replace($replace, "", $phpCommand);
						} else {
							// die;
							$thirdcommand = str_replace('(', '', trim($thendata[$i - 1]));
							$thirdcommand = str_replace(')', '', $thirdcommand);
							$thirdcommand = str_replace('AND', '', $thirdcommand);
							$thirdcommand = str_replace('OR', '', $thirdcommand);
							$thirdlist = explode(' ', $thirdcommand);
							//						var_dump($secondlist);
							//						var_dump($grouplist);
							//							die;

							if (!empty($secondlist)) {
								foreach ($grouplist as $key => $valg) {
									foreach ($secondlist as $row => $value) {
										if ($value == $valg) {
											//echo 'sini';

											return false;
										}
									}
								}
							}
							$oriCommand = $phpCommand;
						}
						// print_r($thendata[$i]);die;
						// if($i == 3){
						// echo $oriCommand;die;
						// echo '<br/>';	
						// }
						// print_r($i);
						//echo $oriCommand;
						$result = $this->generate($oriCommand, $list, $param, $psnumb, $groupalfa, $thengroup);
						//var_dump($result);echo '<br/>';
						if (!$result) {
							// die;
							//print_r($groupalfa);
							//print_r($thendata[$i]);

							if ($groupalfa == trim($thendata[$i])) {

								return true;
							} else {

								//return true;
							}
							/*
							foreach ($grouplist as $key => $valg) {
								 
								if (trim($valg) == trim($thendata[$i])) {
									// die;
									// print_r($i);
									// print_r($thendata);
									// print_r($thendata[$i]);die;
									if ($thendata[$i + 1] == $valg) {
										return true;
									} else {
										// die('here');
										return false;
									}
								}
							} */
						} else {

							//return false;

							// $result = $this->generate($phpCommand,$list,$param,$psnumb);
							// print_r($phpCommand);
							// if($result){}
							// die('here');
						}
						// var_dump($result);
						// die;


					} else {
						//die('here');
						$secondcommand = str_replace('(', '', trim($thendata[$i - 1]));
						$secondcommand = str_replace(')', '', $secondcommand);
						$secondcommand = str_replace('AND', '', $secondcommand);
						$secondcommand = str_replace('OR', '', $secondcommand);
						$secondlist = explode(' ', $secondcommand);

						//var_dump($i);
						// var_dump($thendata);
						//print_r($grouplist);
						//print_r($list);
						//die;
						//if($groupalfa == $gro)
						$approver = array();
						$countlist = array_count_values($list);

						foreach ($list as $key => $value) {

							if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {

								$selectapprover	= $this->_db->select()
									->from(array('C' => 'T_BGAPPROVAL'), array(
										'USER_ID'
									))

									// ->where("C.USER_ID 	= ?" , (string)$this->_userIdLogin)
									->where("C.REG_NUMBER = ?", (string) $psnumb)
									->where("C.GROUP = ?", (string) $value);
								//echo $selectapprover;die;
								$usergroup = $this->_db->fetchAll($selectapprover);


								// print_r($usergroup);
								$approver[$value] = $usergroup;

								if ($countlist[$value] == count($approver[$value]) && $tempusergroup['0']['GROUP'] == $value) {
									//var_dump($countlist[$value]);
									//var_dump($tempusergroup['0']['GROUP']);  
									//die('gere');
									return false;
								}
							}
						}

						//$array = array(1, "hello", 1, "world", "hello");


						//echo '<pre>';
						//var_dump($list);
						//var_dump($approver);
						//die;
						//var_dump($secondlist[0]);die;
						//foreach($approver as $app => $valpp){


						if (!empty($thendata[$i])) {

							if (empty($approver[$secondlist[0]]) && $groupalfa != $secondlist['0']) {
								//die('gere');

								return false;
							}
						}

						//echo '<pre>';
						//var_dump($thendata);
						//var_dump($grouplist);
						//var_dump($approver);
						//var_dump($groupalfa);
						//die;
						foreach ($grouplist as $key => $vg) {
							$newgroupalpa = str_replace('AND', '', $vg);
							$newgroupalpa = str_replace('OR', '', $newgroupalpa);
							$groupsecondlist = explode(' ', $newgroupalpa);

							//var_dump($groupsecondlist);
							//var_dump($groupalfa);die;
							//&& empty($approver[$groupalfa])
							if (in_array($groupalfa, $groupsecondlist)) {
								//echo 'gere';die;
								return true;
								//die('ge');
							}

							if ($vg == $groupalfa  && count($approver[$groupalfa]) > 0) {
								// echo 'gere';die;
								return true;
								//die('ge');
							}
						}

						// var_dump($approver[$groupalfa]);
						// var_dump($grouplist);
						//var_dump($groupalfa);die;

						//echo 'ger';die;
						//print_r($secondlist);die;
						//	 return true;
						if (!empty($secondlist)) {
							foreach ($grouplist as $key => $valg) {
								foreach ($secondlist as $row => $value) {
									if ($value == $valg) {

										if (empty($thendata[1])) {
											//die;
											return true;
										}
										//else{
										//	return false;
										//	}
									}
								}
							}
						}

						//	echo 'here';die;
						$secondresult = $this->generate($thendata[$i - 1], $list, $param, $psnumb, $groupalfa, $thengroup);
						// var_dump($secondresult);
						//print_r($thendata[$i-1]);
						//die;
						//if()
						//	 echo '<pre>';
						//var_dump($grouplist);die;
						foreach ($grouplist as $key => $valgroup) {
							//print_r($valgroup);
							//var_dump($thendata[$i]); 


							if (trim($valg) == trim($thendata[$i])) {
								$cekgroup = false;
								//die('here');
								if ($secondresult) {

									return false;
								} else {
									//die;
									return true;
								}

								//break;
							}

							//else if (trim($valg) == trim($thendata[$i - 1])) {
							//		$cekgroup = false;
							// die('here');
							//	return false;
							//	}
						}
						//die;
						//if (!$cekgroup) {
						// die('here');
						//return false;
						//}
					}

					//		    echo '<br/>';
					//	${$command} = $oriCommand;
				}
			} else if (!empty($thendata) && $thengroup == false) {

				//var_dump($thendata);
				foreach ($thendata as $ky => $vlue) {
					$newsecondcommand = str_replace('(', '', trim($vlue));
					$newsecondcommand = str_replace(')', '', $newsecondcommand);
					$newsecondcommand = str_replace('AND', '', $newsecondcommand);
					$newsecondcommand = str_replace('OR', '', $newsecondcommand);
					$newsecondlist = explode(' ', $newsecondcommand);
					//echo $newsecondlist['0'];
					//echo $groupalfa;die;
					//var_dump($newsecondlist);
					if ($newsecondlist['0'] == $groupalfa) {
						return true;
					}
				}
				return false;
			}




			$approver = array();
			// print_r($list);die;  	
			foreach ($list as $key => $value) {
				if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
					$selectapprover	= $this->_db->select()
						->from(array('C' => 'T_BGAPPROVAL'), array(
							'USER_ID'
						))

						// ->where("C.USER_ID 	= ?" , (string)$this->_userIdLogin)
						->where("C.REG_NUMBER = ?", (string) $psnumb)
						->where("C.GROUP = ?", (string) $value);
					//	 echo $selectapprover;
					$usergroup = $this->_db->fetchAll($selectapprover);
					// print_r($usergroup);
					$approver[$value] = $usergroup;
				}
			}
			//die;




			// print_r($phpCommand);die;
			foreach ($param as $url) {
				if (strpos($phpCommand, $url) !== FALSE) {
					$ta = substr_count($phpCommand, $url);
					// print_r($list);die;

					if (!empty($approver)) {
						// print_r($approver);die;
						foreach ($list as $key => $value) {
							if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
								foreach ($approver[$value] as $row => $val) {
									// print_r($approver);die;
									if (!empty($val)) {
										$values = 'G' . $value;
										${$values}[$row + 1] = true;
										// print_r($B);
									}

									// print_r($val);
								}
							}
						}
					}

					for ($i = 1; $i <= $ta; $i++) {
						// print_r($list);die;
						foreach ($list as $key => $value) {
							if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
								$values = 'G' . $value;
								// print_r(${$values});
								if (empty(${$values}[$i])) {
									${$values}[$i] = false;
								}
								// if(${$value}[$i])
							}
						}
						// print_r($phpCommand);die;
						$BG_NUMBER = $i;
						$label = str_replace('$', '$G', $url);

						$replace = $label . '[' . $BG_NUMBER . ']';

						$alf = str_replace('$', '', $url);
						$values = 'G' . $alf;
						// print_r($values);die;
						if (${$values}[$i] == $replace) {
							$phpCommand = $this->str_replace_first($url, 'true', $phpCommand, 1);
							// print_r($phpCommand);
						} else {
							$phpCommand = $this->str_replace_first($url, 'false', $phpCommand, 1);
							// print_r($phpCommand);die;
						}
						// }
						// }

					}
					// print_r($GB);die;

				}
			}

			$keywords = preg_split("/[\s,]+/", $cleanCommand);
			$result =  false;
			$thendata = explode('THEN$', $phpCommand);
			//var_dump($thendata);die;
			if (!empty($thendata['1'])) {
				$phpCommand = '';
				foreach ($thendata as $tkey => $tval) {
					$phpCommand .= '(';
					$phpCommand .= $tval . ')';
					if (!empty($thendata[$tkey + 1])) {
						$phpCommand .= ' && ';
					}
				}
			} else {

				$phpCommand = str_replace('THEN$', '&&', $phpCommand);
			}
			//var_dump($phpCommand);
			if (!empty($phpCommand)) {
				eval('$result = ' . "$phpCommand;");
			} else {
				return false;
			}
			//var_dump($result);die;
			if (!$result) {

				return true;
			}
			//die('here2');
			//var_dump ($result);die;
			//return $result;
		} else {
			//die('here');
			return true;
		}
	}

  private function str_replace_first($from, $to, $content, $row)
	{
		$from = '/' . preg_quote($from, '/') . '/';
		return preg_replace($from, $to, $content, $row);
	}

  public function generate($command, $list, $param, $psnumb, $group, $thengroup)
	{

		$phpCommand = $command;

		// echo $command;die;

		$approver = array();

		$count_list = array_count_values($list);
		//print_r($count_list);
		foreach ($list as $key => $value) {
			if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
				$selectapprover	= $this->_db->select()
					->from(array('C' => 'T_BGAPPROVAL'), array(
						'USER_ID'
					))

					// ->where("C.USER_ID 	= ?" , (string)$this->_userIdLogin)
					->where("C.REG_NUMBER = ?", (string) $psnumb)
					->where("C.GROUP = ?", (string) $value);
				// echo $selectapprover;
				$usergroup = $this->_db->fetchAll($selectapprover);
				// print_r($usergroup);
				$approver[$value] = $usergroup;
			}
		}
		//var_dump($param);die;
		//var_dump($group);
		foreach ($approver as $appval) {
			$totaldata = count($approver[$group]);
			$totalgroup = $count_list[$group];
			//var_dump($totaldata);
			//var_dump($totalgroup);
			if ($totalgroup == $totaldata && $totalgroup != 0) {

				return false;
			}
		} //die;
		//die;





		foreach ($param as $url) {

			if (strpos($phpCommand, $url) !== FALSE) {
				$ta = substr_count($phpCommand, $url);
				// print_r($list);die;

				if (!empty($approver)) {
					// print_r($approver);die;
					foreach ($list as $key => $value) {
						if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
							foreach ($approver[$value] as $row => $val) {
								// print_r($approver);die;
								if (!empty($val)) {
									$values = 'G' . $value;
									${$values}[$row + 1] = true;
									// print_r($B);
								}

								// print_r($val);
							}
						}
					}
				}


				// print_r($approver);die;

				for ($i = 1; $i <= $ta; $i++) {

					foreach ($list as $key => $value) {
						if (!empty($value)) {
							if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
								$values = 'G' . $value;
								// print_r(${$values});
								if (empty(${$values}[$i])) {
									${$values}[$i] = false;
								}
								// if(${$value}[$i])
							}
						}
					}


					$BG_NUMBER = $i;
					$label = str_replace('$', '$G', $url);
					// print_r($phpCommand);die('here');
					$replace = $label . '[' . $BG_NUMBER . ']';

					$alf = str_replace('$', '', $url);
					$values = 'G' . $alf;

					if (${$values}[$i] == $replace) {
						$phpCommand = $this->str_replace_first($url, 'true', $phpCommand, 1);
						// print_r($phpCommand);
					} else {
						$phpCommand = $this->str_replace_first($url, 'false', $phpCommand, 1);
						// print_r($phpCommand);die;
					}
					// }
					// }

				}
				// print_r($GB);die;

			}
		}

		$keywords = preg_split("/[\s,]+/", $cleanCommand);
		$result =  false;
		$phpCommand = str_replace('THEN$', '&&', $phpCommand);
		//print_r($phpCommand);echo '<br/>';

		if (!empty($phpCommand)) {
			eval('$result = ' . "$phpCommand;");
			//var_dump($thengroup);
			// var_dump($result);

			if ($result) {
				//var_dump($thengroup);die;
				if ($thengroup) {
					return true;
				} else {
					return false;
				}
			} else {

				if ($thengroup) {
					return false;
				} else {

					return true;
				}
			}
			// return $result;
		} else {
			return false;
		}

		// var_dump ($result);die;




	}
  // END PERBAIKAN TEMUAN TO 13 JUNI 2023 
}
