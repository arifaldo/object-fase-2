<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';

require_once 'SGO/Extendedmodule/Google/GoogleAuthenticator.php';
require_once 'SGO/Extendedmodule/phpqrcode/qrlib.php';
require_once 'Crypt/AESMYSQL.php';

class eformworkflow_SigningstagedetailController extends Application_Main
{

    protected $_moduleDB = 'RTF'; // masih harus diganti

    public function indexAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->systemType = $system_type;

        $stamp_fee = $settings->getSetting('stamp_fee');
        $this->view->stamp_fee = $stamp_fee;
        $adm_fee = $settings->getSetting('adm_fee');
        $this->view->adm_fee = $adm_fee;

        $this->view->systemType = $system_type;
        $this->view->ProvFee = 2000000;

        $conf = Zend_Registry::get('config');
        $this->_bankName = $conf['app']['bankname'];
        $this->view->masterbankname = $this->_bankName;

        $selectQuery    = "SELECT GOOGLE_CODE,USER_FAILEDTOKEN FROM M_USER
        WHERE CUST_ID = " . $this->_db->quote($this->_custIdLogin) . " AND USER_ID = " . $this->_db->quote($this->_userIdLogin) . " AND LTRIM(RTRIM(GOOGLE_CODE)) <> ''";
        // echo $selectQuery;
        $usergoogleAuth =  $this->_db->fetchAll($selectQuery);

        $selectcomp = $this->_db->select()
            ->from('M_CUSTOMER', array(
                'value' => new Zend_Db_Expr("CONCAT(CUST_NAME , ' (' , CUST_ID , ')  ' )"),
                'CUST_NAME', 'CUST_ID', 'CUST_TYPE'
            ))
            ->where('CUST_MODEL = ?', "3")
            ->where('CUST_STATUS = 1 ');

        $tempColumn = $this->_db->fetchAll($selectcomp);

        $this->view->custarr = json_encode($tempColumn);

        // var_dump($usergoogleAuth);die;
        if (!empty($usergoogleAuth)) {
            $this->view->googleauth = true;
            //var_dump($usergoogleAuth['0']['USER_FAILEDTOKEN']);
            $settingObj = new Settings();
            $maxtoken = $settingObj->getSetting("max_failed_token");
            $this->view->tokenfail = (int)$maxtoken - 1;
            if ($usergoogleAuth['0']['USER_FAILEDTOKEN'] != '0') {
                //die;
                $this->view->failedtoken = $usergoogleAuth['0']['USER_FAILEDTOKEN'];



                $tokenfail = (int)$maxtoken - (int)($usergoogleAuth['0']['USER_FAILEDTOKEN'] + 1);
                $this->view->tokenfail = $tokenfail;
            }
            if ($release) {
                $step = $this->_getParam('step');
            } else {
                $step = 3;
            }
        } else {
            $this->view->nogoauth = '1';
            if ($release) {
                $step = $this->_getParam('step');
            } else {
                $step = 3;
            }
        }



        $this->view->googleauth = true;


        // $numb = $this->_getParam('bgnumb');

        // decrypt numb
        $setting = new Settings();
        $enc_pass = $setting->getSetting('enc_pass');
        $enc_salt = $setting->getSetting('enc_salt');
        $sessionNamespace = new Zend_Session_Namespace('Tokenenc');
        $pw_hash = md5($enc_salt . $enc_pass);
        $rand = $this->_userIdLogin . date('dHis') . $pw_hash;
        $sessionNamespace->token     = $rand;
        $this->view->token = $sessionNamespace->token;

        $sessionNamespace = new Zend_Session_Namespace('Tokenenc');
        $password = $sessionNamespace->token;
        $this->view->token = $sessionNamespace->token;


        $AESMYSQL = new Crypt_AESMYSQL();

        $BG_NUMBER     = urldecode($this->_getParam('bgnumb'));

        $BG_NUMBER = $AESMYSQL->decrypt($BG_NUMBER, $password);

        // others attachment

        $checkOthersAttachment = $this->_db->select()
            ->from(["A" => "TEMP_BANK_GUARANTEE_FILE"], ["*"])
            ->where("BG_REG_NUMBER = '$BG_NUMBER'")
            ->order('A.INDEX ASC')
            ->query()->fetchAll();

        if (count($checkOthersAttachment) > 0) {
            $this->view->othersAttachment = $checkOthersAttachment;
        }

        // end others attachment

        if (!empty($BG_NUMBER)) {

            $verifyData = $this->_db->select()
                ->from(array('A' => 'TEMP_BGVERIFY_DETAIL'), array('*'))
                ->where('A.BG_REG_NUMBER = ?', $BG_NUMBER)
                ->query()->fetchAll();
            //var_dump($verifyData);die;
            if (!empty($verifyData)) {
                foreach ($verifyData as $vl) {
                    //if()
                    $label = 'check' . $vl['INDEX'];
                    //var_dump($label);
                    $this->view->$label = true;
                    $labeldate = 'verify' . $vl['INDEX'];
                    $verifydate = Application_Helper_General::convertDate($vl['VERIFIED'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
                    $this->view->$labeldate = $verifydate . ' by ' . $vl['VERIFIEDBY'];
                }
            }

            $bgdata = $this->_db->select()
                ->from(array('A' => 'TEMP_BANK_GUARANTEE'), array('*'))

                ->where('A.BG_REG_NUMBER = ?', $BG_NUMBER)
                //->where('A.BG_STATUS = ?', 2)
                ->query()->fetchAll();

            $refdata = $this->_db->select()
                ->from(array('A' => 'M_REFERALCODE'), array('*'))
                ->query()->fetchAll();

            $saveRefData = [];

            foreach ($refdata as $value) {
                $saveRefData[$value['CODE']] = $value["NAME"];
            }

            $this->view->refdataArr = $saveRefData;

            $selectcomp = $this->_db->select()
                ->from(array('A' => 'M_CUSTOMER'), array('*'))
                //  ->joinLeft(array('B' => 'M_COUNTRY'),'A.COUNTRY_CODE = B.COUNTRY_CODE',array('COUNTRY_NAME'))
                ->joinRight(array('B' => 'M_CITYLIST'), 'A.CUST_CITY = B.CITY_CODE', array('B.CITY_NAME'))
                ->where('A.CUST_ID =' . $this->_db->quote((string)$bgdata['0']['CUST_ID']))
                ->query()->fetchAll();

            $this->view->compinfo = $selectcomp[0];

            $bgdatadetail = $this->_db->select()
                ->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL'), array('*'))

                ->where('A.BG_REG_NUMBER = ?', $BG_NUMBER)
                ->query()->fetchAll();
            //$datas = $this->_request->getParams();
            //var_dump($bgdatadetail);


            if (!empty($bgdata)) {

                $data = $bgdata['0'];
                $this->view->data = $data;

                switch ($data["CHANGE_TYPE"]) {
                    case '0':
                        $this->view->suggestionType = "New";
                        break;
                    case '1':
                        $this->view->suggestionType = "Amendment Changes";
                        break;
                    case '2':
                        $this->view->suggestionType = "Amendment Draft";
                        break;
                }

                $policyBoundary = $this->findPolicyBoundary(38, $bgdata['0']['BG_AMOUNT']);
                //echo '<pre>';
                //var_dump($policyBoundary);die;
                $this->view->policyBoundary = $policyBoundary;

                $approverUserList = $this->findUserBoundary(38, $bgdata['0']['BG_AMOUNT']);

                $bgpublishType         = $conf["bgpublish"]["type"]["desc"];
                $bgpublishCode         = $conf["bgpublish"]["type"]["code"];

                $arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

                $this->view->BG_PUBLISH = $arrbgpublish[$data['BG_PUBLISH']];
				
				// start validasi 
				
                if ($bgdata['0']['COUNTER_WARRANTY_TYPE'] == '1') {
                    $bgdatasplit = $this->_db->select()
                        ->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
                        ->where('A.BG_NUMBER = ?', $BG_NUMBER)
                        ->query()->fetchAll();
					$jmlbgdatasplit = count($bgdatasplit);
					for($i=0;$i<$jmlbgdatasplit;$i++){
						
						//echo $bgdatasplit[$i]["AMOUNT"];
						$totalPenjaminan += $bgdatasplit[$i]["AMOUNT"];
						$temp_save = $this->_db->select()
							->from("M_CUSTOMER_ACCT")
							->where("ACCT_NO = ?", $bgdatasplit[$i]["ACCT"])
							->query()->fetchAll();
						

						$bgdatasplit[$i]["CURRENCY"] = $temp_save[$i]["CCY_ID"];
						$bgdatasplit[$i]["TYPE"] = $temp_save[$i]["ACCT_DESC"];
						
						
							//echo $bgdatasplit[$i]["ACCT"];
							//echo $temp_save[$i]["ACCT_DESC"];
							$svcAccount = new Service_Account($bgdatasplit[$i]["ACCT"],'IDR');
							if ($temp_save[$i]["ACCT_DESC"] == 'Giro'){
								$result = $svcAccount->inquiryAccountBalance('AB', FALSE);
								
								if ($result['response_code'] == '00' || $result['response_code'] == '0000') {
									if ($result['status'] != '1' && $result['status'] != '4') {
										//$err = true;
										$errmsg[$result['account_number']] = 'Status rekening tidak aktif';
										$this->view->errorStatusAccount = $errmsg;
										$valid = false;
									}
									
									$getProductType = $this->_db->select()
									->from(array('A' => 'M_PRODUCT_TYPE'), array('A.PRODUCT_CODE'))
									->where('A.PRODUCT_CODE IN (?)', $result['account_type'])
									->query()->fetchAll();
									if (empty($getProductType)) {
										//$err = true;
										$errmsg[$result['account_number']] = 'Product type tidak terdaftar di product management';
										$this->view->errorProductType = $errmsg;
										$valid = false;
									}
									
								
								} else {
									$err = true;
									$errmsg = $result['response_desc'];
									$this->view->errorwaranty = $errmsg;
									$valid = false;
								}
							
							}else if($temp_save[$i]["ACCT_DESC"] == 'Deposito'){
								
								$result = $svcAccount->inquiryDeposito('AB', FALSE);
								//var_dump($result);
								
								if ($result['response_code'] == '00' || $result['response_code'] == '0000') {
									if ($result['status'] != '1' && $result['status'] != '4') {
										//$err = true;
										$errmsg[$result['account_number']] = 'Status rekening tidak aktif';
										$this->view->errorStatusAccount = $errmsg;
										$valid = false;
									}
									
									$getProductType = $this->_db->select()
									->from(array('A' => 'M_PRODUCT_TYPE'), array('A.PRODUCT_CODE'))
									->where('A.PRODUCT_CODE IN (?)', $result['account_type'])
									->query()->fetchAll();
									if (empty($getProductType)) {
										//$err = true;
										$errmsg[$result['account_number']] = 'Product type tidak terdaftar di product management';
										$this->view->errorProductType = $errmsg;
										$valid = false;
									}
									
									if ($result['aro_status'] != 'ARO') {
										//$err = true;
										$errmsg[$result['account_number']] = 'Type bukan ARO';
										$this->view->errorAccountType = $errmsg;
										$valid = false;
									}
									
									if ($result['varian_rate'] != '0') {
										//$err = true;
										$errmsg[$result['account_number']] = 'Variant Rate bukan 0';
										$this->view->errorAccountRate = $errmsg;
										$valid = false;
									}
									
									
									
									$sqlRekeningJaminanExist = $this->_db->select()
									->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
									->where('A.BG_REG_NUMBER != ?', $data['BG_REG_NUMBER'])
									->where('A.ACCT = ?', $result['account_number']);
									
									$getRekeningJaminanExist = $sqlRekeningJaminanExist
									->query()->fetchAll();
									if (!empty($getRekeningJaminanExist)) {
										//$err = true;
										$errmsg[$result['account_number']] = 'Account Number sudah digunakan sebagai jaminan';
										$this->view->errorRekeningJaminanDeposito = $errmsg;
										$valid = false;
									}
									
								
								} else {
									$err = true;
									$errmsg = $result['response_desc'];
									$this->view->errorwaranty = $errmsg;
									$valid = false;
								}
							
							}
							
							//echo '2';
							//var_dump($result);//die;
							//var_dump($value["ACCT"]);
							
						
					}
					
                    $this->view->fullmember = $bgdatasplit;
					
					if ($totalPenjaminan < $data['BG_AMOUNT']){
						
						$errmsg = 'Total nilai penjaminan harus sama dengan / lebih besar dari nilai BG';
						$this->view->minpenjaminan_err = $errmsg;
						$valid = false;
					}
					
					$svcAccount = new Service_Account($data["FEE_CHARGE_TO"],'IDR');
					$result = $svcAccount->inquiryAccountBalance('AB', FALSE);
					
					if ($result['response_code'] == '00' || $result['response_code'] == '0000') {
						if ($result['status'] != '1' && $result['status'] != '4') {
							//$err = true;
							$errmsg = 'Status rekening tidak aktif';
							$this->view->errorChargeStatusAccount = $errmsg;
							$valid = false;
						}
								
						$getProductType = $this->_db->select()
						->from(array('A' => 'M_PRODUCT_TYPE'), array('A.PRODUCT_CODE'))
						->where('A.PRODUCT_CODE IN (?)', $result['account_type'])
						->query()->fetchAll();
						if (empty($getProductType)) {
							//$err = true;
							$errmsg = 'Product type tidak terdaftar di product management';
							$this->view->errorChargeProductType = $errmsg;
							$valid = false;
						}
						
						$getCustomerAccountCharge = $this->_db->select()
						->from(array('A' => 'M_CUSTOMER_ACCT'), array('A.CCY_ID'))
						->where('A.ACCT_NO = ?', $data['FEE_CHARGE_TO'])
						->where('A.CUST_ID = ?', $data['CUST_ID'])
						->query()->fetchAll();
						
						$this->view->chargeto_ccy = $getCustomerAccountCharge[0]['CCY_ID'];
						if ($getCustomerAccountCharge[0]['CCY_ID'] !="IDR") {
							//$err = true;
							$errmsg = 'mata uang bukan IDR';
							$this->view->errorChargeCurrency = $errmsg;
							$valid = false;
						}
								
					} else {
						$err = true;
						$errmsg = $result['response_desc'];
						$this->view->errorwaranty = $errmsg;
						$valid = false;
					}
                }
				
				if ($bgdata['0']['COUNTER_WARRANTY_TYPE'] == '2' || $bgdata['0']['COUNTER_WARRANTY_TYPE'] == '3') {
					$getCustomerStatus = $this->_db->select()
						->from(array('A' => 'M_CUSTOMER'), array('A.CUST_STATUS'))
						->where('A.CUST_ID = ?', $data['BG_INSURANCE_CODE'])
						->query()->fetchAll();
					
					if ($getCustomerStatus[0]['CUST_STATUS'] !="1") { //APPROVED
						//$err = true;
						$errmsg = 'Status asuransi bukan approved';
						$this->view->errorStatusAsuransi = $errmsg;
						$valid = false;
					}
					
					$get_linefacility_insurace = $this->_db->select()
					->from("M_CUST_LINEFACILITY", ["PLAFOND_LIMIT", "FEE_PROVISION", "FEE_ADMIN", "FEE_STAMP", "CUST_ID","STATUS","TICKET_SIZE"])
					->where("CUST_ID = ?", $data["BG_INSURANCE_CODE"])
					->query()->fetchAll();
					
					
					$percent10 = (10 / 100) * (float)$get_linefacility_insurace[0]["PLAFOND_LIMIT"];
					
					if ($get_linefacility_insurace[0]["TICKET_SIZE"] == 1) {
						
						$ticketSizeAmount = $percent10;
					} elseif ($get_linefacility_insurace[0]["TICKET_SIZE"] == 2) {
						
						$ticketSizeAmount = $get_linefacility_insurace[0]["PLAFOND_LIMIT"];
					} else {
						
						$ticketSizeAmount = $get_linefacility_insurace[0]["TICKET_SIZE"];
					}
					
					
					if ($data['BG_AMOUNT'] >= $ticketSizeAmount) { 
						//$err = true;
						$errmsg = 'BG Nominal yang diajukan tidak boleh lebih besar dari ticket size asuransi';
						$this->view->errorTickerSize = $errmsg;
						$valid = false;
					}	
						
						
				}
				
				// end validasi

                $selectHistory    = $this->_db->select()
                    ->from('T_BANK_GUARANTEE_HISTORY')
                    ->where("BG_REG_NUMBER = ?", $BG_NUMBER);

                $history = $this->_db->fetchAll($selectHistory);

                $cust_approver = 1;
                foreach ($history as $row) {
                    //if maker done
                    if ($row['HISTORY_STATUS'] == 1 || $row['HISTORY_STATUS'] == 4 || $row['HISTORY_STATUS'] == 14) {
                        $makerStatus = 'active';
                        $makerIcon = '<i class="fas fa-check"></i>';

                        $makerOngoing = '';
                        if ($cust_reviewer == 0 && $cust_approver == 0) { //kalau tidak ada priv reviewer & approver
                            $reviewerOngoing = '';
                            $approverOngoing = '';
                            //$releaserOngoing = 'ongoing';
                            $releaserOngoing = '';
                        } else {
                            //$reviewerOngoing = 'ongoing';
                            $reviewerOngoing = '';
                            $approverOngoing = '';
                            $releaserOngoing = '';
                        }

                        $custlogin = $row['USER_LOGIN'];

                        $selectCust    = $this->_db->select()
                            ->from('M_USER')
                            ->where("USER_ID = ?", $custlogin)
                            ->where("CUST_ID = ?", $row['CUST_ID']);

                        $customer = $this->_db->fetchAll($selectCust);

                        $custFullname = $customer[0]['USER_FULLNAME'];
                        $custEmail       = $customer[0]['USER_EMAIL'];
                        $custPhone      = $customer[0]['USER_PHONE'];

                        $makerApprovedBy = $custFullname;

                        $efdate = date('d-M-Y', strtotime($row['DATE_TIME']));

                        $align = 'align="center"';
                        $marginRight = '';

                        if ($cust_reviewer == 0 && $cust_approver == 0) {
                            $align = '';
                            $marginRight = 'style="margin-right: 15px;"';
                        }

                        $this->view->makerApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span ' . $marginRight . '>' . $custFullname . '</span></div>';
                    }

                    if ($row['HISTORY_STATUS'] == 15) {
                        $verifyStatus = 'active';
                        $verifyIcon = '<i class="fas fa-check"></i>';

                        $verifyOngoing = '';
                        if ($cust_reviewer == 0 && $cust_approver == 0) { //kalau tidak ada priv reviewer & approver
                            $reviewerOngoing = '';
                            $verifyOngoing = '';
                            $approverOngoing = '';
                            //$releaserOngoing = 'ongoing';
                            $releaserOngoing = '';
                        } else {
                            //$reviewerOngoing = 'ongoing';
                            $reviewerOngoing = '';
                            $verifyOngoing = '';
                            $approverOngoing = '';
                            $releaserOngoing = '';
                        }

                        $custlogin = $row['USER_LOGIN'];

                        $selectCust    = $this->_db->select()
                            ->from('M_USER')
                            ->where("USER_ID = ?", $custlogin)
                            ->where("CUST_ID = ?", $row['CUST_ID']);

                        $customer = $this->_db->fetchAll($selectCust);

                        $custFullname = $customer[0]['USER_FULLNAME'];
                        $custEmail       = $customer[0]['USER_EMAIL'];
                        $custPhone      = $customer[0]['USER_PHONE'];

                        $verifyApprovedBy = $custFullname;

                        $efdate = date('d-M-Y', strtotime($row['DATE_TIME']));

                        $align = 'align="center"';
                        $marginRight = '';

                        if ($cust_reviewer == 0 && $cust_approver == 0) {
                            $align = '';
                            $marginRight = 'style="margin-right: 15px;"';
                        }

                        $this->view->verifyApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span ' . $marginRight . '>' . $custFullname . '</span></div>';
                    }
                    //if reviewer done

                    //if approver done
                    if ($row['HISTORY_STATUS'] == 16) {
                        $makerStatus = 'active';
                        $approveStatus = '';
                        $reviewStatus = '';

                        $makerOngoing = '';
                        $reviewerOngoing = '';
                        $approverOngoing = 'ongoing';
                        $releaserOngoing = '';

                        $custlogin = $row['USER_LOGIN'];

                        $selectuserapp    = $this->_db->select()
                            ->from(array('C' => 'T_APPROVAL'), array(
                                '*'
                            ))
                            ->where("C.PS_NUMBER = ?", $BG_NUMBER)
                            ->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
                            //->where("C.USER_ID 	= ?", (string) $this->_userIdLogin)
                        ;

                        $userapprove = $this->_db->fetchAll($selectuserapp);
                        //tampung data user yang sudah approve
                        if (!empty($userapprove) && empty($userid)) {
                            foreach ($userapprove as $vl) {
                                $userid[] = $vl['USER_ID'];
                            }
                        }
                        //$userid[] = $custlogin;

                        $approveEfDate[] = date('d-M-Y', strtotime($row['DATE_TIME']));
                    }
                    //if releaser done
                    if ($row['HISTORY_STATUS'] == 5) {
                        $makerStatus = 'active';
                        /*$approveStatus = 'active';
                                            $reviewStatus = 'active';
                                            $releaseStatus = 'active';
                                            $releaseIcon = '<i class="fas fa-check"></i>';*/
                        $approveStatus = '';
                        $reviewStatus = '';
                        $releaseStatus = '';
                        $releaseIcon = '';

                        $makerOngoing = '';
                        $reviewerOngoing = '';
                        $approverOngoing = '';
                        $releaserOngoing = '';

                        $custlogin = $row['USER_LOGIN'];

                        $selectCust    = $this->_db->select()
                            ->from('M_USER')
                            ->where("USER_ID = ?", $custlogin)
                            ->where("CUST_ID = ?", $row['CUST_ID']);

                        $customer = $this->_db->fetchAll($selectCust);

                        $custFullname = $customer[0]['USER_FULLNAME'];
                        // $custEmail 	  = $customer[0]['USER_EMAIL'];
                        // $custPhone	  = $customer[0]['USER_PHONE'];

                        $releaserApprovedBy = $custFullname;

                        $align = 'align="center"';
                        $marginLeft = '';
                        if ($cust_reviewer == 0 && $cust_approver == 0) {
                            $align = '';
                            $marginLeft = 'style="margin-left: 15px;"';
                        }

                        $efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
                        //$this->view->releaserApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span ' . $marginLeft . '>' . $custFullname . '</span></div>';
                    }
                }

                //approvernamecircle jika sudah ada yang approve
                if (!empty($userid)) {

                    $alphabet = array('01' => 'A', '02' => 'B', '03' => 'C', '04' => 'D', '05' => 'E', '06' => 'F', '07' => 'G', '08' => 'H', '09' => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z', 27 => 'SG');

                    $flipAlphabet = array_flip($alphabet);

                    $approvedNameList = array();
                    $i = 0;
                    //var_dump($userid);die;
                    foreach ($userid as $key => $value) {

                        //select utk nama dan email
                        $selectusername = $this->_db->select()
                            ->from(array('M_USER'), array(
                                '*'
                            ))
                            ->where("USER_ID = ?", (string) $value)
                            ->where("CUST_ID = ?", (string) $this->_custIdLogin);

                        $username = $this->_db->fetchAll($selectusername);

                        //select utk cek user berada di grup apa
                        $selectusergroup    = $this->_db->select()
                            ->from(array('C' => 'M_APP_GROUP_USER'), array(
                                '*'
                            ))
                            ->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
                            ->where("C.USER_ID 	= ?", (string) $value);

                        $usergroup = $this->_db->fetchAll($selectusergroup);

                        $groupuserid = $usergroup[0]['GROUP_USER_ID'];
                        $groupusername = $usergroup[0]['USER_ID'];
                        $groupuseridexplode = explode("_", $groupuserid);

                        if ($groupuseridexplode[0] == "S") {
                            $usergroupid = "SG";
                        } else {
                            $usergroupid = $alphabet[$groupuseridexplode[2]];
                        }

                        // $tempuserid = "";
                        // foreach ($approverNameCircle as $row => $data) {
                        // 	foreach ($data as $keys => $val) {
                        // 		if ($keys == $usergroupid) {
                        // 			if (preg_match("/active/", $val)) {
                        // 				continue;
                        // 			}else{
                        // 				if ($groupuserid == $tempuserid) {
                        // 					continue;
                        // 				}else{
                        // 					$approverNameCircle[$row][$keys] = '<button class="btnCircleGroup active hovertext" style="margin-right: 1%; margin-top: 12%;" disabled>3
                        // 			<span class="hovertextcontent" style="padding-left: 15px;">'.$username[0]['USER_FULLNAME'].'<br>'.$username[0]['USER_EMAIL'].'</span></button>';
                        // 				}
                        // 				$tempuserid = $groupuserid;
                        // 			}
                        // 		}
                        // 	}
                        // }

                        array_push($approvedNameList, $username[0]['USER_FULLNAME']);

                        $efdate = $approveEfDate[$i];

                        $approverApprovedBy[] = '<div align="center" class="textTheme">' . $efdate . '<br>' . $username[0]['USER_FULLNAME'] . ' (' . $usergroupid . ')</div>';
                        $i++;
                    }
                    //var_dump($approverApprovedBy);die;
                    $this->view->approverApprovedBy = $approverApprovedBy;


                    //kalau sudah approve semua
                    if (!$checkBoundary) {
                        $approveStatus = '';
                        $approverOngoing = '';
                        $approveIcon = '';
                        $releaserOngoing = 'ongoing';
                    }
                }



                $selectsuperuser = $this->_db->select()
                    ->from(array('C' => 'T_APPROVAL'))
                    ->where("C.PS_NUMBER 	= ?", $BG_NUMBER)
                    ->where("C.GROUP 	= 'SG'");

                $superuser = $this->_db->fetchAll($selectsuperuser);

                if (!empty($superuser)) {
                    $userid = $superuser[0]['USER_ID'];

                    //select utk nama dan email
                    $selectusername = $this->_db->select()
                        ->from(array('M_USER'), array(
                            '*'
                        ))
                        ->where("USER_ID = ?", (string) $userid)
                        ->where("CUST_ID = ?", (string) $this->_custIdLogin);

                    $username = $this->_db->fetchAll($selectusername);

                    $approverApprovedBy[] = '<div align="center" class="textTheme">' . $efdate . '<br>' . $username[0]['USER_FULLNAME'] . ' (' . $usergroupid . ')</div>';

                    $approveStatus = 'active';
                    $approverOngoing = '';
                    $approveIcon = '<i class="fas fa-check"></i>';
                    $releaserOngoing = 'ongoing';
                }
                // <span class="hovertextcontent" style="text-align: center;"><p class="m-0 text-white-50" style="font-size: 13px">'.$makerApprovedBy.'</p></span>
                //define circle
                $makerNameCircle = '<button id="makerCircle" style="cursor:default" class="btnCircleGroup ' . $makerStatus . ' ' . $makerOngoing . ' hovertext" disabled>' . $makerIcon . '
                                                                            
                                                                            </button>';

                foreach ($reviewerList as $key => $value) {

                    $textColor = '';
                    if ($value == $reviewerApprovedBy) {
                        $textColor = 'text-white-50';
                    }

                    $reviewerListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value . '</p>';
                }
                // 
                $reviewerNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $reviewStatus . ' ' . $reviewerOngoing . ' hovertext" disabled>' . $reviewIcon . '
                                                                            
                                                                            </button>';

                $verifyNameCircle = '<button style="cursor:default" class="btnCircleGroup  hovertext" disabled>' . $reviewIcon . '
                                                                            
                                                                            </button>';



                $groupNameList = $approverUserList['GROUP_NAME'];
                unset($approverUserList['GROUP_NAME']);

                if ($approverUserList != '') {
                    //echo '<pre>';
                    //var_dump($approverUserList);die;
                    foreach ($approverUserList as $key => $value) {
                        $approverListdata .= $key . ' (' . $groupNameList[$key] . ')' . '<br>';
                        $i = 1;
                        foreach ($value as $key2 => $value2) {

                            $textColor = '';
                            if (in_array($value2, $approvedNameList)) {
                                $textColor = 'text-white-50';
                            }

                            if ($i == count($value)) {
                                $approverListdata .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value2 . '</p><br>';
                            } else {
                                $approverListdata .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value2 . '</p>';
                            }
                            $i++;
                        }
                    }
                } else {
                    $approverListdata = 'There is no Approver User';
                }
                // 
                $spandata = '';
                if (!empty($approverListdata) && !$error_msg2) {
                    $spandata = '<span class="hovertextcontentapprover" style="text-align: center;">' . $approverListdata . '</span>';
                }

                $approverNameCircle = '<button class="btnCircleGroup ' . $approveStatus . ' ' . $approverOngoing . ' hovertext" disabled>' . $approveIcon . '
                                                                            ' . $spandata . '
                                                                            </button>';

                foreach ($releaserList as $key => $value) {

                    $textColor = '';
                    if ($value == $releaserApprovedBy) {
                        $textColor = 'text-white-50';
                    }

                    $releaserListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value . '</p>';
                }
                // 
                $releaserNameCircle = '<button id="releaserCircle" class="btnCircleGroup ' . $releaseStatus . ' ' . $releaserOngoing . ' hovertext" disabled>' . $releaseIcon . '
                                                                            <span class="hovertextcontent" style="left: -100px; text-align: center;">' . $releaserListView . '</span>
                                                                            </button>';


                $this->view->policyBoundary = $policyBoundary;
                $this->view->makerNameCircle = $makerNameCircle;
                $this->view->reviewerNameCircle = $reviewerNameCircle;
                $this->view->verifyNameCircle = $verifyNameCircle;

                $this->view->approverNameCircle = $approverNameCircle;
                $this->view->releaserNameCircle = $releaserNameCircle;

                $this->view->makerStatus = $makerStatus;
                $this->view->verifyStatus = $verifyStatus;
                $this->view->approveStatus = $approveStatus;
                $this->view->reviewStatus = $reviewStatus;
                $this->view->releaseStatus = $releaseStatus;


                $bgType         = $conf["bg"]["type"]["desc"];
                $bgCode         = $conf["bg"]["type"]["code"];

                $arrbgType = array_combine(array_values($bgCode), array_values($bgType));
                //var_dump($arrbgType);
                $this->view->arrbgType = $arrbgType;


                if (!empty($data['BG_BRANCH'])) {
                    $selectbranch = $this->_db->select()
                        ->from(array('A' => 'M_BRANCH'), array('*'))
                        ->where('A.BRANCH_CODE = ?', $data['BG_BRANCH'])
                        ->query()->fetchAll();
                    //var_dump($selectbranch[0]['BRANCH_NAME']);die;
                    $this->view->branchname = $selectbranch[0]['BRANCH_NAME'];
                }

                $this->view->updateStart = Application_Helper_General::convertDate($data['TIME_PERIOD_START'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
                $this->view->updateEnd = Application_Helper_General::convertDate($data['TIME_PERIOD_END'], $this->view->viewDateFormat, $this->view->defaultDateFormat);

                $this->view->TIME_PERIOD_START = $data['TIME_PERIOD_START'];
                $this->view->TIME_PERIOD_END = $data['TIME_PERIOD_END'];

                $config            = Zend_Registry::get('config');
                $BgType         = $config["bg"]["status"]["desc"];
                $BgCode         = $config["bg"]["status"]["code"];

                $arrStatus = array_combine(array_values($BgCode), array_values($BgType));

                $this->view->arrStatus = $arrStatus;

                $CustomerUser = new CustomerUser($data['CUST_ID'], $data['BG_CREATEDBY']);


                $param = array('CCY_IN' => 'IDR', 'ACCT_NO' => $data['FEE_CHARGE_TO']);
                $AccArr = $CustomerUser->getAccounts($param);
                //var_dump($AccArr);die;

                if (!empty($AccArr)) {
                    $this->view->src_name = $AccArr['0']['ACCT_NAME'];
                }

                $arrBankFormat = array(
                    1 => 'Bank Standard',
                    2 => 'Special Format (with bank approval)'
                );

                $this->view->bankFormat = $arrBankFormat[$data['BG_FORMAT']];

                $arrLang = array(
                    1 => 'Indonesian',
                    2 => 'English',
                    3 => 'Billingual',
                );

                $this->view->languagetext = $arrLang[$data['BG_LANGUAGE']];

                $arrWaranty = array(
                    1 => 'Full Cover, Savings Account (Giro / Time Deposit / Savings) or Cash Deposit (MD)',
                    2 => 'Bank Guarantee Line Facility',
                    3 => 'Insurance'

                );

                $this->view->warranty_type_text = $arrWaranty[$data['COUNTER_WARRANTY_TYPE']];

                if (!empty($data['USAGE_PURPOSE'])) {
                    $data['USAGE_PURPOSE'] = explode(',', $data['USAGE_PURPOSE']);
                    foreach ($data['USAGE_PURPOSE'] as $key => $val) {
                        $str = 'checkp' . $val;
                        //var_dump($str);
                        $this->view->$str =  'checked';
                    }
                }

                if ($data['BG_NUMBER'] == '') {
                    $data['BG_NUMBER'] = '-';
                }
                if ($data['BG_SUBJECT'] == '') {
                    $data['BG_SUBJECT'] = '- no subject -';
                }
                $this->view->BG_REG_NUMBER = $data['BG_REG_NUMBER'];
                $this->view->BG_NUMBER = $data['BG_NUMBER'];
                $this->view->BG_SUBJECT = $data['BG_SUBJECT'];
                $this->view->data = $data;
                if (empty($data['SP_OBGLEE_CODE'])) {
                    $this->view->COMPANY_ID = $this->_getParam('COMPANY_ID');;
                    $this->view->COMPANY_TEXT = $this->_getParam('COMPANY_TEXT');;
                } else {
                    $this->view->COMPANY_ID = $data['SP_OBGLEE_CODE'];
                }
                $this->view->bgprovtype = 'Provision IDR';
                $arrProvtype = array(
                    '1' => 'Provision %',
                    '2' => 'Provision IDR',
                );
                $this->view->provtypeArr = $arrProvtype;
                $this->view->recipent_name = $data['RECIPIENT_NAME'];
                $this->view->address = $data['RECIPIENT_ADDRES'];
                $this->view->city = $data['RECIPIENT_CITY'];
                $this->view->contact_number = $data['RECIPIENT_CONTACT'];
                $this->view->contact_person = $data['RECIPIENT_CP'];
                $this->view->phone = $data['RECIPIENT_OFFICE_NUMBER'];
                $this->view->contact_email = $data['RECIPIENT_EMAIL'];
                //$this->view->comment = $data['GUARANTEE_TRANSACTION'];

                $this->view->fileName = $data['FILE'];
                $this->view->bank_amount = $data['BG_AMOUNT'];
                $this->view->warranty_type = $data['COUNTER_WARRANTY_TYPE'];
                $this->view->account_number = $data['COUNTER_WARRANTY_ACCT_NO'];
                $this->view->account_name = $data['COUNTER_WARRANTY_ACCT_NAME'];

                $this->view->amount = $data['COUNTER_WARRANTY_AMOUNT'];
                $this->view->acct = $data['FEE_CHARGE_TO'];
                $this->view->status = $data['BG_STATUS'];

                $Settings = new Settings();
                $claim_period = $Settings->getSetting('claim_period');
                $this->view->claim_period = $claim_period;


                $this->view->GT_DATE = Application_Helper_General::convertDate($data['GT_DOC_DATE'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
                $this->view->GT_NUMBER = $data['GT_DOC_NUMBER'];
                $bgdocType         = $conf["bgdoc"]["type"]["desc"];
                $bgdocCode         = $conf["bgdoc"]["type"]["code"];

                $arrbgdoc = array_combine(array_values($bgdocCode), array_values($bgdocType));

                $this->view->GT_TYPE = $arrbgdoc[$data['GT_DOC_TYPE']];
                //$this->view->GT_TYPE = $data['GT_DOC_TYPE'];
                $this->view->GT_OTHERS = $data['GT_DOC_OTHER'];
                $bgpublishType         = $conf["bgpublish"]["type"]["desc"];
                $bgpublishCode         = $conf["bgpublish"]["type"]["code"];

                $arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

                $this->view->publishForm = $arrbgpublish[$data['BG_PUBLISH']];
                //var_dump($data['USAGE_PURPOSE']);die;
                $this->view->usage_purpose = $data['USAGE_PURPOSE'];
                $this->view->comment = $data['SERVICE'];


                if ($data['BG_STATUS'] == '7' ||  $data['BG_STATUS'] == '10' || !empty($data['BG_REJECT_NOTES']) || !empty($data['BG_CANCEL_NOTES'])) {
                    $selectQuery  = "SELECT
                                                                                a.USER_LOGIN,
                                                                                b.`USER_FULLNAME` AS u_name,
                                                                                c.`BUSER_NAME` AS b_name,
                                                                                a.DATE_TIME,
                                                                                a.BG_REASON,
                                                                                a.HISTORY_STATUS,
                                                                                a.BG_REG_NUMBER
                                                                                
                                                                                
                                                                                FROM
                                                                                T_BANK_GUARANTEE_HISTORY AS a
                                                                                LEFT JOIN M_USER AS b ON a.`USER_LOGIN` = b.`USER_ID` AND a.`CUST_ID` = b.`CUST_ID`
                                                                                LEFT JOIN `M_BUSER` AS c ON a.`USER_LOGIN` = c.`BUSER_ID`
                                                                                WHERE a.BG_REG_NUMBER = " . $this->_db->quote((string) $data['BG_REG_NUMBER']) . " AND a.HISTORY_STATUS = " . $this->_db->quote((string) $data['BG_STATUS']) . " GROUP BY HISTORY_ID ORDER BY DATE_TIME";
                    $result =  $this->_db->fetchAll($selectQuery);
                    if (!empty($result)) {
                        $data['REASON'] = $result['0']['BG_REASON'];
                    }
                    $this->view->reqrepair = true;
                    $this->view->reason = $data['REASON'] . '' . $data['BG_REJECT_NOTES'] . $data['BG_CANCEL_NOTES'];
                }



                if (!empty($bgdatadetail)) {
                    foreach ($bgdatadetail as $key => $value) {

                        if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
                            if ($value['PS_FIELDNAME'] == 'Insurance Name') {
                                $this->view->insuranceName =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
                                $this->view->PrincipalAgreement =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount') {
                                $this->view->insurance_amount =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement Start Date') {
                                $this->view->paDateStart =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement End Date') {
                                $this->view->paDateEnd =   $value['PS_FIELDVALUE'];
                            }
                        } else {

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 1') {
                                $this->view->owner1 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 1') {
                                $this->view->amountowner1 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
                                $this->view->owner2 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
                                $this->view->amountowner2 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
                                $this->view->owner3 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
                                $this->view->amountowner3 =   $value['PS_FIELDVALUE'];
                            }
                        }
                    }
                }



                $conf = Zend_Registry::get('config');
                $this->view->bankname = $conf['app']['bankname'];



                //echo '<pre>';
                //var_dump($data);

                $download = $this->_getParam('download');
                //print_r($edit);die;
                if ($download) {
                    $attahmentDestination = UPLOAD_PATH . '/document/submit/';
                    $this->_helper->download->file($data['FILE'], $attahmentDestination . $data['FILE']);
                }

                if ($this->_request->isPost()) {

                    // $datas = $this->_request->getParams();
                    // Zend_Debug::dump($datas);die;

                    $back = $this->_getParam('back');
                    if ($back) {
                        $this->_redirect('/eformworkflow/formatapproval');
                    }

                    $CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
                    $approve = $this->_getParam('release');
                    $savework = $this->_getParam('savework');
                    $redebate = $this->_getParam('redebate');
                    $resetall = $this->_getParam('reset');

                    //$BG_NUMBER = $this->_getParam('bgnumb');
                    $BG_NUMBER     = urldecode($this->_getParam('bgnumb'));

                    $BG_NUMBER = $AESMYSQL->decrypt($BG_NUMBER, $password);
                    $reject = $this->_getParam('reject');
                    $repair = $this->_getParam('repair');
                    $filter             = new Application_Filtering();
                    $inputtoken1        = $this->_getParam('inputtoken1');
                    $inputtoken2        = $this->_getParam('inputtoken2');
                    $inputtoken3        = $this->_getParam('inputtoken3');
                    $inputtoken4        = $this->_getParam('inputtoken4');
                    $inputtoken5        = $this->_getParam('inputtoken5');
                    $inputtoken6        = $this->_getParam('inputtoken6');

                    $responseCode       = $inputtoken1 . $inputtoken2 . $inputtoken3 . $inputtoken4 . $inputtoken5 . $inputtoken6;

                    $responseCode       = $filter->filter($responseCode, "SELECTION");
                    //print_r($edit);die;
                    //var_dump($approve);die;
                    if ($redebate) {
                        $data = array(
                            //'REDEBATE_STATUS' => $this->_getParam('verifyindex'),
                            'REDEBATE_TYPE' => $this->_getParam('redebate_type'),
                            'REDEBATE_AMOUNT' => $this->_getParam('REDEBATE_AMOUNT')
                        );

                        $where['BG_REG_NUMBER = ?'] = $BG_NUMBER;
                        $this->_db->update('TEMP_BANK_GUARANTEE', $data, $where);
                        $this->setbackURL('/' . $this->_request->getModuleName() . '/formatapproval/');
                        $this->_redirect('/notification/success/index');
                    }

                    if ($resetall) {

                        $where['BG_REG_NUMBER = ?'] = $BG_NUMBER;

                        $this->_db->delete('TEMP_BGVERIFY_DETAIL', $where);


                        $this->setbackURL('/' . $this->_request->getModuleName() . '/formatapproval/');
                        $this->_redirect('/notification/success/index');
                    }


                    if ($approve) {

                        $verifyData = $this->_db->select()
                            ->from(array('A' => 'TEMP_BGVERIFY_DETAIL'), array('*'))
                            ->where('A.BG_REG_NUMBER = ?', $BG_NUMBER)
                            ->query()->fetchAll();
                        //var_dump(count($verifyData));die;
                        $checkVerify = count($this->_getParam('verifyindex'));
                        //$datas = $this->_request->getParams();
                        $totalVerify = count($verifyData) + $checkVerify;
                        //var_dump($totalVerify);die;
                        if ($totalVerify == 7) {

                            /*if (!empty($usergoogleAuth)) {
                                                                                            
                                                                                            $pga = new PHPGangsta_GoogleAuthenticator();
                                                                                            // var_dump($usergoogleAuth['0']['GOOGLE_CODE']);
                                                                                            //var_dump($responseCode);die;
                                                                                            $settingObj = new Settings();
                                                                                            $gduration = $settingObj->getSetting("google_duration");
                                                                                            if ($pga->verifyCode($usergoogleAuth['0']['GOOGLE_CODE'], $responseCode, $gduration)) { */
                            $this->_db->beginTransaction();
                            $resultToken = $resHard['ResponseCode'] == '0000';
                            $datatoken = array(
                                'USER_FAILEDTOKEN' => 0
                            );

                            $wheretoken =  array();
                            $wheretoken['USER_ID = ?'] = $this->_userIdLogin;
                            $wheretoken['CUST_ID = ?'] = $this->_custIdLogin;
                            $data = $this->_db->update('M_USER', $datatoken, $wheretoken);

                            $refCode = $this->_getParam('refCode');
                            $obligeecode =  $this->_getParam('COMPANY_ID');

                            $attahmentDestination   = UPLOAD_PATH . '/document/submit/';
                            $errorRemark            = null;
                            $adapter                = new Zend_File_Transfer_Adapter_Http();
                            $datas = $this->_request->getParams();
                            $sourceFileName = trim($adapter->getFileName("uploadSource"));

                            //die('a');
                            if ($sourceFileName == null) {
                                $sourceFileName = null;
                                $fileType = null;
                            } else {

                                $sourceFileName = trim(substr(basename($adapter->getFileName("uploadSource")), 0));
                                //echo '<pre>';	
                                if ($_FILES["uploadSource"]["type"]) {
                                    $adapter->setDestination($attahmentDestination, "uploadSource");
                                    //echo "<pre>";
                                    //var_dump($attahmentDestination);
                                    //echo $adapter->getFileName();
                                    $files = $adapter->getFileInfo();
                                    //var_dump($files);
                                    //foreach ($files as $file) {
                                    // Print the MIME Type for $file
                                    $fileType = $_FILES["uploadSource"]['type'];

                                    //}      
                                    //die('b');
                                    //$fileType               = $adapter->getMimeType();

                                    $size                   = $_FILES["document"]["size"];
                                } else {
                                    // die('a');
                                    $fileType = null;
                                    $size = null;
                                }
                            }

                            $paramsName['sourceFileName']   = $sourceFileName;
                            // print_r($fileType);
                            $fileTypeMessage = explode('/', $fileType);
                            //$fileTypeMessage2 = explode('/',$fileType2);

                            //var_dump($fileTypeMessage[1]);die;
                            if (!empty($fileTypeMessage[1])) {
                                $fileType =  $fileTypeMessage[1];
                                //$extensionValidator = new Zend_Validate_File_Extension(array(false, $fileExt));
                                //$extensionValidator->setMessage("Extension file must be *.jpg /.jpeg / .pdf");

                                $size           = number_format($size);
                                $sizeTampil     = $size / 1000;
                                //$sizeValidator    = new Zend_Validate_File_Size(array('min' => 1, 'max' => $maxFileSize));
                                // $sizeValidator->setMessage("File size is exceeding 1024 Kb, uploaded file is $sizeTampil byte(s)");

                                //$adapter->setValidators(array($extensionValidator));

                                if ($adapter->isValid("uploadSource")) {
                                    $date = date("dmy");
                                    $time = date("his");

                                    $newFileName = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . $sourceFileName;
                                    $adapter->addFilter('Rename', $newFileName, "uploadSource");

                                    if ($adapter->receive("uploadSource")) {

                                        $filedata = $attahmentDestination . $newFileName;
                                        // var_dump($filedata);
                                        if (file_exists($filedata)) {
                                            exec("clamdscan --fdpass " . $filedata, $out, $int);

                                            // if()
                                            // var_dump($int);
                                            //var_dump($datas);
                                            //die();

                                            if ($int == 1) {

                                                unlink($filedata);
                                                //die('here');
                                                $this->view->error = true;
                                                $errors = array(array('FileType' => 'Error: An infected file. Please upload other file'));
                                                $this->view->report_msg = $errors;
                                            } else {

                                                $othersAttachment = [];
                                                foreach ($files as $file => $fileInfo) {
                                                    if ($file == "uploadSource") continue;

                                                    $sizeFile = $fileInfo['size'] + 0;
                                                    if ($sizeFile == 0) continue;

                                                    $fileName = $fileInfo["name"];
                                                    $adapter->setDestination($attahmentDestination);

                                                    $date = date("dmy");
                                                    $time = date("his");

                                                    $newFileName2 = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . uniqid() . "_" . $fileName;

                                                    $adapter->addFilter('Rename', $newFileName2, $file);
                                                    $adapter->receive($file);
                                                    array_push($othersAttachment, $newFileName2);
                                                }

                                                $datas["othersAttachment"] = $othersAttachment;

                                                $datas['fileName'] = $newFileName;
                                            }
                                        }
                                    }
                                } else {
                                    $errors = array($adapter->getMessages());
                                    //var_dump($errors);die;
                                    $this->view->error = true;
                                    $errors = $this->language->_('Extension file must be *.jpg /.jpeg / .pdf');
                                    $this->view->report_msg = $errors;
                                }
                            }

                            $data = array(
                                'BG_STATUS' => '15',
                                'BG_REFCODE' => $refCode,
                                'SP_OBLIGEE_CODE' => $obligeecode,
                                'BG_APPROVE_DOC' => $datas['fileName'],
                            );


                            $where['BG_REG_NUMBER = ?'] = $BG_NUMBER;
                            $this->_db->update('TEMP_BANK_GUARANTEE', $data, $where);

                            //bgverify detail
                            $verifydt = $this->_getParam('verifyindex');
                            if (!empty($verifydt)) {
                                foreach ($verifydt as $vl) {
                                    $verifyInsert = array(
                                        'VERIFIED'          => new Zend_Db_Expr("now()"),
                                        'BG_REG_NUMBER'     => $BG_NUMBER,
                                        'CUST_ID'           => $this->_custIdLogin,
                                        'VERIFIEDBY'        => $this->_userIdLogin,
                                        'INDEX'             => $vl
                                    );

                                    $this->_db->insert('TEMP_BGVERIFY_DETAIL', $verifyInsert);
                                }
                            }

                            //Others Attachment Documents
                            $footnotes = $this->_getParam('Footnotes');
                            $updatenotes = array(
                                'FILE_NOTE' => $footnotes,
                            );

                            $wherefile['BG_REG_NUMBER = ?'] = $BG_NUMBER;
                            $this->_db->update('TEMP_BANK_GUARANTEE_FILE', $updatenotes, $wherefile);


                            // $notes = $this->_getParam('PS_REASON_REJECT');
                            $historyInsert = array(
                                'DATE_TIME'         => new Zend_Db_Expr("now()"),
                                'BG_REG_NUMBER'         => $BG_NUMBER,
                                'CUST_ID'           => $this->_custIdLogin,
                                'USER_LOGIN'        => $this->_userIdLogin,
                                'HISTORY_STATUS'    => 15
                                //  'BG_REASON'         => $notes,
                            );

                            $this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);
                            $this->_db->commit();
                            $this->setbackURL('/' . $this->_request->getModuleName() . '/formatapproval/');
                            $this->_redirect('/notification/success/index');
                        } else {
                            $this->view->error = true;
                            $errorMsg[] = $this->language->_('Please checked all verify');  //$verToken['ResponseDesc'];
                            $this->view->report_msg           = $errorMsg[0];
                        }

                        /* } else {
                                                                                                    $tokenFailed = $CustUser->setLogToken(); //log token activity
                                                                                                    
                                                                                                    $error = true;
                                                                                                    $errorMsg[] = $this->language->_('Invalid Response Code');  //$verToken['ResponseDesc'];
                                                                                                    $this->view->popauth = true;
                                                                                                    if ($tokenFailed === true) {
                                                                                                        $this->_redirect('/default/index/logout');
                                                                                                    }
                                                                                                } */

                        //$resultToken = $resHard['ResponseCode'] == '0000';
                        //}

                        if ($error) {
                            $this->view->popauth                = true;
                            //var_dump($errorMsg);
                            $this->view->errorMsg           = $errorMsg[0];
                        }
                    }

                    if ($savework) {
                        $datas = $this->_request->getParams();
                        if (!empty($datas['verifyindex'])) {
                            foreach ($datas['verifyindex'] as $vl) {
                                $verifyInsert = array(
                                    'VERIFIED'          => new Zend_Db_Expr("now()"),
                                    'BG_REG_NUMBER'     => $BG_NUMBER,
                                    'CUST_ID'           => $this->_custIdLogin,
                                    'VERIFIEDBY'        => $this->_userIdLogin,
                                    'INDEX'             => $vl
                                );

                                $this->_db->insert('TEMP_BGVERIFY_DETAIL', $verifyInsert);
                            }
                        }

                        $data = array(
                            'SP_OBGLEE_CODE' => $datas['COMPANY_ID']
                        );

                        $where['BG_REG_NUMBER = ?'] = $BG_NUMBER;
                        $this->_db->update('TEMP_BANK_GUARANTEE', $data, $where);

                        //Others Attachment Documents
                        $footnotes = $this->_getParam('Footnotes');
                        $updatenotes = array(
                            'FILE_NOTE' => $footnotes,
                        );

                        $wherefile['BG_REG_NUMBER = ?'] = $BG_NUMBER;
                        $this->_db->update('TEMP_BANK_GUARANTEE_FILE', $updatenotes, $wherefile);

                        $this->setbackURL('/' . $this->_request->getModuleName() . '/formatapproval/');
                        $this->_redirect('/notification/success/index');
                        //echo '<pre>';
                        //var_dump($datas);die;
                    }

                    if ($reject) {
                        $data = array('BG_STATUS' => '11');
                        $where['BG_REG_NUMBER = ?'] = $BG_NUMBER;
                        $this->_db->update('TEMP_BANK_GUARANTEE', $data, $where);



                        $notes = $this->_getParam('PS_REASON_REJECT');
                        $historyInsert = array(
                            'DATE_TIME'         => new Zend_Db_Expr("now()"),
                            'BG_REG_NUMBER'         => $BG_NUMBER,
                            'CUST_ID'           => $this->_custIdLogin,
                            'USER_LOGIN'        => $this->_userIdLogin,
                            'HISTORY_STATUS'    => 11,
                            'BG_REASON'         => $notes,
                        );

                        $this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);

                        $this->setbackURL('/' . $this->_request->getModuleName() . '/formatapproval/');
                        $this->_redirect('/notification/success/index');
                    }


                    if ($repair) {
                        //die('here');
                        $data = array('BG_STATUS' => '10');
                        $where['BG_REG_NUMBER = ?'] = $BG_NUMBER;
                        $this->_db->update('TEMP_BANK_GUARANTEE', $data, $where);

                        $notes = $this->_getParam('PS_REASON_REPAIR');
                        $historyInsert = array(
                            'DATE_TIME'         => new Zend_Db_Expr("now()"),
                            'BG_REG_NUMBER'         => $BG_NUMBER,
                            'CUST_ID'           => $this->_custIdLogin,
                            'USER_LOGIN'        => $this->_userIdLogin,
                            'HISTORY_STATUS'    => 10,
                            'BG_REASON'         => $notes,
                        );

                        $this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);

                        $this->setbackURL('/' . $this->_request->getModuleName() . '/formatapproval/');
                        $this->_redirect('/notification/success/index');
                    }
                }
            }
        }
    }


    public function findPolicyBoundary($transfertype, $amount)
    {


        $selectuser    = $this->_db->select()
            ->from(array('C' => 'M_APP_BOUNDARY'), array(
                'BOUNDARY_MIN'     => 'C.BOUNDARY_MIN',
                'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
                'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
                'C.TRANSFER_TYPE',
                'C.POLICY'
            ))
            ->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
            ->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
            ->where("C.BOUNDARY_MIN 	<= ?", $amount)
            ->where("C.BOUNDARY_MAX 	>= ?", $amount);


        //echo $selectuser;die;
        $datauser = $this->_db->fetchAll($selectuser);

        return $datauser[0]['POLICY'];
    }

    //return tombol jika blm ada yg approve
    public function findUserBoundary($transfertype, $amount)
    {

        $selectuser    = $this->_db->select()
            ->from(array('C' => 'M_APP_BOUNDARY'), array(
                'BOUNDARY_MIN'     => 'C.BOUNDARY_MIN',
                'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
                'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
                'C.TRANSFER_TYPE',
                'C.POLICY'
            ))
            ->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
            ->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
            ->where("C.BOUNDARY_MIN 	<= ?", $amount)
            ->where("C.BOUNDARY_MAX 	>= ?", $amount);


        //echo $selectuser;die();
        $datauser = $this->_db->fetchAll($selectuser);

        $command = str_replace('(', '', $datauser[0]['POLICY']);
        $command = str_replace(')', '', $command);
        $command = $command . ' SG';
        $list = explode(' ', $command);

        $alphabet = array('01' => 'A', '02' => 'B', '03' => 'C', '04' => 'D', '05' => 'E', '06' => 'F', '07' => 'G', '08' => 'H', '09' => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z', 27 => 'SG');

        $flipAlphabet = array_flip($alphabet);

        foreach ($list as $row => $data) {
            foreach ($alphabet as $key => $value) {
                if ($data == $value) {
                    $groupuser[] = $flipAlphabet[$data];
                }
            }
        }

        $uniqueGroupUser = array_unique($groupuser);

        foreach ($uniqueGroupUser as $key => $value) {
            if ($value == '27') {
                $selectGroupName    = $this->_db->select()
                    ->from(array('C' => 'M_APP_GROUP_USER'), array('C.GROUP_NAME'))
                    ->where("C.GROUP_USER_ID LIKE ?", '%S_' . $this->_custIdLogin . '%');
            } else {
                $selectGroupName    = $this->_db->select()
                    ->from(array('C' => 'M_APP_GROUP_USER'), array('C.GROUP_NAME'))
                    ->where("C.GROUP_USER_ID LIKE ?", '%' . $this->_custIdLogin . '_' . $value . '%');
            }

            $groupNameList = $this->_db->fetchAll($selectGroupName);

            array_unique($groupNameList[0]);

            $uniqueGroupName[$value] = $groupNameList[0]['GROUP_NAME'];
        }

        foreach ($uniqueGroupName as $row => $data) {
            foreach ($alphabet as $key => $value) {
                if ($row == $key) {
                    $newUniqueGroupName[$value] = $data;
                }
            }
        }

        foreach ($groupuser as $key => $value) {

            //if special group
            if ($value == 27) {
                $likecondition = "S_%";
            } else {
                $likecondition = "%" . $this->_custIdLogin . "_" . $value . "%";
            }

            $selectgroup = $this->_db->select()
                ->from(array('C' => 'M_APP_GROUP_USER'), array(
                    'USER_ID'
                ))
                ->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
                ->where("C.GROUP_USER_ID LIKE ?", (string) $likecondition);

            $group_user = $this->_db->fetchAll($selectgroup);

            $groups[][$alphabet[$value]] = $group_user;
        }
        //	echo '<pre>';
        //var_dump($groups);
        $tempGroup = array();
        foreach ($groups as $key => $value) {

            foreach ($value as $data => $values) {

                foreach ($values as $row => $val) {
                    $userid = $val['USER_ID'];

                    $selectusername = $this->_db->select()
                        ->from(array('M_USER'), array(
                            '*'
                        ))
                        ->where("CUST_ID = ?", (string) $this->_custIdLogin)
                        ->where("USER_ID = ?", (string) $userid);
                    //echo $selectusername;echo ' ';
                    $username = $this->_db->fetchAll($selectusername);

                    if (!in_array($data, $tempGroup)) {
                        $userlist[$data][] = $username[0]['USER_FULLNAME'];
                    }
                }

                array_push($tempGroup, $data);

                // $approverbtn[][$data] = '<button class="btnCircleGroup hovertext" style="margin-right: 1%; margin-top: 12%;" disabled>'.$data.'
                // 	<span class="hovertextcontent" style="padding-left: 15px;">'.$userlist.'</span></button>';
            }
        }

        $userlist['GROUP_NAME'] = $newUniqueGroupName;

        return $userlist;
    }
}
