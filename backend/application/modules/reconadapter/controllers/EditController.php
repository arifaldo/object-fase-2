<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';

require_once 'General/Customer.php';
require_once 'General/CustomerUser.php';

require_once 'SGO/Extendedmodule/PHPExcel.php';
require_once 'SGO/Extendedmodule/PHPExcel/IOFactory.php';

class reconadapter_EditController extends Application_Main {

	public function indexAction()
    { 
    	$this->_helper->_layout->setLayout('newlayout');

        $profileid = $this->_request->getParam('profileid');

        $selectData = $this->_db->select()
                     ->from(array('H' => 'M_RECON_ADAPTER'), '*')
                     ->join(array('D' => 'M_RECON_ADAPTER_DETAIL'), 'H.ADAPTER_ID = D.ADAPTER_ID', '*')
                     ->where('H.ADAPTER_ID = '.$this->_db->quote($profileid));

        $dataAdapter = $this->_db->fetchAll($selectData);

        // $select = $this->_db->select()
        //         ->from(array('B' => 'M_BANK_TABLE'),array('*'));

        // $bankData = $this->_db->fetchAll($select);

        // $this->view->bankData = $bankData;
 
        $statementFileName = $dataAdapter[0]['STATEMENT_FILE_PATH'];
        $reportFileName = $dataAdapter[0]['REPORT_FILE_PATH'];
       
        $statementData = $this->_helper->parser->parseCSV($statementFileName);
        $reportData = $this->_helper->parser->parseCSV($reportFileName);

        $statementStartRow = $dataAdapter[0]['STATEMENT_START_FROM_ROW'];
        $reportStartRow = $dataAdapter[0]['REPORT_START_FROM_ROW'];

        $statementHeaderData = $statementData[$statementStartRow-1];
        $reportHeaderData = $reportData[$reportStartRow-1];

        if(!empty($statementData) && !empty($reportData))
        {    

             //--------------unset data from above start row---------------------
            if (!empty($statementStartRow)) {
                for($i=0; $i<$statementStartRow;$i++){
                    unset($statementData[$i]);
                }
            }

            if (!empty($reportStartRow)) {
                for($i=0; $i<$reportStartRow;$i++){
                    unset($reportData[$i]);
                }
            }

            //---------get rid of empty spaces between column on statement data---------
            $statementEmptySpaces = array();
            foreach ($statementHeaderData as $key => $value) {
                if (empty($value)) {
                    array_push($statementEmptySpaces, $key);
                    unset($statementHeaderData[$key]);
                }
            }
            foreach ($statementData as $key => $value) {
                foreach ($statementEmptySpaces as $spaces) {
                    unset($statementData[$key][$spaces]);
                }
            }

            //---------get rid of empty spaces between column on report data---------
            $reportEmptySpaces = array();
            foreach ($reportHeaderData as $key => $value) {
                if (empty($value)) {
                    array_push($reportEmptySpaces, $key);
                    unset($reportHeaderData[$key]);
                }
            }
            foreach ($reportData as $key => $value) {
                foreach ($reportEmptySpaces as $spaces) {
                    unset($reportData[$key][$spaces]);
                }
            }
            
            $this->view->statementHeaderData = $statementHeaderData;
            $this->view->reportHeaderData = $reportHeaderData;

            $this->view->statementData = $statementData;
            $this->view->reportData = $reportData;

            $this->view->dataAdapter = $dataAdapter;
        }
        else //kalo total record = 0
        {
            $error_msg[0] = $this->language->_('Error').': '.$this->language->_('Wrong File Formatss').'.';
            $this->view->error      = true;
            $this->view->report_msg = $this->displayError($error_msg);
        }

        if($this->_request->isPost())
        {
            $params = $this->_request->getParams();

            $profileid = $params['profileId'];

            //start m_adapter_profile
            $filter = array(
               'profileName' => array('StripTags','StringTrim'),
               // 'bank' => array('StripTags','StringTrim')
            );
            
            $options = array('allowEmpty' => FALSE);
            
            $validators = array(
                'profileName'   => array(),
                // 'bank'      => array()
            );

            $filter  = new Zend_Filter_Input($filter, $validators, $params, $options);

            if ($filter->isValid()){

                $this->_db->beginTransaction();
                try{
                    $tableName = 'M_RECON_ADAPTER';
                    $arrData = array(
                        'ADAPTER_NAME' => $filter->getEscaped('profileName'),
                        // 'BANK_CODE' => $filter->getEscaped('bank'),
                        'SUGGESTED_DATE' => date('Y-m-d H:i:s'),
                        'SUGGESTED_BY' => $this->_userIdLogin
                    );

                    $whereArr  = array('ADAPTER_ID = ?'=> $profileid);
                    $this->_db->update($tableName,$arrData,$whereArr);
                    
                    $this->_db->commit();

                }catch(Exception $e){
                    $success = false;
                    $this->_db->rollBack();
                    print_r($e);die();
                    $error[] = $e->getMessage();
                }

                 //finish m_adapter profile 

                 //start m_adapter_profile_detail

                //delete the existing data first & insert the new one
                $this->_db->delete('M_RECON_ADAPTER_DETAIL','ADAPTER_ID = '.$this->_db->quote($profileid));

                $label = $params['label'];
                $statementContent = $params['statementContent'];
                $reportContent = $params['reportContent'];
                $operator = $params['operator'];

                $i = 0;
                foreach ($label as $key => $value) {

                     $data2 = array(
                        'ADAPTER_ID' => $profileid,
                        'HEADER_NAME' => $label[$key],
                        'STATEMENT_COLUMN' => $statementContent[$key],
                        'REPORT_COLUMN' => $reportContent[$key],
                        'OPERATOR' => $operator[$key]
                    );

                    $this->_db->beginTransaction();

                    try{
                        $tableName = 'M_RECON_ADAPTER_DETAIL';
                        $this->_db->insert($tableName, $data2);
                        
                        $this->_db->commit();

                        $success = true;

                    }catch(Exception $e){
                        $success = false;
                        $this->_db->rollBack();
                        print_r($e);die();
                        $error[] = $e->getMessage();
                    }
                    $i++;
                }

                if ($success) {
                    Application_Helper_General::writeLog('RPUD','Update Reconciliation Adapter List. Profile Name: '.$arrData['ADAPTER_NAME']);
                    $this->setbackURL('/'.$this->_request->getModuleName().'/index/');
                    $this->_redirect('/notification/success/index');
                }
                else{
                    $this->_db->rollBack();
                    $this->view->error = true;
                    $error_msg = $error;
                    $errors = $this->displayError($error_msg);
                    $this->view->report_msg = $errors;

                }   
            }else{
                $error = $filter->getMessages();

                $this->view->error = true;
                $error_msg = $error;
                $errors = $this->displayError($error_msg);
                $this->view->report_msg = $errors;
            }  
        }
    }  
}
