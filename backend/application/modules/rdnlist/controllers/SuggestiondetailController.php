<?php
require_once 'Zend/Controller/Action.php';

class rdnlist_SuggestiondetailController extends user_Model_User{

  public function indexAction()
  {
    $change_id = $this->_getParam('changes_id');
    $change_id = (Zend_Validate::is($change_id,'Digits'))? $change_id : 0;
    $this->_helper->layout()->setLayout('newpopup');

    $this->view->suggestionType = $this->_suggestType;

    if($change_id)
    {
  	  $select = $this->_db->select()
                             ->from('T_GLOBAL_CHANGES',array('CHANGES_ID'))
                             ->where('CHANGES_ID='.$this->_db->quote($change_id))
                             ->where("CHANGES_STATUS = 'WA' OR CHANGES_STATUS = 'RR'");

      $result = $this->_db->fetchOne($select);

      if(empty($result))  $this->_redirect('/notification/invalid/index');


      if($result)
      {
        //content send to view
        $select = $this->_db->select()
                               ->from(array('T'=>'TEMP_DIRECTDEBIT'))
      	                       ->join(array('G' => 'T_GLOBAL_CHANGES'), 'G.CHANGES_ID = T.CHANGES_ID', array('CHANGES_ID','CHANGES_TYPE','CREATED','CREATED_BY','CHANGES_STATUS','READ_STATUS'))
      	                       ->where('T.CHANGES_ID = ?', $change_id);
      	$resultdata = $this->_db->fetchRow($select);

      	if($result['TEMP_ID'])
      	{
      	    //----------------------------------------------TEMP DATA--------------------------------------------------------------
	  		$this->view->changes_id     = $resultdata['CHANGES_ID'];
	  		$this->view->changes_type   = $resultdata['CHANGES_TYPE'];
	  		$this->view->changes_status = $resultdata['CHANGES_STATUS'];
	  		$this->view->read_status    = $resultdata['READ_STATUS'];
	  		$this->view->created        = $resultdata['CREATED'];
	  		$this->view->created_by     = $resultdata['CREATED_BY'];


      	   $cust_id = strtoupper($resultdata['CUST_ID']);
      	   $user_id = strtoupper($resultdata['USER_ID']);

      	   $this->view->comp_id    = $resultdata['COMP_ID'];
      	   $this->view->comp_name  = $resultdata['COMP_NAME'];
       	   $this->view->comp_type = $resultdata['COMP_TYPE'];
           $this->view->debit_bank = $resultdata['DEBIT_BANK'];
           $this->view->debit_acct   = $resultdata['DEBIT_ACCT'];
           $this->view->acct_name   = $resultdata['ACCT_NAME'];


        	$this->view->dir_filename  = $resultdata['DIR_FILENAME'];

        	$this->view->user_suggested   = $resultdata['DIR_SUGESTED'];
        	$this->view->user_suggestedby = $resultdata['DIR_SUGESTEDBY'];

        //----------------------------------------------MASTER DATA--------------------------------------------------------------
          $m_resultdata = $this->_db->select()
          ->from(array('A' => 'T_DIRECTDEBIT'))
                    ->where('UPPER(COMP_ID)='.$this->_db->quote((string)$resultdata['COMP_ID']))
                    ->where('UPPER(DEBIT_ACCT)='.$this->_db->quote((string)$resultdata['DEBIT_ACCT']))
          ->query()->FetchAll();
           // $m_resultdata = $this->getDebitData($cust_id,$user_id);
      	   if(!empty($m_resultdata)){
      	   $this->view->m_comp_id    = $m_resultdata['COMP_ID'];
      	   $this->view->m_comp_name  = $m_resultdata['COMP_NAME'];
       	   $this->view->m_comp_type = $m_resultdata['COMP_TYPE'];
           $this->view->m_debit_bank = $m_resultdata['DEBIT_BANK'];
           $this->view->m_debit_acct   = $m_resultdata['DEBIT_ACCT'];
           $this->view->m_acct_name   = $m_resultdata['ACCT_NAME'];

        	$this->view->m_dir_filename         = $m_resultdata['DIR_FILENAME'];

        	$this->view->m_user_suggested   = $m_resultdata['USER_SUGESTED'];
        	$this->view->m_user_suggestedby = $m_resultdata['USER_SUGESTEDBY'];
          }




       	}
       	else{ $change_id = 0; }
      }
      else{ $change_id = 0; }
    }





    if(!$change_id)
    {
      $error_remark = $this->language->_('Changes Id is not found');
      $this->_redirect($this->_backURL);
      $this->_redirect('/popuperror/index/index');
    }

    //insert log
	try
	{
	  $this->_db->beginTransaction();
	  $fulldesc = 'CHANGES_ID:'.$change_id;
	  if(!$this->_request->isPost()){
	  Application_Helper_General::writeLog('CSCL','View Frontend User Changes List');
	  }

	  $this->_db->commit();
	}
    catch(Exception $e){
 	  $this->_db->rollBack();
	}

    $this->view->changes_id = $change_id;
  }
}
