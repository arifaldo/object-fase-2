<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';
class authorizationmatrix_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti
	
	public function initController()
	{     
		$this->_helper->layout()->setLayout('newlayout');
        //pengaturan url untuk button back
	    $this->setbackURL('/'.$this->_request->getModuleName().'/index/'); 
	
		$companyCode = $this->_db->fetchAll(
					$this->_db->select()
					   ->from(array('CS' => 'M_CUSTOMER'),array('CS.CUST_ID'))
					
					   );
		$listCompCode = array(''=>'--- '.$this->language->_('Any Value').' ---');
		$listCompCode += Application_Helper_Array::listArray($companyCode,'CUST_ID','CUST_ID');
		$this->view->listcompcode = $listCompCode;
	}

	public function indexAction()
	{
		Application_Helper_General::writeLog('BMLS','View Approver Boundary List');
	    $ccyArr = $this->_db->select()
						->from(array('M_MINAMT_CCY'), array('CCY_ID'))
						->order('CCY_ID ASC')
						->query()
						->fetchAll();
		$ccyArrCode = array(''=>'--- '.$this->language->_('Any Value').' ---');
		$ccyArrCode += Application_Helper_Array::listArray($ccyArr,'CCY_ID','CCY_ID');
		$this->view->ccyArr = $ccyArrCode;
		if(is_array($ccyArr)){
	        	$ccyArrValidate = Application_Helper_Array::simpleArray($ccyArr,'CCY_ID');
	    }
		
		$fields = array(
						'comp_code'  => array('field' => 'MAB.CUST_ID',
											   'label' => $this->language->_('Company Code'),
											   'sortable' => true),
						'comp_name'  => array('field' => 'CUST_NAME',
											   'label' => $this->language->_('Company Name'),
											   'sortable' => true),
						'boundary_value'  => array('field' => 'BOUNDARY_MIN',
											   'label' => $this->language->_('Boundary Value'),
											   'sortable' => true),
						
						'currency'  => array('field' => 'CCY_BOUNDARY',
											   'label' => $this->language->_('Currency'),
											   'sortable' => true),
						/*'group_name'  => array('field' => 'GROUP_NAME',
											   'label' => 'Group Name',
											   'sortable' => true)*/
				);

		$filterlist = array('COMP_ID','COMP_NAME','CCY');
    
    	$this->view->filterlist = $filterlist;
	    
		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','doc_no');
		$sortDir = $this->_getParam('sortdir','asc');
		
		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';
	  

		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	'filter' 	  		=> array('StringTrim','StripTags'),
							'COMP_ID' 			=> array('StringTrim','StripTags','StringToUpper'),
							'COMP_NAME'    		=> array('StringTrim','StripTags','StringToUpper'),
							'CCY'    		=> array('StringTrim','StripTags','StringToUpper'),
		);

		$validators = array('filter' 	  		=> array(),
							'COMP_ID' 			=> array(),
							'COMP_NAME'    		=> array(),
							'CCY'    		=> array(),
		);

		$dataParam = array('COMP_ID','COMP_NAME','CCY','USER_ID','STATUS');
	      $dataParamValue = array();
	        
	      $clean2 = array_diff( $this->_request->getParam('wherecol'),$dataParam); 
	      $dataParam = array_diff( $this->_request->getParam('wherecol'),$clean2); 
	    // print_r($dataParam);die;

	      // print_r($output);die;
	      // print_r($this->_request->getParam('wherecol'));
	      foreach ($dataParam as $no => $dtParam)
	      {
	      
	        if(!empty($this->_request->getParam('wherecol'))){
	          $dataval = $this->_request->getParam('whereval');
	          // print_r($dataval);
	          $order = 0;
	            foreach ($this->_request->getParam('wherecol') as $key => $value) {
	              if($value == "PS_LATEST_APPROVAL" || $value == "PS_LATEST_SUGGESTION"){
	                  $order--;
	                }
	              if($dtParam==$value){
	                $dataParamValue[$dtParam] = $dataval[$order];
	              }
	              $order++;
	            }
	          
	        }
	      }

		$zf_filter = new Zend_Filter_Input($filterArr,$validator,$dataParamValue);
    	$filter     = $this->_getParam('filter');

		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		//var_dump($sortBy,$sortDir);exit;
		
		$select = $this->_db->select()
					   ->from(array('MAB'=>'M_APP_BOUNDARY'),array('MAB.CUST_ID','CS.CUST_NAME','MAB.BOUNDARY_MIN','MAB.BOUNDARY_MAX','MAB.CCY_BOUNDARY'))
					   ->join(array('CS' => 'M_CUSTOMER'),'CS.CUST_ID = MAB.CUST_ID',array(''));
					   
	    $select->order($sortBy.' '.$sortDir);
		
		if($filter == $this->language->_('Set Filter'))
		{
			$fCompCode = $zf_filter->getEscaped('COMP_ID');
			$fCompName = $zf_filter->getEscaped('COMP_NAME');
			$fCurrency = $zf_filter->getEscaped('CCY');
			
	        if($fCompCode)$select->where('UPPER(MAB.CUST_ID) = '.$this->_db->quote(strtoupper($fCompCode)));
	        if($fCompName)$select->where('UPPER(CS.CUST_NAME) LIKE '.$this->_db->quote('%'.strtoupper($fCompName).'%'));
	        if($fCurrency)$select->where('UPPER(MAB.CCY_BOUNDARY) LIKE '.$this->_db->quote('%'.strtoupper($fCurrency).'%'));
						
			$this->view->custid = $fCompCode;
			$this->view->custname = $fCompName;
			$this->view->currencyx = $fCurrency;
		}
		
		$this->paging($select);
		$this->view->fields = $fields;
		$this->view->filter = $filter;

		if(!empty($dataParamValue)){
	      foreach ($dataParamValue as $key => $value) {
	        $wherecol[] = $key;
	        $whereval[] = $value;
	      }
	        $this->view->wherecol     = $wherecol;
	        $this->view->whereval     = $whereval;
	     // print_r($whereval);die;
      	}

	}

	public function newappboundaryAction()
	{
		Application_Helper_General::writeLog('BMLS','View Approver Boundary List');
	    $ccyArr = $this->_db->select()
						->from(array('M_MINAMT_CCY'), array('CCY_ID'))
						->order('CCY_ID ASC')
						->query()
						->fetchAll();
		$ccyArrCode = array(''=>'--- '.$this->language->_('Any Value').' ---');
		$ccyArrCode += Application_Helper_Array::listArray($ccyArr,'CCY_ID','CCY_ID');
		$this->view->ccyArr = $ccyArrCode;
		if(is_array($ccyArr)){
	        	$ccyArrValidate = Application_Helper_Array::simpleArray($ccyArr,'CCY_ID');
	    }
		
		$fields = array(
						'comp_code'  => array('field' => 'MAB.CUST_ID',
											   'label' => $this->language->_('Company Code'),
											   'sortable' => true),
						'comp_name'  => array('field' => 'CUST_NAME',
											   'label' => $this->language->_('Company Name'),
											   'sortable' => true),
						'boundary_value'  => array('field' => 'BOUNDARY_MIN',
											   'label' => $this->language->_('Boundary Value'),
											   'sortable' => true),
						
						'currency'  => array('field' => 'CCY_BOUNDARY',
											   'label' => $this->language->_('Currency'),
											   'sortable' => true),
						/*'group_name'  => array('field' => 'GROUP_NAME',
											   'label' => 'Group Name',
											   'sortable' => true)*/
				);

		$filterlist = array('COMP_ID','COMP_NAME','CCY');
    
    	$this->view->filterlist = $filterlist;
	    
		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','doc_no');
		$sortDir = $this->_getParam('sortdir','asc');
		
		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';
	  

		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	'filter' 	  		=> array('StringTrim','StripTags'),
							'COMP_ID' 			=> array('StringTrim','StripTags','StringToUpper'),
							'COMP_NAME'    		=> array('StringTrim','StripTags','StringToUpper'),
							'CCY'    		=> array('StringTrim','StripTags','StringToUpper'),
		);

		$validators = array('filter' 	  		=> array(),
							'COMP_ID' 			=> array(),
							'COMP_NAME'    		=> array(),
							'CCY'    		=> array(),
		);

		$dataParam = array('COMP_ID','COMP_NAME','CCY','USER_ID','STATUS');
	      $dataParamValue = array();
	        
	      $clean2 = array_diff( $this->_request->getParam('wherecol'),$dataParam); 
	      $dataParam = array_diff( $this->_request->getParam('wherecol'),$clean2); 
	    // print_r($dataParam);die;

	      // print_r($output);die;
	      // print_r($this->_request->getParam('wherecol'));
	      foreach ($dataParam as $no => $dtParam)
	      {
	      
	        if(!empty($this->_request->getParam('wherecol'))){
	          $dataval = $this->_request->getParam('whereval');
	          // print_r($dataval);
	          $order = 0;
	            foreach ($this->_request->getParam('wherecol') as $key => $value) {
	              if($value == "PS_LATEST_APPROVAL" || $value == "PS_LATEST_SUGGESTION"){
	                  $order--;
	                }
	              if($dtParam==$value){
	                $dataParamValue[$dtParam] = $dataval[$order];
	              }
	              $order++;
	            }
	          
	        }
	      }

		$zf_filter = new Zend_Filter_Input($filterArr,$validator,$dataParamValue);
    	$filter     = $this->_getParam('filter');

		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		//var_dump($sortBy,$sortDir);exit;
		
		$select = $this->_db->select()
					   ->from(array('MAB'=>'M_APP_BOUNDARY'),array('MAB.CUST_ID','CS.CUST_NAME','MAB.BOUNDARY_MIN','MAB.BOUNDARY_MAX','MAB.CCY_BOUNDARY'))
					   ->join(array('CS' => 'M_CUSTOMER'),'CS.CUST_ID = MAB.CUST_ID',array(''));
					   
	    $select->order($sortBy.' '.$sortDir);
		
		if($filter == $this->language->_('Set Filter'))
		{
			$fCompCode = $zf_filter->getEscaped('COMP_ID');
			$fCompName = $zf_filter->getEscaped('COMP_NAME');
			$fCurrency = $zf_filter->getEscaped('CCY');
			
	        if($fCompCode)$select->where('UPPER(MAB.CUST_ID) = '.$this->_db->quote(strtoupper($fCompCode)));
	        if($fCompName)$select->where('UPPER(CS.CUST_NAME) LIKE '.$this->_db->quote('%'.strtoupper($fCompName).'%'));
	        if($fCurrency)$select->where('UPPER(MAB.CCY_BOUNDARY) LIKE '.$this->_db->quote('%'.strtoupper($fCurrency).'%'));
						
			$this->view->custid = $fCompCode;
			$this->view->custname = $fCompName;
			$this->view->currencyx = $fCurrency;
		}
		
		$this->paging($select);
		$this->view->fields = $fields;
		$this->view->filter = $filter;

		if(!empty($dataParamValue)){
	      foreach ($dataParamValue as $key => $value) {
	        $wherecol[] = $key;
	        $whereval[] = $value;
	      }
	        $this->view->wherecol     = $wherecol;
	        $this->view->whereval     = $whereval;
	     // print_r($whereval);die;
      	}

	}
}
