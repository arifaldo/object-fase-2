<?php

/**
 * Globalchanges model for BACKEND
 * 
 * @author 
 * @version 
 */

require_once 'Zend/Db/Table/Abstract.php';

class Changemanagement_Model_Globalchanges extends Zend_Db_Table_Abstract
{
	/**
	 * The default table name
	 */
	protected $_name = 'T_GLOBAL_CHANGES';
	protected $_primary = 'CHANGES_ID';
	protected $dbObj;
	protected $_id;
	protected $_row;
	protected $_errorMsg;
	protected $_errorCode;
	protected $_moduleId;
	protected $_suggestData;

	//model mapping,
	//array key = obtained from field MODULE, TABLE T_GLOBAL_CHANGES								
	//array value = model name in this module
	//mapping for backend
	protected $_detailModelMap = array(
		'manageproductaccount' => 'Changemanagement_Model_Tempmanageproductaccount',
		'anchor' => 'Changemanagement_Model_Tempanchor',
		'community' => 'Changemanagement_Model_Tempcommunity',
		'customer' => 'Changemanagement_Model_Tempcustomer',
		'bglist' => 'Changemanagement_Model_Tempbglist',
		'user' => 'Changemanagement_Model_Tempuser',
		'member' => 'Changemanagement_Model_Tempmember',
		'rootcommunity' => 'Changemanagement_Model_Temprootcommunity',
		'group' => 'Changemanagement_Model_Tempgroup',
		'welcome' => 'Changemanagement_Model_Tempwelcome',
		'cutofftime' => 'Changemanagement_Model_Tempcutofftime',
		'minamountccy' => 'Changemanagement_Model_Tempminimumamount',
		'frontendlogin' => 'Changemanagement_Model_Tempfrontendlogin',
		'frontendsetting' => 'Changemanagement_Model_Tempfrontendsetting',
		'cleansing' => 'Changemanagement_Model_Tempcleansing',
		'userBackend' => 'Changemanagement_Model_Tempuserbackend',
		'authorizationmatrix' => 'Changemanagement_Model_Tempauthorizationmatrix',
		'boundary' => 'Changemanagement_Model_Tempauthorizationmatrix',
		'systembalance' => 'Changemanagement_Model_Tempsetsystembalance',
		'generalsetting' => 'Changemanagement_Model_Tempgeneralsetting',
		'transactiondailylimit' => 'Changemanagement_Model_Temptransactiondailylimit',
		'holiday' => 'Changemanagement_Model_Tempholiday',
		'holidaysetting' => 'Changemanagement_Model_Tempholidaysetting',
		'customeraccount' => 'Changemanagement_Model_Tempcustomeraccount',
		'userdailylimit' => 'Changemanagement_Model_Tempuserdailylimit',
		'userlimit' => 'Changemanagement_Model_Tempuserlimit',
		'useropenlimit' => 'Changemanagement_Model_Tempuseropenlimit',
		'coa' => 'Changemanagement_Model_Tempcoa',
		'appgroup' => 'Changemanagement_Model_Tempappgroup',
		'backendgroup' => 'Changemanagement_Model_Tempbackendgroup',
		'assignuserscommunity' => 'Changemanagement_Model_Tempuserstocommunity',
		'physicaldocumentconfig' => 'Changemanagement_Model_Tempphysicaldocumentconfig',
		'supplierfinancing' => 'Changemanagement_Model_Tempsupplierfinancing',
		'distributorfinancing' => 'Changemanagement_Model_Tempdistributorfinancing',
		'automotivedealerfinancing' => 'Changemanagement_Model_Tempautomotivedealerfinancing',
		'globalscheme' => 'Changemanagement_Model_Tempglobalscheme',
		'realtimecharges' => 'Changemanagement_Model_Temprealtimecharges',
		'monthlyaccount' => 'Changemanagement_Model_Tempmonthlyaccount',
		'monthlycompany' => 'Changemanagement_Model_Tempmonthlycompany',
		'adminfeeaccount' => 'Changemanagement_Model_Tempadminfeeaccount',
		'adminfeecompany' => 'Changemanagement_Model_Tempadminfeecompany',
		'disbursementcharges' => 'Changemanagement_Model_Tempdisbursementcharge',
		'settlementcharges' => 'Changemanagement_Model_Tempsettlementcharge',
		'chargesdomestic' => 'Changemanagement_Model_Tempchargesdomestic',
		'trafficapicharges' => 'Changemanagement_Model_Temptrafficapicharges',
		'principal' => 'Changemanagement_Model_Tempprincipal',
		'addbackenduser' => 'Changemanagement_Model_Tempaddbackenduser',
		'chargestemplatelist' => 'Changemanagement_Model_Tempchargestemplatelist',
		'scheme' => 'Changemanagement_Model_Tempschemeconfiguration',
		'backenduser' => 'Changemanagement_Model_Tempbackenduser',
		'escrow' => 'Changemanagement_Model_Tempescrowaccount',
		'setbillercharges' => 'Changemanagement_Model_Tempbillercharges',
		'biller' => 'Changemanagement_Model_Tempbiller',
		'billeraccount' => 'Changemanagement_Model_Tempbilleraccount',
		'setbilleronboardcharges' => 'Changemanagement_Model_Tempbillercharges',
		'billeronboard' => 'Changemanagement_Model_Tempbiller',
		'remittancechar' => 'Changemanagement_Model_Tempremittancechar',
		'specialcharges' => 'Changemanagement_Model_Tempspecialcharges',
		'localremittancechar' => 'Changemanagement_Model_Templocalremittancechar',
		'billeronboardaccount' => 'Changemanagement_Model_Tempbilleraccount',
		'agent' => 'Changemanagement_Model_Tempagent',
		'binsetup' => 'Changemanagement_Model_Tempbinsetup',
		'bintemplate' => 'Changemanagement_Model_Tempbintemplate',
		'boundary' => 'Changemanagement_Model_Tempboundary',
		'accountaggregation' => 'Changemanagement_Model_Tempaccountaggregation',
		// 'boun' => 'Changemanagement_Model_Tempbinsetup'
		'companycharges'	=> 'Changemanagement_Model_Tempcompanycharges',
		'nationalpoolinggroup'	=> 'Changemanagement_Model_Tempnotionalpooling',
		'businessadapter' => 'Changemanagement_Model_Tempbusinessadapter',
		'corporatedcard' => 'Changemanagement_Model_Tempcorporatedcard',
		'corporatedebitcard' => 'Changemanagement_Model_Tempcorporatedebitcard',
		'activationcard' => 'Changemanagement_Model_Tempactivationcard',
		'bgreport' => 'Changemanagement_Model_Tempbgreport',
		'bglimit' => 'Changemanagement_Model_Tempbglist'


	);

	protected $_detailModelIdMap = array(
		'manageproductaccount' => '',
		'anchor' => 'ANC',
		'community' => 'SCM',
		'customer' => 'CST',
		'user' => 'CUS',
		'scheme' => 'SCH',
		'member' => 'SMB',
		'rootcommunity' => 'RCM',
		'group' => 'BGR',
		'welcome' => 'WLT',
		'cutofftime' => 'COT',
		'minamountccy' => 'MAC',
		'frontendlogin' => 'FLS',
		'frontendsetting' => 'FES',
		'cleansing' => 'CLS',
		'userBackend' => 'USB',
		'authorizationmatrix' => 'AMF',
		'boundary' => 'AMF',
		'systembalance' => 'SBL',
		'generalsetting' => 'GNS',
		'transactiondailylimit' => 'TRX',
		'holiday' => 'HDY',
		'holidaysetting' => 'HDY',
		'customeraccount' => '',
		'userdailylimit' => '',
		'userlimit' => '',
		'useropenlimit' => '',
		'coa' => 'COA',
		'appgroup' => '',
		'backendgroup' => '',
		'assignuserscommunity' => '',
		'physicaldocumentconfig' => 'PDC',
		'supplierfinancing' => '',
		'distributorfinancing' => '',
		'automotivedealerfinancing' => '',
		'realtimecharges' => '',
		'monthlyaccount' => '',
		'monthlycompany' => '',
		'adminfeeaccount' => '',
		'adminfeecompany' => '',
		'disbursementcharges' => '',
		'settlementcharges' => '',
		'chargesdomestic' => '',
		'specialcharges' => '',
		'trafficapicharges' => '',
		'principal' => '',
		'chargestemplatelist' => 'CTL',
		'schemeconfiguration' => '',
		'backenduser' => '',
		'globalscheme' => 'GSP',
		'escrow' => 'ECA',
		'setbillercharges' => '',
		'biller' => '',

		'setbilleronboardcharges' => '',
		'billeronboard' => '',
		'remittancechar' => '',
		'localremittancechar' => '',
		'billeronboardaccount' => '',
		'billeraccount' => '',
		'backendgroup' => '',
		'agent' => '',
		'binsetup' => '',
		'bintemplate' => '',
		'boundary' => '',
		'accountaggregation' => '',
		'companycharges'	=> '',
		'nationalpoolinggroup'	=> '',
		'businessadapter'	=> '',
		'corporatedcard' => '',
		'corporatedebitcard' => '',
		'activationcard' => '',
		'bgreport' => '',
		'bglimit' => ''
	);
	/**
	 * Constructor
	 * @param  String $id Change Id
	 */
	public function __construct($id)
	{

		parent::__construct();
		$this->dbObj = $this->getDefaultAdapter();
		$this->setId($id);


		$this->dbObj->getProfiler()->setEnabled(true);
	}

	/**
	 * Destructor
	 */
	public function __destruct()
	{
		//		Zend_Debug::dump($this->dbObj->getProfiler()->getQueryProfiles());
		$this->dbObj->getProfiler()->clear();
	}

	/**
	 * set Id for this object
	 * @param  String $id Change Id
	 * @return boolean 
	 */
	public function setId($id)
	{

		$privi = array();

		if (Zend_Auth::getInstance()->hasIdentity()) {
			$auth  = Zend_Auth::getInstance()->getIdentity();
			$privi = $auth->priviIdLogin;
		}

		if (!empty($privi))		// kalo privilege is empty, gak bisa generate report, for all data
		{
			$row = $this->dbObj
				->select()
				->from($this->_name)
				->where($this->dbObj->quoteInto($this->_primary . ' = ? ', $id))
				// ->where($this->dbObj->quoteInto("CHANGES_FLAG = ?",'B'))
				->query()
				->fetch(Zend_Db::FETCH_ASSOC);
		}

		if ($row) {
			$this->_id = $id;
			$this->_row = $row;
			$this->_moduleId = $this->_detailModelIdMap[$row['MODULE']];
			$this->_suggestData = $row['DISPLAY_TABLENAME'];
			return true;
		} else {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Record with given id not found';
			throw new Exception($this->_errorMsg);
		}
	}

	/**
	 * Reject changes
	 * Update status in changes table to "request repair"
	 *
	 * @param  mixed $actor User name/id who request changes repair,for use in logging into database
	 * @param  String $note OPTIONAL Approval note to set in database
	 * @return boolean indicating operation success/failure
	 */
	public function reject($actor, $note = null)
	{
		$changeModulePrivilegeObj  = new Changemanagement_Model_Privilege();
		$isAuthorizeReject = $changeModulePrivilegeObj->isAuthorizeReject($this->_row['MODULE']);
		if (!$isAuthorizeReject) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'You have no privilege for reject specified changes';
			return false;
		}
		$privilgeDualControl = $changeModulePrivilegeObj->getPrivilege($this->_row['MODULE'], 'Reject');
		//Zend_Debug::dump($privilgeDualControl);die;
		if ($this->_row['DISPLAY_TABLENAME'] == "Holiday Setting") {
			$fullDesc = "Reject Changes Id : {$this->_row['CHANGES_ID']}. Suggest Data : {$this->_row['DISPLAY_TABLENAME']}. Data Id : {$this->_row['KEY_FIELD']}, Reason: {$note}";
		} else if ($this->_row['DISPLAY_TABLENAME'] == "Backend Group") {
			// $fullDesc = "Tolak Usulan ID : {$this->_row['CHANGES_ID']}. Suggest Data : {$this->_row['DISPLAY_TABLENAME']}, Company : {$this->_row['COMPANY_CODE']}, Data ID : {$this->_row['KEY_FIELD']}, Reason : {$note}";
			$fullDesc = "Tolak Usulan ID : {$this->_row['CHANGES_ID']}. Suggest Data : {$this->_row['DISPLAY_TABLENAME']}, Data ID : {$this->_row['KEY_FIELD']}, Reason : {$note}";
		} else {

			$fullDesc = "Reject Changes Id : {$this->_row['CHANGES_ID']} Suggest Data : {$this->_row['DISPLAY_TABLENAME']} Data Id : {$this->_row['KEY_FIELD']}  Data Name : {$this->_row['KEY_VALUE']} ";
		}
		if ($privilgeDualControl == 'CHCR') {
			Application_Helper_General::writeLog('CHCA', $fullDesc);
		} else {
			Application_Helper_General::writeLog($privilgeDualControl, $fullDesc);
		}
		//Application_Helper_General::writeLog($privilgeDualControl,$fullDesc);

		// $this->dbObj->beginTransaction();
		try {
			if (in_array($this->_row['MODULE'], array_keys($this->_detailModelMap))) {
				//create detail model
				$detailModel = new $this->_detailModelMap[$this->_row['MODULE']]($this->_id, $this->_row);
				switch ($this->_row['CHANGES_TYPE']) {
					case 'N':
						$updated = $detailModel->deleteNew();
						$this->_errorCode = $detailModel->getErrorCode();
						$this->_errorMsg = $detailModel->getErrorMessage();
						break;
					case 'E':
						$updated = $detailModel->deleteEdit();
						$this->_errorCode = $detailModel->getErrorCode();
						$this->_errorMsg = $detailModel->getErrorMessage();
						break;
					case 'A':
						$updated = $detailModel->deleteActivate();
						$this->_errorCode = $detailModel->getErrorCode();
						$this->_errorMsg = $detailModel->getErrorMessage();
						break;
					case 'D':
						$updated = $detailModel->deleteDeactivate();
						$this->_errorCode = $detailModel->getErrorCode();
						$this->_errorMsg = $detailModel->getErrorMessage();
						break;
					case 'L':
						$updated = $detailModel->deleteDelete();
						$this->_errorCode = $detailModel->getErrorCode();
						$this->_errorMsg = $detailModel->getErrorMessage();
						break;
					case 'U':
						$updated = $detailModel->deleteUnsuspend();
						$this->_errorCode = $detailModel->getErrorCode();
						$this->_errorMsg = $detailModel->getErrorMessage();
						break;
					case 'S':
						$updated = $detailModel->deleteSuspend();
						$this->_errorCode = $detailModel->getErrorCode();
						$this->_errorMsg = $detailModel->getErrorMessage();
						break;
					default:
						$updated = false;
						$this->_errorCode = '22';
						$this->_errorMsg = 'Unknown changes type';
						throw new Exception($this->_errorMsg);
						break;
				}

				if ($updated) {
					$rowUpdated = $this->dbObj->update(
						$this->_name,
						array(
							'CHANGES_STATUS' => 'RJ',
							'CHANGES_REASON' => $note,
							//'LASTUPDATEDBY' => $actor,
							//NOTE: this field expression is Ms Sql Specific
							'LASTUPDATED' => new Zend_Db_Expr("now()"),
						),
						$this->dbObj->quoteInto('CHANGES_ID = ?', $this->_id)
					);
					if (!(bool) $rowUpdated) {
						$this->_errorCode = '82';
						$this->_errorMsg = $this->getErrorRemark('82');
						throw new Exception($this->_errorMsg);
					}
				} else
					throw new Exception($this->_errorMsg);
			} else {
				$this->_errorCode = '22';
				$this->_errorMsg = 'Unknown Module';
				throw new Exception($this->_errorMsg);
			}
		} catch (Exception $e) {
			$this->dbObj->rollBack();
			//			SGO_Helper_GeneralLog::technicalLog($e);
			//$this->_errorCode = '82';
			//$this->_errorMsg = 'Query Failed (Global Changes)';

			//rethrow exception (use this for debugging)
			throw $e;
			return false;
		}

		// $this->dbObj->commit();
		return true;
	}

	/**
	 * Delete changes
	 * @return boolean indicating operation success/failure
	 */
	public function delete($where = null)
	{

		$this->dbObj->beginTransaction();
		try {
			//check whether $this->_row['MODULE'] value exists in $this->_detailModelMap
			if (in_array($this->_row['MODULE'], array_keys($this->_detailModelMap))) {
				//create detail model				
				$detailModel = new $this->_detailModelMap[$this->_row['MODULE']]($this->_id, $this->_row);
				switch ($this->_row['CHANGES_TYPE']) {
					case 'N':
						$updated = $detailModel->deleteNew();
						$this->_errorCode = $detailModel->getErrorCode();
						$this->_errorMsg = $detailModel->getErrorMessage();
						break;
					case 'E':
						$updated = $detailModel->deleteEdit();
						$this->_errorCode = $detailModel->getErrorCode();
						$this->_errorMsg = $detailModel->getErrorMessage();
						break;
					case 'A':
						$updated = $detailModel->deleteActivate();
						$this->_errorCode = $detailModel->getErrorCode();
						$this->_errorMsg = $detailModel->getErrorMessage();
						break;
					case 'D':
						$updated = $detailModel->deleteDeactivate();
						$this->_errorCode = $detailModel->getErrorCode();
						$this->_errorMsg = $detailModel->getErrorMessage();
						break;
					case 'L':
						$updated = $detailModel->deleteDeactivate();
						$this->_errorCode = $detailModel->getErrorCode();
						$this->_errorMsg = $detailModel->getErrorMessage();
						break;
					default:
						$updated = false;
						$this->_errorCode = '22';
						$this->_errorMsg = 'Unknown changes type';
						throw new Exception($this->_errorMsg);
						break;
				}

				if ($updated) {
					$rowDeleted = $this->dbObj->delete($this->_name, $this->dbObj->quoteInto('CHANGES_ID = ?', $this->_id));
					if (!$rowDeleted) {
						$this->_errorCode = '82';
						$this->_errorMsg = 'Query Failed (Global Changes)';
						throw new Exception($this->_errorMsg);
					}
				} else
					throw new Exception($this->_errorMsg);
			} else {
				$this->_errorCode = '22';
				$this->_errorMsg = 'Unknown Module';
				throw new Exception($this->_errorMsg);
			}
		} catch (Exception $e) {

			$this->dbObj->rollBack();
			//			SGO_Helper_GeneralLog::technicalLog($e);
			//$this->_errorCode = '82';
			//$this->_errorMsg = 'Query Failed (Global Changes)';
			//rethrow exception (use this for debugging)
			//			throw $e;
			return false;
		}

		$this->dbObj->commit();
		return true;
	}

	/**
	 * Request change repair
	 * Update status in changes table to "request repair"
	 *
	 * @param  mixed $actor User name/id who request changes repair,for use in logging into database
	 * @param  String $note OPTIONAL Approval note to set in database
	 * @return boolean indicating operation success/failure
	 */
	public function requestRepair($actor, $note = null)
	{
		if ($this->_row['CHANGES_TYPE'] != 'N' && $this->_row['CHANGES_TYPE'] != 'E') {
			$this->_errorCode = '81';
			$this->_errorMsg = 'Changes could not be processed to request repair due to suggest type status are prohibited';
			return false;
		}

		$changeModulePrivilegeObj  = new Changemanagement_Model_Privilege();
		$isAuthorizeRequestRepair = $changeModulePrivilegeObj->isAuthorizeRequestRepair($this->_row['MODULE']);
		if (!$isAuthorizeRequestRepair) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'You have no privilege for request repair specified changes';
			return false;
		}

		$privilgeDualControl = $changeModulePrivilegeObj->getPrivilege($this->_row['MODULE'], 'Request Repair');
		if ($this->_row['DISPLAY_TABLENAME'] == "Backend Group") {
			// $fullDesc = "Permintaan Perbaikan Usulan ID : {$this->_row['CHANGES_ID']}. Suggest Data : {$this->_row['DISPLAY_TABLENAME']}, Company : {$this->_row['COMPANY_CODE']}, Data ID : {$this->_row['KEY_FIELD']}, Reason : {$note}";
			$fullDesc = "Permintaan Perbaikan Usulan ID : {$this->_row['CHANGES_ID']}. Suggest Data : {$this->_row['DISPLAY_TABLENAME']}, Data ID : {$this->_row['KEY_FIELD']}, Reason : {$note}";
		} else {
			$fullDesc = "Request Repair Changes Id : {$this->_row['CHANGES_ID']} Suggest Data : {$this->_row['DISPLAY_TABLENAME']} Data Id : {$this->_row['KEY_FIELD']}  Data Name : {$this->_row['KEY_VALUE']} ";
		}

		if ($privilgeDualControl == 'CHCR') {
			Application_Helper_General::writeLog('CHCA', $fullDesc);
		}
		if ($privilgeDualControl == 'BGAP') {
			Application_Helper_General::writeLog('BGRR', $fullDesc);
		} else {
			Application_Helper_General::writeLog($privilgeDualControl, $fullDesc);
		}
		//Application_Helper_General::writeLog($privilgeDualControl,$fullDesc);

		$rowsUpdated = $this->dbObj->update(
			$this->_name,
			array(
				'CHANGES_STATUS' => 'RR',
				'CHANGES_REASON' => $note,
				'CREATED'             => new Zend_Db_Expr('now()'),
				'CREATED_BY'          => $actor,
				//'LASTUPDATEDBY' => $actor,
				//'LASTUPDATEDBY' => $actor,
				//'LASTUPDATEDBY' => $actor,
				//NOTE: this field expression is Ms Sql Specific
				'LASTUPDATED' => new Zend_Db_Expr("now()")
			),
			$this->dbObj->quoteInto('CHANGES_ID = ?', $this->_id)
		);

		if (!(bool) $rowsUpdated) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query Failed (Global Changes)';
			return false;
		}

		return true;
	}

	/**
	 * Approve changes
	 *
	 * @param  mixed $actor User name/id who approved changes,for use in logging into database
	 * @param  String $note OPTIONAL Approval note to set in database
	 * @return boolean indicating operation success/failure
	 */
	public function approve($actor = null, $note = null)
	{

		$changeModulePrivilegeObj  = new Changemanagement_Model_Privilege();
		$isAuthorizeAppove = $changeModulePrivilegeObj->isAuthorizeApprove($this->_row['MODULE']);
		if (!$isAuthorizeAppove) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'You have no privilege for approve specified changes';
			return false;
		}

		//print_r($this->_row['MODULE']);die;

		$privilgeDualControl = $changeModulePrivilegeObj->getPrivilege($this->_row['MODULE'], 'Approve');

		if ($this->_row['DISPLAY_TABLENAME'] == "Holiday Setting") {
			$fullDesc = "Approve Changes Id : {$this->_row['CHANGES_ID']}. Suggest Data : {$this->_row['DISPLAY_TABLENAME']}. Data Id : {$this->_row['KEY_FIELD']}, Reason: {$note}";
		} else if ($this->_row['DISPLAY_TABLENAME'] == "Backend Group") {
			// $fullDesc = "Menyetujui Usulan ID : {$this->_row['CHANGES_ID']}. Suggest Data : {$this->_row['DISPLAY_TABLENAME']}, Company : {$this->_row['COMPANY_CODE']}, Data ID : {$this->_row['KEY_FIELD']}, Reason : {$note}";
			$fullDesc = "Menyetujui Usulan ID : {$this->_row['CHANGES_ID']}. Suggest Data : {$this->_row['DISPLAY_TABLENAME']}, Data ID : {$this->_row['KEY_FIELD']}, Reason : {$note}";
		} else if ($this->_row['DISPLAY_TABLENAME'] == "Minimum Amount and Currency Available") {
			$fullDesc = "Approve Changes Id : {$this->_row['CHANGES_ID']}. Suggest Data : {$this->_row['DISPLAY_TABLENAME']} Data Id : {$this->_row['KEY_FIELD']}  Data Name : {$this->_row['KEY_VALUE']}";
		} else {
			$fullDesc = "Approve Changes Id : {$this->_row['CHANGES_ID']} Suggest Data : {$this->_row['DISPLAY_TABLENAME']} Data Id : {$this->_row['KEY_FIELD']}  Data Name : {$this->_row['KEY_VALUE']} ";
		}
		//if ($this->_row['MODULE'] != 'backendgroup') {
		//die('here');
		if ($privilgeDualControl == 'CHCR') {
			Application_Helper_General::writeLog('CHCA', $fullDesc);
		} else {
			Application_Helper_General::writeLog($privilgeDualControl, $fullDesc);
		}
		//}
		//Application_Helper_General::writeLog($privilgeDualControl,$fullDesc);



		try {

			// $this->dbObj->beginTransaction();
			//check whether $this->_row['MODULE'] value exists in $this->_detailModelMap
			//echo '<pre>';
			//var_dump($this->_detailModelMap);
			//var_dump($this->_row['MODULE']);

			if (in_array($this->_row['MODULE'], array_keys($this->_detailModelMap))) {
				//create detail model
				//echo '<pre>';
				//var_dump($this->_id);
				//var_dump($this->_row);
				//var_dump($this->_detailModelMap[$this->_row['MODULE']]);die;
				$detailModel = new $this->_detailModelMap[$this->_row['MODULE']]($this->_id, $this->_row);


				//print_r($this->_detailModelMap[$this->_row['MODULE']]);die;
				//print_r($this->_row['CHANGES_TYPE']);
				switch ($this->_row['CHANGES_TYPE']) {
					case 'N':
						//die('here');
						//$updated = $detailModel->approveNew();
						$updated = $detailModel->approveNew($actor);
						$this->_errorCode = $detailModel->getErrorCode();
						$this->_errorMsg  = $detailModel->getErrorMessage();
						//var_dump($this->_errorCode);
						//var_dump($this->_errorMsg);die;
						break;
					case 'E':
						// die;
						try {
							$updated = $detailModel->approveEdit($actor);
						} catch (Exception $e) {
							// print_r($e);die;
						}

						$this->_errorCode = $detailModel->getErrorCode();
						$this->_errorMsg = $detailModel->getErrorMessage();

						break;
					case 'A':
						$updated = $detailModel->approveActivate($actor);
						$this->_errorCode = $detailModel->getErrorCode();
						$this->_errorMsg = $detailModel->getErrorMessage();
						break;
					case 'D':
						$updated = $detailModel->approveDeactivate($actor);
						$this->_errorCode = $detailModel->getErrorCode();
						$this->_errorMsg = $detailModel->getErrorMessage();
						break;
					case 'L':
						$updated = $detailModel->approveDelete($actor);
						$this->_errorCode = $detailModel->getErrorCode();
						$this->_errorMsg = $detailModel->getErrorMessage();
						break;
					case 'S':

						$updated = $detailModel->approveSuspend($actor);
						$this->_errorCode = $detailModel->getErrorCode();
						$this->_errorMsg = $detailModel->getErrorMessage();
						break;
					case 'U':
						$updated = $detailModel->approveUnsuspend($actor);
						$this->_errorCode = $detailModel->getErrorCode();
						$this->_errorMsg = $detailModel->getErrorMessage();
						break;
					default:
						$updated = false;
						$this->_errorCode = '82';
						$this->_errorMsg = 'Unknown changes type';
						//						throw new Exception($this->_errorMsg);
						break;
				}

				if ($updated) {
					$rowUpdated = $this->dbObj->update(
						$this->_name,
						array(
							'CHANGES_STATUS' => 'AP',
							//'CHANGES_STATUS' => 'RJ',
							'CHANGES_REASON' => $note,
							//'LASTUPDATEDBY' => $actor,
							//NOTE: this field expression is Ms Sql Specific
							'LASTUPDATED' => new Zend_Db_Expr("now()"),
						),
						$this->dbObj->quoteInto('CHANGES_ID = ?', $this->_id)
					);


					if (!(bool) $rowUpdated) {
						$this->_errorCode = '82';
						$this->_errorMsg = 'Query Failed (Global Changes)';
						//						throw new Exception($this->_errorMsg);
					}
				} else {
					return false;
					//throw new Exception($this->_errorMsg);
				}
			} else {
				$this->_errorCode = '82';
				$this->_errorMsg = 'Unknown Module';
				//				throw new Exception($this->_errorMsg);
			}
			// $this->dbObj->commit();	
		} catch (Exception $e) {
			// die('hhere');
			//echo "<pre>";
			//var_dump($e);
			//die();

			$this->dbObj->rollBack();
			// Zend_Debug::dump($e);
			// die('121');
			//			Application_Helper_GeneralLog::technicalLog($e);
			//$this->_errorCode = '82';
			//$this->_errorMsg = 'Query Failed (Global Changes)';
			//rethrow exception (use this for debugging)
			//			throw $e;
			return false;
		}
		// die('hhere1');
		// try {
		// 	$this->dbObj->commit();	
		// } catch (Exception $e) {
		// 	// print_r($e);die;
		// }
		// die;
		return true;
	}

	/**
	 * Get Error Message
	 *
	 * @return String Error Message
	 */
	public function getErrorMessage()
	{
		return $this->_errorMsg;
	}

	public function getErrorCode()
	{
		return $this->_errorCode;
	}

	public function getModuleId()
	{
		return $this->_moduleId;
	}
	public function getSuggestData()
	{
		return $this->_suggestData;
	}
	public function getChangesInfo()
	{
		return $this->_row;
	}

	/**
	 * Get changes detail link
	 *
	 * @return array detail fields as array
	 */
	public function getDetailLink()
	{
		if (!isset($this->_id)) {
			throw new Exception('Can not get detail. id is not set');
		}

		$detailLink = array(
			'controller' => 'suggestiondetail',
			'action' => 'index',
			'module' => $this->_row['MODULE'],
			'params' => array('changes_id' => $this->_id)
		);
		return $detailLink;
		//create detail model 
		//$detailModel = new $this->_detailModelMap[$this->_row['MODULE']]($this->_id);
		//return $detailModel->getDetailLink($this->_id);
		/*
		if($this->_row['MODULE']=='anchor')
		{			
			//changes for anchor and scheme
			$detailLink = array('controller' => '',
								 'action' => '',
								 'module' => '',
								 'params' => array()
								);
		}
		*/
	}
}
