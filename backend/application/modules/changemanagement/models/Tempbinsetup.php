<?php
/**
 * Tempcustomer model
 *
 * @author
 * @version
 */

require_once 'Service/Token.php';
require_once 'General/CustomerUser.php';

class Changemanagement_Model_Tempbinsetup extends Changemanagement_Model_Tempchanges
{
	protected $_moduleId = '';
	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function approveNew($actor = null)
	{
		//query from TEMP_CUSTOMER
		$customer = $this->dbObj->select()
						  ->from('TEMP_CUSTOMER_BIN')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);
        if(!count($customer))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
        }
  
		//insert to master table M_CUSTOMER
		$insertArr = array_diff_key($customer,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		$insertArr['BIN_CREATED']     = new Zend_Db_Expr('now()');
		$insertArr['BIN_CREATEDBY']   = $actor;
		$insertArr['BIN_UPDATED']     = new Zend_Db_Expr('now()');
		$insertArr['APPROVED_DATE']     = new Zend_Db_Expr('now()');
		$insertArr['BIN_UPDATEDBY']   = $actor;
		
		$insertArr['CUST_ID']   = $customer['CUST_ID'];
		$insertArr['CUST_BIN']   = $customer['CUST_BIN'];
	try
		{
			$customerins = $this->dbObj->insert('M_CUSTOMER_BIN',$insertArr);
		}
	catch(exception $e)
		{
			Zend_Debug::dump($e->getMessage());
		}
		if(!(boolean)$customerins)
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
		}

		$deleteChanges  = $this->deleteNew();
		if(!$deleteChanges)
			return false;

		return true;
	}

	/**
     * Approve Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function approveEdit($actor = null)
	{
		$transFailed = false;
		//query from TEMP_CUSTOMER
		$customer = $this->dbObj->select()
						  ->from('TEMP_CUSTOMER_BIN')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($customer))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
        }
		else
		{
			$custId = $customer['CUST_ID'];
		}
//		Zend_Debug::dump($customer);
//		die;
		//update record customer
		$updateArr = array_diff_key($customer,array('TEMP_ID'=>'','CHANGES_ID'=>'','CUST_ID'=>'','BIN_CREATED'=>'','BIN_CREATEDBY'=>'','CUST_BIN_STATUS'=>''));
		$updateArr['BIN_UPDATED']     = new Zend_Db_Expr('now()');
		$updateArr['BIN_UPDATEDBY']   = $actor;

		$whereArr = array(
							'CUST_ID = ?'=>(string)$customer['CUST_ID'],
							'CUST_BIN = ?'=>(string)$customer['CUST_BIN']
						);
						
		$customerupdate = $this->dbObj->update('M_CUSTOMER_BIN',$updateArr,$whereArr);

		if(!(boolean)$customerupdate)
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
		}
		
		//log CRUD
		//Application_Helper_General::writeLog('CCCA','Succesfully Edited, Cust Id : '.$customer['CUST_ID']. ' Cust Name : '.$updateArr['CUST_NAME'].' Change id : '.$this->_changeId);

		$deleteChanges  = $this->deleteEdit();
		if(!$deleteChanges)
			return false;

		return true;
	}

	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveUnsuspend($actor = null) {
		//query from TEMP_CUSTOMER
		$customer = $this->dbObj->select()
						  ->from('TEMP_CUSTOMER_BIN')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($customer)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
        }


		$updateArr['CUST_BIN_STATUS']      = 1;   //1 = AKTIF
		$updateArr['BIN_SUGGESTED']   = $customer['BIN_SUGGESTED'];
		$updateArr['BIN_SUGGESTEDBY'] = $customer['BIN_SUGGESTEDBY'];
		$updateArr['BIN_UPDATED']     = new Zend_Db_Expr('now()');
		$updateArr['BIN_UPDATEDBY']   = $actor;



		$whereArr = array(
			'CUST_ID = ?'=>(string)$customer['CUST_ID'],
			'CUST_BIN = ?'=>(string)$customer['CUST_BIN']
		);
		$customerupdate = $this->dbObj->update('M_CUSTOMER_BIN',$updateArr,$whereArr);
		if(!(boolean)$customerupdate) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
		}

		//log CRUD
		//Application_Helper_General::writeLog('CCCA','Succesfully Unsuspended, Cust Id : '.$customer['CUST_ID']. ' Cust Name : '.$customer['CUST_NAME'].' Change id : '.$this->_changeId);

		/*
		//update all users status
		$updateArr = array('USER_STATUS' => 'A');
		$updateArr['LASTUPDATED'] = new Zend_Db_Expr('now()');
		$userupdate = $this->dbObj->update('M_USER',$updateArr,$whereArr);
		if(!(boolean)$userupdate) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(User)';
			return false;
		}
		*/

		$deleteChanges  = $this->deleteUnsuspend();
		if(!$deleteChanges)
			return false;

		return true;
	}

	/**
     * Approve Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveSuspend($actor = null) {
		//query from TEMP_CUSTOMER
		$customer = $this->dbObj->select()
						  ->from('TEMP_CUSTOMER_BIN')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($customer)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
        }

		//=====================================================================
		//select all anchors made from this customer
		/*$anchors = $this->dbObj->select()
							   ->from('M_ANCHOR')
							   ->where('ANCHOR_CUST = ?',(string)$customer['CUST_ID'])
							   ->query()
							   ->fetchAll(Zend_Db::FETCH_ASSOC);
		//select all member ids of this customer
		$members = $this->dbObj->select()
							   ->from('M_MEMBER')
							   ->where('MEMBER_CUST = ?',(string)$customer['CUST_ID'])
							   ->query()
							   ->fetchAll(Zend_Db::FETCH_ASSOC);	*/

		//======================================================================
		//START VALIDATION
		//Proses me �nonaktifkan Customer� hanya bisa dilakukan jika
		//Customer tersebut sudah tidak mempunyai kewajiban apapun
		//(data Loan Main Account sudah dalam status �Closed�).
		//Khusus untuk Customer yang berfungsi sebagai anchor,
		//maka fungsi ini hanya berlaku jika
		//seluruh membernya sudah tidak mempunyai kewajiban apapun dengan Bank
		//(data Loan Main Account seluruh member harus dengan status �Closed�)

		//find this customer's member Ids
		//see $members above
		//if(is_array($members) && count($members))
		//{
			/*$validation_member_where ='MEMBER_ID IN (';
			foreach($members as $val)
				$validation_member_where .= $this->dbObj->quoteInto('?,',$val['MEMBER_ID']);
			$validation_member_where = substr($validation_member_where,0,-1);
			$validation_member_where .= ')';*/

			/*
			$activeLoan = $this->dbObj->select()
									  ->from('T_LOAN')
									  ->where($validation_member_where)
									  ->where("LOAN_STATUS IN ('C','O')")
									  ->query()
									  ->fetchAll(Zend_Db::FETCH_ASSOC);
			if(count($activeLoan)>0) {
				$this->_errorMsg = 'Cannot approve. Unsettled loan';
				return false;
			}
			*/

			/*$activeTransaction = $this->dbObj->select()
											 ->from('T_TX')
											 ->where($validation_member_where)
											 //->where("TX_STATUS IN ('WA','WH','WV','WR','BA','WE',RT')")
											 ->where("TX_STATUS NOT IN ('RJ','S','F','SP')")
											 ->query()
											 ->fetchAll(Zend_Db::FETCH_ASSOC);
			if(is_array($activeTransaction) && count($activeTransaction)) {
				$this->_errorCode = '80';
				$this->_errorMsg = 'Cannot approve. Unsettled transaction';
				return false;
			}*/
		//}



		//END OF VALIDATION
		//======================================================================

		//update customer status
		//$updateArr = array('CUST_STATUS' => 'I');

		$updateArr['CUST_BIN_STATUS']      = 2;
		$updateArr['BIN_SUGGESTED']   = $customer['BIN_SUGGESTED'];
		$updateArr['BIN_SUGGESTEDBY'] = $customer['BIN_SUGGESTEDBY'];
		$updateArr['BIN_UPDATED']     = new Zend_Db_Expr('now()');
		$updateArr['BIN_UPDATEDBY']   = $actor;


		//$updateArr['LASTUPDATED'] = new Zend_Db_Expr('now()');
		$whereArr = array(
							'CUST_ID = ?'=>(string)$customer['CUST_ID'],
							'CUST_BIN = ?'=>(string)$customer['CUST_BIN']
						);
		$customerupdate = $this->dbObj->update('M_CUSTOMER_BIN',$updateArr,$whereArr);
		if(!(boolean)$customerupdate)
		{
			$this->_errorCode = '82';
		    $this->_errorMsg = 'Query failed(Customer)';
			return false;
		}

		//log CRUD
		//Application_Helper_General::writeLog('CCCA','Succesfully Suspended, Cust Id : '.$customer['CUST_ID']. ' Cust Name : '.$customer['CUST_NAME'].' Change id : '.$this->_changeId);

	    /*//update all this customer's users status
		$updateArr = array('USER_STATUS' => 'I');
		$updateArr['LASTUPDATED'] = new Zend_Db_Expr('now()');
		$userupdate = $this->dbObj->update('M_USER',$updateArr,$whereArr);

		//update member where cust_id = this customer id
		$whereArr = array('MEMBER_CUST = ?'=>(string)$customer['CUST_ID']);
		$updateArr = array('MEMBER_STATUS' => 'I');
		$updateArr['LASTUPDATED'] = new Zend_Db_Expr('now()');
		$memberupdate = $this->dbObj->update('M_MEMBER',$updateArr,$whereArr);*/

		$deleteChanges  = $this->deleteSuspend();
		if(!$deleteChanges)
			return false;

		return true;
	}


	/*
	 if delete customer :
	 - delete user
	 - delete bank account
	 - delete user limit
	 - delete daily limit
	*/
	public function approveDelete($actor = null)
	{
		//query from TEMP_CUSTOMER
		$customer = $this->dbObj->select()
						  ->from('TEMP_CUSTOMER_BIN')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);

		if(!count($customer))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
        }

        //update status customer
	    $updateArr = array('CUST_BIN_STATUS' => 3);
	    $updateArr['BIN_SUGGESTED']   = $customer['BIN_SUGGESTED'];
		$updateArr['BIN_SUGGESTEDBY'] = $customer['BIN_SUGGESTEDBY'];
		$updateArr['BIN_UPDATED']     = new Zend_Db_Expr('now()');
		$updateArr['BIN_UPDATEDBY']   = $actor;
		
		/////////////////
		$updateArrDataTrans = array('PAID_STATUS' => 3);
		$whereArrDataTrans = array(
			'CUST_BIN = ?'=>(string)$customer['CUST_BIN_TMP']
		);
		$customerupdateDataTrans = $this->dbObj->update('T_VP_TRANSACTION',$updateArrDataTrans,$whereArrDataTrans);
		/////////////////
		
		$whereArr = array(
			'CUST_ID = ?'=>(string)$customer['CUST_ID'],
			'CUST_BIN = ?'=>(string)$customer['CUST_BIN_TMP']
		);
		$customerupdate = $this->dbObj->update('M_CUSTOMER_BIN',$updateArr,$whereArr);

		if(!(boolean)$customerupdate)
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
		}
        //END update status customer


		
		//delete temp customer
		$deleteChanges  = $this->deleteDelete();
		if(!$deleteChanges)
			      return false;

		return true;
	}

	/**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew()
	{

		//delete from TEMP_CUSTOMER
		$customerdelete = $this->dbObj->delete('TEMP_CUSTOMER_BIN',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$customerdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer)';
//			return false;
//		}


		//delete from TEMP_FGROUP
		//$schemedelete = $this->dbObj->delete('TEMP_SCHEME',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));

		return true;
	}

	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteEdit() {

		//delete from TEMP_CUSTOMER
		$customerdelete = $this->dbObj->delete('TEMP_CUSTOMER_BIN',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$customerdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer)';
//			return false;
//		}

		//delete from TEMP_FGROUP
		//$schemedelete = $this->dbObj->delete('TEMP_SCHEME',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));

		return true;
	}

	/**
     * Delete Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteUnsuspend() {

		//delete from TEMP_CUSTOMER
		$customerdelete = $this->dbObj->delete('TEMP_CUSTOMER_BIN',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$customerdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer)';
//			return false;
//		}

		return true;
	}

	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteSuspend() {

		//delete from TEMP_CUSTOMER
		$customerdelete = $this->dbObj->delete('TEMP_CUSTOMER_BIN',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$customerdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer)';
//			return false;
//		}

		return true;
	}

	public function deleteDelete()
	{


		$customerdelete = $this->dbObj->delete('TEMP_CUSTOMER_BIN',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));

		return true;



	}


    public function approveDeactivate(){

	}

	public function approveActivate(){

	}

    public function deleteDeactivate(){

	}

	public function deleteActivate(){

	}





}