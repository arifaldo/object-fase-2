<?php
/**
 * Tempuser model
 * 
 * @author 
 * @version
 */

class Changemanagement_Model_Temptransactiondailylimit extends Changemanagement_Model_Tempchanges 
{
	protected $_moduleId = 'GNS';
	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function approveNew($actor = null) {
	}
	
	/**
     * Approve Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function approveEdit($actor = null) {
		//query added global backend makerlimit & dailylimit -- general setting
//		$set = new Settings();
//		$dailyLimitUser = $set->getTmpSetting('daily_limit_user_account');
//		$makerLimitUser = $set->getTmpSetting('maker_limit_user_account');
//
//		$set->updateDailyLimit($dailyLimitUser);
//		$set->updateMakerLimit($makerLimitUser);

		//echo 'here';die;
		//query from TEMP_USER
		$listSetting = $this->dbObj->select()
						  	->from('TEMP_SETTING')
						  	->where('CHANGES_ID = ?',$this->_changeId)
						  	->query()
						  	->fetchAll(Zend_Db::FETCH_ASSOC);
		if(!count($listSetting)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(General Setting)';
			return false;
        }
		$userupdate = 0;
		if(is_array($listSetting)){
			foreach ($listSetting as $row) {
					$updateArr = array_diff_key($row,array('TEMP_ID'=>'','CHANGES_ID'=>'','SETTING_ID'=>''));
					$whereArr = array('SETTING_ID = ?'=> (string) $row['SETTING_ID']);
					$userupdate += $this->dbObj->update('M_SETTING',$updateArr,$whereArr);
					//echo $userupdate; 
    		}
		}
		//die();
		Application_Helper_General::writeLog('STCt','Approve Transaction Daily Limit');

		 
		//update record //disuruh agung
// 		if( $userupdate==0) {
// 			$this->_errorCode = '82';
// 			$this->_errorMsg = 'Query failed(General Setting)';
// 			return false;
// 		}
		
		$deleteChanges  = $this->deleteEdit();
		if(!$deleteChanges)return false;
		
		return true;
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveActivate($actor = null) {
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveDeactivate($actor = null) {
	}
	
	public function approveDelete($actor = null) {
		//will never be called
	}
	
/**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew() {
	}
	
	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteEdit() {
		//delete from TEMP_USER
		$userdelete = $this->dbObj->delete('TEMP_SETTING',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$userdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(User)';
//			return false;
//		}
		
		return true;
	}
	
/**
     * Delete Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteActivate() {
	}
	
	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteDeactivate() {
	}
	
	public function deleteDelete() {
	}
}