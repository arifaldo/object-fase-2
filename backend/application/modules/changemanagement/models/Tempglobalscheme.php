<?php
/**
 * Tempuser model
 * 
 * @author 
 * @version
 */

class Changemanagement_Model_Tempglobalscheme extends Changemanagement_Model_Tempchanges 
{
	protected $_moduleId = '';
	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function approveNew($actor = null) {
	}
	
	/**
     * Approve Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function approveEdit($actor = null) {
	
		//query from TEMP_USER
		$listAccount = $this->dbObj->select()
						  	->from('TEMP_BUSINESS_SCHEME')
						  	->where('CHANGES_ID = ?',$this->_changeId)
						  	->query()
						  	->fetchAll(Zend_Db::FETCH_ASSOC);
		if(!count($listAccount)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(listAccount)';
			return false;
        }
		if(is_array($listAccount)){
			foreach ($listAccount as $row) {
					$updateArr = array_diff_key($row,array('TEMP_ID'=>'','CHANGES_ID'=>''));
					
					$updateArr['UPDATED'] = new Zend_Db_Expr('now()');
					$updateArr['UPDATEDBY'] = $actor;
					$updateArr['SUGGESTED'] =  $this->_changesInfo['CREATED'];
					$updateArr['SUGGESTEDBY'] = $this->_changesInfo['CREATED_BY'];
					
					
					$whereArr = array('BUSS_SCHEME_CODE = ?'=> (string) $row['BUSS_SCHEME_CODE']);
				$userupdate = $this->dbObj->update('M_BUSINESS_SCHEME',$updateArr,$whereArr);
			}
		}				  	
		
		
		// temp busines_scheme_charges
		$delete = $this->dbObj->delete('M_BUSINESS_SCHEME_CHARGES',$this->dbObj->quoteInto('BUSS_SCHEME_CODE = ?',$listAccount[0]['BUSS_SCHEME_CODE']));
		
		$scheme_arr = $this->dbObj->select()
						  	       ->from('TEMP_BUSINESS_SCHEME_CHARGES')
						  	       ->where('CHANGES_ID = ?',$this->_changeId)
						  	       ->query()->fetchAll();
		
		
        if(is_array($scheme_arr) && count($scheme_arr)) 
		{
	       foreach($scheme_arr as $row)
           {  
              // utk menyaring key field sebelum di insert ke database
              $insertArr = array_diff_key($row,array('TEMP_ID'=>'','CHANGES_ID'=>''));
          
              // mengisi nilai field MEMBER_ID
				
		      $insert_scheme_charges = $this->dbObj->insert('M_BUSINESS_SCHEME_CHARGES',$insertArr);
		  
              if(!(boolean)$insert_scheme_charges) 
		      {
			     $this->_errorCode = '82';
			     $this->_errorMsg = 'Query failed(Member Fee)';
			     return false;
		      }
           }
        }
		
		
		//update record
	
		
		$deleteChanges  = $this->deleteEdit();
		if(!$deleteChanges)return false;
		
		return true;
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveActivate($actor = null) {
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveDeactivate($actor = null) {
	}
	
	public function approveDelete($actor = null) {
		//will never be called
	}
	
/**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew() {
	}
	
	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteEdit() {
		//delete from TEMP_USER
		$delete1 = $this->dbObj->delete('TEMP_BUSINESS_SCHEME',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		$delete2 = $this->dbObj->delete('TEMP_BUSINESS_SCHEME_CHARGES',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$userdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(User)';
//			return false;
//		}
		
		return true;
	}
	
/**
     * Delete Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteActivate() {
	}
	
	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteDeactivate() {
	}
	
	public function deleteDelete() {
	}
}