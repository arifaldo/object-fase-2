<?php

/**
 * Tempcustomer model
 *
 * @author
 * @version
 */


class Changemanagement_Model_Tempbusinessadapter extends Changemanagement_Model_Tempchanges
{
	protected $_moduleId = '';
	/**
	 * Approve Changes for changes type: New
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function approveNew($actor = null)
	{
		//query from TEMP_CUSTOMER
		$customer = $this->dbObj->select()
			->from('TEMP_ADAPTER_PROFILE')
			->where('CHANGES_ID = ?', $this->_changeId)
			->query()
			->fetch(Zend_Db::FETCH_ASSOC);
		if (!count($customer)) {
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
		}
		
		$adapterdetailq = $this->dbObj->select()
			->from('TEMP_ADAPTER_PROFILE_DETAIL')
			->where('CHANGES_ID = ?', $this->_changeId);
			//->query()
			//->fetch(Zend_Db::FETCH_ASSOC);
			
		$adapterdetail = $this->dbObj->fetchAll($adapterdetailq);	

		$insertArr = array_diff_key($customer,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		
		//adapterdetail
		//$insertArr['TEMP_CREATED']     = new Zend_Db_Expr('now()');
		//$insertArr['TEMP_CREATEDBY']   = $actor;
		//$insertArr['TEMP_UPDATED']     = new Zend_Db_Expr('now()');
		// $insertArr['APPROVED_DATE']     = new Zend_Db_Expr('now()');
		//$insertArr['TEMP_UPDATEDBY']   = $actor;

		//$insertArr['TEMP_SUGGESTED']   = $customer['TEMP_SUGGESTED'];
		//$insertArr['TEMP_SUGGESTEDBY'] = $customer['TEMP_SUGGESTEDBY'];
			$customerdelete = $this->dbObj->delete('M_ADAPTER_PROFILE', $this->dbObj->quoteInto('PROFILE_ID = ?', (string) $insertArr['PROFILE_ID']));
			$customerdetaildelete = $this->dbObj->delete('M_ADAPTER_PROFILE_DETAIL', $this->dbObj->quoteInto('PROFILE_ID = ?', (string) $insertArr['PROFILE_ID']));
		try {
			$customerins = $this->dbObj->insert('M_ADAPTER_PROFILE', $insertArr);
			
			if(!empty($adapterdetail)){
				foreach($adapterdetail as $val){
					$insertDetailArr = array_diff_key($val,array('TEMP_ID'=>'','CHANGES_ID'=>''));
					$adapterdetailins = $this->dbObj->insert('M_ADAPTER_PROFILE_DETAIL', $insertDetailArr);
				}
			}
			
		} catch (exception $e) {
			Zend_Debug::dump($e->getMessage());
		}
		if (!(bool) $customerins) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
		}
		
		

		$deleteChanges  = $this->deleteNew();
		if (!$deleteChanges)
			return false;

		return true;
	}

	/**
	 * Approve Changes for changes type: Edit
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function approveEdit($actor = null)
	{
		$customer = $this->dbObj->select()
			->from('TEMP_ADAPTER_PROFILE')
			->where('CHANGES_ID = ?', $this->_changeId)
			->query()
			->fetch(Zend_Db::FETCH_ASSOC);
		if (!count($customer)) {
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
		}
		
		$adapterdetailq = $this->dbObj->select()
			->from('TEMP_ADAPTER_PROFILE_DETAIL')
			->where('CHANGES_ID = ?', $this->_changeId);
			//->query()
			//->fetch(Zend_Db::FETCH_ASSOC);
			
		$adapterdetail = $this->dbObj->fetchAll($adapterdetailq);	

		$insertArr = array_diff_key($customer,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		
		//adapterdetail
		//$insertArr['TEMP_CREATED']     = new Zend_Db_Expr('now()');
		//$insertArr['TEMP_CREATEDBY']   = $actor;
		//$insertArr['TEMP_UPDATED']     = new Zend_Db_Expr('now()');
		// $insertArr['APPROVED_DATE']     = new Zend_Db_Expr('now()');
		//$insertArr['TEMP_UPDATEDBY']   = $actor;

		//$insertArr['TEMP_SUGGESTED']   = $customer['TEMP_SUGGESTED'];
		//$insertArr['TEMP_SUGGESTEDBY'] = $customer['TEMP_SUGGESTEDBY'];
			$customerdelete = $this->dbObj->delete('M_ADAPTER_PROFILE', $this->dbObj->quoteInto('PROFILE_ID = ?', (string) $insertArr['PROFILE_ID']));
			$customerdetaildelete = $this->dbObj->delete('M_ADAPTER_PROFILE_DETAIL', $this->dbObj->quoteInto('PROFILE_ID = ?', (string) $insertArr['PROFILE_ID']));
		try {
			$customerins = $this->dbObj->insert('M_ADAPTER_PROFILE', $insertArr);
			
			if(!empty($adapterdetail)){
				foreach($adapterdetail as $val){
					$insertDetailArr = array_diff_key($val,array('TEMP_ID'=>'','CHANGES_ID'=>''));
					$adapterdetailins = $this->dbObj->insert('M_ADAPTER_PROFILE_DETAIL', $insertDetailArr);
				}
			}
			
		} catch (exception $e) {
			Zend_Debug::dump($e->getMessage());
		}
		if (!(bool) $customerins) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
		}
		
		

		$deleteChanges  = $this->deleteNew();
		if (!$deleteChanges)
			return false;

		return true;
	}

	/*
	 if delete customer :
	 - delete user
	 - delete bank account
	 - delete user limit
	 - delete daily limit
	*/
	public function approveDelete($actor = null)
	{
		$customer = $this->dbObj->select()
			->from('TEMP_ADAPTER_PROFILE')
			->where('CHANGES_ID = ?', $this->_changeId)
			->query()
			->fetch(Zend_Db::FETCH_ASSOC);
		if (!count($customer)) {
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
		}
		
		$adapterdetailq = $this->dbObj->select()
			->from('TEMP_ADAPTER_PROFILE_DETAIL')
			->where('CHANGES_ID = ?', $this->_changeId);
			//->query()
			//->fetch(Zend_Db::FETCH_ASSOC);
			
		$adapterdetail = $this->dbObj->fetchAll($adapterdetailq);	

		$insertArr = array_diff_key($customer,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		
		//adapterdetail
		//$insertArr['TEMP_CREATED']     = new Zend_Db_Expr('now()');
		//$insertArr['TEMP_CREATEDBY']   = $actor;
		//$insertArr['TEMP_UPDATED']     = new Zend_Db_Expr('now()');
		// $insertArr['APPROVED_DATE']     = new Zend_Db_Expr('now()');
		//$insertArr['TEMP_UPDATEDBY']   = $actor;

		//$insertArr['TEMP_SUGGESTED']   = $customer['TEMP_SUGGESTED'];
		//$insertArr['TEMP_SUGGESTEDBY'] = $customer['TEMP_SUGGESTEDBY'];
			$customerdelete = $this->dbObj->delete('M_ADAPTER_PROFILE', $this->dbObj->quoteInto('PROFILE_ID = ?', (string) $insertArr['PROFILE_ID']));
			$customerdetaildelete = $this->dbObj->delete('M_ADAPTER_PROFILE_DETAIL', $this->dbObj->quoteInto('PROFILE_ID = ?', (string) $insertArr['PROFILE_ID']));
		try {
			$customerins = $this->dbObj->insert('M_ADAPTER_PROFILE', $insertArr);
			
			if(!empty($adapterdetail)){
				foreach($adapterdetail as $val){
					$insertDetailArr = array_diff_key($val,array('TEMP_ID'=>'','CHANGES_ID'=>''));
					$adapterdetailins = $this->dbObj->insert('M_ADAPTER_PROFILE_DETAIL', $insertDetailArr);
				}
			}
			
		} catch (exception $e) {
			Zend_Debug::dump($e->getMessage());
		}
		if (!(bool) $customerins) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
		}
		
		

		$deleteChanges  = $this->deleteNew();
		if (!$deleteChanges)
			return false;

		return true;
	}
	
	public function approveSuspend($actor = null)
	{
		$customer = $this->dbObj->select()
			->from('TEMP_ADAPTER_PROFILE')
			->where('CHANGES_ID = ?', $this->_changeId)
			->query()
			->fetch(Zend_Db::FETCH_ASSOC);
		if (!count($customer)) {
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
		}
		
		$adapterdetailq = $this->dbObj->select()
			->from('TEMP_ADAPTER_PROFILE_DETAIL')
			->where('CHANGES_ID = ?', $this->_changeId);
			//->query()
			//->fetch(Zend_Db::FETCH_ASSOC);
			
		$adapterdetail = $this->dbObj->fetchAll($adapterdetailq);	
		//echo '<pre>';
		//var_dump($adapterdetail);die;
		$insertArr = array_diff_key($customer,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		
		//adapterdetail
		//$insertArr['TEMP_CREATED']     = new Zend_Db_Expr('now()');
		//$insertArr['TEMP_CREATEDBY']   = $actor;
		//$insertArr['TEMP_UPDATED']     = new Zend_Db_Expr('now()');
		// $insertArr['APPROVED_DATE']     = new Zend_Db_Expr('now()');
		//$insertArr['TEMP_UPDATEDBY']   = $actor;

		//$insertArr['TEMP_SUGGESTED']   = $customer['TEMP_SUGGESTED'];
		//$insertArr['TEMP_SUGGESTEDBY'] = $customer['TEMP_SUGGESTEDBY'];
			$customerdelete = $this->dbObj->delete('M_ADAPTER_PROFILE', $this->dbObj->quoteInto('PROFILE_ID = ?', (string) $insertArr['PROFILE_ID']));
			$customerdetaildelete = $this->dbObj->delete('M_ADAPTER_PROFILE_DETAIL', $this->dbObj->quoteInto('PROFILE_ID = ?', (string) $insertArr['PROFILE_ID']));
		try {
			$customerins = $this->dbObj->insert('M_ADAPTER_PROFILE', $insertArr);
			//var_dump($adapterdetail);die('ge');
			if(!empty($adapterdetail)){
				foreach($adapterdetail as $val){
					$insertDetailArr = array_diff_key($val,array('TEMP_ID'=>'','CHANGES_ID'=>''));
					//var_dump($insertDetailArr);
					$adapterdetailins = $this->dbObj->insert('M_ADAPTER_PROFILE_DETAIL', $insertDetailArr);
				}
			}
			//die('ge');
			
		} catch (exception $e) {
			Zend_Debug::dump($e->getMessage());
		}
		//die('here');
		if (!(bool) $customerins) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
		}
		
		

		$deleteChanges  = $this->deleteNew();
		if (!$deleteChanges)
			return false;

		return true;
	}
	
	
	public function approveUnsuspend($actor = null)
	{
		$customer = $this->dbObj->select()
			->from('TEMP_ADAPTER_PROFILE')
			->where('CHANGES_ID = ?', $this->_changeId)
			->query()
			->fetch(Zend_Db::FETCH_ASSOC);
		if (!count($customer)) {
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
		}
		
		$adapterdetailq = $this->dbObj->select()
			->from('TEMP_ADAPTER_PROFILE_DETAIL')
			->where('CHANGES_ID = ?', $this->_changeId);
			//->query()
			//->fetch(Zend_Db::FETCH_ASSOC);
			
		$adapterdetail = $this->dbObj->fetchAll($adapterdetailq);	

		$insertArr = array_diff_key($customer,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		
		//adapterdetail
		//$insertArr['TEMP_CREATED']     = new Zend_Db_Expr('now()');
		//$insertArr['TEMP_CREATEDBY']   = $actor;
		//$insertArr['TEMP_UPDATED']     = new Zend_Db_Expr('now()');
		// $insertArr['APPROVED_DATE']     = new Zend_Db_Expr('now()');
		//$insertArr['TEMP_UPDATEDBY']   = $actor;

		//$insertArr['TEMP_SUGGESTED']   = $customer['TEMP_SUGGESTED'];
		//$insertArr['TEMP_SUGGESTEDBY'] = $customer['TEMP_SUGGESTEDBY'];
			$customerdelete = $this->dbObj->delete('M_ADAPTER_PROFILE', $this->dbObj->quoteInto('PROFILE_ID = ?', (string) $insertArr['PROFILE_ID']));
			$customerdetaildelete = $this->dbObj->delete('M_ADAPTER_PROFILE_DETAIL', $this->dbObj->quoteInto('PROFILE_ID = ?', (string) $insertArr['PROFILE_ID']));
		try {
			$customerins = $this->dbObj->insert('M_ADAPTER_PROFILE', $insertArr);
			
			if(!empty($adapterdetail)){
				foreach($adapterdetail as $val){
					$insertDetailArr = array_diff_key($val,array('TEMP_ID'=>'','CHANGES_ID'=>''));
					$adapterdetailins = $this->dbObj->insert('M_ADAPTER_PROFILE_DETAIL', $insertDetailArr);
				}
			}
			
		} catch (exception $e) {
			Zend_Debug::dump($e->getMessage());
		}
		if (!(bool) $customerins) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Customer)';
			return false;
		}
		
		

		$deleteChanges  = $this->deleteNew();
		if (!$deleteChanges)
			return false;

		return true;
	}
	
	
	
	
	
	public function deleteNew()
	{

		//delete from TEMP_CUSTOMER
		$customerdelete = $this->dbObj->delete('TEMP_ADAPTER_PROFILE', $this->dbObj->quoteInto('CHANGES_ID = ?', $this->_changeId));
		$customerdelete = $this->dbObj->delete('TEMP_ADAPTER_PROFILE_DETAIL', $this->dbObj->quoteInto('CHANGES_ID = ?', $this->_changeId));

		return true;
	}

	/**
	 * Delete Changes for changes type: Edit
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function deleteEdit()
	{

		//delete from TEMP_CUSTOMER
		$customerdelete = $this->dbObj->delete('TEMP_BIN_TEMPLATE', $this->dbObj->quoteInto('CHANGES_ID = ?', $this->_changeId));

		return true;
	}

	/**
	 * Delete Changes for changes type: Activate
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function deleteUnsuspend()
	{

		//delete from TEMP_CUSTOMER
		$customerdelete = $this->dbObj->delete('TEMP_BIN_TEMPLATE', $this->dbObj->quoteInto('CHANGES_ID = ?', $this->_changeId));

		return true;
	}

	/**
	 * Delete Changes for changes type: Deactivate
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function deleteSuspend()
	{

		//delete from TEMP_CUSTOMER
		$customerdelete = $this->dbObj->delete('TEMP_BIN_TEMPLATE', $this->dbObj->quoteInto('CHANGES_ID = ?', $this->_changeId));

		return true;
	}

	public function deleteDelete()
	{


		$customerdelete = $this->dbObj->delete('TEMP_BIN_TEMPLATE', $this->dbObj->quoteInto('CHANGES_ID = ?', $this->_changeId));

		return true;
	}
	public function approveDeactivate()
	{
	}

	public function approveActivate()
	{
	}

	public function deleteDeactivate()
	{
	}

	public function deleteActivate()
	{
	}
}
