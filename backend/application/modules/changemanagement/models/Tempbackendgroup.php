<?php

class Changemanagement_Model_Tempbackendgroup extends Changemanagement_Model_Tempchanges {
	protected $_moduleId = 'BGR';
	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function approveNew($actor = null) {
		
		//query from TEMP_ROOT_COMM
		$backendGroup = $this->dbObj->select()
									 ->from('TEMP_BGROUP')
									 ->where('CHANGES_ID = ?',$this->_changeId)
									 ->query()
									 ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($backendGroup)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Backend Group)';
			return false;
        }
        

        

		//insert to master table M_BACKEND GROUP
		$insertArr = array_diff_key($backendGroup,array('TEMP_ID'=>'','CHANGES_ID'=>''));

		$backendGroupOld = $this->dbObj->select()
									 ->from('M_BGROUP')
									 ->where('BGROUP_ID = ?',$insertArr['BGROUP_ID'])
									 ->query()
									 ->fetch(Zend_Db::FETCH_ASSOC);

		// $insertArr['CREATED'] = new Zend_Db_Expr('now()');
		$insertArr['UPDATED'] = new Zend_Db_Expr('now()');
		// $insertArr['CREATEDBY'] = $actor;
		$insertArr['UPDATEDBY'] = $actor;
		$backendGroup = $this->dbObj->insert('M_BGROUP',$insertArr);


		
		$backendGroupPrivilege = $this->dbObj->fetchAll(
									$this->dbObj->select()
									 ->from('TEMP_BPRIVI_GROUP')
									 ->where('CHANGES_ID = ?',$this->_changeId)
									 );

		$backendGroupPrivilegeOld = $this->dbObj->fetchAll(
									$this->dbObj->select()
									 ->from('M_BPRIVI_GROUP')
									 ->where('BGROUP_ID = ?',$insertArr['BGROUP_ID'])
									 );
									 
		if(!count($backendGroupPrivilege)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Backend Group Privilege)';
			return false;
        }
        
       if(is_array($backendGroupPrivilege)){
	       	//insert to master table M_BPRIVI_GROUP
	       	foreach ($backendGroupPrivilege as $listPrivilege) {
		       	$insertArr = array_diff_key($listPrivilege,array('TEMP_ID'=>'','CHANGES_ID'=>''));
				$backendGroup = $this->dbObj->insert('M_BPRIVI_GROUP',$insertArr);
	       	}
			
       }

       $jsonuserold = json_encode($backendGroupOld);
       $jsongroupold = json_encode($backendGroupPrivilegeOld);
       $jsonusernew = json_encode($backendGroupPrivilege);
       $jsongroupnew = json_encode($backendGroupPrivilege);

       $fullDesc = "Approve Changes Id : ".$this->_changeId." Old Data : ".$jsonuserold." ".$jsongroupold." Suggest Data : ".$jsonusernew." ".$jsongroupnew."  Data Name : ".$insertArr['BGROUP_ID'];
       Application_Helper_General::writeLog('CHCA', $fullDesc);
		
		$deleteChanges  = $this->deleteNew();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	/**
     * Approve Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function approveEdit($actor=NULL) {
		
		$backendGroup = $this->dbObj->select()
									 ->from('TEMP_BGROUP')
									 ->where('CHANGES_ID = ?',$this->_changeId)
									 ->query()
									 ->fetch(Zend_Db::FETCH_ASSOC);
									 
		if(!count($backendGroup)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Backend Group)';
			return false;
        }
        
		//insert to master table M_BACKEND GROUP
		$insertArr = array_diff_key($backendGroup,array('TEMP_ID'=>'','CHANGES_ID'=>'','BGROUP_ID'=>''));
		 //var_dump($insertArr); exit();
		$backendGroupOld = $this->dbObj->select()
									 ->from('M_BGROUP')
									 ->where('BGROUP_ID = ?',$backendGroup['BGROUP_ID'])
									 ->query()
									 ->fetch(Zend_Db::FETCH_ASSOC);

		$insertArr['UPDATED'] = new Zend_Db_Expr('now()');
		$insertArr['UPDATEDBY'] = $actor;
		$whereArr = array('BGROUP_ID = ?'=>$backendGroup['BGROUP_ID']);
		$updateBackendGroup = $this->dbObj->UPDATE('M_BGROUP',$insertArr,$whereArr);
		
		$backendGroupPrivilege = $this->dbObj->fetchAll(
									$this->dbObj->select()
									 ->from('TEMP_BPRIVI_GROUP')
									 ->where('CHANGES_ID = ?',$this->_changeId)
									 );
		
		$backendGroupPrivilegeOld = $this->dbObj->fetchAll(
									$this->dbObj->select()
									 ->from('M_BPRIVI_GROUP')
									 ->where('BGROUP_ID = ?',$backendGroup['BGROUP_ID'])
									 );
		//die('here');							 
		if(!count($backendGroupPrivilege)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Backend Group Privilege)';
			return false;
        }
         
       $whereArr = array('BGROUP_ID = ?'=>$backendGroup['BGROUP_ID']); 
       $backendGroup = $this->dbObj->delete('M_BPRIVI_GROUP',$whereArr);
       if(is_array($backendGroupPrivilege)){
	       	//insert to master table M_BPRIVI_GROUP
	       	foreach ($backendGroupPrivilege as $listPrivilege) {
		       	$insertArr = array_diff_key($listPrivilege,array('TEMP_ID'=>'','CHANGES_ID'=>''));
				$backendGroup = $this->dbObj->insert('M_BPRIVI_GROUP',$insertArr);
	       	}
			
       }
		
           $jsonuserold = json_encode($backendGroupOld);
	       $jsongroupold = json_encode($backendGroupPrivilegeOld);
	       $jsonusernew = json_encode($backendGroupPrivilege);
	       $jsongroupnew = json_encode($backendGroupPrivilege);

	       $fullDesc = "Approve Changes Id : ".$this->_changeId." Old Data : ".$jsonuserold." ".$jsongroupold." Suggest Data : ".$jsonusernew." ".$jsongroupnew."  Data Name : ".$insertArr['BGROUP_ID'];
	       Application_Helper_General::writeLog('CHCA', $fullDesc);
			//var_dump($fullDesc);die; 
		$deleteChanges  = $this->deleteNew();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveActivate() {
	}
	
	/**
     * Approve Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveDeactivate() {
		
	}
	
	public function approveSuspend($actor=NULL) {
		$backendGroup = $this->dbObj->select()
									 ->from('TEMP_BGROUP')
									 ->where('CHANGES_ID = ?',$this->_changeId)
									 ->query()
									 ->fetch(Zend_Db::FETCH_ASSOC);

		if(!count($backendGroup)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Backend Group)';
			return false;
        }
        
        $whereArr = array('BGROUP_ID = ?'=>$backendGroup['BGROUP_ID']);
		//update to master table M_BACKEND GROUP
		$updateArr = array_diff_key($backendGroup,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		$backendGroupOld = $this->dbObj->select()
									 ->from('M_BGROUP')
									 ->where('BGROUP_ID = ?',$backendGroup['BGROUP_ID'])
									 ->query()
									 ->fetch(Zend_Db::FETCH_ASSOC);
									 
		$updateArr['UPDATED'] = new Zend_Db_Expr('now()');
		$updateArr['UPDATEDBY'] = $actor;
		$updateBackendGroup = $this->dbObj->UPDATE('M_BGROUP',$updateArr,$whereArr);
		
			$jsonuserold = json_encode($backendGroupOld);
	       
	       $jsonusernew = json_encode($backendGroupPrivilege);
	       

	       $fullDesc = "Approve Changes Id : ".$this->_changeId." Old Data : ".$jsonuserold."  Suggest Data : ".$jsonusernew."   Data Name : ".$insertArr['BGROUP_ID'];
	       Application_Helper_General::writeLog('CHCA', $fullDesc);

		$deleteChanges  = $this->deleteSuspend();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	public function approveUnsuspend($actor=NULL) {
		$backendGroup = $this->dbObj->select()
									 ->from('TEMP_BGROUP')
									 ->where('CHANGES_ID = ?',$this->_changeId)
									 ->query()
									 ->fetch(Zend_Db::FETCH_ASSOC);

		if(!count($backendGroup)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Backend Group)';
			return false;
        }
        
        $whereArr = array('BGROUP_ID = ?'=>$backendGroup['BGROUP_ID']);
		//update to master table M_BACKEND GROUP
		$updateArr = array_diff_key($backendGroup,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		$updateArr['UPDATED'] = new Zend_Db_Expr('now()');
		$updateArr['UPDATEDBY'] = $actor;
		$updateBackendGroup = $this->dbObj->UPDATE('M_BGROUP',$updateArr,$whereArr);
		
		$deleteChanges  = $this->deleteUnsuspend();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	public function approveDelete($actor=NULL) {
		$backendGroup = $this->dbObj->select()
									 ->from('TEMP_BGROUP')
									 ->where('CHANGES_ID = ?',$this->_changeId)
									 ->query()
									 ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($backendGroup)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Backend Group)';
			return false;
        }
        $whereArr = array('BGROUP_ID = ?'=>$backendGroup['BGROUP_ID']);
		//update to master table M_BACKEND GROUP
		$updateArr = array_diff_key($backendGroup,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		$updateArr['UPDATED'] = new Zend_Db_Expr('now()');
		$updateArr['UPDATEDBY'] = $actor;
		$updateBackendGroup = $this->dbObj->UPDATE('M_BGROUP',$updateArr,$whereArr);
		
		$deleteChanges  = $this->deleteDelete();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	
/**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew() {

		$backendGroup = $this->dbObj->delete('TEMP_BGROUP',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		$backendGroupPrivilege = $this->dbObj->delete('TEMP_BPRIVI_GROUP',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		
		return true;
	}
	
	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteEdit() {

		$backendGroup = $this->dbObj->delete('TEMP_BGROUP',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		$backendGroupPrivilege = $this->dbObj->delete('TEMP_BPRIVI_GROUP',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		
		return true;
	}
	
/**
     * Delete Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteActivate() {
	}
	
	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteDeactivate() {

	}
	
	public function deleteSuspend() {
		$backendGroup = $this->dbObj->delete('TEMP_BGROUP',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		return true;
	}
	
	public function deleteUnsuspend() {
		$backendGroup = $this->dbObj->delete('TEMP_BGROUP',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		return true;
	}
	
	
	public function deleteDelete() {
		$backendGroup = $this->dbObj->delete('TEMP_BGROUP',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		return true;	
	}
}