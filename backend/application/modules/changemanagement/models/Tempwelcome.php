<?php
/**
 * Tempwelcome model
 * 
 * @author
 * @version
 */

class Changemanagement_Model_Tempwelcome extends Changemanagement_Model_Tempchanges {
	
	protected $_moduleId = 'WLT';
	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function approveNew($actor = null) {
		//will never be called
	}
	
	/**
     * Approve Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function approveEdit($actor = null) {
		
		//query from TEMP_SETTING
		$setting = $this->dbObj->select()
						  	   ->from('TEMP_SETTING')
						  	   ->where('CHANGES_ID = ?',$this->_changeId)
						  	   ->query()
						  	   ->fetch(Zend_Db::FETCH_ASSOC);
						  	   
		if(!count($setting)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Setting)';
			return false;
        }
		
//		$where = $this->dbObj->quoteInto('SETTING_ID = ?',$this->_moduleId);				  	   
		$where = array('SETTING_ID = ?'=>$setting['SETTING_ID']);
		$insertArr = array_diff_key($setting,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		
		$settingdelete = $this->dbObj->update('M_SETTING',$insertArr,$where);	
		if(!(boolean)$settingdelete) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Setting)';
			return false;
		}
//		else{
//			return true;
//		}
//		foreach($setting as $val) {
//			$insertArr = array_diff_key($val,array('TEMP_ID'=>'','CHANGES_ID'=>''));
//			$settinginsert = $this->dbObj->update('M_SETTING',$insertArr,$where);
////			$settinginsert = $this->dbObj->insert('M_SETTING',$insertArr);
//			if(!(boolean)$settinginsert) {
//				$this->_errorCode = '82';
//				$this->_errorMsg = 'Query failed(Setting)';
//				return false;
//			}
//		}
		
		$deleteChanges  = $this->deleteEdit();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveActivate($actor = null) {
		//will never be called
	}
	
	/**
     * Approve Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveDeactivate($actor = null) {
		//will never be called
	}
	
	public function approveDelete() {
		//will never be called
	}
	
	/**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew() {
		//will never be called
	}
	
	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteEdit() {

		//delete from TEMP_SETTING
		$settingdelete = $this->dbObj->delete('TEMP_SETTING',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$settingdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Setting)';
//			return false;
//		}
		
		return true;
	}
	
	/**
     * Delete Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteActivate() {
		//will never be called
	}
	
	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteDeactivate() {
		//will never be called
	}
	
	public function deleteDelete() {
		//will never be called
	}
}