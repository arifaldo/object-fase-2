<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Account.php';
require_once 'CMD/Validate/Validate.php';

class customeraccount_NewController extends customeraccount_Model_Customeraccount
{
	
  public function indexAction() 
  { 
  	$this->_moduleDB = strtoupper($this->_moduleID['user']);
    $cust_id = strtoupper($this->_getParam('cust_id'));
    $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;
    $error_remark = null;
    
    $getCcy = $this->getCcy();
    $this->view->CCYData = $getCcy;
    $this->view->custAcct_msg  = array();
    
    $acct_no = $this->_getParam('acct_no');
    
    
    if($cust_id)
    {
  	  $custData = $this->_db->select()
  	                         ->from('M_CUSTOMER',array('CUST_ID','CUST_STATUS'))
  	                         ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
  	                         //->where('CUST_STATUS=1');
  	                         ->query()->fetch();
  	                         
  	   if(!$custData['CUST_ID']) $cust_id = null;
  	   $cust_status = $custData['CUST_STATUS'];
  	}
  	
  	
  
  	
  	if(!$cust_id)
  	{
  	  $error_remark = 'Invalid Cust ID';
	  $this->_helper->getHelper('FlashMessenger')->addMessage('F');
      $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
      $this->_redirect($this->_backURL);
  	}
  	
  	
    
  	
  	if($this->_request->isPost())
  	{ 
  	   $exclude_fgroup_id = '(UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id);
  	   $exclude_fgroup_id .= ' OR UPPER(CUST_ID)='.$this->_db->quote('BANK');
  	   $exclude_fgroup_id .= ') AND UPPER(FGROUP_STATUS)='.$this->_db->quote(strtoupper($this->_masterStatus['code']['active']));
  	 
       $filters = array('acct_no'        => array('StripTags','StringTrim'),
                        'ccy_id'         => array('StripTags','StringTrim','StringToUpper'),
                        'acct_email'     => array('StripTags','StringTrim'),
                        'cust_id'        => array('StripTags','StringTrim','StringToUpper'),
                        );
                      
	   $TokenError = array();
	  
       $validators =  array('cust_id'       => array(),
       
                            'acct_no'       => array('NotEmpty',
                                                     'Digits',
                                                     array('StringLength',array('min'=>1,'max'=>16)),
      	                                             array('Db_NoRecordExists',array('table'=>'M_CUSTOMER_ACCT','field'=>'ACCT_NO','exclude'=>'ACCT_STATUS!=3')),
      	                                             array('Db_NoRecordExists',array('table'=>'TEMP_CUSTOMER_ACCT','field'=>'ACCT_NO')),
                                                     'messages' => array('Can not be empty',
      	                                                                 'Account Number must be numeric',
      	                                                                 'Maximum length of Account Number is 16 digits',
      	                                                                 'Account already assigned. Please use another',
      	                                                                 'Account Number '.$acct_no.' already suggested! Please Approve or Reject previous suggestion first!',
      	                                                                )
                                                    ),

                           'ccy_id'         => array('NotEmpty',
											         'messages' => array(
											                             'Can not be empty',
												                        )
											        ),
											                  
						  
                           'acct_email'     => array(//new Application_Validate_EmailAddress(),
											         array('StringLength',array('max'=>128)),
											                'allowEmpty' => true,
											                'messages' => array(
											                                  //'Invalid email format',
											                                   'Email lenght cannot be more than 128',
												                               )
												    ),            
                         );
					      
	 
	  $zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
      
      
      
      //--------------validasi tambahan-------------------
      // validasi account dan cust id apakah didelete atau tidak
      $cust_id_acc_flag = true;
     
      
	     /* if($zf_filter_input->acct_no)
	      if(count($accData) > 0)
	      { 
	         $accStatus = $accData['ACCT_STATUS'];
	         $custDataBasedAcc = $this->getCustData($accData['CUST_ID']);
	         
	         if($custDataBasedAcc['CUST_STATUS'] == 3) $cust_id_acc_flag = false;
	         else $cust_id_acc_flag = true;
	      }
	      else $cust_id_acc_flag = true;*/
      
       if($zf_filter_input->acct_no)
       {
             //integrate ke core
             //get number from ccy_id
             $ccy_id_num = Application_Helper_General::getCurrNum($zf_filter_input->ccy_id);
             $account    = new Account($zf_filter_input->acct_no,$ccy_id_num);
	         $result     = $account->accountInquiry();
	         //$result['checkBalanceStatus'] = '00';
	         
	         
             if($result['checkBalanceStatus'] != '00') //00 = success
             {
                 $cust_id_acc_flag = false;
                 $cust_id_acc_type = 1;
             }
             else
             {
                  $accData  = $this->getAccStatus($acct_no); 
                   
	              if(count($accData) > 0)
			      { 
			         $accStatus = $accData['ACCT_STATUS'];
			         $custDataBasedAcc = $this->getCustData($accData['CUST_ID']);
			       }
				  $accountType = $result['AccountType'];
			  }
         }
     
        
      if($cust_status == 1) $cust_id_flag = true;
      else $cust_id_flag = false;
      
  	 //validasi multiple email
      if($zf_filter_input->acct_email)
      {
		$validate = new validate;
      	$cek_multiple_email = $validate->isValidEmailMultiple($zf_filter_input->acct_email);
      }
      else
      {
      	$cek_multiple_email = true;
      }
      
      //----------END validasi tambahan-------------------
      
      if($zf_filter_input->isValid() &&  $cust_id_flag == true && $cust_id_acc_flag == true && $cek_multiple_email == true)
      { 
      
         $acct_email   = $zf_filter_input->acct_email;  
         $ccy_id       = $zf_filter_input->ccy_id;  
         $acct_no      = $zf_filter_input->acct_no;
         //get balance and owner from core
         //$acct_balance = $account->getAvailableBalance();
        // $acct_owner   = $account->getCoreAccountName();
         
         
        /*
      	$info = 'Account No = '.$zf_filter_input->acct_no;
        $acct_data = $this->_acctData;
                            
	    foreach($validators as $key=>$value)
	  	{
	  	  if($zf_filter_input->$key) $acct_data[strtoupper($key)] = $zf_filter_input->$key;
	  	}
	  	
	  	if($zf_filter_input->token_serialno)
	  	{ 
	  	   $user_data['USER_HASTOKEN'] = strtoupper($this->_masterhasStatus['code']['yes']); 
	  	}
	  	else
	  	{ 
	  	   $user_data['USER_HASTOKEN'] = strtoupper($this->_masterhasStatus['code']['no']); 
	  	}
	  	
	  	$acct_data['ACCT_STATUS'] = 1;

	  	
	   try 
	  	{  
		  $this->_db->beginTransaction();
		  
		  $acct_data['ACCT_STATUS'] = 1;
		  $acct_data['ACCT_SUGGESTED']   = new Zend_Db_Expr('now()');
		  $acct_data['ACCT_SUGGESTEDBY'] = $this->_userIdLogin;
		  
		  
		  $change_id = $this->suggestionWaitingApproval('customeraccount',$info,$this->_changeType['code']['new'],null,'M_CUSTOMER_ACCT','TEMP_CUSTOMER_ACCT','CUST_ID,ACCT_NO',$zf_filter_input->cust_id.','.$zf_filter_input->acct_no);
		  
		  $this->insertTempCustomerAcct($change_id,$acct_data);
		  
		  $this->_db->commit();
		  
		  $this->_redirect('/notification/success/index');
	  	}
		catch(Exception $e)
		{
		  $this->_db->rollBack();
		  $error_remark = $this->getErrorRemark('82');
		  SGO_Helper_GeneralLog::technicalLog($e);
		}*/
        
	     Application_Helper_General::writeLog('ACAD','');
	  	
	  	//jika berhasil ke layar confirm :
	  	$this->_redirect('/customeraccount/confirm/index/cust_id/'.$cust_id.'/acct_email/'.$acct_email.'/acct_no/'.$acct_no.'/ccy_id/'.$ccy_id);
	  	
	  	
		
        if(isset($error_remark))
        {
          //insert log
	      try 
	      {
	        $this->_db->beginTransaction();
	        $fulldesc = Application_Helper_General::displayFullDesc($_POST);
	        $this->backendLog(strtoupper($this->_changeType['code']['new']),strtoupper($this->_moduleID['user']),null,$fulldesc,$error_remark);
	        $this->_db->commit();
	      }
          catch(Exception $e)
          {
 	        $this->_db->rollBack();
	        SGO_Helper_GeneralLog::technicalLog($e);
	      }
	      
          $this->_helper->getHelper('FlashMessenger')->addMessage('F');
	      $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
		  $this->_redirect($this->_backURL);
        }
        else
        {
          $this->view->success = 1;
          $msg = $this->getErrorRemark('00','Customer Account',$zf_filter_input->acct_no);
          $this->view->user_msg = $msg;
        }
	  }
	  else
	  {
	  	$this->view->error = 1;
	  	$this->view->acct_no      = ($zf_filter_input->isValid('acct_no'))? $zf_filter_input->acct_no : $this->_getParam('acct_no'); 
		$this->view->ccy_id       = ($zf_filter_input->isValid('ccy_id'))? $zf_filter_input->ccy_id : $this->_getParam('ccy_id'); 
		$this->view->acct_email   = ($zf_filter_input->isValid('acct_email'))? $zf_filter_input->acct_email : $this->_getParam('acct_email');
		//$this->view->user_group = ($zf_filter_input->isValid('fgroup_id'))? $zf_filter_input->fgroup_id : $this->_getParam('fgroup_id');
       
        $error = $zf_filter_input->getMessages();
	  	/*if(count($error))$error_remark = $this->displayErrorRemark($error);
	  	$this->view->user_msg = $this->displayError($error);*/
	  	
	  	
	  	//format error utk ditampilkan di view html 
        $errorArray = null;
        foreach($error as $keyRoot => $rowError)
        {
           foreach($rowError as $errorString)
           {
              $errorArray[$keyRoot] = $errorString;
           }
        }
        
        //konfigurasi error cust id jika didelete
        if($cust_id_acc_flag == false)
        {
             if($cust_id_acc_type == 1)      
             {
               if($result['errorMessage']) $errorArray['acct_no'] = $result['errorMessage'];
               else $errorArray['acct_no'] = 'Error connection with Host';
             }
             //else if($cust_id_acc_type == 2) $errorArray['acct_no'] = 'NOTICE : This account is assigned (already deleted) to Company : ['. $custDataBasedAcc['CUST_NAME'] .' �('. $custDataBasedAcc['CUST_ID'] .')]';
        }
        
        if($cust_id_flag == false) 
        {
           if($cust_status == 3)       $errorArray['acct_no'] = 'Cannot add record, company already deleted';
           else if($cust_status == 2)  $errorArray['acct_no'] = 'Cannot add record, company already Suspended';
        }
        
        //$errorArray['acct_no'] = 'test';
       //Zend_Debug::dump($errorArray); //die;
        
        if(isSet($cek_multiple_email) && $cek_multiple_email == false) $errorArray['acct_email'] = 'Invalid format';
        
        $this->view->custAcct_msg  = $errorArray;
	  }
	  
  	}//if($this->_request->isPost())     
  	else
  	{
  	    $this->view->acct_no     = strip_tags( trim($this->_getParam('acct_no')) );
  	    $this->view->acct_email  = strip_tags( trim($this->_getParam('acct_email')) );
  	    
		$ccy_id = strip_tags( trim($this->_getParam('ccy_id')) );
		if(empty($ccy_id)) $ccy_id = $getCcy[0]['CCY_ID'];
		
		$this->view->ccy_id = $ccy_id;
  	    
  	}
    
  	
  	$select = $this->_db->select()
  	                       ->from('M_FGROUP',array('FGROUP_ID','FGROUP_NAME'))
  	                       ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id).' OR UPPER(CUST_ID)='.$this->_db->quote('BANK'))
  	                       ->where('UPPER(FGROUP_STATUS)='.$this->_db->quote(strtoupper($this->_masterStatus['code']['active'])));
    $this->view->fgroup = $this->_db->fetchAll($select);
    
    $select = $this->_db->select()
  	                       ->from('M_CUSTOMER',array('CUST_NAME'))
  	                       ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id));
    $this->view->cust_name = $this->_db->fetchOne($select);
  	
  	$this->view->cust_id = $cust_id;
    $this->view->modulename = $this->_request->getModuleName();
    
    //insert log
    try
	{
	  $this->_db->beginTransaction();
	  Application_Helper_General::writeLog('ACAD','');
	  $this->_db->commit();
	}
    catch(Exception $e)
    {
 	  $this->_db->rollBack();
	  SGO_Helper_GeneralLog::technicalLog($e);
	}
	
  }
  
  
}