<?php

/**
 * Tempcustomer model
 * 
 * @author 
 * @version
 */
require_once 'General/Account.php';

class Changemanagement_Model_Tempcustomeraccount extends Changemanagement_Model_Tempchanges
{
	protected $_moduleId = 'CST';
	/**
	 * Approve Changes for changes type: New
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function approveNew($actor = null)
	{
		//query from TEMP_CUSTOMER_ACCT
		$accts = $this->dbObj->select()
			->from('TEMP_CUSTOMER_ACCT')
			->where('CHANGES_ID = ?', $this->_changeId)
			->query()
			->fetch(Zend_Db::FETCH_ASSOC);

		if (!count($accts)) {
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Customer Accounts)';
			return false;
		}
		//var_dump($accts['ACCT_NO']);
		// if ($accts['ACCT_DESC'] == 'Deposito') {
		if ($accts['ACCT_TYPE'] == 'T' || $accts['ACCT_TYPE'] == '30') {

			$svcAccountBalance = new Service_Account($accts['ACCT_NO'], null, null, null, null, null);
			$result = $svcAccountBalance->inquiryDeposito();
		} else {
			$svcAccountBalance = new Service_Account($accts['ACCT_NO'], null, null, null, null, null);
			$result = $svcAccountBalance->inquiryAccountBalance();
		}


		if ($result['response_desc'] == 'Success') {
			//==========================================================================================================
			//insert to master table M_CUSTOMER
			$insertArr = array_diff_key($accts, array('TEMP_ID' => '', 'CHANGES_ID' => ''));

			$insertArr['ACCT_CREATED']     = new Zend_Db_Expr('now()');
			$insertArr['ACCT_CREATEDBY']   = $actor;
			$insertArr['ACCT_UPDATED']     = new Zend_Db_Expr('now()');
			$insertArr['ACCT_UPDATEDBY']   = $actor;

			if (trim($insertArr['CCY_ID']) == '') {
				$insertArr['CCY_ID'] = 'IDR';
			}

			$acctInsert = $this->dbObj->insert('M_CUSTOMER_ACCT', $insertArr);
			if (!(bool)$acctInsert) {
				$this->_errorCode = '82';
				$this->_errorMsg = 'Query failed(Customer Accounts)';
				return false;
			}


			$deleteChanges  = $this->deleteNew();
		} else {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Inquiry failed(Customer Accounts)';
			return false;
		}

		if (!$deleteChanges)
			return false;


		return true;
	}

	/**
	 * Approve Changes for changes type: Edit
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function approveEdit($actor = null)
	{

		$transFailed = false;
		//query from TEMP_CUSTOMER_ACCT
		$customerAcct = $this->dbObj->select()
			->from('TEMP_CUSTOMER_ACCT')
			->where('CHANGES_ID = ?', $this->_changeId)
			->query()
			->fetch(Zend_Db::FETCH_ASSOC);
		if (!count($customerAcct)) {
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Temp Customer Account)';
			return false;
		}


		//query from M_CUSTOMER_ACCT
		$m_customerAcct = $this->dbObj->select()
			->from('M_CUSTOMER_ACCT', array('ACCT_STATUS', 'CUST_ID', 'ACCT_CREATED', 'ACCT_CREATEDBY'))
			->where('ACCT_NO=' . $this->dbObj->quote((string)$customerAcct['ACCT_NO']))
			->where('CUST_ID=' . $this->dbObj->quote((string)$customerAcct['CUST_ID']))
			->query()
			->fetch(Zend_Db::FETCH_ASSOC);
		if (!count($m_customerAcct)) {
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Customer Account)';
			return false;
		}


		//tipe : mirip seperti add, jika acct status deleted di master maka:
		if ($m_customerAcct['ACCT_STATUS'] == 3)   //3 = deleted
		{
			$updateArr = array_diff_key($customerAcct, array('TEMP_ID' => '', 'CHANGES_ID' => ''));

			$updateArr['ACCT_ALIAS_NAME']  = $customerAcct['ACCT_ALIAS_NAME'];

			if ($m_customerAcct['CUST_ID'] != $customerAcct['CUST_ID']) {
				$updateArr['ACCT_CREATED']     = new Zend_Db_Expr('now()');
				$updateArr['ACCT_CREATEDBY']   = $actor;
			} else {
				//jika cust id nya sama maka acct created dan created by ikut data master cust id yg lama
				$updateArr['ACCT_CREATED']     = $m_customerAcct['ACCT_CREATED'];
				$updateArr['ACCT_CREATEDBY']   = $m_customerAcct['ACCT_CREATEDBY'];
			}
		} else {
			//update record customer
			//$updateArr = array_diff_key($customerAcct,array('TEMP_ID'=>'','CHANGES_ID'=>'','CUST_ID'=>'','ACCT_NO'=>'','CCY_ID'=>''));
			$updateArr['ACCT_EMAIL']        = $customerAcct['ACCT_EMAIL'];
			$updateArr['ACCT_SUGGESTED']    = $customerAcct['ACCT_SUGGESTED'];
			$updateArr['ACCT_SUGGESTEDBY']  = $customerAcct['ACCT_SUGGESTEDBY'];
		}

		$updateArr['ACCT_UPDATED']      = new Zend_Db_Expr('now()');
		$updateArr['ACCT_UPDATEDBY']    = $actor;


		//Zend_Debug::dump($updateArr); die;


		$whereArr = array('ACCT_NO = ?'=>(string)$customerAcct['ACCT_NO'],
		                  'CUST_ID = ?'=>(string)$customerAcct['CUST_ID']);
		//$whereArr = array('ACCT_NO = ?' => (string)$customerAcct['ACCT_NO']);
		$customerupdate = $this->dbObj->update('M_CUSTOMER_ACCT', $updateArr, $whereArr);


		if (!(bool)$customerupdate) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'update failed(Customer Account)';
			return false;
		}

		$deleteChanges  = $this->deleteEdit();
		if (!$deleteChanges)
			return false;

		return true;
	}


	/**
	 * Approve Changes for changes type: Activate
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function approveUnsuspend($actor = null)
	{

		//query from TEMP_CUSTOMER
		$customerAcct = $this->dbObj->select()
			->from('TEMP_CUSTOMER_ACCT')
			->where('CHANGES_ID = ?', $this->_changeId)
			->query()
			->fetch(Zend_Db::FETCH_ASSOC);
		if (!count($customerAcct)) {
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Customer Account)';
			return false;
		}


		$updateArr['ACCT_STATUS']      = 1;
		$updateArr['ACCT_SUGGESTED']   = $customerAcct['ACCT_SUGGESTED'];
		$updateArr['ACCT_SUGGESTEDBY'] = $customerAcct['ACCT_SUGGESTEDBY'];
		$updateArr['ACCT_UPDATED']     = new Zend_Db_Expr('now()');
		$updateArr['ACCT_UPDATEDBY']   = $actor;


		//$updateArr['LASTUPDATED'] = new Zend_Db_Expr('now()');
		$whereArr = array(
			'CUST_ID = ?' => (string)$customerAcct['CUST_ID'],
			'ACCT_NO = ?' => (string)$customerAcct['ACCT_NO']
		);

		$customerupdate = $this->dbObj->update('M_CUSTOMER_ACCT', $updateArr, $whereArr);

		if (!(bool)$customerupdate) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Customer Account)';
			return false;
		}

		/*
		//update all users status
		$updateArr = array('USER_STATUS' => 'A');
		$updateArr['LASTUPDATED'] = new Zend_Db_Expr('now()');
		$userupdate = $this->dbObj->update('M_USER',$updateArr,$whereArr);
		if(!(boolean)$userupdate) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(User)';
			return false;
		}
		*/

		$deleteChanges  = $this->deleteUnsuspend();
		if (!$deleteChanges)
			return false;

		return true;
	}


	/**
	 * Approve Changes for changes type: Deactivate
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function approveSuspend($actor = null)
	{

		//query from TEMP_CUSTOMER
		$customerAcct = $this->dbObj->select()
			->from('TEMP_CUSTOMER_ACCT')
			->where('CHANGES_ID = ?', $this->_changeId)
			->query()
			->fetch(Zend_Db::FETCH_ASSOC);
		if (!count($customerAcct)) {
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Customer Account)';
			return false;
		}

		//update customer account status		  
		$updateArr['ACCT_STATUS']      = 2;
		$updateArr['ACCT_SUGGESTED']   = $customerAcct['ACCT_SUGGESTED'];
		$updateArr['ACCT_SUGGESTEDBY'] = $customerAcct['ACCT_SUGGESTEDBY'];
		$updateArr['ACCT_UPDATED']     = new Zend_Db_Expr('now()');
		$updateArr['ACCT_UPDATEDBY']   = $actor;

		$whereArr = array(
			'CUST_ID = ?' => (string)$customerAcct['CUST_ID'],
			'ACCT_NO = ?' => (string)$customerAcct['ACCT_NO']
		);

		$customerupdate = $this->dbObj->update('M_CUSTOMER_ACCT', $updateArr, $whereArr);

		if (!(bool)$customerupdate) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Customer Account)';
			return false;
		}



		$deleteChanges  = $this->deleteSuspend();
		if (!$deleteChanges)
			return false;

		return true;
	}

	public function approveDelete($actor = null)
	{
		//query from TEMP_CUSTOMER
		$customerAcct = $this->dbObj->select()
			->from('TEMP_CUSTOMER_ACCT')
			->where('CHANGES_ID = ?', $this->_changeId)
			->query()
			->fetch(Zend_Db::FETCH_ASSOC);
		if (!count($customerAcct)) {
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Customer Account)';
			return false;
		}


		$updateArr = array('ACCT_STATUS' => 3);
		$updateArr['ACCT_SUGGESTED']   = $customerAcct['ACCT_SUGGESTED'];
		$updateArr['ACCT_SUGGESTEDBY'] = $customerAcct['ACCT_SUGGESTEDBY'];
		$updateArr['ACCT_UPDATED']     = new Zend_Db_Expr('now()');
		$updateArr['ACCT_UPDATEDBY']   = $actor;




		$whereArr = array(
			'CUST_ID = ?' => (string)$customerAcct['CUST_ID'],
			'ACCT_NO = ?' => (string)$customerAcct['ACCT_NO']
		);

		$customerupdate = $this->dbObj->update('M_CUSTOMER_ACCT', $updateArr, $whereArr);

		if (!(bool)$customerupdate) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Customer Account)';
			return false;
		}


		//DELETE USER LIMIT
		$updateuserlimitArr = array('MAKERLIMIT_STATUS' => 3);

		$userlimitupdate = $this->dbObj->update('M_MAKERLIMIT', $updateuserlimitArr, $whereArr);
		//	if(!(boolean)$userlimitupdate) 
		//	{
		//		$this->_errorCode = '82';
		//		$this->_errorMsg = 'Query failed(Maker Limit)';
		//		return false;
		//	}
		//END DELETE USER LIMIT
		// BUGS DELETE ACCOUNT JIKA ACCOUNT BLM DISET USER LIMIT BY MELANIE ('BUG PPC 63')

		$deleteChanges  = $this->deleteUnsuspend();
		if (!$deleteChanges)
			return false;

		return true;
	}



	/**
	 * Delete Changes for changes type: New
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function deleteNew()
	{

		//delete from TEMP_CUSTOMER
		//$customerdelete = $this->dbObj->delete('TEMP_CUSTOMER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		//		if(!(boolean)$customerdelete) {
		//			$this->_errorCode = '82';
		//			$this->_errorMsg = 'Query failed(Customer)';
		//			return false;
		//		}

		//delete from TEMP_USER
		//$userdelete = $this->dbObj->delete('TEMP_USER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		//	    if(!(boolean)$userdelete) {
		//			$this->_errorCode = '82';
		//			$this->_errorMsg = 'Query failed(User)';
		//			return false;
		//		}

		//delete from TEMP_CUSTOMER_ACCT
		$acctdelete = $this->dbObj->delete('TEMP_CUSTOMER_ACCT', $this->dbObj->quoteInto('CHANGES_ID = ?', $this->_changeId));
		//	    if(!(boolean)$acctdelete) {
		//			$this->_errorCode = '82';
		//			$this->_errorMsg = 'Query failed(Customer Account)';
		//			return false;
		//		}

		//delete from TEMP_CUSTOMER_LIMIT
		//$limitdelete = $this->dbObj->delete('TEMP_CUSTOMER_LIMIT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		//	    if(!(boolean)$limitdelete) {
		//			$this->_errorCode = '82';
		//			$this->_errorMsg = 'Query failed(Customer Limit)';
		//			return false;
		//		}

		//delete from TEMP_FGROUP
		//$schemedelete = $this->dbObj->delete('TEMP_SCHEME',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));

		return true;
	}

	/**
	 * Delete Changes for changes type: Edit
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function deleteEdit()
	{

		//delete from TEMP_CUSTOMER
		//$customerdelete = $this->dbObj->delete('TEMP_CUSTOMER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		//		if(!(boolean)$customerdelete) {
		//			$this->_errorCode = '82';
		//			$this->_errorMsg = 'Query failed(Customer)';
		//			return false;
		//		}

		//delete from TEMP_CUSTOMER_ACCT
		$acctdelete = $this->dbObj->delete('TEMP_CUSTOMER_ACCT', $this->dbObj->quoteInto('CHANGES_ID = ?', $this->_changeId));
		//	    if(!(boolean)$acctdelete) {
		//			$this->_errorCode = '82';
		//			$this->_errorMsg = 'Query failed(Customer Account)';
		//			return false;
		//		}

		//delete from TEMP_CUSTOMER_LIMIT
		//$limitdelete = $this->dbObj->delete('TEMP_CUSTOMER_LIMIT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		//	    if(!(boolean)$limitdelete) {
		//			$this->_errorCode = '82';
		//			$this->_errorMsg = 'Query failed(Customer Limit)';
		//			return false;
		//		}

		//delete from TEMP_FGROUP
		//$schemedelete = $this->dbObj->delete('TEMP_SCHEME',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));

		return true;
	}

	/**
	 * Delete Changes for changes type: Activate
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function deleteUnsuspend()
	{

		//delete from TEMP_CUSTOMER
		$customerdelete = $this->dbObj->delete('TEMP_CUSTOMER_ACCT', $this->dbObj->quoteInto('CHANGES_ID = ?', $this->_changeId));
		//		if(!(boolean)$customerdelete) {
		//			$this->_errorCode = '82';
		//			$this->_errorMsg = 'Query failed(Customer)';
		//			return false;
		//		}

		return true;
	}

	/**
	 * Delete Changes for changes type: Deactivate
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function deleteSuspend()
	{

		//delete from TEMP_CUSTOMER
		$customerdelete = $this->dbObj->delete('TEMP_CUSTOMER_ACCT', $this->dbObj->quoteInto('CHANGES_ID = ?', $this->_changeId));
		//		if(!(boolean)$customerdelete) {
		//			$this->_errorCode = '82';
		//			$this->_errorMsg = 'Query failed(Customer)';
		//			return false;
		//		}

		return true;
	}

	public function deleteDelete()
	{
		$customerdelete = $this->dbObj->delete('TEMP_CUSTOMER_ACCT', $this->dbObj->quoteInto('CHANGES_ID = ?', $this->_changeId));

		return true;
	}

	public function approveDeactivate()
	{
	}

	public function approveActivate()
	{
	}

	public function deleteDeactivate()
	{
	}

	public function deleteActivate()
	{
	}
}
