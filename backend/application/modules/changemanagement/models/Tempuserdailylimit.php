<?php
/**
 * Tempcustomer model
 * 
 * @author 
 * @version
 */

class Changemanagement_Model_Tempuserdailylimit extends Changemanagement_Model_Tempchanges 
{
	protected $_moduleId = 'CST';
	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function approveNew($actor = null) 
	{
	
		//query from TEMP_CUSTOMER_ACCT
		$dailyLimit = $this->dbObj->select()
						  			->from('TEMP_DAILYLIMIT')
						  			->where('CHANGES_ID = ?',$this->_changeId)
						  			->query()
						  			->fetch(Zend_Db::FETCH_ASSOC);
        if(!count($dailyLimit))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(User Daily Limit)';
			return false;
        }
	
	    //==========================================================================================================
		//insert to master table M_CUSTOMER

		//check if available but deleted
		$check = $this->dbObj->select()
							->from('M_DAILYLIMIT',array('USER_LOGIN'))
							->where('USER_LOGIN = ?', (string)$dailyLimit['USER_LOGIN'])
							->where('CUST_ID = ?', (string)$dailyLimit['CUST_ID'])
							->where('CCY_ID = ?', (string)$dailyLimit['CCY_ID'])
							->where('DAILYLIMIT_STATUS = ?', '3');
		$checkdata = $this->dbObj->fetchOne($check);

		if($checkdata){
			$updateArr = array_diff_key($dailyLimit,array('TEMP_ID'=>'','CHANGES_ID'=>'','CUST_ID'=>'','USER_LOGIN'=>'','CCY_ID'=>''));
			$updateArr['UPDATED']     = new Zend_Db_Expr('now()');
			$updateArr['UPDATEDBY']   = $actor;
			
			
			
			$whereArr = array('CUST_ID = ?' =>(string)$dailyLimit['CUST_ID'],
			                  'USER_LOGIN = ?' =>(string)$dailyLimit['USER_LOGIN'],
							  'CCY_ID  = ?' =>(string)$dailyLimit['CCY_ID']);
			
			
		
			$dailyLimitInsert = $this->dbObj->update('M_DAILYLIMIT',$updateArr,$whereArr);
		}
		else{
			$insertArr = array_diff_key($dailyLimit,array('TEMP_ID'=>'','CHANGES_ID'=>''));
			$insertArr['CREATED']     = new Zend_Db_Expr('now()');
			$insertArr['CREATEDBY']   = $actor;
			$insertArr['UPDATED']     = new Zend_Db_Expr('now()');
			$insertArr['UPDATEDBY']   = $actor;
			
			
			$dailyLimitInsert = $this->dbObj->insert('M_DAILYLIMIT',$insertArr);
		}
		
		if(!(boolean)$dailyLimitInsert) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(User Daily Limit)';
			return false;
		}
				
		
		$deleteChanges  = $this->deleteNew();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	/**
     * Approve Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function approveEdit($actor = null) 
	{
		
		$transFailed = false;
		//query from TEMP_CUSTOMER
		$dailyLimit = $this->dbObj->select()
						   ->from('TEMP_DAILYLIMIT')
						   ->where('CHANGES_ID = ?',$this->_changeId)
						   ->query()
						   ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($dailyLimit))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(User Daily Limit)';
			return false;
        }
        
        //update record customer
		$updateArr = array_diff_key($dailyLimit,array('TEMP_ID'=>'','CHANGES_ID'=>'','CUST_ID'=>'','USER_LOGIN'=>'','CCY_ID'=>'','DAILYLIMIT_STATUS'=>'','CREATED'=>'','CREATEDBY'=>''));
		$updateArr['UPDATED']     = new Zend_Db_Expr('now()');
		$updateArr['UPDATEDBY']   = $actor;
		
		
		
		$whereArr = array('CUST_ID = ?' =>(string)$dailyLimit['CUST_ID'],
		                  'USER_LOGIN = ?' =>(string)$dailyLimit['USER_LOGIN'],
						  'CCY_ID  = ?' =>(string)$dailyLimit['CCY_ID']);
		
		
	
		$dailylimitupdate = $this->dbObj->update('M_DAILYLIMIT',$updateArr,$whereArr);
		
		
		if(!(boolean)$dailylimitupdate) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(User Daily Limit)';
			return false;
		}
		
		$deleteChanges  = $this->deleteEdit();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveActivate($actor = null) {
	
		//query from TEMP_CUSTOMER
		$customerAcct = $this->dbObj->select()
						   		 ->from('TEMP_CUSTOMER_ACCT')
						  		 ->where('CHANGES_ID = ?',$this->_changeId)
						  	     ->query()
						  	     ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($customerAcct))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Customer Account)';
			return false;
        }
						  
		$updateArr = array('ACCT_STATUS' => 1);
		//$updateArr['LASTUPDATED'] = new Zend_Db_Expr('now()');
		$whereArr = array('CUST_ID = ?'=>(string)$customerAcct['CUST_ID'],
		                  'ACCT_NO = ?'=>(string)$customerAcct['ACCT_NO']);
		
		$customerupdate = $this->dbObj->update('M_CUSTOMER_ACCT',$updateArr,$whereArr);
		
		if(!(boolean)$customerupdate) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Customer Account)';
			return false;
		}

		/*
		//update all users status
		$updateArr = array('USER_STATUS' => 'A');
		$updateArr['LASTUPDATED'] = new Zend_Db_Expr('now()');
		$userupdate = $this->dbObj->update('M_USER',$updateArr,$whereArr);
		if(!(boolean)$userupdate) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(User)';
			return false;
		}
		*/
		
		$deleteChanges  = $this->deleteActivate();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	
	/**
     * Approve Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveDeactivate($actor = null) {
	
		//query from TEMP_CUSTOMER
		$customerAcct = $this->dbObj->select()
						  ->from('TEMP_CUSTOMER_ACCT')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($customerAcct ))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Customer Account)';
			return false;
        }
		
        //update customer account status		  
		$updateArr = array('ACCT_STATUS' => 2);
		//$updateArr['LASTUPDATED'] = new Zend_Db_Expr('now()');
		$whereArr = array('CUST_ID = ?'=>(string)$customerAcct['CUST_ID'],
		                  'ACCT_NO = ?'=>(string)$customerAcct['ACCT_NO']);
		
		$customerupdate = $this->dbObj->update('M_CUSTOMER_ACCT',$updateArr,$whereArr);
		
		if(!(boolean)$customerupdate) 
		{
			$this->_errorCode = '82';
		    $this->_errorMsg = 'Query failed(Customer Account)';
			return false;
		}
		
		
		
		$deleteChanges  = $this->deleteDeactivate();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	public function approveDelete($actor = null) 
	{
		//query from TEMP_CUSTOMER
		$dailylimit = $this->dbObj->select()
						   		  ->from('TEMP_DAILYLIMIT')
						  		  ->where('CHANGES_ID = ?',$this->_changeId)
						  	      ->query()
						  	      ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($dailylimit))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(User Daily Limit)';
			return false;
        }
						  
		$updateArr = array('DAILYLIMIT_STATUS' => 3);
		$updateArr['UPDATED']      = new Zend_Db_Expr('now()');
		$updateArr['UPDATEDBY']    = $actor;
		$updateArr['SUGGESTED']    = $dailylimit['SUGGESTED'];
		$updateArr['SUGGESTEDBY']  = $dailylimit['SUGGESTEDBY'];
		
		$whereArr = array('CUST_ID = ?'     => (string)$dailylimit['CUST_ID'],
		                  'USER_LOGIN = ?'  => (string)$dailylimit['USER_LOGIN'],
		                  'CCY_ID  = ?'     => (string)$dailylimit['CCY_ID']);
		
		$customerupdate = $this->dbObj->update('M_DAILYLIMIT',$updateArr,$whereArr);
		
		if(!(boolean)$customerupdate) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(User Daily Limit)';
			return false;
		}

		
		$deleteChanges  = $this->deleteDelete();
		if(!$deleteChanges)
			    return false;
		
		return true;
	}
	
	
	
	/**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew() 
	{

		//delete from TEMP_CUSTOMER
		//$customerdelete = $this->dbObj->delete('TEMP_CUSTOMER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$customerdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer)';
//			return false;
//		}
		
		//delete from TEMP_USER
		//$userdelete = $this->dbObj->delete('TEMP_USER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//	    if(!(boolean)$userdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(User)';
//			return false;
//		}
		
		//delete from TEMP_CUSTOMER_ACCT
		$acctdelete = $this->dbObj->delete('TEMP_DAILYLIMIT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//	    if(!(boolean)$acctdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer Account)';
//			return false;
//		}

		//delete from TEMP_CUSTOMER_LIMIT
		//$limitdelete = $this->dbObj->delete('TEMP_CUSTOMER_LIMIT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//	    if(!(boolean)$limitdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer Limit)';
//			return false;
//		}

		//delete from TEMP_FGROUP
		//$schemedelete = $this->dbObj->delete('TEMP_SCHEME',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		
		return true;
	}
	
	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteEdit() {

		//delete from TEMP_CUSTOMER
		//$customerdelete = $this->dbObj->delete('TEMP_CUSTOMER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$customerdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer)';
//			return false;
//		}
		
		//delete from TEMP_CUSTOMER_ACCT
		$acctdelete = $this->dbObj->delete('TEMP_DAILYLIMIT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//	    if(!(boolean)$acctdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer Account)';
//			return false;
//		}

		//delete from TEMP_CUSTOMER_LIMIT
		//$limitdelete = $this->dbObj->delete('TEMP_CUSTOMER_LIMIT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//	    if(!(boolean)$limitdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer Limit)';
//			return false;
//		}

		//delete from TEMP_FGROUP
		//$schemedelete = $this->dbObj->delete('TEMP_SCHEME',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		
		return true;
	}
	
	/**
     * Delete Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteActivate() {

		//delete from TEMP_CUSTOMER
		$customerdelete = $this->dbObj->delete('TEMP_CUSTOMER_ACCT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$customerdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer)';
//			return false;
//		}
		
		return true;
	}
	
	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteDeactivate() {

		//delete from TEMP_CUSTOMER
		$customerdelete = $this->dbObj->delete('TEMP_CUSTOMER_ACCT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$customerdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Customer)';
//			return false;
//		}
		
		return true;
	}
	
	public function deleteDelete() 
	{
		$customerdelete = $this->dbObj->delete('TEMP_DAILYLIMIT',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));

		return true;
		
		
	}
}