<?php
/**
 * Tempcustomer model
 * 
 * @author 
 * @version
 */

class Changemanagement_Model_Tempbillercharges extends Changemanagement_Model_Tempchanges 
{
	protected $_moduleId = 'CST';
	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function approveNew($actor = null) 
	{
		//query from T_GLOBAL_CHANGES
		$globalChanges = $this->dbObj->select()
						  			->from('T_GLOBAL_CHANGES')
						  			->where('CHANGES_ID = ?',$this->_changeId)
						  			->query()
						  			->fetch(Zend_Db::FETCH_ASSOC);
									
		//query from TEMP_CHARGES_PROVIDER
		$billerCharges = $this->dbObj->select()
						  			->from('TEMP_CHARGES_PROVIDER')
						  			->where('CHANGES_ID = ?',$this->_changeId)
						  			->query()
						  			->fetch(Zend_Db::FETCH_ASSOC);
        if(!count($billerCharges))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Biller Charges)';
			return false;
        }
	
	    //==========================================================================================================
		//insert to master table M_CHARGES_PROVIDER
		$insertArr = array_diff_key($billerCharges,array('TEMP_ID'=>'','CHANGES_ID'=>''));

		// $insertArr['CHARGES_SUGGESTED'] = new Zend_Db_Expr('GETDATE()');
		// $insertArr['CHARGES_SUGGESTEDBY']   = $actor;
		$insertArr['CHARGES_UPDATED'] = new Zend_Db_Expr('GETDATE()');
		$insertArr['CHARGES_UPDATEDBY']   = $actor;
		Zend_Debug::dump($insertArr);die;
		$billerChargesInsert = $this->dbObj->insert('M_CHARGES_PROVIDER',$insertArr);
		if(!(boolean)$billerChargesInsert) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Biller Charges)';
			return false;
		}
				
		
		$deleteChanges  = $this->deleteNew();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	/**
     * Approve Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function approveEdit($actor = null) 
	{
		
		//query from TEMP_CHARGES_PROVIDER
		$billerCharges = $this->dbObj->select()
						   ->from('TEMP_CHARGES_PROVIDER')
						   ->where('CHANGES_ID = ?',$this->_changeId)
						   ->query()
						   ->fetchAll(Zend_Db::FETCH_ASSOC);
		if(!count($billerCharges))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Biller Charges)';
			return false;
        }
        	
		foreach($billerCharges as $charges)
		{
			$cek = $this->dbObj->fetchOne(
						$this->dbObj->select()
							->from(array('B' => 'M_CHARGES_PROVIDER'),array('B.PROVIDER_ID'))
							->where("B.PROVIDER_ID = ?", $charges['PROVIDER_ID'])
							//->where("B.CHARGES_CCY = ?", $this->dbObj->quote((string)$charges['CHARGES_CCY']))
							->where("B.CHARGES_CCY = ?", (string)$charges['CHARGES_CCY'])
					);
					
			if($cek)
			{
				//update master table M_CHARGES_PROVIDER
				$updateArr = array(
								'CHARGES_AMT' 		=> $charges['CHARGES_AMT'],
								'CHARGES_TO' 		=> $charges['CHARGES_TO'],
								'CHARGES_ACCT' 		=> $charges['CHARGES_ACCT'],
								'CHARGES_SUGGESTED' 		=> $charges['CHARGES_SUGGESTED'],
								'CHARGES_SUGGESTEDBY' 		=> $charges['CHARGES_SUGGESTEDBY'],
								'CHARGES_CREATED' 	=> new Zend_Db_Expr('GETDATE()'),
								'CHARGES_CREATEDBY' => $actor,
								'CHARGES_UPDATED' 	=> new Zend_Db_Expr('GETDATE()'),
								'CHARGES_UPDATEDBY' => $actor
							);
				
				$whereArr = array(
								'PROVIDER_ID = ?' 	=> $charges['PROVIDER_ID'],
								'CHARGES_CCY = ?'  	=> (string)$charges['CHARGES_CCY']
							);
				
				$query = $this->dbObj->update('M_CHARGES_PROVIDER',$updateArr,$whereArr);
			}
			else
			{
				//insert master table M_CHARGES_PROVIDER
				$insertArr = array_diff_key($charges,array('TEMP_ID'=>'','CHANGES_ID'=>''));
				
				$insertArr['CHARGES_CREATED']     = new Zend_Db_Expr('GETDATE()');
				$insertArr['CHARGES_CREATEDBY']   = $actor;
				$insertArr['CHARGES_UPDATED']     = new Zend_Db_Expr('GETDATE()');
				$insertArr['CHARGES_UPDATEDBY']   = $actor;
				
				$query = $this->dbObj->insert('M_CHARGES_PROVIDER',$insertArr);
			}
		}
		
		if(!(boolean)$query) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Biller Charges)';
			return false;
		}
		
		$deleteChanges  = $this->deleteEdit();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveActivate($actor = null) {
		
		return true;
	}
	
	
	/**
     * Approve Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveDeactivate($actor = null) {
		
		return true;
	}
	
	public function approveDelete() 
	{
		//query from TEMP_CHARGES_PROVIDER
		$dailylimit = $this->dbObj->select()
						   		  ->from('TEMP_CHARGES_PROVIDER')
						  		  ->where('CHANGES_ID = ?',$this->_changeId)
						  	      ->query()
						  	      ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($dailylimit))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Biller Charges)';
			return false;
        }
						  
		$updateArr = array('DAILYLIMIT_STATUS' => 3);
		
		//$updateArr['LASTUPDATED'] = new Zend_Db_Expr('GETDATE()');
		$whereArr = array('USER_ID = ?'     => (string)$dailylimit['USER_ID'],
		                  'USER_LOGIN = ?'  => (string)$dailylimit['USER_LOGIN'],
		                  'CCY_ID  = ?'     => (string)$dailylimit['CCY_ID']);
		
		$customerupdate = $this->dbObj->update('M_CHARGES_PROVIDER',$updateArr,$whereArr);
		
		if(!(boolean)$customerupdate) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Biller Charges)';
			return false;
		}

		
		$deleteChanges  = $this->deleteDelete();
		if(!$deleteChanges)
			    return false;
		
		return true;
	}
	
	
	
	/**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew() 
	{
		//delete from TEMP_CHARGES_PROVIDER
		$acctdelete = $this->dbObj->delete('TEMP_CHARGES_PROVIDER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		return true;
	}
	
	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteEdit() {
		//delete from TEMP_CHARGES_PROVIDER
		$acctdelete = $this->dbObj->delete('TEMP_CHARGES_PROVIDER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		return true;
	}
	
	/**
     * Delete Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteActivate() {

		//delete from TEMP_CHARGES_PROVIDER
		$customerdelete = $this->dbObj->delete('TEMP_CHARGES_PROVIDER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		return true;
	}
	
	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteDeactivate() {

		//delete from TEMP_CHARGES_PROVIDER
		$customerdelete = $this->dbObj->delete('TEMP_CHARGES_PROVIDER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		return true;
	}
	
	public function deleteDelete() 
	{
		$customerdelete = $this->dbObj->delete('TEMP_CHARGES_PROVIDER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		return true;
	}
}