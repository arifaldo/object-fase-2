<?php
/**
 * Temprootcommunity model
 * 
 * @author
 * @version
 */

class Changemanagement_Model_Temprealtimecharges extends Changemanagement_Model_Tempchanges {
	protected $_moduleId = 'BGR';
	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function approveNew($actor = null) {
		
	}
	
	/**
     * Approve Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */ 
	public function approveEdit($actor = null) {
		//query from TEMP_ROOT_COMM
		$select = $this->dbObj->fetchAll(
									$this->dbObj->select()
									 ->from('TEMP_CHARGES_WITHIN')
									 ->where('CHANGES_ID = ?',$this->_changeId)
									 );
									 
        $select2 = $this->dbObj->fetchRow(
									$this->dbObj->select()
									 ->from('T_GLOBAL_CHANGES')
									 ->where('CHANGES_ID = ?',$this->_changeId)
									 );
		$custid = $select2['COMPANY_CODE'];
     
        //$whereArr = array('BGROUP_ID = ?'=>$backendGroup['BGROUP_ID']);
       $whereArr = array('CUST_ID =?'			=>	$custid,
       					 'BUSINESS_TYPE =?'		=>	'1'); 
       //Zend_Debug::dump($commid); die;
       $assign = $this->dbObj->delete('M_CHARGES_WITHIN',$whereArr);
	   //die;
	   //echo '<pre>';
       if($select){
	       	//insert to master table M_BPRIVI_GROUP
	       	foreach ($select as $list) {
	       		$insertArr = array_diff_key($list,array('TEMP_ID'=>'','CHANGES_ID'=>''));
	       		$insertArr['CHARGES_APPROVED'] = new Zend_Db_Expr('now()');
				$insertArr['CHARGES_APPROVEDBY'] = $actor;
				$insertArr['Column 8'] = '-';
				//var_dump($insertArr);
				$assign = $this->dbObj->insert('M_CHARGES_WITHIN',$insertArr);
	       	}
			
       }

       $select = $this->dbObj->fetchAll(
									$this->dbObj->select()
									 ->from('TEMP_CHARGES_OTHER')
									 ->where('CHANGES_ID = ?',$this->_changeId)
									 );
									 
        $select2 = $this->dbObj->fetchRow(
									$this->dbObj->select()
									 ->from('T_GLOBAL_CHANGES')
									 ->where('CHANGES_ID = ?',$this->_changeId)
									);
		$custid = $select2['COMPANY_CODE'];
	
       $whereArr = array('CUST_ID =?'			=>	$custid,
       					 'CHARGES_TYPE =?'		=>	'1'); 
       //Zend_Debug::dump($commid); die;
       $assign = $this->dbObj->delete('M_CHARGES_OTHER',$whereArr);
       $whereArr = array('CUST_ID =?'			=>	$custid,
       					 'CHARGES_TYPE =?'		=>	'2'); 
       //Zend_Debug::dump($commid); die;
       $assign = $this->dbObj->delete('M_CHARGES_OTHER',$whereArr);
	   
	   $whereArr = array('CUST_ID =?'			=>	$custid,
       					 'CHARGES_TYPE =?'		=>	'8'); 
       //Zend_Debug::dump($commid); die;
       $assign = $this->dbObj->delete('M_CHARGES_OTHER',$whereArr);
	   //die; 
       if($select){
	       	foreach ($select as $list) {
	       		$insertArr = array_diff_key($list,array('TEMP_ID'=>'','CHANGES_ID'=>''));
				$insertArr['CHARGES_APPROVED'] = new Zend_Db_Expr('now()');
				$insertArr['CHARGES_APPROVEDBY'] = $actor;
				if(empty($insertArr['CHARGES_NO'])){
					$insertArr['CHARGES_NO'] = '1';
				}
				//var_dump($insertArr);
				
				$assign = $this->dbObj->insert('M_CHARGES_OTHER',$insertArr);
	       	}
			
       }
		
		//die('here');
		$deleteChanges  = $this->deleteEdit();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveActivate() {
		
	}
	
	/**
     * Approve Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveDeactivate() {
		
	}
	
	public function approveDelete() {
		//will never be called
	}
	
	
/**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew() {

	}
	
	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteEdit($actor = null) {

		//delete from TEMP_ROOT_COMM
		$assigndelete = $this->dbObj->delete('TEMP_CHARGES_WITHIN',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		$assigndelete = $this->dbObj->delete('TEMP_CHARGES_OTHER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$rootcommunitydelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Backend Group)';
//			return false;
//		}
		
		return true;
	}
	
/**
     * Delete Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteActivate() {

	}
	
	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteDeactivate() {

	}
	
	public function deleteDelete() {
		//will never be called
	}
}