<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class nationalpoolingchanges_DetailController extends Application_Main 
{

    protected $_moduleDB = 'RTF'; // masih harus diganti
    
    public function indexAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->systemType = $system_type;

        $np = $this->_getParam("np");
        $csv = $this->_getParam('csv');

        if($np){
            $npdata = $this->_db->select()
                                    ->from(array('A' => 'TEMP_NOTIONAL_POOLING'),array('*'))
                                    ->join(array('B' => 'M_BUSER'), 'B.BUSER_ID = A.N_CREATEDBY',array('B.BUSER_NAME'))
                                    ->join(array('C' => 'T_GLOBAL_CHANGES'), 'C.CHANGES_ID = A.CHANGES_ID',array('C.CHANGES_INFORMATION'))
                                    ->join(array('D' => 'T_FILE_SUBMIT'), 'D.FILE_ID = A.N_UD_ID',array('D.DOCUMENT_NUMBER'))
                                    ->where('A.N_NUMBER = ?',$np)
                                    ->query()->fetchAll();

            if($npdata){
                $data = $npdata[0];

                $selectcomp = $this->_db->select()
                                    ->from(array('A' => 'M_CUSTOMER'),array('*'))
                                    ->where('A.CUST_ID = ?',$data['N_CUST_ID'])
                                    ->query()->fetchAll();

                $this->view->compinfo = $selectcomp[0];

                $this->view->N_NUMBER = $data['N_NUMBER'];
                $this->view->N_CUST_ID = $data['N_CUST_ID'];
                $this->view->N_CIF = $data['N_CIF'];
                $this->view->N_STATUS = $data['N_STATUS'];
                $this->view->N_DESC = $data['N_DESC'];
                $this->view->BUSER_NAME = $data['BUSER_NAME'];
                $this->view->N_CREATEDBY = $data['N_CREATEDBY'];
                $this->view->N_CREATED = $data['N_CREATED'];
                $this->view->N_UD_ID = $data['N_UD_ID'];
                $this->view->CHANGES_INFORMATION = $data['CHANGES_INFORMATION'];
                $this->view->exisdoc_underlying = $data['DOCUMENT_NUMBER'];

                $rowUpdated = $this->_db->update(
                    'T_GLOBAL_CHANGES',
                    array(
                        'CHANGES_INFORMATION' => 'Read Suggestion',
                    ),
                    $this->_db->quoteInto('CHANGES_ID = ?', $data['CHANGES_ID'])
                );

                // $approve            = $this->_getParam('approve');
                // $reject             = $this->_getParam('reject');
                // $repair             = $this->_getParam('repair');

                if($this->_getParam('approve') == 'Approve' ){
                        $actor = $this->_userIdLogin;
                        $bgroup = $this->_db->select()
                              ->from('TEMP_NOTIONAL_POOLING')
                              ->where($this->_db->quoteInto('CHANGES_ID = ?',$data['CHANGES_ID']))
                              ->query()
                              ->fetch(Zend_Db::FETCH_ASSOC);
                              
                        if(!count($bgroup)){
                            $this->_errorCode = '22';
                            $this->_errorMsg = 'Query failed(notional)';
                            return false;
                            return false;
                        }
                        //query from TEMP_BPRIVI_GROUP
                        $bprivigroup = $this->_db->select()
                                                   ->from('TEMP_NOTIONAL_DETAIL')
                                                   ->where($this->_db->quoteInto('CHANGES_ID = ?',$data['CHANGES_ID']))
                                                   ->query()
                                                   ->fetchAll(Zend_Db::FETCH_ASSOC);
                        
                        if(!count($bprivigroup)){
                            $this->_errorCode = '22';
                            $this->_errorMsg = 'Query failed(detail)';
                            return false;
                        }

                        // check if delete or not
                        if($data['N_STATUS'] == '2'){
                            $this->_db->beginTransaction();
                            $dataUpdate =  array(					
                                'N_STATUS' 		=> '2',
                                'N_UD_ID' 		=> $data['N_UD_ID'],
                                'NOTE' 		    => $data['NOTE'],
                                'N_DESC' 	    => $data['N_DESC'],
                                'N_UPDATED' 	=> new Zend_Db_Expr('now()'), 
                                'N_UPDATEDBY' 	=> $actor
                            );							
        
                            $where = array('N_NUMBER = ?' => $data['N_NUMBER']);
                            $result = $this->_db->update('T_NOTIONAL_POOLING',$dataUpdate,$where);
                            $this->_db->commit();
                        }else if($data['N_STATUS'] == '0'){
                            
                            $this->_db->beginTransaction();
                            
                            $dataUpdate =  array(					
                                'N_STATUS' 		=> '1',
                                'N_UD_ID' 		=> $data['N_UD_ID'],
                                'NOTE' 		    => $data['NOTE'],
                                'N_DESC' 	    => $data['N_DESC'],
                                'N_UPDATED' 	=> new Zend_Db_Expr('now()'), 
                                'N_UPDATEDBY' 	=> $actor
                            );							
                            
                            $where = array('N_NUMBER = ?' => $data['N_NUMBER']);
                            $result = $this->_db->update('T_NOTIONAL_POOLING',$dataUpdate,$where);

                            $bprivigroupdelete = $this->_db->delete('T_NOTIONAL_DETAIL',$this->_db->quoteInto('N_NUMBER = ?',$data['N_NUMBER']));

                            foreach($bprivigroup as $key=>$val) {
                                $insertArr = array_diff_key($val,array('TEMP_ID'=>'','CHANGES_ID'=>'', 'BG_ID' => ''));
                                $bprivigroupins = $this->_db->insert('T_NOTIONAL_DETAIL',$insertArr);
                                if(!(boolean)$bprivigroupins) {
                                    $this->_errorCode = '82';
                                    $this->_errorMsg = 'Query failed(detail)';
                                    return false;
                                }
                            }

                            $this->_db->commit();
                        
                        }else{
                            $newstatus = '1';
                            //insert to master table M_BGROUP
                            $insertArr = array_diff_key($bgroup,array('TEMP_ID'=>'','CHANGES_ID'=>''));
                            $insertArr['N_UPDATED'] = new Zend_Db_Expr('now()');
                            $insertArr['N_STATUS'] = $newstatus; 
                            $insertArr['N_UPDATEDBY'] = $actor;
                            $bgroupins = $this->_db->insert('T_NOTIONAL_POOLING',$insertArr);
                            if(!(boolean)$bgroupins) {
                                $this->_errorCode = '82';
                                $this->_errorMsg = 'Query failed(notional)';
                                return false;
                            }
                            
                            //insert to master table M_BPRIVI_GROUP
                            foreach($bprivigroup as $key=>$val) {
                                $insertArr = array_diff_key($val,array('TEMP_ID'=>'','CHANGES_ID'=>'', 'BG_ID' => ''));
                                $bprivigroupins = $this->_db->insert('T_NOTIONAL_DETAIL',$insertArr);
                                if(!(boolean)$bprivigroupins) {
                                    $this->_errorCode = '82';
                                    $this->_errorMsg = 'Query failed(detail)';
                                    return false;
                                }
                            }
                        }
                        
                        $bgroupdelete = $this->_db->delete('TEMP_NOTIONAL_POOLING',$this->_db->quoteInto('CHANGES_ID = ?',$data['CHANGES_ID']));

                        $bprivigroupdelete = $this->_db->delete('TEMP_NOTIONAL_DETAIL',$this->_db->quoteInto('CHANGES_ID = ?',$data['CHANGES_ID']));
                        
                
                        $rowUpdated = $this->_db->update(
                        'T_GLOBAL_CHANGES',
                        array(
                            'CHANGES_STATUS' => 'AP',
                            //'CHANGES_STATUS' => 'RJ',
                            'CHANGES_INFORMATION' => 'Read Suggestion',
                            'CHANGES_REASON' => $note,
                            //'LASTUPDATEDBY' => $actor,
                            //NOTE: this field expression is Ms Sql Specific
                            'LASTUPDATED' => new Zend_Db_Expr("now()"),
                        ),
                        $this->_db->quoteInto('CHANGES_ID = ?', $data['CHANGES_ID'])
                    );
                
                  
                        $this->setbackURL('/'.$this->_request->getModuleName());
                        $this->_redirect('/notification/success');
                }

                if($this->_getParam('reject') == 'Reject' ){
                    
                        
                        $bgroupdelete = $this->_db->delete('TEMP_NOTIONAL_POOLING',$this->_db->quoteInto('CHANGES_ID = ?',$data['CHANGES_ID']));

                        $bprivigroupdelete = $this->_db->delete('TEMP_NOTIONAL_DETAIL',$this->_db->quoteInto('CHANGES_ID = ?',$data['CHANGES_ID']));                
                  
                        $this->setbackURL('/'.$this->_request->getModuleName());
                        $this->_redirect('/notification/success');
                }

                $select2 = $this->_db->select()
                            ->from(array('A' => 'TEMP_NOTIONAL_DETAIL'),array('A.N_IS_SAVING','A.N_IS_CREDIT','A.ACCT_NAME','A.ACCT_DESC','A.N_SOURCE_ACCOUNT','A.N_CIF'))
                            // ->join(array('D' => 'TEMP_NOTIONAL_POOLING'), 'D.N_NUMBER = A.N_NUMBER',array('D.N_CUST_ID','D.N_STATUS','N_NUMBER'))
                            // ->join(array('B' => 'M_PRODUCT_TYPE'), 'B.PRODUCT_CODE = A.PRODUCT_TYPE AND A.PRODUCT_PLAN = B.PRODUCT_PLAN',array('B.PRODUCT_RATE','B.PRODUCT_TYPE'))
                            // ->join(array('C' => 'M_CUSTOMER'), 'C.CUST_ID = D.N_CUST_ID',array('C.CUST_NAME','C.CUST_ID','C.CUST_CIF'))
                            ->where('A.N_NUMBER = ?',$np);
                
            //echo $select;die;
                $dataacct                   = $this->_db->fetchAll($select2);
                $this->view->dataacct = $dataacct;
                
            }
        }

        if ($csv) {
            $npnum = $this->_getParam("np");
            $npdesc = $this->_db->select()
                                    ->from(array('A' => 'TEMP_NOTIONAL_DETAIL'),array('*'))
                                    ->where('A.N_NUMBER = ?',$npnum)
                                    ->query()->fetchAll();

            $listdata = array();
            foreach($npdesc as $fil => $val){
                if($val['N_IS_SAVING'] == '1'){
                    $saving = 'Y';
                }else{
                    $saving = '';
                }

                if($val['N_IS_CREDIT'] == '1'){
                    $credit = 'Y';
                }else{
                    $credit = '';
                }

                $paramTrx = array($val['N_SOURCE_ACCOUNT'],$saving,$credit);
                array_push($listdata, $paramTrx);
            }

            $header = array('NATIONAL POOLING ACCOUNT', 'SAVING INTEREST ACCT*', 'CREDIT INTEREST ACCT*');
            $listable = $listdata;
            $this->_helper->download->csv($header, $listable, null, 'National Pooling Group');
            // Application_Helper_General::writeLog('PRLP', 'Export CSV Waiting Release');
        }

        $select3 = $this->_db->select()
                  ->from(array('A' => 'T_FILE_SUBMIT')); 
        
        $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
        $password = $sessionNamespace->token; 
        $this->view->token = $sessionNamespace->token;  

	    $AESMYSQL = new Crypt_AESMYSQL();
	    $PS_NUMBER      = urldecode($this->_getParam('FILE_ID'));
	    $FILE_ID = $AESMYSQL->decrypt($PS_NUMBER, $password);


		if($FILE_ID)
		{	


			$select3->where('FILE_ID =?',$FILE_ID);
			$data = $this->_db->fetchRow($select3);
			
			$attahmentDestination = UPLOAD_PATH . '/document/submit/';
			$this->_helper->download->file($data['FILE_NAME'],$attahmentDestination.$data['FILE_SYSNAME']);
			
			$updateArr = array();
			$updateArr['FILE_DOWNLOADED'] = $data['FILE_DOWNLOADED']+1;
			$updateArr['FILE_DOWNLOADEDBY'] = $this->_userIdLogin;
			
			$whereArr = array('FILE_ID = ?'=>$FILE_ID);
			
			$fileupdate = $this->_db->update('T_FILE_SUBMIT',$updateArr,$whereArr);
			//echo "file downloaded: ".$data['FILE_DOWNLOADED'];
			Application_Helper_General::writeLog('VDEL','Download File Underlying Document '.$data['FILE_NAME']);
		}
		else{
			Application_Helper_General::writeLog('VDEL','View File Underlying Document');
		}

    }

}