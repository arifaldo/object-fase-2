<?php

require_once 'Zend/Controller/Action.php';

class setbillercharges_RepairController extends Application_Main 
{
  public function indexAction() 
  {
		$model = new setbillercharges_Model_Setbillercharges();
		$modelBiller = new biller_Model_Biller();
		
  		$this->_helper->layout()->setLayout('popup');
  		$params = $this->_request->getParams();
  		
  		if(array_key_exists('changes_id', $params))
  		{
  			$filters = array('changes_id' => array('StripTags', 'StringTrim'));
  			$validators = array(
  				'changes_id' => array(
  					'NotEmpty',
  					'Digits',
					'messages' => array(
						'Suggestion ID does not exist.',
						'Suggestion ID must be number.',
					),
  				),
  			);
  			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $params, $this->_optionsValidator);
	  		if($zf_filter_input->isValid())
	  		{
	  			$changeId = $zf_filter_input->changes_id;
				
				$provider = $modelBiller->getDetailProviderTemp($changeId);
				$this->view->providerData = $provider;
		
				$this->view->billerAcctList = $model->getBillerAcctList($provider['PROVIDER_ID']);
				
	  			$this->view->changes_id = $changeId;
				$this->view->modulename = $this->_request->getModuleName();
			}
	  		else
	  		{
	  			$errors = $zf_filter_input->getMessages();
	  			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
      			$this->_helper->getHelper('FlashMessenger')->addMessage($errors['changes_id']);
	  		}
		}
  		else
  		{
  			$errorRemark = 'Suggestion ID does not exist';
  			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
      		$this->_helper->getHelper('FlashMessenger')->addMessage($errorRemark);
  		}
  	
		if($this->_request->isPost() && $this->view->hasPrivilege('RPBC'))
		{
			$this->_db->beginTransaction();
			try
			{
				$charges_amt = $this->_getParam('charges_amt');
				$charges_ccy = $this->_getParam('charges_ccy');
				$charges_to = $this->_getParam('charges_to');
				$biller_acct = $this->_getParam('biller_acct');
				$changes_id = $this->_getParam('changes_id');
					
				$provider = $modelBiller->getDetailProviderTemp($changeId);
				$model->deleteTemp($changes_id);	
				foreach($charges_ccy as $ccy)
				{
					if($charges_amt[$ccy] == null)
						$amt = '0.00';
					else
						$amt = Application_Helper_General::convertDisplayMoney($charges_amt[$ccy]);
					
					if($charges_to[$ccy] == 1) // jika payer
						$charges_acct = null;
					else if($charges_to[$ccy] == 2) // jika biller
						$charges_acct = $biller_acct[$ccy];
							
					$data= array(
									'CHANGES_ID' 			=> $changes_id,
									'PROVIDER_ID' 			=> $provider['PROVIDER_ID'],
									'BANK_CODE' 			=> '008', // hardcode
									'CHARGES_CCY' 			=> $ccy,
									'CHARGES_AMT' 			=> $amt,
									'CHARGES_TO' 			=> $charges_to[$ccy],
									'CHARGES_ACCT' 			=> $charges_acct,
									'CHARGES_SUGGESTED' 	=> new Zend_Db_Expr('GETDATE()'),
									'CHARGES_SUGGESTEDBY' 	=> $this->_userIdLogin
								);
	
					$model->insertTemp($data);		
				}
				
				$this->updateGlobalChanges($changes_id);
				
				//END LOOP
				Application_Helper_General::writeLog('RPBC','Submitting Repair Biller Charges for Provider : '.$provider['PROVIDER_NAME']);
				$this->_db->commit();
				$this->_redirect('/popup/successpopup');
			}
			catch(Exception $e) 
			{
				//rollback changes
				$this->_db->rollBack();
				$errorMsg = 'Database failed.';
				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
			}
		}
  }
}