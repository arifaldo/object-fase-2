<?php
class bankadminreport_Model_Bankadminreport
{
	protected $_db;
	
    // constructor
	public function __construct()
	{	
		$this->_db = Zend_Db_Table::getDefaultAdapter();
	}
	
	function getCustomerCode(){
		
       $select = $this->_db->select()
      	                   ->from(	array('M'			=>'M_USER'),
									array('custid'		=>'USER_ID') )
      	                   ->query()->fetchAll();
						   
		foreach($select as $key => $val){
			
			$a = $select[$key]["custid"];
			$result[$a] = $a;
			
		}
		return $result;
	  
    }
	
	function getFPriviCode(){
		 $select = $this->_db->select()
      	                   ->from(	array(	'M'			=>'M_FPRIVILEGE'),
									array(	'privicode'	=>'FPRIVI_ID',
											'prividesc'	=>'FPRIVI_DESC') )
      	                   ->query()->fetchAll();
						   
		foreach($select as $key => $val){
			
			$privicode = $select[$key]["privicode"];
			$prividesc = $select[$key]["prividesc"];
			$result[$privicode] = $prividesc;
			
		}
		return $result;
	
	}
	
	function getCCYId()
	{
		$select = $this->_db->select()
      	                   ->from(	array(	'M'			=>'M_MINAMT_CCY'),
									array(	'ccyid'		=>'CCY_ID') )
      	                   ->query()->fetchAll();
		
		foreach($select as $key => $val)
		{
			foreach($val as $key1 => $val1)
			{
				$result[$val1] = $val1;
			}
		}
		return $result;
	}
  
    public function getTopHitsStatistic($fParam,$sortBy,$sortDir,$filter)
    {
		
		$select = $this->_db->select()
							->from(	array(	'FA'		=>'T_FACTIVITY'),
									array(	'hitperday'	=>'count("LOG_DATE")'))
							->joinLeft( array('C'=>'M_USER'),'FA.USER_ID = C.USER_ID',
									array(	'companycode'	=>'USER_ID',
											'companyname'	=>'USER_FULLNAME'))
							->group(array('C.USER_ID','C.USER_FULLNAME'))
							->order('count("LOG_DATE") DESC');
							
		if($fParam['fCOMPANYID'])		{ $select->where('UPPER(C.USER_ID) = '.$this->_db->quote(strtoupper($fParam['fCOMPANYID'])));}
		if($fParam['fCOMPANYNAME'])	{ $select->where('UPPER(C.USER_FULLNAME) LIKE '.$this->_db->quote(strtoupper('%'.$fParam['fCOMPANYNAME'].'%')));}
		if($fParam['fPRIVICODE'])		{ $select->where('UPPER(FA.ACTION_DESC) = '.$this->_db->quote(strtoupper($fParam['fPRIVICODE'])));}
		
		if($fParam['fDATEFROM']){
			
			$select->where('convertsgo("date", FA.LOG_DATE) >= '.$this->_db->quote(strtoupper($fParam['fDATEFROM'])));
		
		}
		
		if($fParam['fDATETO']){
			
			$select->where('convertsgo("date", FA.LOG_DATE) <= '.$this->_db->quote(strtoupper($fParam['fDATETO'])));
			
		}
		
		$select->order($sortBy.' '.$sortDir);
		
		return $this->_db->fetchAll($select);
    }
  
    public function getTopTransactionsStatistic($fParam,$sortBy,$sortDir,$filter)
    {		
		$select = $this->_db->select()
							->from(		array(	'P'			=>'T_PSLIP'),array())
										
							->joinLeft( array(	'T'			=>'T_TRANSACTION'),'P.PS_NUMBER = T.PS_NUMBER',
										array(	'numbertx'	=>'COUNT(TRANSACTION_ID)',
												'valuetx'	=>'SUM(TRA_AMOUNT)'	
											))
							->joinLeft( array(	'C'=>'M_USER'),'P.USER_ID = C.USER_ID',
										array(	'USER_ID'=>'USER_ID',
												'USER_FULLNAME'=>'USER_FULLNAME'
											))
							->group(array('C.USER_ID','C.USER_FULLNAME'))
							->order('COUNT(TRANSACTION_ID) DESC')
							->where("C.USER_ID is NOT NULL ");

		if(isset($fParam['USER_ID']))	if(!empty($fParam['USER_ID']))	{ $select->where('UPPER(C.USER_ID) = '.$this->_db->quote(strtoupper($fParam['USER_ID']))); }
		if(isset($fParam['USER_FULLNAME'])) if(!empty($fParam['USER_FULLNAME']))	{ $select->where('UPPER(C.USER_FULLNAME) LIKE '.$this->_db->quote(strtoupper('%'.$fParam['USER_FULLNAME'].'%'))); }
		if(isset($fParam['fCCYID']))if(!empty($fParam['fCCYID']))			{ $select->where('UPPER(P.PS_CCY) = '.$this->_db->quote(strtoupper($fParam['fCCYID']))); }

		if(isset($fParam['fDATEFROM']))
		{
			if(!empty($fParam['fDATEFROM']))
				$select->where('convertsgo("date",P.PS_CREATED) >= '.$this->_db->quote(strtoupper($fParam['fDATEFROM']))); 
		}
		
		if(isset($fParam['fDATETO']))
		{
			if(!empty($fParam['fDATETO']))
				$select->where('convertsgo("date",P.PS_CREATED) <= '.$this->_db->quote(strtoupper($fParam['fDATETO']))); 
		}
		
		$select->order($sortBy.' '.$sortDir);

		return $this->_db->fetchAll($select);
    }
  
    public function getTransactionsStatistic($fParam,$sortBy,$sortDir,$filter)
    {		
		if($fParam['fSHOWTYPE'] =='' OR $fParam['fSHOWTYPE'] == 1){
						
			$select = $this->_db->select()
								->from(		array(	'P'			=>'T_PSLIP'),
											array(	'day'		=>'P.PS_CREATED'))
										
								->joinLeft( array(	'T'			=>'T_TRANSACTION'),'P.PS_NUMBER = T.PS_NUMBER',
											array(	'numbertx'	=>'COUNT(TRANSACTION_ID)',
													'valuetx'	=>'SUM(TRA_AMOUNT)'	
												))
								->joinLeft( array('C'=>'M_USER'),'P.USER_ID = C.USER_ID',array())
								->group('convertsgo("date", P.PS_CREATED)');	
		}
		else if( $fParam['fSHOWTYPE'] == 2){
			
			$select = $this->_db->select()
								->from(		array(	'P'			=>'T_PSLIP'),
											array(	'week'		=>'WEEK(P.PS_CREATED)'))
										
								->joinLeft( array(	'T'			=>'T_TRANSACTION'),'P.PS_NUMBER = T.PS_NUMBER',
											array(	'numbertx'	=>'COUNT(TRANSACTION_ID)',
													'valuetx'	=>'SUM(TRA_AMOUNT)'	
												))
								->joinLeft( array('C'=>'M_USER'),'P.USER_ID = C.USER_ID',array())
								->group('WEEK(P.PS_CREATED)');			
		}
		else if( $fParam['fSHOWTYPE'] == 3)
		{
			$select = $this->_db->select()
								->from(		array(	'P'			=>'T_PSLIP'),
											array(	'month'		=>'MONTH(P.PS_CREATED)',
													'year'		=>'YEAR(P.PS_CREATED)'
												))
										
								->joinLeft( array(	'T'			=>'T_TRANSACTION'),'P.PS_NUMBER = T.PS_NUMBER',
											array(	'numbertx'	=>'COUNT(TRANSACTION_ID)',
													'valuetx'	=>'SUM(TRA_AMOUNT)'	
												))
								->joinLeft( array('C'=>'M_USER'),'P.USER_ID = C.USER_ID',array())
								->group('MONTH(P.PS_CREATED)');	
		}
		
		if($fParam['fCOMPANYID'])		{ $select->where('UPPER(C.USER_ID) = '.$this->_db->quote(strtoupper($fParam['fCOMPANYID']))); }
		if($fParam['fCOMPANYNAME'])	{ $select->where('UPPER(C.USER_FULLNAME) LIKE '.$this->_db->quote(strtoupper('%'.$fParam['fCOMPANYNAME'].'%'))); }
		if($fParam['fCCYID'])			{ $select->where('UPPER(P.PS_CCY) = '.$this->_db->quote(strtoupper($fParam['fCCYID']))); }
		
		if($fParam['fDATEFROM'])
		{
			if($fParam['fSHOWTYPE'] == "" OR $fParam['fSHOWTYPE'] == 1)	{ $select->where('convertsgo("date",P.PS_CREATED) >= '.$this->_db->quote(strtoupper($fParam['fDATEFROM'])));}
			elseif($fParam['fSHOWTYPE'] == 2)					{ $select->where('convertsgo("date",P.PS_CREATED) >= '.$this->_db->quote(strtoupper($fParam['fDATEFROM'])));}
			elseif($fParam['fSHOWTYPE'] == 3)					{ $select->where('convertsgo("date",P.PS_CREATED) >= '.$this->_db->quote(strtoupper($fParam['fDATEFROM'])));}
		}
		
		if($fParam['fDATETO'])
		{
			if($fParam['fSHOWTYPE'] == "" OR $fParam['fSHOWTYPE'] == 1)	{ $select->where('convertsgo("date",P.PS_CREATED) <= '.$this->_db->quote(strtoupper($fParam['fDATETO']))); }
			elseif($fParam['fSHOWTYPE'] == 2)					{ $select->where('convertsgo("date",P.PS_CREATED) <= '.$this->_db->quote(strtoupper($fParam['fDATETO']))); }
			elseif($fParam['fSHOWTYPE'] == 3)					{ $select->where('convertsgo("date",P.PS_CREATED) <= '.$this->_db->quote(strtoupper($fParam['fDATETO']))); }
			
		}
		
		if($sortBy == 'day')
		{
			$sortBy = 'P.PS_CREATED';
		}
		if($sortBy == 'week')
		{
			$sortBy = 'WEEK(P.PS_CREATED)';
		}
		if($sortBy == 'month')
		{
			$sortBy = 'MONTH(P.PS_CREATED)';
		}
		
		$select->order($sortBy.' '.$sortDir);
		
		return $this->_db->fetchAll($select);
    }
  
    public function getWebUsagesStatistic($fParam,$sortBy,$sortDir,$filter)
    {		
		if($fParam['fSHOWTYPE'] =='' OR $fParam['fSHOWTYPE'] == 1)
		{
			$select = $this->_db->select()
								->from(	array(	'FA'		=>'T_FACTIVITY'),
										array(	'day'		=>'FA.LOG_DATE',
												'hitperday'	=>'count("LOG_DATE")'
												))
								->joinLeft( array('C'=>'M_USER'),'FA.USER_ID = C.USER_ID',array())
								->group('DAY(LOG_DATE)');
		}
		else if( $fParam['fSHOWTYPE'] == 2)
		{
			$select = $this->_db->select()
								->from(	array(	'FA'		=>'T_FACTIVITY'),
										array(	'week'		=>'WEEK(FA.LOG_DATE)',
												'hitperweek'=>'count("LOG_DATE")'
												))
								->joinLeft( array('C'=>'M_USER'),'FA.USER_ID = C.USER_ID',array())
								->group('WEEK(LOG_DATE)');
		}
		else if( $fParam['fSHOWTYPE'] == 3)
		{ 
			$select = $this->_db->select()
								->from(	array(	'FA'		 =>'T_FACTIVITY'),
										array(	'month'		 =>'MONTH(LOG_DATE)',
												'year'		 =>'YEAR(LOG_DATE)',
												'hitpermonth'=>'count("LOG_DATE")'
												))
								->joinLeft( array('C'=>'M_USER'),'FA.USER_ID = C.USER_ID',array())
								->group(array('YEAR(LOG_DATE)','MONTH(LOG_DATE)'));
		}
		else if( $fParam['fSHOWTYPE'] == 4)
		{
			$select = $this->_db->select()
								->from(	array(	'FA'		=>'T_FACTIVITY'),
										array(	'year'		=>'YEAR(FA.LOG_DATE)',
												'hitperyear'=>'count("LOG_DATE")'
												))
								->joinLeft( array('C'=>'M_USER'),'FA.USER_ID = C.USER_ID',array())
								->group('YEAR(LOG_DATE)');
		}
		
		if($fParam['fCOMPANYID'])		{ $select->where('UPPER(FA.USER_ID) = '.$this->_db->quote(strtoupper($fParam['fCOMPANYID'])));}
		if($fParam['fCOMPANYNAME'])	{ $select->where('UPPER(C.USER_FULLNAME) LIKE '.$this->_db->quote(strtoupper('%'.$fParam['fCOMPANYNAME'].'%')));}
		if($fParam['fPRIVICODE'])		{ $select->where('UPPER(FA.ACTION_DESC) = '.$this->_db->quote(strtoupper($fParam['fPRIVICODE'])));}
		
		if($fParam['fDATEFROM'])
		{	
			$select->where('convertsgo("date", FA.LOG_DATE) >= '.$this->_db->quote(strtoupper($fParam['fDATEFROM'])));
		}
		
		if($fParam['fDATETO'])
		{	
			$select->where('convertsgo("date", FA.LOG_DATE) <= '.$this->_db->quote(strtoupper($fParam['fDATETO'])));
		}
		
		$select->order($sortBy.' '.$sortDir);
		
		return $this->_db->fetchAll($select);
    }
}