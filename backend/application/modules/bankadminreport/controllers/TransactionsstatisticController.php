<?php
require_once 'Zend/Controller/Action.php';

class Bankadminreport_TransactionsstatisticController extends Application_Main
{	

	protected $_moduleDB = 'RTF';
	
	public function indexAction() 
	{			
		$this->_helper->layout()->setLayout('newlayout');

		$arrCust 	 = $this->getCustomerCode();
		$arrCcyId    = $this->getCCYId();
		$arrShowType = $this->showType();
		
		$this->view->arrCust 		= $arrCust;
		$this->view->arrCcyId 		= $arrCcyId;
		$this->view->arrShowType 	= $arrShowType;
		
		$day = $this->language->_('Date');
		$numberOfTx = $this->language->_('Number of Transactions');
		$valueOfTx = $this->language->_('Value of Transactions');
		$weekEmail = $this->language->_('Date');
		$monthEmail = $this->language->_('Date');
		
		$fieldsdaily = array(

								'days'  		=> array(	
														'field' => 'days',
														'label' => 'Day',
														'sortable' => true
													),
								'day'  		=> array(	
														'field' => 'day',
														'label' => $day,
														'sortable' => true
													),
								'numbertx' => array(
														'field' => 'numbertx',
														'label' => $numberOfTx,
														'sortable' => true
													),
								// 'ccy' => array(
								// 						'field' => 'ccy',
								// 						'label' => '',
								// 						'sortable' => true
								// 					),
								'valuetx' => array(
														'field' => 'valuetx',
														'label' => $valueOfTx,
														'sortable' => true
													),
						);
		
		$fieldsweekly = array(
								'week'  	=> array	(	
														'field' => 'week',
														'label' => $weekEmail,
														'sortable' => true
													),
								'numbertx' => array(
														'field' => 'numbertx',
														'label' => $numberOfTx,
														'sortable' => true
													),
								'ccy' => array(
														'field' => 'ccy',
														'label' => '',
														'sortable' => true
													),
								'valuetx' => array(
														'field' => 'valuetx',
														'label' => $valueOfTx,
														'sortable' => true
													),
						);
		
		$fieldsmonthly = array	(
		
								'month'  	=> array(	
														'field' => 'month',
														'label' => $monthEmail,
														'sortable' => true
													),
								'numbertx' => array(
														'field' => 'numbertx',
														'label' => $numberOfTx,
														'sortable' => true
													),
								'ccy' => array(
														'field' => 'ccy',
														'label' => '',
														'sortable' => true
													),
								'valuetx' => array(
														'field' => 'valuetx',
														'label' => $valueOfTx,
														'sortable' => true
												),
						);

		$filterlist = array('PS_CREATED','COMP_ID','CCY','COMP_NAME');
		
		$this->view->filterlist = $filterlist;
		
		//validasi page, jika input page bukan angka               
		$page 		 = $this->_getParam('page');
		$csv 		 = $this->_getParam('csv');
		$pdf 		 = $this->_getParam('pdf');
		
		$filter 	 = $this->_getParam('filter');
		$clearfilter = $this->_getParam('clearfilter');
		$fSHOWTYPE 	 = $this->_getParam('SHOWTYPE');
				
		$page 	= (Zend_Validate::is($page,'Digits'))? $page : 1;
		
		if($fSHOWTYPE == '' OR $fSHOWTYPE == '1'){ $fields = $fieldsdaily; }
		else if($fSHOWTYPE == 2){ $fields = $fieldsweekly; }
		else if($fSHOWTYPE == 3){ $fields = $fieldsmonthly; }
		
		
		//validasi sort, jika input sort bukan ASC atau DESC
		$sortBy  = $this->_getParam('sortby');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
		
		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		
		$filterArr = array('COMP_ID'     	=> array('StripTags','StringTrim','StringToUpper'),
						   'COMP_NAME'   	=> array('StripTags','StringTrim','StringToUpper'),
						   'PS_CREATED'  		=> array('StripTags','StringTrim'),
						   'PS_CREATED_END'  		=> array('StripTags','StringTrim'),
						   'CCY' 		=> array('StripTags','StringTrim'),
						   'SHOW' 		=> array('StripTags','StringTrim'),
						  );
		
		// if POST value not null, get post, else get param
		$dataParam = array('COMP_ID','COMP_NAME','CCY','SHOW');
		$dataParamValue = array();
		
		$clean2 = array_diff( $this->_request->getParam('wherecol'),$dataParam); 
		$dataParam = array_diff( $this->_request->getParam('wherecol'),$clean2); 
		// print_r($dataParam);die;

		// print_r($output);die;
		// print_r($this->_request->getParam('wherecol'));
		foreach ($dataParam as $no => $dtParam)
		{
		
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
				// print_r($dataval);
				$order = 0;
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($value == "PS_CREATED"){
								$order--;
							}
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$order];
						}
						$order++;
					}
				
			}
		}
		
		//echo "<pre>";
		//print_r ($dataParamValue);

		if(!empty($this->_request->getParam('createdate'))){
				$createarr = $this->_request->getParam('createdate');
					$dataParamValue['PS_CREATED'] = $createarr[0];
					$dataParamValue['PS_CREATED_END'] = $createarr[1];
			}
		
		// The default is set so all fields allow an empty string		
		$options = array('allowEmpty' => true);
		$validators = array(
						'COMP_ID' 	=> array(array('InArray', array('haystack' => array_keys($arrCust)))),
						'COMP_NAME' 	=> array(),	
						'PS_CREATED' 		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'PS_CREATED_END'		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'CCY' 		=> array(array('InArray', array('haystack' => array_keys($arrCcyId)))),	
						'SHOW' 		=> array(array('InArray', array('haystack' => array_keys($arrShowType)))),							
						);
		
		$zf_filter  = new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);
		$filter 	= $this->_getParam('filter');
		$csv 		= $this->_getParam('csv');
		$pdf 		= $this->_getParam('pdf');
		
//		$fCOMPANYID      = $zf_filter->getEscaped('COMPANYID');
//		$fCOMPANYNAME    = $zf_filter->getEscaped('COMPANYNAME');
		
		$fCOMPANYID      = $zf_filter->getEscaped('COMP_ID');
		$fCOMPANYNAME    = $zf_filter->getEscaped('COMP_NAME');
		
		if($filter == NULL && $clearfilter != 1){
			$fDATETO 	= date("d/m/Y");
			
			$Y = date('Y');
			$d = date('d');
			$m = date('m',strtotime("-1 month")); //last month
			if($m == '12') $Y = $Y -1;
			$fDATEFROM = $d.'/'.$m.'/'.$Y;
		}
		else{
			if($filter != NULL){
				$fDATEFROM 	= html_entity_decode($zf_filter->getEscaped('PS_CREATED'));
				$fDATETO 	= html_entity_decode($zf_filter->getEscaped('PS_CREATED_END'));
			}
			else if($clearfilter == 1){
				$fDATEFROM 	= "";
				$fDATETO 	= "";
			}
		}
		
//		$fCCYID  = $zf_filter->getEscaped('CCYID');
//		$fSHOWTYPE   = $zf_filter->getEscaped('SHOWTYPE');
		
		$fCCYID  = $zf_filter->getEscaped('CCY');
		$fSHOWTYPE   = $zf_filter->getEscaped('SHOW');
		
		if($fSHOWTYPE =='' OR $fSHOWTYPE == 1){
						
			$select = $this->_db->select()
								->from(		array(	'P'			=>'T_PSLIP'),
											array(	'day'		=>'DATE(P.PS_CREATED)',
													'days'		=>'DATE(P.PS_CREATED)'))
										
								->joinLeft( array(	'T'			=>'T_TRANSACTION'),'P.PS_NUMBER = T.PS_NUMBER',
											array(	'numbertx'	=>'COUNT(TRANSACTION_ID)',
													'valuetx'	=>'SUM(TRA_AMOUNT)'	
												))
								->joinLeft( array('C'=>'M_CUSTOMER'),'P.CUST_ID = C.CUST_ID',array())
								->group('DATE(P.PS_CREATED)')
								->order('P.PS_CREATED DESC');	
			
		}
		else if( $fSHOWTYPE == 2){
			
			$select = $this->_db->select()
								->from(		array(	'P'			=>'T_PSLIP'),
											array(	'week'		=>'WEEK(P.PS_CREATED)'))
										
								->joinLeft( array(	'T'			=>'T_TRANSACTION'),'P.PS_NUMBER = T.PS_NUMBER',
											array(	'numbertx'	=>'COUNT(TRANSACTION_ID)',
													'valuetx'	=>'SUM(TRA_AMOUNT)'	
												))
								->joinLeft( array('C'=>'M_CUSTOMER'),'P.CUST_ID = C.CUST_ID',array())
								->group('WEEK(P.PS_CREATED)')
								->order('P.PS_CREATED DESC');
			//echo "<pre>";
			//echo $select->__toString();
			//die;
			
		}
		else if( $fSHOWTYPE == 3){
			
			$select = $this->_db->select()
								->from(		array(	'P'			=>'T_PSLIP'),
											array(	'month'		=>'MONTH(P.PS_CREATED)',
													'year'		=>'YEAR(P.PS_CREATED)'
												))
										
								->joinLeft( array(	'T'			=>'T_TRANSACTION'),'P.PS_NUMBER = T.PS_NUMBER',
											array(	'numbertx'	=>'COUNT(TRANSACTION_ID)',
													'valuetx'	=>'SUM(TRA_AMOUNT)'	
												))
								->joinLeft( array('C'=>'M_CUSTOMER'),'P.CUST_ID = C.CUST_ID',array())
								->group(array('YEAR(P.PS_CREATED)','MONTH(P.PS_CREATED)'))
								->order('P.PS_CREATED DESC');
			// echo "<pre>";
			// echo $select->__toString();

		}
		
		if($fCOMPANYID)		{ $select->where('UPPER(C.CUST_ID) = '.$this->_db->quote(strtoupper($fCOMPANYID))); }
		if($fCOMPANYNAME)	{ $select->where('UPPER(C.CUST_NAME) LIKE '.$this->_db->quote(strtoupper('%'.$fCOMPANYNAME.'%'))); }
		if($fCCYID)			{ $select->where('UPPER(P.PS_CCY) = '.$this->_db->quote(strtoupper($fCCYID))); }
		
		if($fDATEFROM){
			
			$FormatDateFrom = new Zend_Date($fDATEFROM, $this->_dateDisplayFormat);
			$dateFrom   	= $FormatDateFrom->toString($this->_dateDBFormat);	
			
			// if($fSHOWTYPE == "" OR $fSHOWTYPE == 1){ 
				// $select->where('DATE(P.PS_CREATED) >= '.$this->_db->quote(strtoupper($dateFrom)));
			// }
			// elseif($fSHOWTYPE == 2){
				// $select->where('DATE(P.PS_CREATED) >= '.$this->_db->quote(strtoupper($dateFrom)));
			// }
			// elseif($fSHOWTYPE == 3){
				// $select->where('DATE(P.PS_CREATED) >= '.$this->_db->quote(strtoupper($dateFrom)));
			// }
			$select->where('DATE(P.PS_CREATED) >= '.$this->_db->quote(strtoupper($dateFrom)));
			
		
		}
		
		if($fDATETO){
			
			$FormatDateTo 	= new Zend_Date($fDATETO, $this->_dateDisplayFormat);
			$dateTo  		= $FormatDateTo->toString($this->_dateDBFormat);	
			
			// if($fSHOWTYPE == "" OR $fSHOWTYPE == 1){
				// $select->where('DATE(P.PS_CREATED) <= '.$this->_db->quote(strtoupper($dateTo))); 
			// }
			
			// elseif($fSHOWTYPE == 2){ 
				
				// $select->where('DATE(P.PS_CREATED) <= '.$this->_db->quote(strtoupper($dateTo))); 
			// }
			// elseif($fSHOWTYPE == 3){
			
				// $select->where('DATE(P.PS_CREATED) <= '.$this->_db->quote(strtoupper($dateTo))); 
			// }
			$select->where('DATE(P.PS_CREATED) <= '.$this->_db->quote(strtoupper($dateTo))); 
			
		}
		
		// echo "<pre>";
		// echo $select->__toString();die;
			
		
		$select->order($sortBy.' '.$sortDir);
		$dataSQL = $this->_db->fetchAll($select);
		
		//-- get total hit  & average -- //
		
		$totalnumbertx = 0;
		$totalvaluetx = 0;
		
		foreach($dataSQL as $key =>$dt){
				
			$totalnumbertx = $totalnumbertx + $dt["numbertx"];
			$totalvaluetx  = $totalvaluetx + $dt["valuetx"];
				
		}
			
		@$avgnumbertx 	= round($totalnumbertx / count($dataSQL),2);
		@$avgvaluetx 	= round($totalvaluetx / count($dataSQL),2);
			
		//-- end of get total hit & average -- //
		
		// echo "<pre>";
		// print_r ($dataSQL);
		//die;
		
		if ($csv || $pdf || $this->_request->getParam('print'))
		{	$headers  = Application_Helper_Array::simpleArray($fields, "label");}
		else
		{
			$this->paging($dataSQL);
			$dataSQL = $this->view->paginator;
		}
		
		$totalvaluetx 	= Application_Helper_General::displayMoney($totalvaluetx);
		$avgvaluetx 	= Application_Helper_General::displayMoney($avgvaluetx);
		
		if(!empty($dataSQL) && count($dataSQL) > 0){
			
			foreach ($dataSQL as $d => $dt){					
				
				foreach ($fields as $key => $field){
					$value = $dt[$key];
					
					if($fSHOWTYPE == "" OR $fSHOWTYPE == "1"){ 
						if($key== "day"){ $value = Application_Helper_General::convertDate($value,"dd MMM Y",$this->view->defaultDateFormat); }
						
					}
					elseif($fSHOWTYPE == "2"){
						if($key== "week"){ $value = 'Week '.$value;}
						
					}
					elseif($fSHOWTYPE == "3"){
						if($key== "month"){ 
							$monthname = $this->changeMonthName($dt["month"]);
							$value = $monthname." ".$dt["year"]; 
						}
					}
					if($key == "valuetx"){ $value = Application_Helper_General::displayMoney($value);}
					
					$data[$d][$key] = $value;
				}
			}
			
			//count totalhit and average:
			
			$dataPDFCSV = $data;
			// $dataPDFCSV[$d+1][0]	= '';
			//$dataPDFCSV[$d+1][0]	= 'Total TX';
			$dataPDFCSV[$d+1]['number'] = false;
			$dataPDFCSV[$d+1][0] = $this->language->_('Total Hit');
			$dataPDFCSV[$d+1][1]	= $totalnumbertx;
			$dataPDFCSV[$d+1][2]	= $totalvaluetx;

			// $dataPDFCSV[$d+2][0]	= '';
			//$dataPDFCSV[$d+2][0]	= 'Average TX';
			$dataPDFCSV[$d+2]['number'] = false;
			$dataPDFCSV	[$d+2][0]	= $this->language->_('Average').' TX';
			$dataPDFCSV[$d+2][1]	= $avgnumbertx;
			$dataPDFCSV[$d+2][2]	= $avgvaluetx;
			
		}
		else{ $data = array(); }
		

		
		if($csv){
			
			if(!empty($dataSQL) && count($dataSQL) > 0){
				
				foreach ($dataSQL as $d => $dt){					
					
					foreach ($fields as $key => $field){
						$value = $dt[$key];
						
						if($fSHOWTYPE == "" OR $fSHOWTYPE == "1"){ 
							if($key== "day"){ $value = Application_Helper_General::convertDate($value,"dd MMM Y",$this->view->defaultDateFormat); }
							
						}
						elseif($fSHOWTYPE == "2"){
							if($key== "week"){ $value = 'Week '.$value;}
							
						}
						elseif($fSHOWTYPE == "3"){
							if($key== "month"){ 
								$monthname = $this->changeMonthName($dt["month"]);
								$value = $monthname." ".$dt["year"]; 
							}
						}
						if($key == "valuetx"){ $value = Application_Helper_General::displayMoney($value);}
						
						$data[$d][$key] = $value;
					}
				}
				
				//count totalhit and average:
				
				$dataPDFCSV = $data;
				// $dataPDFCSV[$d+1][0]	= '';
				//$dataPDFCSV[$d+1][0]	= 'Total TX';
//				$dataPDFCSV[$d+1]['number'] = false;
				$dataPDFCSV[$d+1][0] = $this->language->_('Total Hit');
				$dataPDFCSV[$d+1][1]	= $totalnumbertx;
				$dataPDFCSV[$d+1][2]	= $totalvaluetx;
	
				// $dataPDFCSV[$d+2][0]	= '';
				//$dataPDFCSV[$d+2][0]	= 'Average TX';
//				$dataPDFCSV[$d+2]['number'] = false;
				$dataPDFCSV	[$d+2][0]	= $this->language->_('Average').' TX';
				$dataPDFCSV[$d+2][1]	= $avgnumbertx;
				$dataPDFCSV[$d+2][2]	= $avgvaluetx;
			}
			else{ $data = array(); }
			
			$this->_helper->download->csv($headers,$dataPDFCSV,null,$this->language->_('Web Transactions Statistic'));  
			Application_Helper_General::writeLog('ARTS','Export CSV Transactios (Release) Statistic');
		}
		elseif($pdf){
		
			$this->_helper->download->pdf($headers,$dataPDFCSV,null,$this->language->_('Web Transactions Statistic'));  
			Application_Helper_General::writeLog('ARTS','Export PDF Transactios (Release) Statistic');
		}
		elseif($this->_request->getParam('print') == 1){
			if(!empty($dataSQL) && count($dataSQL) > 0){
				
				foreach ($dataSQL as $d => $dt){					
					
					foreach ($fields as $key => $field){
						$value = $dt[$key];
						
						if($fSHOWTYPE == "" OR $fSHOWTYPE == "1"){ 
							if($key== "day"){ $value = Application_Helper_General::convertDate($value,"dd MMM Y",$this->view->defaultDateFormat); }
							$key1 = 'day';
							$key2 = 'numbertx';
							$key3 = 'valuetx';
						}
						elseif($fSHOWTYPE == "2"){
							if($key== "week"){ $value = 'Week '.$value;}
							$key1 = 'week';
							$key2 = 'numbertx';
							$key3 = 'valuetx';
							
						}
						elseif($fSHOWTYPE == "3"){
							if($key== "month"){ 
								$monthname = $this->changeMonthName($dt["month"]);
								$value = $monthname." ".$dt["year"]; 
							}
							$key1 = 'month';
							$key2 = 'numbertx';
							$key3 = 'valuetx';
						}
						if($key == "valuetx"){ $value = Application_Helper_General::displayMoney($value);}
						
						$data[$d][$key] = $value;
					}
				}
				
				//count totalhit and average:
				
				$dataPDFCSV = $data;
				// $dataPDFCSV[$d+1][0]	= '';
				//$dataPDFCSV[$d+1][0]	= 'Total TX';
//				$dataPDFCSV[$d+1]['number'] = false;
				$dataPDFCSV[$d+1][$key1] 	= $this->language->_('Total Hit');
				$dataPDFCSV[$d+1][$key2]	= $totalnumbertx;
				$dataPDFCSV[$d+1][$key3]	= $totalvaluetx;
	
				// $dataPDFCSV[$d+2][0]	= '';
				//$dataPDFCSV[$d+2][0]	= 'Average TX';
//				$dataPDFCSV[$d+2]['number'] = false;
				$dataPDFCSV[$d+2][$key1]	= $this->language->_('Average').' TX';
				$dataPDFCSV[$d+2][$key2]	= $avgnumbertx;
				$dataPDFCSV[$d+2][$key3]	= $avgvaluetx;
				
			}
			else{ $data = array(); }
			unset($fields['action']);
			foreach($dataPDFCSV as $key=>$row)
			{
				$dataPDFCSV[$key]['days'] = Application_Helper_General::convertDate($row['days'], Zend_Date::WEEKDAY);
				$dataPDFCSV[$key]['valuetx']  = 'IDR '.$row['valuetx'];

			}

			// echo "<pre>";
			// var_dump($dataPDFCSV);
			// die();

			$this->_forward('print', 'index', 'widget', array('data_content' => $dataPDFCSV, 'data_caption' => 'Web Transactions Statistic', 'data_header' => $fields));
			Application_Helper_General::writeLog('ARTS','Print Transactios (Release) Statistic');
		}
		else{	
		
			$stringParam = array( 	'COMP_ID'		=>'fCOMPANYID',
									'COMP_NAME'	=>'fCOMPANYNAME',
									'PS_CREATED'		=>'fDATEFROM',
									'PS_CREATED_END'		=>'fDATETO',
									'CCY'			=>'fCCYID',
									'SHOW'		=>'fSHOWTYPE',
										
									'clearfilter'		=> $clearfilter,
									'filter'			=> $filter,
								);
				
			$this->view->COMPANYID = $fCOMPANYID;
			$this->view->COMPANYNAME = $fCOMPANYNAME;
			$this->view->DATEFROM 	 = $fDATEFROM;
			$this->view->DATETO 	 = $fDATETO;
			$this->view->CCYID 	 = $fCCYID;
			$this->view->SHOWTYPE 	 = $fSHOWTYPE;
			
			unset($fields['ccy']);
			unset($fields['valuetx']);
			$this->view->fields 		= $fields;
			$this->view->filter 		= $filter;
			$this->view->clearfilter	= $clearfilter;
								
			$this->view->sortBy 		= $sortBy;
			$this->view->sortDir 		= $sortDir;
			$this->view->data 			= $data;
							
			$this->view->arrCust 		= $arrCust;
			$this->view->arrCcyId 		= $arrCcyId;
			$this->view->arrShowType 	= $arrShowType;
			
			$this->view->totalnumbertx 	= $totalnumbertx;
			$this->view->totalvaluetx 	= $totalvaluetx;
			$this->view->avgnumbertx 	= $avgnumbertx;
			$this->view->avgvaluetx 	= $avgvaluetx;
		}
		
		Application_Helper_General::writeLog('ARTS','View Transactios (Release) Statistic');

		if(!empty($dataParamValue)){
	    		$this->view->createStart = $dataParamValue['PS_CREATED'];
	    		$this->view->createEnd = $dataParamValue['PS_CREATED_END'];

	    	  	unset($dataParamValue['PS_CREATED_END']);
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     // print_r($whereval);die;
      }

	}	
	
	function changeMonthName($monthnumber){
	
		$arrmonth = array(
						'1'=>'January',
						'2'=>'February',
						'3'=>'March',
						'4'=>'April',
						'5'=>'May',
						'6'=>'June',
						'7'=>'July',
						'8'=>'August',
						'9'=>'September',
						'10'=>'October',
						'11'=>'November',
						'12'=>'December'
						
					);
		foreach($arrmonth as $key => $value){
			if($key == $monthnumber){ $result = $value; }
		}
		return $result;
	}
	
	function showType(){
	
		$arr = array(	'1'=>$this->language->_('Daily'),
						'2'=>$this->language->_('Weekly'),
						'3'=>$this->language->_('Monthly')
					);
		return $arr;
		
	}
	
	function getCustomerCode(){
		
       $select = $this->_db->select()
      	                   ->from(	array('M'			=>'M_CUSTOMER'),
									array('custid'		=>'CUST_ID') )
      	                   ->query()->fetchAll();
		$result =  array();				   
		foreach($select as $key => $val){
			
			$a = $select[$key]["custid"];
			$result[$a] = $a;
			
		}
		return $result;
	  
    }
	
	function getCCYId(){
	
		 $select = $this->_db->select()
      	                   ->from(	array(	'M'			=>'M_MINAMT_CCY'),
									array(	'ccyid'		=>'CCY_ID') )
      	                   ->query()->fetchAll();
		
		foreach($select as $key => $val){
			foreach($val as $key1 => $val1){
				$result[$val1] = $val1;
			}
		}
		
		return $result;
	
	}
	
}
