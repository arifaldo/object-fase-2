<?php
require_once 'Zend/Controller/Action.php';

class Bankadminreport_TophitsstatisticController extends Application_Main
{	

	protected $_moduleDB = 'RTF';
	
	public function indexAction() 
	{			
		$this->_helper->layout()->setLayout('newlayout');

		$arrCust 	 = $this->getCustomerCode();
		$arrFPrivi   = $this->getFPriviCode();
		$arrShowType = $this->showType();
		
		$this->view->arrCust 		= $arrCust;
		$this->view->arrFPrivi 		= $arrFPrivi;
		$this->view->arrShowType 	= $arrShowType;
		
		$companyCode = $this->language->_('Company Code');
		$company = $this->language->_('Company');
		$hit = $this->language->_('Hit');
		
		$fields = array(
								'companycode' 	=> array(	
														'field' => 'companycode',
														'label' => $companyCode,
														'sortable' => false
													),
								'companyname' 	=> array(	
														'field' => 'companyname',
														'label' => $company,
														'sortable' => false
													),
								'hitperday' => array(
														'field' => 'hitperday',
														'label' => $hit,
														'sortable' => false
													)
						);

		$filterlist = array('PS_CREATED','COMP_ID','ACTION','COMP_NAME');
		
		$this->view->filterlist = $filterlist;
		
		//validasi page, jika input page bukan angka               
		$page 		 = $this->_getParam('page');
		$csv 		 = $this->_getParam('csv');
		$pdf 		 = $this->_getParam('pdf');
		
		$filter 	 = $this->_getParam('filter');
		$clearfilter = $this->_getParam('clearfilter');
		$fSHOWTYPE 	 = $this->_getParam('SHOWTYPE');
				
		$page 	= (Zend_Validate::is($page,'Digits'))? $page : 1;
		
		
		//validasi sort, jika input sort bukan ASC atau DESC
		$sortBy  = $this->_getParam('sortby');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
		
		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		
		$filterArr = array('COMP_ID'     	=> array('StripTags','StringTrim','StringToUpper'),
						   'COMP_NAME'   	=> array('StripTags','StringTrim','StringToUpper'),
						   'PS_CREATED'  		=> array('StripTags','StringTrim'),
						   'PS_CREATED_END' 		=> array('StripTags','StringTrim'),
						   'ACTION' 		=> array('StripTags','StringTrim'),
						  );
		
		// if POST value not null, get post, else get param
		$dataParam = array('COMP_ID','COMP_NAME','ACTION');
		$dataParamValue = array();
		
		$clean2 = array_diff( $this->_request->getParam('wherecol'),$dataParam); 
		$dataParam = array_diff( $this->_request->getParam('wherecol'),$clean2); 
		// print_r($dataParam);die;

		// print_r($output);die;
		// print_r($this->_request->getParam('wherecol'));
		foreach ($dataParam as $no => $dtParam)
		{
		
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
				// print_r($dataval);
				$order = 0;
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($value == "PS_CREATED"){
								$order--;
							}
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$order];
						}
						$order++;
					}
				
			}
		}
		
		//echo "<pre>";
		//print_r ($dataParamValue);

		if(!empty($this->_request->getParam('createdate'))){
				$createarr = $this->_request->getParam('createdate');
					$dataParamValue['PS_CREATED'] = $createarr[0];
					$dataParamValue['PS_CREATED_END'] = $createarr[1];
			}
		
		// The default is set so all fields allow an empty string		
		$options = array('allowEmpty' => true);
		$validators = array(
						'COMP_ID' 	=> array(array('InArray', array('haystack' => array_keys($arrCust)))),
						'COMP_NAME' 	=> array(),	
						'PS_CREATED' 		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'PS_CREATED_END'		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'ACTION' 	=> array(array('InArray', array('haystack' => array_keys($arrFPrivi)))),			
						);
		
		$zf_filter  = new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);
		$filter 	= $this->_getParam('filter');
		$csv 		= $this->_getParam('csv');
		$pdf 		= $this->_getParam('pdf');
		
//		$fCOMPANYID      = $zf_filter->getEscaped('COMPANYID');
//		$fCOMPANYNAME    = $zf_filter->getEscaped('COMPANYNAME');
		
		$fCOMPANYID      = $zf_filter->getEscaped('COMP_ID');
		$fCOMPANYNAME    = $zf_filter->getEscaped('COMP_NAME');
		
		if($filter == NULL && $clearfilter != 1){
			$fDATETO 	= date("d/m/Y");
			
			$Y = date('Y');
			$d = date('d');
			$m = date('m',strtotime("-1 month")); //last month
			if($m == '12') $Y = $Y -1;
			$fDATEFROM = $d.'/'.$m.'/'.$Y;
		}
		else{
			if($filter != NULL){
				$fDATEFROM 	= html_entity_decode($zf_filter->getEscaped('PS_CREATED'));
				$fDATETO 	= html_entity_decode($zf_filter->getEscaped('PS_CREATED_END'));
			}
			else if($clearfilter == 1){
				$fDATEFROM 	= "";
				$fDATETO 	= "";
			}
		}
		
		$fPRIVICODE  = $zf_filter->getEscaped('ACTION');		
//		$fPRIVICODE  = $zf_filter->getEscaped('PRIVICODE');		
		
		
			
		$select = $this->_db->select()
							->from(	array(	'FA'		=>'T_FACTIVITY'),
									array(	'hitperday'	=>'count("LOG_DATE")'))
							->joinLeft( array('C'=>'M_CUSTOMER'),'FA.CUST_ID = C.CUST_ID',
									array(	'companycode'	=>'CUST_ID',
											'companyname'	=>'CUST_NAME'))
							->group(array('C.CUST_ID','C.CUST_NAME'))
							->order('count("LOG_DATE") DESC');
							// ->where("C.CUST_ID is Not NULL ");
			
		
		
		/*
			TA.ACTION_DESC 		= PRIVICODE
			TA.ACTION_FULLDESC 	= PRIVICODE DESCRIPTION
		*/
		
		if($fCOMPANYID)		{ $select->where('UPPER(C.CUST_ID) = '.$this->_db->quote(strtoupper($fCOMPANYID)));}
		if($fCOMPANYNAME)	{ $select->where('UPPER(C.CUST_NAME) LIKE '.$this->_db->quote(strtoupper('%'.$fCOMPANYNAME.'%')));}
		if($fPRIVICODE)		{ $select->where('UPPER(FA.ACTION_DESC) = '.$this->_db->quote(strtoupper($fPRIVICODE)));}
		
		if($fDATEFROM){
			
			$FormatDateFrom = new Zend_Date($fDATEFROM, $this->_dateDisplayFormat);
			$dateFrom   	= $FormatDateFrom->toString($this->_dateDBFormat);	
			
			$select->where('DATE(FA.LOG_DATE) >= '.$this->_db->quote(strtoupper($dateFrom)));
		
		}
		
		if($fDATETO){
			
			$FormatDateTo 	= new Zend_Date($fDATETO, $this->_dateDisplayFormat);
			$dateTo  		= $FormatDateTo->toString($this->_dateDBFormat);	
			
			$select->where('DATE(FA.LOG_DATE) <= '.$this->_db->quote(strtoupper($dateTo)));
			
		}
		
		//echo "<pre>";
		//echo $select->__toString();
		//die;
		
		$select->order($sortBy.' '.$sortDir);
		$dataSQL = $this->_db->fetchAll($select);
		
		// var
		
		//-- get total hit  & average -- //
		
			$total = 0;
			foreach($dataSQL as $key =>$dt){
				
				$total = $total + $dt["hitperday"];
				
			}
			
			@$avg = round($total / count($dataSQL),2);
			
		//-- end of get total hit & average -- //
		
		if ($csv || $pdf || $this->_request->getParam('print'))
		{	$headers  = Application_Helper_Array::simpleArray($fields, "label");}
		else
		{
			$this->paging($dataSQL);
			$dataSQL = $this->view->paginator;
		}
		
		
		//echo "<pre>";
		//echo $select->__toString();
		//print_r ($dataSQL);
		//die;
		
		
//		if(!empty($dataSQL) && count($dataSQL) > 0){
//		
//			foreach ($dataSQL as $d => $dt){
//				
//				
//				
//				foreach ($fields as $key => $field){
//					$value = $dt[$key];
//					$data[$d][$key] = $value;
//				}
//			}
//			
//			//count totalhit and average:
//			
//			
//		}
//		else{ $data = array(); }

		if(!empty($dataSQL) && count($dataSQL) > 0){
			foreach ($dataSQL as $d => $dt){
				foreach ($fields as $key => $field){

					$value = $dt[$key];
					$data[$d][$key] = $value;
				}
			}
			$dataPDFCSV = $data;
			$dataPDFCSV[$d+1]['number'] = false;
			$dataPDFCSV[$d+1][0]	= $this->language->_('Total HIT');
			$dataPDFCSV[$d+1][1]	= '';
			$dataPDFCSV[$d+1][2]	= $total;

			$dataPDFCSV[$d+2]['number'] = false;
			$dataPDFCSV[$d+2][0]	= $this->language->_('Average').' HIT';
			$dataPDFCSV[$d+2][1]	= '';
			$dataPDFCSV[$d+2][2]	= $avg;
		}
		else{ $dataPDFCSV = $data = array(); }
		
		
		
		if($csv){
			if(!empty($dataSQL) && count($dataSQL) > 0){
				foreach ($dataSQL as $d => $dt){
					foreach ($fields as $key => $field){
						$value = $dt[$key];
						$data[$d][$key] = $value;
					}
				}
				$dataPDFCSV = $data;
//				$dataPDFCSV[$d+1]['number'] = false;
				$dataPDFCSV[$d+1][0]	= $this->language->_('Total HIT');
				$dataPDFCSV[$d+1][1]	= '';
				$dataPDFCSV[$d+1][2]	= $total;
	
//				$dataPDFCSV[$d+2]['number'] = false;
				$dataPDFCSV[$d+2][0]	= $this->language->_('Average').' HIT';
				$dataPDFCSV[$d+2][1]	= '';
				$dataPDFCSV[$d+2][2]	= $avg;
			}
			else{ $dataPDFCSV = $data = array(); }
			$this->_helper->download->csv($headers,$dataPDFCSV,null,$this->language->_('View Top Hits Statistic'));  
			Application_Helper_General::writeLog('ARTH','Export CSV Top Hits Statistic');
		}
		elseif($pdf){
			$this->_helper->download->pdf($headers,$dataPDFCSV,null,$this->language->_('View Top Hits Statistic'));  
			Application_Helper_General::writeLog('ARTH','Export PDF Top Hits Statistic');
		}
		elseif($this->_request->getParam('print') == 1){
			if(!empty($dataSQL) && count($dataSQL) > 0){
				foreach ($dataSQL as $d => $dt){
					foreach ($fields as $key => $field){
						$value = $dt[$key];
						$data[$d][$key] = $value;
					}
				}
				$dataPDFCSV = $data;
//				$dataPDFCSV[$d+1]['number'] = false;
				$dataPDFCSV[$d+1]['companyname']	= $this->language->_('Total HIT');
				$dataPDFCSV[$d+1][1]	= '';
				$dataPDFCSV[$d+1]['hitperday']	= $total;
	
//				$dataPDFCSV[$d+2]['number'] = false;
				$dataPDFCSV[$d+2]['companyname']	= $this->language->_('Average').' HIT';
				$dataPDFCSV[$d+2][1]	= '';
				$dataPDFCSV[$d+2]['hitperday']	= $avg;
			}
			else{ $dataPDFCSV = $data = array(); }
			
			unset($fields['action']);
			$this->_forward('print', 'index', 'widget', array('data_content' => $dataPDFCSV, 'data_caption' => 'View Top Hits Statistic', 'data_header' => $fields));
			Application_Helper_General::writeLog('ARTH','Print Top Hits Statistic');
		}
		else{	
		
			$stringParam = array( 	'COMP_ID'		=>'fCOMPANYID',
									'COMP_NAME'	=>'fCOMPANYNAME',
									'PS_CREATED'		=>'fDATEFROM',
									'PS_CREATED_END'		=>'fDATETO',
									'ACTION'		=>'fPRIVICODE',
										
									'clearfilter'		=> $clearfilter,
									'filter'			=> $filter,
								);
				
			$this->view->COMPANYID = $fCOMPANYID;
			$this->view->COMPANYNAME = $fCOMPANYNAME;
			$this->view->DATEFROM 	 = $fDATEFROM;
			$this->view->DATETO 	 = $fDATETO;
			$this->view->PRIVICODE 	 = $fPRIVICODE;
			$this->view->SHOWTYPE 	 = $fSHOWTYPE;
				
			$this->view->fields 		= $fields;
			$this->view->filter 		= $filter;
			$this->view->clearfilter	= $clearfilter;
								
			$this->view->sortBy 		= $sortBy;
			$this->view->sortDir 		= $sortDir;
			$this->view->data 			= $data;
							
			$this->view->arrCust 		= $arrCust;
			$this->view->arrFPrivi 		= $arrFPrivi;
			$this->view->arrShowType 	= $arrShowType;
			
			$this->view->total 	= $total;
			$this->view->avg 	= $avg;
		}
		
		Application_Helper_General::writeLog('ARTH','View Top Hits Statistic');

		if(!empty($dataParamValue)){
	    		$this->view->createStart = $dataParamValue['PS_CREATED'];
	    		$this->view->createEnd = $dataParamValue['PS_CREATED_END'];

	    	  	unset($dataParamValue['PS_CREATED_END']);
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     // print_r($whereval);die;
      }
	}
	
	function showType(){
	
		$arr = array(	'1'=>'Daily',
						'2'=>'Weekly',
						'3'=>'Monthly'
					);
		return $arr;
		
	}
	
	function getCustomerCode(){
		
       $select = $this->_db->select()
      	                   ->from(	array('M'			=>'M_CUSTOMER'),
									array('custid'		=>'CUST_ID') )
      	                   ->query()->fetchAll();
		$result =  array();				   
		foreach($select as $key => $val){
			
			$a = $select[$key]["custid"];
			$result[$a] = $a;
			
		}
		return $result;
	  
    }
	
	function getFPriviCode(){
		 $select = $this->_db->select()
      	                   ->from(	array(	'M'			=>'M_FPRIVILEGE'),
									array(	'privicode'	=>'FPRIVI_ID',
											'prividesc'	=>'FPRIVI_DESC') )
      	                   ->query()->fetchAll();
						   
		foreach($select as $key => $val){
			
			$privicode = $select[$key]["privicode"];
			$prividesc = $select[$key]["prividesc"];
			$result[$privicode] = $prividesc;
			
		}
		return $result;
	
	}
	
}
