<?php
require_once 'Zend/Controller/Action.php';

class Bankadminreport_ToptransactionsstatisticController extends Application_Main
{	

	protected $_moduleDB = 'RTF';
	
	public function indexAction() 
	{			
		$this->_helper->layout()->setLayout('newlayout');

		$arrCust 	 = $this->getCustomerCode();
		$arrCcyId    = $this->getCCYId();
		
		$this->view->arrCust 		= $arrCust;
		$this->view->arrCcyId 		= $arrCcyId;
		
		$companyCode = $this->language->_('Company Code');
		$company = $this->language->_('Company');
		$numberOfTx = $this->language->_('Number of Transactions');
		$valueOfTax = $this->language->_('Value of Transactions');
		
		$fields = array(
								// 'companycode'  	=> array(	
								// 						'field' => 'company',
								// 						'label' => $companyCode,
								// 						'sortable' => false
								// 					),
								'company'  	=> array(	
														'field' => 'company',
														'label' => $company,
														'sortable' => false
													),
								'numbertx' 		=> array(
														'field' => 'numbertx',
														'label' => $numberOfTx,
														'sortable' => false
													),
								// 'ccy'				=> array(
								// 							'field' => 'CCY',
								// 							'label' => '',
								// 							'sortable' => true
								// 						),
								'valuetx' 		=> array(
														'field' => 'valuetx',
														'label' => $valueOfTax,
														'sortable' => false
													),
							);

		$filterlist = array('PS_CREATED','COMP_ID','CCY','COMP_NAME');
		
		$this->view->filterlist = $filterlist;
													
		//validasi page, jika input page bukan angka               
		$page 		 	= $this->_getParam('page');
		$page 			= (Zend_Validate::is($page,'Digits'))? $page : 1;
		
		//validasi sort, jika input sort bukan ASC atau DESC
		$sortBy  = $this->_getParam('sortby');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
		
		$this->view->currentPage 	= $page;
		$this->view->sortBy 		= $sortBy;
		$this->view->sortDir 		= $sortDir;
		
		$filterArr = array('COMP_ID'     	=> array('StripTags','StringTrim','StringToUpper'),
						   'COMP_NAME'   	=> array('StripTags','StringTrim','StringToUpper'),
						   'PS_CREATED'  		=> array('StripTags','StringTrim'),
						   'PS_CREATED_END' 		=> array('StripTags','StringTrim'),
						   'CCY' 		=> array('StripTags','StringTrim'),
						    );
		
		// if POST value not null, get post, else get param
		$dataParam = array('COMP_ID','COMP_NAME','CCY');
		$dataParamValue = array();
		
		$clean2 = array_diff( $this->_request->getParam('wherecol'),$dataParam); 
		$dataParam = array_diff( $this->_request->getParam('wherecol'),$clean2); 
		// print_r($dataParam);die;

		// print_r($output);die;
		// print_r($this->_request->getParam('wherecol'));
		foreach ($dataParam as $no => $dtParam)
		{
		
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
				// print_r($dataval);
				$order = 0;
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($value == "PS_CREATED"){
								$order--;
							}
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$order];
						}
						$order++;
					}
				
			}
		}
		
		//echo "<pre>";
		//print_r ($dataParamValue);

		if(!empty($this->_request->getParam('createdate'))){
				$createarr = $this->_request->getParam('createdate');
					$dataParamValue['PS_CREATED'] = $createarr[0];
					$dataParamValue['PS_CREATED_END'] = $createarr[1];
			}
		// The default is set so all fields allow an empty string		
		$options 	= array('allowEmpty' => true);
		$validators = array(
								'COMP_ID' 	=> array(array('InArray', array('haystack' => array_keys($arrCust)))),
								'COMP_NAME' 	=> array(),	
								'PS_CREATED' 		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
								'PS_CREATED_END'		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
								'CCY' 		=> array(array('InArray', array('haystack' => array_keys($arrCcyId)))),	
							);
		
		$zf_filter  		= new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);
		$filter 			= $this->_getParam('filter');
		$csv 				= $this->_getParam('csv');
		$pdf 				= $this->_getParam('pdf');
		
//		$fCOMPANYID      	= $zf_filter->getEscaped('COMPANYID');
//		$fCOMPANYNAME    	= $zf_filter->getEscaped('COMPANYNAME');
		
		$fCOMPANYID      	= $zf_filter->getEscaped('COMP_ID');
		$fCOMPANYNAME    	= $zf_filter->getEscaped('COMP_NAME');
		
		$fCCYID = $zf_filter->getEscaped('CCY');
		
		if($filter === NULL || $filter == "")
		{
			$Y 			= date('Y');
			$d 			= date('d');
			$m 			= date('m',strtotime("-1 month")); //last month
			if($m == '12') $Y = $Y -1;
			$fDATEFROM 	= $d.'/'.$m.'/'.$Y;
			$fDATETO 	= date("d/m/Y");
		}
		else
		{
			if($filter == 'Set Filter')
			{
				$fDATEFROM 	= html_entity_decode($zf_filter->getEscaped('PS_CREATED'));
				$fDATETO 	= html_entity_decode($zf_filter->getEscaped('PS_CREATED_END'));
			}
			else if($filter == 'Clear Filter')
			{
				$fCOMPANYID      = '';
				$fCOMPANYNAME= '';
				$fCCYID    			= '';
				$fDATEFROM 		= "";
				$fDATETO 			= "";
			}
		}
		
								
		$select = $this->_db->select()
							->from(		array(	'P'			=>'T_PSLIP'),array())
										
							->joinLeft( array(	'T'			=>'T_TRANSACTION'),'P.PS_NUMBER = T.PS_NUMBER',
										array(	'numbertx'	=>'COUNT(TRANSACTION_ID)',
												'valuetx'	=>'SUM(TRA_AMOUNT)',
											))
							->joinLeft( array(	'C'=>'M_CUSTOMER'),'P.CUST_ID = C.CUST_ID',
										array(	'companycode'=>'CUST_ID',
												'companyname'=>'CUST_NAME',
												'company'	=> new Zend_Db_Expr("CONCAT(C.CUST_NAME , ' (' , C.CUST_ID , ')  ' )")
											))
							->group(array('C.CUST_ID','C.CUST_NAME'))
							->order('COUNT(TRANSACTION_ID) DESC')
							->where("C.CUST_ID is NOT NULL ");

		if($fCOMPANYID)		{ $select->where('UPPER(C.CUST_ID) = '.$this->_db->quote(strtoupper($fCOMPANYID))); }
		if($fCOMPANYNAME)	{ $select->where('UPPER(C.CUST_NAME) LIKE '.$this->_db->quote(strtoupper('%'.$fCOMPANYNAME.'%'))); }
		if($fCCYID)			{ $select->where('UPPER(P.PS_CCY) = '.$this->_db->quote(strtoupper($fCCYID))); }
		
		if($fDATEFROM)
		{
			$FormatDateFrom = new Zend_Date($fDATEFROM, $this->_dateDisplayFormat);
			$dateFrom   	= $FormatDateFrom->toString($this->_dateDBFormat);	
			$select->where('DATE(P.PS_CREATED) >= '.$this->_db->quote(strtoupper($dateFrom))); 
		}
		
		if($fDATETO)
		{
			$FormatDateTo 	= new Zend_Date($fDATETO, $this->_dateDisplayFormat);
			$dateTo  		= $FormatDateTo->toString($this->_dateDBFormat);	
			$select->where('DATE(P.PS_CREATED) <= '.$this->_db->quote(strtoupper($dateTo))); 
		}
		
		$select->order($sortBy.' '.$sortDir);
		$dataSQL = $this->_db->fetchAll($select);
		
		//-- get total hit  & average -- //
		
		$totalnumbertx = 0;
		$totalvaluetx = 0;
		foreach($dataSQL as $key =>$dt)
		{
				
			$totalnumbertx = $totalnumbertx + $dt["numbertx"];
			$totalvaluetx  = $totalvaluetx + $dt["valuetx"];
				
		}
			
		@$avgnumbertx 	= round($totalnumbertx / count($dataSQL),2);
		@$avgvaluetx 	= round($totalvaluetx / count($dataSQL),2);
			
		//-- end of get total hit & average -- //

		if ($csv || $pdf || $this->_request->getParam('print'))
		{	$headers  = Application_Helper_Array::simpleArray($fields, "label");}
		else
		{
			$this->paging($dataSQL);
			$dataSQL = $this->view->paginator;
		}

//		if(!empty($dataSQL) && count($dataSQL) > 0){
//			
//			foreach ($dataSQL as $d => $dt){					
//				
//				foreach ($fields as $key => $field){
//					$value = $dt[$key];
//					if($key == "valuetx"){ $value = Application_Helper_General::displayMoney($value);}
//					$data[$d][$key] = $value;
//				}
//			}			
//			//count totalhit and average:
//		}
//		else{ $data = array(); }
		
		$totalvaluetx 	= Application_Helper_General::displayMoney($totalvaluetx);
		$avgvaluetx 	= Application_Helper_General::displayMoney($avgvaluetx);
		
		
		if(!empty($dataSQL) && count($dataSQL) > 0){
			
			foreach ($dataSQL as $d => $dt){					
				
				foreach ($fields as $key => $field){
					$value = $dt[$key];
					if($key == "valuetx"){ $value = Application_Helper_General::displayMoney($value);}
					$data[$d][$key] = $value;
				}
			}
			$dataPDFCSV = $data;
			$dataPDFCSV[$d+1]['number'] = false;
			$dataPDFCSV[$d+1][0]	= '';
			$dataPDFCSV[$d+1][1] = $this->language->_('Total Hit');
			$dataPDFCSV[$d+1][2]	= $totalnumbertx;
			$dataPDFCSV[$d+1][3]	= $totalvaluetx;

			$dataPDFCSV[$d+2]['number'] = false;
			$dataPDFCSV[$d+2][0]	= '';
			$dataPDFCSV[$d+2][1]	= $this->language->_('Average').' TX';
			$dataPDFCSV[$d+2][2]	= $avgnumbertx;
			$dataPDFCSV[$d+2][3]	= $avgvaluetx;
		}
		else{ $dataPDFCSV = $data = array(); }
		
			if($csv)
			{
				if(!empty($dataSQL) && count($dataSQL) > 0){
				
				foreach ($dataSQL as $d => $dt){					
					
					foreach ($fields as $key => $field){
						$value = $dt[$key];
						if($key == "valuetx"){ $value = Application_Helper_General::displayMoney($value);}
						$data[$d][$key] = $value;
					}
				}
				$dataPDFCSV = $data;
//				$dataPDFCSV[$d+1]['number'] = false;
				// $dataPDFCSV[$d+1][0]	= '';
				$dataPDFCSV[$d+1]['company'] = $this->language->_('Total Transaction');
				$dataPDFCSV[$d+1]['numbertx']	= $totalnumbertx;
				$dataPDFCSV[$d+1]['valuetx']	= $totalvaluetx;
	
//				$dataPDFCSV[$d+2]['number'] = false;
				// $dataPDFCSV[$d+2][0]	= '';
				$dataPDFCSV[$d+2]['company']	= $this->language->_('Average').' Transaction';
				$dataPDFCSV[$d+2]['numbertx']	= $avgnumbertx;
				$dataPDFCSV[$d+2]['valuetx']	= $avgvaluetx;
			}
			else{ $dataPDFCSV = $data = array(); }

			unset($fields['action']);
			foreach($dataPDFCSV as $key=>$row)
			{
				// $dataPDFCSV[$key]['days'] = Application_Helper_General::convertDate($row['days'], Zend_Date::WEEKDAY);
				$dataPDFCSV[$key]['valuetx']  = 'IDR '.$row['valuetx'];

			}

			$this->_helper->download->csv($headers,$dataPDFCSV,null,$this->language->_('View Top Transactions (Release) Statistic by Company'));  
			Application_Helper_General::writeLog('ARTT','Export CSV Top Transactions (Release) Statistic by Company');
		}
		elseif($pdf)
		{		
			$this->_helper->download->pdf($headers,$dataPDFCSV,null,$this->language->_('View Top Transactions Statistic'));  
			Application_Helper_General::writeLog('ARTT','Export PDF Top Transactions (Release) Statistic by Company');
		}
		elseif($this->_request->getParam('print') == 1){
			if(!empty($dataSQL) && count($dataSQL) > 0){
				foreach ($dataSQL as $d => $dt){					
					
					foreach ($fields as $key => $field){
						$value = $dt[$key];
						if($key == "valuetx"){ $value = Application_Helper_General::displayMoney($value);}
						$data[$d][$key] = $value;
					}
				}
				$dataPDFCSV = $data;
//				$dataPDFCSV[$d+1]['number'] = false;
				$dataPDFCSV[$d+1][0]	= '';
				$dataPDFCSV[$d+1]['company'] = $this->language->_('Total Transaction');
				$dataPDFCSV[$d+1]['numbertx']	= $totalnumbertx;
				$dataPDFCSV[$d+1]['valuetx']	= $totalvaluetx;
	
//				$dataPDFCSV[$d+2]['number'] = false;
				$dataPDFCSV[$d+2][0]	= '';
				$dataPDFCSV[$d+2]['company']	= $this->language->_('Average').' Transaction';
				$dataPDFCSV[$d+2]['numbertx']	= $avgnumbertx;
				$dataPDFCSV[$d+2]['valuetx']	= $avgvaluetx;
			}
			else{ $dataPDFCSV = $data = array(); }
			
			unset($fields['action']);
			foreach($dataPDFCSV as $key=>$row)
			{
				// $dataPDFCSV[$key]['days'] = Application_Helper_General::convertDate($row['days'], Zend_Date::WEEKDAY);
				$dataPDFCSV[$key]['valuetx']  = 'IDR '.$row['valuetx'];

			}

			$this->_forward('print', 'index', 'widget', array('data_content' => $dataPDFCSV, 'data_caption' => 'View Top Transactions Statistic', 'data_header' => $fields));
			Application_Helper_General::writeLog('ARTT','Print Top Transactions (Release) Statistic by Company');
		}
		
		else
		{	
			$stringParam = array( 	'COMP_ID'		=>'fCOMPANYID',
									'COMP_NAME'	=>'fCOMPANYNAME',
									'PS_CREATED'		=>'fDATEFROM',
									'PS_CREATED_END'		=>'fDATETO',
									'CCY'			=>'fCCYID',
									'filter'		=> $filter,
								);
				
			$this->view->COMPANYID 		= $fCOMPANYID;
			$this->view->COMPANYNAME 	= $fCOMPANYNAME;
			$this->view->DATEFROM 	 	= $fDATEFROM;
			$this->view->DATETO 	 	= $fDATETO;
			$this->view->CCYID 	 		= $fCCYID;

			unset($fields['ccy']);
			unset($fields['valuetx']);
			$this->view->fields 		= $fields;
			$this->view->filter 		= $filter;
			$this->view->sortBy 		= $sortBy;
			$this->view->sortDir 		= $sortDir;
			$this->view->data 			= $data;
			$this->view->arrCust 		= $arrCust;
			$this->view->arrCcyId 		= $arrCcyId;
			$this->view->totalnumbertx 	= $totalnumbertx;
			$this->view->totalvaluetx 	= $totalvaluetx;
			$this->view->avgnumbertx 	= $avgnumbertx;
			$this->view->avgvaluetx 	= $avgvaluetx;
		}
		Application_Helper_General::writeLog('ARTT','View Top Transactions (Release) Statistic by Company');

		if(!empty($dataParamValue)){
	    		$this->view->createStart = $dataParamValue['PS_CREATED'];
	    		$this->view->createEnd = $dataParamValue['PS_CREATED_END'];

	    	  	unset($dataParamValue['PS_CREATED_END']);
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     // print_r($whereval);die;
      }
	}
	
	
	function getCustomerCode()
	{
		$select = $this->_db->select()
      	                   ->from(	array('M'			=>'M_CUSTOMER'),
									array('custid'		=>'CUST_ID') )
      	                   ->query()->fetchAll();
		$result =  array();				   
		foreach($select as $key => $val){
			
			$a = $select[$key]["custid"];
			$result[$a] = $a;
			
		}
		return $result;
    }
	
	function getCCYId()
	{
		$select = $this->_db->select()
      	                   ->from(	array(	'M'			=>'M_MINAMT_CCY'),
									array(	'ccyid'		=>'CCY_ID') )
      	                   ->query()->fetchAll();
		
		foreach($select as $key => $val)
		{
			foreach($val as $key1 => $val1)
			{
				$result[$val1] = $val1;
			}
		}
		return $result;
	}
}