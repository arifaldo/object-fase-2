<?php
require_once 'Zend/Controller/Action.php';
class systembalance_RepairController extends systembalance_Model_Systembalance
{
  	protected $_moduleDB = 'RCM';

	public function indexAction()
  	{
  		$this->_helper->layout()->setLayout('popup');
  		$params = $this->_request->getParams();
		
  	   $fields = array('acct'   => array('field'    => 'A.ACCT_NO',
                                        'label'    => $this->language->_('Account'),
                                        'sortable' => true),
    
                    'acct_name'      => array('field'    => 'A.ACCT_NAME',
                                        'label'    => $this->language->_('Account Name'),
                                        'sortable' => true),
    
                    'bodyno' => array('field'    => 'A.CCY_ID',
                                        'label'    => $this->language->_('Currency'),
                                        'sortable' => true),
    
                    'suggestion'   => array('field'    => '',
                                        'label'    => $this->language->_('Set Suggestion'),
                                        'sortable' => true),
    
                    'sys_balance'   => array('field'    => 'B.ISSYSTEMBALANCE',
                                        'label'    => $this->language->_('Set System Balance'),
                                        'sortable' => true),
    
                    'value'   => array('field'    => 'B.PLAFOND',
                                        'label'    => $this->language->_('Tolerance Value'),
                                        'sortable' => true),
    
                    'set_date'   => array('field'    => 'B.ISPLAFONDDATE',
                                        'label'    => $this->language->_('Set Tolerance Date'),
                                        'sortable' => true),
    
                    'start_date'   => array('field'    => 'B.PLAFOND_START',
                                        'label'    => $this->language->_('Tolerance Start Date'),
                                        'sortable' => true),
    
                    'end_date'   => array('field'    => 'B.PLAFOND_END',
                                        'label'    => $this->language->_('Tolerance End Date'),
                                        'sortable' => true)
                   );
		
		//validasi sort, jika input sort bukan ASC atau DESC
		$sortBy  = $this->_getParam('sortby');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
		
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		
  		$keyValue = null;
  		$excludeChangeId  = 'CHANGES_STATUS IN ('.$this->_db->quote($this->_changeStatus['code']['waitingApproval']).','.$this->_db->quote($this->_changeStatus['code']['repairRequested']).')';
  		$excludeChangeId .= ' AND '.$this->_db->quoteInto('CHANGES_FLAG = ?', $this->_applicationFlag);

  		if(is_array($params) && count($params))
  			$fullDesc = Application_Helper_General::displayFullDesc($params);
  		else
  			$fullDesc = null;

  		if(array_key_exists('changes_id', $params))
  		{
  			$filters = array('changes_id' => array('StripTags', 'StringTrim'));
  			$validators = array(
  				'changes_id' => array(
  					'NotEmpty',
  					'Digits',
  					array('Db_RecordExists', array('table' => 'T_GLOBAL_CHANGES', 'field' => 'CHANGES_ID', 'exclude' => $excludeChangeId)),
					array('Db_RecordExists', array('table' => 'TEMP_SET_SYSTEMBALANCE', 'field' => 'CHANGES_ID')),
					'messages' => array(
						$this->language->_('Suggestion ID does not exist.'),
						$this->language->_('Suggestion ID must be number.'),
						$this->language->_('Suggestion ID does not exist.'),
						$this->language->_('Suggestion ID does not exist.'),
					),
  				),
  			);

  			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $params, $this->_optionsValidator);
	  		if($zf_filter_input->isValid())
	  		{
	  			$cust_id = $tempData = null;
	  			$changeId = $zf_filter_input->changes_id;
				
				$cust_id = $this->_db->fetchOne(
	  				$this->_db->select()
	  				->from('TEMP_SET_SYSTEMBALANCE',array('CUST_ID'))
					->where('CHANGES_ID = ?', $changeId)
	  			);
				
				$arrAcctNo = $this->_db->fetchAll(
	  				$this->_db->select()
	  				->from('TEMP_SET_SYSTEMBALANCE',array('ACCT_NO'))
					->where('CHANGES_ID = ?', $changeId)
	  			);
				$checked = Application_Helper_Array::simpleArray($arrAcctNo,'ACCT_NO');
				
				
	  			$tempData = $this->_db->fetchAll(
	  				$this->_db->select()
	  				->from(array('A' => 'M_CUSTOMER_ACCT'),array('ACCT_NO','ACCT_NAME','CCY_ID'))
					->joinLeft(array('B' => 'TEMP_SET_SYSTEMBALANCE'),'A.ACCT_NO = B.ACCT_NO',array('ISSYSTEMBALANCE','PLAFOND','ISPLAFONDDATE','PLAFOND_START','PLAFOND_END'))
	  				->where('A.CUST_ID = ?', $cust_id)
					->order($sortBy.' '.$sortDir)
	  			);

	  			if($cust_id)
				{
	  				$keyValue = $cust_id;
					
					$row = $this->_db->fetchRow(
							$this->_db->select()
									   ->from(array('M_CUSTOMER'),array('CUST_ID','CUST_NAME','CUST_TYPE'))
									   ->where('CUST_ID='.$this->_db->quote($cust_id))
							);
							
					if($row)
					{
						$this->view->comp_id 	= $row['CUST_ID'];
						$this->view->comp_name 	= $row['CUST_NAME'];
						$this->view->comp_type    = $row['CUST_TYPE'];
					}
				}
				
	  			$this->view->temp_data 	= $tempData;
	  			$this->view->checked 	= $checked;
	  			$this->view->changes_id = $changeId;
	  			$this->view->cust_id = $cust_id;
				$this->view->fields 	= $fields;
    			$this->view->modulename = $this->_request->getModuleName();
				
	  			// $this->backendLog('V',$this->_moduleDB,$keyValue,$fullDesc,null);

	  			if($this->_request->isPost())
				{
					$filters = array();
					$validators = array();
					$error_remark = null;
					$checked = $this->_request->getParam('suggest');
					$sysbalance = $this->_request->getParam('sysbalance');
					$tolvalue = $this->_request->getParam('tolvalue');
					$toldate = $this->_request->getParam('toldate');
					$startdate = $this->_request->getParam('startdate');
					$enddate = $this->_request->getParam('enddate');
					if(in_array(1,$checked)) // kalau ada yang dipilih
					{
						
						foreach($checked as $key=>$value)
						{
							if($value==1)
							{				
								// cek kalo set harus ada value
								$temp1 = 'T'; 
								$temp2 = 'T'; 
								if($sysbalance[$key] && !$tolvalue[$key])
									$temp1 = 'F'; 
								if(!$sysbalance[$key] && $tolvalue[$key])
									$temp2 = 'F'; 
							
								$this->_setParam('validate_setvalue_'.$key,$temp1);
								$filters    = array_merge($filters,array('validate_setvalue_'.$key => array('StripTags','StringTrim','StringToUpper')));
								$validators = array_merge($validators,array('validate_setvalue_'.$key => array(array('Identical','T'),
																									'messages' => array(
																										$this->language->_('Invalid amount. Amount must be numeric.'))
																			)));
							
								$this->_setParam('validate_value_'.$key,$temp2);
								$filters    = array_merge($filters,array('validate_value_'.$key => array('StripTags','StringTrim','StringToUpper')));
								$validators = array_merge($validators,array('validate_value_'.$key => array(array('Identical','T'),
																									'messages' => array(
																										$this->language->_('System Balance has to be set if plafond value is set.'))
																			)));
																			
								// cek kalo Set Tolerance Date dicentang
								if($toldate[$key]==1)
								{
									if($sysbalance[$key]) 
										$temp3 = 'T'; 
									else
										$temp3 = 'F'; 
								
									$this->_setParam('validate_date_'.$key,$temp3);
									$filters    = array_merge($filters,array('validate_date_'.$key => array('StripTags','StringTrim','StringToUpper')));
									$validators = array_merge($validators,array('validate_date_'.$key => array(array('Identical','T'),
																										'messages' => array(
																											$this->language->_('Can not set Tolerance Date if System balance is not set.'))
																				)));
									
									$validateDate = new Zend_Validate_Date(array('format' => 'dd/MM/yyyy'));
									if($validateDate->isValid($startdate[$key]))
										$temp4 = 'T'; 
									else
										$temp4 = 'F'; 
								
									$this->_setParam('validate_startdate_'.$key,$temp4);
									$filters    = array_merge($filters,array('validate_startdate_'.$key => array('StripTags','StringTrim','StringToUpper')));
									$validators = array_merge($validators,array('validate_startdate_'.$key => array(array('Identical','T'),
																										'messages' => array(
																											$this->language->_('Invalid Start Date Format.'))
																				)));
																				
									if($validateDate->isValid($enddate[$key]))
										$temp5 = 'T'; 
									else
										$temp5 = 'F'; 
								
									$this->_setParam('validate_enddate_'.$key,$temp5);
									$filters    = array_merge($filters,array('validate_enddate_'.$key => array('StripTags','StringTrim','StringToUpper')));
									$validators = array_merge($validators,array('validate_enddate_'.$key => array(array('Identical','T'),
																										'messages' => array(
																											$this->language->_('Invalid End Date Format.'))
																				)));
								
								}
								else
								{
									if(!($startdate[$key] && $enddate[$key]))
										$temp6 = 'T'; 
									else
										$temp6 = 'F'; 
								
									$this->_setParam('validate_setdate_'.$key,$temp6);
									$filters    = array_merge($filters,array('validate_setdate_'.$key => array('StripTags','StringTrim','StringToUpper')));
									$validators = array_merge($validators,array('validate_setdate_'.$key => array(array('Identical','T'),
																										'messages' => array(
																											$this->language->_('Tolerance Date has to be set if Tolerance Start Date/End Date is set.'))
																				)));
								}
							}
						}
				
						$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
						if($zf_filter_input->isValid())
						{
							try
							{
								$this->_db->beginTransaction();
								
								//DELETE DATA TEMP
								$this->deleteTemp($changeId);
								
								//INSERT DATA TEMP, LOOPING HERE JIKA ACCOUNT YANG DI SUGGEST BANYAK
								foreach($checked as $key=>$value)
								{
									if($value==1)
									{
										$acctName = $this->_db->fetchOne(
														$this->_db->select()
															 ->from(array('M_CUSTOMER_ACCT'),array('ACCT_NAME'))
															 ->where('ACCT_NO='.$this->_db->quote((string)$key))
													);
										$startDate = null;
										$endDate = null;
										$tolValue = null;
										if($tolvalue[$key]) $tolValue = intval(Application_Helper_General::convertDisplayMoney($tolvalue[$key]));
										if($startdate[$key]) $startDate = Application_Helper_General::convertDate($startdate[$key], $this->_dateDBFormat, $this->_dateDisplayFormat);
										if($enddate[$key]) $endDate = Application_Helper_General::convertDate($enddate[$key], $this->_dateDBFormat, $this->_dateDisplayFormat);
										$data = array(	'CHANGES_ID' => $changeId,
															'ACCT_NO' => (string)$key,
															'CUST_ID' => $cust_id,
															'ACCT_NAME' => $acctName,
															'ISSYSTEMBALANCE' => $sysbalance[$key],																				
															'PLAFOND' => $tolValue,
															'ISPLAFONDDATE' => $toldate[$key],
															'PLAFOND_START' => $startDate,
															'PLAFOND_END' => $endDate
														);
										
										$this->_db->insert('TEMP_SET_SYSTEMBALANCE',$data);
									}
								}
								//END LOOP
				
								//UPDATE GLOBAL STATUS
								$this->updateGlobalChanges($changeId);
								// $updateArr = array('CHANGES_STATUS' => 'WA');
								// $where['CHANGES_ID = ?'] = $changeId;
								// $this->_db->update('T_GLOBAL_CHANGES',$updateArr,$where);
				
								 $this->_db->commit();
								
								// $this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), null);
								//$this->view->success = true;
								//$msg = $this->getErrorRemark('00','Repair System Balance');
								//$this->view->report_msg = $msg;
								$this->_redirect('/popup/successpopup');
							}
							catch(Exception $e) 
							{
								//rollback changes
								$this->_db->rollBack();
								$errorMsg = 'Error: Database Process Failed';
								$this->view->error = true;		
								$this->view->report_msg = $errorMsg;
								
								// $this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $errorMsg);
								Application_Log_GeneralLog::technicalLog($e);
								//$this->_redirect($this->_backURL);			
							}
							
							 
						}
						else
						{
							$this->view->suggest = $checked;
							$this->view->sysbalance = $sysbalance;
							$this->view->tolvalue = $tolvalue;
							$this->view->toldate = $toldate;
							$this->view->startdate = $startdate;
							$this->view->enddate = $enddate;
							
							$this->view->error = true;
							$docErr = $this->language->_('Error in processing form values. Please correct values and re-submit.');
							$this->view->report_msg = $docErr;
							
							foreach($zf_filter_input->getMessages() as $key=>$err)
							{
								$this->view->$key = $this->displayError($err);
							}
							
							// $this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $docErr);
						}
					}
					else // kalau tidak ada 
					{
						$this->view->error = true;
						$msg = 'Error: Please checked selection.';
						$this->view->report_msg = $msg;
					}
					
				   //insert log
				   try
				   {
					   $this->_db->beginTransaction();
					  Application_Helper_General::writeLog('BLCR','Repairing System Balance');
					   $this->_db->commit();
				   }
				   catch(Exception $e) 
				   {
					   $this->_db->rollBack();
						Application_Helper_General::writeLog('BLCR','Roll Back Repair System Balance');
				   }
				
				}
				else
				{
					Application_Helper_General::writeLog('BLCR','Viewing Repair System Balance');
				}
	  		}
	  		else
	  		{
	  			$errors = $zf_filter_input->getMessages();
	  			// $errorRemark = Application_Helper_General::getErrorRemark($errors);
	  			// $this->backendLog($actionId,$this->_moduleDB,null,$fullDesc,$errorRemark);
	  			
	  			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
      			$this->_helper->getHelper('FlashMessenger')->addMessage($errors['changes_id']);
      			$this->_redirect($this->_backURL);
	  		}
  		}
  		else
  		{
  			$errorRemark = $this->language->_('Suggestion ID does not exist');
  			// $this->backendLog($actionId,$this->_moduleDB,null,$fullDesc,$errorRemark);
  			
  			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
      		$this->_helper->getHelper('FlashMessenger')->addMessage($errorRemark);
      		$this->_redirect($this->_backURL);
  		}
		
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
	}
}