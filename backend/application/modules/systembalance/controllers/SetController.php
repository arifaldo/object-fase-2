<?php

require_once 'Zend/Controller/Action.php';

class systembalance_SetController extends Application_Main
 { 
    public function indexAction() 
    {
		$listId = $this->_db->select()
						->from(array('M_CUSTOMER'),
							   array('CUST_ID'))
						->order('CUST_ID ASC')
						->query()->fetchAll();
		//Zend_Debug::Dump($listId);die;
       	
		$this->view->listCustId = Application_Helper_Array::listArray($listId,'CUST_ID','CUST_ID');
	}
	
    public function manageAction() 
    {
		$this->_moduleDB = 'SB'; // masih harus diganti
		$cust_id = $this->_getParam('cust_id');
		
  	   $fields = array('acct'   => array('field'    => 'ACCT_NO',
                                        'label'    => 'Account',
                                        'sortable' => false),
    
                    'acct_name'      => array('field'    => 'ACCT_NAME',
                                        'label'    => 'Account Name',
                                        'sortable' => false),
    
                    'bodyno' => array('field'    => 'CCY_ID',
                                        'label'    => 'Currency',
                                        'sortable' => false),
    
                    'suggestion'   => array('field'    => '',
                                        'label'    => 'Set Suggestion',
                                        'sortable' => false),
    
                    'sys_balance'   => array('field'    => 'ISSYSTEMBALANCE',
                                        'label'    => 'Set System Balance',
                                        'sortable' => false),
    
                    'value'   => array('field'    => 'PLAFOND',
                                        'label'    => 'Tolerance Value',
                                        'sortable' => false),
    
                    'set_date'   => array('field'    => 'ISPLAFONDDATE',
                                        'label'    => 'Set Tolerance Date',
                                        'sortable' => false)
                   );
    
		//validasi page, jika input page bukan angka               
		$page = $this->_getParam('page');
		$page = (Zend_Validate::is($page,'Digits'))? $page : 1;
		
		//validasi sort, jika input sort bukan ASC atau DESC
		$sortBy  = $this->_getParam('sortby');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
	
  	    $this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		
        if($cust_id)
        {
			//check apakah company-nya masih ada approval
			$validator = new Zend_Validate_Db_NoRecordExists(
							array(
								'table' => 'TEMP_SET_SYSTEMBALANCE',
								'field' => 'CUST_ID'
							));
			
			if (!$validator->isValid($cust_id))
			{
				$messages = array('No changes allowed for this record while awaiting approval for previous change.');
				$this->view->isapproval = $this->displayError($messages);
			}
			
			$row = $this->_db->fetchRow(
					$this->_db->select()
							   ->from(array('M_CUSTOMER'),array('CUST_ID','CUST_NAME','CUST_TYPE'))
							   ->where('CUST_ID='.$this->_db->quote($cust_id))
					);
					
			if($row)
			{
				$this->view->comp_id 	= $row['CUST_ID'];
				$this->view->comp_name 	= $row['CUST_NAME'];
				$this->view->comp_type    = $row['CUST_TYPE'];
            }
			else $cust_id = null; 
       }// End if cust_id == true
    
        if(!$cust_id)
        {
           $error_remark = 'Company Code does not exist.';
           //insert log
	       try 
	       {
	         $this->_db->beginTransaction();
	         //$this->backendLog($this->_actionID['cekout'],$this->_moduleID['doc'],null,null,$error_remark);
	         $this->_db->commit();
	       }
           catch(Exception $e)
           {
 	          $this->_db->rollBack();
	          Application_Log_GeneralLog::technicalLog($e);
	       }
	    
           $this->_helper->getHelper('FlashMessenger')->addMessage('F');
           $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
           $this->_redirect($this->_helper->url->url(array('module'=>$this->_request->getModuleName(),'controller'=>'index','action'=>'index')));	
        }
    
		$select = $this->_db->select()
					   ->from(array('M_CUSTOMER_ACCT'))
					   ->where('CUST_ID='.$this->_db->quote($cust_id))
						->order($sortBy.' '.$sortDir);
		
        $this->view->cust_id = $cust_id;
		$this->view->fields = $fields;
		$this->view->data = $select->query()->fetchAll();
        $this->view->modulename = $this->_request->getModuleName();
    
        //insert log
        try
        {
	       $this->_db->beginTransaction();
	       //$this->backendLog($this->_actionID['cekin'],$this->_moduleID['doc'],$cust_id,null,null);
           $this->_db->commit();
	    }
	    catch(Exception $e) 
	    {
	       $this->_db->rollBack();
  	       Application_Log_GeneralLog::technicalLog($e);
	    }
	    
		//----jika tekan tombol submit-------
		if($this->_request->isPost())
		{		
			$filters = array();
			$validators = array();
		    $error_remark = null;
			$checked = $this->_request->getParam('suggest');
			$sysbalance = $this->_request->getParam('sysbalance');
			$tolvalue = $this->_request->getParam('tolvalue');
			$toldate = $this->_request->getParam('toldate');
			$startdate = $this->_request->getParam('startdate');
			$enddate = $this->_request->getParam('enddate');
														   
			// $startdate     = (Zend_Date::isDate($this->_request->getParam('startdate'),$this->_dateDisplayFormat))?
									   // new Zend_Date($this->_request->getParam('startdate'),$this->_dateDisplayFormat):
									   // false;
															   
			// $enddate     = (Zend_Date::isDate($this->_request->getParam('enddate'),$this->_dateDisplayFormat))?
									   // new Zend_Date($this->_request->getParam('enddate'),$this->_dateDisplayFormat):
									   // false;
									   
			// Zend_Debug::dump($startdate);
			// Zend_Debug::dump($enddate);
			// Zend_Debug::dump($this->_request->getParam('startdate'));
			// Zend_Debug::dump($this->_request->getParam('enddate'));
			// Zend_Debug::dump($this->_dateDisplayFormat);die;
									   
			if(in_array(1,$checked)) // kalau ada yang dipilih
			{
				
				foreach($checked as $key=>$value)
				{
					if($value==1)
					{				
						$plafond = Application_Helper_General::convertDisplayMoney($tolvalue[$key]);
						$start = implode('', array_reverse(explode('/', $startdate[$key])));
						$end = implode('', array_reverse(explode('/', $enddate[$key])));
						$ongoing = '';
						$row = $this->_db->fetchRow("SELECT ISSYSTEMBALANCE, PLAFOND, ISPLAFONDDATE, PLAFOND_START, PLAFOND_END FROM M_CUSTOMER_ACCT WHERE ACCT_NO='".$key."'");
						$startDB = Application_Helper_General::convertDate($row['PLAFOND_START'], 'yyyyMMdd');
						$endDB = Application_Helper_General::convertDate($row['PLAFOND_END'],'yyyyMMdd');
						// Zend_Debug::dump($startDB.' '.$start);
						// Zend_Debug::dump($endDB.' '.$end);
						// Zend_Debug::dump($plafond.' '.$row['PLAFOND']);die;
						if($row['ISSYSTEMBALANCE'] != $sysbalance[$key] || $row['ISPLAFONDDATE'] != $toldate[$key] || $plafond != $row['PLAFOND'] || $startDB != $start || $endDB != $end)
						{
							if($startDB <= date('Ymd') && $endDB >= date('Ymd'))
							{			
								if($sysbalance[$key]==0)
									$ongoing = '. System Balance CAN NOT be unset.';
								elseif($plafond != $row['PLAFOND'])
									$ongoing = '. Tolerance value can not be changed.';
								elseif($toldate[$key]==0)
									$ongoing = '. Tolerance Date CAN NOT be unset.';
								elseif($startDB != $start)
									$ongoing = ' with Start Date : '.Application_Helper_General::convertDate($row['PLAFOND_START'],'dd MMMM yyyy').'. Start date CAN NOT be changed.';
								elseif($end <= date('Ymd'))
									$ongoing = '. End date must be in the future.';
									
								$temp8 = 'F'; 
							}
							else
								$temp8 = 'T'; 
						}
						else
							$temp8 = 'T'; 
						
						$this->_setParam('validate_ongoing_'.$key,$temp8);
						$filters    = array_merge($filters,array('validate_ongoing_'.$key => array('StripTags','StringTrim','StringToUpper')));
						$validators = array_merge($validators,array('validate_ongoing_'.$key => array(array('Identical','T'),
																							'messages' => array(
																								'Account has ongoing Tolerance'.$ongoing)
																	)));
																		
						// cek kalo set harus ada value
						$temp1 = 'T'; 
						$temp2 = 'T'; 
						if($sysbalance[$key] && !$tolvalue[$key])
							$temp1 = 'F'; 
						if(!$sysbalance[$key] && $tolvalue[$key]!=0)
							$temp2 = 'F'; 
					
						$this->_setParam('validate_setvalue_'.$key,$temp1);
						$filters    = array_merge($filters,array('validate_setvalue_'.$key => array('StripTags','StringTrim','StringToUpper')));
						$validators = array_merge($validators,array('validate_setvalue_'.$key => array(array('Identical','T'),
																							'messages' => array(
																								'Invalid amount. Amount must be numeric.')
																	)));
					
						$this->_setParam('validate_value_'.$key,$temp2);
						$filters    = array_merge($filters,array('validate_value_'.$key => array('StripTags','StringTrim','StringToUpper')));
						$validators = array_merge($validators,array('validate_value_'.$key => array(array('Identical','T'),
																							'messages' => array(
																								'System Balance has to be set if plafond value is set.')
																	)));
																	
						// cek kalo Set Tolerance Date dicentang
						if($toldate[$key]==1)
						{
							if($sysbalance[$key]) 
								$temp3 = 'T'; 
							else
								$temp3 = 'F'; 
						
							$this->_setParam('validate_date_'.$key,$temp3);
						    $filters    = array_merge($filters,array('validate_date_'.$key => array('StripTags','StringTrim','StringToUpper')));
						    $validators = array_merge($validators,array('validate_date_'.$key => array(array('Identical','T'),
																								'messages' => array(
																									'Can not set Tolerance Date if System balance is not set.')
																		)));
							
							$validateDate = new Zend_Validate_Date(array('format' => 'dd/MM/yyyy'));
							if($validateDate->isValid($startdate[$key]))
								$temp4 = 'T'; 
							else
								$temp4 = 'F'; 
						
							$this->_setParam('validate_startdate_'.$key,$temp4);
						    $filters    = array_merge($filters,array('validate_startdate_'.$key => array('StripTags','StringTrim','StringToUpper')));
						    $validators = array_merge($validators,array('validate_startdate_'.$key => array(array('Identical','T'),
																								'messages' => array(
																									'Invalid Start Date Format.')
																		)));
																		
							if($validateDate->isValid($enddate[$key]))
								$temp5 = 'T'; 
							else
								$temp5 = 'F'; 
						
							$this->_setParam('validate_enddate_'.$key,$temp5);
						    $filters    = array_merge($filters,array('validate_enddate_'.$key => array('StripTags','StringTrim','StringToUpper')));
						    $validators = array_merge($validators,array('validate_enddate_'.$key => array(array('Identical','T'),
																								'messages' => array(
																									'Invalid End Date Format.')
																		)));
							
							// validasi start date harus < end date							
							if($start < $end)
								$temp7 = 'T'; 
							else
								$temp7 = 'F'; 
						
							$this->_setParam('validate_comparedate_'.$key,$temp7);
						    $filters    = array_merge($filters,array('validate_comparedate_'.$key => array('StripTags','StringTrim','StringToUpper')));
						    $validators = array_merge($validators,array('validate_comparedate_'.$key => array(array('Identical','T'),
																								'messages' => array(
																									'End date must be in the future. Start date must be less than End date.')
																		)));
																								
						}
						else
						{
							if(!($startdate[$key] && $enddate[$key]))
								$temp6 = 'T'; 
							else
								$temp6 = 'F'; 
						
							$this->_setParam('validate_setdate_'.$key,$temp6);
						    $filters    = array_merge($filters,array('validate_setdate_'.$key => array('StripTags','StringTrim','StringToUpper')));
						    $validators = array_merge($validators,array('validate_setdate_'.$key => array(array('Identical','T'),
																								'messages' => array(
																									'Tolerance Date has to be set if Tolerance Start Date/End Date is set.')
																		)));
						}
					}
				}
				
				$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
				if($zf_filter_input->isValid())
				{
					try
					{
						$info = 'Customer ID = '.$cust_id;	
						$this->_db->beginTransaction();
				
						$custName = $this->_db->fetchOne(
									$this->_db->select()
											->from(array('M_CUSTOMER'),
												   array('CUST_NAME')
												   )
											->where("CUST_ID = ?", $cust_id)
									);
						
						// insert ke T_GLOBAL_CHANGES
						$change_id = $this->suggestionWaitingApproval('System Balance',$info,$this->_changeType['code']['edit'],null,'M_CUSTOMER_ACCT','TEMP_SET_SYSTEMBALANCE',$cust_id,$custName,$cust_id);
						
						//LOOPING HERE JIKA ACCOUNT YANG DI SUGGEST BANYAK
						foreach($checked as $key=>$value)
						{
							if($value==1)
							{
								$acctName = $this->_db->fetchOne(
												$this->_db->select()
												     ->from(array('M_CUSTOMER_ACCT'),array('ACCT_NAME'))
												     ->where('ACCT_NO='.$this->_db->quote((string)$key))
											);
								$startDate = null;
								$endDate = null;
								$tolValue = null;
								if($tolvalue[$key]) $tolValue = Application_Helper_General::convertDisplayMoney($tolvalue[$key]);
								if($startdate[$key]) $startDate = Application_Helper_General::convertDate($startdate[$key], $this->_dateDBFormat, $this->_dateDisplayFormat);
								if($enddate[$key]) $endDate = Application_Helper_General::convertDate($enddate[$key], $this->_dateDBFormat, $this->_dateDisplayFormat);
								$data = array(	'CHANGES_ID' => $change_id,
													'ACCT_NO' => (string)$key,
													'CUST_ID' => $cust_id,
													'ACCT_NAME' => $acctName,
													'ISSYSTEMBALANCE' => $sysbalance[$key],																				
													'PLAFOND' => $tolValue,
													'ISPLAFONDDATE' => $toldate[$key],
													'PLAFOND_START' => $startDate,
													'PLAFOND_END' => $endDate
												);
												//Zend_Debug::Dump($data);die;
								$this->_db->insert('TEMP_SET_SYSTEMBALANCE',$data);
							}
						}
						//END LOOP
						 $this->_db->commit();
						Application_Helper_General::writeLog('BLUD','Updating System Balance');
						$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/');
						// $this->_redirect('/notification/success');
						$this->_redirect('/notification/submited/index');
					}
					catch(Exception $e) 
					{
						//rollback changes
						$this->_db->rollBack();
						$errorMsg = 'Error: Database Process Failed';
						$this->view->error = true;		
						$this->view->report_msg = $errorMsg;
						Application_Helper_General::writeLog('BLUD','Roll Back Updating System Balance');
					}
					
					 
				}
				else
				{		
					$this->view->suggest = $checked;
					$this->view->sysbalance = $sysbalance;
					$this->view->tolvalue = $tolvalue;
					$this->view->toldate = $toldate;
					$this->view->startdate = $startdate;
					$this->view->enddate = $enddate;
					
					$this->view->error = true;
					$docErr = 'Error in processing form values. Please correct values and re-submit.';
					$this->view->report_msg = $docErr;
					
					foreach($zf_filter_input->getMessages() as $key=>$err)
					{
						$this->view->$key = $this->displayError($err);
					}
					
					// $this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $docErr);
				}
			}
			else // kalau tidak ada 
			{
				$this->view->error = true;
				$msg = 'Error: Please checked selection.';
				$this->view->report_msg = $msg;
			}
			
		   //insert log
		   try
		   {
			   $this->_db->beginTransaction();
			   //$this->backendLog('CO', $this->_moduleDB,null,null,$error_remark);
			   $this->_db->commit();
		   }
		   catch(Exception $e) 
		   {
			   $this->_db->rollBack();
			   Application_Log_GeneralLog::technicalLog($e);
		   }
		   
		}
		else
		{
			Application_Helper_General::writeLog('BLUD','Viewing Update System Balance');
		}
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
   }  
}