<?php
require_once 'Zend/Controller/Action.php';
class systembalance_SuggestiondetailController extends Application_Main
{
	protected $_moduleDB = 'RCM';

  	public function indexAction()
  	{
  		$this->_helper->layout()->setLayout('popup');
  		$params = $this->_request->getParams();
		
		$fields = array('acct'   => array('field'    => 'ACCT_NO',
                                        'label'    => $this->language->_('Account No'),
                                        'sortable' => true),
    
                    'acct_name'      => array('field'    => 'ACCT_NAME',
                                        'label'    => $this->language->_('Account Name'),
                                        'sortable' => true),
    
                    'bodyno' => array('field'    => 'CCY_ID',
                                        'label'    => $this->language->_('Currency'),
                                        'sortable' => true),
    
                    'sys_balance'   => array('field'    => 'ISSYSTEMBALANCE',
                                        'label'    => $this->language->_('System Balance'),
                                        'sortable' => true),
    
                    'value'   => array('field'    => 'PLAFOND',
                                        'label'    => $this->language->_('Tolerance'),
                                        'sortable' => true),
    
                    'set_date'   => array('field'    => 'ISPLAFONDDATE',
                                        'label'    => $this->language->_('Tolerance Date'),
                                        'sortable' => true),
    
                    'start_date'   => array('field'    => 'PLAFOND_START',
                                        'label'    => $this->language->_('Tolerance Start Date'),
                                        'sortable' => true),
    
                    'end_date'   => array('field'    => 'PLAFOND_END',
                                        'label'    => $this->language->_('Tolerance End Date'),
                                        'sortable' => true)
                   );
		
		//validasi sort, jika input sort bukan ASC atau DESC
		$sortBy  = $this->_getParam('sortby');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
		
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		
  		$keyValue = null;
  		$excludeChangeId  = 'CHANGES_STATUS IN ('.$this->_db->quote($this->_changeStatus['code']['waitingApproval']).','.$this->_db->quote($this->_changeStatus['code']['repairRequested']).')';
  		$excludeChangeId .= ' AND '.$this->_db->quoteInto('CHANGES_FLAG = ?', $this->_applicationFlag);

  		if(is_array($params) && count($params))
  			$fullDesc = Application_Helper_General::displayFullDesc($params);
  		else
  			$fullDesc = null;

  		if(array_key_exists('changes_id', $params))
  		{
  			$filters = array('changes_id' => array('StripTags', 'StringTrim'));
  			$validators = array(
  				'changes_id' => array(
  					'NotEmpty',
  					'Digits',
					'messages' => array(
						$this->language->_('Suggestion ID does not exist.'),
						$this->language->_('Suggestion ID must be number.'),
					),
  				),
  			);

  			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $params, $this->_optionsValidator);
	  		if($zf_filter_input->isValid())
	  		{
	  			$changeId = $zf_filter_input->changes_id;
				
				$changeInfo = $this->_db->fetchRow(
						$this->_db->select()
							->from(array('A' => 'T_GLOBAL_CHANGES'),array('CREATED','CREATED_BY'))
						->where('CHANGES_ID = ?', $changeId)
						->where("CHANGES_STATUS = 'WA' OR CHANGES_STATUS = 'RR'")
					);
					
				if(!$changeInfo)
				{
					$this->_redirect('/notification/invalid/index');
				}
				
				$cust_id = $tempData = $masterData = null;
	  			$tempData = $this->_db->fetchAll(
	  				$this->_db->select()
	  				->from(array('A' => 'TEMP_SET_SYSTEMBALANCE'))
					->joinLeft(array('B' => 'M_CUSTOMER_ACCT'),'A.ACCT_NO = B.ACCT_NO',array('CCY_ID'))
	  				->where('CHANGES_ID = ?', $changeId)
					->order($sortBy.' '.$sortDir)
	  			);
				
				if(isset($tempData[0]['CUST_ID']))
					$cust_id = $tempData[0]['CUST_ID'];

	  			if($cust_id)
				{
	  				$keyValue = $cust_id;

					$masterData = $this->_db->fetchAll(
						$this->_db->select()
						->from('M_CUSTOMER_ACCT',array('ACCT_NO','ACCT_NAME','CCY_ID','ISSYSTEMBALANCE','PLAFOND','ISPLAFONDDATE','PLAFOND_START','PLAFOND_END'))
						->where('CUST_ID = ?', $cust_id)
						->order($sortBy.' '.$sortDir)
					);
				}
				
	  			$this->view->suggested_by = $changeInfo['CREATED_BY'];
	  			$this->view->suggestion_date = $changeInfo['CREATED'];

	  			$this->view->temp_data 	= $tempData;
	  			$this->view->master_data 	= $masterData;
	  			$this->view->changes_id = $changeId;
	  			$this->view->cust_id = $cust_id;
				$this->view->fields 	= $fields;
				
	  			// $this->backendLog('V',$this->_moduleDB,$keyValue,$fullDesc,null);
	  		}
	  		else
	  		{
	  			$errors = $zf_filter_input->getMessages();
	  			// $errorRemark = Application_Helper_General::displayErrorRemark($errors);
	  			// $this->backendLog('V',$this->_moduleDB,null,$fullDesc,$errorRemark);

	  			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
	  			$this->_helper->getHelper('FlashMessenger')->addMessage($errors['changes_id']);
      	//		$this->_redirect('/popuperror');
	  		}
  		}
  		else
  		{
  			$errorRemark = 'Suggestion ID does not exist.';
			// $this->backendLog('V',$this->_moduleDB,null,$fullDesc,$errorRemark);

	  		$this->_helper->getHelper('FlashMessenger')->addMessage('F');
	  		$this->_helper->getHelper('FlashMessenger')->addMessage($errorRemark);
      	//	$this->_redirect('/popuperror');
    	}
		Application_Helper_General::writeLog('BLCL','Viewing System Balance Change List');
	}
}