<?php
require_once 'Zend/Controller/Action.php';

class monthlycharges_IndexController extends Application_Main
{
	public function indexAction()
	{

		$setting = new Settings();			  	
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
		$pw_hash = md5($enc_salt.$enc_pass);
		$rand = $this->_userIdLogin.date('dHis').$pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;
	
		$this->_helper->layout()->setLayout('newlayout');
		$select = $this->_db->select()->distinct()
					->from(array('M_CUSTOMER'),array('CUST_ID'))
					->where("CUST_STATUS != '3'")
					->order('CUST_ID ASC')
				 	-> query() ->fetchAll();
		//Zend_Debug::dump($status); die;
		$this->view->var=$select;

		/*$select3 = $this->_db->select()
				->from(array('B' => 'M_CHARGES_WITHIN'),array('*'));
		$coba = $this->_db->fetchAll($select3);
		Zend_Debug::dump($select3); die;*/
		$chargestype = array_combine(array_values($this->_monthlytype['code']),array_values($this->_monthlytype['desc']));
		// print_r($chargestype);die();
		$list = array(	''				=>	'--- '.$this->language->_('Any Value').' ---',
						'Enabled'		=>	$this->language->_('Enabled'),
						'Disabled' 		=> 	$this->language->_('Disabled'));
		$this->view->optionlist = $list;

		$ceklist = array(	'0'		=>	$this->language->_('Disabled'),
							'1' 	=> 	$this->language->_('Enabled'));

		$companyCode = $this->language->_('Company Code');
		$companyName = $this->language->_('Company');
		$monthlyfee = $this->language->_('Monthy Fee Type');
		$companyacct = $this->language->_('Company Account');
		$company_name = $this->language->_('Company Name');
		$monthlyFee = $this->language->_('Monthly Fee');
		$suggestedDate = $this->language->_('Lastest Suggested');
		$suggester = $this->language->_('Suggester');
		$tra_from = $this->language->_('Number trx From');
		$transto = $this->language->_('Number trx To');
		$chargesTo = $this->language->_('Charges Account');
		$approvedDate = $this->language->_('Lastest Approved');
		$fields = array	(
							// 'Company Code'  			=> array	(
							// 										'field' => 'B.CUST_ID',
							// 										'label' => $companyCode,
							// 										'sortable' => true
							// 									),
							'Company'  			=> array	(
																	'field' => 'COMPANY',
																	'label' => $companyName,
																	'sortable' => true
																),
							'MonthlyFee'  			=> array	(
																	'field' => 'CUST_MONTHLYFEE_TYPE',
																	'label' => $monthlyfee,
																	'sortable' => true
																),
							'CompanyAct'  			=> array	(
																	'field' => 'ACCT_NO',
																	'label' => $companyacct,
																	'sortable' => true
																),
							'NumberTrans'  			=> array	(
																	'field' => 'TRA_FROM',
																	'label' => $tra_from,
																	'sortable' => true
																),
							'TransTo'  			=> array	(
																	'field' => 'TRA_TO',
																	'label' => $transto,
																	'sortable' => true
																),
							'Charges'  					=> array	(
																	'field' => 'AMOUNT',
																	'label' => $monthlyFee,
																	'sortable' => true
																),
							'ChargesTo'  		=> array	(
																	'field' => 'CHARGES_ACCT_NO',
																	'label' => $chargesTo,
																	'sortable' => true
																),
							'Suggest Date'  		=> array	(
																	'field' => 'SUGGEST_DATE',
																	'label' => $suggestedDate,
																	'sortable' => true
																),
							'Approve Date'  		=> array	(
																	'field' => 'APPROVE_DATE',
																	'label' => $approvedDate,
																	'sortable' => true
																),
						);

		$filterlist = array("CUST_ID","MONTHLY_FEE_STATUS","CUST_NAME","SUGGESTOR");
		$this->view->filterlist = $filterlist;

		

		$page = $this->_getParam('page');
		$csv  = $this->_getParam('csv');
		$pdf  = $this->_getParam('pdf');

		$page = (Zend_Validate::is($page,'Digits'))? $page : 1;
		$sortBy  = $this->_getParam('sortby');
   		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
   		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';

		$filterArr = array('filter' 			=> array('StripTags','StringTrim'),
	                       'CUST_ID'    			=> array('StripTags','StringTrim'),
	                       'CUST_NAME'  			=> array('StripTags','StringTrim','StringToUpper'),
						   'SUGGESTOR'  		=> array('StripTags','StringTrim','StringToUpper'),
						   // 'chargeStatus'  		=> array('StripTags','StringTrim'),
						   'MONTHLY_FEE_STATUS'  	=> array('StripTags','StringTrim'),
						   // 'fDateFrom'  		=> array('StripTags','StringTrim'),
						   // 'fDateTo'  			=> array('StripTags','StringTrim'),
	                      );

	    $validator = array('filter' 			=> array(),
	                       'CUST_ID'    			=> array(),
	                       'CUST_NAME'  			=> array(),
						   'SUGGESTOR'  		=> array(),
						   // 'chargeStatus'  		=> array(),
						   'MONTHLY_FEE_STATUS'  	=> array(),
						   // 'fDateFrom'  		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
						   // 'fDateTo'  			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
	                      );



	    // $dataParam = array("CLR_CODE","SWIFT_CODE","BANK_NAME");
	    $dataParam = array("CUST_ID","MONTHLY_FEE_STATUS","CUST_NAME","SUGGESTOR");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{
			
			// print_r($dtParam);die;
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}
				
			}
				
			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}


	    $zf_filter = new Zend_Filter_Input($filterArr,$validator,$dataParamValue);
	    $filter = $this->_getParam('filter');

	    
		//Zend_Debug::dump($this->_masterglobalstatus); die;

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;

		/*if($filter == null)
		{	$datefrom = (date("d/m/Y"));
			$dateto = (date("d/m/Y"));
			$this->view->fDateFrom  = (date("d/m/Y"));
			$this->view->fDateTo  = (date("d/m/Y"));
		//Zend_Debug::dump($this->view->fDateFrom); die;
		}*/

		$chargeStatusarr = "(CASE B.CUST_CHARGES_STATUS ";
  		foreach($ceklist as $key=>$val)
  		{
   			$chargeStatusarr .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$chargeStatusarr .= " END)";

  		$adminfeeStatusarr = "(CASE B.CUST_MONTHLYFEE_STATUS ";
  		foreach($ceklist as $key=>$val)
  		{
   			$adminfeeStatusarr .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$adminfeeStatusarr .= " END)";


  		$monthlyfeearr = "(CASE B.CUST_MONTHLYFEE_TYPE ";
  		foreach($chargestype as $key=>$val)
  		{
   			$monthlyfeearr .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$monthlyfeearr .= " END)";

		$select2 = $this->_db->select()
						->from(array('B' => 'M_CUSTOMER'),array('CUST_IDS' => 'B.CUST_ID',
																'B.CUST_NAME',
																'CUST_MONTHLYFEE_TYPE' => $monthlyfeearr,
																'CUST_CHARGES_STATUS' => $chargeStatusarr,
																'CUST_MONTHLYFEE_STATUS' => $adminfeeStatusarr,
																'B.CUST_SUGGESTED',
																'COMPANY'	=> new Zend_Db_Expr("CONCAT(B.CUST_NAME , ' (' , B.CUST_ID , ')  ' )"),
																'B.CUST_SUGGESTEDBY'))
						->joinleft(array('D' => 'M_CUSTOMER_ACCT'), 'B.CUST_ID = D.CUST_ID',array('CUSTACCT_NO' => 'D.ACCT_NO'))
						->joinleft(array('A' => 'M_CHARGES_MONTHLY'), 'A.CUST_ID = B.CUST_ID AND B.CUST_MONTHLYFEE_TYPE = A.MONTHLYFEE_TYPE AND D.ACCT_NO = A.ACCT_NO',array('*'))
						->joinleft(array('C' => 'M_ADMFEE_MONTHLY'), 'C.CUST_ID = B.CUST_ID AND B.CUST_MONTHLYFEE_TYPE = C.MONTHLYFEE_TYPE AND C.ACCT_NO = A.ACCT_NO',array('ACCT' => 'C.ACCT_NO','AMOUNTS'=>'C.AMOUNT','CHARGES_ACCT'=>'C.CHARGES_ACCT_NO'))
						   
						->where("CUST_STATUS != '3'")
						->where("B.CUST_MONTHLYFEE_STATUS = '1'")
						//keperluan demo 28 feb 2020
						->where("CUST_MONTHLYFEE_TYPE != '4'") ;
						//->group("B.CUST_ID");
						//keperluan demo 28 feb 2020

						 //echo $select2;
		$arr = $this->_db->fetchAll($select2);


		$selectglobal = $this->_db->select()
						->from(array('B' => 'M_CHARGES_MONTHLY'),array('*'))
						->where("CUST_ID = ? ",'GLOBAL');
		$arrglobal = $this->_db->fetchAll($selectglobal);		
		$this->view->arrglobal = $arrglobal;		
		// echo "<pre>";
		// print_r($arr);die;
		//if($filter == null || $filter == 'Set Filter')
		if($filter == true)
		{
			
			$custid = html_entity_decode($zf_filter->getEscaped('CUST_ID'));
			$custname = html_entity_decode($zf_filter->getEscaped('CUST_NAME'));
			$suggestor = html_entity_decode($zf_filter->getEscaped('SUGGESTOR'));
			$chargeStatus = html_entity_decode($zf_filter->getEscaped('chargeStatus'));
			$monthlyfeeStatus = html_entity_decode($zf_filter->getEscaped('MONTHLY_FEE_STATUS'));
			$datefrom = html_entity_decode($zf_filter->getEscaped('fDateFrom'));
			$dateto = html_entity_decode($zf_filter->getEscaped('fDateTo'));
			
			$this->view->fDateTo    = $dateto;
			$this->view->fDateFrom  = $datefrom;
			//Zend_Debug::dump($custid); die;

			if(!empty($datefrom))
		            {
		            	$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
						$datefrom  = $FormatDate->toString($this->_dateDBFormat);
		            }

		    if(!empty($dateto))
		            {
		            	$FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
						$dateto    = $FormatDate->toString($this->_dateDBFormat);
						//Zend_Debug::dump($dateto); die;
		            }

			if(!empty($datefrom) && empty($dateto))
		            $select2->where("DATE(B.CUST_SUGGESTED) >= ".$this->_db->quote($datefrom));

		   	if(empty($datefrom) && !empty($dateto))
		            $select2->where("DATE(B.CUST_SUGGESTED) <= ".$this->_db->quote($dateto));

		    if(!empty($datefrom) && !empty($dateto))
		            $select2->where("DATE(B.CUST_SUGGESTED) between ".$this->_db->quote($datefrom)." and ".$this->_db->quote($dateto));

		   	if($chargeStatus)
			{
				$this->view->chargeStatus = $chargeStatus;
				if($chargeStatus == "Enabled")
				{
					$cekchargeStatus = "1";
				}
				if($chargeStatus == "Disabled")
				{
					$cekchargeStatus = "0";
				}
				$select2->where("B.CUST_CHARGES_STATUS LIKE ".$this->_db->quote($cekchargeStatus));
			}

			if($monthlyfeeStatus)
			{
				$this->view->adminfeeStatus = $monthlyfeeStatus;
				if($monthlyfeeStatus == "Enabled")
				{
					$cekadminfeeStatus = "1";
				}
				if($monthlyfeeStatus == "Disabled")
				{
					$cekadminfeeStatus = "0";
				}
				$select2->where("CUST_MONTHLYFEE_STATUS LIKE ".$this->_db->quote($cekadminfeeStatus));
			}

		    if($custid)
		    {
	       		$this->view->cekcustid = $custid;
	       		$select2->where("B.CUST_ID LIKE ".$this->_db->quote($custid));
		    }

			if($custname)
			{
	       		$this->view->custname = $custname;
	       		$select2->where("B.CUST_NAME LIKE ".$this->_db->quote('%'.$custname.'%'));
			}

			if($suggestor)
		    {
	       		$this->view->suggestor = $suggestor;
	       		$select2->where("B.CUST_SUGGESTEDBY LIKE ".$this->_db->quote('%'.$suggestor.'%'));
		    }
		}

		
		// echo '<pre>';
		// echo $select2;

		//$select2->order($sortBy.' '.$sortDir);
		$select2->order($sortBy.' '.$sortDir);
    	$this->paging($select2,30);
    	$this->view->fields = $fields;
    	$this->view->filter = $filter;

    	

    	if($csv || $pdf)
    	{
    		$header = Application_Helper_Array::simpleArray($fields, "label");
    		$arr = $this->_db->fetchAll($select2);

    		foreach($arr as $key=>$value)
			{
				$arr[$key]["CUST_SUGGESTED"] = Application_Helper_General::convertDate($value["CUST_SUGGESTED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
			}

	    	if($csv)
			{
				Application_Helper_General::writeLog('CHLS','Download CSV Company Charges List');
				//Zend_Debug::dump($arr); die;
				$this->_helper->download->csv($header,$arr,null,$this->language->_('Company Charges and Administration Fee'));
			}

			if($pdf)
			{
				Application_Helper_General::writeLog('CHLS','Download PDF Company Charges List');
				$this->_helper->download->pdf($header,$arr,null,$this->language->_('Company Charges and Administration Fee'));
			}
    	}
    	else
    	{
    		Application_Helper_General::writeLog('CHLS','View Company Charges List');
    	}

    	if($this->_request->getParam('print') == 1){
			$this->_forward('print', 'index', 'widget', array('data_content' => $arr, 'data_caption' => 'Monthly Charges', 'data_header' => $fields));
		}


  //   	$this->view->fields = $fields;
		// $this->view->filter = $filter;
		if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = array_unique($wherecol);
        $this->view->whereval     = array_unique($whereval);
     
      }


	}
}
