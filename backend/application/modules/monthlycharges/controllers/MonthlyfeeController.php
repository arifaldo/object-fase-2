<?php


require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class monthlycharges_MonthlyfeeController extends Application_Main
{
	public function indexAction() 
	{						
		$this->_helper->layout()->setLayout('newlayout');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
	    $password = $sessionNamespace->token; 
	    $this->view->token = $sessionNamespace->token;


	    $AESMYSQL = new Crypt_AESMYSQL();
	    $custid = $AESMYSQL->decrypt($this->_getParam('custid'), $password);
	    $custid = (Zend_Validate::is($custid,'Alnum') && Zend_Validate::is($custid,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $custid : null;
	    
		$this->view->custid = $custid;


		$AESMYSQL = new Crypt_AESMYSQL();
		$rand = $password;

		$encrypted_custid = $AESMYSQL->encrypt($custid, $rand);
		$enccustid = urlencode($encrypted_custid);

		$monthlytype = $this->_getParam('monthlytype');

		$charge = $this->_getParam('chargetype');
		// var_dump($charge)
		$this->view->charge = $charge;
		
		$selectcust = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER'),array('*'));
		$selectcust -> where("A.CUST_ID LIKE ".$this->_db->quote('%'.$custid.'%'));
		$result = $selectcust->query()->FetchAll();

		$this->view->result = $result;

		$chargestype = array_combine(array_values($this->_monthlytype['code']),array_values($this->_monthlytype['desc']));
		// var_dump($result);die;
		if(!empty($monthlytype)){
	    	$result['0']['CUST_MONTHLYFEE_TYPE'] = $monthlytype;
	    }
		$this->view->monthlyfee = $result[0]['CUST_MONTHLYFEE_TYPE'];
		$this->view->monthlyarr = $chargestype;
		 $selectccy = $this->_db->select()
             ->from(array('A' => 'M_MINAMT_CCY'), array('CCY_ID'));
            
	    $listccy = $selectccy->query()->fetchAll();
	    $paramccy = array();
	    $paramccy['IDR'] = 'IDR';
	    foreach ($listccy as $key => $value) {
	    	if ($value['CCY_ID'] == 'IDR') {
	    		continue;
	    	}
	    	else{
	    		$paramccy[$value['CCY_ID']] = $value['CCY_ID'];
	    	}
	    }
	    
	    // unset($paramccy['IDR']);
	    // array_unshift($paramccy, array('IDR' => 'IDR'));

	    $this->view->dataccy = $paramccy;


	    $selectacc = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER_ACCT'),array('A.ACCT_NO', 'A.CCY_ID', 'A.ACCT_NAME'));
		$selectacc -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		$selectacc -> where("A.ACCT_SOURCE NOT LIKE '1'");
		$selectacc -> where("A.ACCT_SOURCE IS NOT NULL");
		
		$resultacc = $selectacc->query()->FetchAll();
		$this->view->resultaccount = $resultacc;
		
		foreach($resultacc as $row)
		{	
			$acctno = $row['ACCT_NO'];
			$select4 = $this->_db->select()->distinct()
			        	->from(array('A' => 'M_CHARGES_WITHIN'),array('*'));
			$select4 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
			$select4 -> where("A.ACCT_NO LIKE ".$this->_db->quote($acctno));
			$select4 -> where("BUSINESS_TYPE = '1'");
			$result4 = $select4->query()->FetchAll();

			if($select4)
			{
				foreach($result4 as $row2)
				{	
   					$idamt = $row2['ACCT_NO'].'amount';
					$amt = Application_Helper_General::convertDisplayMoney($row2['AMOUNT']);
					if($amt == null)
					{
						$amt = '0.00';
					}
					$this->view->$idamt = $amt;
				}
			}
		}


		$sqlQueryHoliday = "
			SELECT 
				CHARGE_ID,
				CCY_ID,
				CHARGE_AMT,
				CHARGE_AMOUNT_CCY,
				CHARGE_PROV_MIN_AMT,
				CHARGE_TYPE,
				CHARGE_PROV_MAX_AMT,
				CHARGE_PCT 
			FROM `M_MINAMT_CCY` A
			LEFT JOIN
			(SELECT * 
				FROM M_CHARGES_REMITTANCE  
				WHERE CHARGE_TYPE = ?
				AND CUST_ID = ?) B
			ON A.`CCY_ID` = B.`CHARGE_CCY`";
			//ORDER BY CHARGE_ID DESC";
		// $custid = 'GLOBAL';
      $transfee = $this->_db->fetchAll($sqlQueryHoliday,array('3',$custid));

        		if(empty($transfee['0']['CHARGE_ID'])){
				 	// die;
				 	 $transfee = $this->_db->fetchAll($sqlQueryHoliday,array('3','GLOBAL'));
				 }
 				$fullfee = $this->_db->fetchAll($sqlQueryHoliday,array('4',$custid));
		       if(empty($fullfee['0']['CHARGE_ID'])){
		       	 $fullfee = $this->_db->fetchAll($sqlQueryHoliday,array('4','GLOBAL'));
		       }
		        $prfee = $this->_db->fetchAll($sqlQueryHoliday,array('5',$custid));

		        if(empty($prfee['0']['CHARGE_ID'])){
		        	 $prfee = $this->_db->fetchAll($sqlQueryHoliday,array('5','GLOBAL'));
		        }
        foreach($prfee as $key=>$value)
		{
			// if(!empty($key)){
				$keyw = 'prccy'.$value['CCY_ID'];
				$this->view->$keyw = $value['CHARGE_AMOUNT_CCY'];

				$keymin = 'prmin'.$value['CCY_ID'];
				// print_r($keyamt);
				// echo "<br>";
				$this->view->$keymin = $value['CHARGE_PROV_MIN_AMT'];

				$keymax = 'prmax'.$value['CCY_ID'];
				// print_r($keyamt);
				// echo "<br>";
				$this->view->$keymax = $value['CHARGE_PROV_MAX_AMT'];

				$keypct = 'prpct'.$value['CCY_ID'];
				// print_r($keyamt);
				// echo "<br>";
				$this->view->$keypct = $value['CHARGE_PCT'];
			// }
		}

       foreach($fullfee as $key=>$value)
		{
			// if(!empty($key)){
				$keyw = 'faccy'.$value['CCY_ID'];
				$this->view->$keyw = $value['CHARGE_AMOUNT_CCY'];

				$keyamt = 'faamt'.$value['CCY_ID'];
				// print_r($keyamt);
				// echo "<br>";
				$this->view->$keyamt = $value['CHARGE_AMT'];
			// }
		}
       // echo "<pre>";
       // print_r($transfee);
       foreach($transfee as $key=>$value)
		{
			// if(!empty($key)){
				$keyw = 'tfccy'.$value['CCY_ID'];
				$this->view->$keyw = $value['CHARGE_AMOUNT_CCY'];

				$keyamt = 'tfamt'.$value['CCY_ID'];
				// print_r($keyamt);
				// echo "<br>";
				$this->view->$keyamt = $value['CHARGE_AMT'];
			// }
		}


		$selectdirect = $this->_db->select()
							->from('M_CHARGES_DIRECT');
		$selectdirect -> where("CUST_ID LIKE ".$this->_db->quote($custid));
		// $select1 -> where("CHARGES_TYPE = '1'");
		$resultdirect = $this->_db->fetchRow($selectdirect);
		if($resultdirect)
		{
			$this->view->direct_inhouseamt = $resultdirect['CHARGES_INHOUSE'];
			$this->view->direct_domamt = $resultdirect['CHARGES_DOM'];
			$this->view->charges_acc_direct = $resultdirect['ACCT_NO'];
		}

		$select1 = $this->_db->select()
							->from('M_CHARGES_OTHER');
		$select1 -> where("CUST_ID LIKE ".$this->_db->quote($custid));
		// $select1 -> where("CHARGES_TYPE = '1'");
		$result1 = $this->_db->fetchRow($select1);
		if($result1)
		{
			$this->view->charges_acc_dom = $result1['CHARGES_ACCT_NO'];
		}

		$select2 = $this->_db->select()
							->from('M_CHARGES_OTHER');
		$select2 -> where("CUST_ID LIKE ".$this->_db->quote($custid));
		$select2 -> where("CHARGES_TYPE = '1'");
		$result2 = $this->_db->fetchRow($select2);
		if($result2)
		{
			$cekrtgsccy = $this->_db->select()
							->from(array('A' => 'M_MINAMT_CCY'),array('CCY_ID'))
							->where("CCY_ID LIKE ".$this->_db->quote($result2['CHARGES_CCY']))
				 			->query()->fetchAll();
		
			if($cekrtgsccy)
			{
				$this->view->rtgsccy = $result2['CHARGES_CCY'];
			}
			else
			{
				$this->view->rtgsccy = 'IDR';
			}
			
			$this->view->rtgsamt = $result2['CHARGES_AMT'];
		}
	
		$select3 = $this->_db->select()
							->from('M_CHARGES_OTHER');
		$select3 -> where("CUST_ID LIKE ".$this->_db->quote($custid));
		$select3 -> where("CHARGES_TYPE = '2'");
		$result3 = $this->_db->fetchRow($select3);
		
		if($result3)
		{
			$ceksknccy = $this->_db->select()
							->from(array('A' => 'M_MINAMT_CCY'),array('CCY_ID'))
							->where("CCY_ID LIKE ".$this->_db->quote($result3['CHARGES_CCY']))
				 			->query()->fetchAll();
		
			if($ceksknccy)
			{
				$this->view->sknccy = $result3['CHARGES_CCY'];
			}
			else
			{
				$this->view->sknccy = 'IDR';
			}
			
			$this->view->sknamt = $result3['CHARGES_AMT'];
			
		}


		$select4 = $this->_db->select()
							->from('M_CHARGES_OTHER');
		$select4 -> where("CUST_ID LIKE ".$this->_db->quote($custid));
		$select4 -> where("CHARGES_TYPE = '8'");
		$result4 = $this->_db->fetchRow($select3);
		
		if($result4)
		{
			$cekdomccy = $this->_db->select()
							->from(array('A' => 'M_MINAMT_CCY'),array('CCY_ID'))
							->where("CCY_ID LIKE ".$this->_db->quote($result4['CHARGES_CCY']))
				 			->query()->fetchAll();
		
			if($cekdomccy)
			{
				$this->view->domccy = $result4['CHARGES_CCY'];
			}
			else
			{
				$this->view->domccy = 'IDR';
			}
			
			$this->view->domamt = $result4['CHARGES_AMT'];
			
		}




		
		if($result['0']['CUST_MONTHLYFEE_TYPE'] == '3'){
		$selectprk = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER_ACCT'),array('A.ACCT_NO', 'A.CCY_ID', 'A.ACCT_NAME'));
		$selectprk -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		
		/*$selectnoprk = $this->_db->select()
					        ->from(array('A' => 'M_BENEFICIARY'),array('ACCT_NO' => 'A.BENEFICIARY_ACCOUNT', 'CCY_ID' => 'A.CURR_CODE', 'ACCT_NAME' => 'A.BENEFICIARY_NAME'));
		$selectnoprk -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));*/
		
		$selectnoprk2 = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER_ACCT'),array('A.ACCT_NO', 'A.CCY_ID', 'A.ACCT_NAME'));
		$selectnoprk2 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
// 		$selectnoprk2 -> where("A.ACCT_SOURCE NOT LIKE '1'");
		
		$resultunion = $selectprk->query()->FetchAll();
		$this->view->resultaccount = $resultunion;
								
		$resultlist = $selectnoprk2->query()->FetchAll();
		$this->view->resultlist = $resultlist;
		
		foreach($resultlist as $defaultacct)
		{
			$acctno = $defaultacct['ACCT_NO'];
			$cekacct = 'cek'.$acctno;
			$this->view->cekacct = $defaultacct['ACCT_NO'];
		}
		
		$monthlyfeestatus = $result[0]["CUST_MONTHLYFEE_STATUS"];
		
		if($monthlyfeestatus == 0)
		{
			$this->view->monthlyfeestatus = $this->language->_('Disabled');
		}
		if($monthlyfeestatus == 1)
		{
			$this->view->monthlyfeestatus = $this->language->_('Enabled');
		}
			
		$custname = $result[0]['CUST_NAME'];
		
		foreach($resultunion as $row)
		{	
			$acctno = $row['ACCT_NO'];
			$select4 = $this->_db->select()->distinct()
			        	->from(array('A' => 'M_CHARGES_MONTHLY'),array('*'));
			$select4 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
			$select4 -> where("A.ACCT_NO LIKE ".$this->_db->quote($acctno));
			$select4 -> where("MONTHLYFEE_TYPE = '3'");
			$result4 = $select4->query()->FetchAll();

			$i = 0;
			$x = 0;
			if($select4)
			{
				foreach($result4 as $row2)
				{	
					$idfrom = $row2['ACCT_NO'].'from'.$i;
   					$idto = $row2['ACCT_NO'].'to'.$i;
   					$idamt = $row2['ACCT_NO'].'amount'.$i;
					$amt = Application_Helper_General::convertDisplayMoney($row2['AMOUNT']);
					$cekacct = 'cek'.$acctno;
					
					if($row2['TRA_FROM'] != 0 && $row2['TRA_TO'] != 0)
					{
						$this->view->$idfrom = $row2['TRA_FROM'];
						$this->view->$idto = $row2['TRA_TO'];
					}
					
					$this->view->$idamt = $amt;
					$this->view->cekacct = $row2['CHARGES_ACCT_NO'];
					$i++;
				}
			}
			$x++;
		}

		}else if($result['0']['CUST_MONTHLYFEE_TYPE'] == '2'){
			$custname = $result[0]['CUST_NAME'];
		
			$selectprk = $this->_db->select()
						        ->from(array('A' => 'M_CUSTOMER_ACCT'),array('A.ACCT_NO', 'A.CCY_ID', 'A.ACCT_NAME'));
			$selectprk -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
			
			$selectnoprk = $this->_db->select()
						        ->from(array('A' => 'M_BENEFICIARY'),array('ACCT_NO' => 'A.BENEFICIARY_ACCOUNT', 'CCY_ID' => 'A.CURR_CODE', 'ACCT_NAME' => 'A.BENEFICIARY_NAME'));
			$selectnoprk -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
			
			$selectnoprk2 = $this->_db->select()
						        ->from(array('A' => 'M_CUSTOMER_ACCT'),array('A.ACCT_NO', 'A.CCY_ID', 'A.ACCT_NAME'));
			$selectnoprk2 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
			$selectnoprk2 -> where("A.ACCT_SOURCE NOT LIKE '1'");
			
			//$selectprk = $selectprk->__toString();
			//$selectnoprk = $selectnoprk->__toString();
			//$selectnoprk2 = $selectnoprk2->__toString();
			
			//$unionquery = $this->_db->select()
									//->union(array($selectprk,$selectnoprk));
			
			$resultunion = $selectprk->query()->FetchAll();
			$this->view->resultaccount = $resultunion;
									
			//$selectunion = $this->_db->select()
			//					->from (($unionquery),array('*'));
			//$resultunion = $selectunion->query()->FetchAll();
			//$this->view->resultaccount = $resultunion;
			//Zend_Debug::dump($resultunion);die;
			
			/*$chargeaccount = $this->_db->select()
									->union(array($selectnoprk2,$selectnoprk));*/
			$resultlist = $selectnoprk2->query()->FetchAll();
			$this->view->resultlist = $resultlist;			     
			
			$select4 = $this->_db->select()->distinct()
				        	->from(array('A' => 'M_ADMFEE_MONTHLY'),array('*'));
			$select4 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
			$select4 -> where("MONTHLYFEE_TYPE = '2'");
			$result4 = $this->_db->FetchRow($select4);

			$cekacct = 'cekacct';
			$idamt = 'amount';
			$amt = Application_Helper_General::convertDisplayMoney($result4['AMOUNT']);
			$this->view->$idamt = $amt;
			$this->view->cekacct = $result4['CHARGES_ACCT_NO'];
			$this->view->monthly_ccy2 = $result4['CCY'];
		}else if($result['0']['CUST_MONTHLYFEE_TYPE'] == '1'){
			$selectprk = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER_ACCT'),array('A.ACCT_NO', 'A.CCY_ID', 'A.ACCT_NAME'));
			$selectprk -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
			
			/*$selectnoprk = $this->_db->select()
						        ->from(array('A' => 'M_BENEFICIARY'),array('ACCT_NO' => 'A.BENEFICIARY_ACCOUNT', 'CCY_ID' => 'A.CURR_CODE', 'ACCT_NAME' => 'A.BENEFICIARY_NAME'));
			$selectnoprk -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));*/
			
			$selectnoprk2 = $this->_db->select()
						        ->from(array('A' => 'M_CUSTOMER_ACCT'),array('A.ACCT_NO', 'A.CCY_ID', 'A.ACCT_NAME'));
			$selectnoprk2 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
			//$selectnoprk2 -> where("A.ACCT_SOURCE NOT LIKE '1'");
			
			$resultunion = $selectprk->query()->FetchAll();
			$this->view->resultaccount = $resultunion;

			$resultlist = $selectnoprk2->query()->FetchAll();
			$this->view->resultlist = $resultlist;
			
			foreach($resultlist as $defaultacct)
			{
				$acctno = $defaultacct['ACCT_NO'];
				$cekacct = 'cek'.$acctno;
				$this->view->cekacct = $defaultacct['ACCT_NO'];
			}
			
			$monthlyfeestatus = $result[0]["CUST_MONTHLYFEE_STATUS"];
			
			if($monthlyfeestatus == 0)
			{
				$this->view->monthlyfeestatus = $this->language->_('Disabled');
			}
			if($monthlyfeestatus == 1)
			{
				$this->view->monthlyfeestatus = $this->language->_('Enabled');
			}
				
			$custname = $result[0]['CUST_NAME'];
			
			foreach($resultunion as $row)
			{	
				$acctno = $row['ACCT_NO'];
				$select4 = $this->_db->select()->distinct()
				        	->from(array('A' => 'M_ADMFEE_MONTHLY'),array('*'));
				$select4 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
				$select4 -> where("A.ACCT_NO LIKE ".$this->_db->quote($acctno));
				$select4 -> where("MONTHLYFEE_TYPE = '1'");
				$result4 = $select4->query()->FetchAll();

				$i = 0;
				if($select4)
				{
					foreach($result4 as $row2)
					{	
	   					$idamt = $row2['ACCT_NO'].'amount';
						$amt = Application_Helper_General::convertDisplayMoney($row2['AMOUNT']);
						$cekacct = 'cek'.$acctno; 
						$this->view->$idamt = $amt;
						$this->view->cekacct = $row2['CHARGES_ACCT_NO'];
						$i++;
					}
				}
			}	



		}else if($result['0']['CUST_MONTHLYFEE_TYPE'] == '4'){
			$selectprk = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER_ACCT'),array('A.ACCT_NO', 'A.CCY_ID', 'A.ACCT_NAME'));
			$selectprk -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
			
			$selectnoprk = $this->_db->select()
						        ->from(array('A' => 'M_BENEFICIARY'),array('ACCT_NO' => 'A.BENEFICIARY_ACCOUNT', 'CCY_ID' => 'A.CURR_CODE', 'ACCT_NAME' => 'A.BENEFICIARY_NAME'));
			$selectnoprk -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
			
			$selectnoprk2 = $this->_db->select()
						        ->from(array('A' => 'M_CUSTOMER_ACCT'),array('A.ACCT_NO', 'A.CCY_ID', 'A.ACCT_NAME'));
			$selectnoprk2 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
			//$selectnoprk2 -> where("A.ACCT_SOURCE NOT LIKE '1'");
			
			$resultunion = $selectprk->query()->FetchAll();
			$this->view->resultaccount = $resultunion;
									
			$resultlist = $selectnoprk2->query()->FetchAll();
			$this->view->resultlist = $resultlist;			     
			
			$select4 = $this->_db->select()->distinct()
				        	->from(array('A' => 'M_CHARGES_MONTHLY'),array('*'));
			$select4 -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
			$select4 -> where("MONTHLYFEE_TYPE = '4'");
			$result4 = $select4->query()->FetchAll();

			$i = 0;

				if( !empty($result4) )
				{
					foreach($result4 as $row2)
					{	
						$idamt = 'amount'.$i;
						$idfrom = 'from'.$i;
	   					$idto = 'to'.$i;
						$amt = Application_Helper_General::convertDisplayMoney($row2['AMOUNT']);
						
						if($row2['TRA_FROM'] != 0 && $row2['TRA_TO'] != 0)
						{
							$this->view->$idfrom = $row2['TRA_FROM'];
							$this->view->$idto = $row2['TRA_TO'];
						}
						
						$this->view->$idamt = $amt;
						$i++;
					}
					$cekacct = 'cek'.'acct';
					$this->view->cekacct = $row2['CHARGES_ACCT_NO'];
				}
			
			Application_Helper_General::writeLog('CHUD','Update company charges');
		}	
		
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		

		$submit = $this->_getParam('submit');
		$reset = $this->_getParam('reset');
		// var_dump($submit);die;
		// print_r($this->_request->getParams());die;
		if(!$submit)
		{
			Application_Helper_General::writeLog('CHUD','View Update Monthly transaction account charges page ('.$custid.')');
		}
		
		$cek = $this->_db->select()
							->from('TEMP_ADMFEE_MONTHLY');
		$cek -> where("CUST_ID LIKE ".$this->_db->quote($custid));
		$cek = $cek->query()->FetchAll();
		
		$cek2 = $this->_db->select()
							->from('TEMP_CHARGES_MONTHLY');
		$cek2 -> where("CUST_ID LIKE ".$this->_db->quote($custid));
		$cek2 = $cek2->query()->FetchAll();
		// Zend_Debug::dump($cek); die;
		$cek3 = $this->_db->select()
							->from('TEMP_CHARGES_OTHER');
		$cek3 -> where("CUST_ID LIKE ".$this->_db->quote($custid));
		$cek3 = $cek3->query()->FetchAll();

		$cek4 = $this->_db->select()
							->from('TEMP_CHARGES_REMITTANCE');
		$cek4 -> where("CUST_ID LIKE ".$this->_db->quote($custid));
		$cek4 = $cek4->query()->FetchAll();

		$cek5 = $this->_db->select()
							->from('TEMP_CHARGES_WITHIN');
		$cek5 -> where("CUST_ID LIKE ".$this->_db->quote($custid));
		$cek5 = $cek5->query()->FetchAll();
		// Zend_Debug::dump($cek); die;
		if(!$cek && !$cek2 && !$cek3 && !$cek4 && !$cek5)
		{
			// print_r($this->_request->getParams());die;
			// print_r($this->_request->getParams());die;

			if($submit && $this->_getParam('chargetypes') == 'monthly'){

			if($submit && $this->_getParam('monthly_type') == '3')
			{
				
				Application_Helper_General::writeLog('CHUD','Updating Monthly transaction account charges ('.$custid.')');
					$error = 0;
					$arraycekisi = array();
					foreach($resultunion as $row)
					{
						$cekisi = 0;
						$cekisi2 = 0;
						$cekisiamt = 0;
						$acctno = $row['ACCT_NO'];
						$cekacct = 'cek'.$acctno;
						$rowmax = 0;
						
						for($i=0;$i<5;$i++)
						{
						$z = 0;
						$idamt = $row['ACCT_NO'].'amount'.$i;
						$redebate = $row['ACCT_NO'].'redebate'.$i;
						$idfrom = $row['ACCT_NO'].'from'.$i;
   						$idto = $row['ACCT_NO'].'to'.$i;
   						$err2 = $row['ACCT_NO'].'err2'.$i;
   						// $ccy = $row['ACCT_NO'].'ccy';
   						//Zend_Debug::dump($idamt); die;
						$cekamt = $this->_getParam($idamt);
						$amt = Application_Helper_General::convertDisplayMoney($cekamt);
						$temp_amt = str_replace('.','',$amt);
						$cek_angka = (Zend_Validate::is($temp_amt,'Digits')) ? true : false;
						$this->view->$idamt = $cekamt;
						
						$cekdebate = $this->_getParam($redebate);
						$cek_debate2 = (Zend_Validate::is($cekdebate,'Digits')) ? true : false;
						$this->view->$cekdebate = $cekdebate;
						

						$cekfrom = $this->_getParam($idfrom);
						$cek_angka2 = (Zend_Validate::is($cekfrom,'Digits')) ? true : false;
						$this->view->$idfrom = $cekfrom;
						
						$cekto = $this->_getParam($idto);
						$cek_angka3 = (Zend_Validate::is($cekto,'Digits')) ? true : false;
						$this->view->$idto = $cekto;
						
						$cekcekacct = $this->_getParam($acctno);
   						$cek_angka4 = (Zend_Validate::is($cekcekacct,'Digits')) ? true : false;
   						$this->view->cekacct = $cekcekacct;
   						//Zend_Debug::dump($cekto); die;
   						
   						$selectcekcur = $this->_db->select()
					        			->from (($selectprk),array('*'));
						$selectcekcur -> where("ACCT_NO LIKE ".$this->_db->quote($cekcekacct));
						$cekcur = $this->_db->fetchRow($selectcekcur);
   								
						if($cek_angka && $cek_angka2 && $cek_angka3)
						{
							//Zend_Debug::dump($cekto); die;
							if ($cekfrom >= $cekto && $cekfrom != '0' && $cekto != '0')
							{
								$errid = 'err'.$idfrom;
								$err = $this->language->_('From Range has to be less than To Range');
								$this->view->$errid = $err;
								$error++;
								$z++;
								//die;
							}
							
							if ($cekfrom == '0' && $cekto != '0')
							{
								$errid = 'err'.$idfrom;
								$err = $this->language->_('Range value can not start from 0');
								$this->view->$errid = $err;
								$error++;
								$z++;
								//die;
							}
							
							for($j=$i;$j>=0;$j--)
							{
								if($i != $j)
								{
									$fromerror = false;
									$toerror = false;
									$idfromcek = $row['ACCT_NO'].'from'.$j;
   									$idtocek = $row['ACCT_NO'].'to'.$j;
   									$fromcek = $this->_getParam($idfromcek);
   									$tocek = $this->_getParam($idtocek);
   									
   									if($fromcek && $tocek)
   									{
   										if($fromcek <= $cekfrom && $cekfrom <= $tocek)
   										{
   											$fromerror = true;
   											//echo $cekfrom;die;
   										}
   									
										if($fromcek <= $cekto && $cekto <= $tocek)
   										{
   											$toerror = true;
   										}
   									
   										if($fromerror == true || $toerror == true)
   										{
   											$errid = 'err'.$idto.$j;
											$err = $this->language->_('Value overlap not allowed (overlap with : ').$fromcek.'-'.$tocek.')';
											$this->view->$errid = $err;
											$this->view->$err2 = true;
											//echo $err2;die;
											//Zend_Debug::dump($fromcek.' <= '.$cekto.' - '.$cekto.' <= '.$tocek);
											//Zend_Debug::dump($errid);
											$error++;
											$z++;
											
   										}
   									}	
								}
							}
						}
   						
						if($cek_angka2 && $cek_angka3 && $cekfrom != '0' && $cekto != '0')
							{
								if($cekamt == null || $cekamt == '0.00')
								{
									$errid = 'err'.$idamt;
									$err = $this->language->_('Amount value has to be set for this range');
									$this->view->$errid = $err;
									$error++;
									$z++;
									//die;
								}
							}
						
						if($cek_angka == false && $cekamt != null)
						{
							$errid = 'err'.$idamt;
							$err = $this->language->_('Amount value has to be numeric');
							$this->view->$errid = $err;
							$error++;
							$z++;
							//die;
						}
							
						if($cekcekacct)
						{
								if($cekcur['CCY_ID'] != $row['CCY_ID'])
								{
									$errid = 'err'.$cekacct;
									$err = $this->language->_('Account currency must be same');
									$this->view->$errid = $err;
									$error++;
								}
						}
						
						if($cek_angka && $cekamt != '0.00')
							{
								if($cek_angka2 == false || $cek_angka3 == false)
								{
									$errid = 'err'.$idfrom;
									$err = $this->language->_('From and To value has to be numeric');
									$this->view->$errid = $err;
									$error++;
									$z++;
									//die;
								}
								
								if($cekfrom == null || $cekto == null)
								{
									$errid = 'err'.$idfrom;
									$err = $this->language->_('From and To Range should not be empty while the amount is filled');
									$this->view->$errid = $err;
									$error++;
									$z++;
								}
							}
						
							if(!$cekfrom && !$cekto && ($amt == null || $amt == '0.00'))
							{
								$cekisi2++;
							}
							
							if($z > 0)
							{
								$rowmax++;
							}
   						
						$cekisi++;
						
						}
						
						if($cekisi == $cekisi2)
						{
							$temparray = array($acctno);
							$arraycekisi = array_merge($arraycekisi,$temparray);
						}
						if($cekisi != $cekisi2)
						{
							if(!$cekcekacct)
							{
								$errid = 'err'.$cekacct;
								$err = $this->language->_('Account should be choosen when amount is filled');
								$this->view->$errid = $err;
								$error++;
								//die;
							}
							
							if($cekcekacct)
							{
								if($cek_angka4 == false)
								{
									$errid = 'err'.$cekacct;
									$err = $this->language->_('Account value has to be numeric');
									$this->view->$errid = $err;
									$error++;
								}
							}
						}
						
						$rowmaxid = 'rowmax'.$row['ACCT_NO'];
						$this->view->$rowmaxid = $rowmax;
					}
					//die;
					//Zend_Debug::dump($arraycekisi); die;
				if($error == 0)
				{
					$this->_db->beginTransaction();
					//die;
					try
					{
				    //Zend_Debug::dump($resultunion); die;
					$info = 'Charges';
					$info2 = $this->language->_('Set Charges Monthly per Account');
					
					$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['edit'],null,'M_CHARGES_MONTHLY','TEMP_CHARGES_MONTHLY',$custid,$custname,$custid,null,'monthlyaccount');
					// print_r($resultunion);die;
					foreach($resultunion as $row)
					{
						for($i=0;$i<6;$i++)
						{
							$idamt = $row['ACCT_NO'].'amount'.$i;
							$redebate = $row['ACCT_NO'].'redebate'.$i;
							$idfrom = $row['ACCT_NO'].'from'.$i;
   							$idto = $row['ACCT_NO'].'to'.$i;
   						
							$cekamt = $this->_getParam($idamt);
							$amt = Application_Helper_General::convertDisplayMoney($cekamt);

							$cekdebate = $this->_getParam($redebate);
							$redebate = Application_Helper_General::convertDisplayMoney($cekdebate);

							$cekamt = $this->_getParam($idamt);
							$amt = Application_Helper_General::convertDisplayMoney($cekamt);
						
							$cekfrom = $this->_getParam($idfrom);
						
							$cekto = $this->_getParam($idto);
							$cekcekacct = $this->_getParam($row['ACCT_NO']);
							// $cekcekccy = $this->_getParam($row['ACCT_NO'].'ccy');
							// print_r($cekcekccy);die;
							if($cekfrom && $cekto && $cekamt)
							{
								$data= array(
										'CHANGES_ID' 		=> $change_id,
										'CUST_ID' 			=> $custid,
										'ACCT_NO' 			=> $row['ACCT_NO'],
										'TRA_FROM' 			=> $cekfrom,
										'TRA_TO' 			=> $cekto,
										'AMOUNT' 			=> $amt,
										'REDEBATE' 			=> $redebate,
										// redebate
										'MONTHLYFEE_TYPE' 	=> '3',
										'CHARGES_ACCT_NO'	=> $cekcekacct,
										'CCY'				=> $row['CCY_ID'],
										'SUGGEST_DATE'		=> new Zend_Db_Expr('now()'),
										'SUGGEST_BY'		=> $this->_userIdLogin
									);
								$this->_db->insert('TEMP_CHARGES_MONTHLY',$data);										
								//echo $row['ACCT_NO'].' aaa<br />';
								//Zend_Debug::dump($data);
							}
						}				
					}
					
					if($arraycekisi)
					{
						foreach($arraycekisi as $array)
						{				
							// $paramccy = $array.'ccy';
							$cekcekacct = $this->_getParam($array);
							// $cekcekccy = $this->_getParam($$paramccy);

							$data2= array(
										'CHANGES_ID' 		=> $change_id,
										'CUST_ID' 			=> $custid,
										'ACCT_NO' 			=> $array,
										'TRA_FROM' 			=> '0',
										'TRA_TO' 			=> '0',
										'AMOUNT' 			=> '0.00',
										'MONTHLYFEE_TYPE' 	=> '3',
										'CHARGES_ACCT_NO'	=> $cekcekacct,
										'CCY'				=> $row['CCY_ID']
										);
							$this->_db->insert('TEMP_CHARGES_MONTHLY',$data2);				
						}
					}
					
					if($error == 0)
					{	
						//die;
						//Zend_Debug::dump('commit'); die;
						$this->_db->commit();
						$this->setbackURL('/monthlycharges/monthlyfee/index/custid/'.$enccustid);
						$this->_redirect('/notification/submited/index');
					}
				}
				
				catch(Exception $e)
				{
					die;
					//Zend_Debug::dump('rollback'); die;
					$this->_db->rollBack();
				}
				
			}			
			}else if($submit && $this->_getParam('monthly_type') == '2'){

					$error = 0;
				$cekacct = 'cekacct';
				$idamt = 'amount';
				
				Application_Helper_General::writeLog('CHUD','Updating Administration fee company charges ('.$custid.')');
				
   					//Zend_Debug::dump($idamt); die;
					$cekamt = $this->_getParam($idamt);
					$amt = Application_Helper_General::convertDisplayMoney($cekamt);
					$temp_amt = str_replace('.','',$amt);
					$cek_angka = (Zend_Validate::is($temp_amt,'Digits')) ? true : false;
					$this->view->$idamt = $cekamt;
					
					$cekcekacct = $this->_getParam('acct');
   					$cek_angka2 = (Zend_Validate::is($cekcekacct,'Digits')) ? true : false;
   					$this->view->cekacct = $cekcekacct;
   					//Zend_Debug::dump($cekfrom); die;
   					
   					$cekcekccy = $this->_getParam('monthly_ccy2');

					if($cek_angka == false)
						{
							$errid = 'erramt';
							$err = $this->language->_('Amount value has to be numeric');
							$this->view->$errid = $err;
							$error++;
							//die;
						}
						
					if($cekcekacct != null && $cekcekacct != "")
						{
							if($cek_angka2 == false)
							{
								$errid = 'erracct';
								$err = $this->language->_('Account value has to be numeric');
								$this->view->$errid = $err;
								$error++;
								//die;
							}
						}
					
					if(!$cekcekacct)
					{
						$errid = 'erracct';
						$err = $this->language->_('Charges Account must be choosen');
						$this->view->$errid = $err;
						$error++;
					}

					if(!$cekcekccy)
					{
						$errid = 'erracct';
						$err = $this->language->_('Charges CCY must be choosen');
						$this->view->$errid = $err;
						$error++;
					}

					
   					
				//Zend_Debug::dump($error); die;
					
				if($error == 0)
				{
					//die;
					$this->_db->beginTransaction();
					try
					{
						$info = $this->language->_('Charges');
						$info2 = $this->language->_('Set Charges Administration fee per Company');
						
						$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['edit'],null,'M_ADMFEE_MONTHLY','TEMP_ADMFEE_MONTHLY',$custid,$custname,$custid,null,'adminfeeaccount');
						
	
						$cekacct = 'acct';
						$idamt = 'amount';
							
						$cekamt = $this->_getParam($idamt);
						$amt = Application_Helper_General::convertDisplayMoney($cekamt);
							
						$cekcekacct = $this->_getParam('acct');
							
						if($cekamt && $cekcekacct)
						{
							$data= array(
											'CHANGES_ID' 		=> $change_id,
											'CUST_ID' 			=> $custid,
											'ACCT_NO' 			=> $cekcekacct,
											'AMOUNT' 			=> $amt,
											'MONTHLYFEE_TYPE' 	=> '2',
											'CHARGES_ACCT_NO'	=> $cekcekacct,
											'CCY'				=> $cekcekccy
										);
							$this->_db->insert('TEMP_ADMFEE_MONTHLY',$data);										
							//echo 'aaa';die;
						}
						
						if($error == 0)
						{	
							//die;
							//Zend_Debug::dump('commit'); die;
							$this->_db->commit();
							$this->setbackURL('/monthlycharges/monthlyfee/index/custid/'.$enccustid);
							$this->_redirect('/notification/submited/index');
						}
					}
				
					catch(Exception $e)
					{
						//Zend_Debug::dump($e); die;
						$this->_db->rollBack();
					}
				
				}		


			}else if($submit && $this->_getParam('monthly_type') == '1'){
				Application_Helper_General::writeLog('CHUD','Updating Administration fee account charges ('.$custid.')');
				$error = 0;
				foreach($resultunion as $row)
				{
					$acctno = $row['ACCT_NO'];
					$cekacct = 'cek'.$acctno;
					$idamt = $row['ACCT_NO'].'amount';
					
   					// Zend_Debug::dump($idamt); die;
					$cekamt = $this->_getParam($idamt);
					$amt = Application_Helper_General::convertDisplayMoney($cekamt);
					$temp_amt = str_replace('.','',$amt);
					$cek_angka = (Zend_Validate::is($temp_amt,'Digits')) ? true : false;
					$this->view->$idamt = $cekamt;
					
					$cekcekacct = $this->_getParam($acctno);
   					$cek_angka2 = (Zend_Validate::is($cekcekacct,'Digits')) ? true : false;
   					$this->view->cekacct = $cekcekacct;
   					
   					
   					$selectcekcur = $this->_db->select()
				        			->from (($selectnoprk2),array('*'));
					$selectcekcur -> where("ACCT_NO LIKE ".$this->_db->quote($cekcekacct));
					$cekcur = $this->_db->fetchRow($selectcekcur);			    
					
					if($cekcekacct)
					{
							if($cekcur['CCY_ID'] != $row['CCY_ID'])
							{
								$errid = 'err'.$cekacct;
								$err = $this->language->_('Account currency must be same');
								$this->view->$errid = $err;
								$error++;
							}
					}
					
					if($cek_angka == false && $cek_angka != null)
					{
							$errid = 'err'.$idamt;
							$err = $this->language->_('Amount value has to be numeric');
							$this->view->$errid = $err;
							$error++;
							//die;
					}
					
					if($cekamt != '0.00')
					{
						if(!$cekcekacct)
						{
							$errid = 'err'.$cekacct;
							$err = $this->language->_('Account should be choosen while the amount is filled');
							$this->view->$errid = $err;
							$error++;
							//die;
						}
						if($cekcekacct)
						{
							if($cek_angka2 == false)
							{
								$errid = 'err'.$cekacct;
								$err = $this->language->_('Account value has to be numeric');
								$this->view->$errid = $err;
								$error++;
							}
						}
					}	
				}
				//Zend_Debug::dump($error); die;
					
				if($error == 0)
				{
					//die;
					$this->_db->beginTransaction();
					try
					{
					$info = $this->language->_('Charges');
					$info2 = $this->language->_('Set Charges Administration fee per Account');
					
					$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['edit'],null,'M_ADMFEE_MONTHLY','TEMP_ADMFEE_MONTHLY',$custid,$custname,$custid,null,'adminfeeaccount');
					
					foreach($resultunion as $row)
					{
						$idamt = $row['ACCT_NO'].'amount';
						
						$cekamt = $this->_getParam($idamt);
						if($cekamt == null)
						{
							$cekamt = '0.00';
						}
						$amt = Application_Helper_General::convertDisplayMoney($cekamt);
						
						$cekcekacct = $this->_getParam($row['ACCT_NO']);
						// print_r($row);die;
						$data= array(
										'CHANGES_ID' 		=> $change_id,
										'CUST_ID' 			=> $custid,
										'ACCT_NO' 			=> $row['ACCT_NO'],
										'AMOUNT' 			=> $amt,
										'LAST_EXECUTED' 	=> new Zend_Db_Expr('GETDATE()'),
										'MONTHLYFEE_TYPE' 	=> '1',
										'CHARGES_ACCT_NO'	=> $cekcekacct,
										// 'CCY'				=> $row['CCY_ID']
									);
						$this->_db->insert('TEMP_ADMFEE_MONTHLY',$data);										
						//echo 'aaa';die;
					}
					
					if($error == 0)
					{	
						//die;
						//Zend_Debug::dump('commit'); die;
						$this->_db->commit();
						$this->setbackURL('/monthlycharges/monthlyfee/index/custid/'.$enccustid);
						$this->_redirect('/notification/submited/index');
					}
				}
				
				catch(Exception $e)
				{
					//Zend_Debug::dump('rollback'); die;
					$this->_db->rollBack();
				}
				
			}

			}else if($submit && $this->_getParam('monthly_type') == '4'){
				$error = 0;
					$cekacct = 'acct';
					$cekisi = 0;
					$cekisi2 = 0;
					$arraycekisi = array();
					
					$cekcekacct = $this->_getParam($cekacct);
   					$cek_angka4 = (Zend_Validate::is($cekcekacct,'Digits')) ? true : false;
   					$this->view->cekacct = $cekcekacct;
   					
					if($cekcekacct != null && $cekcekacct != "")
					{
						if($cek_angka4 == false)
						{
							$errid = 'erracct';
							$err = $this->language->_('Account value has to be numeric');
							$this->view->$errid = $err;
							$error++;
									//die;
						}
					}
						
					if(!$cekcekacct)
					{
						$errid = 'erracct';
						$err = $this->language->_('Charges Account must be choosen');
						$this->view->$errid = $err;
						$error++;
					}
					
					for($i=0;$i<10;$i++)
					{
						$idamt = 'amount'.$i;
						$idfrom = 'from'.$i;
   						$idto = 'to'.$i;
   						$err2 = 'err2'.$i;
   						//Zend_Debug::dump($idamt); die;
						$cekamt = $this->_getParam($idamt);
						$amt = Application_Helper_General::convertDisplayMoney($cekamt);
						$temp_amt = str_replace('.','',$amt);
						$cek_angka = (Zend_Validate::is($temp_amt,'Digits')) ? true : false;
						$this->view->$idamt = $cekamt;
						
						$cekfrom = $this->_getParam($idfrom);
						$cek_angka2 = (Zend_Validate::is($cekfrom,'Digits')) ? true : false;
						$this->view->$idfrom = $cekfrom;
						
						$cekto = $this->_getParam($idto);
						$cek_angka3 = (Zend_Validate::is($cekto,'Digits')) ? true : false;
						$this->view->$idto = $cekto;
						
   						//Zend_Debug::dump($cekfrom); die;
   						
						if($cek_angka && $cek_angka2 && $cek_angka3)
						{
							if ($cekfrom >= $cekto)
							{
								$errid = 'err'.$idfrom;
								$err = $this->language->_('From Range has to be less than To Range');
								$this->view->$errid = $err;
								$error++;
							}
							
							if ($cekfrom == '0')
							{
								$errid = 'err'.$idfrom;
								$err = 'Range value can not start from 0';
								$this->view->$errid = $err;
								$error++;
								//die;
							}
							
							for($j=$i;$j>=0;$j--)
							{
								if($i != $j)
								{
									$fromerror = false;
									$toerror = false;
									$idfromcek = 'from'.$j;
   									$idtocek = 'to'.$j;
   									$fromcek = $this->_getParam($idfromcek);
   									$tocek = $this->_getParam($idtocek);
   									
   									if($fromcek && $tocek)
   									{
   										if($fromcek <= $cekfrom && $cekfrom <= $tocek)
   										{
   											$fromerror = true;
   											//echo $cekfrom;die;
   										}
   									
										if($fromcek < $cekto && $cekto < $tocek)
   										{
   											$toerror = true;
   										}
   									
   										if($fromerror == true || $toerror == true)
   										{
   											$errid = 'err'.$idto.$j;
											$err = 'Value overlap not allowed (overlap with : '.$fromcek.'-'.$tocek.')';
											$this->view->$errid = $err;
											$this->view->$err2 = true;
											$error++;
   										}
   									}	
								}
							}
						}
   						
						if($cek_angka2 && $cek_angka3)
							{
								if($cekamt == null || $cekamt == '0.00')
								{
									$errid = 'err'.$idamt;
									$err = 'Amount value has to be set for this range';
									$this->view->$errid = $err;
									$error++;
									//die;
								}
							}
						
						if($cek_angka == false && $cekamt != null)
							{
								$errid = 'err'.$idamt;
								$err = 'Amount value has to be numeric';
								$this->view->$errid = $err;
								$error++;
								//die;
							}
						
						if($cek_angka && $cekamt != '0.00')
							{
								if($cek_angka2 == false || $cek_angka3 == false)
								{
									$errid = 'err'.$idfrom;
									$err = 'From and To value has to be numeric';
									$this->view->$errid = $err;
									$error++;
									//die;
								}
								
								if($cekfrom == null || $cekto == null)
								{
									$errid = 'err'.$idfrom;
									$err = 'From and To Range should not be empty while the amount is filled';
									$this->view->$errid = $err;
									$error++;
									//die;
								}
							}
						if($cekfrom == null && $cekto == null && $amt == '0.00' && $cek_angka4)
							{
								$cekisi++;
							}
						$cekisi2++;					
						}
					
						if($cekisi == $cekisi2)
						{
							//$arraycekisi = array($acctno);
						}
					//Zend_Debug::dump($error); die;
				if($error == 0)
				{
					//die;
					$this->_db->beginTransaction();
					try
					{
				    //Zend_Debug::dump($cek_angka2); die;
					$info = 'Charges';
					$info2 = 'Set Charges Monthly per Company';
					
					$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['edit'],null,'M_CHARGES_MONTHLY','TEMP_CHARGES_MONTHLY',$custid,$custname,$custid,null,'monthlyaccount');
					
					$cekacct = 'acct';
						
					for($i=0;$i<10;$i++)
					{
						$idamt = 'amount'.$i;
						$idfrom = 'from'.$i;
   						$idto = 'to'.$i;
   						
						$cekamt = $this->_getParam($idamt);
						$amt = Application_Helper_General::convertDisplayMoney($cekamt);
						
						$cekfrom = $this->_getParam($idfrom);
						
						$cekto = $this->_getParam($idto);
						
						$cekcekacct = $this->_getParam($cekacct);
						$ccymonthly = $this->_getParam('monthly_ccy4');
						
						
						if($cekamt && $cekfrom && $cekto && $cekcekacct)
						{
						
							$data= array(
										'CHANGES_ID' 		=> $change_id,
										'CUST_ID' 			=> $custid,
										'ACCT_NO' 			=> $cekcekacct,
										'TRA_FROM' 			=> $cekfrom,
										'TRA_TO' 			=> $cekto,
										'AMOUNT' 			=> $amt,
										'MONTHLYFEE_TYPE' 	=> '4',
										'CHARGES_ACCT_NO'	=> $cekcekacct,
										'CCY'				=> $ccymonthly
									);
						$this->_db->insert('TEMP_CHARGES_MONTHLY',$data);										
						//echo 'aaa';die;
						}
					}
					
					if($arraycekisi)
					{
						foreach($arraycekisi as $array)
						{				
							$cekcekacct = $this->_getParam('acct');
								
							$data2= array(
										'CHANGES_ID' 		=> $change_id,
										'CUST_ID' 			=> $custid,
										'ACCT_NO' 			=> $cekcekacct,
										'TRA_FROM' 			=> '0',
										'TRA_TO' 			=> '0',
										'AMOUNT' 			=> '0.00',
										'MONTHLYFEE_TYPE' 	=> '4',
										'CHARGES_ACCT_NO'	=> $cekcekacct
										);
							$this->_db->insert('TEMP_CHARGES_MONTHLY',$data2);				
						}
					}
					
					if($error == 0)
					{	
						//die;
						//Zend_Debug::dump('commit'); die;
						$this->_db->commit();
						$this->setbackURL('/monthlycharges/monthlyfee/index/custid/'.$enccustid);
						$this->_redirect('/notification/submited/index');
					}
				}
				
				catch(Exception $e)
				{
					//Zend_Debug::dump('rollback'); die;
					$this->_db->rollBack();
				}
				
			}	

			}



			}else if($submit && $this->_getParam('chargetypes') == 'inhouse'){
				// die('here');
					$error = 0;
					foreach($resultacc as $row)
					{
						$idamt = $row['ACCT_NO'].'amount';
						
   						//Zend_Debug::dump($idamt); die;
						$cekamt = $this->_getParam($idamt);
						$amt = Application_Helper_General::convertDisplayMoney($cekamt);
						$temp_amt = str_replace('.','',$amt);
						$cek_angka = (Zend_Validate::is($temp_amt,'Digits')) ? true : false;
						$this->view->$idamt = $cekamt;
						
   						//Zend_Debug::dump($cekfrom); die;
   						
						if($cek_angka == false && $cekamt != null)
							{
								$errid = 'err'.$idamt;
								$err = 'Amount value has to be numeric';
								$this->view->$errid = $err;
								$error++;
								//die;
							}	
					}
					//Zend_Debug::dump($custid); die;
						
					if($error == 0)
					{
						//die;
						$this->_db->beginTransaction();
						try
						{
						$info = 'Charges';
						$info2 = 'Set Realtime Charges';
						
						$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['edit'],null,'M_CHARGES_WITHIN','TEMP_CHARGES_WITHIN',$custid,$custname,$custid,null,'realtimecharges');
						// print_r($this->_request->getParams());die;
						foreach($resultacc as $row)
						{
							$idamt = $row['ACCT_NO'].'amount';
							$cekamt = $this->_getParam($idamt);
							$acct = $row['ACCT_NO'];
							$chargeacct = $this->_getParam($acct);
							$amt = Application_Helper_General::convertDisplayMoney($cekamt);
							
							if($cekamt == null)
							{
								$amt = '0.00';
							}
								$data= array(
												'CHANGES_ID' 		=> $change_id,
												'CUST_ID' 			=> $custid,
												'ACCT_NO' 			=> $row['ACCT_NO'],
												'AMOUNT' 			=> $amt,
												'BUSINESS_TYPE' 	=> '1',
												'CHARGES_ACCT_NO'	=> $chargeacct,
												'CCY'				=> $row['CCY_ID'],
												'CHARGES_SUGGESTEDBY'=> $this->_userIdLogin,
												'CHARGES_SUGGESTED'	=> new Zend_Db_Expr('now()')
											);
								// print_r($data);die;
								$this->_db->insert('TEMP_CHARGES_WITHIN',$data);										
								//echo 'aaa';die;
								//Zend_Debug::dump($data);
						}
						//die;
						if($error == 0)
						{	
							//die;
							//Zend_Debug::dump('commit'); die;
							$this->_db->commit();
							$this->setbackURL('/monthlycharges/monthlyfee/index/custid/'.$enccustid);
							$this->_redirect('/notification/submited/index');
						}
					}
					
					catch(Exception $e)
					{
						Zend_Debug::dump('rollback'); die;
						$this->_db->rollBack();
					}
					
				}




			}else if($submit && $this->_getParam('chargetypes') == 'domestic'){
				$this->_db->beginTransaction();
				try
				{
					$info = 'Charges';
					$info2 = 'Domestic Charges';
					$error = 0;
					
					
					
					$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['edit'],null,'M_CHARGES_OTHER','TEMP_CHARGES_OTHER',$custid,$custname,$custid,null,'chargesdomestic');
					
					$rtgsamtinsert = $this->_getParam('rtgs');
					// $rtgsccyinsert = $this->_getParam('rtgsccy');
					$sknamtinsert = $this->_getParam('skn');
					// $sknccyinsert = $this->_getParam('sknccy');

					$domamtinsert = $this->_getParam('dom');

					// $chargestodom = $this->_getParam('charges_acc_dom');
					$chargestodom = 1;

					// $domccyinsert = $this->_getParam('domccy');
					
					$this->view->rtgsamt = $rtgsamtinsert;
					// $this->view->rtgsccy = $rtgsccyinsert;
					$this->view->sknamt = $sknamtinsert;
					// $this->view->sknccy = $sknccyinsert;
					$this->view->domamt = $domamtinsert;
					// $this->view->domccy = $domccyinsert;
					
					$rtgsamt = Application_Helper_General::convertDisplayMoney($rtgsamtinsert);
					$temp_rtgsamt = str_replace('.','',$rtgsamt);
					$cek_angka = (Zend_Validate::is($temp_rtgsamt,'Digits')) ? true : false;
					
					$sknamt = Application_Helper_General::convertDisplayMoney($sknamtinsert);
					$temp_sknamt = str_replace('.','',$sknamt);
					$cek_angka2 = (Zend_Validate::is($temp_sknamt,'Digits')) ? true : false;

					$domamt = Application_Helper_General::convertDisplayMoney($domamtinsert);
					$temp_domamt = str_replace('.','',$domamt);
					$cek_angka3 = (Zend_Validate::is($temp_domamt,'Digits')) ? true : false;
					
					$select5 = $this->_db->select()
							->from(array('A' => 'M_MINAMT_CCY'),array('*'))
							->where("CCY_ID LIKE ".$this->_db->quote($rtgsccyinsert))
				 			-> query()->fetchAll();
				 			
				 	$select6 = $this->_db->select()
							->from(array('A' => 'M_MINAMT_CCY'),array('*'))
							->where("CCY_ID LIKE ".$this->_db->quote($sknccyinsert))
				 			-> query()->fetchAll(); 

				 	$select7 = $this->_db->select()
							->from(array('A' => 'M_MINAMT_CCY'),array('*'))
							->where("CCY_ID LIKE ".$this->_db->quote($domccyinsert))
				 			-> query()->fetchAll(); 
					
					if($rtgsamtinsert != null && !$cek_angka)
					{
						$this->view->errrtgsamt = $this->language->_('Amount value has to be numeric');
						$error++;
					}
					
					if($sknamtinsert != null && !$cek_angka2)
					{
						$this->view->errsknamt = $this->language->_('Amount value has to be numeric');
						$error++;
					}

					if($domamtinsert != null && !$cek_angka3)
					{
						$this->view->errdomamt = $this->language->_('Amount value has to be numeric');
						$error++;
					}
					
					if($rtgsamtinsert == null)
					{
						$this->view->errrtgsamt = $this->language->_('Value must be filled');
						$error++;
					}
					
					if($sknamtinsert == null)
					{
						$this->view->errsknamt = $this->language->_('Value must be filled');
						$error++;
					}

					if($domamtinsert == null)
					{
						$this->view->errdomamt = $this->language->_('Value must be filled');
						$error++;
					}
					
					// if(!$select5)
					// {
					// 	$this->view->errrtgsccy = $this->language->_('Incorrect currency');
					// 	$error++;
					// }
					
					// if(!$select6)
					// {
					// 	$this->view->errsknccy = $this->language->_('Incorrect currency');
					// 	$error++;
					// }

					// if(!$select7)
					// {
					// 	$this->view->errdomccy = $this->language->_('Incorrect currency');
					// 	$error++;
					// }

					if($error == 0)
					{

							$data1 = array(
											'CHANGES_ID'	=> $change_id,
											'CUST_ID' 		=> $custid,
											'CHARGES_NO'	=> $chargestodom,
											'CHARGES_TYPE'	=> '1',
											'CHARGES_CCY' 	=> 'IDR',
											'CHARGES_AMT' 	=> $rtgsamt,
											'CHARGES_SUGGESTEDBY'=> $this->_userIdLogin,
											'CHARGES_SUGGESTED'	=> new Zend_Db_Expr('now()')
											);

							$this->_db->insert('TEMP_CHARGES_OTHER',$data1);
							//echo 'data1';die;
							$data2 = array(
											'CHANGES_ID'	=> $change_id,
											'CUST_ID' 		=> $custid,
											'CHARGES_NO'	=> $chargestodom,
											'CHARGES_TYPE'	=> '2',
											'CHARGES_CCY' 	=> 'IDR',
											'CHARGES_AMT' 	=> $sknamt,
											'CHARGES_SUGGESTEDBY'=> $this->_userIdLogin,
											'CHARGES_SUGGESTED'	=> new Zend_Db_Expr('now()')
											);
							$this->_db->insert('TEMP_CHARGES_OTHER',$data2);


							$data3 = array(
											'CHANGES_ID'	=> $change_id,
											'CUST_ID' 		=> $custid,
											'CHARGES_NO'	=> $chargestodom,
											'CHARGES_TYPE'	=> '8',
											'CHARGES_CCY' 	=> 'IDR',
											'CHARGES_AMT' 	=> $domamt,
											'CHARGES_SUGGESTEDBY'=> $this->_userIdLogin,
											'CHARGES_SUGGESTED'	=> new Zend_Db_Expr('now()')
											);
							$this->_db->insert('TEMP_CHARGES_OTHER',$data3);
							//die;
							$commit = true;
					}
					
					if($error == 0 && $commit)
					{	
						//echo 'commit';die;
						$this->_db->commit();
						$this->setbackURL('/monthlycharges/monthlyfee/index/custid/'.$enccustid);						    
						$this->_redirect('/notification/submited/index');
						Application_Helper_General::writeLog('CHUD','Updating Domestic charges ('.$custid.')');
					}
				}
				catch(Exception $e)
				{
					//echo 'rollback';die;
					$this->_db->rollBack();
				}



			}else if($submit && $this->_getParam('chargetypes') == 'direct'){
				$this->_db->beginTransaction();
				try
				{
					$info = 'Charges';
					$info2 = 'Direct Debit Charges';
					$error = 0;
					
					
					
					$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['edit'],null,'M_CHARGES_DIRECT','TEMP_CHARGES_DIRECT',$custid,$custname,$custid,null,'directchar');
					
					$dirinhouseinsert = $this->_getParam('direct_inhouse');
					// $rtgsccyinsert = $this->_getParam('rtgsccy');
					$dirdominsert = $this->_getParam('direct_dom');
					// $sknccyinsert = $this->_getParam('sknccy');

					// $domamtinsert = $this->_getParam('dom');

					$chargestodirect = $this->_getParam('charges_acc_direct');
					// $domccyinsert = $this->_getParam('domccy');
					
					$this->view->direct_inhouseamt = $dirinhouseinsert;
					// $this->view->rtgsccy = $rtgsccyinsert;
					$this->view->direct_dom = $dirdominsert;
					// $this->view->sknccy = $sknccyinsert;
					// $this->view->domamt = $domamtinsert;
					// $this->view->domccy = $domccyinsert;
					
					$direct_inhouseamt = Application_Helper_General::convertDisplayMoney($dirinhouseinsert);
					$temp_direct_inhouseamt = str_replace('.','',$direct_inhouseamt);
					$cek_angka = (Zend_Validate::is($temp_direct_inhouseamt,'Digits')) ? true : false;
					
					$direct_domamt = Application_Helper_General::convertDisplayMoney($dirdominsert);
					$temp_direct_domamt = str_replace('.','',$direct_domamt);
					$cek_angka2 = (Zend_Validate::is($temp_direct_domamt,'Digits')) ? true : false;

					// $domamt = Application_Helper_General::convertDisplayMoney($domamtinsert);
					// $temp_domamt = str_replace('.','',$domamt);
					// $cek_angka3 = (Zend_Validate::is($temp_domamt,'Digits')) ? true : false;
					
					
					
					if($dirinhouseinsert != null && !$cek_angka)
					{
						$this->view->errrtgsamt = $this->language->_('Amount value has to be numeric');
						$error++;
					}
					
					if($dirdominsert != null && !$cek_angka2)
					{
						$this->view->errsknamt = $this->language->_('Amount value has to be numeric');
						$error++;
					}

					
					if($dirinhouseinsert == null)
					{
						$this->view->errrtgsamt = $this->language->_('Value must be filled');
						$error++;
					}
					
					if($dirdominsert == null)
					{
						$this->view->errsknamt = $this->language->_('Value must be filled');
						$error++;
					}


					if($error == 0)
					{
							$data1 = array(
											'CHANGES_ID'	=> $change_id,
											'CUST_ID' 		=> $custid,
											'CHARGES_INHOUSE'	=> $dirinhouseinsert,
											'CHARGES_DOM'	=> $dirdominsert,
											'ACCT_NO' 	=> $chargestodirect,
											'CHARGES_SUGGESTEDBY'=> $this->_userIdLogin,
											'CHARGES_SUGGESTED'	=> new Zend_Db_Expr('now()')
											// 'CHARGES_AMT' 	=> $rtgsamt,
											);

							$this->_db->insert('TEMP_CHARGES_DIRECT',$data1);
							//echo 'data1';die;
							
							//die;
							$commit = true;
					}
					
					if($error == 0 && $commit)
					{	
						//echo 'commit';die;
						$this->_db->commit();
						$this->setbackURL('/monthlycharges/monthlyfee/index/custid/'.$enccustid);						    
						$this->_redirect('/notification/submited/index');
						Application_Helper_General::writeLog('CHUD','Updating Domestic charges ('.$custid.')');
					}
				}
				catch(Exception $e)
				{
					//echo 'rollback';die;
					$this->_db->rollBack();
				}



			}else if($submit && $this->_getParam('chargetypes') == 'valas'){
				$this->_db->beginTransaction();
				try
				{
					$info = 'Charges';
					$info2 = 'Valas Charges';
					$error = 0;
					
					
					$chargestovalas = $this->_getParam('charges_acc_valas');
					$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['edit'],null,'M_CHARGES_REMITTANCE','TEMP_CHARGES_REMITTANCE',$custid,$custname,$custid,null,'remittancechar');
					$params = $this->getRequest()->getParams();
					// echo '<pre>';
					// print_r($params);die;
					$ccy = $params['ccy'];
						$cust_id = $custid;
					foreach ($params['ccy'] as $val) {
				$type = 'charges_type'.$ccy;
				$type2 = $params[$type];

				$ccytf = 'ccytf'.$val;
				$ccytf2 = $params[$ccytf];

				$tfamount = 'tfamount'.$val;
				$tfamount2 = $params[$tfamount];

				$ccyfa = 'ccyfa'.$val;
				$ccyfa2 = $params[$ccyfa];

				$faamount = 'faamount'.$val;
				$faamount2 = $params[$faamount];

				$ccypf = 'ccypf'.$val;
				$ccypf2 = $params[$ccypf];

				$min = 'min'.$val;
				$min2 = $params[$min];

				$max = 'max'.$val;
				$max2 = $params[$max];

				$pcy = 'pcy'.$val;
				$pcy2 = $params[$pcy];

				if ($ccytf2!='-') {

					$charge_amt =	Application_Helper_General::convertDisplayMoney($tfamount2);
					// $ccytf3 = $ccytf2;
					$info = 'Remittance Charges';
					$info2 = 'Set Edit New Charges';

					// $change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['new'],null,'M_CHARGES_REMITTANCE','TEMP_CHARGES_REMITTANCE','-','Transfer Fee',$cust_id,$cust_name);

					$params_insert_3 = array(
				        'CHANGES_ID' => $change_id,
				        'CUST_ID' => $cust_id,
				        'CHARGE_TYPE' => '3',
				        'CHARGE_CCY' => $val,
				        'CHARGE_AMOUNT_CCY' => $ccytf2,
				        'CHARGE_AMT' => $charge_amt,
				        'CHARGE_ACCT' => $chargestovalas,
				        'CHARGE_SUGGESTEDBY'=> $this->_userIdLogin,
						'CHARGE_SUGGESTED'	=> new Zend_Db_Expr('now()')
				    
				    );
				    try {
				    	$this->_db->insert('TEMP_CHARGES_REMITTANCE',$params_insert_3);
				    } catch (Exception $e) {
				    	print_r($e);die;
				    }

				    

				}

				if ($ccyfa2!='-') {

					$charge_amt =	Application_Helper_General::convertDisplayMoney($faamount2);
					// $ccytf3 = $ccytf2;
					$info = 'Remittance Charges';
					$info2 = 'Set Edit New Charges';

					// $change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['new'],null,'M_CHARGES_REMITTANCE','TEMP_CHARGES_REMITTANCE','-','Full Amount Fee',$cust_id,$cust_name);

					$params_insert_4 = array(
				        'CHANGES_ID' => $change_id,
				        'CUST_ID' => $cust_id,
				        'CHARGE_TYPE' => '4',
				        'CHARGE_CCY' => $val,
				        'CHARGE_AMOUNT_CCY' => $ccytf2,
				        'CHARGE_AMT' => $charge_amt,
				        'CHARGE_ACCT' => $chargestovalas,
				        'CHARGE_SUGGESTEDBY'=> $this->_userIdLogin,
						'CHARGE_SUGGESTED'	=> new Zend_Db_Expr('now()')
				    
				    );
				    try {
				    	$this->_db->insert('TEMP_CHARGES_REMITTANCE',$params_insert_4);	
				    } catch (Exception $e) {
				    	print_r($e);die;
				    }
				    

				}

				if ($ccypf2!='-') {

					$charge_min =	Application_Helper_General::convertDisplayMoney($min2);
					$charge_max =	Application_Helper_General::convertDisplayMoney($max2);
					// $ccytf3 = $ccytf2;
					$info = 'Remittance Charges';
					$info2 = 'Set Edit New Charges';

					// $change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['new'],null,'M_CHARGES_REMITTANCE','TEMP_CHARGES_REMITTANCE','-','Provision Fee',$cust_id,$cust_name);

					$params_insert_5 = array(
									'CHANGES_ID' => $change_id,
									'CUST_ID' => $cust_id,
									'CHARGE_TYPE' => '5',
									'CHARGE_CCY' => $val,
									'CHARGE_AMOUNT_CCY' => $ccypf2,
									'CHARGE_PROV_MIN_AMT' => $charge_min,
									'CHARGE_PROV_MAX_AMT' => $charge_max,
									'CHARGE_PCT' => $pcy2,
									'CHARGE_ACCT' => $chargestovalas,
									'CHARGE_SUGGESTEDBY'=> $this->_userIdLogin,
									'CHARGE_SUGGESTED'	=> new Zend_Db_Expr('now()')
					);

					try {
						$this->_db->insert('TEMP_CHARGES_REMITTANCE',$params_insert_5);	
					} catch (Exception $e) {
						print_r($e);die;
					}

				    

				}
					$commit = true;
			}
			// $this->setbackURL('/companycharges/monthlyfee/index/custid/GLOBAL');
			// $this->_redirect('/notification/submited/index');
					
					if($error == 0 && $commit)
					{	
						//echo 'commit';die;
						$this->_db->commit();
						$this->setbackURL('/monthlycharges/monthlyfee/index/custid/'.$enccustid);						    
						$this->_redirect('/notification/submited/index');
						Application_Helper_General::writeLog('CHUD','Updating Domestic charges ('.$custid.')');
					}
				}
				catch(Exception $e)
				{
					//echo 'rollback';die;
					$this->_db->rollBack();
				}



			}


		}
		if($cek || $cek2 || $cek3 || $cek4 || $cek5)
		{			
			$docErr = "*".$this->language->_('No changes allowed for this record while awaiting approval for previous change')."";	
			$this->view->error = $docErr;
			$this->view->changestatus = $this->language->_("disabled");
		}
		Application_Helper_General::writeLog('CHUD','Update Company charges');
	}
}
