<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class eformworkflow_ReviewController extends Application_Main
{

  protected $_moduleDB = 'RTF'; // masih harus diganti

  public function indexAction()
  {
    $this->_helper->_layout->setLayout('newlayout');

    $settings = new Settings();
    $system_type = $settings->getSetting('system_type');
    $this->view->systemType = $system_type;

    $conf = Zend_Registry::get('config');
    $this->_bankName = $conf['app']['bankname'];
    $this->view->masterbankname = $this->_bankName;

    $this->CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

    $fields = array(
      /*'alias'  => array('field' => 'BENEFICIARY_ALIAS',
                                   'label' => $this->language->_('Alias Name'),
                                   'sortable' => true),*/
                                   'regno'     => array(
                                    'field'    => 'BG_REG_NUMBER',
                                    'label'    => $this->language->_('RegNumber') . ' / ' . $this->language->_('Subject'),
                                  ),
                                   'app' => array(
                                    'field' => 'CUST_ID',
                                    'label' => $this->language->_('Aplicant'),
                                  ),

                                   'obligee' => array(
                                    'field' => 'RECIPIENT_NAME',
                                    'label' => $this->language->_('Obligee'),
                                  ),

                                   'branch'     => array(
                                    'field'    => 'BG_BRANCH',
                                    'label'    => $this->language->_('Branch'),
                                  ),

                                   'countertype'     => array(
                                    'field'    => 'COUNTER_WARRANTY_TYPE',
                                    'label'    => $this->language->_('Counter Type'),
                                  ),

                                   'amount' => array(
                                    'field' => 'BG_AMOUNT',
                                    'label' => $this->language->_('BG Amount'),
                                  ),




      // 'startdate' => array(
      //   'field' => 'TIME_PERIOD_START',
      //   'label' => $this->language->_('Start Date'),
      // ),
      // 'enddate'   => array(
      //   'field'    => 'TIME_PERIOD_END',
      //   'label'    => $this->language->_('End Date'),
      // )
                                   'type'  => array(
                                    'field'    => 'CHANGE_TYPE',
                                    'label'    => $this->language->_('Type')
                                  )
                                 );


    //add reza
    $filterlist = array("BG_REG_NUMBER", "BG_SUBJECT", "APPLICANT", "BRANCH", "COUNTER_TYPE", "TYPE");

    $this->view->filterlist = $filterlist;

    $page    = $this->_getParam('page');

    $sortBy  = ($this->_getParam('sortby')) ?  $this->_getParam('sortby') : ('BG_UPDATED');
    $sortBy  = (Zend_Validate::is($sortBy, 'InArray', array(array_keys($fields)))) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

    $sortDir = $this->_getParam('sortdir');
    $sortDir = (Zend_Validate::is($sortDir, 'InArray', array('haystack' => array('asc', 'desc')))) ? $sortDir : 'desc';

    $this->view->currentPage = $page;
    $this->view->sortBy      = $sortBy;
    $this->view->sortDir     = $sortDir;

    //end reza

    // Add Bahri
    if ($this->view->hasPrivilege("RCCS")) { // Cash Collateral
      $warantyIn = ['1'];
    }
    if ($this->view->hasPrivilege("RNCS")) { // Non-Cash Collateral
      $warantyIn = ['2', '3'];
    }
    if ($this->view->hasPrivilege("RCCS") && $this->view->hasPrivilege("RNCS")) { // All
      $warantyIn = ['1', '2', '3'];
    }
    $select = $this->_db->select()
    ->from(array('A' => 'M_BUSER'), array('BUSER_BRANCH'))
    ->where('A.BUSER_ID = ?', $this->_userIdLogin);
    $buser = $this->_db->fetchRow($select);
    $branchUser = $buser['BUSER_BRANCH'];
    // END Bahri

    $selectbg = $this->_db->select()
    ->from(array('A' => 'TEMP_BANK_GUARANTEE'), array(
      'REG_NUMBER' => 'BG_REG_NUMBER',
      'SUBJECT' => 'BG_SUBJECT',
      'CREATED' => 'BG_CREATED',
        //'TYPE' => (string)'TYPE',
      'TIME_PERIOD_START',
      'TIME_PERIOD_END',
      'CREATEDBY' => 'BG_CREATEDBY',
      'AMOUNT' => 'BG_AMOUNT',
      'COUNTER_WARRANTY_TYPE',
      'BG_INSURANCE_CODE',
      'FULLNAME' => 'T.USER_FULLNAME',
      'SP_OBLIGEE_CODE',
      'IS_AMENDMENT' => 'A.CHANGE_TYPE',
      'RECIPIENT_NAME',
      'B.BRANCH_NAME',
      'C.CUST_NAME'

    ))

    ->join(array('T' => 'M_USER'), 'A.BG_CREATEDBY = T.USER_ID')
    ->join(array('C' => 'M_CUSTOMER'), 'A.CUST_ID = C.CUST_ID')
    ->join(array('B' => 'M_BRANCH'), 'A.BG_BRANCH = B.BRANCH_CODE')

    ->where('A.BG_STATUS = 6')

      ->where('A.COUNTER_WARRANTY_TYPE IN (?)', $warantyIn) // Add Bahri
      ->order('A.BG_CREATED DESC');
      $auth = Zend_Auth::getInstance()->getIdentity();
      if($auth->userHeadQuarter == "NO"){
        $selectbg->where('B.ID = ?', $branchUser); // Add Bahri
      }
      
    //->query()->fetchAll();


      $selectlc = $this->_db->select()
      ->from(array('A' => 'T_LC'), array(
        'REG_NUMBER' => 'LC_REG_NUMBER',
        'SUBJECT' => 'LC_CREDIT_TYPE',
        'CREATED' => 'LC_CREATED',
        'CCYID' => 'LC_CCY',
        //'TYPE' => (string)'TYPE',
        'TIME_PERIOD_END' => 'LC_EXPDATE',
        'CREATEDBY' => 'LC_CREATEDBY',
        'AMOUNT' => 'LC_AMOUNT',
        'FULLNAME' => 'T.USER_FULLNAME'
      ))
      ->join(array('T' => 'M_USER'), 'A.LC_CREATEDBY = T.USER_ID')

      ->where('A.LC_STATUS = 1')
      ->order('A.LC_CREATED DESC');
    //->query()->fetchAll();


    // $result = array_merge($selectbg, $selectlc);

    // echo "<pre>";
    // print_r($result);die;

    // $this->paging($result);


    // select branch ------------------------------------------------------------
      $select_branch = $this->_db->select()
      ->from(array('A' => 'M_BRANCH'), array("BRANCH_CODE", "BRANCH_NAME"))
      ->query()->fetchAll();

      $save_branch = [];

      foreach ($select_branch as $key => $value) {
        $save_branch[$value["BRANCH_CODE"]] = $value["BRANCH_NAME"];
      }

      $this->view->sel_branch = $save_branch;
    //  --------------------------------------------------------------------------

    // select counter type ------------------------------------------------------------
      $save_counter_type = [
        1 => 'FC',
        2 => 'LF',
        3 => 'Insurance'
      ];

      $this->view->counter_type = $save_counter_type;
    //  -------------------------------------------------------------------------- 
	
	// select type ------------------------------------------------------------
    $arr_type = [
      0 => 'New',
      1 => 'Amendment Changes',
      2 => 'Amendment Draft'
    ];

    $this->view->arr_type = $arr_type;
    //  -------------------------------------------------------------------------- 

      $conf = Zend_Registry::get('config');


      $this->view->bankname = $conf['app']['bankname'];


      $config        = Zend_Registry::get('config');
      $BgType     = $config["bg"]["status"]["desc"];
      $BgCode     = $config["bg"]["status"]["code"];

      $arrStatus = array_combine(array_values($BgCode), array_values($BgType));

      $this->view->arrStatus = $arrStatus;


      $arrWaranty = array(
        1 => 'FC',
        2 => 'LF',
        3 => 'INS'
      );
      $this->view->arrWaranty = $arrWaranty;

      $arrType = array(
        1 => 'Standart',
        2 => 'Custom'
      );

      $arrLang = array(
        1 => 'Indonesian',
        2 => 'English',
        3 => 'Bilingual'
      );
      $this->view->langArr = $arrLang;
      $this->view->formatArr = $arrType;
      $this->view->fields = $fields;

      $filterArr = array(
		  'filter'  =>  array('StripTags'),
		  'BG_REG_NUMBER'     =>  array('StringTrim', 'StripTags', 'StringToUpper'),
		  'BG_SUBJECT'     =>  array('StringTrim', 'StripTags', 'StringToUpper'),
		  'APPLICANT'     =>  array('StringTrim', 'StripTags', 'StringToUpper'),
		  'BRANCH'     =>  array('StringTrim', 'StripTags', 'StringToUpper'),
		  'COUNTER_TYPE'     =>  array('StringTrim', 'StripTags', 'StringToUpper'),
		  'TYPE' => array('StripTags', 'StringTrim', 'StringToUpper'),
	  );

      $dataParam = array("BG_REG_NUMBER", "BG_SUBJECT", "APPLICANT", "BRANCH", "COUNTER_TYPE","TYPE");
      $dataParamValue = array();

      foreach ($dataParam as $dtParam) {
        if (!empty($this->_request->getParam('wherecol'))) {
          $dataval = $this->_request->getParam('whereval');
          foreach ($this->_request->getParam('wherecol') as $key => $value) {
            if ($dtParam == $value) {
              $dataParamValue[$dtParam] = $dataval[$key];
            }
          }
        }
      }
		
      if (!empty($this->_request->getParam('efdate'))) {
        $efdatearr = $this->_request->getParam('efdate');
        $dataParamValue['BG_PERIOD'] = $efdatearr[0];
        $dataParamValue['BG_PERIOD_END'] = $efdatearr[1];
      }
	  
	  

    $validator = array(
      'BG_REG_NUMBER'     =>  array(),
      'BG_SUBJECT'     =>  array(),
      'APPLICANT'     =>  array(),
      'BRANCH'     =>  array(),
      'COUNTER_TYPE'     =>  array(),
      'TYPE'     =>  array(),
    );


    // echo "<pre>";
    // print_r($dataParamValue);die;


      $zf_filter   = new Zend_Filter_Input($filterArr, $validator, $dataParamValue);

      if ($zf_filter->isValid()) {
		 
        $filter     = TRUE;
      }


       $filter     = $this->_getParam('filter');
		$BG_REG_NUMBER    = html_entity_decode($zf_filter->getEscaped('BG_REG_NUMBER'));
		$BG_SUBJECT    = html_entity_decode($zf_filter->getEscaped('BG_SUBJECT'));
		$APPLICANT    = html_entity_decode($zf_filter->getEscaped('APPLICANT'));
		$BRANCH    = html_entity_decode($zf_filter->getEscaped('BRANCH'));
		$COUNTER_TYPE    = html_entity_decode($zf_filter->getEscaped('COUNTER_TYPE'));
		$LAST_SAVED_BY    = html_entity_decode($zf_filter->getEscaped('LAST_SAVED_BY'));
		$TYPE    = html_entity_decode($zf_filter->getEscaped('TYPE'));


      if ($filter == null) {
        $datefrom = (date("d/m/Y"));
        $dateto = (date("d/m/Y"));
        $this->view->fDateFrom  = (date("d/m/Y"));
        $this->view->fDateTo  = (date("d/m/Y"));
      }

      if ($filter_clear == '1') {
        $this->view->fDateFrom  = '';
        $this->view->fDateTo  = '';
        $datefrom = '';
        $dateto = '';
      }

      
	  if ($filter == TRUE) {
		
		
		  if ($BG_REG_NUMBER != null) {
			$selectbg->where("A.BG_REG_NUMBER LIKE " . $this->_db->quote('%' . $BG_REG_NUMBER . '%'));
			
		  }

		  if ($BG_SUBJECT != null) {
			$selectbg->where("A.BG_SUBJECT LIKE " . $this->_db->quote('%' . $BG_SUBJECT . '%'));
			
		  }

		  if ($APPLICANT   != null) {
			$selectbg->where("C.CUST_NAME LIKE " . $this->_db->quote('%' . $APPLICANT . '%'));
			
		  }

		  if ($BRANCH     != null) {
			$selectbg->where("B.BRANCH_CODE LIKE " . $this->_db->quote('%' . $BRANCH . '%'));
			
		  }

		  if ($COUNTER_TYPE     != null) {
			$selectbg->where("A.COUNTER_WARRANTY_TYPE LIKE " . $this->_db->quote('%' . $COUNTER_TYPE . '%'));
		   
		  }
		  
		  if ($TYPE     != null) {
			$selectbg->where("A.CHANGE_TYPE LIKE " . $this->_db->quote('%' . $TYPE . '%'));
			
		  }
    }
	//echo $selectbg;
    // $selectbg->order($sortBy.' '.$sortDir);


      $this->view->fields = $fields;
      $this->view->filter = $filter;

      $selectbg = $this->_db->fetchAll($selectbg);
      $selectlc = $this->_db->fetchAll($selectlc);
      $result = array_merge($selectbg, $selectlc);

      $setting = new Settings();
      $enc_pass = $setting->getSetting('enc_pass');
      $enc_salt = $setting->getSetting('enc_salt');
      $sessionNamespace = new Zend_Session_Namespace('Tokenenc');
      $pw_hash = md5($enc_salt . $enc_pass);
      $rand = $this->_userIdLogin . date('dHis') . $pw_hash;
      $sessionNamespace->token   = $rand;
      $this->view->token = $sessionNamespace->token;

      foreach ($result as $key => $value) {
        $get_reg_number = $value["REG_NUMBER"];

        $AESMYSQL = new Crypt_AESMYSQL();
        $rand = $this->token;

        $encrypted_payreff = $AESMYSQL->encrypt($get_reg_number, $rand);
        $encpayreff = urlencode($encrypted_payreff);

        $result[$key]["REG_NUMBER_ENCRYPTED"] = $encpayreff;
      }

    //Zend_Debug::dump($result);

      $this->paging($result);

      if (!empty($dataParamValue)) {

        $this->view->efdateStart = $dataParamValue['BG_PERIOD'];
        $this->view->efdateEnd = $dataParamValue['BG_PERIOD_END'];

        foreach ($dataParamValue as $key => $value) {
          $duparr = explode(',', $value);
          if (!empty($duparr)) {

            foreach ($duparr as $ss => $vs) {
              $wherecol[]  = $key;
              $whereval[] = $vs;
            }
          } else {
            $wherecol[]  = $key;
            $whereval[] = $value;
          }
        }
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
      }


      $allData = [];

      if(!empty($this->_getParam('csv')) || $this->_request->getParam('print') == 1){

        foreach ($result as $row) {
          $subData = [];

          if ($row['SUBJECT'] == '') {
            $row['SUBJECT'] = '- no subject -';
          } else if ($row['SUBJECT'] == '1') {
            $row['SUBJECT'] = 'Letter of Credit';
          } else if ($row['SUBJECT'] == '2') {
            $row['SUBJECT'] = 'Surat Kredit Berdokumen Dalam Negeri';
          }

          if (isset($row['CCYID'])) {
            $ccyid = $row['CCYID'];
          } else {
            $ccyid = "IDR";
          }

          $subData['REG_NUMBER'] = $row['REG_NUMBER']." / ".$row['SUBJECT'];
          $subData['CUSTOMER'] = $row['CUST_NAME'] . '(' . $row['CUST_ID'] . ')';
          $subData['RECIPIENT_NAME'] = $row['RECIPIENT_NAME'];
          $subData['BRANCH_NAME'] = $row['BRANCH_NAME'];
          $subData['COUNTER_WARRANTY_TYPE'] = $arrWaranty[$row['COUNTER_WARRANTY_TYPE']];
          $subData['AMOUNT'] = $ccyid . ' ' . Application_Helper_General::displayMoneyplain($row['AMOUNT']);


          if ($row['IS_AMENDMENT'] == 1) {
            $subData['IS_AMENDMENT'] = 'Amendment Changes';
          } else if ($row['IS_AMENDMENT'] == 0) {
            $subData['IS_AMENDMENT'] = "New";
          } else {
            $subData['IS_AMENDMENT'] = "Amendment Draft";
          }

          $allData[] = $subData;
        }

      }

      if ($this->_getParam('csv')) {

        $this->_helper->download->csv(array($this->language->_('RegNumber / Subject'), $this->language->_('Aplicant'), $this->language->_('Obligee'), $this->language->_('Branch'), $this->language->_('Counter Type'), $this->language->_('BG Amount'), $this->language->_('Type')), $allData, null, 'Review Process List');

      } else if ($this->_request->getParam('print') == 1) {

        $fields = array(
          'regno'     => array(
            'field'    => 'REG_NUMBER',
            'label'    => $this->language->_('RegNumber / Subject'),
          ),
          'aplicant'     => array(
            'field'    => 'CUSTOMER',
            'label'    => $this->language->_('Aplicant'),
          ),
          'recipientname'     => array(
            'field'    => 'RECIPIENT_NAME',
            'label'    => $this->language->_('Obligee'),
          ),
          'bankbranch'     => array(
            'field'    => 'BRANCH_NAME',
            'label'    => $this->language->_('Branch'),
          ),
          'counter'     => array(
            'field'    => 'COUNTER_WARRANTY_TYPE',
            'label'    => $this->language->_('Counter Type'),
          ),
          'bgamount'  => array(
            'field'    => 'AMOUNT',
            'label'    => $this->language->_('BG Amount'),
          ),
          'type'  => array(
            'field'    => 'IS_AMENDMENT',
            'label'    => $this->language->_('Type'),
          ),
        );

        $this->_forward('print', 'index', 'widget', array('data_content' => $allData, 'data_caption' => 'Review Process List', 'data_header' => $fields));
      }
    }
  }
