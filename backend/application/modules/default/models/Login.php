<?php
Class Default_Model_Login extends Application_Main{
	
	public function getPriviId($groupArr,$type=null)
    {
    	
	     $select = $this->_db->select()
						     ->from(array('BPG' => 'M_BPRIVI_GROUP'), array('BPG.BPRIVI_ID'))
							 ->join(array('BG' => 'M_BGROUP'), 'BPG.BGROUP_ID = BG.BGROUP_ID', array())
							 ->joinLeft(array('BP' => 'M_BPRIVILEGE'), 'BPG.BPRIVI_ID = BP.BPRIVI_ID',array())
							 ->where("BPG.BGROUP_ID IN (?)",$groupArr);
		if($type=='1'){
			$select->where('BP.BPRIVI_MODE IN (0,1)');
		  }else if($type=='2'){
			$select->where('BP.BPRIVI_MODE IN (0,2)');
		  }
		//					 echo $select;die;
		$select = $select->query()->fetchAll();
		return $select;					 
	}
	
	public function getGroupId()
    {
	   $select = $this->_db->fetchAll("SELECT UPPER(BGROUP_ID) AS BGROUP_ID FROM M_BGROUP WHERE BGROUP_STATUS = 'A'");
	   return $select;
    }

    public function getPriviMod()
    {
         $select = $this->_db->select()
								->distinct()
								->from (array('M' => 'M_MODULE'), 
										array('M.SYSTEM_MODULE'))
								->join (array('A' => 'M_MODULE_ACTION'), 'M.MODULE_ID = A.MODULE_ID', array())
								->join (array('P' => 'M_BPRIVI_ACTION'), 'A.MODULE_ACTION_ID = P.MODULE_ACTION_ID', array())
								->where("M.SYSTEM_MODULE <> ''")
								->where("M.SYSTEM_MODULE IS NOT NULL")
								->query()->fetchAll();
		return $select;						
    } 
    
}



