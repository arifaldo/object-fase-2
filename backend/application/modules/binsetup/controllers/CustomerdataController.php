<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';

class binsetup_CustomerdataController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti
	
	public function initController()
	{       
		$listCcy = array(''=>'-- Select Currency --');
		$listCcy = array_merge($listCcy,Application_Helper_Array::listArray($this->getCcy(),'CCY_ID','DESCRIPTION'));
		$this->view->ccy = $listCcy;
		$this->_helper->layout()->setLayout('popup');
	}

	public function indexAction()
	{
		$fields = array(
						'cust_id'  => array('field' => 'B.CUST_ID',
											   'label' => $this->language->_('Company Code'),
											   'sortable' => true),
						'cust_name'  => array('field' => 'B.CUST_NAME',
											   'label' => $this->language->_('Company Name'),
											   'sortable' => true),
						'date'   => array('field'    => 'B.CUST_CREATED',
											  'label'    => $this->language->_('Created Date'),
											  'sortable' => true),
				);
				
		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','favorite');
		$sortDir = $this->_getParam('sortdir','desc');
		$id_box = $this->_getParam('id_box');
		//echo $id_box;
		//Zend_Debug::dump($this->_getParam());
		//exit();
		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';
	  

		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	'filter' 	  	=> array('StringTrim','StripTags'),
							'cust_id' 	  	=> array('StringTrim','StripTags'),
							'cust_name'    => array('StringTrim','StripTags'),
		);
		
		$zf_filter = new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		$filter = $zf_filter->getEscaped('filter');

		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		$this->view->id_box = $id_box;
		
		$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
//		$select   = $CustUser->getBeneficiaries($this->view->hasPrivilege('BLBU'));
		$select   = $CustUser->getAllCustomerBin();
		
		if($filter == TRUE)
		{
			$cust_id = $zf_filter->getEscaped('cust_id');
			$cust_name = $zf_filter->getEscaped('cust_name');
	        if($cust_id)$select->where('UPPER(B.CUST_ID) LIKE '.$this->_db->quote('%'.strtoupper($cust_id).'%'));
	        if($cust_name)$select->where('B.CUST_NAME LIKE '.$this->_db->quote('%'.strtoupper($cust_name).'%'));
			
			$this->view->cust_id = $cust_id;
			$this->view->cust_name = $cust_name;
		}
	
	    $select->order($sortBy.' '.$sortDir);   
	    $select->group('B.CUST_ID');
	    // echo $select;die;		
	    $this->paging($select);
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		$this->view->dateTimeDisplayFormat = $this->_dateTimeDisplayFormat;
	    
		//$this->_helper->download->csv(array('Group ID','Group Name','Status'),null,$select,'download group');
	}
	
	
	private function fillParam($zf_filter_input)
	{
		if(isset($zf_filter_input->BENEFICIARY_ID))$this->view->BENEFICIARY_ID = ($zf_filter_input->isValid('BENEFICIARY_ID')) ? $zf_filter_input->BENEFICIARY_ID : $this->_getParam('BENEFICIARY_ID'); 
		$this->view->ACBENEF_ALIAS = ($zf_filter_input->isValid('ACBENEF_ALIAS')) ? $zf_filter_input->ACBENEF_ALIAS : $this->_getParam('ACBENEF_ALIAS'); 
		$this->view->ACBENEF = ($zf_filter_input->isValid('ACBENEF')) ? $zf_filter_input->ACBENEF : $this->_getParam('ACBENEF');
		$this->view->CURR_CODE = ($zf_filter_input->isValid('CURR_CODE')) ? $zf_filter_input->CURR_CODE : $this->_getParam('CURR_CODE');
		$this->view->ACBENEF_EMAIL = ($zf_filter_input->isValid('ACBENEF_EMAIL')) ? $zf_filter_input->ACBENEF_EMAIL : $this->_getParam('ACBENEF_EMAIL');
	}

}
