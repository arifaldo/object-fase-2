<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';
ini_set("display_errors","Off");
class binsetup_ViewController extends binsetup_Model_Binsetup
{

    private $_binType;
    private $_vaType;
    private $_payType;

    public function initController()
    {       
      $this->_binType = $this->_bin['type'];
      $this->_vaType = $this->_bin['virtualaccounttype'];
      $this->_payType = $this->_bin['partialtype'];
      $this->_billMethod = $this->_bin['billmethod'];
    }

    public function indexAction() 
    {
      $this->_helper->layout()->setLayout('newlayout');
      // die('here');
      $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
      $password = $sessionNamespace->token; 
      $this->view->token = $sessionNamespace->token;

      $pdf = $this->_getParam('pdf');
      $print = $this->_getParam('print');
      $this->view->custID   = $this->_getParam('cust_id');
      $this->view->custBIN   = $this->_getParam('cust_bin');


      $AESMYSQL = new Crypt_AESMYSQL();
      $cust_id = $AESMYSQL->decrypt($this->_getParam('cust_id'), $password);
      $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;

      $cust_bin = strtoupper($AESMYSQL->decrypt($this->_getParam('cust_bin'), $password));
    
       //pengaturan url untuk button back
	   $this->setbackURL('/'.$this->_request->getModuleName().'/view/index/cust_id/'.$this->_getParam('cust_id')); 
       
       if($cust_id && $cust_bin)
       {
          $resultdata = $this->getCustomer($cust_id,$cust_bin);
         
          if($resultdata['CUST_ID'])
          {
      	      $this->view->cust_id      = strtoupper($resultdata['CUST_ID']);
       			  $this->view->cust_name    = $resultdata['CUST_NAME'];
      		    $this->view->cust_bin_status   = strtoupper($resultdata['CUST_BIN_STATUS']);
      		    $this->view->cust_bin   = strtoupper($resultdata['CUST_BIN']);
      		    
      		    
      		    $this->view->cust_created     = $resultdata['BIN_CREATED'];
      		    $this->view->cust_createdby   = $resultdata['BIN_CREATEDBY'];
      		    $this->view->cust_suggested   = $resultdata['BIN_SUGGESTED'];
      		    $this->view->cust_suggestedby = $resultdata['BIN_SUGGESTEDBY'];
      		    $this->view->cust_updated     = $resultdata['BIN_UPDATED'];
      		    $this->view->cust_updatedby   = $resultdata['BIN_UPDATEDBY'];
              $this->view->cust_lastsync   = $resultdata['BIN_LAST_SYNC'];

              $binTypeArr    = array_combine($this->_binType["code"], $this->_binType["desc"]);

              $optStatus = array(
        //'' => '-- Please Select --',
                '1' => $this->language->_('No'),
                '0' => $this->language->_('Yes')
              );

              $optH2H = array(
        //'' => '-- Please Select --',
                '0' => $this->language->_('No'),
                '1' => $this->language->_('Yes')
              );

              $optSkip = array(
                '0' => $this->language->_('Yes'),
                '1' => $this->language->_('No')
              );

              //------------------------ecollection opt--------------------------------
              $optVA    = array_combine($this->_vaType["code"], $this->_vaType["desc"]);

              $optExpTime = array(
                '0' => $this->language->_('Input Later'),
                '1' => $this->language->_('Predefined')
              );

              $optExTimeType = array(
                '0' => $this->language->_('Minute(s)'),
                '1' => $this->language->_('Hour(s)'),
                '2' => $this->language->_('Day(s)')
              );

              $optPayment    = array_combine($this->_payType["code"], $this->_payType["desc"]);

              $optBill    = array_combine($this->_billMethod["code"], $this->_billMethod["desc"]);

              //----------------------Va Debit opt-------------------------------

              $optRemainingCredit = array(
                '0' => $this->language->_('Accumulate'),
                '1' => $this->language->_('Auto Reset'),
              );

              $this->view->binTypeArr = $binTypeArr;
              $this->view->resultData = $resultdata;

            
              // print_r($resultdata);die;
              $this->view->ccy   = $resultdata['CCY'];
              $this->view->cif   = $resultdata['CUST_CIF'];
              $this->view->bank   = $resultdata['BANK_NAME'];
              $this->view->branch   = $resultdata['BRANCH_NAME'];
              $this->view->account_alias   = $resultdata['ACCT_ALIAS_NAME'];
              $this->view->account   = $resultdata['ACCT_NO'];
              $this->view->account_name   = $resultdata['ACCT_NAME'];
              $this->view->h2h   = !empty($resultdata['HOSTTOHOST']) ? $optH2H[$resultdata['HOSTTOHOST']] : 'No';
              $this->view->url_inquiry   = $resultdata['URL_INQUIRY'];             
              $this->view->url_payment   = $resultdata['URL_PAYMENT'];  

              $this->view->allowskip        = $optSkip[$resultdata['SKIP_ERROR']];

              //ecollection
              $this->view->va_type   = $optVA[$resultdata['VA_TYPE']];
              $this->view->exp_time   = $optExpTime[$resultdata['EXP_TIME']];

              if ($resultdata['EXP_TIME'] == '1') {
                $this->view->exp_time .= ' ('.$resultdata['EX_TIME'].' '.$optExTimeType[$resultdata['EX_TIME_TYPE']].')';
              }

              $this->view->partial_type   = $optPayment[$resultdata['PARTIAL_TYPE']];
              $this->view->bill_method   = $optBill[$resultdata['BILLING_TYPE']];
              $this->view->vp_max   = 'IDR '.Application_Helper_General::displayMoney($resultdata['VP_MAX']);
              $this->view->vp_min   = 'IDR '.Application_Helper_General::displayMoney($resultdata['VP_MIN']);
              $this->view->seller_fee   = 'IDR '.Application_Helper_General::displayMoney($resultdata['SELLER_FEE']);
              $this->view->buyer_fee   = 'IDR '.Application_Helper_General::displayMoney($resultdata['BUYER_FEE']);

              //Va Debit
              $this->view->va_credit_max   = 'IDR '.Application_Helper_General::displayMoney($resultdata['VA_CREDIT_MAX']);
              $this->view->va_daily_limit   = 'IDR '.Application_Helper_General::displayMoney($resultdata['VA_DAILY_LIMIT']);
              $this->view->remainingCredit   = $optRemainingCredit[$resultdata['REMAINING_AVAILABLE_CREDIT']];

              $dayArr = array(
                '0' => 'Monday',
                '1' => 'Tuesday',
                '2' => 'Wednesday',
                '3' => 'Thursday',
                '4' => 'Friday',
                '5' => 'Saturday',
                '6' => 'Sunday',
              );

              $monthArr = array(
                '1' => 'January',
                '2' => 'February',
                '3' => 'March',
                '4' => 'April',
                '5' => 'May',
                '6' => 'June',
                '7' => 'July',
                '8' => 'August',
                '9' => 'September',
                '10' => 'October',
                '11' => 'November',
                '12' => 'December'
              );

              if ($resultdata['BIN_TYPE'] == $this->_binType['vadebit'] && ($resultdata['HOSTTOHOST'] == '0' || $resultdata['HOSTTOHOST'] == '')) {
                if ($resultdata['REMAINING_AVAILABLE_CREDIT'] == '1') {
                  if ($resultdata['EX_TIME_TYPE'] == '2') {
                    $exTime = 'Weekly every '. $dayArr[$resultdata['EX_TIME']];
                  }
                  else if ($resultdata['EX_TIME_TYPE'] == '3') {
                    $exTime = 'Monthly on '. $resultdata['EX_TIME'];
                  }
                  else if ($resultdata['EX_TIME_TYPE'] == '4') {
                    $explodeTime = explode('_', $resultdata['EX_TIME']);

                    $exTime = 'Yearly every '. $explodeTime[0].' '.$monthArr[$explodeTime[1]];
                  }

                  $this->view->remainingCredit .= ' ('.$exTime.')';
                }
              }


      		 $result = $this->getTempCustomerId($cust_id,$cust_bin,TRUE);
      		 // print($result);die;
      		 if($result){
      		 	$resultAll = $this->getTempCustomerId($cust_id,$cust_bin,TRUE);
             $temp = 1; 
      		 }
      		 else{
      		 	$resultAll = $this->getTempCustomerId($cust_id,$cust_bin,false);
            $temp = 0; 
      		 }
      	      
             $this->view->cust_temp = $temp; 
           }
           else $cust_id = null; 
       }// End if cust_id == true
      
        if(!$cust_id)
        {
           $error_remark = 'Invalid Cust ID';
		    //insert log
			try
			{
			   $this->_db->beginTransaction();
			   
			   Application_Helper_General::writeLog('BNUD','View Detail Customer BIN [Invalid Cust ID]');
			   
			   $this->_db->commit();
			}
			catch(Exception $e) 
			{
			   $this->_db->rollBack();
			}
           $this->_helper->getHelper('FlashMessenger')->addMessage('F');
           $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
           $this->_redirect($this->_helper->url->url(array('module'=>$this->_request->getModuleName(),'controller'=>'index','action'=>'index')));	
         }
    
        $this->view->cust_id = $cust_id;
        $this->view->status_type = $this->_masterglobalstatus; 
        $this->view->modulename = $this->_request->getModuleName();
    
        
        
        
        //insert log
        try
        {
	       $this->_db->beginTransaction();
	       
	       Application_Helper_General::writeLog('BNUD','View Customer BIN Detail ['.$cust_id.']');
	       
           $this->_db->commit();
	    }
	    catch(Exception $e) 
	    {
	       $this->_db->rollBack();
	    }
	   
      if($pdf){

        $outputHTML = "<tr><td>".$this->view->render($this->view->controllername.'/pdf.phtml')."</td></tr>";
        //echo $outputHTML;die;
        $this->_helper->download->pdf(null,null,null,'Customer BIN Detail',$outputHTML);
      }

      if($print){
        $this->_forward('pdf', 'view', 'binsetup', array('printpage' => '1', 'data_caption' => 'Customer BIN Detail'));
      }
  }
  public function binsyncAction(){
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

	    $bin = $this->_getParam('bin');


	    $binAcct = $this->_db->fetchAll(
			$this->_db->select()
				 ->from(array('A' => 'M_BIN_SYNC'))
				 ->where("A.BIN = ?", (string)$bin)
		);

	    $result = array();
	    if (!empty($binAcct)) {
        $updateArr['ACCT_NO']	=   $binAcct[0]['ACCT_NO'];
        $whereArr = array( 
            'CUST_BIN = ?' => (string)$bin
          );

        try{
          $acctupdate = $this->_db->update('M_CUSTOMER_BIN',$updateArr,$whereArr);
        }catch (Exception $e) {
            print_r($e);die;
        }
			  $result['acct_no'] = $binAcct[0]['ACCT_NO'];
	    }else{
        $result['not_found'] = '1';
      }

	    echo json_encode($result);
	}
  public function pdfAction(){
    $this->_helper->layout()->disableLayout();
    $param = $this->_request->getParams();
    //        Zend_Debug::dump($param);
    //        die;
    $this->view->data_caption = $param['data_caption'];
    $this->view->printpage = $param['printpage'];
  }	
  
  
}




