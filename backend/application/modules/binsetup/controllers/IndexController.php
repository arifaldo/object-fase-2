<?php

require_once 'Zend/Controller/Action.php';

class binsetup_IndexController extends binsetup_Model_Binsetup
{

  public function initController()
  {
    //$statusArr = Application_Helper_Array::globalvarArray($this->_masteruserStatus);
    $statusArr = array('' => '-- ' . $this->language->_('Any Value') . ' --', '1' => $this->language->_('Approved'), '2' => $this->language->_('Suspended'), '3' => $this->language->_('Deleted'));
    $this->view->statusArr = $statusArr;

    $custArr  = Application_Helper_Array::listArray($this->getAllCustomer(), 'CUST_ID', 'CUST_ID');
    $custArr  = array_merge(array('' => '-- ' . $this->language->_('Any Value') . ' --'), $custArr);
    $this->view->custArr = $custArr;



    $this->view->signArr = array('EQ' => '=', 'NE' => '!=', 'LT' => '<', 'GT' => '>', 'LE' => '<=', 'GE' => '>=');
    $this->view->binType = array('1' => 'eCollection', '2' => 'VA Debit');
    //format display date
    $this->view->dateDisplayFormat = $this->_dateDisplayFormat;
  }

  public function indexAction()
  {

    $setting = new Settings();          
    $enc_pass = $setting->getSetting('enc_pass');
    $enc_salt = $setting->getSetting('enc_salt');
    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');      
    $pw_hash = md5($enc_salt.$enc_pass);
    $rand = $this->_userIdLogin.date('dHis').$pw_hash;
    $sessionNamespace->token  = $rand;
    $this->view->token = $sessionNamespace->token;

    $this->_helper->layout()->setLayout('newlayout');


    $delete     = $this->_getParam('delete');
    $suspend     = $this->_getParam('suspend');
    $unsuspend     = $this->_getParam('unsuspend');


    //suspend
    if ($suspend || $unsuspend || $delete) {
        $binId = $this->_request->getParam('binid');

        $validators = array (
                                'binid' => array   (
                                                        'NotEmpty',
                                                        'messages' => array (
                                                                                'No Selected bin',
                                                                            )
                                                    ),
                            );

        $filtersVal = array ( 'binid' => array('StringTrim','StripTags'));

        $zf_filter_input = new Zend_Filter_Input($filtersVal,$validators,$params,$this->_optionsValidator);
            $success = false;
        foreach ($binId as $key => $value) {
            if(!empty($value)){
                $success = true;
            }
        }


        if($zf_filter_input->isValid() && $success)
        {

            try
            {
                $this->_db->beginTransaction();
                foreach ($binId as $key => $value)
                {

                  $explodeVal = explode('-', $value);
                  $cust_id = $explodeVal[0];
                  $cust_bin = $explodeVal[1];

                  $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;
                  $error_remark = null;
                  // jika customer id valid
                  if($cust_id)
                  {   

                     if ($suspend) {
                       $cust_status = 1;
                     }
                     else if($unsuspend){
                      $cust_status = 2;
                     }
                     else if($delete){
                      $cust_status = null;
                     }

                     $cust_data = $this->getCustomerActIn($cust_id,$cust_bin, $cust_status);
                     
                     if(!empty($cust_data))
                     {   
                          $tempCustomerId = $this->getTempCustomerId($cust_id,$cust_bin,TRUE);
                      if(!$tempCustomerId)
                      {
          //                   $info = 'Customer ID = '.$cust_data['CUST_ID'].', Customer Name = '.$cust_data['CUST_NAME'];
                         $info = 'BIN';
                         //$cust_data['CUST_STATUS'] = $this->_masteruserStatus['code']['inactive'];

                         try 
                         { 

                          $cust_data['BIN_SUGGESTED']    = new Zend_Db_Expr('now()');
                          $cust_data['BIN_SUGGESTEDBY']  = $this->_userIdLogin;

                          if ($suspend) {
                            $cust_data['CUST_BIN_STATUS'] = 2;
                            $changeType = $this->_changeType['code']['suspend'];
                            $desc = 'suspended';
                          }
                          else if($unsuspend){
                            $cust_data['CUST_BIN_STATUS'] = 1;
                            $changeType = $this->_changeType['code']['unsuspend'];
                            $desc = 'unsuspended';
                          }
                          else if($delete){
                            $cust_data['CUST_BIN_STATUS'] = 3;
                            $changeType = $this->_changeType['code']['delete'];
                            $desc = 'deleted';
                          }
                          
                          // insert ke T_GLOBAL_CHANGES
                          $change_id = $this->suggestionWaitingApproval('BIN',$info, $changeType,null,'M_CUSTOMER_BIN','TEMP_CUSTOMER_BIN','Bin',$cust_bin,$cust_id);

                          $this->insertTempCustomer($change_id,$cust_data);
                          
                          //log CRUD
                          Application_Helper_General::writeLog('BNSP','customer BIN has been '.$desc.', Cust ID : '.$cust_id. ' ,BIN : '.$cust_bin.' ,Change id : '.$change_id);
                          
                         }
                         catch(Exception $e) 
                         { 

                         $this->_db->rollBack();
                         $error_remark = $this->getErrorRemark('82');
                         Application_Log_GeneralLog::technicalLog($e);
                        }
                      }
                      else $error_remark = 'Customer BIN is already suggested'; 
                    }
                    else  $cust_id = null; 
                   }// END IF CUST_ID
                }

                if (empty($error_remark)) {
                  $this->_db->commit();
                  $ns = new Zend_Session_Namespace('FVC');
                  $ns->backURL = '/binsetup';
          
                  $this->_redirect('/notification/submited/index');
                }
            }
            catch(Exception $e)
            {
                $this->_db->rollBack();
            }
        }
        else
        {
            $error          = true;
            $errors         = $zf_filter_input->getMessages();
            $biniderr   = (isset($errors['biniderr']))? $errors['biniderr'] : null;
        }
    }





    $fields = array(
      // 'custid'   => array('field'    => 'CUST_ID',
      //                       'label'    => $this->language->_('Company Code'),
      //                       'sortable' => true),
      'bin'     => array(
        'field'    => 'CUST_CITY',
        'label'    => $this->language->_('BIN'),
        'sortable' => true
      ),
      'type'   => array(
        'field'    => 'BIN_TYPE',
        'label'    => $this->language->_('Type'),
        'sortable' => true
      ),
      'company' => array(
        'field'    => 'COMPANY',
        'label'    => $this->language->_('Company'),
        'sortable' => true
      ),

      'master_account' => array(
        'field'    => 'MASTER_ACCOUNT',
        'label'    => $this->language->_('Master Account'),
        'sortable' => true
      ),
      'latestSuggestion'     => array(
        'field'    => 'BIN_SUGGESTED',
        'label'    => $this->language->_('Last Suggested'),
        'sortable' => true
      ),
      'latestApproval'    => array(
        'field'  => 'BIN_UPDATED',
        'label'    => $this->language->_('Last Approved'),
        'sortable' => true
      ),
      'status'   => array(
        'field'    => 'CUST_STATUS',
        'label'    => $this->language->_('Status'),
        'sortable' => true
      )
      
      // 'latestCreate'     => array(
      //   'field'    => 'BIN_CREATED',
      //   'label'    => $this->language->_('Created Time'),
      //   'sortable' => true
      // ),
    
      // 'latestSuggestor'   => array('field'  => 'CUST_SUGGESTEDBY',
      //                            'label'    => $this->language->_('Last Suggester'),
      //                            'sortable' => true),
      // 'latestSuggestor'   => array('field'  => 'CUST_SUGGESTEDBY',
      //                            'label'    => $this->language->_('Last Suggester'),
      //                            'sortable' => true),
      // 'latestApprover'  => array('field'   => 'CUST_UPDATEDBY',
      //                            'label'    => $this->language->_('Last Approver'),
      //                            'sortable' => true)
    );

    $filterlist = array('BIN_CREATED', 'BIN_CREATEDBY', 'BIN_UPDATEDBY', 'BIN_UPDATED', 'BIN_SUGGESTED', 'BIN_SUGGESTEDBY', 'COMP_ID', 'COMP_NAME', 'BIN', 'Status');

    $this->view->filterlist = $filterlist;

    //validasi page, jika input page bukan angka               
    $page = $this->_getParam('page');

    $page = (Zend_Validate::is($page, 'Digits')) ? $page : 1;

    //validasi sort, jika input sort bukan ASC atau DESC
    $sortBy  = $this->_getParam('sortby');
    $sortBy  = (Zend_Validate::is($sortBy, 'InArray', array(array_keys($fields)))) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
    $sortDir = $this->_getParam('sortdir');
    $sortDir = (Zend_Validate::is($sortDir, 'InArray', array('haystack' => array('asc', 'desc')))) ? $sortDir : 'asc';

    $filterArr = array(
      'filter'  => array('StripTags', 'StringTrim'),
      'COMP_ID'     => array('StripTags', 'StringTrim', 'StringToUpper'),
      'COMP_NAME'   => array('StripTags', 'StringTrim', 'StringToUpper'),
      'Status'  => array('StripTags', 'StringTrim'),
      'BIN'    => array('StripTags', 'StringTrim'),

      'BIN_SUGGESTED' => array('StripTags', 'StringTrim', 'StringToUpper'),
      'BIN_SUGGESTED_END'     => array('StripTags', 'StringTrim'),
      'BIN_SUGGESTEDBY'      => array('StripTags', 'StringTrim', 'StringToUpper'),
      'BIN_UPDATED'   => array('StripTags', 'StringTrim', 'StringToUpper'),
      'BIN_UPDATED_END'       => array('StripTags', 'StringTrim'),
      'BIN_UPDATED'       => array('StripTags', 'StringTrim', 'StringToUpper'),
      'BIN_CREATED'   => array('StripTags', 'StringTrim', 'StringToUpper'),
      'BIN_CREATED_END'       => array('StripTags', 'StringTrim'),
      'BIN_CREATEDBY'       => array('StripTags', 'StringTrim', 'StringToUpper'),
    );

    $validators = array(
      'filter' => array(),
      'COMP_ID'    => array(),
      'COMP_NAME'  => array(),
      'BIN'  => array(),
      'Status' => array(),
      'BIN_SUGGESTED' => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
      'BIN_SUGGESTED_END'     => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
      'BIN_SUGGESTEDBY'      => array(),
      'BIN_UPDATED'   => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
      'BIN_UPDATED_END'       => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
      'BIN_UPDATEDBY'       => array(),
      'BIN_CREATED'   => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
      'BIN_CREATED_END'       => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
      'BIN_CREATEDBY'       => array(),
    );

    $dataParam = array('BIN_SUGGESTED', 'BIN_UPDATED', 'BIN_CREATED', 'COMP_ID', 'COMP_NAME', 'BIN', 'Status');
    $dataParamValue = array();

    $clean2 = array_diff($this->_request->getParam('wherecol'), $dataParam);
    $dataParam = array_diff($this->_request->getParam('wherecol'), $clean2);
    // $clean2 = array_diff( $this->_request->getParam('wherecol'),$dataParam); 
    // $dataParam = array_diff( $this->_request->getParam('wherecol'),$clean2); 
    // print_r($dataParam);die;

    // print_r($output);die;
    // print_r($this->_request->getParam('wherecol'));
    foreach ($dataParam as $no => $dtParam) {

      if (!empty($this->_request->getParam('wherecol'))) {
        $dataval = $this->_request->getParam('whereval');
        // print_r($dataval);
        $order = 0;
        foreach ($this->_request->getParam('wherecol') as $key => $value) {
          if ($value == "BIN_CREATED" || $value == "BIN_SUGGESTED"  || $value == "BIN_UPDATED") {
            $order--;
          }
          if ($dtParam == $value) {
            $dataParamValue[$dtParam] = $dataval[$order];
          }
          $order++;
        }
      }
    }
    // print_r($dataParamValue);
    // die; 
    if (!empty($this->_request->getParam('lsdate'))) {
      $lsarr = $this->_request->getParam('lsdate');
      $dataParamValue['BIN_SUGGESTED'] = $lsarr[0];
      $dataParamValue['BIN_SUGGESTED_END'] = $lsarr[1];
    }
    if (!empty($this->_request->getParam('ladate'))) {
      $laarr = $this->_request->getParam('ladate');
      $dataParamValue['BIN_UPDATED'] = $laarr[0];
      $dataParamValue['BIN_UPDATED_END'] = $laarr[1];
    }
    if (!empty($this->_request->getParam('lcdate'))) {
      $laarr = $this->_request->getParam('lcdate');
      $dataParamValue['BIN_CREATED'] = $laarr[0];
      $dataParamValue['BIN_CREATED_END'] = $laarr[1];
    }

    $zf_filter = new Zend_Filter_Input($filterArr, $validator, $dataParamValue);
    $filter     = $this->_getParam('filter');

    $this->view->currentPage = $page;
    $this->view->sortBy = $sortBy;
    $this->view->sortDir = $sortDir;

    $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
    if (count($temp) > 1) {
      if ($temp[0] == 'F' || $temp[0] == 'S') {
        if ($temp[0] == 'F')
          $this->view->error = 1;
        else
          $this->view->success = 1;
        $msg = '';
        unset($temp[0]);
        foreach ($temp as $value) {
          if (!is_array($value))
            $value = array($value);
          $msg .= $this->view->formErrors($value);
        }
        $this->view->customer_msg = $msg;
      }
    }


    // proses pengambilan data filter,display all,sorting
    $select = array();

    //        $select = $this->_db->select()
    //                ->from('M_CUSTOMER_BIN',array('CUST_ID','CUST_BIN','CUST_BIN_STATUS','BIN_SUGGESTED','BIN_SUGGESTEDBY','BIN_UPDATED','BIN_UPDATEDBY'));
    //->where('CUST_STATUS != 3');
    //->where('CUST_STATUS <> 3');
    //->where("CONVERT(VARCHAR(20), CUST_SUGGESTED, 111) = '2012/04/12'")
    // ->where('UPPER(CUST_ID)='.$this->_db->quote(strtoupper($cid)));
    //  CONVERT(VARCHAR(8), now(), 112) AS [YYYYMMDD]
    // ->query()->fetchAll();


    $select = $this->_db->select()
      ->from(array('A' => 'M_CUSTOMER_BIN'), array('*'))
      //               ->joinLeft(array('B' => 'M_CUSTOMER'),'A.CUST_ID = B.CUST_ID AND A.CUST_ID = B.CUST_ID',array('*'));
      ->joinLeft(array('C' => 'M_CUSTOMER'), 'A.CUST_ID = C.CUST_ID', array(
        'CUST_NAME',
        'COMPANY' => new Zend_Db_Expr("CONCAT(C.CUST_NAME , ' (' , C.CUST_ID , ')  ' )")
      ));










    //saat pertama kali klik, lgsg filter by date today               
    if ($filter == '') {
      /*$today = date('d/m/Y');
            
            $this->view->latestSuggestionFrom = $today;
          $this->view->latestSuggestionTo   = $today;
            
          //konversi date agar dapat dibandingkan
          $latestSuggestionFrom   = (Zend_Date::isDate($today,$this->_dateDisplayFormat))?
                         new Zend_Date($today,$this->_dateDisplayFormat):
                         false;
      
          $latestSuggestionTo     = (Zend_Date::isDate($today,$this->_dateDisplayFormat))?
                         new Zend_Date($today,$this->_dateDisplayFormat):
                         false;
             
             if($latestSuggestionFrom)  $select->where("CONVERT(date,CUST_SUGGESTED) >= CONVERT(DATE,".$this->_db->quote($latestSuggestionFrom->toString($this->_dateDBFormat)).")");
         if($latestSuggestionTo)    $select->where("CONVERT(date,CUST_SUGGESTED) <= CONVERT(DATE,".$this->_db->quote($latestSuggestionTo->toString($this->_dateDBFormat)).")");*/
    }


    if ($filter == TRUE) {
      $cid       = html_entity_decode($zf_filter->getEscaped('COMP_ID'));
      $cname     = html_entity_decode($zf_filter->getEscaped('COMP_NAME'));
      $bin      = html_entity_decode($zf_filter->getEscaped('BIN'));
      $status    = html_entity_decode($zf_filter->getEscaped('Status'));

      $latestSuggestionFrom   = html_entity_decode($zf_filter->getEscaped('BIN_SUGGESTED'));
      $latestSuggestionTo     = html_entity_decode($zf_filter->getEscaped('BIN_SUGGESTED_END'));
      $latestSuggestor        = html_entity_decode($zf_filter->getEscaped('BIN_SUGGESTEDBY'));

      $latestApprovalFrom     = html_entity_decode($zf_filter->getEscaped('BIN_UPDATED'));
      $latestApprovalTo       = html_entity_decode($zf_filter->getEscaped('BIN_UPDATED_END'));
      $latestApprover         = html_entity_decode($zf_filter->getEscaped('BIN_UPDATEDBY'));

      $latestCreatedFrom     = html_entity_decode($zf_filter->getEscaped('BIN_CREATED'));
      $latestCreatedTo       = html_entity_decode($zf_filter->getEscaped('BIN_CREATED_END'));
      $latestCreated         = html_entity_decode($zf_filter->getEscaped('BIN_CREATED'));


      //konversi date agar dapat dibandingkan
      $latestSuggestionFrom   = (Zend_Date::isDate($latestSuggestionFrom, $this->_dateDisplayFormat)) ?
        new Zend_Date($latestSuggestionFrom, $this->_dateDisplayFormat) :
        false;

      $latestSuggestionTo     = (Zend_Date::isDate($latestSuggestionTo, $this->_dateDisplayFormat)) ?
        new Zend_Date($latestSuggestionTo, $this->_dateDisplayFormat) :
        false;

      $latestApprovalFrom     = (Zend_Date::isDate($latestApprovalFrom, $this->_dateDisplayFormat)) ?
        new Zend_Date($latestApprovalFrom, $this->_dateDisplayFormat) :
        false;

      $latestApprovalTo       = (Zend_Date::isDate($latestApprovalTo, $this->_dateDisplayFormat)) ?
        new Zend_Date($latestApprovalTo, $this->_dateDisplayFormat) :
        false;


      $latestCreatedFrom     = (Zend_Date::isDate($latestCreatedFrom, $this->_dateDisplayFormat)) ?
        new Zend_Date($latestCreatedFrom, $this->_dateDisplayFormat) :
        false;

      $latestCreatedTo       = (Zend_Date::isDate($latestCreatedTo, $this->_dateDisplayFormat)) ?
        new Zend_Date($latestCreatedTo, $this->_dateDisplayFormat) :
        false;

      //if($cid)            $select->where('UPPER(CUST_ID) LIKE '.$this->_db->quote('%'.strtoupper($cid).'%'));
      if ($cid)              $select->where('UPPER(A.CUST_ID)=' . $this->_db->quote(strtoupper($cid)));
      if ($bin)              $select->where('UPPER(CUST_BIN) LIKE ' . $this->_db->quote('%' . strtoupper($bin) . '%'));
      if ($cname)            $select->where('UPPER(CUST_NAME) LIKE ' . $this->_db->quote('%' . strtoupper($cname) . '%'));
      if ($status)           $select->where('CUST_BIN_STATUS=?', $status);
      if ($latestSuggestor)  $select->where('UPPER(BIN_SUGGESTEDBY) LIKE ' . $this->_db->quote('%' . strtoupper($latestSuggestor) . '%'));
      if ($latestApprover)   $select->where('UPPER(BIN_UPDATEDBY) LIKE ' . $this->_db->quote('%' . strtoupper($latestApprover) . '%'));

      if ($latestSuggestionFrom)  $select->where("DATE(BIN_SUGGESTED) >= DATE(" . $this->_db->quote($latestSuggestionFrom->toString($this->_dateDBFormat)) . ")");
      if ($latestSuggestionTo)    $select->where("DATE(BIN_SUGGESTED) <= DATE(" . $this->_db->quote($latestSuggestionTo->toString($this->_dateDBFormat)) . ")");
      if ($latestApprovalFrom)    $select->where("DATE(BIN_UPDATED) >= DATE(" . $this->_db->quote($latestApprovalFrom->toString($this->_dateDBFormat)) . ")");
      if ($latestApprovalTo)      $select->where("DATE(BIN_UPDATED) <= DATE(" . $this->_db->quote($latestApprovalTo->toString($this->_dateDBFormat)) . ")");
      if ($latestCreatedFrom)    $select->where("DATE(BIN_CREATED) >= DATE(" . $this->_db->quote($latestCreatedFrom->toString($this->_dateDBFormat)) . ")");
      if ($latestCreatedTo)      $select->where("DATE(BIN_CREATED) <= DATE(" . $this->_db->quote($latestCreatedTo->toString($this->_dateDBFormat)) . ")");


      $this->view->cid     = $cid;
      $this->view->cname   = $cname;
      $this->view->bin    = $bin;
      $this->view->status  = $status;
      $this->view->latestSuggestor  = $latestSuggestor;
      $this->view->latestApprover   = $latestApprover;

      if ($latestSuggestionFrom)  $this->view->latestSuggestionFrom = $latestSuggestionFrom->toString($this->_dateDisplayFormat);
      if ($latestSuggestionTo)    $this->view->latestSuggestionTo   = $latestSuggestionTo->toString($this->_dateDisplayFormat);
      if ($latestApprovalFrom)    $this->view->latestApprovalFrom   = $latestApprovalFrom->toString($this->_dateDisplayFormat);
      if ($latestApprovalTo)      $this->view->latestApprovalTo     = $latestApprovalTo->toString($this->_dateDisplayFormat);
    }
    //utk sorting 
    $select->order($sortBy . ' ' . $sortDir);

    // END proses pengambilan data filter,display all,sorting

    $data = $this->_db->fetchAll($select);

    foreach ($data as $key => $value) {
      $tempCustomerId = $this->getTempCustomerId($value['CUST_ID'], $value['CUST_BIN'], TRUE);
      //if ada di temp
      if ($tempCustomerId) {
        $data[$key]['TEMP_STATUS'] = 1;
      }
      else{
        $data[$key]['TEMP_STATUS'] = 0;
      }
    }

    //   echo "<pre>";
    // print_r($data);die();

    $this->paging($data);
    $this->view->fields = $fields;
    $this->view->filter = $filter;
    $this->view->statusCode = array_flip($this->_masterglobalstatus['code']);
    $this->view->statusDesc = $this->_masterglobalstatus['desc'];
    $this->view->modulename = $this->_request->getModuleName();


    $arr = $this->_db->fetchAll($select);

    if($this->_request->getParam('print') == 1){

      foreach($arr as $key=>$row)
        {
          
          $statusCode = array_flip($this->_masterglobalstatus['code']);
          $statusDesc = $this->_masterglobalstatus['desc'];

          $arr[$key]['BIN_UPDATED']     = Application_Helper_General::convertDate($row['BIN_UPDATED'],"dd MMM yyyy HH:mm:ss ") . ' (' . $row['BIN_UPDATEDBY'] . ')';
          $arr[$key]['BIN_SUGGESTED']   = Application_Helper_General::convertDate($row['BIN_SUGGESTED'],"dd MMM yyyy HH:mm:ss ") . ' (' . $row['BIN_SUGGESTEDBY'] . ')';
          $arr[$key]['MASTER_ACCOUNT']  = $row['ACCT_NO'].' ('.$row['CCY'].') -'.$row['ACCT_NAME'];
          $arr[$key]['CUST_CITY']       = $row['CUST_BIN'];
          $binType                      = array('1' => 'Cash In', '2' => 'Cash Out');
          $arr[$key]['BIN_TYPE']        = (empty($row['BIN_TYPE']) ? '-' : $binType[$row['BIN_TYPE']] );
          $arr[$key]['CUST_STATUS']     = $statusDesc[$statusCode[$row['CUST_BIN_STATUS']]];
          
        }

      $this->_forward('print', 'index', 'widget', array('data_content' => $arr, 'data_caption' => 'Bin Setup', 'data_header' => $fields));
    }

    //insert log
    try {
      $this->_db->beginTransaction();

      Application_Helper_General::writeLog('BNLS', 'View Customer BIN Setup Customer BIN List');

      $this->_db->commit();
    } catch (Exception $e) {
      $this->_db->rollBack();
    }

    if (!empty($dataParamValue)) {
      $this->view->lsdateStart = $dataParamValue['BIN_SUGGESTED'];
      $this->view->lsdateEnd = $dataParamValue['BIN_SUGGESTED_END'];
      $this->view->ladateStart = $dataParamValue['BIN_UPDATED'];
      $this->view->ladateEnd = $dataParamValue['BIN_UPDATED_END'];
      $this->view->lcdateStart = $dataParamValue['BIN_CREATED'];
      $this->view->lcdateEnd = $dataParamValue['BIN_CREATED_END'];

      unset($dataParamValue['BIN_SUGGESTED_END']);
      unset($dataParamValue['BIN_UPDATED_END']);
      unset($dataParamValue['BIN_CREATED_END']);
      foreach ($dataParamValue as $key => $value) {
        $wherecol[] = $key;
        $whereval[] = $value;
      }

      // print_r($whereval);die;
    } else {
      $wherecol = array();
      $whereval = array();
    }

    $this->view->wherecol     = $wherecol;
    $this->view->whereval     = $whereval;
  }
}
