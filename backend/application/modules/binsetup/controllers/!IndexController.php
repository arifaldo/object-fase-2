<?php

require_once 'Zend/Controller/Action.php';

class binsetup_IndexController extends binsetup_Model_Binsetup
{
	
	public function initController()
	{       
		//$statusArr = Application_Helper_Array::globalvarArray($this->_masteruserStatus);
		$statusArr = array(''=>'-- '.$this->language->_('Any Value') .' --','1'=>$this->language->_('Approved'), '2'=>$this->language->_('Suspended'), '3'=>$this->language->_('Deleted'));
	    $this->view->statusArr = $statusArr;
		
		$custArr  = Application_Helper_Array::listArray($this->getAllCustomer(),'CUST_ID','CUST_ID');
		$custArr  = array_merge(array(''=>'-- '.$this->language->_('Any Value') .' --'),$custArr);
		$this->view->custArr = $custArr;
		
		// Zend_Debug::dump($custArr);
		// die;
	
		$this->view->signArr = array('EQ'=>'=', 'NE'=>'!=', 'LT'=>'<', 'GT'=>'>', 'LE'=>'<=', 'GE'=>'>=');
		//format display date
		$this->view->dateDisplayFormat = $this->_dateDisplayFormat;
    }
 
  public function indexAction() 
  {  
	$this->_helper->layout()->setLayout('newlayout');

     $fields = array(
                  // 'custid'   => array('field'    => 'CUST_ID',
                  //                       'label'    => $this->language->_('Company Code'),
                  //                       'sortable' => true),
    
                    'company' => array('field'    => 'CUST_NAME',
                                        'label'    => $this->language->_('Company'),
                                        'sortable' => true),
    
                    'bin'     => array('field'    => 'CUST_CITY',
                                        'label'    => $this->language->_('BIN'),
                                        'sortable' => true),
    
                    'status'   => array('field'    => 'CUST_STATUS',
                                        'label'    => $this->language->_('Status'),
                                        'sortable' => true),
    
                    'latestSuggestion'     => array('field'    => 'CUST_SUGGESTED',
                                               'label'    => $this->language->_('Last Suggestion'),
                                               'sortable' => true),
    
                    'latestSuggestor'   => array('field'  => 'CUST_SUGGESTEDBY',
                                               'label'    => $this->language->_('Last Suggester'),
                                               'sortable' => true),
    
                    'latestApproval'    => array('field'  => 'CUST_UPDATED',
                                               'label'    => $this->language->_('Last Approval'),
                                               'sortable' => true),
    
                    'latestApprover'  => array('field'   => 'CUST_UPDATEDBY',
                                               'label'    => $this->language->_('Last Approver'),
                                               'sortable' => true)
                   );

    $filterlist = array('PS_LATEST_SUGGESTION','PS_LATEST_APPROVAL','LATEST_SUGGESTER','LATEST_APPROVER','COMP_ID','COMP_NAME','BIN','Status');
    
    $this->view->filterlist = $filterlist;
    
    //validasi page, jika input page bukan angka               
    $page = $this->_getParam('page');
    
    $page = (Zend_Validate::is($page,'Digits'))? $page : 1;
    
    //validasi sort, jika input sort bukan ASC atau DESC
    $sortBy  = $this->_getParam('sortby');
    $sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
    $sortDir = $this->_getParam('sortdir');
	$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';

    $filterArr = array('filter'  => array('StripTags','StringTrim'),
                       'COMP_ID'     => array('StripTags','StringTrim','StringToUpper'),
                       'COMP_NAME'   => array('StripTags','StringTrim','StringToUpper'),
                       'Status'  => array('StripTags','StringTrim'),
                       'BIN'    => array('StripTags','StringTrim'),
    
                     'PS_LATEST_SUGGESTION' => array('StripTags','StringTrim','StringToUpper'),
                     'PS_LATEST_SUGGESTION_END'     => array('StripTags','StringTrim'),
                     'LATEST_SUGGESTER'      => array('StripTags','StringTrim','StringToUpper'),
                     'PS_LATEST_APPROVAL'   => array('StripTags','StringTrim','StringToUpper'),
                     'PS_LATEST_APPROVAL_END'       => array('StripTags','StringTrim'),
                     'LATEST_APPROVER'       => array('StripTags','StringTrim','StringToUpper'),
                      );

     $validators = array(
                    'filter' => array(),
                      'COMP_ID'    => array(),
                       'COMP_NAME'  => array(),
                       'BIN'  => array(),
                       'Status' => array(),     
                       'PS_LATEST_SUGGESTION' => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
                       'PS_LATEST_SUGGESTION_END'     => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
                       'LATEST_SUGGESTER'      => array(),
                       'PS_LATEST_APPROVAL'   => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
                       'PS_LATEST_APPROVAL_END'       => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
                       'LATEST_APPROVER'       => array(),
                    );

      $dataParam = array('LATEST_SUGGESTER','LATEST_APPROVER','COMP_ID','COMP_NAME','BIN','Status');
      $dataParamValue = array();
        
      $clean2 = array_diff( $this->_request->getParam('wherecol'),$dataParam); 
      $dataParam = array_diff( $this->_request->getParam('wherecol'),$clean2); 
    // print_r($dataParam);die;

      // print_r($output);die;
      // print_r($this->_request->getParam('wherecol'));
      foreach ($dataParam as $no => $dtParam)
      {
      
        if(!empty($this->_request->getParam('wherecol'))){
          $dataval = $this->_request->getParam('whereval');
          // print_r($dataval);
          $order = 0;
            foreach ($this->_request->getParam('wherecol') as $key => $value) {
              if($value == "PS_LATEST_APPROVAL" || $value == "PS_LATEST_SUGGESTION"){
                  $order--;
                }
              if($dtParam==$value){
                $dataParamValue[$dtParam] = $dataval[$order];
              }
              $order++;
            }
          
        }
      }
      // print_r($dataParamValue);
      // die; 
        if(!empty($this->_request->getParam('lsdate'))){
          $lsarr = $this->_request->getParam('lsdate');
            $dataParamValue['PS_LATEST_SUGGESTION'] = $lsarr[0];
            $dataParamValue['PS_LATEST_SUGGESTION_END'] = $lsarr[1];
        }
        if(!empty($this->_request->getParam('ladate'))){
          $laarr = $this->_request->getParam('ladate');
            $dataParamValue['PS_LATEST_APPROVAL'] = $laarr[0];
            $dataParamValue['PS_LATEST_APPROVAL_END'] = $laarr[1];
        }
                      
    $zf_filter = new Zend_Filter_Input($filterArr,$validator,$dataParamValue);
    $filter     = $this->_getParam('filter');
    
    $this->view->currentPage = $page;
    $this->view->sortBy = $sortBy;
    $this->view->sortDir = $sortDir;
    
  	$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
	if(count($temp)>1){
      	if($temp[0]=='F' || $temp[0]=='S'){
      		if($temp[0]=='F')
        		$this->view->error = 1;
        	else
        		$this->view->success = 1;
      		$msg = ''; unset($temp[0]);
      		foreach($temp as $value)
      		{
      			if(!is_array($value))
      				$value = array($value);
      			$msg .= $this->view->formErrors($value);
      		}
        	$this->view->customer_msg = $msg;
     	}	
    }
    
    
     // proses pengambilan data filter,display all,sorting
        $select = array(); 
    
//        $select = $this->_db->select()
//						    ->from('M_CUSTOMER_BIN',array('CUST_ID','CUST_BIN','CUST_BIN_STATUS','BIN_SUGGESTED','BIN_SUGGESTEDBY','BIN_UPDATED','BIN_UPDATEDBY'));
						       //->where('CUST_STATUS != 3');
						       //->where('CUST_STATUS <> 3');
						     //->where("CONVERT(VARCHAR(20), CUST_SUGGESTED, 111) = '2012/04/12'")
						       // ->where('UPPER(CUST_ID)='.$this->_db->quote(strtoupper($cid)));
						       //  CONVERT(VARCHAR(8), now(), 112) AS [YYYYMMDD]
						     // ->query()->fetchAll();


	  	 $select = $this->_db->select()
  	                         ->from(array('A' => 'M_CUSTOMER_BIN'),array('*'))
//							 ->joinLeft(array('B' => 'M_CUSTOMER'),'A.CUST_ID = B.CUST_ID AND A.CUST_ID = B.CUST_ID',array('*'));
							 ->joinLeft(array('C' => 'M_CUSTOMER'),'A.CUST_ID = C.CUST_ID',array('CUST_NAME',
                                                  'COMPANY' => new Zend_Db_Expr("CONCAT(C.CUST_NAME , ' (' , C.CUST_ID , ')  ' )")
                                                  ));
	  			                     
	  			                     
	  			                     
	  			                     
	  			                     
	  			                     
	  			                     
	  			                     
		  				     
						    
		//saat pertama kali klik, lgsg filter by date today				        
		if($filter == '')
		{
		        /*$today = date('d/m/Y');
		        
		        $this->view->latestSuggestionFrom = $today;
			    $this->view->latestSuggestionTo   = $today;
		        
			    //konversi date agar dapat dibandingkan
			    $latestSuggestionFrom   = (Zend_Date::isDate($today,$this->_dateDisplayFormat))?
								 	       new Zend_Date($today,$this->_dateDisplayFormat):
								 	       false;
			
			    $latestSuggestionTo     = (Zend_Date::isDate($today,$this->_dateDisplayFormat))?
								 	       new Zend_Date($today,$this->_dateDisplayFormat):
								 	       false;
	           
	           if($latestSuggestionFrom)  $select->where("CONVERT(date,CUST_SUGGESTED) >= CONVERT(DATE,".$this->_db->quote($latestSuggestionFrom->toString($this->_dateDBFormat)).")");
			   if($latestSuggestionTo)    $select->where("CONVERT(date,CUST_SUGGESTED) <= CONVERT(DATE,".$this->_db->quote($latestSuggestionTo->toString($this->_dateDBFormat)).")");*/
		}			    
    
		
        if($filter == TRUE)
        {
			$cid       = html_entity_decode($zf_filter->getEscaped('COMP_ID'));
			$cname     = html_entity_decode($zf_filter->getEscaped('COMP_NAME'));
			$bin      = html_entity_decode($zf_filter->getEscaped('BIN'));
			$status    = html_entity_decode($zf_filter->getEscaped('Status'));
			$latestSuggestionFrom   = html_entity_decode($zf_filter->getEscaped('PS_LATEST_SUGGESTION'));
		    $latestSuggestionTo     = html_entity_decode($zf_filter->getEscaped('PS_LATEST_SUGGESTION_END'));
		    $latestSuggestor        = html_entity_decode($zf_filter->getEscaped('LATEST_SUGGESTER'));
		    $latestApprovalFrom     = html_entity_decode($zf_filter->getEscaped('PS_LATEST_APPROVAL'));
		    $latestApprovalTo       = html_entity_decode($zf_filter->getEscaped('PS_LATEST_APPROVAL_END'));
		    $latestApprover         = html_entity_decode($zf_filter->getEscaped('LATEST_APPROVER'));
			
			//konversi date agar dapat dibandingkan
			$latestSuggestionFrom   = (Zend_Date::isDate($latestSuggestionFrom,$this->_dateDisplayFormat))?
								 	   new Zend_Date($latestSuggestionFrom,$this->_dateDisplayFormat):
								 	   false;
			
			$latestSuggestionTo     = (Zend_Date::isDate($latestSuggestionTo,$this->_dateDisplayFormat))?
								 	   new Zend_Date($latestSuggestionTo,$this->_dateDisplayFormat):
								 	   false;
								 	   					 	   
			$latestApprovalFrom     = (Zend_Date::isDate($latestApprovalFrom,$this->_dateDisplayFormat))?
								 	   new Zend_Date($latestApprovalFrom,$this->_dateDisplayFormat):
								 	   false;

			$latestApprovalTo       = (Zend_Date::isDate($latestApprovalTo,$this->_dateDisplayFormat))?
								 	   new Zend_Date($latestApprovalTo,$this->_dateDisplayFormat):
								 	   false;					 	   
			
	        //if($cid)            $select->where('UPPER(CUST_ID) LIKE '.$this->_db->quote('%'.strtoupper($cid).'%'));
			if($cid)              $select->where('UPPER(A.CUST_ID)='.$this->_db->quote(strtoupper($cid)));
			if($bin)              $select->where('UPPER(CUST_BIN) LIKE '.$this->_db->quote('%'.strtoupper($bin).'%'));
			if($cname)            $select->where('UPPER(CUST_NAME) LIKE '.$this->_db->quote('%'.strtoupper($cname).'%'));
			if($status)           $select->where('CUST_BIN_STATUS=?',$status);
			if($latestSuggestor)  $select->where('UPPER(BIN_SUGGESTEDBY) LIKE '.$this->_db->quote('%'.strtoupper($latestSuggestor).'%'));
			if($latestApprover)   $select->where('UPPER(BIN_UPDATEDBY) LIKE '.$this->_db->quote('%'.strtoupper($latestApprover).'%'));

			if($latestSuggestionFrom)  $select->where("DATE(BIN_SUGGESTED) >= DATE(".$this->_db->quote($latestSuggestionFrom->toString($this->_dateDBFormat)).")");
			if($latestSuggestionTo)    $select->where("DATE(BIN_SUGGESTED) <= DATE(".$this->_db->quote($latestSuggestionTo->toString($this->_dateDBFormat)).")");
			if($latestApprovalFrom)    $select->where("DATE(BIN_UPDATED) >= DATE(".$this->_db->quote($latestApprovalFrom->toString($this->_dateDBFormat)).")");
			if($latestApprovalTo)      $select->where("DATE(BIN_UPDATED) <= DATE(".$this->_db->quote($latestApprovalTo->toString($this->_dateDBFormat)).")");
			
			$this->view->cid     = $cid;
			$this->view->cname   = $cname;
			$this->view->bin    = $bin;
			$this->view->status  = $status;
		    $this->view->latestSuggestor  = $latestSuggestor;
			$this->view->latestApprover   = $latestApprover;
			
			if($latestSuggestionFrom)  $this->view->latestSuggestionFrom = $latestSuggestionFrom->toString($this->_dateDisplayFormat);
			if($latestSuggestionTo)    $this->view->latestSuggestionTo   = $latestSuggestionTo->toString($this->_dateDisplayFormat);
			if($latestApprovalFrom)    $this->view->latestApprovalFrom   = $latestApprovalFrom->toString($this->_dateDisplayFormat);
			if($latestApprovalTo)      $this->view->latestApprovalTo     = $latestApprovalTo->toString($this->_dateDisplayFormat);
        }
        //utk sorting 
        $select->order($sortBy.' '.$sortDir);
       
    // END proses pengambilan data filter,display all,sorting

    
    $this->paging($select);
    $this->view->fields = $fields;
    $this->view->filter = $filter;
    $this->view->statusCode = array_flip($this->_masterglobalstatus['code']);
    $this->view->statusDesc = $this->_masterglobalstatus['desc'];
    $this->view->modulename = $this->_request->getModuleName();
    
    
     //insert log
	 try
	 {
	   $this->_db->beginTransaction();
	   
	   Application_Helper_General::writeLog('BNLS','View Customer BIN Setup Customer BIN List');
	   
	   $this->_db->commit();
	 }
     catch(Exception $e)
     {
 	    $this->_db->rollBack();
	 }

	 if(!empty($dataParamValue)){
          $this->view->lsdateStart = $dataParamValue['PS_LATEST_SUGGESTION'];
          $this->view->lsdateEnd = $dataParamValue['PS_LATEST_SUGGESTION_END'];
          $this->view->ladateStart = $dataParamValue['PS_LATEST_APPROVAL'];
          $this->view->ladateEnd = $dataParamValue['PS_LATEST_APPROVAL_END'];

            unset($dataParamValue['PS_LATEST_SUGGESTION_END']);
          unset($dataParamValue['PS_LATEST_APPROVAL_END']);
      foreach ($dataParamValue as $key => $value) {
        $wherecol[] = $key;
        $whereval[] = $value;
      }
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     // print_r($whereval);die;
      }
	
  }
  
  
}


