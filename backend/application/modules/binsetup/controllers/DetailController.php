<?php

require_once 'Zend/Controller/Action.php';
ini_set("display_errors","Off");
class binsetup_DetailController extends binsetup_Model_Binsetup
{

    public function indexAction() 
    {
		$this->_helper->layout()->setLayout('newlayout');
       $cust_id = strtoupper($this->_getParam('cust_id'));
       $cust_bin = strtoupper($this->_getParam('cust_bin'));
       $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;
    
       //pengaturan url untuk button back
	   $this->setbackURL('/'.$this->_request->getModuleName().'/view/index/cust_id/'.$cust_id); 
       
       if($cust_id && $cust_bin)
       {
          $resultdata = $this->getCustomer($cust_id,$cust_bin);
         
          if($resultdata['CUST_ID'])
          {
      	        $this->view->cust_id      = strtoupper($resultdata['CUST_ID']);
       			$this->view->cust_name    = $resultdata['CUST_NAME'];
      		    $this->view->cust_bin_status   = strtoupper($resultdata['CUST_BIN_STATUS']);
      		    $this->view->cust_bin   = strtoupper($resultdata['CUST_BIN']);
      		    
      		    
      		    $this->view->cust_created     = $resultdata['BIN_CREATED'];
      		    $this->view->cust_createdby   = $resultdata['BIN_CREATEDBY'];
      		    $this->view->cust_suggested   = $resultdata['BIN_SUGGESTED'];
      		    $this->view->cust_suggestedby = $resultdata['BIN_SUGGESTEDBY'];
      		    $this->view->cust_updated     = $resultdata['BIN_UPDATED'];
      		    $this->view->cust_updatedby   = $resultdata['BIN_UPDATEDBY'];
			
//      		 if(!$result){   
//			 	$result = $this->getTempCustomerId($cust_id,$cust_bin,false);
//      		 }
//      		 else{
//      		 	$result = $this->getTempCustomerId($cust_id,$cust_bin,true);
//      		 }

      		 $result = $this->getTempCustomerId($cust_id,$cust_bin,TRUE);
      		 
      		 if($result){
      		 	$resultAll = $this->getTempCustomerId($cust_id,$cust_bin,TRUE);
      		 }
      		 else{
      		 	$resultAll = $this->getTempCustomerId($cust_id,$cust_bin,false);
      		 }
      		 
      	     if($resultAll)  $temp = 0; 
      	     else  $temp = 1; 
      	     
      	      
             $this->view->cust_temp = $temp; 
           }
           else $cust_id = null; 
       }// End if cust_id == true
      
        if(!$cust_id)
        {
           $error_remark = 'Invalid Cust ID';
		    //insert log
			try
			{
			   $this->_db->beginTransaction();
			   
			   Application_Helper_General::writeLog('BNUD','View Detail Customer BIN [Invalid Cust ID]');
			   
			   $this->_db->commit();
			}
			catch(Exception $e) 
			{
			   $this->_db->rollBack();
			}
           $this->_helper->getHelper('FlashMessenger')->addMessage('F');
           $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
           $this->_redirect($this->_helper->url->url(array('module'=>$this->_request->getModuleName(),'controller'=>'index','action'=>'index')));	
         }
    
        $this->view->cust_id = $cust_id;
        $this->view->status_type = $this->_masterglobalstatus;
        $this->view->modulename = $this->_request->getModuleName();
    
        
        
        
        //insert log
        try
        {
	       $this->_db->beginTransaction();
	       
	       Application_Helper_General::writeLog('BNUD','View Customer BIN Detail ['.$cust_id.']');
	       
           $this->_db->commit();
	    }
	    catch(Exception $e) 
	    {
	       $this->_db->rollBack();
	    }
	   
   }	
  
  
}




