<?php

require_once 'Zend/Controller/Action.php';

class binsetup_NewbincekController extends binsetup_Model_Binsetup
{

  public function initController()
  {
    //$statusArr = Application_Helper_Array::globalvarArray($this->_masteruserStatus);
    $statusArr = array('' => '-- ' . $this->language->_('Any Value') . ' --', '1' => $this->language->_('Approved'), '2' => $this->language->_('Suspended'), '3' => $this->language->_('Deleted'));
    $this->view->statusArr = $statusArr;

    $custArr  = Application_Helper_Array::listArray($this->getAllCustomer(), 'CUST_ID', 'CUST_ID');
    $custArr  = array_merge(array('' => '-- ' . $this->language->_('Any Value') . ' --'), $custArr);
    $this->view->custArr = $custArr;

    // Zend_Debug::dump($custArr);
    // die;

    $this->view->signArr = array('EQ' => '=', 'NE' => '!=', 'LT' => '<', 'GT' => '>', 'LE' => '<=', 'GE' => '>=');
    //format display date
    $this->view->dateDisplayFormat = $this->_dateDisplayFormat;
  }

  public function indexAction()
  {
    $this->_helper->layout()->setLayout('newlayout');


    $select  = $this->_db->select()
      ->from(array('B' => 'M_CUSTOMER'), array(
        'CUST_ID',
        'CUST_NAME' => new Zend_Db_Expr("CONCAT(CUST_NAME , ' (' , CUST_ID , ')  ' )")
      ));
    $select->where("B.CUST_STATUS not in (3, 2)");
    // echo $select;
    $datacust =  $select->query()->fetchAll();

    $custArr  = Application_Helper_Array::listArray($datacust, 'CUST_ID', 'CUST_NAME');
    $custArr  = array_merge(array('' => '-- ' . $this->language->_('Any Value') . ' --'), $custArr);
    $this->view->custArr = $custArr;
    // Zend_Debug::dump($custArr);
    // die;

    $fields = array(
      // 'custid'   => array('field'    => 'CUST_ID',
      //                       'label'    => $this->language->_('Company Code'),
      //                       'sortable' => true),
      'bin'     => array(
        'field'    => 'CUST_CITY',
        'label'    => $this->language->_('BIN'),
        'sortable' => true
      ),
      'master_account'     => array(
        'field'    => 'ACCT_NO',
        'label'    => $this->language->_('Master Account'),
        'sortable' => true
      ),
      // 'bin_type'     => array(
      //   'field'    => 'BIN_TYPE',
      //   'label'    => $this->language->_('Type'),
      //   'sortable' => true
      // ),
      // 'latestCreate'     => array(
      //   'field'    => 'BIN_CREATED',
      //   'label'    => $this->language->_('Created Time'),
      //   'sortable' => true
      // ),


      // 'acctsrc'     => array(
      //   'field'    => 'ACCT_NO',
      //   'label'    => $this->language->_('Account Number'),
      //   'sortable' => true
      // ),

      // 'ccy' => array(
      //   'field'    => 'CCY',
      //   'label'    => $this->language->_('CCY'),
      //   'sortable' => true
      // ),

      // 'acctsrc_name'     => array(
      //   'field'    => 'ACCT_NAME',
      //   'label'    => $this->language->_('Account Name'),
      //   'sortable' => true
      // ),

      // 'status'   => array(
      //   'field'    => 'CUST_STATUS',
      //   'label'    => $this->language->_('Status'),
      //   'sortable' => true
      // ),
      // 'parameter'   => array(
      //   'field'    => 'CUST_STATUS',
      //   'label'    => $this->language->_('Parameter'),
      //   'sortable' => true
      // ),

    );

    $filterlist = array('BIN_CREATED', 'BIN_CREATEDBY', 'BIN_UPDATEDBY', 'BIN_UPDATED', 'BIN_SUGGESTED', 'BIN_SUGGESTEDBY', 'COMP_ID', 'COMP_NAME', 'BIN', 'Status');

    $this->view->filterlist = $filterlist;

    //validasi page, jika input page bukan angka               
    $page = $this->_getParam('page');

    $page = (Zend_Validate::is($page, 'Digits')) ? $page : 1;

    //validasi sort, jika input sort bukan ASC atau DESC
    $sortBy  = $this->_getParam('sortby');
    $sortBy  = (Zend_Validate::is($sortBy, 'InArray', array(array_keys($fields)))) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
    $sortDir = $this->_getParam('sortdir');
    $sortDir = (Zend_Validate::is($sortDir, 'InArray', array('haystack' => array('asc', 'desc')))) ? $sortDir : 'asc';


    $filter     = $this->_getParam('filter');

    $this->view->currentPage = $page;
    $this->view->sortBy = $sortBy;
    $this->view->sortDir = $sortDir;

    $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
    if (count($temp) > 1) {
      if ($temp[0] == 'F' || $temp[0] == 'S') {
        if ($temp[0] == 'F')
          $this->view->error = 1;
        else
          $this->view->success = 1;
        $msg = '';
        unset($temp[0]);
        foreach ($temp as $value) {
          if (!is_array($value))
            $value = array($value);
          $msg .= $this->view->formErrors($value);
        }
        $this->view->customer_msg = $msg;
      }
    }


    // proses pengambilan data filter,display all,sorting
    $select = array();





    if ($filter == TRUE) {
      $select = $this->_db->select()
        ->from(array('A' => 'M_CUSTOMER_BIN'), array('*'))
        //               ->joinLeft(array('B' => 'M_CUSTOMER'),'A.CUST_ID = B.CUST_ID AND A.CUST_ID = B.CUST_ID',array('*'));
        ->joinLeft(array('C' => 'M_CUSTOMER'), 'A.CUST_ID = C.CUST_ID', array(
          'CUST_NAME',
          'COMPANY' => new Zend_Db_Expr("CONCAT(C.CUST_NAME , ' (' , C.CUST_ID , ')  ' )")
        ));

      $params = $this->_request->getParams();
      // var_dump($params);
      // die('here');
      $cid       = html_entity_decode($params['cid']);


      //if($cid)            $select->where('UPPER(CUST_ID) LIKE '.$this->_db->quote('%'.strtoupper($cid).'%'));
      if ($cid)              $select->where('UPPER(A.CUST_ID)=' . $this->_db->quote(strtoupper($cid)));



      $this->view->cid     = $cid;
      $select->order($sortBy . ' ' . $sortDir);
    }
    //utk sorting 


    // END proses pengambilan data filter,display all,sorting

    $status = array(
      '1' => 'Active',
      '2'  => 'Suspend',
      '3' => 'Delete'
    );

    $binType = array(
      '1' => 'eCollection',
      '2'  => 'VA Debit'
    );

    $this->paging($select);
    $this->view->fields = $fields;
    $this->view->filter = $filter;
    $this->view->statusCode = array_flip($this->_masterglobalstatus['code']);
    $this->view->statusDesc = $status;
    $this->view->binType = $binType;
    $this->view->modulename = $this->_request->getModuleName();


    //insert log
    try {
      $this->_db->beginTransaction();

      Application_Helper_General::writeLog('BNLS', 'View Customer BIN Setup Customer BIN List');

      $this->_db->commit();
    } catch (Exception $e) {
      $this->_db->rollBack();
    }
  }
}
