<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class binsetup_DeleteController extends binsetup_Model_Binsetup 
{
     public function indexAction() 
     {  
  	    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
        $password = $sessionNamespace->token; 
        $this->view->token = $sessionNamespace->token;


        $AESMYSQL = new Crypt_AESMYSQL();
        $cust_id = $AESMYSQL->decrypt($this->_getParam('cust_id'), $password);
        $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;

        $cust_bin = strtoupper($AESMYSQL->decrypt($this->_getParam('cust_bin'), $password));
        
         $error_remark = null;
    
         if($cust_id)
         {
             $tempCustomerId = $this->getTempCustomerId($cust_id,$cust_bin, TRUE);
		
               if(!$tempCustomerId)
               {
                  $cust_data = $this->getCustomerActIn($cust_id,$cust_bin);
               
//                  $info = 'Customer ID = '.$cust_data['CUST_ID'].', Customer Name = '.$cust_data['CUST_NAME'];
                  $info = 'BIN';
                  
                 /* Zend_Debug::dump($cust_data);
                  die;*/
                  
                  try 
                  {
			         $this->_db->beginTransaction();
			         $cust_data['CUST_BIN_STATUS'] = 3;
			         // $cust_data['CUST_BIN_TMP']  = $this->_getParam('cust_bin');
				     
			         $cust_data['BIN_SUGGESTED']   = new Zend_Db_Expr('now()');
		             $cust_data['BIN_SUGGESTEDBY'] = $this->_userIdLogin;
			      
			         //insert ke T_GLOBAL_CHANGES
			         $change_id = $this->suggestionWaitingApproval('BIN',$info,$this->_changeType['code']['delete'],null,'M_CUSTOMER_BIN','TEMP_CUSTOMER_BIN','Bin',$cust_bin,$cust_id);
			         $this->insertTempCustomer($change_id,$cust_data);
			         
			         //log CRUD
			         Application_Helper_General::writeLog('BNUD','Customer BIN has been updated (delete), Cust ID : '.$cust_id. ' ,BIN : '.$cust_bin.' ,Change id : '.$change_id);
			          
			         $this->_db->commit();
			         
			         $ns = new Zend_Session_Namespace('FVC');
					     $ns->backURL = '/binsetup';
			         $this->_redirect('/notification/submited/index');
			         //$this->_redirect('/notification/success/index');
		          }
		          catch(Exception $e) 
		          {
              // echo $e;die();
				    $this->_db->rollBack();
					// $error_remark = $this->getErrorRemark('82');
					// Application_Log_GeneralLog::technicalLog($e);
		          }
               }
               else $error_remark = $this->getErrorRemark('03','Customer ID'); 
         }
    
         
        if(!$cust_id) $error_remark = $this->getErrorRemark('22','Customer ID');
        if($error_remark)
        { 
           $msg = $error_remark; 
           $class = 'F'; 
        }
        else
        {
           $class = 'S';
           $msg = $this->getErrorRemark('00','Customer ID',$cust_id);
        }
    
        //insert log
        try 
        {
	       $this->_db->beginTransaction();
	       
	       Application_Helper_General::writeLog('BNUD','view delete');
	      
           $this->_db->commit();
	    }
	    catch(Exception $e)
	    {
	       $this->_db->rollBack();
  	       Application_Log_GeneralLog::technicalLog($e);
	    }
    
         $this->_helper->getHelper('FlashMessenger')->addMessage($class);
         $this->_helper->getHelper('FlashMessenger')->addMessage($msg);
//         $this->_redirect($this->_backURL);
         
   }
  
  
}








