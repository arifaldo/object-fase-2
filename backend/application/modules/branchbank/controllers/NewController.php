<?php

require_once 'Zend/Controller/Action.php';
require_once 'Zend/Validate/EmailAddress.php';

class Branchbank_NewController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti


	public function indexAction()
	{
		//pengaturan url untuk button back
		//$this->setbackURL('/'.$this->_request->getModuleName().'/index');    
		$this->_helper->layout()->setLayout('newlayout');
		$this->setbackURL('/' . $this->_request->getModuleName() . '/index');

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if (count($temp) > 1) {
			if ($temp[0] == 'F' || $temp[0] == 'S') {
				if ($temp[0] == 'F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = '';
				unset($temp[0]);
				foreach ($temp as $value) {
					if (!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}
		}

		$this->view->report_msg = array();
		if ($this->_request->isPost()) {
			$filters = array(
				'branch_code' => array('StringTrim', 'StripTags'),
				'branch_code_bi' => array('StringTrim', 'StripTags'),
				'branch_name' => array('StringTrim', 'StripTags'),
				'branch_address' => array('StringTrim', 'StripTags'),
				'branch_email' => array('StringTrim', 'StripTags'),
				'city_name' => array('StringTrim', 'StripTags'),
				'region_name' => array('StringTrim', 'StripTags'),
				'contact' => array('StringTrim', 'StripTags'),
				'headquarter' => array('StringTrim', 'StripTags'),
				'branch_code_coverage' => array('StringTrim', 'StripTags'),
				'branch_name_coverage' => array('StringTrim', 'StripTags'),
				'pnsetempat' => array('StringTrim', 'StripTags'),
			);

			$validators = array(
				'branch_code' => array(
					'NotEmpty',
					array('stringLength', array('min' => 3, 'max' => 3)),
					array('Db_NoRecordExists', array('table' => 'M_BRANCH', 'field' => 'BRANCH_CODE')),
					'messages' => array(
						$this->language->_('Can not be empty'),
						$this->language->_('Inputan harus 3 angka'),
						$this->language->_('Branch Code already existed')
					)
				),
				'branch_code_bi' => array(
					'NotEmpty',
					array('stringLength', array('min' => 3, 'max' => 3)),
					array('Db_NoRecordExists', array('table' => 'M_BRANCH', 'field' => 'BRANCH_CODE_BI')),
					'messages' => array(
						$this->language->_('Can not be empty'),
						$this->language->_('Inputan harus 3 angka'),
						$this->language->_('Branch Code BI already existed')
					)
				),
				'branch_name' => array(
					'NotEmpty',
					array('Db_NoRecordExists', array('table' => 'M_BRANCH', 'field' => 'BRANCH_NAME')),
					'messages' => array(
						$this->language->_('Can not be empty'),
						$this->language->_('Branch Name already existed')
					)
				),
				'branch_address'      => array(
					'NotEmpty',
					'messages' => array(
						$this->language->_('Can not be empty')
					)
				),
				'branch_email'      => array(
					'NotEmpty',
					'EmailAddress',
					// array(
					// 	'name' => 'EmailAddress',
					// 	'options' => array(
					// 		'messages' => array(
					// 			Zend_Validate_EmailAddress::INVALID_FORMAT => 'Format Email Salah'
					// 		)
					// 	)
					// ),
					'messages' => array(
						$this->language->_('Can not be empty'),
						$this->language->_('Format Email Salah'),
					)
				),
				'city_name'      => array(
					'NotEmpty',
					'messages' => array(
						$this->language->_('Can not be empty')
					)
				),
				'region_name'      => array(
					'NotEmpty',
					'messages' => array(
						$this->language->_('Can not be empty')
					)
				),
				'contact'      => array(
					'NotEmpty',
					'messages' => array(
						$this->language->_('Can not be empty')
					)
				),
				'pnsetempat'      => array(
					'NotEmpty',
					'messages' => array(
						$this->language->_('Can not be empty')
					)
				),
				'headquarter' => array(
					'NotEmpty',
					'messages' => array(
						$this->language->_('Can not be empty')
					)
				),
				'branch_code_coverage' => array(
					'allowEmpty' => true,
					'messages' => array()
				),
				'branch_name_coverage' => array(
					'allowEmpty' => true,
					'messages' => array()
				),
			);


			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);

			if ($zf_filter_input->isValid()) {
				$branch_name_coverageArr = explode(",", $this->_getParam('branch_name_coverage'));


				$content = array(
					'BRANCH_CODE' 	 => $zf_filter_input->branch_code,
					'BRANCH_CODE_BI' 	 => $zf_filter_input->branch_code_bi,
					'BRANCH_NAME' 	 => $zf_filter_input->branch_name,
					'BANK_ADDRESS' 	 => $zf_filter_input->branch_address,
					'BRANCH_EMAIL' 	 => $zf_filter_input->branch_email,
					'CITY_NAME' 	 => $zf_filter_input->city_name,
					'REGION_NAME' 	 => $zf_filter_input->region_name,
					'CONTACT' 	 => $zf_filter_input->contact,
					'HEADQUARTER' 	 => $zf_filter_input->headquarter,
					'LOCAL_PN' 	 => $zf_filter_input->pnsetempat,
				);


				try {
					//print_r($content);die;
					//-------- insert --------------
					$this->_db->beginTransaction();

					if ($zf_filter_input->headquarter == 'YES') {

						$content2 = array(
							'HEADQUARTER' 	 => 'NO'
						);
						$whereArr2  = array('HEADQUARTER = ?' => $zf_filter_input->headquarter);
						$this->_db->update('M_BRANCH', $content2, $whereArr2);
					}
					//die;

					$this->_db->insert('M_BRANCH', $content);
					if ($branch_name_coverageArr) {


						foreach ($branch_name_coverageArr as $row) {
							if (!empty($row)) {
								$contentCoverage = array(
									'BRANCH_CODE' 	 => $zf_filter_input->branch_code,
									'BRANCH_COVERAGE_CODE' 	 => $this->getBranch($row)["BRANCH_CODE"],
									'BRANCH_COVERAGE_NAME' 	 => $this->getBranch($row)["BRANCH_NAME"],
									'BANK_COVERAGE_ADDRESS' 	 => $this->getBranch($row)["BANK_ADDRESS"],
									'CONTACT' 	 => $this->getBranch($row)["CONTACT"],
									'CREATED' 		 => date("Y-m-d H:i:s"),
									'CREATEDBY' 	 => $this->_userIdLogin
								);
								$this->_db->insert('M_BRANCH_COVERAGE', $contentCoverage);
							}
						}
					}


					$this->_db->commit();
					//$id = $this->_db->lastInsertId();
					Application_Helper_General::writeLog('MNBB', 'Add New Branch Bank. Branch Name : [' . $zf_filter_input->branch_name . '], Branch Address : [' . $zf_filter_input->branch_address . ']');
					$this->view->success = true;
					$this->view->report_msg = array();

					$this->_redirect('/notification/success/index');
				} catch (Exception $e) {
					var_dump($e);
					//rollback changes
					$this->_db->rollBack();

					foreach (array_keys($filters) as $field)
						$this->view->$field = $zf_filter_input->getEscaped($field);

					$errorMsg = 'exeption';
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
				}
			} else {
				$this->view->error = true;
				foreach (array_keys($filters) as $field)
					$this->view->$field = ($zf_filter_input->isValid($field)) ? $zf_filter_input->getEscaped($field) : $this->_getParam($field);


				if ($zf_filter_input->headquarter == 'YES') {
					$this->view->yhead      = 'selected';
				} else if ($zf_filter_input->headquarter == 'NO') {
					$this->view->nhead      = 'selected';
				} else {
					$this->view->nn      = 'selected';
				}
				$error = $zf_filter_input->getMessages();

				//format error utk ditampilkan di view html 
				$errorArray = null;
				foreach ($error as $keyRoot => $rowError) {
					foreach ($rowError as $errorString) {
						// if ($keyRoot == 'branch_email') $errorArray[$keyRoot] = 'Format Email Salah';
						$errorArray[$keyRoot] = $errorString;
					}
				}

				$this->view->succes = false;
				$this->view->report_msg = $errorArray;
			}
		}
	}

	public function getBranch($branch_name)
	{
		$select = $this->_db->select()
			->from(array('M_BRANCH'), array('*'))
			->where('BRANCH_NAME = ?', $branch_name);

		$data = $this->_db->fetchRow($select);

		return $data;
	}
}
