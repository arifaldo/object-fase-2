<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Payment.php';
class tools_PaymenthistoryController extends Application_Main {
	
	public function initController(){
	}
	
	public function indexAction() 
	{
		$paymentRef = $this->_getParam('paymentref');
		$Payment = new 	Payment($paymentRef);
		$listHistory =  $Payment->getHistory();
		$this->view->paymentHistory =  $listHistory;
		$this->view->dateTimeDisplayFormat  =  $this->_dateTimeDisplayFormat;
	}
}

