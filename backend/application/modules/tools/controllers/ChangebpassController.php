<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class tools_ChangebpassController extends Application_Main {
	
	public function indexAction() 
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
		if($this->_request->isPost())
		{
		$password = $this->_getParam('password');
		$buserId 	= $this->_getParam('userId');
		$key			= md5($buserId);

// $key = md5 ( $username );	
					// $pass = ($zf_filter_input->password);

		$encrypt = new Crypt_AESMYSQL ();
		$encryptedNewPassword = md5 ( $encrypt->encrypt ( $password, $key ) );
		
		// $encrypted_pwd = Crypt_AESMYSQL::encrypt($password, $key);
		// $encryptedNewPassword = md5($encrypted_pwd);
		
		$data = array(
						'BUSER_PASSWORD' => $encryptedNewPassword,
						'BUSER_LASTCHANGEPASSWORD' => new Zend_Db_Expr("now()"),
					);
		$where =  array();							
		$where['BUSER_ID = ?'] = $buserId;
		$update = $this->_db->update('M_BUSER',$data,$where);					

			$this->view->message = $update.' password changes';
		
		}
	}	
}
