<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';

class Chargestemplatelist_ChargestemplatelistdetailController extends customer_Model_Customer 
{
	public function indexAction()
	{ 
		$this->_helper->layout()->setLayout('newlayout');
		$template_id = $this->_getParam('tempid');
		//Zend_Debug::dump($template_id);die;
		$delete = $this->_getParam('delete');
		$mData = $this->_db->select()
			->from(array('A' => 'M_TEMPLATE_CHARGES_OTHER'), array('*'))
			->join(array('B' => 'M_TEMPLATE_CHARGES_OTHER_DETAIL'),'A.TEMPLATE_ID = B.TEMPLATE_ID' , array('CHARGES_TYPE','CHARGES_CCY','CHARGES_AMT'))
			->where("A.TEMPLATE_ID = ".$this->_db->quote($template_id));
		$arr = $this->_db->fetchAll($mData);
		$template_name = $arr[0]['TEMPLATE_NAME'];
		
		$this->view->templatedetail = $arr;

		
	 	foreach($arr as $datadetail)
		{
			$TEMPLATE_ID =  $datadetail['TEMPLATE_ID'];
		    $TEMPLATE_NAME =  $datadetail['TEMPLATE_NAME'];
			if ($datadetail['CHARGES_TYPE'] == 1)
			{
			    $RTGSAMT = $datadetail['CHARGES_AMT'];
				$RTGSCCY = $datadetail['CHARGES_CCY'];
			}
			
			if ($datadetail['CHARGES_TYPE'] == 2)
			{
			    $SKNAMT = $datadetail['CHARGES_AMT'];
				$SKNCCY = $datadetail['CHARGES_CCY'];
			}
		}
		
		$cektemp = $this->_db->select()
			->from(array('A' => 'TEMP_TEMPLATE_CHARGES_OTHER'), array('*'))
			->where("A.TEMPLATE_NAME = ".$this->_db->quote($template_name));
		$cektemp = $this->_db->fetchAll($cektemp);
		
		if($cektemp)
		{
			$this->view->disabled = 'disabled';
		}
		
		if($delete && !$cektemp && $this->view->hasPrivilege('CTUD'))
		{
			$this->_db->beginTransaction();
				try 
				{
					$template_id = $this->_getParam('tempid');
					$mData = $this->_db->select()
								->from(array('A' => 'M_TEMPLATE_CHARGES_OTHER'), array('*'))
								->join(array('B' => 'M_TEMPLATE_CHARGES_OTHER_DETAIL'),'A.TEMPLATE_ID = B.TEMPLATE_ID' , array('CHARGES_TYPE','CHARGES_CCY','CHARGES_AMT'))
								->where("A.TEMPLATE_ID = ".$this->_db->quote($template_id));
					$arr = $this->_db->fetchAll($mData);
					
					foreach($arr as $datadetail)
					{
						$TEMPLATE_ID =  $datadetail['TEMPLATE_ID'];
		    			$TEMPLATE_NAME =  $datadetail['TEMPLATE_NAME'];
						if($datadetail['CHARGES_TYPE'] == 1)
						{
			    			$RTGSAMT = $datadetail['CHARGES_AMT'];
							$RTGSCCY = $datadetail['CHARGES_CCY'];
						}
						if ($datadetail['CHARGES_TYPE'] == 2)
						{
			    			$SKNAMT = $datadetail['CHARGES_AMT'];
							$SKNCCY = $datadetail['CHARGES_CCY'];
						}
					}
					
					$tempchargertgs = Application_Helper_General::convertDisplayMoney($RTGSAMT);
					$tempchargeskn = Application_Helper_General::convertDisplayMoney($SKNAMT);
			
					$chargertgs = str_replace('.','',$tempchargertgs);
					$chargeskn = str_replace('.','',$tempchargeskn);
					//Zend_Debug::dump($SKNAMT);die;
					$changeInfo= "Delete Template Charge Other";
					$changeType = $this->_changeType['code']['delete'];
					$keyField = $TEMPLATE_ID;
					$keyValue = $TEMPLATE_NAME;

					$masterTable = 'M_TEMPLATE_CHARGES_OTHER,M_TEMPLATE_CHARGES_OTHER_DETAIL';
					$tempTable =  'TEMP_TEMPLATE_CHARGES_OTHER,TEMP_TEMPLATE_CHARGES_OTHER_DETAIL';
				
					$displayTableName = 'Charges Template';
					//var_dump($dataforinsert);exit;					
					$changesId = $this->suggestionWaitingApproval($displayTableName, $changeInfo, $changeType,'',$masterTable,$tempTable,$keyField,$keyValue);
					//echo $TEMPLATE_ID;die;
			    	$content = array(
							'TEMPLATE_NAME' => $TEMPLATE_NAME,
			    			'TEMPLATE_ID'	=> $TEMPLATE_ID,
							'SUGGESTED' 	=> date("Y-m-d H:i:s"),
							'SUGGESTEDBY' 	=> $this->_userIdLogin,
							'CHANGES_ID' 	=> $changesId,
			    			'STATUS'		=> '3',
						    );
						   // die;
				//insert into temp
					$this->_db->insert('TEMP_TEMPLATE_CHARGES_OTHER',$content);
					
					$lastIdTemp = $this->_db->fetchOne('select @@identity');
				
					$content2 = array(
				            'TEMP_ID' 		=> 	$lastIdTemp,
							'TEMPLATE_NAME' => 	$TEMPLATE_NAME,
							'TEMPLATE_ID'	=> 	$TEMPLATE_ID,
							'CHARGES_TYPE' 	=> 	1,
							'CHARGES_CCY' 	=>	$RTGSCCY,
							'CHARGES_AMT' 	=> 	$chargertgs,
							'CHANGES_ID' 	=> 	$changesId
						    );
					
		//echo $lastIdTemp;
				    $this->_db->insert('TEMP_TEMPLATE_CHARGES_OTHER_DETAIL',$content2);	

                	$content3 = array(
				            'TEMP_ID' 		=>	$lastIdTemp,
							'TEMPLATE_NAME' => 	$TEMPLATE_NAME,
                			'TEMPLATE_ID'	=> 	$TEMPLATE_ID,
							'CHARGES_TYPE' 	=> 	2,
							'CHARGES_CCY' 	=>	$SKNCCY,
							'CHARGES_AMT' 	=> 	$chargeskn,
							'CHANGES_ID' 	=> 	$changesId
						    );
					//echo $lastIdTemp;die;
				   	$this->_db->insert('TEMP_TEMPLATE_CHARGES_OTHER_DETAIL',$content3);
				   	
					$this->_db->commit();
					$this->setbackURL('/'.$this->_request->getModuleName().'/index/index/');
					Application_Helper_General::writeLog('CTUD','Delete Charge Template ('.$template_id.')');
					$this->_redirect('/notification/submited');
				}
				catch(Exception $e) 
				{
					//die;
					$this->_db->rollBack();	
				}
		}
		if(!$this->_request->isPost()){
		Application_Helper_General::writeLog('CTLS','View Charge Template List Detail ('.$template_id.')');
		}
	}
}