<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';

class Chargestemplatelist_SuggestiondetailController extends Application_Main
{
	public function initController(){
			  $this->_helper->layout()->setLayout('newpopup');
	}

	public function indexAction()
	{
		$changes_id = $this->_getParam('changes_id');
		$this->view->typeCode = array_flip($this->_changeType['code']);
    	$this->view->typeDesc = $this->_changeType['desc'];
    	$this->view->modulename = $this->_request->getModuleName();

		$select = $this->_db->select()
					        ->from(array('A' => 'T_GLOBAL_CHANGES'),array('*'));
		$select -> where("A.CHANGES_ID = ?", $changes_id);
		$select -> where("A.CHANGES_STATUS = 'WA' OR A.CHANGES_STATUS = 'RR'");
		$result = $select->query()->FetchAll();
		$this->view->result=$result;

		if(!$result)
		{
			$this->_redirect('/notification/invalid/index');
		}

		$sugdata = $this->_db->select()
			->from(array('A' => 'TEMP_TEMPLATE_CHARGES_OTHER'), array('*'))
			->joinleft(array('B' => 'TEMP_TEMPLATE_CHARGES_OTHER_DETAIL'),'A.CHANGES_ID = B.CHANGES_ID' , array('CHARGES_TYPE','CHARGES_CCY','CHARGES_AMT'))
			->where("A.CHANGES_ID = ".$this->_db->quote($changes_id));
		$arrsug = $this->_db->fetchAll($sugdata);
		$this->view->sugdetail = $arrsug;

		$tempid = $arrsug[0]['TEMPLATE_ID'];


		$curdata = $this->_db->select()
			->from(array('A' => 'M_TEMPLATE_CHARGES_OTHER'), array('*'))
			->join(array('B' => 'M_TEMPLATE_CHARGES_OTHER_DETAIL'),'A.TEMPLATE_ID = B.TEMPLATE_ID' , array('*'))
			->where("A.TEMPLATE_ID LIKE ".$this->_db->quote($tempid));
		$arrcur = $this->_db->fetchAll($curdata);
		$this->view->curdetail = $arrcur;

		$this->view->changes_id = $changes_id;

		$changestype = $result[0]["CHANGES_TYPE"];
   	 	//Zend_Debug::dump($arrsug); die;
    	if($changestype == "N")
    	{
    		$this->_helper->viewRenderer('suggestiondetail/new', null, true);
    	}
   		if($changestype == "E")
    	{
    		$this->_helper->viewRenderer('suggestiondetail/edit', null, true);
    	}
    	if($changestype == "L")
    	{
    		$this->_helper->viewRenderer('suggestiondetail/delete', null, true);
    	}

		if(!$this->_request->isPost()){
		Application_Helper_General::writeLog('CTCL','View Charge Template ('.$changes_id.') Change List');
		}
	}
}
