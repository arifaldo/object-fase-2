<?php


require_once 'Zend/Controller/Action.php';


class Chargestemplatelist_RepairController extends Application_Main
{
    public function indexAction()
	{
		$changesid = $this->_getParam('changes_id');
		$mData = $this->_db->select()
			->from(array('A' => 'TEMP_TEMPLATE_CHARGES_OTHER'), array('*'))
			->join(array('B' => 'TEMP_TEMPLATE_CHARGES_OTHER_DETAIL'),'A.CHANGES_ID = B.CHANGES_ID' , array('CHARGES_TYPE','CHARGES_CCY','CHARGES_AMT'))
			->where("A.CHANGES_ID = ".$this->_db->quote($changesid));
		
		$arr = $this->_db->fetchAll($mData);
		
		$tempname =  $arr[0]['TEMPLATE_NAME'];
		
		if($arr[0]['TEMPLATE_ID'])
		{
			$tempid = $arr[0]['TEMPLATE_ID'];
		}
		
		//Application_Helper_General::writeLog('CTCR','View Repair Charge Template ('.$tempid.') page');
		Application_Helper_General::writeLog('CTCR','View Repair Charge Template');
		
		$ccyArr = $this->_db->select()
						->from(array('M_MINAMT_CCY'), array('CCY_ID'))
						->order('CCY_ID ASC')
						->query()
						->fetchAll();
		$this->view->ccyArr = Application_Helper_Array::listArray($ccyArr,'CCY_ID','CCY_ID');
		
		if(is_array($ccyArr))
		{
	        	$ccyArrValidate = Application_Helper_Array::simpleArray($ccyArr,'CCY_ID');
	    }
	    
		$this->view->charge_skn_curr 	= $this->_getParam('charge_skn_curr');
		$this->view->charge_rtgs_curr 	= $this->_getParam('charge_rtgs_curr');
		$this->view->charge_rtgs 		= $this->_getParam('charge_rtgs');
		$this->view->charge_skn 		= $this->_getParam('charge_skn');
		
		$chargertgs = $this->_getParam('charge_rtgs');
		$chargeskn 	= $this->_getParam('charge_skn');
			
		$tempchargertgs = Application_Helper_General::convertDisplayMoney($chargertgs);
		$tempchargeskn = Application_Helper_General::convertDisplayMoney($chargeskn);
			
		$chargertgs = str_replace('.','',$tempchargertgs);
		$chargeskn = str_replace('.','',$tempchargeskn);
			
		$this->_setParam('charge_rtgs',$chargertgs);
		$this->_setParam('charge_skn',$chargeskn);
		
		//echo $chargertgs;die;
	    
		$this->view->templatedetail = $arr;
		
		$filters = array(
							 'charge_skn_curr' => array('StringTrim','StripTags'),
							 'charge_rtgs_curr'    => array('StringTrim','StripTags'),
							 'charge_rtgs'    => array('StringTrim','StripTags'),
							 'charge_skn'    => array('StringTrim','StripTags')
							);
		 		
		$validators = array(
								'charge_skn_curr' => array('NotEmpty',
			                                         new Zend_Validate_StringLength(array('max'=>3)),
													 'messages' => array(
																        $this->language->_('Can not be empty'),
																         $this->language->_('Data too long (max 3 chars)'),
			                                                             )
													),					
								'charge_rtgs_curr' => array('NotEmpty',
													  new Zend_Validate_StringLength(array('max'=>3)),
													  'messages' => array(
													                     $this->language->_('Can not be empty'),
																         $this->language->_('Data too long (max 3 chars)'),
																         )
														),
								'charge_rtgs'      => array('NotEmpty',
													  'Digits',
													  new Zend_Validate_StringLength(array('max'=>15)),
													  'messages' => array(
													                     $this->language->_('Can not be empty'),
													 					 $this->language->_('Entry Must be Numbers'),
																         $this->language->_('Data too long (max 15 chars)'),
																         )
														),
								'charge_skn'      => array('NotEmpty',
													 'Digits',
													  new Zend_Validate_StringLength(array('max'=>15)),
													 'messages' => array(
													                     $this->language->_('Can not be empty'),
													 					 $this->language->_('Entry Must be Numbers'),
																         $this->language->_('Data too long (max 15 chars)'),
																         )
														),
														
								
								
							   );
							   
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			//Zend_Debug::dump($zf_filter_input->charge_rtgs_curr);die;
			$re = '%^[a-zA-Z0-9+\-]*$%';		
			$cekspecialchar = preg_match($re, $zf_filter_input->template_name)? 1 : 0;
			
			$submit = $this->_getParam('submit');
			
			if($submit && $zf_filter_input->isValid() && $this->view->hasPrivilege('CTCR'))
			{
				$this->_db->beginTransaction();
				Application_Helper_General::writeLog('CTCR','Repairing Charge Template ('.$changesid.') page');
				try
				{
			        $info= "Charges Template";
					//var_dump($dataforinsert);exit;
					
					$where = array('CHANGES_ID = ?' => $changesid);
					$this->_db->delete('TEMP_TEMPLATE_CHARGES_OTHER',$where);
					$this->_db->delete('TEMP_TEMPLATE_CHARGES_OTHER_DETAIL',$where);
					$this->updateGlobalChanges($changesid,$info);
	  				$content = array(
							'TEMPLATE_NAME'  => $tempname,
							'SUGGESTED' => date("Y-m-d H:i:s"),
							'SUGGESTEDBY' => $this->_userIdLogin,
							'CHANGES_ID' => $changesid
						    );
					if ($tempid)
					{
				    	$content['TEMPLATE_ID'] = $tempid;
					}
					
				//insert into temp
					$this->_db->insert('TEMP_TEMPLATE_CHARGES_OTHER',$content);
				
			    	$lastIdTemp = $this->_db->fetchOne('select @@identity');
				
					$content2 = array(
							'TEMP_ID' 		=> $lastIdTemp,
							'TEMPLATE_NAME' => $tempname,
							'CHARGES_TYPE' 	=> 1,
							'CHARGES_CCY' 	=> $zf_filter_input->charge_rtgs_curr,
							'CHARGES_AMT' 	=> $tempchargertgs,
							'CHANGES_ID' 	=> $changesid
						    );
					
					if ($tempid)
					{
				    	$content2['TEMPLATE_ID'] = $tempid;
					}
					$this->_db->insert('TEMP_TEMPLATE_CHARGES_OTHER_DETAIL',$content2);	

                	$content3 = array(
                			'TEMP_ID' 		 => $lastIdTemp,
							'TEMPLATE_NAME'  => $tempname,
							'CHARGES_TYPE' 	 => 2,
							'CHARGES_CCY' 	 => $zf_filter_input->charge_skn_curr,
							'CHARGES_AMT' 	 => $tempchargeskn,
							'CHANGES_ID' 	 => $changesid
						    );
					if ($tempid)
					{
				   	 $content3['TEMPLATE_ID'] = $tempid;
					}
					$this->_db->insert('TEMP_TEMPLATE_CHARGES_OTHER_DETAIL',$content3);	
					//echo $changesid;die;
					$this->_db->commit();
					$this->_redirect('/popup/successpopup/index');
				}
				catch(Exception $e)
				{
					die;
					$this->_db->rollBack();					
				}	
			}
			
			if($submit && !$zf_filter_input->isValid())
			{
				$this->view->error = true;
				$docErr = $this->language->_("Error in processing form values. Please correct values and re-submit.");
				$this->view->errorMsg = $docErr;
				
				foreach($zf_filter_input->getMessages() as $key=>$err)
				{
					$xxx = 'x'.$key;
					$this->view->$xxx = $this->displayError($err);
					if (!$cekspecialchar)
					{		
						$this->view->xtemplate_name = $this->language->_('Must not contain special characters');
						$this->view->template_name	= '';
					}
				}
			}
	}
}