<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';

class Cmdreport_DailyhostreportController extends Application_Main
{
	protected $_moduleDB = 'RTF';
	
	public function indexAction(){
		$this->_helper->layout()->setLayout('newlayout');		
		
       	$reporttype = array(	'0' =>"Select Report Type",
								'1'	=>'BOD',
								'2'	=>'EOD',
							);
		$reporttypepost = $this->_request->getPost("REPORTTYPE");
		$reporttypelist = ($reporttypepost != null)? $reporttypepost: $this->_getParam("REPORTTYPE");					
		
		if($reporttypelist == 2){
			$recordstatus = array(	''=>"Any Value",
									'1'=>'File Created',
									'2'=>'Success',
									'3'=>'Failed',
								);
		
		}
		else $recordstatus = array(""=>"Any Value");
				
		$this->view->reporttype		= $reporttype;
		$this->view->recordstatus	= $recordstatus;
		
		
		$fieldsBOD = array	(
							'type'  			=> array(
															'field' => 'type',
															'label' => 'Type',
															'sortable' => true
														),
							'community'  		=> array(
															'field' => 'community',
															'label' => 'Community',
															'sortable' => true
														),
							'member'  			=> array(
															'field' => 'member',
															'label' => 'Member',
															'sortable' => true
														),
							'loanid'  			=> array(
															'field' => 'loanid',
															'label' => 'Loan ID',
															'sortable' => true
														),
							'invoiceno'  		=> array(
															'field' => 'invoiceno',
															'label' => 'Invoice No',
															'sortable' => true
														),
							'ccy'  				=> array(
															'field' => 'ccy',
															'label' => 'CCY',
															'sortable' => true
														),
							'principalamount'  	=> array(
															'field' => 'principalamount',
															'label' => 'Principal Amount',
															'sortable' => true
														),
							'repaymentamount' 	=> array(
															'field' => 'repaymentamount',
															'label' => 'Repayment Amount',
															'sortable' => true
														),
														
							'intAT'  			=> array(
															'field' => 'intAT',
															'label' => 'Interest Additional Tenor (%)',
															'sortable' => true
														),	
							
							'intGP'  			=> array(
															'field' => 'intGP',
															'label' => 'Interest Grace Period (%)',
															'sortable' => true
														),
							'intPEN'  			=> array(
															'field' => 'intPEN',
															'label' => 'Interest Penalty (%)',
															'sortable' => true
														),
							
						);
						
			
		$fieldsEOD = array	(
							'type'  			=> array(
															'field' => 'type',
															'label' => 'Type',
															'sortable' => true
														),
							'community'  		=> array(
															'field' => 'community',
															'label' => 'Community',
															'sortable' => true
														),
							'member'  			=> array(
															'field' => 'member',
															'label' => 'Member',
															'sortable' => true
														),	
							'loanid'  			=> array(
															'field' => 'loanid',
															'label' => 'Loan ID',
															'sortable' => true
														),
							'invoiceno'  		=> array(
															'field' => 'invoiceno',
															'label' => 'Invoice No',
															'sortable' => true
														),
							'ccy'  				=> array(
															'field' => 'ccy',
															'label' => 'CCY',
															'sortable' => true
														),
							'invoiceamount'  	=> array(
															'field' => 'invoiceamount',
															'label' => 'Invoice Amount',
															'sortable' => true
														),
							'settlementamount' 	=> array(
															'field' => 'settlementamount',
															'label' => 'Settlement Amount',
															'sortable' => true
														),
							'earlyRate'  		=> array(
															'field' => 'earlyRate',
															'label' => 'Early Rate(%)',
															'sortable' => true
														),	
							'ATRate'  			=> array(
															'field' => 'ATRate',
															'label' => 'Additional Tenor Rate (%)',
															'sortable' => true
														),	
							
							'GPRate'  			=> array(
															'field' => 'GPRate',
															'label' => 'Grace Period Rate (%)',
															'sortable' => true
														),
							'PENRate'  			=> array(
															'field' => 'PENRate',
															'label' => 'Penalty Rate (%)',
															'sortable' => true
														),
							'status'  			=> array(
															'field' => 'status',
															'label' => 'Status',
															'sortable' => true
														),
							
						);
						
		//validasi page, jika input page bukan angka               
		$page 			= $this->_getParam('page');
		$csv 			= $this->_getParam('csv');
		$pdf 			= $this->_getParam('pdf');
		$filter 		= $this->_getParam('filter');
		$clearfilter 	= $this->_getParam('clearfilter');
		$page 			= (Zend_Validate::is($page,'Digits'))? $page : 1;
		
		$this->view->filter			= $filter;
		$this->view->clearfilter	= $clearfilter;
		$this->view->currentPage 	= $page;
		
		
		$filterArr = array(
							'REPORTTYPE' 		=> array('StripTags'),
							'INVOICENO'    		=> array('StripTags','StringTrim','StringToUpper'),
							'LOANID'    		=> array('StripTags','StringTrim','StringToUpper'),
							'RECORDSTATUS' 		=> array('StripTags'),						   
	                      );
		
		// if POST value not null, get post, else get param
		$dataParam = array(	"REPORTTYPE","INVOICENO","LOANID","RECORDSTATUS");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{
			$dataPost = $this->_request->getPost($dtParam);
			$dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}
		
		// The default is set so all fields allow an empty string		
		$options = array('allowEmpty' => true);
		$validators = array(
								'REPORTTYPE' 	=> array(array('InArray', array('haystack' => array_keys($reporttype)))),
								'INVOICENO' 	=> array(),	
								'LOANID' 		=> array(),	
								'RECORDSTATUS' 	=> array(array('InArray', array('haystack' => array_keys($recordstatus)))),							
						);
		
		$zf_filter  		= new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);
		
		$fREPORTTYPE 		= html_entity_decode($zf_filter->getEscaped('REPORTTYPE'));
		$fINVOICENO 		= html_entity_decode($zf_filter->getEscaped('INVOICENO'));
		$fLOANID 			= html_entity_decode($zf_filter->getEscaped('LOANID'));
		$fRECORDSTATUS		= html_entity_decode($zf_filter->getEscaped('RECORDSTATUS'));
		
		if($fREPORTTYPE == 0)	$fields = array();
		else if($fREPORTTYPE == 1) $fields = $fieldsBOD;
		else if($fREPORTTYPE == 2) $fields = $fieldsEOD;
		
		//validasi sort, jika input sort bukan ASC atau DESC
		$sortBy  = $this->_getParam('sortby');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
		
		if($fREPORTTYPE == 0){	
			$data 	= array(); 
			
		}
		else if($fREPORTTYPE == 1){ // select from T_LOAN_DETAIL union (T_SETTLEMENT_DETAIL + T_LOAN) -- BOD --
			
			$set = new Settings();
			
			//$lastupdated	= $this->getLastDateBOD();
			
			$lastupdated = $set->getSetting('lastprocessed_BOD');
			//if($lastupdated != NULL) 	$lastupdated 	= Application_Helper_General::convertDate($lastupdated, $this->_dateTimeDisplayFormat);
			if($lastupdated != NULL) 	$lastupdated 	= Application_Helper_General::convertDate($lastupdated, $this->_dateViewFormat);
			else						$lastupdated	= '-';
			
			//DISBURSEMENT
			// $lastupdatedTLOAN = $this->getLastDateTLOANDETAIL();
			
			$lastupdatedTLOAN = $set->getSetting('lastprocessed_bod_loan');
			
			// $lastupdatedTLOAN = '';
			
			if($lastupdatedTLOAN) {
				
				//$date = $lastupdatedTLOAN;
			
				$sTLOAN = $this->_db->select()
									->from(array('LD'=>'T_LOAN_DETAIL'),array(	'loanid'			=>'LOAN_ID',
																				'type'				=>new Zend_Db_Expr("'Disbursement'"),
																				'invoiceno'			=>'DOC_NO',
																				'principalamount'	=>'PRINCIPAL',
																				'settlementamount'	=>'SETTLEMENT_AMOUNT',
																				'repaymentamount'	=>'REPAYMENT_AMOUNT',
																				'totaltobepaid'		=>'TOTALTOBEPAID',
																				'intAT'				=>'AT_RATE',
																				'intGP'				=>'GP_RATE',
																				'intPEN'			=>'PEN_RATE',
																				'ccy'				=>'P.TX_CCY',
																				'community'			=> new Zend_Db_Expr(" P.COMMUNITY_CODE + ' - ' + C.COMMUNITY_NAME"),
																				'member'			=> new Zend_Db_Expr(" P.MEMBER_CODE + ' - ' + P.MEMBER_CUST_ID + ' - ' + P.MEMBER_CUST_NAME "),
																				
																				))
									->joinLeft(array('P'=>'T_TX'),'P.PS_NUMBER = LD.PS_NUMBER',array())
									->joinLeft(array('C'=>'M_COMMUNITY'),'C.COMMUNITY_CODE = P.COMMUNITY_CODE',array())
									->where('LD.LOAN_STATUS is Not NULL AND convert(date, LD.LASTUPDATED) = convert(date, ? )',$lastupdatedTLOAN);
				
			}
			else{ 
				
				$sTLOAN = $this->_db->select()
									->from(array('LD'=>'T_LOAN_DETAIL'),array(	'loanid'			=>'LOAN_ID',
																				'type'				=>new Zend_Db_Expr("'Disbursement'"),
																				'invoiceno'			=>'DOC_NO',
																				'principalamount'	=>'PRINCIPAL',
																				'settlementamount'	=>'SETTLEMENT_AMOUNT',
																				'repaymentamount'	=>'REPAYMENT_AMOUNT',
																				'totaltobepaid'		=>'TOTALTOBEPAID',
																				'intAT'				=>'AT_RATE',
																				'intGP'				=>'GP_RATE',
																				'intPEN'			=>'PEN_RATE',
																				'ccy'				=>'P.TX_CCY',
																				'community'			=> new Zend_Db_Expr(" P.COMMUNITY_CODE + ' - ' + C.COMMUNITY_NAME"),
																				'member'			=> new Zend_Db_Expr(" P.MEMBER_CODE + ' - ' + P.MEMBER_CUST_ID + ' - ' + P.MEMBER_CUST_NAME "),
																				
																				))
									->joinLeft(array('P'=>'T_TX'),'P.PS_NUMBER = LD.PS_NUMBER',array())
									->joinLeft(array('C'=>'M_COMMUNITY'),'C.COMMUNITY_CODE = P.COMMUNITY_CODE',array())
									->where("1=2");
			}
			
			//SETTLEMENT
			// $lastupdatedTSETTLE = $this->getLastDateTSETTLEMENTDETAIL();
			
			$lastupdatedTSETTLE = $set->getSetting('lastprocessed_bod_settl');
			// $lastupdatedTSETTLE = '';
			
			if($lastupdatedTSETTLE){
				
				//$date = $lastupdatedTSETTLE;
				
				$sTSETTLE = $this->_db->select()
									  ->from(array('SD'=>'T_SETTLEMENT_DETAIL'),array(
																				'loanid'			=>'SD.LOAN_ID',
																				'type'				=>new Zend_Db_Expr("'Settlement'"),
																				'invoiceno'			=>'L.DOC_NO',
																				'principalamount'	=>'SD.SETTLEMENT_PRINCIPAL',
																				'settlementamount'	=>'SD.SETTLEMENT_AMOUNT',
																				'repaymentamount'	=>'SD.REPAYMENT_AMOUNT',
																				'totaltobepaid'		=>'L.TOTALTOBEPAID',
																				'intAT'				=>new Zend_Db_Expr("0"),
																				'intGP'				=>new Zend_Db_Expr("0"),
																				'intPEN'			=>new Zend_Db_Expr("0"),
																				'ccy'				=>'L.CCY',
																				'community'			=> new Zend_Db_Expr(" P.COMMUNITY_CODE + ' - ' + C.COMMUNITY_NAME"),
																				'member'			=> new Zend_Db_Expr(" P.MEMBER_CODE + ' - ' + P.MEMBER_CUST_ID  + ' - ' + P.MEMBER_CUST_NAME "),
																				
																				))
									  ->joinLeft(array('L'=>'T_LOAN'),'L.LOAN_ID = SD.LOAN_ID',array())
									  ->joinLeft(array('P'=>'T_TX'),'P.PS_NUMBER = L.PS_NUMBER',array())
									  ->joinLeft(array('C'=>'M_COMMUNITY'),'C.COMMUNITY_CODE = P.COMMUNITY_CODE',array())
									  ->where('convert(date, SD.DATE_TIME) = convert(date, ? )',$lastupdatedTSETTLE);
			}
			else {
			
				$sTSETTLE = $this->_db->select()
									  ->from(array('SD'=>'T_SETTLEMENT_DETAIL'),array(
																				'loanid'			=>'SD.LOAN_ID',
																				'type'				=>new Zend_Db_Expr("'Settlement'"),
																				'invoiceno'			=>'L.DOC_NO',
																				'principalamount'	=>'SD.SETTLEMENT_PRINCIPAL',
																				'settlementamount'	=>'SD.SETTLEMENT_AMOUNT',
																				'repaymentamount'	=>'SD.REPAYMENT_AMOUNT',
																				'totaltobepaid'		=>'L.TOTALTOBEPAID',
																				'intAT'				=>new Zend_Db_Expr("0"),
																				'intGP'				=>new Zend_Db_Expr("0"),
																				'intPEN'			=>new Zend_Db_Expr("0"),
																				'ccy'				=>'L.CCY',
																				'community'			=> new Zend_Db_Expr(" P.COMMUNITY_CODE + ' - ' + C.COMMUNITY_NAME"),
																				'member'			=> new Zend_Db_Expr(" P.MEMBER_CODE + ' - ' + P.MEMBER_CUST_ID  + ' - ' + P.MEMBER_CUST_NAME "),
																				
																				))
									  ->joinLeft(array('L'=>'T_LOAN'),'L.LOAN_ID = SD.LOAN_ID',array())
									  ->joinLeft(array('P'=>'T_TX'),'P.PS_NUMBER = L.PS_NUMBER',array())
									  ->joinLeft(array('C'=>'M_COMMUNITY'),'C.COMMUNITY_CODE = P.COMMUNITY_CODE',array())
									  ->where("1=2");
				
			}
			
			
			
			$selectLoan 	= $sTLOAN	->__toString();
			$selectSettle 	= $sTSETTLE	->__toString();
				
			$unionquery = $this->_db->select()
								->union(array($selectLoan,$selectSettle));
		
			$select 	= $this->_db->select()
							->from(($unionquery),array('*'));
			
							
			// echo "<pre>";
			// echo $sTLOAN->__toString();
			// echo "<br /><br />";
			
			// echo $sTSETTLE;
			// echo $sTSETTLE->__toString();
			// echo "<br />";
			
			// echo $select->__toString();
			// die;

			if($fINVOICENO)	$select->where("invoiceno 	LIKE ".$this->_db->quote('%'.$fINVOICENO.'%'));
			if($fLOANID)	$select->where("loanid		LIKE ".$this->_db->quote('%'.$fLOANID.'%'));
			
			$select->order($sortBy.' '.$sortDir);
			
			// echo $select->__toString();
			// die;

			$dataSQL = $this->_db->fetchAll($select);
			
			// echo "<pre>";
			// print_r($dataSQL);
			
			if(is_array($dataSQL) && count ($dataSQL) > 0){
					
					foreach($dataSQL as $d => $val){
						foreach($fields as $key => $row){
						
							$value 	= $val[$key];
							$loanid = $val["loanid"];
							
							if( $key == "principalamount" || $key == "repaymentamount" || $key == "totaltobepaid" || $key == "intAT" || $key == "intGP" || $key == "intPEN"){ 
								if($val["type"] == "Settlement"){
									$value = '-';
								}
								else $value = Application_Helper_General::displayMoney($value);
								
								//if($value != "000")	{ $value = Application_Helper_General::displayMoney($value); }
								//else 					{ $value = '-'; }
							}
							else if($key == "loanid"){
								if($loanid)	$value = $loanid;
								else 		$value = '-';
							}
												
							$data[$d][$key] = $value;
							
						}
					}
				
			}
			else $data = array();
		
			
			//$data 	= array_merge($dataDisburse,$dataSettle); 
		}
		else{ // EOD 
			
			$addrecordstatus = array(	'1'=>'File Created',
										'2'=>'Success',
										'3'=>'Failed',
									);
			
			$typeSendEOD = array(	'1'=>'Send Disbursement',
									'2'=>'Send Settlement',
									'3'=>'Interest Change',
									);
			
			$lastEOD 			= $this->getLastDateSENDEOD();
			
			if($lastEOD){
			
				$lastupdated 	= Application_Helper_General::convertDate($lastEOD, $this->_dateTimeDisplayFormat);
				$date 			= $lastEOD;
				
				//echo "lasteod: ".$lastEOD;
				
				$caseTypeEOD = "(CASE SE.TYPE ";
				foreach($typeSendEOD as $key => $val)	{ $caseTypeEOD .= " WHEN ".$key." THEN '".$val."'"; }
				$caseTypeEOD .= " END)";
				
				$caseStatus = "(CASE SE.RECORD_STATUS ";
				foreach($addrecordstatus as $key => $val){ $caseStatus .= " WHEN ".$key." THEN '".$val."'"; }
				$caseStatus .= " END)";
				
				$sTSENDEOD = $this->_db->select()
										->from(array('SE'=>'SEND_EOD'),array(
																				'loanid'			=>'SE.LOAN_ID',
																				'type'				=>$caseTypeEOD,
																				'invoiceno'			=>'SE.DOC_NO',
																				'invoiceamount'		=>'SE.DOC_AMOUNT',
																				'settlementamount'	=>'SE.SETTLEMENT_AMOUNT',
																				'earlyRate'			=>'SE.EP_RATE',
																				'ATRate'			=>'SE.AT_RATE',
																				'GPRate'			=>'SE.GP_RATE',
																				'PENRate'			=>'SE.PEN_RATE',
																				'status'			=>$caseStatus,
																				'ccy'				=>'L.CCY',
																				'community'			=> new Zend_Db_Expr(" P.COMMUNITY_CODE + ' - ' + C.COMMUNITY_NAME"),
																				'member'			=> new Zend_Db_Expr(" P.MEMBER_CODE + ' - ' + P.MEMBER_CUST_ID  + ' - ' + P.MEMBER_CUST_NAME "),
																				
																				))
										->joinLeft(array('L'=>'T_LOAN'),'L.LOAN_ID = SE.LOAN_ID',array())
										->joinLeft(array('P'=>'T_TX'),'P.PS_NUMBER = L.PS_NUMBER',array())
										->joinLeft(array('C'=>'M_COMMUNITY'),'C.COMMUNITY_CODE = P.COMMUNITY_CODE',array())
										->where('convert(date, SE.LASTUPDATED) = convert(date, ? )',$date);
										
				// echo $sTSENDEOD ->__toString();
				// die;
				
				if($fINVOICENO)		$sTSENDEOD->where("SE.DOC_NO LIKE ".$this->_db->quote('%'.$fINVOICENO.'%'));
				if($fRECORDSTATUS)	$sTSENDEOD->where("SE.RECORD_STATUS = ".$this->_db->quote($fRECORDSTATUS));
				if($fLOANID)		$sTSENDEOD->where("SE.LOAN_ID 		= ".$this->_db->quote($fLOANID));
				
				$sTSENDEOD->order($sortBy.' '.$sortDir);
				
				$dataTSENDEOD = $this->_db->fetchAll($sTSENDEOD);
				
				if(is_array($dataTSENDEOD) && count ($dataTSENDEOD) > 0){
					
					foreach($dataTSENDEOD as $d => $val){
						foreach($fieldsEOD as $key => $row){
						
							$value 	= $val[$key];
							$loanid	= $val["loanid"];
							
							if( $key == "invoiceamount" || $key == "settlementamount" || $key == "earlyRate" || $key == "ATRate" || $key == "GPRate" || $key == "PENRate"){ 
								
								//$value = Application_Helper_General::displayMoney($value);
								if($value)	{ $value = Application_Helper_General::displayMoney($value); }
								else 		{ $value = '-'; }
							}
							else if($key == 'loanid'){
								
								if($loanid)	$value = $loanid;
								else 		$value = '-';
							}
												
							$dataEOD[$d][$key] = $value;
							
						}
					}
				
				}
				else $dataEOD = array();
			
			}
			else{
			
				$lastupdated 	= "-";
				$dataEOD = array();
				
			}
			
			$recordstatus	= $recordstatus + $addrecordstatus;
			$data 			= $dataEOD;
			
		}
		
		$this->paging($data);
		
		if ($csv || $pdf) {	$header  = Application_Helper_Array::simpleArray($fields, "label"); }		
		
		if($csv){
			$this->_helper->download->csv($header,$data,null,'Daily Host Report');  
			Application_Helper_General::writeLog('VHOS','Export Daily Host Report');
			
		}
		elseif($pdf){
			$this->_helper->download->pdf($header,$data,null,'Daily Host Report');  
			Application_Helper_General::writeLog('VHOS','Export Daily Host Report');
			
		}
		else{	
			
			$stringParam = array(
									'REPORTTYPE'	=>$fREPORTTYPE,
									'INVOICENO'		=>$fINVOICENO,
									'LOANID'		=>$fLOANID,
									'RECORDSTATUS'	=>$fRECORDSTATUS,
									
									'clearfilter'	=> $clearfilter,
									'filter'		=> $filter,
									'lastupdated'	=> $lastupdated,
								
								);
			
			$this->view->fields 		= $fields;
			$this->view->filter 		= $filter;
			$this->view->currentPage 	= $page;
			
			$this->view->clearfilter 	= $clearfilter;
			
			$this->view->reporttype		= $reporttype;
			$this->view->recordstatus	= $recordstatus;
			$this->view->lastupdated	= $lastupdated;
			
			$this->view->REPORTTYPE		= $fREPORTTYPE;
			$this->view->INVOICENO		= $fINVOICENO;
			$this->view->LOANID			= $fLOANID;
			$this->view->RECORDSTATUS	= $fRECORDSTATUS;
			
			$this->view->sortDir 		= $sortDir;
	        $this->view->sortBy 		= $sortBy;
			
			Application_Helper_General::writeLog('VHOS','View Daily Host Report');
			
			
		}
		
		//echo $page;
		
		
	}
	
	private function getLastDateBOD(){
		
		$t1 = $this->getLastDateTLOANDETAIL();
		$t2 = $this->getLastDateTSETTLEMENTDETAIL();
		
		$tt1 = strtotime($t1);
		$tt2 = strtotime($t2);
		
		if($t1 != NULL && $t2 != NULL){
			
			if($tt1 > $tt2) $lastdate = $tt1;
			else			$lastdate = $tt2;
			
		}
		else if($t1 == NULL && $t2 != NULL){
			
			$lasdate = $tt2;
		}
		else if($t1 != NULL && $t2 == NULL){
			
			$lastdate = $tt1;
		}
		else{
		
			$lastdate = "";
		}
		
		return $lastdate;
		
	}
	
	private function getLastDateSENDEOD(){
		
		$select = $this->_db->select()
							->from('SEND_EOD',array('LASTUPDATED'=>'LASTUPDATED'))
							->order('LASTUPDATED DESC');
		$lastupdated = $this->_db->fetchOne($select);
		
		return $lastupdated;

	}

	private function getLastDateTLOANDETAIL(){
		
		$select = $this->_db->select()
							->from('T_LOAN_DETAIL',array('LASTUPDATED'=>'LASTUPDATED'))
							->order('LASTUPDATED DESC');
		$lastupdated = $this->_db->fetchOne($select);
		
		return $lastupdated;
	
	}
	
	private function getLastDateTSETTLEMENTDETAIL(){
		
		$select = $this->_db->select()
							->from('T_SETTLEMENT_DETAIL',array('LASTUPDATED'=>'DATE_TIME'))
							->order('DATE_TIME DESC');
		$lastupdated = $this->_db->fetchOne($select);
		
		return $lastupdated;
	
	}
}

/*
	Daily host report terdiri dari 2:
	1. BOD
		table: 
			T_LOAN_DETAIL		: WHERE LASTUPDATED = $set->getSetting('lastprocessed_bod_loan');
			T_SETTLEMENT_DETAIL : WHERE DATETIME 	= $set->getSetting('lastprocessed_bod_settl');
	2. EOD
		table: 
			SEND_EOD			: WHERE SEND_EOD.DATETIME = LAST(SEND_EOD.DATETIME)
			
			16/01/2013
			SEND_EOD diambil where yg lastupdated paling terakhir
								: WHERE SEND_EOD.LASTUPDATED = LAST(SEND_EOD.LASTUPDATED)

*/
