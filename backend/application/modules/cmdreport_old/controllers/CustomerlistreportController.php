<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';

class cmdreport_customerlistreportController extends Application_Main
{
	protected $_moduleDB = 'RTF';

	public function indexAction()
	{
		if (!$this->view->hasPrivilege("RPCS")) {
			return $this->_redirect($this->view->url(["module" => "notification", "controller" => "notauthorized"]));
		}

		$setting = new Settings();
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$pw_hash = md5($enc_salt . $enc_pass);
		$rand = $this->_userIdLogin . date('dHis') . $pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;

		$this->_helper->layout()->setLayout('newlayout');
		$conf = Zend_Registry::get('config');
		$custmodelCode 		= $conf["cust"]["model"]["code"];
		$custmodelDesc 		= $conf["cust"]["model"]["desc"];
		$arrcustmodel = array_combine(array_values($custmodelCode), array_values($custmodelDesc));

		// echo '<pre>';
		// print_r($arrcustmodel); 
		$listId = $this->_db->select()
			->from(
				array('M_CUSTOMER'),
				array('CUST_ID')
			)
			->order('CUST_ID ASC')
			->query()->fetchAll();


		$this->view->listCustId = Application_Helper_Array::listArray($listId, 'CUST_ID', 'CUST_ID');

		$customerstatus = $this->_customerstatus;

		$options = array_combine(array_values($customerstatus['code']), array_values($customerstatus['desc']));
		unset($options[0]);
		unset($options[4]);
		unset($options[5]);

		$this->view->status = $options;

		$arrCUST_Emobileregis = array(0 => "No", 1 => "Yes");
		$this->view->arremobileregistered = $arrCUST_Emobileregis;

		$fields = array(
			'CompanyID'  				=> array(
				'field' => 'CUST_ID',
				'label' => $this->language->_('Company Code'),
				'sortable' => true
			),
			'Model'  					=> array(
				'field' => 'CUST_MODEL',
				'label' => $this->language->_('Model'),
				'sortable' => true
			),
			'Status'  					=> array(
				'field' => 'STATUS',
				'label' => $this->language->_('Status'),
				'sortable' => true
			),
			'Suggested'  				=> array(
				'field' => 'CUST_SUGGESTED',
				'label' => $this->language->_('Last Suggested'),
				'sortable' => true
			),
			'Approved'  				=> array(
				'field' => 'CUST_UPDATED',
				'label' => $this->language->_('Last Approved'),
				'sortable' => true
			),
		);

		$filterlist = array("COMP_CODE", "COMP_NAME", "MODEL", "STATUS", "LAST_SUGGESTED", "LAST_APPROVED");

		$this->view->filterlist = $filterlist;

		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby');
		$sortDir = $this->_getParam('sortdir');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page, 'Digits') && ($page > 0)) ? $page : 1;

		$sortBy = (Zend_Validate::is(
			$sortBy,
			'InArray',
			array(array_keys($fields))
		)) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is(
			$sortDir,
			'InArray',
			array('haystack' => array('asc', 'desc'))
		)) ? $sortDir : 'asc';
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;

		$filterlist = array("COMP_CODE", "COMP_NAME", "MODEL", "STATUS", "LAST_SUGGESTED", "LAST_APPROVED");

		//get filtering param
		$filters = array(
			'filter' =>  array('StringTrim', 'StripTags'),
			'COMP_CODE'      =>  array('StringTrim', 'StripTags', 'StringToUpper'),
			'COMP_NAME'  	=>  array('StringTrim', 'StripTags', 'StringToUpper'),
			'MODEL' =>  array('StringTrim', 'StripTags', 'StringToUpper'),
			'STATUS'     	=>  array('StringTrim', 'StripTags', 'StringToUpper'),
			'LAST_SUGGESTED_START'     	=>  array('StringTrim', 'StripTags'),
			'LAST_SUGGESTED_END'     	=>  array('StringTrim', 'StripTags'),
			'LAST_APPROVED_START'     	=>  array('StringTrim', 'StripTags'),
			'LAST_APPROVED_END'     	=>  array('StringTrim', 'StripTags'),
		);

		$validators = array(
			'filter' => array('allowEmpty' => true),
			'COMP_CODE' => array('allowEmpty' => true),
			'COMP_NAME' => array('allowEmpty' => true),
			'MODEL' => array('allowEmpty' => true),
			'STATUS' => array('allowEmpty' => true),



			'LAST_SUGGESTED_START'  => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
			'LAST_SUGGESTED_END'  => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
			'LAST_APPROVED_START'  => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
			'LAST_APPROVED_END'  => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
		);

		$headers = array(
			'company_code'  => array(
				'field' => 'COMPANY_CODE',
				'label' => 'Company Code'
			),
			'company_name'  => array(
				'field' => 'COMPANY_NAME',
				'label' => 'Company Name'
			),
			'model'  => array(
				'field' => 'MODEL',
				'label' => 'Model'
			),
			'kab'     => array(
				'field' => 'KAB_CITY',
				'label' => 'Kab/City'
			),
			'status'     => array(
				'field' => 'STATUS',
				'label' => 'Status'
			),
			'last_suggested'     => array(
				'field' => 'LAST_SUGGESTED',
				'label' => 'Last Suggested'
			),
			'last_approved'     => array(
				'field' => 'LAST_APPROVED',
				'label' => 'Last Approved'
			)
		);

		$dataParam = array("COMP_CODE", "COMP_NAME", "MODEL", "STATUS", "LAST_SUGGESTED_START", "LAST_SUGGESTED_END", "LAST_APPROVED_START", "LAST_APPROVED_END");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam) {
			if (!empty($this->_request->getParam('wherecol'))) {
				$dataval = $this->_request->getParam('whereval');
				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if ($dtParam == $value) {
						$dataParamValue[$dtParam] = $dataval[$key];
					}
				}
			}

			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}
		
		if(!empty($this->_request->getParam('createdate'))){
				$createarr = $this->_request->getParam('createdate');
				$dataParamValue['LAST_SUGGESTED_START'] = $createarr[0];
				$dataParamValue['LAST_SUGGESTED_END'] = $createarr[1];
		}
		
		if(!empty($this->_request->getParam('approvedate'))){
				$createarr = $this->_request->getParam('approvedate');
				$dataParamValue['LAST_APPROVED_START'] = $createarr[0];
				$dataParamValue['LAST_APPROVED_END'] = $createarr[1];
		}

		$zf_filter = new Zend_Filter_Input($filters, $validators, $dataParamValue, ['allowEmpty' => true]);

		$filter = $this->_getParam('filter');

		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');
		$cust_id = strtoupper($zf_filter->getEscaped('COMP_CODE'));
		$companyName = strtoupper($zf_filter->getEscaped('COMP_NAME'));
		$model = strtoupper($zf_filter->getEscaped('MODEL'));
		$status = strtoupper($zf_filter->getEscaped('STATUS'));
		
		$sugestedStart = strtoupper($zf_filter->getEscaped('LAST_SUGGESTED_START'));
		$sugestedEnd = strtoupper($zf_filter->getEscaped('LAST_SUGGESTED_END'));
		$approveStart = strtoupper($zf_filter->getEscaped('LAST_APPROVED_START'));
		$approveEnd = strtoupper($zf_filter->getEscaped('LAST_APPROVED_END'));


		$caseStatus = "(CASE CUST_STATUS ";
		foreach ($options as $key => $val) {
			$caseStatus .= " WHEN " . $key . " THEN '" . $val . "'";
		}
		$caseStatus .= " END)";

		$caseModel = "(CASE CUST_MODEL ";
		foreach ($arrcustmodel as $key => $val) {
			$caseModel .= " WHEN " . $key . " THEN '" . $val . "'";
		}
		$caseModel .= " END)";


		$select	= $this->_db->select()
			->FROM(
				array('M' => 'M_CUSTOMER'),
				array(
					'CUST_ID',
					'CUST_NAME',
					'CUST_MODEL' => $caseModel,
					'STATUS' => $caseStatus,
					'CUST_SUGGESTED',
					'CUST_UPDATED',
				)
			)
			->join(array('D' => 'M_CITYLIST'), 'M.CUST_CITY = D.CITY_CODE', array('D.CITY_NAME'));

		if ($filter == TRUE || $csv || $pdf || $this->_request->getParam('print')) {

			// $latestSuggestionFrom   = html_entity_decode($zf_filter->getEscaped('CUST_SUGGESTED'));
			// $latestSuggestionTo     = html_entity_decode($zf_filter->getEscaped('CUST_SUGGESTED_end'));

			// $latestSuggestionFrom   = (Zend_Date::isDate($latestSuggestionFrom,$this->_dateDisplayFormat))?
			// new Zend_Date($latestSuggestionFrom,$this->_dateDisplayFormat):
			// false;

			// $latestSuggestionTo     = (Zend_Date::isDate($latestSuggestionTo,$this->_dateDisplayFormat))?
			// new Zend_Date($latestSuggestionTo,$this->_dateDisplayFormat):
			// false;

			$header = Application_Helper_Array::simpleArray($headers, 'label');

			if ($cust_id) {
				$this->view->cust_id    = $cust_id;
				$select->where("UPPER(CUST_ID) LIKE " . $this->_db->quote('%' . $cust_id . '%'));
			}

			if ($companyName) {
				$this->view->companyName	= $companyName;
				$select->where("UPPER(CUST_NAME) LIKE " . $this->_db->quote('%' . $companyName . '%'));
			}

			if ($model) {
				$this->view->model	= $model;
				$select->where("CUST_MODEL = ? ", $model);
			}

			if ($status) {
				$this->view->status	= $status;
				$select->where("CUST_STATUS = ? ", $status);
			}
			
			 $this->view->createdStart    = $sugestedStart;
			 $this->view->createdEnd  = $sugestedEnd;

			 $this->view->approveStart    = $approveStart;
			 $this->view->approveEnd  = $approveEnd;
			if($sugestedStart){
					$FormatDate 	= new Zend_Date($sugestedStart, $this->_dateDisplayFormat);
					$sugestedStart  	= $FormatDate->toString($this->_dateDBFormat);	
					$select->where("DATE(CUST_SUGGESTED) >= ?", $sugestedStart); 
				   }
	
			if($sugestedEnd){
					$FormatDate 	= new Zend_Date($sugestedEnd, $this->_dateDisplayFormat);
					$sugestedEnd 		= $FormatDate->toString($this->_dateDBFormat);	
					$select->where("DATE(CUST_SUGGESTED) <= ?", $sugestedEnd);
				   }
			
			if($approveStart){
					$FormatDate 	= new Zend_Date($approveStart, $this->_dateDisplayFormat);
					$approveStart  	= $FormatDate->toString($this->_dateDBFormat);	
					$select->where("DATE(CUST_UPDATED) >= ?", $approveStart); 
				   }
	
			if($approveEnd){
					$FormatDate 	= new Zend_Date($approveEnd, $this->_dateDisplayFormat);
					$approveEnd 		= $FormatDate->toString($this->_dateDBFormat);	
					$select->where("DATE(CUST_UPDATED) <= ?", $approveEnd);
				   }
				   
		
			/*if (!empty($dataParamValue['LAST_APPROVED_START']) && !empty($dataParamValue['LAST_APPROVED_END'])) {
				$select->where("CUST_UPDATED >= ?", date('Y-m-d', strtotime($dataParamValue['LAST_APPROVED_START'])));
				$select->where("CUST_UPDATED <= ?", date('Y-m-d', strtotime($dataParamValue['LAST_APPROVED_END'] . ' +1 day')));
			}*/

			// if($latestSuggestionFrom){
			// 	$select->where("DATE(CUST_SUGGESTED) >= DATE(".$this->_db->quote($latestSuggestionFrom->toString($this->_dateDBFormat)).")");
			// }
			// if($latestSuggestionTo){
			// 	$select->where("DATE(CUST_SUGGESTED) <= DATE(".$this->_db->quote($latestSuggestionTo->toString($this->_dateDBFormat)).")");
			// }

			// if($custsuggested)
			// {
			// 	$this->view->custsuggested  = $custsuggested;
			// 	$select->where("UPPER(CUST_SUGGESTED) LIKE ".$this->_db->quote('%'.$custsuggested.'%'));
			// } 

			// if(!empty($custsuggestedstart) && !empty($custsuggestedend))
			// 	$select->where("DATE(CUST_SUGGESTED) between ".$this->_db->quote($custsuggestedstart)." and ".$this->_db->quote($custsuggestedend));	

			// if($custapprove)
			// {
			// 	$this->view->custapprove  = $custapprove;
			// 	$select->where("UPPER(CUST_UPDATED) LIKE ".$this->_db->quote('%'.$custapprove.'%'));
			// }
			// if($customerstat)
			// {
			// 	$this->view->customerstat 	= $customerstat;
			// 	$select->where("CUST_STATUS = ".$this->_db->quote($customerstat));
			// } 

			// if($latestSuggestionFrom)  $this->view->latestSuggestionFrom = $latestSuggestionFrom->toString($this->_dateDisplayFormat);
			// if($latestSuggestionTo)    $this->view->latestSuggestionTo   = $latestSuggestionTo->toString($this->_dateDisplayFormat);

		}
		 //echo $select;die;

		// print_r($select->__toString());
		// die();

		$select->order($sortBy . ' ' . $sortDir);

		$this->paging($select);

		$this->view->fields = $fields;
		$this->view->filter = $filter;


		if (!empty($dataParamValue)) {
			$filterlistdata = array("Filter");
			foreach ($dataParamValue as $fil => $val) {
				$paramTrx = $fil . " - " . $val;
				array_push($filterlistdata, $paramTrx);
			}
		} else {
			$filterlistdata = array("Filter - None");
		}

		$this->view->data_filter = $filterlistdata;
		// echo $select;die;
		if ($csv || $pdf || $this->_request->getParam('print')) {
			$arr = $this->_db->fetchAll($select);
			foreach ($select as $key => $value) {
				unset($select[$key]['ID']);
			}

			if ($csv) {
				$header_export = array($this->language->_('No'), $this->language->_('Company Code'), $this->language->_('Company Name'), $this->language->_('Model'), $this->language->_('Status'), $this->language->_('Last Suggested"'), $this->language->_('Last Approved"'));


				$no = 1;
				foreach ($arr as $pTrx) {
					$paramTrx = array(
						"NOMOR" => $no,
						"COMPANY_CODE" => $pTrx['CUST_ID'],
						"COMPANY_NAME" => $pTrx['CUST_NAME'],
						"MODEL" => $pTrx['CUST_MODEL'],
						//"KAB_CITY" => $pTrx['CUST_CITY'],
						"STATUS" => $pTrx['STATUS'],
						"LAST_SUGGESTED" => $pTrx['CUST_SUGGESTED'],
						"LAST_APPROVED" => $pTrx['CUST_UPDATED'],
					);
					$newData[] = $paramTrx;
					$no++;
				}
				//var_dump($newData);die;


				$this->_db->insert('T_BACTIVITY', array(
					'LOG_DATE'         => new Zend_Db_Expr('now()'),
					'USER_ID'          => $this->_userIdLogin,
					'USER_NAME'        => $this->_userNameLogin,
					'ACTION_DESC'      => 'RPCS',
					'ACTION_FULLDESC'  => 'Download CSV Customer List',
				));

				$this->_helper->download->csv($header_export, $newData, null, $this->language->_('Customer List Report'));

				// Application_Helper_General::writeLog('RPCS','Download CSV Customer List Report');
			} else if ($pdf) {
				$this->_helper->download->pdf($header, $arr, null, $this->language->_('Customer List Report'));
				Application_Helper_General::writeLog('RPCS', 'Download PDF Customer List Report');
			} else if ($this->_request->getParam('print') == 1) {
				$zf_filter = new Zend_Filter_Input($filters, $validators, $dataParamValue, $this->_optionsValidator);
				// $filter 	= $zf_filter->getEscaped('filter');
				$filter 		= $this->_getParam('filter');
				$filterlistdatax = $this->_request->getParam('data_filter');

				// var_dump($filterlistdatax);die;
				// if(count($filterlistdatax) > 1){
				// 	unset($filterlistdatax[0]);
				// }


				//var_dump($filterlistdatax);die;
				//echo $filterlistdatax;die;

				if ($filterlistdatax != "Filter - None") {
					foreach ($filterlistdatax as $key => $val) {
						//echo $val;
						$arr = explode("-", $val);
						$colmn = ltrim(rtrim($arr[0]));
						$value = ltrim(rtrim($arr[1]));

						$arrFilter[$colmn] = $value;


						//echo $colmn;die;
						//echo $this->_db->quote($value->toString($this->_dateDBFormat));die;
						/*if($colmn == 'COMP_ID') $select->where('UPPER(CUST_ID)='.$this->_db->quote(strtoupper($value)));
						if($colmn == 'COMP_NAME')            $select->where('UPPER(CUST_NAME) LIKE '.$this->_db->quote('%'.strtoupper($value).'%'));
						if($colmn == 'CITY')             $select->where('UPPER(CUST_CITY) LIKE '.$this->_db->quote('%'.strtoupper($value).'%'));
					
						if($colmn == 'COUNTRY_CODE')          $select->where('UPPER(COUNTRY_CODE)='.$this->_db->quote(strtoupper($value)));
						if($colmn == 'STATUS')           $select->where('CUST_STATUS=?',$value);
						if($colmn == 'LATEST_SUGGESTER')  $select->where('UPPER(CUST_SUGGESTEDBY) LIKE '.$this->_db->quote('%'.strtoupper($value).'%'));
						if($colmn == 'LATEST_APPROVER')   $select->where('UPPER(CUST_UPDATEDBY) LIKE '.$this->_db->quote('%'.strtoupper($value).'%'));
						
						//if (count($filterlistdatax) > 2){
							if($colmn == 'Last Suggestion Date')  
							$value   = (Zend_Date::isDate($value,$this->_dateDisplayFormat))?
							new Zend_Date($value,$this->_dateDisplayFormat):
							false;

							
								$select->where("DATE(CUST_SUGGESTED) >= DATE(".$this->_db->quote($value->toString($this->_dateDBFormat)).")");
							
							if($colmn == 'Last Suggestion Date_END')    
								$value     = (Zend_Date::isDate($value,$this->_dateDisplayFormat))?
								new Zend_Date($value,$this->_dateDisplayFormat):
								false;
								
								$select->where("DATE(CUST_SUGGESTED) <= DATE(".$this->_db->quote($value->toString($this->_dateDBFormat)).")");
						
						//}
						
					echo $select;die;
						//if($colmn == 'Last Approval Date')    $select->where("DATE(CUST_UPDATED) >= DATE(".$this->_db->quote($value->toString($this->_dateDBFormat)).")");
						//if($colmn == 'Last Approval Date_END')      $select->where("DATE(CUST_UPDATED) <= DATE(".$this->_db->quote($value->toString($this->_dateDBFormat)).")");
						//$data = $this->_db->fetchall($select);
						
						*/

						//
					}

					$cid       = html_entity_decode($arrFilter['COMP_CODE']);
					$cname     = html_entity_decode($arrFilter['COMP_NAME']);
					$model      = html_entity_decode($arrFilter['MODEL']);
					$city      = html_entity_decode($arrFilter['CITY']);
					$country   = html_entity_decode($arrFilter['COUNTRY_CODE']);
					$status    = html_entity_decode($arrFilter['STATUS']);
					$latestSuggestionFrom   = html_entity_decode($arrFilter['CUST_SUGGESTED']);
					$latestSuggestionTo     = html_entity_decode($arrFilter['CUST_SUGGESTED_end']);
					$latestApprovalFrom     = html_entity_decode($arrFilter['CUST_UPDATED']);
					$latestApprovalTo       = html_entity_decode($arrFilter['CUST_UPDATED_END']);

					//konversi date agar dapat dibandingkan
					$latestSuggestionFrom   = (Zend_Date::isDate($latestSuggestionFrom, $this->_dateDisplayFormat)) ?
						new Zend_Date($latestSuggestionFrom, $this->_dateDisplayFormat) :
						false;

					$latestSuggestionTo     = (Zend_Date::isDate($latestSuggestionTo, $this->_dateDisplayFormat)) ?
						new Zend_Date($latestSuggestionTo, $this->_dateDisplayFormat) :
						false;

					$latestApprovalFrom     = (Zend_Date::isDate($latestApprovalFrom, $this->_dateDisplayFormat)) ?
						new Zend_Date($latestApprovalFrom, $this->_dateDisplayFormat) :
						false;

					$latestApprovalTo       = (Zend_Date::isDate($latestApprovalTo, $this->_dateDisplayFormat)) ?
						new Zend_Date($latestApprovalTo, $this->_dateDisplayFormat) :
						false;

					//if($cid)            $select->where('UPPER(CUST_ID) LIKE '.$this->_db->quote('%'.strtoupper($cid).'%'));
					if ($cid)              $select->where('UPPER(CUST_ID)=' . $this->_db->quote(strtoupper($cid)));
					if ($cname)            $select->where('UPPER(CUST_NAME) LIKE ' . $this->_db->quote('%' . strtoupper($cname) . '%'));
					if ($city)             $select->where('UPPER(CUST_CITY) LIKE ' . $this->_db->quote('%' . strtoupper($city) . '%'));
					if ($model)             $select->where('UPPER(CUST_MODEL) LIKE ' . $this->_db->quote('%' . strtoupper($model) . '%'));
					//if($country)        $select->where('UPPER(COUNTRY_CODE) LIKE '.$this->_db->quote('%'.strtoupper($country).'%'));
					if ($country)          $select->where('UPPER(COUNTRY_CODE)=' . $this->_db->quote(strtoupper($country)));
					if ($status)           $select->where('CUST_STATUS=?', $status);
					if ($latestSuggestor)  $select->where('UPPER(CUST_SUGGESTEDBY) LIKE ' . $this->_db->quote('%' . strtoupper($latestSuggestor) . '%'));
					if ($latestApprover)   $select->where('UPPER(CUST_UPDATEDBY) LIKE ' . $this->_db->quote('%' . strtoupper($latestApprover) . '%'));

					if ($latestSuggestionFrom)  $select->where("DATE(CUST_SUGGESTED) >= DATE(" . $this->_db->quote($latestSuggestionFrom->toString($this->_dateDBFormat)) . ")");
					if ($latestSuggestionTo)    $select->where("DATE(CUST_SUGGESTED) <= DATE(" . $this->_db->quote($latestSuggestionTo->toString($this->_dateDBFormat)) . ")");
					if ($latestApprovalFrom)    $select->where("DATE(CUST_UPDATED) >= DATE(" . $this->_db->quote($latestApprovalFrom->toString($this->_dateDBFormat)) . ")");
					if ($latestApprovalTo)      $select->where("DATE(CUST_UPDATED) <= DATE(" . $this->_db->quote($latestApprovalTo->toString($this->_dateDBFormat)) . ")");
				}

				$data = $this->_db->fetchall($select);

				$field_export = array(
					'nomor'  => array(
						'field' => 'NOMOR',
						'label' => 'No'
					),
					'company_code'  => array(
						'field' => 'COMPANY_CODE',
						'label' => 'Company Code'
					),
					'company_namw'  => array(
						'field' => 'COMPANY_NAME',
						'label' => 'Company Name'
					),
					'model'  => array(
						'field' => 'MODEL',
						'label' => 'Model'
					),
					'status'     => array(
						'field' => 'STATUS',
						'label' => 'Status'
					),
					'last_suggested'     => array(
						'field' => 'LAST_SUGGESTED',
						'label' => 'Last Suggested'
					),
					'last_approved'     => array(
						'field' => 'LAST_APPROVED',
						'label' => 'Last Approved'
					)
				);
				$no = 1;
				foreach ($data as $pTrx) {
					$paramTrx = array(
						"NOMOR" => $no,
						"COMPANY_CODE" => $pTrx['CUST_ID'],
						"COMPANY_NAME" => $pTrx['CUST_NAME'],
						"MODEL" => $pTrx['CUST_MODEL'],
						//"KAB_CITY" => $pTrx['CUST_CITY'],
						"STATUS" => $pTrx['STATUS'],
						"LAST_SUGGESTED" => $pTrx['CUST_SUGGESTED'],
						"LAST_APPROVED" => $pTrx['CUST_UPDATED'],
					);
					$newData[] = $paramTrx;
					$no++;
				}

				$this->_forward('print', 'index', 'widget', array('data_content' => $newData, 'data_caption' => 'Customer List Report', 'data_header' => $field_export, 'data_filter' => $filterlistdatax));


				// $this->_forward('print', 'index', 'widget', array('data_content' => $arr, 'data_caption' => 'Customer List Report', 'data_header' => $headers,'data_filter' => $filterlistdatax));
			}
		}

		// echo '<pre>';
		// print_r($select);
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		$this->view->modulename = $this->_request->getModuleName();
		Application_Helper_General::writeLog('RPCS', 'View Customer List Report');
		//var_dump($dataParamValue);
		
		unset($dataParamValue['LAST_SUGGESTED_END']);
    	unset($dataParamValue['LAST_APPROVED_END']);
    	if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     
      }
	}

	public function statusAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$tblName = $this->_getParam('id');

		$customerstatus = $this->_customerstatus;

		$options = array_combine(array_values($customerstatus['code']), array_values($customerstatus['desc']));

		unset($options[0]);
		unset($options[4]);
		unset($options[5]);
		foreach ($options as $key => $value) {
			// print_r($key);die;\
			if (!empty($tblName)) {
				if ($tblName == $key) {
					$select = 'selected';
				} else {
					$select = '';
				}
			}
			$optHtml .= "<option value='" . $key . "' " . $select . ">" . $value . "</option>";
		}

		echo $optHtml;
	}
}
