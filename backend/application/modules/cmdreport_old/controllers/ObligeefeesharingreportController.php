<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class cmdreport_ObligeefeesharingreportController extends Application_Main
{

  protected $_moduleDB = 'RTF'; // masih harus diganti

  public function indexAction()
  {
    $this->_helper->_layout->setLayout('newlayout');

    $settings = new Settings();
    $system_type = $settings->getSetting('system_type');
    $this->view->systemType = $system_type;

    $conf = Zend_Registry::get('config');
    $this->_bankName = $conf['app']['bankname'];
    $this->view->masterbankname = $this->_bankName;

    $this->CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

    $fields = array(

      'obligee'        => array(
        'field' => 'MC.CUST_NAME',
        'label' => $this->language->_('Obligee'),
        'sortable' => true
      ),
      'applicant'        => array(
        'field' => 'MC.CUST_ID',
        'label' => $this->language->_('Applicant'),
        'sortable' => true
      ),
      'refcode'            => array(
        'field' => 'T.RECIPIENT_NAME',
        'label' => $this->language->_('BG Number') . ' / ' . $this->language->_('Subject'),
        'sortable' => true
      ),

      'bankbranch'            => array(
        'field' => 'MC.BRANCH_NAME',
        'label' => $this->language->_('Bank Branch'),
        'sortable' => true
      ),
      'bgamount'            => array(
        'field' => 'BG_AMOUNT',
        'label' => $this->language->_('BG Amount'),
        'sortable' => true
      ),
      'issueddate'  => array(
        'field' => 'BG_ISSUED',
        'label' => $this->language->_('Issued Date'),
        'sortable' => true
      ),
      'provisionchange'  => array(
        'field' => 'PROVISION_FEE',
        'label' => $this->language->_('Provision Charge'),
        'sortable' => true
      ),
      'feesharing'  => array(
        'field' => 'freeSharing',
        'label' => $this->language->_('Fee Sharing'),
        'sortable' => true
      ),
    );

    $filterlist = array(
      "CUST_NAME" => "Aplicant Name",
      // "CUST_ID" => "Aplicant Code",
      "RECIPIENT_NAME" => "Obligee Name",
      // "SP_OBLIGEE_CODE" => "Obligee Code",
      // "BG_REFCODE" => "REFCODE",
      "BG_NUMBER" => "BG Number",
      // "BG_SUBJECT" => "BG Subject",
      "BRANCH_NAME" => "Branch Name",
      // "COUNTER_WARRANTY_TYPE" => "Counter Type",
      // "Provider_Name" => "Provider Name",
      "BG_ISSUED" => "BG Issued"
    );

    //add reza

    // echo "<pre>";
    // print_r($filterlist);die;


    $this->view->filterlist = $filterlist;

    $page    = $this->_getParam('page');

    $sortBy  = ($this->_getParam('sortby')) ?  $this->_getParam('sortby') : ('BG_UPDATED');
    $sortBy  = (Zend_Validate::is($sortBy, 'InArray', array(array_keys($fields)))) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

    $sortDir = $this->_getParam('sortdir');
    $sortDir = (Zend_Validate::is($sortDir, 'InArray', array('haystack' => array('asc', 'desc')))) ? $sortDir : 'desc';

    $this->view->currentPage = $page;
    $this->view->sortBy      = $sortBy;
    $this->view->sortDir     = $sortDir;

    //end reza

    $wherein = [15, 16];
    $select = $this->_db->select()
      ->from(array('T' => 'T_BANK_GUARANTEE'))
      ->joinleft(array('MC' => 'M_CUSTOMER'), 'MC.CUST_ID=T.CUST_ID', array('CUST_NAME', 'CUSTID' => 'CUST_ID'))
      ->joinleft(array('MB' => 'M_BRANCH'), 'T.BG_BRANCH=MB.BRANCH_CODE')
      ->joinleft(
        array('CS' => 'M_CUST_SPOBLIGEE'),
        'T.SP_OBLIGEE_CODE=CS.CUST_ID',
        array(
          'freeSharing'    => new Zend_Db_Expr("(T.PROVISION_FEE/100)*CS.SHARING_FEE")
        )
      )
      ->where('T.BG_STATUS IN (?)', $wherein)
      ->where('T.SP_OBLIGEE_CODE IS NOT NULL')
      ->where('T.SP_OBLIGEE_CODE !=""');

    $conf = Zend_Registry::get('config');


    $this->view->bankname = $conf['app']['bankname'];


    $config     = Zend_Registry::get('config');
    $BgType     = $config["bg"]["status"]["desc"];
    $BgCode     = $config["bg"]["status"]["code"];

    $arrStatus = array_combine(array_values($BgCode), array_values($BgType));

    $this->view->arrStatus = $arrStatus;

    $arrWaranty = array(
      1 => 'FC',
      2 => 'LF',
      3 => 'Insurance'
    );
    $this->view->arrWaranty = $arrWaranty;

    $arrType = array(
      1 => 'Standart',
      2 => 'Custom'
    );

    $arrLang = array(
      1 => 'Indonesian',
      2 => 'English',
      3 => 'Bilingual'
    );
    $this->view->langArr = $arrLang;
    $this->view->formatArr = $arrType;
    $this->view->fields = $fields;


    $filterArr = array(
      'filter' => array('StripTags', 'StringTrim'),
      'CUST_NAME'    => array('StripTags', 'StringTrim', 'StringToUpper'),
      'CUST_ID'  => array('StripTags', 'StringTrim', 'StringToUpper'),
      'RECIPIENT_NAME'    => array('StripTags', 'StringTrim', 'StringToUpper'),
      'SP_OBLIGEE_CODE'  => array('StripTags', 'StringTrim', 'StringToUpper'),
      'BG_REFCODE'  => array('StripTags', 'StringTrim', 'StringToUpper'),
      'BG_NUMBER'  => array('StripTags', 'StringTrim', 'StringToUpper'),
      'BG_SUBJECT'  => array('StripTags', 'StringTrim', 'StringToUpper'),
      'BRANCH_NAME'  => array('StripTags', 'StringTrim', 'StringToUpper'),
      'COUNTER_WARRANTY_TYPE' => array('StripTags', 'StringToUpper'),
      'Provider_Name' => array('StripTags', 'Alnum', 'StringToUpper')
    );

    $dataParam = array("CUST_NAME", "CUST_ID", "RECIPIENT_NAME", "SP_OBLIGEE_CODE", "BG_REFCODE", "BG_NUMBER", "BG_SUBJECT", "COUNTER_WARRANTY_TYPE", "Provider_Name", "BG_ISSUED", "BRANCH_NAME");
    $dataParamValue = array();

    foreach ($dataParam as $dtParam) {
      if (!empty($this->_request->getParam('wherecol'))) {
        $dataval = $this->_request->getParam('whereval');
        foreach ($this->_request->getParam('wherecol') as $key => $value) {
          if ($dtParam == $value) {
            $dataParamValue[$dtParam] = $dataval[$key];
          }
        }
      }
    }

    if (!empty($this->_request->getParam('efdate'))) {
      $efdatearr = $this->_request->getParam('efdate');
      $dataParamValue['BG_ISSUED'] = $efdatearr[0];
      // $dataParamValue['BG_CLAIM_DATE'] = $efdatearr[1];
    }

    $validator = array(
      'CUST_NAME'    => array(),
      'CUST_ID'  => array(),
      'RECIPIENT_NAME'    => array(),
      'SP_OBLIGEE_CODE'  => array(),
      'BG_REFCODE'  => array(),
      'BG_NUMBER'  => array(),
      'BG_SUBJECT'  => array(),
      'BRANCH_NAME'  => array(),
      'COUNTER_WARRANTY_TYPE' => array(),
      'Provider_Name' => array(),
      'BG_ISSUED'    => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
      'BG_ISSUED_END'    => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
    );



    // echo "<pre>";
    // print_r($dataParamValue);die;


    $zf_filter   = new Zend_Filter_Input($filterArr, $validator, $dataParamValue);

    if ($zf_filter->isValid()) {
      $filter     = TRUE;
    }

    // var_dump($zf_filter->getEscaped('BG_ISSUED'), $zf_filter->getEscaped('BG_ISSUED_END'));
    // die();

    $applicantname  = html_entity_decode($zf_filter->getEscaped('CUST_NAME'));
    $applicantcode  = html_entity_decode($zf_filter->getEscaped('CUST_ID'));
    $recipientname  = html_entity_decode($zf_filter->getEscaped('RECIPIENT_NAME'));
    $recipientcode  = html_entity_decode($zf_filter->getEscaped('SP_OBLIGEE_CODE'));
    $refcode  = html_entity_decode($zf_filter->getEscaped('BG_REFCODE'));
    $bgnumbers  = html_entity_decode($zf_filter->getEscaped('BG_NUMBER'));
    $bgsubject  = html_entity_decode($zf_filter->getEscaped('BG_SUBJECT'));
    $branchname  = html_entity_decode($zf_filter->getEscaped('BRANCH_NAME'));
    $counterwar  = html_entity_decode($zf_filter->getEscaped('COUNTER_WARRANTY_TYPE'));
    $providername  = html_entity_decode($zf_filter->getEscaped('Provider_Name'));

    // $datefrom    = html_entity_decode($zf_filter->getEscaped('BG_ISSUED'));
    // $dateto    = html_entity_decode($zf_filter->getEscaped('BG_ISSUED_END'));
    $datefrom    = $efdatearr[0];
    $dateto    = $efdatearr[1];


    if ($filter == null) {
      $datefrom = (date("d/m/Y"));
      $dateto = (date("d/m/Y"));
      $this->view->fDateFrom  = (date("d/m/Y"));
      $this->view->fDateTo  = (date("d/m/Y"));
    }

    if ($filter_clear == '1') {
      $this->view->fDateFrom  = '';
      $this->view->fDateTo  = '';
      $datefrom = '';
      $dateto = '';
    }

    if ($filter == null || $filter == TRUE) {
      $this->view->fDateFrom = $datefrom;
      $this->view->fDateTo = $dateto;
      if (!empty($datefrom)) {
        $FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
        $datefrom  = $FormatDate->toString($this->_dateDBFormat);
      }

      if (!empty($dateto)) {
        $FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
        $dateto    = $FormatDate->toString($this->_dateDBFormat);
      }

      if (!empty($datefrom) && empty($dateto))
        $select->where("T.BG_ISSUED >= " . $this->_db->quote($datefrom));

      if (empty($datefrom) && !empty($dateto))
        $select->where("T.BG_CLAIM_DATE <= " . $this->_db->quote($dateto));

      if (!empty($datefrom) && !empty($dateto)) {
        // var_dump("test");
        // die();
        // $select->where("T.BG_ISSUED >= " . $this->_db->quote($datefrom) . " and T.BG_CLAIM_DATE <= " . $this->_db->quote($dateto));
        $select->where("T.BG_ISSUED >= " . $this->_db->quote($datefrom) . " and T.BG_ISSUED <= " . $this->_db->quote($dateto));
      }
    }


    if ($filter == TRUE) {
      //   echo"<pre>";
      // print_r($bgNubmer);die;


      if ($applicantname != null) {
        $this->view->applicantname = $applicantname;
        $select->where("MC.CUST_NAME LIKE " . $this->_db->quote('%' . $applicantname . '%'));

        // echo"<pre>";
        // print_r($select);die;


      }

      if ($applicantcode != null) {
        $this->view->applicantcode = $applicantcode;
        $select->where("T.CUST_ID LIKE " . $this->_db->quote('%' . $applicantcode . '%'));
        // echo"<pre>";
        // print_r($select);die;
      }

      if ($recipientname != null) {
        $this->view->recipientname = $recipientname;
        $select->where("T.RECIPIENT_NAME LIKE " . $this->_db->quote('%' . $recipientname . '%'));

        // echo"<pre>";
        // print_r($select);die;


      }

      if ($recipientcode != null) {
        $this->view->recipientcode = $recipientcode;
        $select->where("T.SP_OBLIGEE_CODE LIKE " . $this->_db->quote('%' . $recipientcode . '%'));
        // echo"<pre>";
        // print_r($select);die;
      }

      if ($refcode != null) {
        $this->view->refcode = $refcode;
        $select->where("T.BG_REFCODE LIKE " . $this->_db->quote('%' . $refcode . '%'));

        // echo"<pre>";
        // print_r($select);die;


      }

      if ($bgnumbers != null) {
        $this->view->bgnumbers = $bgnumbers;
        $select->where("T.BG_NUMBER LIKE " . $this->_db->quote('%' . $bgnumbers . '%'));
        // echo"<pre>";
        // print_r($select);die;
      }

      if ($bgsubject != null) {
        $this->view->bgsubject = $bgsubject;
        $select->where("T.BG_SUBJECT LIKE " . $this->_db->quote('%' . $bgsubject . '%'));

        // echo"<pre>";
        // print_r($select);die;


      }

      if ($branchname != null) {
        $this->view->branchname = $branchname;
        // $select->where("MB.BRANCH_NAME LIKE " . $this->_db->quote('%' . $branchname . '%'));
        $select->where("MB.ID LIKE " . $this->_db->quote('%' . $branchname . '%'));
        // echo"<pre>";
        // print_r($select);die;
      }

      if ($counterwar != null) {
        $this->view->applicantname = $applicantname;
        $select->where("T.COUNTER_WARRANTY_TYPE LIKE " . $this->_db->quote('%' . $counterwar . '%'));

        // echo"<pre>";
        // print_r($select);die;


      }

      if ($providername != null) {
        $this->view->applicantcode = $applicantcode;
        $select->where("T.CUST_ID LIKE " . $this->_db->quote('%' . $providername . '%'));
        // echo"<pre>";
        // print_r($select);die;
      }
    }

    //$select->order($sortBy.' '.$sortDir);

    $csv = $this->_request->getParam('csv');

    if ($csv || $pdf || $this->_request->getParam('print')) {
      $result = $select->query()->FetchAll();

      foreach ($result as $key => $value) {
        $result[$key]["USER_CREATED"] = Application_Helper_General::convertDate($value["USER_CREATED"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
        $result[$key]["USER_SUGGESTED"] = Application_Helper_General::convertDate($value["USER_SUGGESTED"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
        $result[$key]["USER_UPDATED"] = Application_Helper_General::convertDate($value["USER_UPDATED"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
      }

      $header = Application_Helper_Array::simpleArray($fields, 'label');

      if ($csv) {
        // Application_Helper_General::writeLog('RPFS','Download CSV Obligee Fee Sharing Report');

        $fields = array(
          'regno'     => array(
            'field'    => 'RECIPIENT_NAME',
            'label'    => $this->language->_('Obligee'),
          ),
          'principal'     => array(
            'field'    => 'CUST_NAME',
            'label'    => $this->language->_('Applicant'),
          ),
          'obligee'     => array(
            'field'    => 'BG_NUMBER',
            'label'    => $this->language->_('BG Number / Subject'),
          ),
          'branch'     => array(
            'field'    => 'BRANCH_NAME',
            'label'    => $this->language->_('Bank Branch'),
          ),
          'countertype'     => array(
            'field'    => 'BG_AMOUNT',
            'label'    => $this->language->_('BG Amount'),
          ),
          'status'     => array(
            'field'    => 'BG_ISSUED',
            'label'    => $this->language->_('BG Amount'),
          ),
          'status'     => array(
            'field'    => 'PROVISION_FEE',
            'label'    => $this->language->_('Provision Charge'),
          ),
          'status'     => array(
            'field'    => 'FREESHARING',
            'label'    => $this->language->_('Fee Sharing'),
          )
        );

        $tempData = [];

        foreach ($result as $row) {

          $subData = [];
          $subData['RECIPIENT_NAME'] = $row['RECIPIENT_NAME'] . ' (' . $row['SP_OBLIGEE_CODE'] . ')';
          $subData['CUST_NAME'] = $row['CUST_NAME'] . ' (' . $row['CUST_ID'] . ')';
          $subData['BG_NUMBER'] = $row['BG_NUMBER'] . ' / ' . $row['BG_SUBJECT'];
          $subData['BRANCH_NAME'] = $row['BRANCH_NAME'];
          $subData['BG_AMOUNT'] = Application_Helper_General::displayMoneyplain($row['BG_AMOUNT']);
          $subData['BG_ISSUED'] = Application_Helper_General::convertDate($row['BG_ISSUED'], $this->viewDateFormat, $this->defaultDateFormat);
          $subData['PROVISION_FEE'] = 'IDR ' . Application_Helper_General::displayMoneyplain($row['PROVISION_FEE']);
          $subData['FREESHARING'] = 'IDR ' . Application_Helper_General::displayMoneyplain($row['freeSharing']);

          $tempData[] = $subData;
        }

        $this->_db->insert('T_BACTIVITY', array(
          'LOG_DATE'         => new Zend_Db_Expr('now()'),
          'USER_ID'          => $this->_userIdLogin,
          'USER_NAME'        => $this->_userNameLogin,
          'ACTION_DESC'      => 'RPFS',
          'ACTION_FULLDESC'  => 'Download CSV Obligee Fee Sharing Report',
        ));

        $this->_helper->download->csv($header, $tempData, null, $this->language->_('Obligee Fee Sharing Report'));

        // $this->_helper->download->csv($header,$result,null,'Obligee Fee Sharing Report');
      }

      if ($pdf) {
        Application_Helper_General::writeLog('RPFS', 'Download PDF Obligee Fee Sharing Report');
        $this->_helper->download->pdf($header, $result, null, 'Customer User List');
      }
      if ($this->_request->getParam('print') == 1) {

        $this->_forward('print', 'index', 'widget', array('data_content' => $result, 'data_caption' => 'Obligee Fee Sharing Report', 'data_header' => $fields));
      }
    } else {
      Application_Helper_General::writeLog('RPFS', 'View Obligee Fee Sharing Report');
    }


    $this->view->fields = $fields;
    $this->view->filter = $filter;

    // $select = $this->_db->fetchAll($select);
    // $selectlc = $this->_db->fetchAll($selectlc);
    // $result = array_merge($select, $selectlc);

    $setting = new Settings();
    $enc_pass = $setting->getSetting('enc_pass');
    $enc_salt = $setting->getSetting('enc_salt');
    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');
    $pw_hash = md5($enc_salt . $enc_pass);
    $rand = $this->_userIdLogin . date('dHis') . $pw_hash;
    $sessionNamespace->token   = $rand;
    $this->view->token = $sessionNamespace->token;

    foreach ($result as $key => $value) {
      $get_reg_number = $value["REG_NUMBER"];

      $AESMYSQL = new Crypt_AESMYSQL();
      $rand = $this->token;

      $encrypted_payreff = $AESMYSQL->encrypt($get_reg_number, $rand);
      $encpayreff = urlencode($encrypted_payreff);

      $result[$key]["REG_NUMBER_ENCRYPTED"] = $encpayreff;
    }

    $this->paging($select);

    if (!empty($dataParamValue)) {

      $this->view->efdateStart = $dataParamValue['BG_ISSUED'];
      // $this->view->efdateEnd = $dataParamValue['BG_CLAIM_DATE'];
      $this->view->efdateEnd = $efdatearr[1];

      foreach ($dataParamValue as $key => $value) {
        $duparr = explode(',', $value);
        if (!empty($duparr)) {

          foreach ($duparr as $ss => $vs) {
            $wherecol[]  = $key;
            $whereval[] = $vs;
          }
        } else {
          $wherecol[]  = $key;
          $whereval[] = $value;
        }
      }
      $this->view->wherecol     = $wherecol;
      $this->view->whereval     = $whereval;
    }
  }
}
