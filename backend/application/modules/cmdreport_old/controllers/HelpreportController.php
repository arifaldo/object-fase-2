<?php


require_once 'Zend/Controller/Action.php';


class cmdreport_HelpreportController extends Application_Main
{
	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
		
		/*$listId = $this->_db->select()
						->from(array('M_BUSER'),
							   array('BUSER_ID',
									 'BUSER_NAME',
									 'UPLOADED_BY'=>new zend_db_expr('BUSER_ID + BUSER_NAME')))
						->order('BUSER_NAME ASC')
						->query()->fetchAll();
		
       	$list = array(""=>"--- Please Select ---");
		$list += Application_Helper_Array::listArray($listId,'BUSER_ID','UPLOADED_BY');
		$this->view->listCustId = $list;*/
		
		$now = Zend_Date::now()->toString("dd/MM/yyyy");
		
		$arr = null;
		$viewFilter = null;
		
		$fields = array	(
							'HelpTopic'  			=> array	(
																	'field' => 'HELP_TOPIC',
																	'label' => $this->language->_('Help Topic'),
																	'sortable' => true
																),
							'FileDescription'  					=> array	(
																		'field' => 'HELP_DESCRIPTION',
																		'label' => $this->language->_('Help Description'),
																		'sortable' => true
																	),
							'FileName'  			=> array	(
																	'field' => 'HELP_FILENAME',
																	'label' => $this->language->_('File Name'),
																	'sortable' => true
																),
							'Uploaded By'  					=> array	(
																		'field' => 'UPLOADED_BY',
																		'label' => $this->language->_('Uploaded By'),
																		'sortable' => true
																	),	
							'Upload Date and Time'  					=> array	(
																		'field' => 'UPLOAD_DATETIME',
																		'label' => $this->language->_('Uploaded Date'),
																		'sortable' => true
																	),	
							'Last Edited By'  					=> array	(
																		'field' => 'HELP_EDITEDBY',
																		'label' => $this->language->_('Updated By'),
																		'sortable' => true
																	),	
							'Edit Date and Time'  					=> array	(
																		'field' => 'EDIT_DATETIME',
																		'label' => $this->language->_('Updated Date'),
																		'sortable' => true
																	),		
							'File Is Deleted'  					=> array	(
																		'field' => 'HELP_ISDELETED',
																		'label' => $this->language->_('File is Deleted'),
																		'sortable' => FALSE
																	),
						);

		$this->view->fields = $fields;

		$filterlist = array('HELP_TOPIC','HELP_DESCRIPTION','UPLOADED_BY','PS_UPLOADED','DELETED');
		
		$this->view->filterlist = $filterlist;
		
		$filterArr = array(	'filter' 	  	=> array('StringTrim','StripTags'),
							'SEARCH_TEXT'  	=> array('StringTrim','StripTags','StringToUpper'),
							'HELP_TOPIC'   	=> array('StringTrim','StripTags','StringToUpper'),
							'HELP_DESCRIPTION' => array('StringTrim','StripTags'),
							'UPLOADED_BY' 	=> array('StringTrim','StripTags'),
							'PS_UPLOADED' 	=> array('StringTrim','StripTags'),
							'PS_UPLOADED_END' 	=> array('StringTrim','StripTags'),
							'delete' 	  	=> array('StringTrim','StripTags'),
		);

		$validator = array('filter'			 	=> array(),
	                       'SEARCH_TEXT'  	=> array(),
							'HELP_TOPIC'   	=> array(),
							'HELP_DESCRIPTION' => array(),
							'UPLOADED_BY' 	=> array(),
							'PS_UPLOADED' 	=> array(),
							'PS_UPLOADED_END' 	=> array(),
							'delete' 	  	=> array(),
	                      );
		
		$page = $this->_getParam('page');
		$page = (Zend_Validate::is($page,'Digits'))? $page : 1;
		$sortBy  = $this->_getParam('sortby');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
		
		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		$dataParam = array('SEARCH_TEXT','HELP_TOPIC','HELP_DESCRIPTION','UPLOADED_BY');
		$dataParamValue = array();
		
		$clean2 = array_diff( $this->_request->getParam('wherecol'),$dataParam); 
		$dataParam = array_diff( $this->_request->getParam('wherecol'),$clean2); 
		// print_r($dataParam);die;

		// print_r($output);die;
		// print_r($this->_request->getParam('wherecol'));
		foreach ($dataParam as $no => $dtParam)
		{
		
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
				// print_r($dataval);
				$order = 0;
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($value == "PS_ACTIVITY"){
								$order--;
							}
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$order];
						}
						$order++;
					}
				
			}
		}
		$zf_filter  	= new Zend_Filter_Input($filterArr,$validator, $dataParamValue);
		// print_r($zf_filter);die;
		$custid = html_entity_decode($zf_filter->getEscaped('HELP_TOPIC'));
		// print_r($custid);die;
		$delete 	= $this->_request->getParam('delete');
		if($delete)
		{
			$postreq_id	= $this->_request->getParam('req_id');
			foreach ($postreq_id as  $key =>$value) 
			{		
				$HELP_ID_DELETE =  $postreq_id[$key];
				
				try
				{
					$this->_db->beginTransaction();
					$param = array('HELP_ISDELETED'=> '1',);	
					
					$where = array('HELP_ID = ?' => $HELP_ID_DELETE);
					$query = $this->_db->update ( "T_HELP", $param, $where );
					$this->_db->commit();
					$this->_redirect('/notification/success/index');
				}
				catch(Exception $e)
				{
					$this->_db->rollBack();
				}
			}
		}
		
		$filter 	= $this->_getParam('filter');
		$filter_clear 	= $this->_request->getParam('clearfilter');
		
		
		$pdf   		= $this->_getParam('pdf');
		$csv   		= $this->_getParam('csv');
		
		$select = $this->_db->select()
								->from(	array('TH' => 'T_HELP'),
										array(
												'HELP_ID'=>'TH.HELP_ID',
												'HELP_TOPIC'=>'TH.HELP_TOPIC',
												'HELP_FILENAME'=>'TH.HELP_FILENAME',
												'UPLOAD_DATETIME'=>'TH.UPLOAD_DATETIME', 
												'HELP_DESCRIPTION'=>'TH.HELP_DESCRIPTION', 
												'EDIT_DATETIME'=>'TH.EDIT_DATETIME',
												'HELP_ISDELETED'=>'TH.HELP_ISDELETED',
												'HELP_SYS_FILENAME'=>'TH.HELP_SYS_FILENAME',
												'UPLOADED_BY' => 'TH.UPLOADED_BY',
												'HELP_EDITEDBY' => 'TH.HELP_EDITEDBY',
												));
								// ->joinLeft	(
												// array('MB' => 'M_BUSER'),
												// 'MB.BUSER_ID = TH.UPLOADED_BY',
												// array('UPLOADED_BY'=>'MB.BUSER_NAME')
											// )
								// ->joinLeft	(
												// array('MB2' => 'M_BUSER'),
												// 'MB2.BUSER_ID = TH.HELP_EDITEDBY',
												// array('HELP_EDITEDBY'=>'MB2.BUSER_NAME')
											// );
											
			
		// if( $filter == null || !empty($pdf) || !empty($csv))
		// {
		// 	$DATE_START = $now;
		// 	$DATE_END = $now;
		// }
		
		// if($filter_clear == TRUE)
		// {
		// 	$DATE_START = null;
		// 	$DATE_END = null;
		// }
		
		
		$HELP_ID   	= $this->_getParam('HELP_ID');
		
		if($HELP_ID)
		{
			$select->where('HELP_ID =?',$HELP_ID);
			$data = $this->_db->fetchRow($select);
			$attahmentDestination = UPLOAD_PATH . '/document/help/';
			$this->_helper->download->file($data['HELP_FILENAME'],$attahmentDestination.$data['HELP_SYS_FILENAME']);
		}
		
		if($filter == TRUE)
		{
						$DATE_START = null;
			$DATE_END = null;
			
			if($this->_getParam('PS_UPLOADED'))
			{
				$DATE_START    	= $this->_getParam('PS_UPLOADED');
			}
			if($this->_getParam('PS_UPLOADED_END'))
			{
				$DATE_END		= $this->_getParam('PS_UPLOADED_END');
			}
			$SEARCH_TEXT   	= $zf_filter->getEscaped('SEARCH_TEXT');
			$HELP_TOPIC   	= $zf_filter->getEscaped('HELP_TOPIC');
			$UPLOADED_BY    = $zf_filter->getEscaped('UPLOADED_BY');
			$HELP_ISDELETED	= $zf_filter->getEscaped('DELETED');
			$description	= $zf_filter->getEscaped('HELP_DESCRIPTION');
			
			if($description)
			{
				$select->where("UPPER(TH.HELP_DESCRIPTION) LIKE ".$this->_db->quote('%'.$description.'%'));
				$this->view->description = $description;
			}
			
			if($HELP_ISDELETED != null)
			{
				$select->where("TH.HELP_ISDELETED = ?", $HELP_ISDELETED);
				$this->view->HELP_ISDELETED = $HELP_ISDELETED;
			}
			
			if($SEARCH_TEXT)
			{
				$select->where("UPPER(TH.HELP_FILENAME) LIKE ".$this->_db->quote('%'.$SEARCH_TEXT.'%'));
				$this->view->SEARCH_TEXT = $SEARCH_TEXT;
			}
			
			if($HELP_TOPIC)
			{
				$select->where("UPPER(TH.HELP_TOPIC) LIKE ".$this->_db->quote('%'.$HELP_TOPIC.'%'));
				$this->view->HELP_TOPIC = $HELP_TOPIC;
			}
			
			if($UPLOADED_BY)
			{
				$select->where("UPPER(TH.UPLOADED_BY) LIKE ".$this->_db->quote('%'.$UPLOADED_BY.'%'));
				$this->view->UPLOADED_BY = $UPLOADED_BY;
			}
			
//			if($DATE_START)
//			{
//				$select->where('DATE(TH.UPLOAD_DATETIME) >= '.$this->_db->quote($DATE_START));
//			}
//			
//			if($DATE_END)
//			{
//				$select->where('DATE(TH.UPLOAD_DATETIME) <= '.$this->_db->quote($DATE_END));
//			}
			
			
		}

		// if($filter == 'Set Filter')
		// {
			
			if(!empty($DATE_START))
			{
				$this->view->DATE_START = $DATE_START;		
				$FormatDate = new Zend_Date($DATE_START, $this->_dateDisplayFormat);
				$DATE_START  = $FormatDate->toString($this->_dateDBFormat);
			}
			
			if(!empty($DATE_END))
			{
				$this->view->DATE_END = $DATE_END;				
				$FormatDate = new Zend_Date($DATE_END, $this->_dateDisplayFormat);
				$DATE_END  = $FormatDate->toString($this->_dateDBFormat);
			}
//			if(isset($DATE_START))
//			{
//				$select->where('DATE(TH.UPLOAD_DATETIME) >= '.$this->_db->quote($DATE_START));
//			}
//			
//			if(isset($DATE_END))
//			{
//				$select->where('DATE(TH.UPLOAD_DATETIME) <= '.$this->_db->quote($DATE_END));
//			}

			if(!empty($DATE_START) && empty($DATE_END))
		            $select->where("DATE(TH.UPLOAD_DATETIME) >= ".$this->_db->quote($DATE_START));
//		            
		   	if(empty($DATE_START) && !empty($DATE_END))
		            $select->where("DATE(TH.UPLOAD_DATETIME) <= ".$this->_db->quote($DATE_END));
//		            
		    if(!empty($DATE_START) && !empty($DATE_END))
		            $select->where("DATE(TH.UPLOAD_DATETIME) between ".$this->_db->quote($DATE_START)." and ".$this->_db->quote($DATE_END));
			
		// }

		$select->order($sortBy.' '.$sortDir);
		// echo $select;die;
		$arr = $this->_db->fetchAll($select);
		
		$this->paging($arr);
		
		$this->view->viewFilter = $viewFilter;
		$data = array();
		if($pdf || $csv || $this->_request->getParam('print'))
		{
			$header  = Application_Helper_Array::simpleArray($fields, "label");
			
			$no = 0;
			foreach($arr as $row)
			{
				$status = "Yes";
				if($row['HELP_ISDELETED']==0)
				{
					$status="No";
				}
				
				$data[$no]['HELP_TOPIC'] 		= $row['HELP_TOPIC'];
				$data[$no]['HELP_DESCRIPTION'] 	= $row['HELP_DESCRIPTION'];
				$data[$no]['HELP_FILENAME'] 	= $row['HELP_FILENAME'];
				$data[$no]['UPLOADED_BY'] 		= $row['UPLOADED_BY'];
				$data[$no]['UPLOAD_DATETIME'] 	= Application_Helper_General::convertDate($row['UPLOAD_DATETIME'],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
				$data[$no]['HELP_EDITEDBY'] 	= $row['HELP_EDITEDBY'];
				$data[$no]['EDIT_DATETIME'] 	= Application_Helper_General::convertDate($row['EDIT_DATETIME'],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
				$data[$no]['HELP_ISDELETED'] 	= $status;
				
				$no++;
			}
		}
		
		if($csv)
		{
			$this->_helper->download->csv($header,$data,null,$this->language->_('Help Report'));   
			Application_Helper_General::writeLog('RPHP','Download CSV Help Report'); 
		}
		else if($pdf)
		{
			$this->_helper->download->pdf($header,$data,null,$this->language->_('Help Report'));  
			Application_Helper_General::writeLog('RPHP','Download PDF Help Report');
		}
		else if($this->_request->getParam('print') == 1){
			$this->_forward('print', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'Help Report', 'data_header' => $fields));
			Application_Helper_General::writeLog('RPHP','Print Help Report');
		}
		else
		{
			Application_Helper_General::writeLog('RPHP','View Help Report');
		}

		if(!empty($dataParamValue)){
	    		$this->view->createStart = $dataParamValue['PS_UPLOADED'];
	    		$this->view->createEnd = $dataParamValue['PS_UPLOADED_END'];

	    	  	unset($dataParamValue['PS_UPLOADED_END']);
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
	        $this->view->wherecol     = $wherecol;
	        $this->view->whereval     = $whereval;
	     // print_r($whereval);die;
	      }
		
		
	}	
}
