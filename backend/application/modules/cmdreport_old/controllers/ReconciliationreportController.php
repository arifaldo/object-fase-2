<?php
require_once 'Zend/Controller/Action.php';

class cmdreport_ReconciliationreportController extends Application_Main
{	
	protected $_moduleDB = 'RTF';
	
	public function indexAction() 
	{
		
		$monthlist = array(
						'1'		=>	'January',
						'2' 	=> 	'February',
						'3'		=>	'March',
						'4'		=>	'April',
						'5'		=>	'May',
						'6'		=>	'June',
						'7'		=>	'July',
						'8'		=>	'August',
						'9'		=>	'September',
						'10'	=>	'October',
						'11'	=>	'November',
						'12'	=>	'December');
		$this->view->monthlist = $monthlist;
		
		$thisyear = date("Y");
		$yearlist = array(
						$thisyear-3	=> 	$thisyear-3,
						$thisyear-2	=> 	$thisyear-2,
						$thisyear-1	=> 	$thisyear-1,
						$thisyear	=> 	$thisyear);
		$this->view->yearlist = $yearlist;
		
		$fields = array	(
							'chargestype'  		=> array(	
															'field' => 'chargestype',
															'label' => 'Charge Type',
															'sortable' => false
														),
							'ccy'  				=> array(
															'field' => 'ccy',
															'label' => 'CCY',
															'sortable' => false
														),
							'numbertransaction' => array(
															'field' => 'numbertransaction',
															'label' => 'Number of Transaction',
															'sortable' => false
														),
							'totalcharges'  	=> array(
															'field' => 'totalcharges',
															'label' => 'Total Charges',
															'sortable' => false
														),
						);
		
		//validasi page, jika input page bukan angka               
		$page 	= $this->_getParam('page');
		$page 	= (Zend_Validate::is($page,'Digits'))? $page : 1;
		$this->view->currentPage = $page;
		
		//button csv, pdf & filter
		$csv 	= $this->_getParam('csv');
		$pdf 	= $this->_getParam('pdf');
		$filter 	= $this->_getParam('filter');
		
		//validasi sort, jika input sort bukan ASC atau DESC
		$sortBy  = $this->_getParam('sortby');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
		
		$this->view->currentPage 	= $page;
		$this->view->sortBy 		= $sortBy;
		$this->view->sortDir 		= $sortDir;
		
		//Param date
		$datetype 	= $this->_getParam('datetype');
		$date 		= $this->_getParam('date');
		$month 		= $this->_getParam('month');
		$year 		= $this->_getParam('year');
		
		$zf_filter 	= new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		
		$dataSQL = array();
		
		//Set templates
		$temp = array();
		$temp [0] = 'Monthly Charge'; 
		$temp [1] = 'Realtime Charge'; 
		$temp [2] = 'Supplay Chain Charge'; 
		$this->view->temp = $temp;
		
		$select = array();
		
		$select = $this->_db->select()
							->from(	array('B' => 'T_CHARGES_LOG'),
									array(	'chargestype'=>'CHARGES_TYPE',
											'paymenttype'=>'PAYMENT_TYPE',
											'ccy'=>'CCY',
											'numbertransaction'=>'COUNT(*)',
											'totalcharges'=>'SUM(AMOUNT)')
										)
							->group(array('B.CHARGES_TYPE','B.PAYMENT_TYPE','B.CCY'));
		
		if($datetype == null || $datetype == 'date1'){ 
			
			if(!$date){ 
				$date = (date("d/m/Y"));
				//$date   = $FormatDate->toString($this->_dateDBFormat);				
				//$date = Zend_Date::now()->toString("yyyy-MM-d");
				//$now = Zend_Date::now()->toString("yyyy-MM-d");
				//echo $dateadd;
			}
			else{
				$FormatDate = new Zend_Date($date, $this->_dateDisplayFormat);
				$dateselect   = $FormatDate->toString($this->_dateDBFormat);
			}
			
			$this->view->datemsg = $date;
			$select->where("CONVERT(date, B.LOG_DATETIME) = ".$this->_db->quote($dateselect));
			
		}
		if($datetype == 'date2'){ 
			
			$this->view->datemsg = $monthlist[$month].'&nbsp;'.$year;
			$this->view->month = $month;
			$this->view->year = $year;
			
			$select->where("MONTH(CONVERT(date, B.LOG_DATETIME)) = ".$this->_db->quote($month));
			$select->where("YEAR(CONVERT(date, B.LOG_DATETIME)) = ".$this->_db->quote($year));
		}
		
		
		$dataSQL = $this->_db->fetchAll($select);
		
		//echo "<pre>";
		//echo $select->__toString();
		//echo "<br />Data sql: ";
		//print_r($select);
		//die();
		//Zend_Debug::Dump($dataSQL);
		
		if ($csv || $pdf)
		{	
			$header  = Application_Helper_Array::simpleArray($fields, "label");}
		else
		{
			$this->paging($dataSQL);
			$dataSQL = $this->view->paginator;
		}
		
		$data1 = array(); //monthly charge
		$data2 = array(); //realtime charge
		$data3 = array(); //supplay chain charge
				
		$tot1 = NULL;
		$tot2 = NULL;
		$tot3 = NULL;
	
		
		/*
			1. monthly charge: admfeecompany, admfeeaccount, monthlycompany, monthlyaccount
			2. realtime charge: payment type (1 - 9) + charges type (PB, SKN, RTGS), partners
			3. supplay chain charge: payment type (8 - 9) + charges type (Interest fee + transaction fee)
		
		*/
		
		$mf = $this->getMonthlyfeeChargeRaw();
		$fea = $this->getFeatureIdRaw();
		$pt = $this->getPaymentTypeRaw();
		
		$paydis = $pt[8];
		$payset = $pt[9];
		
		// suply chain charge
		$fd [8] = $paydis;
		$fd [9] = $payset;
		
		$chargesraw = $this->getChargesTypeRaw();
		$c1 = $chargesraw[6];
		$c2 = $chargesraw[7];
		
		$chargesup [6] = $c1;
		$chargesup [7] = $c2;
		
		array_pop($chargesraw);
		array_pop($chargesraw);
				
		//print_r ($chargesraw);
		//print_r ($chargesup);
		//print_r ($pt);
		//print_r ($fd);
		
		foreach ($dataSQL as $d => $dt)
		{	
			
			foreach ($fields as $key => $field)
			{
				
				$value = $dt[$key];
				$paytype = $dt["paymenttype"];
				$chargestype = $dt["chargestype"];
				$ccy = $dt["ccy"];
				$tot = $dt["totalcharges"];
				
				$paytypename = $this->getPaymentTypeCharges($paytype);
				$chargestypename = $this->getChargesType($chargestype,$paytype);
				
				if ($key == "totalcharges" ){	$value = Application_Helper_General::displayMoney($value); }
				else if ($key == "chargestype" ){ $value = $paytypename.' '.$chargestypename;	}	
				
				$value = ($value == "" )? "&nbsp;": $value;
				if($csv){ $data[$d][$key] = $value; }
				
				//adm
				$total['monthly'] = array();
				foreach($mf as $st1 => $mt1){
					if($paytype == $st1){
						$data1[$d][$key] = $value;
						
						if (isset($total['monthly'][$ccy]))
						$total['monthly'][$ccy] = $tot;
						else
						$total['monthly'][$ccy] += $tot;
						
						
					}
					
				}
				
				//payment 1 - 9 (charge type 0 - 5)
				foreach($pt as $st => $mt){
					foreach($chargesraw as $row => $tt){
					
						if($paytype == $st){
							if($chargestype == $row){
								$data2[$d][$key] = $value;
								//echo 'ccy2: '.$ccy.'- tot:'.$tot;
							}
						}
						
					}
				}
				
				//feature
				foreach($fea as $st3 => $mt3){
					if($paytype == $st3){
						$data2[$d][$key] = $value;
						
					}
				}
				
				//payment disbursment, settlement( interest fee [6] + transaction fee [7] )
				foreach($fd as $st => $mt){
					foreach($chargesup as $row => $tt){
					
						if($paytype == $st){
							if($chargestype == $row){
								$data3[$d][$key] = $value;
								//echo 'ccy3: '.$ccy.'- tot:'.$tot;
							}
						}
						
					}
				}
				
				
				
			}
		
		}
		//echo "total monthly: ";
		//Zend_Debug::Dump($total['monthly']['IDR']);
		$this->view->pdf = ($pdf)? true: false;
		
		if($csv)
		{
			//Zend_Debug::Dump($data);die();
			$this->_helper->download->csv($header,$data,null,'Customer Summary Report');  
			$this->frontendLog('CSV', $this->_moduleDB, null,null,null);  
		}
		elseif($pdf)
		{	
			$outputHTML = "<tr><td>".$this->view->render('/chargesummaryreport/index.phtml')."</td></tr>";
			$this->_helper->download->pdf(null,null,null,'Charges Summary Report',$outputHTML);
			
			//$this->_helper->download->pdf($header,$data,null,'Charge Summary Report');  
			//$this->frontendLog('PDF', $this->_moduleDB, null,null,null);  
		}
		else
		{				
			$this->view->data1 			= $data1;
			$this->view->data2 			= $data2;
			$this->view->data3 			= $data3;
			
			$this->view->fields 		= $fields;
			$this->view->filter 		= $filter;
	        $this->view->sortDir 		= $sortDir;
			
			$this->view->datetype 		= $datetype;
			$this->view->datefull 		= $date;
			$this->view->datechoosen 	= $datechoosen;
			
			$this->view->month 			= $month;
			$this->view->year 			= $year;
			
			$this->view->monthlist 		= $monthlist;
			$this->view->yearlist 		= $yearlist;
			
			$this->view->temp 			= $temp;
			//$this->view->pdf 			= ($pdf)? true: false;
			
		}
	}
	
	function getChargesTypeRaw(){
		
		//charges.type.code
		//charges.type.desc
		
		$config = Zend_Registry::get('config');
		
		$ctype 	= $config["charges"]["type"]["code"];
		$cdesc 	= $config["charges"]["type"]["desc"];
		$payall = array_combine($ctype, $cdesc);
		
		return $payall;
	
	}
	
	function getChargesType($chargestype,$paytype){
		
		$chargesallraw = $this->getChargesTypeRaw();
		$payallraw = $this->getPaymentTypeRaw();
		
		/*chargesallraw = 	[0] => Inhouse 
							[1] => RTGS 
							[2] => SKN 
							[3] => Transfer Fee 
							[4] => Full Amount Fee 
							[5] => Provision Fee 
							[6] => Interest Fee 
							[7] => Transaction Fee 
		*/
		
		/*payallraw = 	[1] => Inhouse 
						[2] => Domestic 
						[3] => Remittance 
						[6] => Multi Credit 
						[7] => Multi Debet 
						[4] => Multi Credit - Import 
						[5] => Multi Debet - Import 
						[8] => Disbursement 
						[9] => Settlement
		*/
		
		$paytypenoempty = array(2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8);
		$emptyok = array(1=>1,9=>9); // Payment type: Within
		
		foreach($payallraw as $pp =>$val){
			
			foreach($chargesallraw as $ch => $valch){
			
				if($pp == 1 || $pp == 9){
					if($chargestype == 0){
						$result = " ";
					}
					else{
						if($ch == $chargestype){
							$result = $valch;
						}
						
					}
				}
				else if($pp >= 2 && $pp<= 8){ 
					if($ch == $chargestype){
						$result = $valch;
					}
					//$result = $valch;
				}
				else {
					$result = "";
					
				}
				//if($key == $value){	$chargetype = $val; }
				
			}
		}
			//else{ $chargetype = " "; }		
		//}
			
		return $result;
	
	}
	
	function getMonthlyfeeChargeRaw(){
	
		//monthlyfee.type.desc.
		$config    		= Zend_Registry::get('config');
		$monthlyfee 	= $config["monthlyfee"]["type"]["desc"];
		
		return $monthlyfee;
		
	}
	
	function getFeatureIdRaw(){
	
		//featureid
		$select = $this->_db->select()
							->from(array('T'=>'M_BILLER'),array('FEATURE_ID','FEATURE_NAME'));
		$featuredata = $this->_db->fetchAll($select);
		
		return $featuredata;
		
	}
	
	function getPaymentTypeRaw(){
		
		$config = Zend_Registry::get('config');
		
		//paymenttype: payment.type.code
		$paycode 	= $config["payment"]["type"]["code"];
		$paydesc 	= $config["payment"]["type"]["desc"];
		$payall = array_combine($paycode, $paydesc);
		
		return $payall;
	}
	
	function getPaymentTypeCharges($value){
		
		$monthlyfee = $this->getMonthlyfeeChargeRaw();
		$featuredata = $this->getFeatureIdRaw();
		$payall = $this->getPaymentTypeRaw();

		//$arrayall = array_merge($monthlyfee,$featuredata,$payall);
		
		$arrayall = $monthlyfee + $featuredata + $payall;
		foreach($arrayall as $key => $val){
			if($key == $value){	$paytype = $val; }
			
		}
		
		//print_r($arrayall);
		return $paytype;	
	}

}
