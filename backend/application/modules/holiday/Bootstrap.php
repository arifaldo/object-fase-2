<?php
class holiday_Bootstrap extends Zend_Application_Module_Bootstrap {
	
	protected function _initModule() {
		
		//$env = $this->_application->getEnvironment();
		$env = $this->getApplication()->getEnvironment();
		
		//set options from ini files in configs folder for this module
		//loop through all files in configs directory and add them to bootstrap options
		//uses php5 built in DirectoryIterator class 
		$dir = new DirectoryIterator(dirname(__FILE__).'/configs');
		$arr = array();
		foreach($dir as $file) {
			if(!$file->isDot() && $file->isFile()) {
				$cfg = new Zend_Config_Ini(dirname(__FILE__).'/configs/'.$file->getFileName(), 
										   $env,
										   array('allowModifications'=>true));
				$arr = array_merge($arr,$cfg->toArray());
			}
		}
		
		//set options to this module's bootstrap
		$this->setOptions($arr);
		
		//set options to parent (application) bootstrap
		//$this->getApplication()->setOptions($arr);
		//$this->_application->setOptions($arr);
		
		//Zend_Debug::dump($this->getOptions(), 'Module Options : ');
		//Zend_Debug::dump($this->getApplication()->getOptions(),'Application Options : ');
		
	}
	
	//get module's parent options
	//$options = $this->getApplication()->getOptions();
	//merge the options
	//$arr = array_merge($options,$arr);
	//re-set the options in application's bootstrap
	//$this->getApplication()->setOptions($arr);
}
