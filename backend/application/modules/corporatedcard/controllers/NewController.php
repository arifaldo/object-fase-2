<?php

require_once 'Zend/Controller/Action.php';

class Corporatedcard_NewController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti
	
	
    public function indexAction()
	{
		
	    //pengaturan url untuk button back
	    //$this->setbackURL('/'.$this->_request->getModuleName().'/index');    
	    $this->_helper->layout()->setLayout('newlayout');
	    $this->setbackURL('/country/index');    

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
		
		$selectcomp = $this->_db->select()
                            ->from('M_CUSTOMER', array('value' => new Zend_Db_Expr("CONCAT(CUST_NAME, ' - ', CUST_ID)"),'CUST_NAME','CUST_ID','CUST_TYPE'))
                            // ->where('CUST_NAME = ? ',$tblName)
                            ->where('CUST_STATUS = 1 ');

        $tempColumn = $this->_db->fetchAll($selectcomp);
        
        $this->view->custarr = json_encode($tempColumn); 
		
    	$this->view->report_msg = array();
		if($this->_request->isPost() )
		{
			
			$filters = array(
			                 'COMPANY_NAME' => array('StringTrim','StripTags'),
			                 'COMPANY_ID' => array('StringTrim','StripTags'),
							 'cust_card' => array('StringTrim','StripTags'),
            			    'edc' => array('StringTrim','StripTags'),
            			    'purchase' => array('StringTrim','StripTags'),
            			    
							);

			$validators = array('COMPANY_NAME' => array('NotEmpty',
			                                 //        array('Db_NoRecordExists', array('table' => 'M_BRANCH', 'field' => 'BRANCH_CODE')),
													 'messages' => array(
																         $this->language->_('Can not be empty'),
											//					         $this->language->_('Branch Code already existed')
			                                                             )
													),
			                    'COMPANY_ID' => array('NotEmpty',
			                               //          array('Db_NoRecordExists', array('table' => 'M_BRANCH', 'field' => 'BRANCH_NAME')),
													 'messages' => array(
																         $this->language->_('Can not be empty'),
											//					         $this->language->_('Branch Name already existed')
			                                                             )
													),
								'cust_card'      => array('NotEmpty',
													 'messages' => array(
																		 $this->language->_('Can not be empty')
																         )
														),
                			    'edc'      => array('NotEmpty',
                                    			        'messages' => array(
                                    			            $this->language->_('Can not be empty')
                                    			        )
                                    			    ),
                			    
                			    'purchase'      => array('NotEmpty',
                                    			        'messages' => array(
                                    			            $this->language->_('Can not be empty')
                                    			        )
                                    			    ),
							   );
		
							   
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			//echo '<pre>';
			//var_dump($this->_request->getParams());die;
			
			$errorLim = false;
			if($zf_filter_input->edc == NULL &&  $zf_filter_input->purchase == NULL ){

                  $errorLim = true;
                  $error_remark = "al least one of the EDC or ATM must be on";
              }
			//var_dump($zf_filter_input->edc);die;
			
			if($zf_filter_input->isValid() && !$errorLim)
			{
// 			    die;
				$num_str = sprintf("%08d", mt_rand(1, 99999999));
				$content = array(
								//'BRANCH_CODE' 	 => $zf_filter_input->COMPANY_NAME,
								'REG_NUMBER' => $this->generatePaymentID(1),
								'VA_NUMBER' => $num_str,
								'VA_NAME' => $zf_filter_input->COMPANY_NAME,
								'CUST_ID' 	 => $zf_filter_input->COMPANY_ID,
								'DEBIT_TYPE' 	 => $zf_filter_input->cust_card,
            				    'DEBIT_EDC' 	 => $zf_filter_input->edc,
            				    'DEBIT_ATM' 	 => $zf_filter_input->purchase,
								'DEBIT_DATE'		=> new Zend_Db_Expr('now()'),
								'DEBIT_SUGGESTED'		=> new Zend_Db_Expr('now()'),
								'DEBIT_SUGGESTEDBY' => $this->_userIdLogin,
								'DEBIT_STATUS'		=> 3
						       );
						       
				try 
				{
// 					print_r($content);die;
					//-------- insert --------------
					$this->_db->beginTransaction(); 
					$change_id = $this->suggestionWaitingApproval('Debit Card',$info,$this->_changeType['code']['new'],null,'T_CUST_DEBIT','TEMP_CUST_DEBIT',$zf_filter_input->COMPANY_ID,$zf_filter_input->COMPANY_NAME,$zf_filter_input->COMPANY_ID,$zf_filter_input->COMPANY_NAME);
					$content['CHANGES_ID'] = $change_id;
					$this->_db->insert('TEMP_CUST_DEBIT', $content);
					
					
// 					die;
					$this->_db->commit();
					//$id = $this->_db->lastInsertId();
					Application_Helper_General::writeLog('COAD','Add New Corporate Debit Card. Corporate : ['.$zf_filter_input->COMPANY_NAME.']');
					$this->view->success = true;
					$this->view->report_msg = array();
					 $this->setbackURL('/'.$this->_request->getModuleName().'/index');
					 $this->_redirect('/notification/submited/index');
				}
				catch(Exception $e) 
				{
					//rollback changes
					//var_dump($e);die;
					$this->_db->rollBack();
					
					foreach(array_keys($filters) as $field)
						$this->view->$field = $zf_filter_input->getEscaped($field);

					$errorMsg = 'exeption';
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
				}
			}
			else
			{
				$this->view->error = true;
				foreach(array_keys($filters) as $field)
						$this->view->$field = ($zf_filter_input->isValid($field))? $zf_filter_input->getEscaped($field) : $this->_getParam($field);


				
				$error = $zf_filter_input->getMessages();
				//var_dump($error);die;
				//format error utk ditampilkan di view html 
                $errorArray = null;
		        foreach($error as $keyRoot => $rowError)
		        {
		           foreach($rowError as $errorString)
		           {
		              $errorArray[$keyRoot] = $errorString;
		           }
		        }
        
		        $this->view->succes = false;
				
                $this->view->report_msg = $errorArray;
				if(!empty($error_remark)){
					//die('here');
					$errorArray = array(true);
					$this->view->report_msg = $errorArray;
					$this->view->errormsg = 'Error :'.$this->language->_($error_remark);
				}else{
					$this->view->errormsg = 'Error :'.$this->language->_('in processing form values. Please correct values and re-submit.');	
				}
				$this->view->edc = $zf_filter_input->edc;
				$this->view->purchase = $zf_filter_input->purchase;
				if($zf_filter_input->cust_card == '1'){
					$this->view->custtype1 = 'checked';
				}else{
					$this->view->custtype2 = 'checked';
				}
			}
		}
		
		Application_Helper_General::writeLog('COAD','Add New Branch Bank');
	}
	
	public function generatePaymentID($forTransaction, $PS_PERIODIC = null)
	{
		$currentDate = date("Ymd");
		// $seqNumber	 = $this->getPaymentCounter($forTransaction);
		$seqNumber	 = strtoupper(Application_Helper_General::str_rand(9));
		$checkDigit  = '';
			$paymentID   = "CDC".$currentDate.$seqNumber.$checkDigit;
		
			
		return $paymentID;
	}

}