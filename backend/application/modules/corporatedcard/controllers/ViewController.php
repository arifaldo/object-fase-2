<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'CMD/Payment.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'SGO/Extendedmodule/phpqrcode/qrlib.php';
require_once 'Crypt/AESMYSQL.php';

class corporatedcard_ViewController extends Application_Main
{
	protected $_moduleDB = 'RTF';

	protected $_bankName;

	public function initController()
	{
		$conf = Zend_Registry::get('config');

		$this->_bankName = $conf['app']['bankname'];
		$this->_transferStatus 	= $conf["transfer"]["status"];
	}
	
	

	public function indexAction()
	{
		$this->_helper->_layout->setLayout('newlayout');
		$filter 			= new Application_Filtering();

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
	    $password = $sessionNamespace->token; 
	    $this->view->token = $sessionNamespace->token;

		$trfType 		= array_combine($this->_transfertype["code"], $this->_transfertype["desc"]);

		$AESMYSQL = new Crypt_AESMYSQL();
		$REG_NUMBER 			= urldecode($filter->filter($this->_getParam('id'), "PS_NUMBER"));
		$REG_NUMBER = $AESMYSQL->decrypt($REG_NUMBER, $password);
		
		
				

		$pdf 				= $filter->filter($this->_getParam('pdf'), "BUTTON");
		$cancelfuturedatebtn = $filter->filter($this->_getParam('cancelfuturedate'), "BUTTON");
		$backBtn			= $filter->filter($this->_getParam('back'), "BUTTON");
		
		$select = $this->_db->select()
					        ->from(array('A' => 'T_CUST_DEBIT'))
							->join(array('D' => 'M_CUSTOMER'), 'D.CUST_ID = A.CUST_ID',array('D.CUST_NAME'))
							->where('A.REG_NUMBER = ?',$REG_NUMBER);
							//echo $select;
		$data = $this->_db->fetchRow($select);		
//var_dump($data);die;		
		
		$datainq =  new Service_Account($data['VA_NUMBER'],'000');
		$dataInquiry = $datainq->miniaccountInquiry('AB',FALSE); 
		$balance = 0;
		if($dataInquiry['ResponseCode'] == '00'){
			$balance = Application_Helper_General::displayMoneyplain($dataInquiry['Balance']);
			$insertData = array(
						'DATE'		=> new Zend_Db_Expr('now()'),
						'BALANCE' => $dataInquiry['Balance'],
						'ACCT_NO' => $data['VA_NUMBER'],
						'CUST_ID'	=> $data['CUST_ID']
					);
			$this->_db->insert('T_DEBIT_BALANCE', $insertData);
			$lastupdate =  date('d-M-Y H:i:s');
		}else{
			
			$select = $this->_db->select()
					        ->from(array('A' => 'T_DEBIT_BALANCE'),array('*'))
							->where('A.CUST_ID = ?',$data['CUST_ID']);
			$databalance = $this->_db->fetchRow($select);					
			$balance = Application_Helper_General::displayMoneyplain($databalance['BALANCE']);
			$lastupdate = $databalance['DATE'];
		}
		
		$this->view->balance = 'IDR '.$balance;
		$this->view->lastupdate = $lastupdate;
		//echo '<pre>';
		//var_dump($dataInquiry);die;
		
		if($data['DEBIT_TYPE'] == '1'){
			$data['DEBIT_TYPE'] = 'Corporate';
		}else{
			$data['DEBIT_TYPE'] = 'Card Holder';
		}
		
		if($data['DEBIT_ATM'] == '1'){
			$data['DEBIT_ATM'] = 'On';
		}else{
			$data['DEBIT_ATM'] = 'Off';
		}
		
		if($data['DEBIT_EDC'] == '1'){
			$data['DEBIT_EDC'] = 'On';
		}else{
			$data['DEBIT_EDC'] = 'Off';
		}
		
		
		$this->view->data = $data;
		
		
		$datadebit  = $this->_db->fetchAll(
                     $this->_db->select()
                               ->from(array('T' => 'T_DEBITCARD'))
                               ->joinleft(array('C' => 'M_CUSTOMER'),'C.CUST_ID=T.CUST_ID',array('CUST_NAME','CUST_EMAIL'))
							   ->joinleft(array('D' => 'T_CUST_DEBIT'),'D.REG_NUMBER=T.REG_NUMBER',array('*',
							   'd_sugest'=>'D.DEBIT_SUGGESTED',
							   'd_sugestby'=>'D.DEBIT_SUGGESTEDBY',
							   'd_approve'=>'T.DEBIT_APPROVED',
							   'd_approveby'=>'T.DEBIT_APPROVEDBY',
							   'd_status'=>'T.DEBIT_STATUS',
							   ))
							   ->joinleft(array('E' => 'T_DEBIT_GROUP'),'E.DEBIT_NUMBER=T.DEBIT_NUMBER',array('GROUP_ID'))
							   ->joinleft(array('EG' => 'M_DEBITGROUP'),'EG.GROUP_ID=E.GROUP_ID',array('GROUP_NAME'))
							   ->joinleft(array('F' => 'M_USER_DEBIT'),'F.USER_DEBITNUMBER=T.DEBIT_NUMBER',array('USER_ID'))
							   ->joinleft(array('G' => 'M_USER'),'F.USER_ID=G.USER_ID',array('USER_FULLNAME','USER_EMAIL'))
                               #->where('T.DEBIT_NUMBER = ?', $DEBIT_NUMBER)
							   ->where('D.REG_NUMBER =?',$data['REG_NUMBER'])
							   ->where('T.CUST_ID =?',$data['CUST_ID'])
							   ->order('T.DEBIT_APPROVED DESC')
                               );
		//echo '<pre>';
		//var_dump($datadebit);die;
		if(!empty($datadebit)){
			foreach($datadebit as $key => $val){
				
				if(!empty($val['USER_FULLNAME'])){
								$datadebit[$key]['DEBIT_NAME'] = $val['USER_FULLNAME'];
							}else{
								$datadebit[$key]['DEBIT_NAME'] = $val['CUST_NAME'];
							}	
							
							if(!empty($val['USER_EMAIL'])){
								$datadebit[$key]['DEBIT_EMAIL'] = $val['USER_EMAIL'];
							}else{
								$datadebit[$key]['DEBIT_EMAIL'] = $val['CUST_EMAIL'];
							}
				
					$datainq =  new Service_Account($val['DEBIT_NUMBER'],'000');
					$dataInquiry = $datainq->miniaccountInquiry('AB',FALSE); 
					$balance = 0;
					if($dataInquiry['ResponseCode'] == '00'){
						$balance = Application_Helper_General::displayMoneyplain($dataInquiry['Balance']);
						$insertData = array(
									'DATE'		=> new Zend_Db_Expr('now()'),
									'BALANCE' => $dataInquiry['Balance'],
									'ACCT_NO' => $val['DEBIT_NUMBER'],
									'CUST_ID'	=> $val['CUST_ID']
								);
						$select = $this->_db->select()
										->from(array('A' => 'T_DEBIT_BALANCE'),array('*'))
										->where('A.ACCT_NO = ?',$val['DEBIT_NUMBER'])
										->where('A.CUST_ID = ?',$val['CUST_ID']);
						$databalance = $this->_db->fetchRow($select);	
						if(empty($databalance)){
							$this->_db->insert('T_DEBIT_BALANCE', $insertData);
						}else{
							$whereArr = array(
								'CUST_ID = ?' => $val['CUST_ID'],
								'ACCT_NO = ?' => $val['DEBIT_NUMBER']
								
							);
							
							$updateData = array(
								'BALANCE' => $dataInquiry['Balance'],
								'DATE' => new Zend_Db_Expr('now()')
							);
							
							$this->_db->update('T_DEBIT_BALANCE', $updateData,$whereArr);	
						}
						$lastupdate =  date('d-M-Y H:i:s');
					}else{
						
						$select = $this->_db->select()
										->from(array('A' => 'T_DEBIT_BALANCE'),array('*'))
										->where('A.CUST_ID = ?',$val['DEBIT_NUMBER']);
						$databalance = $this->_db->fetchRow($select);					
						$balance = Application_Helper_General::displayMoneyplain($databalance['BALANCE']);
						$lastupdate = $databalance['DATE'];
					}
							
					$datadebit[$key]['BALANCE'] = $balance;		
				
			}
		}

		if(!empty($param['va'])){
			
			
			$frontendOptions = array ('lifetime' => 2419200,
                                  'automatic_serialization' => true );
			$backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
			$cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
			$cacheID = 'DBCB'.$this->_custIdLogin;
			$data = $cache->load($cacheID);
			if(!empty($data)){
				echo '<pre>';
				var_dump($data);die;
				$no = 0;
				$resultdata = array();
				foreach($data['data'] as $val){
					if($val['VA_NUMBER'] == $param['va']){
						$resultdata[$no] = $val;
						$no++;
					}
				}
				//var_dump($data['resultdatagroup']);die;
				$nogroup = 0;
				$resultdatagroup = array();
				$resultdatagroup['0'] = array('GROUP_ID'=> NULL);
				foreach($data['resultdatagroup'] as $val){
					if($val['VA_NUMBER'] == $param['va']){
						$resultdatagroup[$nogroup] = $val;
						$nogroup++;
					}
				}
			}else{
			
			$tempresultdata  = $this->_db->fetchAll(
                     $this->_db->select()
                               ->from(array('T' => 'T_DEBITCARD'))
                               ->joinleft(array('C' => 'M_CUSTOMER'),'C.CUST_ID=T.CUST_ID',array('CUST_NAME','CUST_EMAIL'))
							   ->joinleft(array('D' => 'T_CUST_DEBIT'),'D.REG_NUMBER=T.REG_NUMBER',array('REG_NUMBER','VA_NUMBER','DEBIT_TYPE','DEBIT_ATM','DEBIT_EDC'))
							   ->joinleft(array('E' => 'T_DEBIT_GROUP'),'E.DEBIT_NUMBER=T.DEBIT_NUMBER',array('GROUP_ID'))
							   ->joinleft(array('EG' => 'M_DEBITGROUP'),'EG.GROUP_ID=E.GROUP_ID',array('GROUP_NAME'))
							   
							   ->joinleft(array('F' => 'M_USER_DEBIT'),'F.USER_DEBITNUMBER=T.DEBIT_NUMBER',array('USER_ID'))
							   ->joinleft(array('G' => 'M_USER'),'F.USER_ID=G.USER_ID',array('USER_FULLNAME','USER_EMAIL'))
							   ->joinLeft(array('B' => 'T_DEBIT_BALANCE'),'B.CUST_ID = T.CUST_ID AND B.ACCT_NO = T.DEBIT_NUMBER',array('BALANCE',
					'rs_datetime' => 'DATE'))
                               ->where('T.CUST_ID = ?', $this->_custIdLogin)
							   ->group('T.DEBIT_NUMBER')
                               );
					//echo $tempresultdata;die;
					$updateArr = array('STATUS' => '0');
					if(!empty($tempresultdata)){
						foreach($tempresultdata as $val){
								
								$whereA44 = array(
									'ACCT_NO = ?',$val['DEBIT_NUMBER'],
									'CUST_ID = ?',$this->_custIdLogin
								);
								$result  = $this->_db->update('T_DEBIT_BALANCE',$updateArr,$whereArr);
						}
					}
					
					$queueService = SGO_Extendedmodule_Queueservice_Service::getInstance();
					$emailQueueProducer = $queueService->getQueueByProfileName("BALANCE_DEBIT");
					$datarab = json_encode($tempresultdata);
					//echo '<pre>';
					//var_dump($datarab);die;
					$emailQueueProducer->insertQueueItem($datarab);		   
							   
				$no = 0;
				$resultdata = array();
				
				foreach($tempresultdata as $val){
					if($val['VA_NUMBER'] == $param['va']){
						$resultdata[$no] = $val;
						$no++;
					}
				}				   
				
							   
			$tempresultdatagroup  = $this->_db->fetchAll(
                     $this->_db->select()
                               ->from(array('T' => 'T_DEBITCARD'))
                               ->joinleft(array('C' => 'M_CUSTOMER'),'C.CUST_ID=T.CUST_ID',array('CUST_NAME'))
							   ->joinleft(array('D' => 'T_CUST_DEBIT'),'D.REG_NUMBER=T.REG_NUMBER',array('REG_NUMBER'))
							   ->joinleft(array('E' => 'T_DEBIT_GROUP'),'E.DEBIT_NUMBER=T.DEBIT_NUMBER',array('GROUP_ID'))
							   ->joinleft(array('EG' => 'M_DEBITGROUP'),'EG.GROUP_ID=E.GROUP_ID',array('GROUP_NAME'))
							   
                               //->where('D.VA_NUMBER = ?', $param['va'])
							   ->group('GROUP_ID')
                               );
			//		echo '<pre>';
			//var_dump($tempresultdatagroup);die;
					$nogroup = 1;
					$resultdatagroup = array();
					$resultdatagroup['0'] = array('GROUP_ID'=> NULL);
					foreach($tempresultdatagroup as $val){
						if($val['VA_NUMBER'] == $param['va']){
							$resultdatagroup[$nogroup] = $val;
							$nogroup++;
						}
					}
							   
			}
						
			//echo '<pre>';			
			//var_dump($resultdata);die;
			$dataArr = array();
			if(!empty($resultdatagroup)){
				foreach($resultdatagroup as $key => $val){
					foreach($resultdata as $row){
						if($val['GROUP_ID'] == $row['GROUP_ID']){
							if(!empty($row['USER_FULLNAME'])){
								$row['DEBIT_NAME'] = $row['USER_FULLNAME'];
							}else{
								$row['DEBIT_NAME'] = $row['CUST_NAME'];
							}	
							
							if(!empty($row['USER_EMAIL'])){
								$row['DEBIT_EMAIL'] = $row['USER_EMAIL'];
							}else{
								$row['DEBIT_EMAIL'] = $row['CUST_EMAIL'];
							}
						$dataArr[$val['GROUP_ID']][] = $row;
						}
					}
				}
			}
			//echo '<pre>';
			//var_dump($resultdata);die;
				if($resultdata['0']['DEBIT_TYPE'] == '1'){
						$resultdata['0']['DEBIT_TYPE'] = 'Corporate';
					}else{
						$resultdata['0']['DEBIT_TYPE'] = 'Card Holder';
					}
					
					if($resultdata['0']['DEBIT_ATM'] == '1'){
						$resultdata['0']['DEBIT_ATM'] = 'On';
					}else{
						$resultdata['0']['DEBIT_ATM'] = 'Off';
					}
					
					if($resultdata['0']['DEBIT_EDC'] == '1'){
						$resultdata['0']['DEBIT_EDC'] = 'On';
					}else{
						$resultdata['0']['DEBIT_EDC'] = 'Off';
					}			
			//echo '<pre>';
			//var_dump($resultdata);die;			
			$datainq =  new Service_Account($resultdata['0']['VA_NUMBER'],'000');
			$dataInquiry = $datainq->miniaccountInquiry('AB',FALSE); 
			
			if($dataInquiry['ResponseCode'] == '00'){
				$resultdata['0']['BALANCE_MASTER'] = $dataInquiry['Balance'];
			}else{
				$resultdata['0']['BALANCE_MASTER'] = 'N/A';
			}
			
							   
			//$this->view->dataselect = $resultdata;
			$this->view->data = $resultdata['0'];
			$this->view->dataArr = $dataArr;
			
			$this->view->va = $param['va'];
			
			$this->view->resultdatagroup = $resultdatagroup;
		}

		//var_dump($data);die;
		$statusarr = array(
					'1' => $this->language->_('Active'),
					'3' => $this->language->_('Delete'),
					'4' => $this->language->_('Suspend')
				);
				$this->view->statusarr = $statusarr;
		
		$this->view->datadebit = $datadebit;
		
		
		
	}
}
