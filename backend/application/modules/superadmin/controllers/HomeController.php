<?php

class superadmin_HomeController extends Zend_Controller_Action {
	private $_db;

	public function init(){

		$this->_helper->layout()->setLayout('layout_superadmin');
		$this->_db 	= Zend_Db_Table::getDefaultAdapter();
		$auth = new Zend_Session_Namespace( 'SYSADMIN' );

		if($auth->isSuperUser != true || $auth->userGroupLogin != "SUPERADMIN")
		{
			$this->_redirect('/superadmin');
		}
		else
		{
			$suser_id	= $auth->userNameLogin;
			$this->view->userId = $suser_id;
			$this->_db = Zend_Db_Table::getDefaultAdapter();
			$reqChpass = $this->_db->FetchRow(
																				$this->_db->SELECT()
																									->FROM( 'M_SUSER',array('IS_REQPASS1','IS_REQPASS2') )
																									->WHERE('SUSER_ID = ?',$suser_id)
																			);

			if($reqChpass['IS_REQPASS1']){
				$this->_redirect('/superadmin/changespassword/index/change/1');
			}
			else if($reqChpass['IS_REQPASS2']){
				$this->_redirect('/superadmin/changespassword/index/change/2');
			}
		}
	}
	
	public function indexAction() {
		
	}
    
}