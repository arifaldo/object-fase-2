<?php
require_once ('Crypt/AESMYSQL.php');
require_once 'General/Settings.php';

class superadmin_IndexController extends Zend_Controller_Action {

	private $_db;
	
	public function init()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
		$this->_helper->layout()->setLayout('layout_superadmin');
		$auth = new Zend_Session_Namespace( 'SYSADMIN' );
		if($auth->isSuperUser == true && $auth->userGroupLogin == "SUPERADMIN")
		{
			$this->_redirect('/superadmin/home');
		}
	}
			
	public function indexAction() {

		//---------------------- GENERATE CAPTCHA IMAGE  --------------------------------------
		$captcha = Application_Captcha::generateCaptcha ();
		$this->view->captchaId = $captcha ['id']; //returns the ID given to session &amp; image
		$this->view->captImgDir = $captcha ['imgDir'];
		//---------------------	END GENERATE CAPTCHA IMAGE -------------------------------------

		if ($this->_request->isPost ()) {

			// ======================== START FILTERING ======================================
			$filters = array ('userId' => array ('StripTags', 'StringTrim', 'StringToUpper' ), 'password' => array () );
			$zf_filter_input = new Zend_Filter_Input ( $filters, null, $this->_getAllParams (), null );
			// ========================== END FILTERING =======================================	

			if (Application_Captcha::validateCaptcha ( $this->_request->getPost ( 'captcha' ) ) === true) 
			{
				$username = ($zf_filter_input->userId);
				$userId = $username;
				$key = md5 ( $username );
				$pass = ($zf_filter_input->password);
				$pass2 = ($zf_filter_input->password2);
				
				$encrypt = new Crypt_AESMYSQL ();
				$password = md5 ( $encrypt->encrypt ( $pass, $key ) );
				$password2 = md5 ( $encrypt->encrypt ( $pass2, $key ) );

				$login = $this->_db->fetchrow ( 
																		$this->_db->SELECT ()
																						->FROM ( 'M_SUSER' )
																						->WHERE ( 'SUSER_ID = ?', $username )
																						->WHERE ( 'SUSER_PASSWORD1 = ?', $password )
																						->WHERE ( 'SUSER_PASSWORD2 = ?', $password2 )
																	);
				
				
				$cekpass = false;
				if ($login) 
				{
					if ($login ['SUSER_PASSWORD1'] == $password && $login ['SUSER_PASSWORD2'] == $password2) 
					{
						$cekpass = true;
					}
				} 
				else 
				{
					$errors = 'Invalid user id & password';	
				}
				
				if($cekpass)
				{
					// -- -- Set Session timeout hardcode 1DAY -- -- //
					$minDiff = 3600;
					$objSessionNamespace = new Zend_Session_Namespace( 'SYSADMIN' );
					$timeExprSession = $minDiff *60;
		  			$objSessionNamespace->setExpirationSeconds( $timeExprSession );


					// set keterangan login ke auth
					
					$objSessionNamespace->userIdLogin = $username;
					$objSessionNamespace->userNameLogin  = "Super Admin";
					$objSessionNamespace->isSuperUser 		= true;
					$objSessionNamespace->userGroupLogin = "SUPERADMIN";
					$objSessionNamespace->custIdLogin = "BANK";

	  				//Application_Helper_General::writeLog('BLGN','Login into system from '.$_SERVER['REMOTE_ADDR']);
					$this->_redirect ( '/superadmin/home' );
				}
			}else {
				$errors = 'Wrong Captcha';
			}
		
		} // END REQUEST IS POST
		
		if(isset($errors)){
			$this->view->errors = $errors;
		}
	} 


	public function logoutAction() {
		Application_Helper_General::writeLog('BLGT','Logout from system');
		if(Zend_Auth::getInstance()->hasIdentity()){
			$auth = Zend_Auth::getInstance()->getIdentity();
    	  	$this->_userIdLogin   = $auth->userIdLogin;
    	  	$this->_custIdLogin   = $auth->custIdLogin;
    	  	Zend_Auth::getInstance()->clearIdentity();
			$BankUser	= new BankUser(strtoupper($this->_userIdLogin));
			$BankUser->forceLogout();
		}
		$this->_redirect('/');
	}
}