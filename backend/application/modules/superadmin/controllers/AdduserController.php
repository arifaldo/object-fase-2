<?php
require_once 'Crypt/AESMYSQL.php';
require_once 'Zend/Controller/Action.php';
require_once 'General/Settings.php';
require_once 'General/BankUser.php';

class superadmin_AdduserController extends Zend_Controller_Action 
{
	private $_db;
	
 	public function init()
 	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
		$this->_helper->layout()->setLayout('layout_superadmin');
		$auth = new Zend_Session_Namespace( 'SYSADMIN' );

		if($auth->isSuperUser != true || $auth->userGroupLogin != "SUPERADMIN")
		{
			$this->_redirect('/superadmin');
		}
 	}
 	
	public function indexAction() 
	{	
		$submit = $this->_getParam('Submit');
		$success = $this->_getParam('success');
		$this->view->success = $success;

		$arrgroup = array('BAAPPV'=>'Bank Admin Approver','BAMAKR'=>'Bank Admin Maker');
	
		$this->view->arrGroup = $arrgroup;
		$flip = array_flip($arrgroup);

		if($submit)
		{
			$filters = array(
							'userId'	=> array('StringToUpper','StripTags', 'StringTrim'),
							'name'		=> array('StripTags', 'StringTrim'),
							'groupId'  	=> array('StripTags', 'StringTrim')
						);

			$validators = array(
									'userId' => array(	
															'NotEmpty',
															'Alnum',
															array('StringLength', array('min' => 5,'max'=>16)),
															'messages' => array(
																					'Can not be empty, Invalid user Id format',
																					'Invalid user Id format',
																					'Invalid user Id format',
																				) 													   					   
														),
									'name' => array(
															'NotEmpty',
															'messages' => array(
																					'Can not be empty',
																				)
														),														
																
									'groupId' => array(
																array('InArray',$flip),
                                                                'messages' => array(																																								
																					'Please Select Group',
																				)
														),																																					
								);
			$params = $this->_request->getParams();
			$zf_filter_input = new Zend_Filter_Input($filters, $validators,$params, $this->_optionsValidator);
			
			$groupid = $this->_request->getParam('groupId');
			$valid = new Zend_Validate_Identical(array($groupid => '0'));
//			Zend_Debug::dump($arrgroup);die;
//			CEK HURUF DAN ANGKA DI USERID			
			$strtemp = 0;
			$digittemp = 0;					
			$user_id = $zf_filter_input->userId;
			$str = str_split($user_id);			
			foreach($str as $string){
				if((Zend_Validate::is($string,'Digits')) ? true : false)
				{$digittemp = 1;}
				if((Zend_Validate::is($string,'Alpha')) ? true : false)
				{$strtemp = 1;}
			}
			$cekkombinasi = false;
			if($digittemp == 1 && $strtemp == 1)
			{
				$cekkombinasi = true;
			}
			
//			echo $strtemp.' '.$digittemp;die;
//			CEK GROUPID
//			$group = (($this->_getParam('groupId')==0) ? false : true);

			$settings =  new Settings();
			$maxLengthPassword = $settings->getSetting('maxbpassword');
			$pass = $this->_getParam('password');
			$confirmpass = $this->_getParam('confirmpassword');
			
			$cekpass = true;
			$cekconfirmpass = true;
			
			if (!$confirmpass)
			{
				$cekconfirmpass = false;
				$errConfirm = "Confirm New Password cannot be left blank. Please correct it.";				
			}
			elseif ($pass != $confirmpass)
			{
				$cekconfirmpass = false;
				$errConfirm = "Sorry, but the two passwords you entered are not the same.";  
			}

			if (!$pass)
			{
				$cekpass = false;
				$errPass = "New Password cannot be left blank. Please correct it.";
			}
			elseif ((strlen($pass) < 8) || (strlen($pass) > $maxLengthPassword))
			{
				$cekpass = false;
				$errPass = "Minimum char 8 and maximum char ".$maxLengthPassword." for New Password length. Please correct it.";
			}
			elseif (Application_Helper_General::checkPasswordStrength($pass) != 3)
			{
				$cekpass = false;
				$errPass = "Password must containt at least one uppercase character, one lowercase character and one number.Please correct it.";													
			}
			
			if($zf_filter_input->isValid() && $cekkombinasi && $cekpass && $cekconfirmpass)
			{						
				
				$email = $zf_filter_input->email;
				
				$selectmaster =	$this->_db->select()
									->from('M_BUSER',array('BUSER_ID'))
									->where('BUSER_ID = ?',$user_id)
									->query()->fetchAll();
				
				if(count($selectmaster) == 0) 
				{
					try
					{
						$this->_db->beginTransaction();	
						
						$insertArr = array(
							'BUSER_ID'			=> $zf_filter_input->userId, 
							'BUSER_NAME' 		=> $zf_filter_input->name, 
							'BGROUP_ID' 		=> $zf_filter_input->groupId,								
							'BUSER_DESC' 		=> $params['desc'],								
						);
						
						$auth = Zend_Auth::getInstance()->getIdentity();
						$encrypt = new Crypt_AESMYSQL ();
						$key = md5((string)$zf_filter_input->userId);
						$clearpassword = new BankUser($zf_filter_input->userId);
       					$generate = $clearpassword->generateNewPassword();
						$password = md5 ( $encrypt->encrypt ( $pass, $key));

					
						//Zend_Debug::dump($cobapassword);die;
						
						$insertArr['CREATED']     				= new Zend_Db_Expr('now()');
						$insertArr['CREATEDBY']   				= $auth->userIdLogin;
						$insertArr['SUGGESTED']     			= new Zend_Db_Expr('now()');
						$insertArr['SUGGESTEDBY']   			= $auth->userIdLogin;
						$insertArr['BUSER_ISLOCKED'] 			= 0;
						$insertArr['BUSER_ISNEW'] 				= 1;
						$insertArr['BUSER_RCHANGE'] 			= 1;
						$insertArr['BUSER_RRESET'] 				= 0;
						$insertArr['BUSER_RPASSWORD_ISEMAILED'] = 0;
						$insertArr['BUSER_RPASSWORD_ISPRINTED'] = 0;
						$insertArr['BUSER_ISEMAILPWD'] 			= 0;
						$insertArr['BUSER_LASTACTIVITY'] 		= new Zend_Db_Expr('now()');
						$insertArr['BUSER_ISREQUIRE_CHANGEPWD'] = 1;
						$insertArr['BUSER_STATUS'] 				= 1;
						$insertArr['BUSER_CLEARTEXT_PASSWORD']	= $generate['CLEAR_TEXT_PASSWORD'];
						$insertArr['BUSER_PASSWORD'] 			= $password;
						
						//Zend_Debug::dump($insertArr);die;
						$this->_db->insert('M_BUSER', $insertArr);
						
						$logArr['LOG_DATE']			= new Zend_Db_Expr('now()');
						$logArr['SUSER_ID']			= $auth->userIdLogin;
						$logArr['ACTION_FULLDESC']	= 'Create new user '.$zf_filter_input->userId.' as '.$zf_filter_input->groupId;
						
						$this->_db->insert('T_SACTIVITY', $logArr);
						
						$this->_db->commit();
						$this->_redirect('/superadmin/adduser/index/success/success');
					}
					catch(Exception $e)
					{
						echo $e;die;
						$this->_db->rollBack();
					}
						
				}
				else if(count($selectmaster)>0)
				{		
					$this->view->error 			= true;		
					$errors 					= "$user_id is already taken ";
					$this->view->errorMsg 		= $errors;
				}
			}
			else
			{
				$this->view->error = true;
				$docErr = "Error in processing form values. Please correct values and re-submit.";
				$this->view->errorMsg = $docErr;
				
				if($cekpass == false)
				{
					$this->view->errPass = $errPass; 
				}
				
				if($cekconfirmpass == false)
				{
					$this->view->errConfirm = $errConfirm; 
				}
				
				if($cekkombinasi == false)
				{
					$err = array('Invalid user Id format');
					$this->view->xuserId = $err;
				}
				
				foreach($zf_filter_input->getMessages() as $key=>$err){
					$xxx = 'x'.$key;
					foreach($err as $key2=>$err2)
					{
						$this->view->$xxx = $err2;
					}
				}
				
				$this->view->userId 	= ($zf_filter_input->isValid('userId'))? $zf_filter_input->userId : $params['userId'];
				$this->view->name	 	= ($zf_filter_input->isValid('name'))? $zf_filter_input->name : $params['name'];
				$this->view->desc 		= $params['desc'];
				$this->view->groupId	= ($zf_filter_input->isValid('groupId'))? $zf_filter_input->groupId : $params['groupId'];
//				echo $docErr,die;
			}	
		}
			
		$desc 		 = $this->_request->getParam('desc');
		$desc_len 	 = (isset($desc))  ? strlen($desc)  : 0;			
		$desc_len 	 = 200 - $desc_len;	

		$this->view->desc_len		= $desc_len;
			
	}
}
