<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/SystemBalance.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'General/BankUser.php';

class myaccount_IndexController extends myaccount_Model_Myaccount
{

	private $minLengthPassword;
	private $maxLengthPassword;
	private $bankName;
	private $userData;

	public function initController()
	{
		$settings =  new Settings();

		$this->minLengthPassword = $settings->getSetting('minbpassword');
		$this->maxLengthPassword = $settings->getSetting('maxbpassword');
		$this->bankName = $settings->getSetting('master_bank_name');

		//get user data                        
		$resultData = $this->getUserData($this->_userIdLogin);
		foreach ($resultData as $key => $value) {
			if (empty($value)) {
				$resultData[$key] = '-';
			}
		}

		$this->userData = $resultData;
	}

	public function indexAction()
	{
		$setting = new Settings();
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$pw_hash = md5($enc_salt . $enc_pass);
		$rand = $this->_userIdLogin . date('dHis') . $pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;
		$password = $sessionNamespace->token;

		$this->_helper->layout()->setLayout('newlayout');

		$this->view->minlengthpass = $this->minLengthPassword;
		$this->view->maxlengthpass = $this->maxLengthPassword;
		$this->view->bankName = $this->bankName;

		$this->view->userData = $this->userData;

		//-------------------------------------------user privilege-------------------------------------------------------------
		$privil = $this->_db->select()
			->FROM('M_BPRIVILEGE')
			->ORDER('BPRIVI_DESC ASC')
			->query()->fetchall();

		$select4 = $this->_db->select()
			->from(array('M_BPRIVI_GROUP'));
		$select4->where("BGROUP_ID LIKE " . $this->_db->quote($this->userData['BGROUP_ID']));
		$result4 = $select4->query()->FetchAll();

		foreach ($privil as $row) {
			// print_r($result4);die;
			foreach ($result4 as $row2) {
				if ($row2['BPRIVI_ID'] == $row['BPRIVI_ID']) {
					$this->view->$row['BPRIVI_ID'] = '1';
					// break;
				}
			}
		}

		$this->view->privil = $privil;
		$this->view->prev = $result4;

		//-------------------------------------------end user privilege-------------------------------------------------------------

	}

	public function changeemailAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$email = $this->_getParam('email');
		$pass = $this->_getParam('pass');
		$username = strtolower($this->userData["BUSER_ID"]);

		$curPass =  $this->userData['BUSER_PASSWORD'];

		$key = md5($this->_userIdLogin);
		$encrypt = new Crypt_AESMYSQL();
		$password = md5($encrypt->encrypt($pass, $key));

		$authAdapter = new SGO_Auth_Adapter_Ldap($username, $pass);

		#$result = $authAdapter->authenticate($authAdapter);
		$result = $authAdapter->verifyldap();
		$resDataLDAP = json_decode($result, true);

		$select = $this->_db->select()
			->from('M_BUSER', array('BUSER_EMAIL', 'BUSER_ID'))
			->where('BUSER_EMAIL = ?', $email)
			->where('BUSER_STATUS != 3');

		$data = $this->_db->fetchRow($select);

		$checkDuplicateEmail = false;
		if (empty($data)) {

			$checkDuplicateEmail = true;
		} else {
			if ($data['BUSER_ID'] == $this->_userIdLogin) {
				$checkDuplicateEmail = true;
			}
		}

		if (intval($resDataLDAP["status"]) !== 1) {
			echo "invalidpass";
		} else if (!$checkDuplicateEmail) {
			echo "duplicateemail";
		} else {
			try {
				$where['BUSER_ID = ?'] = $this->_userIdLogin;
				$updateArr = array('BUSER_EMAIL' => $email);
				$this->_db->update('M_BUSER', $updateArr, $where);

				Application_Helper_General::writeLog('COEM', 'Change My Email, User ID : ' . $this->_userIdLogin . ' to ' . $email);

				echo 'success';
			} catch (Exception $e) {
				echo 'failed';
			}
		}
	}

	public function changepassAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$oldpass = $this->_getParam('curpass');
		$newpass = $this->_getParam('newpass');

		$errDesc = array();
		$validator = new Zend_Validate_Alnum();

		if ((strlen($newpass) < $this->minLengthPassword) || (strlen($newpass) > $this->maxLengthPassword))
			$errDesc['newpass'] = $this->language->_('Error: Minimum char') . " " . $this->minLengthPassword . " " . $this->language->_('and maximum char') . " " . $this->maxLengthPassword . " " . $this->language->_('for New Password length') . ".";
		elseif (Application_Helper_General::checkPasswordStrength($newpass) < 3)
			$errDesc['newpass'] = $this->language->_("Error: New Password must containt at least one uppercase character, one lowercase character and one number");
		if ($newpass == $oldpass) {
			$errDesc['newpass'] = $this->language->_("Error: Password must be different from old password");
		} elseif (!($validator->isValid($newpass))) {
			$errDesc['newpass'] = $this->language->_("Error: New Password may not contain whitespaces or special characters");
		}


		$BankUser =  new BankUser($this->_userIdLogin);
		$result = array();
		$failed = 0;
		if (count($errDesc) > 0) $failed = 1;
		$result = $BankUser->changePassword($oldpass, $newpass, $failed);

		if ($result == true) {
			Application_Helper_General::writeLog('COPW', 'Change My Password, User ID : ' . $this->_userIdLogin);
		}

		if ((is_array($result)  && $result !== true) ||  $failed == 1) {
			if (count($result) > 0) {
				foreach ($result as $key => $value) {
					$errDesc['errorMsg'][] = $value;
				}
			}

			$errDesc['result'] = 'failed';
		} else {

			$errDesc['result'] = 'success';
		}

		if (isset($errDesc['newpass'])) {
			$errDesc['errorMsg'][] = $errDesc['newpass'];
		}

		unset($errDesc['newpass']);
		unset($errDesc['oldpass']);

		echo json_encode($errDesc);
	}

	public function changependingtaskAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$value = $this->_getParam('value');

		try {
			$where['BUSER_ID = ?'] = $this->_userIdLogin;
			$updateArr = array('BUSER_PENDING_NOTIF' => $value);
			$this->_db->update('M_BUSER', $updateArr, $where);

			echo 'success';
		} catch (Exception $e) {
			echo 'failed';
		}
	}
}
