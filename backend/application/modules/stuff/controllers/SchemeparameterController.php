<?php

class stuff_SchemeparameterController extends Application_Main
{
    public function indexAction() 
    { 
	
		$listYesNo = array('0' => 'No',
										'1' => 'Yes',						   											
										);	
		
		$listPaymentSource = array(
														   '0' => 'Combination',
														   '1' => 'PRK',
														   '2' => 'GIRO'
														  );
		
		$listFrequency = array('1' => 'Monthly',
							   '2' => 'Bi-Monthly',
							   '3' => 'Quarterly',
							   '6' => 'Semester',
							   '0.25' => 'Weekly',
							   '0.5' => 'Bi-Weekly'
							  );

		$listDisburseMode = array('1' => 'Online',
								 // '0' => 'Offline'
								 );

		$getCcy = Application_Helper_Array::listArray($this->getCcy(),'CCY_ID','CCY_ID');
		$this->view->ccyList =  array_merge(array(''=>'-- Any Value --'),$getCcy);

		$this->view->listYesNo = $listYesNo;		
		$this->view->listPaymentSource = $listPaymentSource;		
		$this->view->listFrequency = $listFrequency;		
		$this->view->listDisburseMode = $listDisburseMode;			
    }
}