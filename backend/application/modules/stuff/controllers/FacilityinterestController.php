<?php

class stuff_FacilityinterestController extends Application_Main
{
    public function indexAction() 
    { 		
		$listDisbChargestype = array('0' => 'Interest',
								 '1' => 'Transaction Fee',
								 '2' => 'Interest & Transaction Fee'
								);
								
		// $listChargestype = array('0' => 'Interest',
								 // '1' => 'Transaction Fee'
								// );
		
		// $listTrxFee = array('0' => 'Percentage',
							// '1' => 'Fix Amount'
						   // );
		
		// $listDisbType = array('1' => 'On Disbursement (Upfront)',
							  // '2' => 'On Maturity Date (After)'
							 // );
		
		// $listTenorType = array('1' => 'Fee On Invoice Maturity Date (Upfront)',
							   // '2' => 'Fee On Bank Maturity Date (After)'
							  // );
		
		// $listGraceType = array('1' => 'Fee On Bank Maturity Date (Upfront)',
							   // '2' => 'Fee On End Of Grace Period (After)'
							  // );
		
		// $listPenaltyType = array('1' => 'Fee On End Of Grace Period (Upfront)',
							     // '2' => 'Fee On End Of Penalty (After)'
							    // );

		$listYesNo = array('0' => 'No',
										'1' => 'Yes',						   											
										);	
										
		$getCcy = Application_Helper_Array::listArray($this->getCcy(),'CCY_ID','CCY_ID');
		$this->view->ccyList =  array_merge(array(''=>'-- Any Value --'),$getCcy);
		//$getccyid = $this->getCcyId();		
		
		$this->view->listDisbChargestype = $listDisbChargestype;			
		// $this->view->listDisbType = $listDisbType;			
		// $this->view->listTenorType = $listTenorType;			
		// $this->view->listGraceType = $listGraceType;			
		// $this->view->listPenaltyType = $listPenaltyType;			
		// $this->view->listChargestype = $listChargestype;			
		// $this->view->listTrxFee = $listTrxFee;			
		$this->view->listYesNo = $listYesNo;	
    }
}