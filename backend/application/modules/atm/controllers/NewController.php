<?php

require_once 'Zend/Controller/Action.php';

class Atm_NewController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti
	
	
    public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
	    //pengaturan url untuk button back
	    //$this->setbackURL('/'.$this->_request->getModuleName().'/index');    
	    $this->setbackURL('/country/index');    

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}

    	$this->view->report_msg = array();
		if($this->_request->isPost() )
		{
			$filters = array(
			                 'atm_code' => array('StringTrim','StripTags'),
							 'atm_address' => array('StringTrim','StripTags'),
            			    'city_name' => array('StringTrim','StripTags'),
            			    
							);

			$validators = array('atm_code' => array('NotEmpty',
			                                         array('Db_NoRecordExists', array('table' => 'M_ATM', 'field' => 'ATM_CODE')),
													 'messages' => array(
																         $this->language->_('Can not be empty'),
																         $this->language->_('ATM Code already existed')
			                                                             )
													),
			                    
								'atm_address'      => array('NotEmpty',
													 'messages' => array(
																		 $this->language->_('Can not be empty')
																         )
														),
                			    'city_name'      => array('NotEmpty',
                                    			        'messages' => array(
                                    			            $this->language->_('Can not be empty')
                                    			        )
                                    			    )
							   );
		
							   
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

			if($zf_filter_input->isValid())
			{
			    // die;
				$content = array(
								'ATM_CODE' 	 => $zf_filter_input->atm_code,
								'ATM_ADDRESS' 	 => $zf_filter_input->atm_address,
            				    'ATM_CITY' 	 => $zf_filter_input->city_name
            				    
						       );
						       
				try 
				{
					// print_r($content);die;
					//-------- insert --------------
					$this->_db->beginTransaction();
					
					$this->_db->insert('M_ATM', $content);
// 					die;
					$this->_db->commit();
					//$id = $this->_db->lastInsertId();
					Application_Helper_General::writeLog('COAD','Add New ATM. ATM Code : ['.$zf_filter_input->code_name.'], Atm Address : ['.$zf_filter_input->atm_address.']');
					$this->view->success = true;
					$this->view->report_msg = array();
					
					// $this->_redirect('/notification/success/index');
				}
				catch(Exception $e) 
				{
					//rollback changes
					$this->_db->rollBack();
					
					foreach(array_keys($filters) as $field)
						$this->view->$field = $zf_filter_input->getEscaped($field);

					$errorMsg = 'exeption';
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
				}
			}
			else
			{
				$this->view->error = true;
				foreach(array_keys($filters) as $field)
						$this->view->$field = ($zf_filter_input->isValid($field))? $zf_filter_input->getEscaped($field) : $this->_getParam($field);

				$error = $zf_filter_input->getMessages();
				
				//format error utk ditampilkan di view html 
                $errorArray = null;
		        foreach($error as $keyRoot => $rowError)
		        {
		           foreach($rowError as $errorString)
		           {
		              $errorArray[$keyRoot] = $errorString;
		           }
		        }
        
		        $this->view->succes = false;
                $this->view->report_msg = $errorArray;
			}
		}
		
		Application_Helper_General::writeLog('COAD','Add New ATM');
	}

}