<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';

class popup_DocumentunderlyingController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti
	
	public function initController()
	{       
		$listCcy = array(''=>'-- Select Document --');
		$listCcy = array_merge($listCcy,Application_Helper_Array::listArray($this->getCcy(),'CCY_ID','DESCRIPTION'));
		$this->view->ccy = $listCcy;
		$this->_helper->layout()->setLayout('popup');
	}

	public function indexAction()
	{
		$fields = array(
						/*'alias'  => array('field' => 'BENEFICIARY_ALIAS',
											   'label' => $this->language->_('Alias Name'),
											   'sortable' => true),*/
						'document_reff'  => array('field' => 'DOCUMENT_NUMBER',
											   'label' => $this->language->_('Document Reff '),
											   'sortable' => true),
						'file_name'  => array('field' => 'FILE_NAME',
											   'label' => $this->language->_('File Name '),
											   'sortable' => true),
						'document_desc'  => array('field' => 'FILE_DESCRIPTION',
											   'label' => $this->language->_('Document Description '),
											   'sortable' => true)
				);
				
		//get page, sortby, sortdir
		$currency    = $this->_getParam('currency');
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','favorite');
		$sortDir = $this->_getParam('sortdir','desc');
		$id_box = $this->_getParam('id_box');
		$custid = $this->_getParam('custid');
		$moduleid = $this->_getParam('moduleid');
		//echo $id_box;
		//Zend_Debug::dump($this->_getParam());
		//exit();
		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';
	  

		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	'filter' 	  	=> array('StringTrim','StripTags'),
							'alias' 	  	=> array('StringTrim','StripTags'),
							'doc_reff'    => array('StringTrim','StripTags','StringToUpper'),
							'doc_name'    => array('StringTrim','StripTags'),
							'favorit'     	=> array('StringTrim','StripTags')
		);
		
		$zf_filter = new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		$filter = $zf_filter->getEscaped('filter');

		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		$this->view->id_box = $id_box;
		$this->view->currency = $currency;
		$this->view->custid = $custid;
		$this->view->moduleid = $moduleid;

        $select = $this->_db->select()
                    ->distinct()
                    ->from(	
                            array('TFS'=>'T_FILE_SUBMIT'),
                            array(
                                    'FILE_ID'			=>'TFS.FILE_ID',
                                    'FILE_NAME'			=>'TFS.FILE_NAME',
                                    'FILE_DESCRIPTION'	=>'TFS.FILE_DESCRIPTION',
                                    'DOCUMENT_NUMBER'	=>'TFS.DOCUMENT_NUMBER',											
                                    'FILE_UPLOADED_TIME'=>'TFS.FILE_UPLOADED_TIME',
                            ))
					->where('CUST_ID =?', $custid)
					->where('FILE_MODULE =?', $moduleid);

		if($filter == TRUE)
		{
			
			$dreff = $zf_filter->getEscaped('doc_reff');
			$ddesc = $zf_filter->getEscaped('doc_name');
			$fFav = $zf_filter->getEscaped('favorit');
			
	        
	        if($dreff)$select->where('DOCUMENT_NUMBER LIKE '.$this->_db->quote('%'.$dreff.'%'));
	        if($ddesc)$select->where('FILE_NAME LIKE '.$this->_db->quote('%'.$ddesc.'%'));
	        
			$this->view->alias = $fAlias;
			$this->view->doc_reff = $dreff;
			$this->view->doc_name = $ddesc;
			if($fFav==1)$this->view->favorit = true;
			else $this->view->favorit = false;
		}
		else $this->view->favorit = false;
		//print_r($select);die;
	    $select->order($sortBy.' '.$sortDir);   
		$this->paging($select);
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		$this->view->dateTimeDisplayFormat = $this->_dateTimeDisplayFormat;
	    
		//$this->_helper->download->csv(array('Group ID','Group Name','Status'),null,$select,'download group');
	}

}
