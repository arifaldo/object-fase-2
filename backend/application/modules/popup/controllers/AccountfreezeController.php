<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'General/SystemBalance.php';

class Popup_AccountfreezeController extends Application_Main
{
	protected $_moduleDB = 'RTF'; // masih harus diganti
	
	public function initController(){	
		$this->_helper->layout()->setLayout('popup');
	}

	public function indexAction(){
	
		$fields = array(
						'paymentref'  		=> array(	'field' 	=> 'ACCT_NO',
													'label' 	=> 'Payment Ref',
													'sortable' 	=> true),
						'invoiceno'  	=> array(	'field' 	=> 'ACCT_NAME',
													'label' 	=> 'Invoice No',
													'sortable' 	=> true),
						'action'  	=> array(	'field' 	=> 'ACCT_NAME',
													'label' 	=> 'Action',
													'sortable' 	=> true),
						'chargeaccount'   		=> array(	'field'    => 'CCY_ID',
													'label'    	=> 'Charge Account',
													'sortable' 	=> true),
						'chargeamount'   	=> array(	'field'    => '',
													'label'    => 'Charge Amount',
													'sortable' => true),
						'chargematuritydate'   		=> array(	'field'    => '',
													'label'    => 'Charge Maturity Date',
													'sortable' => true),
						'chargetype'   		=> array(	'field'    => '',
													'label'    => 'Charge Type',
													'sortable' => true),
				);
		
		/*
			ACCOUNT_BANK_NAME 	= $systemBalance->getCoreAccountName();
			EFFECTIVEBALANCE 	= $systemBalance->getEffectiveBalance();
			
		*/

		// $CustomerUser 	= new CustomerUser($cust_id, $user_id);
		// $resultaccounts = $CustomerUser->getAccounts();
		// $resultaccounts = Application_Helper_Array::simpleArray($resultaccounts, 'ACCT_NO');
		$account =  $this->_getParam('account');
		// if(in_array($account,$resultaccounts)){
		if(!empty($account))
		{
				$data = $this->_db->fetchAll("select c.PS_NUMBER as 'Payment Ref#', b.SOURCE_ACCOUNT_CCY as CCY, DOC_NO as 'Invoice No', LOG_DATETIME as 'Transaction Date','Freezed' as 'Action',b.SOURCE_ACCOUNT as 'Charge Account',b.TRA_AMOUNT as 'Charge Amount',b.TRA_DATE as 'Charge Maturity Date',b.FEE_TYPE as 'Charges type' from T_CHARGES_LOG a 
  inner join t_tx_fee b on a.TRANSACTION_ID =  CAST( b.FEE_ID as varchar)
  inner join T_TX c on b.PS_NUMBER = c.PS_NUMBER 
  where ACCT_NO =? and b.TRA_STATUS = 4 ORDER BY LOG_DATETIME DESC ",$account );
		}else{
				$data =  array();
		}
	
		$this->view->resultdata = $data;
	}
}
