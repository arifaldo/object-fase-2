<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/SinglePayment.php';
class popup_BanknostroController extends Application_Main
{
	 protected $_moduleDB = 'RTF'; //masih harus diganti

	public function initController()
	{

		$this->_helper->layout()->setLayout('newpopup');
	}

	public function indexAction()
	{

	    $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		 if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}
    	}



	    $fields = array(
						'bank_name'      => array('field' => 'NOSTRO_NAME',
											      'label' => 'Bank Name',
											      'sortable' => true),
						'city'           => array('field' => 'NOSTRO_SWIFTCODE',
											      'label' => 'Bank Code',
											      'sortable' => true),
						'currency'  => array('field' => 'CCY',
											      'label' => 'Currency',
											      'sortable' => false),
						/*'swift_code'     => array('field' => 'SWIFT_CODE',
											      'label' => 'Swift Code',
											      'sortable' => true)*/
	                  );

		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','doc_no');
		$sortDir = $this->_getParam('sortdir','asc');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;

		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';


		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	'filter' 	  	 => array('StringTrim','StripTags'),
							//'clearing_code'  => array('StringTrim','StripTags'),
							'city'           => array('StringTrim','StripTags','StringToUpper'),
							'bank_name'      => array('StringTrim','StripTags'),
							//'swift_code'     => array('StringTrim','StripTags')
		);


		$zf_filter = new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		$filter = $zf_filter->getEscaped('filter');

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;

		$select = $this->_db->select()
					        ->from(array('A' => 'M_NOSTRO_BANK'));
// 		$select->where("A.BANK_ISDISPLAYED = 1");


		if($filter == 'Set Filter')
		{
			//$fclearing_code = $zf_filter->getEscaped('clearing_code');
			$fbank_name     = $zf_filter->getEscaped('bank_name');
			//$fswift_code    = $zf_filter->getEscaped('swift_code');
// 			$fcity          = $zf_filter->getEscaped('city');

	        //if($fclearing_code) $select->where('UPPER(CLR_CODE) LIKE '.$this->_db->quote('%'.strtoupper($fclearing_code).'%'));
	        if($fbank_name)     $select->where('NOSTRO_NAME LIKE '.$this->_db->quote('%'.strtoupper($fbank_name).'%'));
	        //if($fswift_code)    $select->where('UPPER(SWIFT_CODE) LIKE '.$this->_db->quote('%'.strtoupper($fswift_code).'%'));
// 	        if($fcity)          $select->where('UPPER(CITY) LIKE '.$this->_db->quote('%'.strtoupper($fcity).'%'));

			//$this->view->clearing_code = $fclearing_code;
			$this->view->bank_name     = $fbank_name;
			//$this->view->swift_code    = $fswift_code;
// 			$this->view->city          = $fcity;

		}

		//$this->view->success = true;


	    $select->order($sortBy.' '.$sortDir);
		$this->paging($select);
// 		$this->frontendLog('V',$this->_moduleDB,null);
		$this->view->fields = $fields;
		$this->view->filter = $filter;

		//$this->_helper->download->csv(array('Group ID','Group Name','Status'),null,$select,'download group');
	}

}
