<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/SinglePayment.php';
class popup_SubmissionreportpopupController extends Application_Main
{
	 protected $_moduleDB = 'RTF'; //masih harus diganti

	public function initController()
	{       
	
		$this->_helper->layout()->setLayout('popup');
	}

	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newpopup');
		$FILE_ID   	= $this->_getParam('FILE_ID');
		
		$select =	$this->_db->select()
								->from	(
											array('TFS'=>'T_FILE_SUBMIT'),
											array(
													'FILE_ID'			=>'TFS.FILE_ID',
													'FILE_NAME'			=>'TFS.FILE_NAME',
													'USER_LOGIN'		=>'TFS.USER_LOGIN',
													'CUST_ID'			=>'TFS.CUST_ID',
													'FILE_UPLOADED_TIME'=>'TFS.FILE_UPLOADED_TIME',
													'FILE_DELETED'		=>'TFS.FILE_DELETED',
													'FILE_DELETEDBY'	=>'TFS.FILE_DELETEDBY',
													'FILE_DOWNLOADED'	=>'TFS.FILE_DOWNLOADED',
													'FILE_DOWNLOADEDBY'	=>'TFS.FILE_DOWNLOADEDBY',
													'FILE_DESCRIPTION'	=>'TFS.FILE_DESCRIPTION',
													'CUST_NAME'			=>'MC.CUST_NAME'
												)
										)
								->joinLeft	(
												array('MC' => 'M_CUSTOMER'),
												'TFS.CUST_ID = MC.CUST_ID',
												array('CUST_NAME')
											);
		$select->where("UPPER(TFS.FILE_ID) LIKE ".$this->_db->quote($FILE_ID));
		$arr = $this->_db->fetchRow($select);
		$this->view->data = $arr;
		$this->view->FILE_ID = $FILE_ID;
		//Zend_Debug::dump($arr);die;
		$filterArr = array(	'pdf' 	  		=> array('StringTrim','StripTags'),
						);
		$zf_filter 	= new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		
		$pdf 		= $zf_filter->getEscaped('pdf');
		
		if($pdf)
		{
			$status = "No";
			if($arr['FILE_DELETED']==1)
			{
				$status = "yes";
			}
			echo($status);
			
			$htmldata = '
							<tr><th colspan="3"><strong>File Sharing Report Detail</strong></th></tr>
							<tr>
							  <td class="tbl-evencontent" width="150px">Company Code</td>
							  <td class="tbl-evencontent" width="5px">:</td>
							  <td class="tbl-evencontent">'.$arr['CUST_ID'].'</td>
							</tr>
							<tr>
							  <td class="tbl-evencontent" width="150px">Company Name</td>
							  <td class="tbl-evencontent" width="5px">:</td>
							  <td class="tbl-evencontent">'.$arr['CUST_NAME'].'</td>
							</tr>
							<tr>
							  <td class="tbl-evencontent" width="150px">File Name</td>
							  <td class="tbl-evencontent" width="5px">:</td>
							  <td class="tbl-evencontent">'.$arr['FILE_NAME'].'</td>
							</tr>
							<tr>
							  <td class="tbl-evencontent" width="150px">File Description</td>
							  <td class="tbl-evencontent" width="5px">:</td>
							  <td class="tbl-evencontent">'.$arr['FILE_DESCRIPTION'].'</td>
							</tr>
							<tr>
							  <td class="tbl-evencontent" width="150px">Uploaded By</td>
							  <td class="tbl-evencontent" width="5px">:</td>
							  <td class="tbl-evencontent">'.$arr['USER_LOGIN'].'</td>
							</tr>
							<tr>
							  <td class="tbl-evencontent" width="150px">Uploaded Date</td>
							  <td class="tbl-evencontent" width="5px">:</td>
							  <td class="tbl-evencontent">'.Application_Helper_General::convertDate($arr["FILE_UPLOADED_TIME"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat).'</td>
							</tr>
							<tr>
							  <td class="tbl-evencontent" width="150px">Downloaded</td>
							  <td class="tbl-evencontent" width="5px">:</td>
							  <td class="tbl-evencontent">'.$arr['FILE_DOWNLOADED'].' time(s)</td>
							</tr>
							<tr>
							  <td class="tbl-evencontent" width="150px">Last Download By</td>
							  <td class="tbl-evencontent" width="5px">:</td>
							  <td class="tbl-evencontent">'.$arr['FILE_DOWNLOADEDBY'].'</td>
							</tr>
							<tr>
							  <td class="tbl-evencontent" width="150px">File is Deleted</td>
							  <td class="tbl-evencontent" width="5px">:</td>
							  <td class="tbl-evencontent">'.$status.'</td>
							</tr>
							<tr>
							  <td class="tbl-evencontent" width="150px">Deleted By</td>
							  <td class="tbl-evencontent" width="5px">:</td>
							  <td class="tbl-evencontent">'.$arr['FILE_DELETEDBY'].'</td>
							</tr>
						';
			$this->_helper->download->pdf(null,null,null,'File Sharing Report Detail',$htmldata);    
			Application_Helper_General::writeLog('SPSB','Download PDF File Sharing Detail');
		}
		else
		{
			Application_Helper_General::writeLog('SPSB','View File Sharing Detail');
		}
	}

}
