<?php
require_once 'Zend/Controller/Action.php';
class helplist_IndexController extends Application_Main {
	
	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
		
		$arr = null;
		$viewFilter = null;
		$errors = null;
		
		$fields = array	(
							'HelpTopic'  			=> array	(
																	'field' => 'HELP_TOPIC',
																	'label' => $this->language->_('Guide Topic'),
																	'sortable' => true
																),
							'FileType'  			=> array	(
																	'field' => 'HELP_TYPE',
																	'label' => $this->language->_('Type'),
																	'sortable' => true
																),
							'FileDescription'  					=> array	(
																		'field' => 'HELP_DESCRIPTION',
																		'label' => $this->language->_('Description'),
																		'sortable' => true
																	),
							/*'FileName'  			=> array	(
																	'field' => 'HELP_FILENAME',
																	'label' => $this->language->_('File Name'),
																	'sortable' => true
																),*/
							'Uploaded By'  					=> array	(
																		'field' => 'UPLOADED_BY',
																		'label' => $this->language->_('Uploaded By'),
																		'sortable' => true
																	),	
							'Uploaded Date'  					=> array	(
																		'field' => 'UPLOAD_DATETIME',
																		'label' => $this->language->_('Uploaded Date'),
																		'sortable' => true
																	),	
						);
		$this->view->fields = $fields;


		$filterlist = array('SEARCH_TEXT','HELP_TOPIC','UPDATED_BY','UPLOAD_DATE');
		
		$this->view->filterlist = $filterlist;
	  
		
		$filterArr = array(	'filter' 	  	=> array('StringTrim','StripTags'),
							'SEARCH_TEXT'   => array('StringTrim','StripTags','StringToUpper'),
							'HELP_TOPIC'   	=> array('StringTrim','StripTags','StringToUpper'),
							'UPDATED_BY' 	=> array('StringTrim','StripTags'),
							'UPLOAD_DATE' 	=> array('StringTrim','StripTags'),
							'UPLOAD_DATE_END' 	  	=> array('StringTrim','StripTags'),
							'delete' 	  	=> array('StringTrim','StripTags'),
		);
		
		 $dataParam = array("SEARCH_TEXT","HELP_TOPIC","UPDATED_BY");
		$dataParamValue = array();
		// print_r($this->_request->getParam('wherecol'));die;
		foreach ($dataParam as $dtParam)
		{
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}
				
			}

		}

		if(!empty($this->_request->getParam('uploaddate'))){
				$createarr = $this->_request->getParam('uploaddate');
					$dataParamValue['UPLOAD_DATE'] = $createarr[0];
					$dataParamValue['UPLOAD_DATE_END'] = $createarr[1];
			}
		
		$zf_filter = new Zend_Filter_Input($filterArr,array(),$dataParamValue);
		// $filter = $zf_filter->getEscaped('filter');
		$filter 		= $this->_getParam('filter');

		// $zf_filter 	= new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		
		$page = $this->_getParam('page');
		$page = (Zend_Validate::is($page,'Digits'))? $page : 1;
		$sortBy  = $this->_getParam('sortby');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
		
		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		
		// $filter 	= $zf_filter->getEscaped('filter');
		$delete 	= $this->_request->getParam('delete');
		
		if($delete)
		{
			
			$postreq_id	= $this->_request->getParam('req_id');
			if($postreq_id)
			{
				foreach ($postreq_id as $key => $value) 
				{
					if($postreq_id[$key]==0)
					{
						unset($postreq_id[$key]);
					}
					
				}
			}
			
			if($postreq_id == null)
			{
				$params['req_id'] = null;
			}
			else
			{
				$params['req_id'] = 1;
			}
			
			$validators = array	(
										'req_id' => 	array	(	
																	'NotEmpty',
																	'messages' => array	(
																							'Error File ID Submitted',
																						)
																),
									);
			$filtersVal = array	( 	
									'req_id' => array('StringTrim','StripTags')
								);
			$zf_filter_input = new Zend_Filter_Input($filtersVal,$validators,$params,$this->_optionsValidator);
			
			if($zf_filter_input->isValid())
			{			
				try
				{

					foreach ($postreq_id as  $key =>$value) 
					{		
						$HELP_ID_DELETE =  $postreq_id[$key];
				
						$this->_db->beginTransaction();
						$param = array('HELP_ISDELETED'=> '1',);	
						
						$where = array('HELP_ID = ?' => $HELP_ID_DELETE);
						$query = $this->_db->update ( "T_HELP", $param, $where );
						$this->_db->commit();
					}
					$id = implode(",", $postreq_id);
					Application_Helper_General::writeLog('HLUD','Delete Help. Help ID: ['.$id.']');
					$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
					$this->_redirect('/notification/success');
				}
				catch(Exception $e)
				{
					$this->_db->rollBack();
				}
			}
			else
			{

				$error 			= true;
				$errors 		= $zf_filter_input->getMessages();
				$req_idErr 		= (isset($errors['req_id']))? $errors['req_id'] : null;
				
				$this->_redirect("/helplist/index?error=true&req_idErr=$req_idErr&filter=Filter");
			}
			
			//$filter = 'Filter';
			
		}
		
		if($this->_getParam('error'))
		{
			$this->view->error 			= $this->_getParam('error');
			
			$this->view->req_idErr 		= $this->_getParam('req_idErr');
			
		}
		
		//if($filter)
		//{
			$select = $this->_db->select()
								->from(	'T_HELP',
										array(
												'HELP_ID', 
												'HELP_TOPIC',
												'UPLOADED_BY', 
												'UPLOAD_DATETIME', 
												'HELP_DESCRIPTION', 
												'HELP_FILENAME',
												'HELP_SYS_FILENAME'))
								->where("HELP_ISDELETED != 1");
								
		//}
		
		
		$HELP_ID   	= $this->_getParam('HELP_ID');
		
		if($HELP_ID)
		{
			$select->where('HELP_ID =?',$HELP_ID);
			$data = $this->_db->fetchRow($select);
			$attahmentDestination = UPLOAD_PATH . '/document/help/';
			$this->_helper->download->file($data['HELP_FILENAME'],$attahmentDestination.$data['HELP_SYS_FILENAME']);
			Application_Helper_General::writeLog('HLDL','Download Help.  Help ID: ['.$HELP_ID.'] , Help Topic: ['.$data['HELP_TOPIC'].']');
		}
		else
		{
			Application_Helper_General::writeLog('HLLS','View help list');
		}
		
		if($filter == TRUE)
		{
			$SEARCH_TEXT   	= $this->_getParam('SEARCH_TEXT');
			$HELP_TOPIC   	= $this->_getParam('HELP_TOPIC');
			$UPLOADED_BY    = $this->_getParam('UPLOADED_BY');
			$DATE_START    	= $zf_filter->getEscaped('UPLOAD_DATE');					
			$DATE_END		= $zf_filter->getEscaped('UPLOAD_DATE_END');					
			

			// print_r($DATE_START);die;
			$DATESTART   			= (Zend_Date::isDate($DATE_START,$this->_dateDisplayFormat))?
									   new Zend_Date($DATE_START,$this->_dateDisplayFormat):
									   false;
			
			$DATEEND    			= (Zend_Date::isDate($DATE_END,$this->_dateDisplayFormat))?
									   new Zend_Date($DATE_END,$this->_dateDisplayFormat):
									   false;
									   
			$description	= $this->_getParam('description');
			
			if(!empty($DATE_START))
			{
				$select->where("DATE (UPLOAD_DATETIME) >= DATE(".$this->_db->quote($DATESTART->toString($this->_dateDBFormat)).")");
			}
			
			if(!empty($DATE_END))
			{
				$select->where("DATE (UPLOAD_DATETIME) <= DATE(".$this->_db->quote($DATEEND->toString($this->_dateDBFormat)).")");
			}
			
			if($description)
			{
				$select->where("UPPER(HELP_DESCRIPTION) LIKE ".$this->_db->quote('%'.$description.'%'));
			}
			
			if($SEARCH_TEXT)
			{
				$select->where("UPPER(HELP_FILENAME) LIKE ".$this->_db->quote('%'.$SEARCH_TEXT.'%'));
			}
			
			if($HELP_TOPIC)
			{
				$select->where("UPPER(HELP_TOPIC) LIKE ".$this->_db->quote('%'.$HELP_TOPIC.'%'));
			}
			
			if($UPLOADED_BY)
			{
				$select->where("UPPER(UPLOADED_BY) LIKE ".$this->_db->quote('%'.$UPLOADED_BY.'%'));
			}
			// echo $select;die;
			$this->view->DATE_END 		= $DATE_END;
			$this->view->DATE_START 	= $DATE_START;
			$this->view->description 	= $description;
			$this->view->SEARCH_TEXT 	= $SEARCH_TEXT;
			$this->view->HELP_TOPIC 	= $HELP_TOPIC;
			$this->view->UPLOADED_BY 	= $UPLOADED_BY;
		}

		$select->order($sortBy.' '.$sortDir);
		$arr = $this->_db->fetchAll($select);

		$this->view->arr = $arr;
		$this->paging($arr);
		unset($dataParamValue['UPLOAD_DATE_END']);
		if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     
      }
	}
}