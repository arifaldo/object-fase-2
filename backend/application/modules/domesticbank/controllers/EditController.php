<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';

class Domesticbank_EditController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti


    public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
	    //pengaturan url untuk button back
	    $this->setbackURL('/'.$this->_request->getModuleName().'/index');

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}
    	}


    	$this->view->report_msg = array();

		if(!$this->_request->isPost())
		{
			$bank_id = $this->_getParam('bank_id');
			$bank_id = (Zend_Validate::is($bank_id,'Digits'))? $bank_id : null;

			if($bank_id)
			{
			  $resultdata = $this->_db->fetchRow(
								$this->_db->select()
									 ->from(array('M_DOMESTIC_BANK_TABLE'))
									 ->where("BANK_ID=?", $bank_id)
							                   );
			  if($resultdata)
			  {
			        $this->view->bank_id        = $resultdata['BANK_ID'];
					$this->view->bank_name      = $resultdata['BANK_NAME'];
					// $this->view->city 	        = $resultdata['CITY'];
					$this->view->clearing_code  = $resultdata['CLR_CODE'];
					$this->view->swift_code   	= $resultdata['SWIFT_CODE'];
					// $this->view->BANK_OFFICE_NAME   = $resultdata['BANK_OFFICE_NAME'];
					// $this->view->BANK_ADDRESS   = $resultdata['BANK_ADDRESS'];
					/*
					$this->view->CITY   	= $resultdata['CITY_CODE'];
					$this->view->WORKFIELD_CODE   	= $resultdata['WORKFIELD_CODE'];
					$this->view->PROVINCE_CODE   	= $resultdata['PROVINCE_CODE'];
					// $this->view->KBI_CODE   	= $resultdata['KBI_CODE'];
					$this->view->BRANCH_CODE   	= $resultdata['BRANCH_CODE'];
					$this->view->BRANCH_NAME   	= $resultdata['BRANCH_NAME'];
				 	$this->view->BI_ACCOUNT   	= $resultdata['BI_ACCOUNT'];
					$this->view->POP_STATUS_CODE	= $resultdata['POP_STATUS_CODE'];
					$this->view->RES_STATUS_CODE   	= $resultdata['RES_STATUS_CODE'];
					$this->view->CLR_STATUS   		= $resultdata['CLR_STATUS'];
					$this->view->CORD_STATUS_CODE  	= $resultdata['CORD_STATUS_CODE'];
					$this->view->BANK_INST_CODE   	= $resultdata['BANK_INST_CODE'];
					$this->view->ACTIVE_DATE   		= Application_Helper_General::convertDate($resultdata['ACTIVE_DATE'], $this->_dateDisplayFormat); */

			  }
			}
			else
			{
			   $error_remark = 'Bank ID not found';

			   $this->_helper->getHelper('FlashMessenger')->addMessage('F');
			   $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
			   $this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName());
			}
		}
		else
		{
			$filters = array(
							 'bank_id'       => array('StringTrim','StripTags'),
			                 'bank_name'     => array('StringTrim','StripTags'),
			                 // 'BANK_OFFICE_NAME'  => array('StringTrim','StripTags'),
			                 // 'BANK_ADDRESS'  => array('StringTrim','StripTags'),
							 // 'city'       => array('StringTrim','StripTags'),
							 'clearing_code' => array('StringTrim','StripTags'),
							 'swift_code'    => array('StringTrim','StripTags'),
							 'CITY'     => array('StringTrim','StripTags'),
							 'WORKFIELD_CODE'=> array('StringTrim','StripTags'),
							 'PROVINCE_CODE' => array('StringTrim','StripTags'),
							 // 'KBI_CODE'		 => array('StringTrim','StripTags'),
							 'BRANCH_CODE'	 => array('StringTrim','StripTags'),
							 'BRANCH_NAME'	 => array('StringTrim','StripTags'),
							/*  'BI_ACCOUNT'	 => array('StringTrim','StripTags'),
							 'POP_STATUS_CODE'	 => array('StringTrim','StripTags'),
							 'RES_STATUS_CODE'	 => array('StringTrim','StripTags'),
							 'CLR_STATUS'	 => array('StringTrim','StripTags'),
							 'CORD_STATUS_CODE'	 => array('StringTrim','StripTags'),
							 'BANK_INST_CODE'	 => array('StringTrim','StripTags'),
							 'ACTIVE_DATE'	 => array('StringTrim','StripTags'), */
							);

			$validators = array(
								'bank_id'  => array('NotEmpty',
													'Digits',
													'messages' => array(
															           $this->language->_('Can not be empty'),
															           $this->language->_('invalid format'))
													                    ),
			                    'bank_name' => array('NotEmpty',
			                                         new Zend_Validate_StringLength(array('max'=>35)),
													 'messages' => array(
																         $this->language->_('Can not be empty'),
																         $this->language->_('Data too long (max 35 chars)'),
			                                                             )
													),
								/* 'BANK_OFFICE_NAME' => array('allowEmpty' => true,
			                                         new Zend_Validate_StringLength(array('max'=>20)),
													 'messages' => array(
																         'Can not be empty',
																         'Data too long (max 20 chars)',
			                                                             )
													),
								'BANK_ADDRESS' => array('allowEmpty' => true,
			                                         new Zend_Validate_StringLength(array('max'=>50)),
													 'messages' => array(
																         'Can not be empty',
																         'Data too long (max 50 chars)',
			                                                             )
													),
													 */
								/* 'city'      => array(//'NotEmpty',
													 'allowEmpty' => true,
													 new Zend_Validate_StringLength(array('max'=>15)),
													 'messages' => array(
																         'Data too long (max 15 chars)',
																         )
														),
								'CITY'      => array(
													'NotEmpty',
													 // 'allowEmpty' => true,
													 new Zend_Validate_StringLength(array('max'=>4)),
													 'messages' => array(
																         'Can not be empty',
																         'Data too long (max 4 chars)',
																         )
														),
								'WORKFIELD_CODE'      => array('NotEmpty',
													 new Zend_Validate_StringLength(array('max'=>1)),
													 'messages' => array(
																										'Can not be empty',
																         'Data too long (max 1 chars)',
																         )
														),
								'PROVINCE_CODE'      => array(
													 'NotEmpty',
													 new Zend_Validate_StringLength(array('max'=>4)),
													 'messages' => array(
																'Can not be empty',
																         'Data too long (max 4 chars)',
																         )
														),
								/* 'KBI_CODE'      => array('NotEmpty',
													 new Zend_Validate_StringLength(array('max'=>3)),
													 'messages' => array(
																         'Data too long (max 3 chars)',
																         )
														),

								'BRANCH_CODE'      => array(
													'NotEmpty',
													 new Zend_Validate_StringLength(array('max'=>4)),
													 'messages' => array(
													 'Can not be empty',
																         'Data too long (max 4 chars)',
																         )
														),
								'BRANCH_NAME' => array(
													'NotEmpty',
			                                         new Zend_Validate_StringLength(array('max'=>30)),
													 'messages' => array(
																         'Can not be empty',
																         'Data too long (max 30 chars)',
			                                                             )
													),
								/* 'BI_ACCOUNT'      => array(
													 'allowEmpty' => true,
													 new Zend_Validate_StringLength(array('max'=>12)),
													 'messages' => array(
																         'Data too long (max 12 chars)',
																         )
														),
								'POP_STATUS_CODE' => array(
													 'allowEmpty' => true,
													 new Zend_Validate_StringLength(array('max'=>1)),
													 'messages' => array(
																         'Data too long (max 1 chars)',
																         )
														),
								'RES_STATUS_CODE' => array(
													 'allowEmpty' => true,
													 new Zend_Validate_StringLength(array('max'=>1)),
													 'messages' => array(
																         'Data too long (max 1 chars)',
																         )
														),
								'CLR_STATUS' => array(
													 'allowEmpty' => true,
													 new Zend_Validate_StringLength(array('max'=>1)),
													 'messages' => array(
																         'Data too long (max 1 chars)',
																         )
														),
								'CORD_STATUS_CODE' => array(
													 'allowEmpty' => true,
													 new Zend_Validate_StringLength(array('max'=>1)),
													 'messages' => array(
																         'Data too long (max 1 chars)',
																         )
														),
								'BANK_INST_CODE' => array(
													 'allowEmpty' => true,
													 new Zend_Validate_StringLength(array('max'=>1)),
													 'messages' => array(
																         'Data too long (max 1 chars)',
																         )
														), */
								'clearing_code'    => array(
															'NotEmpty',
														    new Zend_Validate_StringLength(array('max'=>7)),
														    // array('Db_RecordExists', array('table' => 'M_DOMESTIC_BANK_TABLE', 'field' => 'CLR_CODE')),
															'messages' => array(
																              $this->language->_('Can not be empty'),
																              $this->language->_('Data too long (max 7 chars)'),
																              // 'Clearing Code Already Exist'
														                      )
														   ),

								'swift_code'      => array(
															'NotEmpty',
														   // 'allowEmpty' => true,
															new Zend_Validate_StringLength(array('max'=>15)),
															'messages' => array(
															$this->language->_('Can not be empty'),
																                 $this->language->_('Data too long (max 15 chars)'),
															                   )
														  ),
								/* 'ACTIVE_DATE'      => array(
														   'allowEmpty' => true,
														  ) */
							   );

			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			//Zend_Debug::Dump($this->_request->getParams());die;

			if($zf_filter_input->isValid())
			{
				$content = array(
								'BANK_NAME' 	 => $zf_filter_input->bank_name,
								// 'BANK_OFFICE_NAME'  => $zf_filter_input->BANK_OFFICE_NAME,
								// 'BANK_ADDRESS'   => $zf_filter_input->BANK_ADDRESS,
								// 'CITY' 			 => $zf_filter_input->city,
								'CLR_CODE' 	 	 => $zf_filter_input->clearing_code,
								'SWIFT_CODE'	 => $zf_filter_input->swift_code,
// 								'CITY_CODE' 	 => $zf_filter_input->CITY,
// 								'WORKFIELD_CODE' => $zf_filter_input->WORKFIELD_CODE,
// 								'PROVINCE_CODE'  => $zf_filter_input->PROVINCE_CODE,
								// 'KBI_CODE' 		 => $zf_filter_input->KBI_CODE,
// 								'BRANCH_CODE' 	 => $zf_filter_input->BRANCH_CODE,
// 								'BRANCH_NAME' 	 => $zf_filter_input->BRANCH_NAME,
								/* 'BI_ACCOUNT' 	 => $zf_filter_input->BI_ACCOUNT,
								'CLR_STATUS' 	 => $zf_filter_input->CLR_STATUS,
								'POP_STATUS_CODE'=> $zf_filter_input->POP_STATUS_CODE,
								'RES_STATUS_CODE'=> $zf_filter_input->RES_STATUS_CODE,
								'CORD_STATUS_CODE'=> $zf_filter_input->CORD_STATUS_CODE,
								'BANK_INST_CODE' => $zf_filter_input->BANK_INST_CODE,
								'ACTIVE_DATE'	 => Application_Helper_General::convertDate($zf_filter_input->ACTIVE_DATE, $this->_dateDBFormat, $this->_dateDisplayFormat), */
								// 'BANK_ISMASTER' => 0,
						       );
				$content['BANK_ISDISPLAYED'] = (!empty($zf_filter_input->swift_code)) ? 1 : 0;

				try
				{
				    //-----insert--------------
					$this->_db->beginTransaction();

					$whereArr  = array('BANK_ID = ?'=>$zf_filter_input->bank_id);
					$this->_db->update('M_DOMESTIC_BANK_TABLE',$content,$whereArr);

					$this->_db->commit();

					foreach(array_keys($filters) as $field)
						$this->view->$field = $zf_filter_input->getEscaped($field);

					Application_Helper_General::writeLog('DBUD','Edit Domestic Bank. Bank ID : ['.$zf_filter_input->bank_id.']');
					$this->view->success = true;
					$this->view->report_msg = array();

					$this->_redirect('/notification/success/index');
				}
				catch(Exception $e)
				{
					//rollback changes
					$this->_db->rollBack();

				    foreach(array_keys($filters) as $field)
						$this->view->$field = $zf_filter_input->getEscaped($field);
					$errorMsg = 'exception';
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
				}
			}
			else
			{
				$this->view->error = true;

				foreach(array_keys($filters) as $field)
						$this->view->$field = $zf_filter_input->getEscaped($field);

				$error = $zf_filter_input->getMessages();

				//format error utk ditampilkan di view html
                $errorArray = null;
		        foreach($error as $keyRoot => $rowError)
		        {
		           foreach($rowError as $errorString)
		           {
		              $errorArray[$keyRoot] = $errorString;
		           }
		        }

		        $this->view->succes = false;
                $this->view->report_msg = $errorArray;
			}
		}
		if(!$this->_request->isPost()){
		Application_Helper_General::writeLog('DBUD','Viewing Edit Domestic Bank');
		}
	}

}
