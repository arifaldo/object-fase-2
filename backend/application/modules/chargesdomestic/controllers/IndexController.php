<?php


require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class chargesdomestic_IndexController extends Application_Main
{
	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
	    $password = $sessionNamespace->token; 
	    $this->view->token = $sessionNamespace->token;


	    $AESMYSQL = new Crypt_AESMYSQL();
	    $custid = $AESMYSQL->decrypt($this->_getParam('custid'), $password);
	    $custid = (Zend_Validate::is($custid,'Alnum') && Zend_Validate::is($custid,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $custid : null;
		
		
		$selectcur = $this->_db->select()
			->from(array('A' => 'M_MINAMT_CCY'),
				array('CCY_ID'))
				->order('CCY_ID ASC')
				 -> query() ->fetchAll();
		//Zend_Debug::dump($custid); die;
		$this->view->cur=$selectcur;
		
		$select = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER'),array('*'));
		$select -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		$result = $this->_db->fetchRow($select);
		$this->view->result = $result;		
		
		$custname = $result['CUST_NAME'];
		
		$select2 = $this->_db->select()
							->from('M_CHARGES_OTHER');
		$select2 -> where("CUST_ID LIKE ".$this->_db->quote($custid));
		$select2 -> where("CHARGES_TYPE = '1'");
		$result2 = $this->_db->fetchRow($select2);
		if($result2)
		{
			$cekrtgsccy = $this->_db->select()
							->from(array('A' => 'M_MINAMT_CCY'),array('CCY_ID'))
							->where("CCY_ID LIKE ".$this->_db->quote($result2['CHARGES_CCY']))
				 			->query()->fetchAll();
		
			if($cekrtgsccy)
			{
				$this->view->rtgsccy = $result2['CHARGES_CCY'];
			}
			else
			{
				$this->view->rtgsccy = 'IDR';
			}
			
			$this->view->rtgsamt = $result2['CHARGES_AMT'];
		}
	
		$select3 = $this->_db->select()
							->from('M_CHARGES_OTHER');
		$select3 -> where("CUST_ID LIKE ".$this->_db->quote($custid));
		$select3 -> where("CHARGES_TYPE = '2'");
		$result3 = $this->_db->fetchRow($select3);
		
		if($result3)
		{
			$ceksknccy = $this->_db->select()
							->from(array('A' => 'M_MINAMT_CCY'),array('CCY_ID'))
							->where("CCY_ID LIKE ".$this->_db->quote($result3['CHARGES_CCY']))
				 			->query()->fetchAll();
		
			if($ceksknccy)
			{
				$this->view->sknccy = $result3['CHARGES_CCY'];
			}
			else
			{
				$this->view->sknccy = 'IDR';
			}
			
			$this->view->sknamt = $result3['CHARGES_AMT'];
			
		}
		
		if($result3 && $result2)
		{
			$this->view->default = true;
		}
		
		$template = $this->_db->select()->distinct()
						->from(array('A' => 'M_TEMPLATE_CHARGES_OTHER'),array())
						->join(array('B' => 'M_TEMPLATE_CHARGES_OTHER_DETAIL'),'A.TEMPLATE_ID = B.TEMPLATE_ID' , array('TEMPLATE_NAME'))
						->where("A.STATUS NOT LIKE '3'");
		$template = $this->_db->fetchAll($template);
		$i = 0;
		//die;
		//Zend_Debug::dump($template);die;
		foreach($template as $list)
		{
			$templatelist = $this->_db->select()
					    			->from(array('A' => 'M_TEMPLATE_CHARGES_OTHER_DETAIL'),array('*'));
			$templatelist ->where("TEMPLATE_NAME LIKE ".$this->_db->quote($list['TEMPLATE_NAME']));
			$templatelist = $this->_db->fetchAll($templatelist);
			$tempnameid = 'temp'.$i;
			$this->view->$tempnameid = $list['TEMPLATE_NAME'];
			foreach($templatelist as $list2)
			{
					if($list2['CHARGES_TYPE'] == '1')
					{
						$rtgsamt = 'rtgsamt'.$i;
						$rtgsccy = 'rtgsccy'.$i;
						$this->view->$rtgsamt = $list2['CHARGES_AMT'];
						$this->view->$rtgsccy = $list2['CHARGES_CCY'];
					}
					if($list2['CHARGES_TYPE'] == '2')
					{
						$sknamt = 'sknamt'.$i;
						$sknccy = 'sknccy'.$i;
						$this->view->$sknamt = $list2['CHARGES_AMT'];
						$this->view->$sknccy = $list2['CHARGES_CCY'];
					}
			}
			$i++;
		}
		if($i > 0)
		{
			$this->view->countlist = $i;
		}

/////////////////////////////////////////////////////////////////////////////////////////////		
		$select4 = $this->_db->select()
							->from('TEMP_CHARGES_OTHER');
		$select4 -> where("CUST_ID LIKE ".$this->_db->quote($custid));
		$select4 -> where("CHARGES_TYPE = '1' OR CHARGES_TYPE = '2'");
		$cek = $select4->query()->FetchAll();
		//Zend_Debug::dump($cek); die;

		$submit = $this->_getParam('submit');
		
		if(!$submit)
		{
			Application_Helper_General::writeLog('CHUD','View Update Domestic charges page ('.$custid.')');
		}
		
		if(!$cek)
		{
			if($submit)
			{
		
				$this->_db->beginTransaction();
				try
				{
					$info = 'Charges';
					$info2 = 'Domestic Charges';
					$error = 0;
					
					
					
					$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['edit'],null,'M_CHARGES_OTHER','TEMP_CHARGES_OTHER',$custid,$custname,$custid);
					
					$rtgsamtinsert = $this->_getParam('rtgs');
					$rtgsccyinsert = $this->_getParam('rtgsccy');
					$sknamtinsert = $this->_getParam('skn');
					$sknccyinsert = $this->_getParam('sknccy');

					$domamtinsert = $this->_getParam('dom');
					$domccyinsert = $this->_getParam('domccy');
					
					$this->view->rtgsamt = $rtgsamtinsert;
					$this->view->rtgsccy = $rtgsccyinsert;
					$this->view->sknamt = $sknamtinsert;
					$this->view->sknccy = $sknccyinsert;
					$this->view->domamt = $domamtinsert;
					$this->view->domccy = $domccyinsert;
					
					$rtgsamt = Application_Helper_General::convertDisplayMoney($rtgsamtinsert);
					$temp_rtgsamt = str_replace('.','',$rtgsamt);
					$cek_angka = (Zend_Validate::is($temp_rtgsamt,'Digits')) ? true : false;
					
					$sknamt = Application_Helper_General::convertDisplayMoney($sknamtinsert);
					$temp_sknamt = str_replace('.','',$sknamt);
					$cek_angka2 = (Zend_Validate::is($temp_sknamt,'Digits')) ? true : false;

					$domamt = Application_Helper_General::convertDisplayMoney($domamtinsert);
					$temp_domamt = str_replace('.','',$domamt);
					$cek_angka3 = (Zend_Validate::is($temp_domamt,'Digits')) ? true : false;
					
					$select5 = $this->_db->select()
							->from(array('A' => 'M_MINAMT_CCY'),array('*'))
							->where("CCY_ID LIKE ".$this->_db->quote($rtgsccyinsert))
				 			-> query()->fetchAll();
				 			
				 	$select6 = $this->_db->select()
							->from(array('A' => 'M_MINAMT_CCY'),array('*'))
							->where("CCY_ID LIKE ".$this->_db->quote($sknccyinsert))
				 			-> query()->fetchAll(); 

				 	$select7 = $this->_db->select()
							->from(array('A' => 'M_MINAMT_CCY'),array('*'))
							->where("CCY_ID LIKE ".$this->_db->quote($domccyinsert))
				 			-> query()->fetchAll(); 
					
					if($rtgsamtinsert != null && !$cek_angka)
					{
						$this->view->errrtgsamt = $this->language->_('Amount value has to be numeric');
						$error++;
					}
					
					if($sknamtinsert != null && !$cek_angka2)
					{
						$this->view->errsknamt = $this->language->_('Amount value has to be numeric');
						$error++;
					}

					if($domamtinsert != null && !$cek_angka3)
					{
						$this->view->errdomamt = $this->language->_('Amount value has to be numeric');
						$error++;
					}
					
					if($rtgsamtinsert == null)
					{
						$this->view->errrtgsamt = $this->language->_('Value must be filled');
						$error++;
					}
					
					if($sknamtinsert == null)
					{
						$this->view->errsknamt = $this->language->_('Value must be filled');
						$error++;
					}

					if($domamtinsert == null)
					{
						$this->view->errdomamt = $this->language->_('Value must be filled');
						$error++;
					}
					
					if(!$select5)
					{
						$this->view->errrtgsccy = $this->language->_('Incorrect currency');
						$error++;
					}
					
					if(!$select6)
					{
						$this->view->errsknccy = $this->language->_('Incorrect currency');
						$error++;
					}

					if(!$select7)
					{
						$this->view->errdomccy = $this->language->_('Incorrect currency');
						$error++;
					}

					if($error == 0)
					{
							$data1 = array(
											'CHANGES_ID'	=> $change_id,
											'CUST_ID' 		=> $custid,
											'CHARGES_NO'	=> '1',
											'CHARGES_TYPE'	=> '1',
											'CHARGES_CCY' 	=> $rtgsccyinsert,
											'CHARGES_AMT' 	=> $rtgsamt,
											);

							$this->_db->insert('TEMP_CHARGES_OTHER',$data1);
							//echo 'data1';die;
							$data2 = array(
											'CHANGES_ID'	=> $change_id,
											'CUST_ID' 		=> $custid,
											'CHARGES_NO'	=> '1',
											'CHARGES_TYPE'	=> '2',
											'CHARGES_CCY' 	=> $sknccyinsert,
											'CHARGES_AMT' 	=> $sknamt,
											);
							$this->_db->insert('TEMP_CHARGES_OTHER',$data2);


							$data3 = array(
											'CHANGES_ID'	=> $change_id,
											'CUST_ID' 		=> $custid,
											'CHARGES_NO'	=> '1',
											'CHARGES_TYPE'	=> '8',
											'CHARGES_CCY' 	=> $domccyinsert,
											'CHARGES_AMT' 	=> $domamt,
											);
							$this->_db->insert('TEMP_CHARGES_OTHER',$data3);
							//die;
							$commit = true;
					}
					
					if($error == 0 && $commit)
					{	
						//echo 'commit';die;
						$this->_db->commit();
						$this->setbackURL('/setcompanycharges/detail/index/custid/'.$this->_getParam('custid'));						    
						$this->_redirect('/notification/submited/index');
						Application_Helper_General::writeLog('CHUD','Updating Domestic charges ('.$custid.')');
					}
				}
				catch(Exception $e)
				{
					//echo 'rollback';die;
					$this->_db->rollBack();
				}
			}
			if($cek)
			{			
				$docErr = "*".$this->language->_('No changes allowed for this record while awaiting approval for previous change')."";	
				$this->view->error = $docErr;
				$this->view->changestatus = "disabled";
			}	
		}
	}
}
