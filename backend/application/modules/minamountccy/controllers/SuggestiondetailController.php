<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Settings.php';

class minamountccy_SuggestionDetailController extends Application_Main
{
	public function initController()
	{
		$this->_helper->layout()->setLayout('newpopup');
	}
	public function indexAction()
	{
		$status = $this->_changeStatus;
		$options = array_combine(array_values($status['code']), array_values($status['desc']));

		$changesid = $this->_getparam('changes_id');

		$select = $this->_db->SELECT()
			->FROM(array('A' => 'T_GLOBAL_CHANGES'), array('*'))
			->WHERE("A.CHANGES_ID = ?", $changesid)
			->WHERE("A.CHANGES_STATUS = 'WA' OR A.CHANGES_STATUS = 'RR'");
		$result = $this->_db->fetchRow($select);
		if (!$result) {
			$this->_redirect('/notification/invalid/index');
		}

		$select = $this->_db->select()
			->FROM(array('B' => 'TEMP_MINAMT_CCY'))
			->WHERE('CHANGES_ID = ?', $changesid)
			->query()->FetchAll();
		if (count($select) > 0) {
			$select	= $this->_db->FetchRow("SELECT CHANGES_STATUS , CREATED , CREATED_BY  FROM T_GLOBAL_CHANGES WHERE CHANGES_ID = " . $this->_db->quote($changesid));

			$this->view->created = $select['CREATED'];
			$this->view->createdby = $select['CREATED_BY'];
			foreach ($options as $codestat => $descstat) {
				if ($codestat == $select['CHANGES_STATUS']) {
					$this->view->status = $descstat;
				}
			}

			$this->view->cek = 1;

			//			DATA BARU
			$selectnew = $this->_db->select()
				->FROM(array('B' => 'TEMP_MINAMT_CCY'))
				->query()->FetchAll();
			$this->view->databaru = $selectnew;
		} else {
			$this->view->cek = 0;
		}
		//			DATA LAMA	-- //new jadi Current Value hanya beberapa yg tampil
		//			$selectold = $this->_db->select()
		//					->from(array('A' => 'M_MINAMT_CCY'))
		//					->query()->FetchAll();
		//			$this->view->datalama = $selectold;
		//

		//new jadi Current Value tampil semua
		$selectold = $this->_db->select()
			->from(array('A' => 'M_CURRENCY'), array())
			->joinleft(
				array('B' => 'M_MINAMT_CCY'),
				'A.CCY_ID = B.CCY_ID',
				array('B.CCY_STATUS', 'A.CCY_DESCRIPTION', 'A.CCY_ID', 'A.CCY_NUM', 'B.MIN_TX_AMT', '')
			)
			->query()->FetchAll();
		$this->view->datalama = $selectold;


		$this->view->changes_id = $changesid;
		if (!$this->_request->isPost()) {
			Application_Helper_General::writeLog('CRCL', 'View Suggestion Detail Minimum Amount and Currency Available');
		}
	}
}
