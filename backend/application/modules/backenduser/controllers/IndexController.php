<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';
class Backenduser_IndexController extends Application_Main
{
	public function indexAction()
	{

		$setting = new Settings();
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$pw_hash = md5($enc_salt . $enc_pass);
		$rand = $this->_userIdLogin . date('dHis') . $pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;

		$this->_helper->layout()->setLayout('newlayout');
		$this->view->statusCode = $this->_masterglobalstatus['code'];
		$this->view->statusDesc = $this->_masterglobalstatus['desc'];
		$this->view->modulename = $this->_request->getModuleName();
		$statusarr = array_combine(array_values($this->_masterglobalstatus['code']), array_values($this->_masterglobalstatus['desc']));
		// print_r($statusarr);die;
		$selectGroup =	$this->_db->select()
			->from('M_BGROUP', array('BGROUP_ID', 'BGROUP_DESC'))
			->where("BGROUP_STATUS = '1'");
		$arrGroup = $this->_db->fetchAll($selectGroup);

		$this->view->arrgroup = $arrGroup;



		$fields = array(
			'userid'   			=> array(
				'field'    => 'userid',
				'label'    => $this->language->_('Backend User'),
				'sortable' => true
			),

			// 'username' 			=> array(	'field'    => 'username',
			//                       	'label'    => $this->language->_('Name'),
			//                       	'sortable' => true),

			'groupname'   		=> array(
				'field'    => 'groupname',
				'label'    => $this->language->_('Group'),
				'sortable' => true
			),
			'branch_name'   		=> array(
				'field'    => 'branch_name',
				'label'    => $this->language->_('Branch'),
				'sortable' => true
			),

			'lock'   			=> array(
				'field'    => 'lock',
				'label'    => $this->language->_('User Is Locked'),
				'sortable' => true
			),

			'lastlogin'	=> array(
				'field'    => 'lastlogin',
				'label'    => $this->language->_('Last Login'),
				'sortable' => true
			),

			'suggested'  		=> array(
				'field'    => 'suggested',
				'label'    => $this->language->_('Last Suggested'),
				'sortable' => true
			),

			// 'suggestedby'   	=> array(	'field'    => 'suggestedby',
			// 								'label'    => $this->language->_('Last Suggester'),
			//                                      	'sortable' => true),

			'updated'  		=> array(
				'field'    => 'updated',
				'label'    => $this->language->_('Last Approved'),
				'sortable' => true
			),

			// 'updatedby'   		=> array(	'field'    => 'updatedby',
			//                                   		'label'    => $this->language->_('Last Approver'),
			//                                   		'sortable' => true),			
			'status'   			=> array(
				'field'    => 'status',
				'label'    => $this->language->_('Status'),
				'sortable' => true
			),
		);

		$filterlist = array("USER_ID", "Group", "branch_name", "lock", "Last Login", "Status", "Last Suggestion", "APPROVE_DATE", "SUGESTBY", "APPROVEBY");

		$this->view->filterlist = $filterlist;

		$page    = $this->_getParam('page');
		$csv 	 = $this->_getParam('csv');
		$pdf  	 = $this->_getParam('pdf');
		$sortBy  = $this->_getParam('sortby', 'userid');
		$sortDir = $this->_getParam('sortdir', 'asc');

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;

		$filter = $this->_getParam('filter');
		$filter_clear = $this->_getParam('filter_clear');


		$filterArr = array(
			'filter'	=>  array('StripTags'),
			'USER_ID'   	=>  array('StringTrim', 'StripTags', 'StringToUpper'),
			'Group'	=>  array('StringTrim', 'StripTags'),
			'branch_name'	=>  array('StringTrim', 'StripTags'),
			'lock'	=>  array('StringTrim', 'StripTags'),
			'Last Login'	=>  array('StringTrim', 'StripTags'),
			'Last Login_END'	=>  array('StringTrim', 'StripTags'),
			'Status'   	=>  array('StringTrim', 'StripTags', 'StringToUpper'),
			'APPROVEBY'  =>  array('StringTrim', 'StripTags', 'StringToUpper'),
			'SUGESTBY' =>  array('StringTrim', 'StripTags', 'StringToUpper'),
			'Last Suggestion'	=>  array('StringTrim', 'StripTags'),
			'Last Suggestion_END'	=>  array('StringTrim', 'StripTags'),
			'APPROVE_DATE' =>  array('StringTrim', 'StripTags'),
			'APPROVE_DATE_END'	=>  array('StringTrim', 'StripTags'),
		);

		$validator = array(
			'filter'			 	=> array(),
			'USER_ID'			 	=> array(),
			'Group'			=> array(),
			'branch_name' => array(),
			'lock' => array(),
			'Last Login'			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
			'Last Login_END'			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
			'Status'			 	=> array(),
			'APPROVEBY'			=> array(),
			'SUGESTBY'			=> array(),
			'Last Suggestion'			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
			'Last Suggestion_END'			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
			'APPROVE_DATE' 		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
			'APPROVE_DATE_END' 			=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
		);


		$dataParam = array("USER_ID", "Group", "branch_name", "lock", "Last Login", "Status", "Last Suggestion", "APPROVE_DATE", "SUGESTBY", "APPROVEBY");
		$dataParamValue = array();

		$clean2 = array_diff($this->_request->getParam('wherecol'), $dataParam);
		$dataParam = array_diff($this->_request->getParam('wherecol'), $clean2);
		// print_r($this->_request->getParam('wherecol'));
		foreach ($dataParam as $no => $dtParam) {

			if (!empty($this->_request->getParam('wherecol'))) {
				$dataval = $this->_request->getParam('whereval');
				// print_r($dataval);
				$order = 0;
				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if ($value == "Last Suggestion" || $value == "APPROVE_DATE" || $value == "Last Login") {
						$order--;
					}
					if ($dtParam == $value) {
						$dataParamValue[$dtParam] = $dataval[$order];
					}
					$order++;
				}
			}
		}
		// print_r($dataParamValue);die;
		if (!empty($this->_request->getParam('logindate'))) {
			$createarr = $this->_request->getParam('logindate');
			$dataParamValue['Last Login'] = $createarr[0];
			$dataParamValue['Last Login_END'] = $createarr[1];
		}
		if (!empty($this->_request->getParam('sugestdate'))) {
			$createarr = $this->_request->getParam('sugestdate');
			$dataParamValue['Last Suggestion'] = $createarr[0];
			$dataParamValue['Last Suggestion_END'] = $createarr[1];
		}
		if (!empty($this->_request->getParam('approvedate'))) {
			$updatearr = $this->_request->getParam('approvedate');
			$dataParamValue['APPROVE_DATE'] = $updatearr[0];
			$dataParamValue['APPROVE_DATE_END'] = $updatearr[1];
		}

		$zf_filter = new Zend_Filter_Input($filterArr, $validator, $dataParamValue);
		// $filter 	= $zf_filter->getEscaped('filter');
		$filter 		= $this->_getParam('filter');
		$userid		= html_entity_decode($zf_filter->getEscaped('USER_ID'));
		$groupid 	= html_entity_decode($zf_filter->getEscaped('Group'));
		$status 	= html_entity_decode($zf_filter->getEscaped('Status'));
		$branch_name 	= html_entity_decode($zf_filter->getEscaped('branch_name'));
		$lock 	= html_entity_decode($zf_filter->getEscaped('lock'));
		$approver 	= html_entity_decode($zf_filter->getEscaped('APPROVEBY'));
		$suggestor 	= html_entity_decode($zf_filter->getEscaped('SUGESTBY'));
		$datefrom 	= html_entity_decode($zf_filter->getEscaped('Last Suggestion'));
		$dateto 	= html_entity_decode($zf_filter->getEscaped('Last Suggestion_END'));
		$datefrom2 	= html_entity_decode($zf_filter->getEscaped('APPROVE_DATE'));
		$dateto2 	= html_entity_decode($zf_filter->getEscaped('APPROVE_DATE_END'));
		$datefrom3 	= html_entity_decode($zf_filter->getEscaped('Last Login'));
		$dateto3 	= html_entity_decode($zf_filter->getEscaped('Last Login_END'));

		$caseStatus = "(CASE A.BUSER_STATUS ";
		foreach ($statusarr as $key => $val) {
			$caseStatus .= " WHEN " . $key . " THEN '" . $val . "'";
		}
		$caseStatus .= " END)";

		$select2 = $this->_db->select()
			->from(array('A' => 'M_BUSER'), array())
			->join(array('B' => 'M_BGROUP'), 'A.BGROUP_ID = B.BGROUP_ID', array(
				'userid'		=>	'A.BUSER_ID',
				'status'		=>	$caseStatus,
				'username'		=>	'A.BUSER_NAME',
				'groupname'		=>	'B.BGROUP_DESC',
				'lock'			=>	new Zend_Db_Expr("(CASE A.BUSER_ISLOCKED 
																								        	  								   	WHEN '1' THEN 'Yes' 
																								        	  									WHEN '0' THEN 'No'  
																								        	  									END)"),
				'lastlogin'		=>	'A.BUSER_LASTLOGIN',
				'suggested'		=>	'A.SUGGESTED',
				'suggestedby'	=>	'A.SUGGESTEDBY',
				'updated'		=>	'A.UPDATED',
				'updatedby'		=>	'A.UPDATEDBY',
			))
			->joinleft(array('C' => 'M_BRANCH'), 'A.BUSER_BRANCH = C.ID', array('branch_id' => 'C.ID', 'branch_name' => 'C.BRANCH_NAME'));
		// $select2->where("A.BUSER_STATUS NOT LIKE '3'");
		//echo $select2;die;
		//$result = $select2->query()->FetchAll();
		// Zend_Debug::dump($result);die;
		//if($filter == null || $filter == 'Set Filter')
		if ($filter_clear == true) {
			$datefrom 	= '';
			$dateto 	= '';
			$datefrom2 	= '';
			$dateto2 	= '';
			$datefrom3 	= '';
			$dateto3 	= '';
		}

		if ($filter == TRUE) {

			$this->view->fDateTo    = $dateto;
			$this->view->fDateFrom  = $datefrom;
			$this->view->fDateTo2    = $dateto2;
			$this->view->fDateFrom2  = $datefrom2;
			$this->view->fDateTo3    = $dateto3;
			$this->view->fDateFrom3  = $datefrom3;

			if ($datefrom) {
				$FormatDate 	= new Zend_Date($datefrom, $this->_dateDisplayFormat);
				$datefrom  	= $FormatDate->toString($this->_dateDBFormat);
				$select2->where("DATE(A.SUGGESTED) >= ?", $datefrom);
			}

			if ($dateto) {
				$FormatDate 	= new Zend_Date($dateto, $this->_dateDisplayFormat);
				$dateto  		= $FormatDate->toString($this->_dateDBFormat);
				$select2->where("DATE(A.SUGGESTED) <= ?", $dateto);
			}


			if ($datefrom2) {
				$FormatDate 	= new Zend_Date($datefrom2, $this->_dateDisplayFormat);
				$datefrom2  	= $FormatDate->toString($this->_dateDBFormat);
				$select2->where("DATE(A.UPDATED) >= ?", $datefrom2);
			}

			if ($dateto2) {
				$FormatDate 	= new Zend_Date($dateto2, $this->_dateDisplayFormat);
				$dateto2  		= $FormatDate->toString($this->_dateDBFormat);
				$select2->where("DATE(A.UPDATED) <= ?", $dateto2);
			}

			if ($datefrom3) {
				$FormatDate 	= new Zend_Date($datefrom3, $this->_dateDisplayFormat);
				$datefrom3  	= $FormatDate->toString($this->_dateDBFormat);
				$select2->where("DATE(A.BUSER_LASTLOGIN) >= ?", $datefrom3);
			}

			if ($dateto3) {
				$FormatDate 	= new Zend_Date($dateto3, $this->_dateDisplayFormat);
				$dateto3  		= $FormatDate->toString($this->_dateDBFormat);
				$select2->where("DATE(A.BUSER_LASTLOGIN) <= ?", $dateto3);
			}



			if ($userid != null) {
				$this->view->userid = $userid;
				$select2->where('UPPER(A.BUSER_ID) LIKE ' . $this->_db->quote('%' . strtoupper($userid) . '%'));
			}

			if ($groupid != null) {
				$this->view->groupid = $groupid;
				$select2->where('UPPER(B.BGROUP_ID) LIKE ' . $this->_db->quote('%' . strtoupper($groupid) . '%'));
			}

			if ($status != null) {
				$this->view->status = $status;
				$select2->where('A.BUSER_STATUS LIKE ' . $this->_db->quote($status));
			}


			if ($branch_name != null) {
				$this->view->branch_name = $branch_name;
				$select2->where('A.BUSER_BRANCH LIKE ' . $this->_db->quote($branch_name));
			}

			if ($lock != null) {
				$this->view->lock = $lock;
				$select2->where('A.BUSER_ISLOCKED LIKE ' . $this->_db->quote($lock));
			}

			if ($suggestor != null) {
				$this->view->suggestor = $suggestor;
				$select2->where('UPPER(A.SUGGESTEDBY) LIKE ' . $this->_db->quote('%' . strtoupper($suggestor) . '%'));
			}

			if ($approver != null) {
				$this->view->approver = $approver;
				$select2->where('UPPER(A.UPDATEDBY) LIKE ' . $this->_db->quote('%' . strtoupper($approver) . '%'));
			}
		}

		$select2->order($sortBy . ' ' . $sortDir);;
		//Zend_Debug::dump($select2); die;
		//echo $select2; die;

		$this->paging($select2);
		$this->view->fields = $fields;
		$this->view->filter = $filter;

		if (!empty($dataParamValue)) {
			$filterlistdata = array("Filter");
			foreach ($dataParamValue as $fil => $val) {
				$paramTrx = $fil . " - " . $val;
				array_push($filterlistdata, $paramTrx);
			}
		} else {
			$filterlistdata = array("Filter - None");
		}

		$this->view->data_filter = $filterlistdata;


		if ($csv || $pdf || $this->_request->getParam('print')) {
			$result = $select2->query()->FetchAll();

			foreach ($result as $key => $value) {
				$result[$key]["lastlogin"] = Application_Helper_General::convertDate($value["lastlogin"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
				$result[$key]["suggested"] = Application_Helper_General::convertDate($value["suggested"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
				$result[$key]["updated"] = Application_Helper_General::convertDate($value["updated"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
			}

			if ($csv) {
				Application_Helper_General::writeLog('ADLS', 'Download CSV Backend User List');
				$header = array('User ID', 'Status', 'Name', 'Group', 'User Is Locked', 'Last Login', 'Latest Suggestion', 'Latest Suggester', 'Latest Approval', 'Latest Approver', 'Branch ID', 'Branch Name');
				$listable = array_merge_recursive(array($header), $result);
				$this->_helper->download->csv($filterlistdata, $listable, null, $this->language->_('Backend User List'));
			}

			if ($pdf) {
				Application_Helper_General::writeLog('ADLS', 'Download PDF Backend User List');
				$this->_helper->download->pdf(array($this->language->_('User ID'), $this->language->_('Status'), $this->language->_('Name'), $this->language->_('Group'), $this->language->_('User Is Locked'), $this->language->_('Last Login'), $this->language->_('Latest Suggestion'), $this->language->_('Latest Suggester'), $this->language->_('Latest Approval'), $this->language->_('Latest Approver')), $result, null, $this->language->_('Backend User List'));
			}
			if ($this->_request->getParam('print')) {
				unset($fields['action']);
				$filterlistdatax = $this->_request->getParam('data_filter');
				$this->_forward('print', 'index', 'widget', array('data_content' => $result, 'data_caption' => 'Backend User List', 'data_header' => $fields, 'data_filter' => $filterlistdatax));
			}
		} else {
			Application_Helper_General::writeLog('ADLS', 'Viewing Backend User List');
		}

		unset($dataParamValue['Last Login_END']);
		unset($dataParamValue['Last Suggestion_END']);
		unset($dataParamValue['APPROVE_DATE_END']);
		if (!empty($dataParamValue)) {
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}

			$this->view->wherecol     = $wherecol;
			$this->view->whereval     = $whereval;
		}
	}
}
