<?php
require_once 'Zend/Controller/Action.php';

class backenduser_SuggestiondetailController extends Application_Main
{
	public function indexAction()
	{
		$changeid = $this->_getParam('changes_id');

		$select = $this->_db->select()
			->from(array('A' => 'T_GLOBAL_CHANGES'), array('*'));
		$select->where("A.CHANGES_ID = ?", $changeid);
		$select->where("A.CHANGES_STATUS = 'WA' OR A.CHANGES_STATUS = 'RR'");
		$result = $this->_db->fetchRow($select);
		$this->view->result = $result;

		if (!$result) {
			$this->_redirect('/notification/invalid/index');
		} else {
			$select2 = $this->_db->select()
				->from(array('A' => 'TEMP_BUSER'), array('*'))
				->joinleft(array('C' => 'M_BRANCH'), 'A.BUSER_BRANCH = C.ID', array('*'))
				->joinleft(array('B' => 'M_BGROUP'), 'A.BGROUP_ID = B.BGROUP_ID', array('*'));
			$select2->where("A.CHANGES_ID = ?", $changeid);
			$result2 = $this->_db->fetchRow($select2);
			$this->view->result2 = $result2;

			$buserid = $result2['BUSER_ID'];


			if ($buserid) {
				/*
			$select3 = $this->_db->select()
							        ->from(array('A' => 'M_BUSER'),array('A.*'))
							        ->join(array('B' => 'M_BGROUP'), 'A.BGROUP_ID = B.BGROUP_ID',array('B.*'));
			$select3 -> where("A.BUSER_ID = ?",$buserid);
			$result3 = $this->_db->fetchRow($select3);
			$this->view->result3=$result3;
			*/
				$select3 = $this->_db->select()
					->from(array('A' => 'M_BUSER'), array('*', 'A.CREATED AS USER_CREATED', 'A.CREATEDBY AS USER_CREATEDBY', 'A.SUGGESTED AS USER_SUGGESTED', 'A.SUGGESTEDBY AS USER_SUGGESTEDBY', 'A.UPDATED AS USER_UPDATED', 'A.UPDATEDBY AS USER_UPDATEDBY'))
					->joinleft(array('C' => 'M_BRANCH'), 'A.BUSER_BRANCH = C.ID', array('*'))
					->joinleft(array('B' => 'M_BGROUP'), 'A.BGROUP_ID = B.BGROUP_ID', array('*'))
					->where("BUSER_ID = ?", $buserid);
				$result3 = $this->_db->fetchRow($select3);
				$this->view->result3 = $result3;
			}
		}


		$this->view->typeCode = array_flip($this->_changeType['code']);
		$this->view->typeDesc = $this->_changeType['desc'];
		$this->view->modulename = $this->_request->getModuleName();
		if (!$this->_request->isPost()) {
			Application_Helper_General::writeLog('ADCL', 'View Backend User Changes List (' . $buserid . ')');
		} else {
			// if (strtolower($this->_request->getPost("submit")) == "approve" && $result["CHANGES_TYPE"] == "E") {
			// 	$newEmail = $result2["BUSER_EMAIL"];
			// 	$oldEMail = $result3["BUSER_EMAIL"];
			// 	$checkPriv = $this->_db->select()
			// 		->from("M_BPRIVI_GROUP")
			// 		->where("BGROUP_ID = ?", $result2["BGROUP_ID"])
			// 		->where("BPRIVI_ID = ?", "SBGC")
			// 		->query()->fetch();
			// 	if ($newEmail != $oldEMail && $checkPriv) {
			// 		$request['email'] = $newEmail;
			// 		$clientUser  =  new SGO_Soap_ClientUser();
			// 		$clientUser->callapi('registerDS', $request);
			// 	}
			// }
		}
		$changestype = $result["CHANGES_TYPE"];
		//     Zend_Debug::dump($changestype); die;
		if ($changestype == "N") {
			$this->_helper->viewRenderer('suggestiondetail/new', null, true);
		}
		if ($changestype == "E") {
			//die;
			$this->_helper->viewRenderer('suggestiondetail/edit', null, true);
		}
		if ($changestype == "S") {
			$this->_helper->viewRenderer('suggestiondetail/suspend', null, true);
		}
		if ($changestype == "U") {
			$this->_helper->viewRenderer('suggestiondetail/unsuspend', null, true);
		}
		if ($changestype == "L") {
			$this->_helper->viewRenderer('suggestiondetail/delete', null, true);
		}

		$this->view->changes_id = $changeid;
	}
}
