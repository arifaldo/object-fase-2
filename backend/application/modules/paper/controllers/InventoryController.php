<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class paper_InventoryController extends Application_Main
{

    protected $_moduleDB = 'RTF'; // masih harus diganti

    public function indexAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        if (!$this->view->hasPrivilege("VPAM")) {
            return $this->_redirect("/home/dashboard");
        }

        Application_Helper_General::writeLog("VPAM", "View Paper Inventory");

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->systemType = $system_type;

        $conf = Zend_Registry::get('config');
        $this->_bankName = $conf['app']['bankname'];
        $this->view->masterbankname = $this->_bankName;

        $this->CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

        $fields = array(
            /*'alias'  => array('field' => 'BENEFICIARY_ALIAS',
                                 'label' => $this->language->_('Alias Name'),
                                 'sortable' => true),*/
            'paperno'     => array(
                'field'    => '#',
                'label'    => $this->language->_('Paper Number'),
            ),
            'quantity' => array(
                'field' => '#',
                'label' => $this->language->_('Qty'),
            ),
            'used' => array(
                'field' => '#',
                'label' => $this->language->_('Used'),
            ),

            'available'     => array(
                'field'    => '#',
                'label'    => $this->language->_('Available'),
            ),
            'damaged'     => array(
                'field'    => '#',
                'label'    => $this->language->_('Damaged'),
            ),
            'bankbranch'     => array(
                'field'    => '#',
                'label'    => $this->language->_('Bank Branch'),
            ),
            'uploaded'     => array(
                'field'    => '#',
                'label'    => $this->language->_('Uploaded'),
            ),
            //'bgamount'  => array('field'    => 'BG_AMOUNT',
            //                    'label'    => $this->language->_('Amount')
            //                 )
        );



        $get_all_branch = $this->_db->select()
            ->from(array('A' => 'M_BRANCH'), array("BRANCH_NAME", "BRANCH_CODE"))
            ->query()->fetchAll();

        $this->view->all_branch = $get_all_branch;

        $select = $this->_db->select()
            ->from(array('A' => 'M_PAPER_GROUP'), array("*"))
            ->joinleft(array('B' => 'M_BRANCH'), 'A.PAPER_BRANCH = B.BRANCH_CODE', array("*"))
            ->order('A.PAPER_GROUP ASC');

        // advance filter ----------------------------------------------------------
        $filterlist = array("BANK BRANCH" => "PAPER_BRANCH", "UPLOADED" => "PAPER_UPDATED");

        $this->view->filterlist = $filterlist;


        $filterArr = array(
            'filter'    =>  array('StripTags'),
            'PAPER_BRANCH'    =>  array('StringTrim', 'StripTags'),
            'PAPER_UPDATED' =>  array('StringTrim', 'StripTags'),
            'PAPER_UPDATED_END'    =>  array('StringTrim', 'StripTags'),
        );

        $validator = array(
            'filter'                 => array(),
            'PAPER_BRANCH' => array(),
            'PAPER_UPDATED'         => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
            'PAPER_UPDATED_END'             => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
        );


        $dataParam = array("PAPER_BRANCH", "PAPER_UPDATED");
        $dataParamValue = array();

        $clean2 = array_diff($this->_request->getParam('wherecol'), $dataParam);
        $dataParam = array_diff($this->_request->getParam('wherecol'), $clean2);
        // print_r($this->_request->getParam('wherecol'));
        foreach ($dataParam as $no => $dtParam) {

            if (!empty($this->_request->getParam('wherecol'))) {
                $dataval = $this->_request->getParam('whereval');
                // print_r($dataval);
                $order = 0;
                foreach ($this->_request->getParam('wherecol') as $key => $value) {
                    if ($value == "PAPER_UPDATED") {
                        $order--;
                    }
                    if ($dtParam == $value) {
                        $dataParamValue[$dtParam] = $dataval[$order];
                    }
                    $order++;
                }
            }
        }


        // print_r($dataParamValue);die;

        if (!empty($this->_request->getParam('uploaded'))) {
            $updatearr = $this->_request->getParam('uploaded');
            $dataParamValue['PAPER_UPDATED'] = $updatearr[0];
            $dataParamValue['PAPER_UPDATED_END'] = $updatearr[1];
        }


        $zf_filter = new Zend_Filter_Input($filterArr, $validator, $dataParamValue);
        // $filter 	= $zf_filter->getEscaped('filter');
        $filter         = $this->_getParam('filter');
        $branch_name     = html_entity_decode($zf_filter->getEscaped('PAPER_BRANCH'));

        $datefrom2     = html_entity_decode($zf_filter->getEscaped('PAPER_UPDATED'));
        $dateto2     = html_entity_decode($zf_filter->getEscaped('PAPER_UPDATED_END'));


        if ($filter_clear == true) {

            $datefrom2     = '';
            $dateto2     = '';
        }

        if ($filter == TRUE) {

            $this->view->fDateTo2    = $dateto2;
            $this->view->fDateFrom2  = $datefrom2;


            if ($datefrom2) {
                $FormatDate     = new Zend_Date($datefrom2, $this->_dateDisplayFormat);
                $datefrom2      = $FormatDate->toString($this->_dateDBFormat);
                $select->where("DATE(A.PAPER_UPDATED) >= ?", $datefrom2);
            }

            if ($dateto2) {
                $FormatDate     = new Zend_Date($dateto2, $this->_dateDisplayFormat);
                $dateto2          = $FormatDate->toString($this->_dateDBFormat);
                $select->where("DATE(A.PAPER_UPDATED) <= ?", $dateto2);
            }

            if ($branch_name != null) {
                $this->view->branch_name = $branch_name;
                $select->where('A.PAPER_BRANCH LIKE ' . $this->_db->quote($branch_name));
            }
        }

        unset($dataParamValue['PAPER_UPDATED_END']);
        if (!empty($dataParamValue)) {
            foreach ($dataParamValue as $key => $value) {
                $wherecol[]    = $key;
                $whereval[] = $value;
            }

            $this->view->wherecol     = $wherecol;
            $this->view->whereval     = $whereval;
        }


        //echo $select; die;

        //print_r($dataParamValue);//die;
        // ----------------------------------------------------------------------

        $select = $select->query()->fetchAll();

        // check_paper ------------------------------------

        foreach ($select as $key => $value) {
            $temp_sheet = ["available" => 0, "used" => 0, "damage" => 0];

            $startpaper = $value["PAPER_FROM"];
            for ($i = 0; $i < $value["PAPER_QTY"]; $i++) {
                $paperss = str_pad($startpaper, 6, "0", STR_PAD_LEFT);
                $paperss = $value["PAPER_KEY"] . $paperss;
                // echo $paperss;die;
                $select2 = $this->_db->select()
                    ->from(array('A' => 'M_PAPER'), array(
                        "*"
                    ))
                    ->where("PAPER_ID = ?", $paperss);

                $res = $select2->query()->fetchAll();
                //var_dump($select2);

                if (count($res) == 0) {
                    $temp_sheet["available"] += 1;
                } else {
                    if ($res[0]["STATUS"] == 1) $temp_sheet["used"] += 1;
                    if ($res[0]["STATUS"] == 2) $temp_sheet["damage"] += 1;
                }

                $startpaper += 1;
            }

            $select[$key]["SHEET"] = $temp_sheet;
        }

        // ------------------------------------------------



        $this->paging($select);

        $this->view->fields = $fields;

        $csv = $this->_getParam('csv');
        if ($csv) {

            // $select = $this->_db->select()
            //     ->from(array('B' => 'M_PAPER_GROUP'), array("PAPER_GROUP", "PAPER_KEY", "PAPER_FROM", "PAPER_TO", "PAPER_QTY", "PAPER_BRANCH", "PAPER_UPDATED", "PAPER_UPDATEDBY"))
            //     ->query()->FetchAll();

            $result = [];
            $itung = 1;

            /*foreach ($select as $key => $value) {
                $start = $value["PAPER_FROM"] + 0;
                for ($i = 0; $i < $value["PAPER_QTY"]; $i++) {
                    $paper_id = str_pad($start, 6, '0', STR_PAD_LEFT);
                    $paper_id = $value["PAPER_KEY"] . $paper_id;
                    $check_m_paper = $this->_db->select()
                        ->from(array('B' => 'M_PAPER'), array("B.*", "PAPER_ID", "STATUS", "PAPER_GROUP", "PAPER_UPDATED", "PAPER_UPDATEDBY"))
                        ->where("PAPER_ID LIKE ?", "%" . $paper_id . "%")
                        ->query()->fetchAll();

                    if (count($check_m_paper) == 0) {
                        $temp = [$itung, $value["PAPER_KEY"] . $paper_id, "Available", '-', $value["PAPER_BRANCH"], $value["PAPER_UPDATED"], $value["PAPER_UPDATEDBY"]];
                    } else {
                        if ($check_m_paper[0]["STATUS"] == '1') $status = "Printed";
                        if ($check_m_paper[0]["STATUS"] == '2') $status = "Damaged";
                        $temp = [$itung, $value["PAPER_KEY"] . $paper_id, $status, $check_m_paper[0]["NOTES"], $value["PAPER_BRANCH"], $check_m_paper[0]["PAPER_UPDATED"], $check_m_paper[0]["PAPER_UPDATEDBY"]];
                    }
                    array_push($result, $temp);

                    $itung += 1;
                    $start += 1;
                }
            }*/


            // check_paper ------------------------------------

            $result = array();
            foreach ($select as $key => $value) {
                $temp_sheet = ["available" => 0, "used" => 0, "damage" => 0];

                $startpaper = $value["PAPER_FROM"];
                for ($i = 0; $i < $value["PAPER_QTY"]; $i++) {
                    $paperss = str_pad($startpaper, 6, "0", STR_PAD_LEFT);
                    $paperss = $value["PAPER_KEY"] . $paperss;
                    // echo $paperss;die;
                    $select2 = $this->_db->select()
                        ->from(array('A' => 'M_PAPER'), array(
                            "*"
                        ))
                        ->where("PAPER_ID = ?", $paperss);

                    $res = $select2->query()->fetchAll();
                    //var_dump($select2);

                    if (count($res) == 0) {
                        $temp_sheet["available"] += 1;
                    } else {
                        if ($res[0]["STATUS"] == 1) $temp_sheet["used"] += 1;
                        if ($res[0]["STATUS"] == 2) $temp_sheet["damage"] += 1;
                    }

                    $startpaper += 1;
                }



                $select[$key]["SHEET"] = $temp_sheet;
            }

            for ($i = 0; $i < count($select); $i++) {

                $result[$i][] = $i + 1;
                $result[$i][] = $select[$i]['PAPER_KEY'] . $select[$i]['PAPER_FROM'] . '-' . $select[$i]['PAPER_KEY'] . $select[$i]['PAPER_TO'];
                $result[$i][] = $select[$i]['PAPER_QTY'];
                $result[$i][] = $select[$i]['SHEET']['used'];
                $result[$i][] = $select[$i]['SHEET']['available'];
                $result[$i][] = $select[$i]['SHEET']['damage'];
                $result[$i][] = $select[$i]['BRANCH_NAME'];
                $result[$i][] =  $select[$i]['PAPER_UPDATED'] . '(' . $select[$i]['PAPER_UPDATEDBY'] . ')';
                //$result[$i] = $select[$i]['PAPER_FROM'];

            }

            Application_Helper_General::writeLog('ADBU', 'Download CSV Paper Inventory Report');
            $this->_helper->download->csv(array($this->language->_('No'), $this->language->_('Paper Number'), $this->language->_('Qty'), $this->language->_('Used'), $this->language->_('Available'), $this->language->_('Damaged'), $this->language->_('Bank Branch'), $this->language->_('Uploaded')), $result, null, $this->language->_('Paper Inventory'));
        }
    }

    public function bbranchAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $selectGroup =  $this->_db->select()
            ->from('M_BRANCH', array('BRANCH_CODE', 'BRANCH_NAME'));
        // ->where("BGROUP_STATUS = '1'");         
        $arrGroup = $this->_db->fetchAll($selectGroup);
        $optHtml = "<option value=''>-- " . $this->language->_('Please Select') . " --</option>";
        foreach ($arrGroup as $key => $value) {
            if ($tblName == $value['BRANCH_CODE']) {
                $select = 'selected';
            } else {
                $select = '';
            }
            $optHtml .= "<option value='" . $value['BRANCH_CODE'] . "' " . $select . ">" . $value['BRANCH_NAME'] . "</option>";
        }

        echo $optHtml;
    }
}
