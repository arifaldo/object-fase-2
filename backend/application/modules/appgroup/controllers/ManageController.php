<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';
class Appgroup_ManageController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti

	// public function initController()
	// {       
		// $this->_helper->layout()->setLayout('popup');
	// }

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$cust_id = $this->_getParam('cust_id');
		$this->view->cust_id = $cust_id;
		
		if($cust_id)
        {
			//check apakah company-nya masih ada approval
			$validator = new Zend_Validate_Db_NoRecordExists(
							array(
								'table' => 'TEMP_APP_GROUP_USER',
								'field' => 'CUST_ID'
							));
			
			if (!$validator->isValid($cust_id))
			{
				$messages = array($this->language->_('No changes allowed for this record while awaiting approval for previous change.'));
				$this->view->isapproval = $this->displayError($messages);
			}
			
			$row = $this->_db->fetchRow(
					$this->_db->select()
							   ->from(array('M_CUSTOMER'),array('CUST_ID','CUST_NAME'))
							   ->where('CUST_ID='.$this->_db->quote($cust_id))
					);
					
			if($row)
			{
				$this->view->comp_id 	= $row['CUST_ID'];
				$this->view->comp_name 	= $row['CUST_NAME'];
		
				$comp = $this->_db->fetchAll(
							$this->_db->select()
							   ->from('M_USER',array('USER_ID','USER_FULLNAME'))
							   ->where('CUST_ID='.$this->_db->quote($cust_id))
							   );
				
				// list user
				$arrayListJob = array();
				foreach($comp as $row) 
				{
					$arrayListJob[$row['USER_ID']] = $row['USER_ID'].' - '.$row['USER_FULLNAME'];
				}
				
				$listUser = $arrayListJob;
				
				$listGroup01 = $this->_db->fetchAll(
							$this->_db->select()
							   ->from('M_APP_GROUP_USER',array('USER_ID'))
							   ->where('GROUP_USER_ID='.$this->_db->quote('N_'.$cust_id.'_01'))
							   ->where('CUST_ID='.$this->_db->quote($cust_id))
							   );
				
				$listGroup02 = $this->_db->fetchAll(
							$this->_db->select()
							   ->from('M_APP_GROUP_USER',array('USER_ID'))
							   ->where('GROUP_USER_ID='.$this->_db->quote('N_'.$cust_id.'_02'))
							   ->where('CUST_ID='.$this->_db->quote($cust_id))
							   );
				
				$listGroup03 = $this->_db->fetchAll(
							$this->_db->select()
							   ->from('M_APP_GROUP_USER',array('USER_ID'))
							   ->where('GROUP_USER_ID='.$this->_db->quote('N_'.$cust_id.'_03'))
							   ->where('CUST_ID='.$this->_db->quote($cust_id))
							   );
				
				$listGroup04 = $this->_db->fetchAll(
							$this->_db->select()
							   ->from('M_APP_GROUP_USER',array('USER_ID'))
							   ->where('GROUP_USER_ID='.$this->_db->quote('N_'.$cust_id.'_04'))
							   ->where('CUST_ID='.$this->_db->quote($cust_id))
							   );
				
				$listGroup05 = $this->_db->fetchAll(
							$this->_db->select()
							   ->from('M_APP_GROUP_USER',array('USER_ID'))
							   ->where('GROUP_USER_ID='.$this->_db->quote('N_'.$cust_id.'_05'))
							   ->where('CUST_ID='.$this->_db->quote($cust_id))
							   );
				
				$listGroup06 = $this->_db->fetchAll(
							$this->_db->select()
							   ->from('M_APP_GROUP_USER',array('USER_ID'))
							   ->where('GROUP_USER_ID='.$this->_db->quote('N_'.$cust_id.'_06'))
							   ->where('CUST_ID='.$this->_db->quote($cust_id))
							   );
				
				$listGroup07 = $this->_db->fetchAll(
							$this->_db->select()
							   ->from('M_APP_GROUP_USER',array('USER_ID'))
							   ->where('GROUP_USER_ID='.$this->_db->quote('N_'.$cust_id.'_07'))
							   ->where('CUST_ID='.$this->_db->quote($cust_id))
							   );
				
				$listGroup08 = $this->_db->fetchAll(
							$this->_db->select()
							   ->from('M_APP_GROUP_USER',array('USER_ID'))
							   ->where('GROUP_USER_ID='.$this->_db->quote('N_'.$cust_id.'_08'))
							   ->where('CUST_ID='.$this->_db->quote($cust_id))
							   );
				
				$listGroup09 = $this->_db->fetchAll(
							$this->_db->select()
							   ->from('M_APP_GROUP_USER',array('USER_ID'))
							   ->where('GROUP_USER_ID='.$this->_db->quote('N_'.$cust_id.'_09'))
							   ->where('CUST_ID='.$this->_db->quote($cust_id))
							   );
				
				$listGroup10 = $this->_db->fetchAll(
							$this->_db->select()
							   ->from('M_APP_GROUP_USER',array('USER_ID'))
							   ->where('GROUP_USER_ID='.$this->_db->quote('N_'.$cust_id.'_10'))
							   ->where('CUST_ID='.$this->_db->quote($cust_id))
							   );
				
				$listSpecialGroup = $this->_db->fetchAll(
							$this->_db->select()
							   ->from('M_APP_GROUP_USER',array('USER_ID'))
							   ->where('GROUP_USER_ID='.$this->_db->quote('S_'.$cust_id))
							   ->where('CUST_ID='.$this->_db->quote($cust_id))
							   );
				
				$this->view->listuser = $listUser;
				$this->view->listgroup01 = Application_Helper_Array::listArray($listGroup01,'USER_ID','USER_ID');
				$this->view->listgroup02 = Application_Helper_Array::listArray($listGroup02,'USER_ID','USER_ID');
				$this->view->listgroup03 = Application_Helper_Array::listArray($listGroup03,'USER_ID','USER_ID');
				$this->view->listgroup04 = Application_Helper_Array::listArray($listGroup04,'USER_ID','USER_ID');
				$this->view->listgroup05 = Application_Helper_Array::listArray($listGroup05,'USER_ID','USER_ID');
				$this->view->listgroup06 = Application_Helper_Array::listArray($listGroup06,'USER_ID','USER_ID');
				$this->view->listgroup07 = Application_Helper_Array::listArray($listGroup07,'USER_ID','USER_ID');
				$this->view->listgroup08 = Application_Helper_Array::listArray($listGroup08,'USER_ID','USER_ID');
				$this->view->listgroup09 = Application_Helper_Array::listArray($listGroup09,'USER_ID','USER_ID');
				$this->view->listgroup10 = Application_Helper_Array::listArray($listGroup10,'USER_ID','USER_ID');
				$this->view->listspecialgroup = Application_Helper_Array::listArray($listSpecialGroup,'USER_ID','USER_ID');
				
            }
			else $cust_id = null; 
       }// End if cust_id == true
    
        if(!$cust_id)
        {
           $error_remark = $this->displayError(array('Error: Company Code does not exist.'));
		   //insert log
	       try 
	       {
	         $this->_db->beginTransaction();
	         //$this->backendLog($this->_actionID['cekout'],$this->_moduleID['doc'],null,null,$error_remark);
	         $this->_db->commit();
	       }
           catch(Exception $e)
           {
 	          $this->_db->rollBack();
	          Application_Log_GeneralLog::technicalLog($e);
	       }
	    
			$this->view->error = true;
			$this->view->report_msg = $error_remark;
           //$this->_redirect($this->_helper->url->url(array('module'=>$this->_request->getModuleName(),'controller'=>'index','action'=>'index')));	
        }
		
		//----jika tekan tombol submit-------
		if($this->_request->isPost())
		{
			$custId = $this->_request->getParam('uid');
			$listGroup01 = $this->_request->getParam('sN_'.$custId.'_01');
			$listGroup02 = $this->_request->getParam('sN_'.$custId.'_02');
			$listGroup03 = $this->_request->getParam('sN_'.$custId.'_03');
			$listGroup04 = $this->_request->getParam('sN_'.$custId.'_04');
			$listGroup05 = $this->_request->getParam('sN_'.$custId.'_05');
			$listGroup06 = $this->_request->getParam('sN_'.$custId.'_06');
			$listGroup07 = $this->_request->getParam('sN_'.$custId.'_07');
			$listGroup08 = $this->_request->getParam('sN_'.$custId.'_08');
			$listGroup09 = $this->_request->getParam('sN_'.$custId.'_09');
			$listGroup10 = $this->_request->getParam('sN_'.$custId.'_10');
			$listSpecialGroup = $this->_request->getParam('sS_'.$custId);
			//Zend_Debug::dump($this->_request->getParams());die;
			
			try
			{	
				$info = 'Customer ID = '.$custId;	
				$this->_db->beginTransaction();
				
				$custName = $this->_db->fetchOne(
							$this->_db->select()
									->from(array('M_CUSTOMER'),
										   array('CUST_NAME')
										   )
									->where("CUST_ID = ?", $custId)
							);
				
				// insert ke T_GLOBAL_CHANGES
				$change_id = $this->suggestionWaitingApproval('Approver Group',$info,$this->_changeType['code']['edit'],null,'M_APP_GROUP_USER','TEMP_APP_GROUP_USER',$custId,$custName,$custId);
				
				if($listGroup01)
				{
					foreach($listGroup01 as $val)
					{
						$content = array(
							'CHANGES_ID' => $change_id,
							'GROUP_USER_ID' => 'N_'.$custId.'_01',
							'CUST_ID' => $custId,
							'USER_ID' => $val,
						);
						$this->_db->insert('TEMP_APP_GROUP_USER',$content);
					}
				}
				
				if($listGroup02)
				{
					foreach($listGroup02 as $val)
					{
						$content = array(
							'CHANGES_ID' => $change_id,
							'GROUP_USER_ID' => 'N_'.$custId.'_02',
							'CUST_ID' => $custId,
							'USER_ID' => $val,
						);
						$this->_db->insert('TEMP_APP_GROUP_USER',$content);
					}
				}
				
				if($listGroup03)
				{
					foreach($listGroup03 as $val)
					{
						$content = array(
							'CHANGES_ID' => $change_id,
							'GROUP_USER_ID' => 'N_'.$custId.'_03',
							'CUST_ID' => $custId,
							'USER_ID' => $val,
						);
						$this->_db->insert('TEMP_APP_GROUP_USER',$content);
					}
				}
				
				if($listGroup04)
				{
					foreach($listGroup04 as $val)
					{
						$content = array(
							'CHANGES_ID' => $change_id,
							'GROUP_USER_ID' => 'N_'.$custId.'_04',
							'CUST_ID' => $custId,
							'USER_ID' => $val,
						);
						$this->_db->insert('TEMP_APP_GROUP_USER',$content);
					}
				}
				
				if($listGroup05)
				{
					foreach($listGroup05 as $val)
					{
						$content = array(
							'CHANGES_ID' => $change_id,
							'GROUP_USER_ID' => 'N_'.$custId.'_05',
							'CUST_ID' => $custId,
							'USER_ID' => $val,
						);
						$this->_db->insert('TEMP_APP_GROUP_USER',$content);
					}
				}
				
				if($listGroup06)
				{
					foreach($listGroup06 as $val)
					{
						$content = array(
							'CHANGES_ID' => $change_id,
							'GROUP_USER_ID' => 'N_'.$custId.'_06',
							'CUST_ID' => $custId,
							'USER_ID' => $val,
						);
						$this->_db->insert('TEMP_APP_GROUP_USER',$content);
					}
				}
				
				if($listGroup07)
				{
					foreach($listGroup07 as $val)
					{
						$content = array(
							'CHANGES_ID' => $change_id,
							'GROUP_USER_ID' => 'N_'.$custId.'_07',
							'CUST_ID' => $custId,
							'USER_ID' => $val,
						);
						$this->_db->insert('TEMP_APP_GROUP_USER',$content);
					}
				}
				
				if($listGroup08)
				{
					foreach($listGroup08 as $val)
					{
						$content = array(
							'CHANGES_ID' => $change_id,
							'GROUP_USER_ID' => 'N_'.$custId.'_08',
							'CUST_ID' => $custId,
							'USER_ID' => $val,
						);
						$this->_db->insert('TEMP_APP_GROUP_USER',$content);
					}
				}
				
				if($listGroup09)
				{
					foreach($listGroup09 as $val)
					{
						$content = array(
							'CHANGES_ID' => $change_id,
							'GROUP_USER_ID' => 'N_'.$custId.'_09',
							'CUST_ID' => $custId,
							'USER_ID' => $val,
						);
						$this->_db->insert('TEMP_APP_GROUP_USER',$content);
					}
				}
				
				if($listGroup10)
				{
					foreach($listGroup10 as $val)
					{
						$content = array(
							'CHANGES_ID' => $change_id,
							'GROUP_USER_ID' => 'N_'.$custId.'_10',
							'CUST_ID' => $custId,
							'USER_ID' => $val,
						);
						$this->_db->insert('TEMP_APP_GROUP_USER',$content);
					}
				}
				
				if($listSpecialGroup)
				{
					foreach($listSpecialGroup as $val)
					{
						$content = array(
							'CHANGES_ID' => $change_id,
							'GROUP_USER_ID' => 'S_'.$custId,
							'CUST_ID' => $custId,
							'USER_ID' => $val,
						);
						$this->_db->insert('TEMP_APP_GROUP_USER',$content);
					}
				}
				$this->_db->commit();
				Application_Helper_General::writeLog('AGUD','Updating Approver Group');
				$this->setbackURL($this->view->backURL);
				// $this->_redirect('/notification/success');
				$this->_redirect('/notification/submited/index');
			}
			catch(Exception $e) 
			{
				//rollback changes
				$this->_db->rollBack();	
			}
		}
		else
		{
			Application_Helper_General::writeLog('AGUD','Viewing Update Approver Group');
		}
	}
}