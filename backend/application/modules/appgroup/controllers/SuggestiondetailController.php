<?php


require_once 'Zend/Controller/Action.php';
require_once 'General/Settings.php';

class Appgroup_SuggestionDetailController extends Application_Main
{
	 public function initController(){
			  $this->_helper->layout()->setLayout('popup');
	}
	public function indexAction() 
	{		
		$status = $this->_changeStatus;
		$options = array_combine(array_values($status['code']),array_values($status['desc']));
		
		$changesid = $this->_getparam('changes_id');

		$select	= $this->_db->fetchRow("SELECT CHANGES_STATUS, CREATED, CREATED_BY, KEY_VALUE, COMPANY_CODE, COMPANY_NAME FROM T_GLOBAL_CHANGES WHERE CHANGES_ID = ".$this->_db->quote($changesid)." AND (CHANGES_STATUS = 'WA' OR CHANGES_STATUS = 'RR')");
								
		if($select)
		{			
			$this->view->changes_id = $changesid;
			$this->view->created = $select['CREATED'];
			$this->view->createdby = $select['CREATED_BY'];
			$this->view->cust_id = $cust_id = $select['COMPANY_CODE'];
			$this->view->cust_name = $select['COMPANY_NAME'];
			foreach($options as $codestat=>$descstat)
			{
				if($codestat == $select['CHANGES_STATUS'])
				{
					$this->view->status = $descstat;
				}
			}
			
//			DATA BARU				
			$selectnew = $this->_db->select()
					->FROM(array('B' => 'TEMP_APP_GROUP_USER'))	
                    ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))				
					->query()->FetchAll();
			
			$newAppGroupArray = array();
			foreach($selectnew as $row)
			{
				$newAppGroupArray[ $row['GROUP_USER_ID'] ][] = $row['USER_ID'];
			}
			$this->view->databaru = $newAppGroupArray;
			
//			DATA LAMA			
			$selectold = $this->_db->select()
					->from(array('A' => 'M_APP_GROUP_USER'))
                    ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
					->query()->FetchAll();				
			
			$oldAppGroupArray = array();
			foreach($selectold as $row)
			{
				$oldAppGroupArray[ $row['GROUP_USER_ID'] ][] = $row['USER_ID'];
			}
			$this->view->datalama = $oldAppGroupArray;		
			//Zend_Debug::dump($newAppGroupArray);die;
			$this->view->cek = 1;
		}
		else{
			$this->_redirect('/notification/invalid/index');
			$this->view->cek = 0;			
		}
		if(!$this->_request->isPost()){
		Application_Helper_General::writeLog('AGCL','Viewing Update Approver Group');
		}
	}
}
