<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';
class Appgroup_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti
	
	public function initController()
	{       
		$this->_helper->layout()->setLayout('newlayout');
	    //pengaturan url untuk button back
	    $this->setbackURL('/'.$this->_request->getModuleName().'/index/'); 
	
		$companyCode = $this->_db->fetchAll(
					$this->_db->select()
					   ->from('M_CUSTOMER',array('CUST_ID'))
					   );
		$listCompCode = array(''=>'--- '.$this->language->_('Any Value').' ---');
		$listCompCode += Application_Helper_Array::listArray($companyCode,'CUST_ID','CUST_ID');
		$this->view->listcompcode = $listCompCode;
	}

	public function indexAction()
	{
		$fields = array(
						'comp_code'  => array('field' => 'CUST_ID',
											   'label' => $this->language->_('Company Code'),
											   'sortable' => true),
						'comp_name'  => array('field' => 'CUST_NAME',
											   'label' => $this->language->_('Company Name'),
											   'sortable' => true),
						/*'group_name'  => array('field' => 'GROUP_NAME',
											   'label' => 'Group Name',
											   'sortable' => true)*/
				);

		$filterlist = array('COMP_ID','COMP_NAME');
    
    	$this->view->filterlist = $filterlist;
				
		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','doc_no');
		$sortDir = $this->_getParam('sortdir','asc');
		
		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';
	  
		
		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	'filter' 	  	=> array('StringTrim','StripTags'),
							'COMP_ID' 	=> array('StringTrim','StripTags','StringToUpper'),
							'COMP_NAME'    	=> array('StringTrim','StripTags','StringToUpper'),
							//'group_name'    => array('StringTrim','StripTags')
		);

		 $validators = array(
                    'filter' => array(),
                      'COMP_ID'    => array(),
                      'COMP_NAME'  => array(),
        );


		
		$dataParam = array('COMP_ID','COMP_NAME');
	    $dataParamValue = array();
	        
	      $clean2 = array_diff( $this->_request->getParam('wherecol'),$dataParam); 
	      $dataParam = array_diff( $this->_request->getParam('wherecol'),$clean2); 
	    // print_r($dataParam);die;

	      // print_r($output);die;
	      // print_r($this->_request->getParam('wherecol'));
	      foreach ($dataParam as $no => $dtParam)
	      {
	      
	        if(!empty($this->_request->getParam('wherecol'))){
	          $dataval = $this->_request->getParam('whereval');
	          // print_r($dataval);
	          $order = 0;
	            foreach ($this->_request->getParam('wherecol') as $key => $value) {
	              if($value == "PS_LATEST_APPROVAL" || $value == "PS_LATEST_SUGGESTION"){
	                  $order--;
	                }
	              if($dtParam==$value){
	                $dataParamValue[$dtParam] = $dataval[$order];
	              }
	              $order++;
	            }
	          
	        }
	      }
	      // print_r($dataParamValue);
	      // die; 
	        if(!empty($this->_request->getParam('lsdate'))){
	          $lsarr = $this->_request->getParam('lsdate');
	            $dataParamValue['PS_LATEST_SUGGESTION'] = $lsarr[0];
	            $dataParamValue['PS_LATEST_SUGGESTION_END'] = $lsarr[1];
	        }
	        if(!empty($this->_request->getParam('ladate'))){
	          $laarr = $this->_request->getParam('ladate');
	            $dataParamValue['PS_LATEST_APPROVAL'] = $laarr[0];
	            $dataParamValue['PS_LATEST_APPROVAL_END'] = $laarr[1];
	        }


	    $zf_filter = new Zend_Filter_Input($filterArr,$validator,$dataParamValue);
	    $filter     = $this->_getParam('filter');
		
		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		
		$select = $this->_db->select()
					   ->from('M_CUSTOMER',array('CUST_ID','CUST_NAME'));
					   
	    $select->order($sortBy.' '.$sortDir);
		
		if($filter == $this->language->_('Set Filter'))
		{
			$fCompCode = $zf_filter->getEscaped('COMP_ID');
			$fCompName = $zf_filter->getEscaped('COMP_NAME');
			//$fGroupName = $zf_filter->getEscaped('group_name');
			
	        if($fCompCode)$select->where('UPPER(CUST_ID) = '.$this->_db->quote(strtoupper($fCompCode)));
	        if($fCompName)$select->where('UPPER(CUST_NAME) LIKE '.$this->_db->quote('%'.strtoupper($fCompName).'%'));
	        //if($fGroupName)$select->where('UPPER(BENEFICIARY_NAME) LIKE '.$this->_db->quote('%'.strtoupper($fGroupName).'%'));
						
			$this->view->COMP_ID = $fCompCode;
			$this->view->COMP_NAME = $fCompName;
			//$this->view->group_name = $fGroupName;
		}
		
		$this->paging($select);
		Application_Helper_General::writeLog('AGLS','Viewing Approver Group List');
		$this->view->fields = $fields;
		$this->view->filter = $filter;

		if(!empty($dataParamValue)){
      foreach ($dataParamValue as $key => $value) {
        $wherecol[] = $key;
        $whereval[] = $value;
      }
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     // print_r($whereval);die;
      }

	}

	public function newappgroupAction()
	{
		$fields = array(
						'comp_code'  => array('field' => 'CUST_ID',
											   'label' => $this->language->_('Company Code'),
											   'sortable' => true),
						'comp_name'  => array('field' => 'CUST_NAME',
											   'label' => $this->language->_('Company Name'),
											   'sortable' => true),
						/*'group_name'  => array('field' => 'GROUP_NAME',
											   'label' => 'Group Name',
											   'sortable' => true)*/
				);

		$filterlist = array('COMP_ID','COMP_NAME');
    
    	$this->view->filterlist = $filterlist;
				
		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','doc_no');
		$sortDir = $this->_getParam('sortdir','asc');
		
		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';
	  
		
		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	'filter' 	  	=> array('StringTrim','StripTags'),
							'COMP_ID' 	=> array('StringTrim','StripTags','StringToUpper'),
							'COMP_NAME'    	=> array('StringTrim','StripTags','StringToUpper'),
							//'group_name'    => array('StringTrim','StripTags')
		);

		 $validators = array(
                    'filter' => array(),
                      'COMP_ID'    => array(),
                      'COMP_NAME'  => array(),
        );


		
		$dataParam = array('COMP_ID','COMP_NAME');
	    $dataParamValue = array();
	        
	      $clean2 = array_diff( $this->_request->getParam('wherecol'),$dataParam); 
	      $dataParam = array_diff( $this->_request->getParam('wherecol'),$clean2); 
	    // print_r($dataParam);die;

	      // print_r($output);die;
	      // print_r($this->_request->getParam('wherecol'));
	      foreach ($dataParam as $no => $dtParam)
	      {
	      
	        if(!empty($this->_request->getParam('wherecol'))){
	          $dataval = $this->_request->getParam('whereval');
	          // print_r($dataval);
	          $order = 0;
	            foreach ($this->_request->getParam('wherecol') as $key => $value) {
	              if($value == "PS_LATEST_APPROVAL" || $value == "PS_LATEST_SUGGESTION"){
	                  $order--;
	                }
	              if($dtParam==$value){
	                $dataParamValue[$dtParam] = $dataval[$order];
	              }
	              $order++;
	            }
	          
	        }
	      }
	      // print_r($dataParamValue);
	      // die; 
	        if(!empty($this->_request->getParam('lsdate'))){
	          $lsarr = $this->_request->getParam('lsdate');
	            $dataParamValue['PS_LATEST_SUGGESTION'] = $lsarr[0];
	            $dataParamValue['PS_LATEST_SUGGESTION_END'] = $lsarr[1];
	        }
	        if(!empty($this->_request->getParam('ladate'))){
	          $laarr = $this->_request->getParam('ladate');
	            $dataParamValue['PS_LATEST_APPROVAL'] = $laarr[0];
	            $dataParamValue['PS_LATEST_APPROVAL_END'] = $laarr[1];
	        }


	    $zf_filter = new Zend_Filter_Input($filterArr,$validator,$dataParamValue);
	    $filter     = $this->_getParam('filter');
		
		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		
		$select = $this->_db->select()
					   ->from('M_CUSTOMER',array('CUST_ID','CUST_NAME'));
					   
	    $select->order($sortBy.' '.$sortDir);
		
		if($filter == $this->language->_('Set Filter'))
		{
			$fCompCode = $zf_filter->getEscaped('COMP_ID');
			$fCompName = $zf_filter->getEscaped('COMP_NAME');
			//$fGroupName = $zf_filter->getEscaped('group_name');
			
	        if($fCompCode)$select->where('UPPER(CUST_ID) = '.$this->_db->quote(strtoupper($fCompCode)));
	        if($fCompName)$select->where('UPPER(CUST_NAME) LIKE '.$this->_db->quote('%'.strtoupper($fCompName).'%'));
	        //if($fGroupName)$select->where('UPPER(BENEFICIARY_NAME) LIKE '.$this->_db->quote('%'.strtoupper($fGroupName).'%'));
						
			$this->view->COMP_ID = $fCompCode;
			$this->view->COMP_NAME = $fCompName;
			//$this->view->group_name = $fGroupName;
		}
		
		$this->paging($select);
		Application_Helper_General::writeLog('AGLS','Viewing Approver Group List');
		$this->view->fields = $fields;
		$this->view->filter = $filter;

		if(!empty($dataParamValue)){
      foreach ($dataParamValue as $key => $value) {
        $wherecol[] = $key;
        $whereval[] = $value;
      }
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     // print_r($whereval);die;
      }
		
	}


}
