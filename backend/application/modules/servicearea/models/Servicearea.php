<?php
Class servicearea_Model_Servicearea {
	
	protected $_db;

    // constructor
	public function __construct()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
	}
  
    public function getData($fParam,$sortBy = null,$sortDir = null,$filter = null)
    {
    	$actQuery = $this->_db->select()
    						  ->from(array('A'=>'M_USER_LKP'),array('count(*)'))
    						  ->where('A.USER_STATUS = ?', '1')
    						  ->where('A.USER_SERVICE_AREA = S.AREA_NAME');
    	$inactQuery = $this->_db->select()
    						  ->from(array('A'=>'M_USER_LKP'),array('count(*)'))
    						  ->where('A.USER_STATUS = ?', '2')
    						  ->where('A.USER_SERVICE_AREA = S.AREA_NAME');
    	$delQuery = $this->_db->select()
    						  ->from(array('A'=>'M_USER_LKP'),array('count(*)'))
    						  ->where('A.USER_STATUS = ?', '3')
    						  ->where('A.USER_SERVICE_AREA = S.AREA_NAME');

		$select = $this->_db->select()
					        ->from(array('S'=>'M_SERVICE_AREA'),array('AREA_ID',
					        										  'AREA_NAME',
					        										  'ACTIVE_AGENT' => new Zend_Db_Expr("($actQuery)"),
					        										  'INACTIVE_AGENT' => new Zend_Db_Expr("($inactQuery)"),
					        										  'DELETED_AGENT' => new Zend_Db_Expr("($delQuery)"),
					        										  'LAST_UPDATED',
					        										  'LAST_UPDATEDBY'
					        )); 
					
        if(!empty($fParam['area_name'])) $select->where('UPPER(S.AREA_NAME) LIKE '.$this->_db->quote('%'.strtoupper($fParam['area_name']).'%'));
        if(!empty($fParam['latestUpdater'])) $select->where('UPPER(S.LAST_UPDATEDBY) = '.$this->_db->quote(strtoupper($fParam['latestUpdater'])));
        if(!empty($fParam['latestUpdatedFrom'])) $select->where("CONVERTSGO('DATE',S.LAST_UPDATED) >= CONVERTSGO('DATE',".$this->_db->quote($fParam['latestUpdatedFrom']).")");
		if(!empty($fParam['latestUpdatedTo'])) $select->where("CONVERTSGO('DATE',S.LAST_UPDATED) <= CONVERTSGO('DATE',".$this->_db->quote($fParam['latestUpdatedTo']).")");

		if( !empty($sortBy) && !empty($sortDir) )
			$select->order($sortBy.' '.$sortDir);

       return $this->_db->fetchall($select);
    }
  
    public function getDetail($area_id)
    {
		$data = $this->_db->fetchRow(
								$this->_db->select()
									 ->from(array('M_SERVICE_AREA')) 
									 ->where("AREA_ID=?", $area_id)
							);
		
       return $data;
    }
  
    public function insertData($content)
    {
		$this->_db->insert("M_SERVICE_AREA",$content);
    }
  
    public function updateData($area_id,$content)
    {
		$whereArr  = array('AREA_ID = ?'=>$area_id);
		$this->_db->update('M_SERVICE_AREA',$content,$whereArr);
    }
  
    public function deleteData($area_id)
    {
		$this->_db->delete('M_SERVICE_AREA','AREA_ID = '.$this->_db->quote($area_id));
    }
}