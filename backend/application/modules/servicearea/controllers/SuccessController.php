<?php
require_once 'Zend/Controller/Action.php';

class servicearea_SuccessController extends Application_Main
{
	//protected $_moduleDB = 'RTF'; //masih harus diganti

	public function initController()
	{       
	
		$this->_helper->layout()->setLayout('newpopup');
	}

	public function indexAction() 
	{
		$tempdata = new Zend_Session_Namespace('tempdata');
		$area = $tempdata->area_name;
		$this->view->area_name = $area;
	}

}
