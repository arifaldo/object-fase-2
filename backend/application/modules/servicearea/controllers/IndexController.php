<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';

class servicearea_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti
	
	
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
	   $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
	   $this->view->report_msg = array();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
	
		$model = new servicearea_Model_Servicearea();
	
	    $fields = array(
						'area_name'      => array('field' => 'AREA_NAME',
											      'label' => 'Area Layanan',
											      'sortable' => true),
						'active_agent'      => array('field' => 'ACTIVE_AGENT',
											      'label' => 'Agen Aktif',
											      'sortable' => true),
						// 'inactive_agent' 	=> array('field' => 'INACTIVE_AGENT',
						// 					      'label' => 'Agen Nonaktif',
						// 					      'sortable' => true),
						// 'deleted_agent' 	=> array('field' => 'DELETED_AGENT',
						// 					      'label' => 'Agen Dihapus',
						// 					      'sortable' => true),
						// 'last_updated' 	=> array('field' => 'LAST_UPDATED',
						// 					      'label' => 'Terakhir Diperbaharui',
						// 					      'sortable' => true),
						'last_updatedby' 	=> array('field' => 'LAST_UPDATEDBY',
											      'label' => 'Last Updated By',
											      'sortable' => true),
				      );
		
		$filterlist = array("SERVICE_AREA");
		
		$this->view->filterlist = $filterlist;
		
		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','bank_code');
		$sortDir = $this->_getParam('sortdir','asc');
		
		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';

		$filterArr = array(	'filter'	=> array('StringTrim','StripTags'),
							'SERVICE_AREA'  => array('StringTrim','StripTags','HtmlEntities'),
							'latestUpdater'=> array('StringTrim','StripTags','StringToUpper','HtmlEntities'),
							'latestUpdatedFrom'  => array('StringTrim','StripTags','HtmlEntities'),
							'latestUpdatedTo'  => array('StringTrim','StripTags','HtmlEntities'),
		);
		
		$dataParam = array("SERVICE_AREA");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{
			
			// print_r($dtParam);die;
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}
				
			}
				
			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}
		
		$zf_filter = new Zend_Filter_Input($filterArr,array(),$dataParamValue);
		$filter 		= $this->_getParam('filter');
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$csv = $this->_getParam('csv');	

		$fParam = array();
		if($filter == 'Set Filter')
		{
			$fParam['area_name'] 		= html_entity_decode($zf_filter->getEscaped('SERVICE_AREA'));
			$fParam['latestUpdater']    = html_entity_decode($zf_filter->getEscaped('latestUpdater'));

			$latestUpdatedTo     = html_entity_decode($zf_filter->getEscaped('latestUpdatedTo'));
			$latestUpdatedFrom       = html_entity_decode($zf_filter->getEscaped('latestUpdatedFrom'));

			$fParam['latestUpdatedTo']   = (Zend_Date::isDate($latestUpdatedTo,$this->_dateDisplayFormat))?
									   new Zend_Date($latestUpdatedTo,$this->_dateDisplayFormat):
									   false;
			
			$fParam['latestUpdatedFrom']  = (Zend_Date::isDate($latestUpdatedFrom,$this->_dateDisplayFormat))?
									   new Zend_Date($latestUpdatedFrom,$this->_dateDisplayFormat):
									   false;

			$this->view->area_name = $fParam['SERVICE_AREA'];
			$this->view->latestUpdater = $fParam['latestUpdater'];
			$this->view->latestUpdatedFrom = $latestUpdatedFrom;
			$this->view->latestUpdatedTo = $latestUpdatedTo;
		}
		else if($filter == 'Clear Filter' || $filter == '')
		{
			$fParam['area_name'] = '';
			$fParam['latestUpdater'] = '';
			$fParam['latestUpdatedFrom'] = '';
			$fParam['latestUpdatedTo'] = '';
		}


		$select = $model->getData($fParam,$sortBy,$sortDir,$filter);		
		
		//menghilangkan index/key AREA_ID utk kepentingan CSV
		$selectPdfCsv = $select;
		foreach($selectPdfCsv as $key => $row)
		{
		    unset($selectPdfCsv[$key]['AREA_ID']);
		    unset($selectPdfCsv[$key]['LAST_UPDATED']);
            unset($selectPdfCsv[$key]['INACTIVE_AGENT']);
            unset($selectPdfCsv[$key]['DELETED_AGENT']);

		    $selectPdfCsv[$key]['LAST_UPDATED'] = Application_Helper_General::convertDate($row["LAST_UPDATED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
		}
		//--------konfigurasicsv dan pdf---------
	    if($csv)
	    {		
				$this->_helper->download->csv(array('Area Layanan','Jumlah Agen Aktif','Terakhir Diperbaharui Oleh'),$selectPdfCsv,null,'Daftar Area Layanan');
				//Application_Helper_General::writeLog('OBLS','Export to CSV');
		}
		else
		{		
				//Application_Helper_General::writeLog('OBLS','View Online Bank List');
		}	
		//-------END konfigurasicsv dan pdf------------

		$this->paging($select);
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     // print_r($whereval);die;
      }
	}
}