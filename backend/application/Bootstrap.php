<?php
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{	
	
	protected function _initApplication() 
	{
	   Zend_Registry::set('bootstrap', $this);

		$resource = $this->getPluginResource('db');
		$db = $resource->getDbAdapter();
		try {  
				$db->getConnection();
		} catch( Exception $e ) {
				header('Location: ../../failed.html');
		}
	  
		//set the Zend_Db_Table default adapter
		$config = $this->getOptions();
		$dbAdapter = Zend_Db::factory($config['resources']['db']['adapter'],$config['resources']['db']['params']);
		Zend_Db_Table_Abstract::setDefaultAdapter($dbAdapter);

	    //FORMAT NUMBER
		$locale = new Zend_Locale();
		$locale->setDefault('en_US');
		Zend_Registry::set('Zend_Locale', 'en_US');
		Zend_Locale_Format::setOptions(array('precision'=>2));
		
		$config = $this->getOptions();
		
		//// Start Multi Language
		// $defaultlanguage='en';
		$defaultlanguage='id';
		$ns = new Zend_Session_Namespace('language');
		$sessionLanguage = $ns->langCode ;
		// $sessionLanguage='id';
		
		if(!is_null($sessionLanguage)){
			$setlang = $sessionLanguage;
		}else{
			// $setlang = $locale->getLanguage();
			$setlang='id';
		}
		if (!empty($setlang) && isset($setlang)) {
			$defaultlanguage=$setlang;
			$ns->langCode = $defaultlanguage;
		}
		Zend_Loader::loadClass('Zend_Translate');
		
		$frontendOptions = array(
			'cache_id_prefix' => 'MyLanguage',
			'lifetime' => 864000,
			'automatic_serialization' => true
		);
		$backendOptions = array('cache_dir' => APPLICATION_PATH.'/../../library/data/cache/language/backend');
		$cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
		Zend_Translate::setCache($cache);
		
		$translate = new Zend_Translate('tmx', APPLICATION_PATH.'/../../library/data/languages/backend/language.tmx', $defaultlanguage);
		Zend_Registry::set('language', $translate);
		//// End Multi Language
		
		Zend_Registry::set('itemCountPerPage',$config['paging']['defaults']['itemCountPerPage']);
		Zend_Registry::set('pageRangeCount',$config['paging']['defaults']['pageRangeCount']);
		Zend_Registry::set('scrollingStyle',$config['paging']['defaults']['scrollingStyle']);
		//Zend_View_Helper_PaginationControl::setDefaultViewPartial('controls.phtml');
		
		Zend_Registry::set('bootstrap', $this);
		
		Zend_Registry::set('config',$config);
		Zend_Registry::set('date',$config['date']);
		
		if(isset($config['app']))
			Zend_Registry::set('app',$config['app']);
		
		if(isset($config['token']))
			Zend_Registry::set('token',$config['token']);
		
		if(isset($config['sms']))
			Zend_Registry::set('sms',$config['sms']);
	}
	
	protected function _initAutoLoader()	
	{
	    //set autoloader for custom library
		$loader = Zend_Loader_Autoloader::getInstance();
		$loader->registerNamespace('Application_');		
		$loader->registerNamespace('SGO_');		
		$loader->registerNamespace('Service_');		
	}
	
	
	protected function _initView() 
	{
	  $view = new Zend_View();
   	   
	  // membuka folder partials agar file phtml didalamnya dpt dipakai di view
	  $view->addScriptPath(APPLICATION_PATH . '/partials'); 
	 
	  // utk manggil fungsi yang ada di application/view/helper agar bisa dipakai di view 
	  $view->addHelperPath('Application/View/Helper/', 'Application_View_Helper');
	  $view->addHelperPath('ZendX/JQuery/View/Helper/', 'ZendX_JQuery_View_Helper');
		
	  //setup jQuery, agar dapat dipakai di view
      $view->addHelperPath('ZendX/JQuery/View/Helper/', 'ZendX_JQuery_View_Helper');
      $view->jQuery()
			 ->setLocalPath('/js/jquery/jquery-1.4.2.min.js')
      		 ->setUiLocalPath('/js/jquery/jquery-ui-1.8.2.custom.min.js')
      		 ->addStylesheet('/js/jquery/css/ui-smoothness/jquery-ui-1.8.2.custom.css');
      //enable jQuery in view
      $view->jQuery()->enable()->uiEnable();
      
	  Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer')->setView($view);   
	}

	
	protected function _initSession()
	{
		$config = array(
			'name'           => 'bosession',
			'primary'        => 'id',
			'modifiedColumn' => 'modified',
			'dataColumn'     => 'data',
			'lifetimeColumn' => 'lifetime',
			'lifetime' => '864000',
			'overrideLifetime' => '864000',
			'cookie_httponly' => false
		);
		 
		//create your Zend_Session_SaveHandler_DbTable and
		//set the save handler for Zend_Session
		Zend_Session::setSaveHandler(new Zend_Session_SaveHandler_DbTable($config));
		 /* 
		Zend_Session::setOptions(array(
		'cookie_lifetime' => 86400,
		'gc_maxlifetime'  => 86400)); //set satu hari
		//start your session!
		Zend_Session::start(); */
		
		//now you can use Zend_Session like any other time
	}
	
	
	/**
	 * initiate user login for all field related
	 */
	protected function _initUserLogin()
	{
	    if(Zend_Auth::getInstance()->hasIdentity())
	    { 
			$auth = Zend_Auth::getInstance()->getIdentity();
			$resource = $this->getPluginResource('db');
			$db = $resource->getDbAdapter();
			try {  
					$db->getConnection();
			} catch( Exception $e ) {
					header('Location: ../../failed.html');
			}
	    }
	    else
	    {
	      //update by Venny - 8 Mar 2011 - kalau gak ada registry -> anggapan nya = system => untuk masukin history dari scheduler seperti auto disbursement
	      	Zend_Registry::set('userLogin','SYSTEM');
	      	Zend_Registry::set('custLogin','SYSTEM');
	      	Zend_Registry::set('userNameLogin','SYSTEM');
	      	Zend_Registry::set('custNameLogin','SYSTEM');
			
	      	$resource = $this->getPluginResource('db');
	      	$db = $resource->getDbAdapter();
	      	try {
	      		$db->getConnection();
	      	} catch( Exception $e ) {
	      		header('Location: ../../failed.html');
	      	}
	      	
// 			$auth['GROUP']=array('2',);
// 			$groupArr = $auth['GROUP'];
// 	   	  	if(is_array($groupArr)){
// 				foreach ($groupArr as &$value) $value = "'".$value."'";
// 	   	  	}
// 	   	  	$groupArr = implode(',',$groupArr);
// 			$select = $db->select()
// 				->from(array('BPG' => 'M_FPRIVI_GROUP'),array( 'FPRIVI_ID'))
// 				->where("FGROUP_ID IN({$groupArr})");
// 			$listPrivilege = $db->fetchAll($select);
// 			$privilege = Application_Helper_Array::simpleArray($listPrivilege,'FPRIVI_ID');
			$privilege = array();
	   		Zend_Registry::set('privilege',$privilege);
	    }
	}
	
	protected function _initActionHelper()
	{
		Zend_Controller_Action_HelperBroker::addPrefix('SGO_Helper');
	}
	
	protected function _initPlugin(){

		$front=Zend_Controller_Front::getInstance();
	    $front->registerPlugin(new Application_Backend_Auth());
		
		$protect = new SGO_Security_CsrfProtect(array(
            'expiryTime' => 864000, //will make the CSRF key expire in 5 minutes.
            'keyName' => 'safetycheck', //will make the CSRF form element be called "safetycheck". Defaults to "csrf"
			'cookie_httponly'	=> false,
		));
        $front->registerPlugin($protect);
//         $front->registerPlugin(new Application_AutoEscapeViewVariable());   
	}
}