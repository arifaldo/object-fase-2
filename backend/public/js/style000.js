ddsmoothmenu.init({
	mainmenuid: "menu_left", //Menu DIV id
	orientation: 'v', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu-v', //class added to menu's outer DIV
	customtheme: ["#00bad3","#AEAEAE"], //#FF9900 
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
});
