//punya acai 5-jan-2011
function popupWindow(url,title){
  var new_window
  var xpos=(screen.width/2)-(350);
  var ypos=(screen.height/2)-(200);
  new_window = window.open(url,"New_Window", "left="+xpos+",top="+ypos+",width=700,height=400,location=no,menubar=no,resizable=yes,scrollbars=yes,scrollable=yes,status=no,titlebar=no,toolbar=no");
  new_window.focus();
}

function confirmation(msg,url) {
  var answer = confirm(msg);
  if(answer){ window.location = url; }
}

function forceDecimalOCBC(evt){
  var charCode = (evt.which) ? evt.which : event.keyCode;
  if (charCode == 44 || charCode == 8 || (charCode >= 48 && charCode <= 57) || charCode == 13)return true;
  return false;
}

function forceAlnumOCBC(evt){
  var charCode = (evt.which) ? evt.which : event.keyCode;
  if (charCode == 8 || (charCode >= 48 && charCode <= 57) || (charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 122) || charCode == 13)return true;
  return false;
}

function forceNumberOCBC(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if ((charCode >= 48 && charCode <= 57) || charCode == 8 || charCode == 13 )
        return true;
    return false;
}

function forceNumber(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if ((charCode >= 48 && charCode <= 57) || charCode == 8 )
        return true;
    return false;
}

function NumberOnly(number,object){
  if (object!='')
	document.getElementById(object).value = trim(number.replace(/[^0-9]/g, ''), ' ');
 else
	    return trim(number.replace(/[^0-9]/g, ''), ' ');;
}

function AlnumOnly(number,object){
	  if (object!='')
		document.getElementById(object).value = trim(number.replace(/[^0-9A-Za-z]/g, ''), ' ');
	 else
		    return trim(number.replace(/[^0-9A-Za-z]/g, ''), ' ');;
	}

function forceCurrencyOCBC(evt){

        var charCode = (evt.which) ? evt.which : event.keyCode

        if (charCode == 44 || charCode == 9|| charCode == 46 || charCode == 8 || (charCode >= 48 && charCode <= 57) || charCode == 13)

            return true;

        return false;

}

function NumFrmt(number,object) {
  var iMin, iMax, sNumber, sExclude;
  var tempNumber;
  
  iMin = 0;
  iMax = 0;                    
  sNumber = '';
  sExclude = 0;
 
  number = currencyOnly(number);
  number = number.replace(/\,/g,'');
  number = ltrim(number, '0');
  
  if(number.length>=1){
	  tempNumber = number.split('.');
	  
	  if (tempNumber.length>1){
		  if(tempNumber[1].length==0)
		  {
			  tempNumber[1]='.00';
		  }
		  else if(tempNumber[1].length==1)
		  {
			  tempNumber[1]='.'+tempNumber[1]+'0';
		  }
		  else
		  {
			  tempNumber[1]=tempNumber[1].substring(0,2);
			  tempNumber[1]='.'+tempNumber[1];
			  // rounded decimal, by Tomo
			  // tempNumber[1] = (Math.round(tempNumber[1]*Math.pow(10,2)))/Math.pow(10,2);
			  // tempNumber[1] = String(tempNumber[1]);
			  // tempNumber[1] = tempNumber[1].substring(1,4);
			  // -------------------------------------------
		  }
	  }
	  else{
		  tempNumber[1]='.00';
	  }

	  number = tempNumber[0];
	  if(number.length==0){
		  number='0';
	  }
	  iMax = parseInt(number.length / 3);
	  if(iMax>0) {
	    sNumber = number.substr(0,number.length - iMax * 3);
	    sExclude = sNumber.length;
	    for(iMin=0;iMin<iMax;iMin++) {
	      if(sNumber!='') sNumber += ',';
	      sNumber += number.substr(iMin * 3 + sExclude,3);
	
	    }
	    if (object!='' && object!=null)
	      document.getElementById(object).value = sNumber+tempNumber[1];
	    else
	      return sNumber+tempNumber[1];
	  }
	  else{
		  if (object!='' && object!=null)
		      document.getElementById(object).value = number+tempNumber[1];
		  else
			  return number+tempNumber[1];
	  }
	}
  	return number;
}

function iNumFrmt(number,object) {
  if (object!='')
    document.getElementById(object).value = number.replace(/\,/g,'');
  else
    return number.replace(/\,/g,'');
}

/*function NumFrmtUS(number,object,decimal) {
  var iMin, iMax, sNumber, sExclude, sDec, tNumber;

  iMin = 0;
  iMax = 0;
  sNumber = '';
  sExclude = 0;
  sDec = '';
  tNumber = '';
  if (number.indexOf('.') > 0) {
    //Mengambil Angka Desimal dimulai dari simbol '.'
    sDec = number.substr(number.indexOf('.'),decimal+1);
    tNumber = number.substr(0,number.length - sDec.length);
    //Menghitung Banyak Pemisah Ribuan
    iMax = parseInt((number.length - sDec.length) / 3);
  }
  else {
    //Menghitung Banyak Pemisah Ribuan
    tNumber = number;
    iMax = parseInt(number.length / 3);
  }
  if(iMax>0) {
    //Mengambil Angka yang tidak termasuk dalam pemisah ribuan
    if (sDec != '')
      sNumber = number.substr(0,number.length - iMax * 3 - sDec.length);
    else
      sNumber = number.substr(0,number.length - iMax * 3);
    //Menghitung posisi pointer awal untuk penggabungan string pemisah ribuan
    sExclude = sNumber.length;
    for(iMin=0;iMin<iMax;iMin++) {
      if(sNumber!='') sNumber += ',';
      sNumber += tNumber.substr(iMin * 3 + sExclude,3);
    }
    if (object!='')
      document.getElementById(object).value = sNumber + sDec;
    else
      return sNumber + sDec;
  }
  else
    return number;
}*/
function NumFrmtUS(number,object) {
	
	
  var iMin, iMax, sNumber, sExclude;
  var tempNumber;
  
  iMin = 0;
  iMax = 0;                    
  sNumber = '';
  sExclude = 0;
 
  
  //-------- EDITED by christian ---------------
  number = currencyOnly(number);
  number = number.replace(/\,/g,'');
  
  if(number == '' || number == 0) number = '0';
  if(number != 0)  number = ltrim(number,'0');
  //------- END EDITED by christian ------------
  
  // rounded decimal, by Tomo
  number = Math.round(number*Math.pow(10,2))/Math.pow(10,2);
  number = String(number);
  // -------------------------------------------
  
  if(number.length>=1){
	  tempNumber = number.split('.');
	  if (tempNumber.length>1){
		  if(tempNumber[1].length==0)
		  {
			  tempNumber[1]='.00';
		  }
		  else if(tempNumber[1].length==1)
		  {
			  tempNumber[1]='.'+tempNumber[1]+'0';
		  }	  
		  else
		  {
			  tempNumber[1]=tempNumber[1].substring(0,2);
			  tempNumber[1]='.'+tempNumber[1];
		  }
	  }
	  else{
		  tempNumber[1]='.00';
	  }
	  
	  number = tempNumber[0];
	  if(number.length==0){
		  number='0';
	  }
	  iMax = parseInt(number.length / 3);
	  if(iMax>0) {
	    sNumber = number.substr(0,number.length - iMax * 3);
	    sExclude = sNumber.length;
	    for(iMin=0;iMin<iMax;iMin++) {
	      if(sNumber!='') sNumber += ',';
	      sNumber += number.substr(iMin * 3 + sExclude,3);
	
	    }
	    if (object!='' && object!=null)
	      document.getElementById(object).value = sNumber+tempNumber[1];
	    else
	      return sNumber+tempNumber[1];
	  }
	  else{
		  if (object!='' && object!=null)
		      document.getElementById(object).value = number+tempNumber[1];
		  else
			  return number+tempNumber[1];
	  }
	}
  	return number;
}
function iNumFrmtUS(number,object) {
  if (object!='')
    document.getElementById(object).value = number.replace(/\,/g,'');
  else
    return number.replace(/\,/g,'');
}


function currencyOnly(number){
	number = trim(number.replace(/[^0-9\.,]/g, ''), ' ');
	return number;
}

function AddZero(str,digit) {
  var temp;
  temp=str;
  while(temp.length() < digit) {
    temp = '0'+temp;
  }
  return temp;
}

function trim(str, chars) {
	return ltrim(rtrim(str, chars), chars);
}
 
function ltrim(str, chars) {
	chars = chars || "\\s";
	return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
}
 
function rtrim(str, chars) {
	chars = chars || "\\s";
	return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
}

//LAST UPDATED = 1 DEC 09 - NEW FUNCTION TO DISPLAY POP UP WINDOW - function showPopUpWindow(pagesource)
//LAST UPDATED = 20 OCT 09 - ALLOW '/' CHARACTER - ASCII - 47, FUNCTION = noSpecialChar3
//LAST UPDATED = 8 SEPT 09 - BLOCK '-' CHARACTER - ASCII = 45, FUNCTION = noSpecialChar3
window.history.forward(1);

function proceed(process)
{
	var x = document.forms.length;
	if(x > 1){
		document.forms[1].process.value = process;
		document.forms[1].submit();
	}
	else{
		document.forms[0].process.value = process;
		document.forms[0].submit();
	}
}

function proceed2(process, num, values, page)
{
	document.forms[0].process.value = process;
	document.forms[0].num.value = num;
	document.forms[0].values.value = values;
	document.forms[0].page.value = page;
	document.forms[0].submit();
}

function proceed3(process, num, fid_num, values, page)
{
	document.forms[0].process.value = process;
	document.forms[0].num.value = num;
	document.forms[0].fid_num.value = fid_num;
	document.forms[0].values.value = values;
	document.forms[0].page.value = page;
	document.forms[0].submit();
}

function confirmedproceed(process, num) 
{
	if (confirm (" Are you sure to delete this record?")) 
	{	
		document.forms[0].process.value = process;
		document.forms[0].num.value = num;
		document.forms[0].submit();
	}
}

function confirmedproceed2(process, num, fid_num) 
{
	if (confirm (" Are you sure to delete this record?")) 
	{	
		document.forms[0].process.value = process;
		document.forms[0].num.value = num;
		document.forms[0].fid_num.value = fid_num;
		document.forms[0].submit();
	}
}

function confirmedproceed3(process, num, page) 
{
	if (confirm (" Are you sure to reject this invoice?")) 
	{	
		document.forms[0].process.value = process;
		document.forms[0].num.value = num;
		document.forms[0].page.value = page;
		document.forms[0].submit();
	}
}

function goPage(page)
{
	document.forms[0].page.value = page;
	document.forms[0].submit();
}

function submitonce(theform)
{
	//if IE 4+ or NS 6+
	if (document.all||document.getElementById)
	{
		//screen thru every element in the form, and hunt down "submit" and "reset"
		for (i=0;i<theform.length;i++)
		{
			var tempobj=theform.elements[i]				
			if (tempobj.type.toLowerCase()=="submit"||tempobj.type.toLowerCase()=="reset")
			//disable em
			tempobj.disabled=true
		}
	}
}

//calculate the ASCII code of the given character
function CalcKeyCode(aChar)
{
	var character = aChar.substring(0,1);
	var code = aChar.charCodeAt(0);
	return code;
}

function old_checkNumber(val)
{
	var strPass = val.value;
	var strLength = strPass.length;
	var lchar = val.value.charAt((strLength) - 1);
	var cCode = CalcKeyCode(lchar);

	/* Check if the keyed in character is a number
		 do you want alphabetic UPPERCASE only ?
		 or lower case only just check their respective
		 codes and replace the 48 and 57 */

	if (cCode < 48 || cCode > 57 )
	{
		var myNumber = val.value.substring(0, (strLength) - 1);
		val.value = myNumber;
	}
	return false;
}

function checkNumber(evt)
{
	var whichCode = (evt.which) ? evt.which : event.keyCode;
	var IsnoSpecialChar = true;

	if ((whichCode == 0) || (whichCode == 13) ||(whichCode == 8) || (whichCode == 32) || (whichCode >= 48 && whichCode <= 57))
	{
		IsnoSpecialChar = true;
	}
	else
	{
		IsnoSpecialChar = false;
	}

	return IsnoSpecialChar;
}

function textCounter(field,cntfield,maxlimit)
{
	if (field.value.length > maxlimit) // if too long...trim it! 
		field.value = field.value.substring(0, maxlimit); 
	else 
		cntfield.value = maxlimit - field.value.length; 
}

function popupImageWindow(url)
{
	window.open(url,'popupImageWindow','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=yes,copyhistory=no,width=300,height=300,screenX=150,screenY=150,top=150,left=150')
}

function NewWindow(mypage,myname,w,h,scroll,pos)
{
	if (pos=="random")
	{
		LeftPosition=(screen.width)?Math.floor(Math.random()*(screen.width-w)):100;
		TopPosition=(screen.height)?Math.floor(Math.random()*((screen.height-h)-75)):100;
	}
	if (pos=="center")
	{
		LeftPosition=(screen.width)?(screen.width-w)/2:100;
		TopPosition=(screen.height)?(screen.height-h)/2:100;
	}
	else if ((pos!="center" && pos!="random") || pos==null)
	{
		LeftPosition=0;TopPosition=20
	}
	settings='width='+w+',height='+h+',top='+TopPosition+',left='+LeftPosition+',scrollbars='+scroll+',location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no';
	win=window.open(mypage,myname,settings);
}

function modelesswin(url,mwidth,mheight)
{
	LeftPosition=(screen.width)?(screen.width-mwidth)/2:100;
	TopPosition=(screen.height)?(screen.height-mheight)/2:100;
	settings='width='+mwidth+'px,height='+mheight+'px,top='+TopPosition+',left='+LeftPosition+',scrollbars=0,resizable=no,location=no,directories=no,status=no,menubar=no,toolbar=no';

	if (document.all&&window.print) //if ie5
		eval('window.showModelessDialog(url,window,"status:false;dialogWidth:'+mwidth+'px;dialogHeight:'+mheight+'px")')
	else
		//eval('window.open(url,"","width='+mwidth+'px,height='+mheight+'px,resizable=1,scrollbars=1")')
		eval('window.open(url,"",settings)')
}

/* ----------------------------- begin countdown idle time ----------------------------- */
function cdLocalTime(container, servermode, offsetMinutes, targetdate, currentDate, debugmode){
	if (!document.getElementById || !document.getElementById(container)) return
	this.container=document.getElementById(container)
	//var servertimestring=(servermode=="server-php")? '<? print date("F d, Y H:i:s", time())?>' : (servermode=="server-ssi")? '<!--#config timefmt="%B %d, %Y %H:%M:%S"--><!--#echo var="DATE_LOCAL" -->' : '<%= Now() %>'
	var servertimestring=(servermode=="server-php")? currentDate : (servermode=="server-ssi")? '<!--#config timefmt="%B %d, %Y %H:%M:%S"--><!--#echo var="DATE_LOCAL" -->' : '<%= Now() %>'
	this.localtime=this.serverdate=new Date(servertimestring)
	this.targetdate=new Date(targetdate)
	this.debugmode=(typeof debugmode!="undefined")? 1 : 0
	this.timesup=false
	this.localtime.setTime(this.serverdate.getTime()+offsetMinutes*60*1000) //add user offset to server time
	this.updateTime()
}

cdLocalTime.prototype.updateTime=function(){
	var thisobj=this
	this.localtime.setSeconds(this.localtime.getSeconds()+1)
	setTimeout(function(){thisobj.updateTime()}, 1000) //update time every second
}

cdLocalTime.prototype.displaycountdown=function(baseunit, functionref){
	this.baseunit=baseunit
	this.formatresults=functionref
	this.showresults()
}

cdLocalTime.prototype.showresults=function(){
	var thisobj=this
	var debugstring=(this.debugmode)? "<p style=\"background-color: #FCD6D6; color: black; padding: 5px\"><big>Debug Mode on!</big><br /><b>Current Local time:</b> "+this.localtime.toLocaleString()+"<br />Verify this is the correct current local time, in other words, time zone of count down date.<br /><br /><b>Target Time:</b> "+this.targetdate.toLocaleString()+"<br />Verify this is the date/time you wish to count down to (should be a future date).</p>" : ""
	
	var timediff=(this.targetdate-this.localtime)/1000 //difference btw target date and current date, in seconds
	if (timediff<0){ //if time is up
		this.timesup=true
		this.container.innerHTML=debugstring+this.formatresults()
		return
	}
	var oneMinute=60 //minute unit in seconds
	var oneHour=60*60 //hour unit in seconds
	var oneDay=60*60*24 //day unit in seconds
	var dayfield=Math.floor(timediff/oneDay)
	var hourfield=Math.floor((timediff-dayfield*oneDay)/oneHour)
	var minutefield=Math.floor((timediff-dayfield*oneDay-hourfield*oneHour)/oneMinute)
	var secondfield=Math.floor((timediff-dayfield*oneDay-hourfield*oneHour-minutefield*oneMinute))
	if (this.baseunit=="hours"){ //if base unit is hours, set "hourfield" to be topmost level
		hourfield=dayfield*24+hourfield
		dayfield="n/a"
	}
	else if (this.baseunit=="minutes"){ //if base unit is minutes, set "minutefield" to be topmost level
		minutefield=dayfield*24*60+hourfield*60+minutefield
		dayfield=hourfield="n/a"
	}
	else if (this.baseunit=="seconds"){ //if base unit is seconds, set "secondfield" to be topmost level
		var secondfield=timediff
		dayfield=hourfield=minutefield="n/a"
	}
	this.container.innerHTML=debugstring+this.formatresults(dayfield, hourfield, minutefield, secondfield)
	setTimeout(function(){thisobj.showresults()}, 1000) //update results every second
}

function formatresults(){
	if (this.timesup==false){//if target date/time not yet met
		var displaystring="<span style='background-color: #CFEAFE'>"+arguments[1]+" hours "+arguments[2]+" minutes "+arguments[3]+" seconds</span> left until launch time"
	}
	else{ //else if target date/time met
		var displaystring="Launch time!"
	}
	return displaystring
}
/* ----------------------------- end countdown idle time ----------------------------- */

/* ----------------------------- begin cookie ----------------------------- */
function Get_Cookie(check_name) {
	// first we'll split this cookie up into name/value pairs
	// note: document.cookie only returns name=value, not the other components
	var a_all_cookies = document.cookie.split( ';' );
	var a_temp_cookie = '';
	var cookie_name = '';
	var cookie_value = '';
	var b_cookie_found = false; // set boolean t/f default f

	for (i = 0; i < a_all_cookies.length; i++)
	{
		// now we'll split apart each name=value pair
		a_temp_cookie = a_all_cookies[i].split( '=' );
		// and trim left/right whitespace while we're at it
		cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');

		// if the extracted name matches passed check_name
		if (cookie_name == check_name)
		{
			b_cookie_found = true;
			cookie_value = unescape(a_temp_cookie[1].replace(/^\s+|\s+$/g, ''));
			return cookie_value;
			break;
		}
		a_temp_cookie = null;
		cookie_name = '';
	}
	if ( !b_cookie_found )
	{
		return null;
	}
}

function Set_Cookie(name, value, expires, path, domain, secure) {
	// set time, it's in milliseconds
	var today = new Date();
	today.setTime(today.getTime());
	// if the expires variable is set, make the correct expires time, the
	// current script below will set it for x number of days, to make it
	// for hours, delete * 24, for minutes, delete * 60 * 24
	if (expires)
	{
		expires = expires * 1000 * 60 * 60 * 24;
	}
	//alert( 'today ' + today.toGMTString() );// this is for testing purpose only
	var expires_date = new Date( today.getTime() + (expires) );
	//alert('expires ' + expires_date.toGMTString());// this is for testing purposes only

	document.cookie = name + "=" +escape( value ) +
		( ( expires ) ? ";expires=" + expires_date.toGMTString() : "" ) + //expires.toGMTString()
		( ( path ) ? ";path=" + path : "" ) + 
		( ( domain ) ? ";domain=" + domain : "" ) +
		( ( secure ) ? ";secure" : "" );
}

function Delete_Cookie(name, path, domain) {
	if ( Get_Cookie( name ) ) document.cookie = name + "=" +
			( ( path ) ? ";path=" + path : "") +
			( ( domain ) ? ";domain=" + domain : "" ) +
			";expires=Thu, 01-Jan-1970 00:00:01 GMT";
}
/* ----------------------------- end cookie ----------------------------- */

function checkBrowser()
{
	// convert all characters to lowercase to simplify testing
	var agt=navigator.userAgent.toLowerCase();

// *** BROWSER VERSION ***
// Note: On IE5, these return 4, so use is_ie5up to detect IE5.
var is_major = parseInt(navigator.appVersion);
var is_minor = parseFloat(navigator.appVersion);

	var is_ie = ((agt.indexOf("msie") != -1) && (agt.indexOf("opera") == -1));
	var is_gecko = (agt.indexOf('gecko') != -1);		

	if (is_ie || is_gecko)
	{
		if (is_ie)
		{
	    var is_ie3    = (is_ie && (is_major < 4));
	    var is_ie4    = (is_ie && (is_major == 4) && (agt.indexOf("msie 4")!=-1) );
	    var is_ie5    = (is_ie && (is_major == 4) && (agt.indexOf("msie 5.0")!=-1) );
	    var is_ie5_5  = (is_ie && (is_major == 4) && (agt.indexOf("msie 5.5") !=-1));
	    var is_ie6up  = (is_ie && !is_ie3 && !is_ie4 && !is_ie5 && !is_ie5_5);

			if (!is_ie6up)
				location.href = "../not_support_browser_version.php"
		}
	}
	else
	{
		location.href = "../not_support_browser.php"
	}
}

function upperCase(x)
{
	var y=document.getElementById(x).value
	document.getElementById(x).value=y.toUpperCase()
}

function ignoreSpaces(string)
{
	var temp = "";
	string = '' + string;
	splitstring = string.split(" ");
	for(i = 0; i < splitstring.length; i++)
		temp += splitstring[i];
	return temp;
}

function noSpecialChar(evt)
{
	var whichCode = (evt.which) ? evt.which : event.keyCode;
	var IsnoSpecialChar = true;
	if ((whichCode > 32 && whichCode < 48) || (whichCode > 57 && whichCode < 65) || (whichCode > 90 && whichCode < 97))
	{
		IsnoSpecialChar = false
	}
	return IsnoSpecialChar;
}

function noSpecialChar2(evt)
{	
	// allow comma(,) and period(.)
	var whichCode = (evt.which) ? evt.which : event.keyCode;
	var IsnoSpecialChar = true;

	if ((whichCode > 32 && whichCode < 44) || (whichCode > 44 && whichCode < 45) || (whichCode > 47 && whichCode < 48) || (whichCode > 57 && whichCode < 65) || (whichCode > 90 && whichCode < 97))
	{
		IsnoSpecialChar = false
	}
	return IsnoSpecialChar;
}

function noSpecialChar3(evt)
{
	//65-90 97-122 44-46 32 8 48-57
	var whichCode = (evt.which) ? evt.which : event.keyCode;
	var IsnoSpecialChar = true;

	//if ((whichCode > 32 && whichCode < 44) || (whichCode > 48 && whichCode < 48) || (whichCode > 57 && whichCode < 65) || (whichCode > 90 && whichCode < 97))
	if ((whichCode == 0) || (whichCode == 13) ||(whichCode == 8) || (whichCode == 32) || (whichCode == 44) || (whichCode == 46) || (whichCode == 47) || (whichCode >= 65 && whichCode <= 90) || (whichCode >= 48 && whichCode <= 57) || (whichCode >= 97 && whichCode <= 122))
	{
		IsnoSpecialChar = true;
	}
	else
	{
		IsnoSpecialChar = false;
	}

	return IsnoSpecialChar;
}

/* ----------------------------- begin currency format ----------------------------- */
function CurrencyFormatted(amount)
{
	if (amount.length > 14) { amount = 0 }
	var i = parseFloat(amount);
	if(isNaN(i)) { i = 0.00; }
	var minus = '';
	if(i < 0) { minus = '-'; }
	i = Math.abs(i);
	i = parseInt((i + .005) * 100);
	i = i / 100;
	s = new String(i);	
	if(s.indexOf('.') < 0) { s += '.00'; }
	if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
	s = minus + s;
	return s;
} // function CurrencyFormatted()

function CommaFormatted(amount)
{
	var delimiter = ",";
	var a = amount.split('.',2)
	var d = a[1];
	var i = parseInt(a[0]);
	if(isNaN(i)) { return ''; }
	var minus = '';
	if(i < 0) { minus = '-'; }
	i = Math.abs(i);
	var n = new String(i);
	var a = [];
	while(n.length > 3)
	{
		var nn = n.substr(n.length-3);
		a.unshift(nn);
		n = n.substr(0,n.length-3);
	}
	if(n.length > 0) { a.unshift(n); }
	n = a.join(delimiter);
	if(d.length < 1) { amount = n; }
	else { amount = n + '.' + d; }
	amount = minus + amount;
	return amount;
} // function CommaFormatted()

function CurrencyUpdate(amount)
{
	var s = new String();
	s = CurrencyFormatted(amount);
	s = CommaFormatted(s);
	return s;
	//document.testing.result.value = s;
} // function CurrencyUpdate()
/* ----------------------------- end currency format ----------------------------- */

/*-----------------------------------------------------------------------------
	Hide status bar msg II script- by javascriptkit.com
	Visit JavaScript Kit (http://javascriptkit.com) for script
	Credit must stay intact for use
	-----------------------------------------------------------------------------*/
var hidestatus;
function hidestatus()
{
	window.status=''
	return true
}

if (document.layers)
document.captureEvents(Event.MOUSEOVER | Event.MOUSEOUT)

document.onmouseover=hidestatus
document.onmouseout=hidestatus

/* ------------------------- begin no right mouse click ------------------------- */
//Disable right click script III- By Renigade (renigade@mediaone.net)
//For full source code, visit http://www.dynamicdrive.com

var message="";
function clickIE() {if (document.all) {(message);return false;}}

function clickNS(e)
{
	if (document.layers||(document.getElementById&&!document.all)) 
	{
		if (e.which==2||e.which==3) {(message);return false;}
	}
}

if (document.layers) 
{
	document.captureEvents(Event.MOUSEDOWN);document.onmousedown=clickNS;
}
else{document.onmouseup=clickNS;document.oncontextmenu=clickIE;}

document.oncontextmenu=new Function("return false")
/* ------------------------- end no right mouse click ------------------------- */

/* ------------------------- [begin set same curr in biller payment]  ------------------------- */
function setcurr()
{
	var s= document.payment.PAYERCODE.value;
	//var lng =(document.payment.PAYERCODE.value.length);
	//var blng = lng - 3;
	var splitrest = s.split("~");
	var curr
	var benef
	var msg
	var psnum
	var ptype
	
	if (splitrest[3]==null)
	{
		curr="";
	}
	else
	{
		curr=splitrest[3];
	}
		if (splitrest[1]==null)
		{
			benef="";
		}
		else
		{
			benef=splitrest[1];
		}
	if (splitrest[4]==null)
	{
		msg="";
	}
	else
	{
		msg="/"+splitrest[4];
	}
	//alert(curr);
	
	document.payment.ACBENEF_CURRCODE.value = curr;
	//splitrest[3];
	document.payment.ACBENEF.value = benef;
	psnum = "/"+document.payment.PS_NUM.value;
	document.payment.TRA_MESSAGE.value =  "PG/"+splitrest[0]+psnum;
	ptype = document.payment.PAYMENTYPE.value;
	document.payment.TRA_MESSAGE.value =  splitrest[0]+" "+ptype;
	//document.payment.ACBENEF_CURRCODE.value =  s.substring(blng,lng);
	//alert(s);
	//alert(s.substring(blng,lng));
	//document.payment.ACBENEF_CURRCODE.value =  s.substring(blng,lng);
	//window.location.href = sURL;
}
/* ------------------------- [end set same curr biller payment] ------------------------- */


/* --------		1DES09_FRED	------- */
/* ------------------------- [begin set same curr in biller iata payment]  ------------------------- */
function setcurriata()
{
	var s= document.payment.ACCTSRC.value;
	var splitrest = s.split("~");
	var curr
	
	if (splitrest[2]==null)
	{
		curr="";
	}
	else
	{
		curr=splitrest[2];
	}
	//alert(curr);
	document.payment.ACBENEF_CURRCODE.value = curr;
}
/* ------------------------- [end set same curr biller iata payment] ------------------------- */

/* ------------------------- [begin set history account statement]  ------------------------- */
function tacodeselect1()
{
	document.payment.add_tacode.style.visibility = "visible";
	document.payment.TA_CODE.style.visibility = "visible";
	document.payment.TACODESEL.style.visibility = "hidden";
	document.payment.managelist.style.visibility = "hidden";
//	document.payment.add_tacode.disabled=false;
//	document.payment.TA_CODE.disabled=false;
//	document.payment.TACODESEL.disabled=true;
//	document.payment.managelist.disabled=true;
	
//	document.getElementById(satu).style.display = "block";
//	document.getElementById(satu).style.visibility = "visible";
//	document.getElementById(dua).style.visibility = "hidden";
	//alert("aaaaaaa");
	//TACODESEL
}
function tacodeselect2()
{
	document.payment.add_tacode.style.visibility = "hidden";
	document.payment.TA_CODE.style.visibility = "hidden";
	document.payment.TACODESEL.style.visibility = "visible";
	document.payment.managelist.style.visibility = "visible";
//	document.payment.add_tacode.style.disabled=true;
//	document.payment.TA_CODE.style.disabled=true;
//	document.payment.TACODESEL.style.disabled=false;
//	document.payment.managelist.style.disabled=false;
	
//	document.getElementById(satu).style.display = "none";
//	document.getElementById(satu).style.visibility = "hidden";
//	document.getElementById(dua).style.visibility = "visible";
	//alert("aaaaaaa");
	//TACODESEL
}
/* ------------------------- [end set same curr biller iata payment] ------------------------- */


/* ------------------------- [begin set history account statement]  ------------------------- */

function hide(id) {
	document.getElementById(id).style.visibility = "hidden";
}
function show(id) {
	document.getElementById(id).style.visibility = "visible";
}

function setdateend(id) {
	var date= document.getElementById(id).value;
	//var lng =(document.payment.PAYERCODE.value.length);
	//var blng = lng - 3;
	var splitdate = date.split("/");
	
	nextday = parseInt(splitdate[0])+1;
	nextdatetime = nextday+"/"+splitdate[1]+"/"+splitdate[2];
	document.getElementById("DATE_END1").value = nextdatetime;
}

//CREATED 1 DEC 09 - CHECKLIST IATA - BUILD DETAIL PAGE OF BILL PAYMENT
function showPopUpWindow(pagesource, wd, hd)	{
	var win = null;
	var w = 650, h = 300;
	
	w = (typeof wd !== 'undefined') ? wd : w;
	h = (typeof hd !== 'undefined') ? hd : h;

	if (win && !win.close())
	{
		win.close();
	}

	LeftPosition=(screen.width)?(screen.width-w)/2:100;
	TopPosition=(screen.height)?(screen.height-h)/2:100;

	settings='width='+w+',height='+h+',top='+TopPosition+',left='+LeftPosition+',scrollbars=1,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=no';
	win=window.open(pagesource,'sgo_popup_window',settings);
	win.focus();
}

/* ------------------------- [end set history account statement] ------------------------- */

function changeAction(url) 
{
    document.form.action = url;
    document.form.submit();
}

// function proceed(process)
// {
// 	document.forms[0].process.value = process;
// 	document.forms[0].submit();
// }

function changeMemberCommunity(commCode, custID)
{
	// Remove all options
	document.forms[0].memberCode.options.length = 0;
	document.forms[0].memberCode.options[0] = new Option('-- Any Value --', '');

	// Generate select menu with new options
	// new Option(text, value, defaultSelected, selected)
	if (commCode != '')
	{
		splitstr = commCode.split("~");
		custRole = splitstr[0];
		commCode = splitstr[1];
	
		if (custRole == "P")
		{
			if (memberPerComm[commCode] != null)
			{
				var len = memberPerComm[commCode].length;
				for(var i=0; i<len; i++) 
				{
					document.forms[0].memberCode.options[i+1] = new Option(memberPerComm[commCode][i], memberPerComm[commCode][i]);
				}
			}
		}
		else if (custRole == "M")
		{
			document.forms[0].memberCode.options[0] = new Option(custID, custID);
		}
	}
	else	// any value, list all member
	{
		var len = memberAll.length;
		for(var i=0; i<len; i++) 
		{
			document.forms[0].memberCode.options[i+1] = new Option(memberAll[i], memberAll[i]);
		}
	}
}