<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/BulkPayment.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'General/CustomerUser.php';
require_once 'General/Customer.php';

class multidebet_BulkController extends Application_Main
{
	protected $_moduleDB = 'RTF';

	protected $_destinationUploadDir = '';
	protected $_listAccValidate = '';
	protected $_maxRow = '';

	public function initController(){
		$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';

		$setting = new Settings();
		$this->_maxRow = $setting->getSetting('max_import_bulk');

		$Customer = new Customer($this->_custIdLogin,$this->_userIdLogin);
		$AccArr = $Customer->getAccounts();
		$this->view->AccArr =  $AccArr;
		$this->_listAccValidate = Application_Helper_Array::simpleArray($AccArr,'ACCT_NO');
	}

	public function indexAction()
	{
			$this->_helper->layout()->setLayout('newlayout');
		$this->setbackURL();

		$this->view->ccyArr = $this->getCcy();
		$this->view->PSEFDATE = Application_Helper_General::convertDate($this->getCurrentDate());

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}
    	}

		if($this->_request->isPost() )
		{
			$filter = new Application_Filtering();
			$confirm = false;

			$PS_SUBJECT 		= $filter->filter($this->_request->getParam('PSSUBJECT'), "PS_SUBJECT");
			$PS_EFDATE 			= $filter->filter($this->_request->getParam('PSEFDATE'), "PS_DATE");
			$ACBENEF 			= $filter->filter($this->_request->getParam('ACBENEF'), "ACCOUNT_NO");
			$ACBENEF_BANKNAME 	= $filter->filter($this->_request->getParam('ACBENEF_BANKNAME'), "ACCOUNT_NAME");
			$ACBENEF_ALIAS 		= $filter->filter($this->_request->getParam('ACBENEF_ALIAS'), "ACCOUNT_ALIAS");
			$ACBENEF_CCY 		= $filter->filter($this->_request->getParam('CURR_CODE'), "SELECTION");

			$minLen = 10;
			$maxLen = 20;
			$error_msg[0] = "";

			if (Zend_Validate::is($ACBENEF, 'NotEmpty') == false)
				$error_msg[0] = $this->language->_('Beneficiary Account cannot be left blank').".";
			elseif (Zend_Validate::is($ACBENEF, 'Digits') == false)
				$error_msg[0] = $this->language->_('Beneficiary Account must be numeric').".";
			elseif (strlen($ACBENEF) < $minLen || strlen($ACBENEF) > $maxLen)
				//$error_msg[0] = "Beneficiary Account length should be between $minLen and $maxLen.";
				$error_msg[0] = $this->language->_('Beneficiary Account length should be between 10 and 20.')."";

			/*elseif ($ACBENEF_ALIAS == "")
				$error_msg[0] = $this->language->_('Beneficiary Alias Name cannot be left blank.')."";*/
			elseif (strlen($ACBENEF_ALIAS) > 35)
				$error_msg[0] = $this->language->_('Maximum lengths of Alias Name is 35 characters. Please correct it').".";
			else if ($ACBENEF_CCY == "")
				$error_msg[0] = $this->language->_('Currency cannot be left blank').".";
			else if(!$PS_EFDATE)
				$error_msg[0] = $this->language->_('Payment Date can not be left blank').".";
			else
			{
				$paramSettingID = array('range_futuredate', 'auto_release_payment');

				$settings = new Application_Settings();
				$settings->setSettings(null, $paramSettingID);       // Zend_Registry => 'APPSETTINGS'
				$ccyList = $settings->setCurrencyRegistered();    // Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
				$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
				$AccArr = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR")));	// show acc in IDR only

				$adapter = new Zend_File_Transfer_Adapter_Http();

				$adapter->setDestination ( $this->_destinationUploadDir );

				$extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv'));
				$extensionValidator->setMessage(
					$this->language->_('Extension file must be').' *.csv'
				);

				$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
				$sizeValidator->setMessage(
					'File exceeds maximum size'
				);

				$adapter->setValidators ( array (
					$extensionValidator,
					$sizeValidator,
				));

				if ($adapter->isValid ())
				{
					$sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
					$newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

					$adapter->addFilter ( 'Rename',$newFileName  );

					if ($adapter->receive ())
					{
						//PARSING CSV HERE
						$csvData = $this->parseCSV($newFileName);
						//after parse delete document temporary
						@unlink($newFileName);
						//end

						$totalRecords = count($csvData);
						if($totalRecords)
						{
							unset($csvData[0]);
							$totalRecords = count($csvData);
						}

						if($totalRecords)
						{
							if($totalRecords <= $this->_maxRow)
							{
								$rowNum = 0;

								$paramPayment = array( "CATEGORY"      		=> "BULK DEBET",
													   "FROM"       		=> "I",
													   "PS_NUMBER"     		=> "",
													   "PS_SUBJECT"    		=> $PS_SUBJECT,
													   "PS_EFDATE"     		=> $PS_EFDATE,
													   "_dateFormat"    	=> $this->_dateDisplayFormat,
													   "_dateDBFormat"    	=> $this->_dateDBFormat,
													   "_addBeneficiary"   	=> $this->view->hasPrivilege('BADA'), // privi BADA (Add Beneficiary)
													   "_beneLinkage"    	=> $this->view->hasPrivilege('BLBU'), // privi BLBU (Linkage Beneficiary User)
													   "_createPB"     		=> $this->view->hasPrivilege('IPMO'), // privi IPMO (Create Bulk Debet Payment by Import File (CSV))
													   "_createDOM"    		=> false,        // cannot create DOM trx
													   "_createREM"    		=> false,        // cannot create REM trx
													  );

								$paramTrxArr = array();

								foreach ( $csvData as $row )
								{
									if(count($row)==4)
									{
										$rowNum++;
										$sourceAcct = trim($row[0]);
										$amount = trim($row[1]);
										$message = trim($row[2]);
										$addMessage = trim($row[3]);

										$filter = new Application_Filtering();

										$TRA_AMOUNT 		= $filter->filter($amount, "AMOUNT");
										$TRA_MESSAGE 		= $filter->filter($message, "TRA_MESSAGE");
										$TRA_REFNO 			= $filter->filter($addMessage, "TRA_REFNO");
										// $ACCTSRC 			= $filter->filter($sourceAcct, "ACCOUNT_NO");
										$ACCTSRC 			= $sourceAcct;
										$TRANSFER_TYPE 		= 'PB';

										$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

										$filter->__destruct();
										unset($filter);

										$paramTrx = array(	"TRANSFER_TYPE" 	=> $TRANSFER_TYPE,
															"TRA_AMOUNT" 		=> $TRA_AMOUNT_num,
															"TRA_MESSAGE" 		=> $TRA_MESSAGE,
															"TRA_REFNO" 		=> $TRA_REFNO,
															"ACCTSRC" 			=> $ACCTSRC,
															"ACBENEF" 			=> $ACBENEF,
															"ACBENEF_CCY" 		=> $ACBENEF_CCY,
															"ACBENEF_EMAIL" 	=> '',

														// for Beneficiary data, except (bene CCY and email), must be passed by reference
															"ACBENEF_BANKNAME" 			=> &$ACBENEF_BANKNAME,
															"ACBENEF_ALIAS" 			=> &$ACBENEF_ALIAS,
														//	"ACBENEF_CITIZENSHIP" 		=> $ACBENEF_CITIZENSHIP,		// 1/R: RESIDENT, 2/NR: NON-RESIDENT
														//	"ACBENEF_ADDRESS1" 			=> $ACBENEF_ADDRESS,
														//	"ACBENEF_ADDRESS2" 			=> &$ACBENEF_ADDRESS2,
														//	"ACBENEF_ADDRESS3" 			=> &$ACBENEF_ADDRESS3,

														//	"ORG_DIR" 					=> $ORG_DIR,
														//	"BANK_CODE" 				=> $CLR_CODE,
														//	"BANK_NAME" 				=> $BANK_NAME,
														//	"BANK_BRANCH" 				=> $BANK_BRANCH,
														//	"BANK_ADDRESS1" 			=> $BANK_ADDRESS1,
														//	"BANK_ADDRESS2" 			=> $BANK_ADDRESS2,
														//	"BANK_ADDRESS3" 			=> $BANK_ADDRESS3,
														 );

										array_push($paramTrxArr,$paramTrx);
									}
									else
									{
										$error_msg[0] = $this->language->_('Wrong File Format').'';
										break;
									}
								}
							}
							// kalo jumlah trx lebih dari setting
							else
							{
								$error_msg[0] = 'The number of rows to be imported should not more than '.$this->_maxRow.'.';
							}

							// kalo gak ada error
							if(!$error_msg[0])
							{
								$validate   = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin);
								$resultVal	= $validate->checkCreate($paramPayment, $paramTrxArr);

								$payment 		= $validate->getPaymentInfo();

								$i = 0;
								foreach($payment['acctsrcArr'] as $key=>$dataAcctType){
									//Zend_Debug::dump($dataAcctType);
									$paramTrxArr[$i++]['ACCOUNT_TYPE'] = $dataAcctType['accountType'];
								}

								if($validate->isError() === false)	// payment data is valid
								{
									$confirm = true;

									$validate->__destruct();
									unset($validate);
								}
								else
								{
									$errorMsg 		= $validate->getErrorMsg();
									$errorTrxMsg 	= $validate->getErrorTrxMsg();	// array

									$validate->__destruct();
									unset($validate);

									if($errorMsg)
									{
										$error_msg[0] = $errorMsg;
									}
									else
									{
										$confirm = true;
									}
								}
							}
						}
						else //kalo total record = 0
						{
							//$error_msg[0] = 'Wrong File Format. There is no data on csv File.';
							$error_msg[0] = $this->language->_('Wrong File Format').'.';
						}
					}
				}
				else
				{
					foreach($adapter->getMessages() as $key=>$val)
					{
						if($key=='fileUploadErrorNoFile')
							$error_msg[0] = $this->language->_('File cannot be left blank. Please correct it').'.';
						else
							$error_msg[0] = $val;
						break;
					}
				}

			}

			if($confirm)
			{
				$content['paramPayment'] = $paramPayment;
				$content['paramTrxArr'] = $paramTrxArr;
				$content['errorTrxMsg'] = $errorTrxMsg;
				$content['payment'] = $payment;

				$sessionNamespace = new Zend_Session_Namespace('confirmBulkDebet');
				$sessionNamespace->content = $content;
				$this->_redirect('/multidebet/bulk/confirm');
			}

			$this->view->error 		= true;
			//$error_msg[0] = $this->language->_('Error').': '.$error_msg[0];
			$error_msg[0] = $error_msg[0];
			$this->view->report_msg	= $this->displayError($error_msg);

			$this->view->PSSUBJECT = $PS_SUBJECT;
			$this->view->ACBENEF = $ACBENEF;
			$this->view->ACBENEF_BANKNAME = $ACBENEF_BANKNAME;
			$this->view->ACBENEF_ALIAS = $ACBENEF_ALIAS;
			$this->view->CURR_CODE = $ACBENEF_CCY;
			$this->view->PSEFDATE = $PS_EFDATE;
		}
		Application_Helper_General::writeLog('IPMO','Viewing Create Bulk Debit Payment by Import File (CSV)');
	}

	public function confirmAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$sessionNamespace = new Zend_Session_Namespace('confirmBulkDebet');
		$data = $sessionNamespace->content;
		//Zend_Debug::dump($data);die;

		if($this->_custSameUser){
			// echo 'here1';
			$this->view->token = true;
			$userOnBehalf = $this->_userIdLogin;
			$tokenIdUser = $this->_db->select()
			->from(
				array('M_USER'),
				array('TOKEN_ID')
			)
			->where('USER_ID = ?',$userOnBehalf)
			->where('CUST_ID = ?',$this->_custIdLogin)
			->limit(1)
		;

		$tokenIdUser = $this->_db->fetchRow($tokenIdUser);
		$tokenIdUser = $tokenIdUser['TOKEN_ID'];

			$Token 			= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
			$challengeCode 	= $Token->generateChallengeCode();
			$this->view->userOnBehalf		= $userOnBehalf;
			$this->view->challengeCode		= $challengeCode;
		}

		$this->view->PS_SUBJECT = $data['paramPayment']['PS_SUBJECT'];
		$this->view->PS_EFDATE = $data['paramPayment']['PS_EFDATE'];
		$this->view->ACBENEF = $data['paramTrxArr'][0]['ACBENEF'];
		$this->view->ACBENEF_CCY = $data['paramTrxArr'][0]['ACBENEF_CCY'];
		$this->view->ACBENEF_BANKNAME = $data['paramTrxArr'][0]['ACBENEF_BANKNAME'];
		$this->view->ACBENEF_ALIAS = $data['paramTrxArr'][0]['ACBENEF_ALIAS'];

		$totalSuccess = 0;
		foreach($data["payment"]["countTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$totalSuccess += $ccy['success'];
			}
		}

		$amountSuccess = 0;
		foreach($data["payment"]["sumTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$amountSuccess += $ccy['success'];
			}
		}

		$totalFailed = 0;
		foreach($data["payment"]["countTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$totalFailed += $ccy['failed'];
			}
		}

		$amountFailed = 0;
		foreach($data["payment"]["sumTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$amountFailed += $ccy['failed'];
			}
		}

		$this->view->totalSuccess = $totalSuccess;
		$this->view->amountSuccess = $amountSuccess;
		$this->view->totalFailed = $totalFailed;
		$this->view->amountFailed = $amountFailed;
		$this->view->totalRecord = $data["payment"]["countTrxTOTAL"];

		if($this->_request->isPost() )
		{
			if($this->_getParam('submit_cancel') == TRUE)
			{
				unset($_SESSION['confirmBulkDebet']);
				$this->_redirect('/newmultibulk');
			}

			$param['PS_SUBJECT'] = $data['paramPayment']['PS_SUBJECT'];
			$param['PS_EFDATE']  = Application_Helper_General::convertDate($data['paramPayment']['PS_EFDATE'], $this->_dateDBFormat, $this->_dateDisplayFormat);
			$param['PS_TYPE'] 	= $this->_paymenttype['code']['bulkdebet'];
			$param['PS_CCY']  = $data['paramTrxArr'][0]['ACBENEF_CCY'];

			$param['TRANSACTION_DATA'] = array();
			foreach($data['paramTrxArr'] as $row)
			{
				$param['TRANSACTION_DATA'][] = array(
						'SOURCE_ACCOUNT' 			=> $row['ACCTSRC'],
						'BENEFICIARY_ACCOUNT' 		=> $row['ACBENEF'],
						'BENEFICIARY_ACCOUNT_CCY' 	=> $row['ACBENEF_CCY'],
						'BENEFICIARY_ACCOUNT_NAME' 	=> $row['ACBENEF_BANKNAME'],
						'BENEFICIARY_ALIAS_NAME' 	=> $row['ACBENEF_ALIAS'],
						// 'BENEFICIARY_EMAIL' 		=> $row['ACBENEF_EMAIL'],
						// 'BENEFICIARY_ADDRESS' 		=> $row['ACBENEF_ADDRESS1'],
						// 'BENEFICIARY_CITIZENSHIP' 	=> $row['ACBENEF_CITIZENSHIP'],
						// 'CLR_CODE' 					=> $row['BANK_CODE'],
						'TRANSFER_TYPE' 			=> $row['TRANSFER_TYPE'],
						'TRA_AMOUNT' 				=> $row['TRA_AMOUNT'],
						'TRA_MESSAGE' 				=> $row['TRA_MESSAGE'],
						'TRA_REFNO' 				=> $row['TRA_REFNO'],
						'sourceAccountType' 		=> $row['ACCOUNT_TYPE'],
				);
			}

			$param['_addBeneficiary'] = $data['paramPayment']['_addBeneficiary'];
			$param['_beneLinkage'] = $data['paramPayment']['_beneLinkage'];
			$param['_priviCreate'] = 'IPMO';
			// Zend_Debug::dump($param);die;

			$BulkPayment = new BulkPayment("", $this->_custIdLogin, $this->_userIdLogin);


			if($this->_custSameUser){
				
				if(!$this->view->hasPrivilege('PRLP')){
					// die('here');
					$error_msg[] = $this->language->_("Error: You don't have privilege to release payment");
					$this->view->error = true;
					$this->view->report_msg = $this->displayError($error_msg);

					$checktoken = false;

				}else{
					// die('sini');

					$challengeCode		= $this->_getParam('challengeCode');

					$inputtoken1 		= $this->_getParam('inputtoken1');
					$inputtoken2 		= $this->_getParam('inputtoken2');
					$inputtoken3 		= $this->_getParam('inputtoken3');
					$inputtoken4 		= $this->_getParam('inputtoken4');
					$inputtoken5 		= $this->_getParam('inputtoken5');
					$inputtoken6 		= $this->_getParam('inputtoken6');

					$responseCode		= $inputtoken1.$inputtoken2.$inputtoken3.$inputtoken4.$inputtoken5.$inputtoken6;

					$Token 		= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
					$verToken 	= $Token->verify($challengeCode, $responseCode);
					// print_r($verToken);
					// die('here');
					if ($verToken['ResponseCode'] != '00'){
						$tokenFailed = $CustUser->setLogToken(); //log token activity

						$this->view->error = true;
						$errorMsg[] = $this->language->_('Invalid Response Code');	//$verToken['ResponseDesc'];

						if ($tokenFailed === true)
						{
							$this->_redirect('/default/index/logout');
						}

						$checktoken = false;

					}else{

						$checktoken = true;
					}
				}
			} else{

				$checktoken = true;
			}

			if($checktoken){
			$result = $BulkPayment->createPayment($param);







			if($result)
			{

				if($this->_custSameUser){

											$paramSQL = array("WA" 				=> false,
															  "ACCOUNT_LIST" 	=> $this->_accountList,
															  "_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
															 );

											// get payment query
											$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
											$select   = $CustUser->getPayment($paramSQL);
											$select->where('P.PS_NUMBER = ?' , (string) $result);
											// echo $select;
											$pslip = $this->_db->fetchRow($select);
											$settingObj = new Settings();
											$setting = array("COT_SKN"  			=> $settingObj->getSetting("cut_off_time_skn", 		"00:00:00"),
															 "COT_RTGS" 			=> $settingObj->getSetting("cut_off_time_rtgs", 	"00:00:00"),
															 "COT_BULK" 			=> $settingObj->getSetting("cut_off_time_bulkpayments", 	"00:00:00"),
															 "COT_BI" 				=> $settingObj->getSetting("cut_off_time_bi", 	"00:00:00"),
															 "COT_REMIT"			=> $settingObj->getSetting("cut_off_time_remittance", "00:00:00"),
															 'start_remit'			=> $settingObj->getSetting("starting_time_remittance", "00:00:00"),
															 "range_futuredate" 	=> $settingObj->getSetting("range_futuredate", 		"0"),
															 "auto_release" 		=> $settingObj->getSetting("auto_release_payment",	"0"),
															 "_dateFormat" 			=> $this->_dateDisplayFormat,
															 "_dateDBFormat" 		=> $this->_dateDBDefaultFormat,
															 "_transfertype" 		=> array_flip($this->_transfertype["code"]),
															);

											$isBackDated  = ($pslip['efdate'] < date('Y-m-d'));
											$isFutureDate = ($pslip['efdate'] > date('Y-m-d'));

											$caseTransferType 	= Application_Helper_General::caseArray($this->_transfertype);
											$caseTransferStatus = Application_Helper_General::caseArray($this->_transferstatus);

											$app = Zend_Registry::get('config');
											$appBankname = $app['app']['bankname'];

											$selectTrx = $this->_db->select()
											  ->from(	array(	'TT' => 'T_TRANSACTION'),
														array(
																'ACCTSRC'				=> 'TT.SOURCE_ACCOUNT',
																'ACCTSRC_CCY'			=> 'TT.SOURCE_ACCOUNT_CCY',
																'ACCTSRC_NAME'			=> new Zend_Db_Expr("
																							CASE WHEN TT.SOURCE_ACCOUNT_ALIAS_NAME is null THEN TT.SOURCE_ACCOUNT_NAME
																								 ELSE CONCAT(TT.SOURCE_ACCOUNT_NAME )
																							END"),
																//'ACCTSRC_ALIAS'		=> 'TT.SOURCE_ACCOUNT_ALIAS_NAME',
																'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
																'ACBENEF_NAME'			=> new Zend_Db_Expr("
																								CONCAT(TT.BENEFICIARY_ACCOUNT_NAME )"),
																'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
																'ACBENEF_EMAIL'			=> 'TT.BENEFICIARY_EMAIL',
																'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
																'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',
																'TRA_REFNO'				=> 'TT.TRA_REFNO',
																'TRANSFER_TYPE'			=> 'TT.TRANSFER_TYPE',
																'TRANSFER_TYPE_disp'	=> new Zend_Db_Expr("CASE TT.TRANSFER_TYPE $caseTransferType ELSE 'N/A' END"),
																'TRA_STATUS'			=> 'TT.TRA_STATUS',
																'TRA_STATUS_disp'		=> new Zend_Db_Expr("CASE TT.TRA_STATUS $caseTransferStatus ELSE 'N/A' END"),
																'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
																'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
																'TRA_REMAIN'			=> 'TT.TRA_REMAIN',
																'EQUIVALENT_AMOUNT_IDR'	=> 'TT.EQUIVALENT_AMOUNT_IDR',
																'CLR_CODE'				=> 'TT.CLR_CODE',
																'TT.RATE',
																'TT.PROVISION_FEE',
																'TT.NOSTRO_NAME',
																'TT.FULL_AMOUNT_FEE',
																'C.PS_CCY','C.CUST_ID',
																'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
																'EQUIVALEN_USD'			=> 'TT.EQUIVALENT_AMOUNT_USD',
																'EQUIVALEN_IDR'			=> 'TT.EQUIVALENT_AMOUNT_IDR',
																'BANK_NAME'				=> new Zend_Db_Expr("
																								CASE WHEN TT.TRANSFER_TYPE = '0' THEN '".$appBankname."'
																								WHEN TT.TRANSFER_TYPE = '9' THEN CONCAT('".$appBankname."',' - ' ,TT.BENEFICIARY_BANK_NAME)
																								 WHEN TT.TRANSFER_TYPE = '10' THEN CONCAT('".$appBankname."',' - ',TT.BENEFICIARY_BANK_NAME)
																									 ELSE TT.BENEFICIARY_BANK_NAME
																								END"),
																'BALANCE_TYPE'			=> new Zend_Db_Expr("(SELECT BALANCE_TYPE
					        																FROM T_PERIODIC_DETAIL Y
					        																inner join T_PSLIP Z
					        																on Y.PS_PERIODIC = Z.PS_PERIODIC
					        																where
					        																Z.PS_NUMBER = TT.PS_NUMBER limit 1)"),
															  )
														)
												->joinLeft(	array(	'C' => 'T_PSLIP' ),'C.PS_NUMBER = TT.PS_NUMBER',array())
												->where('TT.PS_NUMBER = ?', $result);
							// echo $selectTrx;
												$paramTrxArr = $this->_db->fetchAll($selectTrx);

												$validate  	  = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin, $result);
												$paramPayment = array_merge($pslip, $setting);
												// echo '<pre>';
												// print_r($paramPayment);
												// print_r($paramTrxArr);
												// die;
												$check 		  = $validate->checkRelease($paramPayment, $paramTrxArr);
												$infoWarnOri = (!empty($check['infoWarning'])?'*) '.$check['infoWarning']:'');
												$sessionNameConfrim->infoWarnOri = $infoWarnOri;

												if($validate->isError() === true)
												{
													$error = true;
													$errorMsg = array_merge($errorMsg, $check["errorMsg"]);
												}

												$Payment = new Payment($result, $this->_custIdLogin, $this->_userIdLogin);
												// if ($this->_hasPriviReleasePayment){
													$resultRelease = $Payment->releasePayment();
													// print_r($resultRelease);
													$this->view->ps_numb = $result;
													$this->view->hidetoken = true;
													if ($resultRelease['status'] == '00'){
														// $ns = new Zend_Session_Namespace('FVC');
										    			// $ns->backURL = $this->view->backURL;
										    			// $this->view->releaseresult = true;
														// $this->_redirect('/notification/success/index');
													}
													else
													{
														$this->view->releaseresult = false;
														$this->_helper->getHelper('FlashMessenger')->addMessage($result);
									//					$this->_helper->getHelper('FlashMessenger')->addMessage($resultRelease['status']);
									//					$this->_helper->getHelper('FlashMessenger')->addMessage($resultRelease['errorMessage']);
														$this->_redirect('/notification/index/release');
													}
												// }
										}

				
				unset($_SESSION['confirmBulkDebet']);
				$this->_redirect('/notification/success');
			}
			else
			{
				$this->view->error 		= true;
				$error_msg[0] = 'Error: Transaction failed';
				$this->view->report_msg	= $this->displayError($error_msg);
				$this->_redirect('/multidebet/bulk/confirm');
			}
			}else{
				$this->view->error 		= true;
				$error_msg[0] = 'Error: Invalid Token';
				$this->view->report_msg	= $this->displayError($error_msg);
				$this->_redirect('/multidebet/bulk/confirm');	
			}

		}
	}

	private function parseCSV($fileName){
		$csvData = false;
		try {
				$Csv = new Application_Csv (  $fileName, $separator = "," );
				$csvData = $Csv->readAll ();
			} catch ( Exception $e ) {
				echo nl2br ( $e->getTraceAsString () );
			}
			return $csvData;
	}
}
