<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'CMD/Payment.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'SGO/Extendedmodule/phpqrcode/qrlib.php';
require_once 'Crypt/AESMYSQL.php';

class debitbalance_InfoController extends Application_Main
{
	protected $_moduleDB = 'RTF';

	protected $_bankName;

	public function initController()
	{
		$conf = Zend_Registry::get('config');

		$this->_bankName = $conf['app']['bankname'];
		$this->_transferStatus 	= $conf["transfer"]["status"];
	}
	
	

	public function indexAction()
	{
		$this->_helper->_layout->setLayout('newlayout');
		$filter 			= new Application_Filtering();

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
	    $password = $sessionNamespace->token; 
	    $this->view->token = $sessionNamespace->token;

		$trfType 		= array_combine($this->_transfertype["code"], $this->_transfertype["desc"]);

		$AESMYSQL = new Crypt_AESMYSQL();
		$id 			= urldecode($filter->filter($this->_getParam('id'), "id"));
		$id = $AESMYSQL->decrypt($id, $password);
		
		
				

		$pdf 				= $filter->filter($this->_getParam('pdf'), "BUTTON");
		$cancelfuturedatebtn = $filter->filter($this->_getParam('cancelfuturedate'), "BUTTON");
		$backBtn			= $filter->filter($this->_getParam('back'), "BUTTON");
		
		
		
		$select = $this->_db->select()
					        ->from(array('A' => 'T_CUST_DEBIT'))
							->join(array('D' => 'M_CUSTOMER'), 'D.CUST_ID = A.CUST_ID',array('D.CUST_NAME'))
							->where('A.VA_NUMBER = ?',$id);
							//echo $select;
		$data  = $this->_db->fetchRow($select);
							//echo '<pre>';
			//var_dump($data);die;				
	
		
		$this->view->vamaster = $data['VA_NUMBER'];
		
		$this->view->data = $data;


		
	}
}
