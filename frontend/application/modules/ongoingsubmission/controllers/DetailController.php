<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'SGO/Extendedmodule/phpqrcode/qrlib.php';

class ongoingsubmission_DetailController extends Application_Main
{

    protected $_moduleDB = 'RTF'; // masih harus diganti

    public function indexAction()
    {
        // echo "<code>masuk = $data</code>"; die;	
        $this->_helper->_layout->setLayout('newlayout');

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->systemType = $system_type;
        $toc_ind = $settings->getSetting('ftemplate_bg_ind');
        $this->view->toc_ind = $toc_ind;
        $toc_eng = $settings->getSetting('ftemplate_bg_eng');
        $this->view->toc_eng = $toc_eng;
        // echo "<code>systemType = $system_type</code><br>";	
        // echo '<pre>';
        // print_r($system_type); 
        $conf = Zend_Registry::get('config');
        $this->_bankName = $conf['app']['bankname'];
        $this->view->masterbankname = $this->_bankName;


        $Settings = new Settings();
        $claim_period = $Settings->getSettingNew('max_claim_period');
        $this->view->BG_CLAIM_PERIOD = $claim_period;

        $this->view->currencyArr = array(
            // ''=>'-- '.$this->language->_('Any Value').' --',
            '1' => $this->language->_('IDR'),
            '2' => $this->language->_('USD'),
        );

        $selectcomp = $this->_db->select()
            ->from(array('A' => 'M_CUSTOMER'), array('*'))
            ->joinLeft(['CL' => 'M_CITYLIST'], 'A.CUST_CITY = CL.CITY_CODE', ["CITY_NAME" => "CL.CITY_NAME"])
            ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
            ->query()->fetchAll();

        $this->view->compinfo = $selectcomp[0];

        $numb = $this->_getParam('bgnumb');
        $sessToken  = new Zend_Session_Namespace('Tokenenc');
        $password   = $sessToken->token;

        $AESMYSQL = new Crypt_AESMYSQL();
        $decryption = urldecode($numb);
        $numb = $AESMYSQL->decrypt($decryption, $password);
        $this->view->bgnumb = $numb;



        if (!empty($numb)) {
            $bgdata = $this->_db->select()
                ->from(array('A' => 'TEMP_BANK_GUARANTEE'), array('*'))
                ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
                ->where('A.BG_REG_NUMBER = ?', $numb)
                ->query()->fetchAll();

            if (empty($bgdata)) {

                $bgdata = $this->_db->select()
                    ->from(array('A' => 'TEMP_BANK_GUARANTEE'), array('*'))
                    ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
                    ->where('A.BG_REG_NUMBER = ?', $numb)
                    ->query()->fetchAll();

                $bgdatadetail = $this->_db->select()
                    ->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL'), array('*'))
                    // ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
                    ->where('A.BG_REG_NUMBER = ?', $numb)
                    ->query()->fetchAll();

                $bgdatasplit = $this->_db->select()
                    ->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
                    //  ->where('A.CUST_ID ='.$this->_db->quote((string)$this->_custIdLogin))
                    ->where('A.BG_REG_NUMBER = ?', $numb)
                    ->query()->fetchAll();
            } else {

                $bgdatadetail = $this->_db->select()
                    ->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL'), array('*'))
                    // ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
                    ->where('A.BG_REG_NUMBER = ?', $numb)
                    ->query()->fetchAll();

                $bgdatasplit = $this->_db->select()
                    ->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL'), array('*'))
                    //  ->where('A.CUST_ID ='.$this->_db->quote((string)$this->_custIdLogin))
                    ->where('A.BG_REG_NUMBER = ?', $numb)
                    ->query()->fetchAll();
            }

            if ($bgdata[0]['BG_OLD']) {
                $bgOld = $this->_db->select()
                    ->from('T_BANK_GUARANTEE', ['BG_REG_NUMBER', 'PROVISION_FEE'])
                    ->where('BG_NUMBER = ?', $bgdata[0]['BG_OLD'])
                    ->query()->fetch();

                $this->view->bgRegNumberOld = $bgOld['BG_REG_NUMBER'];
                $this->view->prevProv = $bgOld['PROVISION_FEE'];
            }



            if (!empty($bgdata[0]['BG_OLD'])) {
                $tbgdata = $this->_db->select()
                    ->from(["A" => "T_BANK_GUARANTEE"], ["*"])
                    ->joinLeft(array('B' => 'M_CUSTOMER'), 'A.BG_INSURANCE_CODE = B.CUST_ID', array('B.CUST_NAME'))
                    ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
                    ->where('A.BG_NUMBER = ?', $bgdata[0]['BG_OLD'])
                    ->query()->fetch();

                $this->view->tbgdata = $tbgdata;

                // Marginal Deposit Eksisting ---------------
                $bgdatamdeks = $this->_db->select()
                    ->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
                    // ->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
                    // ->join(array('B' => 'T_BANK_GUARANTEE_SPLIT'), 'A.BG_REG_NUMBER = B.BG_REG_NUMBER')
                    ->where('A.BG_REG_NUMBER = ?', $tbgdata['BG_REG_NUMBER'] ? $tbgdata['BG_REG_NUMBER'] : $numb)
                    ->query()->fetchAll();

                $this->view->bgdatamdeks = $bgdatamdeks;
                // Marginal Deposit Eksisting ---------------

                // Top Up Marginal Deposit ---------------
                $bgdatamd = $this->_db->select()
                    ->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
                    // ->from(array('A' => 'TEMP_BANK_GUARANTEE'), array('*'))
                    // ->join(array('B' => 'TEMP_BANK_GUARANTEE_SPLIT'), 'A.BG_REG_NUMBER = B.BG_REG_NUMBER')
                    ->where('A.BG_REG_NUMBER = ?', $numb)
                    ->query()->fetchAll();

                $this->view->bgdatamd = $bgdatamd;
            }

            $checkOthersAttachment = $this->_db->select()
                ->from(["A" => "TEMP_BANK_GUARANTEE_FILE"], ["*"])
                ->where("BG_REG_NUMBER = '$numb'")
                ->order('A.INDEX ASC')
                ->query()->fetchAll();

            if (count($checkOthersAttachment) > 0) {
                $this->view->othersAttachment = $checkOthersAttachment;
            }

            if (!empty($tbgdata['BG_REG_NUMBER'])) {
                $checkOthersAttachmentT = $this->_db->select()
                    ->from(["A" => "T_BANK_GUARANTEE_FILE"], ["*"])
                    ->where("BG_REG_NUMBER = ?", $tbgdata['BG_REG_NUMBER'])
                    ->order('A.INDEX ASC')
                    ->query()->fetchAll();

                $this->view->othersAttachmentT = $checkOthersAttachmentT ?? [];
            }

            $kontra = $bgdata[0]['COUNTER_WARRANTY_TYPE'];
            $timePeriodStart = $bgdata[0]['TIME_PERIOD_START'];
            $timePeriodEnd = $bgdata[0]['TIME_PERIOD_END'];
            $bgAmount = $bgdata[0]['BG_AMOUNT'];
            $custId = $bgdata[0]['CUST_ID'];
            $usagePurpose = $bgdata[0]['USAGE_PURPOSE'];
            $usagePurposeDesc = $bgdata[0]['USAGE_PURPOSE_DESC'];
            $insuranceCode = $bgdata[0]['BG_INSURANCE_CODE'];

            // Guaranted Transaction -----------------------

            $getGuarantedTransanctions = $this->_db->select()
                ->from('TEMP_BANK_GUARANTEE_UNDERLYING')
                ->where('BG_REG_NUMBER = ?', $numb)
                ->query()->fetchAll();

            $this->view->guarantedTransanctions = $getGuarantedTransanctions;

            if (!empty($tbgdata['BG_REG_NUMBER'])) {
                $getGuarantedTransanctionsT = $this->_db->select()
                    ->from('T_BANK_GUARANTEE_UNDERLYING')
                    ->where('BG_REG_NUMBER = ?', $tbgdata['BG_REG_NUMBER'] ? $tbgdata['BG_REG_NUMBER'] : '')
                    ->query()->fetchAll();

                $this->view->guarantedTransanctionsT = $getGuarantedTransanctionsT;

                $bgdatadetailT = $this->_db->select()
                    ->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
                    ->where('A.BG_REG_NUMBER = ?', $tbgdata['BG_REG_NUMBER'])
                    ->query()->fetchAll();

                $this->view->bgdatadetailT = array_combine(array_map('strtolower', array_column($bgdatadetailT, 'PS_FIELDNAME')), array_column($bgdatadetailT, 'PS_FIELDVALUE'));

                $bgdatadetail = $this->_db->select()
                    ->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL'), array('*'))
                    ->where('A.BG_REG_NUMBER = ?', $numb)
                    ->query()->fetchAll();

                $this->view->bgdatadetail = array_combine(array_map('strtolower', array_column($bgdatadetail, 'PS_FIELDNAME')), array_column($bgdatadetail, 'PS_FIELDVALUE'));
            }

            $getMinCharge = $this->_db->select()
                ->from("M_CHARGES_BG", ["CHARGES_AMT"])
                ->where("CHARGES_ID = ?", "MINPROV01")
                ->query()->fetch();

            // penkondisian kontra biaya provisi
            if ($kontra == '3') {

                $get_customer_ins = $this->_db->select()
                        ->from("M_CUSTOMER")
                        ->where("CUST_ID = ?", $bgdata[0]['BG_INSURANCE_CODE'])
                        ->query()->fetch();

                $get_percentage = $this->_db->select()
                    ->from("M_CUST_LINEFACILITY_DETAIL")
                    ->where("CUST_ID = ?", $get_customer_ins["CUST_ID"])
                    ->where("OFFER_TYPE = ?", $bgdata[0]["USAGE_PURPOSE_DESC"])
                    ->where("GRUP_BUMN = ?", $get_customer_ins["GRUP_BUMN"])
                    ->query()->fetch();

                $percentage = round(floatval($get_percentage["FEE_PROVISION"]), 2);

                $paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $bgdata[0]['COUNTER_WARRANTY_TYPE'];
                $paramProvisionFee['CUST_ID'] = $bgdata[0]["BG_INSURANCE_CODE"];
                $paramProvisionFee['GRUP'] = $bgdata[0]["CUST_ID"];
                $paramProvisionFee['USAGE_PURPOSE'] = $bgdata[0]['USAGE_PURPOSE'];
                $paramProvisionFee['USAGE_PURPOSE_DESC'] = $bgdata[0]['USAGE_PURPOSE_DESC'];
                $getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);
                $percentage = round(floatval($getProvisionFee['provisionFee']), 2);

                $date_start = date_create($bgdata[0]["TIME_PERIOD_START"]);
                $date_end = date_create($bgdata[0]["TIME_PERIOD_END"]);
                $tenor = intval(date_diff($date_start, $date_end)->format('%a')) + 1;

                $prorata = $tenor / 360;

                if ($bgdata[0]["REDEBATE_STATUS"] == "1") {
                    $cekrebate = $this->_db->select()
                        ->from("TEMP_REQUEST_REBATE")
                        ->where("BG_REG_NUMBER = ?", $bgdata[0]['BG_REG_NUMBER'])
                        ->query()->fetchAll();
                    $this->view->REBATE_APPROVEDBY = $cekrebate[0]["REBATE_APPROVEDBY"];

                    $this->view->REBATE_APPROVED = Application_Helper_General::convertDate($cekrebate[0]["REBATE_APPROVED"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);

                    $percentage = floatval($bgdata[0]['REDEBATE_AMOUNT']);
                }

                // end rebate ------------------------------------------------------------

                if (in_array($bgdata[0]['CHANGE_TYPE'], [1, 2])) {
                    $provisi = Application_Helper_General::countProvision($bgdata[0]['COUNTER_WARRANTY_TYPE'], ['TIME_PERIOD_START' => $bgdata[0]['TIME_PERIOD_START'], 'TIME_PERIOD_END' => $bgdata[0]['TIME_PERIOD_END'], 'BG_AMOUNT' => $bgdata[0]['BG_AMOUNT']], $percentage);

                    $previousProvision = $bgOld['PROVISION_FEE'];

                    $getMinGlobalProvisi = $this->_db->select()
                        ->from('M_CHARGES_BG')
                        ->where('CHARGES_ID = ?', 'MINPROV01')
                        ->query()->fetch();

                    $minGlobalProvisi = $getMinGlobalProvisi['CHARGES_AMT'];

                    $selisihProvisi = $provisi - $previousProvision;

                    if ($provisi <= $previousProvision) {
                        $biaya_provisi = 0;
                    } elseif ($selisihProvisi < $minGlobalProvisi) {
                        $biaya_provisi = $minGlobalProvisi * 100;
                    } else {
                        $biaya_provisi = $selisihProvisi * 100;
                    }
                } else {
                    $biaya_provisi = round($prorata * ($percentage / 100) * $bgdata[0]["BG_AMOUNT"], 2) * 100;

                    if (($biaya_provisi / 100) < intval($getMinCharge["CHARGES_AMT"])) {
                        $biaya_provisi = intval($getMinCharge["CHARGES_AMT"]) * 100;
                    }
                }

                $biaya_administrasi = round($bgdata[0]["ADM_FEE"], 2) * 100;
                $biaya_materai = round($bgdata[0]["STAMP_FEE"], 2) * 100;


                $this->view->percentage = $percentage;
                $this->view->biaya_provisi = $biaya_provisi;
                $this->view->biaya_administrasi = $biaya_administrasi;
                $this->view->biaya_materai = $biaya_materai;

                // $paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $kontra;
                // $paramProvisionFee['CUST_ID'] = $insuranceCode;
                // $paramProvisionFee['GRUP'] = $custId;
                // $paramProvisionFee['USAGE_PURPOSE'] = $usagePurpose;
                // $paramProvisionFee['USAGE_PURPOSE_DESC'] = $usagePurposeDesc;
                // $getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);

                // $provisionPercentage = $getProvisionFee['provisionFee'];

                // $getLfIns = $this->_db->select()
                //     ->from(["MCLF" => "M_CUST_LINEFACILITY"], ["FEE_DEBITED"])
                //     ->where("CUST_ID = ?", $insuranceCode)
                //     ->query()->fetch();

                // $this->view->feeDebited = $getLfIns['FEE_DEBITED'];
            }

            if ($kontra == '1') {

                $paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $bgdata[0]['COUNTER_WARRANTY_TYPE'];
                $paramProvisionFee['TIME_PERIOD_START'] = $bgdata[0]['TIME_PERIOD_START'];
                $paramProvisionFee['TIME_PERIOD_END'] = $bgdata[0]['TIME_PERIOD_END'];
                $getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);
                $percentage = $getProvisionFee['provisionFee'];
                $bgdata[0]["ADM_FEE"] = $getProvisionFee['adminFee'];
                $bgdata[0]["STAMP_FEE"] = $getProvisionFee['stampFee'];

                if ($bgdata[0]["REDEBATE_STATUS"] == "1") {
                    $cekrebate = $this->_db->select()
                        ->from("TEMP_REQUEST_REBATE")
                        ->where("BG_REG_NUMBER = ?", $bgdata[0]['BG_REG_NUMBER'])
                        ->query()->fetchAll();
                    $this->view->REBATE_APPROVEDBY = $cekrebate[0]["REBATE_APPROVEDBY"];

                    $this->view->REBATE_APPROVED = Application_Helper_General::convertDate($cekrebate[0]["REBATE_APPROVED"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);

                    $percentage = floatval($bgdata[0]['REDEBATE_AMOUNT']);
                }

                // end rebate --------------------------------------------------------


                if (in_array($bgdata[0]['CHANGE_TYPE'], [1, 2])) {

                    $date_start = date_create($bgdata[0]["TIME_PERIOD_START"]);
                    $date_end = date_create($bgdata[0]["TIME_PERIOD_END"]);
                    $tenor = intval(date_diff($date_start, $date_end)->format('%a')) + 1;

                    if ($tenor <= 90) {
                        // $percentage = 0.25;
                        $prorata = $tenor / 90;
                    } else if ($tenor <= 180) {
                        // $percentage = 0.50;
                        $prorata = $tenor / 180;
                    } else if ($tenor <= 270) {
                        // $percentage = 0.75;
                        $prorata = $tenor / 270;
                    } else {
                        // $percentage = 0.75;
                        $prorata = $tenor / 360;
                    }

                    $provisi = Application_Helper_General::countProvision($bgdata[0]['COUNTER_WARRANTY_TYPE'], ['TIME_PERIOD_START' => $bgdata[0]['TIME_PERIOD_START'], 'TIME_PERIOD_END' => $bgdata[0]['TIME_PERIOD_END'], 'BG_AMOUNT' => $bgdata[0]['BG_AMOUNT']], $percentage);

                    $previousProvision = $bgOld['PROVISION_FEE'];

                    $getMinGlobalProvisi = $this->_db->select()
                        ->from('M_CHARGES_BG')
                        ->where('CHARGES_ID = ?', 'MINPROV01')
                        ->query()->fetch();

                    $minGlobalProvisi = $getMinGlobalProvisi['CHARGES_AMT'];

                    $selisihProvisi = $provisi - $previousProvision;

                    if ($provisi <= $previousProvision) {
                        $biaya_provisi = 0;
                    } elseif ($selisihProvisi < $minGlobalProvisi) {
                        $biaya_provisi = $minGlobalProvisi * 100;
                    } else {
                        $biaya_provisi = $selisihProvisi * 100;
                    }
                } else {
                    $biaya_provisi = round($prorata * ($percentage / 100) * $bgdata[0]["BG_AMOUNT"], 2) * 100;

                    if (($biaya_provisi / 100) < intval($getMinCharge["CHARGES_AMT"])) {
                        $biaya_provisi = intval($getMinCharge["CHARGES_AMT"]) * 100;
                    }
                }

                $biaya_administrasi = round($bgdata[0]["ADM_FEE"], 2) * 100;
                $biaya_materai = round($bgdata[0]["STAMP_FEE"], 2) * 100;

                $this->view->percentage = $percentage;
                $this->view->biaya_provisi = $biaya_provisi;
                $this->view->biaya_administrasi = $biaya_administrasi;
                $this->view->biaya_materai = $biaya_materai;

                // $paramProvisionFee['TIME_PERIOD_START'] = $timePeriodStart;
                // $paramProvisionFee['TIME_PERIOD_END'] = $timePeriodEnd;
                // $paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $kontra;
                // $paramProvisionFee['CUST_ID'] = $custId;
                // $paramProvisionFee['GRUP'] = $custId;
                // $paramProvisionFee['USAGE_PURPOSE'] = $usagePurpose;
                // $paramProvisionFee['USAGE_PURPOSE_DESC'] = $usagePurposeDesc;
                // $getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);

                // $provisionPercentage = $getProvisionFee['provisionFee'];
            }

            if ($kontra == '2') {

                $getCust = $this->_db->select()
                    ->from('M_CUSTOMER')
                    ->where('CUST_ID = ?', $bgdata[0]['CUST_ID'])
                    ->query()->fetch();

                $get_percentage = $this->_db->select()
                    ->from("M_CUST_LINEFACILITY_DETAIL")
                    ->where("CUST_ID = ?", $bgdata[0]["CUST_ID"])
                    ->where("OFFER_TYPE = ?", $bgdata[0]["USAGE_PURPOSE_DESC"])
                    ->where("GRUP_BUMN = ?", $getCust['GRUP_BUMN'])
                    ->query()->fetch();

                $percentage = round(floatval($get_percentage["FEE_PROVISION"]), 2);

                $date_start = date_create($bgdata[0]["TIME_PERIOD_START"]);
                $date_end = date_create($bgdata[0]["TIME_PERIOD_END"]);
                $tenor = intval(date_diff($date_start, $date_end)->format('%a')) + 1;

                $paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $bgdata[0]['COUNTER_WARRANTY_TYPE'];
                $paramProvisionFee['CUST_ID'] = $bgdata[0]['CUST_ID'];
                $paramProvisionFee['USAGE_PURPOSE'] = $bgdata[0]['USAGE_PURPOSE'];
                $paramProvisionFee['USAGE_PURPOSE_DESC'] = $bgdata[0]['USAGE_PURPOSE_DESC'];
                $getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);
                $this->view->provisionFee = $getProvisionFee['provisionFee'];
                $bgdata[0]["ADM_FEE"] = $getProvisionFee['adminFee'];
                $bgdata[0]["STAMP_FEE"] = $getProvisionFee['stampFee'];

                $prorata = $tenor / 360;

                if ($bgdata[0]["REDEBATE_STATUS"] == "1") {
                    $cekrebate = $this->_db->select()
                        ->from("TEMP_REQUEST_REBATE")
                        ->where("BG_REG_NUMBER = ?", $bgdata[0]['BG_REG_NUMBER'])
                        ->query()->fetchAll();
                    $this->view->REBATE_APPROVEDBY = $cekrebate[0]["REBATE_APPROVEDBY"];

                    $this->view->REBATE_APPROVED = Application_Helper_General::convertDate($cekrebate[0]["REBATE_APPROVED"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);

                    $percentage = floatval($bgdata[0]['REDEBATE_AMOUNT']);
                }
                // end rebate -------------------------------------------------------

                if (in_array($bgdata[0]['CHANGE_TYPE'], [1, 2])) {
                    $provisi = Application_Helper_General::countProvision($bgdata[0]['COUNTER_WARRANTY_TYPE'], ['TIME_PERIOD_START' => $bgdata[0]['TIME_PERIOD_START'], 'TIME_PERIOD_END' => $bgdata[0]['TIME_PERIOD_END'], 'BG_AMOUNT' => $bgdata[0]['BG_AMOUNT']], $percentage);

                    $previousProvision = $bgOld['PROVISION_FEE'];

                    $getMinGlobalProvisi = $this->_db->select()
                        ->from('M_CHARGES_BG')
                        ->where('CHARGES_ID = ?', 'MINPROV01')
                        ->query()->fetch();

                    $minGlobalProvisi = $getMinGlobalProvisi['CHARGES_AMT'];

                    $selisihProvisi = $provisi - $previousProvision;

                    if ($provisi <= $previousProvision) {
                        $biaya_provisi = 0;
                    } elseif ($selisihProvisi < $minGlobalProvisi) {
                        $biaya_provisi = $minGlobalProvisi * 100;
                    } else {
                        $biaya_provisi = $selisihProvisi * 100;
                    }
                } else {
                    $biaya_provisi = round($prorata * ($percentage / 100) * $bgdata[0]["BG_AMOUNT"], 2) * 100;

                    if (($biaya_provisi / 100) < intval($getMinCharge["CHARGES_AMT"])) {
                        $biaya_provisi = intval($getMinCharge["CHARGES_AMT"]) * 100;
                    }
                }

                $biaya_administrasi = round($bgdata[0]["ADM_FEE"], 2) * 100;
                $biaya_materai = round($bgdata[0]["STAMP_FEE"], 2) * 100;

                $this->view->percentage = $percentage;
                $this->view->biaya_provisi = $biaya_provisi;
                $this->view->biaya_administrasi = $biaya_administrasi;
                $this->view->biaya_materai = $biaya_materai;

                // $paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $kontra;
                // $paramProvisionFee['CUST_ID'] = $custId;
                // $paramProvisionFee['GRUP'] = $custId;
                // $paramProvisionFee['USAGE_PURPOSE'] = $usagePurpose;
                // $paramProvisionFee['USAGE_PURPOSE_DESC'] = $usagePurposeDesc;
                // $getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);

                // $provisionPercentage = $getProvisionFee['provisionFee'];
            }
            // end pengkondisian kontra

            // $provisi = Application_Helper_General::countProvision($kontra, ['TIME_PERIOD_START' => $timePeriodStart, 'TIME_PERIOD_END' => $timePeriodEnd, 'BG_AMOUNT' => $bgAmount], $provisionPercentage);

            // $this->view->allToProvision = [$provisionPercentage, $provisi, $getProvisionFee];

            $CustomerUser = new CustomerUser($bgdata[0]['CUST_ID'], $this->_userIdLogin);

            $param = array('CCY_IN' => 'IDR', 'ACCT_NO' => $bgdata[0]['FEE_CHARGE_TO']);
            $AccArr = $CustomerUser->getAccountsBG($param);

            if (!empty($AccArr)) {
                $this->view->src_name = $AccArr['0']['ACCT_NAME'];
                $this->view->currency_type = $AccArr['0']['CCY_ID'];
            }

            switch ($bgdata[0]["CHANGE_TYPE"]) {
                case '0':
                    $this->view->suggestion_type = "Pengajuan Baru";
                    break;
                case '1':
                    $this->view->suggestion_type = "Amendemen Isi";
                    break;
                case '2':
                    $this->view->suggestion_type = "Amendemen Format";
                    break;
            }

            $this->view->bgdatasplit = $bgdatasplit;
            $this->view->data = $bgdata[0];
            // var_dump($datas);
            // echo '<pre>';
            // print_r($bgdatasplit);



            if (!empty($bgdata)) {

                $data = $bgdata['0'];
                if ($data['COUNTER_WARRANTY_TYPE'] == '3') {
                    $this->view->isinsurance = true;
                }
                //cek privilage
                $custidlike = '%' . $this->_custIdLogin . '%';

                $principleData = [];
                // if ($data["COUNTER_WARRANTY_TYPE"] == "3") {
                foreach ($bgdatadetail as $key => $value) {
                    $principleData[$value["PS_FIELDNAME"]] = $value["PS_FIELDVALUE"];
                }

                $this->view->principleData = $principleData;

                //select user with releaser privilage
                $selectReleaser = $this->_db->select()
                    ->from(array('P' => 'M_FPRIVI_USER'))
                    ->from(array('U' => 'M_USER'))
                    ->where("P.FPRIVI_ID = 'PRLP' AND P.FUSER_ID LIKE ?", (string) $custidlike)
                    ->where("INSTR( P.FUSER_ID, U.USER_ID) > 0")
                    ->where("U.CUST_ID = ?", (string) $this->_custIdLogin);

                // echo "<code>selectReleaser = $selectReleaser</code><BR>";  
                $userReleaser = $this->_db->fetchAll($selectReleaser);

                $releaserList = array();
                foreach ($userReleaser as $row) {
                    $userIdReleaser = explode($this->_custIdLogin, $row['FUSER_ID']);

                    //get user name
                    $selectReleaserName = $this->_db->select()
                        ->from('M_USER')
                        ->where("USER_ID = ?", (string) $userIdReleaser[1]);

                    $userReleaserName = $this->_db->fetchAll($selectReleaserName);

                    array_push($releaserList, $userReleaserName[0]['USER_FULLNAME']);
                }


                //cek ada privilage reviewer atau approver
                $selectpriv = $this->_db->select()
                    ->from(array('M_CUSTOMER'))
                    ->where("CUST_ID = ?", $this->_custIdLogin);
                $userpriv = $this->_db->fetchrow($selectpriv);

                if ($userpriv['CUST_REVIEW'] != 1) {
                    $cust_reviewer = 0;
                } else {
                    $cust_reviewer = 1;
                }

                if ($userpriv['CUST_APPROVER'] != 1) {
                    $cust_approver = 0;
                } else {
                    $cust_approver = 1;
                }


                $selectHistory    = $this->_db->select()
                    ->from('T_BANK_GUARANTEE_HISTORY')
                    ->where("BG_REG_NUMBER = ?", $numb)
                    ->order("DATE_TIME DESC");

                $history = $this->_db->fetchAll($selectHistory);

                $get_notes = $history[0]["BG_REASON"];
                $this->view->notes = $get_notes;

                $cust_approver = 1;

                $bg_submission_hisotrys = $this->_db->select()
                    ->from('T_BANK_GUARANTEE_HISTORY')
                    ->where("BG_REG_NUMBER = ?", $numb)
                    ->where("CUST_ID = ? ", $selectcomp[0]["CUST_ID"]);
                $bg_submission_hisotrys = $this->_db->fetchAll($bg_submission_hisotrys);

                foreach ($bg_submission_hisotrys as $bg_submission_hisotry) {

                    // maker
                    if ($bg_submission_hisotry['HISTORY_STATUS'] == 1 || $bg_submission_hisotry['HISTORY_STATUS'] == 33) {
                        $makerStatus = 'active';
                        $makerIcon = '<i class="fas fa-check"></i>';

                        $custlogin = $bg_submission_hisotry['USER_LOGIN'];

                        $selectCust = $this->_db->select()
                            ->from('M_USER')
                            ->where("USER_ID = ?", $custlogin)
                            ->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

                        $customer = $this->_db->fetchAll($selectCust);

                        // $custFullname = $customer[0]['USER_FULLNAME'];
                        $custFullname = $customer[0]['USER_FULLNAME'];

                        $efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));

                        $align = 'align="center"';

                        $this->view->makerBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
                    }

                    // approver
                    if ($bg_submission_hisotry['HISTORY_STATUS'] == 2) {
                        $approverStatus = 'active';
                        $approverIcon = '<i class="fas fa-check"></i>';

                        $custlogin = $bg_submission_hisotry['USER_LOGIN'];

                        $selectCust = $this->_db->select()
                            ->from('M_USER')
                            ->where("USER_ID = ?", $custlogin)
                            ->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

                        $customer = $this->_db->fetchAll($selectCust);

                        // $custFullname = $customer[0]['USER_FULLNAME'];
                        $custFullname = $customer[0]['USER_FULLNAME'];

                        $efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));

                        $align = 'align="center"';

                        $this->view->approverBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';

                        $this->view->approverStatus = $approverStatus;
                    }

                    if ($data["COUNTER_WARRANTY_TYPE"] == '3') {

                        $getInsuranceBranch = array_search("Insurance Branch", array_column($bgdatadetail, "PS_FIELDNAME"));
                        $getInsuranceBranch = $bgdatadetail[$getInsuranceBranch];

                        $insuranceBranch = $this->_db->select()
                            ->from("M_INS_BRANCH")
                            ->where("INS_BRANCH_CODE = ?", $getInsuranceBranch["PS_FIELDVALUE"])
                            ->query()->fetchAll();

                        $this->view->insuranceBranch = $insuranceBranch[0]["INS_BRANCH_NAME"];
                        $this->view->insuranceAcct = $insuranceBranch[0]["INS_BRANCH_ACCT"];

                        //if releaser
                        // if ($bg_submission_hisotry['HISTORY_STATUS'] == 8 || $bg_submission_hisotry['HISTORY_STATUS'] == 10) {
                        if ($bg_submission_hisotry['HISTORY_STATUS'] == 5) {
                            $releaserStatus = 'active';
                            $releaserIcon = '<i class="fas fa-check"></i>';

                            $custlogin = $bg_submission_hisotry['USER_LOGIN'];

                            $selectCust = $this->_db->select()
                                ->from('M_USER')
                                ->where("USER_ID = ?", $custlogin)
                                ->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

                            $customer = $this->_db->fetchAll($selectCust);

                            // $custFullname = $customer[0]['USER_FULLNAME'];
                            $custFullname = $customer[0]['USER_FULLNAME'];

                            $efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));

                            $align = 'align="center"';

                            $this->view->releaserBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
                            $this->view->releaserStatus = $releaserStatus;
                        }
                    } else {
                        //if releaser
                        if ($bg_submission_hisotry['HISTORY_STATUS'] == 5) {
                            $releaserStatus = 'active';
                            $releaserIcon = '<i class="fas fa-check"></i>';

                            $custlogin = $bg_submission_hisotry['USER_LOGIN'];

                            $selectCust = $this->_db->select()
                                ->from('M_USER')
                                ->where("USER_ID = ?", $custlogin)
                                ->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

                            $customer = $this->_db->fetchAll($selectCust);

                            // $custFullname = $customer[0]['USER_FULLNAME'];
                            $custFullname = $customer[0]['USER_FULLNAME'];

                            $efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));

                            $align = 'align="center"';

                            $this->view->releaserBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
                            $this->view->releaserStatus = $releaserStatus;
                        }
                    }


                    // if ($bg_submission_hisotry['HISTORY_STATUS'] == 4 || $bg_submission_hisotry['HISTORY_STATUS'] == 14) {
                    // 	$releaserStatus = 'active';
                    // 	$releaserIcon = '<i class="fas fa-check"></i>';

                    // 	$custlogin = $bg_submission_hisotry['USER_LOGIN'];

                    // 	$selectCust	= $this->_db->select()
                    // 		->from('M_USER')
                    // 		->where("USER_ID = ?", $custlogin)
                    // 		->where("CUST_ID = ?", $bg_submission_hisotry['CUST_ID']);

                    // 	$customer = $this->_db->fetchAll($selectCust);

                    // 	$custFullname = $customer[0]['USER_FULLNAME'];

                    // 	$efdate = date('d-M-Y', strtotime($bg_submission_hisotry['DATE_TIME']));

                    // 	$align = 'align="center"';

                    // 	$this->view->releaserBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span></span></div>';
                    // 	$this->view->releaserStatus = $releaserStatus;
                    // }
                }

                foreach ($history as $row) {

                    if ($data["COUNTER_WARRANTY_TYPE"] == '3') {
                        if ($row['HISTORY_STATUS'] == 8 || $row['HISTORY_STATUS'] == 24) {
                            $makerStatus = 'active';
                            $insuranceIcon = '<i class="fas fa-check"></i>';
                            /*$approveStatus = 'active';
							$reviewStatus = 'active';
							$releaseStatus = 'active';
							$releaseIcon = '<i class="fas fa-check"></i>';*/
                            $insuranceStatus = 'active';
                            $reviewStatus = '';
                            //$releaseStatus = '';
                            //$releaseIcon = '';

                            $makerOngoing = '';
                            $reviewerOngoing = '';
                            $approverOngoing = '';
                            //$releaserOngoing = '';

                            $custlogin = $row['USER_LOGIN'];

                            $selectCust    = $this->_db->select()
                                ->from('M_USER')
                                ->where("USER_ID = ?", $custlogin);
                            //->where("CUST_ID = ?", $row['CUST_ID']);

                            $customer = $this->_db->fetchAll($selectCust);

                            $custFullname = $customer[0]['USER_FULLNAME'];
                            // $custFullname = $customer[0]['USER_FULLNAME'];
                            // $custEmail 	  = $customer[0]['USER_EMAIL'];
                            // $custPhone	  = $customer[0]['USER_PHONE'];

                            $insuranceApprovedBy = $custFullname;

                            $align = 'align="center"';
                            $marginLeft = '';
                            if ($cust_reviewer == 0 && $cust_approver == 0) {
                                $align = '';
                                $marginLeft = 'style="margin-left: 15px;"';
                            }

                            $efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
                            $this->view->insuranceApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>ASURANSI</span></div>';
                        }
                    }

                    if ($row['HISTORY_STATUS'] == 6) {
                        $verifyStatus = 'active';
                        $verifyIcon = '<i class="fas fa-check"></i>';

                        $verifyOngoing = '';
                        if ($cust_reviewer == 0 && $cust_approver == 0) { //kalau tidak ada priv reviewer & approver
                            $reviewerOngoing = '';
                            $verifyOngoing = '';
                            $approverOngoing = '';
                            //$releaserOngoing = 'ongoing';
                            $releaserOngoing = '';
                        } else {
                            //$reviewerOngoing = 'ongoing';
                            $reviewerOngoing = '';
                            $verifyOngoing = '';
                            $approverOngoing = '';
                            $releaserOngoing = '';
                        }

                        $custlogin = $row['USER_LOGIN'];

                        $selectCust = $this->_db->select()
                            ->from('M_BUSER')
                            ->where("BUSER_ID = ?", $custlogin);

                        $customer = $this->_db->fetchAll($selectCust);

                        $custFullname = $customer[0]['BUSER_NAME'];

                        $efdate = date('d-M-Y', strtotime($row['DATE_TIME']));

                        $align = 'align="center"';
                        $marginRight = '';

                        if ($cust_reviewer == 0 && $cust_approver == 0) {
                            $align = '';
                            $marginRight = 'style="margin-right: 15px;"';
                        }

                        $this->view->verifyApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span ' . $marginRight . '>BANK</span></div>';
                    }

                    //if reviewer done
                    if ($row['HISTORY_STATUS'] == 7) {
                        $bankReviewStatus = "active";
                        $bankReviewOngoing = "ongoing";
                        $bankReviewIcon = '<i class="fas fa-check"></i>';

                        $custlogin = $row['USER_LOGIN'];

                        $selectCust = $this->_db->select()
                            ->from('M_BUSER')
                            ->where("BUSER_ID = ?", $custlogin);

                        $customer = $this->_db->fetchAll($selectCust);

                        $custFullname = $customer[0]['BUSER_NAME'];

                        $align = 'align="center"';

                        $efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
                        $this->view->bankReviewedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>BANK</span></div>';
                    }

                    //if approver done
                    if ($row['HISTORY_STATUS'] == 14) {
                        $bankApproveStatus = "active";
                        $bankReviewOngoing = "ongoing";
                        $bankApproveIcon = '<i class="fas fa-check"></i>';

                        $custlogin = $row['USER_LOGIN'];

                        $selectCust = $this->_db->select()
                            ->from('M_BUSER')
                            ->where("BUSER_ID = ?", $custlogin);

                        $customer = $this->_db->fetchAll($selectCust);

                        $custFullname = $customer[0]['BUSER_NAME'];

                        $align = 'align="center"';

                        $efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
                        $this->view->bankApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>BANK</span></div>';
                    }

                    //if releaser done
                    if ($row['HISTORY_STATUS'] == 1) {
                        $makerStatus = 'active';
                        /*$approveStatus = 'active';
						$reviewStatus = 'active';
						$releaseStatus = 'active';
						$releaseIcon = '<i class="fas fa-check"></i>';*/
                        $approveStatus = '';
                        $reviewStatus = '';
                        $releaseStatus = '';
                        $releaseIcon = '';

                        $makerOngoing = '';
                        $reviewerOngoing = '';
                        $approverOngoing = '';
                        $releaserOngoing = '';

                        $custlogin = $row['USER_LOGIN'];

                        $selectCust = $this->_db->select()
                            ->from('M_USER')
                            ->where("USER_ID = ?", $custlogin)
                            ->where("CUST_ID = ?", $row['CUST_ID']);

                        $customer = $this->_db->fetchAll($selectCust);

                        $custFullname = $customer[0]['USER_FULLNAME'];
                        // $custEmail 	  = $customer[0]['USER_EMAIL'];
                        // $custPhone	  = $customer[0]['USER_PHONE'];

                        $releaserApprovedBy = $custFullname;

                        $align = 'align="center"';
                        $marginLeft = '';
                        if ($cust_reviewer == 0 && $cust_approver == 0) {
                            $align = '';
                            $marginLeft = 'style="margin-left: 15px;"';
                        }

                        $efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
                        // $this->view->releaserApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span>' . $custFullname . '</span></div>';
                    }
                }


                //approvernamecircle jika sudah ada yang approve
                if (!empty($userid)) {

                    // echo "<code>masuk = $data</code><BR>";  
                    $alphabet = array('01' => 'A', '02' => 'B', '03' => 'C', '04' => 'D', '05' => 'E', '06' => 'F', '07' => 'G', '08' => 'H', '09' => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z', 27 => 'SG');

                    $flipAlphabet = array_flip($alphabet);

                    $approvedNameList = array();
                    $i = 0;
                    foreach ($userid as $key => $value) {

                        //select utk nama dan email
                        $selectusername = $this->_db->select()
                            ->from(array('M_USER'), array(
                                '*'
                            ))
                            ->where("CUST_ID = ?", $this->_custIdLogin)
                            ->where("USER_ID = ?", (string) $value);

                        $username = $this->_db->fetchAll($selectusername);

                        //select utk cek user berada di grup apa
                        $selectusergroup    = $this->_db->select()
                            ->from(array('C' => 'M_APP_GROUP_USER'), array(
                                '*'
                            ))
                            ->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
                            ->where("C.USER_ID 	= ?", (string) $value);

                        $usergroup = $this->_db->fetchAll($selectusergroup);

                        $groupuserid = $usergroup[0]['GROUP_USER_ID'];
                        $groupusername = $usergroup[0]['USER_ID'];
                        $groupuseridexplode = explode("_", $groupuserid);

                        if ($groupuseridexplode[0] == "S") {
                            $usergroupid = "SG";
                        } else {
                            $usergroupid = $alphabet[$groupuseridexplode[2]];
                        }



                        array_push($approvedNameList, $username[0]['USER_FULLNAME']);

                        $efdate = $approveEfDate[$i];

                        $approverApprovedBy[] = '<div align="center" class="textTheme">' . $efdate . '<br>' . $username[0]['USER_FULLNAME'] . ' (' . $usergroupid . ')</div>';

                        $i++;
                    }

                    $this->view->approverApprovedBy = $approverApprovedBy;

                    //kalau sudah approve semua
                    if (!$checkBoundary) {
                        $approveStatus = 'active';
                        $approverOngoing = '';
                        $approveIcon = '<i class="fas fa-check"></i>';
                        $releaserOngoing = 'ongoing';
                    }
                }

                //define circle
                $makerNameCircle = '<button id="makerCircle" style="cursor:default" class="btnCircleGroup ' . $makerStatus . ' ' . $makerOngoing . ' hovertext" disabled>' . $makerIcon . '</button>';


                // $approverNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $approveStatus . ' ' . $approverOngoing . ' hovertext" disabled><i class="fas fa-check"></i>
                // 	</button>';
                $approverNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $approveStatus . ' ' . $approverOngoing . ' hovertext" disabled>
                ' . $approveIcon . '
                </button>';

                foreach ($releaserList as $key => $value) {

                    $textColor = '';
                    if ($value == $releaserApprovedBy) {
                        $textColor = 'text-white-50';
                    }

                    $releaserListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value . '</p>';
                }

                $releaserNameCircle = '<button id="releaserCircle" class="btnCircleGroup  ' . $releaseStatus . ' ' . $releaserOngoing . ' hovertext" disabled>' . $releaseIcon . '
					<span class="hovertextcontent" style="left: -100px; text-align: center;">' . $releaserListView . '</span></button>';

                $verifyNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $verifyStatus . ' ' . $verifyOngoing . ' hovertext" disabled>' . $verifyIcon . ' </button>';

                foreach ($insuranceList as $key => $value) {

                    $textColor = '';
                    // if ($value == $insuranceApprovedBy) {
                    $textColor = 'text-white-50';
                    // }

                    $insuranceListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value . '</p>';
                }
                // 
                $insuranceNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $insuranceStatus . ' ' . $insuranceOngoing . ' hovertext" disabled>' . $insuranceIcon . ' </button>';


                $releasenewNameCircle = '<button id="makerCircle" style="cursor:default" class="btnCircleGroup ' . $releasenewStatus . ' ' . $makerOngoing . ' hovertext" disabled>' . $releasenewIcon . '</button>';

                $bankReviewNameCircle = '<button id="makerCircle" style="cursor:default" class="btnCircleGroup ' . $bankReviewStatus . ' ' . $reviewOngoing . ' hovertext" disabled>' . $bankReviewIcon . '</button>';

                $bankApproveNameCircle = '<button id="makerCircle" style="cursor:default" class="btnCircleGroup ' . $bankApproveStatus . ' ' . $reviewOngoing . ' hovertext" disabled>' . $bankApproveIcon . '</button>';

                $bankApproverNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $bankApproverStatus . ' ' . $bgapproverOngoing . ' hovertext" disabled> ' . $bankApproverIcon . ' </button>';

                foreach ($releaserList as $key => $value) {

                    $textColor = '';
                    if ($value == $verificationApprovedBy) {
                        $textColor = 'text-white-50';
                    }

                    $verificationListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value . '</p>';
                }

                $verificationNameCircle = '<button id="releaserCircle" class="btnCircleGroup  ' . $verificationStatus . ' ' . $verificationOngoing . ' hovertext" disabled>' . $verificationIcon . ' <span class="hovertextcontent" style="left: -100px; text-align: center;">' . $verificationListView . '</span></button>';

                // echo "<code>cust_reviewer = $cust_reviewer | cust_approver = $cust_approver </code><BR>";  
                $this->view->cust_reviewer = $cust_reviewer;
                $this->view->cust_approver = $cust_approver;
                $this->view->releasenewNameCircle = $releasenewNameCircle;
                $this->view->insuranceNameCircle = $insuranceNameCircle;
                $this->view->makerNameCircle = $makerNameCircle;
                $this->view->bankReviewNameCircle = $bankReviewNameCircle;
                $this->view->bankApproveNameCircle = $bankApproveNameCircle;
                $this->view->approverNameCircle = $approverNameCircle;
                $this->view->releaserNameCircle = $releaserNameCircle;
                $this->view->verifyNameCircle = $verificationNameCircle;
                $this->view->verifyNameCircle = $verifyNameCircle;


                $this->view->makerStatus = $makerStatus;
                $this->view->approveStatus = $approveStatus;
                $this->view->releaseStatus = $releaseStatus;
                $this->view->verificationStatus = $verificationStatus;
                // echo "<code>releaseStatus = $releaseStatus</code><BR>"; 



                if (!empty($data['BG_BRANCH'])) {
                    $selectbranch = $this->_db->select()
                        ->from(array('A' => 'M_BRANCH'), array('*'))
                        ->where('A.BRANCH_CODE = ?', $data['BG_BRANCH'])
                        ->query()->fetchAll();
                    //var_dump($selectbranch[0]['BRANCH_NAME']);die;
                    $this->view->branchname = $selectbranch[0]['BRANCH_NAME'];
                }

                $this->view->updateStart = Application_Helper_General::convertDate($data['TIME_PERIOD_START'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
                $this->view->updateEnd = Application_Helper_General::convertDate($data['TIME_PERIOD_END'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
                // $updateEnd = Application_Helper_General::convertDate($data['TIME_PERIOD_END'],$this->view->viewDateFormat,$this->view->defaultDateFormat);


                // echo "<code>updateEnd = $updateEnd</code><BR>"; die;
                $config            = Zend_Registry::get('config');
                $BgType         = $config["bg"]["status"]["desc"];
                $BgCode         = $config["bg"]["status"]["code"];

                $arrStatus = array_combine(array_values($BgCode), array_values($BgType));

                $this->view->arrStatus = $arrStatus;

                $comp = "'" . $this->_custIdLogin . "'";
                $acctlist = $this->_db->fetchAll(
                    $this->_db->select()
                        ->from(array('A' => 'M_APIKEY'))
                        ->join(array('C' => 'M_APICREDENTIAL'), 'A.APIKEY_ID = C.ID', array('*'))
                        ->join(array('B' => 'M_BANKTABLE'), 'B.BANK_CODE = A.BANK_CODE', array('B.BANK_NAME'))
                        // ->where('A.ACCT_STATUS = ?','5')
                        ->where("A.CUST_ID IN (" . $comp . ")")
                        ->order('A.APIKEY_ID ASC')
                    // echo $acctlist;
                );

                // echo "<pre>";
                // var_dump($acctlist);

                $account = array();
                foreach ($acctlist as $key => $value) {
                    $account[$value['ID']][$value['FIELD']] = $value['VALUE'];
                    $account[$value['ID']]['BANK_CODE'] = $value['BANK_CODE'];
                    $account[$value['ID']]['SENDER_ID'] = $value['SENDER_ID'];
                    $account[$value['ID']]['AUTH_USER'] = $value['AUTH_USER'];
                    $account[$value['ID']]['AUTH_PASS'] = $value['AUTH_PASS'];
                    $account[$value['ID']]['BANK_NAME'] = $value['BANK_NAME'];
                    $account[$value['ID']]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
                }
                // echo "<pre>";
                // var_dump($account);die;
                // $acct = array();
                $i = 0;
                foreach ($account as $key => $value) {

                    $acct[$i]['ACCT_NO'] = $value['account_number'];
                    $acct[$i]['ACCT_BANK'] = $value['BANK_NAME'];
                    $acct[$i]['BANK_CODE'] = $value['BANK_CODE'];
                    $acct[$i]['BANK_NAME'] = $value['BANK_NAME'];
                    $acct[$i]['ACCT_NAME'] = $value['account_name'];
                    $i++;
                }

                $this->view->sourceAcct = $acct;


                // $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);


                // $param = array('CCY_IN' => 'IDR', 'ACCT_NO' => $data['FEE_CHARGE_TO']);
                // $AccArr = $CustomerUser->getAccounts($param);

                //usd
                // $paramUSD = array('CCY_IN' => 'USD');
                // $AccArrUSD = $CustomerUser->getAccountsBG($paramUSD);
                // if (!empty($AccArrUSD)) {
                //     foreach ($AccArrUSD as $iUSD => $valueUSD) {
                //         $AccArrUSD[$iUSD]['ACCT_BANK'] = $this->_bankName;
                //     }
                // }

                // $this->view->AccArrUSD = $AccArrUSD;

                $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

                if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
                    $checkInsurance = $this->_db->select()
                        ->from('M_CUST_LINEFACILITY', ['FEE_DEBITED'])
                        ->where('CUST_ID = ?', $data['BG_INSURANCE_CODE'])
                        ->query()->fetch();

                    if ($checkInsurance['FEE_DEBITED'] == '2') $CustomerUser = new CustomerUser($data['BG_INSURANCE_CODE'], $this->_userIdLogin);
                }
                $param = array('CCY_IN' => 'IDR', 'ACCT_NO' => $data['FEE_CHARGE_TO']);
                $AccArr = $CustomerUser->getAccountsBG($param);
                //var_dump($AccArr);die;

                if (!empty($AccArr)) {
                    $this->view->src_name = $AccArr['0']['ACCT_NAME'];
                }

                $arrBankFormat = array(
                    1 => 'Bank Standard',
                    2 => 'Special Format (with bank approval)'
                );

                $this->view->BG_FORMAT = $data['BG_FORMAT'];
                $this->view->bankFormat = $arrBankFormat[$data['BG_FORMAT']];
                $this->view->bankFormatNumber = $data['BG_FORMAT'];

                if ($data['BG_STATUS'] == '7' || $data['BG_STATUS'] == '11' ||  $data['BG_STATUS'] == '5' || $data['BG_STATUS'] == '10' || !empty($data['BG_REJECT_NOTES']) || !empty($data['BG_CANCEL_NOTES'])) {
                    $selectQuery  = "SELECT
                        a.USER_LOGIN,
                        b.`USER_FULLNAME` AS u_name,
                        c.`BUSER_NAME` AS b_name,
                        a.DATE_TIME,
                        a.BG_REASON,
                        a.HISTORY_STATUS,
                        a.BG_REG_NUMBER
                        
                        
                    FROM
                        T_BANK_GUARANTEE_HISTORY AS a
                        LEFT JOIN M_USER AS b ON a.`USER_LOGIN` = b.`USER_ID` AND a.`CUST_ID` = b.`CUST_ID`
                        LEFT JOIN `M_BUSER` AS c ON a.`USER_LOGIN` = c.`BUSER_ID`
                    WHERE a.BG_REG_NUMBER = " . $this->_db->quote((string) $data['BG_REG_NUMBER']) . " AND a.HISTORY_STATUS IN (10,5) GROUP BY HISTORY_ID ORDER BY DATE_TIME";
                    if ($data['BG_STATUS'] == '10' || $data['BG_STATUS'] == '5') {
                        $result =  $this->_db->fetchAll($selectQuery);
                    } else {
                        $result = array();
                    }
                    if (!empty($result)) {
                        $this->view->reqrepair = true;

                        $data['REASON'] = $resut['0']['BG_REASON'];
                        $this->view->username = ' (By ' . $result['0']['u_name'] . '' . $result['0']['b_name'] . ')';
                        foreach ($result as $key => $value) {
                            $reason[$value['HISTORY_STATUS']][] = $value['BG_REASON'];
                        }
                    }
                    $rowindex = count($reason[10]) - 1;

                    $this->view->reason = $reason[10][$rowindex] . '' . $data['REASON'] . '' . $data['BG_REJECT_NOTES'] . $data['BG_CANCEL_NOTES'];
                }

                $this->view->TIME_PERIOD_START = $data['TIME_PERIOD_START'];
                $this->view->TIME_PERIOD_END = $data['TIME_PERIOD_END'];

                $arrLang = array(
                    1 => 'Indonesian',
                    2 => 'English',
                    3 => 'Billingual',
                );

                $this->view->BG_LANGUAGE = $data['BG_LANGUAGE'];
                $this->view->languagetext = $arrLang[$data['BG_LANGUAGE']];

                $arrWaranty = array(
                    1 => 'Full Cover, Savings Account (Giro / Time Deposit / Savings) or Cash Deposit (MD)',
                    2 => 'Indirect Credit Facility (Non Cash Loan) / Bank Guarantee Ceiling',
                    3 => 'Insurance'

                );

                // BG TYPE
                $bgType         = $conf["bg"]["type"]["desc"];
                $bgCode         = $conf["bg"]["type"]["code"];

                $arrbgType = array_combine(array_values($bgCode), array_values($bgType));

                foreach ($arrbgType as $key => $val) {
                    $arrbgType_new[$key] =  array(
                        'ID' => $key,
                        'VALUE' => $val,
                    );
                }
                $list_bg_purpose                = array('' => ' --- ' . $this->language->_('Choose One') . ' ---');
                $list_bg_purpose                += Application_Helper_Array::listArray($arrbgType_new, 'ID', 'VALUE');
                $this->view->bgpurposeArr       = $list_bg_purpose;

                // $this->view->arrbgType = $arrbgType;
                $this->view->BG_PURPOSE = $data['USAGE_PURPOSE'];
                $this->view->BG_PURPOSE_LBL = $arrbgType[$data['USAGE_PURPOSE']];
                $this->view->BG_PURPOSE_DESC = $arrbgType[$data['USAGE_PURPOSE']];

                //BG Document Type
                $bgdocType         = $conf["bgdoc"]["type"]["desc"];
                $bgdocCode         = $conf["bgdoc"]["type"]["code"];

                $arrbgdoc = array_combine(array_values($bgdocCode), array_values($bgdocType));

                foreach ($arrbgdoc as $key => $val) {
                    $arrbgdoc_new[$key] =  array(
                        'ID' => $key,
                        'VALUE' => $val,
                    );
                }
                $list_document_type                = array('' => ' --- ' . $this->language->_('Choose One') . ' ---');
                $list_document_type                += Application_Helper_Array::listArray($arrbgdoc_new, 'ID', 'VALUE');
                $this->view->documenttypeArr     = $list_document_type;

                $this->view->GT_DOC_TYPE =  $data['GT_DOC_TYPE'];
                $this->view->GT_DOC_DESC = $arrbgdoc[$data['GT_DOC_TYPE']];

                //BG Counter Guarantee Type
                $bgcgType         = $conf["bgcg"]["type"]["desc"];
                $bgcgCode         = $conf["bgcg"]["type"]["code"];

                $arrbgcg = array_combine(array_values($bgcgCode), array_values($bgcgType));

                foreach ($arrbgcg as $key => $val) {
                    $arrbgcg_new[$key] =  array(
                        'ID' => $key,
                        'VALUE' => $val,
                    );
                }
                $list_warranty_type                = array('' => ' --- ' . $this->language->_('Choose One') . ' ---');
                $list_warranty_type                += Application_Helper_Array::listArray($arrbgcg_new, 'ID', 'VALUE');
                $this->view->warrantytypeArr     = $list_warranty_type;

                $this->view->COUNTER_WARRANTY_TYPE = $data['COUNTER_WARRANTY_TYPE'];
                $this->view->warranty_type_text = $arrbgcg[$data['COUNTER_WARRANTY_TYPE']];


                $linefacilitydata = $this->_db->fetchRow(
                    $this->_db->select()
                        ->from(array('M_CUSTOMER'))
                        ->where("CUST_ID=?",  $data['CUST_ID'])->limit(1)
                );
                $get_linefacility = $this->_db->select()
                    ->from("M_CUST_LINEFACILITY", ["PLAFOND_LIMIT", "FEE_PROVISION", "FEE_ADMIN", "FEE_STAMP"])
                    ->where("CUST_ID = ?", $data["CUST_ID"])
                    ->query()->fetchAll();

                $this->view->current_limit = $get_linefacility[0]["PLAFOND_LIMIT"] - $total_bgamount_on_risk - $total_bgamount_on_temp;
                $this->view->max_limit = $get_linefacility[0]["PLAFOND_LIMIT"];
                $this->view->linefacilitydata = $linefacilitydata;


                // BG Publishing Form
                $bgpublishType         = $conf["bgpublish"]["type"]["desc"];
                $bgpublishCode         = $conf["bgpublish"]["type"]["code"];

                $arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));
                foreach ($arrbgpublish as $key => $val) {
                    $arrbgpublish_new[$key] =  array(
                        'ID' => $key,
                        'VALUE' => $val,
                    );
                }
                $list_bg_publist                = array('' => ' --- ' . $this->language->_('Any Value') . ' ---');
                $list_bg_publist                += Application_Helper_Array::listArray($arrbgpublish_new, 'ID', 'VALUE');
                $this->view->BG_PUBLISH_ARR     = $list_bg_publist;
                // echo '<pre>';
                // print_r($list_bg_publist); 
                $this->view->BG_PUBLISH = $data['BG_PUBLISH'];
                $this->view->BG_PUBLISH_DESC = $arrbgpublish[$data['BG_PUBLISH']];

                //$this->view->warranty_type_text = $arrWaranty[$data['COUNTER_WARRANTY_TYPE']];

                if (!empty($data['USAGE_PURPOSE'])) {
                    $data['USAGE_PURPOSE'] = explode(',', $data['USAGE_PURPOSE']);
                    foreach ($data['USAGE_PURPOSE'] as $key => $val) {
                        $str = 'checkp' . $val;
                        //var_dump($str);
                        $this->view->$str =  'checked';
                    }
                }

                if ($data['BG_NUMBER'] == '') {
                    $data['BG_NUMBER'] = '-';
                }
                if ($data['BG_SUBJECT'] == '') {
                    $data['BG_SUBJECT'] = '- no subject -';
                }
                $this->view->BG_REG_NUMBER = $data['BG_REG_NUMBER'];
                $this->view->BG_NUMBER = $data['BG_NUMBER'];
                $this->view->BG_SUBJECT = $data['BG_SUBJECT'];

                $this->view->recipent_name = $data['RECIPIENT_NAME'];
                $this->view->BG_CLAIM_DATE = $data['BG_CLAIM_DATE'];
                $this->view->address = $data['RECIPIENT_ADDRES'];
                $this->view->city = $data['RECIPIENT_CITY'];
                $this->view->contact_number = $data['RECIPIENT_CONTACT'];
                $this->view->comment = $data['GUARANTEE_TRANSACTION'];

                $this->view->fileName = $data['FILE'];
                $this->view->bank_amount = $data['BG_AMOUNT'];
                $this->view->warranty_type = $data['COUNTER_WARRANTY_TYPE'];
                $this->view->account_number = $data['COUNTER_WARRANTY_ACCT_NO'];
                $this->view->account_name = $data['COUNTER_WARRANTY_ACCT_NAME'];

                $this->view->amount = $data['COUNTER_WARRANTY_AMOUNT'];
                $this->view->acct = $data['FEE_CHARGE_TO'];
                $this->view->status = $data['BG_STATUS'];

                $this->view->CUST_EMAIL = $data['CUST_EMAIL'];

                $this->view->CUST_NAME = $data['CUST_NAME'];
                $this->view->CUST_CP = $data['CUST_CP'];
                $this->view->CUST_EMAIL = $data['CUST_EMAIL'];
                $this->view->CUST_CONTACT_NUMBER = $data['CUST_CONTACT_NUMBER'];

                $this->view->RECIPIENT_CP = $data['RECIPIENT_CP'];
                $this->view->RECIPIENT_OFFICE_NUMBER = $data['RECIPIENT_OFFICE_NUMBER'];
                $this->view->RECIPIENT_EMAIL = $data['RECIPIENT_EMAIL'];
                $this->view->RECIPIENT_CONTACT = $data['RECIPIENT_CONTACT'];
                $this->view->SERVICE = $data['SERVICE'];

                $this->view->GT_DOC_NUMBER = $data['GT_DOC_NUMBER'];
                $this->view->GT_DOC_DATE = $data['GT_DOC_DATE'];

                // $this->view->BG_CLAIM_PERIOD = $data['BG_CLAIM_PERIOD'];
                $this->view->BG_UNDERLYING_DOC = $data['BG_UNDERLYING_DOC'];

                $checkOthersAttachment = $this->_db->select()
                    ->from(['A' => 'TEMP_BANK_GUARANTEE_FILE'], ['*'])
                    ->where('BG_REG_NUMBER = ?', $data['BG_REG_NUMBER'])
                    ->order('A.INDEX ASC')
                    ->query()->fetchAll();

                if (count($checkOthersAttachment) > 0) {
                    $this->view->othersAttachment = $checkOthersAttachment;
                }

                if ($data['COUNTER_WARRANTY_TYPE'] == 1) {

                    $bgdatasplit = $this->_db->select()
                        ->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
                        ->where('A.BG_REG_NUMBER = ?', $data['BG_REG_NUMBER'])
                        ->query()->fetchAll();

                    $this->view->fullmember = $bgdatasplit;
                }

                if (!empty($bgdatadetail)) {
                    foreach ($bgdatadetail as $key => $value) {
                        $insurancedata = $this->_db->fetchRow(
                            $this->_db->select()
                                ->from(array('M_CUSTOMER'))
                                ->where("CUST_ID=?",  $value['PS_FIELDVALUE'])->joinLeft(
                                    array('MCL' => 'M_CITYLIST'),
                                    'MCL.CITY_CODE = M_CUSTOMER.CUST_CITY',
                                    array('CITY_NAME')
                                )->limit(1)
                        );
                        $insurancebranch = $this->_db->fetchRow(
                            $this->_db->select()
                                ->from(array('M_INS_BRANCH'))
                                ->where("INS_BRANCH_CODE=?",  $value['PS_FIELDVALUE'])->limit(1)
                        );
                        if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
                            if ($value['PS_FIELDNAME'] == 'Insurance Name') {
                                $this->view->insuranceName =   $insurancedata['CUST_NAME'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Insurance Branch') {
                                $this->view->insuranceBranch =   $insurancebranch['INS_BRANCH_NAME'];
                            }


                            if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
                                $this->view->PrincipalAgreement =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount') {
                                $this->view->insurance_amount =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement Start Date') {
                                $this->view->paDateStart =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement End Date') {
                                $this->view->paDataEnd =   $value['PS_FIELDVALUE'];
                            }
                        } else {

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 1') {
                                $this->view->owner1 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 1') {
                                $this->view->amountowner1 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
                                $this->view->owner2 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
                                $this->view->amountowner2 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
                                $this->view->owner3 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
                                $this->view->amountowner3 =   $value['PS_FIELDVALUE'];
                            }
                        }
                    }
                }

                // Marginal Deposit View ------------------------
                // Marginal Deposit Principal ---------------
                $bgdatadetailmd = $this->_db->select()
                    ->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL'), array('*'))
                    ->where('A.BG_REG_NUMBER = ?', $numb)
                    ->query()->fetchAll();

                if (!empty($bgdatadetailmd)) {
                    foreach ($bgdatadetailmd as $key => $value) {
                        if ($value['PS_FIELDNAME'] == 'Marginal Deposit Percentage') {
                            $this->view->marginalDepositPercentage =   $value['PS_FIELDVALUE'];
                        }
                    }
                }
                // Marginal Deposit Principal ---------------

                // Top Up Marginal Deposit ---------------
                $bgdatamd = $this->_db->select()
                    ->from(array('A' => 'TEMP_BANK_GUARANTEE'), array('*'))
                    ->join(array('B' => 'TEMP_BANK_GUARANTEE_SPLIT'), 'A.BG_REG_NUMBER = B.BG_REG_NUMBER')
                    ->where('B.BG_REG_NUMBER = ?', $numb)
                    ->query()->fetchAll();

                $this->view->bgdatamd = $bgdatamd;
                // Top Up Marginal Deposit ---------------
                // Marginal Deposit View ------------------------


                $conf = Zend_Registry::get('config');
                $this->view->bankname = $conf['app']['bankname'];
                // preliminary Member
                $selectMP = $this->_db->select()
                    ->from(array('MP' => 'M_PRELIMINARY_MEMBER'), array('*'));
                $selectMP->where('MP.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin));
                // echo "<code>selectMP = $selectMP</code><BR>";  
                $preliminaryMemberArr = $this->_db->fetchAll($selectMP);
                // echo '<pre>';
                // print_r($preliminaryMemberArr);
                $this->view->preliminaryMemberArr = $preliminaryMemberArr;

                $params = $this->_request->getParams();
                $toa = $params['toa'];

                $BG_UNDERLYING_DOC = $this->_getParam('BG_UNDERLYING_DOC');
                //print_r($edit);die;
                if ($BG_UNDERLYING_DOC) {
                    $attahmentDestination = UPLOAD_PATH . '/document/submit/';
                    $this->_helper->download->file($data['BG_UNDERLYING_DOC'], $attahmentDestination . $data['BG_UNDERLYING_DOC']);
                }

                if ($this->_request->isPost()) {
                    $cancelReason = $this->_request->getParam('cancle-reason');

                    $makerCode = 1;
                    $lastMaker = $this->_db->select()
                        ->from('T_BANK_GUARANTEE_HISTORY', ['DATE_TIME', 'USER_LOGIN'])
                        ->where("BG_REG_NUMBER = ?", $numb)
                        ->where("HISTORY_STATUS = ?", $makerCode)
                        ->order('DATE_TIME DESC');
                    $lastMaker = $this->_db->fetchRow($lastMaker);

                    $cekPreliminary = $this->_db->select()
                        ->from('TEMP_BANK_GUARANTEE_PRELIMINARY')
                        ->where('BG_REG_NUMBER = ?', $numb);
                    $cekPreliminary = $this->_db->fetchAll($cekPreliminary);

                    $cekPreliminaryMember = $this->_db->select()
                        ->from('TEMP_BANK_GUARANTEE_PRELIMINARY_MEMBER')
                        ->where('BG_REG_NUMBER = ?', $numb);
                    $cekPreliminaryMember = $this->_db->fetchAll($cekPreliminaryMember);

                    $dataUpdate = [
                        'BG_STATUS' => 13,
                        'BG_UPDATED' => $lastMaker['DATE_TIME'],
                        'BG_UPDATEDBY' => $lastMaker['USER_LOGIN'],
                    ];

                    $wheredata['BG_REG_NUMBER = ?'] = $data['BG_REG_NUMBER'];

                    try {
                        $this->_db->beginTransaction();
                        // Update data ke table TEMP_BANK_GUARANTEE
                        $this->_db->update("TEMP_BANK_GUARANTEE", $dataUpdate, $wheredata);

                        // Hapus data Dokumen Awal dari table TEMP_BANK_GUARANTEE_PRELIMINARY dan TEMP_BANK_GUARANTEE_PRELIMINARY_MEMBER
                        if ($cekPreliminary) {
                            $this->_db->delete("TEMP_BANK_GUARANTEE_PRELIMINARY", $wheredata);
                        }

                        if ($cekPreliminaryMember) {
                            $this->_db->delete("TEMP_BANK_GUARANTEE_PRELIMINARY_MEMBER", $wheredata);
                        }

                        // Hapus Data kontra Asuransi dari table TEMP_BANK_GUARANTEE_DETAIL
                        if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
                            $fieldInsurance = [
                                ['value' => 'Counter Guarantee Number'],
                                ['value' => 'Counter Guarantee Granted Date'],
                                ['value' => 'Counter Guarantee Document'],
                                ['value' => 'Insurance Special Format Approval Document'],
                                ['value' => 'Principle Agreement Number'],
                                ['value' => 'Principle Agreement Granted Date'],
                                ['value' => 'Principle Insurance Document'],
                                ['value' => 'Principle Insurance Premium'],
                                ['value' => 'Principle Insurance Administration'],
                                ['value' => 'Principle Insurance Stamp'],
                                ['value' => 'Marginal Deposit Percentage'],
                            ];

                            foreach ($fieldInsurance as $field) {
                                $this->_db->delete("TEMP_BANK_GUARANTEE_DETAIL", [
                                    'BG_REG_NUMBER = ?' => $data['BG_REG_NUMBER'],
                                    'PS_FIELDNAME = ?' => $field['value']
                                ]);
                            }
                        }

                        // Insert data ke table T_BANK_GUARANTEE_HISTORY
                        $historyInsert = array(
                            'DATE_TIME' => date('Y-m-d H:i:s'),
                            'BG_REG_NUMBER' => $data['BG_REG_NUMBER'],
                            'USER_LOGIN' => $this->_userIdLogin,
                            'CUST_ID' => $this->_custIdLogin,
                            'BG_REASON' => $cancelReason,
                            'HISTORY_STATUS' => 34,
                        );

                        $this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);

                        $this->_db->commit();
                    } catch (\Throwable $th) {
                        $this->_db->rollBack();
                    }

                    // Simpan log aktifitas user ke table T_FACTIVITY
                    Application_Helper_General::writeLog('SBRP', 'Pembatalan Pengajuan untuk Nomor Registrasi : ' . $data['BG_REG_NUMBER'] . ', Subjek : ' . $data['BG_SUBJECT'] . ', Catatan : ' . $cancelReason);

                    $this->setbackURL('/ongoingsubmission');
                    $this->_redirect('/notification/success');
                }

                $download = $this->_getParam('download');
                $BG_APPROVE_DOC = $this->_getParam('BG_APPROVE_DOC');
                //print_r($edit);die;
                if ($BG_APPROVE_DOC) {
                    $attahmentDestination = UPLOAD_PATH . '/document/bg/';
                    //var_dump($attahmentDestination.$data['BG_APPROVE_DOC']);die;
                    $this->_helper->download->file($data['BG_APPROVE_DOC'], $attahmentDestination . $data['BG_APPROVE_DOC']);
                }
            }

            Application_Helper_General::writeLog('SBRP', 'Lihat Detail Data Bank Garansi Dalam Proses Pengajuan untuk Nomor Registrasi : ' . $data['BG_REG_NUMBER'] . ', Subjek : ' . $data['BG_SUBJECT']);
        }
    }

    public function Terbilang($nilai)
    {
        $huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
        if ($nilai == 0) {
            return "";
        } elseif ($nilai < 12 & $nilai != 0) {
            return "" . $huruf[$nilai];
        } elseif ($nilai < 20) {
            return $this->Terbilang($nilai - 10) . " Belas ";
        } elseif ($nilai < 100) {
            return $this->Terbilang($nilai / 10) . " Puluh " . $this->Terbilang($nilai % 10);
        } elseif ($nilai < 200) {
            return " Seratus " . $this->Terbilang($nilai - 100);
        } elseif ($nilai < 1000) {
            return $this->Terbilang($nilai / 100) . " Ratus " . $this->Terbilang($nilai % 100);
        } elseif ($nilai < 2000) {
            return " Seribu " . $this->Terbilang($nilai - 1000);
        } elseif ($nilai < 1000000) {
            return $this->Terbilang($nilai / 1000) . " Ribu " . $this->Terbilang($nilai % 1000);
        } elseif ($nilai < 1000000000) {
            return $this->Terbilang($nilai / 1000000) . " Juta " . $this->Terbilang($nilai % 1000000);
        } elseif ($nilai < 1000000000000) {
            return $this->Terbilang($nilai / 1000000000) . " Milyar " . $this->Terbilang($nilai % 1000000000);
        } elseif ($nilai < 100000000000000) {
            return $this->Terbilang($nilai / 1000000000000) . " Trilyun " . $this->Terbilang($nilai % 1000000000000);
        } elseif ($nilai <= 100000000000000) {
            return "Maaf Tidak Dapat di Prose Karena Jumlah nilai Terlalu Besar ";
        }
    }

    public function Terbilangen($nilai)
    {
        $huruf = array("", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", 'Nineteen');
        if ($nilai == 0) {
            return "";
        } elseif ($nilai < 20 & $nilai != 0) {
            return "" . $huruf[$nilai];
        } elseif ($nilai < 100) {
            return $this->Terbilangen($nilai / 10) . "ty " . $this->Terbilangen($nilai % 10);
        } elseif ($nilai < 200) {
            return " Seratus " . $this->Terbilangen($nilai - 100);
        } elseif ($nilai < 1000) {
            return $this->Terbilangen($nilai / 100) . " hundred " . $this->Terbilangen($nilai % 100);
        } elseif ($nilai < 2000) {
            return " Seribu " . $this->Terbilangen($nilai - 1000);
        } elseif ($nilai < 1000000) {
            return $this->Terbilangen($nilai / 1000) . " thausand " . $this->Terbilangen($nilai % 1000);
        } elseif ($nilai < 1000000000) {
            return $this->Terbilangen($nilai / 1000000) . " million " . $this->Terbilangen($nilai % 1000000);
        } elseif ($nilai < 1000000000000) {
            return $this->Terbilangen($nilai / 1000000000) . " Bilion " . $this->Terbilangen($nilai % 1000000000);
        } elseif ($nilai < 100000000000000) {
            return $this->Terbilangen($nilai / 1000000000000) . " Trillion " . $this->Terbilangen($nilai % 1000000000000);
        } elseif ($nilai <= 100000000000000) {
            return "Maaf Tidak Dapat di Prose Karena Jumlah nilai Terlalu Besar ";
        }
    }

    public function indodate($tanggal)
    {
        $bulan = array(
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);

        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun

        return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
    }

    public function generateTransactionID($branch, $bgtype)
    {


        // echo "<code>branch = $branch | bgtype = $bgtype  </code><BR>"; die;	
        $currentDate = date("Ymd");
        $seqNumber   = Application_Helper_General::get_rand_id(4);
        $trxId = $seqNumber . $branch . '01' . $bgtype;

        return $trxId;
    }


    public function cancelAction()
    {
        $params = $this->_request->getParams();


        $updatedata['BG_STATUS'] = 99;
        $wheredata['BG_REG_NUMBER = ?'] = $param['BG_REG_NUMBER'];
        $query = $this->_db->update("TEMP_BANK_GUARANTEE", $updatedata, $wheredata);


        $this->_redirect("ongoingsubmission");
        //Zend_Debug::dump($_SESSION['import_predefbenef']);die;
    }
}
