<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class ongoingsubmission_IndexController extends Application_Main
{

  protected $_moduleDB = 'RTF'; // masih harus diganti

  public function indexAction()
  {
    $this->_helper->_layout->setLayout('newlayout');

    $settings = new Settings();
    $system_type = $settings->getSetting('system_type');
    $this->view->systemType = $system_type;

    $conf = Zend_Registry::get('config');
    $this->_bankName = $conf['app']['bankname'];
    $this->view->masterbankname = $this->_bankName;

    $this->CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

    $fields = array(
      /*'alias'  => array('field' => 'BENEFICIARY_ALIAS',
      'label' => $this->language->_('Alias Name'),
      'sortable' => true),*/
      'regno'     => array(
        'field'    => 'BG_REG_NUMBER',
        'label'    => $this->language->_('Reg No# /  BG No#'),
      ),
      'subject'     => array(
        'field'    => 'BG_SUBJECT',
        'label'    => $this->language->_('Subject'),
      ),
      'ccy'  => array(
        'field'    => 'BG_AMOUNT',
        'label'    => 'CCY',
      ),
      'bgamount'  => array(
        'field'    => 'BG_AMOUNT',
        'label'    => $this->language->_('BG Amount'),
      ),
      'startdate' => array(
        'field' => 'TIME_PERIOD_START',
        'label' => $this->language->_('Date From'),
      ),
      'enddate'   => array(
        'field'    => 'TIME_PERIOD_END',
        'label'    => $this->language->_('Date To'),
      ),
      'chargeaccount' => array(
        'field' => 'FEE_CHARGE_TO',
        'label' => $this->language->_('Charge Account'),
      ),
      'format'   => array(
        'field'    => 'BG_FORMAT',
        'label'    => $this->language->_('Format/Lang'),
      )
    );

    //$filterlist = array('BG_REG_NUMBER', 'BG_SUBJECT', 'BG_NUMBER', 'BG_AMOUNT', 'BRANCH_NAME', 'COUNTER_WARRANTY_TYPE', 'TANGGAL', 'BG_STATUS');
    $filterlist = array('BG_REG_NUMBER', 'BG_SUBJECT', 'BG_NUMBER', 'BRANCH_NAME', 'COUNTER_WARRANTY_TYPE', 'TANGGAL', 'BG_STATUS');

    $this->view->filterlist = $filterlist;

    $filterArr = array(
      'BG_NUMBER'  => array('StringTrim', 'StripTags'),
      'BG_SUBJECT'  => array('StringTrim', 'StripTags'),
      'BG_REG_NUMBER'  => array('StringTrim', 'StripTags'),
      //'BG_AMOUNT'   => array('StringTrim', 'StripTags'),
      'BRANCH_NAME'   => array('StringTrim', 'StripTags'),
      'COUNTER_WARRANTY_TYPE'    => array('StringTrim', 'StripTags'),
      'TIME_PERIOD_START'  => array('StringTrim', 'StripTags'),
      'TIME_PERIOD_END'   => array('StringTrim', 'StripTags'),
      'BG_STATUS'   => array('StringTrim', 'StripTags')
    );

    //$dataParam = array('BG_NUMBER', 'BG_SUBJECT', 'BG_REG_NUMBER', 'BG_STATUS', 'BG_AMOUNT', 'BRANCH_NAME', 'COUNTER_WARRANTY_TYPE', 'TIME_PERIOD_START', 'TIME_PERIOD_END');
    $dataParam = array('BG_NUMBER', 'BG_SUBJECT', 'BG_REG_NUMBER', 'BG_STATUS', 'BRANCH_NAME', 'COUNTER_WARRANTY_TYPE', 'TIME_PERIOD_START', 'TIME_PERIOD_END');
    $dataParamValue = array();

    foreach ($dataParam as $dtParam) {
      if (!empty($this->_request->getParam('wherecol'))) {
        $dataval = $this->_request->getParam('whereval');
        foreach ($this->_request->getParam('wherecol') as $key => $value) {
          if ($dtParam == $value) {
            if (!empty($dataParamValue[$dtParam])) {
              $dataval[$key] = $dataParamValue[$dtParam] . ',' . $dataval[$key];
            }
            $dataParamValue[$dtParam] = $dataval[$key];
          }
        }
      }
    }

    if ($this->_request->getParam('createdate')) {
      $dataParamValue['TIME_PERIOD_START'] = $this->_request->getParam('createdate')[0];
      $dataParamValue['TIME_PERIOD_END'] = $this->_request->getParam('createdate')[1];
    }

    $options = array('allowEmpty' => true);
    $validators = array(
      'TIME_PERIOD_START'  => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
      'TIME_PERIOD_END'   => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
      'BG_NUMBER' => array(),
      'BG_SUBJECT' => array(),
      'BG_REG_NUMBER' => array(),
      //'BG_AMOUNT' => array(),
      'BRANCH_NAME' => array(),
      'COUNTER_WARRANTY_TYPE' => array(),
      'BG_STATUS' => array()
    );

    $zf_filter  = new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);
    $filter = ($zf_filter->filter) ? $zf_filter->filter : $this->_request->getParam('filter');
    $fbgnumb    = $zf_filter->getEscaped('BG_NUMBER');
    $subject   = $zf_filter->getEscaped('BG_SUBJECT');
    $fregnumber   = $zf_filter->getEscaped('BG_REG_NUMBER');
    //$fbgmaount   = $zf_filter->getEscaped('BG_AMOUNT');
    $fbranchname   = $zf_filter->getEscaped('BRANCH_NAME');
    $fcounterType   = $zf_filter->getEscaped('COUNTER_WARRANTY_TYPE');
    $fstatus   = $zf_filter->getEscaped('BG_STATUS');

    $select = $this->_db->select()
      ->from(array('A' => 'TEMP_BANK_GUARANTEE'), array('*'))
      ->join(array('C' => 'M_BRANCH'), 'A.BG_BRANCH = C.BRANCH_CODE', array('BRANCH_NAME' => 'C.BRANCH_NAME'))
      ->joinLeft(array('D' => 'M_CUSTOMER'), 'D.CUST_ID = A.CUST_ID', array('CUST_NAME'))
      ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
      ->where('A.BG_STATUS IN (?)', [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 17, 18, 19, 20, 21, 22, 23, 24, 25])
      ->order('BG_CREATED DESC');

    if ($fbgnumb) {
      $select->where("BG_NUMBER LIKE " . $this->_db->quote('%' . $fbgnumb . '%'));
    }
    if ($subject) {
      $select->where("BG_SUBJECT LIKE " . $this->_db->quote('%' . $subject . '%'));
    }
    if ($fregnumber) {
      $select->where("BG_REG_NUMBER LIKE " . $this->_db->quote('%' . $fregnumber . '%'));
    }
    /*
    if ($fbgmaount) {
      $select->where("BG_AMOUNT LIKE " . $this->_db->quote('%' . $fbgmaount . '%'));
    }
	  */
    if ($fbranchname) {
      $select->where("BRANCH_NAME LIKE " . $this->_db->quote('%' . strtoupper($fbranchname) . '%'));
    }
    if ($fcounterType) {
      $select->where("COUNTER_WARRANTY_TYPE = ?", $fcounterType);
    }
    if ($fstatus) {
      $select->where("BG_STATUS = ?", $fstatus);
    }

    if (!empty($dataParamValue['TIME_PERIOD_START']) && !empty($dataParamValue['TIME_PERIOD_END'])) {

      $select->where("TIME_PERIOD_START >= ?", date('Y-m-d', strtotime($dataParamValue['TIME_PERIOD_START'])));
      $select->where("TIME_PERIOD_END >= ?", date('Y-m-d', strtotime($dataParamValue['TIME_PERIOD_END'])));
    }

    // print_r($select->__toString());
    // die();


    $select = $select->query()->fetchAll();

    try {
      $this->_db->beginTransaction();

      $selectClose = $this->_db->select()
        ->from(array('A' => 'T_BANK_GUARANTEE'), 
          array('*',
            'B.BRANCH_NAME',
            'CHANGETYPECLOSE' => 'D.CHANGE_TYPE',
            'SUGGESTIONSTATUS' => 'D.SUGGESTION_STATUS',
          )
        )
        ->join(array('D' => 'TEMP_BANK_GUARANTEE_CLOSE'), 'D.BG_NUMBER = A.BG_NUMBER', array('CLOSE_REF_NUMBER'))
        ->joinLeft(array('B' => 'M_BRANCH'), 'B.BRANCH_CODE = A.BG_BRANCH', array('BRANCH_NAME'))
        ->joinLeft(array('C' => 'M_CUSTOMER'), 'C.CUST_ID = A.CUST_ID', array('CUST_NAME'))
        ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
        ->where('D.CHANGE_TYPE <> ?', 3)
        ->order('BG_CREATED DESC');

      $selectClose = $selectClose->query()->fetchAll();

      $this->_db->commit();
    } catch (\Throwable $th) {
      $selectClose = [];
    }

    // echo '<pre>';
    // var_dump($selectClose);
    // die;

    $this->paging(array_merge($select, $selectClose));

    // $conf = Zend_Registry::get('config');


    $this->view->bankname = $conf['app']['bankname'];

    $config       = Zend_Registry::get('config');
    $BgType     = $config["bg"]["status"]["desc"];
    $BgCode     = $config["bg"]["status"]["code"];

    $arrStatus = array_combine(array_values($BgCode), array_values($BgType));

    $this->view->arrStatus = $arrStatus;

    $arrChangeType = array(
      '0' => 'Pengajuan Baru',
      //'2' => 'Indirect',
      '1' => 'Amendemen Isi',
      '2' => 'Amendemen Format'
    );

    $this->view->arrChangeType = $arrChangeType;

    $ctdesc = $conf["bgclosing"]["changetype"]["desc"];
    $ctcode = $conf["bgclosing"]["changetype"]["code"];
    $arr_type = array_combine(array_values($ctcode), array_values($ctdesc));
    $this->view->arr_type_close = $arr_type;

    $bgclosingStatus = $conf["bgclosing"]["status"]["desc"];
    $bgclosingstatusCode = $conf["bgclosing"]["status"]["code"];
    $arrbgclosingStatus = array_combine(array_values($bgclosingstatusCode), array_values($bgclosingStatus));
    $this->view->arr_status_close = $arrbgclosingStatus;

    // BG limit
    $cust_id = $this->_db->quote((string)$this->_custIdLogin);
    $select2 = $this->_db->select()
      ->from(array('BG' => 'TEMP_BANK_GUARANTEE'), array('*'))
      ->join(array('C' => 'M_CUSTOMER'), 'BG.CUST_ID = C.CUST_ID', array('CUST_NAME' => 'C.CUST_NAME'))
      ->order('BG_UPDATED DESC');
    $select2->where('BG.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin));

    $bglist = $this->_db->fetchAll($select2);

    $usedbg = 0;
    foreach ($bglist as $vl) {
      if ($vl['BG_STATUS'] == '5' && $vl['COUNTER_WARRANTY_TYPE'] == '2') {
        $usedbg = $usedbg + $vl['BG_AMOUNT'];
      }
    }
    $this->view->bgused = Application_Helper_General::displayMoney($usedbg);
    $this->view->bglist = $bglist;

    $select3 = $this->_db->select()
      ->from(array('C' => 'M_CUSTOMER'), array('*'));
    $select3->where('C.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin));
    $bglimit = $this->_db->fetchAll($select3);

    foreach ($bglimit as $val) {
      $this->view->bglimit   = Application_Helper_General::displayMoney($val['CUST_BG_LIMIT']);
    }

    // END BG limit

    $arrWarrantyType = array(
      '1' => 'FC',
      '2' => 'LF',
      '3' => 'INS'
    );

    $this->view->arrWarrantyType = $arrWarrantyType;

    $arrType = array(
      1 => 'Standard',
      2 => 'Custom'
    );

    $arrLang = array(
      1 => 'Indonesian',
      2 => 'English',
      3 => 'Bilingual'
    );
    $this->view->langArr = $arrLang;
    $this->view->formatArr = $arrType;
    $this->view->fields = $fields;

    if (!empty($dataParamValue)) {

      $this->view->createdStart = $dataParamValue['TIME_PERIOD_START'];
      $this->view->createdEnd = $dataParamValue['TIME_PERIOD_END'];

      foreach ($dataParamValue as $key => $value) {
        $duparr = explode(',', $value);
        if (!empty($duparr)) {

          foreach ($duparr as $ss => $vs) {
            $wherecol[] = $key;
            $whereval[] = $vs;
          }
        } else {
          $wherecol[] = $key;
          $whereval[] = $value;
        }
      }
      $this->view->wherecol     = $wherecol;
      $this->view->whereval     = $whereval;
    }

    $this->view->filter 		= $filter;
    $this->view->fbgnumb 		= $fbgnumb;
    $this->view->subject 		= $subject;
    $this->view->fregnumber 		= $fregnumber;
    $this->view->fbranchname 		= $fbranchname;
    $this->view->fcounterType 		= $fcounterType;
    $this->view->fstatus 		= $fstatus;
    $allData = [];
	

    if (!empty($this->_getParam('csv')) || $this->_request->getParam('print') == 1) {
		
		if ($filter) {
			
			$fbgnumb    = $this->_request->getParam('fbgnumb');
			$subject   = $this->_request->getParam('subject');
			$fregnumber   = $this->_request->getParam('fregnumber');
			$fbranchname   = $this->_request->getParam('fbranchname');
			$fcounterType   = $this->_request->getParam('fcounterType');
			$fstatus = $this->_request->getParam('fstatus');
			$createdStart = $this->_request->getParam('fcreatedStart');
			$createdEnd = $this->_request->getParam('fcreatedEnd');
			
			if ($fbgnumb) {
			  $select->where("BG_NUMBER LIKE " . $this->_db->quote('%' . $fbgnumb . '%'));
			}
			if ($subject) {
			  $select->where("BG_SUBJECT LIKE " . $this->_db->quote('%' . $subject . '%'));
			}
			if ($fregnumber) {
			  $select->where("BG_REG_NUMBER LIKE " . $this->_db->quote('%' . $fregnumber . '%'));
			}
			/*
			if ($fbgmaount) {
			  $select->where("BG_AMOUNT LIKE " . $this->_db->quote('%' . $fbgmaount . '%'));
			}
			*/
			if ($fbranchname) {
			  $select->where("BRANCH_NAME LIKE " . $this->_db->quote('%' . strtoupper($fbranchname) . '%'));
			}
			if ($fcounterType) {
			  $select->where("COUNTER_WARRANTY_TYPE = ?", $fcounterType);
			}
			if ($fstatus) {
			  $select->where("BG_STATUS = ?", $fstatus);
			}

			if ($createdStart) {

			  $select->where("TIME_PERIOD_START >= ?", date('Y-m-d', strtotime($createdStart)));
			  
			}
			
			if ($createdEnd) {

			  $select->where("TIME_PERIOD_END >= ?", date('Y-m-d', strtotime($createdEnd)));
			  
			}
			
			
		}
	
		
      $dataExport = $select->query()->fetchAll();
      //var_dump($select);
      foreach ($dataExport as $row) {
        $subData = [];

        if ($row['BG_NUMBER'] == '') {
          $row['BG_NUMBER'] = '-';
        }

        if ($row['BG_SUBJECT'] == '') {
          $row['BG_SUBJECT'] = '- no subject -';
        }

        $subData['BG_REG_NUMBER'] = $row['BG_REG_NUMBER'] . " / " . $row['BG_SUBJECT'];
        $subData['BG_NUMBER'] = $row['BG_NUMBER'] . ' / ' . $row['ACCT_NO'];
        $subData['BG_AMOUNT'] = 'IDR ' . Application_Helper_General::displayMoneyplain($row['BG_AMOUNT']);
        $subData['BRANCH_NAME'] = $row['BRANCH_NAME'];
        $subData['COUNTER_WARRANTY_TYPE'] = $arrWarrantyType[$row['COUNTER_WARRANTY_TYPE']];
        $subData['TIME_PERIOD_START'] = Application_Helper_General::convertDate($row['TIME_PERIOD_START'], $this->viewDateFormat, $this->defaultDateFormat);
        $subData['TIME_PERIOD_END'] = Application_Helper_General::convertDate($row['TIME_PERIOD_END'], $this->viewDateFormat, $this->defaultDateFormat);
        $subData['BG_STATUS'] = $arrStatus[$row['BG_STATUS']];

        $allData[] = $subData;
      }
    }

    if ($this->_getParam('csv')) {
      //var_dump($allData);
      $this->_helper->download->csv(array($this->language->_('Reg No# / Subject'), $this->language->_('BG Number'), $this->language->_('BG Amount'), $this->language->_('Branch'), $this->language->_('Counter Type'), $this->language->_('Dari Tanggal'), $this->language->_('Date To'), $this->language->_('Status')), $allData, null, 'Ongoing Submission List');
    } else if ($this->_request->getParam('print') == 1) {

      $fields = array(
        'regno'     => array(
          'field'    => 'BG_REG_NUMBER',
          'label'    => $this->language->_('Reg No# / Subject'),
        ),
        'number'     => array(
          'field'    => 'BG_NUMBER',
          'label'    => $this->language->_('BG Number'),
        ),
        'amount'     => array(
          'field'    => 'BG_AMOUNT',
          'label'    => $this->language->_('BG Amount'),
        ),
        'bankbranch'     => array(
          'field'    => 'BRANCH_NAME',
          'label'    => $this->language->_('Branch'),
        ),
        'counter'     => array(
          'field'    => 'COUNTER_WARRANTY_TYPE',
          'label'    => $this->language->_('Counter Type'),
        ),
        'datestart'  => array(
          'field'    => 'TIME_PERIOD_START',
          'label'    => $this->language->_('Dari Tanggal'),
        ),
        'dateto'  => array(
          'field'    => 'TIME_PERIOD_END',
          'label'    => $this->language->_('Date To'),
        ),
        'status'  => array(
          'field'    => 'BG_STATUS',
          'label'    => $this->language->_('Status'),
        ),
      );

      $this->_forward('print', 'index', 'widget', array('data_content' => $allData, 'data_caption' => 'Ongoing Submission List', 'data_header' => $fields));
    }

    Application_Helper_General::writeLog('SBRP', 'Lihat Daftar Bank Garansi Dalam Pengajuan');
  }

  public function getstatusAction()
  {
    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout()->disableLayout();

    $tblName = $this->_getParam('id');

    $optHtml = "<option value=''>-- " . $this->language->_('Please Select') . " --</option>";

    $config        = Zend_Registry::get('config');
    $BgType     = $config["bg"]["status"]["desc"];
    $BgCode     = $config["bg"]["status"]["code"];

    $arrStatus = array_combine(array_values($BgCode), array_values($BgType));

    foreach ($arrStatus as $key => $row) {
      if ($key == 2 || $key == 3 || $key == 4 || $key == 5 || $key == 6 || $key == 7 || $key == 3 || $key == 9 || $key == 10 || $key == 11 || $key == 12 || $key == 13 || $key == 14) {
        if ($tblName == $key) {
          $select = 'selected';
        } else {
          $select = '';
        }
        $optHtml .= "<option value='" . $key . "' " . $select . ">" . $row . "</option>";
      }
    }

    echo $optHtml;
  }

  public function getwarantyAction()
  {
    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout()->disableLayout();

    $tblName = $this->_getParam('id');

    $optHtml = "<option value=''>-- " . $this->language->_('Please Select') . " --</option>";

    $arrWarrantyType = array(
      '1' => 'Full Cover',
      '2' => 'Line Facility',
      '3' => 'Insurance'
    );

    foreach ($arrWarrantyType as $key => $row) {
      if ($tblName == $key) {
        $select = 'selected';
      } else {
        $select = '';
      }
      $optHtml .= "<option value='" . $key . "' " . $select . ">" . $row . "</option>";
    }

    echo $optHtml;
  }

  //   public function ntuAction(){


  //     $data = $this->_db->select()
  //     ->from(array('A' => 'TEMP_BANK_GUARANTEE'),array('*'))
  //     ->where('A.BG_STATUS = 2')
  //     ->orwhere('A.BG_STATUS = 3')
  //     ->order('BG_CREATED DESC')->query()->fetchAll();

  //         //echo $data;die;
  // //print_r($data);die;
  // //   echo $data->__toString();die;

  // //Zend_Debug::dump($data, 'Test'); die;  

  // if($data)
  // {
  // //Zend_Debug::dump($data); die;
  // foreach($data as $row)
  // {
  //     echo $row['BG_CREATED'];
  //     $BG_CREATED              = $row['BG_CREATED'];
  //     $NTU_COUNTING                = $row['NTU_COUNTING'];

  //     $NTU_DATE = date('Y-m-d', strtotime('+'.$NTU_COUNTING.' days', strtotime($BG_CREATED))); //operasi penjumlahan tanggal sebanyak 6 hari

  //     if(date('Y-m-d') >= $NTU_DATE){
  //         $data = array ('BG_STATUS' => 18);
  //         $where['BG_REG_NUMBER = ?'] = $row['BG_REG_NUMBER'];
  //         $this->_db->update('TEMP_BANK_GUARANTEE',$data,$where);

  //     }

  // }    
  // }
  // }

}
