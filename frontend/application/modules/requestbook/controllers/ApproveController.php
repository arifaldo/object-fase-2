<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';

class requestbook_ApproveController extends Application_Main
{
	

	public function indexAction()
	{

		$this->_helper->layout()->setLayout('newlayout');

		$filter_clear 		= $this->_getParam('clearfilter');
		
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr = $CustomerUser->getAccounts();
		$this->view->AccArr = $AccArr;
		
		//select m_user - begin
		$userData = $CustomerUser->getUser($this->_userIdLogin);

		$this->view->tokentype 		= $userData['TOKEN_TYPE'];
		$this->view->tokenIdUser 	= $userData['TOKEN_ID'];

		$tokenType 					= $userData['TOKEN_TYPE'];
		$tokenIdUser 				= $userData['TOKEN_ID'];

		$userMobilePhone 			= trim($userData['USER_MOBILE_PHONE']);

		//added new hard token
		$HardToken 						= new Service_Token($this->_custIdLogin, $this->_userIdLogin, $tokenIdUser);
		$challengeCode 					= $HardToken->generateChallengeCode();
		$this->view->challengeCode 		= $challengeCode;
		$this->view->challengeCodeReq 	= $challengeCode;
		//select m_user - end
		
		/*$transstatus = $this->_transferstatus;
		$transstatuscode = array_flip($transstatus['code']);
		$statusarr = array_combine(array_values($this->_transferstatus['code']),array_values($this->_transferstatus['desc']));
		
		$paymenttype = $this->_paymenttype;

		$arrPayType  = Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
		$this->view->arrPayType = $arrPayType;
		$typearr = array_combine(array_values($this->_paymenttype['code']),array_values($this->_paymenttype['desc']));
		
		$tratypearr = array_combine(array_values($this->_transfertype['code']),array_values($this->_transfertype['desc']));
		
		$select = $this->_db->select()->distinct()
			->from(array('A' => 'M_CUSTOMER'),
				array('CUST_ID'))
				->order('CUST_ID ASC')
				 -> query() ->fetchAll();
		//Zend_Debug::dump($arrPayType); die;
		
		$this->view->var=$select;*/
		
		$fields = array(
						'PS_NUMBER'      	=> array('field' => 'PS_NUMBER',
													'label' => $this->language->_('Payment Ref').'#',
													'sortable' => true),
						'PS_REQUESTED'      	=> array('field' => 'PS_REQUESTED',
											      'label' => $this->language->_('Date-Time Request'),
											      'sortable' => true),
						'PS_SUBJECT'  	=> array('field' => 'PS_SUBJECT',
													'label' => $this->language->_('Subject'),
													'sortable' => true),
						'SOURCE_ACCOUNT'  	=> array('field' => 'SOURCE_ACCOUNT',
													'label' => $this->language->_('Source Account'),
													'sortable' => true),
						'PRODUCT_TYPE'     		=> array('field' => 'PRODUCT_TYPE',
											      'label' => $this->language->_('Product Type'),
											      'sortable' => true),
						'NO_OF_SHEETS'  	=> array('field' => 'NO_OF_SHEETS',
													'label' => $this->language->_('Number of Sheets'),
													'sortable' => true),
						'ADMIN_FEE'  	=> array('field' => 'ADMIN_FEE',
											      'label' => $this->language->_('Admin Fee'),
											      'sortable' => true),						
						'PS_REQUESTBY'  	=> array('field' => 'PS_REQUESTBY',
													'label' => $this->language->_('Request By'),
													'sortable' => true),
						
				      );

		$filterlist = array('PAY_REFF','SOURCE_ACCOUNT','PS_EFDATE','PRODUCT_TYPE','PS_STATUS');

		$this->view->filterlist = $filterlist;
				      
		$page    = $this->_getParam('page');
		
		// $sortBy  = $this->_getParam('sortby','B.PS_UPDATED');
		$sortBy  = ($this->_getParam('sortby')) ?  $this->_getParam('sortby') : ('PS_UPDATED');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		
		// $sortDir = $this->_getParam('sortdir','desc');
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'desc';
		
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');
		
		$this->view->tokentype = '2';
		
		/*$caseType = "(CASE B.PS_TYPE ";
  		foreach($typearr as $key=>$val)
  		{
   			$caseType .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseType .= " END)";
  			
  		$caseStatus = "(CASE C.TRA_STATUS ";
  		foreach($statusarr as $key=>$val)
  		{
   			$caseStatus.= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseStatus .= " END)";
  			
  		$caseTraType = "(CASE C.TRANSFER_TYPE ";
  		foreach($tratypearr as $key=>$val)
  		{
   			$caseTraType.= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseTraType .= " END)";*/
		
		$select2 = $this->_db->select()
					        ->from('T_REQ_BOOK')
					        ->where('SUGGEST_STATUS = ?', 'WA')			        
					        ;

		$select2->where('CUST_ID = ? ', $this->_custIdLogin);
		
		
		$filterArr = array(
           'SOURCE_ACCOUNT'		=> array('StripTags','StringTrim','StringToUpper'),	                       
		   'PRODUCT_TYPE'	=> array('StripTags'),	                       
		   'PAY_REFF'			=> array('StripTags'),						   
		   'PS_EFDATE'		=> array('StripTags','StringTrim'),
		   'PS_EFDATE_END'		=> array('StripTags','StringTrim'),
          );
	                      
		$dataParam = array("SOURCE_ACCOUNT","PAY_REFF","PRODUCT_TYPE");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{
			// print_r($dtParam);die;
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}

			}
		}

		if(!empty($this->_request->getParam('efdate'))){
			$efdatearr = $this->_request->getParam('efdate');
			$dataParamValue['PS_EFDATE'] = $efdatearr[0];
			$dataParamValue['PS_EFDATE_END'] = $efdatearr[1];
		}
	                      
		$validator = array(                  
		   'SOURCE_ACCOUNT'	=> array(),	 
		   'PRODUCT_TYPE'	=> array(),	                       
		   'PAY_REFF'			=> array(),						   
		   'PS_EFDATE'		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
		   'PS_EFDATE_END'		=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
	      );
	   
	     $zf_filter 	= new Zend_Filter_Input($filterArr,$validator,$dataParamValue);

	     if ($zf_filter->isValid()) {
	     	$filter 		= TRUE;
	     }
	    
	     
	     $fPaymentReff 	= html_entity_decode($zf_filter->getEscaped('PAY_REFF'));
	     $AccArr 		= html_entity_decode($zf_filter->getEscaped('SOURCE_ACCOUNT'));		 
		 $producttype	= html_entity_decode($zf_filter->getEscaped('PRODUCT_TYPE'));		 
		 $datefrom 		= html_entity_decode($zf_filter->getEscaped('PS_EFDATE'));
		 $dateto 		= html_entity_decode($zf_filter->getEscaped('PS_EFDATE_END'));
		 
		 
		if($filter == null)
		{	
			$datefrom = (date("d/m/Y"));
			$dateto = (date("d/m/Y"));
			$this->view->fDateFrom  = (date("d/m/Y"));
			$this->view->fDateTo  = (date("d/m/Y"));
		}
		
		if($filter_clear == '1'){
			$this->view->fDateFrom  = '';
			$this->view->fDateTo  = '';
			$datefrom = '';
			$dateto = '';
			
		}
		
		if($filter == null || $filter ==TRUE)
		{
			$this->view->fDateFrom = $datefrom;
			$this->view->fDateTo = $dateto;	 
		if(!empty($datefrom))
	            {
	            	$FormatDate = new Zend_Date($datefrom, $this->_dateDisplayFormat);
					$datefrom  = $FormatDate->toString($this->_dateDBFormat);	
	            }
	            
	    if(!empty($dateto))
	            {
	            	$FormatDate = new Zend_Date($dateto, $this->_dateDisplayFormat);
					$dateto    = $FormatDate->toString($this->_dateDBFormat);
	            }
		
		if(!empty($datefrom) && empty($dateto))
	            $select2->where("DATE(PS_REQUESTED) >= ".$this->_db->quote($datefrom));
	            
	   	if(empty($datefrom) && !empty($dateto))
	            $select2->where("DATE(PS_REQUESTED) <= ".$this->_db->quote($dateto));
	            
	    if(!empty($datefrom) && !empty($dateto))
	            $select2->where("DATE(PS_REQUESTED) between ".$this->_db->quote($datefrom)." and ".$this->_db->quote($dateto));
		}

		
		if($filter == TRUE)
	    {		
	    	
	    	if($fPaymentReff!=null)
			{ 				
				$this->view->payReff = $fPaymentReff;
				$select2->where("PS_NUMBER LIKE ".$this->_db->quote($fPaymentReff));
			}
	    	
			if($AccArr != null)
			{
			   $this->view->ACCTSRC = $AccArr;
		       $select2->where("SOURCE_ACCOUNT LIKE ".$this->_db->quote($AccArr));
			}
		       
		    /*if($payment != null)
		    {
		    	$this->view->payment = $payment;
		       	$select2->where("PS_NUMBER LIKE ".$this->_db->quote('%'.$payment.'%'));
		    }*/
		    
		    if($producttype != null)
		    {
		       	$this->view->producttype = $producttype;
		       	//$fpaymentarr = explode(',',$producttype);
		        $select2->where("PRODUCT_TYPE IN (?)",$producttype);
		    }

			
		 //    if($status != null)
		 //    {
		 //    	$this->view->status = $status;
		 //    	//$status = 
		 //    	$select2->where("SUGGEST_STATUS LIKE ".$this->_db->quote($status));}
			// }
		}

		$select2->order($sortBy.' '.$sortDir);
		// echo $select2;die;
		//$arr = $this->_db->fetchAll($select2);
		//Zend_Debug::dump($arr);die;
		
		if($this->_request->isPost() )
		{
			
			$psNumberArr = $this->_getParam('change_id');
			
			//Zend_Debug::dump($this->_getParam('change_id')); die;
			
			if(count($psNumberArr) > 0)
			{			
			
				if($this->_getParam('submit')=='Approve')
				{	
				          
					foreach($psNumberArr as $key=>$row){						
						try 
						{
							
							$reqbook		= new requestbook_Model_Requestbook();
							$this->_db->beginTransaction();		
		
							$dataapprove =  array(					
													'USER_ID_LOGIN' => $this->_userIdLogin, 
													'CUST_ID_LOGIN' => $this->_custIdLogin, 
													'PS_NUMBER' => $row
												);							
							
							$update = $reqbook->approve($dataapprove);
							Application_Helper_General::writeLog('BEDA','Approve Request Book '.$content['PS_NUMBER']);	
							
							$this->_db->commit();							
							
						}
						catch(Exception $e) 
						{
							$this->_db->rollBack();
							
						}
				    }     
				    $this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
					$this->_redirect('/notification/success');					
					
					
				}elseif($this->_getParam('submit')=='Reject')
				{
					
					foreach($psNumberArr as $key=>$row){						
						try 
						{
							
							$reqbook		= new requestbook_Model_Requestbook();
							$this->_db->beginTransaction();		
		
							$datareject =  array(					
													'USER_ID_LOGIN' => $this->_userIdLogin, 
													'CUST_ID_LOGIN' => $this->_custIdLogin, 
													'PS_NUMBER' => $row
												);							
							
							$update = $reqbook->reject($datareject);
							Application_Helper_General::writeLog('BEDA','Approve Request Book '.$content['PS_NUMBER']);	
							
							$this->_db->commit();							
							
						}
						catch(Exception $e) 
						{
							$this->_db->rollBack();
							
						}
				    }     
				    $this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
					$this->_redirect('/notification/success');		
					
				}
			
			}else{
				$this->view->error = TRUE;
				$this->view->report_msg = $this->language->_('Error: Please select payment ref#');
			}
			
		}
		
		if($csv || $pdf || $this->_request->getParam('print'))
		{
			$arr = $this->_db->fetchAll($select2);
			foreach ($arr as $key => $value)
			{
				unset($arr[$key]['CUST_ID']);
				//echo $key;
				$arr[$key]["PS_REQUESTED"] = Application_Helper_General::convertDate($value["PS_REQUESTED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);

				$arr[$key]["SOURCE_ACCOUNT"] = $arr[$key]["SOURCE_ACCOUNT"]." [".$arr[$key]["SOURCE_ACCOUNT_CCY"]."] / ".$arr[$key]["SOURCE_ACCOUNT_NAME"];
				unset($arr[$key]["SOURCE_ACCOUNT_CCY"]);
				unset($arr[$key]["SOURCE_ACCOUNT_NAME"]);

				$arrProductType = array('1'=>'Cheque','2'=>'Bilyet Giro');
				$arr[$key]["PRODUCT_TYPE"] = $arrProductType[$arr[$key]["PRODUCT_TYPE"]];

				unset($arr[$key]['PS_APPROVEBY']);

				$requestby = $arr[$key]['PS_REQUESTBY'];
				unset($arr[$key]['PS_REQUESTBY']);

				$arr[$key]["ADMIN_FEE"] = "IDR ".Application_Helper_General::displayMoney($arr[$key]["ADMIN_FEE"]);
				unset($arr[$key]["SUGGEST_STATUS"]);

				$arr[$key]["PS_REQUESTBY"] = $requestby;

				
				// $arr[$key]["SOURCE_ACCOUNT_NAME"]= $value["SOURCE_ACCOUNT_NAME"];
				// $arr[$key]["BENEFICIARY_ACCOUNT_NAME"]= $value["BENEFICIARY_ACCOUNT_NAME"];
				// $arr[$key]["BENEFICIARY_ACCOUNT_CCY"]= $value["BENEFICIARY_ACCOUNT_CCY"].' / '.$value["TRA_AMOUNT"];
				// $arr[$key]["PS_TYPE"]= $this->language->_($value["PS_TYPE"]).' ('.$value["TRANSFER_TYPE"].')';
				
				// $amount = Application_Helper_General::displayMoney( $value['TRA_AMOUNT'] );
				
				// $updated = Application_Helper_General::convertDate($value["PS_UPDATED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
				// $efdate = Application_Helper_General::convertDate($value["PS_EFDATE"],$this->view->viewDateFormat,$this->view->defaultDateFormat);
				// $arr[$key]["AMOUNT_EQ"]= $amounteq;
				
				// $status = $this->language->_($value["TRA_STATUS"]);
				
				// $pstype = $this->language->_($value["PS_TYPE"]);
				
				// $arr[$key]["PS_UPDATED"] = $updated;
				// $arr[$key]["PS_EFDATE"] = $efdate;
				// $arr[$key]["TRA_STATUS"] = $status;
				// $arr[$key]["PS_TYPE"] = $pstype;
				
			}
// 			echo "<pre>";
// 			print_r($arr);die;
			$header = Application_Helper_Array::simpleArray($fields, 'label');
			//Zend_Debug::dump($arr);die;
			if($csv)
			{
				Application_Helper_General::writeLog('DTRX','Download CSV Transaction Report');
				//Zend_Debug::dump($arr);die;
				//array('Created Date','Last Updated Date','Payment Date', 'Payment Ref#', 'Source Account', 'Source Account Name / Alias', 'Beneficiary Account', 'Beneficiary Account Name / Alias', 'CCY / Amount', 'Transaction ID', 'Message', 'Additional Message', 'Status', 'Payment Type')
				$this->_helper->download->csv($header,$arr,null,'Executed Transaction');
			}
			
			if($pdf)
			{
				Application_Helper_General::writeLog('DTRX','Download PDF Transaction Report');
				//array('Created Date','Last Updated Date','Payment Date', 'Payment Ref#', 'Source Account', 'Source Account Name / Alias', 'Beneficiary Account', 'Beneficiary Account Name / Alias', 'CCY / Amount', 'Transaction ID', 'Message', 'Additional Message', 'Status', 'Payment Type')
				$this->_helper->download->pdf($header,$arr,null,'Executed Transaction');
			}
			if($this->_request->getParam('print') == 1){
				
	                $this->_forward('print', 'index', 'widget', array('data_content' => $arr, 'data_caption' => 'Executed Transaction', 'data_header' => $fields));
	        }
		}
		else
		{
			Application_Helper_General::writeLog('DTRX','View Transaction Report');
		}
// 		echo '<pre>';
// 		print_r($select2->query());die;
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		$this->paging($select2);

		if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     print_r($whereval);die;
      }
	}
}