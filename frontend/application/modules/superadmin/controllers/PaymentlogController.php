<?php

class superadmin_PaymentlogController extends Zend_Controller_Action 
{
	
	public function init()
	{
		$this->_helper->layout()->setLayout('layout_superadmin');
		$this->_db 	= Zend_Db_Table::getDefaultAdapter();
		$objSessionNamespace = new Zend_Session_Namespace( 'sysadmin' );
		
		if(isset($objSessionNamespace->userIdLogin) && isset($objSessionNamespace->userNameLogin))
		{
			$suser_id	= $objSessionNamespace->userIdLogin;
			$this->view->userId = $suser_id;
		}
		else
		{
			$this->_redirect('/superadmin');
		}
	}
	
	public function indexAction() 
	{

		$arrPayType = array(
										1 =>'In House',
										2 =>'Domestic',
										4 =>'Multi Credit',
										5 =>'Multi Debit',
										6 =>'Multi Credit - Import',
										7 =>'Multi Debit - Import',
									);
		
		$arrPayStatus = array(
									'1'=>'Waiting for Approval',
									'2'=>'Waiting to Release',
									'3'=>'Request Repair',
									'4'=>'Rejected',
									'5'=>'Completed',
									
									'6'=>'Transfer Failed',
									'7'=>'Pending Future Date',
									'8'=>'In Progress',
									'9'=>'Exception',
									'10'=>'Pending Bulk Payment (FutureDate)',
									
									'13'=>'Pending Process',
									'14'=>'Cancel Future Date',
									'15'=>'Deleted',
									'16'=>'Waiting Bank Approval',
								);

		$fields = array	(	
							'PSNUMBER'  	   	=> 	array	(	
															'field'    => 'PSNUMBER',
															'label'    => 'PAYMENT REF#',
															'sortable' => false
														),
							'PSTYPE' 		=> 	array	(	
															'field'    => 'PSTYPE',
															'label'    => 'PAYMENT TYPE',
															'sortable' => false
														),
							'PSSTATUS'   => 	array	(	
															'field'    => 'PSSTATUS',
															'label'    => 'PAYMENT STATUS',
															'sortable' => false
														),
							'SENDFILE_STATUS'   => 	array	(	
															'field'    => 'SENDFILE_STATUS',
															'label'    => 'SENDFILE STATUS',
															'sortable' => false
														)
						);
						
		$this->view->fields = $fields;
		
		// if POST value not null, get post, else get param
		$dataParam = array(	"PSNUMBER","LIMIT","PSTYPE","PSSTATUS");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{
			$dataPost = $this->_request->getPost($dtParam);
			$dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}
		
		$filterArr = array(
								'PSNUMBER' 	=> array('StripTags','StringTrim'),
								'LIMIT'    	=> array('StripTags','StringTrim'),
								'PSTYPE' 	=> array('StripTags'),
								'PSSTATUS' 	=> array('StripTags'),
							);
		
		// The default is set so all fields allow an empty string	
		$options = array('allowEmpty' => true);
		$validators = array(
		
						'PSNUMBER' 	=> array(),					
						'LIMIT' 	=> array(),					
						'PSTYPE' 	=> array(array('InArray', array('haystack' => array_keys($arrPayType)))),							
						'PSSTATUS' 	=> array(array('InArray', array('haystack' => array_keys($arrPayStatus)))),		
						
						);
		
		// $zf_filter 	= new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		$zf_filter  	= new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);
		
		
		$fPSNUMBER 		= html_entity_decode($zf_filter->getEscaped('PSNUMBER'));
		$fLIMIT 		= html_entity_decode($zf_filter->getEscaped('LIMIT'));
		$fPSTYPE 		= html_entity_decode($zf_filter->getEscaped('PSTYPE'));
		$fPSSTATUS 		= html_entity_decode($zf_filter->getEscaped('PSSTATUS'));
	
		
		$casePayType = "(CASE TP.PS_TYPE ";
  		foreach($arrPayType as $key => $val)	{ $casePayType .= " WHEN ".$key." THEN '".$val."'"; }
  		$casePayType .= " END)";
		
		$casePayStatus = "(CASE TP.PS_STATUS ";
  		foreach($arrPayStatus as $key => $val)	{ $casePayStatus .= " WHEN ".$key." THEN '".$val."'"; }
  		$casePayStatus .= " END)";
	
		/*
			
			multicredit = 6 -> Multi Credit
			multidebet  = 7 -> Multi Debet
			bulkcredit  = 4 -> Multi Credit - Import
			bulkdebet  	= 5 -> Multi Debet - Import
		
		*/	
			
			
		$select = $this->_db->select()
							->from(
									array(	'TP'				=>'T_PSLIP'),
									array(
											'PSNUMBER'			=>'TP.PS_NUMBER',
											'PSTYPE'			=> $casePayType,
											'PSSTATUS'			=> $casePayStatus,
											'SENDFILE_STATUS'	=>'TP.SENDFILE_STATUS',
										)
									)
							->where("TP.PS_TYPE IN ('1','2','6','7','4','5')");
							
		if($fLIMIT) $select->limit($fLIMIT,1);
		else 		{
			$select->limit(50,1);
			$fLIMIT = 50;
		}
		
		if($fPSNUMBER){
			$select->where("TP.PS_NUMBER = ".$this->_db->quote($fPSNUMBER));
		}
		if($fPSTYPE){
			$select->where("TP.PS_TYPE = ".$this->_db->quote($fPSTYPE));
		}
		if($fPSSTATUS){
			$select->where("TP.PS_STATUS = ".$this->_db->quote($fPSSTATUS));
		}
		
		$data = $this->_db->fetchAll($select);
		
		
		
		$this->view->data 			= $data;
		$this->view->fPSNUMBER 		= $fPSNUMBER;
		$this->view->fLIMIT 		= $fLIMIT;
		$this->view->fPSTYPE 		= $fPSTYPE;
		$this->view->fPSSTATUS 		= $fPSSTATUS;
		
		$this->view->arrPayType 	= $arrPayType;
		$this->view->arrPayStatus 	= $arrPayStatus;
	}
}