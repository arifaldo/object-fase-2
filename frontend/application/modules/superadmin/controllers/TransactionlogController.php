<?php

class superadmin_TransactionlogController extends Zend_Controller_Action 
{
	
	public function init()
	{
		$this->_helper->layout()->setLayout('layout_superadmin');
		$this->_db 	= Zend_Db_Table::getDefaultAdapter();
		$objSessionNamespace = new Zend_Session_Namespace( 'sysadmin' );
		
		if(isset($objSessionNamespace->userIdLogin) && isset($objSessionNamespace->userNameLogin))
		{
			$suser_id	= $objSessionNamespace->userIdLogin;
			$this->view->userId = $suser_id;
		}
		else
		{
			$this->_redirect('/superadmin');
		}
	}
	
	public function indexAction() 
	{
		$select = $this->_db->select()->distinct()
										->from(
													array('T_INTERFACE_LOG'),
													array(
															'FUNCTION_NAME',
														)
												);
		$arrfunction 				= $this->_db->fetchAll($select);
		$this->view->arrfunction 	= $arrfunction;
		
		$fields = array	(	
							'FUNCTION_NAME'  	   	=> 	array	(	
															'field'    => 'FUNCTION_NAME',
															'label'    => 'FUNCTION_NAME',
															'sortable' => false
														),
							'STRING'   => 	array	(	
															'field'    => 'STRING',
															'label'    => 'STRING',
															'sortable' => false
														),
							'UID' 		=> 	array	(	
															'field'    => 'UID',
															'label'    => 'UID',
															'sortable' => false
														),
							'LOG_DATE'   => 	array	(	
															'field'    => 'LOG_DATE',
															'label'    => 'LOG_DATE',
															'sortable' => false
														),
							'ACTION'   => 	array	(	
															'field'    => 'ACTION',
															'label'    => 'ACTION',
															'sortable' => false
														)
						);
						
		$this->view->fields = $fields;

		//$select->order($sortBy.' '.$sortDir);
		$filterArr = array(
								'filter' 			=> array('StripTags','StringTrim'),
								'FUNCTION_NAME' 	=> array('StripTags','StringTrim'),
								'STRING'    		=> array('StripTags','StringTrim'),
								'UID' 				=> array('StripTags','StringTrim'),
								'LOG_DATE'  		=> array('StripTags','StringTrim'),
								'limit'  			=> array('StripTags','StringTrim'),
								'sort'  			=> array('StripTags','StringTrim'),
							);
		$zf_filter = new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		
		//Zend_Debug::dump($this->_request->getParams());
		//die;
		if($zf_filter->getEscaped('filter')=='Set Filter')
		{
			$select = $this->_db->select()->from(
													array('T_INTERFACE_LOG'),
													array(
															'LOG_DATE',
															'FUNCTION_NAME',
															'UID',
															'ACTION',
															'STRING',
															'ERR_CODE',
															'ERR_MESSAGE',
															'INTERFACE_TO',
														)
												);
											//->limit(500,1);
											//->limit(1,1);
			if($zf_filter->getEscaped('limit'))
			{
				$select->limit($zf_filter->getEscaped('limit'),1);
				$this->view->FUNCTION_NAME = $zf_filter->getEscaped('FUNCTION_NAME');
			}
			else
			{
				$select->limit(50,1);
			}			
			
			if($zf_filter->getEscaped('FUNCTION_NAME'))
			{
				$select->where('FUNCTION_NAME LIKE '.$this->_db->quote('%'.$zf_filter->getEscaped('FUNCTION_NAME').'%'));
				$this->view->FUNCTION_NAME = $zf_filter->getEscaped('FUNCTION_NAME');
			}
			if($zf_filter->getEscaped('STRING'))
			{
				$select->where('STRING LIKE '.$this->_db->quote('%'.$zf_filter->getEscaped('STRING').'%'));
				$this->view->fSTRING = $zf_filter->getEscaped('STRING');
			}
			if($zf_filter->getEscaped('UID'))
			{
				$select->where('UID LIKE '.$this->_db->quote('%'.$zf_filter->getEscaped('UID').'%'));
				$this->view->UID = $zf_filter->getEscaped('UID');
			}
			
			if($zf_filter->getEscaped('LOG_DATE'))
			{
				$from   = 	(Zend_Date::isDate($zf_filter->getEscaped('LOG_DATE'),"dd/MM/yyyy"))?new Zend_Date($zf_filter->getEscaped('from'),$this->_dateDisplayFormat):false;
				
				if($from)
				{
					$select->where('DATE(LOG_DATE) >= '.$this->_db->quote($from));
				}
				$this->view->LOG_DATE = $zf_filter->getEscaped('LOG_DATE');
			}
			if($zf_filter->getEscaped('sortdate'))
			{
				$sortBy = 'LOG_DATE';
				$sortDir = $zf_filter->getEscaped('sortdate');
				$select->order($sortBy.' '.$sortDir);
				$this->view->sortdate = $zf_filter->getEscaped('sortdate');
			}
			
			if($zf_filter->getEscaped('sort'))
			{
				$sortBy = 'UID';
				
				$sortDir = 'Desc';
				if($zf_filter->getEscaped('sort') == '1')
				{
					$sortDir = 'Asc';
				}
				
				$select->order($sortBy.' '.$sortDir);
				$this->view->sort = $zf_filter->getEscaped('sort');
			}

			$arr = $this->_db->fetchAll($select);
			

			$this->view->arr = $arr;
		}
		
	}
}