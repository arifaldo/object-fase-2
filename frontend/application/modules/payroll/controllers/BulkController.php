<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/BulkPayment.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'General/CustomerUser.php';
require_once 'General/Customer.php';
require_once 'General/Settings.php';
require_once 'General/Charges.php';
require_once 'CMD/Payment.php';

class payroll_BulkController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti

	protected $_destinationUploadDir = '';
	protected $_maxRow = '';

	public function initController()
	{
		$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';

		$setting = new Settings();
		$this->_maxRow = $setting->getSetting('max_import_bulk');
	}

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$this->setbackURL();

		$settingObj = new Settings();
		$this->view->THRESHOLD_LLD		= $settingObj->getSetting("threshold_lld"	, 0);

		$this->view->PSEFDATE = Application_Helper_General::convertDate($this->getCurrentDate());
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr 	  = $CustomerUser->getAccounts();
		$this->view->AccArr =  $AccArr;
		$listAccValidate = Application_Helper_Array::simpleArray($AccArr,'ACCT_NO');

		if($this->_request->isPost() )
		{
		    $this->_request->getParams();
			$data= 		    		    $this->_request->getParams();
// print_r($data);die;


			$filter = new Application_Filtering();
			$confirm = false;
			$error_msg[0] = "";

			$PS_SUBJECT 	= $filter->filter($this->_request->getParam('PSSUBJECT'), "PS_SUBJECT");
			$PS_EFDATE 		= $filter->filter($this->_request->getParam('PSEFDATE'), "PS_DATE");
			$ACCTSRC 		= $filter->filter($this->_request->getParam('ACCTSRC'), "ACCOUNT_NO");

			if(!$ACCTSRC)
			{
				$error_msg[0] = $this->language->_('Error').': '.$this->language->_('Source Account cannot be left blank.').'';
				$this->view->error 		= true;
				$this->view->report_msg	= $this->displayError($error_msg);
			}
			else if(!$PS_EFDATE)
			{
				$error_msg[0] = 'Error: Payment Date can not be left blank.';
				$this->view->error 		= true;
				$this->view->report_msg	= $this->displayError($error_msg);
			}
			else
			{
				$paramSettingID = array('range_futuredate', 'auto_release_payment');

				$settings = new Application_Settings();
				$settings->setSettings(null, $paramSettingID);       // Zend_Registry => 'APPSETTINGS'
				$ccyList = $settings->setCurrencyRegistered();    // Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
				$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
				$AccArr = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR")));	// show acc in IDR only

				$adapter = new Zend_File_Transfer_Adapter_Http();

				$adapter->setDestination ( $this->_destinationUploadDir );
				$extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv'));
				$extensionValidator->setMessage(
					$this->language->_('Error').': '.$this->language->_('Extension file must be').' *.csv'
				);

				$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
				$sizeValidator->setMessage(
					'Error: File exceeds maximum size'
				);

				$adapter->setValidators ( array (
					$extensionValidator,
					$sizeValidator,
				));

				if ($adapter->isValid ())
				{
// die;
					$sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
					$newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

					$adapter->addFilter ( 'Rename',$newFileName  );

					if ($adapter->receive ())
					{
						//PARSING CSV HERE
						$csvData = $this->parseCSV($newFileName);
						$csvData2 = $this->parseCSV($newFileName);
						//after parse delete document temporary
						Zend_Debug::dump($csvData);die;
						@unlink($newFileName);
						//end

						$totalRecords2 = count($csvData2);

						if($totalRecords2)
							{
								for ($a= 1; $a<$totalRecords2; $a++ ){
									unset($csvData2[$a]);
								}
//								unset($csvData2[1]);
//								unset($csvData2[2]);
//								unset($csvData2[3]);
								$totalRecords2 = count($csvData2);
						}


						foreach ( $csvData2 as $row )
						{
							$type =  trim($row[0]);
						}
// 						print_r($type);d

						if (strtoupper(trim($type)) == 'PAYROLL'){
//							echo 'berhasil';
//	die;

							$totalRecords = count($csvData);
							if($totalRecords)
							{
								unset($csvData[0]);
								unset($csvData[1]);
								$totalRecords = count($csvData);
							}
							//Zend_Debug::dump($csvData);die;
							if($totalRecords)
							{
								if($totalRecords <= $this->_maxRow)
								{
									$rowNum = 0;

									$paramPayment = array( 	"CATEGORY"      	=> "BULK PAYROLL",
															"FROM"       		=> "I",
															"PS_NUMBER"     	=> "",
															"PS_SUBJECT"   	 	=> $PS_SUBJECT,
															"PS_EFDATE"     	=> $PS_EFDATE,
															"_dateFormat"    	=> $this->_dateDisplayFormat,
															"_dateDBFormat"    	=> $this->_dateDBFormat,
															"_addBeneficiary"   => $this->view->hasPrivilege('BADA'), // privi BADA (Add Beneficiary)
															"_beneLinkage"    	=> $this->view->hasPrivilege('BLBU'), // privi BLBU (Linkage Beneficiary User)
															"_createPB"     	=> $this->view->hasPrivilege('CBPW'), // privi CBPW (Create Bulk Credit Payment PB by Import File (CSV))
															"_createDOM"    	=> $this->view->hasPrivilege('CBPI'), // privi CBPI (Create Bulk Credit Payment Domestic by Import File (CSV))
															"_createREM"    	=> false,        // cannot create REM trx
														  );

									$paramTrxArr = array();
									//Zend_Debug::dump($csvData); die;
									foreach ( $csvData as $row )
									{

										if(count($row)==5)
										{
											$rowNum++;
											$benefAcct 		= trim($row[0]);
// 											$benefName 		= trim($row[1]);
											$ccy 			= strtoupper(trim($row[1]));
											$amount 		= trim($row[2]);
											$message 		= trim($row[3]);
											$addMessage 	= trim($row[4]);
// 											$email 			= trim($row[6]);
// 											$phoneNumber	= trim($row[7]);
											$type 			= 'PB';
// 											$bankCode 		= trim($row[6]);
											//$bankName 		= trim($row[10]);
	//										$bankCity = trim($row[10]);
// 											$benefAdd		= trim($row[9]);
// 											$citizenship	= strtoupper(trim($row[10]));
											//$resident = strtoupper(trim($row[11]));

											/*
											 * Change parameter into document
											 */
											$fullDesc = array(
												'BENEFICIARY_ACCOUNT' 		=> $benefAcct,
												'BENEFICIARY_NAME' 			=> '',
												'BENEFICIARY_ACCOUNT_CCY' 	=> $ccy,
												'TRA_AMOUNT' 				=> $amount,
												'TRA_MESSAGE'				=> $message,
												'REFNO' 					=> $addMessage,
												'BENEFICIARY_EMAIL' 		=> '',
												'BENEFICIARY_MOBILE_PHONE_NUMBER' => '',
												'TRANSFER_TYPE' 			=> 'PB',
												//'CLR_CODE' 					=> $bankCode,
												//'BENEFICIARY_BANK_NAME' 	=> $bankName,
	//											'BENEFICIARY_CITY' => $bankCity,
												'BENEFICIARY_ADDRESS'		=> '',
												'BENEFICIARY_CITIZENSHIP' 	=> ''
												//'BENEFICIARY_RESIDENT' => $resident
											);

											$filter = new Application_Filtering();

											$TRA_AMOUNT 		= $filter->filter($amount, "AMOUNT");
											$TRA_MESSAGE 		= $filter->filter($message, "TRA_MESSAGE");
											$TRA_REFNO 			= $filter->filter($addMessage, "TRA_REFNO");
											$ACBENEF 			= $filter->filter($benefAcct, "ACCOUNT_NO");
											$ACBENEF_BANKNAME 	= $filter->filter('', "ACCOUNT_NAME");
											$ACBENEF_ALIAS 		= $filter->filter('', "ACCOUNT_ALIAS");
											$ACBENEF_EMAIL 		= $filter->filter('', "EMAIL");
											$ACBENEF_PHONE 		= $filter->filter('', "MOBILE_PHONE_NUMBER");
											$ACBENEF_CCY 		= $filter->filter($ccy, "SELECTION");
											$ACBENEF_ADDRESS	= $filter->filter('', "ADDRESS");
											$ACBENEF_CITIZENSHIP= $filter->filter('', "SELECTION");
											//$ACBENEF_RESIDENT= $filter->filter($resident, "SELECTION");
											//$BANK_NAME			= $filter->filter($bankName, "BANK_NAME");
											//$BANK_CITY			= $filter->filter($bankCity, "ADDRESS");
											//$CLR_CODE			= $filter->filter($bankCode, "BANK_CODE");
											$TRANSFER_TYPE 		= $filter->filter($type, "SELECTION");

											$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

											if($TRANSFER_TYPE == 'RTGS'){
												$chargeType = '1';
												$select = $this->_db->select()
																->from('M_CHARGES_OTHER',array('*'))
																->where("CUST_ID = ?",$this->_custIdLogin)
																->where("CHARGES_TYPE = ?",$chargeType);
												$resultSelecet = $this->_db->FetchAll($select);
												$chargeAmt = $resultSelecet['0']['CHARGES_AMT'];
											}
											else if($TRANSFER_TYPE == 'SKN'){
												$chargeType1 = '2';
												$select1 = $this->_db->select()
																->from('M_CHARGES_OTHER',array('*'))
																->where("CUST_ID = ?",$this->_custIdLogin)
																->where("CHARGES_TYPE = ?",$chargeType1);
												$resultSelecet1 = $this->_db->FetchAll($select1);
												$chargeAmt = $resultSelecet1['0']['CHARGES_AMT'];
											}
											else{
												$chargeAmt = '0';
											}


											$filter->__destruct();
											unset($filter);

											$paramTrx = array("TRANSFER_TYPE" 			=> $TRANSFER_TYPE,
																"TRA_AMOUNT" 				=> $TRA_AMOUNT_num,
																"TRANSFER_FEE" 				=> $chargeAmt,
																"TRA_MESSAGE" 				=> $TRA_MESSAGE,
																"TRA_REFNO" 				=> $TRA_REFNO,
																"ACCTSRC" 					=> $ACCTSRC,
																"ACBENEF" 					=> $ACBENEF,
																"ACBENEF_CCY" 				=> $ACBENEF_CCY,
																"ACBENEF_EMAIL" 			=> $ACBENEF_EMAIL,
																"ACBENEF_PHONE" 			=> $ACBENEF_PHONE,

															// for Beneficiary data, except (bene CCY and email), must be passed by reference
																"ACBENEF_BANKNAME" 			=> $ACBENEF_BANKNAME,
																"ACBENEF_ALIAS" 			=> $ACBENEF_ALIAS,
																"ACBENEF_CITIZENSHIP" 		=> $ACBENEF_CITIZENSHIP,		// W: WNI, N: WNA
															//	"ACBENEF_RESIDENT" 			=> $ACBENEF_RESIDENT,		// R: RESIDENT, NR: NON-RESIDENT
																"ACBENEF_ADDRESS1" 			=> $ACBENEF_ADDRESS,
															//	"ACBENEF_ADDRESS2" 			=> &$ACBENEF_ADDRESS2,
															//	"ACBENEF_ADDRESS3" 			=> &$ACBENEF_ADDRESS3,

															//	"ORG_DIR" 					=> $ORG_DIR,
																//"BANK_CODE" 				=> $CLR_CODE,
															//	"BANK_NAME" 				=> $BANK_NAME,
															//	"BANK_BRANCH" 				=> $BANK_BRANCH,
															//	"BANK_ADDRESS1" 			=> $BANK_ADDRESS1,
															//	"BANK_ADDRESS2" 			=> $BANK_ADDRESS2,
															//	"BANK_ADDRESS3" 			=> $BANK_ADDRESS3,
															 );

											array_push($paramTrxArr,$paramTrx);
										}
										else
										{
											$error_msg[0] = $this->language->_('Error').': '.$this->language->_('Wrong File Formatvv').'.';
											$this->view->error 		= true;
											$this->view->report_msg	= $this->displayError($error_msg);
											break;
										}
									}
								}
								// kalo jumlah trx lebih dari setting
								else
								{
									$error_msg[0] = 'Error: The number of rows to be imported should not more than '.$this->_maxRow.'.';
									$this->view->error 		= true;
									$this->view->report_msg	= $this->displayError($error_msg);
								}

								// kalo gak ada error
								if(!$error_msg[0])
								{

									$validate   = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin);
									$resultVal	= $validate->checkCreatePayroll($paramPayment, $paramTrxArr);

									$payment 		= $validate->getPaymentInfo();

// 									Zend_Debug::dump($validate->getErrorMsg(),'err');
// 									Zend_Debug::dump($validate->getErrorTrxMsg(),'errT');
// 									die('asd');

									if($validate->isError() === false)	// payment data is valid
									{
										$confirm = true;

										$validate->__destruct();
										unset($validate);
									}
									else
									{
										$errorMsg 		= $validate->getErrorMsg();
										$errorTrxMsg 	= $validate->getErrorTrxMsg();	// array

										$validate->__destruct();

										unset($validate);
										//Zend_Debug::dump($validate);die;
										if($errorMsg)
										{
											$error_msg[0] = 'Error: '.$errorMsg;
											$this->view->error 		= true;
											$this->view->report_msg	= $this->displayError($error_msg);
										}
										else
										{
											$confirm = true;
										}
									}
								}
							}
							else //kalo total record = 0
							{
// 								echo 'here';die;
								//$error_msg[0] = 'Error: Wrong File Format. There is no data on csv File.';
								$error_msg[0] = $this->language->_('Error').': '.$this->language->_('Wrong File Formatss').'.';
								$this->view->error 		= true;
								$this->view->report_msg	= $this->displayError($error_msg);
							}
						} else { //kalo csv bukan payroll
							//echo 'gagal';
// 							echo 'here1';die;
							$error_msg[0] = $this->language->_('Error').': '.$this->language->_('Wrong File Formatssss').'.';
							$this->view->error 		= true;
							$this->view->report_msg	= $this->displayError($error_msg);
						}
					}

				}
				else
				{
					$this->view->error = true;
					foreach($adapter->getMessages() as $key=>$val)
					{
						if($key=='fileUploadErrorNoFile')
							$error_msg[0] = $this->language->_('Error').': '.$this->language->_('File cannot be left blank. Please correct it').'.';
						else
							$error_msg[0] = $val;
						break;
					}
					$errors = $this->displayError($error_msg);
					$this->view->report_msg = $errors;
				}
			}

			if($confirm)
			{

				$content['paramPayment'] = $paramPayment;
				$content['paramTrxArr'] = $paramTrxArr;
				$content['errorTrxMsg'] = $errorTrxMsg;
				$content['payment'] = $payment;
				//echo '<pre>';
				//print_r($payment);die;
				$sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
				$sessionNamespace->content = $content;
				$this->_redirect('/payroll/bulk/confirm');
			}

			$this->view->PSSUBJECT = $PS_SUBJECT;
			$this->view->ACCTSRC = $ACCTSRC;
			$this->view->PSEFDATE = $PS_EFDATE;
		}
		Application_Helper_General::writeLog('CBPI','Viewing Create Bulk Credit Payment Domestic by Import File (CSV)');
		Application_Helper_General::writeLog('CBPW','Viewing Create Bulk Credit Payment In House by Import File (CSV)');
	}

	public function confirmAction()
	{

		$this->_helper->layout()->setLayout('newlayout');
		$sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
		$data = $sessionNamespace->content;
		//Zend_Debug::dump($data['paramTrxArr']);

		if($this->_custSameUser){
			// echo 'here1';
			$this->view->token = true;
			$userOnBehalf = $this->_userIdLogin;
			$tokenIdUser = $this->_db->select()
			->from(
				array('M_USER'),
				array('TOKEN_ID')
			)
			->where('USER_ID = ?',$userOnBehalf)
			->where('CUST_ID = ?',$this->_custIdLogin)
			->limit(1)
		;

		$tokenIdUser = $this->_db->fetchRow($tokenIdUser);
		$tokenIdUser = $tokenIdUser['TOKEN_ID'];

			$Token 			= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
			$challengeCode 	= $Token->generateChallengeCode();
			$this->view->userOnBehalf		= $userOnBehalf;
			$this->view->challengeCode		= $challengeCode;
		}

		$sourceAcct = $data['payment']['acctsrcArr'][$data['paramTrxArr'][0]['ACCTSRC']];

		$this->view->CCY = $sourceAcct['CCY_ID'];
		$this->view->PS_SUBJECT = $data['paramPayment']['PS_SUBJECT'];
		$this->view->PS_EFDATE = $data['paramPayment']['PS_EFDATE'];
		$this->view->ACCTSRC = $data['paramTrxArr'][0]['ACCTSRC'].' / '.$sourceAcct['ACCT_NAME'].' / '.$sourceAcct['CCY_ID'];
		if($sourceAcct['ACCT_ALIAS']) $this->view->ACCTSRC .= ' / '.$sourceAcct['ACCT_ALIAS'];

		$this->view->countTrxCCY = $data["payment"]["countTrxCCY"];
		$this->view->sumTrxCCY = $data["payment"]["sumTrxCCY"];

		$chargesAmt = array();
		$totalChargesAmt = 0;

		foreach($data['paramTrxArr'] as $row)
		{
			if(!isset($chargesAmt[$row['TRANSFER_TYPE']][$row['ACBENEF_CCY']])) $chargesAmt[$row['TRANSFER_TYPE']][$row['ACBENEF_CCY']] = 0;
			//$chargesObj = Charges::factory($this->_custIdLogin, $row['TRANSFER_TYPE']);

//			if(empty($row['TRANSFER_TYPE'])){
//				$transferType = '0';
//			}
//			else{
//				$transferType = $row['TRANSFER_TYPE'];
//			}
			$chargesObj = Charges::factory($this->_custIdLogin, isset($row['TRANSFER_TYPE']));

			$paramCharges = array("accsrc" => $row['ACCTSRC'], "transferType" => $row['TRANSFER_TYPE']);
			if(!isset($data['errorTrxMsg'][$row['TRANSFER_TYPE']][$row['ACBENEF_CCY']]))
			{
// 				$chargesAmt[$row['TRANSFER_TYPE']][$row['ACBENEF_CCY']] += $chargesObj->getCharges($paramCharges);
// 				$totalChargesAmt += $chargesObj->getCharges($paramCharges);
			}
		}
		$this->view->chargesAmt = $chargesAmt;
		$this->view->totalChargesAmt = $totalChargesAmt;

		$totalSuccess = 0;
		foreach($data["payment"]["countTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$totalSuccess += $ccy['success'];
			}
		}

		$amountSuccess = 0;
		foreach($data["payment"]["sumTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$amountSuccess += $ccy['success'];
			}
		}

		$totalFailed = 0;
		//Zend_Debug::dump($data["payment"]["countTrxCCY"]);die;
		foreach($data["payment"]["countTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$totalFailed += $ccy['failed'];
			}
		}

		$amountFailed = 0;
		foreach($data["payment"]["sumTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$amountFailed += $ccy['failed'];
			}
		}

		$settings = new Settings();
		$this->view->cutOffSKN = $settings->getSetting('cut_off_time_skn');
		$this->view->cutOffRTGS = $settings->getSetting('cut_off_time_rtgs');

		$this->view->totalSuccess = $totalSuccess;
		$this->view->amountSuccess = $amountSuccess;
		$this->view->totalFailed = $totalFailed;
		$this->view->amountFailed = $amountFailed;

		if($this->_request->isPost() )
		{
			if($this->_getParam('submit_cancel') == TRUE)
			{
				unset($_SESSION['confirmBulkCredit']);
				$this->_redirect('/newmultibulk');
			}

			if ($data["payment"]["countTrxPB"] == 0)
				$priviCreate = 'CBPI';
			else
				$priviCreate = 'CBPW';

			$param['PS_SUBJECT'] = $data['paramPayment']['PS_SUBJECT'];
			$param['PS_EFDATE']  = Application_Helper_General::convertDate($data['paramPayment']['PS_EFDATE'], $this->_dateDBFormat, $this->_dateDisplayFormat);
			$param['PS_TYPE'] 	= $this->_paymenttype['code']['payroll'];
			$param['PS_CCY']  = $data['payment']['acctsrcArr'][$data['paramTrxArr'][0]['ACCTSRC']]['CCY_ID'];

			$param['TRANSACTION_DATA'] = array();
			foreach($data['paramTrxArr'] as $row)
			{
				$param['TRANSACTION_DATA'][] = array(
					'SOURCE_ACCOUNT' 					=> $row['ACCTSRC'],
					'BENEFICIARY_ACCOUNT' 				=> $row['ACBENEF'],
					'BENEFICIARY_ACCOUNT_CCY' 			=> $row['ACBENEF_CCY'],
					'BENEFICIARY_ACCOUNT_NAME' 			=> $row['ACBENEF_BANKNAME'],
					'BENEFICIARY_ALIAS_NAME' 			=> $row['ACBENEF_ALIAS'],
					'BENEFICIARY_EMAIL' 				=> $row['ACBENEF_EMAIL'],
					'BENEFICIARY_MOBILE_PHONE_NUMBER' 	=> $row['ACBENEF_PHONE'],
// 					'BENEFICIARY_ADDRESS' 				=> $row['ACBENEF_ADDRESS1'],
					'BENEFICIARY_CITIZENSHIP' 			=> $row['ACBENEF_CITIZENSHIP'],
					'BENEFICIARY_RESIDENT' 				=> $row['ACBENEF_RESIDENT'],
					'CLR_CODE' 							=> $row['BANK_CODE'],
					'TRANSFER_TYPE' 					=> $row['TRANSFER_TYPE'],
					'TRA_AMOUNT' 						=> $row['TRA_AMOUNT'],
					'TRANSFER_FEE' 						=> $row['TRANSFER_FEE'],
					'TRA_MESSAGE' 						=> $row['TRA_MESSAGE'],
					'TRA_REFNO' 						=> $row['TRA_REFNO'],
				);
			}

			$param['_addBeneficiary'] = $data['paramPayment']['_addBeneficiary'];
			$param['_beneLinkage'] = $data['paramPayment']['_beneLinkage'];
			$param['_priviCreate'] = $priviCreate;

			$BulkPayment = new BulkPayment("", $this->_custIdLogin, $this->_userIdLogin);

			$paymentRef = NULL;

			if($this->_custSameUser){
				
				if(!$this->view->hasPrivilege('PRLP')){
					// die('here');
					$error_msg[] = $this->language->_("Error: You don't have privilege to release payment");
					$this->view->error = true;
					$this->view->report_msg = $this->displayError($error_msg);

					$checktoken = false;

				}else{
					// die('sini');

					$challengeCode		= $this->_getParam('challengeCode');

					$inputtoken1 		= $this->_getParam('inputtoken1');
					$inputtoken2 		= $this->_getParam('inputtoken2');
					$inputtoken3 		= $this->_getParam('inputtoken3');
					$inputtoken4 		= $this->_getParam('inputtoken4');
					$inputtoken5 		= $this->_getParam('inputtoken5');
					$inputtoken6 		= $this->_getParam('inputtoken6');

					$responseCode		= $inputtoken1.$inputtoken2.$inputtoken3.$inputtoken4.$inputtoken5.$inputtoken6;

					$Token 		= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
					$verToken 	= $Token->verify($challengeCode, $responseCode);
					// print_r($verToken);
					// die('here');
					if ($verToken['ResponseCode'] != '00'){
						$tokenFailed = $CustUser->setLogToken(); //log token activity

						$this->view->error = true;
						$errorMsg[] = $this->language->_('Invalid Response Code');	//$verToken['ResponseDesc'];

						if ($tokenFailed === true)
						{
							$this->_redirect('/default/index/logout');
						}

						$checktoken = false;

					}else{

						$checktoken = true;
					}
				}
			} else{

				$checktoken = true;
			}


			if($checktoken){
			$result = $BulkPayment->createPayment($param,$paymentRef);
			// var_dump($result);
			// var_dump($paymentRef);die;

			if($this->_custSameUser){

											$paramSQL = array("WA" 				=> false,
															  "ACCOUNT_LIST" 	=> $this->_accountList,
															  "_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
															 );

											// get payment query
											$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
											$select   = $CustUser->getPayment($paramSQL);
											$select->where('P.PS_NUMBER = ?' , (string) $paymentRef);
											// echo $select;
											$pslip = $this->_db->fetchRow($select);
											$settingObj = new Settings();
											$setting = array("COT_SKN"  			=> $settingObj->getSetting("cut_off_time_skn", 		"00:00:00"),
															 "COT_RTGS" 			=> $settingObj->getSetting("cut_off_time_rtgs", 	"00:00:00"),
															 "COT_BULK" 			=> $settingObj->getSetting("cut_off_time_bulkpayments", 	"00:00:00"),
															 "COT_BI" 				=> $settingObj->getSetting("cut_off_time_bi", 	"00:00:00"),
															 "COT_REMIT"			=> $settingObj->getSetting("cut_off_time_remittance", "00:00:00"),
															 'start_remit'			=> $settingObj->getSetting("starting_time_remittance", "00:00:00"),
															 "range_futuredate" 	=> $settingObj->getSetting("range_futuredate", 		"0"),
															 "auto_release" 		=> $settingObj->getSetting("auto_release_payment",	"0"),
															 "_dateFormat" 			=> $this->_dateDisplayFormat,
															 "_dateDBFormat" 		=> $this->_dateDBDefaultFormat,
															 "_transfertype" 		=> array_flip($this->_transfertype["code"]),
															);

											$isBackDated  = ($pslip['efdate'] < date('Y-m-d'));
											$isFutureDate = ($pslip['efdate'] > date('Y-m-d'));

											$caseTransferType 	= Application_Helper_General::caseArray($this->_transfertype);
											$caseTransferStatus = Application_Helper_General::caseArray($this->_transferstatus);

											$app = Zend_Registry::get('config');
											$appBankname = $app['app']['bankname'];

											$selectTrx = $this->_db->select()
											  ->from(	array(	'TT' => 'T_TRANSACTION'),
														array(
																'ACCTSRC'				=> 'TT.SOURCE_ACCOUNT',
																'ACCTSRC_CCY'			=> 'TT.SOURCE_ACCOUNT_CCY',
																'ACCTSRC_NAME'			=> new Zend_Db_Expr("
																							CASE WHEN TT.SOURCE_ACCOUNT_ALIAS_NAME is null THEN TT.SOURCE_ACCOUNT_NAME
																								 ELSE CONCAT(TT.SOURCE_ACCOUNT_NAME )
																							END"),
																//'ACCTSRC_ALIAS'		=> 'TT.SOURCE_ACCOUNT_ALIAS_NAME',
																'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
																'ACBENEF_NAME'			=> new Zend_Db_Expr("
																								CONCAT(TT.BENEFICIARY_ACCOUNT_NAME )"),
																'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
																'ACBENEF_EMAIL'			=> 'TT.BENEFICIARY_EMAIL',
																'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
																'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',
																'TRA_REFNO'				=> 'TT.TRA_REFNO',
																'TRANSFER_TYPE'			=> 'TT.TRANSFER_TYPE',
																'TRANSFER_TYPE_disp'	=> new Zend_Db_Expr("CASE TT.TRANSFER_TYPE $caseTransferType ELSE 'N/A' END"),
																'TRA_STATUS'			=> 'TT.TRA_STATUS',
																'TRA_STATUS_disp'		=> new Zend_Db_Expr("CASE TT.TRA_STATUS $caseTransferStatus ELSE 'N/A' END"),
																'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
																'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
																'TRA_REMAIN'			=> 'TT.TRA_REMAIN',
																'EQUIVALENT_AMOUNT_IDR'	=> 'TT.EQUIVALENT_AMOUNT_IDR',
																'CLR_CODE'				=> 'TT.CLR_CODE',
																'TT.RATE',
																'TT.PROVISION_FEE',
																'TT.NOSTRO_NAME',
																'TT.FULL_AMOUNT_FEE',
																'C.PS_CCY','C.CUST_ID',
																'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
																'EQUIVALEN_USD'			=> 'TT.EQUIVALENT_AMOUNT_USD',
																'EQUIVALEN_IDR'			=> 'TT.EQUIVALENT_AMOUNT_IDR',
																'BANK_NAME'				=> new Zend_Db_Expr("
																								CASE WHEN TT.TRANSFER_TYPE = '0' THEN '".$appBankname."'
																								WHEN TT.TRANSFER_TYPE = '9' THEN CONCAT('".$appBankname."',' - ' ,TT.BENEFICIARY_BANK_NAME)
																								 WHEN TT.TRANSFER_TYPE = '10' THEN CONCAT('".$appBankname."',' - ',TT.BENEFICIARY_BANK_NAME)
																									 ELSE TT.BENEFICIARY_BANK_NAME
																								END"),
																'BALANCE_TYPE'			=> new Zend_Db_Expr("(SELECT BALANCE_TYPE
					        																FROM T_PERIODIC_DETAIL Y
					        																inner join T_PSLIP Z
					        																on Y.PS_PERIODIC = Z.PS_PERIODIC
					        																where
					        																Z.PS_NUMBER = TT.PS_NUMBER limit 1)"),
															  )
														)
												->joinLeft(	array(	'C' => 'T_PSLIP' ),'C.PS_NUMBER = TT.PS_NUMBER',array())
												->where('TT.PS_NUMBER = ?', $paymentRef);
							// echo $selectTrx;
												$paramTrxArr = $this->_db->fetchAll($selectTrx);

												$validate  	  = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin, $paymentRef);
												$paramPayment = array_merge($pslip, $setting);
												// echo '<pre>';
												// print_r($paramPayment);
												// print_r($paramTrxArr);
												// die;
												$check 		  = $validate->checkRelease($paramPayment, $paramTrxArr);
												$infoWarnOri = (!empty($check['infoWarning'])?'*) '.$check['infoWarning']:'');
												$sessionNameConfrim->infoWarnOri = $infoWarnOri;

												if($validate->isError() === true)
												{
													$error = true;
													$errorMsg = array_merge($errorMsg, $check["errorMsg"]);
												}

												$Payment = new Payment($paymentRef, $this->_custIdLogin, $this->_userIdLogin);
												// if ($this->_hasPriviReleasePayment){
													$resultRelease = $Payment->releasePayment();
													// print_r($resultRelease);
													$this->view->ps_numb = $paymentRef;
													$this->view->hidetoken = true;
													if ($resultRelease['status'] == '00'){
														$ns = new Zend_Session_Namespace('FVC');
										    			$ns->backURL = $this->view->backURL;
										    			$this->view->releaseresult = true;
														// $this->_redirect('/notification/success/index');
													}
													else
													{
														$this->view->releaseresult = false;
														$this->_helper->getHelper('FlashMessenger')->addMessage($paymentRef);
									//					$this->_helper->getHelper('FlashMessenger')->addMessage($resultRelease['status']);
									//					$this->_helper->getHelper('FlashMessenger')->addMessage($resultRelease['errorMessage']);
														$this->_redirect('/notification/index/release');
													}
												// }
										}



			if($result)
			{
				unset($_SESSION['confirmBulkCredit']);
				$this->_redirect('/notification/success');
			}
			else
			{
				$this->view->error = true;
				$error_msg[0] = 'Error: Transaction failed';
				$this->view->report_msg	= $this->displayError($error_msg);
				$this->_redirect('/payroll/bulk/confirm');
			}

			}else{
				$this->view->error = true;
				$error_msg[0] = 'Error: Invalid Token';
				$this->view->report_msg	= $this->displayError($error_msg);
				$this->_redirect('/payroll/bulk/confirm');	
			}
		}

	}

	private function parseCSV($fileName){
		$csvData = false;
		try {
				$Csv = new Application_Csv (  $fileName, $separator = "," );
				$csvData = $Csv->readAll ();
			} catch ( Exception $e ) {
				echo nl2br ( $e->getTraceAsString () );
			}
			return $csvData;
	}
}
