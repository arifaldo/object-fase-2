<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class notionalpoolingnew_ListController extends Application_Main 
{
    protected $_moduleDB = 'RTF'; // masih harus diganti
    
    public function indexAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->systemType = $system_type;

        $select = $this->_db->select()
                            ->from(array('A' => 'T_NOTIONAL_POOLING'),array('N_NUMBER','N_CUST_ID','N_CREATED','N_CREATEDBY','N_UPDATED','N_UPDATEDBY','N_STATUS'))
                            ->join(array('B' => 'M_CUSTOMER'), 'B.CUST_ID = A.N_CUST_ID',array('B.CUST_NAME'))
                            ->where('A.N_CUST_ID ='.$this->_db->quote((string)$this->_custIdLogin))
                            ->order('N_UPDATED DESC');
                            
        $this->paging($select);
         
         $select = $this->_db->select()
                            ->from(array('A' => 'T_NOTIONAL_DETAIL'),array('A.N_IS_SAVING','A.N_IS_CREDIT'))
                            ->join(array('D' => 'T_NOTIONAL_POOLING'), 'D.N_NUMBER = A.N_NUMBER',array('D.N_CUST_ID','D.N_STATUS','N_NUMBER'))
                            ->join(array('B' => 'M_CUSTOMER_ACCT'), 'B.ACCT_NO = A.N_SOURCE_ACCOUNT',array('B.ACCT_NO','B.ACCT_DESC','B.CCY_ID','B.ACCT_NAME'))
                            ->join(array('C' => 'M_CUSTOMER'), 'C.CUST_ID = D.N_CUST_ID',array('C.CUST_NAME','C.CUST_ID'))
                            ->order('N_UPDATED DESC');
    //echo $select;die;
        $dataacct                   = $this->_db->fetchAll($select);
        //echo '<pre>';
        $this->view->dataacct = $dataacct;
        //var_dump($dataacct);die;
    }

}