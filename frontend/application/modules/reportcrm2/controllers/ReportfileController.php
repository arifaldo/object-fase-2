<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';

require_once 'General/Customer.php';
require_once 'General/CustomerUser.php';
require_once 'Crypt/AESMYSQL.php';
class reportcrm2_ReportfileController extends Application_Main {


    
    public function indexAction()
    { 
        $this->_helper->_layout->setLayout('newlayout');

        $select = $this->_db->select()
                            ->from('information_schema.tables', array('table_name'))
                            ->where('table_schema = ?', 'digitalbanking360');

        $tableList = $this->_db->fetchAll($select);

        // $report =  $this->_db->fetchAll(
        //                 $this->_db->select()->distinct()
        //                      ->from(array('A' => 'T_REPORT_GENERATOR'),array('A.*'))
        //                      ->where("A.REPORT_CUST = ".$this->_db->quote($this->_custIdLogin))
        //                      ->where("A.REPORT_CREATEDBY = ".$this->_db->quote($this->_userIdLogin))
        //              );
        // foreach ($report as $key => $value) {
        //     $report[$key]['REPORT_CREATED'] = Application_Helper_General::convertDate($value['REPORT_CREATED'],$this->displayDateTimeFormat,$this->defaultDateFormat);
        // }
        

        $report =  $this->_db->fetchAll(
                        $this->_db->select()->distinct()
                             //->from(array('A' => 'T_REPORT_GENERATOR'),array('A.REPORT_NAME','A.ID','A.REPORT_FILE','A.REPORT_EMAIL','A.REPORT_CREATED','A.REPORT_CREATEDBY'))
                             ->from(array('A' => 'T_REPORT_GENERATOR'),array('A.*'))
                             ->joinLeft(array('B' => 'T_REPORT_FILE'),'A.ID = B.REPORT_ID',array('B.*'))
                             ->where("A.REPORT_CUST = ".$this->_db->quote($this->_custIdLogin))
                             ->where("A.REPORT_CREATEDBY = ".$this->_db->quote($this->_userIdLogin))
                             ->order('A.REPORT_CREATED ASC')
                     );
        // echo $report;die;
        $output = '';
        $secret_key = 'cmdemo';
        $secret_iv = 'democm';
            // hash
        $key = hash('sha256', $secret_key);
        
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        $encrypt_method = "AES-256-CBC";
        if(!empty($report)){
        foreach ($report as $keys => $value) {
            
            $string = $value['ID'];
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
            $report[$keys]['CODE'] = $output;
            //$destination = LIBRARY_PATH.'/data/uploads/document/submit/';
            
            
        } 
    }
    
    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
    $password = $sessionNamespace->token; 
    $this->view->token = $sessionNamespace->token;  
    
    $AESMYSQL = new Crypt_AESMYSQL();
    $PS_NUMBER      = urldecode($this->_getParam('FILE_ID'));
    $FILE_ID = $AESMYSQL->decrypt($this->_getParam('FILE_ID'), $password);
    
    if($FILE_ID)
    {   
        $idx = $this->_getParam('idx');
        
        $selectdata =    $this->_db->select()->distinct()
                            //->from(array('A' => 'T_REPORT_GENERATOR'),array('A.REPORT_NAME','A.ID','A.REPORT_FILE','A.REPORT_EMAIL','A.REPORT_CREATED','A.REPORT_CREATEDBY'))
                            ->from(array('A' => 'T_REPORT_GENERATOR'),array('A.*'))
                            ->joinLeft(array('B' => 'T_REPORT_FILE'),'A.ID = B.REPORT_ID',array('B.*'))
                            ->where("A.ID = ".$this->_db->quote($FILE_ID))
                            
                            ->where("A.REPORT_CUST = ".$this->_db->quote($this->_custIdLogin))
                            ->where("A.REPORT_CREATEDBY = ".$this->_db->quote($this->_userIdLogin));
                            
                    
        if(!empty($idx))
        {
            $selectdata->where('B.REPORT_INDEX = ?',$idx);
        }
        //$idx
        $selectdata->order('A.REPORT_CREATED ASC');
        $data =  $this->_db->fetchRow($selectdata);		 
        //echo "<pre>";
        //var_dump($data);die();
        $namefileex = explode('.',$data['REPORT_FILENAME']);
        //var_dump($namefileex);
        //var_dump(count($namefileex));die();
        // if ($data['REPORT_FILE'] == 1)
        // {

            //PDF
            // echo "<code>PDF = $data</code>"; die;	
       

            if(count($namefileex)<=2)
            { 
                $filename = substr($namefileex[0], 0, -12);

                $attahmentDestination = UPLOAD_PATH . '/document/submit/';
                $this->_helper->download->file($filename.'.'.$namefileex['1'],$attahmentDestination.$data['REPORT_FILENAME']);
            
            }
            else
            {
                $fileArr = explode(';',$data['REPORT_FILENAME']);
                foreach($fileArr as $kval){
                    $namefileex = explode('.',$kval);
                    $filename = substr($namefileex[0], 0, -12);
                    $attahmentDestination = UPLOAD_PATH . '/document/submit/';
                    //var_dump($filename.'.'.$namefileex['1']);die;
                    
                    $this->_helper->download->file($filename.'.'.$namefileex['1'],$attahmentDestination.$kval);
                    break;
                    //echo $attahmentDestination.$kval;
                    //var_dump($kval);die;
                }
                
            }

        // }
        // else
        // {
        //     //CSV
        //     // echo "<code>CSV = $data</code>"; die;	


            
        // }
        //$updateArr = array();
        //$updateArr['FILE_DOWNLOADED'] = $data['FILE_DOWNLOADED']+1;
        //$updateArr['FILE_DOWNLOADEDBY'] = $this->_userIdLogin;
        
        //$whereArr = array('FILE_ID = ?'=>$FILE_ID);
        
        //$fileupdate = $this->_db->update('T_FILE_SUBMIT',$updateArr,$whereArr);
        //echo "file downloaded: ".$data['FILE_DOWNLOADED'];
        //Application_Helper_General::writeLog('VDEL','Download File Sharing report '.$data['REPORT_FILENAME']);
    }
    else
    {
        Application_Helper_General::writeLog('VDEL','View File Sharing report');
    }
    // print_r($report);die;
    // echo "<pre>";
    // print_r($report);die;
    
    $this->view->report = $report;
         $select = $this->_db->select()
                                    ->from('information_schema.columns', array('DATA_TYPE','COLUMN_NAME'))
                                    ->where('table_schema = ?', 'digitalbanking360')
                                    ->where('table_name in ("T_PSLIP","T_TRANSACTION")')
                                    ->where('COLUMN_NAME NOT in ("T_PSLIP","T_TRANSACTION","ESCROW_ACC",
                                    "ESCROW_ACC_TYPE",
                                    "HOST_RESPONSE",
                                    "REVERSAL_DESC",
                                    "REVERSAL_STATUS",
                                    "UUID",
                                    "LOG",
                                    "REFF_ID",
                                    "BENEFICIARY_ID",
                                    "BENEF_ACCT_BANK_CODE",
                                    "BENEFICIARY_DATA",
                                    "TRA_CHARGE_TO",
                                    "SENDFILE_sTATUS",
                                    "RELEASE_TYPE",
                                    "EFT_STATUS",
                                    "EFT_BANKCODE",
                                    "BANK_RESPONSE",
                                    "DATE_UPDATE",
                                    "BENEFICIARY_ADDRESS2",
                                    "BENEFICIARY_ADDRESS3",
                                    "PROVIDER_CHARGES",
                                    "LLD_CODE",
                                    "TRA_REFNO",
                                    "TRX_ID",
                                    "TRA_REMAIN",
                                    "TRANSFER_FEE_STATUS",
                                    "RELEASE_TYPE",
                                    "PS_BILLER_ID",
                                    "PS_PERIODIC",
                                    "PS_REMAIN",
                                    "FEATURE_ID",
                                    "PS_TXCOUNT",
                                    "PS_RELEASER_CHALLENGE",
                                    "PS_RELEASER_USER_LOGIN",
                                    "DISPLAY_FLAG",
                                    "RAW_REQUEST",
                                    "REVERSAL_DESC",
                                    "TRACE_NO",
                                    "TX_FEE_SCM_CHARGE_TO",
                                    "DISKONTO_AMOUNT",
                                    "BILLER_ORDER_ID",
                                    "EFT_BANKRESPONSE",
                                    "SKN_TRANSACTION_TYPE",
                                    "ORG_DIR",
                                    "SWIFT_CODE",
                                    "NOSTRO_CODE",
                                    "CLR_CODE",
                                    "BANK_CODE",
                                    "POB_NUMBER",
                                    "BENEFICIARY_BI_ACCOUNT",
                                    "BENEFICIARY_BANK_ADDRESS2",
                                    "BENEFICIARY_BANK_ADDRESS3" )');
            // echo $select;die;    
        $tempColumn = $this->_db->fetchAll($select);
        foreach ($tempColumn as $key => $value) {
            if($value['COLUMN_NAME'] == 'PS_NUMBER'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Payment Ref');
            }else if($value['COLUMN_NAME'] == 'PS_SUBJECT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Subject');
            }else if($value['COLUMN_NAME'] == 'PS_CREATED'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Create Date');
            }else if($value['COLUMN_NAME'] == 'PS_UPDATED'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Update Date');
            }else if($value['COLUMN_NAME'] == 'PS_EFDATE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transaction Date');
            }else if($value['COLUMN_NAME'] == 'PS_STATUS'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Payment Status');
            }else if($value['COLUMN_NAME'] == 'CUST_ID'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Company ID');
            }else if($value['COLUMN_NAME'] == 'PS_TOTAL_AMOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Total Amount');
            }else if($value['COLUMN_NAME'] == 'PS_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Payment Type');
            }else if($value['COLUMN_NAME'] == 'PS_CATEGORY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Category');
            }else if($value['COLUMN_NAME'] == 'PS_CCY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Curency');
            }else if($value['COLUMN_NAME'] == 'PS_CREATEDBY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Create By');
            }else if($value['COLUMN_NAME'] == 'EQUIVALENT_AMOUNT_IDR'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Equivalent Amount');
            }else if($value['COLUMN_NAME'] == 'TRANSACTION_ID'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transaction Id');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ACCOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Account');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_EMAIL'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Email');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CITIZENSHIP'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Citizenship');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_RESIDENT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Resident');
            }else if($value['COLUMN_NAME'] == 'TRA_AMOUNT'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Amount');
            }else if($value['COLUMN_NAME'] == 'TRANSFER_FEE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Fee');
            }else if($value['COLUMN_NAME'] == 'TRANSFER_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Type');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ALIAS_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Alias');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ACCOUNT_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Name');
            }else if($value['COLUMN_NAME'] == 'RATE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Rate Sell');
            }else if($value['COLUMN_NAME'] == 'RATE_BUY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Rate Buy');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ALIAS_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Type');
            }else if($value['COLUMN_NAME'] == 'BOOK_RATE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Book Rate Sell');
            }else if($value['COLUMN_NAME'] == 'BOOK_RATE_BUY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Book Rate Buy');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCT_BANK_CODE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Bank');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_CCY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account CCY');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Name');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_ALIAS_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Alias');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Type');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ACCOUNT_CCY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination CCY');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_MOBILE_PHONE_NUMBER'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Phone');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ADDRESS'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Address');
            }else if($value['COLUMN_NAME'] == 'SOURCE_ACCOUNT_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Source Account Name');
            }else if($value['COLUMN_NAME'] == 'TOTAL_CHARGES'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Total Charges');
            }else if($value['COLUMN_NAME'] == 'FULL_AMOUNT_FEE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Full Amount Fee');
            }else if($value['COLUMN_NAME'] == 'PROVISION_FEE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Provision Fee');
            }else if($value['COLUMN_NAME'] == 'EQUIVALENT_AMOUNT_USD'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Total USD');
            }else if($value['COLUMN_NAME'] == 'TRA_MESSAGE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Message');
            }else if($value['COLUMN_NAME'] == 'TRA_ADDITIONAL_MESSAGE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Additional Message');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Bank');
            }else if($value['COLUMN_NAME'] == 'TRA_STATUS'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Transfer Status');
            }else if($value['COLUMN_NAME'] == 'LLD_TRANSACTOR_RELATIONSHIP'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Transactor Relationship');
            }else if($value['COLUMN_NAME'] == 'LLD_TRANSACTION_PURPOSE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Transaction Purpose');
            }else if($value['COLUMN_NAME'] == 'LLD_IDENTITY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Identity');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ID_TYPE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Id Type');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_ID_NUMBER'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Id Number');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CITY_CODE'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination City');
            }else if($value['COLUMN_NAME'] == 'NOSTRO_NAME'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Nostro Name');
            }else if($value['COLUMN_NAME'] == 'LLD_DESC'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('LLD Description');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CITIZENSHIP_COUNTRY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Country');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_ADDRESS1'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Address');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_BRANCH'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Branch');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_COUNTRY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Bank Country');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_CATEGORY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Category');
            }else if($value['COLUMN_NAME'] == 'BENEFICIARY_BANK_CITY'){
                $tempColumn[$key]['COLOMN'] = $this->language->_('Destination Bank City');
            }
            else{
                $tempColumn[$key]['COLOMN'] = $value['COLUMN_NAME'];
            }
            
        }
        // echo '<pre>';
        // print_r($tempColumn);die;
        $this->view->colomndata = $tempColumn;

        $this->view->tableList = $tableList;

        if($this->_request->isPost())
        {

            $params     = $this->_request->getParams();
            // print_r($params);die;
            $filters    = array('tablename' => array('StringTrim','StripTags','HtmlEntities'),
                                'tablecols' => array('StringTrim','StripTags','HtmlEntities'),
                                'sortasc' => array('StringTrim','StripTags','HtmlEntities'),
                                'sortdesc' => array('StringTrim','StripTags','HtmlEntities'),
                                'datalimit' => array('StringTrim','StripTags','HtmlEntities'),
                                'wherecol' => array('StringTrim','StripTags','HtmlEntities'),
                                'whereopt' => array('StringTrim','StripTags','HtmlEntities'),
                                'whereval' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_name' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_email' => array('StringTrim','StripTags','HtmlEntities'),
                                'label' => array('StringTrim','StripTags','HtmlEntities'),
                                'colomn' => array('StringTrim','StripTags','HtmlEntities'),
                                'type' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_schedule' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_date' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_day' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_time' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_data' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_month' => array('StringTrim','StripTags','HtmlEntities'),
                                'sortby' => array('StringTrim','StripTags','HtmlEntities'),
                                'sortasc' => array('StringTrim','StripTags','HtmlEntities'),
                                'sortdesc' => array('StringTrim','StripTags','HtmlEntities')
                                
                                
            );

            $validators =  array('tablename'     => array(),
                                'tablecols'      => array(),
                                'sortasc'        => array(),
                                'sortdesc'       => array(),
                                'wherecol'       => array(),
                                'whereopt'       => array(),
                                'whereval'          => array('allowEmpty'=>true
                                                            // new Zend_Validate_Regex(array('pattern' => '/^[0-9A-Za-z\\s-_.]+$/')),
                                                            // 'messages' => array('Invalid report condition')
                                                    ),
                                'datalimit'         => array('allowEmpty' => true,
                                                            'Digits',
                                                            'messages' => array('Invalid data limit format')
                                                    ),
                                'report_name'       => array('NotEmpty',
                                                            array('StringLength',array('max'=>200)),
                                                            'messages' => array('Can not be empty',
                                                                            'Report name length cannot be more than 200',
                                                                        )
                                                    ),
                                'label'       => array('NotEmpty',
                                                            'messages' => array('Can not be empty',
                                                                        )
                                                    ),

                                'colomn'       => array('NotEmpty',
                                                            'messages' => array('Can not be empty',
                                                                        )
                                                    ),
                                'type'       => array('NotEmpty',
                                                            'messages' => array('Can not be empty',
                                                                        )
                                                    ),
                                'report_email'      => array('allowEmpty'=>true,
                                                            array('StringLength',array('max'=>128)),
                                                            'messages' => array(//'Can not be empty',
                                                                            'Email length cannot be more than 128',
                                                                        )
                                                    ),
                                'report_schedule'   => array('allowEmpty'=>true,
                                                            'Alpha',
                                                            'messages' => array(//'Can not be empty',
                                                                                'Invalid schedule'
                                                                        )
                                                    ),
                                'report_date'       => array('allowEmpty'=>true),
                                'report_day'       => array('allowEmpty'=>true),
                                'report_time'       => array('allowEmpty'=>true),
                                'report_data'       => array('allowEmpty'=>true),
                                'report_month'       => array('allowEmpty'=>true),
                                'sortby'       => array('allowEmpty'=>true),
                                'sortasc'       => array('allowEmpty'=>true),
                                'sortdesc'       => array('allowEmpty'=>true)
                                
                                
            );

            $zf_filter = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

            $cek_multiple_email = true;

            if($zf_filter->report_email)
            {
                $validate = new Validate();
                $cek_multiple_email = $validate->isValidEmailMultiple($zf_filter->report_email);
            }
            // echo 'here';die;
            if($zf_filter->isValid() && $cek_multiple_email == true)
            {
                // print_r($zf_filter);die;
                // print_r($zf_filter->sortasc);die;
                $selectedCols = implode(",", $zf_filter->tablecols);
                // $sortAsc = implode(",", $zf_filter->sortasc);
                // $sortDesc = implode(",", $zf_filter->sortdesc);

                $whereCols = $zf_filter->wherecol;
                $whereOpts = $zf_filter->whereopt;
                $whereVals = $zf_filter->whereval;

                $optsArr = array("EQUAL" => "=",
                                "NOT EQUAL" => "<>",
                                "LESS THAN" => "<",
                                "GREATER THAN" => ">",
                                "LESS THAN OR EQUAL TO" => "<=",
                                "GREATER THAN OR EQUAL TO" => ">="
                );

                
                // print_r($whereCols);
                // print_r($whereVals);
                
                if(!empty($whereCols))
                {
                    $tempWhere = array();
                    $index = 0;
                    // $withoutDuplicates = array_unique(array_map("strtoupper", $whereCols));
                    // print_r($withoutDuplicates);
                    $row = 0;
                    // print_r($whereVals);
                    // echo '<pre>';
                    // print_r($whereCols);die;
                    foreach($whereCols as $key => $val){
                        $tempName = explode("-", $val);
                        $colName = $tempName[0];
                        if($key%2==0){

                            
                            // print_r($test);
                        if($whereOpts[$key] != "LIKE"){
                            $opt = $optsArr[$whereOpts[$index]];
                            // print_r($whereVals);
                            $whereval = $whereCols[$key+1];
                        }
                        else{
                            $opt = $whereOpts[$index];
                            $whereval = "%".$whereCols[$key+1]."%";
                        }
                        // if($whereVals==''){
                        //     $whereval = 0;
                        // }
                        $duplicate = in_array($val, $whereCols);
                        if($duplicate){
                            // print_r($whereval);
                            if(empty($whereVals[$row])){
                                $whereVals[$row] = 0;
                            }
                            if($row == '0'){
                                $tempWhere[$row] = $colName." ".$opt." ".$whereVals[$row];  
                            }else{
                                $tempWhere[$row-1] .= " OR ".$colName." ".$opt." ".$whereVals[$row];  
                            }
                            
                            $row++;
                        }else{

                            $tempWhere[$row] = $colName." ".$opt." ".$whereval;      
                            $row++;
                        }
                        
                        $index++;
                        }
                        // $index++;
                    }

                    // print_r($tempWhere);

                    $wheres = implode(";",$tempWhere);
                }
                // print_r($zf_filter->report_data);
               

               
            
                // print_r($wheres);die;
                

                if(empty($zf_filter->report_email)){
                    $schedule = null;
                }
                else{
                    if(empty($zf_filter->report_schedule))
                        $schedule = "weekly";
                    else
                        $schedule = strtolower($zf_filter->report_schedule);
                }

                if(empty($zf_filter->datalimit))
                    $limit = 0;
                else
                    $limit = $zf_filter->datalimit;
                
                $insArr = array("REPORT_NAME" => $zf_filter->report_name,
                                "REPORT_TABLE" => $zf_filter->tablename,
                                "REPORT_COLUMNS" => $selectedCols,
                                "REPORT_WHERE" => $wheres,
                                // "REPORT_SORT_ASC" => $sortAsc,
                                // "REPORT_SORT_DESC" => $sortDesc,
                                "REPORT_LIMIT" => $limit,
                                "REPORT_EMAIL" => $zf_filter->report_email,
                                "REPORT_SCHEDULE" => $schedule,
                                "REPORT_CUST" => $this->_custIdLogin,
                                "REPORT_CREATED" => new Zend_Db_Expr("GETDATE()"),
                                "REPORT_CREATEDBY" => $this->_userIdLogin,
                                "REPORT_DATA" => $zf_filter->report_data

                );

                if($zf_filter->sortby == '1')
                {
                    // "REPORT_SORT_ASC" => $sortAsc,
                    $insArr['REPORT_SORT_ASC'] =  $zf_filter->sortasc;
                }
                else if($zf_filter->sortby == '2')
                {
                    $insArr['REPORT_SORT_DESC'] =  $zf_filter->sortdesc;
                }


                if($schedule == 'monthly'){
                    $insArr['REPORT_DATE'] =  $zf_filter->report_date;
                }else if($schedule == 'weekly'){
                    $insArr['REPORT_DAY'] =  $zf_filter->report_day;
                    $insArr['REPORT_TIME'] =  $zf_filter->report_time;
                }else if($schedule == 'daily'){
                    // $insArr['REPORT_DAY'] =  $zf_filter->report_day;
                    $insArr['REPORT_TIME'] =  $zf_filter->report_time;
                }else if($schedule == 'yearly'){
                    $insArr['REPORT_MONTH'] =  $zf_filter->report_month;
                    $insArr['REPORT_DAY'] =  $zf_filter->report_day;
                }

                try
                {
                    $this->_db->beginTransaction();

                    $this->_db->insert('T_REPORT_GENERATOR',$insArr);

                    $lastId = $this->_db->lastInsertId();
                    // print_r($zf_filter->label);die;
                    if(!empty($zf_filter->label)){
                        foreach ($zf_filter->label as $key => $value) {
                                    // print_r($value);
                                if($zf_filter->type[$key]=='datetime'){
                                    $type = 4;
                                }elseif($zf_filter->type[$key]=='date'){
                                    $type = 3;
                                }elseif($zf_filter->type[$key]=='varchar'){
                                    $type = 1;
                                }elseif($zf_filter->type[$key]=='text'){
                                    $type = 4;
                                }elseif($zf_filter->type[$key]=='decimal' || $zf_filter->type[$key]=='int'){
                                    $type = 2;
                                }else{
                                    $type = 1;
                                }

                                $colmnArr = array("COLM_NAME" => $value,
                                    "COLM_FIELD" => $zf_filter->colomn[$key],
                                    "COLM_REPORD_ID" => $lastId,
                                    "COLM_INDEX" => $key,
                                    "COLM_TYPE" => $type
                                );
                                // print_r($colmnArr);die;
                                $this->_db->insert('T_REPORT_COLOMN',$colmnArr);
                        }

                    }

                    Application_Helper_General::writeLog('ADRG','New report has been added, Report Name : '.$zf_filter->report_name. ' Creator : '.$this->_custIdLogin." | ".$this->_userIdLogin);

                    $this->_db->commit();
                    $this->setbackURL('/reportcrm2');
                    $this->_redirect('/notification/success/index');
                }
                catch(Exception $e){
                    // print_r($e);die;
                    $this->_db->rollBack();
                    $error_remark = $this->language->_('An Error Occured. Please Try Again');
                }
            // die;
                if(isset($error_remark))
                {
                    // die;
                    Application_Helper_General::writeLog('ADRG','Add Report');
                    $this->_helper->getHelper('FlashMessenger')->addMessage('F');
                    $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
                    $this->_redirect('/reportcrm2');
                }
            }
            else
            {
                $this->view->tablename = ($zf_filter->isValid('tablename'))? $zf_filter->tablename : $this->_getParam('tablename');
                $this->view->tablecols = ($zf_filter->isValid('tablecols'))? $zf_filter->tablecols : $this->_getParam('tablecols');
                $this->view->sortasc = ($zf_filter->isValid('sortasc'))? $zf_filter->sortasc : $this->_getParam('sortasc');
                $this->view->sortdesc = ($zf_filter->isValid('sortdesc'))? $zf_filter->sortdesc : $this->_getParam('sortdesc');
                $this->view->datalimit = ($zf_filter->isValid('datalimit'))? $zf_filter->datalimit : $this->_getParam('datalimit');
                $this->view->wherecol = ($zf_filter->isValid('wherecol'))? $zf_filter->wherecol : $this->_getParam('wherecol');
                $this->view->whereopt = ($zf_filter->isValid('whereopt'))? $zf_filter->whereopt : $this->_getParam('whereopt'); 
                $this->view->whereval = ($zf_filter->isValid('whereval'))? $zf_filter->whereval : $this->_getParam('whereval');
                $this->view->report_name = ($zf_filter->isValid('report_name'))? $zf_filter->report_name : $this->_getParam('report_name');
                $this->view->report_email = ($zf_filter->isValid('report_email'))? $zf_filter->report_email : $this->_getParam('report_email');
                $this->view->report_schedule = ($zf_filter->isValid('report_schedule'))? $zf_filter->report_schedule : $this->_getParam('report_schedule');

                $error = $zf_filter->getMessages();
                // print_r($error);die;
                $errorArray = null;
                foreach($error as $keyRoot => $rowError)
                {
                   foreach($rowError as $errorString)
                   {
                      $errorArray[$keyRoot] = $errorString;
                   }
                }
        
                if(isSet($cek_multiple_email) && $cek_multiple_email == false) $errorArray['report_email'] = 'Invalid email format';
        
                $this->view->error_msg = $errorArray;

                $tblName = ($zf_filter->isValid('tablename'))? $zf_filter->tablename : $this->_getParam('tablename');


                $frontendOptions = array ('lifetime' => 259200, 
                                  'automatic_serialization' => true );
                $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
                $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
                $cacheID = 'TEMPCOLOMN';
                
                $tempColumn = $cache->load($cacheID);
                //var_dump($select_int);
                if(empty($tempColumn)){

                $select = $this->_db->select()
                                    ->from('information_schema.columns', array('DATA_TYPE','COLUMN_NAME'))
                                    ->where('table_schema = ?', 'digitalbanking360')
                                    ->where('table_name in ("T_PSLIP","T_TRANSACTION")');
                
                $tempColumn = $this->_db->fetchAll($select);
                $cache->save($tempColumn,$cacheID);
                }

                $this->view->columnList = $tempColumn;

                $tblCols = ($zf_filter->isValid('tablecols'))? $zf_filter->tablecols : $this->_getParam('tablecols');
                $tblAsc = ($zf_filter->isValid('sortasc'))? $zf_filter->sortasc : $this->_getParam('sortasc');
                $tblDesc = ($zf_filter->isValid('sortdesc'))? $zf_filter->sortdesc : $this->_getParam('sortdesc');

                $leftCols = array();
                $leftAsc = array();
                $leftDesc = array();
                foreach($tempColumn as $row){
                    if(!in_array($row['COLUMN_NAME'], $tblCols))
                        $leftCols[] = $row['COLUMN_NAME'];

                    if(!in_array($row['COLUMN_NAME'], $tblAsc))
                        $leftAsc[] = $row['COLUMN_NAME'];

                    if(!in_array($row['COLUMN_NAME'], $tblDesc))
                        $leftDesc[] = $row['COLUMN_NAME'];
                }

                $this->view->leftcols = $leftCols;
                $this->view->leftasc = $leftAsc;
                $this->view->leftdesc = $leftDesc;

                $whereCols = ($zf_filter->isValid('wherecol'))? $zf_filter->wherecol : $this->_getParam('wherecol');
                if(!empty($whereCols)){
                    $wherectr = count($whereCols)+1;
                }
                else{
                    $wherectr = 1;
                }

                $this->view->wherectr = $wherectr;
            }
        }
        else
        {
            $this->view->wherectr = 1;
        }

        // $download = $this->_getParam('download');

        // if($download){

        //     $postmPayReff = $this->_request->getParam('mPayReff');

        //     foreach ($postmPayReff as $key => $value) {
                
        //         $explodeId = explode('_', $value);

        //         $id = $explodeId[0];
                
        //         if ($explodeId[1] == 'pdf') {
        //             $pdf = true;
        //             $csv = false;
        //         }
        //         else{
        //             $pdf = false;
        //             $csv = true;
        //         }

        //         $selectcolomn = $this->_db->select()
        //                     ->from('T_REPORT_COLOMN', array('COLM_NAME','COLM_FIELD','COLM_TYPE'))
        //                     ->where('COLM_REPORD_ID = ?', $id);

        //         $ColomnList = $this->_db->fetchAll($selectcolomn);

        //         if(empty($id)){
        //             $this->_redirect('home');
        //         }
        //         $selectreport = $this->_db->select()
        //                             ->from('T_REPORT_GENERATOR', array('*'))
        //                             ->where('ID = ?', $id);
        //                             // echo $selectreport;die;
        //         $reportdata = $this->_db->fetchRow($selectreport);

        //         // $where['CUST_ID = ?'] = $this->_custIdLogin;
        //         $views = $reportdata['REPORT_VIEWS']+1;

        //         $updateArr = array('REPORT_VIEWS' => $views);
        //         $where['ID = ?'] = $id;
        //         $this->_db->update('T_REPORT_GENERATOR',$updateArr,$where);

        //         $paramPayment = array("WA"              => false,
        //                       "ACCOUNT_LIST"    => $this->_accountList,
        //                       "_beneLinkage"    => $this->view->hasPrivilege('BLBU'),
        //                      );

        //         $CustomerUser   = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
        //         // get payment query
        //         $select   = $CustomerUser->getPayment($paramPayment);

        //         if(!empty($reportdata['REPORT_WHERE'])){
        //             $whereArr = explode(';', $reportdata['REPORT_WHERE']);
        //             foreach ($whereArr as $key => $value) {

        //                 $searchkey = ( explode( ' ', $value ));
        //                 // var_dump($searchkey);
        //                 if($searchkey['0'] == ''){
        //                     if(trim($searchkey['1']) == 'PS_EFDATE' || trim($searchkey['1']) == 'PS_CREATED' || trim($searchkey['1']) == 'PS_UPDATED'){
        //                     $var = $searchkey['3'];
        //                     $searchkey['3'] = date("Y-m-d", strtotime($var) );
        //                     $value = 'DATE('.$searchkey['1'].') '.$searchkey['2'].' "'.$searchkey['3'].'"';
        //                     }
        //                 }

        //                 if(trim($searchkey['0']) == 'PS_EFDATE' || trim($searchkey['0']) == 'PS_CREATED' || trim($searchkey['0']) == 'PS_UPDATED'){
        //                     $var = $searchkey['2'];
        //                     $searchkey['2'] = date("Y-m-d", strtotime($var) );
        //                     $value = 'DATE('.$searchkey['0'].') '.$searchkey['1'].' "'.$searchkey['2'].'"';
        //                 }

        //                 if(trim($value) != ''){
        //                     $select->where($value);
        //                 }

        //             }

        //         }
               
        //         if(!empty($reportdata['REPORT_DATA'])){
        //                     if($reportdata['REPORT_DATA'] == '1'){
        //                         $select->where('MONTH(PS_CREATED) = MONTH(NOW()) AND YEAR(PS_CREATED) = YEAR(NOW())');
        //                     }else if($reportdata['REPORT_DATA'] == '2'){
        //                         $select->where('DATE(PS_CREATED) BETWEEN DATE("'.date('Y-m-01', strtotime('-1 MONTH')).'") AND DATE_SUB(CURDATE(),INTERVAL EXTRACT(DAY FROM NOW()) DAY)');
        //                     }else if($reportdata['REPORT_DATA'] == '3'){
        //                         $select->where('DATE(PS_CREATED) BETWEEN DATE("'.date('Y-m-01', strtotime('-3 MONTH')).'") AND DATE_SUB( CURDATE( ) ,INTERVAL 3 MONTH )');
        //                     }else if($reportdata['REPORT_DATA'] == '4'){
        //                         $select->where('DATE(PS_CREATED) BETWEEN DATE("'.date('Y-m-01', strtotime('-6 MONTH')).'") AND DATE_SUB( CURDATE( ) ,INTERVAL 6 MONTH )');
        //                     }else if($reportdata['REPORT_DATA'] == '5'){
        //                         $select->where('YEAR(PS_CREATED) = YEAR(NOW())');
        //                     }

        //                 }



        //         if(!empty($reportdata['REPORT_SCHEDULE'])){
        //                     if($reportdata['REPORT_SCHEDULE'] == 'daily'){
        //                         $select->where('DATE(PS_CREATED) = "'.date('Y-m-d', strtotime('-1 day')).'"');
        //                     }else if($reportdata['REPORT_SCHEDULE'] == 'hourly'){
        //                         $select->where('DATE(PS_CREATED) = DATE(NOW()) AND (HOUR(NOW())-1) = HOUR(PS_CREATED)');
        //                     }else if($reportdata['REPORT_SCHEDULE'] == 'weekly'){
        //                         $select->where('WEEK(PS_CREATED) = (WEEK(NOW())-1) AND YEAR(PS_CREATED) = (YEAR(NOW())) ');
        //                     }else if($reportdata['REPORT_SCHEDULE'] == 'monthly'){
        //                         $select->where('MONTH(PS_CREATED) = (MONTH(NOW())-1) AND YEAR(PS_CREATED) = (YEAR(NOW())) ');
        //                     }else if($reportdata['REPORT_SCHEDULE'] == 'yearly'){
        //                         $select->where('YEAR(PS_CREATED) = (YEAR(NOW())-1)');
        //                     }

        //                 }

        //         if(!empty($reportdata['REPORT_SORT_ASC'])){

        //             $sortascdata = explode(',', $reportdata['REPORT_SORT_ASC']);
        //                     // print_r($sortascdata);die;
        //             if(!empty($sortascdata)){
        //                 // die;
        //                 foreach ($sortascdata as $key => $value) {
        //                         // 4
        //                             // die;
        //                         if($value == 'PS_NUMBER'){
        //                             $value = 'T.PS_NUMBER';
        //                         }
        //                         $select->order($value.' ASC');
        //                 }
        //             }

        //         }

        //         if(!empty($reportdata['REPORT_SORT_DESC'])){
        //             // $sortby = $reportdata['REPORT_SORT_DESC'];
        //             $sortdesdata = explode(',', $reportdata['REPORT_SORT_DESC']);
        //             $sortdirdesc = 'DESC';
        //              if(!empty($sortdesdata)){
        //                 foreach ($sortdesdata as $key => $value) {
        //                         // 4
        //                     if($value == 'PS_NUMBER'){
        //                             $value = 'T.PS_NUMBER';
        //                         }
        //                         $select->order($value.' DESC');
        //                 }
        //             }
        //         }

        //         if(!empty($reportdata['REPORT_LIMIT'])){
        //             $select->limit($reportdata['REPORT_LIMIT']);
        //         }

        //         $dataSQL = $this->_db->fetchAll($select);

        //         $statusarr = array_combine(array_values($this->_transferstatus['code']),array_values($this->_transferstatus['desc']));
        //         $this->view->trastatus = $statusarr;
                
        //         if ($csv || $pdf)
        //         {   $header  = Application_Helper_Array::simpleArray($ColomnList, "COLM_NAME");     }
        //         else
        //         {
                   
        //         }

        //         $dataprint = array();
        //         foreach ($dataSQL as $key => $value) {

        //             foreach ($ColomnList as $row => $val) {
        //                 if($val['COLM_FIELD'] == 'PS_STATUS'){
        //                     $val['COLM_FIELD']  = 'payStatus';
        //                 }
        //                 $dataprint[$key][$val['COLM_FIELD']] =  $value[$val['COLM_FIELD']];

        //             }
        //         }
        //         if($csv)
        //         {
        //             // print_r($reportdata['REPORT_NAME']);die;
        //             $this->_helper->download->csv($header,$dataprint,null,$reportdata['REPORT_NAME']);  
        //             Application_Helper_General::writeLog('DARC','Export CSV View Payment');
        //         }
        //         elseif($pdf)
        //         {
        //             // die;
        //             // print_r($ColomnList);die;

        //             $this->_helper->download->pdf($header,$dataprint,null,$reportdata['REPORT_NAME']);
        //             Application_Helper_General::writeLog('DARC','Export PDF View Payment');
        //         }
        //     }
        // }


       

        $delete     = $this->_getParam('delete');

        if($delete)
        {
            $postmPayReff = $this->_request->getParam('mPayReff');

            $validators = array (
                                    'mPayReff' => array   (
                                                            'NotEmpty',
                                                            'messages' => array (
                                                                                    'Error File ID Submitted',
                                                                                )
                                                        ),
                                );

            $filtersVal = array ( 'mPayReff' => array('StringTrim','StripTags'));

            $zf_filter_input = new Zend_Filter_Input($filtersVal,$validators,$params,$this->_optionsValidator);
                $success = false;
            foreach ($postmPayReff as $key => $value) {
                if(!empty($value)){
                    $success = true;
                }
            }


            if($zf_filter_input->isValid() && $success)
            {
                try
                {
                    foreach ($postmPayReff as $key => $value)
                    {

                        $id = explode('_', $value);

                        $this->_db->beginTransaction();

                        $report =  //$this->_db->fetchAll(
                        $this->_db->select()->distinct()
                             //->from(array('A' => 'T_REPORT_GENERATOR'),array('A.REPORT_NAME','A.ID','A.REPORT_FILE','A.REPORT_EMAIL','A.REPORT_CREATED','A.REPORT_CREATEDBY'))
                             ->from(array('A' => 'T_REPORT_FILE'),array('A.*'));
                             //->where("A.REPORT_CUST = ".$this->_db->quote($this->_custIdLogin))
                            // ->where("A.REPORT_CREATEDBY = ".$this->_db->quote($this->_userIdLogin));
                     //);
                        $report->where('REPORT_ID =?',$id[0]);
                        $report->where('REPORT_INDEX =?',$id[1]);
                        $data = $this->_db->fetchRow($report);
                        
                        $attahmentDestination = UPLOAD_PATH . '/document/submit/';
                        //$this->_helper->download->file($data['REPORT_FILENAME'],$attahmentDestination.$data['REPORT_FILENAME']);
                        unlink($attahmentDestination.$data['REPORT_FILENAME']);

                        $where['REPORT_ID = ?'] = $id[0];
                        $where['REPORT_INDEX = ?'] = $id[1];
                        $result =  $this->_db->delete('T_REPORT_FILE',$where);

                        $queueService = SGO_Extendedmodule_Queueservice_Service::getInstance();
                        $insArr['ID'] = $id[0];
                        $emailQueueProducer = $queueService->getQueueByProfileName("REPORTGEN_PRODUCER");
                        $datarab = json_encode($insArr);
                        $emailQueueProducer->insertQueueItem($datarab);
                        
                        
                        
                        

                        $this->_db->commit();
                    }
                }
                catch(Exception $e)
                {
                    $this->_db->rollBack();
                }
                $this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
                $this->_redirect('/notification/success/index');
            }
            else
            {
                $error          = true;
                $errors         = $zf_filter_input->getMessages();
                $mPayReffErr      = (isset($errors['mPayReff']))? $errors['mPayReff'] : null;

                //$this->_redirect("/datasubmision/index?error=true&mPayReffErr=$mPayReffErr&filter=Clear+Filter");
            }

        }
        
        
        $sendmail     = $this->_getParam('sendmail');

        if($sendmail)
        {
            $postmPayReff = $this->_request->getParam('mPayReff');

            $validators = array (
                                    'mPayReff' => array   (
                                                            'NotEmpty',
                                                            'messages' => array (
                                                                                    'Error File ID Submitted',
                                                                                )
                                                        ),
                                );

            $filtersVal = array ( 'mPayReff' => array('StringTrim','StripTags'));

            $zf_filter_input = new Zend_Filter_Input($filtersVal,$validators,$params,$this->_optionsValidator);
                $success = false;
            foreach ($postmPayReff as $key => $value) {
                if(!empty($value)){
                    $success = true;
                }
            }


            if($zf_filter_input->isValid() && $success)
            {
                try
                {
					//var_dump($postmPayReff);die;
                    foreach ($postmPayReff as $key => $value)
                    {

                        $id = explode('_', $value);
                        
                        $sendmail     = $this->_getParam('report_email');
                        
                        $email = explode(',', $sendmail);

                        // echo '<pre>';
                        // print_r($email);die;
                        // $this->_db->beginTransaction();

                        $report =  //$this->_db->fetchAll(
                        $this->_db->select()->distinct()
                             //->from(array('A' => 'T_REPORT_GENERATOR'),array('A.REPORT_NAME','A.ID','A.REPORT_FILE','A.REPORT_EMAIL','A.REPORT_CREATED','A.REPORT_CREATEDBY'))
                             ->from(array('A' => 'T_REPORT_FILE'),array('A.*'));
                             //->where("A.REPORT_CUST = ".$this->_db->quote($this->_custIdLogin))
                            // ->where("A.REPORT_CREATEDBY = ".$this->_db->quote($this->_userIdLogin));
                        //);
                        $report->where('REPORT_ID =?',$id[0]);
                        if(!empty($id[1])){
                            $report->where('REPORT_INDEX =?',$id[1]);
                        }
                        //echo $report;
		                // echo "<code>report = $report</code>"; die;	
                        $data = $this->_db->fetchRow($report);
                        
                        //var_dump($data);die;
                        $config     = Zend_Registry::get('config');
                        $bankName    = $config['app']['bankname'];
                        $appName     = $config['app']['name'];
                        $appBankCode   = $config['app']['bankcode'];

                        $Settings = new Settings();
                        $templateEmailMasterBankAddress = $Settings->getSetting('master_bank_address');
                        $templateEmailMasterBankAppName = $Settings->getSetting('master_bank_app_name');
                        $templateEmailMasterBankAppUrl = $Settings->getSetting('master_bank_app_url');
                        $templateEmailMasterBankCity = $Settings->getSetting('master_bank_city');
                        $templateEmailMasterBankCountry = $Settings->getSetting('master_bank_country');
                        $templateEmailMasterBankEmail = $Settings->getSetting('master_bank_email');
                        $templateEmailMasterBankFax = $Settings->getSetting('master_bank_fax');
                        $templateEmailMasterBankName = $Settings->getSetting('master_bank_name');
                        $templateEmailMasterBankProvince = $Settings->getSetting('master_bank_province');
                        $templateEmailMasterBankTelp = $Settings->getSetting('master_bank_telp');
                        
                        $template = file_get_contents(LIBRARY_PATH.'/email template/emailreport.html');
                        $FOOTER_ID = file_get_contents(LIBRARY_PATH.'/email template/footer_id.txt');
                        $FOOTER_EN = file_get_contents(LIBRARY_PATH.'/email template/footer_en.txt');
                        // print_r($template);die;
                        $transFT   = array( '[[APP_NAME]]'          => $templateEmailMasterBankAppName,
                                            '[[BANK_NAME]]'         => $templateEmailMasterBankName,
                                            '[[APP_URL]]'           => $templateEmailMasterBankAppUrl                                                   
                                            );

                        $FOOTER_ID = strtr($FOOTER_ID, $transFT);
                        $FOOTER_EN = strtr($FOOTER_EN, $transFT);
                        
                        //if($string != ''){  
                        $translate = array( 
                        '[[DATA_CONTENT]]' => '',
                        '[[FOOTER_ID]]'     => $FOOTER_ID,
                        '[[FOOTER_EN]]'     => $FOOTER_EN,
                        '[[APP_NAME]]'      => $templateEmailMasterBankAppName,
                        '[[BANK_NAME]]'     => $templateEmailMasterBankName,
                        '[[APP_URL]]'       => $templateEmailMasterBankAppUrl,
                        '[[master_bank_name]]'  => $templateEmailMasterBankName,
                        '[[master_bank_fax]]'   => $templateEmailMasterBankFax,
                        '[[master_bank_address]]'   => $templateEmailMasterBankAddress,
                        '[[master_bank_telp]]'  => $templateEmailMasterBankTelp,    
                        );
                          
                        $mailContent = strtr($template,$translate);
						  //var_dump($mailContent);die;
                        // echo '<pre>';
                        // print_r($data['REPORT_FILENAME']);die;

                        $attahmentDestination = UPLOAD_PATH . '/document/submit/';
						//echo $attahmentDestination;die;
                        foreach($email as $ky => $vmail)
                        {
                            // $test= 'riduan@sgo.co.id';
                            $status = Application_Helper_Email::sendEmailAttachementpath($vmail,'Report Generator',$mailContent,$data['REPORT_FILENAME'],$data['REPORT_FILENAME'],$attahmentDestination);
                            //var_dump($status);die;
								
                        }
                        
		                // echo "<code>masuk = $data</code>"; die;	
                        //echo "<pre>";
                        //var_dump($translate);
                        //echo "######";
                        //var_dump($status);die;

                     //   $this->_db->commit();
                    }
                }
                catch(Exception $e)
                {
					var_dump($e);die;
                    $this->_db->rollBack();
                }
                $this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
                $this->_redirect('/notification/success/index');
            }
            else
            {
                $error          = true;
                $errors         = $zf_filter_input->getMessages();
                $mPayReffErr      = (isset($errors['mPayReff']))? $errors['mPayReff'] : null;

                //$this->_redirect("/datasubmision/index?error=true&mPayReffErr=$mPayReffErr&filter=Clear+Filter");
            }

        }
    }
    
}
