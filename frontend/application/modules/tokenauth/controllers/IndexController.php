<?php


require_once 'Zend/Controller/Action.php';
// require_once 'General/CustomerUser.php';
require_once 'CMD/Validate/Validate.php';
require_once 'SGO/Extendedmodule/Google/GoogleAuthenticator.php';
require_once ('Crypt/AES.php');
require_once 'Service/Token.php';
// require_once '/GoogleAuthenticator/GoogleAuthenticator.php'; //added new

//NOTE:
//Watch the modulename, filename and classname carefully
class Tokenauth_IndexController extends Application_Main {
	/**
	 * The default action - show the home page
	 */
	
	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
			
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
		$cust_id = $this->_custIdLogin;
		$user_id = $this->_userIdLogin;
		$this->view->user_id 	= $user_id;
		
		$settings =  new Settings();
		$clientUser  =  new SGO_Soap_ClientUser();
		
		$select3 = $this->_db->select()
					 ->from(array('C' => 'M_USER'));
		$select3->where("USER_ID = ".$this->_db->quote($this->_userIdLogin));
		$select3->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin));
		$data2 = $this->_db->fetchRow($select3);
		 //print_r($data2);die;
		$this->view->old_email		= $data2['USER_EMAIL'];

		if(!empty($data2['TOKEN_ID']) && $data2['TOKEN_TYPE'] == '6'){
			$this->view->googleauth = true;
			$this->view->tokenid = $data2['TOKEN_ID'];
		}else{
			$this->view->googleauth = false;
		}
		//var_dump($this->view->googleauth);die;

		

		$error_message = '';

		$random_number = random_int(10000000, 99999999);
		$this->view->chlcode = chunk_split($random_number, 4, ' ');

		
				
		if(is_string($user_id))
		{
			if($this->_request->isPost())
			{
				// var_dump($this->_request->getParam('btnCheck'));
				//echo '<pre>';
				 //var_dump($this->_getAllParams());die;
				if($this->_request->getParam('btnValidate')){

				$filters = array(
					'regcode'     => array('StripTags','StringTrim'),
			 		'regid'     => array('StripTags','StringTrim'),
					'time'     => array('StripTags','StringTrim'),
					// 'confirm_email'     => array('StripTags','StringTrim'),
				);
			
				$validators =  array(
							'regcode'       => array('NotEmpty',														
													'messages' => array(
																	   $this->language->_('Register Code Can not be left blank'),																		   
																	   )
													),						  
						  
						   'regid'    => array('NotEmpty',
													
													'messages' => array(
																	   $this->language->_('Register Id cannot be left blank'),
													
																	   )
													),
							'time'    => array('NotEmpty',
													
													'messages' => array(
																	   $this->language->_('Time cannot be left blank'),
													
																	   )
													),
						
				);

				$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getPost());
	
				$regcode	= $zf_filter_input->regcode;
				$regid	= $zf_filter_input->regid;
				$time	= $zf_filter_input->time;
				$device = $this->_request->getParam('device');
				// $confirm_email	= $zf_filter_input->confirm_email;
				

				

// 				if($newpass!=$confirm_email){
// 					$this->view->error = 1;
// // 					$this->view->old_email	= $oldpass;
// 					$this->view->msg_failed = $this->language->_('Error: Email does not match.');
// 				}else
// 				{				
				$errDesc = array();			
			
				if($zf_filter_input->isValid())
	  			{
	  				

	  				if ($regcode == "" || $regid == "" || $time == "") {
			        $error_message = 'Please Scan above QR code to configure your application and enter genereated authentication code to validated!';
				    }
				    else
				    {
				    	// var_dump($google_code);
				    	// var_dump($code);die;
						
				    	$setting 		= new Settings();
						//$google_duration 	= $setting->getSetting('google_duration');
						
						//$strURL = $urltoken."/mCodeXpressAuth/registerNewToken";
						//$strPostFields = "respFormat=JSON&logLevel=FINE&operatorId=SaltSimulator&tokenIdentity=Purbo&assignedUser=&registrationCode=".$regcode."&regKeyId=".$regid."&timeId=".$time;
						$request = array();
						$request['mod'] = 'prosesRegisterMcode';
						$request['registrationCode'] = $regcode;
						$request['regKeyId'] = $regid;
						$request['timeId'] = $time;
						//$request['tokenid'] = $this->_custIdLogin.$this->_userIdLogin;
						$request['tokenid'] = $this->_userIdLogin.sprintf("%010d", mt_rand(1, 99999999999));
						$success = $clientUser->callapi('token',$request);
						$result  = $clientUser->getResult();
						//var_dump($result);die;
						
				        if($result == 'CORRECT')
				        {
				            // success


				        	$date = date('Y-m-d h:i:s', strtotime("+1 days"));
							$str=rand(); 
							$rand = md5($str); 
							
						$info = 'Token';
						$info2 = 'Token';
						
						//$change_id = $this->suggestionWaitingApproval($info,$info2,$this->_changeType['code']['new'],null,'M_USER','TEMP_USER','','Token','',null,'mobiletoken');
							
						/*	$select	= $this->_db->select()
										->from(array('U'	 			=> 'M_USER'),
											   array('USER_ID' 			=> 'U.USER_ID',
											   		 'USER_FULLNAME'	=> 'U.USER_FULLNAME',
											   		 'USER_EMAIL' 		=> 'U.USER_EMAIL',
											   		 'USER_STATUS' 		=> 'U.USER_STATUS',
											   		 'USER_RRESET' 		=> 'U.USER_RRESET',
											   		 'USER_RPWD_ISBYBO' => 'U.USER_RPWD_ISBYBO',
											   		)
											   )
										->where("U.CUST_ID 		= ?", (string) $this->_custIdLogin)
										->where("U.USER_ID 		= ?", (string) $this->_userIdLogin);
							$userData = $this->_db->fetchRow($select); */
							//$userData['CHANGES_ID'] = $change_id;
							
							$data = array(
									
									'TOKEN_TYPE'					=> '6',
									'TOKEN_ID'					=> $request['tokenid'],
									'TOKEN_DEVICE'		=> $device,
									'USER_SUGGESTEDBY'		=> $this->_userIdLogin,
									'USER_SUGGESTED'		=> new Zend_Db_Expr('now()')
							);

							$where =  array();
							$where['CUST_ID 		 = ?'] 	= $this->_custIdLogin;
							$where['USER_ID 		 = ?'] 	= $this->_userIdLogin;
							$result = $this->_db->update('M_USER',$data,$where);
							
							
							// update table sb_user
							

							$select	= $this->_db->select()
										->from(array('U'	 			=> 'M_USER'),
											   array('USER_ID' 			=> 'U.USER_ID',
											   		 'USER_FULLNAME'	=> 'U.USER_FULLNAME',
											   		 'USER_EMAIL' 		=> 'U.USER_EMAIL',
											   		 'USER_STATUS' 		=> 'U.USER_STATUS',
											   		 'USER_RRESET' 		=> 'U.USER_RRESET',
											   		 'USER_RPWD_ISBYBO' => 'U.USER_RPWD_ISBYBO',
											   		)
											   )
										->where("U.CUST_ID 		= ?", (string) $this->_custIdLogin)
										->where("U.USER_ID 		= ?", (string) $this->_userIdLogin);
							$userData = $this->_db->fetchRow($select);
							$USER_EMAIL = $userData['USER_EMAIL'];
							// insert activity log
							// $ua_desc = "Delete Google Auth by Customer: User ID = ".$this->_userIdLogin."; Name = ".$userData['USER_FULLNAME']."; Email = $USER_EMAIL";
							// Application_Helper_General::writeLogAnonymous('RFPW', $ua_desc, $this->_userIdLogin, $this->_custIdLogin);

							// Send Email
						    $mailTemplate 	= $setting->getSetting('femailtemplate_authadd');
							$templateEmailMasterBankName = $setting->getSetting('master_bank_name');
						  	$templateEmailMasterBankAppName = $setting->getSetting('master_bank_app_name');
						  	$templateEmailMasterBankEmail = $setting->getSetting('master_bank_email');
						  	$templateEmailMasterBankTelp = $setting->getSetting('master_bank_telp');
						  	$templateEmailMasterBankWapp = $setting->getSetting('master_bank_wapp');
						  	$url_fo 	= $setting->getSetting('url_fo');
						  	// $isi = $this->_db->fetchrow($isi);
							$actual_link = $_SERVER['SERVER_NAME'];
							$key = md5 ('permataNet92');
							$encrypt = new Crypt_AES ();
							// print_r($this->_userId);
							// $user = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $this->_userIdLogin, MCRYPT_MODE_ECB);
							// $cust = ( $encrypt->encrypt ( $this->_custIdLogin ) );
							// $user = ( $encrypt->encrypt ( $this->_userIdLogin ) );
							$cust = ( $this->sslEnc(  $this->_custIdLogin ) );
							$user = ( $this->sslEnc( $this->_userIdLogin ) );
							// $dateenc = ( $encrypt->encrypt ( $date ) );
							
							// echo $result; die;
					 		//$newPassword = $url_fo.'/googleauth/index/validateauth?safetycheck=&type='.urldecode($google_code).'&code='.urldecode($rand).'&cust_id='.urlencode($cust).'&user_id='.urlencode($user);
					 		//$datenow = date('d-M-Y H:i:s', strtotime("+1 days"));
        					// $template = str_ireplace('[[exp_date]]',$datenow,$template);
					 		// print_r($newPassword);die;
							$data = array(  '[[comp_accid]]' 				=> $this->_custIdLogin,
											'[[user_login]]' 				=> $this->_userIdLogin,
											'[[user_email]]' 				=> $data2['USER_EMAIL'],
											'[[user_fullname]]' 			=> $userData['USER_FULLNAME'],
											'[[exp_date]]'					=> '',
											'[[user_cleartext_password]]' 	=> '',
											'[[master_bank_app_name]]'		=> $templateEmailMasterBankAppName,
											'[[master_bank_name]]' 			=> $templateEmailMasterBankName,
											'[[master_bank_email]]' 		=> $templateEmailMasterBankEmail,
											'[[master_bank_telp]]' 			=> $templateEmailMasterBankTelp,
											'[[master_bank_wapp]]' 			=> $templateEmailMasterBankWapp,
											
											'[[confirm_link]]'				=> $newPassword
										 );
							// print_r($data);
							$mailContent  = strtr($mailTemplate, $data);
							// echo $mailContent;die;
							//$mainResponse = Application_Helper_Email::sendEmail($USER_EMAIL, 'Add Token Auth Information', $mailContent);





				   //          $updateArr = array(
							// 				'GOOGLE_CODE'			=> $google_code
							// 				// 'ACCT_ALIAS_NAME' 	=> $value
							// 			);
						
							// $where['USER_EMAIL = ?'] = (string)$data2['USER_EMAIL'];
							// $this->_db->update('M_USER',$updateArr,$where);

							$msg = 'Please validate Token Auth activation action by clicking on the link we have sent to your email at: <p class="text-primary">'.$this->maskingEmail($USER_EMAIL).'</p>';
							$headmsg = 'Mobile token has been Verified';
							$sessionNamespace = new Zend_Session_Namespace('resultMsg');
							$sessionNamespace->msg = $msg;
							$sessionNamespace->headmsg = $headmsg;


				            $this->_redirect('/tokenauth/index/emailsuccess');
				            // header("Location: googleauth/index/result");
				        }
				        else
				        {
				            // fail
				            $this->view->error = true;
				            $error_message = 'Invalid Authentication Code!';
				            $this->view->msg_failed = $error_message;
				        }
				    }

	  		// 		$filters = array('*' => array('HtmlEntities', 'StripTags', 'StringTrim'));
					// $validators = array(
					// 	'responseCodeReq' => array(
					// 		'notEmpty',
					// 		'alnum',
					// 		'presence' => 'required',
					// 	)
					// );

					// $filter = new Zend_Filter_Input($filters, $validators, $this->_getAllParams(), array('breakChainOnFailure' => true));
										  			
					
	  			}
	  			
	  		}else if($this->_request->getParam('btnCheck')){
				
				$tokenapli = $this->_request->getParam('token_apli');
				
				

	  							$select3 = $this->_db->select()
									 ->from(array('C' => 'M_USER'));
								$select3->where("USER_ID = ".$this->_db->quote($this->_userIdLogin));
								$select3->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin));
								$data2 = $this->_db->fetchRow($select3);

								// $code = $param['googleauth'];
								//var_dump($tokenapli);die;
								if($tokenapli == '2'){
								$request = array();
								$request['mod'] = 'prosesCR';
								$request['tokenid'] = $data2['TOKEN_ID'];
								$request['chl'] = $this->_request->getParam('chlcode');
								$request['otp'] = $this->_request->getParam('chltoken');
								
								$request['token'] = 'mcodex';
								
								$success = $clientUser->callapi('token',$request);
								$result  = $clientUser->getResult();
								}else{
								
								$request = array();
								$request['mod'] = 'prosesOTP';
								$request['tokenid'] = $data2['TOKEN_ID'];
								//$request['chl'] = $this->_request->getParam('chlcode');
								$request['otp'] = $this->_request->getParam('chltoken');
								
								$request['token'] = 'mcodex';
								
								$success = $clientUser->callapi('token',$request);
								$result  = $clientUser->getResult();
								
								}
								if($result == 'CORRECT')
								{
									$this->_redirect('/tokenauth/index/resultsuccess');
								}else{
									$this->_redirect('/tokenauth/index/resultfailed');
								}
								//echo '<pre>';
								//var_dump($this->getRequest()->getParams());
								//var_dump($result);
								//die;		resynctime
								
	  		}else if($this->_request->getParam('btnResync')){
				
				$timeid = $this->_request->getParam('timeid');
				
				

	  							$select3 = $this->_db->select()
									 ->from(array('C' => 'M_USER'));
								$select3->where("USER_ID = ".$this->_db->quote($this->_userIdLogin));
								$select3->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin));
								$data2 = $this->_db->fetchRow($select3);

								$request = array();
								$request['mod'] = 'resynctime';
								$request['tokenid'] = $data2['TOKEN_ID'];
								$request['timeId'] = $timeid;
								
								
								$request['token'] = 'mcodex';
								
								$success = $clientUser->callapi('token',$request);
								$result  = $clientUser->getResult();
								
								if($result == 'CORRECT')
								{
									$this->_redirect('/tokenauth/index/resultsuccess');
								}else{
									$this->_redirect('/tokenauth/index/resultfailed');
								}
								//echo '<pre>';
								//var_dump($this->getRequest()->getParams());
								//var_dump($result);
								//die;		resynctime
								
	  		}else if($this->_request->getParam('btnRemove')){

	  				$date = date('Y-m-d G:i:s', strtotime("+1 days"));
					$str=rand(); 
					$rand = md5($str); 

					// update table sb_user
					$data = array(
									
									'TOKEN_TYPE'					=> '1',
									'TOKEN_ID'					=> NULL,
									'TOKEN_DEVICE'		=> NULL
					);

					$where =  array();
					$where['CUST_ID 		 = ?'] 	= $this->_custIdLogin;
					$where['USER_ID 		 = ?'] 	= $this->_userIdLogin;
					$this->_db->update('M_USER',$data,$where);

					$select	= $this->_db->select()
								->from(array('U'	 			=> 'M_USER'),
									   array('USER_ID' 			=> 'U.USER_ID',
									   		 'USER_FULLNAME'	=> 'U.USER_FULLNAME',
									   		 'USER_EMAIL' 		=> 'U.USER_EMAIL',
									   		 'USER_STATUS' 		=> 'U.USER_STATUS',
									   		 'USER_RRESET' 		=> 'U.USER_RRESET',
									   		 'USER_RPWD_ISBYBO' => 'U.USER_RPWD_ISBYBO',
									   		)
									   )
								->where("U.CUST_ID 		= ?", (string) $this->_custIdLogin)
								->where("U.USER_ID 		= ?", (string) $this->_userIdLogin);
					$userData = $this->_db->fetchRow($select);
					$USER_EMAIL = $userData['USER_EMAIL'];
					// insert activity log
					// $ua_desc = "Delete Google Auth by Customer: User ID = ".$this->_userIdLogin."; Name = ".$userData['USER_FULLNAME']."; Email = $USER_EMAIL";
					// Application_Helper_General::writeLogAnonymous('RFPW', $ua_desc, $this->_userIdLogin, $this->_custIdLogin);

					// Send Email
				    $setting 		= new Settings();
					$mailTemplate 	= $setting->getSetting('femailtemplate_authremove');
					$templateEmailMasterBankName = $setting->getSetting('master_bank_name');
				  	$templateEmailMasterBankAppName = $setting->getSetting('master_bank_app_name');
				  	$templateEmailMasterBankEmail = $setting->getSetting('master_bank_email');
				  	$templateEmailMasterBankTelp = $setting->getSetting('master_bank_telp');
				  	$templateEmailMasterBankWapp = $setting->getSetting('master_bank_wapp');

				  	// $isi = $this->_db->fetchrow($isi);
					$actual_link = $_SERVER['SERVER_NAME'];
					$key = md5 ('permataNet92');
					$encrypt = new Crypt_AES ();
					// print_r($this->_userId);
					// $user = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $this->_userIdLogin, MCRYPT_MODE_ECB);
					// $cust = ( $encrypt->encrypt ( $this->_custIdLogin ) );
					// $user = ( $encrypt->encrypt ( $this->_userIdLogin ) );

					$cust = ( $this->sslEnc(  $this->_custIdLogin ) );
					$user = ( $this->sslEnc( $this->_userIdLogin ) );
					$url_fo 	= $setting->getSetting('url_fo');
					// $dateenc = ( $encrypt->encrypt ( $date ) );
					
					// echo $result; die;
			 		$newPassword = $url_fo.'/googleauth/index/validatedelete?safetycheck=&code='.urldecode($rand).'&cust_id='.urlencode($cust).'&user_id='.urlencode($user);
			 		// print_r($newPassword);die;
			 		$datenow = date('d-M-Y H:i:s', strtotime("+1 days"));
					$data = array(  '[[comp_accid]]' 				=> $this->_custIdLogin,
									'[[user_login]]' 				=> $this->_userIdLogin,
									'[[user_email]]' 				=> $USER_EMAIL,
									'[[user_fullname]]' 			=> $userData['USER_FULLNAME'],
									'[[exp_date]]'					=> '',
									'[[user_cleartext_password]]' 	=> '',
									'[[master_bank_app_name]]'		=> $templateEmailMasterBankAppName,
									'[[master_bank_name]]' 			=> $templateEmailMasterBankName,
									'[[master_bank_email]]' 		=> $templateEmailMasterBankEmail,
									'[[master_bank_telp]]' 			=> $templateEmailMasterBankTelp,
									'[[master_bank_wapp]]' 			=> $templateEmailMasterBankWapp,
									'[[confirm_link]]'				=> $newPassword
								 );
					// print_r($data);
					$mailContent  = strtr($mailTemplate, $data);
					// echo $mailContent;die;
					//$mainResponse = Application_Helper_Email::sendEmail($USER_EMAIL, 'Remove Google Auth Information', $mailContent);

					$msg = 'Please validate Token Auth remove action by clicking on the link we have sent to your email at: <p class="text-primary">'.$this->maskingEmail($USER_EMAIL).'</p>';
					$headmsg = 'Mobile token has been revoked';
					$sessionNamespace = new Zend_Session_Namespace('resultMsg');
					$sessionNamespace->msg = $msg;
					$sessionNamespace->headmsg = $headmsg;

					$this->_redirect('/tokenauth/index/emailsuccess');

	  		}
			// }
	  			




			}	
		}
		$this->view->token_apli = '1';
		Application_Helper_General::writeLog('CHME','Change My Email');
	}

	public function maskingEmail($email){
		$emailSplit = explode('@', $email);
		$length = strlen($emailSplit[0]);

		$maskedStrLength = round($length/2);

		$strMasked = '';
		for($i=0; $i<$maskedStrLength; $i++){
			$strMasked .= '*';
		}

		return substr_replace($email,$strMasked,$maskedStrLength-1).'@'.$emailSplit[1];
	}


	public function resultAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}

		// Zend_Session::namespaceUnset('confirmTransact');
		
		// $sessionNameConfrim->bank_code	   = $param['bank_code'];
		// $this->view->desc  		= $sessionNameConfrim->desc;

    }

    public function resultsuccessAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}

    	$sessionNamespace = new Zend_Session_Namespace('resultMsg');
		

		if (!empty($sessionNamespace->msg)) {
			$this->view->msg = $sessionNamespace->msg;
		}
		else{
			$this->view->msg = 'Token Authentication Success';
		}

		Zend_Session::namespaceUnset('resultMsg');
		
		// $sessionNameConfrim->bank_code	   = $param['bank_code'];
		// $this->view->desc  		= $sessionNameConfrim->desc;

    }

    public function emailsuccessAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}

    	$sessionNamespace = new Zend_Session_Namespace('resultMsg');
		$this->view->msg = $sessionNamespace->msg;
		$this->view->headmsg = $sessionNamespace->headmsg;
		

		Zend_Session::namespaceUnset('resultMsg');

		// Zend_Session::namespaceUnset('confirmTransact');
		
		// $sessionNameConfrim->bank_code	   = $param['bank_code'];
		// $this->view->desc  		= $sessionNameConfrim->desc;

    }

    public function resultfailedAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}

		// Zend_Session::namespaceUnset('confirmTransact');
		
		// $sessionNameConfrim->bank_code	   = $param['bank_code'];
		// $this->view->desc  		= $sessionNameConfrim->desc;

    }



    

    public function validateauthAction()
	{
		// if(Zend_Auth::getInstance()->hasIdentity()){
		// 	$this->_redirect ( '/home/dashboard' );
		// }
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

		$cust = $this->getRequest()->getParam('cust_id');
		$user = $this->getRequest()->getParam('user_id');
		$code = urldecode($this->getRequest()->getParam('code'));
		$type = urldecode($this->getRequest()->getParam('type'));
		// print_r($user);die;
		// print_r($this->getRequest()->getParams());die;

		$key = md5 ('permataNet92');
		// $pass = ($zf_filter_input->password);

		// $encrypt = new Crypt_AESMYSQL ();
		$encrypt = new Crypt_AES ();
		// echo "<pre>";
		// print_r($encrypt);die;
		// $decrypcust_id =  ( $encrypt->decrypt ( $cust ) );

		// $decrypuser_id = ( $encrypt->decrypt ( $user ) );

		$decrypcust_id =  ( $this->sslDec ( $cust ) );

		$decrypuser_id = ( $this->sslDec ( $user ) );
		
		$settings =  new Settings();
		


		$select = $this->_db->select()
					   ->from(array('A' => 'M_USER'));
					   // ->joinLeft(array('G' => 'M_GROUPING'),'A.GROUP_ID = G.GROUP_ID',array('GROUP_NAME'));
		$select->where("A.CUST_ID = ".str_replace("\0", '',$this->_db->quote($decrypcust_id)));
		$select->where("A.USER_ID = ".str_replace("\0", '',$this->_db->quote($decrypuser_id)));
		// $select->where("A.GOOGLE_CODE IS NULL");

		$select->where("DATE(A.USER_DATEPASS) >= DATE(NOW())");
		 //echo "<pre>";
  		// echo $select;die;
  		$data = $this->_db->fetchOne($select);
  		// print_r($data);die;
  		if(!empty($data)){
			
  			try {
  					$dataupdate = array(
									
									'GOOGLE_CODE'					=> $type
									// 'USER_DATEPASS'					=> NULL
					);
// str_replace(search, replace, subject)
					$where =  array();
					$cust = str_replace("\0", "",$decrypcust_id);
					$user = str_replace("\0", "",$decrypuser_id);
					$where['CUST_ID 		 = ?'] 	= $cust;
					$where['USER_ID 		 = ?'] 	= $user;
					$result = $this->_db->update('M_USER',$dataupdate,$where);
				// if(Zend_Auth::getInstance()->hasIdentity()){
				//	 var_dump($result);
				//	 var_dump($dataupdate);
				//	 var_dump($where);die;

					$msg = 'Your Google Auth has been activated.';

					$sessionNamespace = new Zend_Session_Namespace('resultMsg');
					$sessionNamespace->msg = $msg;

					if($result){
						//die('here');
						if(empty($this->_userIdLogin)){
							//die('here');
							//$this->_redirect('/default/index/logout');	
							$this->_redirect('/default/index/logout');	
						}else{
							//die('here1');
							//$this->_redirect('/default/index/logout');
							$select	= $this->_db->select()
										->from(array('U'	 			=> 'M_USER'),
											   array('USER_FULLNAME'	=> 'U.USER_FULLNAME',
											   		 'USER_EMAIL' 		=> 'U.USER_EMAIL',
											   		)
											   )
										->where("U.CUST_ID 		= ?", (string) $this->_custIdLogin)
										->where("U.USER_ID 		= ?", (string) $this->_userIdLogin);
							$userData = $this->_db->fetchRow($select);
							Application_Helper_General::writeLog('GADL','Add Google Auth by Customer, User = '.$userData['USER_FULLNAME'].' ('.$this->_userIdLogin.'), Company Code = '.$this->_custIdLogin.', Email = '.$userData['USER_EMAIL']);

							$this->_redirect('/googleauth/index/resultsuccess');	
						}
						

					}else{
						$this->_redirect('/default/index/logout');	
					}
				 
  			} catch (Exception $e) {
  				var_dump($e);die;
  			}
  					
				// }else{
				// $this->redirect('/');
				// }
		}else{
			// die('here');
			$this->_redirect('/default/index/logout');	
			$this->view->accDisplay = true;

		}

	}


    public function validatedeleteAction()
	{
		// if(Zend_Auth::getInstance()->hasIdentity()){
		// 	$this->_redirect ( '/home/dashboard' );
		// }
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

		$cust = $this->getRequest()->getParam('cust_id');
		$user = $this->getRequest()->getParam('user_id');
		$code = urldecode($this->getRequest()->getParam('code'));
		// print_r($user);die;
		// print_r($this->getRequest()->getParams());die;

		$key = md5 ('permataNet92');
		// $pass = ($zf_filter_input->password);

		// $encrypt = new Crypt_AESMYSQL ();
		$encrypt = new Crypt_AES ();
		// echo "<pre>";
		// print_r($encrypt);die;
		$decrypcust_id =  ( $this->sslDec ( $cust ) );

		$decrypuser_id = ( $this->sslDec ( $user ) );
		// $decrypcust_id =  ( $encrypt->decrypt ( $cust ) );

		// $decrypuser_id = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $user, MCRYPT_MODE_ECB);
		// $decrypuser_id =   ( $encrypt->decrypt ( $user ) );
		
		$settings =  new Settings();
		
		$select = $this->_db->select()
					   ->from(array('A' => 'M_USER'));
					   // ->joinLeft(array('G' => 'M_GROUPING'),'A.GROUP_ID = G.GROUP_ID',array('GROUP_NAME'));
		$select->where("A.CUST_ID = ".str_replace("\0", '',$this->_db->quote($decrypcust_id)));
		$select->where("A.USER_ID = ".str_replace("\0", '',$this->_db->quote($decrypuser_id)));
		// $select->where("A.GOOGLE_CODE IS NOT NULL");

		$select->where("DATE(A.USER_DATEPASS) >= DATE(NOW())");
  		// echo $select;die;
  		$data = $this->_db->fetchOne($select);
  		// print_r($data);die;
  		if(!empty($data)){
  			try {
  					$dataupdate = array(
									
									'GOOGLE_CODE'					=> NULL
									// 'USER_DATEPASS'					=> NULL
					);
// str_replace(search, replace, subject)
					$where =  array();
					$cust = str_replace("\0", "",$decrypcust_id);
					$user = str_replace("\0", "",$decrypuser_id);
					$where['CUST_ID 		 = ?'] 	= $cust;
					$where['USER_ID 		 = ?'] 	= $user;
					$result = $this->_db->update('M_USER',$dataupdate,$where);
				// if(Zend_Auth::getInstance()->hasIdentity()){
					// var_dump($result);
					// var_dump($dataupdate);
					// var_dump($where);

					$msg = 'Your Token Auth has been removed.';

					$sessionNamespace = new Zend_Session_Namespace('resultMsg');
					$sessionNamespace->msg = $msg;

					if($result){

						if(empty($this->_userIdLogin)){
							//die('here');
							//$this->_redirect('/default/index/logout');	
							$this->_redirect('/default/index/logout');	
						}else{
							//die('here1');
							//$this->_redirect('/default/index/logout');
							$select	= $this->_db->select()
										->from(array('U'	 			=> 'M_USER'),
											   array('USER_FULLNAME'	=> 'U.USER_FULLNAME',
											   		 'USER_EMAIL' 		=> 'U.USER_EMAIL',
											   		)
											   )
										->where("U.CUST_ID 		= ?", (string) $this->_custIdLogin)
										->where("U.USER_ID 		= ?", (string) $this->_userIdLogin);
							$userData = $this->_db->fetchRow($select);
							Application_Helper_General::writeLog('GADL','Delete Token Auth by Customer, User = '.$userData['USER_FULLNAME'].' ('.$this->_userIdLogin.'), Company Code = '.$this->_custIdLogin.', Email = '.$userData['USER_EMAIL']);

							$this->_redirect('/tokenauth/index/resultsuccess');	
						}
						// $this->_redirect('/googleauth/index/resultsuccess');	
					}else{
						$this->_redirect('/default/index/logout');				
					}
				 
  			} catch (Exception $e) {
  				var_dump($e);die;
  			}
  					
				// }else{
				// $this->redirect('/');
				// }
		}else{
			// die('here');
			$this->_redirect('/default/index/logout');	
			$this->view->accDisplay = true;

		}

	}

	public function sslPrm()
	{
	return array("6a1f325be4c0492063e83a8cb2cb9ae7","IV (optional)","aes-128-cbc");
	}

	public function sslDec($msg)
	{
	  list ($pass, $iv, $method)=$this->sslPrm();
		 return trim(urldecode(openssl_decrypt(urldecode($msg), $method, $pass, false, $iv)));
	}

	public function sslEnc($msg)
	{
	  list ($pass, $iv, $method)=$this->sslPrm();
		 return urlencode(openssl_encrypt(urlencode($msg), $method, $pass, false, $iv));
	}





}

