<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';
require_once 'General/CustomerUser.php';

class Incoming_ActivationController extends Application_Main
{
	protected $_moduleDB = 'RTF';
	
	public function setPaymentType($paymentType, $transferType)
	{
		$filterType		 = array();
		
		foreach($paymentType["code"] as $key => $val)
		{	
			//|| $key == "remittance"
			if ($key == "within" || $key == "domestic" || $key == "remittance" || $key == "purchase" || $key == "payment" )
			{
				$filterType[$paymentType["code"][$key]] = $paymentType["desc"][$key];
			}
			elseif ($key == "multicredit")
			{
				$code = $paymentType["code"]["multicredit"].",".$paymentType["code"]["bulkcredit"];
				$desc = $paymentType["desc"][$key];
				$filterType["{$code}"] = $desc;
			}
			elseif ($key == "multidebet")
			{
				$code = $paymentType["code"]["multidebet"].",".$paymentType["code"]["bulkdebet"];
				$desc = $paymentType["desc"][$key];
				$filterType["{$code}"] = $desc;
			}
		}
		
		return $filterType;
	}
	
	public function indexAction()
	{	
		$this->_helper->layout()->setLayout('newlayout');
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}	
		
		$CustomerUser 	= new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$filter 		= new Application_Filtering();
		
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr 	  = $CustomerUser->getAccounts(array("ACCT_TYPE" => array('10','20')));	// show acc in IDR only
		$this->view->AccArr 			= $AccArr;
		
		
		$ProductTypeArr = array(
		    '1' => $this->language->_('Cheque'),
		    '2' => $this->language->_('Bilyet Giro'),
		    );
		$this->view->ProdType 			= $ProductTypeArr;
		
		
		$StatusArr = array(
		    '1' => $this->language->_('Active'),
		    '2' => $this->language->_('Draw'),
		    '3' => $this->language->_('Ready'),
		);
		$this->view->StatusArr 			= $StatusArr;
		$this->view->tokentype 			= '2';
		
		
		$opt[""] = "-- " .$this->language->_('Please Select'). " --";
		
		$payType 	= Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
		//$optpaytype 	= $this->setPaymentType($this->_paymenttype,$this->_transfertype);
		$arrPayStatus	= array_combine($this->_paymentstatus["code"], $this->_paymentstatus["desc"]);
		$trfType 		= array_combine($this->_transfertype["code"], $this->_transfertype["desc"]);
		$arrAccount 	= $CustomerUser->getAccounts();
		
		foreach($payType as $key => $value){ 
// 			if($key != 3) $optpaytypeRaw[$key] = $this->language->_($value);
				
			 $optpaytypeRaw[$key] = $this->language->_($value); 
		}
		//foreach($trfType as $key => $value){ if($key != 3 && $key != 4) $filterTrfType[$key] = $this->language->_($value); }
		foreach($trfType as $key => $value){ $filterTrfType[$key] = $this->language->_($value); }
		foreach($arrPayStatus as $key => $value){ if($key != 3) $optpayStatusRaw[$key] = $this->language->_($value); }
		
		if(is_array($arrAccount) && count($arrAccount) > 0){
			foreach($arrAccount as $key => $value){
				
				$val 		= $arrAccount[$key]["ACCT_NO"];
				$ccy 		= $arrAccount[$key]["CCY_ID"];
				$acctname 	= $arrAccount[$key]["ACCT_NAME"];
				//$acctalias 	= $arrAccount[$key]["ACCT_ALIAS_NAME"];
				$accttype 	= ($arrAccount[$key]["ACCT_TYPE"] == '10') ? 'SAVING': 'GIRO';	// 10 : saving, 20 : giro;
				
				$arrAccountRaw[$val] = $val.' ['.$ccy.'] '.$acctname.' ('.$accttype.')';
				
			}
		}
		else { $arrAccountRaw = array();}
		
		
		unset ($arrPayStatus[10]);
		unset ($arrPayStatus[15]);
		unset ($arrPayStatus[16]);
		
		$optPayType 	= $opt + $optpaytypeRaw;
		
		//$optPayStatus 	= $opt + $arrPayStatus;
		$optPayStatus 	= $opt + $optpayStatusRaw;
		unset($optPayStatus[10]);
		unset($optPayStatus[15]);
		unset($optPayStatus[16]);
		
		//hilangkan payment type payroll, Sweep In, Sweep Out
		$optPayTypePayroll = "11,11";
		$optPayTypeSweepIn = "14,14";
		$optPayTypeSweepOut = "15,15";
		$optPayTypeCredit = "6,4";
		$optPayTypeDebit = "7,5";
		//unset($optPayType[$optPayTypePayroll]);
		//unset($optPayType[$optPayTypeSweepIn]);
		//unset($optPayType[$optPayTypeSweepOut]);
		
		//unset($optPayType[$optPayTypeDebit]);
		//unset($optPayType[$optPayTypeCredit]);
		
		$optarrAccount 	= $opt + $arrAccountRaw;
		
		$this->view->optPayType 	= $optPayType;
		$this->view->optPayStatus 	= $optPayStatus;
		$this->view->optarrAccount 	= $optarrAccount;		
		
		$fields = array	(	'payReff'  					=> array	(
																		'field' => 'payReff',
																		'label' => $this->language->_('CH/BG No'),
																		'sortable' => true
																),
							'created'  					=> array	(
																		'field' => 'created',
																		'label' => $this->language->_('Status'),
																		'sortable' => true
																	),
							'paySubj'  					=> array	(
																		'field' => 'paySubj',
																		'label' => $this->language->_('Disburse Date'),
																		'sortable' => true
																	),	
							'accsrc'  					=> array	(
																		'field' => 'accsrc',
																		'label' => $this->language->_('Date Time Activation'),
																		'sortable' => true
																	),
							'accsrc_name' 				=> array	(
																		'field' => 'accsrc_name',
																		'label' => $this->language->_('Number Of Sheets'),
																		'sortable' => true
																	),
							'acbenef'  					=> array	(
																		'field' => 'acbenef',
																		'label' => $this->language->_('Seri'),
																		'sortable' => true
																	),
							'acbenef_name'  			=> array	(
																		'field' => 'acbenef_name',
																		'label' => $this->language->_('Action'),
																		'sortable' => true
																	),
							
						);
		
		//get page, sortby, sortdir
		$page    		= $this->_getParam('page');
		$sortBy  		=($this->_getParam('sortby')) ?  $this->_getParam('sortby') : ('updated');
		$sortDir 		= $this->_getParam('sortdir');
		$filter 		= $this->_getParam('filter');
		$clearfilter	= $this->_getParam('clearfilter');
		
		$csv 			= $this->_getParam('csv');
		$pdf 			= $this->_getParam('pdf');
		
		$this->view->sortBy			= $sortBy;
		$this->view->sortDir 		= $sortDir;
		$this->view->filter			= $filter;
		$this->view->clearfilter	= $clearfilter;
		
		//validate parameters before passing to view and query
		$page 		= (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
		$sortBy 	= (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir 	= (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'desc';	
		
		$this->view->currentPage = $page;
		
		$filterArr = array(	'updatedStart' 	=> array('StringTrim','StripTags'),
							'updatedEnd' 	=> array('StringTrim','StripTags'),
							'createdStart' 	=> array('StringTrim','StripTags'),
							'createdEnd' 	=> array('StringTrim','StripTags'),
							'paymentStart' 	=> array('StringTrim','StripTags'),
							'paymentEnd' 	=> array('StringTrim','StripTags'),
							'accsrc' 		=> array('StringTrim','StripTags'),
							'payReff' 		=> array('StringTrim','StripTags','StringToUpper'),
							'paymentStatus'	=> array('StringTrim','StripTags'),
							'paymentType' 	=> array('StringTrim','StripTags'),
							'transferType' 	=> array('StringTrim','StripTags'),
							
						
		);
		
		// if POST value not null, get post, else get param
		$dataParam = array("updatedStart","updatedEnd","createdStart","createdEnd","paymentStart","paymentEnd","accsrc","payReff","paymentStatus","paymentType","transferType");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{
			$dataPost = $this->_request->getPost($dtParam);
			$dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}	
		
		// The default is set so all fields allow an empty string		
		$options = array('allowEmpty' => true);
		$validators = array(
						'updatedStart' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'updatedEnd' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'createdStart'	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'createdEnd' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'paymentStart' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'paymentEnd' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'accsrc' 		=> array(array('InArray', array('haystack' => array_keys($optarrAccount)))),	// $filter!
						'payReff'		=> array(),	
						'paymentStatus' => array(array('InArray', array('haystack' => array_keys($optPayStatus)))),	// $filter!
						'paymentType' 	=> array(array('InArray', array('haystack' => array_keys($optPayType)))),	// $filter!
						'transferType' => array(array('InArray', array('haystack' => array_keys($filterTrfType)))),	// $filter!
							
						);
		
		$zf_filter  = new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);
		
		
		$fCreatedStart 	= $zf_filter->getEscaped('createdStart');
		$fCreatedEnd 	= $zf_filter->getEscaped('createdEnd');
		$fPaymentStart 	= $zf_filter->getEscaped('paymentStart');
		$fPaymentEnd 	= $zf_filter->getEscaped('paymentEnd');
		
		$fAcctsrc 		= $zf_filter->getEscaped('accsrc');
	
		$fPaymentReff 	= $zf_filter->getEscaped('payReff');
		$fPaymentStatus = $zf_filter->getEscaped('paymentStatus');
		$fPaymentType 	= $zf_filter->getEscaped('paymentType');
		$fTransferType 	= $zf_filter->getEscaped('transferType');
		
		
		if($filter == NULL && $clearfilter != 1){
			$fUpdatedStart 	= date("d/m/Y");
			$fUpdatedEnd 	= date("d/m/Y");
			
			// echo "f0 c1";
		}
		else{
		
			if($filter != NULL){
				$fUpdatedStart 	= $zf_filter->getEscaped('updatedStart');
				$fUpdatedEnd 	= $zf_filter->getEscaped('updatedEnd');
				// echo "f1 c0";
			}
			else if($clearfilter == 1){
				$fUpdatedStart 	= "";
				$fUpdatedEnd 	= "";
				// echo "f0 c0";
			}
		}
		
		
		//jika tombol csv dan pdf ditekan
		if($pdf || $csv)
		{
		    $fUpdatedStart 	= $zf_filter->getEscaped('updatedStart');
		    $fUpdatedEnd 	= $zf_filter->getEscaped('updatedEnd');
		}
		
		$paramPayment = array("WA" 				=> false,
							  "ACCOUNT_LIST" 	=> $this->_accountList,
							  "_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
							 );
		
		// get payment query
		$select   = $CustomerUser->getPayment($paramPayment);
		
		// Filter Data
		if($fUpdatedStart)
		{
			$FormatDate 	= new Zend_Date($fUpdatedStart, $this->_dateDisplayFormat);
			$updatedFrom   	= $FormatDate->toString($this->_dateDBFormat);	
			$select->where('DATE(P.PS_UPDATED) >= ?', $updatedFrom);
		}
		
		if($fUpdatedEnd)
		{
			$FormatDate 	= new Zend_Date($fUpdatedEnd, $this->_dateDisplayFormat);
			$updatedTo   	= $FormatDate->toString($this->_dateDBFormat);	
			$select->where('DATE(P.PS_UPDATED) <= ?', $updatedTo);
		}
		
		if($fCreatedStart)
		{
			$FormatDate 	= new Zend_Date($fCreatedStart, $this->_dateDisplayFormat);
			$createdFrom   	= $FormatDate->toString($this->_dateDBFormat);	
			$select->where('DATE(P.PS_CREATED) >= ?', $createdFrom);
		}
		
		if($fCreatedEnd)
		{
			$FormatDate 	= new Zend_Date($fCreatedEnd, $this->_dateDisplayFormat);
			$createdTo   	= $FormatDate->toString($this->_dateDBFormat);	
			$select->where('DATE(P.PS_CREATED) <= ?', $createdTo);
		}
		
		if($fPaymentStart)
		{
			$FormatDate 	= new Zend_Date($fPaymentStart, $this->_dateDisplayFormat);
			$payDateFrom   	= $FormatDate->toString($this->_dateDBFormat);	
			$select->where('DATE(P.PS_EFDATE) >= ?', $payDateFrom);
		}
		
		if($fPaymentEnd)
		{
			$FormatDate 	= new Zend_Date($fPaymentEnd, $this->_dateDisplayFormat);
			$payDateTo  	= $FormatDate->toString($this->_dateDBFormat);	
			$select->where('DATE(P.PS_EFDATE) <= ?', $payDateTo);
		}
		
		if($fAcctsrc)
	{	$select->where("(select count(PS_NUMBER) from T_TRANSACTION where PS_NUMBER = P.PS_NUMBER and SOURCE_ACCOUNT = ?) > 0", (string)$fAcctsrc);		}
//		{	$select->where("T.SOURCE_ACCOUNT LIKE ".$this->_db->quote('%'.$fAcctsrc.'%'));	}	// ACCSRC ?????
		
		if($fPaymentReff)
		{	$select->where("UPPER(P.PS_NUMBER) LIKE ".$this->_db->quote('%'.$fPaymentReff.'%'));	}	
		
		if($fPaymentStatus)
		{	
			//$fPayStatus	 	= explode(",", $fPaymentStatus);
			$select->where("P.PS_STATUS = ? ", $fPaymentStatus);					
		}
		
		if($fPaymentType)
		{	
			$fPayType 	 	= explode(",", $fPaymentType);
			$select->where("P.PS_TYPE in (?) ", $fPayType);					
			
		}

		if($fTransferType != '')
		{
		//	$select->where("(select count(PS_NUMBER) from T_TRANSACTION where PS_NUMBER = P.PS_NUMBER and TRANSFER_TYPE = ?) > 0", (string)$fTransferType);
			$select->where("T.TRANSFER_TYPE = ? ", $fTransferType);
		}
		
		//echo $select;die;		
		
		$select->order($sortBy.' '.$sortDir);

		$dataSQL = $this->_db->fetchAll($select);
		
		if ($csv || $pdf)
		{	$header  = Application_Helper_Array::simpleArray($fields, "label");		}
		else
		{
			// $this->paging($dataSQL);	
			// $dataSQL = $this->view->paginator;
		}
		
		$data = array();
 	//	echo '<pre>';
 	//	print_r($dataSQL);die;
		foreach ($dataSQL as $d => $dt)
		{
							//print_r($dt);die;
			$persenLabel = $dt["BALANCE_TYPE"] == '2' ? ' %' : '';
			foreach ($fields as $key => $field)
			{
				$value 	   = $dt[$key];
				$PSSTATUS  = $dt["PS_STATUS"];
				$PSNUMBER  = $dt["payReff"];
				$payStatus = $dt["payStatus"];
				
				if ($key == "payStatus"){
					if($PSSTATUS == 5){
						
						// COMPLETED WITH (_) TRANSACTION (S) FAILED 
						// TRA_STATUS FAILED (4)
						
						$select = $this->_db->select()
											->from('T_TRANSACTION',array('countfailed'=>'count(TRANSACTION_ID)'))
											->where("TRA_STATUS = '4' AND PS_NUMBER = ?",$PSNUMBER);
						$countFailed = $this->_db->fetchOne($select);
						
						if($countFailed == 0) 	$value = $payStatus;
						else 					$value = 'Completed with '.$countFailed.' Failed Transaction(s)';
						
					}
					else $value = $payStatus;
					
				}

				if ($key == "amount" && !$csv)	{
					//print_r($dt['PS_STATUS']);die;
					if(!empty($persenLabel)){
						if(($dt['PS_TYPE']=='14' || $dt['PS_TYPE']=='15')){
							if($dt['PS_STATUS'] == '5' || $dt['PS_STATUS'] == '1' || $dt['PS_STATUS'] == '2'){
							$value = Application_Helper_General::displayMoney($value).$persenLabel.' ( IDR '.Application_Helper_General::displayNumber($dt['PS_REMAIN']).')';
							}else{
							$value = '-';
							}
						}else{
							$value = Application_Helper_General::displayMoney($value).$persenLabel.' ( IDR '.Application_Helper_General::displayNumber($dt['PS_REMAIN']).')';

						}
					}elseif(($dt['PS_TYPE']=='14' || $dt['PS_TYPE']=='15') ){
							
							if($dt['PS_STATUS'] == '5' || $dt['PS_STATUS'] == '1' || $dt['PS_STATUS'] == '2'){
							$value = Application_Helper_General::displayMoney($dt['PS_REMAIN']);
							}else{
							$value = '-';
							}
					}else{
						$value = Application_Helper_General::displayMoney($value);
					}
					 
				
				}
				elseif ($key == "amount" && $csv)	{
						if(!empty($persenLabel)){
						if(($dt['PS_TYPE']=='14' || $dt['PS_TYPE']=='15')){
							if($dt['PS_STATUS'] == '5' || $dt['PS_STATUS'] == '1' || $dt['PS_STATUS'] == '2'){
							$value = Application_Helper_General::displayMoney($value).$persenLabel.' ( IDR '.Application_Helper_General::displayNumber($dt['PS_REMAIN']).')';
							}else{
							$value = '-';
							}
						}else{
							$value = Application_Helper_General::displayMoney($value).$persenLabel.' ( IDR '.Application_Helper_General::displayNumber($dt['PS_REMAIN']).')';

						}
						}elseif(($dt['PS_TYPE']=='14' || $dt['PS_TYPE']=='15')){
							if($dt['PS_STATUS'] == '5' || $dt['PS_STATUS'] == '1' || $dt['PS_STATUS'] == '2'){
							$value = Application_Helper_General::displayMoney($dt['PS_REMAIN']);
							}else{
							$value = '-';
							}
		
						}else{
							$value = Application_Helper_General::displayMoney($value);
						}
					}				
				elseif ($key == "created")		{ $value = Application_Helper_General::convertDate($value,$this->view->displayDateTimeFormat); }
				elseif ($key == "efdate")		{ $value = Application_Helper_General::convertDate($value,$this->_dateViewFormat,$this->view->defaultDateFormat); }
				elseif ($key == "updated")		{ $value = Application_Helper_General::convertDate($value,$this->view->displayDateTimeFormat); }
				// elseif ($key == "accsrc_name")	{			
					// $alias_name  = (!empty($dt["accsrc_alias"]) && $dt["accsrc_alias"] != "-")? " / ".$dt["accsrc_alias"]: "";
					// $value		 = trim($value).$alias_name;
				// }
				// elseif ($key == "acbenef_name"){			
					// $alias_name  = (!empty($dt["acbenef_alias"]) && $dt["acbenef_alias"] != "-")? " / ".$dt["acbenef_alias"]: "";
					// $value		 = trim($value).$alias_name;
				// }
				
				$value = ($value == "" && !$csv)? "&nbsp;": $value;
				$data[$d][$key] = $this->language->_($value);
			}
		
		}
		
		$this->paging($data);	
		
		if($csv)
		{
			$this->_helper->download->csv($header,$data,null,'List View Payment');  
			Application_Helper_General::writeLog('DARC','Export CSV View Payment');
		}
		elseif($pdf)
		{
			$this->_helper->download->pdf($header,$data,null,'List View Payment');  
			Application_Helper_General::writeLog('DARC','Export PDF View Payment');
		}
		else
		{	
			$stringParam = array(
								'updatedStart' 	=> $fUpdatedStart,
								'updatedEnd'	=> $fUpdatedEnd,
								'createdStart'	=> $fCreatedStart,
								'createdEnd'	=> $fCreatedEnd,		
								'paymentStart'	=> $fPaymentStart,		
								'paymentEnd'	=> $fPaymentEnd,		
								'accsrc'		=> $fAcctsrc,	
								'payReff'		=> $fPaymentReff,
								'paymentStatus'	=> $fPaymentStatus,		
								'paymentType'	=> $fPaymentType,	
								'transferType'	=> $fTransferType,
								'clearfilter'	=> $clearfilter,
							
								);
			/*
			$stringParam = array('payReff'		=> $fPaymentReff,
								 'payType'		=> $fPaymentType,
								 'trfType'		=> $fTrfType,
								 'createdFrom'	=> $fCreatedFrom,
								 'createdTo'	=> $fCreatedTo,
								 'payDateFrom'	=> $fPayDateFrom,
								 'payDateTo'	=> $fPayDateTo,
							    );	
			*/
			
			//echo "fPaymentStatus: $fPaymentStatus";
			//echo "fAcctsrc: $fAcctsrc";
			//echo "fPaymentType: $fPaymentType";
			
			// $this->view->filterPayType 		= $filterPayType;
			// $this->view->optfilterPayStatus 	= $optfilterPayStatus;
			// $this->view->CustomerArr 		= $CustomerArr;
			
			
			$this->view->optPayType 	= $optPayType;
			$this->view->optPayStatus 	= $optPayStatus;
			$this->view->optarrAccount 	= $optarrAccount;
		
			
			$this->view->updatedStart 	= $fUpdatedStart;
			$this->view->updatedEnd 	= $fUpdatedEnd;
			$this->view->createdStart 	= $fCreatedStart;
			$this->view->createdEnd 	= $fCreatedEnd;
			$this->view->paymentStart 	= $fPaymentStart;
			$this->view->paymentEnd 	= $fPaymentEnd;
			$this->view->accsrc 		= $fAcctsrc;
			$this->view->payReff 		= $fPaymentReff;
			$this->view->paymentStatus 	= $fPaymentStatus;
			$this->view->paymentType 	= $fPaymentType;
			$this->view->transferType 	= $fTransferType;
			
			//$this->view->query_string_params = $stringParam;
			
			$this->view->data 				= $data;
			$this->view->fields 			= $fields;
			$this->view->filter 			= $filter;
	        $this->view->sortBy 			= $sortBy;
			$this->view->sortDir 			= $sortDir;
			
			$this->view->clearfilter 		= $clearfilter;
			
			// $this->view->currentPage 		= $page;
			// $this->view->paginator 			= $data;
			
			Application_Helper_General::writeLog('DARC','View Payment');
			
			//$this->view->sortBy = $sortBy;
			//$this->view->sortDir = $sortDir;
			
		}
		
	}
}