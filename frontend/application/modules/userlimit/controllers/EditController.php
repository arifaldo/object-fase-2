<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';
require_once 'Crypt/AESMYSQL.php';
class userlimit_EditController extends user_Model_User
{
  public function indexAction()
  {
    $this->_helper->layout()->setLayout('newlayout');
    $cust_id = strtoupper($this->_getParam('cust_id'));
    $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;
     $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
       $password = $sessionNamespace->token; 
       $this->view->token = $sessionNamespace->token;  


    $AESMYSQL = new Crypt_AESMYSQL();
    $PS_NUMBER      = urldecode($this->_getParam('user_id'));
    $user_id = $AESMYSQL->decrypt($PS_NUMBER, $password);

    $user_id = strtoupper($user_id);

    //$user_id = (Zend_Validate::is($user_id,'Alnum') && Zend_Validate::is($user_id,'StringLength',array('min'=>1,'max'=>25)))? $user_id : null;
    $error_remark = null;

    //get user data
    $select = $this->_db->select()
                           ->from('M_USER')
                           ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                           ->where('UPPER(USER_ID)='.$this->_db->quote((string)$user_id));
    $resultdata = $this->_db->fetchRow($select);

    if($cust_id)
    {
      $select = $this->_db->select()
                             ->from('M_CUSTOMER',array('CUST_ID'))
                             ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                             ->where('CUST_STATUS = ?', '1');
      $result = $this->_db->fetchOne($select);
      if(!$result)$cust_id = null;
    }

    if(!$cust_id)
    {
      $error_remark = 'Cust ID is not found';
      //insert log
      Application_Helper_General::writeLog('ADML','Add Maker Limit');
      $this->_helper->getHelper('FlashMessenger')->addMessage('F');
      $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
      $this->_redirect($this->view->backURL);
    }

    if($user_id)
    {
      $select = $this->_db->select()
                           ->from('M_USER',array('USER_ID'))
                           ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                           ->where('UPPER(USER_ID)='.$this->_db->quote((string)$user_id));
                           //->where('USER_STATUS=1');
      $result = $this->_db->fetchOne($select);

      if($result)
      {
        $this->view->user_name = $resultdata['USER_FULLNAME'];
        $this->view->user_status = $resultdata['USER_STATUS'];

        // $selectLimit = $this->_db->select()
        //                         ->from(array('CA'=>'M_CUSTOMER_ACCT'))
        //                         ->joinLeft(array('M'=>'M_CUSTOMER_ACCT'),'CA.ACCT_NO = M.ACCT_NO',array('CA.ACCT_NO','ACCT_NAME','CCY_ID','M.MAXLIMIT'))
        //                         ->where('CA.CUST_ID = ?', $cust_id)
        //                         ->where('USER_LOGIN = ?' , $user_id)
        //                         //->where('MAKERLIMIT_STATUS <> 3')
        //                         ->where('ACCT_STATUS <> 3');


        $fields = array('acct_no'   => array('field' => 'ACCT_NO',
                                        'label'    => 'Account Number',
                                        'sortable' => true),

                    'acct_name' => array('field'    => 'ACCT_NAME',
                                        'label'    => 'Account Name',
                                        'sortable' => true),

                    'ccy' => array('field'    => 'CCY_ID',
                                        'label'    => 'Currency',
                                        'sortable' => true),
                   );

        $sortBy = $this->_getParam('sortby','acct_no');
        $sortDir = $this->_getParam('sortdir','asc');
        $sortBy = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
        $sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';

        $this->view->fields = $fields;
        $this->view->sortBy = $sortBy;
        $this->view->sortDir = $sortDir;

        $userLimit = $this->_db->select()
                               ->from(array('A' => 'M_CUSTOMER_ACCT'),array('ACCT_NO','ACCT_NAME','CCY_ID','MAXLIMIT' => '(SELECT MAXLIMIT FROM M_MAKERLIMIT WHERE USER_LOGIN=\''.$user_id.'\' AND CUST_ID=\''.$cust_id.'\' AND ACCT_NO=A.ACCT_NO AND MAKERLIMIT_STATUS = 1)'))
                               ->where('UPPER(A.CUST_ID)='.$this->_db->quote(strtoupper($cust_id)))
                               ->where('A.ACCT_STATUS <> 3')
                               ->order('A.ACCT_NO ASC')
                               ->query()->fetchAll();

        $this->view->userLimit = $userLimit;


        $select = $this->_db->select()
                           ->from('TEMP_MAKERLIMIT',array('CHANGES_ID'))
                           ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                           ->where('UPPER(USER_LOGIN)='.$this->_db->quote((string)$user_id));
        
        $result = $this->_db->fetchOne($select);

        if($result){
          $msg = array($this->language->_('No changes allowed for this record while awaiting approval for previous change.'));
          $this->view->isapproval = $this->displayError($msg);
        }
      }
      else{ $user_id = null; }
    }

    if(!$user_id)
    {
      if(!$error_remark)$error_remark = 'User ID is not found';
      //insert log
      Application_Helper_General::writeLog('ADML','Add Maker Limit');
      $this->_helper->getHelper('FlashMessenger')->addMessage('F');
      $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
      $this->_redirect($this->view->backURL);
    }


    if($this->_request->isPost())
    {

      $maxlimit = $this->_request->getParam('maxlimit');
      $error = false;
      $errorMsg = array();
    
    $select = $this->_db->select()
              ->from(array('A'      => 'M_DAILYLIMIT'),
                   array(
                       'LIMIT_AMT'  => 'A.DAILYLIMIT'
                      )
                   )
              ->where("A.DAILYLIMIT_STATUS = 1")
              ->where("A.CUST_ID    = ?", $cust_id)
              ->where("A.USER_LOGIN = ?", $this->_userIdLogin);
              
    $resultdaily = $this->_db->fetchOne($select);
    
      
      foreach($maxlimit as $key=>$value)
      {
        $maxlimit[$key] = $maxlimit[$key]?Application_Helper_General::convertDisplayMoney($value):null;
        if(!$maxlimit[$key]) 
        { 
          unset($maxlimit[$key]);
          continue;
        }
        if(!empty($resultdaily)){
      $dailylimit = $resultdaily;
    }else{
      $dailylimit = 9999999999999;
    }
    //echo '<pre>';
    //var_dump($resultdaily);
    //var_dump($dailylimit);die;
    
    
        if($maxlimit[$key] < 0 || $maxlimit[$key] > (int)$dailylimit)
        {
          $error = true;
       
          //$errorMsg[$key] = $this->language->_('Too many significant digits. Maximum digit allowed : 13 digit(s)');
      $errorMsg[$key] = $this->language->_('Maker limit amount is exceeding your daily limit amount').' (max  IDR '.Application_Helper_General::displayMoneyplain($dailylimit).')';
        }     
      }
      
      if($error)
      {
        $this->view->error = true;
        $this->view->errorMsg = $errorMsg;
        $this->view->maxlimit = $maxlimit;
        $this->view->report_msg = $this->language->_('Error in processing form values. Please correct values and re-submit.');
      }
      else
      {
        try
        {
          $info = 'User Limit, ID = '.$user_id; 
          $this->_db->beginTransaction();
      
          // insert ke T_GLOBAL_CHANGES
          $change_id = $this->suggestionWaitingApproval('User Limit',$info,$this->_changeType['code']['edit'],null,'M_MAKERLIMIT','TEMP_MAKERLIMIT',$user_id,$resultdata['USER_FULLNAME'],$cust_id);
          
          //LOOPING HERE JIKA ACCOUNT YANG DI LIMIT BANYAK
          foreach($maxlimit as $key=>$value)
          {
            $data = array('CHANGES_ID' => $change_id,
                    'USER_LOGIN' => $user_id,
                    'ACCT_NO' => (string)$key,
                    'CUST_ID' => $cust_id,
                    'MAXLIMIT' => $maxlimit[$key],
                    'SUGGESTED' => new Zend_Db_Expr('now()'), 
                    'SUGGESTEDBY' => $this->_userIdLogin, 
                    'MAKERLIMIT_STATUS' => 1
                  );
            $this->_db->insert('TEMP_MAKERLIMIT',$data);
          }
          //END LOOP
          
          
          Application_Helper_General::writeLog('ADML','Maker Limit added for Customer ID : '.$cust_id.', User ID : '.$user_id);
          $this->_db->commit();


          // $this->setBackURL($this->view->backURL);
          $this->_redirect('/notification/submited/index');
        }
        catch(Exception $e) 
        {
          //rollback changes
          $this->_db->rollBack();
          $this->view->error = 1;
          $this->view->maxlimit = $maxlimit;
          $this->view->report_msg = $this->language->_('An Error Occured. Please Try Again');
        }
      }
    }

    $this->view->cust_id = $cust_id;
    $this->view->user_id = $user_id;

    Application_Helper_General::writeLog('ADML','Add Maker Limit');
    
}


}