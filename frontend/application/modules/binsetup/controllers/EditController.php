<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';

class binsetup_EditController extends binsetup_Model_Binsetup 
{

    public function indexAction() 
    {
       //pengaturan url untuk button back
	   $this->setbackURL('/'.$this->_request->getModuleName().'/index');    
	    
       $cust_id = strtoupper($this->_getParam('cust_id'));
       $cust_bin = strtoupper($this->_getParam('cust_bin'));
       $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;
       $cust_view = 1; 
       $error_remark = null; 
       $flag = 0;
    
       //data customer
       $resultdata = $this->getCustomer($cust_id,$cust_bin);  
        if($resultdata['CUST_ID'])
          {
      	        $this->view->cust_id      = strtoupper($resultdata['CUST_ID']);
       			$this->view->cust_name    = $resultdata['CUST_NAME'];
       			if($resultdata['CUST_BIN_STATUS']=='1'){
                $resultdata['CUST_BIN_STATUS'] = 'Approved';
              }else if($resultdata['CUST_BIN_STATUS'] == '2'){
                $resultdata['CUST_BIN_STATUS'] = 'Suppend';
              }else{
                $resultdata['CUST_BIN_STATUS'] = 'Deleted';
              }
      		    $this->view->cust_bin_status   = strtoupper($resultdata['CUST_BIN_STATUS']);
      		    $this->view->cust_bin   = strtoupper($resultdata['CUST_BIN']);
      		    
      		    
      		    $this->view->cust_created     = $resultdata['BIN_CREATED'];
      		    $this->view->cust_createdby   = $resultdata['BIN_CREATEDBY'];
      		    $this->view->cust_suggested   = $resultdata['BIN_SUGGESTED'];
      		    $this->view->cust_suggestedby = $resultdata['BIN_SUGGESTEDBY'];
      		    $this->view->cust_updated     = $resultdata['BIN_UPDATED'];
      		    $this->view->cust_updatedby   = $resultdata['BIN_UPDATEDBY'];

              $this->view->bank   = $resultdata['BANK_NAME'];
              $this->view->branch   = $resultdata['BRANCH_NAME'];
              $this->view->acc_no   = $resultdata['ACCT_NO'];
              $this->view->acc_name   = $resultdata['ACCT_NAME'];
              $this->view->app_date   = $resultdata['APPROVED_DATE'];
              $this->view->ccy   = $resultdata['CCY'];
              $this->view->alias_name   = $resultdata['ACCT_ALIAS_NAME'];
              $this->view->bin_name   = $resultdata['BIN_NAME'];
			        
		}
       $expid 			= "CUST_BIN != ".$this->_db->quote($resultdata['CUST_BIN']);
       if($cust_id)
       {
			$tempCustomerId = $this->getTempCustomerId($cust_id);
			if($tempCustomerId) $error_remark = 'invalid format'; 
			else $flag = 1; 
       }
       else
       {
          $error_remark = 'invalid format'; 
       } 

      if($this->_request->isPost())
      {

      	// die;
         $haystack_cust_type = array($this->_custType['code']['individual'],$this->_custType['code']['company']);
		 $customer_filter = array();

            $filters = array(
			                 'bin_name'         => array('StripTags','StringTrim'),
			                 );

	     $validators =  array(
							 'bin_name'        => array('NotEmpty',
      	                                                       'messages' => array($this->language->_('Can not be empty'))
      	                                                      ),
						                                                                           
      	                                                      
							);

            $zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
            
            if($zf_filter_input->isValid())
            {
	           $info = "BIN";
               // $cust_data = $this->_custData;
	  	       foreach($validators as $key=>$value)
	  	       {
	  	          if($zf_filter_input->$key) $cust_data[strtoupper($key)] = $zf_filter_input->$key;
	  	       }
	  	       	// print_r($zf_filter_input->bin_name);die;
              try 
              {
              	// print_r($zf_filter_input->isValid('bin_name'));die;
		          $this->_db->beginTransaction();
		          $cust_data['CUST_ID']      = $resultdata['CUST_ID'];
		          $cust_data['CUST_BIN']      = $resultdata['CUST_BIN'];
		          $cust_data['CUST_BIN_TMP']      = $resultdata['CUST_BIN_TMP'];
		          // $cust_data['CUST_BIN_TMP']  = $this->_getParam('cust_bin');
		          
		          $cust_data['CUST_BIN_STATUS']      = '1';
		          $cust_data['BIN_CREATED']     = $resultdata['BIN_CREATED'];
		          $cust_data['BIN_CREATEDBY']   = $resultdata['BIN_CREATEDBY'];
		          $cust_data['BIN_UPDATED']     = $resultdata['BIN_UPDATED'];
		          $cust_data['BIN_UPDATEDBY']   = $resultdata['BIN_UPDATEDBY'];
		          $cust_data['BIN_SUGGESTED']    = new Zend_Db_Expr('now()');
		          $cust_data['BIN_SUGGESTEDBY']  = $this->_userIdLogin;

		          // print_r($resultdata);
		          $cust_data['BANK_NAME']   = $resultdata['BANK_NAME'];
		          $cust_data['BRANCH_NAME']   = $resultdata['BRANCH_NAME'];
		          $cust_data['ACCT_NO']   = $resultdata['ACCT_NO'];
		          $cust_data['ACCT_NAME']   = $resultdata['ACCT_NAME'];
		          $cust_data['ACCT_ALIAS_NAME']   = $resultdata['ACCT_ALIAS_NAME'];
		          $cust_data['CCY']   = $resultdata['CCY'];
		          $cust_data['APPROVED_DATE']   = $resultdata['APPROVED_DATE'];
		          $cust_data['BIN_NAME']   = $zf_filter_input->bin_name;
		           
     
         //Zend_Debug::dump($cust_data);          die;  
     			


     			 $content = array(
                     'BIN_NAME'      => $zf_filter_input->bin_name
                     );
			    $whereArr = array('CUST_BIN = ?'=>$resultdata['CUST_BIN']);
				$customerupdate = $this->_db->update('M_CUSTOMER_BIN',$content,$whereArr);
		          // print_r($cust_data);die;
		          // insert ke T_GLOBAL_CHANGES
//		  	      $change_id = $this->suggestionWaitingApproval('BIN',$info,$this->_changeType['code']['edit'],null,'M_CUSTOMER_BIN','TEMP_CUSTOMER_BIN',$zf_filter_input->cust_id,$zf_filter_input->cust_name,$zf_filter_input->cust_id);
				  // $change_id = $this->suggestionWaitingApproval('BIN',$info,$this->_changeType['code']['edit'],null,'M_CUSTOMER_BIN','TEMP_CUSTOMER_BIN','Bin',$cust_data['CUST_BIN'],$resultdata['CUST_ID'],$resultdata['CUST_NAME']);
		  	      // $this->insertTempCustomer($change_id,$cust_data);

		  	      //log CRUD
			      Application_Helper_General::writeLog('BNUD','Customer BIN has been updated (edit), Cust ID : '.$resultdata['CUST_ID']. ' ,BIN : '.$cust_data['CUST_BIN'].' ,Change id : '.$change_id);

		          $this->_db->commit();

		          $ns = new Zend_Session_Namespace('FVC');
				  $ns->backURL = '/binsetup';
					  
		          $this->_redirect('/notification/success/index');
	  	      }
		      catch(Exception $e)
		      {
		         $this->_db->rollBack();
		  	     $error_remark = $this->language->_('Database Error');
	 	      }

		     if(isset($error_remark))
		     { 
		        $msg = $error_remark; 
		        $class = 'F'; 
		     }
             else
             {
		       $msg = 'Success';
		       $class = 'S';
		     }
		
		     $this->_helper->getHelper('FlashMessenger')->addMessage($class);
		     $this->_helper->getHelper('FlashMessenger')->addMessage($msg);
//		     $this->_redirect($this->_backURL);
		     
	  }//END IF is VALID
	  else
	  {
	  	
	  	$this->view->cust_id   = ($zf_filter_input->isValid('cust_id'))? $zf_filter_input->cust_id : $this->_getParam('cust_id');
	  	$this->view->cust_name = ($zf_filter_input->isValid('cust_name'))? $zf_filter_input->cust_name : $this->_getParam('cust_name');
	  	$this->view->cust_bin = ($zf_filter_input->isValid('cust_bin'))? $zf_filter_input->cust_bin : $this->_getParam('cust_bin'); 
		$this->view->cust_bin_ori = ($zf_filter_input->isValid('cust_bin_ori'))? $zf_filter_input->cust_bin_ori : $this->_getParam('cust_bin_ori'); 
	  	
	  	$error = $zf_filter_input->getMessages();
		//format error utk ditampilkan di view html 
        $errorArray = null;
        foreach($error as $keyRoot => $rowError)
        {
           foreach($rowError as $errorString)
           {
              $errorArray[$keyRoot] = $errorString;
           }
        }
        
        //END pengaturan error untuk cust emobile, phone dan fax

        $this->view->customer_msg  = $errorArray;

	  }
	  
    }// END if($this->_request->isPost())
    else 
    {
       //$resultdata = $this->getCustomer($cust_id);  
    
       $this->view->cust_id        = strtoupper($resultdata['CUST_ID']);
       $this->view->cust_name      = $resultdata['CUST_NAME'];
       $this->view->cust_bin       = $resultdata['CUST_BIN'];
       $this->view->cust_bin_ori       = $resultdata['CUST_BIN'];
    }

     $this->view->modulename    = $this->_request->getModuleName();
     $this->view->lengthCustId  = $this->_custIdLength;
     $this->view->approve_type  =  $this->_masterhasStatus;
    
    
    
    //insert log
	try
	{
	   $this->_db->beginTransaction();
	  
	   Application_Helper_General::writeLog('CCUD','Edit Customer ['.$this->view->cust_id.']');
	   
	   $this->_db->commit();
	}
    catch(Exception $e)
    {
 	     $this->_db->rollBack();
	     Application_Log_GeneralLog::technicalLog($e);
	}
	
	
	
  }
  
  
}




