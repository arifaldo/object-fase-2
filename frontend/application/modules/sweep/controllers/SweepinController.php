<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/SweepPayment.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'General/CustomerUser.php';
require_once 'General/Customer.php';
require_once 'General/Settings.php';

class Sweep_SweepinController extends Application_Main {
	
	
	
	
	public function indexAction(){
		$this->_helper->layout()->setLayout('newlayout');
		$custID = $this->_custIdLogin;
		$userID = $this->_userIdLogin;
		$this->view->ccyArr = $this->getCcy();
		$CustomerUser = new CustomerUser($custID,$userID);
		
		// value awal
		$this->view->TrfPeriodicType 		= 5;
		$this->view->REMAIN_BALANCE_TYPE 	= 1;
		$this->view->sesion_sweep 			= 'morning';		
		//$this->view->PERIODIC_EVERY			= 'Friday';
		$periodicEveryArr = array(
    								'1' => $this->language->_('Monday'), 
    								'2' => $this->language->_('Tuesday'), 
    								'3' => $this->language->_('Wednesday'), 
    								'4' => $this->language->_('Thursday'), 
    								'5' => $this->language->_('Friday'), 
    								'6' => $this->language->_('Saturday'), 
    								'7' => $this->language->_('Sunday'),);
		$periodicEveryDateArr = range(1,31);
		$this->view->periodicEveryArr  	= $periodicEveryArr;
		$this->view->periodicEveryDateArr  = $periodicEveryDateArr;
		
		/*
		 * Verify Page or not
		*/
		$verifyPage = 0;
		$this->view->verifyPage = $verifyPage;
		
		/*
		 * generate payment ref
		 */
		$this->view->paymentreff = $this->generatePaymentReff(1); //send payment ref to view
		
		
		/*
		 * Get customer user source acc
		 */
		$paramscr = array('CCY_IN' => 'IDR');
		$this->view->sourceAcc = $CustomerUser->getAccounts($paramscr);
//		$this->view->sourceAcc = $CustomerUser->getAccounts();
// 		print_r($this->view->sourceAcc);die;
		//$sourceAcc =  $CustomerUser->getAccounts();
		
		$select   = $CustomerUser->getBeneficiaries($this->view->hasPrivilege('BLBU'));
		$select->where("B.BENEFICIARY_TYPE = ?", (string) $this->_paymenttype["code"]["within"]);
		$select->where("B.BENEFICIARY_ISAPPROVE = ?", 1);
		
		// $resultBeneficiary = $this->_db->fetchAll($select);
		// $this->view->listBeneficiary = $resultBeneficiary;
		$this->view->listBeneficiary = $CustomerUser->getAccounts($paramscr);
		
		/*
		 * Verify form
		*/
		
		/* filter */
		$filterArr = array(	
				'teamplate_reff' => array('StripTags','StringTrim'),
				'paymentsubject'    => array('StripTags','StringTrim'),
				'beneficiary'  => array('StripTags','StringTrim'),
				'curr'  => array('StripTags','StringTrim'),
				'dayname'  => array('StripTags','StringTrim'),
				'day'  => array('StripTags','StringTrim')
				
		);
		
		// every atau every date
		$filter = new Application_Filtering();
		$RECURING 				= $filter->filter($this->_request->getParam('tranferdateperiodictype'), "PERIODIC_TYPE");
		
		if ($RECURING == 5){
			$RECURING_TYPE		= 'dayname';
			$RECURING_TYPE_VAL	= 'Every';
		}elseif ($RECURING == 6){
			$RECURING_TYPE		= 'day';
			$RECURING_TYPE_VAL	= 'Every Date of';
		}else{
			$RECURING_TYPE		= '';
			$RECURING_TYPE_VAL 	= '';
		}
		
		
		$validators = array(
    						'paymentsubject' => array('NotEmpty',
														'messages' => array(
	      	                                                				$this->language->_('Payment Subject cannot be left blank.')
																			)
													),
							'ACBENEF' => array('NotEmpty',
														'messages' => array(
	      	                                                				$this->language->_('Beneficiary Account cannot be left blank.')
																			)
													),
							$RECURING_TYPE => array('NotEmpty',
													array('Between', array('min'=>1,'max'=>31)),
														'messages' => array(
	      	                                                				$this->language->_('Recurring '.$RECURING_TYPE_VAL." ".'cannot be left blank.'),
	      	                                                				$this->language->_('Recurring '.$RECURING_TYPE_VAL." ".'cannot be left blank.')
																			)
													),
							'end_date' => array('NotEmpty',
														'messages' => array(
	      	                                                				$this->language->_('End of Date cannot be left blank.')
																				)
													),
							'filter' =>  array('NotEmpty',
														'messages' => array(
	      	                                                				'kagak boleh kosong'
																			)
													)
							);
		
		
		
		$zf_filter = new Zend_Filter_Input($filterArr,$validators,$this->_request->getParams());		
		$filter = $zf_filter->getEscaped('filter');
	
		$param = $this->_getAllParams();
		if($zf_filter->isValid()){ 	
			if($filter == 'Verify'){
				
			//sourceacc
				$sourceaccview = array();
				$sourceaccamount = array();
				$singelsourceaccview = array();
				$sourceacc = $param["source"];
				$remainbalance = $param["remainbalance"];
				$sourceacclenght = count($sourceacc); //lenght source acc
					//loop push to array
					for($i=0; $i<$sourceacclenght; $i++){
						if(!empty($sourceacc[$i])){
							$singelAcc['ACCT_NO'] = $sourceacc[$i];
							if($param["REMAIN_BALANCE_TYPE"] == 2){
							$account 	  = new Account($sourceacc[$i], 'IDR');
							$accountCheck = $account->checkBalance(true);
							$amount = $account->getCoreAccountAmount();
							$singelsourceaccamountconcat = $amount['availableBalance']*((100-$remainbalance[$i])/100);
							
							}else{
								$singelsourceaccamountconcat = array();
							}
							$getSingelAcc2 = $CustomerUser->getAccounts($singelAcc);
							$acctType = ($getSingelAcc2[0]['ACCT_TYPE'] == '10')? 'SAVING': 'GIRO';	// 10 : saving, 20 : giro
							$alias	  = (!empty($getSingelAcc2[0]['ACCT_ALIAS_NAME']))  ? " / ".$getSingelAcc2[0]['ACCT_ALIAS_NAME']	: "";
							$accType  = (!empty($getSingelAcc2[0]['ACCT_TYPE']))  	   ? " (".$acctType.")"		: "";
							$singelsourceaccviewconcat =  $sourceacc[$i]." [".$getSingelAcc2[0]["CCY_ID"]."] - ".$getSingelAcc2[0]["ACCT_NAME"].$alias.$accType;
							array_push($sourceaccview, $singelsourceaccviewconcat);
							array_push($sourceaccamount, $singelsourceaccamountconcat);
							array_push($singelsourceaccview, $sourceacc[$i]);
						}
					}
				$this->view->sourceaccview = $sourceaccview;
				
				$this->view->singelsourceamountview = $sourceaccamount;
				$this->view->singelsourceaccview = $singelsourceaccview;
				
				// cek source account duplicate
				if(count($singelsourceaccview) != count(array_unique($singelsourceaccview))){ //jika duplicate
				  	$notDuplicateSourceAcc = false;				  	
				}else{	//jika tidak duplicate
					$notDuplicateSourceAcc = true;					
				}
				
				//cek benef to customer account
				//print_r($param);			
				if(empty($param['ACBENEF'])){
					$param['ACBENEF'] = $param['beneficiary'];
				}
				$selectBenefEksis = $this->_db->select	()
				->FROM	(array('MCA' => 'M_BENEFICIARY'),array('BENEFICIARY_ACCOUNT'))
				->WHERE('MCA.BENEFICIARY_ACCOUNT = ?', $param['ACBENEF'])				
				->WHERE('MCA.CUST_ID = ?', $this->_custIdLogin);	
				//echo $selectBenefEksis;												
				$cekBenefEksis = $this->_db->fetchRow($selectBenefEksis);
				
				
				//paymentmessage
				$paymentmessageview = array();
				$paymentmessage = $param["paymentmessage"];
				$paymentmessagelenght = count($paymentmessage);
					//loop push to array
					for($i=0; $i<$paymentmessagelenght; $i++){
						if(!empty($paymentmessage[$i])){
							$singelpaymentmessage = $paymentmessage[$i];
							array_push($paymentmessageview, $singelpaymentmessage);
						}
					}
				$this->view->paymentmessageview = $paymentmessageview;
				
				//paymentaddmessage
				$paymentaddmessageview = array();
				$paymentaddmessage = $param["paymentaddmessage"];
				$paymentaddmessagelenght = count($paymentaddmessage);
					//loop push to array
					for($i=0; $i<$paymentaddmessagelenght; $i++){
						if(!empty($paymentaddmessage[$i])){
							$singelpaymentaddmessage = $paymentaddmessage[$i];
							array_push($paymentaddmessageview, $singelpaymentaddmessage);
						}
					}
				$this->view->paymentaddmessageview = $paymentaddmessageview;
				
				//remainbalance
				$remainbalanceview = array();
				$remainbalance = $param["remainbalance"];
				$remainbalancelenght = count($remainbalance);
					//loop push to array
					for($i=0; $i<$remainbalancelenght; $i++){
						if(!empty($remainbalance[$i])){
							$singelremainbalance = $remainbalance[$i];
							array_push($remainbalanceview, $singelremainbalance);
						}
					}
				$this->view->remainbalanceview = $remainbalanceview;
				
				// cek value of remain balance percentage
				$RBVal = 0;
				if ($param["REMAIN_BALANCE_TYPE"] == 2){ // jika percentage
					foreach($remainbalanceview as $val){						
						$valx = str_replace('.00', '', Application_Helper_General::convertDisplayMoney($val));
						
						if ($valx > 100){	// jika lebih dari 100%
							$RBVal = $RBVal + 1;
						}else{
							$RBVal = $RBVal + 0;
						}
					}
					
				}else{
					$RBVal = 0;
				}
												
								
				// NextDate														
				if ($RECURING ==5){ //every day
					
					$dateNow = mktime(0,0,0,date("n"),date("j"),date("Y"));
					$PERIODIC_EVERY = $param["dayname"];
					$every = (int)$PERIODIC_EVERY;
					$d = date("w",$dateNow);
					
					$addDay = $every;
					if ($every > $d){
						$addDay = $addDay;
					}else{
						$addDay = $addDay + 7;
					}
					$nextDate  =  (int)$addDay - (int)$d;
					$NEXT_DATE = date("Y-m-d",strtotime("+$nextDate day"));
					$PERIODIC_EVERY_VAL = $PERIODIC_EVERY;
					
				}elseif ($RECURING ==6){ //every date
					
					$dateNextMonth = mktime(0,0,0,date("n"),date("j")+1,date("Y"));
					
					$dateNow = date("j");
					$maxDays=date('t', $dateNextMonth);
					//echo $maxDays; die;
					$PERIODIC_EVERYDATE = $param["day"];
					$every = (int)$PERIODIC_EVERYDATE;

					if ($every > $dateNow){
						$addMonth = 0;
					}else{
						$addMonth = 1; 
					}
					
					if ($maxDays >=  $every){
						$every = $every;
					}else{
						$every = $maxDays;
					}
					
					$nextDate = mktime(0,0,0,date("n")+$addMonth,$every,date("Y"));
					$NEXT_DATE = date("Y-m-d",$nextDate);
					$PERIODIC_EVERY_VAL = $PERIODIC_EVERYDATE;
				}
				// akhir next date
				
				$END_DATE = join('-',array_reverse(explode('/',$param["end_date"])));
								
				if(strtotime($NEXT_DATE) > strtotime($END_DATE)){  //jika End Date lebih kecil dari next date
					$errorMsg		 		= $this->language->_('End Date must grather than'). " ". $NEXT_DATE;
					$this->view->error 		= true;
					$this->view->report_msg	= $errorMsg;
					
					$this->fillParams($param);
					
				}elseif (count($sourceaccview) == 0){ //jika tidak ada source account yg dipilih
					$errorMsg		 		= $this->language->_('Source Account Min. 1 Record');
					$this->view->error 		= true;
					$this->view->report_msg	= $errorMsg;
					
					$this->fillParams($param);
					
				}elseif (count($remainbalanceview) < count($sourceaccview)){ //jika remain balance ada yg kosong
					$errorMsg		 		= $this->language->_('Remain Balance cannot be left blank');
					$this->view->error 		= true;
					$this->view->report_msg	= $errorMsg;
					
					$this->fillParams($param);
					
				}elseif ($RBVal > 0){ //jika remain balance percentage lebih dari 100%					
					$errorMsg		 		= $this->language->_('Remain Balance must not grather than 100');
					$this->view->error 		= true;
					$this->view->report_msg	= $errorMsg;
					
					$this->fillParams($param);
					
				}elseif (!$notDuplicateSourceAcc){ //jika source account ada yg sama
					$errorMsg		 		= $this->language->_('Source Account Can not duplicate');
					$this->view->error 		= true;
					$this->view->report_msg	= $errorMsg;
					
					$this->fillParams($param);
					
				}elseif (empty($cekBenefEksis)){ //jika benef tidak ada di table source account
					$errorMsg		 		= $this->language->_('Beneficiary is not exist');
					$this->view->error 		= true;
					$this->view->report_msg	= $errorMsg;
					
					$this->fillParams($param);
					
				}else{ //jika semuanya valid
										
					
					
					
					/************************************************************************************ buat view */
					
					//teamplate_reff
					$this->view->teamplate_reff = $param["teamplate_reff"];
					
					//Beneficiary Account
					$this->view->ACBENEF = $param["ACBENEF"];
					
					//paymentsubject
					$this->view->paymentsubject = $param["paymentsubject"];
					
					//beneficiaryacc
					$singelAcc = array();
					$singelAcc['ACCT_NO'] = $param["ACBENEF"];
					
					$filter = new Application_Filtering();
					$ACBENEF 				= $filter->filter($this->_request->getParam('ACBENEF'), "ACCOUNT_NO");
					$ACBENEF_BANKNAME 		= $filter->filter($this->_request->getParam('ACBENEF_BANKNAME'), "ACCOUNT_NAME");
					$ACBENEF_ALIAS 			= $filter->filter($this->_request->getParam('ACBENEF_ALIAS'), "ACCOUNT_ALIAS");
					$ACBENEF_CCY 			= $filter->filter($this->_request->getParam('CURR_CODE'), "SELECTION");
					$REMAIN_BALANCE_TYPE 	= $filter->filter($this->_request->getParam('REMAIN_BALANCE_TYPE'), "SELECTION");
					
									
					
					$getSingelAcc = $CustomerUser->getAccounts($singelAcc);
					//concat beneficiari
					if(!$param["ACBENEF"]){
						$beneficiaryConcat = "";
						$this->view->beneficiary = $beneficiaryConcat;
					}else{
						
						/*$singelAcc['ACCT_NO'] = $ACBENEF;
						$getSingelAcc2 = $CustomerUser->getAccounts($singelAcc);*/
													
						$select   = $CustomerUser->getBeneficiaries($this->view->hasPrivilege('BLBU'));
						$select->where("B.BENEFICIARY_TYPE = ?", (string) $this->_paymenttype["code"]["within"]);
						$select->where("B.BENEFICIARY_ACCOUNT = ?", (string) $ACBENEF);
						$resultBeneficiary = $this->_db->fetchRow($select);
																	
						//$beneType 			= ($getSingelAcc2[0]['ACCT_TYPE'] == '10')? 'SAVING': 'GIRO';	// 10 : saving, 20 : giro
						$alias	  			= (!empty($resultBeneficiary['BENEFICIARY_ALIAS']))  ? " / ".$resultBeneficiary['BENEFICIARY_ALIAS'] : "";
						//$beneficiaryType  	= (!empty($getSingelAcc2[0]['ACCT_TYPE']))  	   ? " (".$beneType.")"		: "";
						$beneficiaryConcat =  $ACBENEF." [".$resultBeneficiary["CURR_CODE"]."] - ".$resultBeneficiary['BENEFICIARY_NAME'].$alias;
												
						//$beneficiaryConcat = $ACBENEF." [".$ACBENEF_CCY."]"." - ".$ACBENEF_BANKNAME;
						$this->view->beneficiary 		= $beneficiaryConcat;
						$this->view->beneficiaryName 	= $resultBeneficiary['BENEFICIARY_NAME'];
						$this->view->beneficiaryAlias 	= $resultBeneficiary['BENEFICIARY_ALIAS'];
						$this->view->beneficiaryCCY 	= $resultBeneficiary['CURR_CODE'];
						$this->view->beneficiarySingle 	= $ACBENEF;
						/*$this->view->beneficiaryEmail 	= ""; //$resultBeneficiary['BENEFICIARY_EMAIL'];
						$this->view->beneficiaryBankCode = ""; //$resultBeneficiary['BANK_CODE'];
						$this->view->beneficiaryBankName = ""; //$resultBeneficiary['BANK_NAME'];*/
					}
					
					
					$settings = new Application_Settings();
					$settings->setSettings(null, $paramSettingID);
					$ccyList = $settings->setCurrencyRegistered();    // Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
					
					$PS_EFDATE 	= date("d/m/Y", strtotime($NEXT_DATE));
					$PS_SUBJECT	= $param["paymentsubject"];
						  
					$paramPayment = array(	"CATEGORY" 					=> "BULK DEBET",		// BULK DEBET, SINGLE PB
											"FROM" 						=> "F",				// F: Form, I: Import
											"PS_NUMBER"					=> $PS_NUMBER,
											"PS_SUBJECT"				=> $PS_SUBJECT,
											"PS_EFDATE"					=> $PS_EFDATE,
											"_dateFormat"				=> $this->_dateDisplayFormat,
											"_dateDBFormat"				=> $this->_dateDBFormat,
											"_addBeneficiary"			=> $this->view->hasPrivilege('BADA'),		// privi BADA (Add Beneficiary)
											"_beneLinkage"				=> $this->view->hasPrivilege('BLBU'),		// privi BLBU (Linkage Beneficiary User)
											"_createPB"					=> $this->view->hasPrivilege('CRSP'),		// privi CRSP (Create Single Payment)
											"_createDOM"				=> false,									// cannot create DOM trx
											"_createREM"				=> false,									// cannot create REM trx
										 );
								
					$paramTrxArr = array();
					
					/*echo $sourceacclenght."<br>";
					echo $remainbalancelenght;
					die;*/
					//Zend_Debug::dump($singelsourceaccview); die;
					
					$sourceacc = $param["source"];
					$paymentmessage = $param["paymentmessage"];
					$paymentaddmessage = $param["paymentaddmessage"];
					$remainbalance = $param["remainbalance"];
					for($i=0; $i<$sourceacclenght; $i++)
					{
						
						if(!empty($sourceacc[$i])){
							$ACCTSRC			= $sourceacc[$i];
							$TRA_AMOUNT_num		= Application_Helper_General::convertDisplayMoney($remainbalance[$i]);
							$chargeAmt			= "";
							$TRA_MESSAGE		= $paymentmessage[$i];
							$TRA_REFNO			= $paymentaddmessage[$i];
							$ACBENEF_CCY		= $resultBeneficiary['CURR_CODE'];
							$ACBENEF_BANKNAME	= $resultBeneficiary['BENEFICIARY_NAME'];
							$ACBENEF_ALIAS		= $resultBeneficiary['BENEFICIARY_ALIAS'];
							$ACBENEF_EMAIL		= $resultBeneficiary['BENEFICIARY_EMAIL'];							
							
							$paramTrx = array(
										"TRANSFER_TYPE" 			=> 'PB',
										"TRA_AMOUNT" 				=> $TRA_AMOUNT_num,
										"TRANSFER_FEE" 				=> $chargeAmt,	
										"TRA_MESSAGE" 				=> $TRA_MESSAGE,	
										"TRA_ADDITIONAL_MESSAGE" 	=> $TRA_REFNO,
										"ACCTSRC" 					=> $ACCTSRC,	
										"ACBENEF" 					=> $ACBENEF,	
										"ACBENEF_CCY" 				=> $resultBeneficiary['CURR_CODE'],	
										"ACBENEF_EMAIL" 			=> $ACBENEF_EMAIL,		
				
										// for Beneficiary data, except (bene CCY and email), must be passed by reference
										"ACBENEF_BANKNAME" 			=> $ACBENEF_BANKNAME,	
										"ACBENEF_ALIAS" 			=> $ACBENEF_ALIAS,
							
										"SWEEP"			 			=> '1',
										"REMAIN_BALANCE_TYPE"		=> $REMAIN_BALANCE_TYPE,
									 );	
					
							array_push($paramTrxArr,$paramTrx);
						}
						
					}
					
					/*Zend_Debug::dump($paramPayment);
					Zend_Debug::dump($paramTrxArr); die;*/
					
					$validate   = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin);
					$res = array();
					$resultVal	= $validate->checkCreateSweep($paramPayment, $paramTrxArr,$res);		
							
					$payment 	= $validate->getPaymentInfo();
								
					
					if($validate->isError() === false)	// payment data is valid
					{
						$confirm = true;
						$verifyPage = 1;
						$validate->__destruct();
						unset($validate);
					}
					else
					{
						$verifyPage = 0;
						$errorMsg 		= $validate->getErrorMsg();
						$errorTrxMsg 	= $validate->getErrorTrxMsg();	// array
						
						$validate->__destruct();
						unset($validate);
						
						if($errorMsg)
						{
							$error_msg[0] = $errorMsg;
							$this->view->error 		= true;
							$this->view->report_msg	= $this->displayError($error_msg);
							
						}elseif($errorTrxMsg){
							$errMessage 	= (!empty($errorMsg))? $errorMsg: reset(reset(reset($errorTrxMsg)));
								
								$this->view->error 		= true;
								$this->view->report_msg	= $errMessage;
						}
						
						else
						{
							$confirm = true;
						}
					}
					
					
					/*
					 * Verify Page or not
					*/
					
					$this->view->verifyPage = $verifyPage;
					
					//currency
	// 				$this->view->currency = $param["curr"];
	
					//recurring type
					$this->view->TrfPeriodicType = $param["tranferdateperiodictype"];
					
					//dayname
					$this->view->dayname = $param["dayname"];
					
					//day
					$this->view->day = $param["day"];
					
					//end_date
					$this->view->end_date = $param["end_date"];
					
					//next date					
					$this->view->next_date = $NEXT_DATE;
	
					//currency
					$this->view->currency = $ACBENEF_CCY;
					
					// Remain Balance Type
					$this->view->REMAIN_BALANCE_TYPE = $REMAIN_BALANCE_TYPE;
					if ($REMAIN_BALANCE_TYPE == 1){
						$this->view->remainBalanceType = "Fixed Amount";
					}elseif ($REMAIN_BALANCE_TYPE == 2){
						$this->view->remainBalanceType = "Percentage";
					}
					
					//sesion
					$this->view->sesion_sweep = $param["sesion_sweep"];
					
					
					
					
					
				}
						
			}//end of if($filter == 'Verify')
				
		}else{//else apabila tidak valid
				$this->view->error = true;
				$docErr = $this->displayError($zf_filter->getMessages());
				$this->view->report_msg = $docErr;
				
				$this->fillParams($param);
								
				//sourceacc
				// print_r($param["remainbalance"]);
				$sourceaccview = array();
				$sourceaccamount = array();
				$singelsourceaccview = array();
				$param['TRA_REMAIN'] = array();
				$sourceacc = $param["source"];
				$sourceacclenght = count($sourceacc); //lenght source acc
					//loop push to array
					for($i=0; $i<$sourceacclenght; $i++){
						if(!empty($sourceacc[$i])){
							$singelAcc['ACCT_NO'] = $sourceacc[$i];
							if($param["REMAIN_BALANCE_TYPE"] == 2){
								$account 	  = new Account($sourceacc[$i], 'IDR');
								$accountCheck = $account->checkBalance(true);
								$amount = $account->getCoreAccountAmount();
								$singelsourceaccamountconcat = $amount['availableBalance']*((100-$remainbalance[$i])/100);
								$param['TRA_REMAIN'] = $param["remainbalance"][$i];
								$remainbalanceinsert[$i] = $singelsourceaccamountconcat;
								
							}else{
								$singelsourceaccamountconcat = array();
							}
							$getSingelAcc2 = $CustomerUser->getAccounts($singelAcc);
							$singelsourceaccviewconcat =  $sourceacc[$i]." - ".$getSingelAcc2[0]["ACCT_NAME"]." ( ".$getSingelAcc2[0]["ACCT_TYPE"]." )";
							array_push($sourceaccview, $singelsourceaccviewconcat);		
							array_push($sourceaccamount, $singelsourceaccamountconcat);
							array_push($singelsourceaccview, $sourceacc[$i]);
						}
					}
				
				$this->view->sourceaccview = $sourceaccview;
				
				$this->view->singelsourceaccview = $singelsourceaccview;
				$this->view->singelsourceamountview = $sourceaccamount;
				
				
				
				//paymentmessage
				$paymentmessageview = array();
				$paymentmessage = $param["paymentmessage"];
				$paymentmessagelenght = count($paymentmessage);
					//loop push to array
					for($i=0; $i<$paymentmessagelenght; $i++){
						if(!empty($paymentmessage[$i])){
							$singelpaymentmessage = $paymentmessage[$i];
							array_push($paymentmessageview, $singelpaymentmessage);
						}
					}
				$this->view->paymentmessageview = $paymentmessageview;
				
				//paymentaddmessage
				$paymentaddmessageview = array();
				$paymentaddmessage = $param["paymentaddmessage"];
				$paymentaddmessagelenght = count($paymentaddmessage);
					//loop push to array
					for($i=0; $i<$paymentaddmessagelenght; $i++){
						if(!empty($paymentaddmessage[$i])){
							$singelpaymentaddmessage = $paymentaddmessage[$i];
							array_push($paymentaddmessageview, $singelpaymentaddmessage);
						}
					}
				$this->view->paymentaddmessageview = $paymentaddmessageview;
				
				//remainbalance
				$remainbalanceview = array();
				$remainbalance = $param["remainbalance"];
				$remainbalancelenght = count($remainbalance);
					//loop push to array
					for($i=0; $i<$remainbalancelenght; $i++){
						if(!empty($remainbalance[$i])){
							$singelremainbalance = $remainbalance[$i];
							array_push($remainbalanceview, $singelremainbalance);
						}
					}
				$this->view->remainbalanceview = $remainbalanceview;
				
		}
			
		if($filter == 'Submit'){
				
			$loop 						= $param["loop"];
			$sourceaccinsert 			= $param["sourceacc"];
			$paymentmessageinsert 		= $param["paymentmessage"];
			$paymentaddmessageinsert 	= $param["paymentaddmessage"];
			$remainbalanceinsert 		= $param["remainbalance"];
			$TrfPeriodicType			= $param["tranferdateperiodictype"];
			$daynameinsert 				= $param["dayname"];
			$dayinsert 					= $param["day"];
			$nextDate					= $param["next_date"];
			$convertDatex 				= new Zend_Date($nextDate);
			$nextDateVal				= $convertDatex->toString($this->_dateDBFormat);
			
			if ($param["sesion_sweep"] == "morning"){
				$sessionSweep			= 1;
			}elseif ($param["sesion_sweep"] == "evening"){
				$sessionSweep			= 2;
			}elseif ($param["sesion_sweep"] == "night"){
				$sessionSweep			= 3;
			}
			
			/* convert date */
			$datetime 		= $param["end_date"];
			$convertDate 	= new Zend_Date($datetime);
			$datesql 		= $convertDate->toString($this->_dateDBFormat);
											
			$param['PS_SUBJECT'] 			= $param["paymentsubject"];
			$param['PS_EFDATE']  			= $nextDate; //Application_Helper_General::convertDate($nextDateVal, $this->_dateDBFormat, $this->_dateDisplayFormat);
			$param['PS_TYPE'] 				= $this->_paymenttype['code']['sweepin'];
			$param['PS_CCY']  				= $param["beneficiaryCCY"];
			$param['REF_ID']  				= $param["teamplate_reff"];
			$param['REMAIN_BALANCE_TYPE']	= $param["remainbalancetype"];
			$param['SWEEP'] 				= "SI";
			
						
			$paramPayment = array( 
					"CATEGORY"      		=> "SWEEP IN",
					"FROM"       			=> "I",
					"PS_NUMBER"     		=> $param["teamplate_reff"],
					"_dateFormat"    		=> $this->_dateDisplayFormat,
					"_dateDBFormat"    		=> $this->_dateDBFormat,
					"_addBeneficiary"   	=> $this->view->hasPrivilege('BADA'), // privi BADA (Add Beneficiary)
					"_beneLinkage"    		=> $this->view->hasPrivilege('BLBU'), // privi BLBU (Linkage Beneficiary User)
					"_createPB"     		=> $this->view->hasPrivilege('IPMO'), // privi IPMO (Create Bulk Debet Payment by Import File (CSV))
					"_createDOM"    		=> false,        // cannot create DOM trx
					"_createREM"    		=> false,        // cannot create REM trx
			);
			
			$this->_db->beginTransaction();
			
			// Insert T_PERIODIC		
			$PERIODIC_EVERY_VAL = $TrfPeriodicType == 5 ? $daynameinsert : $dayinsert ;
			$START_DATE = join('-',array_reverse(explode('/',date('d/m/Y'))));
			$EXPIRY_DATE = join('-',array_reverse(explode('/',$datetime)));
					
			$insertPeriodic = array(
					'PS_EVERY_PERIODIC' 	=> (int)$PERIODIC_EVERY_VAL,
					'PS_EVERY_PERIODIC_UOM' => $TrfPeriodicType, 	// 5: every day of, 6: every date of
					'PS_PERIODIC_STARTDATE' => $START_DATE,
					'PS_PERIODIC_ENDDATE'	=> $EXPIRY_DATE,
					'PS_PERIODIC_NEXTDATE'	=> $nextDate,
					'PS_PERIODIC_STATUS' 	=> 2,					// 2: INPROGRESS KALO BELUM BERAKHIR, 1: COMPLETE KALO SUDAH HABIS END DATE, 0: CANCEL
					'USER_ID' 				=> $this->_userIdLogin,
					'PS_CREATED' 			=> new Zend_Db_Expr("GETDATE()"),
					'SESSION_TYPE'			=> $sessionSweep,
				);
			//Zend_Debug::dump($insertPeriodic); die;
			$this->_db->insert('T_PERIODIC', $insertPeriodic);
			$psPeriodicID =  $this->_db->lastInsertId();
			$param['PS_PERIODIC'] = $psPeriodicID;
			// End Insert T_PERIODIC
			$this->_db->commit();
				
			$param['TRANSACTION_DATA'] = array();
				
			for($i=0;$i<$loop;$i++){
				
				// Insert T_PERIODIC_DETAIL																					
				$select	= $this->_db->select()
				->from(array('A'	 		=> 'M_CUSTOMER_ACCT'),
					array(
				   		'ACCT_NAME' 	=> 'A.ACCT_NAME',
				   		'ACCT_ALIAS' 	=> 'A.ACCT_ALIAS_NAME',
				   		'ACCT_CCY' 	=> 'A.CCY_ID','A.ACCT_TYPE'
					)
				)
				->where("A.CUST_ID 			 = ?", $this->_custIdLogin)
				->where("A.ACCT_NO		 	 = ?", $sourceaccinsert[$i]);
															
				$accsrc = $this->_db->fetchRow($select);
		
				$sourceAccountName 		=  $accsrc['ACCT_NAME'];
				$sourceAccountAliasName =  $accsrc['ACCT_ALIAS'];
				$sourceAccountCCY 		=  $accsrc['ACCT_CCY'];
				$sourceAccountType 		=  $accsrc['ACCT_TYPE'];
				
				if($param["remainbalancetype2"] == '2'){
// 				foreach ($param["remainbalance"] as $key => $val ){
					$account 	  = new Account($sourceaccinsert[$i], 'IDR');
					$accountCheck = $account->checkBalance(true);
					$amount = $account->getCoreAccountInfo();
					$param['TRA_REMAIN'] = $amount['availableBalance']*((100-$param["remainbalance"][$i])/100);
// 					$param['TRA_REMAIN'] = $val;

// 				}
				}
				if($param["remainbalancetype2"] == '1'){
// 				foreach ($param["remainbalance"] as $key => $val ){
					$account 	  = new Account($sourceaccinsert[$i], 'IDR');
					$accountCheck = $account->checkBalance(true);
					$amount = $account->getCoreAccountInfo();
					$param['TRA_REMAIN'] = $amount['availableBalance']-$param["remainbalance"][$i];
// 					$param['TRA_REMAIN'] = $val;

// 				}
				}
// 				Zend_Debug::dump($remainbalanceinsert); 
				$insertPeriodicDetail = array(
						'PS_PERIODIC' 				=> $psPeriodicID,
						'SOURCE_ACCOUNT' 			=> $sourceaccinsert[$i],
						'SOURCE_ACCOUNT_CCY' 		=> $sourceAccountCCY,
						'SOURCE_ACCOUNT_NAME' 		=> $sourceAccountName,
						'SOURCE_ACCOUNT_TYPE' 		=> $sourceAccountType,
						'SOURCE_ACCOUNT_BANK_CODE' 	=> "",
						'SOURCE_ACCOUNT_BANK_NAME' 	=> "",
						'BENEFICIARY_ACCOUNT' 		=> $param["beneficiary"],
						'BENEFICIARY_ACCOUNT_CCY' 	=> $param['beneficiaryCCY'],
						'BENEFICIARY_ACCOUNT_NAME' 	=> $param['beneficiaryName'],
						'BENEFICIARY_EMAIL' 		=> $param['beneficiaryEmail'],
						'BENEFICIARY_BANK_CODE' 	=> (isset($param['beneficiaryBankCode']))? $param['beneficiaryBankCode'] : "",
						'BENEFICIARY_BANK_NAME' 	=> (isset($param['beneficiaryBankName']))? $param['beneficiaryBankName'] : "",
						'TRA_AMOUNT' 				=> $remainbalanceinsert[$i],
						'TRA_MESSAGE' 				=> $paymentmessageinsert[$i],	
						'TRA_MESSAGE_ADD'			=> $paymentaddmessageinsert[$i],	
						'PAYMENT_TYPE'				=> $param['PS_TYPE'],					
						'TRANSFER_TYPE' 			=> 0,	 // 0 : Inhouse, 1: RTGS, 2: SKN
						'BALANCE_TYPE'				=> $param["remainbalancetype2"],  // 1=Fixed Amount;2=Percentage
						'TRA_REMAIN'				=> $param['TRA_REMAIN']
					);
// 				Zend_Debug::dump($insertPeriodicDetail); die;
				$this->_db->insert('T_PERIODIC_DETAIL', $insertPeriodicDetail);
				// End Insert T_PERIODIC_DETAIL
					
					
				$content = array(
					'CATEGORY' => $param["controller"],
					
					'TEMPLATE_REFF' => $param["teamplate_reff"],
						
					'PAYMENT_SUBJECT' => $param["paymentsubject"],
						
					'BENEFICIARY_ACC' => $param["beneficiary"],
				
					'REMAIN_BALANCE_TYPE' => $param["remainbalancetype"],
				
					'PERRIODIC_TYPE' => $param["tranferdateperiodictype"],
						
					'DAYNAME' => $daynameinsert,
							
					'DAY' => $dayinsert,
							
					'END_DATE' => $datesql,
					
					'SESSION' => $param["sesion_sweep"],
						
					'SOURCE_ACC' => $sourceaccinsert[$i],
						
					'PAYMENT_MESSAGE' => $paymentmessageinsert[$i],
						
					'PAYMENT_ADD_MESSAGE' => $paymentaddmessageinsert[$i],
						
					'REMAIN_BALANCE' => $remainbalanceinsert[$i]);
					
					
					$param['TRANSACTION_DATA'][] = array(
							'SOURCE_ACCOUNT' 			=> $sourceaccinsert[$i],
					        'SOURCE_ACCOUNT_TYPE' 		=> $sourceAccountType,
							'BENEFICIARY_ACCOUNT' 		=> $param['beneficiary'],
							'BENEFICIARY_ACCOUNT_CCY' 	=> $param['beneficiaryCCY'],
							'BENEFICIARY_ACCOUNT_NAME' 	=> $param['beneficiaryName'],
							'BENEFICIARY_ALIAS_NAME' 	=> $param['beneficiaryAlias'],
							// 'BENEFICIARY_EMAIL' 		=> $row['ACBENEF_EMAIL'],
							// 'BENEFICIARY_ADDRESS' 		=> $row['ACBENEF_ADDRESS1'],
							// 'BENEFICIARY_CITIZENSHIP' 	=> $row['ACBENEF_CITIZENSHIP'],
							// 'CLR_CODE' 					=> $row['BANK_CODE'],
							'TRANSFER_TYPE' 			=> 'PB',
							'TRA_AMOUNT' 				=> $remainbalanceinsert[$i],
							'TRA_MESSAGE' 				=> $paymentmessageinsert[$i],
							'TRA_REFNO'					=> $paymentaddmessageinsert[$i],
							'TRA_ADDITIONAL_MESSAGE' 	=> '',
							'TRA_REMAIN'				=> $param['TRA_REMAIN']
					);
					//Zend_Debug::dump($content);
					//$insertsweep = new sweep_Model_Sweep($content);
					
			}
// 			$param['sourceAccountType'] = $sourceAccountType;
			$param['_addBeneficiary'] = $paramPayment['_addBeneficiary'];
			$param['_beneLinkage'] = $paramPayment['_beneLinkage'];
			$param['_priviCreate'] = 'IPMO';
				
			$SweepPayment = new SweepPayment(null, $this->_custIdLogin, $this->_userIdLogin);
			
			$result = $SweepPayment->createPayment($param);
//  			Zend_Debug::dump($param); die;
			
			$this->setbackURL('/sweep/sweepin');
			$this->_redirect('/notification/success/index');
		}
	}
	
	public function generatePaymentReff($forTransaction){
		
		/*$paymentreff = "SI".date("Y").date("m").date("d").strtoupper(uniqid());
		
		return $paymentreff;*/
		
		$currentDate = date("Ymd");		
		$seqNumber	 = strtoupper(Application_Helper_General::str_rand(8));
		$checkDigit  = '';
		
		$paymentreff   = "SI".$currentDate.$seqNumber.$checkDigit;
		
		return $paymentreff;
	}
	
	private function fillParams($param){
		$this->view->paymentsubject			= $param["paymentsubject"];
		$this->view->ACBENEF				= $param["ACBENEF"];
		$this->view->ACBENEF_BANKNAME		= $param["ACBENEF_BANKNAME"];
		$this->view->ACBENEF_ALIAS			= $param["ACBENEF_ALIAS"];
		$this->view->CURR_CODE				= $param["CURR_CODE"];
		$this->view->datetime				= $param["end_date"];
		$this->view->end_date				= $param["end_date"];
		$this->view->REMAIN_BALANCE_TYPE	= $param["REMAIN_BALANCE_TYPE"];
		$this->view->TrfPeriodicType		= $param["tranferdateperiodictype"];
		$this->view->dayname 				= $param["dayname"];				
		$this->view->day					= $param["day"];				
		$this->view->sesion_sweep 			= $param["sesion_sweep"];
		$this->view->ACCTSRC				= $param["sesion_sweep"];
	}
	
	
	
	
}