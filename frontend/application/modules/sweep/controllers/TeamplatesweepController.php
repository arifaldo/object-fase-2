<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';

class Sweep_TeamplatesweepController extends Application_Main {
	
	
	public function indexAction(){
		
		/*get acc this login*/
		$custID = $this->_custIdLogin;
		$userID = $this->_userIdLogin;
		$CustomerUser = new CustomerUser($custID,$userID);
		$getAcc = $CustomerUser->getAccounts();
		$this->view->accnumber = $getAcc;
		
		/*select tabel T_TEMP_SWEEP*/
		$sweep = $this->_db->select()
							->from(array('T_TEMP_SWEEP'),
									array(
										'Teamplate Reff' 	=> 'teamplate_reff',
										'Payment Subject'	=> 'payment_subject',
										'Beneficiary'		=> 'beneficiari_acc',
										'Source Account'	=> 'source_acc',
										'Day Name'			=> 'dayname',
										'Day'				=> 'day',
										'Sesion'			=> 'sesion',
										'End Date'			=> 'end_date',
										'Category'			=> 'category'	
							));
		
 		
		
		/* tabel headbar */
		$fields = array('teamplatereff'				=> array('field'    => 'teamplate_reff',
						'label'    					=> 'Template reff',
						'sortable' 					=> true),
				
						'sourceaccount' 			=> array('field'    => 'source_acc',
						'label'    					=> 'Source Account',
						'sortable' 					=> true),
		
						'beneficiaryaccount'     	=> array('field'    => 'beneficiari_acc',
						'label'    					=> 'Beneficiary',
						'sortable' 					=> true),
		 
						'recurring'    				=> array('field'  => 'dayname',
						'label'   					=> 'Recurring',
						'sortable' 					=> true),
						
						'session'    				=> array('field'  => 'sesion',
						'label'   					=> 'Session',
						'sortable' 					=> true),
				
						'enddate'    				=> array('field'  => 'end_date',
						'label'   					=> 'End Date',
						'sortable' 					=> true),
						
						'teamplatetype'    			=> array('field'  => 'category',
						'label'   					=> 'Template Type',
						'sortable' 					=> true),
				
						
		);
		

		$page = $this->_getParam('page');
		$page = (Zend_Validate::is($page,'Digits'))? $page : 1;
		$sortBy = $this->_getParam('sortby');
		$sortBy = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'desc';
		
		$sweep->order($sortBy.' '.$sortDir);
		
		$zf_filter = new Zend_Filter_Input(array(),array(),$this->_request->getParams());
		$filter = $zf_filter->getEscaped('filter');
		
		//get param
		
		
		if($filter=='Set Filter'){
			
			$param = $this->_getAllParams();
// 			Zend_Debug::dump($param);

			$teamplatereff = html_entity_decode($zf_filter->getEscaped('teamplatereff'));
			$sourceaccount = html_entity_decode($zf_filter->getEscaped('sourceaccount'));
			$beneficiary = html_entity_decode($zf_filter->getEscaped('beneficiary'));
			$sesion = html_entity_decode($zf_filter->getEscaped('sesion'));
			$type = html_entity_decode($zf_filter->getEscaped('type'));
			
// 			Zend_Debug::dump($sourceaccount);
			if(!empty($teamplatereff)){
				$this->view->teamplatereff = $teamplatereff;
				$sweep->where("teamplate_reff like".$this->_db->quote($teamplatereff));
// 				echo $sweep->__toString();
			}
			
			if(!empty($sourceaccount)){
				$this->view->sourceaccount = $sourceaccount;
				$sweep->where("source_acc like".$this->_db->quote($sourceaccount));
// 				echo $sweep->__toString();
			}
			
			if(!empty($beneficiary)){
				$this->view->beneficiary = $beneficiary;
				$sweep->where("beneficiari_acc like".$this->_db->quote($beneficiary));
// 				echo $sweep->__toString();
			}
			
			if(!empty($sesion)){
				$this->view->sesion = $sesion;
				$sweep->where("sesion like".$this->_db->quote($sesion));
// 				echo $sweep->__toString();
			}
			
			if(!empty($type)){
				$this->view->type = $type;
				$sweep->where("category like".$this->_db->quote($type));
// 				echo $sweep->__toString();
			}
			
			
			
		}
		
		
		$this->view->fields = $fields; //lempar table header ke view
		$this->paging($sweep); //untuk mengecek jumlah page dari query
	}
}