<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';
require_once 'General/CustomerUser.php';

class rdnaccount_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF';
	
	public function indexAction()
	{   
		$this->_helper->layout()->setLayout('newlayout');
		$listId = $this->_db->select()->distinct()
						->from(array('M_CUSTOMER'),
							   array('CUST_ID'))
						->where('CUST_SECURITIES = 1')
						->order('CUST_ID ASC')
						->query()->fetchAll();

		$this->view->listCustId = array('' => '-- '.$this->language->_('Any Value').' --');
		$this->view->listCustId += Application_Helper_Array::listArray($listId,'CUST_ID','CUST_ID');

		$status = array(
    					'1' => $this->language->_('Active'), 
    					'2' => $this->language->_('Suspend'), 
    					'3' => $this->language->_('Delete'), 
    					);
//		$options = array_combine(array_values($status['code']),array_values($status['desc']));
		$this->view->listStatus = array('' =>'-- '.$this->language->_('Any Value').' --');
		$this->view->listStatus += $status;
		
		$select = $this->_db->select()
				->from(array('A' => 'M_PRODUCT_TYPE'),array('*'));
		$select->where('A.ACCT_TYPE = ? ','4');
		$select ->order('PRODUCT_ID ASC');
		$arr = $this->_db->fetchall($select);

		foreach ($arr as $key => $val)
		{
			$arrProdtype[$val['PRODUCT_NAME']] = $val['PRODUCT_NAME'];
		}
		$this->view->typearr = array("" => "-- Any Value --");
		If ($arrProdtype)
			$this->view->typearr += $arrProdtype;
		
		
		// hit service accountListRDN
		$userId = $this->_userIdLogin;
		$custId = $this->_custIdLogin;
		
		$model = new accountaggregation_Model_Accountaggregation();

		$param['custId'] = $custId;
		$userAcctInfo = $model->getCustAccount($param);
		$accountNo 	= $userAcctInfo[0]['ACCT_NO'];
		$accountType = $userAcctInfo[0]['ACCT_TYPE'];
		$accountCcy = $userAcctInfo[0]['CCY_ID'];
		//echo $custId;
		$PECode = $this->_db->fetchRow(
							$this->_db->select()
							->from(array('C' => 'M_CUSTOMER'),array('C.CUST_CODE'))
							->where("CUST_ID = ".$this->_db->quote($custId))
							->limit(1)
					);
		//echo $PECode;die;
		//var_dump($PECode['CUST_CODE']);die;
		$Account = new Account($accountNo, $accountCcy, $accountType);
		$Account->setFlag(false);
		$res = $Account->accountListRDN($PECode['CUST_CODE']);
		// $res = array();

		if ($res['checkBalanceStatus'] == "00" && !empty($res['AccountList'])){				
			$data = $res['AccountList'];
			
			//DELETE DATA
			 $where = array('CUST_ID = ?' => $custId);
			 $this->_db->delete('M_RDN_ACCT', $where);
			
			$this->_db->beginTransaction();
			
			foreach ($data as $key => $value)
			{
				
				$rdninfo = new rdninformation_Model_Rdninformation(); 
				$product = $rdninfo->getProduct($value['planCode'],$value['productPlan']);
				$productName = $product[0]['PRODUCT_NAME'];
				
				if(!empty($value['openingAccountDate'])){
				$yy= substr($value['openingAccountDate'], 0,4);
				$mm= substr($value['openingAccountDate'], 4,2);
				$dd= substr($value['openingAccountDate'], 6,2);
				$rdnOpening = $yy."-".$mm."-".$dd;
				}else{
					$rdnOpening = "2020-02-04";
				}
				
				if(trim($value['accountNo'])!=''){
				$insertArr = array(
					'RDN_NO' 		=> $value['accountNo'], 
					'CUST_ID'		=> $custId, 
					'RDN_NAME' 		=> $value['accountName'], 
					'RDN_CCY' 		=> $value['currency'],								
//					'RDN_TYPE' 		=> $value['RDN_TYPE'],								
					'RDN_DESC'		=> $productName,
					'RDN_EMAIL'     => $value['emailAddress'],
					'RDN_SID'   	=> $value['sid'],
					'RDN_SRE'   	=> $value['sre'],
					'RDN_STATUS'   	=> $value['status'],
					'RDN_OPENING'   => $rdnOpening,
				);			
				
//				Zend_Debug::dump($insertArr);die;
				$this->_db->insert('M_RDN_ACCT', $insertArr);
				}
				
			}
			$this->_db->commit();
		}else{
			$this->_db->beginTransaction();
			
			$this->_db->commit();

		}

		$fields = array	(
							'acct'  					=> array	(
																		'field' => 'CA.RDN_NO',
																		'label' => $this->language->_('RDN Account Number'),
																		'sortable' => true
																	),
							'accountname'  					=> array	(
																		'field' => 'CA.RDN_NAME',
																		'label' => $this->language->_('RDN Account Name'),
																		'sortable' => true
																	),	
							'sid'  					=> array	(
																		'field' => 'CA.RDN_SID',
																		'label' => $this->language->_('SID'),
																		'sortable' => true
																	),
							'sre'  					=> array	(
																		'field' => 'CA.RDN_SRE',
																		'label' => $this->language->_('SRE'),
																		'sortable' => true
																	),	
							'currency'  					=> array	(
																		'field' => 'CA.RDN_CCY',
																		'label' => $this->language->_('Currency'),
																		'sortable' => false
																	),	
							'Type'  					=> array	(
																		'field' => 'CA.RDN_DESC',
																		'label' => $this->language->_('Type'),
																		'sortable' => true
																	),	
							'emailaddress'  					=> array	(
																		'field' => 'CA.RDN_EMAIL',
																		'label' => $this->language->_('Email'),
																		'sortable' => true
																	),
							'openingacct'  					=> array	(
																		'field' => 'CA.RDN_OPENING',
																		'label' => $this->language->_('Opening Account'),
																		'sortable' => true
																	),
														
							'Status'  => array	(
																		'field' => 'CA.RDN_STATUS',
																		'label' => $this->language->_('Status'),
																		'sortable' => true																	
																	),
		);
		$filterlist = array('PS_CREATED','PS_NUMBER','SOURCE_ACCOUNT','PS_EFDATE','PS_UPDATED','PS_STATUS','PS_TYPE','TRANSFER_TYPE');
		$this->view->filterlist = $filterlist;
		//get page, sortby, sortdir
        $page    = $this->_getParam('page');		
        $sortBy  = $this->_getParam('sortby','C.CUST_ID');
        $sortDir = $this->_getParam('sortdir','asc');
		foreach ($this->_request->getParams() as $key => $value)
		{
			//print_r($key);
			if($key == 'module' || $key == 'controller' || $key == 'action' || $key == 'safetycheck' ){

			}else{
				$printvar[$key] = str_replace('/','-',$value);
			}
			
			
		}
		$printvar['print'] = '1';
		$this->view->qfilter = $printvar;	
        //validate parameters before passing to view and query
        $page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
                    
        $sortBy = (Zend_Validate::is($sortBy,'InArray',
        	array(array_keys($fields))
            ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

        $sortDir = (Zend_Validate::is($sortDir,'InArray',
        	array('haystack'=>array('asc','desc'))
            ))? $sortDir : 'asc';
		
		$filterArr = array('filter' => array('StripTags','StringTrim'),
			   'companyid'    => array('StripTags','StringTrim','StringToUpper'),
			   'companyname'  => array('StripTags','StringTrim','StringToUpper'),
			   'accnumber'    => array('StripTags','StringTrim','StringToUpper'),
			   'accname'  => array('StripTags','StringTrim','StringToUpper'),
			   'status' => array('StripTags','StringToUpper'),
			   'type' => array('StripTags','StringTrim'),
		);

		$dataParam = array("SOURCE_ACCOUNT","PS_NUMBER","PS_STATUS","PS_TYPE","TRANSFER_TYPE");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{

			// print_r($dtParam);die;
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}

			}

			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}
			if(!empty($this->_request->getParam('createdate'))){
				$createarr = $this->_request->getParam('createdate');
					$dataParamValue['PS_CREATED'] = $createarr[0];
					$dataParamValue['PS_CREATED_END'] = $createarr[1];
			}
			if(!empty($this->_request->getParam('updatedate'))){
				$updatearr = $this->_request->getParam('updatedate');
					$dataParamValue['PS_UPDATED'] = $updatearr[0];
					$dataParamValue['PS_UPDATED_END'] = $updatearr[1];
			}

			if(!empty($this->_request->getParam('efdate'))){
				$efdatearr = $this->_request->getParam('efdate');
					$dataParamValue['PS_EFDATE'] = $efdatearr[0];
					$dataParamValue['PS_EFDATE_END'] = $efdatearr[1];
			}
		$zf_filter = new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		$filter = $zf_filter->getEscaped('filter');
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;		
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');	
		
//		$caseStatus = "(CASE CA.ACCT_STATUS ";
//		foreach($options as $key=>$val)
//		{
//			$caseStatus .= " WHEN ".$key." THEN '".$val."'";
//		}
//		$caseStatus .= " END)";
		
		$select = $this->_db->select()
							 ->FROM (array('C' => 'M_CUSTOMER'),array(''))
							 ->JOIN (array('CA' => 'M_RDN_ACCT'),'C.CUST_ID = CA.CUST_ID',array('CA.RDN_NO','CA.RDN_NAME','CA.RDN_SID','CA.RDN_SRE','CA.RDN_CCY','CA.RDN_DESC','CA.RDN_EMAIL',
							 																	'CA.RDN_OPENING','CA.RDN_STATUS')
							 		)
							 ->where('C.CUST_SECURITIES = 1')
							 ->where("CA.RDN_STATUS != 'Close'")
							 ->where("UPPER(C.CUST_ID) = ".$this->_db->quote($this->_custIdLogin));
		
			if( $filter == TRUE || ($filter == TRUE &&( $csv || $pdf ) ))
			{
				$companyid = html_entity_decode($zf_filter->getEscaped('companyid'));
				$companyname = html_entity_decode($zf_filter->getEscaped('companyname'));
				$accnumber = html_entity_decode($zf_filter->getEscaped('accnumber'));
				$accname = html_entity_decode($zf_filter->getEscaped('accname'));
				$stat = $zf_filter->getEscaped('status');
				$type = $zf_filter->getEscaped('type');
				
				if($accnumber)
				{
					$this->view->accnumber = $accnumber;
					$select->where("RDN_NO LIKE ".$this->_db->quote('%'.$accnumber.'%'));
				}
				
				if($accname)
				{
					$this->view->accname = $accname;
					$select->where("UPPER(CA.RDN_NAME) LIKE ".$this->_db->quote('%'.$accname.'%'));
				} 
				
				if( $stat !== null && $stat !== "" )
				{
					$this->view->stat = ($stat);
					$select->where("CA.RDN_STATUS LIKE ".$this->_db->quote($stat));
				}
				
				if( $type !== null && $type !== "" )
				{
					$this->view->type = ($type);
					$select->where("CA.RDN_DESC LIKE ".$this->_db->quote('%'.$type.'%'));
				}
				
			}

			$select->order($sortBy.' '.$sortDir);	
//			echo $select;
			//PAGING
			if($csv || $pdf || $this->_request->getParam('print') == 1)
			{
				$data = array();
//				echo $select;
				$data = $this->_db->fetchall($select);
				
				if($csv)
				{		
					Application_Helper_General::writeLog('RPRF','Download CSV RDN Account List');
					$this->_helper->download->csv(array($this->language->_('RDN Account Number'),$this->language->_('RDN Account Name'),$this->language->_('SID'),$this->language->_('SRE'),$this->language->_('Currency'),$this->language->_('Type'),$this->language->_('Email Address'),$this->language->_('Opening Account'),$this->language->_('Status')),$data,null,$this->language->_('RDN Account List'));  
				}
				else if($pdf)
				{
					Application_Helper_General::writeLog('RPRF','Download PDF RDN Account List');
					$this->_helper->download->pdf(array($this->language->_('RDN Account Number'),$this->language->_('RDN Account Name'),$this->language->_('SID'),$this->language->_('SRE'),$this->language->_('Currency'),$this->language->_('Type'),$this->language->_('Email Address'),$this->language->_('Opening Account'),$this->language->_('Status')),$data,null,$this->language->_('RDN Account List'));  
				}
				else if($this->_request->getParam('print') == 1){
					$this->_forward('print', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'RDN Account List', 'data_header' => $fields));
				}
				else
				{
					//Application_Helper_General::writeLog('RPAC','View Bank Account List Report');
				}
			}
			// echo $select;die;
			$this->paging($select);
			$this->view->fields = $fields;
			$this->view->filter = $filter;
			$this->view->modulename = $this->_request->getModuleName();
			if(!$this->_request->isPost()){
			Application_Helper_General::writeLog('RPRF','View RDN Account List');
			}
	}
}