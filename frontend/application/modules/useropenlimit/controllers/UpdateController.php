<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class useropenlimit_UpdateController extends Application_Main 
{

  public function initController()
  {       
    //$this->_helper->layout()->setLayout('popup');
    $listCcy = array(''=>'-- All --');
    $listCcy = array_merge($listCcy,Application_Helper_Array::listArray($this->getCcy(),'CCY_ID','CCY_ID'));
    $this->view->listCcy = $listCcy;
  }
  
  public function indexAction() 
  {
    $this->_helper->layout()->setLayout('newlayout');

    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
    $password = $sessionNamespace->token; 
    $this->view->token = $sessionNamespace->token;


    $AESMYSQL = new Crypt_AESMYSQL();
    
    $cust_id = $this->_custIdLogin;
    
    $acct_no = $AESMYSQL->decrypt($this->_getParam('acct_no'), $password);

    $this->view->cust_id = $cust_id;
    
    $acct_no = (Zend_Validate::is($acct_no,'Alnum'))? $acct_no : null;
    $this->view->acct_no = $acct_no;
    
    $type = $this->_getParam('type');
    $this->view->type = $type;
        
  // $select = $this->_db->select()
  //         ->from(array('M_USER'),
  //              array('USER_FULLNAME')
  //              )
  //         ->where("CUST_ID = ?", $cust_id)
  //         ->where("USER_ID = ?", $user_id);
  // $userName = $this->_db->fetchRow($select);
    
  // $this->view->user_name = $userName['USER_FULLNAME'];
  //   $this->view->user_status = $userName['USER_STATUS'];

    $userData = $this->_db->fetchAll(
      $this->_db->select()->distinct()
        ->from(array('A' => 'M_USER'),array('USER_ID','USER_FULLNAME'))
        ->joinLeft(array('MU'=>'M_MAKERLIMIT'),'MU.USER_LOGIN=A.USER_ID AND MU.CUST_ID=A.CUST_ID',array())
    ->joinLeft(array('TM'=>'TEMP_MAKERLIMIT'),'TM.USER_LOGIN=A.USER_ID AND TM.CUST_ID=A.CUST_ID',array())
        ->where('A.CUST_ID = ?', $cust_id)
        //->where('MU.MAXLIMIT IS NULL')
    ->where('TM.TEMP_ID IS NULL')

    );

    $this->view->userData = $userData;

    if($this->_getParam('filter'))
    {
      $user_id = $this->_getParam('user_login');
      $this->view->user_id = $user_id;
      $username = $this->_getParam('username');
      $this->view->username = $username;

    }

    if($cust_id && $user_id)
    {
      
      $select = $this->_db->select()
                             ->from('M_CUSTOMER',array('CUST_ID','CUST_NAME','CUST_LIMIT_IDR'))
                             ->where('UPPER(CUST_ID)='.$this->_db->quote(strtoupper($cust_id)));
      $result = $this->_db->fetchRow($select);
      // var_dump($result);
      if($result['CUST_ID'])
      {
        $this->view->cust_name = $result['CUST_NAME'];
      }
    
      //check apakah company-nya masih ada approval
      $validator = new Zend_Validate_Db_NoRecordExists(
            array(
              'table' => 'TEMP_MAKERLIMIT',
              'field' => 'USER_LOGIN'
            ));
    
      if (!$validator->isValid($user_id))
      { 
        $messages = array($this->language->_('No changes allowed for this record while awaiting approval for previous change.'));
        $this->view->isapproval = $this->displayError($messages);
      }
                 
      $fields = array(
            'BANK_NAME'       => array('field'    => 'BANK_NAME',
                         'label'    => $this->language->_('Bank'),
                         'sortable' => true),
            'acct_no'       => array('field'    => 'A.ACCT_NO',
                         'label'    => $this->language->_('Account Number'),
                         'sortable' => true),
    
            'acct_name'    => array('field'   => 'A.ACCT_NAME',
                        'label'    => $this->language->_('Account Name'),
                        'sortable' => true),
    
            'ccy'      => array('field'   => 'A.CCY_ID',
                        'label'    => $this->language->_('Currency'),
                        'sortable' => true),
            );
      $sortBy = $this->_getParam('sortby','ACCT_NO');
      $sortBy = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
      $sortDir = $this->_getParam('sortdir','asc');
      $sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
      
      $this->view->sortBy = $sortBy;
      $this->view->sortDir = $sortDir;


      $acctlist = $this->_db->fetchAll(
          $this->_db->select()
             ->from(array('A' => 'M_APIKEY'))
              ->join(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
              ->where("A.FIELD IN ('account_number','account_name','account_currency')")
             ->where("A.CUST_ID IN ('".$cust_id."')")
             // ->order('A.APIKEY_ID ASC')
             ->order('B.BANK_NAME ASC')
              //echo $acctlist;
        );
             // echo $acctlist;die; 
    // echo '<pre>';
    // echo $acctlist;
     
      $masterData = $this->_db->fetchAll(
            $this->_db->select()
              ->from(array('A' => 'M_MAKERLIMIT'),array('USER_LOGIN','ACCT_NO','ACCT_NAME','ACCT_CCY','CUST_ID','MAXLIMIT'))
              ->joinLeft(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
              //->joinLeft(array('B' => 'M_CUSTOMER_ACCT'),'A.ACCT_NO = B.ACCT_NO',array('CCY_ID','ACCT_NAME'))
              ->where('A.CUST_ID = ?', $cust_id)
              ->where('A.USER_LOGIN = ?', $user_id)
              ->where('A.MAXLIMIT > 0')
              ->where('A.MAXLIMIT_OPEN = 1')
              ->where('A.MAKERLIMIT_STATUS <> 3')
              ->order('B.BANK_NAME ASC')
            //  ->where('B.ACCT_STATUS <> 3')
              //->order($sortBy.' '.$sortDir)
          );
     
     //echo '<pre>';
     //var_dump($masterData);die;
      $account = array();
      foreach ($acctlist as $key => $value) {
        $account[$value['APIKEY_ID']][$value['FIELD']] = $value['VALUE'];
        $account[$value['APIKEY_ID']]['BANK_CODE'] = $value['BANK_CODE'];
        $account[$value['APIKEY_ID']]['BANK_NAME'] = $value['BANK_NAME'];
        if($value['FIELD'] == 'account_number'){
          foreach ($masterData as $newkey => $valuemaster) {
            if($value['VALUE'] == $valuemaster['ACCT_NO']){
              $account[$value['APIKEY_ID']]['MAXLIMIT'] = $valuemaster['MAXLIMIT'];
            }
          }
        }
        $i = $value['APIKEY_ID'];
      }
	  
	  $debitlist = $this->_db->fetchAll(
          $this->_db->select()
             ->from(array('T' => 'T_DEBITCARD'))
			 ->joinleft(array('C' => 'M_CUSTOMER'),'C.CUST_ID=T.CUST_ID',array('CUST_NAME','CUST_EMAIL'))
							   ->joinleft(array('D' => 'T_CUST_DEBIT'),'D.REG_NUMBER=T.REG_NUMBER',array('REG_NUMBER','VA_NUMBER'))
							   ->joinleft(array('E' => 'T_DEBIT_GROUP'),'E.DEBIT_NUMBER=T.DEBIT_NUMBER',array('GROUP_ID'))
							   ->joinleft(array('EG' => 'M_DEBITGROUP'),'EG.GROUP_ID=E.GROUP_ID',array('GROUP_NAME'))
							   
							   ->joinleft(array('F' => 'M_USER_DEBIT'),'F.USER_DEBITNUMBER=T.DEBIT_NUMBER',array('USER_ID'))
							   ->joinleft(array('G' => 'M_USER'),'F.USER_ID=G.USER_ID',array('USER_FULLNAME','USER_EMAIL'))
			 ->where('T.CUST_ID = ? ',$this->_custIdLogin)
			 );
		
		if(!empty($debitlist)){
			foreach($debitlist as $vl){
				$i++;
				$account[$i]['account_number'] = $vl['DEBIT_NUMBER'];
				$account[$i]['BANK_CODE'] = '999';
				$account[$i]['BANK_NAME'] = '-';
						if(!empty($vl['USER_FULLNAME'])){
								$vl['DEBIT_NAME'] = $vl['USER_FULLNAME'];
							}else{
								$vl['DEBIT_NAME'] = $vl['CUST_NAME'];
							}
				$account[$i]['account_name'] = $vl['DEBIT_NAME'];
				$account[$i]['account_currency'] = 'IDR';
				foreach ($masterData as $newkey => $valuemaster) {
				if($vl['DEBIT_NUMBER'] == $valuemaster['ACCT_NO']){
				  $account[$i]['MAXLIMIT'] = $valuemaster['MAXLIMIT'];
				}
			  }
				
			}
		}
		
		$masterdebitlist = $this->_db->fetchAll(
          $this->_db->select()
             ->from(array('T' => 'T_CUST_DEBIT'))
			 ->joinleft(array('C' => 'M_CUSTOMER'),'C.CUST_ID=T.CUST_ID',array('CUST_NAME','CUST_EMAIL'))
			 ->where('T.CUST_ID = ? ',$this->_custIdLogin)
			 );
			 
		if(!empty($masterdebitlist)){
			foreach($masterdebitlist as $vl){
				$i++;
				$account[$i]['account_number'] = $vl['VA_ACCOUNT'];
				$account[$i]['BANK_CODE'] = '999';
				$account[$i]['BANK_NAME'] = '-';
				$account[$i]['account_name'] = $vl['CUST_NAME'];
				$account[$i]['account_currency'] = 'IDR';
				foreach ($masterData as $newkey => $valuemaster) {
				if($vl['VA_ACCOUNT'] == $valuemaster['ACCT_NO']){
				  $account[$i]['MAXLIMIT'] = $valuemaster['MAXLIMIT'];
				}
			  }
				
			}
		}	 
	//echo '<pre>';	
	//var_dump($debitlist);
	//var_dump($account);die;
		
	
      $this->view->userLimit = $account;
      $this->view->fields = $fields;
      
      $this->view->status_type = $this->_masterglobalstatus;
      $this->view->cust_id = $cust_id;
      // $this->view->user_id = $user_id;
      $this->view->acct_no = $acct_no;
      $this->view->modulename = $this->_request->getModuleName();
    }
    else
    {
      // $error_remark = $this->language->_('Customer ID does not exist.');
      //insert log
      // try 
      // {
      // $this->_db->beginTransaction();
      // $this->backendLog(strtoupper($this->_actionID['view']),strtoupper($this->_moduleID['user']),null,null,$error_remark);
        // $this->_db->commit();
    // }
    // catch(Exception $e) 
    // {
      // $this->_db->rollBack();
        // SGO_Helper_GeneralLog::technicalLog($e);
    // }
      
    // $this->view->error = true;
    // $this->view->report_msg = $error_remark;
    }
    
    if($this->_request->isPost())
  {
    $user_id = $AESMYSQL->decrypt($this->_getParam('user_id'), $password);
    $userName = $AESMYSQL->decrypt($this->_getParam('uname'), $password);
   // var_dump($user_id);
   // echo '<pre>';  
   // print_r($this->_request->getParams());die;
   // $user_id = $this->_getParam('userid');
  //  $userName = $this->_getParam('username');

    $maxlimit = $this->_request->getParam('maxlimit');
    $bankcode = $this->_request->getParam('bankcode');
    $acct_name = $this->_request->getParam('ACCT_NAME');
    $ccy = $this->_request->getParam('ccy');
    $error = false;
    $errorMsg = array();
    
    foreach($maxlimit as $key=>$value)
    {
      $maxlimit[$key] = $maxlimit[$key]?Application_Helper_General::convertDisplayMoney($value):null;
      if(!$maxlimit[$key]) 
      { 
        unset($maxlimit[$key]);
        continue;
      }
      
      if($maxlimit[$key] < 0 || $maxlimit[$key] > 9999999999999)
      {
        $error = true;
        $errorMsg[$key] = $this->language->_('Too many significant digits. Maximum digit allowed : 13 digit(s)');
      } 
      // echo "string";
      // var_dump($maxlimit[$key]);
      // var_dump($result['CUST_LIMIT_IDR']);
      // if((int)$maxlimit[$key] > (int)$result['CUST_LIMIT_IDR']){
      //  $error = true;
      //  // echo "string";
      //  $errorMsg[$key] = $this->language->_('Maximum Amount of Daily Limit in IDR is ').$value;
      // }    
    }
    // var_dump($result['CUST_LIMIT_IDR']);
    // die;

    // 

    if($error)
    {
      $this->view->error = true;
      $this->view->errorMsg = $errorMsg;
      $this->view->maxlimit = $maxlimit;
    }
    else
    {
      try
      { 
        $info = 'User Limit, ID = '.$user_id; 
        $this->_db->beginTransaction();
    
        // insert ke T_GLOBAL_CHANGES
        $change_id = $this->suggestionWaitingApproval('User Open Banking Limit',$info,$this->_changeType['code']['edit'],null,'M_MAKERLIMIT','TEMP_MAKERLIMIT',$user_id,$userName,$cust_id);
        
        //LOOPING HERE JIKA ACCOUNT YANG DI LIMIT BANYAK
        foreach($maxlimit as $key=>$value)
        { 
          $data = array(  'CHANGES_ID' => $change_id,
                  'USER_LOGIN' => $user_id, 
                  'ACCT_NO' => (string)$key,
                  'ACCT_NAME' => $acct_name[$key],
                  'ACCT_CCY' => 'IDR',
                  'CUST_ID' => $cust_id,
                  'BANK_CODE' => $bankcode[$key],
                  'MAXLIMIT' => $maxlimit[$key],
                  'SUGGESTED' => new Zend_Db_Expr('now()'), 
                  'SUGGESTEDBY' => $this->_userIdLogin, 
                  'MAKERLIMIT_STATUS' => 1,
                  'MAXLIMIT_OPEN' => 1
                );
          $this->_db->insert('TEMP_MAKERLIMIT',$data);
        }
        //END LOOP
        
        
        Application_Helper_General::writeLog('MLUD','Customer ID : '.$cust_id.', User ID : '.$user_id);
        $this->_db->commit();
        
        //$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/filter/Filter/acct_no/'.$acct_no.'/user_login/'.$user_id.'/cust_id/'.$cust_id);
        // $this->_redirect('/notification/success');
        $this->_redirect('/notification/submited/index');
      }
      catch(Exception $e) 
      {

        //rollback changes
        $this->_db->rollBack(); 
      }
    }
    
    $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
    if(count($temp)>1){
          if($temp[0]=='F' || $temp[0]=='S'){
            if($temp[0]=='F')
              $this->view->error = 1;
            else
              $this->view->success = 1;
            $msg = ''; unset($temp[0]);
            foreach($temp as $value)
            {
              if(!is_array($value))
                $value = array($value);
              $msg .= $this->view->formErrors($value);
            }
            $this->view->report_msg = $msg;
        } 
      }
  }
  if(!$this->_request->isPost())
    Application_Helper_General::writeLog('MLUD','Customer ID : '.$cust_id.', User ID : '.$user_id);
  }
}