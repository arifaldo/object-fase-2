<?php
require_once 'Zend/Controller/Action.php';
class news_IndexController extends Application_Main {
	
	public function indexAction() 
	{

		$this->_helper->layout()->setLayout('newlayout');
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
		
		$arr = null;
		$viewFilter = null;
		$errors = null;
		
		$id = $this->_getParam('id');
	
			$select = $this->_db->select()
								->from(	'M_CONTENT',
										array(
												'*'));
								// ->where("HELP_ISDELETED != 1");
			
			if(!empty($id)){
				$select->where('ID = ?',$id);
			}	
					// $select->order($sortBy.' '.$sortDir);
					// echo $select;die;
		$arr = $this->_db->fetchAll($select);
				
		// print_r($arr);die;
		$this->view->arr = $arr['0'];
		
	}
}