<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/BulkPayment.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'General/CustomerUser.php';
require_once 'General/Customer.php';
require_once 'General/Settings.php';
require_once 'General/Charges.php';
require_once 'CMD/SinglePayment.php';
require_once 'CMD/Validate/ValidatePaymentMultiple.php';

require_once 'SGO/Extendedmodule/PHPExcel.php';
require_once 'SGO/Extendedmodule/PHPExcel/IOFactory.php';


class newmultibulk_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti

	protected $_destinationUploadDir = '';
	protected $_maxRow = '';

	public function initController()
	{
		$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';

		$setting = new Settings();
		$this->_maxRow = $setting->getSetting('max_import_bulk');
	}

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$this->setbackURL();
		$this->view->ccyArr = $this->getCcy();

		$settingObj = new Settings();
		$this->view->THRESHOLD_LLD		= $settingObj->getSetting("threshold_lld"	, 0);

		$this->view->PSEFDATE = Application_Helper_General::convertDate($this->getCurrentDate());
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR")));	// show acc in IDR only
		$this->view->AccArr =  $AccArr;
		$listAccValidate = Application_Helper_Array::simpleArray($AccArr,'ACCT_NO');
		// print_r($this->_paymenttype);die;
		$BULK_TYPEARR = $this->_request->getParam('BULK_TYPE');
		if(!empty($BULK_TYPEARR)){
			$this->view->BULK_TYPE  = $BULK_TYPEARR;
		}
		$bulkarr = array(
    								'6' => $this->language->_('Multi Credit'),
    								'7' => $this->language->_('Multi Debet'),
    								'4' => $this->language->_('Payroll'),
    								'17' => $this->language->_('Many to many'),
    								'18' => $this->language->_('E-Money'),
    								);
		$this->view->BulkType = $bulkarr;

		// if($this->_custSameUser){
		// 	// echo 'here1';
		// 	$this->view->token = true;
		// 	$userOnBehalf = $this->_userIdLogin;
		// 	$tokenIdUser = $this->_db->select()
		// 	->from(
		// 		array('M_USER'),
		// 		array('TOKEN_ID')
		// 	)
		// 	->where('USER_ID = ?',$userOnBehalf)
		// 	->where('CUST_ID = ?',$this->_custIdLogin)
		// 	->limit(1)
		// ;

		// $tokenIdUser = $this->_db->fetchRow($tokenIdUser);
		// $tokenIdUser = $tokenIdUser['TOKEN_ID'];

		// 	$Token 			= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
		// 	$challengeCode 	= $Token->generateChallengeCode();
		// 	$this->view->userOnBehalf		= $userOnBehalf;
		// 	$this->view->challengeCode		= $challengeCode;
		// }

		//get all adapter profile type (payroll, manytomany, e-money, multi credit, multi debit)
		$select = $this->_db->select()
						->from(array('C' => 'M_CUST_ADAPTER_PROFILE'),array('*'))
						->join(array('A' => 'M_ADAPTER_PROFILE'), 'C.ADAPTER_PROFILE_ID = A.PROFILE_ID', '*')
						->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'A.PROFILE_ID = D.PROFILE_ID', '*')
						->where("CUST_ID = ?",$this->_custIdLogin)
						->where("TRF_TYPE = ?", 'payroll');
		$adapterDataPayroll = $this->_db->FetchAll($select);

		$select = $this->_db->select()
						->from(array('C' => 'M_CUST_ADAPTER_PROFILE'),array('*'))
						->join(array('A' => 'M_ADAPTER_PROFILE'), 'C.ADAPTER_PROFILE_ID = A.PROFILE_ID', '*')
						->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'A.PROFILE_ID = D.PROFILE_ID', '*')
						->where("CUST_ID = ?",$this->_custIdLogin)
						->where("TRF_TYPE = ?", 'manytomany');
		$adapterDataMtm = $this->_db->FetchAll($select);

		$select = $this->_db->select()
						->from(array('C' => 'M_CUST_ADAPTER_PROFILE'),array('*'))
						->join(array('A' => 'M_ADAPTER_PROFILE'), 'C.ADAPTER_PROFILE_ID = A.PROFILE_ID', '*')
						->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'A.PROFILE_ID = D.PROFILE_ID', '*')
						->where("CUST_ID = ?",$this->_custIdLogin)
						->where("TRF_TYPE = ?", 'emoney');
		$adapterDataEmoney = $this->_db->FetchAll($select);

		$select = $this->_db->select()
						->from(array('C' => 'M_CUST_ADAPTER_PROFILE'),array('*'))
						->join(array('A' => 'M_ADAPTER_PROFILE'), 'C.ADAPTER_PROFILE_ID = A.PROFILE_ID', '*')
						->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'A.PROFILE_ID = D.PROFILE_ID', '*')
						->where("CUST_ID = ?",$this->_custIdLogin)
						->where("TRF_TYPE = ?", 'onetomany');
		$adapterDataOtm = $this->_db->FetchAll($select);

		$select = $this->_db->select()
						->from(array('C' => 'M_CUST_ADAPTER_PROFILE'),array('*'))
						->join(array('A' => 'M_ADAPTER_PROFILE'), 'C.ADAPTER_PROFILE_ID = A.PROFILE_ID', '*')
						->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'A.PROFILE_ID = D.PROFILE_ID', '*')
						->where("CUST_ID = ?",$this->_custIdLogin)
						->where("TRF_TYPE = ?", 'manytoone');
		$adapterDataMto = $this->_db->FetchAll($select);

		$this->view->adapterDataPayroll = $adapterDataPayroll;
		$this->view->adapterDataMtm = $adapterDataMtm;
		$this->view->adapterDataEmoney = $adapterDataEmoney;
		$this->view->adapterDataOtm = $adapterDataOtm;
		$this->view->adapterDataMto = $adapterDataMto;

		if($this->_request->isPost() )
		{
$datas = $this->_request->getParams();
// echo "<pre>";
		    // var_dump($datas);die;
			if($this->_custSameUser){
				
				if(!$this->view->hasPrivilege('PRLP')){
					// die('here');
					$error_msg[] = $this->language->_("Error: You don't have privilege to release payment");
					$this->view->error = true;
					$this->view->report_msg = $this->displayError($error_msg);

					$checktoken = false;

				}else{
					// die('sini');

					$challengeCode		= $this->_getParam('challengeCode');

					$inputtoken1 		= $this->_getParam('inputtoken1');
					$inputtoken2 		= $this->_getParam('inputtoken2');
					$inputtoken3 		= $this->_getParam('inputtoken3');
					$inputtoken4 		= $this->_getParam('inputtoken4');
					$inputtoken5 		= $this->_getParam('inputtoken5');
					$inputtoken6 		= $this->_getParam('inputtoken6');

					$responseCode		= $inputtoken1.$inputtoken2.$inputtoken3.$inputtoken4.$inputtoken5.$inputtoken6;

					$Token 		= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
					$verToken 	= $Token->verify($challengeCode, $responseCode);
					// print_r($verToken);
					// die('here');
					if ($verToken['ResponseCode'] != '00'){
						$tokenFailed = $CustUser->setLogToken(); //log token activity

						$this->view->error = true;
						$errorMsg[] = $this->language->_('Invalid Response Code');	//$verToken['ResponseDesc'];

						if ($tokenFailed === true)
						{
							$this->_redirect('/default/index/logout');
						}

						$checktoken = false;

					}else{

						$checktoken = true;
					}
				}
			} else{

				$checktoken = true;
			}


			if ($checktoken) {

				$this->_request->getParams();

			$filter = new Application_Filtering();
			$confirm = false;
			$error_msg[] = "";

			$BULK_TYPE 	= $filter->filter($this->_request->getParam('BULK_TYPE'), "BULK_TYPE");
			// print_r($BULK_TYPE);die;
			// $BULK_TYPE 	= '4';
			// var_dump($BULK_TYPE); die;
			if($BULK_TYPE == '0'){
			// die('here');

			//adapter profile not defaults
			if (!empty($adapterDataOtm)) {
				$extension = $adapterDataOtm[0]['FILE_FORMAT'];
			}
			else{
				$extension = 'csv';
			}

			$fileName = $adapterDataOtm[0]['FILE_PATH'];
	        $fixLength = $adapterDataOtm[0]['FIXLENGTH'];
	        $fixLengthType = $adapterDataOtm[0]['FIXLENGTH_TYPE'];
	        $fixLengthHeader = $adapterDataOtm[0]['FIXLENGTH_HEADER_ORDER'];
	        $fixLengthHeaderName = $adapterDataOtm[0]['FIXLENGTH_HEADER_NAME'];
	        $fixLengthContent = $adapterDataOtm[0]['FIXLENGTH_CONTENT_ORDER'];
	        $delimitedWith = $adapterDataOtm[0]['DELIMITED_WITH'];

			$PS_SUBJECT 	= $filter->filter($this->_request->getParam('PSSUBJECT'), "PS_SUBJECT");
			$PS_EFDATE 		= $filter->filter($this->_request->getParam('PSEFDATE'), "PS_DATE");
			$ACCTSRC 		= $filter->filter($this->_request->getParam('ACCTSRC'), "ACCOUNT_NO");
			// var_dump($ACCTSRC); die;
			if(!$ACCTSRC)
			{
				$error_msg[] = $this->language->_('Source Account cannot be left blank.').'';
				$this->view->error 		= true;
				$this->view->report_msg	= $this->displayError($error_msg);
			}
			else if(!$PS_EFDATE)
			{
				$error_msg[] = $this->language->_('Error').': '.$this->language->_('Payment Date cannot be left blank.').'';
				$this->view->error 		= true;
				$this->view->report_msg	= $this->displayError($error_msg);
			}
			else
			{
				$paramSettingID = array('range_futuredate', 'auto_release_payment');

				$settings = new Application_Settings();
				$settings->setSettings(null, $paramSettingID);       // Zend_Registry => 'APPSETTINGS'
				$ccyList = $settings->setCurrencyRegistered();    // Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
				$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
				$AccArr = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR")));	// show acc in IDR only

				$adapter = new Zend_File_Transfer_Adapter_Http();
				$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';
				$adapter->setDestination ( $this->_destinationUploadDir );
				$extensionValidator = new Zend_Validate_File_Extension(array(false, $extension));
				$extensionValidator->setMessage(
													'Error: Extension file must be *.'.$extension
												);

				$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
				$sizeValidator->setMessage(
					'Error: File exceeds maximum size'
				);

				$adapter->setValidators ( array (
					$extensionValidator,
					$sizeValidator,
				));

				if ($adapter->isValid ())
				{


										$srcData = $this->_db->select()
										->from(array('M_CUSTOMER_ACCT'),array('ACCT_NO','ACCT_TYPE','CCY_ID'))
										->where('ACCT_NO = ?', $ACCTSRC)
										->limit(1);

										$acsrcData = $this->_db->fetchRow($srcData);

										if(!empty($acsrcData)){
											$accsrc = $acsrcData['ACCT_NO'];
											$accsrctype = $acsrcData['ACCT_TYPE'];
											$accsccy = $acsrcData['CCY_ID'];
										}

										$svcInquiry = new Service_Inquiry($this->_userIdLogin,$ACCTSRC,$acsrcData['ACCT_TYPE']);
										$resultKursEx = $svcInquiry->rateInquiry();

										//rate inquiry for display
										$kurssell = '';
										$kurs = '';
										$book = '';
										if($resultKursEx['ResponseCode']=='00'){
											$kursList = $resultKursEx['DataList'];
											$kurssell = '';
											$kurs = '';

											foreach($kursList as $row){
												if($row["currency"] == 'USD'){
													$row["sell"] = str_replace(',','',$row["sell"]);
													$row["book"] = str_replace(',','',$row["book"]);
													$row["buy"] = str_replace(',','',$row["buy"]);
													$kurssell = $row["sell"];
													$book = $row["book"];
													if($ACCTSRC_CURRECY=='IDR'){
														$kurs = $row["sell"];
													}else{
														$kurs = $row["buy"];
													}
												}
											}

										}

					$sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
					$newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

					// var_dump($newFileName);die;

					$adapter->addFilter ( 'Rename',$newFileName  );

					if ($adapter->receive ())
					{	
						$data = $this->convertFileToArray($newFileName, $extension, $delimitedWith);
						//var_dump($adapterDataOtm);die;
						@unlink($newFileName);
						//end

						if (!empty($adapterDataOtm)) {
							//unset header if not fixlength
							if ($fixLength != 1) {
								unset($data[0]);	
							}
						}
						else{
							//unset defaults
							unset($data[0]);
							unset($data[1]);
							unset($data[2]);
							unset($data[3]);
							unset($data[4]);
							unset($data[5]);
							unset($data[6]);
							unset($data[7]);
						}

						$totalRecords = count($data);

						//proses convert ke order yg benar
						if($totalRecords)
						{

							foreach ($adapterDataOtm as $key => $value) {
								$headerOrder[] = $value['HEADER_CONTENT'];
							}

							foreach ($data as $datakey => $datavalue) {

								if (!empty($headerOrder)) {
									foreach ($headerOrder as $key => $value) {

										$headerOrderArr = explode(',', $value);

										if (count($headerOrderArr) > 1) {
											$i = 0;
											$contentStr = '';
											foreach ($headerOrderArr as $key2 => $value2) {

												if ($i != count($headerOrderArr)) {
													$contentStr .= $data[$datakey][$value2].' ';
												}
												$i++;
											}	
											$newData[$datakey][] = $contentStr;
										}
										else{
											$newData[$datakey][] = $data[$datakey][$value];
										}
									}	
								}
								else{
									$newData[$datakey][] = null;
								}
							}
						}

						//check mandatory yang bo setup saat buat business adapter profile
						$mandatoryCheck = $this->validateField($newData, $adapterDataOtm);

						//auto mapping agar isi field file sesuai dengan yang di setup di m_mapping bo
						//echo '<pre>';
							//	 var_dump($data);
							//	 var_dump($totalRecords);
						 		 
						//$fixData = $this->autoMapping($newData, $adapterDataOtm);
						$fixData = $data;
						if($totalRecords)
						{
							if($totalRecords <= $this->_maxRow)
							{
								//die('here');
								$rowNum = 0;

								$paramPayment = array( 	"CATEGORY"      	=> "BULK CREDITT",
														"FROM"       		=> "I",
														"PS_NUMBER"     	=> "",
														"PS_SUBJECT"   	 	=> $PS_SUBJECT,
														"PS_EFDATE"     	=> $PS_EFDATE,
														"_dateFormat"    	=> $this->_dateDisplayFormat,
														"_dateDBFormat"    	=> $this->_dateDBFormat,
														"_addBeneficiary"   => $this->view->hasPrivilege('BADA'),
														"_beneLinkage"    	=> $this->view->hasPrivilege('BLBU'),
														"_createPB"     	=> $this->view->hasPrivilege('CBPW'),
														"_createDOM"    	=> $this->view->hasPrivilege('CBPI'),
														"_createREM"    	=> false,
													  );

								$paramTrxArr = array();
								 
								foreach ( $fixData as $row )
								{
									 //print_r(count($row));
									if(count($row)==7)
									{
										$rowNum++;
										$benefAcct = trim($row[0]);
										$ccy = 'IDR';
										$amount = trim($row[2]);
										$purpose = trim($row[3]);
										$message = trim($row[4]);
										$addMessage = '';
										$type = trim($row[5]);
										$bankCode = trim($row[6]);


										//$TRA_SMS					= trim($row[7]);
										$TRA_SMS					= '';
										$TRA_EMAIL				= '';
										$REFRENCE				= '';

										$fullDesc = array(
											'BENEFICIARY_ACCOUNT' => $benefAcct,
											'BENEFICIARY_ACCOUNT_CCY' => $ccy,
											'TRA_AMOUNT' => $amount,
											'TRA_MESSAGE' => $message,
											'TRA_PURPOSE' => $purpose,
											'REFNO' => $addMessage,
											'TRANSFER_TYPE' => $type,
											'CLR_CODE' => $bankCode,
										);
										// print_r($fullDesc);die;

										$filter = new Application_Filtering();

										$TRA_AMOUNT 		= $filter->filter($amount, "AMOUNT");
										$TRA_MESSAGE 		= $filter->filter($message, "TRA_MESSAGE");
										$TRA_REFNO 			= $filter->filter($addMessage, "TRA_REFNO");
										$ACBENEF 			= $filter->filter($benefAcct, "ACCOUNT_NO");
										$ACBENEF_EMAIL 		= $filter->filter($email, "EMAIL");
										$ACBENEF_CCY 		= $filter->filter($ccy, "SELECTION");
										$ACBENEF_ADDRESS	= $filter->filter($bankCity, "ADDRESS");
										$CLR_CODE			= $filter->filter($bankCode, "BANK_CODE");
										$TRANSFER_TYPE 		= $filter->filter($type, "SELECTION");
										$TRANS_PURPOSE 		= $filter->filter($purpose, "SELECTION");

										$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

										if($TRANSFER_TYPE == 'RTGS'){
											$chargeType = '1';
											$select = $this->_db->select()
															->from('M_CHARGES_OTHER',array('*'))
															->where("CUST_ID = ?",$this->_custIdLogin)
															->where("CHARGES_TYPE = ?",$chargeType);
											$resultSelecet = $this->_db->FetchAll($select);
											$chargeAmt = isset($resultSelecet['0']['CHARGES_AMT']);
										}
										else if($TRANSFER_TYPE == 'SKN'){
											$chargeType1 = '2';
											$select1 = $this->_db->select()
															->from('M_CHARGES_OTHER',array('*'))
															->where("CUST_ID = ?",$this->_custIdLogin)
															->where("CHARGES_TYPE = ?",$chargeType1);
											$resultSelecet1 = $this->_db->FetchAll($select1);
											$chargeAmt = isset($resultSelecet1['0']['CHARGES_AMT']);
										}
										else{
											$chargeAmt = '0';
										}

										$filter->__destruct();
										unset($filter);
										if($accsccy != 'IDR' ){
											// print_r($kurs);
											// print_r($TRA_AMOUNT_num);die;
											$TRA_AMOUNT_NET =	$TRA_AMOUNT_num;
											$TRA_AMOUNT_EQ	=  $TRA_AMOUNT_num/$kurs;
										}else{
											$TRA_AMOUNT_NET = $TRA_AMOUNT_num;
											$TRA_AMOUNT_EQ = $TRA_AMOUNT_num;
										}

										$paramTrx = array("TRANSFER_TYPE" 			=> $TRANSFER_TYPE,
															"TRA_AMOUNT" 				=> $TRA_AMOUNT_NET,
															"TRA_AMOUNTEQ" 				=> $TRA_AMOUNT_EQ,
															"TRANSFER_FEE" 				=> $chargeAmt,
															"TRA_MESSAGE" 				=> $TRA_MESSAGE,
															"TRA_REFNO" 				=> $TRA_REFNO,
															"ACCTSRC" 					=> $ACCTSRC,
															"ACBENEF" 					=> $ACBENEF,
															"ACBENEF_CCY" 				=> $ACBENEF_CCY,
															"ACBENEF_EMAIL" 			=> $ACBENEF_EMAIL,
															"BENEFICIARY_RESIDENT" 		=> $BENEFICIARY_RESIDENT,
															"ACBENEF_CITIZENSHIP" 		=> $ACBENEF_CITIZENSHIP,
															"BANK_CODE" 				=> $CLR_CODE,
															"BENEFICIARY_ID_NUMBER" 	=> $BENEFICIARY_ID_NUMBER,
															"BENEFICIARY_ID_TYPE" 		=> $BENEFICIARY_ID_TYPE,
															"BENEFICIARY_CITY_CODE" 	=> $BENEFICIARY_CITY_CODE,
															"BENEFICIARY_CATEGORY" 		=> $BENEFICIARY_CATEGORY,
															"BANK_NAME" 				=> $BANK_NAME,
															"TRANS_PURPOSE"				=> $TRANS_PURPOSE,
															"PS_NOTIF"					=> $TRA_NOTIF,

															"PS_SMS"					=> $TRA_SMS,
															"PS_EMAIL"					=> $TRA_EMAIL,
															"REFRENCE"					=> $REFRENCE,

														 );
										if($accsccy != 'IDR' ){
											$paramTrx['RATE'] 		=	$kurssell;
											$paramTrx['RATE_BUY'] 	=	$kurs;
											$paramTrx['BOOKRATE'] 	=	$book;
										}
									//	echo '<pre>';
									//	var_dump($paramTrxArr);
								//		var_dump($paramTrx);
										array_push($paramTrxArr,$paramTrx);
										// die('here');
									}
									else
									{
									//	die('here');
										$error_msg[] = $this->language->_('Error').': '.$this->language->_('Wrong File Format').'.';
										$this->view->error 		= true;
										$this->view->report_msg	= $this->displayError($error_msg);
										
										
										break;
									}
								}
							}

							else
							{
								$error_msg[] = 'Error: The number of rows to be imported should not more than '.$this->_maxRow.'.';
								$this->view->error 		= true;
								$this->view->report_msg	= $this->displayError($error_msg);
							}
								//echo '<pre>';
								//var_dump($paramPayment);
								//var_dump($paramTrxArr);
								//var_dump($paramTrx);die;
							if(!empty($error_msg))
							{

								$resWs = array();

								$validate   = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin);
								$resWs = array();
								$resultVal	= $validate->checkCreateBulk($paramPayment, $paramTrxArr,$resWs);
								//$resultVal	= true;
								$payment 		= $validate->getPaymentInfo();

								// Zend_Debug::dump($validate->getErrorMsg(),'err');
								// Zend_Debug::dump($validate->getErrorTrxMsg(),'errT');
								 //die('asd');
								 


								if($validate->isError() === false)	// payment data is valid
								{

									$confirm = true;

									$validate->__destruct();
									unset($validate);
								}
								else
								{
									$errorMsg 		= $validate->getErrorMsg();
									$errorTrxMsg 	= $validate->getErrorTrxMsg();	// array
									$confirm = true;
									$validate->__destruct();

									unset($validate);
									 //print_r($);
									 
									 //Zend_Debug::dump($errorMsg);die;
									if($errorMsg)
									{
										$confirm = true;
										$this->view->PSEFDATE	= $PS_EFDATE;
										$this->view->BULK_TYPE	= $BULK_TYPE;
										$error_msg[] = 'Error: '.$errorMsg;
										$this->view->error 		= true;
										$this->view->report_msg	= $this->displayError($error_msg);
									}
									else
									{
										$confirm = true;
									}
								}
							}
						}
						else
						{
							$error_msg[] = $this->language->_('Error').': '.$this->language->_('Wrong File Format').'.';
							$this->view->error 		= true;
							$this->view->report_msg	= $this->displayError($error_msg);
						}
					}
				}
				else
				{
					// print_r($adapter->getMessages());die;
					$this->view->error = true;
					foreach($adapter->getMessages() as $key=>$val)
					{
						if($key=='fileUploadErrorNoFile')
							$error_msg[] = $this->language->_('Error').': '.$this->language->_('File cannot be left blank. Please correct it').'.';
						else
							$error_msg[] = $val;
						break;
					}
					$errors = $this->displayError($error_msg);
					$this->view->report_msg = $errors;
				}
				}
			}else if($BULK_TYPE == '1'){

				//adapter profile not defaults
				if (!empty($adapterDataMto)) {
					$extension = $adapterDataMto[0]['FILE_FORMAT'];
				}
				else{
					$extension = 'csv';
				}

				$fileName = $adapterDataMto[0]['FILE_PATH'];
		        $fixLength = $adapterDataMto[0]['FIXLENGTH'];
		        $fixLengthType = $adapterDataMto[0]['FIXLENGTH_TYPE'];
		        $fixLengthHeader = $adapterDataMto[0]['FIXLENGTH_HEADER_ORDER'];
		        $fixLengthHeaderName = $adapterDataMto[0]['FIXLENGTH_HEADER_NAME'];
		        $fixLengthContent = $adapterDataMto[0]['FIXLENGTH_CONTENT_ORDER'];
		        $delimitedWith = $adapterDataMto[0]['DELIMITED_WITH'];

				// die('here1');
				$PS_SUBJECT 		= $filter->filter($this->_request->getParam('PSSUBJECT'), "PS_SUBJECT");
				$PS_EFDATE 			= $filter->filter($this->_request->getParam('PSEFDATE'), "PS_DATE");
				$ACBENEF 			= $filter->filter($this->_request->getParam('ACBENEF'), "ACCOUNT_NO");
				$ACBENEF_BANKNAME 	= $filter->filter($this->_request->getParam('ACBENEF_BANKNAME'), "ACCOUNT_NAME");
				$ACBENEF_ALIAS 		= $filter->filter($this->_request->getParam('ACBENEF_ALIAS'), "ACCOUNT_ALIAS");
				$ACBENEF_CCY 		= $filter->filter($this->_request->getParam('CURR_CODE'), "SELECTION");

				$this->view->ACBENEF = $ACBENEF;
				$this->view->ACBENEF_BANKNAME = $ACBENEF_BANKNAME;
				$this->view->ACBENEF_ALIASLABEL = $ACBENEF_ALIAS;
				$this->view->CURR_CODE = $ACBENEF_CCY;
				$this->view->PSSUBJECT = $PS_SUBJECT;

				$minLen = 10;
				$maxLen = 20;
				$error_msg[] = "";

				if (Zend_Validate::is($ACBENEF, 'NotEmpty') == false) {
					$error_msg[] = $this->language->_('Beneficiary Account cannot be left blank').".";
					$this->view->error 		= true;
					$this->view->report_msg	= $this->displayError($error_msg);
				}

				elseif (Zend_Validate::is($ACBENEF, 'Digits') == false) {
					$error_msg[] = $this->language->_('Beneficiary Account must be numeric').".";
					$this->view->error 		= true;
					$this->view->report_msg	= $this->displayError($error_msg);
				}

				elseif (strlen($ACBENEF) < $minLen || strlen($ACBENEF) > $maxLen) {
					//$error_msg[] = "Beneficiary Account length should be between $minLen and $maxLen.";
					$error_msg[] = $this->language->_('Beneficiary Account length should be between 10 and 20.')."";
					$this->view->error 		= true;
					$this->view->report_msg	= $this->displayError($error_msg);
				}


				/*elseif ($ACBENEF_ALIAS == "")
					$error_msg[] = $this->language->_('Beneficiary Alias Name cannot be left blank.')."";*/
				elseif (strlen($ACBENEF_ALIAS) > 35) {
					$error_msg[] = $this->language->_('Maximum lengths of Alias Name is 35 characters. Please correct it').".";
					$this->view->error 		= true;
					$this->view->report_msg	= $this->displayError($error_msg);
				}

				else if ($ACBENEF_CCY == "") {
						$error_msg[] = $this->language->_('Currency cannot be left blank').".";
						$this->view->error 		= true;
						$this->view->report_msg	= $this->displayError($error_msg);
				}

				else if(!$PS_EFDATE) {
						$error_msg[] = $this->language->_('Payment Date can not be left blank').".";
						$this->view->error 		= true;
						$this->view->report_msg	= $this->displayError($error_msg);
						// die('here');
				}



			
				// die('here');
				$paramSettingID = array('range_futuredate', 'auto_release_payment');

				$settings = new Application_Settings();
				$settings->setSettings(null, $paramSettingID);       // Zend_Registry => 'APPSETTINGS'
				$ccyList = $settings->setCurrencyRegistered();    // Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
				$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
				$AccArr = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR")));	// show acc in IDR only

				$adapter = new Zend_File_Transfer_Adapter_Http();

				$adapter->setDestination ( $this->_destinationUploadDir );

				$extensionValidator = new Zend_Validate_File_Extension(array(false, $extension));
				$extensionValidator->setMessage(
					'Error: Extension file must be *.'.$extension
				);

				// $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
				// $sizeValidator->setMessage(
				// 	'File exceeds maximum size'
				// );

				// $adapter->setValidators ( array (
				// 	$extensionValidator,
				// 	$sizeValidator,
				// ));

				if ($adapter->isValid ())
				{
					// die('ger');
					$sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
					$newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

					$adapter->addFilter ( 'Rename',$newFileName  );

					if ($adapter->receive ())
					{
						$data = $this->convertFileToArray($newFileName, $extension, $delimitedWith);

						@unlink($newFileName);
						
						if (!empty($adapterDataMto)) {
							//unset header if not fixlength
							if ($fixLength != 1) {
								unset($data[0]);	
							}
						}
						else{
							//unset defaults
							unset($data[0]);
							unset($data[1]);
							unset($data[2]);
							unset($data[3]);
							unset($data[4]);
							unset($data[5]);
							unset($data[6]);
							unset($data[7]);
						}

						$totalRecords = count($data);

						//proses convert ke order yg benar
						if($totalRecords)
						{

							foreach ($adapterDataMto as $key => $value) {
								$headerOrder[] = $value['HEADER_CONTENT'];
							}

							foreach ($data as $datakey => $datavalue) {

								if (!empty($headerOrder)) {
									foreach ($headerOrder as $key => $value) {

										$headerOrderArr = explode(',', $value);

										if (count($headerOrderArr) > 1) {
											$i = 0;
											$contentStr = '';
											foreach ($headerOrderArr as $key2 => $value2) {

												if ($i != count($headerOrderArr)) {
													$contentStr .= $data[$datakey][$value2].' ';
												}
												$i++;
											}	
											$newData[$datakey][] = $contentStr;
										}
										else{
											$newData[$datakey][] = $data[$datakey][$value];
										}
									}	
								}
								else{
									$newData[$datakey][] = null;
								}
							}
						}

						//check mandatory yang bo setup saat buat business adapter profile
						$mandatoryCheck = $this->validateField($newData, $adapterDataMto);

						//auto mapping agar isi field file sesuai dengan yang di setup di m_mapping bo
						//$fixData = $this->autoMapping($newData, $adapterDataMto);
					//	$fixData = $data;
						//var_dump($data);die('here');
						//if(empty($fixData)){
							$fixData = $data;
							$totalRecords = count($data);
						//}
						if($totalRecords)
						{
							if($totalRecords <= $this->_maxRow)
							{
								// die('here');
								$rowNum = 0;

								$paramPayment = array( "CATEGORY"      		=> "BULK DEBET",
													   "FROM"       		=> "I",
													   "PS_NUMBER"     		=> "",
													   "PS_SUBJECT"    		=> $PS_SUBJECT,
													   "PS_EFDATE"     		=> $PS_EFDATE,
													   "_dateFormat"    	=> $this->_dateDisplayFormat,
													   "_dateDBFormat"    	=> $this->_dateDBFormat,
													   "_addBeneficiary"   	=> $this->view->hasPrivilege('BADA'), // privi BADA (Add Beneficiary)
													   "_beneLinkage"    	=> $this->view->hasPrivilege('BLBU'), // privi BLBU (Linkage Beneficiary User)
													   "_createPB"     		=> $this->view->hasPrivilege('IPMO'), // privi IPMO (Create Bulk Debet Payment by Import File (CSV))
													   "_createDOM"    		=> false,        // cannot create DOM trx
													   "_createREM"    		=> false,        // cannot create REM trx
													  );

								$paramTrxArr = array();
								// echo "<pre>";
								 //print_r($csvData);die;
								foreach ( $fixData as $key=>$row)
								{
									// var_dump(count($row));
									if(count($row)==5)
									{
										// die('here');
										$rowNum++;
										$sourceAcct = trim($row[0]);
										$ccy = trim($row[1]);
										$amount = trim($row[2]);
										$purpose = trim($row[3]);
										$message = trim($row[4]);
										$addMessage = '';

										// if(!empty($row[4]) || !empty($row[5])){
											$TRA_NOTIF			= '2';
										// }else{
										// 	$TRA_NOTIF			= '1';
										// }
										$TRA_SMS					= '';
										$TRA_EMAIL				= '';
										$REFRENCE				= trim($row[4]);


										$filter = new Application_Filtering();

										$TRA_AMOUNT 		= $filter->filter($amount, "AMOUNT");
										$TRA_MESSAGE 		= $filter->filter($message, "TRA_MESSAGE");
										$TRA_REFNO 			= $filter->filter($addMessage, "TRA_REFNO");
										// $ACCTSRC 			= $filter->filter($sourceAcct, "ACCOUNT_NO");
										$ACCTSRC 			= $sourceAcct;
										$TRANSFER_TYPE 		= 'PB';

										$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

										$filter->__destruct();
										unset($filter);

										$paramTrx = array(	"TRANSFER_TYPE" 	=> $TRANSFER_TYPE,
															"TRA_AMOUNT" 		=> $TRA_AMOUNT_num,
															"TRA_MESSAGE" 		=> $TRA_MESSAGE,
															"TRA_REFNO" 		=> $TRA_REFNO,
															"ACCTSRC" 			=> $ACCTSRC,
															"ACBENEF" 			=> $ACBENEF,
															"ACBENEF_CCY" 		=> $ACBENEF_CCY,
															"ACBENEF_EMAIL" 	=> '',

														// for Beneficiary data, except (bene CCY and email), must be passed by reference
															"ACBENEF_BANKNAME" 			=> &$ACBENEF_BANKNAME,
															"ACBENEF_ALIAS" 			=> &$ACBENEF_ALIAS,
															"PS_NOTIF"					=> $TRA_NOTIF,

															"PS_SMS"					=> $TRA_SMS,
															"PS_EMAIL"					=> $TRA_EMAIL,
															"REFRENCE"					=> $REFRENCE,
														//	"ACBENEF_CITIZENSHIP" 		=> $ACBENEF_CITIZENSHIP,		// 1/R: RESIDENT, 2/NR: NON-RESIDENT
														//	"ACBENEF_ADDRESS1" 			=> $ACBENEF_ADDRESS,
														//	"ACBENEF_ADDRESS2" 			=> &$ACBENEF_ADDRESS2,
														//	"ACBENEF_ADDRESS3" 			=> &$ACBENEF_ADDRESS3,

														//	"ORG_DIR" 					=> $ORG_DIR,
														//	"BANK_CODE" 				=> $CLR_CODE,
														//	"BANK_NAME" 				=> $BANK_NAME,
														//	"BANK_BRANCH" 				=> $BANK_BRANCH,
														//	"BANK_ADDRESS1" 			=> $BANK_ADDRESS1,
														//	"BANK_ADDRESS2" 			=> $BANK_ADDRESS2,
														//	"BANK_ADDRESS3" 			=> $BANK_ADDRESS3,
														 );

										array_push($paramTrxArr,$paramTrx);
									}
									else
									{
										//var_dump(count($row));die;
										// print_r($key);
										// print_r($row);
										// die('here');
										$error_msg[] = $this->language->_('Wrong File Format').'';
										$this->view->error 		= true;
										$this->view->report_msg	= $this->displayError($error_msg);
										break;
									}
								}
							}
							// kalo jumlah trx lebih dari setting
							else
							{
								// die('here1');
								$error_msg[] = 'The number of rows to be imported should not more than '.$this->_maxRow.'.';
								$this->view->error 		= true;
								$this->view->report_msg	= $this->displayError($error_msg);
							}
							// print_r($error_msg);die;
							// kalo gak ada error
							if(!empty($error_msg))
							{
								$resWs = array();
								$validate   = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin);
								$resultVal	= $validate->checkCreate($paramPayment , $paramTrxArr, $resWs);

								$payment 		= $validate->getPaymentInfo();
								// var_dump($validate->isError()); die;

								if($validate->isError() === false)	// payment data is valid
								{
									$confirm = true;

									$validate->__destruct();
									unset($validate);

								}
								else
								{
									$errorMsg 		= $validate->getErrorMsg();
									$errorTrxMsg 	= $validate->getErrorTrxMsg();	// array

									$confirm = true;
									
									$validate->__destruct();
									$confirm = true;
									unset($validate);
									// print_r($error_msg);
									// Zend_Debug::dump($validate);die;
									if($errorMsg)
									{
										$error_msg[] = 'Error: '.$errorMsg;
										$this->view->error 		= true;
										$this->view->report_msg	= $this->displayError($error_msg);
									}
									else
									{
										$confirm = true;
									}
								}
							}
						}
						else //kalo total record = 0
						{
							//$error_msg[] = 'Wrong File Format. There is no data on csv File.';
							$error_msg[] = $this->language->_('Wrong File Format').'.';
							$this->view->error 		= true;
							$this->view->report_msg	= $this->displayError($error_msg);
						}
					}
				}
				else
				{
					// print_r($adapter->getMessages());die;
					foreach($adapter->getMessages() as $key=>$val)
					{
						if($key=='fileUploadErrorNoFile'){
							$error_msg[] = $this->language->_('File cannot be left blank. Please correct it').'.';
							$this->view->error 		= true;
							$this->view->report_msg	= $this->displayError($error_msg);
						}else{
							$error_msg[] = $val;
							$this->view->error 		= true;
							$this->view->report_msg	= $this->displayError($error_msg);
						break;
						}
					}
				}



			
		}else if($BULK_TYPE == '2'){

			//adapter profile not defaults
			if (!empty($adapterDataPayroll)) {
				$extension = $adapterDataPayroll[0]['FILE_FORMAT'];
			}
			else{
				$extension = 'csv';
			}

			$fileName = $adapterDataPayroll[0]['FILE_PATH'];
	        $fixLength = $adapterDataPayroll[0]['FIXLENGTH'];
	        $fixLengthType = $adapterDataPayroll[0]['FIXLENGTH_TYPE'];
	        $fixLengthHeader = $adapterDataPayroll[0]['FIXLENGTH_HEADER_ORDER'];
	        $fixLengthHeaderName = $adapterDataPayroll[0]['FIXLENGTH_HEADER_NAME'];
	        $fixLengthContent = $adapterDataPayroll[0]['FIXLENGTH_CONTENT_ORDER'];
	        $delimitedWith = $adapterDataPayroll[0]['DELIMITED_WITH'];

			$PS_SUBJECT 	= $filter->filter($this->_request->getParam('PSSUBJECT'), "PS_SUBJECT");
			$PS_EFDATE 		= $filter->filter($this->_request->getParam('PSEFDATE'), "PS_DATE");
			$ACCTSRC 		= $filter->filter($this->_request->getParam('ACCTSRC'), "ACCOUNT_NO");

			if(!$ACCTSRC)
			{
				$error_msg[] = $this->language->_('Error').': '.$this->language->_('Source Account cannot be left blank.').'';
				$this->view->error 		= true;
				$this->view->report_msg	= $this->displayError($error_msg);
			}
			else if(!$PS_EFDATE)
			{
				$error_msg[] = 'Error: Payment Date can not be left blank.';
				$this->view->error 		= true;
				$this->view->report_msg	= $this->displayError($error_msg);
			}
			else
			{
				$paramSettingID = array('range_futuredate', 'auto_release_payment');

				$settings = new Application_Settings();
				$settings->setSettings(null, $paramSettingID);       // Zend_Registry => 'APPSETTINGS'
				$ccyList = $settings->setCurrencyRegistered();    // Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
				$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
				$AccArr = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR")));	// show acc in IDR only

				$adapter = new Zend_File_Transfer_Adapter_Http();

				$adapter->setDestination ( $this->_destinationUploadDir );
				$extensionValidator = new Zend_Validate_File_Extension(array(false, $extension));
				$extensionValidator->setMessage(
					'Error: Extension file must be *.'.$extension
				);

				$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
				$sizeValidator->setMessage(
					'Error: File exceeds maximum size'
				);

				$adapter->setValidators ( array (
					$extensionValidator,
					$sizeValidator,
				));

				if ($adapter->isValid ())
				{
// die;
					$sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
					$newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

					$adapter->addFilter ( 'Rename',$newFileName  );

					if ($adapter->receive ())
					{
							$data = $this->convertFileToArray($newFileName, $extension, $delimitedWith);
							// var_dump($data);die;
							@unlink($newFileName);

							if (!empty($adapterDataPayroll)) {
								//unset header if not fixlength
								if ($fixLength != 1) {
									unset($data[0]);	
								}
							}
							else{
								//unset defaults
								unset($data[0]);
								unset($data[1]);
								unset($data[2]);
								unset($data[3]);
								unset($data[4]);
								unset($data[5]);
								unset($data[6]);
								unset($data[7]);
								unset($data[8]);
							}

							$totalRecords = count($data);

							//proses convert ke order yg benar
							if($totalRecords)
							{

								foreach ($adapterDataPayroll as $key => $value) {
									$headerOrder[] = $value['HEADER_CONTENT'];
								}

								foreach ($data as $datakey => $datavalue) {

									if (!empty($headerOrder)) {
										foreach ($headerOrder as $key => $value) {

											$headerOrderArr = explode(',', $value);

											if (count($headerOrderArr) > 1) {
												$i = 0;
												$contentStr = '';
												foreach ($headerOrderArr as $key2 => $value2) {

													if ($i != count($headerOrderArr)) {
														$contentStr .= $data[$datakey][$value2].' ';
													}
													$i++;
												}	
												$newData[$datakey][] = $contentStr;
											}
											else{
												$newData[$datakey][] = $data[$datakey][$value];
											}
										}	
									}
									else{
										$newData[$datakey][] = null;
									}
								}
							}
							// echo "<pre>";
							// var_dump($newData)
							// var_dump($adapterDataPayroll);die;
							//check mandatory yang bo setup saat buat business adapter profile
							$mandatoryCheck = $this->validateField($newData, $adapterDataPayroll);

							//auto mapping agar isi field file sesuai dengan yang di setup di m_mapping bo
							$fixData = $this->autoMapping($newData, $adapterDataPayroll);
							 //echo "<pre>";
							// var_dump($adapterDataPayroll);
							 //var_dump($fixData);die;
							// echo $newFileName;
							// if(!empty($fixData)){

							// }
							if(empty($fixData)){
								$fixData = $data;	
								$totalRecords = count($data);
							}
							
							
							//var_dump($data);die;

							if($totalRecords)
							{
								if($totalRecords <= $this->_maxRow)
								{
									$rowNum = 0;

									$paramPayment = array( 	"CATEGORY"      	=> "BULK PAYROLL",
															"FROM"       		=> "I",
															"PS_NUMBER"     	=> "",
															"PS_SUBJECT"   	 	=> $PS_SUBJECT,
															"PS_EFDATE"     	=> $PS_EFDATE,
															"_dateFormat"    	=> $this->_dateDisplayFormat,
															"_dateDBFormat"    	=> $this->_dateDBFormat,
															"_addBeneficiary"   => $this->view->hasPrivilege('BADA'), // privi BADA (Add Beneficiary)
															"_beneLinkage"    	=> $this->view->hasPrivilege('BLBU'), // privi BLBU (Linkage Beneficiary User)
															"_createPB"     	=> $this->view->hasPrivilege('CBPW'), // privi CBPW (Create Bulk Credit Payment PB by Import File (CSV))
															"_createDOM"    	=> $this->view->hasPrivilege('CBPI'), // privi CBPI (Create Bulk Credit Payment Domestic by Import File (CSV))
															"_createREM"    	=> false,        // cannot create REM trx
														  );

									$paramTrxArr = array();
									// Zend_Debug::dump($fixData); die;

									foreach ( $fixData as $row )
									{
										// if(count($row)==4)
										// {
											// var_dump($row);die;
											$rowNum++;
											$benefAcct 		= trim($row[0]);
// 											$benefName 		= trim($row[1]);
											$ccy 			= "IDR";
											$amount 		= trim($row[1]);
											$message 		= trim($row[2]);
											$reference 		= trim($row[3]);
											$addMessage 	= '';
// 											$email 			= trim($row[6]);
// 											$phoneNumber	= trim($row[7]);
											$type 			= 'PB';
// 											$bankCode 		= trim($row[6]);
											//$bankName 		= trim($row[10]);
	//										$bankCity = trim($row[10]);
// 											$benefAdd		= trim($row[9]);
// 											$citizenship	= strtoupper(trim($row[10]));
											//$resident = strtoupper(trim($row[11]));

											/*
											 * Change parameter into document
											 */
											$fullDesc = array(
												'BENEFICIARY_ACCOUNT' 		=> $benefAcct,
												'BENEFICIARY_NAME' 			=> '',
												'BENEFICIARY_ACCOUNT_CCY' 	=> $ccy,
												'TRA_AMOUNT' 				=> $amount,
												'TRA_MESSAGE'				=> $message,
												'REFNO' 					=> $addMessage,
												'BENEFICIARY_EMAIL' 		=> '',
												'BENEFICIARY_MOBILE_PHONE_NUMBER' => '',
												'TRANSFER_TYPE' 			=> 'PB',
												//'CLR_CODE' 					=> $bankCode,
												//'BENEFICIARY_BANK_NAME' 	=> $bankName,
	//											'BENEFICIARY_CITY' => $bankCity,
												'BENEFICIARY_ADDRESS'		=> '',
												'BENEFICIARY_CITIZENSHIP' 	=> ''
												//'BENEFICIARY_RESIDENT' => $resident
											);

											$filter = new Application_Filtering();

											$TRA_AMOUNT 		= $filter->filter($amount, "AMOUNT");
											$TRA_MESSAGE 		= $filter->filter($message, "TRA_MESSAGE");
											$TRA_REFNO 			= $filter->filter($addMessage, "TRA_REFNO");
											$ACBENEF 			= $filter->filter($benefAcct, "ACCOUNT_NO");
											$ACBENEF_BANKNAME 	= $filter->filter('', "ACCOUNT_NAME");
											$ACBENEF_ALIAS 		= $filter->filter('', "ACCOUNT_ALIAS");
											$ACBENEF_EMAIL 		= $filter->filter('', "EMAIL");
											$ACBENEF_PHONE 		= $filter->filter('', "MOBILE_PHONE_NUMBER");
											$ACBENEF_CCY 		= $filter->filter($ccy, "SELECTION");
											$ACBENEF_ADDRESS	= $filter->filter('', "ADDRESS");
											$ACBENEF_CITIZENSHIP= $filter->filter('', "SELECTION");
											//$ACBENEF_RESIDENT= $filter->filter($resident, "SELECTION");
											//$BANK_NAME			= $filter->filter($bankName, "BANK_NAME");
											//$BANK_CITY			= $filter->filter($bankCity, "ADDRESS");
											//$CLR_CODE			= $filter->filter($bankCode, "BANK_CODE");
											$TRANSFER_TYPE 		= $filter->filter($type, "SELECTION");

											$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

											if($TRANSFER_TYPE == 'RTGS'){
												$chargeType = '1';
												$select = $this->_db->select()
																->from('M_CHARGES_OTHER',array('*'))
																->where("CUST_ID = ?",$this->_custIdLogin)
																->where("CHARGES_TYPE = ?",$chargeType);
												$resultSelecet = $this->_db->FetchAll($select);
												$chargeAmt = $resultSelecet['0']['CHARGES_AMT'];
											}
											else if($TRANSFER_TYPE == 'SKN'){
												$chargeType1 = '2';
												$select1 = $this->_db->select()
																->from('M_CHARGES_OTHER',array('*'))
																->where("CUST_ID = ?",$this->_custIdLogin)
																->where("CHARGES_TYPE = ?",$chargeType1);
												$resultSelecet1 = $this->_db->FetchAll($select1);
												$chargeAmt = $resultSelecet1['0']['CHARGES_AMT'];
											}
											else{
												$chargeAmt = '0';
											}


											$filter->__destruct();
											unset($filter);

											$paramTrx = array("TRANSFER_TYPE" 			=> $TRANSFER_TYPE,
																"TRA_AMOUNT" 				=> $TRA_AMOUNT_num,
																"TRANSFER_FEE" 				=> $chargeAmt,
																"TRA_MESSAGE" 				=> $TRA_MESSAGE,
																"TRA_REFNO" 				=> $TRA_REFNO,
																"ACCTSRC" 					=> $ACCTSRC,
																"ACBENEF" 					=> $ACBENEF,
																"ACBENEF_CCY" 				=> $ACBENEF_CCY,
																"ACBENEF_EMAIL" 			=> $ACBENEF_EMAIL,
																"ACBENEF_PHONE" 			=> $ACBENEF_PHONE,

															// for Beneficiary data, except (bene CCY and email), must be passed by reference
																"ACBENEF_BANKNAME" 			=> $ACBENEF_BANKNAME,
																"ACBENEF_ALIAS" 			=> $ACBENEF_ALIAS,
																"ACBENEF_CITIZENSHIP" 		=> $ACBENEF_CITIZENSHIP,		// W: WNI, N: WNA
															//	"ACBENEF_RESIDENT" 			=> $ACBENEF_RESIDENT,		// R: RESIDENT, NR: NON-RESIDENT
																"ACBENEF_ADDRESS1" 			=> $ACBENEF_ADDRESS,
																"REFERENCE"					=> $reference,
															//	"ACBENEF_ADDRESS2" 			=> &$ACBENEF_ADDRESS2,
															//	"ACBENEF_ADDRESS3" 			=> &$ACBENEF_ADDRESS3,

															//	"ORG_DIR" 					=> $ORG_DIR,
																//"BANK_CODE" 				=> $CLR_CODE,
															//	"BANK_NAME" 				=> $BANK_NAME,
															//	"BANK_BRANCH" 				=> $BANK_BRANCH,
															//	"BANK_ADDRESS1" 			=> $BANK_ADDRESS1,
															//	"BANK_ADDRESS2" 			=> $BANK_ADDRESS2,
															//	"BANK_ADDRESS3" 			=> $BANK_ADDRESS3,
															 );

											array_push($paramTrxArr,$paramTrx);

										// }
										// else
										// {
										// 	$error_msg[] = $this->language->_('Error').': '.$this->language->_('Wrong File Formatvv').'.';
										// 	$this->view->error 		= true;
										// 	$this->view->report_msg	= $this->displayError($error_msg);
										// 	break;
										// }
									}
								}
								// kalo jumlah trx lebih dari setting
								else
								{
									$error_msg[] = 'Error: The number of rows to be imported should not more than '.$this->_maxRow.'.';
									$this->view->error 		= true;
									$this->view->report_msg	= $this->displayError($error_msg);
								}

								// kalo gak ada error
								if(!empty($error_msg))
								{


									$validate   = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin);
									// echo "<pre>";
									// var_dump($paramPayment);
									// var_dump($paramTrxArr);
									// die;
									$msg = array();
									$resultVal	= $validate->checkCreatePayroll($paramPayment, $paramTrxArr, $msg);
									$payment 		= $validate->getPaymentInfo();

									// Zend_Debug::dump($validate->getErrorMsg(),'err');
									// Zend_Debug::dump($validate->getErrorTrxMsg(),'errT');
									

									if($validate->isError() === false)	// payment data is valid
									{
										$confirm = true;

										$validate->__destruct();
										unset($validate);
									}
									else
									{
										$confirm = true;
										$errorMsg 		= $validate->getErrorMsg();
										$errorTrxMsg 	= $validate->getErrorTrxMsg();	// array

										$validate->__destruct();

										unset($validate);
										// echo "<pre>";
										// print_r($errorMsg);die;
										// Zend_Debug::dump($validate);die;
										if($errorMsg)
										{
											$error_msg[] = 'Error: '.$errorMsg;
											$this->view->error 		= true;
											$this->view->report_msg	= $this->displayError($error_msg);
										}
										else
										{
											$confirm = true;
										}
									}
									// die;
								}
							}
							else //kalo total record = 0
							{
// 								echo 'here';die;
								//$error_msg[] = 'Error: Wrong File Format. There is no data on csv File.';
								$error_msg[] = $this->language->_('Error').': '.$this->language->_('Wrong File Formatss').'.';
								$this->view->error 		= true;
								$this->view->report_msg	= $this->displayError($error_msg);
							}
						// } else { //kalo csv bukan payroll
							//echo 'gagal';
// 							echo 'here1';die;
						// 	$error_msg[] = $this->language->_('Error').': '.$this->language->_('Wrong File Formatssss').'.';
						// 	$this->view->error 		= true;
						// 	$this->view->report_msg	= $this->displayError($error_msg);
						// }
					}

				}
				else
				{
					$this->view->error = true;
					foreach($adapter->getMessages() as $key=>$val)
					{
						if($key=='fileUploadErrorNoFile')
							$error_msg[] = $this->language->_('Error').': '.$this->language->_('File cannot be left blank. Please correct it').'.';
						else
							$error_msg[] = $val;
						break;
					}
					$errors = $this->displayError($error_msg);
					$this->view->report_msg = $errors;
				}
			}



		}else if($BULK_TYPE == '4'){

				//adapter profile not defaults
				if (!empty($adapterDataMtm)) {
					$extension = $adapterDataMtm[0]['FILE_FORMAT'];
				}
				else{
					$extension = 'csv';
				}

				$fileName = $adapterDataMtm[0]['FILE_PATH'];
		        $fixLength = $adapterDataMtm[0]['FIXLENGTH'];
		        $fixLengthType = $adapterData[0]['FIXLENGTH_TYPE'];
		        $fixLengthHeader = $adapterDataMtm[0]['FIXLENGTH_HEADER_ORDER'];
		        $fixLengthHeaderName = $adapterDataMtm[0]['FIXLENGTH_HEADER_NAME'];
		        $fixLengthContent = $adapterDataMtm[0]['FIXLENGTH_CONTENT_ORDER'];
		        $delimitedWith = $adapterDataMtm[0]['DELIMITED_WITH'];



				// die('here2');
				$filter 	= new Application_Filtering();
				$adapter 	= new Zend_File_Transfer_Adapter_Http ();

				$validate   = new ValidatePaymentMultiple($this->_custIdLogin, $this->_userIdLogin);
				$max		= $this->getSetting('max_import_single_payment');

				$adapter->setDestination ( $this->_destinationUploadDir );
				$extensionValidator = new Zend_Validate_File_Extension(array(false, $extension));
				$extensionValidator->setMessage(
													'Error: Extension file must be *.'.$extension
												);

				$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
				$sizeValidator->setMessage(
											'Error: File siz must not more than '.$this->getSetting('Fe_attachment_maxbyte')
										);

				$adapter->setValidators(array($extensionValidator,$sizeValidator));
				// die('here');
				if ($adapter->isValid ())
				{
					// die('here');
					$sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);

					$newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

					$adapter->addFilter ( 'Rename',$newFileName  );

					if ($adapter->receive ())
					{

						$data = $this->convertFileToArray($newFileName, $extension, $delimitedWith);

						@unlink($newFileName);

						if (!empty($adapterDataMtm)) {
							//unset header if not fixlength
							if ($fixLength != 1) {
								unset($data[0]);	
							}
						}
						else{
							//unset defaults
							unset($data[0]);
							unset($data[1]);
							unset($data[2]);
							unset($data[3]);
							unset($data[4]);
							unset($data[5]);
							unset($data[6]);
							unset($data[7]);
						}


						
						$totalRecords = count($data);

						//proses convert ke order yg benar
						if($totalRecords)
						{

							foreach ($adapterDataMtm as $key => $value) {
								$headerOrder[] = $value['HEADER_CONTENT'];
							}

							foreach ($data as $datakey => $datavalue) {

								if (!empty($headerOrder)) {
									foreach ($headerOrder as $key => $value) {

										$headerOrderArr = explode(',', $value);

										if (count($headerOrderArr) > 1) {
											$i = 0;
											$contentStr = '';
											foreach ($headerOrderArr as $key2 => $value2) {

												if ($i != count($headerOrderArr)) {
													$contentStr .= $data[$datakey][$value2].' ';
												}
												$i++;
											}	
											$newData[$datakey][] = $contentStr;
										}
										else{
											$newData[$datakey][] = $data[$datakey][$value];
										}
									}	
								}
								else{
									$newData[$datakey][] = null;
								}
							}

							//check mandatory yang bo setup saat buat business adapter profile
							$mandatoryCheck = $this->validateField($newData, $adapterDataMtm);

							//auto mapping agar isi field file sesuai dengan yang di setup di m_mapping bo
							$fixData = $this->autoMapping($newData, $adapterDataMtm);

						}

						if(empty($fixData)){
							$fixData = $data;
							$totalRecords = count($data);
						}
						
						if ($totalRecords && empty($mandatoryCheck)){
							if($totalRecords <= $max)
							{
								$no =0;
								foreach ( $fixData as $columns )
								{
									if(count($columns)==14)
									{
										$params['PAYMENT_SUBJECT'] 			= trim($columns[0]);
										$params['SOURCE_ACCT_NO'] 			= trim($columns[1]);
										$params['CCY'] 						= trim('IDR');
										$params['BENEFICIARY_ACCT_NO'] 		= trim($columns[3]);
										$params['BENEFICIARY_ACCT_CCY'] 	= trim('IDR');
										$params['AMOUNT'] 					= trim($columns[5]);
										$params['MESSAGE'] 					= trim($columns[7]);
										$params['TRANS_PURPOSE'] 			= trim($columns[6]);
										$params['ADDITIONAL_MESSAGE'] 		= '';
										$origDate = trim($columns[8]);
										$date = str_replace('/', '-', $origDate );
										$date2 = date_create($date);
										$params['PAYMENT_DATE'] 			= date_format($date2,"Y-m-d");
										$params['TRANSFER_TYPE'] 			= trim($columns[9]);
										$params['BANK_CODE'] 				= trim($columns[10]);
										if(!empty($columns[12]) || !empty($columns[13])){
											$params['TRA_NOTIF']			= '2';
										}else{
											$params['TRA_NOTIF']			= '1';
										}
										$params['PS_SMS']					= trim($columns[12]);
										$params['PS_EMAIL']				= trim($columns[13]);
										$params['TREASURY_NUM']				= '';
										// $params['LLD_TRANSACTION_PURPOSE']  = trim($columns[14]);
										$params['LLD_TRANSACTION_PURPOSE']  = '';


										// var_dump($params['PAYMENT DATE']); die;
										// echo "<pre>";
										// print_r($params);die;

										$PS_SUBJECT 		= $filter->filter($params['PAYMENT_SUBJECT'],"PS_SUBJECT");
										$PS_EFDATE 			= $filter->filter($params['PAYMENT_DATE'],"PS_DATE");
										$TRA_AMOUNT 		= $filter->filter($params['AMOUNT'],"AMOUNT");
										$TRA_MESSAGE 		= $filter->filter($params['MESSAGE'],"TRA_MESSAGE");
										$TRA_REFNO 			= $filter->filter($params['ADDITIONAL_MESSAGE'],"TRA_REFNO");
										$ACCTSRC 			= $filter->filter($params['SOURCE_ACCT_NO'],"ACCOUNT_NO");
										$ACBENEF 			= $filter->filter($params['BENEFICIARY_ACCT_NO'],"ACCOUNT_NO");
										$ACBENEF_BANKNAME 	= $filter->filter($params['BENEFICIARY_NAME'],"ACCOUNT_NAME");
										$ACBENEF_CCY 		= $filter->filter($params['CCY'],"SELECTION");
										$CLR_CODE			= $filter->filter($params['BANK_CODE'], "BANK_CODE");
										$TRANSFER_TYPE 		= $filter->filter($params['TRANSFER_TYPE'], "SELECTION");

										$BENEFICIARY_ACCT_CCY			= $filter->filter($params['BENEFICIARY_ACCT_CCY'], "BENEFICIARY_ACCT_CCY");
										$TRA_NOTIF			= $filter->filter($params['TRA_NOTIF'], "TRA_NOTIF");
										$TRA_SMS			= $filter->filter($params['TRA_SMS'], "TRA_SMS");
										$TRA_EMAIL			= $filter->filter($params['TRA_EMAIL'], "TRA_EMAIL");
										$TREASURY_NUM		= $filter->filter($params['TREASURY_NUM'], "TREASURY_NUM");
										$LLD_TRANSACTION_PURPOSE		= $filter->filter($params['LLD_TRANSACTION_PURPOSE'], "LLD_TRANSACTION_PURPOSE");

										// var_dump($PS_EFDATE); die;
										$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

										if($TRANSFER_TYPE == 'RTGS'){
											$chargeType = '1';
											$select = $this->_db->select()
															->from('M_CHARGES_OTHER',array('*'))
															->where("CUST_ID = ?",$this->_custIdLogin)
															->where("CHARGES_TYPE = ?",$chargeType);
											$resultSelecet = $this->_db->FetchAll($select);
											$chargeAmt = $resultSelecet['0']['CHARGES_AMT'];

											//$param['TRANSFER_FEE'] = $chargeAmt;
										}
										else if($TRANSFER_TYPE == 'SKN'){
											$chargeType1 = '2';
											$select1 = $this->_db->select()
															->from('M_CHARGES_OTHER',array('*'))
															->where("CUST_ID = ?",$this->_custIdLogin)
															->where("CHARGES_TYPE = ?",$chargeType1);
											$resultSelecet1 = $this->_db->FetchAll($select1);
											$chargeAmt = $resultSelecet1['0']['CHARGES_AMT'];

											//$param['TRANSFER_FEE'] = $chargeAmt1;
										}
										else{
											$chargeAmt = '0';
											//$param['TRANSFER_FEE'] = $chargeAmt2;
										}


										if($BENEFICIARY_ACCT_CCY != $ACBENEF_CCY){
											$CROSS_CURR = '2';
										}else{
											$CROSS_CURR = '1';
										}

										$paramPayment = array(
												"CATEGORY" 					=> "SINGLE PAYMENT",
												"FROM" 						=> "I",				// F: Form, I: Import
												"PS_NUMBER"					=> "",
												"PS_SUBJECT"				=> $PS_SUBJECT,
												"PS_EFDATE"					=> $PS_EFDATE,
												"_dateFormat"				=> 'yyyy-MM-dd',
												"_dateDBFormat"				=> $this->_dateDBFormat,
												"_addBeneficiary"			=> $this->view->hasPrivilege('BADA'),	// privi BADA (Add Beneficiary)
												"_beneLinkage"				=> $this->view->hasPrivilege('BLBU'),	// privi BLBU (Linkage Beneficiary User)
												"_createPB"					=> $this->view->hasPrivilege('CRIP'),								// cannot create PB trx
												"_createDOM"				=> $this->view->hasPrivilege('CRDI'),	// privi CDFT (Create Domestic Fund Transfer)
												"_createREM"				=> $this->view->hasPrivilege('CRIR'),								// cannot create REM trx
												"TRA_CCY"					=> $BENEFICIARY_ACCT_CCY,
												"CROSS_CURR"				=> $CROSS_CURR
										);

										$paramTrxArr[0] = array(
												"TRANSFER_TYPE" 			=> $TRANSFER_TYPE,
												"TRA_AMOUNT" 				=> $TRA_AMOUNT_num,
												"TRANSFER_FEE" 				=> $chargeAmt,
												"TRA_MESSAGE" 				=> $TRA_MESSAGE,
												"TRA_REFNO" 				=> $TRA_REFNO,
												"ACCTSRC" 					=> $ACCTSRC,
												"ACBENEF" 					=> $ACBENEF,
												"ACBENEF_CCY" 				=> $ACBENEF_CCY,
												"ACBENEF_EMAIL" 			=> $ACBENEF_EMAIL,
												"ACBENEF_BANKNAME" 			=> $ACBENEF_BANKNAME,
//												"ACBENEF_ALIAS" 			=> $ACBENEF_ALIAS,
												"ACBENEF_CITIZENSHIP" 		=> $ACBENEF_CITIZENSHIP,		// 1/R: RESIDENT, 2/NR: NON-RESIDENT
												// 																"ACBENEF_ADDRESS1" 			=> $BANK_CITY,
//												"ACBENEF_RESIDENT" 			=> $ACBENEF_RESIDENT,
												"BENEFICIARY_RESIDENT" 		=> $BENEFICIARY_RESIDENT,
												"BANK_CODE" 				=> $CLR_CODE,
//												"BENEFICIARY_BANK_NAME"		=> $BANK_NAME,
//												"LLD_IDENTICAL" 			=> "",
//												"LLD_CATEGORY" 				=> "",
//												"LLD_RELATIONSHIP" 			=> "",
//												"LLD_PURPOSE" 				=> "",
//												"LLD_DESCRIPTION" 			=> "",
												"LLD_TRANSACTION_PURPOSE"	=> $LLD_TRANSACTION_PURPOSE,
												"BENEFICIARY_ID_NUMBER" 	=> $BENEFICIARY_ID_NUMBER,
												"BENEFICIARY_ID_TYPE" 		=> $BENEFICIARY_ID_TYPE,
												"BENEFICIARY_CITY_CODE" 	=> $BENEFICIARY_CITY_CODE,
												"BENEFICIARY_ACCT_CCY" 		=> $BENEFICIARY_ACCT_CCY,
												"PS_NOTIF" 				=> $TRA_NOTIF,
												"PS_SMS" 					=> $TRA_SMS,
												"PS_EMAIL" 				=> $TRA_EMAIL,
												"REFERENCE" 				=> $TREASURY_NUM,

												"BANK_NAME" 	=> $BANK_NAME,

										);

										$arr[$no]['paramPayment'] = $paramPayment;
										$arr[$no]['paramTrxArr'] = $paramTrxArr;
									}
									else
									{
										// die('ge');
										$this->view->error 		= true;
										break;
									}
									$no++;
								}

								if(!$this->view->error)
								{
									$resWs = array();
									$err 	= array();

									$resultVal	= $validate->checkCreate($arr, $resWs);
									$payment 	= $validate->getPaymentInfo();
									$errorTrxMsg 	= $validate->getErrorTrxMsg();

									// Zend_Debug::dump($errorTrxMsg);die;
									$i = 0;
									foreach($resWs as $key=>$dataAcctType){
										//Zend_Debug::dump($dataAcctType);
										$arr[$key]['paramTrxArr'][0]['ACCOUNT_TYPE'] = $dataAcctType['accountType'];
									}


									//die;

									$sourceAccountType 	= $resWs['accountType'];

									$content['payment'] = $payment;
									$content['arr'] 	= $arr;
									$content['errorTrxMsg'] 	= $errorTrxMsg;
									$content['sourceAccountType'] 	= $sourceAccountType;

									$sessionNamespace = new Zend_Session_Namespace('confirmImportCredit');
									$sessionNamespace->content = $content;

									$this->_redirect('/singlepayment/import/confirm');
								}

							}
							else
							{
								// die('here');
								$this->view->error2 = true;
								$this->view->max 	= $max;
							}
						}else{
							// die('here1');
							$this->view->error = true;

							if (!empty($adapter->getMessages())) {
								$error_msg = array($adapter->getMessages());
							}

							if (!empty($mandatoryCheck)) {
								foreach ($mandatoryCheck as $key => $value) {
									array_push($error_msg, $value.' Field Cannot be left blank');
								}
							}

							$this->view->report_msg = $this->displayError($error_msg);
						}
					}
				}
				else
				{
					// die('here3');
					$this->view->error = true;
					$error_msg = array($adapter->getMessages());
					$this->view->report_msg = $this->displayError($error_msg);
				}

			}


			if($confirm)
			{
				$content['paramPayment'] = $paramPayment;
				$content['paramTrxArr'] = $paramTrxArr;
				$content['errorTrxMsg'] = $errorTrxMsg;
				$content['payment'] = $payment;
				if($BULK_TYPE=='0'){
					$content['sourceAccountType'] = $sourceAccountType;
					$sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
					$sessionNamespace->content = $content;
					$this->_redirect('/multicredit/bulk/confirm');

				}else if($BULK_TYPE=='1'){
					$sessionNamespace = new Zend_Session_Namespace('confirmBulkDebet');
					$sessionNamespace->content = $content;
					$this->_redirect('/multidebet/bulk/confirm');

				}else if($BULK_TYPE=='2'){
					$sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
					$sessionNamespace->content = $content;
					$this->_redirect('/payroll/bulk/confirm');
				}else if($BULK_TYPE == '3'){

									$resWs = array();
									$err 	= array();

									$resultVal	= $validate->checkCreate($arr, $resWs);
									$payment 	= $validate->getPaymentInfo();
									$errorTrxMsg 	= $validate->getErrorTrxMsg();

									//Zend_Debug::dump($resWs);die;
									$i = 0;
									foreach($resWs as $key=>$dataAcctType){
										//Zend_Debug::dump($dataAcctType);
										$arr[$key]['paramTrxArr'][0]['ACCOUNT_TYPE'] = $dataAcctType['accountType'];
									}


									//die;

									$sourceAccountType 	= $resWs['accountType'];

									$content['payment'] = $payment;
									$content['arr'] 	= $arr;
									$content['errorTrxMsg'] 	= $errorTrxMsg;
									$content['sourceAccountType'] 	= $sourceAccountType;

									$sessionNamespace = new Zend_Session_Namespace('confirmImportCredit');
									$sessionNamespace->content = $content;

									$this->_redirect('/singlepayment/import/confirm');


				}




			}

			
			
			$this->view->PSSUBJECT = $PS_SUBJECT;
			$this->view->ACCTSRC = $ACCTSRC;
			$this->view->PSEFDATE = $PS_EFDATE;
				
			}



		}
		
		$this->view->PSEFDATE = $PS_EFDATE;
			//var_dump($BULK_TYPE)
			if($BULK_TYPE == '0'){
				$this->view->BULK_TYPE = '0';
			}else if(empty($BULK_TYPE)){
				$this->view->BULK_TYPE = '2';	
			}else{
				$this->view->BULK_TYPE = $BULK_TYPE;
			}
		Application_Helper_General::writeLog('CBPI','Viewing Create Bulk Credit Payment Domestic by Import File (CSV)');
		Application_Helper_General::writeLog('CBPW','Viewing Create Bulk Credit Payment In House by Import File (CSV)');
	}

	private function resData($benefAccount){
			$select	= $this->_db->select()
								->from(array('B'	 			=> 'M_BENEFICIARY'), array('BANK_NAME','BENEFICIARY_NAME','BENEFICIARY_ACCOUNT','BENEFICIARY_CITIZENSHIP','BENEFICIARY_RESIDENT','BENEFICIARY_ID_NUMBER','BENEFICIARY_ID_TYPE','BENEFICIARY_CITY_CODE','BENEFICIARY_CATEGORY')
									   );
			$select->where("B.BENEFICIARY_ACCOUNT = ?", $benefAccount);

			$bene = $this->_db->fetchAll($select);
			return $bene;
	}

	public function confirmAction()
	{
		$sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
		$data = $sessionNamespace->content;
//		die;
		$sourceAcct = $data['payment']['acctsrcArr'][$data['paramTrxArr'][0]['ACCTSRC']];

		$this->view->CCY = $sourceAcct['CCY_ID'];
		$this->view->PS_SUBJECT = $data['paramPayment']['PS_SUBJECT'];
		$this->view->PS_EFDATE = $data['paramPayment']['PS_EFDATE'];
		$this->view->ACCTSRC = $data['paramTrxArr'][0]['ACCTSRC'].' / '.$sourceAcct['ACCT_NAME'].' / '.$sourceAcct['CCY_ID'];
		//if($sourceAcct['ACCT_ALIAS']) $this->view->ACCTSRC .= ' / '.$sourceAcct['ACCT_ALIAS'];

		$this->view->countTrxCCY = $data["payment"]["countTrxCCY"];
		$this->view->sumTrxCCY = $data["payment"]["sumTrxCCY"];

		$chargesAmt = array();
		$totalChargesAmt = 0;

		foreach($data['paramTrxArr'] as $row)
		{
//			$benefAccount = $row["ACBENEF"];
//			if($row["TRANSFER_TYPE"] !="PB"){
//				$dataRes = $this->resData($benefAccount);
//
//				$row['BENEFICIARY_ID_NUMBER'] = $dataRes[0]['BENEFICIARY_ID_NUMBER'];
//				$row['BENEFICIARY_ID_TYPE'] = $dataRes[0]['BENEFICIARY_ID_TYPE'];
//				$row['BENEFICIARY_CITY_CODE'] = $dataRes[0]['BENEFICIARY_CITY_CODE'];
//				$row['BENEFICIARY_CATEGORY'] = $dataRes[0]['BENEFICIARY_CATEGORY'];
//				$row['BENEFICIARY_BANK_NAME'] = $dataRes[0]['BANK_NAME'];
//				$row['BENEFICIARY_CITIZENSHIP'] = $dataRes[0]['BENEFICIARY_CITIZENSHIP'];
//				$row['BENEFICIARY_RESIDENT'] = $dataRes[0]['BENEFICIARY_RESIDENT'];
//				$row['BENEFICIARY_ACCOUNT_NAME'] = $dataRes[0]['BENEFICIARY_NAME'];
//			}

			if(!isset($chargesAmt[$row['TRANSFER_TYPE']][$row['ACBENEF_CCY']])) $chargesAmt[$row['TRANSFER_TYPE']][$row['ACBENEF_CCY']] = 0;
			//$chargesObj = Charges::factory($this->_custIdLogin, $row['TRANSFER_TYPE']);

//			if(empty($row['TRANSFER_TYPE'])){
//				$transferType = '0';
//			}
//			else{
//				$transferType = $row['TRANSFER_TYPE'];
//			}
			$chargesObj = Charges::factory($this->_custIdLogin, isset($row['TRANSFER_TYPE']));

			$paramCharges = array("accsrc" => $row['ACCTSRC'], "transferType" => $row['TRANSFER_TYPE']);
			if(!isset($data['errorTrxMsg'][$row['TRANSFER_TYPE']][$row['ACBENEF_CCY']]))
			{
// 				$chargesAmt[$row['TRANSFER_TYPE']][$row['ACBENEF_CCY']] += $chargesObj->getCharges($paramCharges);
// 				$totalChargesAmt += $chargesObj->getCharges($paramCharges);
			}
		}
		$this->view->chargesAmt = $chargesAmt;
		$this->view->totalChargesAmt = $totalChargesAmt;

		$totalSuccess = 0;
		foreach($data["payment"]["countTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$totalSuccess += $ccy['success'];
			}
		}

		$amountSuccess = 0;
		foreach($data["payment"]["sumTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$amountSuccess += $ccy['success'];
			}
		}

		$totalFailed = 0;
		foreach($data["payment"]["countTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$totalFailed += $ccy['failed'];
			}
		}

		$amountFailed = 0;
		foreach($data["payment"]["sumTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$amountFailed += $ccy['failed'];
			}
		}

		$settings = new Settings();
		$this->view->cutOffSKN = $settings->getSetting('cut_off_time_skn');
		$this->view->cutOffRTGS = $settings->getSetting('cut_off_time_rtgs');
		$this->view->cutOffBI = $settings->getSetting('cut_off_time_bi');

		$this->view->totalSuccess = $totalSuccess;
		$this->view->amountSuccess = $amountSuccess;
		$this->view->totalFailed = $totalFailed;
		$this->view->amountFailed = $amountFailed;

		if($this->_request->isPost() )
		{
			if($this->_getParam('submit_cancel') == TRUE)
			{
				unset($_SESSION['confirmBulkCredit']);
				$this->_redirect('/multicredit/bulk');
			}

			if ($data["payment"]["countTrxPB"] == 0)
				$priviCreate = 'CBPI';
			else
				$priviCreate = 'CBPW';

			$param['PS_SUBJECT'] = $data['paramPayment']['PS_SUBJECT'];
			$param['PS_EFDATE']  = Application_Helper_General::convertDate($data['paramPayment']['PS_EFDATE'], $this->_dateDBFormat, $this->_dateDisplayFormat);
			$param['PS_TYPE'] 	= $this->_paymenttype['code']['bulkcredit'];
			$param['PS_CCY']  = $data['payment']['acctsrcArr'][$data['paramTrxArr'][0]['ACCTSRC']]['CCY_ID'];

			$param['TRANSACTION_DATA'] = array();
			foreach($data['paramTrxArr'] as $row)
			{
				$param['TRANSACTION_DATA'][] = array(
						'SOURCE_ACCOUNT' 			=> $row['ACCTSRC'],
						'BENEFICIARY_ACCOUNT' 		=> $row['ACBENEF'],
						'BENEFICIARY_ACCOUNT_CCY' 	=> $row['ACBENEF_CCY'],
						'BENEFICIARY_ACCOUNT_NAME' 	=> $row['ACBENEF_BANKNAME'], //ADA
//						'BENEFICIARY_ALIAS_NAME' 	=> $row['ACBENEF_ALIAS'], //ADA
						'BENEFICIARY_ALIAS_NAME' 	=> $row['ACBENEF_BANKNAME'], //ADA
						'BENEFICIARY_EMAIL' 		=> $row['ACBENEF_EMAIL'],
						'BENEFICIARY_ADDRESS' 		=> $row['ACBENEF_ADDRESS1'], //ADA
						'BENEFICIARY_CITIZENSHIP' 	=> $row['ACBENEF_CITIZENSHIP'],
						//'ACBENEF_ADDRESS1' 			=> $row['ACBENEF_ADDRESS1'],
						'BENEFICIARY_RESIDENT' 		=> $row['BENEFICIARY_RESIDENT'],
						'CLR_CODE' 					=> $row['BANK_CODE'],
						'TRANSFER_TYPE' 			=> $row['TRANSFER_TYPE'], //SKN,RTGS,PB
						'TRA_AMOUNT' 				=> $row['TRA_AMOUNT'],
						'TRANSFER_FEE' 				=> $row['TRANSFER_FEE'],
						'TRA_MESSAGE' 				=> $row['TRA_MESSAGE'],
						'TRA_REFNO' 				=> $row['TRA_REFNO'],
						'sourceAccountType' 		=> $data['sourceAccountType'],

						'BENEFICIARY_BANK_NAME' 	=> $row['BANK_NAME'], //ADA
						'LLD_CATEGORY' 				=> $row['BENEFICIARY_CATEGORY'],
						'CITY_CODE' 				=> $row['BENEFICIARY_CITY_CODE'],
						'LLD_BENEIDENTIF' 			=> $row['BENEFICIARY_ID_TYPE'],
						'LLD_BENENUMBER' 			=> $row['BENEFICIARY_ID_NUMBER'],

				);
			}

			$param['_addBeneficiary'] = $data['paramPayment']['_addBeneficiary'];
			$param['_beneLinkage'] = $data['paramPayment']['_beneLinkage'];
			$param['_priviCreate'] = $priviCreate;
			//$param['sourceAccountType'] = $data['sourceAccountType'];


			$BulkPayment = new BulkPayment("", $this->_custIdLogin, $this->_userIdLogin);
//			Zend_Debug::dump($param);die;
			$result = $BulkPayment->createPayment($param);
			if($result)
			{
				unset($_SESSION['confirmBulkCredit']);
				$this->_redirect('/notification/success');
			}
			else
			{
				$this->view->error = true;
				$error_msg[] = 'Error: Transaction failed';
				$this->view->report_msg	= $this->displayError($error_msg);
				$this->_redirect('/multicredit/bulk');
			}
		}
	}

	private function parseCSV($fileName){
		$csvData = false;
		try {
				$Csv = new Application_Csv (  $fileName, $separator = "," );
				$csvData = $Csv->readAll ();
				// var_dump($csvData);die;
			} catch ( Exception $e ) {
				echo nl2br ( $e->getTraceAsString () );
			}
			return $csvData;
	}

	private function convertFileToArray($newFileName, $extension, $delimitedWith){

		$file_contents = file_get_contents($newFileName);

        //if csv occured
        if ($extension === 'csv') {

            if (!empty($delimitedWith)) {
                $data = $this->_helper->parser->parseCSV($newFileName, $delimitedWith);
            }
            //if fix length
            else if ($fixLength == 1) {

            	$fileContents = file($newFileName);

            	$contentOrder = $fixLengthContent;

            	// if with header
                if ($fixLengthType == 1) {
                    //karena yg diambil hanya order dri row ke 2 saja
                    $startArrIndex = 1;
                    $surplusIndex = 0;
                }
                else if ($fixLengthType == 3) {
                    //karena yg diambil hanya order dri row ke 1 saja
                    $startArrIndex = 0;
                    $surplusIndex = 1;
                }

                $contentOrderArr = explode(',', $contentOrder);

                if (count($contentOrderArr) > 1) {
                    foreach ($fileContents as $key => $value) {
                        if ($key >= $startArrIndex) {
                            foreach ($contentOrderArr as $key2 => $value2) {
                                //first order, startIndex from 0
                                if ($key2 == 0) {
                                    $startIndex = 0;
                                    $endIndex = (int) $value2 + 1;

                                    $data[$key + $surplusIndex][] = substr($value,$startIndex,$endIndex);
                                }
                                else{
                                    $startIndex = (int) ($contentOrderArr[$key2 - 1] + 1);
                                    $endIndex = (int) ($value2 - ($startIndex - 1));

                                    $data[$key + $surplusIndex][] = substr($value,$startIndex,$endIndex);
                                }
                            }       
                        }   
                    }
                }
            }
            else{
                $data = $this->_helper->parser->parseCSV($newFileName);
            }
        }
        //if txt occured
        else if ($extension === 'txt') {

           $lines = file($newFileName);

           $checkMt940 = false;
           $checkMt101 = false;
           foreach ($lines as $line) {
               if (strpos($line, '{1:') !== false) {
                   $checkMt940 = true;
               }
               else if (strpos($line, ':20:') !== false) {
                   $checkMt101 = true;
               }
           }

           //if mt940 format
           if ($checkMt940) {
               
                $data = $this->_helper->parser->mt940($newFileName);
                $mtFile = true;
           }
           else if($checkMt101){
                $data = $this->_helper->parser->mt101($newFileName);
                $mtFile = true;
           }
           else{
           		//parse csv jg bs utk txt
                if (!empty($delimitedWith)) {
                    $data = $this->_helper->parser->parseCSV($newFileName, $delimitedWith);
                }
                //if fix length
                else if ($fixLength == 1) {

                    $fileContents = file($newFileName);

	            	$contentOrder = $fixLengthContent;

	            	// if with header
	                if ($fixLengthType == 1) {
	                    //karena yg diambil hanya order dri row ke 2 saja
	                    $startArrIndex = 1;
	                    $surplusIndex = 0;
	                }
	                else if ($fixLengthType == 3) {
	                    //karena yg diambil hanya order dri row ke 1 saja
	                    $startArrIndex = 0;
	                    $surplusIndex = 1;
	                }

	                $contentOrderArr = explode(',', $contentOrder);

	                if (count($contentOrderArr) > 1) {
	                    foreach ($fileContents as $key => $value) {
	                        if ($key >= $startArrIndex) {
	                            foreach ($contentOrderArr as $key2 => $value2) {
	                                //first order, startIndex from 0
	                                if ($key2 == 0) {
	                                    $startIndex = 0;
	                                    $endIndex = (int) $value2 + 1;

	                                    $data[$key + $surplusIndex][] = substr($value,$startIndex,$endIndex);
	                                }
	                                else{
	                                    $startIndex = (int) ($contentOrderArr[$key2 - 1] + 1);
	                                    $endIndex = (int) ($value2 - ($startIndex - 1));

	                                    $data[$key + $surplusIndex][] = substr($value,$startIndex,$endIndex);
	                                }
	                            }       
	                        }   
	                    }
	                }                   
                }
                else{
                 	$data = $this->_helper->parser->parseCSV($newFileName, ',');
                }
           }
        }
        //if json occured
        else if ($extension === 'json') {

            $datajson = json_decode($file_contents, 1);
            $i = 0;
            foreach ($datajson as $key => $value) {
                if ($i == 0) {
                    $data[$i] = array_keys($value);
                    $data[$i + 1] = array_values($value);
                }
                else{
                    $data[$i+1] = array_values($value);
                }

                $i++;
            }
        }
        //if xml occured
        else if($extension === 'xml'){

            $xml = (array) simplexml_load_string($file_contents);

             $i = 0;
            foreach ($xml as $key => $value) {
                foreach ($value as $key2 => $value2) {
                    if ($i == 0) {
                        $data[$i] = array_keys((array)$value2);
                        $data[$i + 1] = array_values((array)$value2);
                    }
                    else{
                        $data[$i+1] = array_values((array)$value2);
                    }

                    $i++;
                }
            }

            for($i=0; $i<count($data); $i++){
                if ($i > 0) {
                    foreach ($data[$i] as $key => $value) {
                        if (empty($value)) {
                            $data[$i][$key] = null;
                        }
                    }
                }
            }

            // print_r($data);die();
        }
        else if($extension === 'xls' || $extension === 'xlsx'){
            try {
                $inputFileType = IOFactory::identify($newFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($newFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($newFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
             
            for ($row = 1; $row <= $highestRow; $row++){                        
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row);

                $data[$row - 1] = $rowData[0];
            }
        }

        return $data;
	}

	//validate mandatory
	private function validateField($data, $adapterData){

		foreach ($adapterData as $key => $value) {
			if ($value['MANDATORY'] == 1) {
				$mandatoryArr[] = $value['HEADER_INDEX'];
			}
		}

		$field = array();
		foreach ($data as $key => $value) {

			foreach ($mandatoryArr as $key2 => $value2) {
				if(empty($value[$value2])){
					$field[] = $adapterData[$value2]['HEADER_NAME'];
				}
			}
		}
		return $field;
	}

	//validate 
	private function autoMapping($data, $adapterData){

		foreach ($adapterData as $key => $value) {
			if (!empty($value['HEADER_FUNCTION'])) {

				$mappingTag = explode('_', $value['HEADER_FUNCTION']);

				$functionField[] = array(
					'headerIndex' => $value['HEADER_INDEX'],
					'mappingTag' => $mappingTag[0]
				);
			}
		}

		$newData = array();
		foreach ($functionField as $key => $value) {

			if (!empty($newData)) {

				foreach ($newData as $newdatakey => $newdatavalue) {
					//select m_mapping
					$select = $this->_db->select()
							->from('M_MAPPING',array('*'))
							->where("TAG = ?", $value['mappingTag'])
							->where("TEXT LIKE ?", $newdatavalue[$value['headerIndex']]);
					$mappingData = $this->_db->fetchRow($select);

					if (!empty($mappingData)) {
						$newdatavalue[$value['headerIndex']] = $mappingData['MAPPING_TO'];

					}
					$newData[$newdatakey] = $newdatavalue;
				}
			}
			else{

				foreach ($data as $datakey => $datavalue) {
					//select m_mapping
					$select = $this->_db->select()
							->from('M_MAPPING',array('*'))
							->where("TAG = ?", $value['mappingTag'])
							->where("TEXT LIKE ?", $datavalue[$value['headerIndex']]);
					$mappingData = $this->_db->fetchRow($select);

					if (!empty($mappingData)) {
						$datavalue[$value['headerIndex']] = $mappingData['MAPPING_TO'];

					}
					$newData[$datakey] = $datavalue;
				}
			}
		}

		return $newData;
	}

}
