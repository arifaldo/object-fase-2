<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/SystemBalance.php';
require_once 'General/CustomerUser.php';

class apikeymanagement_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF'; // masih harus diganti
	protected $_destinationUploadDir = '';

	public function initController()
    {
        $this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';

        $setting = new Settings();
        $this->_maxRow = $setting->getSetting('max_import_bulk');
    }

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$step = $this->_getParam('step');
		
		
		
		
		$banklist = $this->_db->fetchAll(
                    $this->_db->select()
                         ->from(array('A' => 'M_BANK_TABLE'))
                         // ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
                         //  ->join(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
                         //  // ->join(array('D' => 'M_DOMESTIC_BANK_TABLE'),'D.BANK_NAME = B.BANK_NAME',array('D.SWIFT_CODE'))
                         // // ->where('A.ACCT_STATUS = ?','5')
                         ->where("A.BANK_CODE IN ('008','014','013','009','032','031','153','002','022','046') ")
                         // ->order('A.APIKEY_ID ASC')
                         // || $tempdata['bankcode'] == '032' || $tempdata['bankcode'] == '031' || $tempdata['bankcode'] == '153'
                );

    			$this->view->banklist =$banklist;

    	$frontendOptions = array ('lifetime' => 86400,
								  'automatic_serialization' => true );
		$backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
		$cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
		$cacheID = 'MAGR'.$this->_custIdLogin;
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

		$datacache = $cache->load($cacheID);
		if(!empty($datacache)){$total = count($datacache['datapers']);}
		
		// echo "<pre>";
		// var_dump($datacache);

		// $datarek = array_push($inputdata,$data['datapers']);

		// var_dump($datarek);die;

		// $accountdata = $data['datapers'];					        
		// var_dump(expression)


		if ($this->_getParam('submit')) {

			//---------------------------------initial step-------------------------------------
			if ($step == 0) {

				$sessionNamespace 	= new Zend_Session_Namespace('tempdata');
				
				$bankcode = $this->_getParam('bank_code');

				$tempdata['bankcode'] = $bankcode;
				$sessionNamespace->tempdata 	= $tempdata;
				// var_dump($bankcode);
				if(trim($bankcode) != ''){
					// die;
						$step = 1;
						$this->view->step = $step;
						$this->view->bankcode = $bankcode;
				}else{
						$step = 0;
						// die;
						$this->view->step = $step;
						$this->view->bankcode = $bankcode;
						$this->view->report_msg = array('error'=> 'Bank Name cannot be empty.');
				}
				

			}
			//--------------------------------second step-------------------------------------
			else if ($step == 1){

				$sessionNamespace 	= new Zend_Session_Namespace('tempdata');
				$tempdata 			= $sessionNamespace->tempdata;

				if ($tempdata['bankcode'] == '014' || $tempdata['bankcode'] == '046') {
					
					$filters = array(
				        'acctnumber' => array('StringTrim','StripTags'),
				        'acctname' => array('StringTrim','StripTags'),
				        'acctccy' => array('StringTrim','StripTags'),
				        'corpid' => array('StringTrim','StripTags')
					);

				
					
					$validators = array(
			            'acctnumber' => array(
			            				'NotEmpty',
			            				'Digits',
			            				new Zend_Validate_StringLength(array('min'=>10)),
										'messages' => array(
													    $this->language->_('Account Number can not be empty')
			                                          )
									),
						'acctname'  => array(
										'NotEmpty',
									//	new Zend_Validate_StringLength(array('max'=>30)),
										'messages' => array(
														$this->language->_('Account Name can not be empty')
													  )
									),
						'acctccy' => array(
			            				'NotEmpty',
										'messages' => array(
													    $this->language->_('Account Currency can not be empty'),
			                                          )
									),
						'corpid'  => array(
										'NotEmpty',
										new Zend_Validate_StringLength(array('max'=>10)),
										'messages' => array(
														$this->language->_('Corporate ID can not be empty')
													  )
									)
					);

					$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

					if($zf_filter_input->isValid())
					{

						$data = array(
							'account_number' 	=> $zf_filter_input->acctnumber,
							'account_name' 	 	=> $zf_filter_input->acctname,
							'account_currency' 	=> $zf_filter_input->acctccy,
							'corporate_id_bank' => $CustomerUser->sslencrypt($zf_filter_input->corpid),
						);

					}
				}else if ($tempdata['bankcode'] == '002') {
					
					$filters = array(
				        'acctnumber' => array('StringTrim','StripTags'),
				        'acctname' => array('StringTrim','StripTags'),
				        'acctccy' => array('StringTrim','StripTags')
					);

				
					
					$validators = array(
			            'acctnumber' => array(
			            				'NotEmpty',
			            				'Digits',
			            				new Zend_Validate_StringLength(array('max'=>15,'min'=>15)),
										'messages' => array(
													    $this->language->_('Account Number can not be empty')
			                                          )
									),
						'acctname'  => array(
										'NotEmpty',
										//new Zend_Validate_StringLength(array('max'=>30)),
										'messages' => array(
														$this->language->_('Account Name can not be empty')
													  )
									),
						'acctccy' => array(
			            				'NotEmpty',
										'messages' => array(
													    $this->language->_('Account Currency can not be empty'),
			                                          )
									)
					);

					$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

					if($zf_filter_input->isValid())
					{

						$data = array(
							'account_number' 	=> $zf_filter_input->acctnumber,
							'account_name' 	 	=> $zf_filter_input->acctname,
							'account_currency' 	=> $zf_filter_input->acctccy
						);
					}
				}
				else if($tempdata['bankcode'] == '008'){

					$filters = array(
				        'acctnumber' => array('StringTrim','StripTags'),
				        'acctname' => array('StringTrim','StripTags'),
					);

					$validators = array(
			            'acctnumber' => array(
			            				'NotEmpty',
			            				'Digits',
			            				new Zend_Validate_StringLength(array('max'=>13,'min'=>13)),
										'messages' => array(
													    $this->language->_('Account Number can not be empty')
			                                          )
									),
						'acctname'  => array(
										'NotEmpty',
										//new Zend_Validate_StringLength(array('max'=>30)),
										'messages' => array(
														$this->language->_('Account Name can not be empty'),
													  )
									)
					);

					$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

					if($zf_filter_input->isValid())
					{
						$data = array(
							'account_number' 	=> $zf_filter_input->acctnumber,
							'account_name' 	 	=> $zf_filter_input->acctname
						);
					}
				}
				else if($tempdata['bankcode'] == '022'){

					$filters = array(
				        'acctnumber' => array('StringTrim','StripTags'),
				        'acctname' => array('StringTrim','StripTags'),
					);

					$validators = array(
			            'acctnumber' => array(
			            				'NotEmpty',
			            				'Digits',
			            				new Zend_Validate_StringLength(array('max'=>12,'min'=>12)),
										'messages' => array(
													    $this->language->_('Account Number can not be empty')
			                                          )
									),
						'acctname'  => array(
										'NotEmpty',
										//new Zend_Validate_StringLength(array('max'=>30)),
										'messages' => array(
														$this->language->_('Account Name can not be empty'),
													  )
									)
					);

					$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

					if($zf_filter_input->isValid())
					{
						$data = array(
							'account_number' 	=> $zf_filter_input->acctnumber,
							'account_name' 	 	=> $zf_filter_input->acctname
						);
					}
				}
				else if($tempdata['bankcode'] == '009'){

					$filters = array(
				        'acctnumber' => array('StringTrim','StripTags'),
				        'acctname' => array('StringTrim','StripTags'),
				        'client' => array('StringTrim','StripTags'),
						'secretkey' => array('StringTrim','StripTags')
					);

					$validators = array(
			            'acctnumber' => array(
			            				'NotEmpty',
			            				'Digits',
			            				new Zend_Validate_StringLength(array('max'=>10,'min'=>10)),
										'messages' => array(
													    $this->language->_('Account Number can not be empty'),
			                                          )
									),
						'acctname'  => array(
										'NotEmpty',
										//new Zend_Validate_StringLength(array('max'=>30)),
										'messages' => array(
														$this->language->_('Account Name can not be empty'),
													  )
									),
						'client' => array(
			            				'NotEmpty',
										'messages' => array(
													    $this->language->_('Client ID can not be empty'),
			                                          )
									),
						'secretkey'  => array(
										'NotEmpty',
										'messages' => array(
														$this->language->_('Secret Key can not be empty')
													  )
									)
					);

					$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

					if($zf_filter_input->isValid())
					{
						$data = array(
							'account_number' 	=> $zf_filter_input->acctnumber,
							'account_name' 	 	=> $zf_filter_input->acctname,
							'auth_username' 	=> $CustomerUser->sslencrypt($zf_filter_input->client),
							'auth_password' 	=> $CustomerUser->sslencrypt($zf_filter_input->secretkey)
						);
					}
				}
				else if($tempdata['bankcode'] == '013' || $tempdata['bankcode'] == '032' || $tempdata['bankcode'] == '031' || $tempdata['bankcode'] == '153'){

					$filters = array(
				        'bankcode' => array('StringTrim','StripTags'),
				        'acctnumber' => array('StringTrim','StripTags'),
				        'acctname' => array('StringTrim','StripTags'),
				        'acctccy' => array('StringTrim','StripTags'),
				        'groupid' => array('StringTrim','StripTags')
					);

					$validators = array(
						'bankcode' => array(
										'NotEmpty',
										'messages' => array(
													    $this->language->_('Bank Code can not be empty'),
			                                          )
									),
			            'acctnumber' => array(
			            				'NotEmpty',
			            				'Digits',
			            				new Zend_Validate_StringLength(array('max'=>11,'min'=>9)),
										'messages' => array(
													    $this->language->_('Account Number can not be empty'),
			                                          )
									),
						'acctname'  => array(
										'NotEmpty',
										//new Zend_Validate_StringLength(array('max'=>30)),
										'messages' => array(
														$this->language->_('Account Name can not be empty'),
													  )
									),
						'acctccy' => array(
			            				'NotEmpty',
										'messages' => array(
													    $this->language->_('Account Currency can not be empty'),
			                                          )
									),
						'groupid'  => array(
										'NotEmpty',
										'messages' => array(
														$this->language->_('Group ID can not be empty')
													  )
									)
					);

					$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

					if($zf_filter_input->isValid())
					{

						$data = array(
							'account_number' 	=> $zf_filter_input->acctnumber,
							'account_name' 	 	=> $zf_filter_input->acctname,
							'account_currency' 	=> $zf_filter_input->acctccy,
							'corporate_id_bank' => $CustomerUser->sslencrypt($zf_filter_input->groupid)
						);
					}
				}

				//if not valid
				if (!$zf_filter_input->isValid()) {

					$this->view->error = true;
					foreach(array_keys($filters) as $field)
							$this->view->$field = ($zf_filter_input->isValid($field))? $zf_filter_input->getEscaped($field) : $this->_getParam($field);


					if($zf_filter_input->headquarter=='YES'){
							$this->view->yhead      = 'selected';	
						}else if($zf_filter_input->headquarter=='NO'){
							$this->view->nhead      = 'selected';	
						}else{
							$this->view->nn      = 'selected';	
						}
					$error = $zf_filter_input->getMessages();
					
					//format error utk ditampilkan di view html 
	                $errorArray = null;
			        foreach($error as $keyRoot => $rowError)
			        {
			           foreach($rowError as $errorString)
			           {
			              $errorArray[$keyRoot] = $errorString;
			           }
			        }
	        		// var_dump($errorArray);die;

			        $this->view->succes = false;
	                $this->view->report_msg = $errorArray;

	                $step = 1;
	                $this->view->step = $step;

				
				//if valid tampung ke sesi data yang sudah di validasi
				}else{

					$tempdata['data'] = $data;
					$sessionNamespace->tempdata = $tempdata;

					$step = 2;
					$this->view->step = $step;
				}

				$this->view->bankcode = $tempdata['bankcode'];
				$this->view->acctnumber = $zf_filter_input->acctnumber;
				$this->view->acctname = $zf_filter_input->acctname;
				$this->view->acctccy = $zf_filter_input->acctccy;
				$this->view->corpid = $zf_filter_input->corpid;
				$this->view->client = $zf_filter_input->client;
				$this->view->secretkey = $zf_filter_input->secretkey;
				$this->view->groupid = $zf_filter_input->groupid;
			}	

			//--------------------------third step--------------------------------
			else if($step == 2){

				$sessionNamespace 	= new Zend_Session_Namespace('tempdata');
				$tempdata 			= $sessionNamespace->tempdata;


				if ($tempdata['bankcode'] == '014') {
					
					$filters = array(
				        'clientid' => array('StringTrim','StripTags'),
				        'clientsecretkey' => array('StringTrim','StripTags'),
						'apikey' => array('StringTrim','StripTags'),
						'apisecret' => array('StringTrim','StripTags')
					);

					$validators = array(
			            'clientid' => array(
			            				'NotEmpty',
										'messages' => array(
													    $this->language->_('Client ID Can not be empty'),
			                                          )
									),
						'clientsecretkey'  => array(
										'NotEmpty',
										'messages' => array(
														$this->language->_('Client Secret Key Can not be empty')
													  )
									),
						'apikey' => array(
			            				'NotEmpty',
										'messages' => array(
													    $this->language->_('API Key Can not be empty'),
			                                          )
									),
						'apisecret'  => array(
										'NotEmpty',
										'messages' => array(
														$this->language->_('API Secret Key Can not be empty')
													  )
									)
					);

					$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

					if($zf_filter_input->isValid())
					{

						$data = array(
							'auth_username' 	=> $CustomerUser->sslencrypt($zf_filter_input->clientid),
							'auth_password' 	=> $CustomerUser->sslencrypt($zf_filter_input->clientsecretkey),
							'password' 			=> $CustomerUser->sslencrypt($zf_filter_input->apikey),
							'signature_key' 	=> $CustomerUser->sslencrypt($zf_filter_input->apisecret)
						);
					}
				}else if ($tempdata['bankcode'] == '002') {
					
					$filters = array(
				        // 'acctccy' => array('StringTrim','StripTags'),
				        'apisecret' => array('StringTrim','StripTags'),
						'clientid' => array('StringTrim','StripTags'),
						'corpid' => array('StringTrim','StripTags')
					);

					$validators = array(
			      //       'acctccy' => array(
			      //       				'NotEmpty',
									// 	'messages' => array(
									// 				    $this->language->_('Can not be empty'),
			      //                                     )
									// ),
						'corpid'  => array(
										'NotEmpty',
										//new Zend_Validate_StringLength(array('max'=>10)),
										'messages' => array(
														$this->language->_('Corporate ID Can not be empty')
													  )
									),
						'apisecret'  => array(
										'NotEmpty',
										'messages' => array(
														$this->language->_('API Secret Can not be empty')
													  )
									),
						'clientid' => array(
			            				'NotEmpty',
										'messages' => array(
													    $this->language->_('Client ID Can not be empty'),
			                                          )
									)
					);

					$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

					if($zf_filter_input->isValid())
					{

						$data = array(
							// 'acctccy' 	=> $zf_filter_input->acctccy,
							'corporate_id_bank' => $CustomerUser->sslencrypt($zf_filter_input->corpid),
							'client_id' 		=> $CustomerUser->sslencrypt($zf_filter_input->clientid),
							'signature_key' 	=> $CustomerUser->sslencrypt($zf_filter_input->apisecret)
						);
					}
				}else if ($tempdata['bankcode'] == '046') {
					
					$filters = array(
				        // 'acctccy' => array('StringTrim','StripTags'),
				        'stpid' => array('StringTrim','StripTags'),
						'icc' => array('StringTrim','StripTags'),
						'enq_id' => array('StringTrim','StripTags'),
						'skn' => array('StringTrim','StripTags'),
						'atmb' => array('StringTrim','StripTags'),
						'sva_id' => array('StringTrim','StripTags')
					);

					$validators = array(
			      //       'acctccy' => array(
			      //       				'NotEmpty',
									// 	'messages' => array(
									// 				    $this->language->_('Can not be empty'),
			      //                                     )
									// ),
						'stpid'  => array(
										'allowEmpty' => true,
										//new Zend_Validate_StringLength(array('max'=>10)),
										'messages' => array(
														
													  )
									),
						'icc'  => array(
										'allowEmpty' => true,
										//new Zend_Validate_StringLength(array('max'=>10)),
										'messages' => array(
														
													  )
									),
						'enq_id'  => array(
										'allowEmpty' => true,
										//new Zend_Validate_StringLength(array('max'=>10)),
										'messages' => array(
														
													  )
									),
						'skn'  => array(
										'allowEmpty' => true,
										//new Zend_Validate_StringLength(array('max'=>10)),
										'messages' => array(
														
													  )
									),
						'atmb'  => array(
										'allowEmpty' => true,
										//new Zend_Validate_StringLength(array('max'=>10)),
										'messages' => array(
														
													  )
									),
						'sva_id'  => array(
										'allowEmpty' => true,
										//new Zend_Validate_StringLength(array('max'=>10)),
										'messages' => array(
														
													  )
									)
					);

					$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

					if($zf_filter_input->isValid())
					{

						$data = array(
							// 'acctccy' 	=> $zf_filter_input->acctccy,
							'stpid' => $CustomerUser->sslencrypt($zf_filter_input->stpid),
							'icc' 		=> $CustomerUser->sslencrypt($zf_filter_input->icc),
							'enq_id' 	=> $CustomerUser->sslencrypt($zf_filter_input->enq_id),
							'skn' 	=> $CustomerUser->sslencrypt($zf_filter_input->skn),
							'atmb' 	=> $CustomerUser->sslencrypt($zf_filter_input->atmb),
							'sva_id' 	=> $CustomerUser->sslencrypt($zf_filter_input->sva_id)
						);
					}
				}
				else if ($tempdata['bankcode'] == '022') {
					
					$filters = array(
				        // 'acctccy' => array('StringTrim','StripTags'),
				        'apisecret' => array('StringTrim','StripTags'),
						'corpid' => array('StringTrim','StripTags')
					);

					$validators = array(
			      //       'acctccy' => array(
			      //       				'NotEmpty',
									// 	'messages' => array(
									// 				    $this->language->_('Can not be empty'),
			      //                                     )
									// ),
						'apisecret'  => array(
										'NotEmpty',
										'messages' => array(
														$this->language->_('API Secret Can not be empty')
													  )
									),
						'corpid' => array(
			            				'NotEmpty',
										'messages' => array(
													    $this->language->_('Client ID Can not be empty'),
			                                          )
									)
					);

					$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

					if($zf_filter_input->isValid())
					{

						$data = array(
							// 'acctccy' 	=> $zf_filter_input->acctccy,
							'corporate_id_bank' 		=> $CustomerUser->sslencrypt($zf_filter_input->corpid),
							'signature_key' 	=> $CustomerUser->sslencrypt($zf_filter_input->apisecret)
						);
					}
				}
				else if($tempdata['bankcode'] == '008'){

					$filters = array(
				        'authbasicuser' => array('StringTrim','StripTags'),
						'authbasicpass' => array('StringTrim','StripTags'),
						'clientid' => array('StringTrim','StripTags')
					);

					$validators = array(
			            'authbasicuser' => array(
			            				'NotEmpty',
										'messages' => array(
													    $this->language->_('Auth Basic User Can not be empty'),
			                                          )
									),
						'authbasicpass'  => array(
										'NotEmpty',
										'messages' => array(
														$this->language->_('Auth Basic Password Can not be empty')
													  )
									),
						'clientid' => array(
			            				'NotEmpty',
										'messages' => array(
													    $this->language->_('Client ID Can not be empty'),
			                                          )
									)
					);

					$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

					if($zf_filter_input->isValid())
					{
						$data = array(
							'auth_username' 	=> $CustomerUser->sslencrypt($zf_filter_input->authbasicuser),
							'auth_password' 	=> $CustomerUser->sslencrypt($zf_filter_input->authbasicpass),
							'client_id' 		=> $CustomerUser->sslencrypt($zf_filter_input->clientid)
						);
					}
				}
				else if($tempdata['bankcode'] == '009'){

					$filters = array(
				        'clientid' => array('StringTrim','StripTags')
						//'certificate' => array('StringTrim','StripTags')
					);

					$validators = array(
			            'clientid' => array(
			            				'NotEmpty',
										'messages' => array(
													    $this->language->_('Client ID Can not be empty'),
			                                          )
									)
						/*'certificate' => array(
			            				'NotEmpty',
										'messages' => array(
													    $this->language->_('Certificate Can not be empty'),
			                                          )
									) */
					);

					$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

					if($zf_filter_input->isValid())
					{
						/*
						//read file
						$adapter = new Zend_File_Transfer_Adapter_Http();

		                $adapter->setDestination ( $this->_destinationUploadDir );
		                $extensionValidator = new Zend_Validate_File_Extension(array(false, 'asc'));
		                $extensionValidator->setMessage(
		                    $this->language->_('Error').': '.$this->language->_('Extension file must be').' *.asc'
		                );

		                $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
		                $sizeValidator->setMessage(
		                    'Error: File exceeds maximum size'
		                );

		                $adapter->setValidators ( array (
		                    $extensionValidator,
		                    $sizeValidator,
		                ));

		                if ($adapter->isValid ())
                		{
                			// print_r($adapter);die;
                			$sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
		                    $newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

		                    $adapter->addFilter ('Rename',$newFileName);


	  						if ($adapter->receive ())
	                    	{
	                    		$file_contents = file_get_contents($newFileName);

	                    		//$data = explode("\n",$file_contents);
	                    		//var_dump($file_contents);die;
	                    		$certificate = $file_contents;
	                    		/* foreach ($data as $key => $value) {
	                    				
	                    			//start from row 3
	                    			//if ($key >= 3) {
										//var_dump(trim($value));
	                    				if (!empty($value) ) {
												if(trim($value) == '-----END PRIVATE KEY-----'){$value = '';}
												if(trim($value) == '-----BEGINPRIVATEKEY-----'){$value = '';}
	                    					$certificate .= $value;
	                    				}	                    				
	                    			//}
	                    		} 
			                }
			                else
			                {
			                    $this->view->error = true;
			                    foreach($adapter->getMessages() as $key=>$val)
			                    {
			                        if($key=='fileUploadErrorNoFile')
			                            $error_msg[0] = $this->language->_('Error').': '.$this->language->_('File cannot be left blank. Please correct it').'.';
			                        else
			                            $error_msg[0] = $val;
			                        break;
			                    }
			                    $errors = $this->displayError($error_msg);
			                    $this->view->report_msg = $errors;
		                	}
		            	}else{
		            			$this->view->error = true;
  			 					$errors = array($adapter->getMessages());
  			 					// var_dump($errors);die;
   								$this->view->errorMsg = $errors;
		            	}
							*/
						$data = array(
							'client_id' 		=> $CustomerUser->sslencrypt($zf_filter_input->clientid)
						//	'signature_key' 	=> $certificate
						);
						
						//var_dump($data);die;
					}
				}
				else if($tempdata['bankcode'] == '013' || $tempdata['bankcode'] == '032' || $tempdata['bankcode'] == '031' || $tempdata['bankcode'] == '153'){

					$filters = array(
				       'clientid' => array('StringTrim','StripTags'),
				       'clientsecretkey' => array('StringTrim','StripTags'),
						'apikey' => array('StringTrim','StripTags'),
						'apiclientstatickey' => array('StringTrim','StripTags')
					);

					$validators = array(
						'clientid' => array(
			            				'NotEmpty',
										'messages' => array(
													    $this->language->_('Client ID Can not be empty'),
			                                          )
									),
						'clientsecretkey'  => array(
										'NotEmpty',
										'messages' => array(
														$this->language->_('Client Secret Key Can not be empty')
													  )
									),
						'apikey' => array(
			            				'NotEmpty',
										'messages' => array(
													    $this->language->_('API Key Can not be empty'),
			                                          )
									),
						'apiclientstatickey'  => array(
										'NotEmpty',
										'messages' => array(
														$this->language->_('API Client Static Key Can not be empty')
													  )
									)
					);

					$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

					if($zf_filter_input->isValid())
					{

						$data = array(
							'auth_username' 	=> $CustomerUser->sslencrypt($zf_filter_input->clientid),
							'auth_password' 	=> $CustomerUser->sslencrypt($zf_filter_input->clientsecretkey),
							'password' 			=> $CustomerUser->sslencrypt($zf_filter_input->apikey),
							'signature_key' 	=> $CustomerUser->sslencrypt($zf_filter_input->apiclientstatickey)
						);
					}
				}

				//if not valid
				if (!$zf_filter_input->isValid()) {

					$this->view->error = true;
					foreach(array_keys($filters) as $field)
							$this->view->$field = ($zf_filter_input->isValid($field))? $zf_filter_input->getEscaped($field) : $this->_getParam($field);


					if($zf_filter_input->headquarter=='YES'){
							$this->view->yhead      = 'selected';	
						}else if($zf_filter_input->headquarter=='NO'){
							$this->view->nhead      = 'selected';	
						}else{
							$this->view->nn      = 'selected';	
						}
					$error = $zf_filter_input->getMessages();
					
					//format error utk ditampilkan di view html 
	                $errorArray = null;
			        foreach($error as $keyRoot => $rowError)
			        {
			           foreach($rowError as $errorString)
			           {
			              $errorArray[$keyRoot] = $errorString;
			           }
			        }
					//var_dump($error);die;
			        $this->view->succes = false;
	                $this->view->report_msg = $errorArray;

	                $step = 2;
	                $this->view->step = $step;
				
				//if valid tampung ke sesi data yang sudah di validasi
				}else{

					$tempdata['data'] = array_merge($tempdata['data'], $data);



					// -------- insert to db --------------
		       		try 
					{
						$field = array_keys($tempdata['data']);
						$value = array_values($tempdata['data']);
						// echo "<pre>";
						// var_dump($field);
						// var_dump($value);
						//get insert id
						$selectapikey = $this->_db->select()
						 				->from(array('A' => 'M_APIKEY'))
						 				->order('A.APIKEY_ID DESC')
						 				->limit(1);
			
						$apikey = $this->_db->fetchAll($selectapikey);

						if (!empty($apikey)) {
							
							$id = $apikey[0]['APIKEY_ID'] + 1;
						}else{

							$id = 1;
						}

						// if ($this->_isAdmin) {

							// $this->_db->beginTransaction();
							$request = array();
							foreach ($field as $key => $val) {
								$request[$val] = $value[$key]; 
							}

							//call api
							$clientUser  =  new SGO_Soap_ClientUser();
							
							$auth = Zend_Auth::getInstance()->getIdentity();
							$request['corporate_id_channel'] = $this->_custIdLogin;
							$request['corporate_name'] = $this->_custIdLogin;
							$request['bankcode'] = $tempdata['bankcode'];

							 //echo '<pre>';
							 //print_r($request);die;
							
							$success = $clientUser->callapi('registerapi',$request,'b2b/account/register');

							//insert T_DIGI_LOG
							$paramlog = array(
				                    'DIGI_USER' => $this->_userIdLogin,
				                    'DIGI_CUST'	=> $this->_custIdLogin,
				                    'DIGI_BANK' => $request['BANK_CODE'],
				                    'DIGI_ACCOUNT' => $request['account_number'],
				                    'DIGI_ERROR_CODE' => $result->error_code,
				                    'DIGI_TIMESTAMP' => new Zend_Db_Expr("now()"),
				                    'DIGI_SERVICE'	=> '0'
					        );

					        $this->_helper->ServiceLog->serviceLog($paramlog);

					        $resultregist  = $clientUser->getResult();
							// print_r($resultregist);
							// var_dump($resultregist);die;
							
							if($clientUser->isTimedOut()){
								$returnStruct = array(
										'ResponseCode'	=>'XT',
										'ResponseDesc'	=>'Service Timeout',					
										'Cif'			=>'',
										'AccountList'	=> '',
								);
							}else{
								if($resultregist->error_code == '0000'){

									// $request['BANK_CODE'] = $bankcode;
									
									$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

									$request['BANK_CODE'] = $request['bankcode'];
									$request['SENDER_ID'] = $CustomerUser->sslencrypt($resultregist->eoa_credentials->sender_id);
									$request['AUTH_USER'] = $CustomerUser->sslencrypt($resultregist->eoa_credentials->auth_username);
									$request['AUTH_PASS'] = $CustomerUser->sslencrypt($resultregist->eoa_credentials->auth_password);
									$request['EOA_SENDER'] = $CustomerUser->sslencrypt($resultregist->eoa_credentials->sender_id);

									$request['SIGNATURE_KEY'] = $CustomerUser->sslencrypt($resultregist->eoa_credentials->signature_key);

									foreach ($banklist as $key => $value) {
										if($value['BANK_CODE']==$request['bankcode']){
											$bank_name = $value['BANK_NAME'];
										}
									}

														// echo "<pre>";
									// print_r($request);

									$success 		= $clientUser->callapi('balance',$request,'b2b/inquiry/balance');
    								$result  = $clientUser->getResult();
			
    								//insert T_DIGI_LOG
    								$paramlog = array(
							            'DIGI_USER' => $this->_userIdLogin,
							            'DIGI_CUST' => $this->_custIdLogin,
							            'DIGI_BANK' => $request['BANK_CODE'],
							            'DIGI_ACCOUNT' => $request['account_number'],
							            'DIGI_ERROR_CODE' => $result->error_code,
							            'DIGI_TIMESTAMP' => new Zend_Db_Expr("now()"),
							            'DIGI_SERVICE'  => 1
							        );

							        $this->_helper->ServiceLog->serviceLog($paramlog);

							        // print_r($result);

								if($result->error_code == '0000' || $result->error_code == '0100'){
									try{
										 //echo "<pre>";
										 //var_dump($tempdata);die;
										$field = array_keys($tempdata['data']);
										$value = array_values($tempdata['data']);
										
										
										$fieldArr = array('account_currency','account_name','account_number','account_alias');
										for($i=0; $i<count($tempdata['data']); $i++){
										
										
											
										
											//if(!in_array($field[$i], $fieldArr)){
											//		$value[$i] 	  = $CustomerUser->sslencrypt($value[$i]);
											//}
											$content = array(
												'APIKEY_ID' 	 => $id,
												'BANK_CODE' 	 => $tempdata['bankcode'],
												'CUST_ID' 		 => $this->_custIdLogin,
												'FIELD' 	 	 => $field[$i],
												'VALUE' 	 	 => $value[$i]
									       	);
									       	// if($field[$i] == 'account_name'){
									       	// 	$account_name = $value[$i];
									       	// }

											$this->_db->insert('M_APIKEY',$content);
										}




									$inputdata = array(
													'bank_name' => $bank_name,
													'bank_code' =>$request['bankcode'],
													'account_number' =>$request['account_number'],
													'account_name' =>$result->account_name,
													'account_currency' =>$result->account_currency,

													'account_balance' =>(int)$result->account_balance,
													're_datetime' =>date('Y-m-d h:i:s'),
													'account_status' =>'1',
													'user' =>$this->_userIdLogin,
													'cust' =>$this->_custIdLogin
											);

										if(!empty($datacache)){
											$newdatacache['lastupdate'] = $datacache['lastupdate'];
											$newdatacache['totalmanual'] = $datacache['totalmanual'];
											$newdatacache['totalonline'] = (int)$datacache['totalonline']+(int)$result->account_balance;
											$datapersnew = $datacache['datapers'];
											$datapersnew[$total] = $inputdata;
											$newdatacache['datapers'] = $datapersnew;
											$cache->save($newdatacache,$cacheID);	
										}


										// var_dump($result);
										// var_dump($resultregist);
										// die;
										
										$insertkey['ID'] = $id;
										$insertkey['SENDER_ID']  = $CustomerUser->sslencrypt($resultregist->eoa_credentials->sender_id);
										$insertkey['AUTH_USER']  = $CustomerUser->sslencrypt($resultregist->eoa_credentials->auth_username);
										$insertkey['AUTH_PASS']  = $CustomerUser->sslencrypt($resultregist->eoa_credentials->auth_password);
										$insertkey['SIGNATURE_KEY']  = $CustomerUser->sslencrypt($resultregist->eoa_credentials->signature_key);
										$insertkey['ALLOW_IP']  = $resultregist->eoa_credentials->allowed_ip;
										$insertkey['CREATED']  = new Zend_Db_Expr("now()");
										$insertkey['CREATEDBY']  = $this->_userIdLogin;
										$this->_db->insert('M_APICREDENTIAL',$insertkey);

										$result = $this->_db->fetchRow(
											$this->_db->select()
												->from(array('C' => 'M_USER'))
												->where("USER_ID = ".$this->_db->quote($this->_userIdLogin))
										);

										if($result['USER_HOME_CONTENT'] == ''){
											$updateArr = array(
															'USER_HOME_CONTENT' => '1'
														);

										}else{

											$strdata = $result['USER_HOME_CONTENT'].',1';

											$updateArr = array(
															'USER_HOME_CONTENT' => $strdata
														);
										}
										

		  	 							$updateWhere['USER_ID = ?'] = (string)$this->_userIdLogin;
										$updateWhere['CUST_ID = ?'] = (string)$this->_custIdLogin;
		  	 							$this->_db->update('M_USER',$updateArr,$updateWhere);


		  	 							//clear cache if account added

		  	 							
		  	 							$this->view->success = true;
										$this->view->report_msg = array();
										Application_Helper_General::writeLog('ADAC','Account has been Added, '.$request['account_number'].', Bank Name : '.$bank_name);
										$this->setbackURL('/'.$this->_request->getModuleName());
										$this->_redirect('/notification/success');

									}catch(Exception $e){
										 //echo '<pre>';  
										 //var_dump($e);die;
										$this->view->report_msg = array('error'=>'Invalid Data');
									}

								}else{


									$success 		= $clientUser->callapi('accountdelete',$request,'b2b/account/delete');
    								$result  = $clientUser->getResult();
									// die('here');
									$this->view->report_msg_service = array('error'=>'Service Error');
									$this->view->service_error = 'Register failed. Please try again later';
								}



								}else{

									if($resultregist->error_code == '12'){
										$this->view->report_msg = array('error'=>'account number has been registered in system');
										$this->view->service_error = $resultregist->error_message;	
										
									}else{
										$this->view->report_msg = array('error'=>'Service Error');
										$this->view->service_error = $resultregist->error_message;	
									}
									// var_dump($result);die('gfe');
									
								}
							}
							
						// }
						// else{

						// 	$acctnumber = $tempdata['data']['account_number'];
						// 	$acctname = $tempdata['data']['account_name'];

						// 	$info = 'Suggested By = '.$this->_userIdLogin.', Account Number = '.$acctnumber.', Account Name = '.$acctname;

						// 	try{
						// 		$change_id = $this->suggestionWaitingApproval('Add Account',$info,$this->_changeType['code']['new'],null,'M_APIKEY','TEMP_APIKEY',$acctnumber,$acctname,$this->_custIdLogin);

						// 		for($i=0; $i<count($tempdata['data']); $i++){

						// 			$content = array(
						// 				'CHANGES_ID' 	 => $change_id,
						// 				'BANK_CODE' 	 => $tempdata['bankcode'],
						// 				'CUST_ID' 		 => $this->_custIdLogin,
						// 				'FIELD' 	 	 => $field[$i],
						// 				'VALUE' 	 	 => $value[$i]
						// 	       	);

						// 			$this->_db->insert('TEMP_APIKEY',$content);
						// 		}

								 

						// 		$this->view->success = true;
						// 		$this->view->report_msg = array();

						// 		$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
						// 		$this->_redirect('/notification/success');

						// 	}catch(Exception $e){
						// 		$this->view->report_msg = array('error'=>true);
						// 	}
						// }

					}
					catch(Exception $e) 
					{
					//	var_dump($e);die;
						//rollback changes
						$this->_db->rollBack();
						
						foreach(array_keys($filters) as $field)
							$this->view->$field = $zf_filter_input->getEscaped($field);

						$errorMsg = 'exeption';
						$this->_helper->getHelper('FlashMessenger')->addMessage('F');
						$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
					}
				}

				$this->view->bankcode = $tempdata['bankcode'];
				$this->view->clientid = $zf_filter_input->clientid;
				$this->view->clientsecretkey = $zf_filter_input->clientsecretkey;
				$this->view->apikey = $zf_filter_input->apikey;
				$this->view->apisecret = $zf_filter_input->apisecret;
				$this->view->signature_key = $zf_filter_input->signature_key;
				$this->view->client_id = $zf_filter_input->client_id;
				$this->view->authbasicuser = $zf_filter_input->authbasicuser;
				$this->view->authbasicpass = $zf_filter_input->authbasicpass;
				$this->view->certificate = $zf_filter_input->certificate;
				$this->view->apiclientstatickey = $zf_filter_input->apiclientstatickey;
			}

			Application_Helper_General::writeLog('ADAC','View Register Account API');
		}
		


		// 	if (!empty($bankcode)){
				
		// 		//if BCA
		// 		if ($bankcode == '014') {
					
		// 			$filters = array(
		// 		        'bankcode' => array('StringTrim','StripTags'),
		// 		        'acctnumber' => array('StringTrim','StripTags'),
		// 		        'acctname' => array('StringTrim','StripTags'),
		// 		        'acctccy' => array('StringTrim','StripTags'),
		// 		        'corpid' => array('StringTrim','StripTags'),
		// 		        'clientid' => array('StringTrim','StripTags'),
		// 		        'clientsecretkey' => array('StringTrim','StripTags'),
		// 				'apikey' => array('StringTrim','StripTags'),
		// 				'apisecret' => array('StringTrim','StripTags')
		// 			);

		// 			$validators = array(
		// 				'bankcode' => array(
		// 								'NotEmpty',
		// 								'messages' => array(
		// 											    $this->language->_('Can not be empty'),
		// 	                                          )
		// 							),
		// 	            'acctnumber' => array(
		// 	            				'NotEmpty',
		// 								'messages' => array(
		// 											    $this->language->_('Can not be empty'),
		// 	                                          )
		// 							),
		// 				'acctname'  => array(
		// 								'NotEmpty',
		// 								'messages' => array(
		// 												$this->language->_('Can not be empty')
		// 											  )
		// 							),
		// 				'acctccy' => array(
		// 	            				'NotEmpty',
		// 								'messages' => array(
		// 											    $this->language->_('Can not be empty'),
		// 	                                          )
		// 							),
		// 				'corpid'  => array(
		// 								'NotEmpty',
		// 								'messages' => array(
		// 												$this->language->_('Can not be empty')
		// 											  )
		// 							),
		// 				'clientid' => array(
		// 	            				'NotEmpty',
		// 								'messages' => array(
		// 											    $this->language->_('Can not be empty'),
		// 	                                          )
		// 							),
		// 				'clientsecretkey'  => array(
		// 								'NotEmpty',
		// 								'messages' => array(
		// 												$this->language->_('Can not be empty')
		// 											  )
		// 							),
		// 				'apikey' => array(
		// 	            				'NotEmpty',
		// 								'messages' => array(
		// 											    $this->language->_('Can not be empty'),
		// 	                                          )
		// 							),
		// 				'apisecret'  => array(
		// 								'NotEmpty',
		// 								'messages' => array(
		// 												$this->language->_('Can not be empty')
		// 											  )
		// 							)
		// 			);

		// 			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

		// 			if($zf_filter_input->isValid())
		// 			{

		// 				$data = array(
		// 					'account_number' 	=> $zf_filter_input->acctnumber,
		// 					'account_name' 	 	=> $zf_filter_input->acctname,
		// 					'account_currency' 	=> $zf_filter_input->acctccy,
		// 					'corporate_id_bank' => $zf_filter_input->corpid,
		// 					'auth_username' 	=> $zf_filter_input->clientid,
		// 					'auth_password' 	=> $zf_filter_input->clientsecretkey,
		// 					'password' 			=> $zf_filter_input->apikey,
		// 					'signature_key' 	=> $zf_filter_input->apisecret
		// 				);
		// 			}
		// 		}
		// 		//if Mandiri
		// 		else if ($bankcode == '008') {

		// 			$filters = array(
		// 		        'bankcode' => array('StringTrim','StripTags'),
		// 		        'acctnumber' => array('StringTrim','StripTags'),
		// 		        'acctname' => array('StringTrim','StripTags'),
		// 		        'authbasicuser' => array('StringTrim','StripTags'),
		// 				'authbasicpass' => array('StringTrim','StripTags'),
		// 				'clientid' => array('StringTrim','StripTags')
		// 			);

		// 			$validators = array(
		// 				'bankcode' => array(
		// 								'NotEmpty',
		// 								'messages' => array(
		// 											    $this->language->_('Can not be empty'),
		// 	                                          )
		// 							),
		// 	            'acctnumber' => array(
		// 	            				'NotEmpty',
		// 								'messages' => array(
		// 											    $this->language->_('Can not be empty'),
		// 	                                          )
		// 							),
		// 				'acctname'  => array(
		// 								'NotEmpty',
		// 								'messages' => array(
		// 												$this->language->_('Can not be empty')
		// 											  )
		// 							),
		// 				'authbasicuser' => array(
		// 	            				'NotEmpty',
		// 								'messages' => array(
		// 											    $this->language->_('Can not be empty'),
		// 	                                          )
		// 							),
		// 				'authbasicpass'  => array(
		// 								'NotEmpty',
		// 								'messages' => array(
		// 												$this->language->_('Can not be empty')
		// 											  )
		// 							),
		// 				'clientid' => array(
		// 	            				'NotEmpty',
		// 								'messages' => array(
		// 											    $this->language->_('Can not be empty'),
		// 	                                          )
		// 							)
		// 			);

		// 			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

		// 			if($zf_filter_input->isValid())
		// 			{
		// 				$data = array(
		// 					'account_number' 	=> $zf_filter_input->acctnumber,
		// 					'account_name' 	 	=> $zf_filter_input->acctname,
		// 					'auth_username' 	=> $zf_filter_input->authbasicuser,
		// 					'auth_password' 	=> $zf_filter_input->authbasicpass,
		// 					'client_id' 		=> $zf_filter_input->clientid
		// 				);
		// 			}
		// 		}
		// 		//if BNI
		// 		else if ($bankcode == '009') {

		// 			$filters = array(
		// 		        'bankcode' => array('StringTrim','StripTags'),
		// 		        'acctnumber' => array('StringTrim','StripTags'),
		// 		        'acctname' => array('StringTrim','StripTags'),
		// 		        'client' => array('StringTrim','StripTags'),
		// 				'secretkey' => array('StringTrim','StripTags'),
		// 				'clientid' => array('StringTrim','StripTags'),
		// 				'certificate' => array('StringTrim','StripTags')
		// 			);

		// 			$validators = array(
		// 				'bankcode' => array(
		// 								'NotEmpty',
		// 								'messages' => array(
		// 											    $this->language->_('Can not be empty'),
		// 	                                          )
		// 							),
		// 	            'acctnumber' => array(
		// 	            				'NotEmpty',
		// 								'messages' => array(
		// 											    $this->language->_('Can not be empty'),
		// 	                                          )
		// 							),
		// 				'acctname'  => array(
		// 								'NotEmpty',
		// 								'messages' => array(
		// 												$this->language->_('Can not be empty')
		// 											  )
		// 							),
		// 				'client' => array(
		// 	            				'NotEmpty',
		// 								'messages' => array(
		// 											    $this->language->_('Can not be empty'),
		// 	                                          )
		// 							),
		// 				'secretkey'  => array(
		// 								'NotEmpty',
		// 								'messages' => array(
		// 												$this->language->_('Can not be empty')
		// 											  )
		// 							),
		// 				'clientid' => array(
		// 	            				'NotEmpty',
		// 								'messages' => array(
		// 											    $this->language->_('Can not be empty'),
		// 	                                          )
		// 							),
		// 				'certificate' => array(
		// 	            				'NotEmpty',
		// 								'messages' => array(
		// 											    $this->language->_('Can not be empty'),
		// 	                                          )
		// 							)
		// 			);

		// 			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

		// 			if($zf_filter_input->isValid())
		// 			{

		// 				//read file
		// 				$adapter = new Zend_File_Transfer_Adapter_Http();

		//                 $adapter->setDestination ( $this->_destinationUploadDir );
		//                 $extensionValidator = new Zend_Validate_File_Extension(array(false, 'asc'));
		//                 $extensionValidator->setMessage(
		//                     $this->language->_('Error').': '.$this->language->_('Extension file must be').' *.asc'
		//                 );

		//                 $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
		//                 $sizeValidator->setMessage(
		//                     'Error: File exceeds maximum size'
		//                 );

		//                 $adapter->setValidators ( array (
		//                     $extensionValidator,
		//                     $sizeValidator,
		//                 ));

		//                 if ($adapter->isValid ())
  //               		{

  //               			$sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
		//                     $newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

		//                     $adapter->addFilter ('Rename',$newFileName);


	 //  						if ($adapter->receive ())
	 //                    	{
	 //                    		$file_contents = file_get_contents($newFileName);

	 //                    		$data = explode("\n",$file_contents);

	 //                    		$certificate = '';
	 //                    		foreach ($data as $key => $value) {
	                    			
	 //                    			//start from row 3
	 //                    			if ($key >= 3) {

	 //                    				if (!empty($value) && $value !== '-----END PGP PRIVATE KEY BLOCK-----') {

	 //                    					$certificate .= $value;
	 //                    				}	                    				
	 //                    			}
	 //                    		}
		// 	                }
		// 	                else
		// 	                {
		// 	                    $this->view->error = true;
		// 	                    foreach($adapter->getMessages() as $key=>$val)
		// 	                    {
		// 	                        if($key=='fileUploadErrorNoFile')
		// 	                            $error_msg[0] = $this->language->_('Error').': '.$this->language->_('File cannot be left blank. Please correct it').'.';
		// 	                        else
		// 	                            $error_msg[0] = $val;
		// 	                        break;
		// 	                    }
		// 	                    $errors = $this->displayError($error_msg);
		// 	                    $this->view->report_msg = $errors;
		//                 	}
		//             	}

		// 				$data = array(
		// 					'account_number' 	=> $zf_filter_input->acctnumber,
		// 					'account_name' 	 	=> $zf_filter_input->acctname,
		// 					'auth_username' 	=> $zf_filter_input->client,
		// 					'auth_password' 	=> $zf_filter_input->secretkey,
		// 					'client_id' 		=> $zf_filter_input->clientid,
		// 					'signature_key' 	=> $certificate
		// 				);
		// 			}
		// 		}
		// 		//if Permata
		// 		else if ($bankcode == '013') {

		// 			$filters = array(
		// 		        'bankcode' => array('StringTrim','StripTags'),
		// 		        'acctnumber' => array('StringTrim','StripTags'),
		// 		        'acctname' => array('StringTrim','StripTags'),
		// 		        'acctccy' => array('StringTrim','StripTags'),
		// 		        'groupid' => array('StringTrim','StripTags'),
		// 		        'clientid' => array('StringTrim','StripTags'),
		// 		        'clientsecretkey' => array('StringTrim','StripTags'),
		// 				'apikey' => array('StringTrim','StripTags'),
		// 				'apiclientstatickey' => array('StringTrim','StripTags')
		// 			);

		// 			$validators = array(
		// 				'bankcode' => array(
		// 								'NotEmpty',
		// 								'messages' => array(
		// 											    $this->language->_('Can not be empty'),
		// 	                                          )
		// 							),
		// 	            'acctnumber' => array(
		// 	            				'NotEmpty',
		// 								'messages' => array(
		// 											    $this->language->_('Can not be empty'),
		// 	                                          )
		// 							),
		// 				'acctname'  => array(
		// 								'NotEmpty',
		// 								'messages' => array(
		// 												$this->language->_('Can not be empty')
		// 											  )
		// 							),
		// 				'acctccy' => array(
		// 	            				'NotEmpty',
		// 								'messages' => array(
		// 											    $this->language->_('Can not be empty'),
		// 	                                          )
		// 							),
		// 				'groupid'  => array(
		// 								'NotEmpty',
		// 								'messages' => array(
		// 												$this->language->_('Can not be empty')
		// 											  )
		// 							),
		// 				'clientid' => array(
		// 	            				'NotEmpty',
		// 								'messages' => array(
		// 											    $this->language->_('Can not be empty'),
		// 	                                          )
		// 							),
		// 				'clientsecretkey'  => array(
		// 								'NotEmpty',
		// 								'messages' => array(
		// 												$this->language->_('Can not be empty')
		// 											  )
		// 							),
		// 				'apikey' => array(
		// 	            				'NotEmpty',
		// 								'messages' => array(
		// 											    $this->language->_('Can not be empty'),
		// 	                                          )
		// 							),
		// 				'apiclientstatickey'  => array(
		// 								'NotEmpty',
		// 								'messages' => array(
		// 												$this->language->_('Can not be empty')
		// 											  )
		// 							)
		// 			);

		// 			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

		// 			if($zf_filter_input->isValid())
		// 			{

		// 				$data = array(
		// 					'account_number' 	=> $zf_filter_input->acctnumber,
		// 					'account_name' 	 	=> $zf_filter_input->acctname,
		// 					'account_currency' 	=> $zf_filter_input->acctccy,
		// 					'corporate_id_bank' => $zf_filter_input->groupid,
		// 					'auth_username' 	=> $zf_filter_input->clientid,
		// 					'auth_password' 	=> $zf_filter_input->clientsecretkey,
		// 					'password' 			=> $zf_filter_input->apikey,
		// 					'signature_key' 	=> $zf_filter_input->apiclientstatickey
		// 				);
		// 			}
		// 		}
		// 	}
		
		// 	if($zf_filter_input->isValid())
		// 	{

	 //       		//-------- insert --------------
	 //       		try 
		// 		{
		// 			$field = array_keys($data);
		// 			$value = array_values($data);

		// 			//get insert id
		// 			$selectapikey = $this->_db->select()
		// 			 				->from(array('A' => 'M_APIKEY'))
		// 			 				->order('A.APIKEY_ID DESC')
		// 			 				->limit(1);
		
		// 			$apikey = $this->_db->fetchAll($selectapikey);

		// 			if (!empty($apikey)) {
						
		// 				$id = $apikey[0]['APIKEY_ID'] + 1;
		// 			}else{

		// 				$id = 1;
		// 			}

					
		// 			// $this->_db->beginTransaction();

					


		// 			$param = array();

		// 			//call api

		// 			$clientUser  =  new SGO_Soap_ClientUser();


		// 			// $acctbalance = array(0=>'');
		// 			$request = $this->getRequest()->getParams();
		// 			// $datapers = array();
		// 			// foreach ($acctbalance as $key => $value) {
		// 				# code...
		// 			if($request['bankcode'] == '009'){
		// 				$request['certificate'] = $certificate;
		// 				$request['corpid'] = $request['client'];	
		// 			}
					

		// 			// $request = array();
		// 			$success = $clientUser->callapi('registerapi',$request,'http://192.168.86.26:20809/b2b/account/register');
					
		// 			if($clientUser->isTimedOut()){
		// 				$returnStruct = array(
		// 						'ResponseCode'	=>'XT',
		// 						'ResponseDesc'	=>'Service Timeout',					
		// 						'Cif'			=>'',
		// 						'AccountList'	=> '',
		// 				);
		// 			}else{
		// 				$result  = $clientUser->getResult();
		// 				// print_r($result);die;
		// 				if($result->error_code == '0000'){
		// 					try{
		// 					for($i=0; $i<count($data); $i++){

		// 						$content = array(
		// 							'APIKEY_ID' 	 => $id,
		// 							'BANK_CODE' 	 => $zf_filter_input->bankcode,
		// 							'CUST_ID' 		 => $this->_custIdLogin,
		// 							'FIELD' 	 	 => $field[$i],
		// 							'VALUE' 	 	 => $value[$i]
		// 				       	);

		// 						$this->_db->insert('M_APIKEY',$content);
		// 					}

		// 					$insertkey['ID'] = $id;
		// 					$insertkey['SENDER_ID']  = $result->eoa_credentials->sender_id;
		// 					$insertkey['AUTH_USER']  = $result->eoa_credentials->auth_username;
		// 					$insertkey['AUTH_PASS']  = $result->eoa_credentials->auth_password;
		// 					$insertkey['SIGNATURE_KEY']  = $result->eoa_credentials->signature_key;
		// 					$insertkey['ALLOW_IP']  = $result->eoa_credentials->allowed_ip;
							
		// 					$this->_db->insert('M_APICREDENTIAL',$insertkey);
		// 					}catch(Exception $e){

		// 						print_r($e);die();
		// 					}

		// 				}else{
		// 					print_r($result);die;
		// 				}


		// 				// print_r($result);die;
		// 			}
		// 			// }

		// 			// if ($param['ResponseCode'] == '0000') {
						
		// 			// 	try{

		// 			// 		//insert to db if success

							
		// 			// 		// $this->_db->insert('',$param);

		// 			// 	}
		// 			// 	catch(Exception $e){

		// 			// 		echo $e;die();
		// 			// 	}


		// 			// }else{

		// 			// }


					

		// 			// $this->_db->commit();
					
		// 			$this->view->success = true;
		// 			$this->view->report_msg = array();

		// 			$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
		// 			$this->_redirect('/notification/success');

					

		// 		}
		// 		catch(Exception $e) 
		// 		{
		// 			//rollback changes
		// 			$this->_db->rollBack();
					
		// 			foreach(array_keys($filters) as $field)
		// 				$this->view->$field = $zf_filter_input->getEscaped($field);

		// 			$errorMsg = 'exeption';
		// 			$this->_helper->getHelper('FlashMessenger')->addMessage('F');
		// 			$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
		// 		}
		// 	}
		// 	else
		// 	{
		// 		$this->view->error = true;
		// 		foreach(array_keys($filters) as $field)
		// 				$this->view->$field = ($zf_filter_input->isValid($field))? $zf_filter_input->getEscaped($field) : $this->_getParam($field);


		// 		if($zf_filter_input->headquarter=='YES'){
		// 				$this->view->yhead      = 'selected';	
		// 			}else if($zf_filter_input->headquarter=='NO'){
		// 				$this->view->nhead      = 'selected';	
		// 			}else{
		// 				$this->view->nn      = 'selected';	
		// 			}
		// 		$error = $zf_filter_input->getMessages();
				
		// 		//format error utk ditampilkan di view html 
  //               $errorArray = null;
		//         foreach($error as $keyRoot => $rowError)
		//         {
		//            foreach($rowError as $errorString)
		//            {
		//               $errorArray[$keyRoot] = $errorString;
		//            }
		//         }
        
		//         $this->view->succes = false;
  //               $this->view->report_msg = $errorArray;
		// 	}

		

		// Application_Helper_General::writeLog('COAD','Add New Branch Bank');

	}

	public function banksuggestionAction()
	{
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $bankarr = array(
        	'008' => array(
        		'name' 		=> 'Bank Mandiri',
        		'imgsource' => '/assets/themes/assets/newlayout/img/mandiri.png' 
        	),
        	'009' => array(
        		'name' 		=> 'Bank BNI',
        		'imgsource' => '/assets/themes/assets/newlayout/img/bni.png' 
        	),
        	'013' => array(
        		'name' 		=> 'Bank Permata',
        		'imgsource' => '/assets/themes/assets/newlayout/img/permatabank.png' 
        	),
        	'014' => array(
        		'name' 		=> 'Bank BCA',
        		'imgsource' => '/assets/themes/assets/newlayout/img/bca.png' 
        	),
        	'032' => array(
        		'name' 		=> 'Bank Jpmorgan Chase',
        		'imgsource' => '/assets/themes/assets/newlayout/img/chase.png' 
        	),
        	'031' => array(
        		'name' 		=> 'Citibank',
        		'imgsource' => '/assets/themes/assets/newlayout/img/citibank.png' 
        	),
        	'153' => array(
        		'name' 		=> 'Bank Sinarmas',
        		'imgsource' => '/assets/themes/assets/newlayout/img/sinar.png' 
        	),
        	'002' => array(
        		'name' 		=> 'Bank BRI',
        		'imgsource' => '/assets/themes/assets/newlayout/img/bri.png' 
        	),
			'022' => array(
        		'name' 		=> 'Bank CIMB',
        		'imgsource' => '/assets/themes/assets/newlayout/img/cimb.png' 
        	),
        	'046' => array(
        		'name' 		=> 'Bank DBS Indonesia',
        		'imgsource' => '/assets/themes/assets/newlayout/img/dbs.png' 
        	)

        	
        );

        $searchTerm = $_GET['term'];

        $bankData = array();
        foreach ($bankarr as $key => $value) {

        	if (strpos(strtolower($value['name']), strtolower($searchTerm)) !== false) {
        		$data['id']    = $key;
		        $data['value'] = $value['name'];
		        $data['label'] = '
		        <a href="javascript:void(0);" style="text-decoration:none;">
		            <img class="img-fluid" src="'.$value['imgsource'].'" width="70"/>
		            <span>  '.$value['name'].'</span>
		        </a>';
		        array_push($bankData, $data);
        	}
        }

        echo json_encode($bankData);

	}
}
