<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'SGO/Extendedmodule/phpqrcode/qrlib.php';
require_once ("Service/Account.php");

class eform_AmdetailController extends Application_Main
{

    protected $_moduleDB = 'RTF'; // masih harus diganti

    public function indexAction()
    {
        // echo "<code>masuk = $data</code>"; die;	
        $this->_helper->_layout->setLayout('newlayout');

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->systemType = $system_type;
        // echo "<code>systemType = $system_type</code><br>";	
        // echo '<pre>';
        // print_r($system_type); 
        $conf = Zend_Registry::get('config');
        $this->_bankName = $conf['app']['bankname'];
        $this->view->masterbankname = $this->_bankName;

        $this->view->currencyArr = array(
            // ''=>'-- '.$this->language->_('Any Value').' --',
            '1' => $this->language->_('IDR'),
            '2' => $this->language->_('USD'),
        );

        // get cash collateral

        $get_cash_collateral = $this->_db->select()
            ->from("M_CHARGES_OTHER", ["CHARGES_PCT", "CHARGES_ADM", "CHARGES_STAMP"])
            ->where("CUST_ID = ?", "GLOBAL")
            ->where("CHARGES_TYPE = ?", "10")
            ->query()->fetchAll();

        $this->view->cash_collateral = $get_cash_collateral[0];

        // end get cash collateral

        $selectcomp = $this->_db->select()
            ->from(array('A' => 'M_CUSTOMER'), array('*'))
            //  ->joinLeft(array('B' => 'M_COUNTRY'),'A.COUNTRY_CODE = B.COUNTRY_CODE',array('COUNTRY_NAME'))
            ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
            ->query()->fetchAll();

        $this->view->compinfo = $selectcomp[0];
        $numb = $this->_getParam('bgnumb');
        // $bg = $this->_getParam('bg');


        // $sessToken  = new Zend_Session_Namespace('Tokenenc');
        // $password   = $sessToken->token;

        // $AESMYSQL = new Crypt_AESMYSQL();
        // $decryption = urldecode($bg);
        // $numb = $AESMYSQL->decrypt($decryption, $password);
        // $this->view->bgnumb = $numb;
        // $this->view->bg = $numb;
        // echo "<code>numb = $numb</code><BR>";

        if (!empty($numb)) {
            $bgdata = $this->_db->select()
                ->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
                ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
                ->where('A.BG_REG_NUMBER = ?', $numb)
                ->query()->fetchAll();

            $bgdatadetail = $this->_db->select()
                ->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
                ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
                ->where('A.BG_REG_NUMBER = ?', $numb)
                ->query()->fetchAll();

            $bgdatasplit = $this->_db->select()
                ->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
                //  ->where('A.CUST_ID ='.$this->_db->quote((string)$this->_custIdLogin))
                ->where('A.BG_REG_NUMBER = ?', $numb)
                ->query()->fetchAll();
            $this->view->bgdatasplit = $bgdatasplit;
            // var_dump($datas);
            // echo '<pre>';
            // print_r($bgdatasplit);

            if (!empty($bgdata)) {

                $data = $bgdata['0'];
                $this->view->bgdata = $data;

                //$data = $bgdata['0'];

                if (!empty($data['BG_BRANCH'])) {
                    $selectbranch = $this->_db->select()
                        ->from(array('A' => 'M_BRANCH'), array('*'))
                        ->where('A.BRANCH_CODE = ?', $data['BG_BRANCH'])
                        ->query()->fetchAll();
                    //var_dump($selectbranch[0]['BRANCH_NAME']);die;
                    $this->view->branchname = $selectbranch[0]['BRANCH_NAME'];
                }

                $this->view->updateStart2 = $data['TIME_PERIOD_START'];
                $this->view->updateStart = Application_Helper_General::convertDate($data['TIME_PERIOD_START'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
                $this->view->updateEnd2 = $data['TIME_PERIOD_END'];
                $this->view->updateEnd = Application_Helper_General::convertDate($data['TIME_PERIOD_END'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
                // $updateEnd = Application_Helper_General::convertDate($data['TIME_PERIOD_END'],$this->view->viewDateFormat,$this->view->defaultDateFormat);


                // echo "<code>updateEnd = $updateEnd</code><BR>"; die;

                // BG status
                /*$bgStatus       = $conf["bg"]["status"]["desc"];
                $bgCode         = $conf["bg"]["status"]["code"];

                $arrStatus = array_combine(array_values($bgCode), array_values($bgStatus));
                */

                // $arrStatus = array(
                //     '0' => 'New',
                //     '1' => 'Amendment Changes',
                //     '2' => 'Amendment Draft'
                // );

                $arrStatus = array(
                    '7'  => 'Canceled',
                    '20' => 'On Risk',
                    '21' => 'Off Risk',
                    '22' => 'Claimed On Process',
                    '23' => 'Claimed'
                  );
              


                $this->view->arrStatus = $arrStatus;

                $comp = "'" . $this->_custIdLogin . "'";
                $acctlist = $this->_db->fetchAll(
                    $this->_db->select()
                        ->from(array('A' => 'M_APIKEY'))
                        ->join(array('C' => 'M_APICREDENTIAL'), 'A.APIKEY_ID = C.ID', array('*'))
                        ->join(array('B' => 'M_BANKTABLE'), 'B.BANK_CODE = A.BANK_CODE', array('B.BANK_NAME'))
                        // ->where('A.ACCT_STATUS = ?','5')
                        ->where("A.CUST_ID IN (" . $comp . ")")
                        ->order('A.APIKEY_ID ASC')
                    // echo $acctlist;
                );

                // echo "<pre>";
                // var_dump($acctlist);

                $account = array();
                foreach ($acctlist as $key => $value) {
                    $account[$value['ID']][$value['FIELD']] = $value['VALUE'];
                    $account[$value['ID']]['BANK_CODE'] = $value['BANK_CODE'];
                    $account[$value['ID']]['SENDER_ID'] = $value['SENDER_ID'];
                    $account[$value['ID']]['AUTH_USER'] = $value['AUTH_USER'];
                    $account[$value['ID']]['AUTH_PASS'] = $value['AUTH_PASS'];
                    $account[$value['ID']]['BANK_NAME'] = $value['BANK_NAME'];
                    $account[$value['ID']]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
                }
                // echo "<pre>";
                // var_dump($account);die;
                // $acct = array();
                $i = 0;
                foreach ($account as $key => $value) {

                    $acct[$i]['ACCT_NO'] = $value['account_number'];
                    $acct[$i]['ACCT_BANK'] = $value['BANK_NAME'];
                    $acct[$i]['BANK_CODE'] = $value['BANK_CODE'];
                    $acct[$i]['BANK_NAME'] = $value['BANK_NAME'];
                    $acct[$i]['ACCT_NAME'] = $value['account_name'];
                    $i++;
                }

                $this->view->sourceAcct = $acct;
                // echo "<code>acct start =========</code>";
                // echo '<pre>'; 
                // print_r($acct);
                // echo "<code>acct end=========</code>";


                $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);


                $param = array('CCY_IN' => "");
        	// $param = array('ACCT_TYPE' => "My Giro");
        	$AccArr = $CustomerUser->getAccountsBG($param);
        	//}
        	if (!empty($AccArr)) {
            		foreach ($AccArr as $i => $value) {

                		$AccArr[$i]['ACCT_BANK'] = $this->_bankName;
            		}
        	}

        	$this->view->AccArr = $AccArr;

                //usd
                $paramUSD = array('CCY_IN' => 'USD');
                $AccArrUSD = $CustomerUser->getAccountsBG($paramUSD);
                if (!empty($AccArrUSD)) {
                    foreach ($AccArrUSD as $iUSD => $valueUSD) {
                        $AccArrUSD[$iUSD]['ACCT_BANK'] = $this->_bankName;
                    }
                }

                $this->view->AccArrUSD = $AccArrUSD;
                // echo '<pre>';
                // echo "<code>AccArrUSD = $AccArrUSD</code>";
                // print_r($AccArrUSD);

                if (!empty($AccArr)) {
                    $this->view->src_name = $AccArr['0']['ACCT_NAME'];
                }

                $arrBankFormat = array(
                    1 => 'Bank Standard',
                    2 => 'Special Format (with bank approval)'
                );

                $this->view->BG_FORMAT = $data['BG_FORMAT'];
                $this->view->bankFormat = $arrBankFormat[$data['BG_FORMAT']];
                if ($data['BG_STATUS'] == '7' || $data['BG_STATUS'] == '11' ||  $data['BG_STATUS'] == '10' || !empty($data['BG_REJECT_NOTES']) || !empty($data['BG_CANCEL_NOTES'])) {
                    $selectQuery  = "SELECT
                        a.USER_LOGIN,
                        b.`USER_FULLNAME` AS u_name,
                        c.`BUSER_NAME` AS b_name,
                        a.DATE_TIME,
                        a.BG_REASON,
                        a.HISTORY_STATUS,
                        a.BG_REG_NUMBER
                        
                        
                    FROM
                        T_BANK_GUARANTEE_HISTORY AS a
                        LEFT JOIN M_USER AS b ON a.`USER_LOGIN` = b.`USER_ID` AND a.`CUST_ID` = b.`CUST_ID`
                        LEFT JOIN `M_BUSER` AS c ON a.`USER_LOGIN` = c.`BUSER_ID`
                    WHERE a.BG_REG_NUMBER = " . $this->_db->quote((string) $data['BG_REG_NUMBER']) . " AND a.HISTORY_STATUS IN (10) GROUP BY HISTORY_ID ORDER BY DATE_TIME";
                    if ($data['BG_STATUS'] == '10') {
                        $result =  $this->_db->fetchAll($selectQuery);
                    } else {
                        $result = array();
                    }
                    if (!empty($result)) {
                        $this->view->reqrepair = true;
                        $data['REASON'] = $result['0']['BG_REASON'];
                        $this->view->username = ' (By ' . $result['0']['u_name'] . '' . $result['0']['b_name'] . ')';
                    }
                    //var_dump($result);die;


                    $this->view->reason = $data['REASON'] . '' . $data['BG_REJECT_NOTES'] . $data['BG_CANCEL_NOTES'];
                }

                $this->view->TIME_PERIOD_START = $data['TIME_PERIOD_START'];
                $this->view->TIME_PERIOD_END = $data['TIME_PERIOD_END'];

                $arrLang = array(
                    1 => 'Indonesian',
                    2 => 'English',
                    3 => 'Billingual',
                );

                $this->view->BG_LANGUAGE = $data['BG_LANGUAGE'];
                $this->view->languagetext = $arrLang[$data['BG_LANGUAGE']];

                $arrWaranty = array(
                    1 => 'Full Cover, Savings Account (Giro / Time Deposit / Savings) or Cash Deposit (MD)',
                    2 => 'Indirect Credit Facility (Non Cash Loan) / Bank Guarantee Ceiling',
                    3 => 'Insurance'

                );

                // BG TYPE
                $bgType         = $conf["bg"]["type"]["desc"];
                $bgCode         = $conf["bg"]["type"]["code"];

                $arrbgType = array_combine(array_values($bgCode), array_values($bgType));

                foreach ($arrbgType as $key => $val) {
                    $arrbgType_new[$key] =  array(
                        'ID' => $key,
                        'VALUE' => $val,
                    );
                }
                $list_bg_purpose                = array('' => ' --- ' . $this->language->_('Choose One') . ' ---');
                $list_bg_purpose                += Application_Helper_Array::listArray($arrbgType_new, 'ID', 'VALUE');
                $this->view->bgpurposeArr       = $list_bg_purpose;

                // $this->view->arrbgType = $arrbgType;
                $this->view->BG_PURPOSE = $data['USAGE_PURPOSE'];
                $this->view->BG_PURPOSE_LBL = $arrbgType[$data['USAGE_PURPOSE']];
                $this->view->BG_PURPOSE_DESC = $arrbgType[$data['USAGE_PURPOSE']];

                //BG Document Type
                $bgdocType         = $conf["bgdoc"]["type"]["desc"];
                $bgdocCode         = $conf["bgdoc"]["type"]["code"];

                $arrbgdoc = array_combine(array_values($bgdocCode), array_values($bgdocType));

                foreach ($arrbgdoc as $key => $val) {
                    $arrbgdoc_new[$key] =  array(
                        'ID' => $key,
                        'VALUE' => $val,
                    );
                }
                $list_document_type                = array('' => ' --- ' . $this->language->_('Choose One') . ' ---');
                $list_document_type                += Application_Helper_Array::listArray($arrbgdoc_new, 'ID', 'VALUE');
                $this->view->documenttypeArr     = $list_document_type;

                $this->view->GT_DOC_TYPE =  $data['GT_DOC_TYPE'];
                $this->view->GT_DOC_DESC = $arrbgdoc[$data['GT_DOC_TYPE']];
                // echo '<pre>';
                // print_r($bgdocCode); 
                // print_r($bgdocType); 
                // print_r($arrbgdoc); 
                // print_r($data['GT_DOC_TYPE']); 

                //BG Counter Guarantee Type
                $bgcgType         = $conf["bgcg"]["type"]["desc"];
                $bgcgCode         = $conf["bgcg"]["type"]["code"];

                $arrbgcg = array_combine(array_values($bgcgCode), array_values($bgcgType));

                foreach ($arrbgcg as $key => $val) {
                    $arrbgcg_new[$key] =  array(
                        'ID' => $key,
                        'VALUE' => $val,
                    );
                }
                $list_warranty_type                = array('' => ' --- ' . $this->language->_('Choose One') . ' ---');
                $list_warranty_type                += Application_Helper_Array::listArray($arrbgcg_new, 'ID', 'VALUE');
                $this->view->warrantytypeArr     = $list_warranty_type;

                // echo '<pre>';
                // print_r($bgcgCode); 
                // print_r($bgcgType); 
                // print_r($arrbgcg); 
                // print_r($arrbgcg_new); 
                // print_r($list_warranty_type); 
                $this->view->COUNTER_WARRANTY_TYPE = $data['COUNTER_WARRANTY_TYPE'];
                $this->view->warranty_type_text = $arrbgcg[$data['COUNTER_WARRANTY_TYPE']];

                // BG Publishing Form
                $bgpublishType         = $conf["bgpublish"]["type"]["desc"];
                $bgpublishCode         = $conf["bgpublish"]["type"]["code"];

                $arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));
                foreach ($arrbgpublish as $key => $val) {
                    $arrbgpublish_new[$key] =  array(
                        'ID' => $key,
                        'VALUE' => $val,
                    );
                }
                $list_bg_publist                = array('' => ' --- ' . $this->language->_('Any Value') . ' ---');
                $list_bg_publist                += Application_Helper_Array::listArray($arrbgpublish_new, 'ID', 'VALUE');
                $this->view->BG_PUBLISH_ARR     = $list_bg_publist;
                // echo '<pre>';
                // print_r($list_bg_publist); 
                $this->view->BG_PUBLISH = $data['BG_PUBLISH'];
                $this->view->BG_PUBLISH_DESC = $arrbgpublish[$data['BG_PUBLISH']];

                //$this->view->warranty_type_text = $arrWaranty[$data['COUNTER_WARRANTY_TYPE']];

                if (!empty($data['USAGE_PURPOSE'])) {
                    $data['USAGE_PURPOSE'] = explode(',', $data['USAGE_PURPOSE']);
                    foreach ($data['USAGE_PURPOSE'] as $key => $val) {
                        $str = 'checkp' . $val;
                        //var_dump($str);
                        $this->view->$str =  'checked';
                    }
                }

                if ($data['BG_NUMBER'] == '') {
                    $data['BG_NUMBER'] = '-';
                }
                if ($data['BG_SUBJECT'] == '') {
                    $data['BG_SUBJECT'] = '- no subject -';
                }
                $this->view->BG_REG_NUMBER = $data['BG_REG_NUMBER'];
                $this->view->BG_NUMBER = $data['BG_NUMBER'];
                $this->view->BG_SUBJECT = $data['BG_SUBJECT'];


                $this->view->recipent_name = $data['RECIPIENT_NAME'];
                $this->view->address = $data['RECIPIENT_ADDRES'];
                $this->view->city = $data['RECIPIENT_CITY'];
                $this->view->contact_number = $data['RECIPIENT_CONTACT'];
                $this->view->comment = $data['GUARANTEE_TRANSACTION'];

                $this->view->fileName = $data['FILE'];
                $this->view->bank_amount = $data['BG_AMOUNT'];
                $this->view->warranty_type = $data['COUNTER_WARRANTY_TYPE'];
				$this->view->COUNTER_WARRANTY_TYPE = $data['COUNTER_WARRANTY_TYPE'];
				
                $this->view->account_number = $data['COUNTER_WARRANTY_ACCT_NO'];
                $this->view->account_name = $data['COUNTER_WARRANTY_ACCT_NAME'];

                $this->view->amount = $data['COUNTER_WARRANTY_AMOUNT'];
                $this->view->acct = $data['FEE_CHARGE_TO'];
                $this->view->status = $data['BG_STATUS'];

                $this->view->CUST_EMAIL = $data['CUST_EMAIL'];

                $this->view->CUST_CP = $data['CUST_CP'];
                $this->view->CUST_EMAIL = $data['CUST_EMAIL'];
                $this->view->CUST_CONTACT_NUMBER = $data['CUST_CONTACT_NUMBER'];

                $this->view->RECIPIENT_CP = $data['RECIPIENT_CP'];
                $this->view->RECIPIENT_OFFICE_NUMBER = $data['RECIPIENT_OFFICE_NUMBER'];
                $this->view->RECIPIENT_EMAIL = $data['RECIPIENT_EMAIL'];
                $this->view->RECIPIENT_CONTACT = $data['RECIPIENT_CONTACT'];
                $this->view->SERVICE = $data['SERVICE'];

                $this->view->GT_DOC_NUMBER = $data['GT_DOC_NUMBER'];
                $this->view->GT_DOC_DATE = $data['GT_DOC_DATE'];

                $this->view->BG_CLAIM_PERIOD = $data['BG_CLAIM_PERIOD'];
                $this->view->BG_UNDERLYING_DOC = $data['BG_UNDERLYING_DOC'];

                $this->view->CHANGE_TYPE = $data['CHANGE_TYPE'];


                if (!empty($bgdatadetail)) {
                    foreach ($bgdatadetail as $key => $value) {

                        if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
                            if ($value['PS_FIELDNAME'] == 'Insurance Name') {
                                $this->view->insuranceName =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
                                $this->view->PrincipalAgreement =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount') {
                                $this->view->insurance_amount =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement Start Date') {
                                $this->view->paDateStart =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement End Date') {
                                $this->view->paDataEnd =   $value['PS_FIELDVALUE'];
                            }
                        } else {

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 1') {
                                $this->view->owner1 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 1') {
                                $this->view->amountowner1 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
                                $this->view->owner2 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
                                $this->view->amountowner2 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
                                $this->view->owner3 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
                                $this->view->amountowner3 =   $value['PS_FIELDVALUE'];
                            }
                        }
                    }
                }



                $conf = Zend_Registry::get('config');
                $this->view->bankname = $conf['app']['bankname'];
                // preliminary Member
                $selectMP = $this->_db->select()
                    ->from(array('MP' => 'M_PRELIMINARY_MEMBER'), array('*'));
                $selectMP->where('MP.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin));
                // echo "<code>selectMP = $selectMP</code><BR>";  
                $preliminaryMemberArr = $this->_db->fetchAll($selectMP);
                // echo '<pre>';
                // print_r($preliminaryMemberArr);
                $this->view->preliminaryMemberArr = $preliminaryMemberArr;

                $params = $this->_request->getParams();
                $toa = $params['toa'];

                $BG_UNDERLYING_DOC = $this->_getParam('BG_UNDERLYING_DOC');
                $GT_DOC_NUMBER = $this->_getParam('GT_DOC_NUMBER');
                //print_r($edit);die;

                $selectCity = $this->_db->select()
                    ->from(array('MP' => 'M_CITYLIST'), array('*'));
                $selectCityArr = $this->_db->fetchAll($selectMP);

                $this->view->arrCity = $selectCityArr;


                $download = $this->_getParam('download');
                if ($download) {

                    $attahmentDestination = UPLOAD_PATH . '/document/submit/';
                    if ($GT_DOC_NUMBER) {
                        $this->_helper->download->file($data['FILE'], $attahmentDestination . $data['FILE']);
                    }

                    if ($BG_UNDERLYING_DOC) {
                        // echo "<code>masuk = $GT_DOC_NUMBER</code><BR>"; die;
                        // $attahmentDestination = UPLOAD_PATH . '/document/submit/';
                        $this->_helper->download->file($data['BG_UNDERLYING_DOC'], $attahmentDestination . $data['BG_UNDERLYING_DOC']);
                    }
                }


                if ($this->_request->isPost()) {
				if(!empty($bgdatasplit)){
					foreach($bgdatasplit as $vl){
						//var_dump($vl);
						$svcAccount = new Service_Account($vl['ACCT'], 'IDR');
						$result = $svcAccount->inquiryAccountBalance('AB',TRUE);
						if($result['balanceactive']<= $vl['AMOUNT']){
							$err = true;
							$err_msg = 'not enought balance '.$vl['ACCT'];
						}
						
					}
				}
				
				if(!empty($data['FEE_CHARGE_TO'])){
					$svcAccount = new Service_Account($vl['FEE_CHARGE_TO'], 'IDR');
						$result = $svcAccount->inquiryAccountBalance('AB',TRUE);
						if($result['response_code']!= '0000' && $result['balance_active']>= 0){
							$err = true;
							$err_msg = 'not enought balance '.$vl['ACCT'];
						}
				}
				//die('here');

                    $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
                    if (count($temp) > 1) {
                        if ($temp[0] == 'F' || $temp[0] == 'S') {
                            if ($temp[0] == 'F')
                                $this->view->error = 1;
                            else
                                $this->view->success = 1;
                            $msg = '';
                            unset($temp[0]);
                            foreach ($temp as $value) {
                                if (!is_array($value))
                                    $value = array($value);
                                $msg .= $this->view->formErrors($value);
                            }
                            $this->view->report_msg = $msg;
                        }
                    }
                    $attahmentDestination     = UPLOAD_PATH . '/document/submit/';
                    $errorRemark             = null;
                    $adapter                 = new Zend_File_Transfer_Adapter_Http();
                    $fileExt                 = "jpeg,jpg,pdf";

                    $sourceFileName = $adapter->getFileName();

                    if ($sourceFileName == null) {
                        $sourceFileName = null;
                        $fileType = null;
                    } else {
                        $sourceFileName = substr(basename($adapter->getFileName()), 0);
                        // echo "<code>sourceFileName = $sourceFileName</code>"; die;	
                        if ($_FILES["uploadSource"]["type"]) {
                            $adapter->setDestination($attahmentDestination);
                            $maxFileSize             = $this->getSetting('Fe_attachment_maxbyte');
                            $fileType = $adapter->getMimeType();
                            $fileInfo = $adapter->getFileInfo();
                            $size = $_FILES["uploadSource"]["size"];
                        } else {
                            $fileType = null;
                            $size = null;
                        }
                    }

                    // $filters = array(
                    //     'updatedate'                        => array('StripTags','StringTrim'),   
                    // );

                    // if ($toa=='1')
                    // {
                    $filters = array(
                        'updatedate'                    => array('StripTags', 'StringTrim'),
                        'CUST_CP_TXT'                   => array('StripTags', 'StringTrim'),
                        'CUST_CONTACT_NUMBER_TXT'       => array('StripTags', 'StringTrim'),
                        'CUST_EMAIL_TXT'                => array('StripTags', 'StringTrim'),
                        'BG_PURPOSE_TXT'                => array('StripTags', 'StringTrim'),
                        'bank_amount_txt'               => array('StripTags', 'StringTrim'),
                        'currency'                      => array('StripTags', 'StringTrim'),
                        'RECIPIENT_CP_TXT'              => array('StripTags', 'StringTrim'),
                        'RECIPIENT_CONTACT_TXT'         => array('StripTags', 'StringTrim'),
                        'RECIPIENT_EMAIL_TXT'           => array('StripTags', 'StringTrim'),
                        'SERVICE_TXT'                   => array('StripTags', 'StringTrim'),
                        'GT_DOC_TYPE_TXT'               => array('StripTags', 'StringTrim'),
                        'GT_DOC_NUMBER_TXT'             => array('StripTags', 'StringTrim'),
                        'underlyingDocument'            => array('StripTags', 'StringTrim'),
                        'GT_DOC_DATE_TXT'               => array('StripTags', 'StringTrim'),
                        'acct_txt'                      => array('StripTags', 'StringTrim'),
                        'COUNTER_WARRANTY_TYPE_TXT'     => array('StripTags', 'StringTrim'),
                        'BG_PUBLISH_TXT'                => array('StripTags', 'StringTrim'),
                        'BG_FORMAT'                     => array('StripTags', 'StringTrim'),
                        'BG_LANGUAGE'                   => array('StripTags', 'StringTrim'),

                    );
                    // }

                    $validators =  array(
                        'acct_txt'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'BG_PUBLISH_TXT'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'BG_FORMAT'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),
                        'BG_LANGUAGE'      => array(
                            'NotEmpty',
                            array('StringLength', array('min' => 1, 'max' => 50)),
                            'messages' => array(
                                'Can not be empty',
                                'invalid'
                            )
                        ),

                    );

                    // echo "<code>toa = $toa</code><BR>"; die;	
                    if ($toa == '1') {

                        $validators +=  array(
                            'CUST_CP_TXT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'CUST_CONTACT_NUMBER_TXT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'CUST_EMAIL_TXT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'BG_PURPOSE_TXT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'bank_amount_txt'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'updatedate'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'currency'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'RECIPIENT_CP_TXT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'RECIPIENT_CONTACT_TXT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'RECIPIENT_EMAIL_TXT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'SERVICE_TXT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'GT_DOC_TYPE_TXT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'GT_DOC_NUMBER_TXT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'underlyingDocument'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'GT_DOC_DATE_TXT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'COUNTER_WARRANTY_TYPE_TXT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),

                        );
                    }

                    // echo "<code>masuk1 = $data</code>"; die;	
                    $zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);

                    if ($zf_filter_input->isValid()) {

                        // echo "<code>masuk1 = $data</code><BR>"; die;	

                        if ($zf_filter_input->COUNTER_WARRANTY_TYPE_TXT == '1') {

                            if (!empty($params['flselect_account_manual'])) {
                                $splitArr = array();
                                foreach ($params['flselect_account_manual'] as $ky => $vl) {
                                    if ($vl == '') {
                                        $nameAcct = explode('|', $params['flselect_account_number'][$ky]);
                                        $splitArr[$ky]['acct'] = $nameAcct[0];
                                    } else {
                                        $splitArr[$ky]['acct'] = $vl;
                                    }

                                    if ($params['flname_manual'][$ky] == '') {
                                        $splitArr[$ky]['acct_name'] = $params['flname'][$ky];
                                    } else {
                                        $splitArr[$ky]['acct_name'] = $params['flname_manual'][$ky];
                                    }

                                    $splitArr[$ky]['amount'] = $params['flamount'][$ky];
                                    $splitArr[$ky]['flag'] = $params['flselect_own'][$ky];
                                    $splitArr[$ky]['bank_code'] = '999';


                                    // echo "<code>masuk = $params</code>"; 
                                }
                                $this->view->acctSplit = $splitArr;
                            }
                        }
                        // echo '<pre>';
                        // print_r($splitArr);die;

                        if ($params['currency_type'] == "1") {

                            // echo "<code>masuk1 = $data</code><BR>"; die;
                            // echo '<pre>';
                            // print_r($params['flselect_own']);die;
                            // foreach($params['flselect_own'] as $i => $val)
                            // {
                            //     if($val == "1")
                            //     {
                            //         $arr_select_account_number = explode("|",$select_account_number[$i]);
                            //         $account_number .= $arr_select_account_number[0].',';
                            //         $account_name .= $arr_select_account_number[1].',';
                            //         $amount .= $params['flamount'][$i].',';
                            //     }
                            //     else if($val == "2")
                            //     {
                            //         $account_number .= $select_account_number[$i].',';
                            //         $account_name .= $flname_manual[$i].',';
                            //         $amount .= $params['flamount'][$i].',';
                            //     }
                            // }

                            // echo "<code>account_number = $account_number</code><BR>"; die;
                            // if($params['select_own'] == "1")
                            // {
                            // echo "<code>masuk1 = $data</code><BR>"; die;
                            $select_account_number = $params['flselect_account_number'];
                            $account_number = '';
                            $account_name = '';
                            $amount = '';
                            for ($i = 0; $i <= count($params['flselect_account_number']); $i++) {
                                $arr_select_account_number = explode("|", $select_account_number[$i]);
                                $account_number .= $arr_select_account_number[0] . ',';
                                $account_name .= $arr_select_account_number[1] . ',';
                                $amount .= $params['flamount'][$i] . ',';
                            }
                            $account_number = substr($account_number, 0, -1);
                            $account_name = substr($account_name, 0, -1);
                            $amount = substr($amount, 0, -1);

                            // }
                            // else
                            // {
                            //     // echo "<code>masuk2 = $data</code><BR>"; die;
                            //     $select_account_number = $params['flselect_account_manual'];
                            //     $flname_manual = $params['flname_manual'];

                            //     $account_number = '';
                            //     $account_name = '';
                            //     $amount = '';
                            //     for($i=0;$i<=count($params['flselect_account_manual']);$i++){
                            //         $account_number .= $select_account_number[$i].',';
                            //         $account_name .= $flname_manual[$i].',';
                            //         $amount .= $params['flamount'][$i].',';
                            //     }
                            //     $account_number = substr($account_number, 0, -1);
                            //     $account_name = substr($account_name, 0, -1);
                            //     $amount = substr($amount, 0, -1);
                            // }


                            $acct = $params['acct'];
                        } elseif ($params['currency_type'] == "2") {
                            $select_account_numberUSD = $params['select_account_numberUSD'];
                            for ($i = 0; $i <= count($params['select_account_numberUSD']); $i++) {
                                $arr_account_numberUSD = explode("|", $select_account_numberUSD[$i]);
                                $account_number .= $arr_account_numberUSD[0] . ',';;
                                $account_name .= $arr_account_numberUSD[1] . ',';;
                            }

                            $acct = $params['acctUSD'];
                        }

                        $fileTypeMessage = explode('/', $fileType);
                        $fileType =  $fileTypeMessage[1];
                        $extensionValidator = new Zend_Validate_File_Extension(array(false, $fileExt));
                        $extensionValidator->setMessage("Extension file must be *.pdf");

                        $maxFileSize = "1024000";
                        $size = number_format($size);

                        $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $maxFileSize));
                        $sizeValidator->setMessage("File size is exceeding 1,024,000 byte(s), uploaded file is $size byte(s)");

                        $adapter->setValidators(array($extensionValidator, $sizeValidator));

                        if (!empty($fileInfo['uploadSource']["name"])) {

                            if ($adapter->isValid()) {

                                if ($adapter->receive()) {
                                    // echo "<code>masuk = $data</code>"; die;	
                                    $file_name = $fileInfo['uploadSource']["name"];
                                    // unlink($attahmentDestination.$data['BG_UNDERLYING_DOC']); 

                                } else {
                                    $errmsgs = $adapter->getMessages();
                                    foreach ($errmsgs as $key => $val) {
                                        $errfield = "error_" . $fieldname;
                                        $errorArray[$errfield] = $val;
                                    }
                                    $this->view->error = true;
                                    $this->view->err_msg = $errmsgs;
                                    // echo '<pre>';
                                    // echo "<code>errors1 = $errors</code>"; die;	
                                    // print_r($errmsgs);die;
                                }
                            } else {
                                $this->view->error = true;
                                $errors = $this->language->_('Extension file must be *.jpg /.jpeg / .pdf');
                                $this->view->err_msg = $errors;
                                // echo '<pre>';
                                // echo "<code>errors2 new= $errors</code>"; die;	
                                // print_r($errors);die;
                            }
                        } else {
                            $file_name = $data['BG_UNDERLYING_DOC'];
                        }

                        // echo '<pre>';
                        // print_r($fileInfo);die;

                        // echo "<code>file_name = $file_name</code>"; die;
                        try {

                            $this->_db->beginTransaction();

                            if ($zf_filter_input->BG_FORMAT == 1) {
                                $BG_LANGUAGE = $zf_filter_input->BG_LANGUAGE;
                            } else {
                                $BG_LANGUAGE = 0;
                            }

                            if ($toa == '1') {
                                // $insertArr = array(   
                                //     'BG_REG_NUMBER'                     => $this->generateTransactionID($data['BG_BRANCH'],$zf_filter_input->BG_PURPOSE_TXT), 
                                //     'CUST_CP'                           => $zf_filter_input->CUST_CP_TXT,
                                //     'CUST_CONTACT_NUMBER'               => $zf_filter_input->CUST_CONTACT_NUMBER_TXT,
                                //     'CUST_EMAIL'                        => $zf_filter_input->CUST_EMAIL_TXT,
                                //     'BG_AMOUNT'                         => $zf_filter_input->bank_amount_txt, 
                                //     'USAGE_PURPOSE'                     => $zf_filter_input->BG_PURPOSE_TXT,
                                //     'RECIPIENT_CP'                      => $zf_filter_input->RECIPIENT_CP_TXT,
                                //     'RECIPIENT_CONTACT'                 => $zf_filter_input->RECIPIENT_CONTACT_TXT,
                                //     'RECIPIENT_EMAIL'                   => $zf_filter_input->RECIPIENT_EMAIL_TXT,
                                //     'SERVICE'                           => $zf_filter_input->SERVICE_TXT,
                                //     'GT_DOC_TYPE'                       => $zf_filter_input->GT_DOC_TYPE_TXT,
                                //     'GT_DOC_NUMBER'                     => $zf_filter_input->GT_DOC_NUMBER_TXT,
                                //     'GT_DOC_DATE'                       => $zf_filter_input->GT_DOC_DATE_TXT,
                                //     'FEE_CHARGE_TO'                     => $zf_filter_input->acct_txt,
                                //     'COUNTER_WARRANTY_TYPE'             => $zf_filter_input->COUNTER_WARRANTY_TYPE_TXT,
                                //     'BG_PUBLISH'                        => $zf_filter_input->BG_PUBLISH_TXT, 
                                //     'BG_FORMAT'                         => $zf_filter_input->BG_FORMAT,
                                //     'BG_LANGUAGE'                       => $zf_filter_input->BG_LANGUAGE,
                                //     'BG_UNDERLYING_DOC'                 => $file_name,   
                                //     'BG_UPDATED'                        => new Zend_Db_Expr('now()'),
                                //     'BG_UPDATEDBY'                      => $this->_userIdLogin, 
                                // );


                                $insertArr = array(
                                    'BG_REG_NUMBER'                     => $this->generateTransactionID($data['BG_BRANCH'], $zf_filter_input->BG_PURPOSE_TXT),
                                    // 'BG_REG_NUMBER'                     => $data['BG_REG_NUMBER'], 
                                    'ACCT_NO'                           => $data['ACCT_NO'],
                                    'BG_NUMBER'                         => $data['BG_NUMBER'],
                                    'BG_SUBJECT'                        => $data['BG_SUBJECT'],
                                    'CUST_ID'                           => $this->_custIdLogin,
                                    'CUST_CP'                           => $zf_filter_input->CUST_CP_TXT,
                                    'CUST_CONTACT_NUMBER'               => $zf_filter_input->CUST_CONTACT_NUMBER_TXT,
                                    'CUST_EMAIL'                        => $zf_filter_input->CUST_EMAIL_TXT,
                                    'BG_STATUS'                         => 2,
                                    'USAGE_PURPOSE'                     => $zf_filter_input->BG_PURPOSE_TXT,
                                    'USAGE_PURPOSE_DESC'                => $data['USAGE_PURPOSE_DESC'],
                                    'RECIPIENT_NAME'                    => $data['RECIPIENT_NAME'],
                                    'RECIPIENT_ADDRES'                  => $data['RECIPIENT_ADDRES'],
                                    'RECIPIENT_CITY'                    => $data['RECIPIENT_CITY'],
                                    'RECIPIENT_CP'                      => $zf_filter_input->RECIPIENT_CP_TXT,
                                    'RECIPIENT_OFFICE_NUMBER'           => $data['RECIPIENT_OFFICE_NUMBER'],
                                    'RECIPIENT_CONTACT'                 => $zf_filter_input->RECIPIENT_CONTACT_TXT,
                                    'RECIPIENT_EMAIL'                   => $zf_filter_input->RECIPIENT_EMAIL_TXT,

                                    "PROVISION_FEE" => Application_Helper_General::convertDisplayMoney($this->_getParam("provision_fee_input")),
                                    "ADM_FEE" => Application_Helper_General::convertDisplayMoney($this->_getParam("administration_fee_input")),
                                    "STAMP_FEE" => Application_Helper_General::convertDisplayMoney($this->_getParam("stamp_fee_input")),

                                    'SERVICE'                           => $zf_filter_input->SERVICE_TXT,
                                    'GT_DOC_TYPE'                       => $zf_filter_input->GT_DOC_TYPE_TXT,
                                    'GT_DOC_NUMBER'                     => $zf_filter_input->GT_DOC_NUMBER_TXT,
                                    'GT_DOC_DATE'                       => $zf_filter_input->GT_DOC_DATE_TXT,
                                    'GT_DOC_OTHER'                      => $data['GT_DOC_OTHER'],
                                    'GUARANTEE_TRANSACTION'             => $data['GUARANTEE_TRANSACTION'],
                                    'SP_OBLIGEE_CODE'                   => $data['SP_OBLIGEE_CODE'],
                                    'FILE'                              => $data['FILE'],
                                    'BG_AMOUNT'                         => Application_Helper_General::convertDisplayMoney($zf_filter_input->bank_amount_txt),
                                    'TIME_PERIOD_START'                 => $data['TIME_PERIOD_START'],
                                    'TIME_PERIOD_END'                   => $zf_filter_input->updatedate,

                                    'COUNTER_WARRANTY_TYPE'             => $zf_filter_input->COUNTER_WARRANTY_TYPE_TXT,
                                    // 'COUNTER_WARRANTY_TYPE'             => $data['COUNTER_WARRANTY_TYPE'], 
                                    'COUNTER_WARRANTY_ACCT_NO'          => rtrim($account_number, ","),
                                    'COUNTER_WARRANTY_ACCT_NAME'        => rtrim($account_name, ","),
                                    'COUNTER_WARRANTY_AMOUNT'           => Application_Helper_General::convertDisplayMoney(rtrim($amount, ",")),

                                    'FEE_CHARGE_TO'                     => $zf_filter_input->acct_txt,
                                    'BG_FORMAT'                         => $zf_filter_input->BG_FORMAT,
                                    'BG_LANGUAGE'                       => $BG_LANGUAGE,

                                    'BG_CREATED'                        => new Zend_Db_Expr('now()'),
                                    'BG_CREATEDBY'                      => $this->_userIdLogin,
                                    'BG_UPDATED'                        => new Zend_Db_Expr('now()'),
                                    'BG_UPDATEDBY'                      => $this->_userIdLogin,
                                    'BG_REJECT_NOTES'                   => $data['BG_REJECT_NOTES'],
                                    'BG_APPROVE_DOC'                    => $data['BG_APPROVE_DOC'],
                                    'REASON'                            => $data['REASON'],
                                    'BG_CLAIM_PERIOD'                   => $data['BG_CLAIM_PERIOD'],
                                    'BG_CLAIMEDBY'                      => $data['BG_CLAIMEDBY'],
                                    'BG_CLAIM_NOTES'                    => $data['BG_CLAIM_NOTES'],
                                    'BG_CANCELEDBY'                     => $data['BG_CANCELEDBY'],
                                    'BG_CANCEL_NOTES'                   => $data['BG_CANCEL_NOTES'],
                                    'BG_UNDERLYING_DOC'                 => $data['BG_UNDERLYING_DOC'],
                                    'BG_INSURANCE_CODE'                 => $data['BG_INSURANCE_CODE'],
                                    'BG_BRANCH'                         => $data['BG_BRANCH'],
                                    'BG_PUBLISH'                        => $zf_filter_input->BG_PUBLISH_TXT,
                                    'IS_AMENDMENT'                      => 1,
                                    'CHANGE_TYPE'                       => $toa

                                );
                                // echo '<pre>';
                                // print_r($insertArr);die;
                                // echo "<code>masuk2 = $data</code><BR>"; die;	
                                $this->_db->insert('TEMP_BANK_GUARANTEE', $insertArr);

                                // echo "<code>masuk2 = $data</code><BR>"; die;	
                                if ($zf_filter_input->COUNTER_WARRANTY_TYPE_TXT == '1') {
                                    // echo "<code>COUNTER_WARRANTY_TYPE_TXT = 1</code>"; die;	   
                                    // $this->_db->delete('T_BANK_GUARANTEE_SPLIT','BG_NUMBER = '.$this->_db->quote($numb));

                                    foreach ($splitArr as $ky => $vl) {
                                        $tmparrDetail = array(
                                            'BG_REG_NUMBER'         => $insertArr['BG_REG_NUMBER'],//update reza
                                            'ACCT'              => $vl['acct'],
                                            'BANK_CODE'         => $vl['bank_code'],
                                            'NAME'              => $vl['acct_name'],
                                            'AMOUNT'            => Application_Helper_General::convertDisplayMoney(rtrim($vl['amount'], ",")),
                                            'FLAG'              => $vl['flag']
                                        );

                                        // echo '<pre>';
                                        // print_r($tmparrDetail);die;
                                        $this->_db->insert('TEMP_BANK_GUARANTEE_SPLIT', $tmparrDetail);
                                    }
                                }

                                // else
                                // {
                                //     echo "<code>COUNTER_WARRANTY_TYPE_TXT = $COUNTER_WARRANTY_TYPE_TXT</code>"; die;
                                // }

                            } else if ($toa == '2') {

                                // echo '<pre>';
                                // print_r($zf_filter_input->updatedate);die;
                                $insertArr = array(
                                    'BG_REG_NUMBER'                     => $this->generateTransactionID($data['BG_BRANCH'], $data['USAGE_PURPOSE'][0]),
                                    'ACCT_NO'                           => $data['ACCT_NO'],
                                    'BG_NUMBER'                         => $data['BG_NUMBER'],
                                    'BG_SUBJECT'                        => $data['BG_SUBJECT'],
                                    'CUST_ID'                           => $data['CUST_ID'],
                                    'CUST_CP'                           => $data['CUST_CP'],
                                    'CUST_EMAIL'                        => $data['CUST_EMAIL'],
                                    'CUST_CONTACT_NUMBER'               => $data['CUST_CONTACT_NUMBER'],
                                    'BG_STATUS'                         => 2,
                                    'USAGE_PURPOSE'                     => $data['USAGE_PURPOSE'][0],
                                    'USAGE_PURPOSE_DESC'                => $data['USAGE_PURPOSE_DESC'],
                                    'RECIPIENT_NAME'                    => $data['RECIPIENT_NAME'],
                                    'RECIPIENT_ADDRES'                  => $data['RECIPIENT_ADDRES'],
                                    'RECIPIENT_CITY'                    => $data['RECIPIENT_CITY'],
                                    'RECIPIENT_CP'                      => $data['RECIPIENT_CP'],
                                    'RECIPIENT_OFFICE_NUMBER'           => $data['RECIPIENT_OFFICE_NUMBER'],
                                    'RECIPIENT_EMAIL'                   => $data['RECIPIENT_EMAIL'],
                                    'RECIPIENT_CONTACT'                 => $data['RECIPIENT_CONTACT'],
                                    'SERVICE'                           => $data['SERVICE'],
                                    'GT_DOC_TYPE'                       => $data['GT_DOC_TYPE'],
                                    'GT_DOC_OTHER'                      => $data['GT_DOC_OTHER'],
                                    'GT_DOC_NUMBER'                     => $data['GT_DOC_NUMBER'],
                                    'GT_DOC_DATE'                       => $data['GT_DOC_DATE'],
                                    'GUARANTEE_TRANSACTION'             => $data['GUARANTEE_TRANSACTION'],
                                    'SP_OBLIGEE_CODE'                   => $data['SP_OBLIGEE_CODE'],
                                    'FILE'                              => $data['FILE'],
                                    'BG_AMOUNT'                         => Application_Helper_General::convertDisplayMoney($data['BG_AMOUNT']),
                                    'TIME_PERIOD_START'                 => $data['TIME_PERIOD_START'],
                                    'TIME_PERIOD_END'                    => $data['TIME_PERIOD_END'],
                                    // 'TIME_PERIOD_END'                   => $zf_filter_input->updatedate,

                                    'COUNTER_WARRANTY_TYPE'             => $data['COUNTER_WARRANTY_TYPE'],
                                    'COUNTER_WARRANTY_ACCT_NO'          => $data['COUNTER_WARRANTY_ACCT_NO'],
                                    'COUNTER_WARRANTY_ACCT_NAME'        => $data['COUNTER_WARRANTY_ACCT_NAME'],
                                    'COUNTER_WARRANTY_AMOUNT'           => $data['COUNTER_WARRANTY_AMOUNT'],

                                    // 'FEE_CHARGE_TO'                     => $data['FEE_CHARGE_TO'],
                                    // 'BG_FORMAT'                         => $data['BG_FORMAT'],
                                    // 'BG_LANGUAGE'                       => $data['BG_LANGUAGE'],

                                    'FEE_CHARGE_TO'                     => $zf_filter_input->acct_txt,
                                    'BG_FORMAT'                         => $zf_filter_input->BG_FORMAT,
                                    'BG_LANGUAGE'                       => $BG_LANGUAGE,

                                    "PROVISION_FEE" => 0,
                                    "ADM_FEE" => Application_Helper_General::convertDisplayMoney($this->_getParam("administration_fee_input")),
                                    "STAMP_FEE" => Application_Helper_General::convertDisplayMoney($this->_getParam("stamp_fee_input")),

                                    'BG_CREATED'                        => new Zend_Db_Expr('now()'),
                                    'BG_CREATEDBY'                      => $this->_userIdLogin,
                                    'BG_UPDATED'                        => new Zend_Db_Expr('now()'),
                                    'BG_UPDATEDBY'                      => $this->_userIdLogin,
                                    'BG_REJECT_NOTES'                   => $data['BG_REJECT_NOTES'],
                                    'BG_APPROVE_DOC'                    => $data['BG_APPROVE_DOC'],
                                    'REASON'                            => $data['REASON'],
                                    'BG_CLAIM_PERIOD'                   => $data['BG_CLAIM_PERIOD'],
                                    'BG_CLAIMEDBY'                      => $data['BG_CLAIMEDBY'],
                                    'BG_CLAIM_NOTES'                    => $data['BG_CLAIM_NOTES'],
                                    'BG_CANCELEDBY'                     => $data['BG_CANCELEDBY'],
                                    'BG_CANCEL_NOTES'                   => $data['BG_CANCEL_NOTES'],
                                    'BG_UNDERLYING_DOC'                 => $data['BG_UNDERLYING_DOC'],
                                    'BG_INSURANCE_CODE'                 => $data['BG_INSURANCE_CODE'],
                                    'BG_BRANCH'                         => $data['BG_BRANCH'],
                                    // 'BG_PUBLISH'                        => $data['BG_PUBLISH'],
                                    'BG_PUBLISH'                        => $zf_filter_input->BG_PUBLISH_TXT,
                                    'IS_AMENDMENT'                      => 1,
                                    'CHANGE_TYPE'                       => $toa,

                                );
                                // echo '<pre>';
                                
                                // print_r($insertArr);die;
                                // $this->_db->insert('T_BANK_GUARANTEE', $insertArr);
                                $this->_db->insert('TEMP_BANK_GUARANTEE', $insertArr);

                                if ($data['COUNTER_WARRANTY_TYPE'] == '1') {

                                    foreach ($bgdatasplit as $ky => $vl) {
                                        $tmparrDetail = array(
                                            'BG_REG_NUMBER'         => $insertArr['BG_REG_NUMBER'],//update reza
                                            'ACCT'              => $vl['ACCT'],
                                            'BANK_CODE'         => $vl['BANK_CODE'],
                                            'NAME'              => $vl['NAME'],
                                            'AMOUNT'            => Application_Helper_General::convertDisplayMoney(rtrim($vl['AMOUNT'], ",")),
                                            'FLAG'              => $vl['FLAG']
                                        );

                                        // echo '<pre>';
                                        // print_r($tmparrDetail);die;
                                        // $this->_db->insert('T_BANK_GUARANTEE_SPLIT', $tmparrDetail);
                                        $this->_db->insert('TEMP_BANK_GUARANTEE_SPLIT', $tmparrDetail);
                                    }
                                }
                            }

                            $historyInsert = array(
                                'DATE_TIME'         => new Zend_Db_Expr("now()"),
                                'BG_REG_NUMBER'     => $insertArr['BG_REG_NUMBER'],
                                'CUST_ID'           => $this->_custIdLogin,
                                'USER_LOGIN'        => $this->_userIdLogin,
                                'HISTORY_STATUS'    => 2
                            );

                            $this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);

                            // echo '<pre>';
                            // print_r($insertArr);die;
                            // $where = array('BG_REG_NUMBER = ?' => $numb);
                            // $query = $this->_db->update ( "T_BANK_GUARANTEE", $updArr, $where ); 

                            $bg_number = $data['BG_NUMBER'];
                            $bg_reg_number = $insertArr['BG_REG_NUMBER'];
                            if ($toa == '1') {
                                Application_Helper_General::writeLog('CCBG', "Amendment Submission Change. BG Number: $bg_number. Register Number: [BG_REG_NUMBER - $bg_reg_number] ");
                            } else if ($toa == '2') {
                                Application_Helper_General::writeLog('CCBG', "Amendment Submission Reprint Draft. BG Number: $bg_number. Register Number: [BG_REG_NUMBER - $bg_reg_number] ");
                            }

                            $this->_db->commit();
                            $this->setbackURL('/' . $this->_request->getModuleName() . '/');
                            $this->_redirect('/notification/success');
                        } catch (Exception $e) {

                            echo '<pre>';
                            print_r($e);
                            die;
                            echo "<code>masuk3 = $data</code><BR>";
                            die;
                            $this->_db->rollBack();
                            $this->view->errorMsg = array(array($this->language->_('An Error Occured. Please Try Again.')));
                        }
                    } else {
                        // echo "<code>masuk4 = $data</code><BR>"; die;	
                        $error = $zf_filter_input->getMessages();
                        // $error = true;



                        //format error utk ditampilkan di view html 
                        $errorArray = null;
                        foreach ($error as $keyRoot => $rowError) {
                            foreach ($rowError as $errorString) {
                                $errorArray[$keyRoot] = $errorString;
                            }
                        }
                        $this->view->report_msg = $errorArray;
                        // echo '<pre>';
                        // print_r($errorArray);die;
                    }
                }


                $download = $this->_getParam('download');
                $BG_APPROVE_DOC = $this->_getParam('BG_APPROVE_DOC');
                //print_r($edit);die;
                if ($BG_APPROVE_DOC) {
                    $attahmentDestination = UPLOAD_PATH . '/document/bg/';
                    //var_dump($attahmentDestination.$data['BG_APPROVE_DOC']);die;
                    $this->_helper->download->file($data['BG_APPROVE_DOC'], $attahmentDestination . $data['BG_APPROVE_DOC']);
                }
            }
        }
    }

    public function Terbilang($nilai)
    {
        $huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
        if ($nilai == 0) {
            return "";
        } elseif ($nilai < 12 & $nilai != 0) {
            return "" . $huruf[$nilai];
        } elseif ($nilai < 20) {
            return $this->Terbilang($nilai - 10) . " Belas ";
        } elseif ($nilai < 100) {
            return $this->Terbilang($nilai / 10) . " Puluh " . $this->Terbilang($nilai % 10);
        } elseif ($nilai < 200) {
            return " Seratus " . $this->Terbilang($nilai - 100);
        } elseif ($nilai < 1000) {
            return $this->Terbilang($nilai / 100) . " Ratus " . $this->Terbilang($nilai % 100);
        } elseif ($nilai < 2000) {
            return " Seribu " . $this->Terbilang($nilai - 1000);
        } elseif ($nilai < 1000000) {
            return $this->Terbilang($nilai / 1000) . " Ribu " . $this->Terbilang($nilai % 1000);
        } elseif ($nilai < 1000000000) {
            return $this->Terbilang($nilai / 1000000) . " Juta " . $this->Terbilang($nilai % 1000000);
        } elseif ($nilai < 1000000000000) {
            return $this->Terbilang($nilai / 1000000000) . " Milyar " . $this->Terbilang($nilai % 1000000000);
        } elseif ($nilai < 100000000000000) {
            return $this->Terbilang($nilai / 1000000000000) . " Trilyun " . $this->Terbilang($nilai % 1000000000000);
        } elseif ($nilai <= 100000000000000) {
            return "Maaf Tidak Dapat di Prose Karena Jumlah nilai Terlalu Besar ";
        }
    }

    public function Terbilangen($nilai)
    {
        $huruf = array("", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", 'Nineteen');
        if ($nilai == 0) {
            return "";
        } elseif ($nilai < 20 & $nilai != 0) {
            return "" . $huruf[$nilai];
        } elseif ($nilai < 100) {
            return $this->Terbilangen($nilai / 10) . "ty " . $this->Terbilangen($nilai % 10);
        } elseif ($nilai < 200) {
            return " Seratus " . $this->Terbilangen($nilai - 100);
        } elseif ($nilai < 1000) {
            return $this->Terbilangen($nilai / 100) . " hundred " . $this->Terbilangen($nilai % 100);
        } elseif ($nilai < 2000) {
            return " Seribu " . $this->Terbilangen($nilai - 1000);
        } elseif ($nilai < 1000000) {
            return $this->Terbilangen($nilai / 1000) . " thausand " . $this->Terbilangen($nilai % 1000);
        } elseif ($nilai < 1000000000) {
            return $this->Terbilangen($nilai / 1000000) . " million " . $this->Terbilangen($nilai % 1000000);
        } elseif ($nilai < 1000000000000) {
            return $this->Terbilangen($nilai / 1000000000) . " Bilion " . $this->Terbilangen($nilai % 1000000000);
        } elseif ($nilai < 100000000000000) {
            return $this->Terbilangen($nilai / 1000000000000) . " Trillion " . $this->Terbilangen($nilai % 1000000000000);
        } elseif ($nilai <= 100000000000000) {
            return "Maaf Tidak Dapat di Prose Karena Jumlah nilai Terlalu Besar ";
        }
    }

    public function indodate($tanggal)
    {
        $bulan = array(
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);

        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun

        return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
    }

    public function generateTransactionID($branch, $bgtype)
    {

        // echo "<code>branch = $branch | bgtype = $bgtype  </code><BR>"; die;	
        $currentDate = date("Ymd");
        //$seqNumber   = Application_Helper_General::get_rand_id(4);
        $seqNumber   = mt_rand(1111, 9999);
        $trxId = $seqNumber . $branch . '01' . $bgtype;

        return $trxId;
    }

    public function inquirydepositoAction(){
		
		$app = Zend_Registry::get('config');
		$app = $app['app']['bankcode'];

		$this->_helper->viewRenderer->setNoRender();
        	$this->_helper->layout()->disableLayout();

        	$acct_no = $this->_getParam('acct_no');
		//echo $acct_no;
		
		$svcAccount = new Service_Account($acct_no, Application_Helper_General::getCurrNum('IDR'), $app, '1');
		$result = $svcAccount->inquiryDeposito('AB',TRUE);
		//var_dump($result);
		
		$data['data'] = false;
		is_array($result) ? $result :  $result = $data;
		echo json_encode($result);
	}

	public function inquiryaccountbalanceAction(){
		
		$app = Zend_Registry::get('config');
		$app = $app['app']['bankcode'];

		$this->_helper->viewRenderer->setNoRender();
        	$this->_helper->layout()->disableLayout();

        	$acct_no = $this->_getParam('acct_no');
		//echo $acct_no;
		
		$svcAccount = new Service_Account($acct_no, Application_Helper_General::getCurrNum('IDR'), $app, '1');
		$result = $svcAccount->inquiryAccountBalance('AB',TRUE);
		//var_dump($result);
		//die();
		$data['data'] = false;
		is_array($result) ? $result :  $result = $data;
		echo json_encode($result);
	}
	
	public function inquiryaccountinfoAction(){
		
		//integrate ke core
		  $this->_helper->viewRenderer->setNoRender();
		  $this->_helper->layout()->disableLayout();
		  $acct_no = $this->_getParam('acct_no');
		  $svcAccount = new Service_Account($acct_no, null);
		  $result = $svcAccount->inquiryAccontInfo('AI',TRUE);
		 
		  if($result['response_desc'] == 'Success') //00 = success
		  {
			$svcAccountCIF = new Service_Account($result['account_number'], null, null, null, null, $result['cif']);
			$result2 = $svcAccountCIF->inquiryCIFAccount();
			
			$filterBy = $result['account_number']; // or Finance etc.
			  $new = array_filter($result2['accounts'], function ($var) use ($filterBy) {
					return ($var['account_number'] == $filterBy);
			  });
			  
			  $singleArr = array();
			  foreach($new as $key=>$val){
				$singleArr = $val;  
			  }
		  }
			
			if($result['type_desc'] == 'Deposito'){
				$svcAccountDeposito = new Service_Account($result['account_number'], null, null, null, null, null);
				$result3 = $svcAccountDeposito->inquiryDeposito();
				
				
			}else{
				$svcAccountBalance = new Service_Account($result['account_number'], null, null, null, null, null);
				$result3 = $svcAccountBalance->inquiryAccountBalance();
			}
			$return = array_merge($result,$singleArr,$result3);
			$data['data'] = false;
			is_array($return) ? $return :  $return = $data;
			echo json_encode($return);
		  
		}
}
