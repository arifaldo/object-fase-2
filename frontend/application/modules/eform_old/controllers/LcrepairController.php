<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class eform_LcrepairController extends Application_Main 
{

    protected $_moduleDB = 'RTF'; // masih harus diganti
    
    public function indexAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->systemType = $system_type;

        $Settings = new Settings();

        $toc = $Settings->getSetting('ftemplate_bg');
        $this->view->toc = $toc;

        $conf = Zend_Registry::get('config');
        $this->_bankName = $conf['app']['bankname'];
        $this->view->masterbankname = $this->_bankName;

        $this->CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
        
        $selectcomp = $this->_db->select()
                             ->from(array('A' => 'M_CUSTOMER'),array('*'))
                           //  ->joinLeft(array('B' => 'M_COUNTRY'),'A.COUNTRY_CODE = B.COUNTRY_CODE',array('COUNTRY_NAME'))
                             ->where('A.CUST_ID ='.$this->_db->quote((string)$this->_custIdLogin))
                             ->query()->fetchAll();

        $this->view->compinfo = $selectcomp[0];
        
        $complist = $this->_db->fetchAll(
            $this->_db->select()
                 ->from(array('A' => 'M_USER'),array('CUST_ID'))
               
                 ->where("A.USER_ID = ? ", $this->_userIdLogin)
        );	
        // echo $complist;;die;
        // var_dump($complist);die;
        $comp = "'";
        // print_r($complist);die;
        foreach ($complist as $key => $value) {
            $comp .= "','".$value['CUST_ID']."','";
        }
        $comp .= "'";

        $acctlist = $this->_db->fetchAll(
            $this->_db->select()
                    ->from(array('A' => 'M_APIKEY'))
                    ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
                    ->join(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME', 'B.BANK_CODE'))
                    // ->where('A.ACCT_STATUS = ?','5')
                    ->where("A.CUST_ID IN (".$comp.")")
                    ->order('A.APIKEY_ID ASC')
                    // echo $acctlist;
        );

        $listbankArr = array();
        foreach ($acctlist as $key => $value) {
            $listbank = array(
                                "BANK_NAME" => $value['BANK_NAME'],
                                "BANK_CODE" => $value['BANK_CODE']
            );
            array_push($listbankArr, $listbank);
        }

        $listbankArr = array_unique($listbankArr, SORT_REGULAR);
        $this->view->listBank = $listbankArr;

        // var_dump($listbankArr);

        // echo "<pre>";
        // var_dump($acctlist);

        $account = array();
		foreach ($acctlist as $key => $value) {
			$account[$value['ID']][$value['FIELD']] = $value['VALUE'];
			$account[$value['ID']]['BANK_CODE'] = $value['BANK_CODE'];
			$account[$value['ID']]['SENDER_ID'] = $value['SENDER_ID'];
			$account[$value['ID']]['AUTH_USER'] = $value['AUTH_USER'];
			$account[$value['ID']]['AUTH_PASS'] = $value['AUTH_PASS'];
			$account[$value['ID']]['BANK_NAME'] = $value['BANK_NAME'];
			$account[$value['ID']]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
		}
		// echo "<pre>";
		// var_dump($account);die;
		// $acct = array();
		$i = 0;
		foreach ($account as $key => $value) {

			$acct[$i]['ACCT_NO'] = $value['account_number'];
			$acct[$i]['ACCT_BANK'] = $value['BANK_NAME'];
			$acct[$i]['BANK_CODE'] = $value['BANK_CODE'];
			$acct[$i]['BANK_NAME'] = $value['BANK_NAME'];
			$acct[$i]['ACCT_NAME'] = $value['account_name'];
			$i++;
        }
    
        $this->view->sourceAcct = $acct;
        
        $accData = $this->_db->select()
					->from('M_CUSTOMER_ACCT')
					->where('CUST_ID IN ('.$comp.')');

        $accData = $this->_db->fetchAll($accData);
        foreach($accData as $key => $value){
            $accdata[$i]['ACCT_NO'] = $value['ACCT_NO'];
            $accdata[$i]['ACCT_NAME'] = $value['ACCT_NAME'];
            $accdata[$i]['ACCT_BANK'] = $this->_bankName;
        }
        $this->view->AccArr = $accdata;

        // $paramac = array(
		// 	'CCY_IN' => 'IDR'
		// );
		// $AccArr 	  				= $this->CustomerUser->getAccounts($paramac);
        // $this->view->AccArr 		= $AccArr;
        $maxFileSize = "1024"; //"1024000"; //1024
        //~ $fileExt = "csv,dbf";
        $fileExt = "jpeg,jpg,pdf";
        if($this->_request->isPost() )
        {
            
            $attahmentDestination   = UPLOAD_PATH . '/document/submit/';
            $errorRemark            = null;
            $adapter                = new Zend_File_Transfer_Adapter_Http();
            $datas = $this->_request->getParams();
            
            $sourceFileName = trim($adapter->getFileName());
             
            if($sourceFileName == null){
                //die('here');
                $sourceFileName = null;
                $fileType = null;
            }
            else
            {

                $sourceFileName = trim(substr(basename($adapter->getFileName()), 0));
                if($_FILES["uploadSource"]["type"])
                {
                    $adapter->setDestination($attahmentDestination);
                    $fileType               = $adapter->getMimeType();
                    $size                   = $_FILES["document"]["size"];
                }
                else{
                    $fileType = null;
                    $size = null;
                }
            }
            $paramsName['sourceFileName']   = $sourceFileName;
            //print_r($sourceFileName);die;
            $fileTypeMessage = explode('/',$fileType);
            if (isset($fileTypeMessage[1])){
                $fileType =  $fileTypeMessage[1];
                $extensionValidator = new Zend_Validate_File_Extension(array(false, $fileExt));
                $extensionValidator->setMessage("Extension file must be *.jpg /.jpeg / .pdf");

                $size           = number_format($size);
                $sizeTampil     = $size/1000;
                //$sizeValidator    = new Zend_Validate_File_Size(array('min' => 1, 'max' => $maxFileSize));
                // $sizeValidator->setMessage("File size is exceeding 1024 Kb, uploaded file is $sizeTampil byte(s)");

                $adapter->setValidators(array($extensionValidator));

                if($adapter->isValid())
                {
                    $date = date("dmy");
                    $time = date("his");

                    $newFileName = $date."_".$time."_".$this->_custIdLogin."_". $sourceFileName;
                    $adapter->addFilter('Rename', $newFileName);

                    if($adapter->receive())
                    {


                        $filedata = $attahmentDestination.$newFileName;
                        // var_dump($filedata);
                        if (file_exists($filedata)) {
                        exec("clamdscan --fdpass ".$filedata, $out ,$int);
                            
                        // if()
                            // var_dump($int);
                            // var_dump($out);
                            // die;
                        if($int == 1){

                            unlink($filedata);
                            //die('here');
                            $this->view->error = true;
                            $errors = array(array('FileType'=>'Error: An infected file. Please upload other file'));
                            $this->view->errorMsg = $errors;


                        }else{
                            $datas['fileName'] = $newFileName;
                            $sessionNamespace = new Zend_Session_Namespace('lc');
                            $sessionNamespace->content = $datas;
                            $this->_redirect('/eform/lc/confirm');
                            
                            
                        }
                        
                        
                        }
                    }
                    
                }else{
                    $this->view->error = true;
                    $errors = array($adapter->getMessages());
                    $this->view->errorMsg = $errors;
                }
                
            }else{
                $sessionNamespace = new Zend_Session_Namespace('lc');
                $sessionNamespace->content = $datas;
                $this->_redirect('/eform/lc/confirm');
            }            
            
        }

        
        // if($this->_request->isPost() )
        // {
        //     $datas = $this->_request->getParams();

        //     // without upload file
        //     $sessionNamespace = new Zend_Session_Namespace('lc');
        //     $sessionNamespace->content = $datas;
        //     $this->_redirect('/eform/lc/confirm');
            
        // }

    }

    public function confirmAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->systemType = $system_type;

        
         $conf = Zend_Registry::get('config');
        $this->_bankName = $conf['app']['bankname'];
        $this->view->masterbankname = $this->_bankName;
        
        
        $selectcomp = $this->_db->select()
                             ->from(array('A' => 'M_CUSTOMER'),array('*'))
                           //  ->joinLeft(array('B' => 'M_COUNTRY'),'A.COUNTRY_CODE = B.COUNTRY_CODE',array('COUNTRY_NAME'))
                             ->where('A.CUST_ID ='.$this->_db->quote((string)$this->_custIdLogin))
                             ->query()->fetchAll();

        $this->view->compinfo = $selectcomp[0];

        $complist = $this->_db->fetchAll(
            $this->_db->select()
                 ->from(array('A' => 'M_USER'),array('CUST_ID'))
               
                 ->where("A.USER_ID = ? ", $this->_userIdLogin)
        );	
        // echo $complist;;die;
        // var_dump($complist);die;
        $comp = "'";
        // print_r($complist);die;
        foreach ($complist as $key => $value) {
            $comp .= "','".$value['CUST_ID']."','";
        }
        $comp .= "'";

        $acctlist = $this->_db->fetchAll(
            $this->_db->select()
                    ->from(array('A' => 'M_APIKEY'))
                    ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
                    ->join(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME', 'B.BANK_CODE'))
                    // ->where('A.ACCT_STATUS = ?','5')
                    ->where("A.CUST_ID IN (".$comp.")")
                    ->order('A.APIKEY_ID ASC')
                    // echo $acctlist;
        );

        $listbankArr = array();
        foreach ($acctlist as $key => $value) {
            $listbank = array(
                                "BANK_NAME" => $value['BANK_NAME'],
                                "BANK_CODE" => $value['BANK_CODE']
            );
            array_push($listbankArr, $listbank);
        }

        $listbankArr = array_unique($listbankArr, SORT_REGULAR);

        $sessionNamespace = new Zend_Session_Namespace('lc');
        $data = $sessionNamespace->content;

        // var_dump($data);die;
        
        $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

        
            $param = array('CCY_IN' => 'IDR','ACCT_NO'=>$data['chargetoAccount']);
            $AccArr = $CustomerUser->getAccounts($param);
        
         
        if(!empty($AccArr)){
            $this->view->src_name = $AccArr['0']['ACCT_NAME'];
        }

        if(!empty($data['chkrequired'])){
            foreach($data['chkrequired'] as $key => $val){
                $str = 'checkp'.$val;
                //var_dump($str);
                $this->view->$str =  'checked';
            }
        }else{
            $this->view->notcheck = '1';
        }
        
        foreach($data as $key => $val){
            
            if($key != 'language'){
                $this->view->$key = $val;
            }
        }
       
        $this->view->expiryDate = Application_Helper_General::convertDate($data['expiryDate'],$this->view->viewDateFormat,$this->view->defaultDateFormat);
        $this->view->lastShipment = Application_Helper_General::convertDate($data['lastShipment'],$this->view->viewDateFormat,$this->view->defaultDateFormat);
        
        $arrcredit_type = array(
            1 => 'Letter of Credit',
            2 => 'Surat Kredit Berdokumen Dalam Negeri'
        );
        
        $this->view->credit_type_text = $arrcredit_type[$data['credit_type']];
        
        foreach($listbankArr as $key => $value){
            $arrconfirming_bank[$value['BANK_CODE']] = $value['BANK_NAME'];
        }
        // var_dump($arrconfirming_bank);
        $this->view->confirming_bank_text = $arrconfirming_bank[$data['confirming_bank']];
        
        $arrtransferable = array(
            1 => 'Transferable',
            2 => 'Non Transferable'
        );
        
        $this->view->transferable_text = $arrtransferable[$data['transferable']];
        
        $arrconfirmation = array(
            1 => 'Without',
            2 => 'May Add',
            3 => 'Confirmed'
        );
        
        $this->view->confirmation_text = $arrconfirmation[$data['confirmation']];
        
        $arradvBank = array(
            1 => 'PHILLIP BANK - SWIFT: HDSBKHHPP',
        );
        
        $this->view->advBank_text = $arradvBank[$data['advising_bank']];
        
        $arrlcBy = array(
            1 => 'Negotation',
            2 => 'Acceptance',
            3 => 'Confirmed',
            4 => 'Payment'
        );
        
        $this->view->lcBy_text = $arrlcBy[$data['lcBy']];
        
        $arrdifered_payment = array(
            1 => 'delivery of goods but not exceeding the expiry date',
            2 => 'proof of delivery goods but not exceeding the expired date'
        );
        
        $this->view->difered_payment_text = $arrdifered_payment[$data['difered_payment']];
        
        $arrtrade_terms = array(
            0 => '---',
            1 => 'EXW',
            2 => 'FCA',
            3 => 'CPT',
            4 => 'CIP',
            5 => 'DAT',
            6 => 'DAP',
            7 => 'DDP',
            8 => 'FAS',
            9 => 'FOB',
            10 => 'CFR',
            11 => 'CIF'
        );
        
        $this->view->trade_terms_text = $arrtrade_terms[$data['trade_terms']];
        
        $arrtenor = array(
            1 => 'Sight',
            2 => 'Usance',
            3 => 'Other'
        );
        
        $this->view->tenor_text = $arrtenor[$data['tenor']];
        
        $arrconfirmationCharge = array(
            1 => 'Not Applicable'            
        );
        
        $this->view->confirmationCharge_text = $arrconfirmationCharge[$data['confirmationCharge']];
        
        $arrcharge = array(
            1 => 'Applicant',
            2 => 'Beneficiary',
        );
        
        $this->view->issuance_text = $arrcharge[$data['issuance']];
        $this->view->discrepancy_text = $arrcharge[$data['discrepancy']];
        $this->view->chargesOutside_text = $arrcharge[$data['chargesOutside']];

        if($this->_request->isPost() )
        {
            $usage = '';
            foreach($data['chkrequired'] as $val){
                $usage .= $val.',';
            }
            $usage = substr($usage, 0, -1);
            
            try
            {
                if($data['bank_amount'] == ''){
                    $data['bank_amount'] = 0;
                }

                if($data['unitPrice'] == ''){
                    $data['unitPrice'] = 0;
                }

                if($data['toleranceMin'] == ''){
                    $data['toleranceMin'] = 0;
                }

                if($data['toleranceMax'] == ''){
                    $data['toleranceMax'] = 0;
                }

                $this->_db->beginTransaction();
                $insertArr = array(
                    'LC_REG_NUMBER'             => $this->generateTransactionID(),
                    'LC_CUST'                   => $this->_custIdLogin,
                    'LC_BENEF_NAME'             => $data['benef_name'],
                    'LC_BENEF_ADDRESS'          => $data['benef_address'],
                    'LC_CREDIT_TYPE'            => $data['credit_type'],
                    'LC_TRANSFERABLE'           => $data['transferable'],
                    'LC_CONFIRMATION'           => $data['confirmation'],
                    'LC_CCY'                    => $data['ccyid'],
                    'LC_AMOUNT'                 => Application_Helper_General::convertDisplayMoney($data['bank_amount']),
                    //tolerance
                    'LC_TOLERANCE_TYPE'         => $data['tolerance'],
                    'LC_TOLERANCE_MIN'          => Application_Helper_General::convertDisplayMoney($data['toleranceMin']),
                    'LC_TOLERANCE_MAX'          => Application_Helper_General::convertDisplayMoney($data['toleranceMax']),
                    //
                    'LC_EXPDATE'                => $data['expiryDate'],
                    'LC_EXP_PLACE'              => $data['expiryPlace'],
                    'LC_LASTSHIP_DATE'          => $data['lastShipment'],
                    'LC_SHIP_PERIOD'            => $data['shipmentPeriod'],
                    //tenor
                    'LC_TENOR'                  => $data['tenor'],
                    'LC_TENORUSANCE_FROM'       => $data['usance'],
                    'LC_TENORUSANCE_TO'         => $data['usance2'],
                    'LC_TENORTRANSHIP'          => $data['trans_shipment'],
                    'LC_TENORPARTIAL'           => $data['partial_shipment'],
                    //
                    'LC_ADVISING'               => $data['advising_bank'],
                    'LC_CONFIRMING'             => $data['confirming_bank'],
                    'LC_PLACETALK'              => $data['placeCharge'],
                    'LC_PORTLOAD'               => $data['portLoading'],
                    'LC_PLACEFINAL'             => $data['placeDestination'],
                    'LC_PORTDISCHARGE'          => $data['portofDischarge'],
                    // availtype
                    'LC_AVAIL_TYPE'             => $data['availablewith'],
                    'LC_AVAIL_OTHER'            => $data['otherAvailable'],
                    //
                    'LC_BY'                     => $data['lcBy'],
                    'LC_DOC_PERIOD'             => $data['documentPeriod'],
                    'LC_DOC_AFTER'              => $data['difered_payment'],
                    'LC_DESC_GOOD'              => $data['descGoods'],
                    'LC_CONTACT'                => $data['contractNo'],
                    'LC_HSCODE'                 => $data['hsCode'],
                    'LC_COUNTRY'                => $data['countryOrigin'],
                    'LC_VOLUME'                 => $data['goodsVolume'],
                    'LC_CCYUNIT'                => $data['ccyid2'],
                    'LC_UNITAMOUNT'             => Application_Helper_General::convertDisplayMoney($data['unitPrice']),
                    //trade
                    'LC_TRADETYPE'              => $data['trade'],
                    'LC_TRADEIN'                => $data['trade_terms'],
                    'LC_TRADEOTHER'             => $data['tradeOthers'],
                    //
                    'LC_ADD_CONDITION'          => $data['comment'],
                    'LC_ISSUANCE'               => $data['issuance'],
                    'LC_DISCREPANCY'            => $data['discrepancy'],
                    'LC_CONFIRM'                => $data['confirmationCharge'],
                    'LC_CHARGESOUTSIDE'         => $data['chargesOutside'],
                    'LC_CHARGEACCOUNT'          => $data['chargetoAccount'],
                    'LC_DOCREQ'                 => $usage,
                    'LC_STATUS'                 => 1,
                    'LC_FILE'                   => $data['fileName'],
                    'LC_CREATED'                => new Zend_Db_Expr('now()'),
                    'LC_CREATEDBY'              => $this->_userIdLogin

                );

                $this->_db->insert('T_LC', $insertArr);

                $docreq = (explode(",",$usage));

                if (in_array('1', $docreq, TRUE)){
                    $arrDetail = array(
                        'LC_NUMBER'                 => $insertArr['LC_REG_NUMBER'],
                        'DOCREQ_ID'                 => '1',
                        'DOCREQ_VAL'                => $data['docReq1'],
                        'DOCREQ_INDEX'              => '1'
                    );
                    $this->_db->insert('T_LC_DETAIL', $arrDetail);

                    $arrDetail = array(
                        'LC_NUMBER'                 => $insertArr['LC_REG_NUMBER'],
                        'DOCREQ_ID'                 => '1',
                        'DOCREQ_VAL'                => $data['docReq2'],
                        'DOCREQ_INDEX'              => '2'
                    );
                    $this->_db->insert('T_LC_DETAIL', $arrDetail);
                }

                if (in_array('2', $docreq, TRUE)){
                    $arrDetail = array(
                        'LC_NUMBER'                 => $insertArr['LC_REG_NUMBER'],
                        'DOCREQ_ID'                 => '2',
                        'DOCREQ_VAL'                => $data['docReq3'],
                        'DOCREQ_INDEX'              => '3'
                    );
                    $this->_db->insert('T_LC_DETAIL', $arrDetail);

                    $arrDetail = array(
                        'LC_NUMBER'                 => $insertArr['LC_REG_NUMBER'],
                        'DOCREQ_ID'                 => '2',
                        'DOCREQ_VAL'                => $data['docReq4'],
                        'DOCREQ_INDEX'              => '4'
                    );
                    $this->_db->insert('T_LC_DETAIL', $arrDetail);
                }

                if (in_array('3', $docreq, TRUE)){
                    $arrDetail = array(
                        'LC_NUMBER'                 => $insertArr['LC_REG_NUMBER'],
                        'DOCREQ_ID'                 => '2',
                        'DOCREQ_VAL'                => $data['docReq5'],
                        'DOCREQ_INDEX'              => '5'
                    );
                    $this->_db->insert('T_LC_DETAIL', $arrDetail);

                    $arrDetail = array(
                        'LC_NUMBER'                 => $insertArr['LC_REG_NUMBER'],
                        'DOCREQ_ID'                 => '3',
                        'DOCREQ_VAL'                => $data['docReq6'],
                        'DOCREQ_INDEX'              => '6'
                    );
                    $this->_db->insert('T_LC_DETAIL', $arrDetail);

                    $arrDetail = array(
                        'LC_NUMBER'                 => $insertArr['LC_REG_NUMBER'],
                        'DOCREQ_ID'                 => '3',
                        'DOCREQ_VAL'                => $data['docReq7'],
                        'DOCREQ_INDEX'              => '7'
                    );
                    $this->_db->insert('T_LC_DETAIL', $arrDetail);
                    
                    $arrDetail = array(
                        'LC_NUMBER'                 => $insertArr['LC_REG_NUMBER'],
                        'DOCREQ_ID'                 => '3',
                        'DOCREQ_VAL'                => $data['docReq8'],
                        'DOCREQ_INDEX'              => '8'
                    );
                    $this->_db->insert('T_LC_DETAIL', $arrDetail);
                }
                    
                if (in_array('4', $docreq, TRUE)){
                    $arrDetail = array(
                        'LC_NUMBER'                 => $insertArr['LC_REG_NUMBER'],
                        'DOCREQ_ID'                 => '3',
                        'DOCREQ_VAL'                => $data['docReq9'],
                        'DOCREQ_INDEX'              => '9'
                    );
                    $this->_db->insert('T_LC_DETAIL', $arrDetail);

                    $arrDetail = array(
                        'LC_NUMBER'                 => $insertArr['LC_REG_NUMBER'],
                        'DOCREQ_ID'                 => '3',
                        'DOCREQ_VAL'                => $data['docReq10'],
                        'DOCREQ_INDEX'              => '10'
                    );
                    $this->_db->insert('T_LC_DETAIL', $arrDetail);
                    
                    $arrDetail = array(
                        'LC_NUMBER'                 => $insertArr['LC_REG_NUMBER'],
                        'DOCREQ_ID'                 => '3',
                        'DOCREQ_VAL'                => $data['docReq11'],
                        'DOCREQ_INDEX'              => '11'
                    );
                    $this->_db->insert('T_LC_DETAIL', $arrDetail);
                    
                    $arrDetail = array(
                        'LC_NUMBER'                 => $insertArr['LC_REG_NUMBER'],
                        'DOCREQ_ID'                 => '3',
                        'DOCREQ_VAL'                => $data['docReq12'],
                        'DOCREQ_INDEX'              => '12'
                    );
                    $this->_db->insert('T_LC_DETAIL', $arrDetail);
                    
                    $arrDetail = array(
                        'LC_NUMBER'                 => $insertArr['LC_REG_NUMBER'],
                        'DOCREQ_ID'                 => '3',
                        'DOCREQ_VAL'                => $data['docReq13'],
                        'DOCREQ_INDEX'              => '13'
                    );
                    $this->_db->insert('T_LC_DETAIL', $arrDetail);
                    
                    $arrDetail = array(
                        'LC_NUMBER'                 => $insertArr['LC_REG_NUMBER'],
                        'DOCREQ_ID'                 => '3',
                        'DOCREQ_VAL'                => $data['docReq14'],
                        'DOCREQ_INDEX'              => '14'
                    );
                    $this->_db->insert('T_LC_DETAIL', $arrDetail);
                    
                    $arrDetail = array(
                        'LC_NUMBER'                 => $insertArr['LC_REG_NUMBER'],
                        'DOCREQ_ID'                 => '3',
                        'DOCREQ_VAL'                => $data['docReq15'],
                        'DOCREQ_INDEX'              => '15'
                    );
                    $this->_db->insert('T_LC_DETAIL', $arrDetail);
                    
                    $arrDetail = array(
                        'LC_NUMBER'                 => $insertArr['LC_REG_NUMBER'],
                        'DOCREQ_ID'                 => '3',
                        'DOCREQ_VAL'                => $data['docReq16'],
                        'DOCREQ_INDEX'              => '16'
                    );
                    $this->_db->insert('T_LC_DETAIL', $arrDetail);

                    $arrDetail = array(
                        'LC_NUMBER'                 => $insertArr['LC_REG_NUMBER'],
                        'DOCREQ_ID'                 => '3',
                        'DOCREQ_VAL'                => $data['docReq17'],
                        'DOCREQ_INDEX'              => '17'
                    );
                    $this->_db->insert('T_LC_DETAIL', $arrDetail);
                }

                $this->_db->commit();

                //Application_Helper_General::writeLog('DELI',"Successfully upload ".$newFileName);

                $this->setbackURL('/eformworkflow/review');
                $this->_redirect('/notification/success');

            }catch(Exception $e)
            {
                var_dump($e);die;
                $this->_db->rollBack();
                //Application_Log_GeneralLog::technicalLog($e);
            }
        }
        
    }

    public function generateTransactionID(){

        $currentDate = date("Ymd");
        $seqNumber   = strtoupper(Application_Helper_General::str_rand(6));
        $trxId = 'LCREG'.$currentDate.$seqNumber;

        return $trxId;
    }

}