<?php

Class userdailylimit_Model_Userdailylimit extends Application_Main
{

    protected $_dailyLimitData = array('USER_LOGIN'        => null,
                                       'CUST_ID'           => null,
  	                                   'CCY_ID'            => null,
                                       'DAILYLIMIT'        => null,
                                       'DAILYLIMIT_STATUS' => null
        				              );

   
    public function getAllCustomer()
    { 
       $select = $this->_db->select()
                           ->from('M_CUSTOMER',array('CUST_ID'))
                           ->query()->fetchAll();
	     return $select;
    }

    public function getAllUser($cust_id)
    { 
       $select = $this->_db->select()
                           ->from('M_USER',array('USER_ID'))
                           ->where('CUST_ID = ?', $cust_id)
                           ->query()->fetchAll();
       return $select;
    }

   
        				              
    public function insertTempDailyLimit($change_id,$daily_limit_data) 
    {  
       /* Zend_Debug::dump($change_id);
		  die;*/
    
       $content = array('CHANGES_ID'        => $change_id,
						'CUST_ID'           => $daily_limit_data['CUST_ID'],
  	                 	'USER_LOGIN'        => $daily_limit_data['USER_LOGIN'],
		             	'CCY_ID'            => $daily_limit_data['CCY_ID'],
                        'DAILYLIMIT'        => $daily_limit_data['DAILYLIMIT'],
                        'DAILYLIMIT_STATUS' => $daily_limit_data['DAILYLIMIT_STATUS'],
                        'SUGGESTED'         => $daily_limit_data['SUGGESTED'],
                        'SUGGESTEDBY'       => $daily_limit_data['SUGGESTEDBY'],
       
                      
                        'CREATED'       => $daily_limit_data['CREATED'],
                        'CREATEDBY'     => $daily_limit_data['CREATEDBY'],
                        'UPDATED'       => $daily_limit_data['UPDATED'],
                        'UPDATEDBY'     => $daily_limit_data['UPDATEDBY'],
                       );
					
        //sif(isset($user_data['TOKEN_SERIALNO']))$content = array_merge($content,array('TOKEN_SERIALNO' => $user_data['TOKEN_SERIALNO']));
	    $this->_db->insert('TEMP_DAILYLIMIT',$content);
	    
  }
  
   public function updateTempDailyLimit($changes_id,$daily_limit_data) 
   {
        $content = array(
						//'CUST_ID'           => $daily_limit_data['CUST_ID'],
  	                 	//'USER_LOGIN'        => $daily_limit_data['USER_LOGIN'],
		             	//'CCY_ID'            => $daily_limit_data['CCY_ID'],
                        'DAILYLIMIT'          => $daily_limit_data['DAILYLIMIT'],
                        //'DAILYLIMIT_STATUS' => $daily_limit_data['DAILYLIMIT_STATUS'],
                       // 'SUGGESTED'         => $daily_limit_data['SUGGESTED'],
                       // 'SUGGESTEDBY'       => $daily_limit_data['SUGGESTEDBY'],
       
                      
                       // 'CREATED'       => $daily_limit_data['CREATED'],
                        //'CREATEDBY'     => $daily_limit_data['CREATEDBY'],
                        //'UPDATED'       => $daily_limit_data['UPDATED'],
                       // 'UPDATEDBY'     => $daily_limit_data['UPDATEDBY'],
					   );
					
        $whereArr = array('CHANGES_ID = ?' => $changes_id);
		$update = $this->_db->update('TEMP_DAILYLIMIT',$content,$whereArr);
	    
  }
  
  
  
   public function getTempUserDailyLimit($changes_id)
   {
       $select = $this->_db->select()
                           ->from(array('T' =>'TEMP_DAILYLIMIT'))
                          // ->join(array('G' => 'T_GLOBAL_CHANGES'), 'G.CHANGES_ID = T.CHANGES_ID', array('CHANGES_ID','CHANGES_TYPE','CREATED','CREATED_BY','CHANGES_STATUS'))
      	                   ->where('T.CHANGES_ID = ?', $changes_id)
      	                   ->query()->fetch();
       return $select;
   }

   public function getCcyList()
   {
      return $ccyArr = $this->_db->select()
                ->from(array('M_MINAMT_CCY'), array('CCY_ID', 'CCY_NUM', 'DESCRIPTION'))
                //->where("M_MINAMT_CCY.CCY_STATUS=?", $this->_masterStatus['code']['active'])
                ->where('CCY_ID IN (?)', array('IDR','USD'))
                ->order('CCY_ID ASC')
                ->query()
                ->fetchAll();
  }
    
}




