<?php

require_once 'Zend/Controller/Action.php';

class userdailylimit_IndexController extends userdailylimit_Model_Userdailylimit 
{

  public function indexAction() 
  {
    $this->_helper->layout()->setLayout('newlayout');
    //pengaturan url untuk button back
    $this->setbackURL('/'.$this->_request->getModuleName().'/index/'); 

    $setting = new Settings();          
  
  if(!$this->view->hasPrivilege('VDLU')){
    
      if($this->view->hasPrivilege('VMLU')){
        $this->_redirect('/useropenlimit');
      }else{
        $this->_redirect('/authorizationacl/index/index');
      }
    }

    $enc_pass = $setting->getSetting('enc_pass');
    $enc_salt = $setting->getSetting('enc_salt');
    $system_type = $setting->getSetting('system_type');
    $this->view->system_type = $system_type;
    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');      
    $pw_hash = md5($enc_salt.$enc_pass);
    $rand = $this->_userIdLogin.date('dHis').$pw_hash; 
    $sessionNamespace->token  = $rand;
    $this->view->token = $sessionNamespace->token; 
    
    $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
    if(count($temp)>1){
          if($temp[0]=='F' || $temp[0]=='S'){
            if($temp[0]=='F')
              $this->view->error = 1;
            else
              $this->view->success = 1;
            $msg = ''; unset($temp[0]);
            foreach($temp as $value)
            {
              if(!is_array($value))
                $value = array($value);
              $msg .= $this->view->formErrors($value);
            }
            $this->view->user_msg = $msg;
        } 
    }
  
    $this->view->CCYData = $this->getCcy();
    
    // $custArr  = Application_Helper_Array::listArray($this->getAllCustomer(),'CUST_ID','CUST_ID');
   //  $custArr  = array_merge(array(''=>'-- '.$this->language->_('Any Value').' --'),$custArr);
   //  $this->view->custArr = $custArr;

    $cust_id = $this->_custIdLogin;

    $userArr  = Application_Helper_Array::listArray($this->getAllUser($cust_id),'USER_ID','USER_ID');
    $userArr  = array_merge(array(''=>'-- '.$this->language->_('Any Value').' --'),$userArr);
    $this->view->userArr = $userArr;
    
    //$statusArr = Application_Helper_Array::globalvarArray($this->_masteruserStatus);
    //$statusArr = array(''=>'-- '.$this->language->_('Any Value').' --','1'=>$this->language->_('Approved'),'3'=>$this->language->_('Deleted'));
    //$this->view->statusArr = $statusArr;
    
    
    //$this->view->signArr = array('EQ'=>'=', 'NE'=>'!=', 'LT'=>'<', 'GT'=>'>', 'LE'=>'<=', 'GE'=>'>=');
    
    //format display date
    $this->view->dateDisplayFormat = $this->_dateDisplayFormat;
    
    
    $fields = array(
                    
                    'userId'        => array('field'     => 'd.USER_LOGIN',
                                             'label'     => $this->language->_('User ID'),
                                             'sortable'  => true),

                    'userName'        => array('field'     => 'USER_FULLNAME',
                                             'label'     => $this->language->_('User Name'),
                                             'sortable'  => true),
    
                    'ccy'         => array('field'     => 'CCY_ID',
                                             'label'     => $this->language->_('Currency'),
                                             'sortable'  => true),
    
                    'dailyLimit'    => array('field'     => 'DAILYLIMIT',
                                             'label'     => $this->language->_('Max Limit'),
                                             'sortable'  => true),
    
                    'status'        => array('field'     => 'DAILYLIMIT_STATUS',
                                             'label'     => $this->language->_('Status'),
                                             'sortable'  => true),
    
                    'latestSuggestion'     => array('field'  => 'SUGGESTED',
                                               'label'       => $this->language->_('Latest Suggestion'),
                                               'sortable'    => true),
    
                    'latestSuggestor'   => array('field'  => 'SUGGESTEDBY',
                                               'label'    => $this->language->_('Latest Suggester'),
                                               'sortable' => true),
    
                    'latestApproval'    => array('field'  => 'UPDATED',
                                               'label'    => $this->language->_('Latest Approval'),
                                               'sortable' => true),
    
                    'latestApprover'  => array('field'    => 'UPDATEDBY',
                                               'label'    => $this->language->_('Latest Approver'),
                                               'sortable' => true)
                    );

    
    $filterlist = array('USER_ID','CCY_ID','USER_SUGGESTED','USER_UPDATED');
    
    $this->view->filterlist = $filterlist;

    $csv	 = $this->_getParam('csv');
    $page = $this->_getParam('page');
    $page = (Zend_Validate::is($page,'Digits'))? $page : 1;
    $sortBy = $this->_getParam('sortby');
    $sortBy = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
    $sortDir = $this->_getParam('sortdir');
    $sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
  
    /*echo $sortBy . '  '. $sortDir;
    die;*/

    $filterArr = array('filter'      => array('StripTags','StringTrim'),
                       'USER_ID'    => array('StripTags','StringTrim','StringToUpper'),
                       'CCY_ID'  => array('StripTags','StringTrim','StringToUpper'),
                       'USER_SUGGESTED' => array('StripTags','StringTrim'),
                       'USER_UPDATED' => array('StripTags','StringTrim'),
                       'USER_SUGGESTED_END' => array('StripTags','StringTrim'),
                       'USER_UPDATED_END' => array('StripTags','StringTrim')
                      );
     
    $dataParam = array("USER_ID","CCY_ID",);
    $dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{

			 
			 
			if(!empty($this->_request->getParam('whereco'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('whereco') as $key => $value) {
						if($dtParam==$value){
							if(!empty($dataParamValue[$dtParam])){
								$dataval[$key] = $dataParamValue[$dtParam] .','.$dataval[$key]; 
							}
							
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}

			}

			

			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
    } 
    
      if(!empty($this->_request->getParam('updatedate'))){
        $createarr = $this->_request->getParam('updatedate');
          $dataParamValue['USER_UPDATED'] = $createarr[0];
          $dataParamValue['USER_UPDATED_END'] = $createarr[1];
      }
      

      if(!empty($this->_request->getParam('sugestdate'))){
        $efdatearr = $this->_request->getParam('sugestdate');
          $dataParamValue['USER_SUGGESTED'] = $efdatearr[0];
          $dataParamValue['USER_SUGGESTED_END'] = $efdatearr[1];
      }

      // print_r($dataParamValue);die;
    $options = array('allowEmpty' => true);
    $validators = array(
      'USER_ID'		=> array(),
      'CCY_ID' 		=> array(),
      'USER_SUGGESTED'   	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
      'USER_SUGGESTED_END'     	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
      'USER_SUGGESTED'     	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
      'USER_UPDATED_END'     	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
      
    );

    $zf_filter = new Zend_Filter_Input($filterArr,$validators,$dataParamValue,$options);
    // $filter    = $zf_filter->getEscaped('filter');
    $filter     = $this->_getParam('filter');
    
    
    $this->view->currentPage = $page;
    $this->view->sortBy = $sortBy;
    $this->view->sortDir = $sortDir;

    $select = $this->_db->select()
          ->from(array('d' => 'M_DAILYLIMIT'))
          ->join(array('u' => 'M_USER'),'d.CUST_ID = u.CUST_ID AND d.USER_LOGIN = u.USER_ID',array('USER_FULLNAME'))
          ->where('d.DAILYLIMIT_STATUS != ?', '3')
          ->where('d.CUST_ID = ?', $cust_id);
// print_r($filter);die;
    if($filter == $this->language->_('Set Filter'))
    {
      $fuser_id    = html_entity_decode($zf_filter->getEscaped('USER_ID'));

      $fccy_id     = html_entity_decode($zf_filter->getEscaped('CCY_ID'));
      $status      = html_entity_decode($zf_filter->getEscaped('status'));
      
      $latestSuggestionFrom   = html_entity_decode($zf_filter->getEscaped('USER_SUGGESTED'));
      $latestSuggestionTo     = html_entity_decode($zf_filter->getEscaped('USER_SUGGESTED_END'));
      $latestApprovalFrom     = html_entity_decode($zf_filter->getEscaped('USER_UPDATED'));
      $latestApprovalTo       = html_entity_decode($zf_filter->getEscaped('USER_UPDATED_END'));
    
      //konversi date agar dapat dibandingkan
      $latestSuggestionFrom   = (Zend_Date::isDate($latestSuggestionFrom,$this->_dateDisplayFormat))?
                     new Zend_Date($latestSuggestionFrom,$this->_dateDisplayFormat):
                     false;
      
      $latestSuggestionTo     = (Zend_Date::isDate($latestSuggestionTo,$this->_dateDisplayFormat))?
                     new Zend_Date($latestSuggestionTo,$this->_dateDisplayFormat):
                     false;
                                 
      $latestApprovalFrom     = (Zend_Date::isDate($latestApprovalFrom,$this->_dateDisplayFormat))?
                     new Zend_Date($latestApprovalFrom,$this->_dateDisplayFormat):
                     false;

      $latestApprovalTo       = (Zend_Date::isDate($latestApprovalTo,$this->_dateDisplayFormat))?
                     new Zend_Date($latestApprovalTo,$this->_dateDisplayFormat):
                     false;   
    
      $this->view->fuser_id   = $fuser_id;
      $this->view->fccy_id    = $fccy_id;
      $this->view->status     = $status;

      // if($fuser_id)    $select->where('UPPER(d.USER_LOGIN) LIKE '.$this->_db->quote('%'.$fuser_id.'%'));
      // if($fccy_id)     $select->where('UPPER(d.CCY_ID) LIKE '.$this->_db->quote('%'.$fccy_id.'%'));
      if($status)      $select->where('d.DAILYLIMIT_STATUS='.$status);

      if($fuser_id)
			{	
        
				$fuser_idarr = explode(',', $fuser_id);
				$select->where("UPPER(d.USER_LOGIN)  in (?)",$fuser_idarr );	
      }
      
      if($fccy_id)
			{	
				$fccy_idarr = explode(',', $fccy_id);
				$select->where("UPPER(d.CCY_ID)  in (?)",$fccy_idarr );	
			}
    
      //filter date
      if($latestSuggestionFrom)  $select->where("DATE(d.SUGGESTED) >= DATE(".$this->_db->quote($latestSuggestionFrom->toString($this->_dateDBFormat)).")");
      if($latestSuggestionTo)    $select->where("DATE(d.SUGGESTED) <= DATE(".$this->_db->quote($latestSuggestionTo->toString($this->_dateDBFormat)).")");
      if($latestApprovalFrom)    $select->where("DATE(d.UPDATED) >= DATE(".$this->_db->quote($latestApprovalFrom->toString($this->_dateDBFormat)).")");
      if($latestApprovalTo)      $select->where("DATE(d.UPDATED) <= DATE(".$this->_db->quote($latestApprovalTo->toString($this->_dateDBFormat)).")");
      
      if($latestSuggestionFrom)  $this->view->latestSuggestionFrom = $latestSuggestionFrom->toString($this->_dateDisplayFormat);
      if($latestSuggestionTo)    $this->view->latestSuggestionTo   = $latestSuggestionTo->toString($this->_dateDisplayFormat);
      if($latestApprovalFrom)    $this->view->latestApprovalFrom   = $latestApprovalFrom->toString($this->_dateDisplayFormat);
      if($latestApprovalTo)      $this->view->latestApprovalTo     = $latestApprovalTo->toString($this->_dateDisplayFormat);

  
    }

    // $data_val = array();

		// // echo "<pre>";
		// // print_r($data_val);die;

		// foreach($data as $valu){
		// 	$paramTrx = array(	"Payment Ref"  		=> $valu['payref'],
		// 						"Payment Subject"  	=> $valu['subject'],
		// 						"Release Date"  	=> $valu['transferdate'],
		// 						"Payment Type"  	=> $valu['paytype'],
		// 						"Status" 			=> $valu['paystatus'],
		// 					);

		// 	array_push($data_val, $paramTrx);
		// }
    
    // exportfile
    
    
    //utk sorting
    $select->order($sortBy.' '.$sortDir);
          

    $select = $select->query()->fetchAll();   
    // var_dump($select);die;
    
    $this->paging($select);

    $data_val=array();
		foreach($select as $valu){
			$paramTrx = array ("USER_LOGIN" => $valu['USER_LOGIN'],
								"CUST_ID"	=> $valu['CUST_ID'],
								"CCY_ID"	=> $valu['CCY_ID'],
								"DAILYLIMIT"	=> $valu['DAILYLIMIT'],
								"DAILYLIMIT_STATUS"	=> $valu['DAILYLIMIT_STATUS'],
								"CREATED"	=> $valu['CREATED'],
								"CREATEDBY"	=> $valu['CREATEDBY'],
								"UPDATED"	=> $valu['UPDATEDBY'] . " (" . $valu['UPDATED']. ")",
								"SUGGESTED"	=> $valu['SUGGESTEDBY'] . " (" . $valu['SUGGESTED']. ")",
							
			);
			array_push($data_val, $paramTrx);
    }
    
    $filterlistdata = array("Filter");
    foreach($dataParamValue as $fil => $val){
        $paramTrx = $fil . " - " . $val;
        array_push($filterlistdata, $paramTrx);
    }
    
    if ($csv) {
      // $this->_helper->download->csv(array($this->language->_('User Name'),$this->language->_('CCY'),$this->language->_('Daily Limit'),$this->language->_('Last Suggested By	'), $this->language->_('Last Approved By')),$data_val,null,'User Daily Limit Scheme');
      $header  = Application_Helper_Array::simpleArray($fields, "label");
      $listable = array_merge_recursive(array($header), $data_val);
			$this->_helper->download->csv($filterlistdata, $listable, null, 'User Daily Limit Scheme');
    }
    
    $this->view->fields = $fields;
    $this->view->filter = $filter;
    
    //$this->view->status_type = $this->_masteruserStatus;
    $this->view->modulename = $this->_request->getModuleName();
    $this->view->statusCode = array_flip($this->_masterglobalstatus['code']);
    $this->view->statusDesc = $this->_masterglobalstatus['desc'];

    if (!empty($dataParamValue)) {
			// $this->view->createdStart = $dataParamValue['PS_CREATED'];
			// $this->view->createdEnd = $dataParamValue['PS_CREATED_END'];
		//	$this->view->efdateStart = $dataParamValue['PS_EFDATE'];
		//	$this->view->efdateEnd = $dataParamValue['PS_EFDATE_END'];

			unset($dataParamValue['USER_SUGGESTED_END']);
			unset($dataParamValue['PS_CREATED_END']);
			unset($dataParamValue['PS_EFDATE_END']);

			foreach ($dataParamValue as $key => $value) {
				$duparr = explode(',',$value);
							if(!empty($duparr)){
								
								foreach($duparr as $ss => $vs){
									$wherecol[]	= $key;
									$whereval[] = $vs;
								}
							}else{
									$wherecol[]	= $key;
									$whereval[] = $value;
							}
			}
			$this->view->wherecol     = $wherecol;
			$this->view->whereval     = $whereval;
				
    }

    // print_r($wherecol);
    // print_r($whereval);
    
    // if(!empty($this->_request->getParam('wherecol'))){
    //     $this->view->wherecol     = $this->_request->getParam('wherecol');
    //   }

    //   if(!empty($this->_request->getParam('whereopt'))){
    //     $this->view->whereopt     = $this->_request->getParam('whereopt');
    //   }

    //   if(!empty($this->_request->getParam('whereval'))){
    //     $this->view->whereval     = $this->_request->getParam('whereval');
    //   }
    //insert log
    if(!$this->_request->isPost()){
      Application_Helper_General::writeLog('VDLU','View User Daily Limit List');
    }

    $userData = $this->_db->select()
                              ->from('M_USER',array('USER_ID','USER_FULLNAME'))
                              ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                              ->where('USER_STATUS!=3')
                              ->query()->fetchAll();
     

     $tempuserData = $this->_db->select()
                              ->from('M_DAILYLIMIT',array('USER_LOGIN'))
                              ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                             // ->where('USER_STATUS!=3');
                              ->query()->fetchAll();
        //echo $tempuserData;die;      
                
      foreach ($userData as $key => $value) {

          foreach ($tempuserData as $keytemp => $valuetemp) {
             if($value['USER_ID'] == $valuetemp['USER_LOGIN']){
                unset($userData[$key]);
             }
          }
      }

      $this->view->userData = $userData;
  }
  
  
  public function checkdailyAction(){
    $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
    $cust_id = $this->_custIdLogin;
    $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;
    $user_id      = $this->_getParam('user_id');
    $user_id = strtoupper($user_id);
    $ccy_id = 'IDR';
    $ccy_id = (Zend_Validate::is($ccy_id,'Alnum') && Zend_Validate::is($ccy_id,'StringLength',array('min'=>1,'max'=>3)))? $ccy_id : null;
    
    if($user_id){
    $select = $this->_db->select()
                             ->from('TEMP_DAILYLIMIT',array('TEMP_ID'))
                               ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                               ->where('UPPER(USER_LOGIN)='.$this->_db->quote((string)$user_id))
                               ->where('UPPER(CCY_ID)='.$this->_db->quote((string)$ccy_id));
                 //echo $select;
                 //die('here');
        $result = $this->_db->fetchOne($select);
      if($result){
        echo false;
      }else{
        echo true;
      }
    }else{
      echo false;
    }
    
  }
  
   
  public function deletedailyAction(){
    $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
    
    $cust_id = $this->_custIdLogin;
   
     $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;
    
     //$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
      // $password = $sessionNamespace->token; 
      // $this->view->token = $sessionNamespace->token;  


    //$AESMYSQL = new Crypt_AESMYSQL();
  //$this->view->encuser = $this->_getParam('user_id');
    $user_id      = $this->_getParam('user_id');
    //$user_id = $AESMYSQL->decrypt($PS_NUMBER, $password);

    $user_id = strtoupper($user_id);
//     $user_id = (Zend_Validate::is($user_id,'Alnum') && Zend_Validate::is($user_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $user_id : null;
    
     $ccy_id = 'IDR';
     $ccy_id = (Zend_Validate::is($ccy_id,'Alnum') && Zend_Validate::is($ccy_id,'StringLength',array('min'=>1,'max'=>3)))? $ccy_id : null;
   
   
    if($user_id && $ccy_id)
    {
      $select = $this->_db->select()
                             ->from(array('d'=>'M_DAILYLIMIT'))
                             ->join(array('u'=>'M_USER'),'d.USER_LOGIN=u.USER_ID and d.CUST_ID=u.CUST_ID',array('USER_FULLNAME'))
                             ->where('UPPER(d.CUST_ID)='.$this->_db->quote((string)$cust_id))
                             ->where('UPPER(d.USER_LOGIN)='.$this->_db->quote((string)$user_id))
                             ->where('UPPER(d.CCY_ID)='.$this->_db->quote((string)$ccy_id))
                             ->where('d.DAILYLIMIT_STATUS!=3');
      $dailyLimitData = $this->_db->fetchRow($select);
      
     
      if($dailyLimitData['DAILYLIMIT'])
      {
        
          $select = $this->_db->select()
                               ->from('TEMP_DAILYLIMIT',array('TEMP_ID'))
                               ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                               ->where('UPPER(USER_LOGIN)='.$this->_db->quote((string)$user_id))
                               ->where('UPPER(CCY_ID)='.$this->_db->quote((string)$ccy_id));
          $result = $this->_db->fetchOne($select);
          
          if(!$result)
          {
             $info = 'Cust ID = '.$cust_id.', User Id = '.$user_id.', Ccy Id = '.$ccy_id;
             
             $dailyLimitData['DAILYLIMIT_STATUS'] = 3;
             $dailyLimitData['SUGGESTED']    = new Zend_Db_Expr('now()');
             $dailyLimitData['SUGGESTEDBY']  = $this->_userIdLogin;
             
             try 
             {
              $this->_db->beginTransaction();
                $change_id = $this->suggestionWaitingApproval('User Daily Limit',$info,strtoupper($this->_changeType['code']['delete']),null,'M_DAILYLIMIT','TEMP_DAILYLIMIT',$user_id,$dailyLimitData['USER_FULLNAME'],$cust_id);
              $this->insertTempDailyLimit($change_id,$dailyLimitData);
              
              //log CRUD
              Application_Helper_General::writeLog('UDLU','User Daily Limit has been Updated (delete), User Id : '.$user_id. ' CCY : '.$dailyLimitData['CCY_ID'].' Change id : '.$change_id);
              
              $this->_db->commit();
              
              //$this->_redirect('/notification/submited/index');
           //   echo true;
            }
            catch(Exception $e) 
            {
              $this->_db->rollBack();
              $error_remark = $this->language->_('An Error Occured. Please Try Again');
          echo $error_remark;
              SGO_Helper_GeneralLog::technicalLog($e);
            }

            if(!$error_remark)
            { 
              Application_Helper_General::writeLog('UDLU','Update User Daily Limit');
                
              echo true;
            }
          }
          else
          { 
              $user_id = null;
              $error_remark = $this->language->_('No changes allowed for this record while awaiting approval for previous change.');
        echo $error_remark;
          }
        }
        else
        {
            $user_id = null;
            $error_remark = 'Daily limit data is not found.';
      echo $error_remark;
        }
     
    }
    else{
      $user_id = null;
    echo false;
    }
        
  }
  
  public function editdailyAction(){
    $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        
    
    
    
     
    
    $cust_id = $this->_custIdLogin;
   
     $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;
    
     //$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
      // $password = $sessionNamespace->token; 
      // $this->view->token = $sessionNamespace->token;  


    //$AESMYSQL = new Crypt_AESMYSQL();
  //$this->view->encuser = $this->_getParam('user_id');
    $user_id      = $this->_getParam('user_id');
    //$user_id = $AESMYSQL->decrypt($PS_NUMBER, $password);

    $user_id = strtoupper($user_id);
//     $user_id = (Zend_Validate::is($user_id,'Alnum') && Zend_Validate::is($user_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $user_id : null;
    
     $ccy_id = 'IDR';
     $ccy_id = (Zend_Validate::is($ccy_id,'Alnum') && Zend_Validate::is($ccy_id,'StringLength',array('min'=>1,'max'=>3)))? $ccy_id : null;
     $error_remark = null;
     
     //convert dailylimit agar bisa masuk ke database
     $dailyLimit = $this->_getParam('dailylimit');
     $dailyLimit = Application_Helper_General::convertDisplayMoney($dailyLimit);
     $this->_setParam('dailylimit',$dailyLimit); 
     //END convert dailylimit agar bisa masuk ke database
     
     
     $this->view->userDailyLimit_msg = array();
     

    if($cust_id)
    {
      $select = $this->_db->select()
                             ->from('M_CUSTOMER',array('CUST_ID','CUST_LIMIT_IDR','CUST_LIMIT_USD'))
                             ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                             //->where('UPPER(CUST_STATUS)='.$this->_db->quote(strtoupper($this->_masterStatus['code']['active'])));
                             ->where('CUST_STATUS!=3');
      $result = $this->_db->fetchRow($select);
      if(!$result)$cust_id = null;
      else{
        $companyLim = array();
        $companyLim['IDR'] = Application_Helper_General::convertDisplayMoney($result['CUST_LIMIT_IDR']);
        $companyLim['USD'] = Application_Helper_General::convertDisplayMoney($result['CUST_LIMIT_USD']);
      }
    }
    
    if(!$cust_id)
    {
      return false;
    }
    
    if($user_id && $ccy_id)
    {
      $select  =  $this->_db->select()
                           ->from(array('d'=>'M_DAILYLIMIT'),array('DAILYLIMIT'))
                             ->where('UPPER(d.CUST_ID)='.$this->_db->quote((string)$cust_id))
                             ->where('UPPER(d.USER_LOGIN)='.$this->_db->quote((string)$user_id))
                             ->where('UPPER(d.CCY_ID)='.$this->_db->quote((string)$ccy_id))
                             ->where('d.DAILYLIMIT_STATUS!=3');
      $result = $this->_db->fetchOne($select);
      
      if($result)
      {
      
        $select = $this->_db->select()
                             ->from('TEMP_DAILYLIMIT',array('TEMP_ID'))
                               ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                               ->where('UPPER(USER_LOGIN)='.$this->_db->quote((string)$user_id))
                               ->where('UPPER(CCY_ID)='.$this->_db->quote((string)$ccy_id));
                 //echo $select;
                 //die('here');
        $result = $this->_db->fetchOne($select);
        
        if($result)
        {
          $daily_data = null;
          $error_remark = $this->language->_('No changes allowed for this record while awaiting approval for previous change.');
      echo $error_remark;
        }
        else{
          $daily_data = 1;
        }
      }
      else{ $daily_data = null; }
    }
    else{
      $daily_data = null;
      $error_remark = 'User ID / CCY is not found.';
    echo $error_remark;
    }
    
    
    $getCcy = $this->getCcy();
      $sumArr = array();
      foreach($getCcy as $val){
        $tempCCY = $val['CCY_ID'];
        $selectSum = $this->_db->select()
                              ->from('M_DAILYLIMIT', new Zend_Db_Expr('SUM(DAILYLIMIT)'))
                              ->where('UPPER(CUST_ID)= ?', $cust_id)
                              ->where('CCY_ID = ?', $tempCCY);
        $sumArr[$val['CCY_ID']] = $this->_db->fetchOne($selectSum);
  $selectUser = $this->_db->select()
      ->from('M_DAILYLIMIT', new Zend_Db_Expr('SUM(DAILYLIMIT)'))
      ->where('UPPER(CUST_ID)= ?', $cust_id)
      ->where('UPPER(USER_LOGIN)= ?', $user_id)
      ->where('CCY_ID = ?', $tempCCY);
  //echo $tempCCY;die;
      $totalArr[$val['CCY_ID']] = $this->_db->fetchOne($selectUser);
      }
    
     $ccy = 'IDR';
        $dailylim = $this->_getParam('dailylimit');
        $totalamount = $sumArr[$ccy]-$totalArr[$ccy];
        // $dailylimLeft = $companyLim[$ccy] - Application_Helper_General::convertDisplayMoney($totalamount);
        $dailylimLeft = $companyLim[$ccy];
  //print_r($totalArr[$ccy]);die;
        $errorLim = false;

        if($dailylim > $dailylimLeft){
          $errorLim = true;
          // $error_remark = "Maximum Amount of Daily Limit in ".$ccy." is ".Application_Helper_General::displayMoney($companyLim[$ccy]);
          // $remain = (int)$companyLim[$ccy]-(int)$dailylimLeft;
           // $error_remark = "Remaining amount of  company daily limit in ".$ccy." is ".Application_Helper_General::displayMoney($dailylimLeft)." ( from ".$ccy." ".Application_Helper_General::displayMoney($companyLim[$ccy])." )";
           $error_remark = "Maximum Amount of Daily Limit in ".$ccy." is ".Application_Helper_General::displayMoney($companyLim[$ccy]);
       return $error_remark;
        }
        else{
            $info = 'Cust ID = '.$cust_id.', User Id = '.$user_id.', Ccy Id = '.$ccy_id;
            
            $dailyLimitData = array();
            
      $select = $this->_db->select()
                             ->from(array('d'=>'M_DAILYLIMIT'))
                             ->join(array('u'=>'M_USER'),'d.USER_LOGIN=u.USER_ID and d.CUST_ID=u.CUST_ID',array('USER_FULLNAME'))
                            // ->where('UPPER(u.CUST_ID)='.$this->_db->quote((string)$cust_id))
                             ->where('UPPER(d.CUST_ID)='.$this->_db->quote((string)$cust_id))
                             ->where('UPPER(d.USER_LOGIN)='.$this->_db->quote((string)$user_id))
                             ->where('UPPER(d.CCY_ID)='.$this->_db->quote((string)$ccy_id))
                             ->where('d.DAILYLIMIT_STATUS!=3');

      $resultdata = $this->_db->fetchRow($select);  

      $dailyLimitData['CUST_ID']  = $cust_id;
      $dailyLimitData['USER_LOGIN']  = $user_id;
      $dailyLimitData['CCY_ID']  = $ccy;
      $dailyLimitData['DAILYLIMIT']  = $dailylim;

            //$dailyLimitData['DAILYLIMIT_STATUS'] = 1;
            $dailyLimitData['DAILYLIMIT_STATUS']  = $resultdata['DAILYLIMIT_STATUS'];
            $dailyLimitData['CREATED']       = $resultdata['CREATED'];
            $dailyLimitData['CREATEDBY']     = $resultdata['CREATEDBY'];
            $dailyLimitData['UPDATED']       = $resultdata['UPDATED'];
            $dailyLimitData['UPDATEDBY']     = $resultdata['UPDATEDBY'];
              
              
            $dailyLimitData['SUGGESTED']    = new Zend_Db_Expr('now()');
            $dailyLimitData['SUGGESTEDBY']  = $this->_userIdLogin;

            try 
            {
              $this->_db->beginTransaction();
              
              $change_id = $this->suggestionWaitingApproval('User Daily Limit',$info,strtoupper($this->_changeType['code']['edit']),null,'M_DAILYLIMIT','TEMP_DAILYLIMIT',$user_id,$resultdata['USER_FULLNAME'],$cust_id);
              
              $this->insertTempDailyLimit($change_id,$dailyLimitData);
              
              //log CRUD
              Application_Helper_General::writeLog('UDLU','User Daily Limit has been Updated (edit), User Id : '.$user_id. ' CCY : '.$resultdata['CCY_ID'].' Change id : '.$change_id);
              
              $this->_db->commit();
        echo true;
              //$this->_redirect('/notification/submited/index');
              //$this->_redirect('/notification/submited/index');
            }
            catch(Exception $e)
            {
              $this->_db->rollBack();
              $error_remark = $this->language->_('An Error Occured. Please Try Again');
              SGO_Helper_GeneralLog::technicalLog($e);
        echo $error_remark;
            }
        }
    
      
  }
  
  
  
  public function adddailyAction(){
    $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        
    
    
    
     
    
    $cust_id = $this->_custIdLogin;
   
     $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;
    
     //$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
      // $password = $sessionNamespace->token; 
      // $this->view->token = $sessionNamespace->token;  


    //$AESMYSQL = new Crypt_AESMYSQL();
  //$this->view->encuser = $this->_getParam('user_id');
    $user_id      = $this->_getParam('user_id');
    //$user_id = $AESMYSQL->decrypt($PS_NUMBER, $password);

    $user_id = strtoupper($user_id);
//     $user_id = (Zend_Validate::is($user_id,'Alnum') && Zend_Validate::is($user_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $user_id : null;
    
     $ccy_id = 'IDR';
     $ccy_id = (Zend_Validate::is($ccy_id,'Alnum') && Zend_Validate::is($ccy_id,'StringLength',array('min'=>1,'max'=>3)))? $ccy_id : null;
     $error_remark = null;
     
     //convert dailylimit agar bisa masuk ke database
     $dailyLimit = $this->_getParam('dailylimit');
     $dailyLimit = Application_Helper_General::convertDisplayMoney($dailyLimit);
     $this->_setParam('dailylimit',$dailyLimit); 
     //END convert dailylimit agar bisa masuk ke database
     
     
     $this->view->userDailyLimit_msg = array();
     

    if($cust_id)
    {
      $select = $this->_db->select()
                             ->from('M_CUSTOMER',array('CUST_ID','CUST_LIMIT_IDR','CUST_LIMIT_USD'))
                             ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                             //->where('UPPER(CUST_STATUS)='.$this->_db->quote(strtoupper($this->_masterStatus['code']['active'])));
                             ->where('CUST_STATUS!=3');
      $result = $this->_db->fetchRow($select);
      if(!$result)$cust_id = null;
      else{
        $companyLim = array();
        $companyLim['IDR'] = Application_Helper_General::convertDisplayMoney($result['CUST_LIMIT_IDR']);
        $companyLim['USD'] = Application_Helper_General::convertDisplayMoney($result['CUST_LIMIT_USD']);
      }
    }
    
    if(!$cust_id)
    {
      return false;
    }
    
     $userData = $this->_db->select()
                              ->from('M_USER',array('USER_ID','USER_FULLNAME'))
                              ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                              ->where('USER_STATUS!=3')
                              ->query()->fetchAll();
     

     $tempuserData = $this->_db->select()
                              ->from('TEMP_DAILYLIMIT',array('USER_LOGIN'))
                              ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                             // ->where('USER_STATUS!=3');
                              ->query()->fetchAll();
        //echo $tempuserData;die;      
                
      foreach ($userData as $key => $value) {

          foreach ($tempuserData as $keytemp => $valuetemp) {
             if($value['USER_ID'] == $valuetemp['USER_LOGIN']){
      //   die('gsa');
                unset($userData[$key]);
             }
          }
        # code...
      }
     
    
    $getCcy = $this->getCcy();
      $sumArr = array();
      foreach($getCcy as $val){
        $tempCCY = $val['CCY_ID'];
        $selectSum = $this->_db->select()
                              ->from('M_DAILYLIMIT', new Zend_Db_Expr('SUM(DAILYLIMIT)'))
                              ->where('UPPER(CUST_ID)= ?', $cust_id)
                              ->where('CCY_ID = ?', $tempCCY);
        $sumArr[$val['CCY_ID']] = $this->_db->fetchOne($selectSum);
  $selectUser = $this->_db->select()
      ->from('M_DAILYLIMIT', new Zend_Db_Expr('SUM(DAILYLIMIT)'))
      ->where('UPPER(CUST_ID)= ?', $cust_id)
      ->where('UPPER(USER_LOGIN)= ?', $user_id)
      ->where('CCY_ID = ?', $tempCCY);
  //echo $tempCCY;die;
      $totalArr[$val['CCY_ID']] = $this->_db->fetchOne($selectUser);
      }
    
    
    $checkMaster = $this->_db->select()
                              ->from(array('M_DAILYLIMIT'), array('USER_LOGIN'))
                              ->where('USER_LOGIN = ?', $user_id)
                              ->where('CUST_ID = ?', $cust_id)
                              ->where('CCY_ID = ?', $ccy_id)
                              ->where('DAILYLIMIT_STATUS = ?', '1');
        $uniqueMaster = $this->_db->fetchOne($checkMaster);


        $checkTemp = $this->_db->select()
                              ->from(array('TEMP_DAILYLIMIT'), array('USER_LOGIN'))
                              ->where('USER_LOGIN = ?', $user_id)
                              ->where('CUST_ID = ?', $cust_id);
                             // ->where('CCY_ID = ?', $zf_filter_input->ccy_id);
        $uniqueTemp = $this->_db->fetchOne($checkTemp);


        if(empty($uniqueMaster) && empty($uniqueTemp)){
    
     $ccy = 'IDR';
        $dailylim = $this->_getParam('dailylimit');
        $totalamount = $sumArr[$ccy]-$totalArr[$ccy];
        // $dailylimLeft = $companyLim[$ccy] - Application_Helper_General::convertDisplayMoney($totalamount);
        $dailylimLeft = $companyLim[$ccy];
  //print_r($totalArr[$ccy]);die;
        $errorLim = false;

        if($dailylim > $dailylimLeft){
          $errorLim = true;
          // $error_remark = "Maximum Amount of Daily Limit in ".$ccy." is ".Application_Helper_General::displayMoney($companyLim[$ccy]);
          // $remain = (int)$companyLim[$ccy]-(int)$dailylimLeft;
           // $error_remark = "Remaining amount of  company daily limit in ".$ccy." is ".Application_Helper_General::displayMoney($dailylimLeft)." ( from ".$ccy." ".Application_Helper_General::displayMoney($companyLim[$ccy])." )";
           $error_remark = "Maximum Amount of Daily Limit in ".$ccy." is ".Application_Helper_General::displayMoney($companyLim[$ccy]);
       echo $error_remark;
        }
        else{
            $info = 'Cust ID = '.$cust_id.', User Id = '.$user_id.', Ccy Id = '.$ccy_id;
            
            $dailyLimitData = array();
            
       $user_info = $this->_db->select()
                                       ->from('M_USER',array('USER_FULLNAME'))
                                       ->where('UPPER(USER_ID)='.$this->_db->quote((string)$user_id))
                                       ->query()->fetch();
          
              $user_fullname = $user_info['USER_FULLNAME'];  

      $dailyLimitData['CUST_ID']  = $cust_id;
      $dailyLimitData['USER_LOGIN']  = $user_id;
      $dailyLimitData['CCY_ID']  = $ccy;
      $dailyLimitData['DAILYLIMIT']  = $dailylim;

            //$dailyLimitData['DAILYLIMIT_STATUS'] = 1;
            $dailyLimitData['DAILYLIMIT_STATUS'] = 1;
              
              $dailyLimitData['UPDATED']     = null;
              $dailyLimitData['UPDATEDBY']   = null;
              $dailyLimitData['CREATED']     = new Zend_Db_Expr('now()');
              $dailyLimitData['CREATEDBY']   = $this->_userIdLogin;
              $dailyLimitData['SUGGESTED']    = new Zend_Db_Expr('now()');
              $dailyLimitData['SUGGESTEDBY']  = $this->_userIdLogin;

             try 
            {  
              $this->_db->beginTransaction();
              $change_id = $this->suggestionWaitingApproval('User Daily Limit',$info,$this->_changeType['code']['new'],null,'M_DAILYLIMIT','TEMP_DAILYLIMIT',$user_id,$user_fullname,$cust_id);
          
              $this->insertTempDailyLimit($change_id,$dailyLimitData);
              
              //log CRUD
              Application_Helper_General::writeLog('ADLU','User Daily Limit has been Added, User Id : '.$user_id. ' CCY : '.$ccy_id.' Change id : '.$change_id);
              
              $this->_db->commit();
              echo true;
                //$this->_redirect('/notification/submited/index');
                //$this->_redirect('/notification/submited/index');
            }
            catch(Exception $e)
            {
              $this->_db->rollBack();
              $error_remark = $this->language->_('An Error Occured. Please Try Again');
              SGO_Helper_GeneralLog::technicalLog($e);
        echo $error_remark;
            }
        }
    
    }else{
      echo false;
    }
    
      
  }
  
  
}