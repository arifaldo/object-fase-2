<?php
require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';

class preliminary_SuggestiondetailController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('popup');

		$model = new preliminary_Model_Preliminary();

		$changes_id = urldecode($this->_getParam('changes_id'));

		$sessToken 	= new Zend_Session_Namespace('Tokenenc');
		$password 	= $sessToken->token;

		$AESMYSQL   = new Crypt_AESMYSQL();
		$decryption = urldecode($id);
		$changes_id = $AESMYSQL->decrypt($changes_id, $password);

		$this->view->changes_id 			= $changes_id;
		$this->view->suggestionType 		= $this->_suggestType;
		$this->view->changes_id_encrypted 	= $this->_getParam('changes_id');

		$config = Zend_Registry::get('config');

		$entityCode = $config["bg"]["entity"]["code"];
		$entityDesc = $config["bg"]["entity"]["desc"];
		$entityArr  = array_combine(array_values($entityCode), array_values($entityDesc));

		$citizenshipCode = $config["bg"]["citizen"]["code"];
		$citizenshipDesc = $config["bg"]["citizen"]["desc"];
		$citizenshipArr  = array_combine(array_values($citizenshipCode), array_values($citizenshipDesc));

		$statusCode = $config["bg"]["statusowner"]["code"];
		$statusDesc = $config["bg"]["statusowner"]["desc"];
		$statusArr  = array_combine(array_values($statusCode), array_values($statusDesc));

		$getPosition = $model->getPosition();
		$positionArr = Application_Helper_Array::listArray($getPosition, 'CODE', 'POSITION');

		$getCity = $model->getCity();
		$cityArr = Application_Helper_Array::listArray($getCity, 'CITY_CODE', 'CITY_NAME');

		$getRelationship = $model->getRelationship();
		$relationshipArr = Application_Helper_Array::listArray($getRelationship, 'NO', 'RELATION_WITH_BANK');

		if ($changes_id) {
			$select = $this->_db->select()
				->from('T_GLOBAL_CHANGES')
				->where('CHANGES_ID = ?', $changes_id)
				->where("CHANGES_STATUS = 'WA' OR CHANGES_STATUS = 'RR'");
			$result = $this->_db->fetchRow($select);

			if (empty($result)) {
				$this->_redirect('/notification/invalid/index');
			} else {
				$cust_id = $result['CUST_ID'];

				$this->view->interface = $result['CHANGES_FLAG'];

				$this->view->changes_type   		= $result['CHANGES_TYPE'];
				$this->view->changes_status 		= $result['CHANGES_STATUS'];
				$this->view->read_status    		= $result['READ_STATUS'];
				$this->view->created        		= $result['CREATED'];
				$this->view->created_by     		= $result['CREATED_BY'];

				// DATA TEMP PRELIMINARY
				$dataTemp = $model->getTempPreliminary($cust_id);

				$this->view->relationship 			= $relationshipArr[$dataTemp['RELATIONSHIP']];
				$this->view->cust_name 				= $dataTemp['CUST_NAME'];
				$this->view->npwp 					= $dataTemp['NPWP'];
				$this->view->pendirian_number 		= $dataTemp['PENDIRIAN_NUMBER'];
				$this->view->pendirian_date 		= $dataTemp['PENDIRIAN_DATE'];
				$this->view->pendirian_place 		= $dataTemp['PENDIRIAN_PLACE'];
				$this->view->pengesahan_pendirian_number 		= $dataTemp['SK_PENGESAHAN_NUMBER'];
				$this->view->pengesahan_pendirian_date 		= $dataTemp['SK_PENGESAHAN_DATE'];
				$this->view->perubahan_number 		= $dataTemp['PERUBAHAN_NUMBER'];
				$this->view->perubahan_date 		= $dataTemp['PERUBAHAN_DATE'];
				$this->view->sk_perubahan_number 	= $dataTemp['SK_PERUBAHAN_NUMBER'];
				$this->view->sk_perubahan_date 		= $dataTemp['SK_PERUBAHAN_DATE'];

				$this->view->dataTemp = $dataTemp;

				// DATA MASTER PRELIMINARY
				$dataMaster = $model->getPreliminary($cust_id);
				if ($dataMaster) {
					$this->view->m_relationship 		= $relationshipArr[$dataMaster['RELATIONSHIP']];
					$this->view->m_cust_name 			= $dataMaster['CUST_NAME'];
					$this->view->m_npwp 				= $dataMaster['NPWP'];
					$this->view->m_pendirian_number 	= $dataMaster['PENDIRIAN_NUMBER'];
					$this->view->m_pendirian_date 		= $dataMaster['PENDIRIAN_DATE'];
					$this->view->m_pendirian_place 		= $dataMaster['PENDIRIAN_PLACE'];
					$this->view->m_pengesahan_pendirian_number 	= $dataMaster['SK_PENGESAHAN_NUMBER'];
					$this->view->m_pengesahan_pendirian_date 		= $dataMaster['SK_PENGESAHAN_DATE'];
					$this->view->m_perubahan_number 	= $dataMaster['PERUBAHAN_NUMBER'];
					$this->view->m_perubahan_date 		= $dataMaster['PERUBAHAN_DATE'];
					$this->view->m_sk_perubahan_number 	= $dataMaster['SK_PERUBAHAN_NUMBER'];
					$this->view->m_sk_perubahan_date 	= $dataMaster['SK_PERUBAHAN_DATE'];

					$this->view->dataMaster = $dataMaster;
				}

				// DATA TEMP PRELIMINARY MEMBER
				$dataTempMember = $model->getTempPreliminaryMember($cust_id);

				$this->view->totalMember = count($dataTempMember);

				foreach ($dataTempMember as $key => $val) {
					$name_member 	= 'name_member' . $key;
					$entity_id 		= 'entity_id' . $key;
					$id_type 		= 'id_type' . $key;
					$id_number 		= 'id_number' . $key;
					$position 		= 'position' . $key;
					$share 			= 'share' . $key;
					$status 		= 'status' . $key;
					$address 		= 'address' . $key;
					$kelurahan 		= 'kelurahan' . $key;
					$kecamatan 		= 'kecamatan' . $key;
					$kab_city 		= 'kab_city' . $key;
					$this->view->$name_member 	= $val['NAME'];
					$this->view->$entity_id 	= $entityArr[$val['ENTITY_ID']];
					$this->view->$id_type 		= $val['ID_TYPE'];
					$this->view->$id_number 	= $val['ID_NUMBER'];
					$this->view->$position 		= $positionArr[$val['POSITION']];
					$this->view->$share 		= $val['SHARE'];
					$this->view->$status 		= $statusArr[$val['STATUS']];
					$this->view->$address 		= $val['ADDRESS'];
					$this->view->$kelurahan 	= $val['KELURAHAN'];
					$this->view->$kecamatan 	= $val['KECAMATAN'];
					$this->view->$kab_city 		= $cityArr[$val['KAB_CITY']];
				}

				// DATA MASTER PRELIMINARY MEMBER
				$dataMasterMember = $model->getPreliminaryMember($cust_id);
				if ($dataMasterMember) {
					foreach ($dataMasterMember as $key => $val) {
						$m_name_member 	= 'm_name_member' . $key;
						$m_entity_id 	= 'm_entity_id' . $key;
						$m_id_type 		= 'm_id_type' . $key;
						$m_id_number 	= 'm_id_number' . $key;
						$m_position 	= 'm_position' . $key;
						$m_share 		= 'm_share' . $key;
						$m_status 		= 'm_status' . $key;
						$m_address 		= 'm_address' . $key;
						$m_kelurahan 	= 'm_kelurahan' . $key;
						$m_kecamatan 	= 'm_kecamatan' . $key;
						$m_kab_city 	= 'm_kab_city' . $key;
						$this->view->$m_name_member 	= $val['NAME'];
						$this->view->$m_entity_id 		= $entityArr[$val['ENTITY_ID']];
						$this->view->$m_id_type 		= $val['ID_TYPE'];
						$this->view->$m_id_number 		= $val['ID_NUMBER'];
						$this->view->$m_position 		= $positionArr[$val['POSITION']];
						$this->view->$m_share 			= $val['SHARE'];
						$this->view->$m_status 			= $statusArr[$val['STATUS']];
						$this->view->$m_address 		= $val['ADDRESS'];
						$this->view->$m_kelurahan 		= $val['KELURAHAN'];
						$this->view->$m_kecamatan 		= $val['KECAMATAN'];
						$this->view->$m_kab_city 		= $cityArr[$val['KAB_CITY']];
					}
				}

				$submit_approve 		= $this->_getParam('submit_approve');
				$submit_reject 			= $this->_getParam('submit_reject');
				$submit_request_repair 	= $this->_getParam('submit_request_repair');

				if ($submit_approve) {
					try {
						$this->_db->beginTransaction();
						// INSERT/UPDATE DATA
						$data1 = [
							'CUST_ID'				=> $cust_id,
							'RELATIONSHIP'			=> $dataTemp['RELATIONSHIP'],
							'NPWP'					=> $dataTemp['NPWP'],
							'NPWP_FILE'				=> $dataTemp['NPWP_FILE'],
							'PENDIRIAN_NUMBER'		=> $dataTemp['PENDIRIAN_NUMBER'],
							'PENDIRIAN_NUMBER_FILE'	=> $dataTemp['PENDIRIAN_NUMBER_FILE'],
							'PENDIRIAN_DATE'		=> $dataTemp['PENDIRIAN_DATE'],
							'PENDIRIAN_PLACE'		=> $dataTemp['PENDIRIAN_PLACE'],
							'SK_PENGESAHAN_NUMBER'		=> $dataTemp['SK_PENGESAHAN_NUMBER'],
							'SK_PENGESAHAN_FILE'	=> $dataTemp['SK_PENGESAHAN_FILE'],
							'SK_PENGESAHAN_DATE'		=> $dataTemp['SK_PENGESAHAN_DATE'],
							'PERUBAHAN_NUMBER'		=> $dataTemp['PERUBAHAN_NUMBER'],
							'PERUBAHAN_FILE'		=> $dataTemp['PERUBAHAN_FILE'],
							'PERUBAHAN_DATE'		=> $dataTemp['PERUBAHAN_DATE'],
							'SK_PERUBAHAN_NUMBER'	=> $dataTemp['SK_PERUBAHAN_NUMBER'],
							'SK_PERUBAHAN_FILE'		=> $dataTemp['SK_PERUBAHAN_FILE'],
							'SK_PERUBAHAN_DATE'		=> $dataTemp['SK_PERUBAHAN_DATE'],
							'LAST_SUGGESTED'		=> $dataTemp['LAST_SUGGESTED'],
							'LAST_SUGGESTEDBY'		=> $dataTemp['LAST_SUGGESTEDBY'],
							'LAST_APPROVED'			=> new Zend_Db_Expr('now()'),
							'LAST_APPROVEDBY'		=> $this->_userIdLogin
						];
						$where1 = ['CUST_ID = ?' => $cust_id];

						if ($result['CHANGES_TYPE'] == 'E') {
							$this->_db->update('M_PRELIMINARY', $data1, $where1);
						} else {
							$this->_db->insert('M_PRELIMINARY', $data1);
						}

						$this->_db->delete('M_PRELIMINARY_MEMBER', $where1);

						// INSERET DATA MEMBER
						foreach ($dataTempMember as $row) {
							$data2 = [
								'CUST_ID'		=> $cust_id,
								'NAME'			=> $row['NAME'],
								'ENTITY_ID'		=> $row['ENTITY_ID'],
								'CITIZENSHIP'	=> $row['CITIZENSHIP'],
								'POSITION'		=> $row['POSITION'],
								'SHARE'			=> $row['SHARE'],
								'STATUS'		=> $row['STATUS'],
								'ID_TYPE'		=> $row['ID_TYPE'],
								'ID_NUMBER'		=> $row['ID_NUMBER'],
								'ID_FILE'		=> $row['ID_FILE'],
								'ADDRESS'		=> $row['ADDRESS'],
								'KELURAHAN'		=> $row['KELURAHAN'],
								'KECAMATAN'		=> $row['KECAMATAN'],
								'KAB_CITY'		=> $row['KAB_CITY']
							];
							$this->_db->insert('M_PRELIMINARY_MEMBER', $data2);
						}

						$this->_db->delete('TEMP_PRELIMINARY', $where1);
						$this->_db->delete('TEMP_PRELIMINARY_MEMBER', $where1);

						// kembalikan data menjadi status permintaan verifikasi ulang

						$getAllBgOnRevApp = $this->_db->select()
							->from("TEMP_BANK_GUARANTEE")
							->where('BG_STATUS IN (?)', [6, 7, 5, 20, 8, 10])
							->where("CUST_ID = ?", $cust_id)
							->query()->fetchAll();

						foreach ($getAllBgOnRevApp as $bg) {
							# code...
							$status = (intval($bg['BG_STATUS']) == 8 || intval($bg['BG_STATUS']) == 10 || intval($bg['BG_STATUS']) == 5) ? strval($bg['BG_STATUS']) : '20';
							$data = array(
								'BG_STATUS' => $status,
								'SP_OBLIGEE_CODE' => '',
								'AGREE_FORMAT' => "",
								'MEMO_LEGAL' => "",
								'KUASA_DIREKSI_FILE' => "",
							);
							$where['BG_REG_NUMBER = ?'] = $bg['BG_REG_NUMBER'];

							$this->_db->delete('TEMP_BANK_GUARANTEE_PRELIMINARY', $where);
							$this->_db->delete('TEMP_BANK_GUARANTEE_PRELIMINARY_MEMBER', $where);

							$this->_db->update('TEMP_BANK_GUARANTEE', $data, $where);

							$this->_db->delete('TEMP_BGVERIFY_DETAIL', $where);

							$this->_db->delete('TEMP_BANK_GUARANTEE_DETAIL', "TEMP_BANK_GUARANTEE_DETAIL.BG_REG_NUMBER = '" . $bg['BG_REG_NUMBER'] . "' AND TEMP_BANK_GUARANTEE_DETAIL.PS_FIELDNAME = 'Info Slik OJK'");

							$historyInsert = array(
								'DATE_TIME'         => new Zend_Db_Expr("now()"),
								'BG_REG_NUMBER'         => $bg['BG_REG_NUMBER'],
								'CUST_ID'           => $cust_id,
								'USER_LOGIN'        => "Sistem",
								'BG_REASON'			=> "Terdapat pembaharuan data dokumen awal dari Principal", //'Request Reverification',
								'HISTORY_STATUS'    => 11
							);

							$this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);

							// if ($bg["COUNTER_WARRANTY_TYPE"] != "1") {
							// 	Application_Helper_General::writeLog('RNCS', "Permintaan Verifikasi Ulang BG. Noreg : [" . $bg['BG_REG_NUMBER'] . "]");
							// } else {
							// 	Application_Helper_General::writeLog('RCCS', "Permintaan Verifikasi Ulang BG. Noreg : [" . $bg['BG_REG_NUMBER'] . "]");
							// }
						}
						// end

						$data2  = [
							'CHANGES_STATUS'	=> 'AP',
							'LASTUPDATED'		=> new Zend_Db_Expr('now()')
						];
						$where2 = ['CHANGES_ID = ?' => $changes_id];
						$this->_db->update('T_GLOBAL_CHANGES', $data2, $where2);

						$this->_db->commit();

						$this->view->is_approve = true;

						Application_Helper_General::writeLog('APDC', 'Menyetujui Usulan Perubahan Dokumen Awal');
					} catch (Exception $error) {
						$this->_db->rollBack();
						echo '<pre>';
						print_r($error->getMessage());
						echo '</pre><br>';
						die;
					}
				} elseif ($submit_reject) {
					try {
						$this->_db->beginTransaction();

						$where1 = ['CUST_ID = ?' => $cust_id];
						$this->_db->delete('TEMP_PRELIMINARY', $where1);
						$this->_db->delete('TEMP_PRELIMINARY_MEMBER', $where1);

						$data2  = [
							'CHANGES_STATUS'	=> 'RJ',
							'LASTUPDATED'		=> new Zend_Db_Expr('now()')
						];
						$where2 = ['CHANGES_ID = ?' => $changes_id];
						$this->_db->update('T_GLOBAL_CHANGES', $data2, $where2);

						$this->_db->commit();

						$this->view->is_reject = true;

						Application_Helper_General::writeLog('APDC', 'Tolak Usulan Perubahan Dokumen Awal');
					} catch (Exception $error) {
						$this->_db->rollBack();
						echo '<pre>';
						print_r($error->getMessage());
						echo '</pre><br>';
						die;
					}
				} elseif ($submit_request_repair) {
					try {
						$this->_db->beginTransaction();

						$data2  = [
							'CHANGES_STATUS'	=> 'RR',
							'LASTUPDATED'		=> new Zend_Db_Expr('now()'),
							'CHANGES_REASON' => $this->_getParam('notes')
						];
						$where2 = ['CHANGES_ID = ?' => $changes_id];
						$this->_db->update('T_GLOBAL_CHANGES', $data2, $where2);

						$this->_db->commit();

						Application_Helper_General::writeLog('VPDC', 'Permintaan Perbaikan Usulan Perubahan Dokumen Awal');

						$this->view->is_request_repair = true;
					} catch (Exception $error) {
						$this->_db->rollBack();
						echo '<pre>';
						print_r($error->getMessage());
						echo '</pre><br>';
						die;
					}
				} else {
					Application_Helper_General::writeLog('VPDC', 'Lihat Usulan Perubahan Dokumen Awal');
				}
			}
		} else {
			$this->_redirect('/popuperror/index/index');
		}
	}

	public function repairAction()
	{
		$this->_helper->layout()->setLayout('popup');

		$model = new preliminary_Model_Preliminary();

		$changes_id = urldecode($this->_getParam('changes_id'));

		$sessToken 	= new Zend_Session_Namespace('Tokenenc');
		$password 	= $sessToken->token;

		$AESMYSQL   = new Crypt_AESMYSQL();
		$decryption = urldecode($id);
		$changes_id = $AESMYSQL->decrypt($changes_id, $password);

		$select = $this->_db->select()
			->from('T_GLOBAL_CHANGES', array('CHANGES_ID', 'CHANGES_FLAG', 'CUST_ID'))
			->where('CHANGES_ID = ?', $changes_id)
			->where("CHANGES_STATUS = 'WA' OR CHANGES_STATUS = 'RR'");
		$result = $this->_db->fetchRow($select);

		$cust_id = $result['CUST_ID'];

		if (empty($result)) {
			$this->_redirect('/notification/invalid/index');
		} else {
			$config = Zend_Registry::get('config');

			$entityCode = $config["bg"]["entity"]["code"];
			$entityDesc = $config["bg"]["entity"]["desc"];
			$entityArr  = array_combine(array_values($entityCode), array_values($entityDesc));
			$this->view->entityArr = $entityArr;

			$citizenshipCode = $config["bg"]["citizen"]["code"];
			$citizenshipDesc = $config["bg"]["citizen"]["desc"];
			$citizenshipArr  = array_combine(array_values($citizenshipCode), array_values($citizenshipDesc));
			$this->view->citizenshipArr = $citizenshipArr;

			$statusCode = $config["bg"]["statusowner"]["code"];
			$statusDesc = $config["bg"]["statusowner"]["desc"];
			$statusArr  = array_combine(array_values($statusCode), array_values($statusDesc));
			$this->view->statusArr = $statusArr;

			$getPosition = $model->getPosition();
			$positionArr = Application_Helper_Array::listArray($getPosition, 'CODE', 'POSITION');
			$this->view->positionArr = $positionArr;

			$getRelationship = $model->getRelationship();
			$relationshipArr = Application_Helper_Array::listArray($getRelationship, 'NO', 'RELATION_WITH_BANK');
			$this->view->relationshipArr = $relationshipArr;

			$getCity = $model->getCity();
			$cityArr = Application_Helper_Array::listArray($getCity, 'CITY_CODE', 'CITY_NAME');
			$this->view->cityArr = $cityArr;

			$getCustomer = $model->getCustomerById($cust_id);
			$this->view->cust_name = $getCustomer['CUST_NAME'];

			$data = $model->getTempPreliminary($cust_id);
			$this->view->data = $data;

			//$dataMember = $model->getPreliminaryMember($cust_id);
			$dataMember = $model->getTempPreliminaryMember($cust_id);
			$this->view->dataMember = $dataMember;

			if ($this->_request->isPost()) {
				$params = $this->_request->getParams();

				$npwp_file 			= $_FILES['npwp_file'];
				$sk_number_file 	= $_FILES['sk_number_file'];
				$sk_change_file 	= $_FILES['sk_change_file'];
				$sk_pengesahan_file = $_FILES['sk_pengesahan_file'];
				$sk_pengesahan_pendirian_file = $_FILES['sk_pengesahan_pendirian_file'];
				$ktp_file 			= $_FILES['ktp_file'];

				$id_files = $_FILES['ktp_file'];

				$path = UPLOAD_PATH . '/document/submit/';

				if (!empty($npwp_file['name'])) {
					$filename_npwp = strtolower(uniqid()) . '_' . strtolower(str_replace(' ', '_', $npwp_file['name']));
					$path_old_npwp = $npwp_file['tmp_name'];
					$path_new_npwp = $path . $filename_npwp;
					move_uploaded_file($path_old_npwp, $path_new_npwp);
				} else {
					$filename_npwp = $data["NPWP_FILE"];
				}

				if (!empty($sk_number_file['name'])) {
					$filename_sk_number = strtolower(uniqid()) . '_' . strtolower(str_replace(' ', '_', $sk_number_file['name']));
					$path_old_sk_number = $sk_number_file['tmp_name'];
					$path_new_sk_number = $path . $filename_sk_number;
					move_uploaded_file($path_old_sk_number, $path_new_sk_number);
				} else {
					$filename_sk_number = $data["PENDIRIAN_NUMBER_FILE"];
				}

				if (!empty($sk_change_file['name'])) {
					$filename_sk_change = strtolower(uniqid()) . '_' . strtolower(str_replace(' ', '_', $sk_change_file['name']));
					$path_old_sk_change = $sk_change_file['tmp_name'];
					$path_new_sk_change = $path . $filename_sk_change;
					move_uploaded_file($path_old_sk_change, $path_new_sk_change);
				} else {
					$filename_sk_change = $data["PERUBAHAN_FILE"];
				}

				if (!empty($sk_pengesahan_file['name'])) {
					$filename_sk_pengesahan = strtolower(uniqid()) . '_' . strtolower(str_replace(' ', '_', $sk_pengesahan_file['name']));
					$path_old_sk_pengesahan = $sk_pengesahan_file['tmp_name'];
					$path_new_sk_pengesahan = $path . $filename_sk_pengesahan;
					move_uploaded_file($path_old_sk_pengesahan, $path_new_sk_pengesahan);
				} else {
					$filename_sk_pengesahan = $data["SK_PERUBAHAN_FILE"];
				}

				if (!empty($sk_pengesahan_pendirian_file['name'])) {
					$filename_sk_pengesahan_pendirian = strtolower(uniqid()) . '_' . strtolower(str_replace(' ', '_', $sk_pengesahan_pendirian_file['name']));
					$path_old_sk_pengesahan_pendirian = $sk_pengesahan_pendirian_file['tmp_name'];
					$path_new_sk_pengesahan_pendirian = $path . $filename_sk_pengesahan_pendirian;
					move_uploaded_file($path_old_sk_pengesahan_pendirian, $path_new_sk_pengesahan_pendirian);
				} else {
					$filename_sk_pengesahan_pendirian = $data["SK_PENGESAHAN_FILE"];
				}

				$npwpsubtr_dot = str_replace(".", "", $params['npwp']);
				$npwpsubtr_strip = str_replace("-", "", $npwpsubtr_dot);

				$this->_db->delete('TEMP_PRELIMINARY', [
					'CUST_ID = ?' => $cust_id
				]);

				$this->_db->delete('TEMP_PRELIMINARY_MEMBER', [
					'CUST_ID = ?' => $cust_id
				]);

				try {
					$this->_db->beginTransaction();

					$data1 = [
						'CHANGES_ID'	=> $changes_id,
						'CUST_ID'				=> $cust_id,
						'RELATIONSHIP'			=> $params['relationship'],
						'NPWP'					=> $npwpsubtr_strip,
						'NPWP_FILE'				=> $filename_npwp,
						'PENDIRIAN_NUMBER'		=> $params['sk_number'],
						'PENDIRIAN_NUMBER_FILE'	=> $filename_sk_number,
						'PENDIRIAN_DATE'		=> $params['sk_date'] ?: $data['PENDIRIAN_DATE'],
						'SK_PENGESAHAN_NUMBER'	=> $params['sk_pengesahan_pendirian'],
						'SK_PENGESAHAN_FILE'	=> $filename_sk_pengesahan_pendirian,
						'SK_PENGESAHAN_DATE'	=> $params['sk_pengesahan_pendirian_date'],
						'PENDIRIAN_PLACE'		=> $params['sk_place'] ?: $data['PENDIRIAN_PLACE'],
						'PERUBAHAN_NUMBER'		=> $params['sk_change'] ?: $data['PERUBAHAN_NUMBER'],
						'PERUBAHAN_FILE'		=> ($sk_change_file['name'] != '') ? $filename_sk_change : $data['PERUBAHAN_FILE'],
						'PERUBAHAN_DATE'		=> $params['sk_change_date'] ?: $data['PERUBAHAN_FILE'],
						'SK_PERUBAHAN_NUMBER'	=> $params['sk_pengesahan'] ?: $data['SK_PERUBAHAN_NUMBER'],
						'SK_PERUBAHAN_FILE'		=> ($sk_pengesahan_file['name'] != '') ? $filename_sk_pengesahan : $data['SK_PERUBAHAN_FILE'],
						'SK_PERUBAHAN_DATE'		=> $params['sk_pengesahan_date'] ?: $data['SK_PERUBAHAN_DATE'],
						'LAST_SUGGESTED'		=> new Zend_Db_Expr('now()'),
						'LAST_SUGGESTEDBY'		=> $this->_userIdLogin
					];
					$where1 = ['CHANGES_ID = ?' => $changes_id];
					$this->_db->insert('TEMP_PRELIMINARY', $data1, $where1);

					for ($i = 0; $i < count($params['name']); $i++) {
						if ($id_files['size'][$i] > 0) {
							$filename_ktp = strtolower(uniqid()) . '_' . strtolower(str_replace(' ', '_', $ktp_file['name'][$i]));
							$path_old_ktp = $ktp_file['tmp_name'][$i];
							$path_new_ktp = $path . $filename_ktp;
							move_uploaded_file($path_old_ktp, $path_new_ktp);
						} else {
							$filename_ktp = $params['file_id'][$i];
							if ($params['entity_id'][$i] == 'M') $filename_ktp = '-';
						}

						if ($params['entity_id'][$i] == 'B') {
							$idType = $params['citizenship'][$i] == 2 ? 'Dokumen Legal' : 'NPWP';
						} elseif ($params['entity_id'][$i] == 'M') {
							$idType = '-';
						} elseif ($params['citizenship'][$i] == 1) {
							$idType = 'KTP';
						} elseif ($params['citizenship'][$i] == 2) {
							$idType = 'Paspor';
						}


						$data2 = [
							'CHANGES_ID'	=> $changes_id,
							'CUST_ID'		=> $cust_id,
							'NAME'			=> $params['name'][$i],
							'ENTITY_ID'		=> $params['entity_id'][$i],
							'CITIZENSHIP'	=> $params['citizenship'][$i],
							'POSITION'		=> $params['position'][$i],
							'SHARE'			=> $params['share'][$i],
							// 'STATUS'		=> $params['status'][$i],
							'STATUS'		=> 1,
							'ID_TYPE'		=> ($params['entity_id'][$i] == 'M') ? '-' : $idType,
							'ID_NUMBER'		=> ($params['entity_id'][$i] == 'M') ? '-' : ($params['ktp'][$i] ?: '-'),
							'ID_FILE'		=> $filename_ktp,
							'ADDRESS'		=> ($params['entity_id'][$i] == 'M') ? '-' : ($params['address'][$i] ?: '-'),
							'KELURAHAN'		=> ($params['entity_id'][$i] == 'M') ? '-' : ($params['subdistrict'][$i] ?: '-'),
							'KECAMATAN'		=> ($params['entity_id'][$i] == 'M') ? '-' : ($params['district'][$i] ?: '-'),
							'KAB_CITY'		=> ($params['entity_id'][$i] == 'M') ? '-' : ($params['city'][$i] ?: '-')
						];
						$where2 = ['ID = ?' => $params['id_member'][$i]];
						$this->_db->insert('TEMP_PRELIMINARY_MEMBER', $data2, $where2);
					}

					$data3  = [
						'CHANGES_STATUS' => 'WA',
						'READ_STATUS'	=> 2,
						'LASTUPDATED'	=> new Zend_Db_Expr('now()')
					];
					$where3 = ['CHANGES_ID = ?' => $changes_id];
					$this->_db->update('T_GLOBAL_CHANGES', $data3, $where3);

					$this->_db->commit();

					Application_Helper_General::writeLog('RPDC', 'Perbaiki Usulan Perubahan Dokumen Awal');
					$this->_redirect('/popup/successpopup/index');
				} catch (Exception $error) {
					$this->_db->rollBack();
					echo '<pre>';
					print_r($error->getMessage());
					echo '</pre><br>';
					die;
				}
			}
		}
	}
}
