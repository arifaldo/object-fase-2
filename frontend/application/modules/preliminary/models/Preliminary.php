<?php

class preliminary_Model_Preliminary
{
	protected $_db;

	public function __construct()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
	}

	public function getPreliminary($cust_id)
	{
		$select = $this->_db->select()
			->from(array('MP' => 'M_PRELIMINARY'))
			->joinLeft(
				array('MC' => 'M_CUSTOMER'),
				'MC.CUST_ID = MP.CUST_ID',
				array('CUST_NAME')
			)
			->where('MP.CUST_ID = ?', $cust_id);
		return $this->_db->fetchRow($select);
	}

	public function getTempPreliminary($cust_id)
	{
		$select = $this->_db->select()
			->from(array('TP' => 'TEMP_PRELIMINARY'))
			->joinLeft(
				array('MC' => 'M_CUSTOMER'),
				'MC.CUST_ID = TP.CUST_ID',
				array('CUST_NAME')
			)
			->where('TP.CUST_ID = ?', $cust_id);
		return $this->_db->fetchRow($select);
	}

	public function getPreliminaryMember($cust_id)
	{
		$select = $this->_db->select()
			->from('M_PRELIMINARY_MEMBER')
			->where('CUST_ID = ?', $cust_id);
		return $this->_db->fetchAll($select);
	}

	public function getTempPreliminaryMember($cust_id)
	{
		$select = $this->_db->select()
			->from('TEMP_PRELIMINARY_MEMBER')
			->where('CUST_ID = ?', $cust_id);
		return $this->_db->fetchAll($select);
	}

	public function getPreliminaryMemberById($id)
	{
		$select = $this->_db->select()
			->from('M_PRELIMINARY_MEMBER')
			->where('ID = ?', $id);
		return $this->_db->fetchRow($select);
	}

	public function getPreliminaryMemberByIdNumber($cust_id, $id)
	{
		$select = $this->_db->select()
			->from('M_PRELIMINARY_MEMBER')
			->where('CUST_ID = ?', $cust_id)
			->where('ID_NUMBER = ?', $id);
		return $this->_db->fetchRow($select);
	}

	public function getPosition()
	{
		$select = $this->_db->select()
			->from('M_POSITION');
		return $this->_db->fetchAll($select);
	}

	public function getRelationship()
	{
		$select = $this->_db->select()
			->from('BG_RELATIONSHIP_WITH_REPORTER');
		return $this->_db->fetchAll($select);
	}

	public function getCity()
	{
		$select = $this->_db->select()
			->from('M_CITYLIST');
		return $this->_db->fetchAll($select);
	}

	public function getCustomerById($cust_id)
	{
		$select = $this->_db->select()
			->from('M_CUSTOMER')
			->where('CUST_ID = ?', $cust_id);
		return $this->_db->fetchRow($select);
	}
}
