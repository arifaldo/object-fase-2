<?php
class unifiedstatement_Model_Accountstatement
{
	protected $_db;

    // constructor
	public function __construct()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
	}


	public function getUser($userId)
	{
		$result = $this->_db->fetchRow(
				$this->_db->select()
					->from(array('C' => 'M_USER'))
					->where("USER_ID = ".$this->_db->quote($userId))
			);
		return $result;
	}

	
	
	public function getProduct($plan, $code)
	{
		// print_r($code);die;
		$data = $this->_db->select()
		->from(array('A' => 'M_PRODUCT_TYPE'),array('PRODUCT_CODE','PRODUCT_NAME'))
		->where('PRODUCT_CODE = ?' , $code)
		// echo $data;die;
//		->where('PRODUCT_PLAN = ?' , $plan)
		->query()->FetchAll();
	
		return $data;
	}

	public function getCustomerAccountKurs($param)
	{
		
		$select = $this->_db->select()
				->from(array('A' => 'M_CUSTOMER_ACCT'))
				->join(array('C' => 'M_MAKERLIMIT'),'A.ACCT_NO = C.ACCT_NO',array())
				->join(array('D' => 'M_PRODUCT_TYPE'),'D.PRODUCT_CODE = A.ACCT_TYPE',array('D.PRODUCT_CODE','D.PRODUCT_NAME'))
				->joinLeft(array('B' => 'M_GROUPING'),'A.GROUP_ID = B.GROUP_ID',array('GROUP_NAME'))
				->where("A.ACCT_STATUS = 1")
				->where("C.MAXLIMIT > 0");				
				
				if(isset($param['USER_ID'])) $select->where("C.USER_LOGIN = ".$this->_db->quote($param['USER_ID']));
				if(isset($param['CUST_ID'])) $select->where("A.CUST_ID = ".$this->_db->quote($param['CUST_ID']));
				 				
				$select->order('B.GROUP_NAME DESC');
				$select->order('A.ORDER_NO ASC');
				$select->limit(1);	
			
//		echo $select; die;
		$result = $this->_db->fetchAll($select);

		return $result;
	}
	
	public function getCustomerAccount($param)
	{
// 		print_r($param);die;
		$select = $this->_db->select()
				->from(array('A' => 'M_CUSTOMER_ACCT'))
				->join(array('C' => 'M_MAKERLIMIT'),'A.ACCT_NO = C.ACCT_NO',array())
				->join(array('D' => 'M_PRODUCT_TYPE'),'D.PRODUCT_CODE = A.ACCT_TYPE',array('D.PRODUCT_CODE','D.PRODUCT_NAME'))
				->joinLeft(array('B' => 'M_GROUPING'),'A.GROUP_ID = B.GROUP_ID',array('GROUP_NAME'))
				->where("A.ACCT_STATUS = 1")
				->where("A.CUST_ID = ".$this->_db->quote($param['custid']))
// 				->where("C.USER_LOGIN = ".$this->_db->quote($this->_userIdLogin))
				->where("C.MAXLIMIT > 0")
				->order('B.GROUP_NAME DESC')
				->order('A.ORDER_NO ASC');
		
// 				print_r($select->query());die;
		$result = $this->_db->fetchAll($select);
// 		$data = $this->_db->fetchAll(
// 				$this->_db->select()
// 				->from(array('A' => 'M_CUSTOMER_ACCT'))
// 				->join(array('C' => 'M_MAKERLIMIT'),'A.ACCT_NO = C.ACCT_NO',array())
// 				->join(array('D' => 'M_PRODUCT_TYPE'),'D.PRODUCT_CODE = A.ACCT_TYPE',array('D.PRODUCT_CODE','D.PRODUCT_NAME'))
// 				->joinLeft(array('B' => 'M_GROUPING'),'A.GROUP_ID = B.GROUP_ID',array('GROUP_NAME'))
// 				->where("A.ACCT_STATUS = 1")
// 				->where("A.CUST_ID = ".$this->_db->quote($param['custid']))
// 				->where("C.USER_LOGIN = ".$this->_db->quote($this->acctNo))
// 				->where("C.MAXLIMIT > 0")
// 				->order('B.GROUP_NAME DESC')
// 				->order('A.ORDER_NO ASC')
// 		);

 // print_r($result);die;
		return $result;
	}

	public function getCustAccount($param)
	{
		$select = $this->_db->select()
		->from(array('A' => 'M_CUSTOMER_ACCT'))
		->join(array('C' => 'M_MAKERLIMIT'),'A.ACCT_NO = C.ACCT_NO',array())
		->join(array('D' => 'M_PRODUCT_TYPE'),'D.PRODUCT_CODE = A.ACCT_TYPE',array('D.PRODUCT_CODE','D.PRODUCT_NAME'))
		->where("A.ACCT_STATUS = 1")
		->where("C.MAXLIMIT > 0");
		if(isset($param['userId'])) $select->where("C.USER_LOGIN = ".$this->_db->quote($param['userId']));
		if(isset($param['acctNo'])) $select->where("C.ACCT_NO = ".$this->_db->quote($param['acctNo']));
		if(isset($param['acctType'])) $select->where("A.ACCT_TYPE = ".$param['acctType']);
		$select->order('C.USER_LOGIN ASC');
		 //		print_r($select->query());die;
		$result = $this->_db->fetchAll($select);
		return $result;
	}

	public function getUserAccount($param)
	{
		$select = $this->_db->select()
			->from(array('A' => 'M_USER_ACCT'))
			->join(array('C' => 'M_MAKERLIMIT'),'A.ACCT_NO = C.ACCT_NO',array())
			->join(array('D' => 'M_PRODUCT_TYPE'),'D.PRODUCT_CODE = A.ACCT_TYPE',array('D.PRODUCT_CODE','D.PRODUCT_NAME'))
			->where("A.ACCT_STATUS = 1")
			->where("C.MAXLIMIT > 0");
		if(isset($param['userId'])) $select->where("C.USER_LOGIN = ".$this->_db->quote($param['userId']));
		if(isset($param['acctNo'])) $select->where("C.ACCT_NO = ".$this->_db->quote($param['acctNo']));
		if(isset($param['acctType'])) $select->where("A.ACCT_TYPE = ".$param['acctType']);
		$select->order('C.USER_LOGIN ASC');
 		//print_r($select->query());die;
		$result = $this->_db->fetchAll($select);
		return $result;
	}
}