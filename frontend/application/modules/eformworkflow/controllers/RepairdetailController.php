<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';
require_once("Service/Account.php");

class eformworkflow_repairdetailController extends Application_Main
{

    protected $_moduleDB = 'RTF'; // masih harus diganti

    public function indexAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->systemType = $system_type;

        $conf = Zend_Registry::get('config');
        $this->_bankName = $conf['app']['bankname'];
        $this->view->masterbankname = $this->_bankName;

        $selectcomp = $this->_db->select()
            ->from(array('A' => 'M_CUSTOMER'), array('*'))
            //  ->joinLeft(array('B' => 'M_COUNTRY'),'A.COUNTRY_CODE = B.COUNTRY_CODE',array('COUNTRY_NAME'))
            ->joinRight(array('B' => 'M_CITYLIST'), 'A.CUST_CITY = B.CITY_CODE', array('B.CITY_NAME'))
            ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
            ->query()->fetchAll();
        // echo '<pre>';
        // print_r($selectcomp);
        $this->view->compinfo = $selectcomp[0];
        $this->view->owner1 = $selectcomp[0]["CUST_NAME"];
        $numb = $this->_getParam('bgnumb');

        // decrypt numb
        $setting = new Settings();
        $enc_pass = $setting->getSetting('enc_pass');
        $enc_salt = $setting->getSetting('enc_salt');
        $sessionNamespace = new Zend_Session_Namespace('Tokenenc');
        $pw_hash = md5($enc_salt . $enc_pass);
        $rand = $this->_userIdLogin . date('dHis') . $pw_hash;
        $sessionNamespace->token     = $rand;
        $this->view->token = $sessionNamespace->token;

        $sessionNamespace = new Zend_Session_Namespace('Tokenenc');
        $password = $sessionNamespace->token;
        $this->view->token = $sessionNamespace->token;


        $AESMYSQL = new Crypt_AESMYSQL();

        $BG_NUMBER     = urldecode($this->_getParam('bgnumb'));

        $BG_NUMBER = $AESMYSQL->decrypt($BG_NUMBER, $password);

        $numb = $BG_NUMBER;

        $get_ccy_exist = $this->_db->select()
            ->from("M_MINAMT_CCY", ["CCY_ID"])
            ->query()->fetchAll();

        $this->view->ccy_exist = $get_ccy_exist;

        $Settings = new Settings();

        $toc_ind = $Settings->getSetting('ftemplate_bg_ind');
        $this->view->toc_ind = $toc_ind;
        $toc_eng = $Settings->getSetting('ftemplate_bg_eng');
        $this->view->toc_eng = $toc_eng;

        $params = $this->_request->getParams();
        if (!empty($numb)) {

            // Guaranted Transaction -----------------------

            $getGuarantedTransanctions = $this->_db->select()
                ->from('TEMP_BANK_GUARANTEE_UNDERLYING')
                ->where('BG_REG_NUMBER = ?', $numb)
                ->query()->fetchAll();

            $this->view->guarantedTransanctions = $getGuarantedTransanctions;

            $conf = Zend_Registry::get('config');

            $bgdocType         = $conf["bgdoc"]["type"]["desc"];
            $bgdocCode         = $conf["bgdoc"]["type"]["code"];

            $arrbgdoc = array_combine(array_values($bgdocCode), array_values($bgdocType));
            $this->view->arrbgdoc = $arrbgdoc;

            // end Guaranted Transaction -------------------

            $bgdata = $this->_db->select()
                ->from(array('A' => 'TEMP_BANK_GUARANTEE'), array('*'))
                ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
                ->where('A.BG_REG_NUMBER = ?', $numb)
                ->where('A.BG_STATUS IN (4, 11, 19)')
                ->query()->fetchAll();


            if (in_array($bgdata[0]['CHANGE_TYPE'], [1, 2])) {
                $this->repairAmendment($numb);
                $this->render('amandment');

                return true;
            }

            $tbgdata = $this->_db->select()
                ->from(["A" => "TEMP_BANK_GUARANTEE"], ["*"])
                ->joinLeft(array('B' => 'M_CUSTOMER'), 'A.BG_INSURANCE_CODE = B.CUST_ID', array('B.CUST_NAME'))
                ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
                ->where('A.BG_NUMBER = ?', $bgdata[0]['BG_OLD'] ? $bgdata[0]['BG_OLD'] : '')
                ->query()->fetch();

            $bgdatadetail = $this->_db->select()
                ->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL'), array('*'))
                ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
                ->where('A.BG_REG_NUMBER = ?', $numb)
                ->query()->fetchAll();

            $tbgdatadetail = $this->_db->select()
                ->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL'), array('*'))
                ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
                ->where('A.BG_REG_NUMBER = ?', $tbgdata['BG_REG_NUMBER'] ? $tbgdata['BG_REG_NUMBER'] : '')
                ->query()->fetchAll();

            $get_others_attachment = $this->_db->select()
                ->from("TEMP_BANK_GUARANTEE_FILE")
                ->where("BG_REG_NUMBER = ?", $numb)
                ->query()->fetchAll();

            $this->view->get_others_attachment = $get_others_attachment;


            $this->view->currency = $bgdatadetail[array_search("Currency", array_column($bgdatadetail, "PS_FIELDNAME"))]["PS_FIELDVALUE"];
            //$datas = $this->_request->getParams();
            // var_dump($datas);

            // echo "<BR><code>bgdata = $bgdata</code><BR>"; die;

            if (!empty($bgdata)) {

                $getGuarantedTransanctionsT = $this->_db->select()
                    ->from('TEMP_BANK_GUARANTEE_UNDERLYING')
                    ->where('BG_REG_NUMBER = ?', $tbgdata['BG_REG_NUMBER'] ? $tbgdata['BG_REG_NUMBER'] : '')
                    ->query()->fetchAll();

                $this->view->guarantedTransanctionsT = $getGuarantedTransanctionsT;


                // get cash collateral

                $get_cash_collateral = $this->_db->select()
                    ->from("M_CHARGES_OTHER", ["CHARGES_PCT", "CHARGES_ADM", "CHARGES_STAMP"])
                    ->where("CUST_ID = ?", "GLOBAL")
                    ->where("CHARGES_TYPE = ?", "10")
                    ->query()->fetchAll();

                $this->view->cash_collateral = $get_cash_collateral[0];

                // end get cash collateral

                $REPAIR_NOTE = $this->_db->select()
                    ->from(
                        array('D' => 'T_BANK_GUARANTEE_HISTORY'),
                        array(
                            'BG_REASON'      => 'D.BG_REASON'
                        )
                    )
                    ->where('D.HISTORY_STATUS IN (3, 10, 15)')
                    ->where("D.BG_REG_NUMBER = ?", $numb)
                    ->limit(1)
                    ->order('D.DATE_TIME DESC')
                    ->query()->fetchAll();

                $this->view->reason = $REPAIR_NOTE[0]["BG_REASON"];

                // get linefacillity
                $paramLimit = array();

                $paramLimit['CUST_ID'] =  $this->_custIdLogin;
                $paramLimit['COUNTER_WARRANTY_TYPE'] = 2;
                $getLineFacility = Application_Helper_General::getLineFacility($paramLimit);


                $this->view->current_limit = $getLineFacility['currentLimit'];
                $this->view->max_limit =  $getLineFacility['plafondLimit'];

                //$this->view->linefacility = $get_linefacility[0];

                // end get linefacility

                $bgdatasplit = $this->_db->select()
                    ->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
                    ->joinLeft(['B' => 'M_CUSTOMER_ACCT'], 'A.ACCT = B.ACCT_NO AND B.CUST_ID = ' . $this->_db->quote($this->_custIdLogin) . '', ["ACCT_DESC2" => "B.ACCT_DESC", "ACCT_TYPE2" => "B.ACCT_TYPE", 'CUST_ID'])
                    // ->joinLeft(["MCA" => "M_CUSTOMER_ACCT"], "MCA.ACCT_NO = A.ACCT", ["ACCT_TYPE2" => "MCA.ACCT_TYPE", "ACCT_DESC2" => "MCA.ACCT_DESC"])
                    // ->where('MCA.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
                    ->where('A.BG_REG_NUMBER = ?', $numb)
                    ->query()->fetchAll();
                $this->view->bgdatasplit = $bgdatasplit;

                // echo '<pre>';
                // var_dump($bgdatasplit);
                // die;

                if ($bgdata['0']['COUNTER_WARRANTY_TYPE'] == '1') {
                    $bgdatasplitA = $this->_db->select()
                        ->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
                        ->where('A.BG_REG_NUMBER = ?', $numb)
                        ->query()->fetchAll();

                    foreach ($bgdatasplitA as $key => $value) {
                        $temp_save = $this->_db->select()
                            ->from("M_CUSTOMER_ACCT")
                            ->where("ACCT_NO = ?", $value["ACCT"])
                            ->query()->fetchAll();

                        $bgdatasplitA[$key]["CURRENCY"] = $temp_save[0]["CCY_ID"];
                        $bgdatasplitA[$key]["TYPE"] = $temp_save[0]["ACCT_DESC"];
                    }

                    $bgdatasplitTA = $this->_db->select()
                        ->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
                        ->where('A.BG_NUMBER = ?', $bgdata[0]['BG_OLD'] ? $bgdata[0]['BG_OLD'] : '')
                        ->query()->fetchAll();

                    foreach ($bgdatasplitTA as $key => $value) {
                        $temp_save = $this->_db->select()
                            ->from("M_CUSTOMER_ACCT")
                            ->where("ACCT_NO = ?", $value["ACCT"])
                            ->query()->fetchAll();

                        $bgdatasplitTA[$key]["CURRENCY"] = $temp_save[0]["CCY_ID"];
                        $bgdatasplitTA[$key]["TYPE"] = $temp_save[0]["ACCT_DESC"];
                    }

                    $this->view->fullmember = $bgdatasplitA[0];
                    $this->view->fullmemberT = $bgdatasplitTA[0];
                }

                $data = $bgdata['0'];
                $this->view->data = $data;
                $this->view->tbgdata = $tbgdata;


                $this->view->bankFormatNumber = $data["BG_FORMAT"];

                // if ($data['IS_AMENDMENT'] == 1) {
                //     $this->view->suggestion_type = 'Change';
                // } else if ($data['IS_AMENDMENT'] == 0) {
                //     $this->view->suggestion_type = 'New';
                // }

                // new code untuk amendemen
                $getct        = Zend_Registry::get('config');
                $ctdesc     = $getct["bg"]["changetype"]["desc"];
                $ctcode     = $getct["bg"]["changetype"]["code"];
                $suggestion_type = array_combine(array_values($ctcode), array_values($ctdesc));

                $this->view->suggestion_type = $suggestion_type;

                if (!empty($data['BG_BRANCH'])) {
                    $selectbranch = $this->_db->select()
                        ->from(array('A' => 'M_BRANCH'), array('*'))
                        ->where('A.BRANCH_CODE = ?', $data['BG_BRANCH'])
                        ->query()->fetchAll();
                    //var_dump($selectbranch[0]['BRANCH_NAME']);die;
                    $this->view->branchname = $selectbranch[0]['BRANCH_NAME'];
                }

                $this->view->updateStart = Application_Helper_General::convertDate($data['TIME_PERIOD_START'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
                $this->view->updateEnd = Application_Helper_General::convertDate($data['TIME_PERIOD_END'], $this->view->viewDateFormat, $this->view->defaultDateFormat);

                $this->view->TIME_PERIOD_START = $data['TIME_PERIOD_START'];
                $this->view->TIME_PERIOD_END = $data['TIME_PERIOD_END'];

                $formatTimePeriodStart = Application_Helper_General::convertDate($data['TIME_PERIOD_START'], 'dd-mm-yyyy', 'dd-mm-yyyy');
                $formatTimePeriodEnd = Application_Helper_General::convertDate($data['TIME_PERIOD_END'], 'dd-mm-yyyy', 'dd-mm-yyyy');

                $this->view->formatTimePeriodStart = $formatTimePeriodStart;
                $this->view->formatTimePeriodEnd = $formatTimePeriodEnd;

                // echo '<pre>';
                // var_dump($this->view->TIME_PERIOD_START);
                // var_dump(Application_Helper_General::convertDate($data['TIME_PERIOD_START'], 'dd-mm-yyyy', 'dd-mm-yyyy'));
                // die;


                // $arrStatus = array(
                //     '1' => 'Waiting for review',
                //     '2' => 'Waiting for approve',
                //     '3' => 'Waiting to release',
                //     '4' => 'Waiting for bank approval',
                //     '5' => 'Issued',
                //     '6' => 'Expired',
                //     '7' => 'Canceled',
                //     '8' => 'Claimed by applicant',
                //     '9' => 'Claimed by recipient',
                //     '10' => 'Request Repair',
                //     '11' => 'Reject'
                // );

                //$config    		= Zend_Registry::get('config');
                $config = Zend_Registry::get('config');
                $BgType         = $config["bg"]["status"]["desc"];
                $BgCode         = $config["bg"]["status"]["code"];

                $arrStatus = array_combine(array_values($BgCode), array_values($BgType));

                $this->view->arrStatus = $arrStatus;


                $comp = "'" . $this->_custIdLogin . "'";
                $acctlist = $this->_db->fetchAll(
                    $this->_db->select()
                        ->from(array('A' => 'M_APIKEY'))
                        ->join(array('C' => 'M_APICREDENTIAL'), 'A.APIKEY_ID = C.ID', array('*'))
                        ->join(array('B' => 'M_BANKTABLE'), 'B.BANK_CODE = A.BANK_CODE', array('B.BANK_NAME'))
                        // ->where('A.ACCT_STATUS = ?','5')
                        ->where("A.CUST_ID IN (" . $comp . ")")
                        ->order('A.APIKEY_ID ASC')
                    // echo $acctlist;
                );

                $account = array();
                foreach ($acctlist as $key => $value) {
                    $account[$value['ID']][$value['FIELD']] = $value['VALUE'];
                    $account[$value['ID']]['BANK_CODE'] = $value['BANK_CODE'];
                    $account[$value['ID']]['SENDER_ID'] = $value['SENDER_ID'];
                    $account[$value['ID']]['AUTH_USER'] = $value['AUTH_USER'];
                    $account[$value['ID']]['AUTH_PASS'] = $value['AUTH_PASS'];
                    $account[$value['ID']]['BANK_NAME'] = $value['BANK_NAME'];
                    $account[$value['ID']]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
                }
                // echo "<pre>";
                // var_dump($account);die;
                // $acct = array();
                $i = 0;
                foreach ($account as $key => $value) {

                    $acct[$i]['ACCT_NO'] = $value['account_number'];
                    $acct[$i]['ACCT_BANK'] = $value['BANK_NAME'];
                    $acct[$i]['BANK_CODE'] = $value['BANK_CODE'];
                    $acct[$i]['BANK_NAME'] = $value['BANK_NAME'];
                    $acct[$i]['ACCT_NAME'] = $value['account_name'];
                    $i++;
                }

                // $this->view->sourceAcct = $acct;

                // echo "<code>acct start =========</code>";
                // echo '<pre>'; 
                // print_r($acct);
                // echo "<code>acct end=========</code>";

                $arrBankFormat = array(
                    1 => 'Bank Standard',
                    2 => 'Special Format (with bank approval)'
                );

                $this->view->bankFormat = $arrBankFormat[$data['BG_FORMAT']];

                $arrLang = array(
                    1 => 'Indonesian',
                    2 => 'English',
                    3 => 'Billingual',
                );

                $this->view->BG_LANGUAGE = $data['BG_LANGUAGE'];
                $this->view->languagetext = $arrLang[$data['BG_LANGUAGE']];

                $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

                // $param = array('CCY_IN' => 'IDR', 'ACCT_NO' => $data['FEE_CHARGE_TO']);
                // $param = array('CCY_IN' => 'IDR');
                $param = array('CCY_IN in (?)' => ['IDR', 'USD']);
                $AccArr = $CustomerUser->getAccountsBG($param);
                //}
                if (!empty($AccArr)) {
                    foreach ($AccArr as $i => $value) {

                        $AccArr[$i]['ACCT_BANK'] = $this->_bankName;
                    }
                }

                $getAllProduct = $this->_db->select()
                    ->from("M_PRODUCT")
                    ->query()->fetchAll();

                $newArr2 = [];
                foreach ($AccArr as $keyArr => $arrAcct) {

                    $accountTypeCheck = false;
                    $productTypeCheck = false;

                    $svcAccount = new Service_Account($arrAcct['ACCT_NO'], null, null, null, null, null);
                    $result = $svcAccount->inquiryAccountBalance();
                    if ($result['response_code'] == '0000') {
                        $accountType = $result['account_type'];
                        $productType = $result['product_type'];
                    } else {
                        $result = $svcAccount->inquiryDeposito();

                        $accountType = $result['account_type'];
                        $productType = $result['product_type'];
                    }

                    foreach ($getAllProduct as $key => $value) {
                        # code...
                        if ($value['PRODUCT_CODE'] == $accountType) {
                            $accountTypeCheck = true;
                        };

                        if ($value['PRODUCT_PLAN'] == $productType) {
                            $productTypeCheck = true;
                        };
                    }

                    if ($accountTypeCheck && $productTypeCheck) $newArr2[] = $arrAcct;
                }

                $this->view->AccArr = $newArr2;
                // $this->view->AccArr = $AccArr;

                $type = array('D', 'S', '20', '10');
                $param = array('ACCT_TYPE' => $type);
                //var_dump($param);die;
                //$param = array('ACCT_TYPE' => "20");
                $AccArr2 = $CustomerUser->getAccountsBG($param);

                $newArr2 = [];
                foreach ($AccArr2 as $keyArr => $arrAcct) {

                    if (strtolower($arrAcct['ACCT_TYPE']) == 't') continue;

                    $accountTypeCheck = false;
                    $productTypeCheck = false;

                    $svcAccount = new Service_Account($arrAcct['ACCT_NO'], null, null, null, null, null);
                    $result = $svcAccount->inquiryAccountBalance();
                    if ($result['response_code'] == '0000') {
                        $accountType = $result['account_type'];
                        $productType = $result['product_type'];
                    } else {
                        $result = $svcAccount->inquiryDeposito();

                        $accountType = $result['account_type'];
                        $productType = $result['product_type'];
                    }

                    foreach ($getAllProduct as $key => $value) {
                        # code...
                        if ($value['PRODUCT_CODE'] == $accountType) {
                            $accountTypeCheck = true;
                        };

                        if ($value['PRODUCT_PLAN'] == $productType) {
                            $productTypeCheck = true;
                        };
                    }

                    if ($accountTypeCheck && $productTypeCheck) $newArr2[] = $arrAcct;
                }

                $this->view->AccArr2 = $newArr2;
                // $this->view->AccArr2 = $AccArr2;

                // preliminary Member
                $selectMP = $this->_db->select()
                    ->from(array('MP' => 'M_PRELIMINARY_MEMBER'), array('*'));
                $selectMP->where('MP.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin));

                $preliminaryMemberArr = $this->_db->fetchAll($selectMP);
                $this->view->preliminaryMemberArr = $preliminaryMemberArr;

                $select_cust_linefacility = $this->_db->select()
                    ->from(
                        array('A' => 'M_CUST_LINEFACILITY'),
                        array('*')
                    )
                    ->joinLeft(
                        array('B' => 'M_CUSTOMER'),
                        'B.CUST_ID = A.CUST_ID',
                        array('CUST_NAME', 'COLLECTIBILITY_CODE')
                    )
                    ->where("A.CUST_ID = ?", $this->_custIdLogin);
                //->where();
                // $select_cust_linefacility->where("DATE(NOW()) between DATE(A.PKS_START_DATE) AND DATE(A.PKS_EXP_DATE)");
                // ->where("A.STATUS in (2,3,4)");
                //echo $select_cust_linefacility;
                $select_cust_linefacility = $this->_db->fetchRow($select_cust_linefacility);

                if (!empty($select_cust_linefacility)) {
                    $this->view->statusLF = $select_cust_linefacility["STATUS"];
                }

                //usd
                $paramUSD = array('CCY_IN' => 'USD');
                $AccArrUSD = $CustomerUser->getAccountsBG($paramUSD);
                if (!empty($AccArrUSD)) {
                    foreach ($AccArrUSD as $iUSD => $valueUSD) {
                        $AccArrUSD[$iUSD]['ACCT_BANK'] = $this->_bankName;
                    }
                }

                $this->view->AccArrUSD = $AccArrUSD;


                if (!empty($AccArr)) {
                    $this->view->src_name = $AccArr['0']['ACCT_NAME'];
                }

                $arrWaranty = array(
                    1 => 'Full Cover, Savings Account (Giro / Time Deposit / Savings) or Cash Deposit (MD)',
                    2 => 'Indirect Credit Facility (Non Cash Loan) / Bank Guarantee Ceiling',
                    3 => 'Insurance'

                );

                $this->view->warranty_type_text = $arrWaranty[$data['COUNTER_WARRANTY_TYPE']];

                if (!empty($data['USAGE_PURPOSE'])) {
                    $data['USAGE_PURPOSE'] = explode(',', $data['USAGE_PURPOSE']);
                    foreach ($data['USAGE_PURPOSE'] as $key => $val) {
                        $str = 'checkp' . $val;
                        //var_dump($str);
                        $this->view->$str =  'checked';
                    }
                }

                if ($data['BG_NUMBER'] == '') {
                    $data['BG_NUMBER'] = '-';
                }
                if ($data['BG_SUBJECT'] == '') {
                    $data['BG_SUBJECT'] = '- no subject -';
                }


                // echo '<pre>';
                // print_r($conf);

                // BG TYPE
                $bgType         = $conf["bg"]["type"]["desc"];
                $bgCode         = $conf["bg"]["type"]["code"];

                $arrbgType = array_combine(array_values($bgCode), array_values($bgType));

                foreach ($arrbgType as $key => $val) {
                    $arrbgType_new[$key] =  array(
                        'ID' => $key,
                        'VALUE' => $val,
                    );
                }
                $list_bg_purpose                = array('' => ' --- ' . $this->language->_('Choose One') . ' ---');
                $list_bg_purpose                += Application_Helper_Array::listArray($arrbgType_new, 'ID', 'VALUE');
                $this->view->bgpurposeArr       = $list_bg_purpose;
                $this->view->BG_PURPOSE = $data['USAGE_PURPOSE'];

                // BG TYPE
                $bgType         = $conf["bg"]["type"]["desc"];
                $bgCode         = $conf["bg"]["type"]["code"];

                $arrbgType = array_combine(array_values($bgCode), array_values($bgType));

                $arrbgType["F4299"] = ["Jaminan Pembayaran", "Jaminan Pemeliharaan", "Lainnya (SP2D)"];

                $this->view->arrbgType = $arrbgType;

                //BG Document Type
                $bgdocType         = $conf["bgdoc"]["type"]["desc"];
                $bgdocCode         = $conf["bgdoc"]["type"]["code"];

                $arrbgdoc = array_combine(array_values($bgdocCode), array_values($bgdocType));

                foreach ($arrbgdoc as $key => $val) {
                    $arrbgdoc_new[$key] =  array(
                        'ID' => $key,
                        'VALUE' => $val,
                    );
                }
                $list_document_type                = array('' => ' --- ' . $this->language->_('Choose One') . ' ---');
                $list_document_type                += Application_Helper_Array::listArray($arrbgdoc_new, 'ID', 'VALUE');
                $this->view->documenttypeArr     = $list_document_type;
                $this->view->GT_DOC_TYPE =  $data['GT_DOC_TYPE'];


                //BG Counter Guarantee Type
                $bgcgType         = $conf["bgcg"]["type"]["desc"];
                $bgcgCode         = $conf["bgcg"]["type"]["code"];

                $arrbgcg = array_combine(array_values($bgcgCode), array_values($bgcgType));

                foreach ($arrbgcg as $key => $val) {
                    $arrbgcg_new[$key] =  array(
                        'ID' => $key,
                        'VALUE' => $val,
                    );
                }
                $list_warranty_type                = array('' => ' --- ' . $this->language->_('Choose One') . ' ---');
                $list_warranty_type                += Application_Helper_Array::listArray($arrbgcg_new, 'ID', 'VALUE');
                $this->view->warrantytypeArr     = $list_warranty_type;
                // $this->view->COUNTER_WARRANTY_TYPE = $data['COUNTER_WARRANTY_TYPE'];


                $this->view->currencyArr = array(
                    '1' => $this->language->_('IDR'),
                    '2' => $this->language->_('USD'),
                );

                // BG Publishing Form
                $bgpublishType         = $conf["bgpublish"]["type"]["desc"];
                $bgpublishCode         = $conf["bgpublish"]["type"]["code"];

                $arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));
                foreach ($arrbgpublish as $key => $val) {
                    $arrbgpublish_new[$key] =  array(
                        'ID' => $key,
                        'VALUE' => $val,
                    );
                }
                $list_bg_publist                = array('' => ' --- ' . $this->language->_('Any Value') . ' ---');
                $list_bg_publist                += Application_Helper_Array::listArray($arrbgpublish_new, 'ID', 'VALUE');
                $this->view->BG_PUBLISH_ARR     = $list_bg_publist;



                $Settings = new Settings();
                $claim_period = $Settings->getSetting('max_claim_period');

                $this->view->maxCalendarLimit = $claim_period;

                // echo '<pre>';
                // print_r($list_bg_publist); 
                $this->view->BG_PUBLISH = $data['BG_PUBLISH'];

                $this->view->BG_FORMAT = $data['BG_FORMAT'];

                $this->view->BG_REG_NUMBER = $data['BG_REG_NUMBER'];
                $this->view->BG_NUMBER = $data['BG_NUMBER'];
                $this->view->BG_AMOUNT = $data['BG_AMOUNT'];

                $this->view->BG_SUBJECT = $data['BG_SUBJECT'];

                $this->view->BG_CLAIM_PERIOD = $data['BG_CLAIM_PERIOD'];
                $this->view->BG_UNDERLYING_DOC = $data['BG_UNDERLYING_DOC'];

                $this->view->CUST_CP = $data['CUST_CP'];
                $this->view->CUST_EMAIL = $data['CUST_EMAIL'];
                $this->view->CUST_CP = $data['CUST_CP'];
                $this->view->CUST_EMAIL = $data['CUST_EMAIL'];
                $this->view->CUST_CONTACT_NUMBER = $data['CUST_CONTACT_NUMBER'];


                $this->view->recipent_name = $data['RECIPIENT_NAME'];
                $this->view->address = $data['RECIPIENT_ADDRES'];
                $this->view->city = $data['RECIPIENT_CITY'];

                $this->view->RECIPIENT_OFFICE_NUMBER = $data['RECIPIENT_OFFICE_NUMBER'];
                $this->view->RECIPIENT_CP = $data['RECIPIENT_CP'];
                $this->view->RECIPIENT_CONTACT = $data['RECIPIENT_CONTACT'];
                $this->view->RECIPIENT_EMAIL = $data['RECIPIENT_EMAIL'];

                $this->view->SERVICE = $data['SERVICE'];
                $this->view->GT_DOC_NUMBER = $data['GT_DOC_NUMBER'];
                $this->view->GT_DOC_DATE = $data['GT_DOC_DATE'];

                $this->view->BG_CLAIM_PERIOD = $data['BG_CLAIM_PERIOD'];
                $this->view->BG_UNDERLYING_DOC = $data['BG_UNDERLYING_DOC'];

                // $this->view->contact_number = $data['RECIPIENT_CONTACT'];
                $this->view->comment = $data['GUARANTEE_TRANSACTION'];

                $this->view->fileName = $data['FILE'];
                $this->view->bank_amount = $data['BG_AMOUNT'];
                $this->view->warranty_type = $data['COUNTER_WARRANTY_TYPE'];
                $this->view->account_number = $data['COUNTER_WARRANTY_ACCT_NO'];
                $this->view->account_name = $data['COUNTER_WARRANTY_ACCT_NAME'];

                $this->view->amount = $data['COUNTER_WARRANTY_AMOUNT'];
                $this->view->acct = $data['FEE_CHARGE_TO'];
                $this->view->status = $data['BG_STATUS'];
                $this->view->TIME_PERIOD_START = $data['TIME_PERIOD_START'];
                $this->view->TIME_PERIOD_END = $data['TIME_PERIOD_END'];


                if ($data['COUNTER_WARRANTY_TYPE'] == 3) {

                    $getDataInsurance = $this->_db->select()
                        ->from("TEMP_BANK_GUARANTEE", ["BG_INSURANCE_CODE", "BG_BRANCH"])
                        ->where("COUNTER_WARRANTY_TYPE = '3'")
                        ->where("BG_REG_NUMBER = ? ", $BG_NUMBER)
                        ->query()
                        ->fetchAll();

                    $this->view->resInsuranceCode = $getDataInsurance[0]['BG_INSURANCE_CODE'];
                    $this->view->resInsuranceBranch = $getDataInsurance[0]['BG_BRANCH'];
                } elseif ($data['COUNTER_WARRANTY_TYPE'] == 1) {
                    $getFullConver = $this->_db->select()
                        ->from("TEMP_BANK_GUARANTEE_SPLIT", ["*"])
                        ->where("BG_REG_NUMBER = ? ", $BG_NUMBER)
                        ->query()
                        ->fetchAll();

                    $this->view->getFullConver = $getFullConver;
                }

                if ($data['BG_STATUS'] == '7' ||  $data['BG_STATUS'] == '10' || !empty($data['BG_REJECT_NOTES']) || !empty($data['BG_CANCEL_NOTES'])) {
                    $selectQuery  = "SELECT
                        a.USER_LOGIN,
                        b.`USER_FULLNAME` AS u_name,
                        c.`BUSER_NAME` AS b_name,
                        a.DATE_TIME,
                        a.BG_REASON,
                        a.HISTORY_STATUS,
                        a.BG_REG_NUMBER
                        
                        
                        FROM
                        T_BANK_GUARANTEE_HISTORY AS a
                        LEFT JOIN M_USER AS b ON a.`USER_LOGIN` = b.`USER_ID` AND a.`CUST_ID` = b.`CUST_ID`
                        LEFT JOIN `M_BUSER` AS c ON a.`USER_LOGIN` = c.`BUSER_ID`
                        WHERE a.BG_REG_NUMBER = " . $this->_db->quote((string) $data['BG_REG_NUMBER']) . " AND a.HISTORY_STATUS = " . $this->_db->quote((string) $data['BG_STATUS']) . " GROUP BY HISTORY_ID ORDER BY DATE_TIME";
                    $result =  $this->_db->fetchAll($selectQuery);
                    if (!empty($result)) {
                        $data['REASON'] = $result['0']['BG_REASON'];
                    }
                    $this->view->reqrepair = true;

                    // $this->view->reason = $data['REASON'] . '' . $data['BG_REJECT_NOTES'] . $data['BG_CANCEL_NOTES'];
                }

                if (!empty($bgdatadetail)) {
                    foreach ($bgdatadetail as $key => $value) {

                        if ($data['COUNTER_WARRANTY_TYPE'] == 3) {

                            if ($value['PS_FIELDNAME'] == 'Insurance Name') {
                                $this->view->insuranceName =   $value['PS_FIELDVALUE'];

                                $getInsuranceCustomer = $this->_db->select()
                                    ->from(["A" => "M_CUSTOMER"], ["CUST_ID", "CUST_NAME"])
                                    ->joinLeft(["MCLF" => "M_CUST_LINEFACILITY"], "MCLF.CUST_ID = A.CUST_ID")
                                    ->where("A.CUST_ID = ?", $value["PS_FIELDVALUE"])
                                    // ->where("A.CUST_MODEL = '2'")
                                    // ->where("A.CUST_STATUS = '1'")
                                    // ->where("MCLF.CUST_SEGMENT = ?", "4")
                                    // ->where("MCLF.STATUS = ?", "1")
                                    ->query()->fetchAll();

                                $simpanInsurance = [];

                                $simpanInsurance[0] = ["key" => "-", "value" => "-- Choose one --"];

                                foreach ($getInsuranceCustomer as $key => $customer) {
                                    $getId = $customer["CUST_ID"];
                                    $getName = $customer["CUST_NAME"];
                                    if ($customer["TICKET_SIZE"] == "1") {
                                        $ticket_size = floatval((10 / 100) * $customer["PLAFOND_LIMIT"]);
                                    } else {
                                        $ticket_size = floatval($customer["PLAFOND_LIMIT"]);
                                    }
                                    array_push($simpanInsurance, ["key" => $getId, "value" => $getName, "ticket_size" => $ticket_size]);
                                }

                                $this->view->simpanInsurance = $simpanInsurance;
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
                                $this->view->PrincipalAgreement =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount') {
                                $this->view->insurance_amount =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement Date') {
                                $this->view->paDate =   $value['PS_FIELDVALUE'];
                            }
                        } else {

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 1') {
                                $this->view->owner1 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 1') {
                                $this->view->amountowner1 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
                                $this->view->owner2 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
                                $this->view->amountowner2 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
                                $this->view->owner3 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
                                $this->view->amountowner3 =   $value['PS_FIELDVALUE'];
                            }
                        }
                    }
                }

                if (!empty($tbgdatadetail)) {
                    foreach ($tbgdatadetail as $key => $value) {

                        if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
                            if ($value['PS_FIELDNAME'] == 'Insurance Name') {
                                $this->view->tinsuranceName =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
                                $this->view->tPrincipalAgreement =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount') {
                                $this->view->tinsurance_amount =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement Start Date') {
                                $this->view->tpaDateStart =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement End Date') {
                                $this->view->tpaDateEnd =   $value['PS_FIELDVALUE'];
                            }
                        } else {

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner') {
                                $this->view->towner1 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner') {
                                $this->view->tamountowner1 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
                                $this->view->towner2 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
                                $this->view->tamountowner2 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
                                $this->view->towner3 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
                                $this->view->tamountowner3 =   $value['PS_FIELDVALUE'];
                            }
                        }
                    }
                }



                $conf = Zend_Registry::get('config');
                $this->view->bankname = $conf['app']['bankname'];



                //echo '<pre>';
                //var_dump($data);

                $download = $this->_getParam('download');
                //print_r($edit);die;
                if ($download) {
                    $attahmentDestination = UPLOAD_PATH . '/document/submit/';
                    $this->_helper->download->file($data['FILE'], $attahmentDestination . $data['FILE']);
                }

                if ($this->_request->isPost()) {

                    $approve = $this->_getParam('approve');
                    $BG_NUMBER = $this->_getParam('bgnumb');
                    $reject = $this->_getParam('reject');
                    $repair = $this->_getParam('repair');

                    $datadump = $this->_request->getParams();
                    $newamount = str_replace(',', '', $datadump['BG_AMOUNT']);


                    // if ($datadump['currency_type'] == '1') {
                    //     $selectbankamount = $this->_db->select()
                    //         ->from(array('M_MINAMT_CCY'), array('MIN_TX_AMT'))
                    //         ->where('CCY_ID = ?', 'IDR');
                    // } else {
                    //     $selectbankamount = $this->_db->select()
                    //         ->from(array('M_MINAMT_CCY'), array('MIN_TX_AMT'))
                    //         ->where('CCY_ID = ?', 'USD');
                    // }

                    $selectbankamount = $this->_db->select()
                        ->from(array('M_MINAMT_CCY'), array('MIN_TX_AMT'))
                        ->where('CCY_ID = ?', $datadump['currency_type']);

                    $databankamount = $this->_db->fetchRow($selectbankamount);



                    //$datas = $this->_request->getParams();
                    //echo '<pre>';
                    //var_dump($datas);die;

                    $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
                    if (count($temp) > 1) {
                        if ($temp[0] == 'F' || $temp[0] == 'S') {
                            if ($temp[0] == 'F')
                                $this->view->error = 1;
                            else
                                $this->view->success = 1;
                            $msg = '';
                            unset($temp[0]);
                            foreach ($temp as $value) {
                                if (!is_array($value))
                                    $value = array($value);
                                $msg .= $this->view->formErrors($value);
                            }
                            $this->view->report_msg = $msg;
                        }
                    }
                    $attahmentDestination     = UPLOAD_PATH . '/document/submit/';
                    $errorRemark             = null;
                    $adapter                 = new Zend_File_Transfer_Adapter_Http();
                    $fileExt                 = "jpeg,jpg,pdf";

                    $sourceFileName = $adapter->getFileName("uploadSource");

                    if ($sourceFileName == null) {
                        $sourceFileName = null;
                        $fileType = null;
                    } else {
                        $sourceFileName = substr(basename($adapter->getFileName("uploadSource")), 0);
                        // echo "<code>sourceFileName = $sourceFileName</code>"; die;	
                        if ($_FILES["uploadSource"]["type"]) {
                            $adapter->setDestination($attahmentDestination);
                            $maxFileSize             = $this->getSetting('Fe_attachment_maxbyte');
                            $fileType = $_FILES["uploadSource"]['type'];
                            // $fileType = $adapter->getMimeType("uploadSource");
                            $fileInfo = $adapter->getFileInfo("uploadSource");
                            // Zend_Debug::dump($fileInfo);
                            // die();
                            $size = $_FILES["uploadSource"]["size"];
                        } else {
                            $fileType = null;
                            $size = null;
                        }
                    }


                    $filters = array(
                        'CUST_CP'                   => array('StripTags', 'StringTrim'),
                        'CUST_CONTACT_NUMBER'       => array('StripTags', 'StringTrim'),
                        'CUST_EMAIL'                => array('StripTags', 'StringTrim'),
                        'BG_PURPOSE'                => array('StripTags', 'StringTrim'),
                        'BG_AMOUNT'                 => array('StripTags', 'StringTrim'),
                        'updatedate'                => array('StripTags', 'StringTrim'),
                        'RECIPIENT_CP'              => array('StripTags', 'StringTrim'),
                        'RECIPIENT_CONTACT'         => array('StripTags', 'StringTrim'),
                        'RECIPIENT_EMAIL'           => array('StripTags', 'StringTrim'),
                        'SERVICE'                   => array('StripTags', 'StringTrim'),
                        'GT_DOC_TYPE'               => array('StripTags', 'StringTrim'),
                        'GT_DOC_NUMBER'             => array('StripTags', 'StringTrim'),
                        'GT_DOC_DATE'               => array('StripTags', 'StringTrim'),
                        'COUNTER_WARRANTY_TYPE'     => array('StripTags', 'StringTrim'),
                        'acct'                      => array('StripTags', 'StringTrim'),
                        'BG_PUBLISH'                => array('StripTags', 'StringTrim'),
                        'BG_FORMAT'                 => array('StripTags', 'StringTrim'),
                        'BG_LANGUAGE'               => array('StripTags', 'StringTrim'),

                    );


                    if ($newamount < $databankamount['MIN_TX_AMT']) {
                        $error_msg[] = $this->language->_('Error') . ': ' . $this->language->_('Amount cannot be lower than ') . $databankamount['MIN_TX_AMT'] . '.';
                        $this->view->error      = true;
                        $this->view->error_msg = $error_msg;


                        $validators =  array(
                            'CUST_CP'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'CUST_CONTACT_NUMBER'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'CUST_EMAIL'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'BG_PURPOSE'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'BG_AMOUNT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'updatedate'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'RECIPIENT_CP'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'RECIPIENT_CONTACT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'RECIPIENT_EMAIL'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            // 'SERVICE'      => array(
                            //     'NotEmpty',
                            //     array('StringLength', array('min' => 1, 'max' => 50)),
                            //     'messages' => array(
                            //         'Can not be empty',
                            //         'invalid'
                            //     )
                            // ),
                            // 'GT_DOC_TYPE'      => array(
                            //     'NotEmpty',
                            //     array('StringLength', array('min' => 1, 'max' => 50)),
                            //     'messages' => array(
                            //         'Can not be empty',
                            //         'invalid'
                            //     )
                            // ),
                            // 'GT_DOC_NUMBER'      => array(
                            //     'NotEmpty',
                            //     array('StringLength', array('min' => 1, 'max' => 50)),
                            //     'messages' => array(
                            //         'Can not be empty',
                            //         'invalid'
                            //     )
                            // ),
                            // 'GT_DOC_DATE'      => array(
                            //     'NotEmpty',
                            //     array('StringLength', array('min' => 1, 'max' => 50)),
                            //     'messages' => array(
                            //         'Can not be empty',
                            //         'invalid'
                            //     )
                            // ),
                            'COUNTER_WARRANTY_TYPE'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'acct'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'BG_PUBLISH'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'BG_FORMAT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),

                        );

                        if ($this->_request->getPost("BG_FORMAT") == 1) {
                            $validators['language'] = array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            );
                        }

                        $zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);
                    } else {

                        $validators =  array(
                            'CUST_CP'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'CUST_CONTACT_NUMBER'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'CUST_EMAIL'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'BG_PURPOSE'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'BG_AMOUNT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'updatedate'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'RECIPIENT_CP'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'RECIPIENT_CONTACT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'RECIPIENT_EMAIL'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            // 'SERVICE'      => array(
                            //     'NotEmpty',
                            //     array('StringLength', array('min' => 1, 'max' => 128)),
                            //     'messages' => array(
                            //         'Can not be empty',
                            //         'invalid'
                            //     )
                            // ),
                            // 'GT_DOC_TYPE'      => array(
                            //     'NotEmpty',
                            //     array('StringLength', array('min' => 1, 'max' => 50)),
                            //     'messages' => array(
                            //         'Can not be empty',
                            //         'invalid'
                            //     )
                            // ),
                            // 'GT_DOC_NUMBER'      => array(
                            //     'NotEmpty',
                            //     array('StringLength', array('min' => 1, 'max' => 50)),
                            //     'messages' => array(
                            //         'Can not be empty',
                            //         'invalid'
                            //     )
                            // ),
                            // 'GT_DOC_DATE'      => array(
                            //     'NotEmpty',
                            //     array('StringLength', array('min' => 1, 'max' => 50)),
                            //     'messages' => array(
                            //         'Can not be empty',
                            //         'invalid'
                            //     )
                            // ),
                            'COUNTER_WARRANTY_TYPE'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'acct'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'BG_PUBLISH'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'BG_FORMAT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),

                        );

                        if ($this->_request->getPost("BG_FORMAT") == 1) {
                            $validators['language'] = array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            );
                        }

                        $zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);

                        if ($zf_filter_input->isValid()) {

                            // echo '<pre>';
                            // print_r($params['flselect_account_manual']);die;
                            // $dd = $zf_filter_input->COUNTER_WARRANTY_TYPE;
                            // echo "<code>dd = $dd</code>"; die;
                            if ($zf_filter_input->COUNTER_WARRANTY_TYPE == '1') {

                                if (!empty($params['flselect_account_manual'])) {
                                    $splitArr = array();
                                    foreach ($params['flselect_account_manual'] as $ky => $vl) {
                                        if ($vl == '') {
                                            $nameAcct = explode('|', $params['flselect_account_number'][$ky]);
                                            $splitArr[$ky]['acct'] = $nameAcct[0];
                                            // $splitArr[$ky]['type'] = $nameAcct[0];
                                            $splitArr[$ky]['type'] = $params['type_desc'][$ky];
                                            $splitArr[$ky]['currency'] = $params['currency'][$ky];
                                        } else {
                                            $splitArr[$ky]['acct'] = $vl;
                                            $splitArr[$ky]['type'] = $params['type_desc'][$ky];
                                            $splitArr[$ky]['currency'] = $params['currency'][$ky];
                                        }

                                        if ($params['flname_manual'][$ky] == '') {
                                            $splitArr[$ky]['acct_name'] = $params['flname'][$ky];
                                        } else {
                                            $splitArr[$ky]['acct_name'] = $params['flname_manual'][$ky];
                                        }

                                        $splitArr[$ky]['amount'] = $params['flamount'][$ky];
                                        $splitArr[$ky]['flag'] = $params['flselect_own'][$ky];
                                        $splitArr[$ky]['bank_code'] = '999';


                                        // echo "<code>masuk = $params</code>"; 
                                    }
                                    $this->view->acctSplit = $splitArr;
                                }
                            }

                            if ($params['currency_type'] == "IDR") {

                                // echo "<code>masuk1 = $data</code><BR>"; die;
                                // echo '<pre>';
                                // print_r($params['flselect_own']);die;
                                // foreach($params['flselect_own'] as $i => $val)
                                // {
                                //     if($val == "1")
                                //     {
                                //         $arr_select_account_number = explode("|",$select_account_number[$i]);
                                //         $account_number .= $arr_select_account_number[0].',';
                                //         $account_name .= $arr_select_account_number[1].',';
                                //         $amount .= $params['flamount'][$i].',';
                                //     }
                                //     else if($val == "2")
                                //     {
                                //         $account_number .= $select_account_number[$i].',';
                                //         $account_name .= $flname_manual[$i].',';
                                //         $amount .= $params['flamount'][$i].',';
                                //     }
                                // }

                                // echo "<code>account_number = $account_number</code><BR>"; die;
                                // if($params['select_own'] == "1")
                                // {
                                // echo "<code>masuk1 = $data</code><BR>"; die;
                                $select_account_number = $params['flselect_account_number'];
                                $account_number = '';
                                $account_name = '';
                                $amount = '';
                                for ($i = 0; $i <= count($params['flselect_account_number']); $i++) {
                                    $arr_select_account_number = explode("|", $select_account_number[$i]);
                                    $account_number .= $arr_select_account_number[0] . ',';
                                    $account_name .= $arr_select_account_number[1] . ',';
                                    $amount .= $params['flamount'][$i] . ',';
                                }
                                $account_number = substr($account_number, 0, -1);
                                $account_name = substr($account_name, 0, -1);
                                $amount = substr($amount, 0, -1);

                                // }
                                // else
                                // {
                                //     // echo "<code>masuk2 = $data</code><BR>"; die;
                                //     $select_account_number = $params['flselect_account_manual'];
                                //     $flname_manual = $params['flname_manual'];

                                //     $account_number = '';
                                //     $account_name = '';
                                //     $amount = '';
                                //     for($i=0;$i<=count($params['flselect_account_manual']);$i++){
                                //         $account_number .= $select_account_number[$i].',';
                                //         $account_name .= $flname_manual[$i].',';
                                //         $amount .= $params['flamount'][$i].',';
                                //     }
                                //     $account_number = substr($account_number, 0, -1);
                                //     $account_name = substr($account_name, 0, -1);
                                //     $amount = substr($amount, 0, -1);
                                // }


                                $acct = $params['acct'];
                            }
                            // elseif ($params['currency_type'] == "2") {
                            //     $select_account_numberUSD = $params['select_account_numberUSD'];
                            //     for ($i = 0; $i <= count($params['select_account_numberUSD']); $i++) {
                            //         $arr_account_numberUSD = explode("|", $select_account_numberUSD[$i]);
                            //         $account_number .= $arr_account_numberUSD[0] . ',';;
                            //         $account_name .= $arr_account_numberUSD[1] . ',';;
                            //     }

                            //     $acct = $params['acctUSD'];
                            // }

                            $fileTypeMessage = explode('/', $fileType);
                            $fileType =  $fileTypeMessage[1];
                            $extensionValidator = new Zend_Validate_File_Extension(array(false, $fileExt));
                            $extensionValidator->setMessage("Extension file must be *.pdf");

                            $maxFileSize = "10240000";
                            $size = number_format($size);

                            $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $maxFileSize));
                            $sizeValidator->setMessage("File size is exceeding 1,024,000 byte(s), uploaded file is $size byte(s)");

                            $adapter->setValidators(array($extensionValidator, $sizeValidator));

                            // if (!empty($fileInfo['uploadSource']["name"]) || empty($fileInfo['uploadSource']["name"]) && $fileInfo['uploadSource']["name"] != NULL) {
                            if (!is_null($fileInfo['uploadSource']["name"])) {

                                if ($adapter->isValid("uploadSource")) {

                                    $date = date("dmy");
                                    $time = date("his");

                                    $newFileName2 = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . uniqid() . "_" . $fileInfo['uploadSource']["name"];
                                    $adapter->addFilter('Rename', $newFileName2, 'uploadSource');

                                    if ($adapter->receive("uploadSource")) {
                                        // echo "<code>masuk = $data</code>"; die;	
                                        // $file_name = $fileInfo['uploadSource']["name"];
                                        $file_name = $newFileName2;
                                        // unlink($attahmentDestination.$data['BG_UNDERLYING_DOC']); 

                                    } else {
                                        $errmsgs = $adapter->getMessages();
                                        foreach ($errmsgs as $key => $val) {
                                            $errfield = "error_" . $fieldname;
                                            $errorArray[$errfield] = $val;
                                        }
                                        $this->view->error = true;
                                        $this->view->err_msg = $errmsgs;
                                        // echo '<pre>';
                                        // echo "<code>errors1 = $errors</code>"; die;	
                                        // print_r($errmsgs);die;
                                    }
                                } else {
                                    $this->view->error = true;
                                    $errors = $this->language->_('Extension file must be *.jpg /.jpeg / .pdf');
                                    $this->view->err_msg = $errors;
                                    // echo '<pre>';
                                    // echo "<code>errors2 new= $errors</code>"; die;	
                                    // print_r($errors);die;
                                }
                            } else {
                                $file_name = $data['FILE'];
                            }

                            // check file sebelumnya 

                            $attahmentDestination = UPLOAD_PATH . '/document/submit/';
                            $files = $adapter->getFileInfo();
                            $othersAttachment = [];

                            foreach (explode(",", $datadump["save_others_attachment"]) as $key => $value) {
                                if (array_search($value, array_column($get_others_attachment, "BG_FILE")) !== false && (!empty($get_others_attachment))) {
                                    array_push($othersAttachment, $value);
                                }
                            }

                            $specialFormatFile = "";
                            foreach ($files as $file => $fileInfo) {
                                if ($file == "uploadSource") continue;

                                if ($file == "specialFormatFile") {
                                    $sizeFile = $fileInfo['size'] + 0;
                                    if ($sizeFile == 0) continue;

                                    $fileName = $fileInfo["name"];
                                    $adapter->setDestination($attahmentDestination);

                                    $date = date("dmy");
                                    $time = date("his");

                                    $newFileName2 = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . uniqid() . "_" . $fileName;

                                    $adapter->addFilter('Rename', $newFileName2, $file);
                                    // $adapter->receive($file);
                                    move_uploaded_file($_FILES["specialFormatFile"]["tmp_name"], $attahmentDestination . $newFileName2);
                                    $specialFormatFile = $newFileName2;
                                    continue;
                                }

                                if (strpos(strtolower($file), 'othersattachment') !== false) {
                                    $sizeFile = $fileInfo['size'] + 0;
                                    if ($sizeFile == 0) continue;

                                    $fileName = $fileInfo["name"];
                                    $adapter->setDestination($attahmentDestination);

                                    $date = date("dmy");
                                    $time = date("his");

                                    $newFileName2 = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . uniqid() . "_" . $fileName;

                                    $adapter->addFilter('Rename', $newFileName2, $file);
                                    $adapter->receive($file);
                                    array_push($othersAttachment, $newFileName2);
                                }
                            }

                            if (count($othersAttachment) > 0) {

                                $this->_db->beginTransaction();
                                $this->_db->delete("TEMP_BANK_GUARANTEE_FILE", ["BG_REG_NUMBER = ?" => $numb]);

                                foreach ($othersAttachment as $key => $value) {
                                    $this->_db->insert("TEMP_BANK_GUARANTEE_FILE", [
                                        "BG_REG_NUMBER" => $numb,
                                        "BG_FILE" => $value,
                                        "INDEX" => $key
                                    ]);
                                }

                                $this->_db->commit();

                                // jika file ingin dihapus

                                // $checkOnDb = $this->_db->select()
                                // ->from(["A" => "TEMP_BANK_GUARANTEE_FILE"], ["*"])
                                // ->where("BG_REG_NUMBER = '$numb'")
                                // ->query()->fetchAll();

                                // if(count($checkOnDb) > 0) {
                                //     foreach ($checkOnDb as $key => $value) {
                                //         $getFileName = $value["BG_FILE"];
                                //         unlink($attahmentDestination.$getFileName);
                                //     }
                                // }

                                // end jika file ingin dihapus 
                            }

                            // end check file sebelumnya

                            // insert guaranted transactions

                            // check guaranted ransaction
                            $saveGuarantedTransaction = $this->checkGuarantedTransaction($_FILES['uploadSource'], $_POST, $numb);
                            $this->insertToTempUnderlying($saveGuarantedTransaction, $numb);

                            try {
                                $this->_db->beginTransaction();
                                if ($zf_filter_input->BG_FORMAT == 1) {
                                    $BG_LANGUAGE = $zf_filter_input->language;
                                } else {
                                    $BG_LANGUAGE = 0;
                                }

                                if ($repair) {
                                    if ($zf_filter_input->BG_FORMAT != 2) {
                                        $specialFormatFile = "";
                                    }

                                    $status = 3;
                                    if ($selectcomp[0]['CUST_APPROVER']) $status = 2;

                                    $UpdateArr = array(
                                        // 'BG_REG_NUMBER'                     => $data['BG_REG_NUMBER'],   
                                        'CUST_CP'                           => $zf_filter_input->CUST_CP,
                                        'CUST_CONTACT_NUMBER'               => $zf_filter_input->CUST_CONTACT_NUMBER,
                                        'CUST_EMAIL'                        => $zf_filter_input->CUST_EMAIL,
                                        'BG_STATUS'                         => $status,
                                        'USAGE_PURPOSE'                     => $zf_filter_input->BG_PURPOSE,
                                        'USAGE_PURPOSE_DESC'                     => $this->_request->getPost('purpose_desc'),
                                        'BG_AMOUNT'                         => Application_Helper_General::convertDisplayMoney($zf_filter_input->BG_AMOUNT),
                                        'PROVISION_FEE' => Application_Helper_General::convertDisplayMoney($this->_getParam("provision_fee_input")),
                                        'ADM_FEE' => Application_Helper_General::convertDisplayMoney($this->_getParam("administration_fee_input")),
                                        'STAMP_FEE' => Application_Helper_General::convertDisplayMoney($this->_getParam("stamp_fee_input")),
                                        'RECIPIENT_CP'                      => $zf_filter_input->RECIPIENT_CP,
                                        'RECIPIENT_CONTACT'                 => $zf_filter_input->RECIPIENT_CONTACT,
                                        'RECIPIENT_EMAIL'                   => $zf_filter_input->RECIPIENT_EMAIL,

                                        'SERVICE'                           => $zf_filter_input->SERVICE,
                                        'GT_DOC_TYPE'                       => $zf_filter_input->GT_DOC_TYPE,
                                        'GT_DOC_NUMBER'                     => $zf_filter_input->GT_DOC_NUMBER,
                                        'GT_DOC_DATE'                       => date("Y-m-d", strtotime($zf_filter_input->GT_DOC_DATE)),
                                        //'GT_DOC_DATE'                       => $zf_filter_input->GT_DOC_DATE,
                                        'TIME_PERIOD_START'         => date("Y-m-d", strtotime($zf_filter_input->updatedate[0])), //$data['updatedate'][0],
                                        'TIME_PERIOD_END'           => date("Y-m-d", strtotime($zf_filter_input->updatedate[1])), //$data['updatedate'][1],

                                        //'TIME_PERIOD_START'                 => $zf_filter_input->updatedate[0],
                                        //'TIME_PERIOD_END'                   => $zf_filter_input->updatedate[1],
                                        'BG_CLAIM_DATE' =>   date('Y-m-d', strtotime('+' . $claim_period . ' days', strtotime($zf_filter_input->updatedate[1]))),
                                        'COUNTER_WARRANTY_TYPE'             => $zf_filter_input->COUNTER_WARRANTY_TYPE,
                                        'BG_PUBLISH'                        => $zf_filter_input->BG_PUBLISH,
                                        'BG_FORMAT'                         => $zf_filter_input->BG_FORMAT,
                                        'BG_LANGUAGE'                       => $BG_LANGUAGE,
                                        'COUNTER_WARRANTY_ACCT_NO'          => rtrim($account_number, ","),
                                        'COUNTER_WARRANTY_ACCT_NAME'        => rtrim($account_name, ","),
                                        'COUNTER_WARRANTY_AMOUNT'           => Application_Helper_General::convertDisplayMoney(rtrim($amount, ",")),
                                        'FEE_CHARGE_TO'                     => $zf_filter_input->acct,

                                        'BG_UNDERLYING_DOC'                 => $file_name,
                                        'FILE'                              => $file_name,
                                        'BG_CREATED'                        => new Zend_Db_Expr('now()'),
                                        'BG_CREATEDBY'                      => $this->_userIdLogin,
                                        'BG_UPDATED'                        => new Zend_Db_Expr('now()'),
                                        'BG_UPDATEDBY'                      => $this->_userIdLogin,
                                        // 'IS_AMENDMENT'                      => 0,
                                        'SPECIAL_FORMAT_DOC' => $specialFormatFile

                                    );

                                    $whereArr['BG_REG_NUMBER = ?'] = $numb;
                                    // echo '<pre>';
                                    // print_r($UpdateArr);die;
                                    $this->_db->update('TEMP_BANK_GUARANTEE', $UpdateArr, $whereArr);

                                    if ($zf_filter_input->COUNTER_WARRANTY_TYPE == '1') {
                                        // echo "<code>COUNTER_WARRANTY_TYPE_TXT = 1</code>"; die;	   
                                        $this->_db->delete('TEMP_BANK_GUARANTEE_SPLIT', ['BG_REG_NUMBER = ?' => $numb]);

                                        foreach ($splitArr as $ky => $vl) {
                                            $tmparrDetail = array(
                                                'BG_REG_NUMBER'         => $numb,
                                                'ACCT'              => $vl['acct'],
                                                'BANK_CODE'         => $vl['bank_code'],
                                                'NAME'              => $vl['acct_name'],
                                                'AMOUNT'            => Application_Helper_General::convertDisplayMoney(rtrim($vl['amount'], ",")),
                                                'FLAG'              => $vl['flag'],
                                                'ACCT_DESC'         => $vl['type'],
                                                'CCY_ID'            => $vl['currency']
                                            );
                                            // echo '<pre>';
                                            // print_r($tmparrDetail);die;

                                            $this->_db->insert('TEMP_BANK_GUARANTEE_SPLIT', $tmparrDetail);
                                        }
                                    }

                                    if ($zf_filter_input->COUNTER_WARRANTY_TYPE == '3') {
                                        // echo "<code>COUNTER_WARRANTY_TYPE_TXT = 1</code>"; die;	   
                                        $this->_db->delete('TEMP_BANK_GUARANTEE_DETAIL', ['BG_REG_NUMBER = ?' => $numb]);

                                        $arrDetail = array(
                                            'BG_REG_NUMBER' => $data['BG_REG_NUMBER'],
                                            'CUST_ID' => $data['CUST_ID'],
                                            'USER_ID' =>  $this->_userIdLogin,
                                            'PS_FIELDNAME' => 'Insurance Name',
                                            'PS_FIELDTYPE' => 1,
                                            'PS_FIELDVALUE' => $this->_request->getParam("insuranceName")
                                        );
                                        $this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL', $arrDetail);

                                        $arrDetail = array(
                                            'BG_REG_NUMBER' => $data['BG_REG_NUMBER'],
                                            'CUST_ID' => $data['CUST_ID'],
                                            'USER_ID' =>  $this->_userIdLogin,
                                            'PS_FIELDNAME' => 'Insurance Branch',
                                            'PS_FIELDTYPE' => 1,
                                            'PS_FIELDVALUE' => $this->_request->getParam("insuranceBranch")
                                        );
                                        $this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL', $arrDetail);

                                        // if ($this->_request->getParam("currency_type") == "1") {
                                        //     $Currency = "IDR";
                                        // } elseif ($data['currency_type'] == "2") {
                                        //     $Currency = "USD";
                                        // }

                                        $arrDetail = array(
                                            'BG_REG_NUMBER' => $data['BG_REG_NUMBER'],
                                            'CUST_ID' => $data['CUST_ID'],
                                            'USER_ID' =>  $this->_userIdLogin,
                                            'PS_FIELDNAME' => 'Currency',
                                            'PS_FIELDTYPE' => 1,
                                            'PS_FIELDVALUE' => $this->_request->getParam("currency_type")
                                        );

                                        $this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL', $arrDetail);
                                    }
                                }

                                $historyInsert = array(
                                    'DATE_TIME'         => new Zend_Db_Expr("now()"),
                                    'BG_REG_NUMBER'     => $numb,
                                    'CUST_ID'           => $this->_custIdLogin,
                                    'USER_LOGIN'        => $this->_userIdLogin,
                                    'HISTORY_STATUS'    => 1
                                );

                                $this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);

                                $this->_db->delete('T_APPROVAL', ['PS_NUMBER = ?' => $numb]);

                                $this->_db->commit();
                                // $this->setbackURL('/'.$this->_request->getModuleName().'/');
                                $this->setbackURL('/' . $this->_request->getModuleName() . '/repair/');
                                $this->_redirect('/notification/success');
                            } catch (Exception $e) {

                                echo '<pre>';
                                print_r($e);
                                die;
                                // echo "<code>masuk3 = $data</code><BR>"; die;	
                                $this->_db->rollBack();
                                $this->view->errorMsg = array(array($this->language->_('An Error Occured. Please Try Again.')));
                            }
                        } else {
                            // echo "<code>masuk4 = $data</code><BR>"; die;	
                            $error = $zf_filter_input->getMessages();
                            // $error = true;



                            //format error utk ditampilkan di view html 
                            $errorArray = null;
                            foreach ($error as $keyRoot => $rowError) {
                                foreach ($rowError as $errorString) {
                                    $errorArray[$keyRoot] = $errorString;
                                }
                            }
                            $this->view->report_msg = $errorArray;
                            // echo '<pre>';
                            // print_r($errorArray);die;
                        }
                        //print_r($edit);die;
                        if ($approve) {
                            $data = array('BG_STATUS' => '3');
                            $where['BG_REG_NUMBER = ?'] = $BG_NUMBER;
                            $this->_db->update('TEMP_BANK_GUARANTEE', $data, $where);

                            $historyInsert = array(
                                'DATE_TIME'         => new Zend_Db_Expr("now()"),
                                'BG_REG_NUMBER'         => $BG_NUMBER,
                                'CUST_ID'           => $this->_custIdLogin,
                                'USER_LOGIN'        => $this->_userIdLogin,
                                'HISTORY_STATUS'    => 3
                            );

                            $this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);

                            $this->setbackURL('/' . $this->_request->getModuleName() . '/review/');
                            $this->_redirect('/notification/success/index');
                        }

                        if ($reject) {
                            $data = array('BG_STATUS' => '11');
                            $where['BG_REG_NUMBER = ?'] = $BG_NUMBER;
                            $this->_db->update('TEMP_BANK_GUARANTEE', $data, $where);

                            $notes = $this->_getParam('PS_REASON_REJECT');
                            $historyInsert = array(
                                'DATE_TIME'         => new Zend_Db_Expr("now()"),
                                'BG_REG_NUMBER'         => $BG_NUMBER,
                                'CUST_ID'           => $this->_custIdLogin,
                                'USER_LOGIN'        => $this->_userIdLogin,
                                'HISTORY_STATUS'    => 11,
                                'BG_REASON'         => $notes,
                            );

                            $this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);

                            $this->setbackURL('/' . $this->_request->getModuleName() . '/review/');
                            $this->_redirect('/notification/success/index');
                        }
                    }
                    // $back = $this->_getParam('back');
                    // if($back)
                    // {
                    //     $this->_redirect('/eformworkflow/review');
                    // }


                }
            }
        }
    }

    public function cobaAction()
    {
        $total = 100;

        $Date1 = date("Y-m-d H:i:s");
        $date = new DateTime($Date1);

        while ($total > 0) {
            $date->modify('+1 day');
            $Date2 = $date->format('Y-m-d');
            $check_weekend = date("N", strtotime($Date2));

            if ($check_weekend < 6) {
                $total--;
            }
        }

        $this->_db->update("T_BANK_GUARANTEE", [
            "BG_CG_DEADLINE" => $Date2
        ], [
            "BG_REG_NUMBER = ?" => "992616101F4201"
        ]);

        var_dump($Date2);
        die();

        $test = new Service_Account('1601300020088', '');
        $info = $test->inquiryAccontInfo();
        $girosaving = $test->inquiryAccountBalance();
        $deposito = $test->inquiryDeposito();

        echo "<pre>";
        var_dump($info, $girosaving, $deposito);
        echo "</pre>";
        die();


        // $path_file = "/app/bg/library/data/temp/2022-07-28-85b4ce22-13ac-41e3-bf72-1731abe2f95f.pdf";
        $path_file = "/app/bg/library/data/temp/baru123.pdf";
        // $path_file = "/app/bg/library/data/temp/testing123.pdf";
        $filecontent = file_get_contents($path_file);

        $command = "gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.6  -dPDFSETTINGS=/prepress -dNOPAUSE -dQUIET -dBATCH -sOutputFile=/app/bg/library/data/temp/baru123.pdf " . $path_file . "";

        // gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dQUIET -dBATCH -sOutputFile=new-pdf1.5.pdf original.pdf
        // $p_result =  exec($command);

        var_dump($filecontent);
        die();

        if (preg_match("/^%PDF-1.5/", $filecontent)) {
            echo "Valid pdf";
        } else {
            echo "In Valid pdf";
        }
        // var_dump(file_exists($path_file));
        die();

        die();
        $svcCekStatus = new SGO_Soap_ClientUser();
        $svcCekStatus->callapi("inqaccount", null, [
            "account_number" => "101400135552"
            // "ref_trace" => "220728026908",
            // "trans_code" => "8774"
        ]);

        var_dump($svcCekStatus->getResult());
        die();

        // die("Hello");

        // $this->_db->update("T_BANK_GUARANTEE_SPLIT", [
        //     "AMOUNT" => round(12000000, 2)
        // ], [
        //     "BG_REG_NUMBER = '149616101F4201' AND ACCT_DESC = 'Escrow'"
        // ]);

        // die();
        // $get_temp_bank_guarantee = $this->_db->select()
        //     ->from("TEMP_BANK_GUARANTEE")
        //     ->where('DOCUMENT_ID = ? ', "96d69090-efe5-4e3b-b58f-65614bae9714")
        //     ->query()->fetch();
        // // ->where('DOCUMENT_ID = ? ', "96a9d028-47a6-4e29-b0f6-b8e2af140967");

        // unset($get_temp_bank_guarantee["NTU_COUNTING"]);
        // if (array_key_exists("SAVE_LINK", $get_temp_bank_guarantee)) {
        //     unset($get_temp_bank_guarantee["SAVE_LINK"]);
        // }

        // // $currentDate = date("ymd");
        // // $seqNumber   = mt_rand(1111, 9999);
        // // $bg_number_gen = strval($get_temp_bank_guarantee["BG_BRANCH"]) . strval($currentDate) . strval($get_temp_bank_guarantee["USAGE_PURPOSE"]) . strval($seqNumber);

        // $get_temp_bank_guarantee["BG_ISSUED"] = new Zend_Db_Expr("now()");

        // if ($get_temp_bank_guarantee["COUNTER_WARRANTY_TYPE"] == 3) {
        //     $get_detail_insurance = $this->_db->select()
        //         ->from("TEMP_BANK_GUARANTEE_DETAIL")
        //         ->where("BG_REG_NUMBER = ?", $get_temp_bank_guarantee["BG_REG_NUMBER"])
        //         ->where("PS_FIELDNAME = ?", "Insurance Name")
        //         ->query()->fetch();

        //     $get_detail_lf = $this->_db->select()
        //         ->from("M_CUST_LINEFACILITY")
        //         ->where("CUST_ID = ?", $get_detail_insurance["PS_FIELDVALUE"])
        //         ->query()->fetch();


        //     $get_temp_bank_guarantee["BG_CG_DEADLINE"] = date("Y-m-d", strtotime(date("Y-m-d") . " + " . ($get_detail_lf["DOC_DEADLINE"] - 1) . " days"));
        // }

        // $get_temp_bank_guarantee["BG_STATUS"] = "15";
        // $get_temp_bank_guarantee["COMPLETION_STATUS"] = "2";

        // $get_temp_bank_guarantee["REBATE_STATUS"] = $get_temp_bank_guarantee["REDEBATE_STATUS"];
        // $get_temp_bank_guarantee["REBATE_TYPE"] = $get_temp_bank_guarantee["REDEBATE_TYPE"];
        // $get_temp_bank_guarantee["REBATE_AMOUNT"] = $get_temp_bank_guarantee["REDEBATE_AMOUNT"];
        // unset($get_temp_bank_guarantee["REDEBATE_STATUS"]);
        // unset($get_temp_bank_guarantee["REDEBATE_TYPE"]);
        // unset($get_temp_bank_guarantee["REDEBATE_AMOUNT"]);
        // unset($get_temp_bank_guarantee["IS_TRANSFER_COA"]);
        // unset($get_temp_bank_guarantee["TRANSFER_COA_TIMEOUT"]);
        // unset($get_temp_bank_guarantee["IS_VALID"]);

        // $this->_db->insert("T_BANK_GUARANTEE", $get_temp_bank_guarantee);

        // die();
        // $svctemp = new Service_Account("1601306666109", "IDR");
        // $save_name = $svctemp->inquiryAccontInfo();
        // $balance = $svctemp->inquiryAccountBalance();

        // $svctemp1 = new Service_Account("1601306666109", "IDR");
        // $save_name1 = $svctemp1->inquiryAccontInfo()["account_name"];
        // $balance1 = $svctemp1->inquiryAccountBalance();

        // // var_dump($balance);
        // // die();

        // $temp_param = [
        //     "SOURCE_ACCOUNT_NUMBER" => "1601306666109",
        //     "SOURCE_ACCOUNT_NAME" => $save_name1,
        //     "BENEFICIARY_ACCOUNT_NUMBER" => "4801300004657",
        //     "BENEFICIARY_ACCOUNT_NAME" => $save_name,
        //     "PHONE_NUMBER_FROM" => "08123123",
        //     "AMOUNT" => "10000",
        //     "PHONE_NUMBER_TO" => "0825312312",
        //     "RATE1" => "10000000",
        //     "RATE2" => "10000000",
        //     "RATE3" => "10000000",
        //     "RATE4" => "10000000",
        //     "DESCRIPTION" => "Pemindahan Dana",
        //     "DESCRIPTION_DETAIL" => "Biaya Asuransi",

        // ];

        // $temp_service =  new Service_TransferWithin($temp_param);
        // $temp_result = $temp_service->sendTransfer();

        // var_dump($temp_result, $balance, $balance1);
        // die();

        // $checkQuota = new SGO_Soap_ClientUser();
        // $checkQuota->callapi("checkquota", null);
        // var_dump($checkQuota->getResult());
        // die();

        // $app      = Zend_Registry::get('config');
        // $bankCode = $app['app']['bankcode'];


        // $svcAccount_temp = new Service_Account("101400135170", "IDR", $bankCode, null, null, null, "EBG19");
        // // $temp_lock_deposito = $svcAccount_temp->lockDeposito(["balance" => "100000"]);
        // $temp_lock_deposito = $svcAccount_temp->inquiryDeposito();

        // var_dump($temp_lock_deposito);
        // die();


        $svcTransferCoa = new SGO_Soap_ClientUser();
        // $svcTransferCoa->callapi("inquiryrate", null, [
        //     "currency" => "IDR"
        // ]);


        $svcTransferCoa->callapi("transfercoa", null, [
            "source_account_currency" => "IDR",
            "source_account_number" => "1601306666109",
            "source_amount" => "600000", //debit amount
            "beneficiary_account_currency" => "IDR",
            "beneficiary_account_number" => "0",
            "branch_code" => "161",
            "beneficiary_amount" => "3000000", //credit amount
            "fee1" => "2000000",
            "fee2" => "1000000",
            "rate1" => "10000000",
            "rate2" => "10000000",
            "rate3" => "10000000",
            "rate4" => "10000000",
            "rate5" => "10000000",
            "remark1" => "Provision Fee",
            "remark2" => "Administration Fee",
            "remark3" => "Stamp Fee",
            "gl_account1" => "467115496250", //467115496250
            "gl_account2" => "46794840", //46794840
            "gl_account3" => "189991081011", //189991081011
            "message" => "Transfer COA"
        ]);


        header("Content-type: application/json");
        echo $svcTransferCoa->getResultPure();
        die();
    }


    public function getstatuslfAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        $custid = $this->_getParam('custid');

        // get linefacillity

        $get_linefacility = $this->_db->select()
            ->from("M_CUST_LINEFACILITY")
            ->where("CUST_ID = ?", $custid)
            ->query()->fetchAll();

        //$result[];//json_encode(["linefacility" => $get_linefacility]);
        //$result = $get_linefacility;

        echo json_encode($get_linefacility[0]); // json_encode($get_linefacility,JSON_UNESCAPED_SLASHES);
    }


    public function inquirydepositoAction()
    {

        $app = Zend_Registry::get('config');
        $app = $app['app']['bankcode'];

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $acct_no = $this->_getParam('acct_no');
        //echo $acct_no;

        $svcAccount = new Service_Account($acct_no, Application_Helper_General::getCurrNum('IDR'), $app, '1');
        $result = $svcAccount->inquiryDeposito('AB', TRUE);
        //var_dump($result);

        $data['data'] = false;
        is_array($return) ? $return :  $return = $data;
        echo json_encode($result);
    }

    public function inquiryaccountbalanceAction()
    {

        $app = Zend_Registry::get('config');
        $app = $app['app']['bankcode'];

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $acct_no = $this->_getParam('acct_no');
        //echo $acct_no;

        $svcAccount = new Service_Account($acct_no, Application_Helper_General::getCurrNum('IDR'), $app, '1');
        $result = $svcAccount->inquiryAccountBalance('AB', TRUE);
        //var_dump($result);
        //die();
        $data['data'] = false;
        is_array($return) ? $return :  $return = $data;
        echo json_encode($result);
    }

    public function inquiryaccountinfoAction()
    {

        //integrate ke core
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        $acct_no = $this->_getParam('acct_no');
        $svcAccount = new Service_Account($acct_no, null);
        $result = $svcAccount->inquiryAccontInfo('AI', TRUE);

        if ($result['response_desc'] == 'Success') //00 = success
        {
            $svcAccountCIF = new Service_Account($result['account_number'], null, null, null, null, $result['cif']);
            $result2 = $svcAccountCIF->inquiryCIFAccount();

            $filterBy = $result['account_number']; // or Finance etc.
            $new = array_filter($result2['accounts'], function ($var) use ($filterBy) {
                return ($var['account_number'] == $filterBy);
            });

            $singleArr = array();
            foreach ($new as $key => $val) {
                $singleArr = $val;
            }
        }

        if ($singleArr['type_desc'] == 'Deposito') {
            $svcAccountDeposito = new Service_Account($result['account_number'], null, null, null, null, null);
            $result3 = $svcAccountDeposito->inquiryDeposito();
        } else {
            $svcAccountBalance = new Service_Account($result['account_number'], null, null, null, null, null);
            $result3 = $svcAccountBalance->inquiryAccountBalance();
        }
        $return = array_merge($result, $singleArr, $result3);

        $data['data'] = false;
        is_array($return) ? $return :  $return = $data;


        echo json_encode($return);
    }

    public function insertToTempUnderlying($data, $bgregnumber)
    {
        foreach ($data['service'] as $key => $service) {
            try {
                $this->_db->insert('TEMP_BANK_GUARANTEE_UNDERLYING', [
                    'BG_REG_NUMBER' => $bgregnumber,
                    'DOC_NAME' => $service,
                    'DOC_TYPE' => $data['gt_doc_type'][$key],
                    'DOC_NUMBER' => $data['gt_doc_number'][$key],
                    'DOC_DATE'   => date("Y-m-d", strtotime($data['gt_doc_date'][$key])),
                    //'DOC_DATE' => $data['gt_doc_date'][$key],
                    'DOC_FILE' => $data['filename'][$key],
                ]);
            } catch (Exception $e) {
                $this->_db->rollBack();
            }
        }
    }

    public function checkGuarantedTransaction($files, $post, $bgregnumber)
    {
        $saveAll = [];
        // check file and input

        foreach ($post['SERVICE'] as $key => $file) {
            # code...
            $date = date("dmy");
            $time = date("his");
            $newFileName = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . uniqid() . '_' . $files['name'][$key];

            // destination
            $attachmentDestination   = UPLOAD_PATH . '/document/submit/';
            $extension = strtolower(pathinfo(basename($files['name'][$key]), PATHINFO_EXTENSION));

            // if file not null
            if ($files['size'][$key] > 0 && $extension == 'pdf') {
                move_uploaded_file($files['tmp_name'][$key], $attachmentDestination . $newFileName);
                $saveAll['filename'][$key] = $newFileName;
            } else {
                // $saveAll['filename'][$key] = $guarantedTransactions[$key]['DOC_FILE'];
                $saveAll['filename'][$key] = $post['check_exist_gt'][$key];
            }


            // check input
            // service
            $saveAll['service'][$key] = $post['SERVICE'][$key];

            // gt doc type
            $saveAll['gt_doc_type'][$key] = $post['GT_DOC_TYPE'][$key];

            // gt doc number
            $saveAll['gt_doc_number'][$key] = $post['GT_DOC_NUMBER'][$key];

            // gt doc date
            $saveAll['gt_doc_date'][$key] = $post['GT_DOC_DATE'][$key];
        }

        // delete data lama
        $this->_db->delete('TEMP_BANK_GUARANTEE_UNDERLYING', [
            'BG_REG_NUMBER = ?' => $bgregnumber
        ]);

        return $saveAll;
    }

    private function getCurrency()
    {
        $getCurrency = $this->_db->select()
            ->from('M_MINAMT_CCY')
            ->query()->fetchAll();

        $result = array_map(function ($item) {
            return $item['CCY_ID'];
        }, $getCurrency);

        $resultMin = array_map(function ($item) {
            return $item['MIN_TX_AMT'];
        }, $getCurrency);

        return [array_combine($result, $result), array_combine($result, $resultMin)];
    }

    private function getGuarantedTransactions($bgregnumber)
    {
        $getGuarantedTrasactions = $this->_db->select()
            ->from('TEMP_BANK_GUARANTEE_UNDERLYING')
            ->where('BG_REG_NUMBER = ?', $bgregnumber)
            ->query()->fetchAll();

        $result = $getGuarantedTrasactions;

        return $result;
    }

    public function getLampiranPendukung($bgregnumber)
    {
        $lampiranPendukungs = $this->_db->select()
            ->from('TEMP_BANK_GUARANTEE_FILE')
            ->where('BG_REG_NUMBER = ?', $bgregnumber)
            ->query()->fetchAll();

        foreach ($lampiranPendukungs as $key => $value) {
            $explodeTemp = explode('_', $value['BG_FILE']);
            $cleanText = str_replace([$explodeTemp[0], $explodeTemp[1], $explodeTemp[2], $explodeTemp[3]], '', $value['BG_FILE']);
            $trimText = trim($cleanText, '_');
            $lampiranPendukungs[$key]['NAMA_FILE'] = $trimText;
        }

        return $lampiranPendukungs;
    }

    public function repairAmendment($numb)
    {

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->maxClaimPeriod = $settings->getSetting('max_claim_period');
        $this->view->systemType = $system_type;

        $conf = Zend_Registry::get('config');
        $this->_bankName = $conf['app']['bankname'];
        $this->view->masterbankname = $this->_bankName;

        list($getCurrency, $minNominal) = $this->getCurrency();

        $this->view->currencyArr = $getCurrency;
        $this->view->minNominal = $minNominal;

        // get cash collateral
        $get_cash_collateral = $this->_db->select()
            ->from("M_CHARGES_OTHER", ["CHARGES_PCT", "CHARGES_ADM", "CHARGES_STAMP"])
            ->where("CUST_ID = ?", "GLOBAL")
            ->where("CHARGES_TYPE = ?", "10")
            ->query()->fetchAll();

        $this->view->cash_collateral = $get_cash_collateral[0];
        // end get cash collateral

        // ambil config
        $getct        = Zend_Registry::get('config');
        $ctdesc     = $getct["bg"]["changetype"]["desc"];
        $ctcode     = $getct["bg"]["changetype"]["code"];
        $this->view->suggestion_type = array_combine(array_values($ctcode), array_values($ctdesc));

        $selectcomp = $this->_db->select()
            ->from(array('A' => 'M_CUSTOMER'), array('*'))
            //  ->joinLeft(array('B' => 'M_COUNTRY'),'A.COUNTRY_CODE = B.COUNTRY_CODE',array('COUNTRY_NAME'))
            ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
            ->query()->fetchAll();

        $this->view->compinfo = $selectcomp[0];

        if (!empty($numb)) {
            $bgdata = $this->_db->select()
                ->from(array('A' => 'TEMP_BANK_GUARANTEE'), array('*'))
                ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
                ->where('A.BG_REG_NUMBER = ?', $numb)
                ->query()->fetchAll();

            $bgdatasplit = $this->_db->select()
                ->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
                ->joinLeft(['MCA' => 'M_CUSTOMER_ACCT'], 'MCA.ACCT_NO = A.ACCT AND MCA.CUST_ID = ' . $this->_db->quote($this->_custIdLogin) . '', ['ACCT_TYPE_MCA' => 'MCA.ACCT_TYPE', 'CCY_ID_MCA' => 'MCA.CCY_ID', 'ACCT_DESC_MCA' => 'MCA.ACCT_DESC'])
                ->where('A.BG_REG_NUMBER = ?', $numb)
                ->where('A.IS_TRANSFER = ?', '0')
                ->query()->fetchAll();

            $this->view->bgdatasplittopup = $bgdatasplit;

            // get previous provision
            $getPrevProv = $this->_db->select()
                ->from('T_BANK_GUARANTEE', ['PROVISION_FEE'])
                ->where('BG_NUMBER = ?', $bgdata[0]['BG_OLD'])
                ->query()->fetch();

            $this->view->prevProv = $getPrevProv['PROVISION_FEE'];

            // get BG REG NUMBER OLD
            $BG_REG_NUMBER_OLD = $this->_db->select()
                ->from('T_BANK_GUARANTEE', ['BG_REG_NUMBER', 'BG_AMOUNT'])
                ->where('BG_NUMBER = ?', $bgdata[0]['BG_OLD'])
                ->query()->fetch();

            $this->view->bg_amount_cek_limit = $BG_REG_NUMBER_OLD['BG_AMOUNT'] ?: 0;

            $AESMYSQL = new Crypt_AESMYSQL();
            $rand = $this->token;

            $encrypted = $AESMYSQL->encrypt($BG_REG_NUMBER_OLD['BG_REG_NUMBER'], uniqid());
            $this->view->BG_REG_NUMBER_OLD_ENCRYPT = urlencode($encrypted);

            // get history and notes
            $this->view->history = $this->_db->select()
                ->from('T_BANK_GUARANTEE_HISTORY')
                ->where('BG_REG_NUMBER = ?', $bgdata[0]['BG_REG_NUMBER'])
                ->order('DATE_TIME DESC')
                ->query()->fetch();

            // get guarantee transactions
            $guaranteeTrasactions = $this->getGuarantedTransactions($bgdata[0]['BG_REG_NUMBER']);
            $this->view->guaranteeTrasactions = $guaranteeTrasactions;

            $gtStatus       = $conf["bgdoc"]["type"]["desc"];
            $gtCode         = $conf["bgdoc"]["type"]["code"];

            $gtStatus = array_combine(array_values($gtCode), array_values($gtStatus));
            $this->view->gtStatus = $gtStatus;
            // end get guarantee transactions

            // lampiran dokumen pendukung

            $getLampiranPendukung = $this->getLampiranPendukung($numb);
            $this->view->getLampiranPendukung = $getLampiranPendukung;

            // end lampiran dokumen pendukung

            $bgdatadetail = $this->_db->select()
                ->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL'), array('*'))
                ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
                ->where('A.BG_REG_NUMBER = ?', $numb)
                ->query()->fetchAll();

            // pengkondisian kontra
            if ($bgdata[0]['COUNTER_WARRANTY_TYPE'] == '3') {
                $getInsuranceBranch = array_filter($bgdatadetail, function ($bgdatadetail) {
                    return strtolower($bgdatadetail['PS_FIELDNAME']) == 'insurance branch';
                });

                $getInsuranceBranch = array_shift($getInsuranceBranch)['PS_FIELDVALUE'];
                list($getInsurance, $getInsuranceBranch) = $this->getInsuranceInfo($bgdata[0]['BG_INSURANCE_CODE'], $getInsuranceBranch);

                $this->view->getInsurance = $getInsurance;
                $this->view->getInsuranceBranch = $getInsuranceBranch;

                $paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $bgdata[0]['COUNTER_WARRANTY_TYPE'];
                $paramProvisionFee['CUST_ID'] = $bgdata[0]["BG_INSURANCE_CODE"];
                $paramProvisionFee['GRUP'] = $bgdata[0]["CUST_ID"];
                $paramProvisionFee['USAGE_PURPOSE'] = $bgdata[0]['USAGE_PURPOSE'];
                $paramProvisionFee['USAGE_PURPOSE_DESC'] = $bgdata[0]['USAGE_PURPOSE_DESC'];
                $getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);

                $date_start = date_create($bgdata[0]["TIME_PERIOD_START"]);
                $date_end = date_create($bgdata[0]["TIME_PERIOD_END"]);
                $tenor = intval(date_diff($date_start, $date_end)->days) + 1;

                $prorata = $tenor / 360;

                $percentage = $getProvisionFee['provisionFee'];

                $biaya_provisi = round($prorata * ($percentage / 100) * $bgdata[0]["BG_AMOUNT"], 2);

                $getMinCharge = $this->_db->select()
                    ->from("M_CHARGES_BG", ["CHARGES_AMT"])
                    ->where("CHARGES_ID = ?", "MINPROV01")
                    ->query()->fetch();

                if (($biaya_provisi) < intval($getMinCharge["CHARGES_AMT"])) {
                    $biaya_provisi = intval($getMinCharge["CHARGES_AMT"]);
                }

                $this->view->biayaProvisi = $biaya_provisi;

                $this->view->getFee = $getProvisionFee;
            }

            if ($bgdata[0]['COUNTER_WARRANTY_TYPE'] == '1') {

                $this->view->getInsurance = $getInsurance;
                $this->view->getInsuranceBranch = $getInsuranceBranch;

                $paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $bgdata[0]['COUNTER_WARRANTY_TYPE'];
                $paramProvisionFee['CUST_ID'] = $bgdata[0]["CUST_ID"];
                $paramProvisionFee['GRUP'] = $bgdata[0]["CUST_ID"];
                $paramProvisionFee['USAGE_PURPOSE'] = $bgdata[0]['USAGE_PURPOSE'];
                $paramProvisionFee['USAGE_PURPOSE_DESC'] = $bgdata[0]['USAGE_PURPOSE_DESC'];
                $getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);

                $date_start = date_create($bgdata[0]["TIME_PERIOD_START"]);
                $date_end = date_create($bgdata[0]["TIME_PERIOD_END"]);
                $tenor = intval(date_diff($date_start, $date_end)->days) + 1;

                $prorata = $tenor / 360;

                $percentage = $getProvisionFee['provisionFee'];

                $biaya_provisi = round($prorata * ($percentage / 100) * $bgdata[0]["BG_AMOUNT"], 2);

                $getMinCharge = $this->_db->select()
                    ->from("M_CHARGES_BG", ["CHARGES_AMT"])
                    ->where("CHARGES_ID = ?", "MINPROV01")
                    ->query()->fetch();

                if (($biaya_provisi) < intval($getMinCharge["CHARGES_AMT"])) {
                    $biaya_provisi = intval($getMinCharge["CHARGES_AMT"]);
                }

                $this->view->biayaProvisi = $biaya_provisi;

                $this->view->getFee = $getProvisionFee;
            }

            if ($bgdata[0]['COUNTER_WARRANTY_TYPE'] == '2') {

                $this->view->getInsurance = $getInsurance;
                $this->view->getInsuranceBranch = $getInsuranceBranch;

                $paramProvisionFee['COUNTER_WARRANTY_TYPE'] = $bgdata[0]['COUNTER_WARRANTY_TYPE'];
                $paramProvisionFee['CUST_ID'] = $bgdata[0]["CUST_ID"];
                $paramProvisionFee['GRUP'] = $bgdata[0]["CUST_ID"];
                $paramProvisionFee['USAGE_PURPOSE'] = $bgdata[0]['USAGE_PURPOSE'];
                $paramProvisionFee['USAGE_PURPOSE_DESC'] = $bgdata[0]['USAGE_PURPOSE_DESC'];
                $getProvisionFee = Application_Helper_General::getProvisionFee($paramProvisionFee);

                $date_start = date_create($bgdata[0]["TIME_PERIOD_START"]);
                $date_end = date_create($bgdata[0]["TIME_PERIOD_END"]);
                $tenor = intval(date_diff($date_start, $date_end)->days) + 1;

                $prorata = $tenor / 360;

                $percentage = $getProvisionFee['provisionFee'];

                $biaya_provisi = round($prorata * ($percentage / 100) * $bgdata[0]["BG_AMOUNT"], 2);

                $getMinCharge = $this->_db->select()
                    ->from("M_CHARGES_BG", ["CHARGES_AMT"])
                    ->where("CHARGES_ID = ?", "MINPROV01")
                    ->query()->fetch();

                if (($biaya_provisi) < intval($getMinCharge["CHARGES_AMT"])) {
                    $biaya_provisi = intval($getMinCharge["CHARGES_AMT"]);
                }

                $this->view->biayaProvisi = $biaya_provisi;

                $this->view->getFee = $getProvisionFee;

                $paramLimit['CUST_ID'] =  $bgdata[0]["CUST_ID"];
                $paramLimit['COUNTER_WARRANTY_TYPE'] = 2;
                $getLineFacility = Application_Helper_General::getLineFacility($paramLimit);

                $this->view->getLineFacility = $getLineFacility;
            }
            // end pengkondisian kontra

            // rekening pembebanan biaya
            $callService = new Service_Account($bgdata[0]['FEE_CHARGE_TO'], '');
            $getAcctInfo = $callService->inquiryAccontInfo();
            $getAcctBalance = $callService->inquiryAccountBalance();

            $cif = $getAcctInfo['cif'];
            $serviceCif = new Service_Account('', '', '', '', '', $cif);
            $getAllAccount = $serviceCif->inquiryCIFAccount();

            if ($getAllAccount['response_code'] != '0000') {
                header('Content-Type: application/json');
                echo json_encode(['result' => 'fail inquiry']);
                return true;
            }

            $acctTrim = ltrim($bgdata[0]['FEE_CHARGE_TO'], '0');

            $selectAccount = array_shift(array_filter($getAllAccount['accounts'], function ($account) use ($acctTrim) {
                if (strpos($account['account_number'], $acctTrim) !== false) return true;
            }));

            $this->view->rekeningPembebananBiaya = array_merge($selectAccount, $getAcctBalance, $getAcctInfo);

            $bgdatasplit = $this->_db->select()
                ->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
                ->joinLeft(['MCA' => 'M_CUSTOMER_ACCT'], 'MCA.ACCT_NO = A.ACCT AND MCA.CUST_ID = ' . $this->_db->quote($this->_custIdLogin) . '', ['ACCT_TYPE_MCA' => 'MCA.ACCT_TYPE', 'CCY_ID_MCA' => 'MCA.CCY_ID', 'ACCT_DESC_MCA' => 'MCA.ACCT_DESC'])
                ->where('A.BG_REG_NUMBER = ?', $numb)
                ->query()->fetchAll();

            $bgdatasplit = array_filter($bgdatasplit, function ($acct) {
                if (strtolower($acct['ACCT_DESC']) != 'giro' && (strtolower($acct['ACCT_TYPE_MCA']) != '20' || strtolower($acct['ACCT_TYPE_MCA']) != 'd') && strtolower($acct['ACCT_DESC_MCA']) != 'giro')
                    return true;
            });

            $this->view->bgdatasplit = $bgdatasplit;

            $mdAmount = array_sum(array_column($bgdatasplit, 'AMOUNT'));
            $this->view->totalMd = $mdAmount;

            if (!empty($bgdata)) {

                $data = $bgdata['0'];
                $this->view->bgdata = $data;

                //$data = $bgdata['0'];

                if (!empty($data['BG_BRANCH'])) {
                    $selectbranch = $this->_db->select()
                        ->from(array('A' => 'M_BRANCH'), array('*'))
                        ->where('A.BRANCH_CODE = ?', $data['BG_BRANCH'])
                        ->query()->fetchAll();
                    //var_dump($selectbranch[0]['BRANCH_NAME']);die;
                    $this->view->branchname = $selectbranch[0]['BRANCH_NAME'];
                }

                $this->view->updateStart2 = $data['TIME_PERIOD_START'];
                $this->view->updateStart = Application_Helper_General::convertDate($data['TIME_PERIOD_START'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
                $this->view->updateEnd2 = $data['TIME_PERIOD_END'];
                $this->view->updateEnd = Application_Helper_General::convertDate($data['TIME_PERIOD_END'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
                // $updateEnd = Application_Helper_General::convertDate($data['TIME_PERIOD_END'],$this->view->viewDateFormat,$this->view->defaultDateFormat);


                // echo "<code>updateEnd = $updateEnd</code><BR>"; die;

                // BG status
                /*$bgStatus       = $conf["bg"]["status"]["desc"];
                $bgCode         = $conf["bg"]["status"]["code"];

                $arrStatus = array_combine(array_values($bgCode), array_values($bgStatus));
                */

                // $arrStatus = array(
                //     '0' => 'New',
                //     '1' => 'Amendment Changes',
                //     '2' => 'Amendment Draft'
                // );

                // $arrStatus = array(
                //     '7'  => 'Canceled',
                //     '20' => 'On Risk',
                //     '21' => 'Off Risk',
                //     '22' => 'Claimed On Process',
                //     '23' => 'Claimed'
                //   );

                $bgStatus       = $conf["bg"]["status"]["desc"];
                $bgCode         = $conf["bg"]["status"]["code"];
                $arrStatus = array_combine(array_values($bgCode), array_values($bgStatus));

                $this->view->arrStatus = $arrStatus;

                $comp = "'" . $this->_custIdLogin . "'";
                $acctlist = $this->_db->fetchAll(
                    $this->_db->select()
                        ->from(array('A' => 'M_APIKEY'))
                        ->join(array('C' => 'M_APICREDENTIAL'), 'A.APIKEY_ID = C.ID', array('*'))
                        ->join(array('B' => 'M_BANKTABLE'), 'B.BANK_CODE = A.BANK_CODE', array('B.BANK_NAME'))
                        // ->where('A.ACCT_STATUS = ?','5')
                        ->where("A.CUST_ID IN (" . $comp . ")")
                        ->order('A.APIKEY_ID ASC')
                    // echo $acctlist;
                );

                // echo "<pre>";
                // var_dump($acctlist);

                $account = array();
                foreach ($acctlist as $key => $value) {
                    $account[$value['ID']][$value['FIELD']] = $value['VALUE'];
                    $account[$value['ID']]['BANK_CODE'] = $value['BANK_CODE'];
                    $account[$value['ID']]['SENDER_ID'] = $value['SENDER_ID'];
                    $account[$value['ID']]['AUTH_USER'] = $value['AUTH_USER'];
                    $account[$value['ID']]['AUTH_PASS'] = $value['AUTH_PASS'];
                    $account[$value['ID']]['BANK_NAME'] = $value['BANK_NAME'];
                    $account[$value['ID']]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
                }
                // echo "<pre>";
                // var_dump($account);die;
                // $acct = array();
                $i = 0;
                foreach ($account as $key => $value) {

                    $acct[$i]['ACCT_NO'] = $value['account_number'];
                    $acct[$i]['ACCT_BANK'] = $value['BANK_NAME'];
                    $acct[$i]['BANK_CODE'] = $value['BANK_CODE'];
                    $acct[$i]['BANK_NAME'] = $value['BANK_NAME'];
                    $acct[$i]['ACCT_NAME'] = $value['account_name'];
                    $i++;
                }

                $this->view->sourceAcct = $acct;

                $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);


                $param = array('CCY_IN' => "");
                // $param = array('ACCT_TYPE' => "My Giro");
                $AccArr = $CustomerUser->getAccountsBG($param);
                //}
                if (!empty($AccArr)) {
                    foreach ($AccArr as $i => $value) {

                        $AccArr[$i]['ACCT_BANK'] = $this->_bankName;
                    }
                }

                $this->view->AccArr = $AccArr;

                //usd
                $paramUSD = array('CCY_IN' => 'USD');
                $AccArrUSD = $CustomerUser->getAccountsBG($paramUSD);
                if (!empty($AccArrUSD)) {
                    foreach ($AccArrUSD as $iUSD => $valueUSD) {
                        $AccArrUSD[$iUSD]['ACCT_BANK'] = $this->_bankName;
                    }
                }

                $this->view->AccArrUSD = $AccArrUSD;
                // echo '<pre>';
                // echo "<code>AccArrUSD = $AccArrUSD</code>";
                // print_r($AccArrUSD);

                if (!empty($AccArr)) {
                    $this->view->src_name = $AccArr['0']['ACCT_NAME'];
                }

                $arrBankFormat = array(
                    1 => 'Bank Standard',
                    2 => 'Special Format (with bank approval)'
                );

                $this->view->BG_FORMAT = $data['BG_FORMAT'];
                $this->view->bankFormat = $arrBankFormat[$data['BG_FORMAT']];
                if ($data['BG_STATUS'] == '7' || $data['BG_STATUS'] == '11' ||  $data['BG_STATUS'] == '10' || !empty($data['BG_REJECT_NOTES']) || !empty($data['BG_CANCEL_NOTES'])) {
                    $selectQuery  = "SELECT
                        a.USER_LOGIN,
                        b.`USER_FULLNAME` AS u_name,
                        c.`BUSER_NAME` AS b_name,
                        a.DATE_TIME,
                        a.BG_REASON,
                        a.HISTORY_STATUS,
                        a.BG_REG_NUMBER
                        
                        
                    FROM
                        T_BANK_GUARANTEE_HISTORY AS a
                        LEFT JOIN M_USER AS b ON a.`USER_LOGIN` = b.`USER_ID` AND a.`CUST_ID` = b.`CUST_ID`
                        LEFT JOIN `M_BUSER` AS c ON a.`USER_LOGIN` = c.`BUSER_ID`
                    WHERE a.BG_REG_NUMBER = " . $this->_db->quote((string) $data['BG_REG_NUMBER']) . " AND a.HISTORY_STATUS IN (10) GROUP BY HISTORY_ID ORDER BY DATE_TIME";
                    if ($data['BG_STATUS'] == '10') {
                        $result =  $this->_db->fetchAll($selectQuery);
                    } else {
                        $result = array();
                    }
                    if (!empty($result)) {
                        $this->view->reqrepair = true;
                        $data['REASON'] = $result['0']['BG_REASON'];
                        $this->view->username = ' (By ' . $result['0']['u_name'] . '' . $result['0']['b_name'] . ')';
                    }
                    //var_dump($result);die;


                    $this->view->reason = $data['REASON'] . '' . $data['BG_REJECT_NOTES'] . $data['BG_CANCEL_NOTES'];
                }

                $this->view->TIME_PERIOD_START = $data['TIME_PERIOD_START'];
                $this->view->TIME_PERIOD_END = $data['TIME_PERIOD_END'];



                $arrLang = array(
                    1 => 'Indonesian',
                    2 => 'English',
                    3 => 'Billingual',
                );

                $this->view->BG_LANGUAGE = $data['BG_LANGUAGE'];
                $this->view->languagetext = $arrLang[$data['BG_LANGUAGE']];

                $arrWaranty = array(
                    1 => 'Full Cover, Savings Account (Giro / Time Deposit / Savings) or Cash Deposit (MD)',
                    2 => 'Indirect Credit Facility (Non Cash Loan) / Bank Guarantee Ceiling',
                    3 => 'Insurance'

                );

                // BG TYPE
                $bgType         = $conf["bg"]["type"]["desc"];
                $bgCode         = $conf["bg"]["type"]["code"];

                $arrbgType = array_combine(array_values($bgCode), array_values($bgType));

                foreach ($arrbgType as $key => $val) {
                    $arrbgType_new[$key] =  array(
                        'ID' => $key,
                        'VALUE' => $val,
                    );
                }
                $list_bg_purpose                = array('' => ' --- ' . $this->language->_('Choose One') . ' ---');
                $list_bg_purpose                += Application_Helper_Array::listArray($arrbgType_new, 'ID', 'VALUE');
                $this->view->bgpurposeArr       = $list_bg_purpose;

                // $this->view->arrbgType = $arrbgType;
                $this->view->BG_PURPOSE = $data['USAGE_PURPOSE'];
                $this->view->BG_PURPOSE_LBL = $arrbgType[$data['USAGE_PURPOSE']];
                $this->view->BG_PURPOSE_DESC = $arrbgType[$data['USAGE_PURPOSE']];

                //BG Document Type
                $bgdocType         = $conf["bgdoc"]["type"]["desc"];
                $bgdocCode         = $conf["bgdoc"]["type"]["code"];

                $arrbgdoc = array_combine(array_values($bgdocCode), array_values($bgdocType));

                $this->view->arrBgDoc = $arrbgdoc;

                foreach ($arrbgdoc as $key => $val) {
                    $arrbgdoc_new[$key] =  array(
                        'ID' => $key,
                        'VALUE' => $val,
                    );
                }
                $list_document_type                = array('' => ' --- ' . $this->language->_('Choose One') . ' ---');
                $list_document_type                += Application_Helper_Array::listArray($arrbgdoc_new, 'ID', 'VALUE');
                $this->view->documenttypeArr     = $list_document_type;


                $this->view->GT_DOC_TYPE =  $data['GT_DOC_TYPE'];
                $this->view->GT_DOC_DESC = $arrbgdoc[$data['GT_DOC_TYPE']];
                // echo '<pre>';
                // print_r($bgdocCode); 
                // print_r($bgdocType); 
                // print_r($arrbgdoc); 
                // print_r($data['GT_DOC_TYPE']); 

                //BG Counter Guarantee Type
                $bgcgType         = $conf["bgcg"]["type"]["desc"];
                $bgcgCode         = $conf["bgcg"]["type"]["code"];

                $arrbgcg = array_combine(array_values($bgcgCode), array_values($bgcgType));
                // echo '<pre>';
                // var_dump($arrbgcg[$data['COUNTER_WARRANTY_TYPE']]);
                // die;

                foreach ($arrbgcg as $key => $val) {
                    $arrbgcg_new[$key] =  array(
                        'ID' => $key,
                        'VALUE' => $val,
                    );
                }
                $list_warranty_type                = array('' => ' --- ' . $this->language->_('Choose One') . ' ---');
                $list_warranty_type                += Application_Helper_Array::listArray($arrbgcg_new, 'ID', 'VALUE');
                $this->view->warrantytypeArr     = $list_warranty_type;


                // echo '<pre>';
                // print_r($bgcgCode); 
                // print_r($bgcgType); 
                // print_r($arrbgcg); 
                // print_r($arrbgcg_new); 
                // print_r($list_warranty_type); 
                $this->view->COUNTER_WARRANTY_TYPE = $data['COUNTER_WARRANTY_TYPE'];
                $this->view->warranty_type_text = $arrbgcg[$data['COUNTER_WARRANTY_TYPE']];

                // BG Publishing Form
                $bgpublishType         = $conf["bgpublish"]["type"]["desc"];
                $bgpublishCode         = $conf["bgpublish"]["type"]["code"];

                $arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));
                foreach ($arrbgpublish as $key => $val) {
                    $arrbgpublish_new[$key] =  array(
                        'ID' => $key,
                        'VALUE' => $val,
                    );
                }
                $list_bg_publist                = array('' => ' --- ' . $this->language->_('Any Value') . ' ---');
                $list_bg_publist                += Application_Helper_Array::listArray($arrbgpublish_new, 'ID', 'VALUE');

                // hilangkan swift
                $list_bg_publist = array_diff_key($list_bg_publist, ['3' => '']);

                $this->view->BG_PUBLISH_ARR     = $list_bg_publist;
                // echo '<pre>';
                // print_r($list_bg_publist); 
                $this->view->BG_PUBLISH = $data['BG_PUBLISH'];
                $this->view->BG_PUBLISH_DESC = $arrbgpublish[$data['BG_PUBLISH']];

                //$this->view->warranty_type_text = $arrWaranty[$data['COUNTER_WARRANTY_TYPE']];

                if (!empty($data['USAGE_PURPOSE'])) {
                    $data['USAGE_PURPOSE'] = explode(',', $data['USAGE_PURPOSE']);
                    foreach ($data['USAGE_PURPOSE'] as $key => $val) {
                        $str = 'checkp' . $val;
                        //var_dump($str);
                        $this->view->$str =  'checked';
                    }
                }

                if ($data['BG_NUMBER'] == '') {
                    $data['BG_NUMBER'] = '-';
                }
                if ($data['BG_SUBJECT'] == '') {
                    $data['BG_SUBJECT'] = '- no subject -';
                }
                $this->view->BG_REG_NUMBER = $data['BG_REG_NUMBER'];
                $this->view->BG_NUMBER = $data['BG_NUMBER'];
                $this->view->BG_SUBJECT = $data['BG_SUBJECT'];


                $this->view->recipent_name = $data['RECIPIENT_NAME'];
                $this->view->address = $data['RECIPIENT_ADDRES'];
                $this->view->city = $data['RECIPIENT_CITY'];
                $this->view->contact_number = $data['RECIPIENT_CONTACT'];
                $this->view->comment = $data['GUARANTEE_TRANSACTION'];

                $this->view->fileName = $data['FILE'];
                $this->view->bank_amount = $data['BG_AMOUNT'];
                $this->view->warranty_type = $data['COUNTER_WARRANTY_TYPE'];
                $this->view->COUNTER_WARRANTY_TYPE = $data['COUNTER_WARRANTY_TYPE'];

                $this->view->account_number = $data['COUNTER_WARRANTY_ACCT_NO'];
                $this->view->account_name = $data['COUNTER_WARRANTY_ACCT_NAME'];

                $this->view->amount = $data['COUNTER_WARRANTY_AMOUNT'];
                $this->view->acct = $data['FEE_CHARGE_TO'];
                $this->view->status = $data['BG_STATUS'];

                $this->view->CUST_EMAIL = $data['CUST_EMAIL'];

                $this->view->CUST_CP = $data['CUST_CP'];
                $this->view->CUST_EMAIL = $data['CUST_EMAIL'];
                $this->view->CUST_CONTACT_NUMBER = $data['CUST_CONTACT_NUMBER'];

                $this->view->RECIPIENT_CP = $data['RECIPIENT_CP'];
                $this->view->RECIPIENT_OFFICE_NUMBER = $data['RECIPIENT_OFFICE_NUMBER'];
                $this->view->RECIPIENT_EMAIL = $data['RECIPIENT_EMAIL'];
                $this->view->RECIPIENT_CONTACT = $data['RECIPIENT_CONTACT'];
                $this->view->SERVICE = $data['SERVICE'];

                $this->view->GT_DOC_NUMBER = $data['GT_DOC_NUMBER'];
                $this->view->GT_DOC_DATE = $data['GT_DOC_DATE'];

                $this->view->BG_CLAIM_PERIOD = $data['BG_CLAIM_PERIOD'];
                $this->view->BG_UNDERLYING_DOC = $data['BG_UNDERLYING_DOC'];

                $this->view->CHANGE_TYPE = $data['CHANGE_TYPE'];


                if (!empty($bgdatadetail)) {
                    foreach ($bgdatadetail as $key => $value) {

                        if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
                            if ($value['PS_FIELDNAME'] == 'Insurance Name') {
                                $this->view->insuranceName =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
                                $this->view->PrincipalAgreement =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount') {
                                $this->view->insurance_amount =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement Start Date') {
                                $this->view->paDateStart =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement End Date') {
                                $this->view->paDataEnd =   $value['PS_FIELDVALUE'];
                            }
                        } else {

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 1') {
                                $this->view->owner1 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 1') {
                                $this->view->amountowner1 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
                                $this->view->owner2 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
                                $this->view->amountowner2 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
                                $this->view->owner3 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
                                $this->view->amountowner3 =   $value['PS_FIELDVALUE'];
                            }
                        }

                        if (strtolower($value['PS_FIELDNAME']) == 'currency') {
                            $this->view->currency = $value['PS_FIELDVALUE'];
                        }
                    }
                }



                $conf = Zend_Registry::get('config');
                $this->view->bankname = $conf['app']['bankname'];
                // preliminary Member
                $selectMP = $this->_db->select()
                    ->from(array('MP' => 'M_PRELIMINARY_MEMBER'), array('*'));
                $selectMP->where('MP.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin));
                // echo "<code>selectMP = $selectMP</code><BR>";  
                $preliminaryMemberArr = $this->_db->fetchAll($selectMP);
                // echo '<pre>';
                // print_r($preliminaryMemberArr);
                $this->view->preliminaryMemberArr = $preliminaryMemberArr;

                $params = $this->_request->getParams();
                $toa = $params['toa'];

                $BG_UNDERLYING_DOC = $this->_getParam('BG_UNDERLYING_DOC');
                $GT_DOC_NUMBER = $this->_getParam('GT_DOC_NUMBER');
                //print_r($edit);die;

                $selectCity = $this->_db->select()
                    ->from(array('MP' => 'M_CITYLIST'), array('*'));
                $selectCityArr = $this->_db->fetchAll($selectCity);
                $searchCity = array_search($selectcomp[0]['CUST_CITY'], array_column($selectCityArr, 'CITY_CODE'));

                $this->view->arrCity = $selectCityArr[$searchCity];


                $download = $this->_getParam('download');
                if ($download) {

                    $attahmentDestination = UPLOAD_PATH . '/document/submit/';
                    if ($GT_DOC_NUMBER) {
                        $this->_helper->download->file($data['FILE'], $attahmentDestination . $data['FILE']);
                    }

                    if ($BG_UNDERLYING_DOC) {
                        // echo "<code>masuk = $GT_DOC_NUMBER</code><BR>"; die;
                        // $attahmentDestination = UPLOAD_PATH . '/document/submit/';
                        $this->_helper->download->file($data['BG_UNDERLYING_DOC'], $attahmentDestination . $data['BG_UNDERLYING_DOC']);
                    }
                }

                // Marginal Deposit View ------------------------
                // Marginal Deposit Principal ---------------
                $bgdatadetailmd = $this->_db->select()
                    ->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL'), array('*'))
                    ->where('A.BG_REG_NUMBER = ?', $numb)
                    ->query()->fetchAll();

                if (!empty($bgdatadetailmd)) {
                    foreach ($bgdatadetailmd as $key => $value) {
                        if ($value['PS_FIELDNAME'] == 'Marginal Deposit Percentage') {
                            $this->view->marginalDepositPercentage =   $value['PS_FIELDVALUE'];
                        }
                    }
                }
                // Marginal Deposit Principal ---------------

                // Marginal Deposit Eksisting ---------------
                $bgdatamdeks = $this->_db->select()
                    ->from(array('A' => 'TEMP_BANK_GUARANTEE'), array('*'))
                    ->join(array('B' => 'TEMP_BANK_GUARANTEE_SPLIT'), 'A.BG_REG_NUMBER = B.BG_REG_NUMBER')
                    ->where('B.BG_REG_NUMBER = ?', $numb)
                    ->query()->fetchAll();

                $this->view->bgdatamdeks = $bgdatamdeks;
                // Marginal Deposit Eksisting ---------------

                // Top Up Marginal Deposit ---------------
                $bgdatamd = $this->_db->select()
                    ->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
                    // ->from(array('A' => 'TEMP_BANK_GUARANTEE'), array('*'))
                    // ->join(array('B' => 'TEMP_BANK_GUARANTEE_SPLIT'), 'A.BG_REG_NUMBER = B.BG_REG_NUMBER')
                    ->where('A.BG_REG_NUMBER = ?', $numb)
                    ->query()->fetchAll();

                $this->view->bgdatamd = $bgdatamd;
                // Top Up Marginal Deposit ---------------
                // Marginal Deposit View ------------------------

                if ($this->_request->isPost()) {
                    $this->validateAll($bgdata[0], $this->_request->getParams(), $_FILES, $getInsurance);

                    if (false) {
                        if (!empty($bgdatasplit)) {
                            foreach ($bgdatasplit as $vl) {
                                //var_dump($vl);
                                $svcAccount = new Service_Account($vl['ACCT'], 'IDR');
                                $result = $svcAccount->inquiryAccountBalance('AB', TRUE);
                                if ($result['balanceactive'] <= $vl['AMOUNT']) {
                                    $err = true;
                                    $err_msg = 'not enought balance ' . $vl['ACCT'];
                                }
                            }
                        }

                        if (!empty($data['FEE_CHARGE_TO'])) {
                            $svcAccount = new Service_Account($vl['FEE_CHARGE_TO'], 'IDR');
                            $result = $svcAccount->inquiryAccountBalance('AB', TRUE);
                            if ($result['response_code'] != '0000' && $result['balance_active'] >= 0) {
                                $err = true;
                                $err_msg = 'not enought balance ' . $vl['ACCT'];
                            }
                        }
                        //die('here');

                        $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
                        if (count($temp) > 1) {
                            if ($temp[0] == 'F' || $temp[0] == 'S') {
                                if ($temp[0] == 'F')
                                    $this->view->error = 1;
                                else
                                    $this->view->success = 1;
                                $msg = '';
                                unset($temp[0]);
                                foreach ($temp as $value) {
                                    if (!is_array($value))
                                        $value = array($value);
                                    $msg .= $this->view->formErrors($value);
                                }
                                $this->view->report_msg = $msg;
                            }
                        }
                        $attahmentDestination     = UPLOAD_PATH . '/document/submit/';
                        $errorRemark             = null;
                        $adapter                 = new Zend_File_Transfer_Adapter_Http();
                        $fileExt                 = "jpeg,jpg,pdf";

                        $sourceFileName = $adapter->getFileName();

                        if ($sourceFileName == null) {
                            $sourceFileName = null;
                            $fileType = null;
                        } else {
                            $sourceFileName = substr(basename($adapter->getFileName()), 0);
                            // echo "<code>sourceFileName = $sourceFileName</code>"; die;	
                            if ($_FILES["uploadSource"]["type"]) {
                                $adapter->setDestination($attahmentDestination);
                                $maxFileSize             = $this->getSetting('Fe_attachment_maxbyte');
                                $fileType = $adapter->getMimeType();
                                $fileInfo = $adapter->getFileInfo();
                                $size = $_FILES["uploadSource"]["size"];
                            } else {
                                $fileType = null;
                                $size = null;
                            }
                        }

                        // $filters = array(
                        //     'updatedate'                        => array('StripTags','StringTrim'),   
                        // );

                        // if ($toa=='1')
                        // {
                        $filters = array(
                            'updatedate'                    => array('StripTags', 'StringTrim'),
                            'CUST_CP_TXT'                   => array('StripTags', 'StringTrim'),
                            'CUST_CONTACT_NUMBER_TXT'       => array('StripTags', 'StringTrim'),
                            'CUST_EMAIL_TXT'                => array('StripTags', 'StringTrim'),
                            'BG_PURPOSE_TXT'                => array('StripTags', 'StringTrim'),
                            'bank_amount_txt'               => array('StripTags', 'StringTrim'),
                            'currency'                      => array('StripTags', 'StringTrim'),
                            'RECIPIENT_CP_TXT'              => array('StripTags', 'StringTrim'),
                            'RECIPIENT_CONTACT_TXT'         => array('StripTags', 'StringTrim'),
                            'RECIPIENT_EMAIL_TXT'           => array('StripTags', 'StringTrim'),
                            'SERVICE_TXT'                   => array('StripTags', 'StringTrim'),
                            'GT_DOC_TYPE_TXT'               => array('StripTags', 'StringTrim'),
                            'GT_DOC_NUMBER_TXT'             => array('StripTags', 'StringTrim'),
                            'underlyingDocument'            => array('StripTags', 'StringTrim'),
                            'GT_DOC_DATE_TXT'               => array('StripTags', 'StringTrim'),
                            'acct_txt'                      => array('StripTags', 'StringTrim'),
                            'COUNTER_WARRANTY_TYPE_TXT'     => array('StripTags', 'StringTrim'),
                            'BG_PUBLISH_TXT'                => array('StripTags', 'StringTrim'),
                            'BG_FORMAT'                     => array('StripTags', 'StringTrim'),
                            'BG_LANGUAGE'                   => array('StripTags', 'StringTrim'),

                        );
                        // }

                        $validators =  array(
                            'acct_txt'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'BG_PUBLISH_TXT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'BG_FORMAT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'BG_LANGUAGE'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),

                        );

                        // echo "<code>toa = $toa</code><BR>"; die;	
                        if ($toa == '1') {

                            $validators +=  array(
                                'CUST_CP_TXT'      => array(
                                    'NotEmpty',
                                    array('StringLength', array('min' => 1, 'max' => 50)),
                                    'messages' => array(
                                        'Can not be empty',
                                        'invalid'
                                    )
                                ),
                                'CUST_CONTACT_NUMBER_TXT'      => array(
                                    'NotEmpty',
                                    array('StringLength', array('min' => 1, 'max' => 50)),
                                    'messages' => array(
                                        'Can not be empty',
                                        'invalid'
                                    )
                                ),
                                'CUST_EMAIL_TXT'      => array(
                                    'NotEmpty',
                                    array('StringLength', array('min' => 1, 'max' => 50)),
                                    'messages' => array(
                                        'Can not be empty',
                                        'invalid'
                                    )
                                ),
                                'BG_PURPOSE_TXT'      => array(
                                    'NotEmpty',
                                    array('StringLength', array('min' => 1, 'max' => 50)),
                                    'messages' => array(
                                        'Can not be empty',
                                        'invalid'
                                    )
                                ),
                                'bank_amount_txt'      => array(
                                    'NotEmpty',
                                    array('StringLength', array('min' => 1, 'max' => 50)),
                                    'messages' => array(
                                        'Can not be empty',
                                        'invalid'
                                    )
                                ),
                                'updatedate'      => array(
                                    'NotEmpty',
                                    array('StringLength', array('min' => 1, 'max' => 50)),
                                    'messages' => array(
                                        'Can not be empty',
                                        'invalid'
                                    )
                                ),
                                'currency'      => array(
                                    'NotEmpty',
                                    array('StringLength', array('min' => 1, 'max' => 50)),
                                    'messages' => array(
                                        'Can not be empty',
                                        'invalid'
                                    )
                                ),
                                'RECIPIENT_CP_TXT'      => array(
                                    'NotEmpty',
                                    array('StringLength', array('min' => 1, 'max' => 50)),
                                    'messages' => array(
                                        'Can not be empty',
                                        'invalid'
                                    )
                                ),
                                'RECIPIENT_CONTACT_TXT'      => array(
                                    'NotEmpty',
                                    array('StringLength', array('min' => 1, 'max' => 50)),
                                    'messages' => array(
                                        'Can not be empty',
                                        'invalid'
                                    )
                                ),
                                'RECIPIENT_EMAIL_TXT'      => array(
                                    'NotEmpty',
                                    array('StringLength', array('min' => 1, 'max' => 50)),
                                    'messages' => array(
                                        'Can not be empty',
                                        'invalid'
                                    )
                                ),
                                'SERVICE_TXT'      => array(
                                    'NotEmpty',
                                    array('StringLength', array('min' => 1, 'max' => 50)),
                                    'messages' => array(
                                        'Can not be empty',
                                        'invalid'
                                    )
                                ),
                                'GT_DOC_TYPE_TXT'      => array(
                                    'NotEmpty',
                                    array('StringLength', array('min' => 1, 'max' => 50)),
                                    'messages' => array(
                                        'Can not be empty',
                                        'invalid'
                                    )
                                ),
                                'GT_DOC_NUMBER_TXT'      => array(
                                    'NotEmpty',
                                    array('StringLength', array('min' => 1, 'max' => 50)),
                                    'messages' => array(
                                        'Can not be empty',
                                        'invalid'
                                    )
                                ),
                                'underlyingDocument'      => array(
                                    'NotEmpty',
                                    array('StringLength', array('min' => 1, 'max' => 50)),
                                    'messages' => array(
                                        'Can not be empty',
                                        'invalid'
                                    )
                                ),
                                'GT_DOC_DATE_TXT'      => array(
                                    'NotEmpty',
                                    array('StringLength', array('min' => 1, 'max' => 50)),
                                    'messages' => array(
                                        'Can not be empty',
                                        'invalid'
                                    )
                                ),
                                'COUNTER_WARRANTY_TYPE_TXT'      => array(
                                    'NotEmpty',
                                    array('StringLength', array('min' => 1, 'max' => 50)),
                                    'messages' => array(
                                        'Can not be empty',
                                        'invalid'
                                    )
                                ),

                            );
                        }

                        // echo "<code>masuk1 = $data</code>"; die;	
                        $zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);

                        if ($zf_filter_input->isValid()) {

                            // echo "<code>masuk1 = $data</code><BR>"; die;	

                            if ($zf_filter_input->COUNTER_WARRANTY_TYPE_TXT == '1') {

                                if (!empty($params['flselect_account_manual'])) {
                                    $splitArr = array();
                                    foreach ($params['flselect_account_manual'] as $ky => $vl) {
                                        if ($vl == '') {
                                            $nameAcct = explode('|', $params['flselect_account_number'][$ky]);
                                            $splitArr[$ky]['acct'] = $nameAcct[0];
                                        } else {
                                            $splitArr[$ky]['acct'] = $vl;
                                        }

                                        if ($params['flname_manual'][$ky] == '') {
                                            $splitArr[$ky]['acct_name'] = $params['flname'][$ky];
                                        } else {
                                            $splitArr[$ky]['acct_name'] = $params['flname_manual'][$ky];
                                        }

                                        $splitArr[$ky]['amount'] = $params['flamount'][$ky];
                                        $splitArr[$ky]['flag'] = $params['flselect_own'][$ky];
                                        $splitArr[$ky]['bank_code'] = '999';


                                        // echo "<code>masuk = $params</code>"; 
                                    }
                                    $this->view->acctSplit = $splitArr;
                                }
                            }
                            // echo '<pre>';
                            // print_r($splitArr);die;

                            if ($params['currency_type'] == "1") {

                                // echo "<code>masuk1 = $data</code><BR>"; die;
                                // echo '<pre>';
                                // print_r($params['flselect_own']);die;
                                // foreach($params['flselect_own'] as $i => $val)
                                // {
                                //     if($val == "1")
                                //     {
                                //         $arr_select_account_number = explode("|",$select_account_number[$i]);
                                //         $account_number .= $arr_select_account_number[0].',';
                                //         $account_name .= $arr_select_account_number[1].',';
                                //         $amount .= $params['flamount'][$i].',';
                                //     }
                                //     else if($val == "2")
                                //     {
                                //         $account_number .= $select_account_number[$i].',';
                                //         $account_name .= $flname_manual[$i].',';
                                //         $amount .= $params['flamount'][$i].',';
                                //     }
                                // }

                                // echo "<code>account_number = $account_number</code><BR>"; die;
                                // if($params['select_own'] == "1")
                                // {
                                // echo "<code>masuk1 = $data</code><BR>"; die;
                                $select_account_number = $params['flselect_account_number'];
                                $account_number = '';
                                $account_name = '';
                                $amount = '';
                                for ($i = 0; $i <= count($params['flselect_account_number']); $i++) {
                                    $arr_select_account_number = explode("|", $select_account_number[$i]);
                                    $account_number .= $arr_select_account_number[0] . ',';
                                    $account_name .= $arr_select_account_number[1] . ',';
                                    $amount .= $params['flamount'][$i] . ',';
                                }
                                $account_number = substr($account_number, 0, -1);
                                $account_name = substr($account_name, 0, -1);
                                $amount = substr($amount, 0, -1);

                                // }
                                // else
                                // {
                                //     // echo "<code>masuk2 = $data</code><BR>"; die;
                                //     $select_account_number = $params['flselect_account_manual'];
                                //     $flname_manual = $params['flname_manual'];

                                //     $account_number = '';
                                //     $account_name = '';
                                //     $amount = '';
                                //     for($i=0;$i<=count($params['flselect_account_manual']);$i++){
                                //         $account_number .= $select_account_number[$i].',';
                                //         $account_name .= $flname_manual[$i].',';
                                //         $amount .= $params['flamount'][$i].',';
                                //     }
                                //     $account_number = substr($account_number, 0, -1);
                                //     $account_name = substr($account_name, 0, -1);
                                //     $amount = substr($amount, 0, -1);
                                // }


                                $acct = $params['acct'];
                            } elseif ($params['currency_type'] == "2") {
                                $select_account_numberUSD = $params['select_account_numberUSD'];
                                for ($i = 0; $i <= count($params['select_account_numberUSD']); $i++) {
                                    $arr_account_numberUSD = explode("|", $select_account_numberUSD[$i]);
                                    $account_number .= $arr_account_numberUSD[0] . ',';;
                                    $account_name .= $arr_account_numberUSD[1] . ',';;
                                }

                                $acct = $params['acctUSD'];
                            }

                            $fileTypeMessage = explode('/', $fileType);
                            $fileType =  $fileTypeMessage[1];
                            $extensionValidator = new Zend_Validate_File_Extension(array(false, $fileExt));
                            $extensionValidator->setMessage("Extension file must be *.pdf");

                            $maxFileSize = "1024000";
                            $size = number_format($size);

                            $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $maxFileSize));
                            $sizeValidator->setMessage("File size is exceeding 1,024,000 byte(s), uploaded file is $size byte(s)");

                            $adapter->setValidators(array($extensionValidator, $sizeValidator));

                            if (!empty($fileInfo['uploadSource']["name"])) {

                                if ($adapter->isValid()) {

                                    if ($adapter->receive()) {
                                        // echo "<code>masuk = $data</code>"; die;	
                                        $file_name = $fileInfo['uploadSource']["name"];
                                        // unlink($attahmentDestination.$data['BG_UNDERLYING_DOC']); 

                                    } else {
                                        $errmsgs = $adapter->getMessages();
                                        foreach ($errmsgs as $key => $val) {
                                            $errfield = "error_" . $fieldname;
                                            $errorArray[$errfield] = $val;
                                        }
                                        $this->view->error = true;
                                        $this->view->err_msg = $errmsgs;
                                        // echo '<pre>';
                                        // echo "<code>errors1 = $errors</code>"; die;	
                                        // print_r($errmsgs);die;
                                    }
                                } else {
                                    $this->view->error = true;
                                    $errors = $this->language->_('Extension file must be *.jpg /.jpeg / .pdf');
                                    $this->view->err_msg = $errors;
                                    // echo '<pre>';
                                    // echo "<code>errors2 new= $errors</code>"; die;	
                                    // print_r($errors);die;
                                }
                            } else {
                                $file_name = $data['BG_UNDERLYING_DOC'];
                            }

                            // echo '<pre>';
                            // print_r($fileInfo);die;

                            // echo "<code>file_name = $file_name</code>"; die;
                            try {

                                $this->_db->beginTransaction();

                                if ($zf_filter_input->BG_FORMAT == 1) {
                                    $BG_LANGUAGE = $zf_filter_input->BG_LANGUAGE;
                                } else {
                                    $BG_LANGUAGE = 0;
                                }

                                if ($toa == '1') {
                                    // $insertArr = array(   
                                    //     'BG_REG_NUMBER'                     => $this->generateTransactionID($data['BG_BRANCH'],$zf_filter_input->BG_PURPOSE_TXT), 
                                    //     'CUST_CP'                           => $zf_filter_input->CUST_CP_TXT,
                                    //     'CUST_CONTACT_NUMBER'               => $zf_filter_input->CUST_CONTACT_NUMBER_TXT,
                                    //     'CUST_EMAIL'                        => $zf_filter_input->CUST_EMAIL_TXT,
                                    //     'BG_AMOUNT'                         => $zf_filter_input->bank_amount_txt, 
                                    //     'USAGE_PURPOSE'                     => $zf_filter_input->BG_PURPOSE_TXT,
                                    //     'RECIPIENT_CP'                      => $zf_filter_input->RECIPIENT_CP_TXT,
                                    //     'RECIPIENT_CONTACT'                 => $zf_filter_input->RECIPIENT_CONTACT_TXT,
                                    //     'RECIPIENT_EMAIL'                   => $zf_filter_input->RECIPIENT_EMAIL_TXT,
                                    //     'SERVICE'                           => $zf_filter_input->SERVICE_TXT,
                                    //     'GT_DOC_TYPE'                       => $zf_filter_input->GT_DOC_TYPE_TXT,
                                    //     'GT_DOC_NUMBER'                     => $zf_filter_input->GT_DOC_NUMBER_TXT,
                                    //     'GT_DOC_DATE'                       => $zf_filter_input->GT_DOC_DATE_TXT,
                                    //     'FEE_CHARGE_TO'                     => $zf_filter_input->acct_txt,
                                    //     'COUNTER_WARRANTY_TYPE'             => $zf_filter_input->COUNTER_WARRANTY_TYPE_TXT,
                                    //     'BG_PUBLISH'                        => $zf_filter_input->BG_PUBLISH_TXT, 
                                    //     'BG_FORMAT'                         => $zf_filter_input->BG_FORMAT,
                                    //     'BG_LANGUAGE'                       => $zf_filter_input->BG_LANGUAGE,
                                    //     'BG_UNDERLYING_DOC'                 => $file_name,   
                                    //     'BG_UPDATED'                        => new Zend_Db_Expr('now()'),
                                    //     'BG_UPDATEDBY'                      => $this->_userIdLogin, 
                                    // );


                                    $insertArr = array(
                                        'BG_REG_NUMBER'                     => $this->generateTransactionID($data['BG_BRANCH'], $zf_filter_input->BG_PURPOSE_TXT),
                                        // 'BG_REG_NUMBER'                     => $data['BG_REG_NUMBER'], 
                                        'ACCT_NO'                           => $data['ACCT_NO'],
                                        'BG_NUMBER'                         => $data['BG_NUMBER'],
                                        'BG_SUBJECT'                        => $data['BG_SUBJECT'],
                                        'CUST_ID'                           => $this->_custIdLogin,
                                        'CUST_CP'                           => $zf_filter_input->CUST_CP_TXT,
                                        'CUST_CONTACT_NUMBER'               => $zf_filter_input->CUST_CONTACT_NUMBER_TXT,
                                        'CUST_EMAIL'                        => $zf_filter_input->CUST_EMAIL_TXT,
                                        'BG_STATUS'                         => 2,
                                        'USAGE_PURPOSE'                     => $zf_filter_input->BG_PURPOSE_TXT,
                                        'USAGE_PURPOSE_DESC'                => $data['USAGE_PURPOSE_DESC'],
                                        'RECIPIENT_NAME'                    => $data['RECIPIENT_NAME'],
                                        'RECIPIENT_ADDRES'                  => $data['RECIPIENT_ADDRES'],
                                        'RECIPIENT_CITY'                    => $data['RECIPIENT_CITY'],
                                        'RECIPIENT_CP'                      => $zf_filter_input->RECIPIENT_CP_TXT,
                                        'RECIPIENT_OFFICE_NUMBER'           => $data['RECIPIENT_OFFICE_NUMBER'],
                                        'RECIPIENT_CONTACT'                 => $zf_filter_input->RECIPIENT_CONTACT_TXT,
                                        'RECIPIENT_EMAIL'                   => $zf_filter_input->RECIPIENT_EMAIL_TXT,

                                        "PROVISION_FEE" => Application_Helper_General::convertDisplayMoney($this->_getParam("provision_fee_input")),
                                        "ADM_FEE" => Application_Helper_General::convertDisplayMoney($this->_getParam("administration_fee_input")),
                                        "STAMP_FEE" => Application_Helper_General::convertDisplayMoney($this->_getParam("stamp_fee_input")),

                                        'SERVICE'                           => $zf_filter_input->SERVICE_TXT,
                                        'GT_DOC_TYPE'                       => $zf_filter_input->GT_DOC_TYPE_TXT,
                                        'GT_DOC_NUMBER'                     => $zf_filter_input->GT_DOC_NUMBER_TXT,
                                        'GT_DOC_DATE'                       => $zf_filter_input->GT_DOC_DATE_TXT,
                                        'GT_DOC_OTHER'                      => $data['GT_DOC_OTHER'],
                                        'GUARANTEE_TRANSACTION'             => $data['GUARANTEE_TRANSACTION'],
                                        'SP_OBLIGEE_CODE'                   => $data['SP_OBLIGEE_CODE'],
                                        'FILE'                              => $data['FILE'],
                                        'BG_AMOUNT'                         => Application_Helper_General::convertDisplayMoney($zf_filter_input->bank_amount_txt),
                                        'TIME_PERIOD_START'                 => date('Y-m-d', strtotime($data['TIME_PERIOD_START'])),
                                        'TIME_PERIOD_END'                   => date('Y-m-d', strtotime($zf_filter_input->updatedate)),

                                        'COUNTER_WARRANTY_TYPE'             => $zf_filter_input->COUNTER_WARRANTY_TYPE_TXT,
                                        // 'COUNTER_WARRANTY_TYPE'             => $data['COUNTER_WARRANTY_TYPE'], 
                                        'COUNTER_WARRANTY_ACCT_NO'          => rtrim($account_number, ","),
                                        'COUNTER_WARRANTY_ACCT_NAME'        => rtrim($account_name, ","),
                                        'COUNTER_WARRANTY_AMOUNT'           => Application_Helper_General::convertDisplayMoney(rtrim($amount, ",")),

                                        'FEE_CHARGE_TO'                     => $zf_filter_input->acct_txt,
                                        'BG_FORMAT'                         => $zf_filter_input->BG_FORMAT,
                                        'BG_LANGUAGE'                       => $BG_LANGUAGE,

                                        'BG_CREATED'                        => new Zend_Db_Expr('now()'),
                                        'BG_CREATEDBY'                      => $this->_userIdLogin,
                                        'BG_UPDATED'                        => new Zend_Db_Expr('now()'),
                                        'BG_UPDATEDBY'                      => $this->_userIdLogin,
                                        'BG_REJECT_NOTES'                   => $data['BG_REJECT_NOTES'],
                                        'BG_APPROVE_DOC'                    => $data['BG_APPROVE_DOC'],
                                        'REASON'                            => $data['REASON'],
                                        'BG_CLAIM_PERIOD'                   => $data['BG_CLAIM_PERIOD'],
                                        'BG_CLAIMEDBY'                      => $data['BG_CLAIMEDBY'],
                                        'BG_CLAIM_NOTES'                    => $data['BG_CLAIM_NOTES'],
                                        'BG_CANCELEDBY'                     => $data['BG_CANCELEDBY'],
                                        'BG_CANCEL_NOTES'                   => $data['BG_CANCEL_NOTES'],
                                        'BG_UNDERLYING_DOC'                 => $data['BG_UNDERLYING_DOC'],
                                        'BG_INSURANCE_CODE'                 => $data['BG_INSURANCE_CODE'],
                                        'BG_BRANCH'                         => $data['BG_BRANCH'],
                                        'BG_PUBLISH'                        => $zf_filter_input->BG_PUBLISH_TXT,
                                        'IS_AMENDMENT'                      => 1,
                                        'CHANGE_TYPE'                       => $toa

                                    );
                                    // echo '<pre>';
                                    // print_r($insertArr);die;
                                    // echo "<code>masuk2 = $data</code><BR>"; die;	
                                    $this->_db->insert('TEMP_BANK_GUARANTEE', $insertArr);

                                    // echo "<code>masuk2 = $data</code><BR>"; die;	
                                    if ($zf_filter_input->COUNTER_WARRANTY_TYPE_TXT == '1') {
                                        // echo "<code>COUNTER_WARRANTY_TYPE_TXT = 1</code>"; die;	   
                                        // $this->_db->delete('TEMP_BANK_GUARANTEE_SPLIT','BG_NUMBER = '.$this->_db->quote($numb));

                                        foreach ($splitArr as $ky => $vl) {
                                            $tmparrDetail = array(
                                                'BG_REG_NUMBER'         => $insertArr['BG_REG_NUMBER'], //update reza
                                                'ACCT'              => $vl['acct'],
                                                'BANK_CODE'         => $vl['bank_code'],
                                                'NAME'              => $vl['acct_name'],
                                                'AMOUNT'            => Application_Helper_General::convertDisplayMoney(rtrim($vl['amount'], ",")),
                                                'FLAG'              => $vl['flag']
                                            );

                                            // echo '<pre>';
                                            // print_r($tmparrDetail);die;
                                            $this->_db->insert('TEMP_BANK_GUARANTEE_SPLIT', $tmparrDetail);
                                        }
                                    }

                                    // else
                                    // {
                                    //     echo "<code>COUNTER_WARRANTY_TYPE_TXT = $COUNTER_WARRANTY_TYPE_TXT</code>"; die;
                                    // }

                                } else if ($toa == '2') {

                                    // echo '<pre>';
                                    // print_r($zf_filter_input->updatedate);die;
                                    $insertArr = array(
                                        'BG_REG_NUMBER'                     => $this->generateTransactionID($data['BG_BRANCH'], $data['USAGE_PURPOSE'][0]),
                                        'ACCT_NO'                           => $data['ACCT_NO'],
                                        'BG_NUMBER'                         => $data['BG_NUMBER'],
                                        'BG_SUBJECT'                        => $data['BG_SUBJECT'],
                                        'CUST_ID'                           => $data['CUST_ID'],
                                        'CUST_CP'                           => $data['CUST_CP'],
                                        'CUST_EMAIL'                        => $data['CUST_EMAIL'],
                                        'CUST_CONTACT_NUMBER'               => $data['CUST_CONTACT_NUMBER'],
                                        'BG_STATUS'                         => 2,
                                        'USAGE_PURPOSE'                     => $data['USAGE_PURPOSE'][0],
                                        'USAGE_PURPOSE_DESC'                => $data['USAGE_PURPOSE_DESC'],
                                        'RECIPIENT_NAME'                    => $data['RECIPIENT_NAME'],
                                        'RECIPIENT_ADDRES'                  => $data['RECIPIENT_ADDRES'],
                                        'RECIPIENT_CITY'                    => $data['RECIPIENT_CITY'],
                                        'RECIPIENT_CP'                      => $data['RECIPIENT_CP'],
                                        'RECIPIENT_OFFICE_NUMBER'           => $data['RECIPIENT_OFFICE_NUMBER'],
                                        'RECIPIENT_EMAIL'                   => $data['RECIPIENT_EMAIL'],
                                        'RECIPIENT_CONTACT'                 => $data['RECIPIENT_CONTACT'],
                                        'SERVICE'                           => $data['SERVICE'],
                                        'GT_DOC_TYPE'                       => $data['GT_DOC_TYPE'],
                                        'GT_DOC_OTHER'                      => $data['GT_DOC_OTHER'],
                                        'GT_DOC_NUMBER'                     => $data['GT_DOC_NUMBER'],
                                        'GT_DOC_DATE'                       => $data['GT_DOC_DATE'],
                                        'GUARANTEE_TRANSACTION'             => $data['GUARANTEE_TRANSACTION'],
                                        'SP_OBLIGEE_CODE'                   => $data['SP_OBLIGEE_CODE'],
                                        'FILE'                              => $data['FILE'],
                                        'BG_AMOUNT'                         => Application_Helper_General::convertDisplayMoney($data['BG_AMOUNT']),
                                        'TIME_PERIOD_START'                 => date('Y-m-d', strtotime($data['TIME_PERIOD_START'])),
                                        'TIME_PERIOD_END'                    => date('Y-m-d', strtotime($data['TIME_PERIOD_END'])),
                                        // 'TIME_PERIOD_END'                   => $zf_filter_input->updatedate,

                                        'COUNTER_WARRANTY_TYPE'             => $data['COUNTER_WARRANTY_TYPE'],
                                        'COUNTER_WARRANTY_ACCT_NO'          => $data['COUNTER_WARRANTY_ACCT_NO'],
                                        'COUNTER_WARRANTY_ACCT_NAME'        => $data['COUNTER_WARRANTY_ACCT_NAME'],
                                        'COUNTER_WARRANTY_AMOUNT'           => $data['COUNTER_WARRANTY_AMOUNT'],

                                        // 'FEE_CHARGE_TO'                     => $data['FEE_CHARGE_TO'],
                                        // 'BG_FORMAT'                         => $data['BG_FORMAT'],
                                        // 'BG_LANGUAGE'                       => $data['BG_LANGUAGE'],

                                        'FEE_CHARGE_TO'                     => $zf_filter_input->acct_txt,
                                        'BG_FORMAT'                         => $zf_filter_input->BG_FORMAT,
                                        'BG_LANGUAGE'                       => $BG_LANGUAGE,

                                        "PROVISION_FEE" => 0,
                                        "ADM_FEE" => Application_Helper_General::convertDisplayMoney($this->_getParam("administration_fee_input")),
                                        "STAMP_FEE" => Application_Helper_General::convertDisplayMoney($this->_getParam("stamp_fee_input")),

                                        'BG_CREATED'                        => new Zend_Db_Expr('now()'),
                                        'BG_CREATEDBY'                      => $this->_userIdLogin,
                                        'BG_UPDATED'                        => new Zend_Db_Expr('now()'),
                                        'BG_UPDATEDBY'                      => $this->_userIdLogin,
                                        'BG_REJECT_NOTES'                   => $data['BG_REJECT_NOTES'],
                                        'BG_APPROVE_DOC'                    => $data['BG_APPROVE_DOC'],
                                        'REASON'                            => $data['REASON'],
                                        'BG_CLAIM_PERIOD'                   => $data['BG_CLAIM_PERIOD'],
                                        'BG_CLAIMEDBY'                      => $data['BG_CLAIMEDBY'],
                                        'BG_CLAIM_NOTES'                    => $data['BG_CLAIM_NOTES'],
                                        'BG_CANCELEDBY'                     => $data['BG_CANCELEDBY'],
                                        'BG_CANCEL_NOTES'                   => $data['BG_CANCEL_NOTES'],
                                        'BG_UNDERLYING_DOC'                 => $data['BG_UNDERLYING_DOC'],
                                        'BG_INSURANCE_CODE'                 => $data['BG_INSURANCE_CODE'],
                                        'BG_BRANCH'                         => $data['BG_BRANCH'],
                                        // 'BG_PUBLISH'                        => $data['BG_PUBLISH'],
                                        'BG_PUBLISH'                        => $zf_filter_input->BG_PUBLISH_TXT,
                                        'IS_AMENDMENT'                      => 1,
                                        'CHANGE_TYPE'                       => $toa,

                                    );
                                    // echo '<pre>';

                                    // print_r($insertArr);die;
                                    // $this->_db->insert('TEMP_BANK_GUARANTEE', $insertArr);
                                    $this->_db->insert('TEMP_BANK_GUARANTEE', $insertArr);

                                    if ($data['COUNTER_WARRANTY_TYPE'] == '1') {

                                        foreach ($bgdatasplit as $ky => $vl) {
                                            $tmparrDetail = array(
                                                'BG_REG_NUMBER'         => $insertArr['BG_REG_NUMBER'], //update reza
                                                'ACCT'              => $vl['ACCT'],
                                                'BANK_CODE'         => $vl['BANK_CODE'],
                                                'NAME'              => $vl['NAME'],
                                                'AMOUNT'            => Application_Helper_General::convertDisplayMoney(rtrim($vl['AMOUNT'], ",")),
                                                'FLAG'              => $vl['FLAG']
                                            );

                                            // echo '<pre>';
                                            // print_r($tmparrDetail);die;
                                            // $this->_db->insert('TEMP_BANK_GUARANTEE_SPLIT', $tmparrDetail);
                                            $this->_db->insert('TEMP_BANK_GUARANTEE_SPLIT', $tmparrDetail);
                                        }
                                    }
                                }

                                $historyInsert = array(
                                    'DATE_TIME'         => new Zend_Db_Expr("now()"),
                                    'BG_REG_NUMBER'     => $insertArr['BG_REG_NUMBER'],
                                    'CUST_ID'           => $this->_custIdLogin,
                                    'USER_LOGIN'        => $this->_userIdLogin,
                                    'HISTORY_STATUS'    => 2
                                );

                                $this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);

                                // echo '<pre>';
                                // print_r($insertArr);die;
                                // $where = array('BG_REG_NUMBER = ?' => $numb);
                                // $query = $this->_db->update ( "T_BANK_GUARANTEE", $updArr, $where ); 

                                $bg_number = $data['BG_NUMBER'];
                                $bg_reg_number = $insertArr['BG_REG_NUMBER'];
                                if ($toa == '1') {
                                    Application_Helper_General::writeLog('CCBG', "Amendment Submission Change. BG Number: $bg_number. Register Number: [BG_REG_NUMBER - $bg_reg_number] ");
                                } else if ($toa == '2') {
                                    Application_Helper_General::writeLog('CCBG', "Amendment Submission Reprint Draft. BG Number: $bg_number. Register Number: [BG_REG_NUMBER - $bg_reg_number] ");
                                }

                                $this->_db->commit();
                                $this->setbackURL('/' . $this->_request->getModuleName() . '/');
                                $this->_redirect('/notification/success');
                            } catch (Exception $e) {

                                echo '<pre>';
                                print_r('test');
                                print_r($e);
                                die;
                                echo "<code>masuk3 = $data</code><BR>";
                                die;
                                $this->_db->rollBack();
                                $this->view->errorMsg = array(array($this->language->_('An Error Occured. Please Try Again.')));
                            }
                        } else {
                            // echo "<code>masuk4 = $data</code><BR>"; die;	
                            $error = $zf_filter_input->getMessages();
                            // $error = true;



                            //format error utk ditampilkan di view html 
                            $errorArray = null;
                            foreach ($error as $keyRoot => $rowError) {
                                foreach ($rowError as $errorString) {
                                    $errorArray[$keyRoot] = $errorString;
                                }
                            }
                            $this->view->report_msg = $errorArray;
                            // echo '<pre>';
                            // print_r($errorArray);die;
                        }
                    }
                }


                $download = $this->_getParam('download');
                $BG_APPROVE_DOC = $this->_getParam('BG_APPROVE_DOC');
                //print_r($edit);die;
                if ($BG_APPROVE_DOC) {
                    $attahmentDestination = UPLOAD_PATH . '/document/bg/';
                    //var_dump($attahmentDestination.$data['BG_APPROVE_DOC']);die;
                    $this->_helper->download->file($data['BG_APPROVE_DOC'], $attahmentDestination . $data['BG_APPROVE_DOC']);
                }
            }
        }
    }

    public function mdfcAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $bgRegNumber = $this->_request->getParam('bgregnumber') ?: null;

        if ($bgRegNumber == null) {
            header('Content-Type: application/json');
            echo json_encode(['result' => 'nothing']);
            return true;
        }

        $bgdatasplit = $this->_db->select()
            ->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
            ->joinLeft(['MCA' => 'M_CUSTOMER_ACCT'], 'MCA.ACCT_NO = A.ACCT AND MCA.CUST_ID = ' . $this->_db->quote($this->_custIdLogin) . '', ['ACCT_TYPE_MCA' => 'MCA.ACCT_TYPE', 'CCY_ID_MCA' => 'MCA.CCY_ID', 'ACCT_DESC_MCA' => 'MCA.ACCT_DESC'])
            ->where('A.BG_REG_NUMBER = ?', $bgRegNumber)
            ->where('A.IS_TRANSFER = ?', '1')
            ->query()->fetchAll();

        $bgdatasplit = array_filter($bgdatasplit, function ($acct) {
            if (strtolower($acct['ACCT_DESC']) != 'giro' && (strtolower($acct['ACCT_TYPE_MCA']) != '20' || strtolower($acct['ACCT_TYPE_MCA']) != 'd') && strtolower($acct['ACCT_DESC_MCA']) != 'giro')
                return true;
        });

        $mdAmount = 0;

        $htmlResult = "
        <table id=\"marginalDepositFCTable\" class=\"tableFile\">
            <thead>
                <th>" . $this->language->_('Hold Seq') . "</th>
                <th>" . $this->language->_('Nomor Rekening') . "</th>
                <th>" . $this->language->_('Nama Rekening') . "</th>
                <th>" . $this->language->_('Tipe') . "</th>
                <th>" . $this->language->_('Valuta') . "</th>
                <th>" . $this->language->_('Nominal') . "</th>
            </thead>
            <tbody>
            ";

        foreach ($bgdatasplit as $key => $value) {

            $callService = new Service_Account($value['ACCT'], '');
            $cekAcctType = strtolower($value['ACCT_DESC']) ?: (strtolower($value['ACCT_DESC_MCA']) ?: ((strtolower($value['ACCT_TYPE_MCA']) == 'T' || strtolower($value['ACCT_TYPE_MCA']) == '30') ? 'deposito' : ''));

            if (strtolower($value['ACCT_DESC']) == 'escrow') {
                $inqbalance = $callService->inquiryAccountBalance();
                // if (intval($inqbalance['available_balance']) == 0) {
                //     // hapus data atau ??
                //     continue;
                // } else
                $mdAmount += $value['AMOUNT'];
            } else {
                // jika deposito
                if (strtolower($cekAcctType) == 'deposito') {
                    $inqbalance = $callService->inquiryDeposito();

                    // if (strtolower($inqbalance['hold_description']) == 'n') {
                    //     // hapus data atau ??
                    //     continue;
                    // } else
                    $mdAmount += $value['AMOUNT'];
                }
                // end jika deposito

                // jika tabungan
                else {
                    $inqbalance = $callService->inqLockSaving();

                    $cekSaving = array_shift(array_filter($inqbalance['lock'], function ($sequence) use ($value) {
                        return strval($value['HOLD_SEQUENCE']) == strval($sequence['sequence_number']);
                    }));

                    // if (!$cekSaving) {
                    //     // hapus data atau
                    //     continue;
                    // } else
                    $mdAmount += $value['AMOUNT'];
                }
                // end jika tabungan
            }

            // if ($inqbalance['varian_rate']) {
            //     if (floatval($inqbalance['varian_rate']) !== floatval('0')) {
            //         $iconSpecialRate = '<i id="icwarning' . str_pad($value['ACCT'], 16, '0', STR_PAD_LEFT) . '" class="fas fa-info pt-1 ml-1" title="Special Rate" style="background-color: white;color: red;width: 23px;height: 23px;text-align: center;border: 1px solid red;border-radius: 50px;font-size: 12px;"></i>';
            //     }
            // }

            $htmlResult .= "
            <tr>
                <td>" . ($value['HOLD_SEQUENCE'] ?: '-') . "</td>
                <td>" . str_pad($value['ACCT'], 16, '0', STR_PAD_LEFT) . "</td>
                <td>" . $value['NAME'] . "</td>
                <td>" . ($value['ACCT_DESC'] ?: $value['ACCT_DESC_MCA']) . "</td>
                <td>" . ((strtolower($value['ACCT_DESC']) == 'escrow') ? 'IDR' : ($value['CCY_ID'] ?: $value['CCY_ID_MCA'])) . "</td>
                <td>" . number_format($value['AMOUNT'], 2) . "</td>
            </tr>";
        }

        $htmlResult .= "</tbody></table>";
        $htmlResult .= "<input type='hidden' value='" . $mdAmount . "' id='mdfceksis'></input>";

        echo $htmlResult;
    }

    private function validateAll($bgdata, $request, $files, $insurace = null)
    {
        $saveResult = [];

        // load settings
        $settings = new Settings();

        $attachmentDestination = UPLOAD_PATH . '/document/submit/'; // save file path

        $tipeUsulan = $bgdata['CHANGE_TYPE']; // 1 : amandemen isi , 2 : amandemen format
        $saveResult['tipeUsulan'] = $tipeUsulan;

        $rekeningPembebananBiaya = $request['acct_txt'] ?: '-'; // rekening pembebanan biaya
        $saveResult['rekeningPembebananBiaya'] = $rekeningPembebananBiaya;

        $bentukPenerbitan = $request['BG_PUBLISH_TXT']; // 1 : sertifikat, 2 : elektronik
        $formatBankGaransi = $request['BG_FORMAT']; // 1 : bank standard, 2 : spesial format
        if ($formatBankGaransi == '2') {
            $getSpecialFormatFile = $files['sepcialFormatFile'];
            $newName = date('dmy') . '_' . date('his') . '_' . uniqid('amd') . '_' . $getSpecialFormatFile['name'];
            $getTmpName = $getSpecialFormatFile['tmp_name'];

            $saveResult['specialFormatFile'] = $newName;

            move_uploaded_file($getTmpName, $attachmentDestination . $newName);
        }

        if ($tipeUsulan == '1') {
            switch ($bgdata['COUNTER_WARRANTY_TYPE']) {
                case '1':
                    $resultUsulan = $this->validateFc($bgdata, $request, $files);
                    break;

                case '2':
                    $resultUsulan = $this->validateLf($bgdata, $request, $files);
                    break;

                case '3':
                    $resultUsulan = $this->validateIns($bgdata, $request, $files, $insurace);
                    break;
            }
        }

        // cek apakah ada dokumen underlying yang dihapus
        $deleteUnderlying = [];
        foreach ($request as $key => $value) {
            if (strpos($key, 'gtx_') !== false) {
                $deleteUnderlying[] = str_replace('~X~', '.', str_replace('~|~', ' ', str_replace(['gtx_'], '', $key)));
            }
        }

        if (!$deleteUnderlying) $deleteUnderlying = [''];
        $getNotDeleteUnderlying = $this->_db->select()
            ->from('TEMP_BANK_GUARANTEE_UNDERLYING')
            ->where('DOC_FILE NOT IN (?)', $deleteUnderlying)
            ->where('BG_REG_NUMBER = ?', $bgdata['BG_REG_NUMBER'])
            ->query()->fetchAll();
        // end apakah ada dokumen underlying yang dihapus

        // cek ada penambahan dokumen underlying
        if ($files['uploadSource']) {
            foreach ($files['uploadSource']['name'] as $key => $value) {
                $saveName = $value;
                $newName = date('dmy') . '_' . date('his') . '_' . $bgdata['CUST_ID'] . '_' . uniqid('gt') . '_' . $saveName;
                move_uploaded_file($files['uploadSource']['tmp_name'][$key], $attachmentDestination . $newName);

                $getNotDeleteUnderlying[] = [
                    'DOC_FILE' => $newName,
                    'DOC_NAME' => $request['SERVICE'][$key],
                    'DOC_TYPE' => $request['GT_DOC_TYPE'][$key],
                    'DOC_NUMBER' => $request['GT_DOC_NUMBER'][$key],
                    'DOC_DATE' => $request['GT_DOC_DATE'][$key],
                ];
            }
        }
        // end cek ada penambahan dokumen underlying

        // cek apakah ada lampiran dokumen yang dihapus
        $deleteOthersDocument = [];
        foreach ($request as $key => $value) {
            if (strpos($key, 'odx_') !== false) {
                $deleteOthersDocument[] = str_replace('~X~', '.', str_replace('~|~', ' ', str_replace(['odx_'], '', $key)));
            }
        }

        if (!$deleteOthersDocument) $deleteOthersDocument = [''];
        $getNotDeleteOthersDocument = $this->_db->select()
            ->from('TEMP_BANK_GUARANTEE_FILE')
            ->where('BG_FILE NOT IN (?)', $deleteOthersDocument)
            ->where('BG_REG_NUMBER = ?', $bgdata['BG_REG_NUMBER'])
            ->query()->fetchAll();
        // end cek apakah ada lampiran dokumen yang dihapus

        // cek ada penambahan lampiran dokumen
        if ($files['othersDocument']) {
            foreach ($files['othersDocument']['name'] as $key => $value) {
                $saveName = $value;
                $newName = date('dmy') . '_' . date('his') . '_' . $bgdata['CUST_ID'] . '_' . uniqid('od') . '_' . $saveName;
                move_uploaded_file($files['othersDocument']['tmp_name'][$key], $attachmentDestination . $newName);

                $getNotDeleteOthersDocument[] = [
                    'BG_FILE' => $newName,
                    'FILE_NOTE' => $request['od'][$key],
                ];
            }
        }
        // end cek ada penambahan dokumen underlying

        if ($tipeUsulan == '1') {
            $saveResult = array_merge($saveResult, $resultUsulan, ['underlying' => $getNotDeleteUnderlying, 'othersDocument' => $getNotDeleteOthersDocument], $request, ['bgOld' => $bgdata['BG_NUMBER']]);
        } else {
            $saveResult = array_merge($bgdata, $request, $saveResult, ['bgOld' => $bgdata['BG_NUMBER']], ['underlying' => $getNotDeleteUnderlying, 'othersDocument' => $getNotDeleteOthersDocument]);
        }

        // insert data -----------------------------------------------------------------------------------------

        $requestAmd = $saveResult;

        // check apakah principal menggunakan approver
        $cekPrincipal = $this->_db->select()
            ->from('M_CUSTOMER')
            ->where('CUST_ID = ?', $bgdata['CUST_ID'])
            ->query()->fetch();

        $custApprover = $cekPrincipal['CUST_APPROVER'];

        // ntu
        $max_ntu = $settings->getSetting('max_ntu_date');

        // amandement
        $tipeUsulan = $bgdata['CHANGE_TYPE'];

        // status
        $status = ($custApprover == '1') ? '2' : '3';

        // insert data to TEMP_BANK_GUARANTEE
        $BG_CLAIM_DATE = date('Y-m-d', strtotime('+' . $settings->getSetting('max_claim_period') . ' days', strtotime($requestAmd['updatedate'][1])));

        $updateData = [
            'CUST_CP'                   => ($tipeUsulan == '2') ? $bgdata['CUST_CP'] : $requestAmd['CUST_CP_TXT'],
            'CUST_EMAIL'                => ($tipeUsulan == '2') ? $bgdata['CUST_EMAIL'] : $requestAmd['CUST_EMAIL_TXT'],
            'CUST_CONTACT_NUMBER'       => ($tipeUsulan == '2') ? $bgdata['CUST_CONTACT_NUMBER'] : $requestAmd['CUST_CONTACT_NUMBER_TXT'],
            'BG_STATUS'                 => $status,
            'BG_SUBJECT'                => $bgdata['BG_SUBJECT'],
            'RECIPIENT_NAME'            => ($tipeUsulan == '2') ? $bgdata['RECIPIENT_NAME'] : $requestAmd['RECIPIENT_NAME'],
            'RECIPIENT_ADDRES'          => ($tipeUsulan == '2') ? $bgdata['RECIPIENT_ADDRES'] : $requestAmd['RECIPIENT_ADDRESS'],
            'RECIPIENT_CITY'            => ($tipeUsulan == '2') ? $bgdata['RECIPIENT_CITY'] : $requestAmd['RECIPIENT_CITY'],
            'RECIPIENT_CP'              => ($tipeUsulan == '2') ? $bgdata['RECIPIENT_CP'] : $requestAmd['RECIPIENT_CP_TXT'],
            'RECIPIENT_OFFICE_NUMBER'   => ($tipeUsulan == '2') ? $bgdata['RECIPIENT_OFFICE_NUMBER'] : $requestAmd['RECIPIENT_OFFICE_NUMBER'],
            'RECIPIENT_EMAIL'           => ($tipeUsulan == '2') ? $bgdata['RECIPIENT_EMAIL'] : $requestAmd['RECIPIENT_EMAIL_TXT'],
            'RECIPIENT_CONTACT'         => ($tipeUsulan == '2') ? $bgdata['RECIPIENT_CONTACT'] : $requestAmd['RECIPIENT_CONTACT_TXT'],
            'SERVICE'                   => '-',
            'GT_DOC_TYPE'               => '-',
            'GT_DOC_NUMBER'             => '-',
            'GT_DOC_DATE'               => '-',
            'FILE'                      => '-',
            'BG_AMOUNT'                 => ($tipeUsulan == '2') ? str_replace([','], '', $bgdata['BG_AMOUNT']) : str_replace([','], '', $requestAmd['bank_amount_txt']),
            'PROVISION_FEE'             => '',
            'ADM_FEE'                   => '',
            'STAMP_FEE'                 => '',
            'TIME_PERIOD_START'         => ($tipeUsulan == '2') ? date('Y-m-d', strtotime($bgdata['TIME_PERIOD_START'])) : date('Y-m-d', strtotime($requestAmd['updatedate'][0])),
            'TIME_PERIOD_END'           => ($tipeUsulan == '2') ? date('Y-m-d', strtotime($bgdata['TIME_PERIOD_END'])) : date('Y-m-d', strtotime($requestAmd['updatedate'][1])),
            'COUNTER_WARRANTY_TYPE'     => $bgdata['COUNTER_WARRANTY_TYPE'],
            'COUNTER_WARRANTY_ACCT_NO'  => '-',
            'COUNTER_WARRANTY_ACCT_NAME' => '-',
            'COUNTER_WARRANTY_AMOUNT'   => '-',
            'FEE_CHARGE_TO'             => $requestAmd['rekeningPembebananBiaya'],
            'BG_FORMAT'                 => $requestAmd['BG_FORMAT'],
            'BG_LANGUAGE'               => $requestAmd['BG_LANGUAGE'] ?: '-',
            'USAGE_PURPOSE'             => $bgdata['USAGE_PURPOSE'],
            'USAGE_PURPOSE_DESC'        => $bgdata['USAGE_PURPOSE_DESC'],
            'BG_INSURANCE_CODE'         => $bgdata['BG_INSURANCE_CODE'],
            'BG_UNDERLYING_DOC'         => '-',
            'BG_BRANCH'                 => $bgdata['BG_BRANCH'],
            'BG_PUBLISH'                => $requestAmd['BG_PUBLISH_TXT'],
            'SPECIAL_FORMAT_DOC'        => $bgdata['sepcialFormatFile'] ?: '-',
            'NTU_COUNTING'              => $max_ntu,
            'BG_CLAIM_DATE'             => ($tipeUsulan == '2') ? $bgdata['BG_CLAIM_DATE'] : $BG_CLAIM_DATE,
            'IS_AMENDMENT'              => 1,
            'CHANGE_TYPE'               => $tipeUsulan,
        ];

        $this->_db->update('TEMP_BANK_GUARANTEE', $updateData, ['BG_REG_NUMBER = ?' => $bgdata['BG_REG_NUMBER']]);

        if ($bgdata['COUNTER_WARRANTY_TYPE'] == '2') {
            $this->_db->update('TEMP_BANK_GUARANTEE_DETAIL', ['PS_FIELDVALUE' => $updateData['BG_AMOUNT']], ['LOWER(PS_FIELDNAME) = ?' => 'amount owner', 'BG_REG_NUMBER = ?' => $bgdata['BG_REG_NUMBER']]);
        }

        // update history
        $historyInsert = array(
            'DATE_TIME'         => new Zend_Db_Expr("now()"),
            'BG_REG_NUMBER'     => $bgdata['BG_REG_NUMBER'],
            'CUST_ID'           => $this->_custIdLogin,
            'USER_LOGIN'        => $this->_userIdLogin,
            'HISTORY_STATUS'    => 18
        );
        $this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);

        // insert log
        if ($tipeUsulan == '1') {
            $log = 'Perbaikan Amendemen Isi atas Nomor BG : ' . $bgdata['BG_NUMBER'] . ', Subjek : ' . $bgdata['BG_SUBJECT'] . ', dengan Nomor Registrasi Pengajuan : ' . $bgdata['BG_REG_NUMBER'] . '';
        } else {
            $log = 'Perbaikan Amendemen Format atas Nomor BG : ' . $bgdata['BG_NUMBER'] . ', Subjek : ' . $bgdata['BG_SUBJECT'] . ', dengan Nomor Registrasi Pengajuan : ' . $bgdata['BG_REG_NUMBER'] . '';
        }
        Application_Helper_General::writeLog('CCBG', $log);

        // insert temp_bank_guarantee_underlying
        $this->_db->delete('TEMP_BANK_GUARANTEE_UNDERLYING', ['BG_REG_NUMBER = ?' => $bgdata['BG_REG_NUMBER']]);
        if ($requestAmd['underlying']) {
            foreach ($requestAmd['underlying'] as $key => $value) {
                $insertTBGU = [
                    'BG_REG_NUMBER' => $bgdata['BG_REG_NUMBER'],
                    'DOC_NAME' => $value['DOC_NAME'],
                    'DOC_TYPE' => $value['DOC_TYPE'],
                    'DOC_NUMBER' => $value['DOC_NUMBER'],
                    'DOC_DATE' => date('Y-m-d', strtotime($value['DOC_DATE'])),
                    'DOC_FILE' => $value['DOC_FILE'],
                ];
                $this->_db->insert('TEMP_BANK_GUARANTEE_UNDERLYING', $insertTBGU);
            }
        }

        // insert temp_bank_guarantee_file
        $this->_db->delete('TEMP_BANK_GUARANTEE_FILE', ['BG_REG_NUMBER = ?' => $bgdata['BG_REG_NUMBER']]);
        if ($requestAmd['othersDocument']) {
            foreach ($requestAmd['othersDocument'] as $key => $value) {
                $insertTBGU = [
                    'BG_REG_NUMBER' => $bgdata['BG_REG_NUMBER'],
                    'BG_FILE' => $value['BG_FILE'],
                    'FILE_NOTE' => $value['FILE_NOTE'],
                    'INDEX' => $key,
                ];
                $this->_db->insert('TEMP_BANK_GUARANTEE_FILE', $insertTBGU);
            }
        }

        // hapus approval
        $this->_db->delete('T_APPROVAL', ['PS_NUMBER = ?' => $bgdata['BG_REG_NUMBER']]);

        // jika kontra fullcover
        if ($bgdata['COUNTER_WARRANTY_TYPE'] == '1' || $bgdata['COUNTER_WARRANTY_TYPE'] == '3') {

            // get split
            // $getSplit = $this->_db->select()
            //     ->from(['TBS' => 'TEMP_BANK_GUARANTEE_SPLIT'])
            //     ->joinLeft(['MCA' => 'M_CUSTOMER_ACCT'], 'MCA.ACCT_NO = TBS.ACCT AND MCA.CUST_ID = ' . $this->_db->quote($bgdata['CUST_ID']) . '', ['ACCT_TYPE_MCA' => 'MCA.ACCT_TYPE', 'CCY_ID_MCA' => 'MCA.CCY_ID', 'ACCT_DESC_MCA' => 'MCA.ACCT_DESC'])
            //     ->where('BG_REG_NUMBER = ?', $bgdata['BG_REG_NUMBER'])
            //     ->query()->fetchAll();

            $getSplit = $this->_db->select()
                ->from(['TBS' => 'T_BANK_GUARANTEE_SPLIT'])
                ->where('BG_NUMBER = ?', $bgdata['BG_OLD'])
                ->query()->fetchAll();

            $saveSplit = [];
            if ($requestAmd['own_account']) {
                foreach ($requestAmd['own_account'] as $key => $value) {
                    $saveSplit[] = [
                        "BG_REG_NUMBER" => "-",
                        "BG_NUMBER" => "-",
                        "ACCT" => ($value == '1') ? $requestAmd['own_account_number'][$key] : $requestAmd['manual_account_number'][$key],
                        "BANK_CODE" => "-",
                        "NAME" => ($value == '1') ? '-' : $requestAmd['manual_account_name'][$key],
                        "AMOUNT" => str_replace([','], '', $requestAmd['account_amount'][$key]),
                        "FLAG" => $value,
                        "CCY_ID" => $requestAmd['currency_acct'][$key],
                        "ACCT_TYPE" => '',
                        "ACCT_DESC" => $requestAmd['acct_type'][$key],
                        "HOLD_BY_BRANCH" => '',
                        "HOLD_SEQUENCE" => '',
                        "HOLD_REMARK" => '',
                    ];
                }

                foreach ($saveSplit as $key => $value) {
                    if (strtolower($value['ACCT_DESC']) == 'escrow') continue;

                    if (strtolower($value['NAME']) == '-' && $value['FLAG'] == '1') {
                        $getTempName = $this->_db->select()
                            ->from('M_CUSTOMER_ACCT')
                            ->where('ACCT_NO = ?', $value['ACCT'])
                            ->query()->fetch();

                        $saveSplit[$key]['NAME'] = $getTempName['ACCT_NAME'];
                    }
                    $totalAmountNew += floatval($value['AMOUNT']);
                }
            }

            // delete split sebelumnya
            $this->_db->delete('TEMP_BANK_GUARANTEE_SPLIT', [
                'BG_REG_NUMBER = ?' => $bgdata['BG_REG_NUMBER']
            ]);

            // jika tipe usulan format
            if ($getSplit) {
                foreach ($getSplit as $key => $value) {
                    $insertSplit = [
                        'BG_REG_NUMBER' => $bgdata['BG_REG_NUMBER'],
                        'ACCT' => $value['ACCT'],
                        'BANK_CODE' => '999',
                        'NAME' => $value['NAME'],
                        'AMOUNT' => $value['AMOUNT'],
                        'FLAG' => $value['FLAG'],
                        'ACCT_TYPE' => $value['ACCT_TYPE'],
                        'ACCT_DESC' => $value['ACCT_DESC'],
                        'CCY_ID' => $value['CCY_ID'],
                        'HOLD_SEQUENCE' => $value['HOLD_SEQUENCE'],
                        'HOLD_REMARK' => $value['HOLD_REMARK'],
                        'HOLD_BY_BRANCH' => $value['HOLD_BY_BRANCH'],
                        'IS_TRANSFER' => '1'
                    ];
                    $this->_db->insert('TEMP_BANK_GUARANTEE_SPLIT', $insertSplit);
                }
            }
            if ($tipeUsulan == '1') {
                if ($saveSplit) {
                    foreach ($saveSplit as $key => $value) {
                        $insertSplit = [
                            'BG_REG_NUMBER' => $bgdata['BG_REG_NUMBER'],
                            'ACCT' => $value['ACCT'],
                            'BANK_CODE' => '999',
                            'NAME' => $value['NAME'],
                            'AMOUNT' => $value['AMOUNT'],
                            'FLAG' => $value['FLAG'],
                            'ACCT_TYPE' => $value['ACCT_TYPE'],
                            'ACCT_DESC' => $value['ACCT_DESC'],
                            'CCY_ID' => $value['CCY_ID'],
                            'HOLD_SEQUENCE' => '',
                            'HOLD_REMARK' => '',
                            'HOLD_BY_BRANCH' => ''
                        ];
                        $this->_db->insert('TEMP_BANK_GUARANTEE_SPLIT', $insertSplit);
                    }
                }
            }
        }

        $this->setbackURL('/' . $this->_request->getModuleName() . '/repair/');
        $this->_redirect('/notification/success');
    }

    private function validateFc($bgdata, $request, $files)
    {
        // check apakah ada penambahan MD FC
        $checkAddMd = $request['own_account'];
        $saveAddAcct = [];
        if (is_array($checkAddMd) && count($checkAddMd) > 0) {
            // simpan ke session
            foreach ($checkAddMd as $key => $value) {
                // jika rekening milik sendiri
                if ($value == '1') {
                    $acctNumber = $request['own_account_number'][$key];
                    $getName = $this->_db->select()
                        ->from('M_CUSTOMER_ACCT')
                        ->where('ACCT_NO = ?', $acctNumber)
                        ->where('CUST_ID = ?', $bgdata['CUST_ID'])
                        ->query()->fetch();
                    $acctName = $getName['ACCT_NAME'];
                }

                // jika rekening pihak ketiga
                else {
                    $acctNumber = $request['manual_account_number'][$key];
                    $acctName = $request['manual_account_name'][$key];
                }

                $acctType = $request['acct_type'][$key];
                $currency = $request['currency_acct'][$key];
                $amount = str_replace([','], '', $request['account_amount'][$key]);

                $saveAddAcct[] = [
                    'acctNumber' => $acctNumber,
                    'acctName' => $acctName,
                    'acctType' => $acctType,
                    'currency' => $currency,
                    'amount' => $amount,
                ];
            }
        }
        $result = ['topUpMd' => $saveAddAcct];

        return $result;
    }

    private function validateLf($bgdata, $request, $files)
    {
        // validate Line Facility
        return [];
    }

    private function validateIns($bgdata, $request, $files, $insurace)
    {
        // validate Line Facility
        return $insurace;
    }

    public function cekusulanAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $valid = true;

        $bgRegNumber = $this->_request->getParam('bg_reg_number');
        $bgdata = $this->_db->select()
            ->from(['TBG' => 'TEMP_BANK_GUARANTEE'], [
                '*',
                'INSURANCE_BRANCH' => new Zend_Db_Expr('(SELECT PS_FIELDVALUE FROM TEMP_BANK_GUARANTEE_DETAIL TBGD WHERE TBGD.BG_REG_NUMBER = TBG.BG_REG_NUMBER AND LOWER(PS_FIELDNAME) = \'insurance branch\' LIMIT 1)')
            ])
            ->where('BG_REG_NUMBER = ?', $bgRegNumber)
            ->query()->fetchAll();

        if (!$bgdata) {
            $bgdata = $this->_db->select()
                ->from(['TBG' => 'T_BANK_GUARANTEE'], [
                    '*',
                    'INSURANCE_BRANCH' => new Zend_Db_Expr('(SELECT PS_FIELDVALUE FROM TEMP_BANK_GUARANTEE_DETAIL TBGD WHERE TBGD.BG_REG_NUMBER = TBG.BG_REG_NUMBER AND LOWER(PS_FIELDNAME) = \'insurance branch\' LIMIT 1)')
                ])
                ->where('BG_REG_NUMBER = ?', $bgRegNumber)
                ->query()->fetchAll();
        }

        // pengkondisian kontra
        $param = [];
        if ($bgdata[0]['COUNTER_WARRANTY_TYPE'] == '3') {
            $param['COUNTER_WARRANTY_TYPE'] = $bgdata[0]['COUNTER_WARRANTY_TYPE'];
            $param['CUST_ID'] = $bgdata[0]["BG_INSURANCE_CODE"];
            $param['GRUP'] = $bgdata[0]["CUST_ID"];
            $param['USAGE_PURPOSE'] = $bgdata[0]['USAGE_PURPOSE'];
            $param['USAGE_PURPOSE_DESC'] = $bgdata[0]['USAGE_PURPOSE_DESC'];
        }

        if ($bgdata[0]['COUNTER_WARRANTY_TYPE'] == '2') {
            $param['COUNTER_WARRANTY_TYPE'] = $bgdata[0]['COUNTER_WARRANTY_TYPE'];
            $param['CUST_ID'] = $bgdata[0]["CUST_ID"];
            $param['GRUP'] = $bgdata[0]["CUST_ID"];
            $param['USAGE_PURPOSE'] = $bgdata[0]['USAGE_PURPOSE'];
            $param['USAGE_PURPOSE_DESC'] = $bgdata[0]['USAGE_PURPOSE_DESC'];
        }
        // end pengkondisian kontra

        if (in_array($bgdata[0]['COUNTER_WARRANTY_TYPE'], [2, 3])) {

            $getCustomer = $this->_db->select()
                ->from('M_CUSTOMER')
                ->where('CUST_ID = ?', $param['CUST_ID'])
                ->query()->fetch();

            $kontra = (strval($bgdata[0]['COUNTER_WARRANTY_TYPE']) === '2') ? ['LF'] : ['INS', 'ASURANSI'];
            $grup_BUMN = (strval($getCustomer['GRUP_BUMN']) === '0') ? $kontra[0] . 'NONBUMN' : $kontra[0] . 'BUMN';
            $jaminan = $kontra[1] . ' ' . $param['USAGE_PURPOSE_DESC'];
            $jaminan = strtolower($jaminan);

            $getCharges = $this->_db->select()
                ->from('M_CHARGES_BG')
                ->where('CHARGES_ID LIKE ?', '%' . $grup_BUMN . '%')
                ->where('LOWER(CHARGES_NAME) LIKE ?', '%' . $jaminan . '%')
                ->query()->fetch();

            if (strtolower($getCharges['ACTIVE']) != 'y') $valid = ($valid == true) ? false : $valid;

            $getDetailLf =  $this->_db->select()
                ->from('M_CUST_LINEFACILITY_DETAIL')
                ->where('CUST_ID = ?', $param['CUST_ID'])
                ->where('LOWER(OFFER_TYPE) LIKE ?', '%' . strtolower($param['USAGE_PURPOSE_DESC']) . '%')
                ->where('GRUP_BUMN = ?', $getCustomer['GRUP_BUMN'])
                ->query()->fetch();

            if ($getDetailLf['FLAG'] != '1') $valid = ($valid == true) ? false : $valid;
        }

        header('Content-Type: application/json');

        echo json_encode(['valid' => $valid]);
        return true;
    }

    private function getInsuranceInfo($insuranceCode, $insuranceBranch)
    {
        $getCustomer = $this->_db->select()
            ->from(['MC' => 'M_CUSTOMER'], [
                'CUST_NAME',
                'CUST_ID',
                'FEE_DEBITED' => new Zend_Db_Expr("(SELECT FEE_DEBITED FROM M_CUST_LINEFACILITY MCL WHERE MCL.CUST_ID = MC.CUST_ID LIMIT 1)")
            ])
            ->where('CUST_ID = ?', $insuranceCode)
            ->query()->fetch();

        $getInsuranceBranch = $this->_db->select()
            ->from('M_INS_BRANCH')
            ->where('INS_BRANCH_CODE = ?', $insuranceBranch)
            ->query()->fetch();

        return [$getCustomer, $getInsuranceBranch];
    }
}
