<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Settings.php';

class user_UserprintpdfController extends Application_Main
{
	public function initController(){
		  $this->_helper->layout()->setLayout('popup');
	 }
	public function indexAction()
	{

		$set = new Settings();
		$FResetPass = $set->getSetting('ftemplate_resetpwd');
		
		$user_id = $this->_getParam('user_id');
		$cust_id = $this->_custIdLogin;

		$isi = $this->_db->SELECT ()
								->FROM ('M_USER', array('USER_ID','CUST_ID','USER_EMAIL','USER_FULLNAME','USER_CLEARTEXT_PWD'))
								->WHERE ('USER_ID = ? ', $user_id)
								->WHERE ('CUST_ID = ?', $cust_id);
								
		$isi = $this->_db->fetchrow($isi);
		$newpassword = substr(base64_decode($isi['USER_CLEARTEXT_PWD']),4, -4);
		
		$FResetPass = str_ireplace('[[user_fullname]]',$isi['USER_FULLNAME'],$FResetPass);
		$FResetPass = str_ireplace('[[comp_accid]]',$isi['CUST_ID'],$FResetPass);
		$FResetPass = str_ireplace('[[user_login]]',$isi['USER_ID'],$FResetPass);
		$FResetPass = str_ireplace('[[user_email]]',$isi['USER_EMAIL'],$FResetPass);
		$FResetPass = str_ireplace('[[user_cleartext_password]]',$newpassword,$FResetPass);
	
		$this->view->forms = $FResetPass;
		$filename = $user_id.'_PDF';
		$pdf = $this->_getParam('pdf');
		
		if($pdf)
		{
			$data = array(
										'USER_RPWD_ISPRINTED' => 1,
									);
			$where =  array();
			$where['CUST_ID = ?'] = $cust_id;
			$where['USER_ID = ?'] = $user_id;
			$updated = $this->_db->update('M_USER',$data,$where);

			Application_Helper_General::writeLog('UVEP','Download PDF User Print Pdf Cust Id ( '.$cust_id.' ) ,User Id ( '.$user_id.' )');

			$FResetPass = "<tr><td>".$FResetPass."</td></tr>";
			$this->_helper->download->pdf(null,null,null,$filename,$FResetPass);
			$data = array(
									'USER_RPWD_ISPRINTED' 			=> 1,
								);
			$where =  array();
			$where['CUST_ID 		 = ?'] 	= $cust_id;
			$where['USER_ID 		 = ?'] 	= $user_id;
			$this->_db->update('M_USER',$data,$where);
			$this->view->forms = $FEmailResetPass;
		}
		else
			Application_Helper_General::writeLog('UVEP','View User Print Pdf Cust Id ( '.$cust_id.' ) ,User Id ( '.$user_id.' )');
	}
}
?>