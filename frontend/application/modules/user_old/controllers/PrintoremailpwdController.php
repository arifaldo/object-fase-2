<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Settings.php';

class user_PrintoremailpwdController extends Application_Main
{

	public function indexAction()
	{	 

		$setting = new Settings();			  	
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
		$pw_hash = md5($enc_salt.$enc_pass);
		$rand = $this->_userIdLogin.date('dHis').$pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;
	
		$this->_helper->layout()->setLayout('newlayout');
	
		
		$status= array(
													'-1' => '--- '.$this->language->_('Any Value').' ---',
													'0' => $this->language->_('No'),
													'1' => $this->language->_('Yes')
												);
		$this->view->status = $status;
												
		 $fields = array	(
							
							// 										'field' => 'b.CUST_NAME',
							// 										'label' => $this->language->_('Company Name'),
							// 										'sortable' => true
							// 									),
							'User Id'  			=> array	(
																	'field' => 'a.USER_ID',
																	'label' => $this->language->_('User'),
																	'sortable' => true
																),
							// 'Name'  			=> array	(
							// 										'field' => 'a.USER_FULLNAME',
							// 										'label' => $this->language->_('User Name'),
							// 										'sortable' => true
							// 									),
							'New User'  			=> array	(
																	'field' => 'a.USER_ISNEW',
																	'label' => $this->language->_('New User'),
																	'sortable' => true
																),
							/*'Password Emailed'  			=> array	(
																	'field' => 'a.USER_RPWD_ISEMAILED',
																	'label' => $this->language->_('Password Emailed'),
																	'sortable' => true
																),
							'Password Printed'  			=> array	(
																	'field' => 'a.USER_RPWD_ISPRINTED',
																	'label' => $this->language->_('Password Printed'),
																	'sortable' => true
																),		*/														
							'Recommended Method'  			=> array	(
																	'field' => 'a.USER_ISEMAILPWD',
																	'label' => $this->language->_('Send Method'),
																	'sortable' => true
																),																	
						);	
		
		//get page, sortby, sortdir
        $page    = $this->_getParam('page');		
        $sortBy  = $this->_getParam('sortby');
        $sortDir = $this->_getParam('sortdir');
		
        //validate parameters before passing to view and query
        $page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
                    
        $sortBy = (Zend_Validate::is($sortBy,'InArray',
        	array(array_keys($fields))
            ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

        $sortDir = (Zend_Validate::is($sortDir,'InArray',
        	array('haystack'=>array('asc','desc'))
            ))? $sortDir : 'asc';
	    $this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;	
		
		$this->view->currentPage = $page;
		//Zend_Debug::dump($lockstatus);die;
		$select = $this->_db->SELECT()
										->FROM(array('a' => 'M_USER') ,array('a.CUST_ID','a.USER_ID','a.USER_FULLNAME','a.USER_ISNEW','a.USER_RPWD_ISEMAILED','a.USER_RPWD_ISPRINTED','a.USER_ISEMAILPWD'))
										->JOIN(array('b' => 'M_CUSTOMER'), 'a.CUST_ID = b.CUST_ID' ,array('b.CUST_NAME','company'	=> new Zend_Db_Expr("CONCAT(b.CUST_NAME , ' (' , b.CUST_ID , ')  ' )"),
								 		'user'	=> new Zend_Db_Expr("CONCAT(a.USER_FULLNAME , ' (' , a.USER_ID , ')  ' )"),))
										->WHERE('a.USER_STATUS != 3 ')
										->where('a.CUST_ID = ?',$this->_custIdLogin)
										->WHERE('a.USER_ISREQUIRE_CHANGEPWD = 1 ')
										->WHERE('a.USER_ISLOCKED <> 1 ')
										->WHERE('a.USER_RCHANGE = 1 ');	
// die($select);
		// advance search
		$filterlist = array("USER ID" => "USER_ID", "USER ISNEW" => "USER_ISNEW","SEND METHOD"=>"USER_ISPRINTED");

        $this->view->filterlist = $filterlist;


        $filterArr = array(
            'filter'    =>  array('StripTags'),
            'USER_ID'    =>  array('StringTrim', 'StripTags'),
            'USER_ISNEW'    =>  array('StringTrim', 'StripTags'),
            'USER_ISPRINTED' =>  array('StringTrim', 'StripTags'),
        );

        $validator = array(
            'filter'                 => array(),
            'USER_ID' => array(),
            'USER_ISNEW' => array(),
            'USER_ISPRINTED' => array(),
        );


        $dataParam = array("USER_ID","USER_ISNEW","USER_ISPRINTED");
        $dataParamValue = array();

        $clean2 = array_diff($this->_request->getParam('wherecol'), $dataParam);
        $dataParam = array_diff($this->_request->getParam('wherecol'), $clean2);
        // print_r($this->_request->getParam('wherecol'));
        foreach ($dataParam as $no => $dtParam) {

            if (!empty($this->_request->getParam('wherecol'))) {
                $dataval = $this->_request->getParam('whereval');
                // print_r($dataval);
                $order = 0;
                foreach ($this->_request->getParam('wherecol') as $key => $value) {
                    /*if ($value == "QUEST_DATE") {
                        $order--;
                    }*/
                    if ($dtParam == $value) {
                        $dataParamValue[$dtParam] = $dataval[$order];
                    }
                    $order++;
                }
            }
        }
			
		/*if (!empty($this->_request->getParam('questdate'))) {
            $questdateArr = $this->_request->getParam('questdate');
            $dataParamValue['QUEST_DATE'] = $questdateArr[0];
            $dataParamValue['QUEST_DATE_END'] = $questdateArr[1];
        }*/


        $zf_filter = new Zend_Filter_Input($filterArr, $validator, $dataParamValue);
        // $filter 	= $zf_filter->getEscaped('filter');
        $filter         = $this->_getParam('filter');
		
		$USER_ID     = html_entity_decode($zf_filter->getEscaped('USER_ID'));
		$USER_ISNEW     = html_entity_decode($zf_filter->getEscaped('USER_ISNEW'));
        $USER_ISPRINTED     = html_entity_decode($zf_filter->getEscaped('USER_ISPRINTED'));
        
		
		/*if ($filter_clear == true) {
            $datefrom     = '';
            $dateto     = '';
        }*/
		
		if ($filter == TRUE) {

            /*$this->view->fDateTo    = $dateto;
            $this->view->fDateFrom  = $datefrom;
			*/
			
			if($USER_ID != null)
			{
				$select->where("UPPER(a.USER_ID) LIKE ".$this->_db->quote('%'.$USER_ID.'%'));
			} 

			if($USER_ISPRINTED != null)
			{
				$select->where("USER_ISEMAILPWD LIKE ".$this->_db->quote('%'.$USER_ISPRINTED.'%'));
			} 
			
			if($USER_ISNEW != null)
			{
				$select->where("USER_ISNEW LIKE ".$this->_db->quote('%'.$USER_ISNEW.'%'));
			}
			
		
        }

        //unset($dataParamValue['QUEST_DATE_END']);
        if (!empty($dataParamValue)) {
            foreach ($dataParamValue as $key => $value) {
                $wherecol[]    = $key;
                $whereval[] = $value;
            }

            $this->view->wherecol     = $wherecol;
            $this->view->whereval     = $whereval;
        }
		
		//
		
		$select->order($sortBy.' '.$sortDir);
		$pdf = $this->_getParam('pdf');
		
		if($pdf)
		{
			$set = new Settings();
			$templateEmailMasterBankAddress = $set->getSetting('master_bank_address');
			$templateEmailMasterBankAppName = $set->getSetting('master_bank_app_name');
			$templateEmailMasterBankAppUrl = $set->getSetting('master_bank_app_url');
			$templateEmailMasterBankCity = $set->getSetting('master_bank_city');
			$templateEmailMasterBankCountry = $set->getSetting('master_bank_country');
			$templateEmailMasterBankEmail = $set->getSetting('master_bank_email');
			$templateEmailMasterBankEmail1 = $set->getSetting('master_bank_email1');
			$templateEmailMasterBankFax = $set->getSetting('master_bank_fax');
			$templateEmailMasterBankName = $set->getSetting('master_bank_name');
			$templateEmailMasterBankProvince = $set->getSetting('master_bank_province');
			$templateEmailMasterBankTelp = $set->getSetting('master_bank_telp');
			$templateEmailMasterBankWebsite = $set->getSetting('master_bank_website');
		
			$user_id = $this->_getParam('pdfuser_id');
			$cust_id = $this->_getParam('pdfcust_id');
			
			$isi = $this->_db->SELECT ()
							->FROM ('M_USER', array('USER_ID','CUST_ID','USER_EMAIL','USER_FULLNAME','USER_CLEARTEXT_PWD','USER_ISNEW'))
							->WHERE ('USER_ID = ? ', $user_id)
							->WHERE ('CUST_ID = ? ',$cust_id);

			$isi = $this->_db->fetchrow($isi);
//			$FResetPass = $set->getSetting('ftemplate_resetpwd');
			if($isi['USER_ISNEW'] == 1){
//				$FEmailResetPass = $set->getSetting('femailtemplate_newuser');
				$FResetPass = $set->getSetting('femailtemplate_newuser');
			}
			else{
//				$FEmailResetPass = $set->getSetting('femailtemplate_resetpwd');
				$FResetPass = $set->getSetting('femailtemplate_resetpwd');
			}
			
//			$newPassword = strtoupper(substr(base64_decode($isi['USER_CLEARTEXT_PWD']),4, -4));
			$newPassword = substr(base64_decode($isi['USER_CLEARTEXT_PWD']),4, -4) ;
			
			$FResetPass = str_ireplace('[[user_fullname]]',$isi['USER_FULLNAME'],$FResetPass);
			$FResetPass = str_ireplace('[[comp_accid]]',$isi['CUST_ID'],$FResetPass);
			$FResetPass = str_ireplace('[[user_login]]',$isi['USER_ID'],$FResetPass);
			$FResetPass = str_ireplace('[[user_email]]',$isi['USER_EMAIL'],$FResetPass);
			$FResetPass = str_ireplace('[[user_cleartext_password]]',$newPassword,$FResetPass);
			$FResetPass = str_ireplace('[[master_bank_name]]',$templateEmailMasterBankName,$FResetPass);
			$FResetPass = str_ireplace('[[master_bank_email]]',$templateEmailMasterBankEmail,$FResetPass);
			$FResetPass = str_ireplace('[[master_bank_telp]]',$templateEmailMasterBankTelp,$FResetPass);
			
			
			$FResetPass = "<tr><td>".$FResetPass."</td></tr>";
			
			$filename = $user_id.'_PDF';		     
			$this->_helper->download->pdf(null,null,null,$filename,$FResetPass);
			$data = array(
										'USER_RPWD_ISPRINTED' 			=> 1,
									);
			$where =  array();
			$where['CUST_ID 		 = ?'] 	= $cust_id;
			$where['USER_ID 		 = ?'] 	= $user_id;
			$this->_db->update('M_USER',$data,$where);
			
		}

		//Zend_Debug::dump($select);die;
		$select = $this->_db->fetchAll($select);
		$this->paging($select);
		$this->view->fields = $fields;	
		if(!$this->_request->isPost()){
		Application_Helper_General::writeLog('CSRL',' Viewing Customer User Print Or Email Password'); 

		if(!$this->_request->isPost()){
			Application_Helper_General::writeLog('CSCP','View Customer Setup > Reset Password Request List');
		}
		
		}
	}
}
?>
