<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';
class user_DeleteController extends user_Model_User 
{

  public function indexAction() 
  {
    
  	$this->_moduleDB = strtoupper($this->_moduleID['user']);
  	$cust_id = strtoupper($this->_getParam('cust_id'));
    $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;
    
    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
       $password = $sessionNamespace->token; 
       $this->view->token = $sessionNamespace->token;  


    $AESMYSQL = new Crypt_AESMYSQL();
    $PS_NUMBER      = urldecode($this->_getParam('user_id'));
    $user_id = $AESMYSQL->decrypt($PS_NUMBER, $password);

    $user_id = strtoupper($user_id);
//    $user_id = (Zend_Validate::is($user_id,'Alnum') && Zend_Validate::is($user_id,'StringLength',array('min'=>1,'max'=>25)))? $user_id : null;
    $error_remark = null;
    
    if($cust_id)
    {
      $select = $this->_db->select()
                             ->from('M_CUSTOMER',array('CUST_STATUS','CUST_ID'))
                             ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id));
      $cust_data = $this->_db->fetchRow($select);
      if(!$cust_data['CUST_ID'])$cust_id = null;
    }
    
    
    if(!$cust_id)
    {
      $error_remark = 'Customer ID is not found';
      Application_Helper_General::writeLog('UPUS','Delete User');
      $this->_helper->getHelper('FlashMessenger')->addMessage('F');
      $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
      $this->_redirect($this->view->backURL);
    }
    
    
    if($user_id)
    {
       $select = $this->_db->select()
                             ->from('M_USER')
                             ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                             ->where('UPPER(USER_ID)='.$this->_db->quote((string)$user_id));
                             // REMARK CONDITION BY VENNY - 4 FEB 11 -> BIKIN ERR 22 KLO STATUS USER = S/D ->where('UPPER(USER_STATUS)='.$this->_db->quote(strtoupper($this->_masterStatus['code']['inactive'])));
                             //->where('UPPER(USER_STATUS)<>'.$this->_db->quote(strtoupper($this->_masterStatus['code']['active'])));
      $user_data = $this->_db->fetchRow($select);
      
      if($user_data['USER_ID'])
      {
      	
      	  $select = $this->_db->select()
                                 ->from('TEMP_USER',array('TEMP_ID'))
                                 ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                                 ->where('UPPER(USER_ID)='.$this->_db->quote((string)$user_id));
          $result = $this->_db->fetchOne($select);
          if(!$result)
          {
            $info = 'User ID = '.$user_id.', User Name = '.$user_data['USER_NAME'];
            $user_data['USER_STATUS'] = 3;
            try 
            {
		           $this->_db->beginTransaction();
      		  	  $change_id = $this->suggestionWaitingApproval('User List',$info,strtoupper($this->_changeType['code']['delete']),null,'M_USER','TEMP_USER',$user_id,$user_data['USER_FULLNAME'],$cust_id);
      			   $this->insertTempUser($change_id,$user_data);
      			  
      			    //log CRUD
      			   Application_Helper_General::writeLog('UPUS','User has been Updated (delete), User ID : '.$user_id. ' User Name : '.$user_data['USER_FULLNAME'].' Change id : '.$change_id);
      			  
      			   $this->_db->commit();
      			  
      			   $this->setbackURL('/user/userlist');
      			  
      			   $this->_redirect('/notification/submited/index');
      		    }
      		    catch(Exception $e) 
      		    {
        			  $this->_db->rollBack();
        			  $error_remark = $this->language->_('An Error Occured. Please Try Again');
      		    }

              if($error_remark){
                Application_Helper_General::writeLog('UPUS','Delete User');
                $this->_helper->getHelper('FlashMessenger')->addMessage('F');
                $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
                $this->_redirect($this->view->backURL);
              }
          }
          else
          { 
              $error_remark = 'No changes allowed for this record while awaiting approval for previous change';
              $user_id = null;
          }
      	}
      	else
      	{
      	    $user_id = null;
      	}
    }
    
    
    if(!$user_id)
    {
      if(!$error_remark) $error_remark = 'User ID is not found';
      Application_Helper_General::writeLog('UPUS','Delete User');
      $this->_helper->getHelper('FlashMessenger')->addMessage('F');
      $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
      $this->_redirect($this->view->backURL);
    }
  }
}
