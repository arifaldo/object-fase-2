<?php

require_once 'Zend/Controller/Action.php';

class user_DeactivateController extends Application_Main{

  public function indexAction() 
  {
  	$this->_moduleDB = strtoupper($this->_moduleID['user']);
  	$cust_id = strtoupper($this->_getParam('cust_id'));
    $cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? $cust_id : null;
    $user_id = strtoupper($this->_getParam('user_id'));
    $user_id = (Zend_Validate::is($user_id,'Alnum') && Zend_Validate::is($user_id,'StringLength',array('min'=>1,'max'=>25)))? $user_id : null;
    $error_remark = null;

    if($cust_id)
    {
      $select = $this->_db->select()
                             ->from('M_CUSTOMER',array('CUST_ID'))
                             ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                             ->where('UPPER(CUST_STATUS)='.$this->_db->quote(strtoupper($this->_masterStatus['code']['active'])));
      $result = $this->_db->fetchOne($select);
      if(!$result)$cust_id = null;
    }
    
    if(!$cust_id)
    {
      $error_remark = $this->getErrorRemark('22','Customer ID');
      //insert log
      try 
      {
	    $this->_db->beginTransaction();
	    $this->backendLog(strtoupper($this->_changeType['code']['deactivate']),strtoupper($this->_moduleID['user']),null,null,$error_remark);
        $this->_db->commit();
	  }
	  catch(Exception $e) 
	  {
	    $this->_db->rollBack();
  	    Application_Helper_GeneralLog::technicalLog($e);
	  }
	    
	  $this->_helper->getHelper('FlashMessenger')->addMessage('F');
	  $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
      $this->_redirect($this->_backURL);
    }
    
    if($user_id)
    {
      $select = $this->_db->select()
                             ->from('M_USER')
                             ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                             ->where('UPPER(USER_ID)='.$this->_db->quote((string)$user_id))
                             ->where('UPPER(USER_STATUS)='.$this->_db->quote(strtoupper($this->_masterStatus['code']['active'])));
      $user_data = $this->_db->fetchRow($select);
      
      if($user_data['USER_ID'])
      {
      	$select = $this->_db->select()
                               ->from('TEMP_USER',array('TEMP_ID'))
                               ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
                               ->where('UPPER(USER_ID)='.$this->_db->quote((string)$user_id));
        $result = $this->_db->fetchOne($select);
        if(!$result)
        {
          $info = 'User ID = '.$user_id.', User Name = '.$user_data['USER_NAME'];
          $user_data['USER_STATUS'] = strtoupper($this->_masterStatus['code']['inactive']);
          
          try 
          {
		    $this->_db->beginTransaction();
			$change_id = $this->suggestionWaitingApproval('User',$info,strtoupper($this->_changeType['code']['deactivate']),null,'M_USER','TEMP_USER','CUST_ID,USER_ID',$cust_id.','.$user_id);
			$this->insertTempUser($change_id,$user_data);
			$this->_db->commit();
		  }
		  catch(Exception $e) 
		  {
			$this->_db->rollBack();
			$error_remark = $this->getErrorRemark('82');
			Application_Helper_GeneralLog::technicalLog($e);
		  }
        }else{ $error_remark = $this->getErrorRemark('03','User ID'); }
      }else{ $user_id = null; }
    }
    
    if(!$user_id)
    {
      $error_remark = $this->getErrorRemark('22','User ID');
      $fulldesc     = 'CUST_ID:'.$cust_id;
      $key_value    = null;
    }
    else
    {
      $fulldesc  = null;
      $key_value = $cust_id.','.$user_id;
    }
    
    if($error_remark){ $class = 'F'; $msg = $error_remark; }
    else
    {
      $msg = $this->getErrorRemark('00','User ID',$user_id);
      $class = 'S';
    }
    
    //insert log
    try 
    {
	  $this->_db->beginTransaction();
	  $this->backendLog(strtoupper($this->_changeType['code']['deactivate']),strtoupper($this->_moduleID['user']),$key_value,$fulldesc,$error_remark);
      $this->_db->commit();
	}
	catch(Exception $e) 
	{
	  $this->_db->rollBack();
  	  Application_Helper_GeneralLog::technicalLog($e);
	}
	
	$this->_helper->getHelper('FlashMessenger')->addMessage($class);
    $this->_helper->getHelper('FlashMessenger')->addMessage($msg);
    $this->_redirect($this->_backURL);
  }
  
  
   function insertTempUser($change_id,$user_data) 
   {
  	  $content = array('CHANGES_ID'   => $change_id,
					 'CUST_ID'        => $user_data['CUST_ID'],
  	                 'USER_ID'        => $user_data['USER_ID'],
		             'USER_NAME'      => $user_data['USER_NAME'],
                     'USER_EMAIL'     => $user_data['USER_EMAIL'],
                     'USER_PHONE'     => $user_data['USER_PHONE'],
                     'USER_MOBILENO'  => $user_data['USER_MOBILENO'],
                     'USER_DIVISION'  => $user_data['USER_DIVISION'],
                     'USER_STATUS'    => $user_data['USER_STATUS'],
  	                 'FGROUP_ID'      => $user_data['FGROUP_ID'],
  	                 'USER_HASTOKEN'  => $user_data['USER_HASTOKEN']
					);
     if(isset($user_data['TOKEN_SERIALNO']))$content = array_merge($content,array('TOKEN_SERIALNO' => $user_data['TOKEN_SERIALNO']));
	 $this->_db->insert('TEMP_USER',$content);
   }
   
   
}


