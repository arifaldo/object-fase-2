<?php

require_once 'Zend/Controller/Action.php';


class User_IndexController extends Application_Main{
	

	protected $_txStatus = array();
	//protected $moduleDB = 'USF';
	var $user_url = array();
  	var $user_qs;

//	public function initVar(){
//		$this->user_url["module"] = 'user';
//		$this->user_url["controller"] = 'index';
//		$cust_id = $this->_getParam('cust_id');
//		if($cust_id)
//			$this->user_url["cust_id"] = $cust_id;
//		$page = $this->_getParam('page');
//		if($page)
//			$this->user_url["page"] = $page;
//	    $sortBy = $this->_getParam('sortby');
//	    if($sortBy)
//	    	$this->user_url["sortby"] = $sortBy;
//	    $sortDir = $this->_getParam('sortdir');
//		if($sortDir)
//			$this->user_url["sortdir"] = $sortDir;
//		$this->view->user_link = $this->user_url;
//		$this->user_qs = $_SERVER['QUERY_STRING'];
//		$this->view->user_qstring = $this->user_qs;
//   }

	public function indexAction() 
	{
	    $moduleDB = 'USF';
	
	  	$cust_id = $this->_custIdLogin;
	  	//$cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>$this->_custIdLength)))? strtoupper($cust_id) : '';
	  	$cust_id = (Zend_Validate::is($cust_id,'Alnum') && Zend_Validate::is($cust_id,'StringLength',array('min'=>1,'max'=>25)))? strtoupper($cust_id) : '';
	  	
	  	if($cust_id)
	  	{
	  	   $select = $this->_db->select()
	  	                         ->from('M_CUSTOMER',array('CUST_ID','CUST_NAME','CUST_STATUS'))
	  	                         ->where('CUST_ID='.$this->_db->quote((string)$cust_id));
	  	                         
	      $result = $this->_db->fetchRow($select);
	      
	      if($result['CUST_ID'])
	      {
	      	$this->view->cust_name = $result['CUST_NAME'];
	      	$this->view->cust_status = $result['CUST_STATUS'];
	      }
	      else{ $cust_id = null; }
	  	}
	  	
	  	if($cust_id)
	  	{
		  $fields = array('userid'   => array('field'    => 'USER_ID',
	                                          'label'    => 'User ID',
	                                          'sortable' => true),
	    
	                      'username' => array('field'    => 'USER_NAME',
	                                          'label'    => 'Name',
	                                          'sortable' => true),
	    
	                      'fgroup'   => array('field'    => 'g.FGROUP_NAME',
	                                          'label'    => 'Frontend Group',
	                                          'sortable' => true),
	    
	                      'status'   => array('field'    => 'USER_STATUS',
	                                          'label'    => 'Status',
	                                          'sortable' => true),
	
	                      'token'    => array('field'    => 'USER_HASTOKEN',
	                                          'label'    => 'Has Token',
	                                          'sortable' => true)
	                     );
	
	      $page = $this->_getParam('page');
	      $sortBy = $this->_getParam('sortby');
	      $sortDir = $this->_getParam('sortdir');
		  $page = (Zend_Validate::is($page,'Digits'))? $page : 1;
	      $sortBy = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
	      $sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
	
	      $filterArr = array('filter' => array('StripTags','StringTrim'),
	                         'uid'    => array('StripTags','StringTrim','StringToUpper'),
	                         'uname'  => array('StripTags','StringTrim','StringToUpper'),
	                         'status' => array('StripTags','Alnum','StringToUpper')
	                        );
	                        
	      $zf_filter = new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
	      $filter    = $zf_filter->getEscaped('filter');
	      $this->view->currentPage = $page;
	      $this->view->sortBy      = $sortBy;
	      $this->view->sortDir     = $sortDir;
	      $this->view->user_msg    = $this->_helper->getHelper('FlashMessenger')->getMessages();
	    
	      
	      if($filter == 'Filter')
	      {
	      	$haystack_status = array($this->_masteruserStatus['code']['active'],
	      							 $this->_masteruserStatus['code']['inactive'],
	      							 $this->_masteruserStatus['code']['suspended'],
	      							 $this->_masteruserStatus['code']['disable']);
	        $uid = html_entity_decode($zf_filter->getEscaped('uid'));
	        $uname = html_entity_decode($zf_filter->getEscaped('uname'));
	        $status = $zf_filter->getEscaped('status');
	        $status = (Zend_Validate::is($status,'InArray',array('haystack'=>$haystack_status)))? $status : '';
	        $this->view->uid = $uid;
	        $this->view->uname = $uname;
	        $this->view->status = $status;
	      }
	
	      if($filter)
	      {
		    $select = $this->_db->select()
			  				       ->from(array('u'=>'M_USER'),array('USER_ID','USER_NAME','USER_HASTOKEN','USER_STATUS'))
			  				       ->join(array('g'=>'M_FGROUP'),'g.FGROUP_ID=u.FGROUP_ID',array('FGROUP_NAME'))
			  				       ->where('u.CUST_ID='.$this->_db->quote((string)$cust_id));
	      }else{ $select = array(); }
	    
	      
	      if($filter == 'Filter')
	      {
	        if($uid)$select->where('UPPER(USER_ID) LIKE '.$this->_db->quote('%'.strtoupper($uid).'%'));
	        if($uname)$select->where('UPPER(USER_NAME) LIKE '.$this->_db->quote('%'.strtoupper($uname).'%'));
	        if($status)$select->where('USER_STATUS='.$this->_db->quote((string)$status));
	      }
	    
	      if($filter)$select->order($sortBy.' '.$sortDir);                                   
	      $this->paging($select);
	      $this->view->fields = $fields;
	      $this->view->filter = $filter;
	  	}
		
	  	
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
    	
		Application_Helper_General::writeLog('ULST','Viewing');
	  	$this->view->change_type = $this->_changeType;
	    $this->view->status_type = $this->_masteruserStatus;
	    $this->view->ynstatus_type = $this->_masterhasStatus;
	  	$this->view->cust_id = $cust_id;
	  	$this->view->btn_add = '/'.$this->_request->getModuleName().'/new/index/';
	  	$this->view->btn_view = '/'.$this->_request->getModuleName().'/view/index/user_id/';
	  	$this->view->btn_active = '/'.$this->_request->getModuleName().'/active/index/user_id/';
	  	
	}
	
	public function successAction()
	{
		$this->_redirect($this->_backURL);
	}
	

}

