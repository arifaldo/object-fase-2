<?php

require_once 'Zend/Controller/Action.php';

class CheckBalance_IndexController extends Application_Main{
	
	protected $_moduleDB = null;
	
	public function initController()
	{        
		  $this->view->masterhasStatusDesc = $this->_masterhasStatus['desc'];
		  $this->view->masterhasStatusCode = array_flip($this->_masterhasStatus['code']);
		  
		  $this->view->masterStatus = $this->_masterStatus;
		  $this->_txStatusCode      = $this->_txStatus['code'];
          $this->_txStatusDesc      = $this->_txStatus['desc'];
          
		  $this->view->dateDisplayFormat     = $this->_dateDisplayFormat;
		  $this->view->dateTimeDisplayFormat = $this->_dateTimeDisplayFormat;
		  $this->view->dateDBFormat          = $this->_dateDBFormat;
		  $this->view->dateDBDefaultFormat   = $this->_dateDBDefaultFormat;
		  
		  $this->_helper->layout()->setLayout('popup');
	 }
	
	public function indexAction() 
	{
		$this->_moduleDB = 'POP';	
	}
	
	
	function authSessionAccount($accountNo)
	{
		$concatMemberId = '';
		
		$anchorCust = $this->_db->fetchOne(
							    $this->_db->select()
							         ->from(array('A' => 'M_ANCHOR'), array('ANCHOR_ID'))
							         ->where("A.ANCHOR_CUST=?", $this->_custIdLogin));
		
		$memberId = $this->_db->select()
						->from(array('S' => 'M_SCHEME'), array())
						->join(array('C' => 'M_COMMUNITY'), 'C.SCHEME_ID=S.SCHEME_ID', array())
						->join(array('M' => 'M_MEMBER'), 'M.COMM_ID=C.COMM_ID', array('MEMBER_ID'))
						->where("S.ANCHOR_ID = ?", $anchorCust)
						->query()->fetchAll();

		$memberId = Application_Helper_Array::simpleArray($memberId,'MEMBER_ID');
		
		$concatMemberId = implode(',', $memberId);	

		$memberId = $this->_db->select()
					          ->from(array('M' => 'M_MEMBER'), array('MEMBER_ID'))
					          ->where("M.MEMBER_CUST=?", $this->_custIdLogin)
					          ->query()->fetchAll();
		
		$memberId = Application_Helper_Array::simpleArray($memberId,'MEMBER_ID');
		
		$memberId = implode(',', $memberId);
		
		if($memberId!="")
		{
			if($concatMemberId!="")  $concatMemberId = $concatMemberId.','.$memberId;
			else  $concatMemberId = $memberId;
		}

		if($concatMemberId=='')  $concatMemberId = 'NULL';
		
		$memberAcct = $this->_db->select()
					            ->from(array('M' => 'M_MEMBER_ACCT'), array('LOAN_ACCT_NO'))
								->where("M.MEMBER_ID IN (" . $concatMemberId . ")")
								->where("M.LOAN_ACCT_NO=?", $accountNo)
								->query()->fetchAll();
					
		if(count($memberAcct)==0) return false;
		
		return true;
	}
	
	
	public function failedAction()
	{
		$this->_redirect('/popuperror/index/index');
	}

}


	

