<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/SinglePayment.php';
require_once 'CMD/Validate/ValidatePaymentMultiple.php';
require_once 'General/CustomerUser.php';

class vapayment_IndexController extends Application_Main
{
	protected 	$_moduleDB 	= 'RTF';
	protected	$_rowNum 	= 0;
	protected	$_errmsg 	= null;	
	protected	$params		= null;
	
	public function initController()
	{
		$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';
		$this->_listCCYValidate = Application_Helper_Array::simpleArray($this->getCcy(),'CCY_ID');  	
	}

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$this->view->binArr = $CustomerUser->getCustomerBin();
		$VA_OPEN_CLOSE		= $this->getSetting('VA_OPEN_CLOSE'); //1=close only, 2=open + close va
		$this->view->VA_OPEN_CLOSE = $VA_OPEN_CLOSE;
		
		if($this->_request->isPost() )
		{
			$this->view->cust_bin 		= $this->_getParam('cust_bin');
					
			$adapter1 					= new Zend_File_Transfer_Adapter_Http();
			$params 					= $this->_request->getParams();
			$sourceFileName 			= $adapter1->getFileName();
			$cust_bin 					= $this->_getParam('cust_bin');


			$repettimes 				= $this->_getParam('repettimes');
			$repettype 					= $this->_getParam('repettype');
			$repetdate 					= $this->_getParam('repetdate');

			$startdate 					= $this->_getParam('ps_startdate');
			$repeat 					= $this->_getParam('repetition');
			
			if($sourceFileName == null)
			{
				$sourceFileName = null;
			}
			else
			{
				$sourceFileName = substr(basename($adapter1->getFileName()), 0);
			}
			
			$paramsName['sourceFileName'] 	= $sourceFileName;
			$paramsName['cust_bin'] 	= $cust_bin;
			
			$filtersName = array(
									'sourceFileName'		=> array('StringTrim'),
									'cust_bin'		=> array('StringTrim'),
									'ps_startdate'		=> array('StringTrim'),
									'repettype'		=> array('StringTrim'),
									'repetdate'		=> array('StringTrim'),
									'repettimes'		=> array('StringTrim'),
									'vatype'		=> array('StringTrim'),
								);
			
			$validatorsName = array(
									'sourceFileName' => array(
															'NotEmpty',
															'messages' => array(
																					"Error : File size too big or left blank"
																				)
														),
									// 'cust_bin' => array(
									// 						'NotEmpty',
									// 						'messages' => array(
									// 												"Error : File BIN cannot left blank. Please selectt"
									// 											)
									// 					),
									// 'ps_startdate' => array(
									// 						'NotEmpty',
									// 						'messages' => array(
									// 												"Error : File BIN cannot left blank. Please selectt"
									// 											)
									// 					),
									'repettype' => array(),
									'repetdate' => array(),
									'repettimes' => array(),
									'vatype' => array(),

																												
								);
			$zf_filter_input_name = new Zend_Filter_Input($filtersName, $validatorsName, $paramsName, $this->_optionsValidator);
			
			if($cust_bin == '')
			{
				$this->view->error4		= true;
			}
			else{
				if($zf_filter_input_name->isValid())
				{
					// die;
					$filter 	= new Application_Filtering();
					$adapter 	= new Zend_File_Transfer_Adapter_Http ();
					$validate   = new ValidatePaymentMultiple($this->_custIdLogin, $this->_userIdLogin);
					$max		= $this->getSetting('max_import_single_payment');
					
					$adapter->setDestination ( $this->_destinationUploadDir );
					$extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv'));
					$extensionValidator->setMessage(
														'Error: Extension file must be *.csv'
													);
					
					$sizeValidator = new Zend_Validate_File_Size(array('min' => 0, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
					$sizeValidator->setMessage(
												'Error: File siz must not more than '.$this->getSetting('Fe_attachment_maxbyte')
											);
							
					$adapter->setValidators(array($extensionValidator,$sizeValidator));
						
					if ($adapter->isValid ()) 
					{
						// die('here');
						$sourceFileName = $adapter->getFileName();
						$newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';
							
						$adapter->addFilter ( 'Rename',$newFileName  );
							
						if ($adapter->receive ())
						{
							$Csv = new Application_Csv ($newFileName,",");
							$csvData = $Csv->readAll ();
							$csvData2 = $Csv->readAll();
							
							// Zend_Debug::dump($csvData);
							// die;
							
							@unlink($newFileName);
							//Zend_Debug::dump($csvData);die;
							//end
	
							$totalRecords2 = count($csvData2);
	
							if($totalRecords2)
								{
									for ($a= 1; $a<$totalRecords2; $a++ ){
										unset($csvData2[$a]);
									}
	//								unset($csvData2[1]);
	//								unset($csvData2[2]);
	//								unset($csvData2[3]);
									$totalRecords2 = count($csvData2);
							}
	
	
							foreach ( $csvData2 as $row )
							{
								$type =  trim($row[0]);
							}
	
							$totalRecords = count($csvData);
							if($totalRecords)
							{
								unset($csvData[0]);
								$totalRecords = count($csvData);
							}
							// print_r($csvData);die;
							if ($totalRecords){
								if($totalRecords <= $max)
								{

									$no =0;
									foreach ( $csvData as $columns )
									{
										if(count($columns)==3)
										{
											// die;
											if (strtoupper(trim($type)) == 'VA_NUMBER'){ //ini statis va(Open)
												$params['VP_NUMBER'] 			= trim($columns[0]);
												$params['INVOICE_NAME'] 		= trim($columns[1]);
												$params['INVOICE_DESC'] 		= trim($columns[2]);
												
												$VP_NUMBER 			= $filter->filter($params['VP_NUMBER'],"VP_NUMBER");
												$INVOICE_NAME 		= $filter->filter($params['INVOICE_NAME'],"INVOICE_NAME");
												$INVOICE_DESC 		= $filter->filter($params['INVOICE_DESC'],"INVOICE_DESC");
												$CCY 				= 'IDR';
												
												$AMOUNT 			= 0;
												$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($AMOUNT);
												
												$paramPayment = array(
														"CATEGORY" 					=> 'VIRTUAL PAYMENT',
														"FROM" 						=> "I",				// F: Form, I: Import
														//"PS_NUMBER"					=> "",
														"VA_TYPE"					=> 'Open',
														"PS_EFDATE"					=> date('d/m/y'),
														"_dateFormat"				=> $this->_dateUploadFormat,
														"_dateDBFormat"				=> $this->_dateDBFormat,
												);
									
												$paramTrxArr[0] = array(
														"TRANSFER_TYPE" 			=> 'VP',
														"TRA_AMOUNT" 				=> $TRA_AMOUNT_num,
														"ACBENEF_CCY" 				=> strtoupper($CCY),
														"COMP_ID" 					=> $this->_custIdLogin,
														"VP_NUMBER" 				=> $VP_NUMBER,
														"INVOICE_NAME" 				=> $INVOICE_NAME,
														"INVOICE_DESC" 				=> $INVOICE_DESC,

														"REPEAT" 					=> $repeat,
														"REPET_TYPE" 				=> $repettype,
														"REPET_DATE" 				=> $repetdate,
														"REPET_TIMES" 				=> $repettimes,
														"START_DATE" 				=> $startdate,

														"CUST_BIN" 					=> $cust_bin,
												);
											
											}
											else{ //dinamis va (Close)
												// die('here');
	//											$params['VP_NUMBER'] 			= trim($columns[0]);
												$params['INVOICE_NAME'] 		= trim($columns[0]);
												$params['INVOICE_DESC'] 		= trim($columns[1]);
	//											$params['CCY'] 					= trim($columns[3]);
												$params['AMOUNT'] 				= trim($columns[2]);
												
												$INVOICE_NAME 		= $filter->filter($params['INVOICE_NAME'],"INVOICE_NAME");
												$INVOICE_DESC 		= $filter->filter($params['INVOICE_DESC'],"INVOICE_DESC");
												$CCY 				= 'IDR';
												$AMOUNT 			= $filter->filter($params['AMOUNT'],"VP_AMOUNT");
													
												$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($AMOUNT);
												
												$paramPayment = array(
														"CATEGORY" 					=> 'VIRTUAL PAYMENT',
														"FROM" 						=> "I",				// F: Form, I: Import
														//"PS_NUMBER"					=> "",
														"VA_TYPE"					=> 'Close',
														"PS_EFDATE"					=> date('d/m/y'),
														"_dateFormat"				=> $this->_dateUploadFormat,
														"_dateDBFormat"				=> $this->_dateDBFormat,
												);
									
												$paramTrxArr[0] = array(
														"TRANSFER_TYPE" 			=> 'VP',
														"TRA_AMOUNT" 				=> $TRA_AMOUNT_num,
														"ACBENEF_CCY" 				=> strtoupper($CCY),
														"COMP_ID" 					=> $this->_custIdLogin,
														"VP_NUMBER" 				=> '',
														"INVOICE_NAME" 				=> $INVOICE_NAME,
														"INVOICE_DESC" 				=> $INVOICE_DESC,
														"REPEAT" 					=> $repeat,
														"REPET_TYPE" 				=> $repettype,
														"REPET_DATE" 				=> $repetdate,
														"REPET_TIMES" 				=> $repettimes,
														"START_DATE" 				=> $startdate,

														"CUST_BIN" 					=> $cust_bin,
												);
											}
												
											$arr[$no]['paramPayment'] = $paramPayment;
											$arr[$no]['paramTrxArr'] = $paramTrxArr;
										}
										else
										{
											$this->view->error 		= true;
											break;
										}
										// print_r($arr);die;
										$no++;
									}
										
									if(!$this->view->error)
									{
										// print_r($arr); die;
										$resultVal	= $validate->checkCreateVp($arr);
										// print_r($resultVal);die;
										$payment 	= $validate->getPaymentInfo();
										$errorTrxMsg 	= $validate->getErrorTrxMsg();
											
										$content['payment'] = $payment;
										$content['arr'] 	= $arr;
										$content['errorTrxMsg'] 	= $errorTrxMsg;
										$sessionNamespace = new Zend_Session_Namespace('confirmImportCredit');
										$sessionNamespace->content = $content;
										$this->_redirect('/vapayment/index/confirm');
									}
								}
								else
								{
									$this->view->error2 = true;
									$this->view->max 	= $max;
								}
							}else{
								$this->view->error = true;
							}
						}
					}
					else
					{
						$this->view->error = true;
						$errors = array($adapter->getMessages());
						$this->view->errorMsg = $errors;
					}
				}
				else
				{
					$this->view->error3		= true;
				}
			}
				
		}
		Application_Helper_General::writeLog('IFVP','Viewing Virtual Payment by Import File (CSV)');
		
		
	}

	public function confirmAction()
	{
		$sessionNamespace = new Zend_Session_Namespace('confirmImportCredit');
		$data = $sessionNamespace->content;
		
		$vpNumber 	= array('');
//		$vpType 	= array('');
		foreach($data["arr"] as $row)
		{
			if($row['paramTrxArr'][0]['VP_NUMBER'] != ''){
				array_push($vpNumber, $row['paramTrxArr'][0]['VP_NUMBER']);
				$vpTypeP = 'Open';
			}
		}
		$vpType = (!empty($vpTypeP)?$vpTypeP:'');
		
		if(!$data["payment"]["countTrxCCY"])
		{
			$this->_redirect("/authorizationacl/index/nodata");
		}
		
		$this->view->countTrxCCY = $data["payment"]["countTrxCCY"];
		
		$this->view->sumTrxCCY = $data["payment"]["sumTrxCCY"];
		
		$totalSuccess = 0;
		foreach($data["payment"]["countTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$totalSuccess += $ccy['success'];
			}
		}
		
		$amountSuccess = 0;
		foreach($data["payment"]["sumTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$amountSuccess += $ccy['success'];
			}
		}
		
		$totalFailed = 0;
		foreach($data["payment"]["countTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$totalFailed += $ccy['failed'];
			}
		}
		
		$amountFailed = 0;
		foreach($data["payment"]["sumTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$amountFailed += $ccy['failed'];
			}
		}
		
		$cust_bin = 0;
		foreach($data["arr"] as $row)
		{
			$cust_bin =$row['paramTrxArr'][0]['CUST_BIN'];
		}
		
		if(count(array_unique($vpNumber))<count($vpNumber))
		{
//		    echo 'Array has duplicates';
//			$vals = array_count_values($vpNumber);
			$totalFailed = 1;
			$totalFailedVp = 1;
		}
		
		function showDups($vpNumber)
		{
		   $array_temp = array();
		   $i = 1;
		   foreach($vpNumber as $val)
		   {
		     if (!in_array($val, $array_temp))
		     {
		       $array_temp[] = $val;
		     }
		     else
		     {
		       echo '
		       <b><font color="red">'.$i++. '. Duplicate VA Number : ' . $val . ' in the file.</b></font><br />';
		     }
		   }
		}
		
		showDups($vpNumber);
		       
		$this->view->totalSuccess = $totalSuccess;
		$this->view->amountSuccess = $amountSuccess;
		$this->view->totalFailed = $totalFailed;
		$this->view->totalFailedVp = (!empty($totalFailedVp)?$totalFailedVp:'');
		
		$this->view->amountFailed = $amountFailed;
		$this->view->cust_bin = $cust_bin;
		
		$fields = array(
						'PaymentType'     			 => array(
														'field'    => '',
														'label'    => $this->language->_('BIN'),
														'sortable' => false
													),
//						'CCY'     			 => array(
//														'field'    => '',
//														'label'    => $this->language->_('CCY'),
//														'sortable' => false
//													),
						'Payment Success'     			 => array(
														'field'    => '',
														'label'    => $this->language->_('Payment Success'),
														'sortable' => false
													),
						'Total Amount Success'     			 => array(
														'field'    => '',
														'label'    => $this->language->_('Total Amount Success'),
														'sortable' => false
													),
						'Payment Failed'     			 => array(
														'field'    => '',
														'label'    => $this->language->_('Payment Failed'),
														'sortable' => false
													),
						'Total Amount Failed'     			 => array(
														'field'    => '',
														'label'    => $this->language->_('Total Amount Failed'),
														'sortable' => false
													),
                        );
		$this->view->fields = $fields;
		
		
			$selectCountUser = $this->_db->select()
		  	                   ->from('T_VP_COUNTER',array('*'))
		  	                   ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$this->_custIdLogin));
//	  	                       ->where("SUBSTRING(VP_CREATED,1,10)= ?", $dateNow)
//	  	                       ->where("VP_MODE= ?", 1);
//	  	                       ->where("A.PAID_STATUS in (1,2)");	
		    $dataCountUser = $this->_db->fetchRow($selectCountUser);   

			$date_val	= date('Y-m-d');
			$dataCountUpdated = substr($dataCountUser['COUNTER_UPDATED'], 0,10);
			
		    $jumUser = $dataCountUser['COUNTER'];
			
			if($jumUser == 0 || $jumUser == ''){
				$lastCounter = 1;
				$totalLastData = $totalSuccess;
			}
			else{
				$lastCounter = $jumUser+1;
				$totalLastData = $totalSuccess+$jumUser;
			}
			
			$i = $lastCounter;
			
			if($date_val > $dataCountUpdated){
				$where = array('CUST_ID = ?' => $this->_custIdLogin);
				$this->_db->delete('T_VP_COUNTER',$where);
//				echo 'sudah lewat tanggal hapus counter';
			}
			else{
				if($vpType == 'Open'){
				}
				else{
					if ($totalLastData >= 999999 || $i >= 999999)
					{
						unset($_SESSION['confirmImportCredit']);
//						redirect(buildEncLink('warning_payment_overquota.php',''));
						$this->_redirect('/virtualpayment/warning');
					}
				}
			}
		
		if($this->_request->isPost() )
		{			
			if($this->_getParam('submit_cancel') == TRUE)
			{
				unset($_SESSION['confirmImportCredit']); 
				$this->_redirect('/virtualpayment/new');
			}
			
			foreach($data["arr"] as $row)
			{
				// print_r($row['paramTrxArr'][0]);die;
				$param['COMP_ID'] 			= $this->_userIdLogin;
				//$param['PS_EFDATE']  		= Application_Helper_General::convertDate($row['paramPayment']['PS_EFDATE'], $this->_dateDBFormat, $this->_dateUploadFormat);
//				$param['INSTITUTIONS_ID'] 	= $row['paramTrxArr'][0]['INSTITUTIONS_ID'];
				$param['VP_NUMBER']  		= $row['paramTrxArr'][0]['VP_NUMBER'];
				$param['INVOICE_NAME']  	= $row['paramTrxArr'][0]['INVOICE_NAME'];
				$param['INVOICE_DESC']		= $row['paramTrxArr'][0]['INVOICE_DESC'];
				$param['CCY'] 				= $row['paramTrxArr'][0]['ACBENEF_CCY'];
				$param['AMOUNT'] 			= $row['paramTrxArr'][0]['TRA_AMOUNT'];

				$timestamp = strtotime(str_replace('/', '.', $row['paramTrxArr'][0]['REPET_DATE']));
				$repetdate = date('Y-m-d', $timestamp); 
				$param['REPET_DATE']		= $repetdate;
				$param['REPET_TIMES'] 			= $row['paramTrxArr'][0]['REPET_TIMES'];
				$timestampef = strtotime(str_replace('/', '.', $row['paramTrxArr'][0]['VP_EFDATE']));
				$vp_efdate = date('Y-m-d', $timestampef); 
				$param['START_DATE'] 			= $vp_efdate;
				$param['REPEAT'] 			= $row['paramTrxArr'][0]['REPEAT'];
				$param['REPET_TYPE'] 			= $row['paramTrxArr'][0]['REPET_TYPE'];
				$param['REPEAT_EVERY'] 			= $row['paramTrxArr'][0]['REPET_EVERY'];
				if(empty($row['paramTrxArr'][0]['DUE_DATE_TYPE'])){
					$row['paramTrxArr'][0]['DUE_DATE_TYPE'] = '0';
				}
				$param['DUE_DATE_TYPE'] 			= $row['paramTrxArr'][0]['DUE_DATE_TYPE'];

				$param['CUST_BIN'] 			= $row['paramTrxArr']['0']['CUST_BIN'];

				$param['VP_CREATEDBY'] 			= $row['paramTrxArr'][0]['VP_CREATEDBY'];
				$param['TRANSFER_TYPE'] 			= $row['paramTrxArr'][0]['TRANSFER_TYPE'];
				$param['REMINDER_SMS'] 			= $row['paramTrxArr'][0]['REMINDER_SMS'];
				$param['REMINDER_EMAIL'] 			= $row['paramTrxArr'][0]['REMINDER_EMAIL'];
				$param['REPEAT'] 			= $row['paramTrxArr'][0]['REPEAT'];
				$param['REPET_TYPE'] 			= $row['paramTrxArr'][0]['REPET_TYPE'];
				$timestamprd = strtotime(str_replace('/', '.', $row['paramTrxArr'][0]['REPET_DATE']));
				$repet_date = date('Y-m-d', $timestamprd); 
				$param['REPET_DATE'] 			= $repet_date;
				$param['REPET_TIMES'] 			= $row['paramTrxArr'][0]['REPET_TIMES'];

				$param['REMINDER_NOTIFICATION'] = $row['paramTrxArr'][0]['REMINDER_NOTIFICATION'];
				$param['VP_TIME'] 			= $row['paramTrxArr'][0]['VP_TIME'];
				$param['VP_AFTER'] 			= $row['paramTrxArr'][0]['VP_AFTER'];
				// print_r($row);die;
				// $timestampdue = strtotime(str_replace('/', '.', $row['paramTrxArr'][0]['DUE_DATE']));
				// $due_date = date('Y-m-d', $timestampdue); 
				$param['DUE_DATE'] 			= $row['paramTrxArr'][0]['DUE_DATE'];
				$param['VP_PAYMENT'] 			= $row['paramTrxArr'][0]['VP_PAYMENT'];
				$param['VP_VALIDATE'] 			= $row['paramTrxArr'][0]['VP_VALIDATE'];
				
				$param['TOTAL'] 			= $totalLastData;
//				$param['TOTAL_TEMP'] 		= $totalSuccess;
				$param['TOTAL_COUNTER'] 	= $i++;
				
//				$param['AREA'] 				= $row['paramTrxArr'][0]['AREA'];
				$param['BENEFICIARY_ACCOUNT'] = $row['paramTrxArr'][0]['BENEFICIARY_ACCOUNT'];
				$param['BENEFICIARY_NAME'] 	  = $row['paramTrxArr'][0]['BENEFICIARY_NAME'];

//				$param['_addBeneficiary'] 			= $row['paramPayment']['_addBeneficiary'];
//				$param['_beneLinkage'] 				= $row['paramPayment']['_beneLinkage'];
//				$param["_dateFormat"]				= $row['paramPayment']['_dateFormat'];
//				$param["_dateDBFormat"]				= $row['paramPayment']['_dateDBFormat'];
//				$param["_createPB"]					= $row['paramPayment']['_createPB'];
//				$param["_createDOM"]				= $row['paramPayment']['_createDOM'];
//				$param["_createREM"]				= $row['paramPayment']['_createREM'];
	  			// print_r($param);die;
				try 
				{
					$SinglePayment = new SinglePayment("", $this->_custIdLogin, $this->_userIdLogin);					
					//$result = $SinglePayment->createPayment($param);
					// Zend_Debug::dump($param);die;
					$result = $SinglePayment->createVirtualPayment($param);
					
				}
				catch(Exception $e)
				{
					Application_Helper_General::exceptionLog($e);
					$errr = 1;
				}
			}
			unset($sessionNamespace->content);
			
			$this->_helper->getHelper('FlashMessenger')->addMessage('/vapayment/index/');
			// die;
			if ($errr != 1)
			{	$this->_redirect('/notification/success/index');	}
			else
			{	$this->_redirect('/notification/success/index');	}	// TODO: what to do, if failed create payment
			
		}
		Application_Helper_General::writeLog('IFVP','Confirm Virtual Payment by Import File (CSV)');
	
	}

	
}
