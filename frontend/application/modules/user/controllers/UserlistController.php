<?php
require_once 'Zend/Controller/Action.php';
class user_UserlistController extends user_Model_User
{

	public function indexAction()
	{
		$this->setbackURL();
		$this->_helper->layout()->setLayout('newlayout');
		$cust_id = $this->_custIdLogin;
		$this->view->cust_id = $cust_id;
		$user_id = $this->_userIdLogin;
		$this->view->user_id = $user_id;
		$setting = new Settings();
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$pw_hash = md5($enc_salt . $enc_pass);
		$rand = $this->_userIdLogin . date('dHis') . $pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if (count($temp) > 1) {
			if ($temp[0] == 'F' || $temp[0] == 'S') {
				if ($temp[0] == 'F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = '';
				unset($temp[0]);
				foreach ($temp as $value) {
					if (!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}
		}

		$fields = array(
			// 'User ID'  			=> array	(
			// 										'field' => 'USER_ID',
			// 										'label' => $this->language->_('User ID'),
			// 										'sortable' => true
			// 									),
			'User Name'  			=> array(
				'field' => 'USER_FULLNAME',
				'label' => $this->language->_('User Name'),
				'sortable' => true
			),
			'User Email'  			=> array(
				'field' => 'USER_EMAIL',
				'label' => $this->language->_('User Email'),
				'sortable' => true
			),
			// 'Privilege Role Name'  			=> array(
			// 	'field' => '',
			// 	'label' => $this->language->_('Privilege Role Name'),
			// 	'sortable' => false
			// ),
			'Status'  			=> array(
				'field' => 'USER_STATUS',
				'label' => $this->language->_('Status'),
				'sortable' => true
			),
			'Last Login'  			=> array(
				'field' => 'USER_LASTLOGIN',
				'label' => $this->language->_('Last Login'),
				'sortable' => true
			),
			// 'Web Service'  			=> array	(
			// 										'field' => 'USER_ISWEBSERVICES',
			// 										'label' => $this->language->_('Web Service'),
			// 										'sortable' => true
			// 									)
			'Login'  			=> array(
				'field' => '',
				'label' => $this->language->_('Login'),
				'sortable' => false
			),

			'Action'  			=> array(
				'field' => '',
				'label' => $this->language->_('Action'),
				'sortable' => false
			),

		);

		$this->view->checkUnlockResetUser = false;
		$this->view->checkForceLogout = false;
		if ($this->view->hasPrivilege('UPUS')) {
			// $fields += array(
			// 					'Unlock and Reset' =>	array	(
			// 																			'field' => '',
			// 																			'label' => $this->language->_('Unlock and Reset'),
			// 																			'sortable' => false
			// 																		) 
			// 					);
			$this->view->checkUnlockResetUser = true;
		}
		if ($this->view->hasPrivilege('UFLG')) {
			// $fields += array(
			// 					'Force Logout' =>	array	(
			// 																			'field' => 'USER_ISLOGIN',
			// 																			'label' => $this->language->_('Force Logout'),
			// 																			'sortable' => true
			// 																		) 
			// 					);
			$this->view->checkForceLogout = true;
		}

		if ($this->view->hasPrivilege('UPUS')) {
			// die;
			$this->view->checkUpdateUser = true;
		}
		//get page, sortby, sortdir
		$csv    = $this->_getParam('csv');
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby', 'User ID');
		$sortDir = $this->_getParam('sortdir', 'asc');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page, 'Digits') && ($page > 0)) ? $page : 1;

		$sortBy = (Zend_Validate::is(
			$sortBy,
			'InArray',
			array(array_keys($fields))
		)) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is(
			$sortDir,
			'InArray',
			array('haystack' => array('asc', 'desc'))
		)) ? $sortDir : 'asc';


		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;

		$select =	$this->_db->select()
			->from('M_USER', array(
				'USER_ID',
				'USER_FULLNAME',
				'USER_EMAIL',
				'USER_STATUS',
				'USER_LASTLOGIN',
				// 'USER_ISWEBSERVICES', 
				'USER_ISLOGIN',
				'USER_RRESET',
				'USER_RPWD_ISBYBO',
				'USER_ISLOCKED',
				'FGROUP_ID',
				'USER_RCHANGE'
			))
			->where('CUST_ID = \'' . $cust_id . '\'');
		// ->where('USER_STATUS != 3');

		$select_temp =	$this->_db->select()
			->from('TEMP_USER', array(
				'USER_ID',
				'USER_FULLNAME',
				'USER_EMAIL',
				'USER_STATUS',
				'USER_LASTLOGIN',
				// 'USER_ISWEBSERVICES', 
				'USER_ISLOGIN',
				'USER_RRESET',
				'USER_RPWD_ISBYBO',
				'USER_ISLOCKED',
				'FGROUP_ID',
				'USER_RCHANGE'
			))
			->where('CUST_ID = \'' . $cust_id . '\'');
		// ->where('USER_STATUS != 3');
		$temp_result = $this->_db->fetchAll($select_temp);
		// print_r($temp_result);die;
		$this->view->temp = $temp_result;
		// 							echo $select;
		Application_Helper_General::writeLog('ULST', 'View User List');
		$select->order($sortBy . ' ' . $sortDir);


		$result = $this->_db->fetchAll($select);


		foreach ($result as $resultKey => $resultValue) {
			//----------------find user priv name--------------------
			//-------------------------------------------user privilege-------------------------------------------------------------
			$fuser_id  = $this->_custIdLogin . $resultValue['USER_ID'];
			$privilege = $this->_db->select()
				->from('M_FPRIVI_USER')
				->where('FUSER_ID =' . $this->_db->quote($fuser_id))
				->query()->fetchAll();

			if (count($privilege) > 0) {
				$priviListArr = Application_Helper_Array::simpleArray($privilege, 'FPRIVI_ID');
				$this->view->priviview = $priviListArr;
			}

			//modifikasi privilege agar bisa ditampilkan sesuai golongan
			$this->view->getModuleDescArr = $this->getModuleDescArr();

			$privilege_final = array();
			$privilege_final_modif = array();

			$getprivilege = $this->getprivilege();
			foreach ($getprivilege as $row) {
				$fprivi_moduleid = trim($row['FPRIVI_MODULEID']);
				$privilege_final[$fprivi_moduleid][] = $row;
				$privilege_final_modif = $this->modifPrivi($privilege_final);
			}

			$setting = new Settings();
			$system_type = $setting->getSetting('system_type');
			$this->view->fprivilege = $privilege_final_modif;
			$this->view->template   = Application_Helper_Array::listArray($this->getTemplate($system_type), 'FTEMPLATE_ID', 'FTEMPLATE_DESC');

			$priviTemplate = array();
			foreach ($this->getPriviTemplate() as $row) {
				$priviTemplate[$row['FTEMPLATE_ID']][$row['FPRIVI_ID']] = $row['FPRIVI_DESC'];
			}
			//var_dump($priviTemplate);
			$this->view->priviTemplate = $priviTemplate;

			$flag_template = 0;
			$user_role     = '';
			$template_id   = '';
			if (count($priviTemplate) > 0) {
				foreach ($priviTemplate as $key => $row) {
					$count_row = count($row);
					$count_priviListArr = count($priviListArr);
					if ($count_row == $count_priviListArr) {
						$arr_diff = array_diff_key(array_flip($priviListArr), $row);
						if (count($arr_diff) == 0) {
							$template_id = $key;
							$flag_template = 1;
							break;
						}
					}
				}
			}

			if ($flag_template == 0) {
				if (!empty($resultValue['FGROUP_ID'])) {
					$template_id = $resultValue['FGROUP_ID'];
				} else {
					$template_id = 'A01';
				}

				$select = $this->_db->select()
					->from('M_FTEMPLATE')
					->where('UPPER(FTEMPLATE_ID)=' . $this->_db->quote($template_id))
					->query()->fetch();
				$user_role = $select['FTEMPLATE_DESC'];
			} else if ($flag_template == 1) {
				$select = $this->_db->select()
					->from('M_FTEMPLATE')
					->where('UPPER(FTEMPLATE_ID)=' . $this->_db->quote($template_id))
					->query()->fetch();
				$user_role = $select['FTEMPLATE_DESC'];
			}

			$result[$resultKey]['USER_ROLE_NAME'] = $user_role;

			//-------------------------------------------end user privilege-------------------------------------------------------------
		}

		$this->paging($result);

		// echo "<pre>";
		// print_r($result);die;

		$data_val = array();
		foreach ($result as $valu) {
			$statusUser = "";
			switch (strval($valu['USER_STATUS'])) {
				case '0':
					$statusUser = "Disabled";
					break;
				case '1':
					$statusUser = "Approved";
					break;
				case '2':
					$statusUser = "Suspended";
					break;
				case '3':
					$statusUser = "Deleted";
					break;
			}

			$isLogin = "";
			switch (strval($valu['USER_ISLOGIN'])) {
				case '0':
					$isLogin = $this->language->_("No");
					break;
				case '1':
					$isLogin = $this->language->_("Yes");
					break;
			}
			$paramTrx = array(
				"USER_FULLNAME"	=> $valu['USER_FULLNAME'],
				"USER_EMAIL"	=> $valu['USER_EMAIL'],
				// "USER_ROLE_NAME"	=> $valu['USER_ROLE_NAME'],
				"USER_STATUS"	=> $statusUser,
				"USER_LASTLOGIN"	=> $valu['USER_LASTLOGIN'],
				"USER_ISLOGIN"	=> $isLogin,
			);
			array_push($data_val, $paramTrx);
		}

		$this->view->fields = $fields;

		if ($csv) {
			// $this->_helper->download->csv(array($this->language->_('User Name'),$this->language->_('CCY'),$this->language->_('Daily Limit'),$this->language->_('Last Suggested By	'), $this->language->_('Last Approved By')),$data_val,null,'User Daily Limit Scheme');
			// $this->_helper->download->csv(array($this->language->_('User Name'), $this->language->_('User Email'), $this->language->_('Privilege Role Name'), $this->language->_('Status'), $this->language->_('Last Login'), $this->language->_('Login')), $data_val, null, 'User List');
			$this->_helper->download->csv(array($this->language->_('User Name'), $this->language->_('User Email'), $this->language->_('Status'), $this->language->_('Last Login'), $this->language->_('Login')), $data_val, null, 'User List');
		}
	}
}
