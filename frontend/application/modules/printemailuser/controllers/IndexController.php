<?php
require_once 'General/Settings.php';

class printemailuser_IndexController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$model = new printemailuser_Model_Printemailuser();

		$userId 	= $this->language->_('User ID');
		$userName 	= $this->language->_('User Name');
		$userNew 	= $this->language->_('New User');
		$userMethod = $this->language->_('Method');
		$userSent 	= $this->language->_('Sent To');

		$fields = [
			'userId' => [
				'field' 	=> 'USER_ID',
				'label'		=> $userId,
				'sortable'	=> true
			],
			'userName' => [
				'field' 	=> 'USER_FULLNAME',
				'label'		=> $userName,
				'sortable'	=> true
			],
			'userNew' => [
				'field' 	=> 'USER_ISNEW',
				'label'		=> $userNew,
				'sortable'	=> true
			],
			'userMethod' => [
				'field' 	=> 'USER_ISEMAILPWD',
				'label'		=> $userMethod,
				'sortable'	=> true
			],
			'userSent' => [
				'field' 	=> 'USER_EMAIL',
				'label'		=> $userSent,
				'sortable'	=> true
			]
		];

		//get page, sortby, sortdir
		$page 			= $this->_getParam('page');
		$sortBy 		= $this->_getParam('sortby');
		$sortDir 		= $this->_getParam('sortdir');
		$filter_clear 	= $this->_getParam('filter_clear');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0)) ? $page : 1;
		            
		$sortBy = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields)))) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc')))) ? $sortDir : 'desc';

		//get filtering param
		$filters = array(
			'filter'		=>  array('StringTrim', 'StripTags'),
			'fUserId'		=>  array('StringTrim', 'StripTags'),
			'fUserNew'		=>  array('StringTrim', 'StripTags'),
			'fUserMethod'	=>  array('StringTrim', 'StripTags')
		);

		$validators = array(
			'filter'		=>  array('allowEmpty' => true),
			'fUserId'		=>  array('allowEmpty' => true),
			'fUserNew'		=>  array('allowEmpty' => true),
			'fUserMethod'	=>  array('allowEmpty' => true)
		);

		$optionValidators = array('breakChainOnFailure' => false);

		$zf_filter = new Zend_Filter_Input($filters, $validators, $this->getRequest()->getParams(), $optionValidators);

		$filter = ($zf_filter->filter) ? $zf_filter->filter : $this->_request->getParam('filter');

		if($filter == TRUE && $filter_clear == NULL){
			$fUserId 		= $filterParam['fUserId'] = $this->_request->getParam('fUserId');
			$fUserNew 		= $filterParam['fUserNew'] = $this->_request->getParam('fUserNew');
			$fUserMethod 	= $filterParam['fUserMethod'] = $this->_request->getParam('fUserMethod');
		}

		$data = $model->getData($filterParam,$sortBy,$sortDir,$filter,$filter_clear);
		// echo '<pre>';print_r($data);die;
		$this->paging($data);

		$userNewArr = [
			'1'	=> 'Yes',
			'2'	=> 'No'
		];

		$userMethodArr = [
			'1'	=> 'Email',
			'2'	=> 'Posted Mail'
		];

		$this->view->currentPage	= $page;
		$this->view->sortby 		= $sortBy;
		$this->view->sortDir 		= $sortDir;
		$this->view->fields 		= $fields;
		$this->view->filter 		= $filter;
		$this->view->fUserId 		= $fUserId;
		$this->view->fUserNew 		= $fUserNew;
		$this->view->fUserMethod 	= $fUserMethod;
		$this->view->userNewArr 	= $userNewArr;
		$this->view->userMethodArr 	= $userMethodArr;

		Application_Helper_General::writeLog('CSRL','View Resend Password');
	}

	public function sendemailAction()
	{
		$this->_helper->layout()->setLayout('newpopup');

		$model = new printemailuser_Model_Printemailuser();

		$userId = $this->_getParam('userid');

		$user = $model->getUser($userId);

		$email 		= $user['USER_EMAIL'];
		$password 	= substr(base64_decode($user['USER_CLEARTEXT_PWD']),4, -4);

		echo '<pre>';print_r($user);echo '<br>';
		echo '<pre>';print_r($password);echo '<br>';
		die;
	}

}