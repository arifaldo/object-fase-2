<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Account.php';
require_once 'General/CustomerUser.php';
class balanceview_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF'; // masih harus diganti

	// public function __construct()
	// {
	// 	$this->_db = Zend_Db_Table::getDefaultAdapter();
	// }


	public function chartAction()
	{
		// $custId = $this->_custIdLogin;
		$this->_helper->layout()->setLayout('newlayout');

		$sessionNamespace = new Zend_Session_Namespace('pdfdata');

		$this->view->allData = $sessionNamespace->allData;
		$this->view->labelbank = $sessionNamespace->labelbank;
		$this->view->graphbank = $sessionNamespace->graphbank;
		$this->view->userId = $sessionNamespace->userId;
		$this->view->totalasset = $sessionNamespace->totalasset;
	}


	public function updatebalancerealtimeAction(){
 //		ini_set('display_errors', 1);
 //ini_set('display_startup_errors', 1);
 //error_reporting(E_ALL);
	
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $rsdate = $this->_getParam('rsdate');
        // var_dump($rsdate)
        

        $rabbit = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'T_RABBIT_FLAG'))
						 
						 
						 ->where("A.CUST_ID = ? ",$this->_custIdLogin)
						 // ->where("A.USER_ID = ? ",$this->_userIdLogin)
						 ->order('A.ID DESC')
						 ->limit(1)
				);
				
				$rabbitna = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'T_RABBIT_FLAGNA'))
						 
						 
						 ->where("A.CUST_ID = ? ",$this->_custIdLogin)
						 // ->where("A.USER_ID = ? ",$this->_userIdLogin)
						 ->order('A.ID DESC')
						 ->limit(1)
				);
				
				//var_dump($rabbit);
				//var_dump($rabbitna);
				//die;

        if(!empty($rabbit) || !empty($rabbitna)){

        	
		//var_dump($rabbit['0']['DONE']);

        	if($rabbit['0']['DONE'] == '' || $rabbitna['0']['DONE'] == ''){
	//die('here');
        			$balance = $this->_db->fetchAll(
								$this->_db->select()->distinct()
									 ->from(array('A' => 'M_CUSTOMER_ACCT'))
									 ->join(array('C' => 'M_MAKERLIMIT'),'A.ACCT_NO = C.ACCT_NO',array())
									 ->join(array('D' => 'M_PRODUCT_TYPE'),'D.PRODUCT_CODE = A.ACCT_TYPE',array('D.PRODUCT_CODE','D.PRODUCT_NAME'))
									 ->joinLeft(array('B' => 'M_GROUPING'),'A.GROUP_ID = B.GROUP_ID',array('GROUP_NAME'))
									 ->joinLeft(array('E' => 'M_BALANCE'),'E.CUST_ID = A.CUST_ID AND E.ACCT_NO = A.ACCT_NO',array('BALANCE',
										'rs_datetime' => 'UPDATED','E.RABBIT_ID'))
									 ->where("A.ACCT_STATUS = 1")
									 ->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin))
									 ->where("C.USER_LOGIN = ".$this->_db->quote($this->_userIdLogin))
									 ->where("C.MAXLIMIT > 0")
									 ->group('A.ACCT_NO')						 
									 ->order('B.GROUP_NAME DESC')
									 ->order('A.ORDER_NO ASC')
								);

        			if(!empty($balance)){

        				foreach ($balance as $key => $value) {
        					// $rabbit['0']
        						if($rabbit['0']['ID'] == $value['RABBIT_ID']){
        							$balance[$key]['status'] = '1';
        						}else{
        							$balance[$key]['status'] = '2';
        						}

        				}


						$data = json_encode($balance);
						echo $data;
					}else{
						echo '';
					}


        	}else if(!empty($rabbit['0']['DONE']) || !empty($rabbitna['0']['DONE'])) {
				
        			$seconds = 200000;
					$dateupdate = date("Y-m-d H:i:s", (strtotime(date($rabbit['0']['DONE'])) + $seconds));
					
					$dateupdatena = date("Y-m-d H:i:s", (strtotime(date($rabbitna['0']['DONE'])) + $seconds));


        			if($dateupdate >= date("Y-m-d H:i:s") || $dateupdatena >= date("Y-m-d H:i:s") ){


        					$frontendOptions = array ('lifetime' => 2419200,
                                  'automatic_serialization' => true );
							$backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
							$cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
							$cacheID = 'BVCB'.$this->_custIdLogin;
							
					        $data = $cache->load($cacheID);
							//echo '<pre>';
							//var_dump($cacheID);
							//var_dump($data);die('ter');
					       // $cache->remove($cacheID);
						
							$balance = $this->_db->fetchAll(
								$this->_db->select()->distinct()
									 ->from(array('A' => 'M_CUSTOMER_ACCT'))
									 ->join(array('C' => 'M_MAKERLIMIT'),'A.ACCT_NO = C.ACCT_NO',array())
									 ->join(array('D' => 'M_PRODUCT_TYPE'),'D.PRODUCT_CODE = A.ACCT_TYPE',array('D.PRODUCT_CODE','D.PRODUCT_NAME'))
									 ->joinLeft(array('B' => 'M_GROUPING'),'A.GROUP_ID = B.GROUP_ID',array('GROUP_NAME'))
									 ->joinLeft(array('E' => 'M_BALANCE'),'E.CUST_ID = A.CUST_ID AND E.ACCT_NO = A.ACCT_NO',array('BALANCE',
										'rs_datetime' => 'UPDATED','E.RABBIT_ID'))
									 ->where("A.ACCT_STATUS = 1")
									 ->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin))
									 ->where("C.USER_LOGIN = ".$this->_db->quote($this->_userIdLogin))
									 ->where("C.MAXLIMIT > 0")
									 ->group('A.ACCT_NO')						 
									 ->order('B.GROUP_NAME DESC')
									 ->order('A.ORDER_NO ASC')
								);
								
								/*
        					$balance = $this->_db->fetchAll(
								$this->_db->select()
									 ->from(array('A' => 'M_BALANCE'))
									 
									 
									 ->where("A.CUST_ID = ? ",$this->_custIdLogin)
									 ->where("A.ACCT_NO IS NOT NULL ")
									 // ->where("A.ACCT_NO = ? ",$request['account_number'])
									 // ->where("A.BANK_CODE = ? ",$request['BANK_CODE'])
									 
								); */

							

		        			if(!empty($balance)){

		        				foreach ($balance as $key => $value) {
		        					// $rabbit['0']
		        						if($rabbit['0']['ID'] == $value['RABBIT_ID'] || $rabbitna['0']['ID'] == $value['RABBIT_ID']){
		        							$balance[$key]['status'] = '1';
		        						}else{
		        							$balance[$key]['status'] = '2';
		        						}

		        				}

		        				$save['lastupdate']	= date('Y-m-d H:i:s');
								$save['datapers'] = $data['datapers'];
								$save['totalmanual'] = $data['totalmanual']-$balance['0']['PLAFOND'];
								$save['totalonline'] = $data['totalonline'];
								
								//var_dump($data['datapers']); 
								foreach ($data['datapers'] as $key => $value) {
									foreach ($balance as $key => $valb) {
										
										//var_dump($valb['ACCT_NO']);echo ' - -';
										if($value['bank_code'] == $valb['BANK_CODE'] && $value['account_number'] == $valb['ACCT_NO']){
										//unset($save['datapers'][$key]);
												// var_dump($value['account_number']);
											$save['datapers'][$key]['account_balance'] = $valb['BALANCE'];
										}
									}
									
								}
								//echo '<pre>';
								//var_dump($save);
								
								$cache->save($save,$cacheID);


								$data = json_encode($balance);
								echo $data;
							}else{
								echo '';
							}
        			}


        	}else{
        		echo '';
        	}




        }else{
        	echo '';
        }

        	
	}
 

	public function lastupdatedAction(){
// 		ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
	
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        // $rsdate = $this->_getParam('rsdate');

         $frontendOptions = array ('lifetime' => 2419200,
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'BVCB'.$this->_custIdLogin;
        $data = $cache->load($cacheID);

		
		//var_dump($data);die;
		if(!empty($data)){
			$save['lastupdate']	= date('Y-m-d H:i:s');
			$save['datapers'] = $data['datapers'];
			$save['totalmanual'] = $data['totalmanual']-$balance['0']['PLAFOND'];
			$save['totalonline'] = $data['totalonline'];	
			$cache->save($save,$cacheID);
			$dateupdate = date("d-M-Y H:i:s");
			//echo $save['lastupdate'];
			echo $dateupdate.',-';
			
		}else{
		
	
		// foreach ($account as $key => $request) {
			# code...
				
			
				$balance = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'T_RABBIT_FLAG'),array('DONE','USER_ID'))
						 
						 
						 ->where("A.CUST_ID = ? ",$this->_custIdLogin)
						 ->order('A.ID DESC')
						 // ->where("A.ACCT_NO = ? ",$request['account_number'])
						 // ->where("A.BANK_CODE = ? ",$request['BANK_CODE'])
						 
				);

						 // var_dump($balance);
				if(!empty($balance)){
				// print_r($balance);die;
				$date=date_create($balance['0']['DONE']);
				$dateupdate = date_format($date,"d-M-Y H:i:s");
				
					echo $dateupdate.','.$balance['0']['USER_ID'];
				}
			// }
		}
        	
	}

	public function newacctAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        // $tblName = $this->_getParam('id');
        $param = $this->getRequest()->getParams();
      

//      if ($priviBeneLinkage == true)
//      {
           		 $this->_db->beginTransaction();
				
				// $where['ID = ?'] = $tblName;
           		 $inserthistory = array(
		                      'CUST_ID'    => $this->_custIdLogin,
		                      'ACCT_NO'    => $param['ACCTSRC_NUMBER'],
		                      'CCY_ID'    => 'IDR',
		                      'ACCT_NAME'    => $param['ACCTSRC_NAME'],
		                      'PLAFOND'    => $param['OPENING_BALANCE'],
		                      'ACCT_STATUS'    => '5',
		                      'PLAFOND_END'    => date('Y-m-d'),
		                      'BANK_CODE'    => $param['ACCTSRC_BANK'],
		                      'PLAFOND_DATE' => new Zend_Db_Expr("now()")
		                   		 ); 

				// $this->_db->delete('T_HISTORY_STATEMENT',$where);
				$this->_db->insert('T_BALANCE',$inserthistory);
				
				$this->_db->commit();
				

				$bankdata = $this->_db->select()
						 ->from(array('C' => 'M_BANK_TABLE'));
				$bankdata->where("BANK_CODE = ".$this->_db->quote($param['ACCTSRC_BANK']));
				$databank 					= $this->_db->fetchAll($bankdata);
				// Application_Helper_General::writeLog('GRAM','Deleting History Statement'.$tblName);


				$key = 0;
				$parad = "'".$key."','".$param['ACCTSRC_NUMBER']."'";
                  $delete_action = '<a href="javascript:;" title="delete" onClick="delelemodal('.$parad.')" id="delete'.$key.$row ['account_number'].'"><i class="fa fa-trash"></i></a>'; 
				$optHtml = '<tr><td  class="tbl-evencontent">' . $databank['0']['BANK_NAME'] . '</td>';
				$optHtml .= '<td class="tbl-evencontent">' . $param['ACCTSRC_NUMBER']. '</td>';
				$optHtml .= '<td class="tbl-evencontent"> IDR </td>';
				$optHtml .= '<td class="tbl-evencontent" style="text-align:right">'.Application_Helper_General::displayMoney($param['OPENING_BALANCE']). '</td>';
				$optHtml .= '<td class="tbl-evencontent" >'.date('d/m/Y').'</td>';
				$optHtml .= '<td class="tbl-evencontent" align="center"><div><span class="indicator offline"></span> </div></td>';
				$optHtml .= '<td class="tbl-evencontent"><a href="javascript:;" title="edit" id="edit'.$key.$param['ACCTSRC_NUMBER'].'"><i class="fa fa-edit"></i></a>&nbsp;<a href="javascript:;" title="delete" onClick="delelemodal('.$parad.')" id="delete'.$key.$param['ACCTSRC_NUMBER'].'"><i class="fa fa-trash"></i></a></td></tr>';
          echo $optHtml;
    }

    function balanceupdateAction(){
    	$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

       
		

		$id = (string)$this->_getParam('id');
		$bank = (string)$this->_getParam('bank');
		
		$systemBalance = new SystemBalance($this->_custIdLogin,$id,Application_Helper_General::getCurrNum($bank));
		$systemBalance->setFlag(false);
		$systemBalance->checkBalance();
		
		$temmpbalance =  $this->_db->select()
					              ->from(array('A' => 'M_BALANCE'))
					              ->where("A.ACCT_NO = ".$this->_db->quote($id))
					              ->where("A.CCY = ".$this->_db->quote($bank))
								  ->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin));
								 
								//echo $temmpbalance;
		$balance = $this->_db->fetchAll($temmpbalance);
								//  die('here');
								
								 
					
					             
		

		if($systemBalance->getEffectiveBalance() != 'N/A'){
					if(empty($balance)){
					                  $param = array(
					                     'ACCT_NO' => $id,
					                     'BANK_CODE' => null,
										 'ACCT_NAME' => null,
					                     'CUST_ID' => $this->_custIdLogin,
					                     'CCY' => $bank,
					                     'BALANCE' => $systemBalance->getEffectiveBalance(),
					                     'UPDATED' => new Zend_Db_Expr("now()"),
					                     'RABBIT_ID' => null
					                 );
					                 $this->_db->insert('M_BALANCE',$param);
					             }else{
					                $updateArr['BALANCE'] = $systemBalance->getEffectiveBalance();
									$updateArr['ACCT_NAME']	= null;
									$updateArr['RABBIT_ID']	= null;
					                $updateArr['UPDATED'] = new Zend_Db_Expr('now()');
					                $whereArr = array( #'CUST_ID = ?' => (string)$custId,
					                        'ACCT_NO = ?' => (string)$id,
					                       // 'BANK_CODE = ? ' => (string)$request['BANK_CODE'],
					                        'CUST_ID = ? ' => (string)$this->_custIdLogin
					                    );

					                try{
					                	$balanceupdate = $this->_db->update('M_BALANCE',$updateArr,$whereArr);
					                }catch (Exception $e) {
					                    print_r($e);die;
									}
					            }
								
			$data = $this->_db->fetchAll(
			$this->_db->select()->distinct()
				 ->from(array('A' => 'M_CUSTOMER_ACCT'))
				 ->join(array('C' => 'M_MAKERLIMIT'),'A.ACCT_NO = C.ACCT_NO',array())
				 ->join(array('D' => 'M_PRODUCT_TYPE'),'D.PRODUCT_CODE = A.ACCT_TYPE',array('D.PRODUCT_CODE','D.PRODUCT_NAME'))
				 ->joinLeft(array('B' => 'M_GROUPING'),'A.GROUP_ID = B.GROUP_ID',array('GROUP_NAME'))
				 ->joinLeft(array('E' => 'M_BALANCE'),'E.CUST_ID = A.CUST_ID AND E.ACCT_NO = A.ACCT_NO',array('BALANCE',
					'rs_datetime' => 'UPDATED'))
				 ->where("A.ACCT_STATUS = 1")
				 ->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin))
				 ->where("C.USER_LOGIN = ".$this->_db->quote($this->_userIdLogin))
				 ->where("C.MAXLIMIT > 0")
				 ->group('A.ACCT_NO')						 
				 ->order('B.GROUP_NAME DESC')
				 ->order('A.ORDER_NO ASC')
			);
			
					// echo $data;die;
			$isGroup = false;
			if(count($data))
			{
				foreach($data as $row)
				{
					if($row['GROUP_ID'])
					{
						$isGroup = true;
					}
				}
			}

			$resultdataGroup =  array();
			if(!empty($data)){
				foreach ($data as $rowGroup){
					$resultdataGroup[$rowGroup['GROUP_NAME']][] = $rowGroup; 
				}
			}
			//echo '<pre>';
			//var_dump($resultdataGroup);die;
			
			$save['lastupdate']	= date('Y-m-d H:i:s');
			//$save['datapers'] = $datapersonal;
			//$save['totalmanual'] = $totalmanual;
			//$save['totalonline'] = $totalonline;
			$save['resultdataGroup'] = $resultdataGroup;
			// $totalonline = $data['totalonline'];
   //          $totalmanual = $data['totalmanual']; 
			$frontendOptions = array ('lifetime' => 2419200,
									  'automatic_serialization' => true );
			$backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
			$cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
			$cacheID = 'BVCB'.$this->_custIdLogin;
	        $cache->save($save,$cacheID);
								
					echo Application_Helper_General::displayMoney($systemBalance->getEffectiveBalance());
					//break;
		}else{
					echo 'N/A';
					//break;
		}
		
		

       

    		

    }


    function balanceupdatenewAction(){
    	$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $complist = $this->_db->fetchAll(
                    $this->_db->select()
                         ->from(array('A' => 'M_USER'),array('CUST_ID'))
                         ->where("A.USER_ID = ? ", $this->_userIdLogin)
               );	
    	$comp = "'";
    	foreach ($complist as $key => $value) {
    		$comp .= "','".$value['CUST_ID']."','";
    	}
    	$comp .= "'";

        $acctlist = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'M_APIKEY'))
						 ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
						  ->join(array('B' => 'M_BANK_TABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
						 ->where("A.CUST_ID IN (".$comp.")")
						 ->order('A.APIKEY_ID ASC')
				);
						 // echo $acctlist;die;
		
		$account = array();
		foreach ($acctlist as $key => $value) {
			$account[$value['ID']][$value['FIELD']] = $value['VALUE'];
			$account[$value['ID']]['BANK_CODE'] = $value['BANK_CODE'];
			$account[$value['ID']]['SENDER_ID'] = $value['SENDER_ID'];
			$account[$value['ID']]['AUTH_USER'] = $value['AUTH_USER'];
			$account[$value['ID']]['AUTH_PASS'] = $value['AUTH_PASS'];
			$account[$value['ID']]['BANK_NAME'] = $value['BANK_NAME'];
			$account[$value['ID']]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
		}


		$id = (string)$this->_getParam('id');
		$bank = (string)$this->_getParam('bank');
		 $clientUser  =  new SGO_Soap_ClientUser();
    	
		foreach ($account as $key => $request) {

			// var_dump((string)$id);echo ' bank ';
			// var_dump((string)$bank);break;
		if($request['account_number'] == $id && $request['BANK_CODE'] == $bank){

			$success = $clientUser->callapi('balance',$request,'b2b/inquiry/balance');
    		$result  = $clientUser->getResult();

    		$frontendOptions = array ('lifetime' => 2419200,
                                  'automatic_serialization' => true );
	        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
	        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
	        $cacheID = 'BVCB'.$this->_custIdLogin;
	        $data = $cache->load($cacheID);
	        foreach ($data['datapers'] as $k => $val) {
	        	if($val['bank_code'] == $request['BANK_CODE'] && $val['account_number'] == $request['account_number']){
	        		$data['datapers'][$k]['account_balance'] = $result->account_balance;
	        	}
	        }
    		$cache->save($data,$cacheID);

			//insert T_DIGI_LOG
			$paramlog = array(
	            'DIGI_USER' => $this->_userIdLogin,
	            'DIGI_CUST' => $this->_custIdLogin,
	            'DIGI_BANK' => $request['BANK_CODE'],
	            'DIGI_ACCOUNT' => $request['account_number'],
	            'DIGI_ERROR_CODE' => $result->error_code,
	            'DIGI_TIMESTAMP' => new Zend_Db_Expr("now()"),
	            'DIGI_SERVICE'  => 1
	        );

	        $this->_helper->ServiceLog->serviceLog($paramlog);

			if($clientUser->isTimedOut()){
				echo 'N/A';
					break;

			}else{
				$result  = $clientUser->getResult();
				// print_r($result);die;
				if($result->error_code == '0000'){
					echo Application_Helper_General::displayMoney($result->account_balance);
					break;
				}else{
					echo 'N/A';
					break;
				}
			}	

			

		}
			
			
		}

       

    		

    }


    function updatebalanceAction(){
	
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $id = $this->_getParam('id');
        $cur = $this->_getParam('acct');


        // $where['ACCT_NO = ?'] = $acct;

        //-----------------------------------------start balance-------------------------------------

        $where['ACCT_NO = ?'] = $cur;
         // $where['PLAFOND_DATE = ?'] = new Zend_Db_Expr("now()");
         // $where['PLAFOND_END = ?'] = date('Y-m-d');


		$updateArr = array('PLAFOND' => $id,
							'PLAFOND_DATE'=> new Zend_Db_Expr("now()"),
							'PLAFOND_END' => date('Y-m-d'));
		// console.log()
		try {
			$this->_db->update('T_BALANCE',$updateArr,$where);
		} catch (Exception $e) {
			var_dump($e);die;
		}
		//-----------------------------------------end balance-------------------------------------
	}

	function updatealiasnameoffAction(){
	
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $frontendOptions = array ('lifetime' => 2419200,
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'BVCB'.$this->_custIdLogin;
        $data = $cache->load($cacheID);
        $cache->remove($cacheID);

        $cur = $this->_getParam('acct');
        $alias = $this->_getParam('alias');
        $bankcode = $this->_getParam('bankcode');

        //-----------------------------------------start alias-------------------------------------

    	$where['CUST_ID = ?'] = $this->_custIdLogin;
    	$where['ACCT_NO = ?'] = $cur;
    	$where['BANK_CODE = ?'] = $bankcode;

		$updateArr = array('ACCT_ALIAS' => $alias);

		try {
			$this->_db->update('T_BALANCE',$updateArr,$where);
			echo "Sukses Update";
		} catch (Exception $e) {
			echo "Gagal Update";
		}

        //-----------------------------------------end alias-------------------------------------
	}


	function clearcacheAction(){
	
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $frontendOptions = array ('lifetime' => 2419200,
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'BVCB'.$this->_custIdLogin;
        $data = $cache->load($cacheID);
        $cache->remove($cacheID);

        //-----------------------------------------end alias-------------------------------------
	}

	function updatealiasnameonAction(){
	
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $frontendOptions = array ('lifetime' => 2419200,
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'BVCB'.$this->_custIdLogin;
        $data = $cache->load($cacheID);
        $cache->remove($cacheID);

        $alias = $this->_getParam('alias');
        $acct = $this->_getParam('acct');
        $bankcode = $this->_getParam('bankcode');
        $new = $this->_getParam('new');
        $apikey_id = $this->_getParam('apikey');

        $dataApi = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'M_APIKEY'), array('*'))
						 ->where("A.APIKEY_ID = ? ", $apikey_id)
					);

        foreach ($dataApi as $key => $value) {
        	$newDataApi[$value['FIELD']] = $value['VALUE'];
        }

        $account = $newDataApi['account_number'];
        if (isset($newDataApi['account_alias'])) {
        	$checkAlias = false;
        }
        else{
        	$checkAlias = true;
        }

        //if new, brrti belum ada acct alias sebelumnya
        if ($new == 1 && $checkAlias) {

        	$insertAPI= array(
	          'APIKEY_ID'    => $apikey_id,
	          'BANK_CODE'    => $bankcode,
	          'CUST_ID'    	 => $this->_custIdLogin,
	          'FIELD'    	 => 'account_alias',
	          'VALUE'    	 => $alias
       		 ); 

        	try {
				$this->_db->insert('M_APIKEY', $insertAPI);

				echo "Sukses Insert";

			} catch (Exception $e) {

				echo "Gagal Insert";
			}
        }
        else{
        	 $where['APIKEY_ID = ?'] = $apikey_id;
        	 $where['FIELD = ?'] = 'account_alias';

			$updateArr = array('VALUE' => $alias);

			try {
				$this->_db->update('M_APIKEY',$updateArr,$where);
				echo "Sukses Update";
			} catch (Exception $e) {
				echo "Gagal Update";
			}
        }



        $complist = $this->_db->fetchAll(
                    $this->_db->select()
                         ->from(array('A' => 'M_USER'),array('CUST_ID'))
                         ->where("A.USER_ID = ? ", $this->_userIdLogin)
               );	
    	$comp = "'";
    	foreach ($complist as $key => $value) {
    		$comp .= "','".$value['CUST_ID']."','";
    	}
    	$comp .= "'";

        
    	
		

	    foreach ($data['datapers'] as $k => $val) {
	      	// if($val['account_balance'] == 'N/A'){
	        		// foreach ($account as $key => $request) {

	        				if($dataApi['0']['BANK_CODE'] == $val['bank_code'] && $account == $val['account_number']){
	        					
								$data['datapers'][$k]['account_alias'] = $alias;

								
	        				}

					// }
	        // }
	       	
	    }
	    $cache->save($data,$cacheID);



	} 

	function balancenanewAction(){
		
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        $frontendOptions = array ('lifetime' => 2419200,
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'BVCB'.$this->_custIdLogin;
        $data = $cache->load($cacheID);



        $complist = $this->_db->fetchAll(
                    $this->_db->select()
                         ->from(array('A' => 'M_USER'),array('CUST_ID'))
                         ->where("A.USER_ID = ? ", $this->_userIdLogin)
               );	
    	$comp = "'";
    	foreach ($complist as $key => $value) {
    		$comp .= "','".$value['CUST_ID']."','";
    	}
    	$comp .= "'";

        $acctlist = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'M_APIKEY'))
						 ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
						  ->join(array('B' => 'M_BANK_TABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
						 ->where("A.CUST_ID IN (".$comp.")")
						 ->order('A.APIKEY_ID ASC')
				);
						 // echo $acctlist;die;
		
		$account = array();
		foreach ($acctlist as $key => $value) {
			$account[$value['ID']][$value['FIELD']] = $value['VALUE'];
			$account[$value['ID']]['BANK_CODE'] = $value['BANK_CODE'];
			$account[$value['ID']]['SENDER_ID'] = $value['SENDER_ID'];
			$account[$value['ID']]['AUTH_USER'] = $value['AUTH_USER'];
			$account[$value['ID']]['AUTH_PASS'] = $value['AUTH_PASS'];
			$account[$value['ID']]['BANK_NAME'] = $value['BANK_NAME'];
			$account[$value['ID']]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
		}


		 $clientUser  =  new SGO_Soap_ClientUser();
    	
		
		$lastid = $this->_db->fetchAll(
                    $this->_db->select()
                         ->from(array('A' => 'T_RABBIT_FLAGNA'),array('ID'))
                         ->where("A.CUST_ID IN (".$comp.")")
						 ->order('A.ID ASC')
						 ->limit(1)
               );	
		
		$lastid = $lastid['0']['ID']+1;
		 $totaldatana = 0;
		 $accountna = array();
		 //echo '<pre>';
		 //var_dump($data['datapers']);die;
	    foreach ($data['datapers'] as $k => $val) {
	      	if($val['account_balance'] == 'N/A' || $val['account_balance'] == NULL){
	        		foreach ($account as $key => $request) {

	        				if($request['BANK_CODE'] == $val['bank_code'] && $request['account_number'] == $val['account_number']){


	        					$totaldatana++;




	        					

	        					$request['user'] = $this->_userIdLogin;
								$request['cust'] = $this->_custIdLogin;
								$request['type'] = 'na';
								$request['id'] = $lastid;
								$accountna[$totaldatana] = $request;


								


	       //  					$success = $clientUser->callapi('balance',$request,'http://192.168.86.26:20809/b2b/inquiry/balance');
    				// 			$result  = $clientUser->getResult();
								// $data['datapers'][$k]['account_balance'] = $result->account_balance;


									// try {
									// 	$paramlog = array(
							  //                   'DIGI_USER' => $this->_userIdLogin,
							  //                   'DIGI_CUST'	=> $this->_custIdLogin,
							  //                   'DIGI_BANK' => $request['BANK_CODE'],
							  //                   'DIGI_ACCOUNT' => $request['account_number'],
							  //                   'DIGI_ERROR_CODE' => $result->error_code,
							  //                   'DIGI_TIMESTAMP' => new Zend_Db_Expr("now()"),
							  //       );

							  //       $this->_db->insert('T_DIGI_LOG',$paramlog);


									// } catch (Exception $e) {
									// 	var_dump($e);die;				
									// }


	        				}

					}
	        }
	       	
	    }



							   $totalaccount = count($accountna);
	    		// var_dump($totalaccount);die;


									$paramrabbit = array(
							                    // 'ACCT_NO' => $request['account_number'],
							                    // 'BANK_CODE'	=> $request['BANK_CODE'],
							                    'DATA'	=> json_encode($accountna),
							                    'RABBIT_FLAG' => 0,
							                    'COUNT' => $totalaccount,
							                    'CUST_ID' => $this->_custIdLogin,
										        'USER_ID' => $this->_userIdLogin,
										        'CREATED' => new Zend_Db_Expr("now()")
							        );

						        $this->_db->insert('T_RABBIT_FLAGNA',$paramrabbit);
	    // $cache->save($data,$cacheID);


	}


	function balancenaAction(){
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        $frontendOptions = array ('lifetime' => 2419200,
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'BVCB'.$this->_custIdLogin;
        $datacache = $cache->load($cacheID);



        $data = $this->_db->fetchAll(
			$this->_db->select()->distinct()
				 ->from(array('A' => 'M_CUSTOMER_ACCT'))
				 ->join(array('C' => 'M_MAKERLIMIT'),'A.ACCT_NO = C.ACCT_NO',array())
				 ->join(array('D' => 'M_PRODUCT_TYPE'),'D.PRODUCT_CODE = A.ACCT_TYPE',array('D.PRODUCT_CODE','D.PRODUCT_NAME'))
				 ->joinLeft(array('B' => 'M_GROUPING'),'A.GROUP_ID = B.GROUP_ID',array('GROUP_NAME'))
				 ->joinLeft(array('E' => 'M_BALANCE'),'E.CUST_ID = A.CUST_ID AND E.ACCT_NO = A.ACCT_NO',array('BALANCE',
					'rs_datetime' => 'UPDATED'))
				 ->where("A.ACCT_STATUS = 1")
				 ->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin))
				 ->where("C.USER_LOGIN = ".$this->_db->quote($this->_userIdLogin))
				 ->where("C.MAXLIMIT > 0")
				 ->group('A.ACCT_NO')						 
				 ->order('B.GROUP_NAME DESC')
				 ->order('A.ORDER_NO ASC')
			);
    	
	//	echo '<pre>';
		//var_dump($data['datapers']);die;
	    foreach ($data as $k => $val) {
			//var_dump($val);
			//if($val['account_number'] == '46613930121')
				//$val['account_balance'] ='N/A';
			//}	
			foreach($val as $ky => $vl){
	      	if($vl['BALANCE'] == 'N/A' || $vl['BALANCE'] == NULL){
								
								$systemBalance = new SystemBalance($this->_custIdLogin,$vl['ACCT_NO'],Application_Helper_General::getCurrNum($vl['CCY_ID']));
								$systemBalance->setFlag(false);
								$systemBalance->checkBalance();
	        				
	        					$resultdataGroup[$k][$ky] = $vl;
								//var_dump($result);
								$datacache['resultdataGroup'][$k][$ky]['BALANCE'] = $systemBalance->getEffectiveBalance();
								//var_dump($request);die;
								$temmpbalance =  $this->_db->select()
					              ->from(array('A' => 'M_BALANCE'))
					              ->where("A.ACCT_NO = ".$this->_db->quote($vl['ACCT_NO']))
					              ->where("A.BANK_CODE IS NULL")
								  ->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin));
								 
								//echo $temmpbalance;
								  $balance = $this->_db->fetchAll($temmpbalance);
								//  die('here');
								
								 
					
					             if(empty($balance)){
					                  $param = array(
					                     'ACCT_NO' => $vl['ACCT_NO'],
					                     'BANK_CODE' => NULL,
										 'ACCT_NAME' => NULL,
					                     'CUST_ID' => $this->_custIdLogin,
					                     'CCY' => $vl['CCY_ID'],
					                     'BALANCE' => $systemBalance->getEffectiveBalance(),
					                     'UPDATED' => new Zend_Db_Expr("now()"),
					                     'RABBIT_ID' => NULL
					                 );
					                 $this->_db->insert('M_BALANCE',$param);
					             }else{
					                $updateArr['BALANCE'] = $systemBalance->getEffectiveBalance();
									//$updateArr['ACCT_NAME']	= $result->account_name;
									//$updateArr['RABBIT_ID']	= (string)$request['id'];
					                $updateArr['UPDATED'] = new Zend_Db_Expr('now()');
					                $whereArr = array( #'CUST_ID = ?' => (string)$custId,
					                        'ACCT_NO = ?' => (string)$vl['ACCT_NO'],
					                        'BANK_CODE = ? ' => NULL,
					                        'CUST_ID = ? ' => (string)$this->_custIdLogin
					                    );

					                try{
					                	$balanceupdate = $this->_db->update('M_BALANCE',$updateArr,$whereArr);
					                }catch (Exception $e) {
					                    print_r($e);die;
									}
					            } 
								
								
	        				

					
	        }
		}
	       	
	    }
	    $cache->save($datacache,$cacheID);
					$totalaccount = count($account);
	    		
					$paramrabbit = array(
			                    // 'ACCT_NO' => $request['account_number'],
			                    // 'BANK_CODE'	=> $request['BANK_CODE'],
			                    'DATA'	=> json_encode($resultdataGroup),
			                    'RABBIT_FLAG' => 2,
			                    'COUNT' => $totalaccount,
			                    'CUST_ID' => $this->_custIdLogin,
						        'USER_ID' => $this->_userIdLogin,
						        'CREATED' => new Zend_Db_Expr("now()")
			        );

			        $this->_db->insert('T_RABBIT_FLAG',$paramrabbit);


	}


 
	function refreshchartAction(){
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        $frontendOptions = array ('lifetime' => 2419200,
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'BVCB'.$this->_custIdLogin;
        $data = $cache->load($cacheID);  

        $datapersonal = $data['resultdataGroup'];
		//echo '<pre>';
		//var_dump($datapersonal);
		$totalmanual = 0;
		$totalonline = 0;
		$totalna = 0;
		
		if(!empty($datapersonal)){
			//echo '<pre>';
			
		//	var_dump($data['datapers']);die;
			foreach ($datapersonal as $key => $value) {
				
				foreach($value as $vl){
					
					if($vl['BALANCE'] == 'N/A' || $vl['BALANCE'] == NULL){
						 //echo "here";die;
						$totalna++;
					}else{
						$totalonline +=  $vl['BALANCE'];
					}
				}
			}
		}
		$totalasset = $totalonline+$totalmanual;
        $graphbank = array();
		$bankTotals = array();
		$labelbank = array();
	//	foreach($datapersonal as $amount)
	//	{
	//	  $graphbank[$amount['bank_name']] += $amount['account_balance'];
	//	  $labelbank[]						= $amount['bank_name'];
	//	}
		foreach($datapersonal as $kam => $amount)
		{
		  foreach($amount as $val){
			$graphbank[$kam] 				+= $val['BALANCE'];
		  }
		  $labelbank[]						= $kam;
		}
		// echo "<pre>";
		// var_dump($labelbank);die;

		$labelbankArr = array_unique($labelbank);
		$graphbankArr = $graphbank;
		$arraychart = array();
		foreach ($labelbankArr as $key => $value) {
            // if($this->totalasset == 0){
            $percentbank = ($graphbankArr[$value] / $totalasset) * 100;
            // }else{
               //$percentbank = 20;
			   //$graphbankArr[$value] = 2000;    
            // }
			$arraychart[$key]['y'] = $percentbank;
			$arraychart[$key]['label'] = $value;
			$arraychart[$key]['nominal'] = Application_Helper_General::displayMoney($graphbankArr[$value]);
			
            ///echo  "{ y : " . $percentbank . " , label : '" . $value . "' , nominal : '" . Application_Helper_General::displayMoney($graphbankArr[$value]) . "'},";
         }
        echo json_encode(array_values($arraychart));


	}


	function balanceupdatecacheAction(){
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $frontendOptions = array ('lifetime' => 2419200,
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'BVCB'.$this->_custIdLogin;

                       
			                $updateArr['CUST_BALANCE_CORE'] = '0';
			                $whereArr = array( 'CUST_ID = ?' => $this->_custIdLogin);
			                $balanceupdate = $this->_db->update('M_CUSTOMER',$updateArr,$whereArr);
        

        $cache->remove($cacheID);
        return true;
	}

	function deletemanualbalanceAction(){
	
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $frontendOptions = array ('lifetime' => 2419200,
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'BVCB'.$this->_custIdLogin;
        $data = $cache->load($cacheID);
        $cache->remove($cacheID);
        $bankcode = $this->_getParam('bankcode');
        $acct = $this->_getParam('acct');
		//return $acct;die;
        $balance = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'T_BALANCE'))
						 
						 
						 ->where("A.CUST_ID = ? ",$this->_custIdLogin)
						 ->where("A.ACCT_NO = ? ",$acct)
						 ->where("A.BANK_CODE = ? ",$bankcode)
						 
					);

        $save['lastupdate']	= date('Y-m-d H:i:s');
		$save['datapers'] = $data['datapers'];
		$save['totalmanual'] = $data['totalmanual']-$balance['0']['PLAFOND'];
		$save['totalonline'] = $data['totalonline'];
		
		
		foreach ($data['datapers'] as $key => $value) {
			if($value['bank_code'] == $bankcode && $value['account_number'] == $acct){
				unset($save['datapers'][$key]);
			}
		}
		//return $save['datapers'];die;
		$cache->save($save,$cacheID);
        // echo $acct;
       	$this->_db->beginTransaction();
				
		$where['ACCT_NO = ?'] = $acct;
		$this->_db->delete('T_BALANCE',$where);
		
		$this->_db->commit();
		return json_encode($save);
	}


	function deleteopenAction(){
	
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

   		// $frontendOptions = array ('lifetime' => 86400,
     //                              'automatic_serialization' => true );
     //    $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
     //    $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
     //    $cacheID = 'BVCB'.$this->_custIdLogin;

        // $data = $cache->load($cacheID);
		// $lastupdate = $data['lastupdate'];
        // $datapersonal = $data['datapers'];
        // echo "<pre>";

        // var_dump($datapersonal);die;

         $frontendOptions = array ('lifetime' => 2419200,
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'BVCB'.$this->_custIdLogin;
        $data = $cache->load($cacheID);

        $save['lastupdate']	= date('Y-m-d H:i:s');
		$save['datapers'] = $data['datapers'];
		$save['totalmanual'] = $data['totalmanual'];
		



        $acct = $this->_getParam('acct');
        $bank_code = $this->_getParam('bank_code');

        if(!empty($acct) && !empty($bank_code)){

        $acctlist = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'M_APIKEY'))
						 ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
						  // ->join(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
						 // ->where('A.ACCT_STATUS = ?','5')
						 ->where("A.CUST_ID = ? ",$this->_custIdLogin)
						 ->where("A.VALUE = ? ",$acct)
						 ->where("A.BANK_CODE = ? ",$bank_code)
						 // ->order('A.APIKEY_ID ASC')
						 // echo $acctlist;
				);

    	}else{
    		$acctlist = array();
    	}
    	// $acctlist['query'] = $this->_db->select()
					// 	 ->from(array('A' => 'M_APIKEY'))
					// 	 ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
					// 	  // ->join(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
					// 	 // ->where('A.ACCT_STATUS = ?','5')
					// 	 ->where("A.CUST_ID = ? ",$this->_custIdLogin)
					// 	 ->where("A.VALUE = ? ",$acct)
					// 	 ->where("A.BANK_CODE = ? ",$bank_code);
    	// return json_encode($acctlist);
    	if(!empty($acctlist)){
    		$request = array();
    		$request['BANK_CODE'] = $acctlist['0']['BANK_CODE'];
    		$request['EOA_SENDER'] = $acctlist['0']['SENDER_ID'];
    		$request['AUTH_USER'] = $acctlist['0']['AUTH_USER'];
    		$request['AUTH_PASS'] = $acctlist['0']['AUTH_PASS'];
    		$request['SIGNATURE_KEY'] = $acctlist['0']['SIGNATURE_KEY'];
    		$request['account_number'] = $acct;
    		$clientUser  =  new SGO_Soap_ClientUser();
    		$success 		= $clientUser->callapi('accountdelete',$request,'b2b/account/delete');
    		$result  = $clientUser->getResult();

    		if($result->error_code == '0000'){
    			$this->_db->beginTransaction();
				
				$where['APIKEY_ID = ?'] = $acctlist['0']['APIKEY_ID'];
				$this->_db->delete('M_APIKEY',$where);

				$wherecredential['ID = ?'] = $acctlist['0']['APIKEY_ID'];
				$this->_db->delete('M_APICREDENTIAL',$wherecredential);
				
				$balance = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'M_BALANCE'))
						 
						 
						 ->where("A.CUST_ID = ? ",$this->_custIdLogin)
						 ->where("A.ACCT_NO = ? ",$acct)
						 ->where("A.BANK_CODE = ? ",$bank_code)
						 
					);

				$save['totalonline'] = $data['totalonline']-$balance['0']['BALANCE'];
				foreach ($data['datapers'] as $key => $value) {
					if($value['bank_code'] == $bank_code && $value['account_number'] == $acct){
						unset($save['datapers'][$key]);
					}
				}
				$cache->remove($cacheID);
				$cache->save($save,$cacheID);

				
				$this->_db->commit();
    			return json_encode($save);	
    		}else{
    			return false;
    		}
    		
    	}else{
    		return false;
    	}

    //     foreach ($datapersonal as $key => $value) {
    //     	# code...
    //     	if($value['account_number'] == $acct && $value['bank_code'] == $bank_code){

				// $request['BANK_CODE'] = $bank_code;
				// $request['SENDER_ID'] = $value[''];
				// $request['AUTH_USER'] = $value[''];
				// $request['AUTH_PASS'] = $value[''];
				// $request['EOA_SENDER'] = $resultregist->eoa_credentials->sender_id;

				// $request['SIGNATURE_KEY'] = $resultregist->eoa_credentials->signature_key;
		
    //     	}
    //     }

        
        // echo $acct;
       	// $this->_db->beginTransaction();
				
		// $where['ACCT_NO = ?'] = $acct;
		// $this->_db->delete('T_BALANCE',$where);
		
		// $this->_db->commit();
	}




















	
	public function indexAction()
	{
	
		$this->_helper->layout()->setLayout('newlayout');
	
		$model = new multiaggregation_Model_Accountaggregation();
// die('here');

		$fields1 = array(
					'acct'  => array('field' => 'ACCT_NO',
										   'label' => $this->language->_('Account No'),
										   'sortable' => true),
					'protype'   => array('field'    => '',
											'label'    => $this->language->_('Product Type'),
											'sortable' => true),
					'ccy'   => array('field'    => 'CCY_ID',
										  'label'    => $this->language->_('CCY'),
										  'sortable' => true),
					'available'   => array('field'    => 'AVAILABLE_BALANCE',
										  'label'    => $this->language->_('Available Balance'),
										  'sortable' => true),
		            'equivalen'   => array('field'    => 'OUTSTANDING',
                        		        'label'    => $this->language->_('Ekuivalent (IDR)'),
                        		        'sortable' => true),
		);

		$fields2 = array(
					'depo_acct'  => array('field' => 'DEPOSIT_NO',
										   'label' => $this->language->_('Deposit No'),
										   'sortable' => true),
					'protype'   => array('field'    => 'PROD_TYPE',
											'label'    => $this->language->_('Product Type'),
											'sortable' => true),
					'depo_ccy'   => array('field'    => 'CCY_ID',
										  'label'    => $this->language->_('CCY'),
										  'sortable' => true),
					'amount'   => array('field'    => 'AVAIL_BAL',
										  'label'    => $this->language->_('Available Balance'),
										  'sortable' => true),
		            'equivalen'   => array('field'    => 'EQUIVALEN',
		                                  'label'    => $this->language->_('Ekuivalent (IDR)'),
		                                  'sortable' => true),
		);

		$fields3 = array(
					'cardno'  => array('field' => '',
										   'label' => $this->language->_('Card No'),
										   'sortable' => true),
					'protype'   => array('field'    => '',
											'label'    => $this->language->_('Product Type'),
											'sortable' => true),
					'creditlimit'   => array('field'    => '',
										  'label'    => $this->language->_('Credit Limit'),
										  'sortable' => true),
					'closingbal'   => array('field'    => '',
										  'label'    => $this->language->_('Closing Balance'),
										  'sortable' => true),
		);



		$fields4 = array(
					'loan_acct'  => array('field' => 'ACCT_NO',
										   'label' => $this->language->_('Account No'),
										   'sortable' => true),
					'protype'   => array('field'    => 'PROD_TYPE',
											'label'    => $this->language->_('Product Type'),
											'sortable' => true),
		            'loan_ccy'   => array('field'    => 'CCY_ID',
                            		        'label'    => $this->language->_('CCY'),
                            		        'sortable' => true),
					'plafond'   => array('field'    => 'PLAFOND',
										  'label'    => $this->language->_('Plafond'),
										  'sortable' => true),
					'outstanding'   => array('field'    => 'OUTSTANDING',
										  'label'    => $this->language->_('Outstanding'),
										  'sortable' => true),
		             'equivalen'   => array('field'    => 'OUTSTANDING',
                            		        'label'    => $this->language->_('Ekuivalent (IDR)'),
                            		        'sortable' => true),
		);

		$fields5 = array(
        		    'ccy'  => array('field' => 'ACCT_NO',
                            		        'label' => $this->language->_('CCY'),
                            		        'sortable' => true),
        		    'personalaccount'   => array('field'    => 'PROD_TYPE',
                            		        'label'    => $this->language->_('Personal Account'),
                            		        'sortable' => true),
        		    'depositaccount'   => array('field'    => 'PLAFOND',
                            		        'label'    => $this->language->_('Deposit Account'),
                            		        'sortable' => true),
        		    'loanaccount'   => array('field'    => 'OUTSTANDING',
                            		        'label'    => $this->language->_('Loan Account'),
                            		        'sortable' => true),
		            'networth'   => array('field'    => 'OUTSTANDING',
                            		        'label'    => $this->language->_('Net Worth'),
                            		        'sortable' => true),
		            'equivalen'   => array('field'    => 'OUTSTANDING',
                            		        'label'    => $this->language->_('Ekuivalent (IDR)'),
                            		        'sortable' => true),
		);

		$sortByParam  = $this->_getParam('sortby');
		$sortDirParam = $this->_getParam('sortdir');


		//check param for personal table
		if(Zend_Validate::is($sortByParam,'InArray',array(array_keys($fields1)))){
			$sortBy1  =  $fields1[$sortByParam]['field'];
			$sortDir1 = (Zend_Validate::is($sortDirParam,'InArray',array('haystack'=>array('asc','desc'))))? $sortDirParam : 'asc';
		}
		else{
			$sortBy1  = $fields1[key($fields1)]['field'];
			$sortDir1 = 'asc';
		}


		

		$frontendOptions = array ('lifetime' => 2419200,
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'BVCB'.$this->_custIdLogin;


		
		
		$this->view->isGroup = $isGroup;
		$this->view->custId = $this->_custIdLogin;
		$this->view->resultdata = $data;
		$this->view->fields = $fields;


		
		
		$data = $cache->load($cacheID);
		 //echo "<pre>";
		 //var_dump($data);die;

		
		// $true = true; //hanya utk lolosin, yang dipake sbnrnya yg if !empty($data), utk keperluan demo
		// echo "<pre>";

		$resultcust = $this->_db->fetchRow(
									$this->_db->select()
										->from(array('C' => 'M_CUSTOMER'))
										->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin))
								);

		if(empty($data) && $resultcust['CUST_BALANCE_CORE'] == '1'){
			$this->view->notifcache = true;
		}
		// var_dump($data);
		if($resultcust['CUST_BALANCE_CORE'] == '0')
		// if (true) 
        {
	 //var_dump($data);
		
//die('he');
			$data = $this->_db->fetchAll(
			$this->_db->select()->distinct()
				 ->from(array('A' => 'M_CUSTOMER_ACCT'))
				 ->join(array('C' => 'M_MAKERLIMIT'),'A.ACCT_NO = C.ACCT_NO',array())
				 ->join(array('D' => 'M_PRODUCT_TYPE'),'D.PRODUCT_CODE = A.ACCT_TYPE',array('D.PRODUCT_CODE','D.PRODUCT_NAME'))
				 ->joinLeft(array('B' => 'M_GROUPING'),'A.GROUP_ID = B.GROUP_ID',array('GROUP_NAME'))
				 ->joinLeft(array('E' => 'M_BALANCE'),'E.CUST_ID = A.CUST_ID AND E.ACCT_NO = A.ACCT_NO',array('BALANCE',
					'rs_datetime' => 'UPDATED'))
				 ->where("A.ACCT_STATUS = 1")
				 ->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin))
				 ->where("C.USER_LOGIN = ".$this->_db->quote($this->_userIdLogin))
				 ->where("C.MAXLIMIT > 0")
				 ->group('A.ACCT_NO')						 
				 ->order('B.GROUP_NAME DESC')
				 ->order('A.ORDER_NO ASC')
			);
			
					// echo $data;die;
			$isGroup = false;
			if(count($data))
			{
				foreach($data as $row)
				{
					if($row['GROUP_ID'])
					{
						$isGroup = true;
					}
				}
			}

			$resultdataGroup =  array();
			if(!empty($data)){
				foreach ($data as $rowGroup){
					$resultdataGroup[$rowGroup['GROUP_NAME']][] = $rowGroup; 
				}
			}
			//echo '<pre>';
			//var_dump($resultdataGroup);die;
			
			$save['lastupdate']	= date('Y-m-d H:i:s');
			//$save['datapers'] = $datapersonal;
			//$save['totalmanual'] = $totalmanual;
			//$save['totalonline'] = $totalonline;
			$save['resultdataGroup'] = $resultdataGroup;
			// $totalonline = $data['totalonline'];
   //          $totalmanual = $data['totalmanual'];
	        $cache->save($save,$cacheID);
			
					$totalaccount = count($account);
	    		
					$paramrabbit = array(
			                    // 'ACCT_NO' => $request['account_number'],
			                    // 'BANK_CODE'	=> $request['BANK_CODE'],
			                    'DATA'	=> json_encode($resultdataGroup),
			                    'RABBIT_FLAG' => 2,
			                    'COUNT' => $totalaccount,
			                    'CUST_ID' => $this->_custIdLogin,
						        'USER_ID' => $this->_userIdLogin,
						        'CREATED' => new Zend_Db_Expr("now()")
			        );

			        $this->_db->insert('T_RABBIT_FLAG',$paramrabbit);
					
					$updateArr['CUST_BALANCE_CORE'] = '1';
			                $whereArr = array( 'CUST_ID = ?' => $this->_custIdLogin);
			                $balanceupdate = $this->_db->update('M_CUSTOMER',$updateArr,$whereArr);
		
		}
		else{

			$data = $cache->load($cacheID);
			
			$lastupdate = $data['lastupdate'];
            //$datapersonal = $data['datapers'];
            //$totalonline = $data['totalonline'];
            //$totalmanual = $data['totalmanual'];
			$resultdataGroup = $data['resultdataGroup'];
			
		}
		
		$this->view->acct = $resultdataGroup;
		//var_dump($resultdataGroup);die;
		$this->view->resultdataGroup = $resultdataGroup;


		// $data = $cache->load($cacheID);
		$this->view->lastupdate = $lastupdate;
//

		$totalmanual = 0;
		$totalonline = 0;
		$totalna = 0;
		
		if(!empty($resultdataGroup)){
			//echo '<pre>';
			
		//	var_dump($data['datapers']);die;
			foreach ($resultdataGroup as $key => $value) {
				
				foreach($value as $vl){
					
					if($vl['BALANCE'] == 'N/A' || $vl['BALANCE'] == NULL){
						 //echo "here";die;
						$totalna++;
					}else{
						$totalonline +=  $vl['BALANCE'];
					}
				}
			}
		}
		 //var_dump($totalna);
		if($totalna>=1){
			$this->view->modalna = true;
			$this->view->totalna = $totalna;
		}
		// echo "<pre>";
		// var_dump($datapersonal);
		// die;
		 //var_dump($totalonline);
		 //var_dump($totalmanual);die;
		$totalasset = $totalonline+$totalmanual;

		$this->view->totalonline = ($totalonline/$totalasset)*100;
		$this->view->totalmanual = ($totalmanual/$totalasset)*100;
		$this->view->totalasset	 = $totalasset;
		// var_dump($totalonline);
		// var_dump($totalmanual);
		// var_dump($totalasset);
		// var_dump($this->view->totalonline);
		// var_dump($this->view->totalmanual);die;
// 		$tempLoan[0]['ACCT_NO'] = "100345721001";
// 		$tempLoan[0]['PROD_TYPE'] = "Pinjaman BJB";
// 		$tempLoan[0]['PLAFOND'] = "100,000,000.00";
// 		$tempLoan[0]['OUTSTANDING'] = "60,000,000.00";

// 		$tempLoan[1]['ACCT_NO'] = "1003457823004";
// 		$tempLoan[1]['PROD_TYPE'] = "Pinjaman BJB";
// 		$tempLoan[1]['PLAFOND'] = "150,000,000.00";
// 		$tempLoan[1]['OUTSTANDING'] = "45,000,000.00";


		//check param for deposit table
		if(Zend_Validate::is($sortByParam,'InArray',array(array_keys($fields2)))){
			$sortBy2  =  $fields2[$sortByParam]['field'];
			$sortDir2 = (Zend_Validate::is($sortDirParam,'InArray',array('haystack'=>array('asc','desc'))))? $sortDirParam : 'asc';
		}
		else{
			$sortBy2  = $fields2[key($fields2)]['field'];
			$sortDir2 = 'asc';
		}

		//any query + logic for deposit

		//check param for cc table
		if(Zend_Validate::is($sortByParam,'InArray',array(array_keys($fields3)))){
			$sortBy3  =  $fields3[$sortByParam]['field'];
			$sortDir3 = (Zend_Validate::is($sortDirParam,'InArray',array('haystack'=>array('asc','desc'))))? $sortDirParam : 'asc';
		}
		else{
			$sortBy3  = $fields3[key($fields3)]['field'];
			$sortDir3 = 'asc';
		}

		//any query + logic for cc

		//check param for loan table
		if(Zend_Validate::is($sortByParam,'InArray',array(array_keys($fields4)))){
			$sortBy4  =  $fields4[$sortByParam]['field'];
			$sortDir4 = (Zend_Validate::is($sortDirParam,'InArray',array('haystack'=>array('asc','desc'))))? $sortDirParam : 'asc';
		}
		else{
			$sortBy4  = $fields4[key($fields4)]['field'];
			$sortDir4 = 'asc';
		}

		//check param for loan table
		if(Zend_Validate::is($sortByParam,'InArray',array(array_keys($fields5)))){
		    $sortBy5  =  $fields5[$sortByParam]['field'];
		    $sortDir5 = (Zend_Validate::is($sortDirParam,'InArray',array('haystack'=>array('asc','desc'))))? $sortDirParam : 'asc';
		}
		else{
		    $sortBy5  = $fields5[key($fields5)]['field'];
		    $sortDir5 = 'asc';
		}

		//any query + logic for deposit


//die;

		$logDesc = 'Viewing';


		Application_Helper_General::writeLog('BAIQ',$logDesc);
// 		print_r($temp);die;
		$this->view->custId = $this->_custIdLogin;
		$this->view->isGroupPersonal = $isGroupPersonal;
		// $this->view->isGroupDeposit = $isGroupDeposit;
		// $this->view->isGroupLoan = $isGroupLoan;

		$graphbank = array();
		$bankTotals = array();
		$labelbank = array();
		//echo '<pre>';
		//var_dump($resultdataGroup);
		foreach($resultdataGroup as $kam => $amount)
		{
		  foreach($amount as $val){
			$graphbank[$kam] 				+= $val['BALANCE'];
		  }
		  $labelbank[]						= $kam;
		}
		 //echo "<pre>";
		 //var_dump($labelbank);
		 //var_dump($graphbank);
		 //die;
 
		$this->view->labelbank = array_unique($labelbank);
		$this->view->graphbank = $graphbank;
		$this->view->userId = $this->_userIdLogin;

		//sort data
		foreach ($datapersonal as $key => $row) {
		    $bank_name[$key]  = $row['bank_name'];
		    $account_number[$key] = $row['account_number'];
		}

    	foreach ($datapersonal as $key => $value) {
    		if ($value['bank_code'] == '014') {
    			$dataBca[] = $value;
    		}
    		else if($value['bank_code'] == '013'){
    			$dataPermata[] = $value;
    		}
    		else if($value['bank_code'] == '009'){
    			$dataBni[] = $value;
    		}
    		else if($value['bank_code'] == '008'){
    			$dataMandiri[] = $value;
    		}
    		else if($value['bank_code'] == '050'){
    			$dataStanchart[] = $value;
    		}
    		else if($value['bank_code'] == '031'){
    			$dataCitibank[] = $value;
    		}
    		else if($value['bank_code'] == '032'){
    			$dataChase[] = $value;
    		}
			else if($value['bank_code'] == '153'){
    			$dataSinar[] = $value;
    		}
    		else{
    			$dataOthers[] = $value;
    		}
    	}

    	// echo "<pre>";
    	// print_r($dataBca);die();

    	$allData = array_merge((array)$dataBca, (array)$dataPermata, (array)$dataBni, (array)$dataMandiri, (array)$dataStanchart, (array)$dataCitibank, (array)$dataChase, (array)$dataOthers); 

    	$this->view->allData = $allData;
    	// echo "<pre>";
			// var_dump($dataPermata);die;
		$this->view->dataBca = $dataBca;
		$this->view->dataPermata = $dataPermata;
		$this->view->dataBni = $dataBni;
		$this->view->dataMandiri = $dataMandiri;
		$this->view->dataStanchart = $dataStanchart;
		$this->view->dataCitibank = $dataCitibank;
		$this->view->dataChase = $dataChase;
		$this->view->dataSinar = $dataSinar;
		
		$this->view->dataOthers = $dataOthers;

		$this->view->personal = $datapersonal;
		//$this->view->creditcard = $datacc;
		$dataloan = array();
		/*$dataloan[] = array(
						'ACCT_NO' => '30210009282',
						'LOANTYPE' => 'Loan KK-KPR',
						'CCY_ID' => 'IDR',
						'OUTSTANDING' => number_format('120562540',0,'.','.'),
						'EQUIVALEN' => 2500000000,
					);

		$dataloan[] = array(
						'ACCT_NO' => '30210920001',
						'LOANTYPE' => 'Loan KK-TASPEN',
						'CCY_ID' => 'IDR',
						'OUTSTANDING' => number_format('96232540',0,'.','.'),
						'EQUIVALEN' => 160000000,
					);*/
		$totalloan  = 0;
		if(!empty($dataloan)){
		foreach ($dataloan as $key => $value) {
			$totalloan = $totalloan + $value['EQUIVALEN'];
		}
		} 


		$this->view->totalloan = $totalloan;
		$this->view->deposit = $datadeposit;
		$this->view->loan = $dataloan;

		$this->view->fields1 = $fields1; //personal
		$this->view->fields2 = $fields2; //deposit
		$this->view->fields3 = $fields3; //credit card
		$this->view->fields4 = $fields4; //loan
		$this->view->fields5 = $fields5; //loan

		$this->view->sortBy1 = $sortBy1;
		$this->view->sortBy2 = $sortBy2;
		$this->view->sortBy3 = $sortBy3;
		$this->view->sortBy4 = $sortBy4;
		$this->view->sortBy5 = $sortBy5;
//die;
		$this->view->sortDir1 = $sortDir1;
		$this->view->sortDir2 = $sortDir2;
		$this->view->sortDir3 = $sortDir3;
		$this->view->sortDir4 = $sortDir4;
		$this->view->sortDir5 = $sortDir5;



		$pdf = $this->_getParam('pdf');
		$aggregationType = $this->_getParam('aggregationType');

		if($pdf)
		{

			$base64 = $this->_getParam('assetsChartBase64');

			Application_Helper_General::writeLog('BAIQ','Print PDF');
			// $HTMLchart = $this->view->render($this->view->controllername.'/chart.phtml');
			$HTMLtable = $this->view->render($this->view->controllername.'/pdf.phtml');

//TO DO aggregation type utk nentukan print tab yang mana

			$chartWidth = '400px';
			if ($aggregationType == '2') {
				$chartWidth = '750px';
			} 
			//var_dump($lastupdate);die;
			
			
				
			$this->_helper->download->pdfWithChart($dateupdate,null,null,'360° Balance Overview',$this->_custNameLogin,'CONSOLIDATED ACCOUNT BALANCE REPORT',$base64,$chartWidth,$HTMLtable);

			// $sessionNamespace = new Zend_Session_Namespace('pdfdata');

			// $sessionNamespace->labelbank = array_unique($labelbank);
			// $sessionNamespace->graphbank = $graphbank;
			// $sessionNamespace->userId = $this->_userIdLogin;
			// $sessionNamespace->totalasset	 = $totalasset;

   //          $sessionNamespace->allData = $allData;
   //          $this->_redirect('/multiaggregation/index/chart'); 
		}  

		
	}



}
