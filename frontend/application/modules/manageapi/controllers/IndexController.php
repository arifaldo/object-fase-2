<?php
require_once 'Zend/Controller/Action.php';

class manageapi_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF';

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$custId = $this->_custIdLogin;
		$db = Zend_Db_Table::getDefaultAdapter();
		$select = $db->select()
								 ->from('M_CUSTOMER')
								 ->where('CUST_ID = ?', $custId);
		$results = $select->query()->fetchAll();
		// var_dump($results); die;
		$this->view->acctlist = $results;
	}

}
