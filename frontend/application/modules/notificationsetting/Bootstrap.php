<?php
class notificationsetting_Bootstrap extends Zend_Application_Module_Bootstrap {
	
	protected function _initModule() {
		
		//set options from ini files in configs folder for this module
		//loop through all files in configs directory and add them to bootstrap options
		//uses php5 SPL DirectoryIterator class 
		$dir = new DirectoryIterator(dirname(__FILE__).'/configs');
		$arr = array();
		foreach($dir as $file) {
			if(!$file->isDot() && $file->isFile()) {
				$cfg = new Zend_Config_Ini(dirname(__FILE__).'/configs/'.$file->getFileName(), 
										   $this->getApplication()->getEnvironment(),
										   array('allowModifications'=>true));
				$arr = array_merge($arr,$cfg->toArray());
			}
		}
		
		//set options to this module's bootstrap
		$this->setOptions($arr);
		
		
		//set options to parent (application) bootstrap
		//$this->getApplication()->setOptions($arr);
		//$this->_application->setOptions($arr);
		
		//Zend_Debug::dump($this->getOptions(), 'Module Options : ');
		//Zend_Debug::dump($this->getApplication()->getOptions(),'Application Options : ');
		
	}
	
	//protected function _initAcl() {
	//	$acl = new Sgo_Acl_Loader('blah,bleh');
	//}
	
	/*
	protected function _initView() {
		
		//create a new Zend_View Object and inject it to the ViewRenderer action helper
		$view = new Zend_View();
		$view->addScriptPath(APPLICATION_PATH . '/modules/'.$this->getModuleName().'/views/partials'); //path designated to store view helper partials templates
		//$view->addScriptPath(APPLICATION_PATH . '/modules/'.$this->getModuleName().'/views/scripts'); //the default view script path
		Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer')->setView($view);
		return $view;
	}
	*/
		
	//get module's parent options
	//$options = $this->getApplication()->getOptions();
	//merge the options
	//$arr = array_merge($options,$arr);
	//re-set the options in application's bootstrap
	//$this->getApplication()->setOptions($arr);
}
