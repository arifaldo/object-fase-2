<?php


require_once 'Zend/Controller/Action.php';
ini_set("display_errors","Off");

class  authorizationmatrix_RepairController extends Application_Main
{
	public function indexAction() 
	{			
		$changesId = $this->_request->getParam('changes_id');
		
		$amount = $this->_db->select()
					       	->from(array('A' => 'TEMP_APP_BOUNDARY'),array('BOUNDARY_ID','CCY_BOUNDARY','BOUNDARY_MIN','BOUNDARY_MAX','BOUNDARY_ISUSED','CUST_ID','ROW_INDEX'));
		$amount -> where("A.CHANGES_ID LIKE ".$this->_db->quote($changesId));
		$ramount = $amount->query()->FetchAll();
		
		if(!empty($ramount))
		{
			$custid = $ramount[0]['CUST_ID'];
			$this->view->custid = $custid;
		}
		$select = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER'),array('*'));
		$select -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		$result = $select->query()->FetchAll();
		$this->view->result = $result;
		
		$ccyArr = $this->_db->select()
						->from(array('M_MINAMT_CCY'), array('CCY_ID'))
						->order('CCY_ID ASC')
						->query()
						->fetchAll();
		$this->view->ccyArr = Application_Helper_Array::listArray($ccyArr,'CCY_ID','CCY_ID');
		
		for($i=1;$i<11;$i++)
		{
			$groupuserid = 'N_'.$custid.'_0'.$i;
			$selectgroup = $this->_db->select()
					       	->from(array('A' => 'M_APP_GROUP_USER'),array('USER_ID'));
			$selectgroup -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
			$selectgroup -> where("A.GROUP_USER_ID LIKE ".$this->_db->quote($groupuserid));
			$resultgroup = $selectgroup->query()->FetchAll();
			$groupuseridview = 'group'.$i;
			$this->view->$groupuseridview = $resultgroup;
		}
		
		$specialid = 'S_'.$custid;
		$selectspecial = $this->_db->select()
					       		->from(array('A' => 'M_APP_GROUP_USER'),array('USER_ID'));
		$selectspecial -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		$selectspecial -> where("A.GROUP_USER_ID LIKE ".$this->_db->quote($specialid));
		$resultspecial = $selectspecial->query()->FetchAll();
		$this->view->special = $resultspecial;
		
		//Zend_Debug::dump($ramount);die;
		foreach($ramount as $listbound)
		{
			$i = $listbound['ROW_INDEX'];
			$boundid = 'bound'.$i;
			$checkid = 'check'.$i;
    		$curid = 'cur'.$i;
    		$lowid = 'low'.$i;
    		$upid = 'up'.$i;
    		$specialid = 'special'.$i;
    		
    		$selectspecialid = 'S_'.$custid;
    		
    		$selectspecial = $this->_db->select()
					       		->from(array('A' => 'TEMP_APP_BOUNDARY_GROUP'),array('A.GROUP_USER_ID'));
			$selectspecial -> where("A.CHANGES_ID LIKE ".$this->_db->quote($changesId));
			$selectspecial -> where("A.ROW_INDEX LIKE ".$this->_db->quote($listbound['ROW_INDEX'])); //added
			$selectspecial -> where("A.GROUP_USER_ID LIKE ".$this->_db->quote($selectspecialid));
			$resultspecial = $this->_db->fetchRow($selectspecial);
    		
			$this->view->$boundid = $listbound['BOUNDARY_ID'];
    		$this->view->$checkid = $listbound['BOUNDARY_ISUSED'];
    		$this->view->$curid = $listbound['CCY_BOUNDARY'];
    		$this->view->$lowid = Application_Helper_General::DisplayMoney($listbound['BOUNDARY_MIN']);
    		$this->view->$upid = Application_Helper_General::DisplayMoney($listbound['BOUNDARY_MAX']);
    		
    		if($resultspecial)
    		{
    			$this->view->$specialid = "1";
    		}
    		
    		$group = $this->_db->select()
					       	->from(array('A' => 'TEMP_APP_BOUNDARY_GROUP'),array('*'));
			$group -> where("A.CHANGES_ID LIKE ".$this->_db->quote($changesId));
			$group -> where("A.ROW_INDEX LIKE ".$this->_db->quote($listbound['ROW_INDEX']));
			$group -> where("A.GROUP_USER_ID NOT LIKE ".$this->_db->quote($selectspecialid));
			$rgroup = $group->query()->FetchAll();
			
			foreach($rgroup as $grouplist)
			{
				$groupid = $grouplist['GROUP_USER_ID'].'-'.$i;
				$this->view->$groupid = '1';
			}
		} 

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		

		$submit = $this->_getParam('submit');
		$reset = $this->_getParam('reset');
		
		if($submit && $this->view->hasPrivilege('RBGC'))
		{
			Application_Helper_General::writeLog('RBGC','Repair Approver Boundary Group Limit ('.$custid.')');
			$error = 0;
			$insertarray = array();
			$allgrouparr = array();
			
			for($i=1;$i<21;$i++)
			{
				$boundid = 'bound'.$i;
				$checkid = 'check'.$i;
	    		$curid = 'cur'.$i;
	    		$lowid = 'low'.$i;
	    		$upid = 'up'.$i;
	    		$specialid = 'special'.$i;
   				//Zend_Debug::dump($idamt); die;
   				
	    		$insertbound = $this->_getParam($boundid);
	    		
				$cekcheckid = $this->_getParam($checkid);
				$this->view->$checkid = $cekcheckid;
				
				$special = $this->_getParam($specialid);
				$this->view->$specialid = $special;
				
				$cur = $this->_getParam($curid);
				$this->view->$curid = $cur;
				
				$cekcur = $this->_db->select()
								->from(array('M_MINAMT_CCY'), array('CCY_ID'))
								-> where("CCY_ID LIKE ".$this->_db->quote($cur))
								->query()->fetchAll();
				
				$low = $this->_getParam($lowid);
				$this->view->$lowid = $low;
				$low = Application_Helper_General::convertDisplayMoney($low);
				$insertlow = str_replace('.','',$low);
				$cek_low = (Zend_Validate::is($insertlow,'Digits')) ? true : false;
				
				$up = $this->_getParam($upid);
				$this->view->$upid = $up;
				$up = Application_Helper_General::convertDisplayMoney($up);
				$insertup = str_replace('.','',$up);
				$cek_up = (Zend_Validate::is($insertup,'Digits')) ? true : false;
				
   				//Zend_Debug::dump($cekfrom); die;
   				//echo $cekcheckid;
   				$grupcek = 0;
				$arraygroup = array();
				for($x=1;$x<11;$x++)
				{
					$nogroup = sprintf("%02d", $x);
					$grupid = 'N_'.$custid.'_'.$nogroup.'-'.$i;
					$grup = $this->_getParam($grupid);
					$this->view->$grupid = $grup;
					if($grup)
					{
						$insertgroup = 'N_'.$custid.'_'.$nogroup;
						$arraygroup[$x] = $insertgroup;
						$grupcek++;
					}
				}
				
				
				if($cekcheckid)
				{
					$insertactive = '1';
					
					if(!$cekcur)
					{
						$errid = 'err'.$curid;
						$err = 'Invalid Currency';
						$this->view->$errid = $err;
						$error++;
					}
					
					if($low != '' && $low != null && $up != '' && $up != null)
					{
						//echo 'masuk';
						for($j=$i;$j>0;$j--)
						{
							if($i != $j)
							{
		    					$ccycekid = 'cur'.$j;
		    					$ccycek = $this->_getParam($ccycekid);
   								
   								if($ccycek == $cur)
   								{
   									$lowerror = false;
									$uperror = false;
									$lowcekid = 'low'.$j;
			    					$upcekid = 'up'.$j;
			    					$lowcek = $this->_getParam($lowcekid);
		    						$upcek = $this->_getParam($upcekid);
			    					$lowcek = Application_Helper_General::convertDisplayMoney($lowcek);
   									$upcek = Application_Helper_General::convertDisplayMoney($upcek);	

   									if($lowcek <= $low && $low <= $upcek)
   									{
   										$lowerror = true;
   										//echo 'error';
   										//echo $cekfrom;die;
   									}
   								
									if($lowcek <= $up && $up <= $upcek)
   									{
   										$uperror = true;
   										//echo 'error';
   									}
   								
   									if($lowerror == true || $uperror == true)
   									{
   										$errid = 'overlap'.$i.'-'.$j;
										$err = 'Value overlap not allowed (overlap with : '.$lowcek.'-'.$upcek.')';
										$this->view->$errid = $err;
										$errid2 = 'overlap'.$i;
										$this->view->$errid2 = true;
										$error++;
   									}
   								}
   								
							}
						}
						
						if ($low >= $up)
						{
							$errid = 'err'.$lowid;
							$err = $this->language->_('Lower Boundary Range has to be less than Upper Boundary Range');
							$this->view->$errid = $err;
							$error++;
						}
						
						if($cek_low == false)
						{
							$errid = 'err'.$lowid;
							$err = $this->language->_('Amount value has to be numeric');
							$this->view->$errid = $err;
							$error++;
						}
				
						if($cek_up == false)
						{
							$errid = 'err'.$upid;
							$err = $this->language->_('Amount value has to be numeric');
							$this->view->$errid = $err;
							$error++;
						}
					}
					
					if ($low == '' || $low == null)
					{
						$errid = 'err'.$lowid;
						$err = 'Lower Boundary Range should not be empty while the boundary is active';
						$this->view->$errid = $err;
						$error++;
						//die;
					}
					
					if ($up == '' || $up == null)
					{
						$errid = 'err'.$upid;
						$err = $this->language->_('Upper Boundary Range should not be empty while the boundary is active');
						$this->view->$errid = $err;
						$error++;
						//die;
					}
					
					if($grupcek == 0 && ($special == '' || $special == null))
					{
						$errid = 'errgrup'.$i;
						$err = $this->language->_('Boundary must have group member(s)');
						$this->view->$errid = $err;
						$error++;
					}
				}
				else
				{
					$insertactive = '0';
				}
				
				if($ramount)
				{
					if($insertbound)
					{
						$insertarray[$i]['bound'] = $insertbound;
						$insertarray[$i]['low'] = $low;
						$insertarray[$i]['up'] = $up;
						$insertarray[$i]['ccy'] = $cur;
						$insertarray[$i]['active'] = $insertactive;
						$insertarray[$i]['group'] = $arraygroup;
						$insertarray[$i]['special'] = $special;
						$insertarray[$i]['row'] = $i;
					}
					else
					{
						if(($low != (1000*($i-1))+1 && $up != (1000*$i)) || $insertactive || $arraygroup)
						{
							$insertarray[$i]['bound'] = $insertbound;
							$insertarray[$i]['low'] = $low;
							$insertarray[$i]['up'] = $up;
							$insertarray[$i]['ccy'] = $cur;
							$insertarray[$i]['active'] = $insertactive;
							$insertarray[$i]['group'] = $arraygroup;
							$insertarray[$i]['special'] = $special;
							$insertarray[$i]['row'] = $i;
						}
					}
				}
				else
				{
					if(($low != (1000*($i-1))+1 && $up != (1000*$i)) || $insertactive || $arraygroup)
					{
						$insertarray[$i]['bound'] = $insertbound;
						$insertarray[$i]['low'] = $low;
						$insertarray[$i]['up'] = $up;
						$insertarray[$i]['ccy'] = $cur;
						$insertarray[$i]['active'] = $insertactive;
						$insertarray[$i]['group'] = $arraygroup;
						$insertarray[$i]['special'] = $special;
						$insertarray[$i]['row'] = $i;
					}	
				}
				unset($arraygroup);
			}
			
			//Zend_Debug::dump($insertarray);die;
   			//die;		
			if($error == 0)
			{
				$this->_db->beginTransaction();
				
				try
				{
				   	$changeInfo= "Edit Boundary";
					$changeType = $this->_changeType['code']['edit'];
					$masterTable = 'M_APP_BOUNDARY,M_APP_BOUNDARY_GROUP';
					$tempTable =  'TEMP_APP_BOUNDARY,TEMP_APP_BOUNDARY_GROUP';
					$keyField = 'CUST_ID';
					$keyValue = $custid;
					$displayTableName = 'Approver Group Boundary';
				
					$this->updateGlobalChanges($changesId,$changeInfo);
					
					$where = array('CHANGES_ID = ?' => $changesId);
					$this->_db->delete('TEMP_APP_BOUNDARY',$where);
					$this->_db->delete('TEMP_APP_BOUNDARY_GROUP',$where);
					
					$specialinsertid = 'S_'.$custid;
					
					foreach($insertarray as $insert)
					{	
						$data = array(
										'CHANGES_ID' 		=> $changesId,
										'CCY_BOUNDARY' 		=> $insert['ccy'],
										'BOUNDARY_ID'		=> $insert['bound'],
										'BOUNDARY_MIN'		=> $insert['low'],
										'BOUNDARY_MAX'		=> $insert['up'],
										'CUST_ID' 			=> $custid,
										'BOUNDARY_ISUSED'	=> $insert['active'],
										'ROW_INDEX'			=> $insert['row'],
									);
						$this->_db->insert('TEMP_APP_BOUNDARY',$data);										
						//echo 'aaa';die;
						
						foreach($insert['group'] as $key=>$val)
						{
							$datagroup = array(
												'CHANGES_ID' 	=> $changesId,
												'BOUNDARY_ID'	=> $insert['bound'],
												'GROUP_USER_ID'	=> $val,
												'ROW_INDEX'		=> $insert['row'],
												);
							$this->_db->insert('TEMP_APP_BOUNDARY_GROUP',$datagroup);
						}
						
						if($insert['special'])
						{
							
							$dataspecial = array(
												 'CHANGES_ID' 		=> $changesId,
												 'BOUNDARY_ID'		=> $insert['bound'],
												 'GROUP_USER_ID'	=> $specialinsertid,
												 'ROW_INDEX'		=> $insert['row'],
												);
							$this->_db->insert('TEMP_APP_BOUNDARY_GROUP',$dataspecial);
						}
					}

					if($error == 0)
					{	
						//die;
						//Zend_Debug::dump('commit'); die;
						$this->_db->commit();
						$this->_redirect('/popup/successpopup/index');
					}
				}
				
				catch(Exception $e)
				{
					//Zend_Debug::dump('rollback'); die;
					$this->_db->rollBack();
				}
				
			}
		}
		Application_Helper_General::writeLog('RBGC','View Repair Approver Boundary Group Limit ('.$custid.') page');		
	}
}
