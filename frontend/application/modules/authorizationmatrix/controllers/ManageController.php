<?php


require_once 'Zend/Controller/Action.php';


class  authorizationmatrix_ManageController extends Application_Main
{
	public function indexAction() 
	{	
		$this->_helper->layout()->setLayout('newlayout');
		$custid = $this->_getParam('cust_id');
		$this->view->custid = $custid;
		
		$select = $this->_db->select()
					        ->from(array('A' => 'M_CUSTOMER'),array('CUST_ID','CUST_NAME'));
		$select -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		$result = $this->_db->fetchRow($select);
		$this->view->result = $result;
		
		$ccyArr = $this->_db->select()
						->from(array('M_MINAMT_CCY'), array('CCY_ID'))
						->order('CCY_ID ASC')
						->query()
						->fetchAll();
		$this->view->ccyArr = Application_Helper_Array::listArray($ccyArr,'CCY_ID','CCY_ID');
		
		for($i=1;$i<11;$i++)
		{
			$groupuserid = 'N_'.$custid.'_0'.$i;
			if(strlen($i) == 2)
			{
				$groupuserid = 'N_'.$custid.'_'.$i;
			}
			$selectgroup = $this->_db->select()
					       	->from(array('A' => 'M_APP_GROUP_USER'),array('USER_ID'));
			$selectgroup -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
			$selectgroup -> where("A.GROUP_USER_ID LIKE ".$this->_db->quote($groupuserid));
			$resultgroup = $selectgroup->query()->FetchAll();
			$groupuseridview = 'group'.$i;
			$this->view->$groupuseridview = $resultgroup;
		}
		
		$specialid = 'S_'.$custid;
		$selectspecial = $this->_db->select()
					       		->from(array('A' => 'M_APP_GROUP_USER'),array('USER_ID'));
		$selectspecial -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		$selectspecial -> where("A.GROUP_USER_ID LIKE ".$this->_db->quote($specialid));
		$resultspecial = $selectspecial->query()->FetchAll();
		$this->view->special = $resultspecial;
		
		$amount = $this->_db->select()
					       	->from(array('A' => 'M_APP_BOUNDARY'),array('BOUNDARY_ID','CCY_BOUNDARY','BOUNDARY_MIN','BOUNDARY_MAX','BOUNDARY_ISUSED','ROW_INDEX'));
		$amount -> where("A.CUST_ID LIKE ".$this->_db->quote($custid));
		$ramount = $amount->query()->FetchAll();
		//Zend_Debug::dump($ramount);die;
		foreach($ramount as $listbound)
		{
			$i = $listbound['ROW_INDEX'];
			$boundid = 'bound'.$i;
			$checkid = 'check'.$i;
    		$curid = 'cur'.$i;
    		$lowid = 'low'.$i;
    		$upid = 'up'.$i;
    		$specialid = 'special'.$i;
    		
    		$selectspecialid = 'S_'.$custid;
    		
    		$selectspecial = $this->_db->select()
					       		->from(array('A' => 'M_APP_BOUNDARY_GROUP'),array('A.GROUP_USER_ID'));
			$selectspecial -> where("A.BOUNDARY_ID LIKE ".$this->_db->quote($listbound['BOUNDARY_ID']));
			$selectspecial -> where("A.GROUP_USER_ID LIKE ".$this->_db->quote($selectspecialid));
			$resultspecial = $this->_db->fetchRow($selectspecial);
    		
			$this->view->$boundid = $listbound['BOUNDARY_ID'];
    		$this->view->$checkid = $listbound['BOUNDARY_ISUSED'];
    		$this->view->$curid = $listbound['CCY_BOUNDARY'];
    		$this->view->$lowid = Application_Helper_General::DisplayMoney($listbound['BOUNDARY_MIN']);
    		$this->view->$upid = Application_Helper_General::DisplayMoney($listbound['BOUNDARY_MAX']);
    		
    		if($resultspecial)
    		{
    			$this->view->$specialid = "1";
    		}
    		
    		$group = $this->_db->select()
					       	->from(array('A' => 'M_APP_BOUNDARY_GROUP'),array('*'));
			$group -> where("A.BOUNDARY_ID LIKE ".$this->_db->quote($listbound['BOUNDARY_ID']));
			$rgroup = $group->query()->FetchAll();
			
			foreach($rgroup as $grouplist)
			{
				$groupid = $grouplist['GROUP_USER_ID'].'-'.$i;
				$this->view->$groupid = '1';
				//Zend_Debug::dump($groupid);die;
			}
		} 

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		

		$submit = $this->_getParam('submit');
		$reset = $this->_getParam('reset');
		
		$select2 = $this->_db->select()
							->from('TEMP_APP_BOUNDARY');
		$select2 -> where("CUST_ID LIKE ".$this->_db->quote($custid));
		$cek = $select2->query()->FetchAll();

		if(!$cek)
		{
			if($submit && $this->view->hasPrivilege('UABG'))
			{
				Application_Helper_General::writeLog('UABG','Update Approver Boundary Group ('.$custid.')');
				$error = 0;
				$insertarray = array();
				$allgrouparr = array();
				
				for($i=1;$i<21;$i++)
				{
					$boundid = 'bound'.$i;
					$checkid = 'check'.$i;
		    		$curid = 'cur'.$i;
		    		$lowid = 'low'.$i;
		    		$upid = 'up'.$i;
		    		$specialid = 'special'.$i;

		    		$insertbound = $this->_getParam($boundid);
		    		
					$cekcheckid = $this->_getParam($checkid);
					$this->view->$checkid = $cekcheckid;
					
					$special = $this->_getParam($specialid);
					$this->view->$specialid = $special;
					
					$cur = $this->_getParam($curid);
					$this->view->$curid = $cur;
					
					$cekcur = $this->_db->select()
									->from(array('M_MINAMT_CCY'), array('CCY_ID'))
									-> where("CCY_ID LIKE ".$this->_db->quote($cur))
									->query()->fetchAll();
					
					$low = $this->_getParam($lowid);
					$this->view->$lowid = $low;
					$low = Application_Helper_General::convertDisplayMoney($low);
					$insertlow = str_replace('.','',$low);
					$cek_low = (Zend_Validate::is($insertlow,'Digits')) ? true : false;
					
					$up = $this->_getParam($upid);
					$this->view->$upid = $up;
					$up = Application_Helper_General::convertDisplayMoney($up);
					$insertup = str_replace('.','',$up);
					$cek_up = (Zend_Validate::is($insertup,'Digits')) ? true : false;

					$grupcek = 0;
					$arraygroup = array();
					for($x=1;$x<11;$x++)
					{
						$nogroup = sprintf("%02d", $x);
						$grupid = 'N_'.$custid.'_'.$nogroup.'-'.$i;
						$grup = $this->_getParam($grupid);
						$this->view->$grupid = $grup;
						if($grup)
						{
							$insertgroup = 'N_'.$custid.'_'.$nogroup;
							$arraygroup[$x] = $insertgroup;
							$grupcek++;
						}
					}
					
					if($cekcheckid)
					{
						$insertactive = '1';
						
						if(!$cekcur)
						{
							$errid = 'err'.$curid;
							$err = 'Invalid Currency';
							$this->view->$errid = $err;
							$error++;
						}

						if($low != '' && $low != null && $up != '' && $up != null)
						{
							for($j=$i;$j>=1;$j--)
							{
								if($i != $j)
								{
			    					$ccycekid = 'cur'.$j;
			    					$ccycek = $this->_getParam($ccycekid);
   									
   									if($ccycek == $cur)
   									{
   										$lowerror = false;
										$uperror = false;
										$lowcekid = 'low'.$j;
				    					$upcekid = 'up'.$j;
				    					$lowcek = $this->_getParam($lowcekid);
			    						$upcek = $this->_getParam($upcekid);
				    					$lowcek = Application_Helper_General::convertDisplayMoney($lowcek);
   										$upcek = Application_Helper_General::convertDisplayMoney($upcek);	
			    						
	   									if($lowcek <= $low && $low <= $upcek)
	   									{
	   										$lowerror = true;
	   										//echo 'error';
	   										//echo $cekfrom;die;
	   									}
	   								
										if($lowcek <= $up && $up <= $upcek)
	   									{
	   										$uperror = true;
	   										//echo 'error';
	   									}
	   								
	   									if($lowerror == true || $uperror == true)
	   									{
	   										$errid = 'overlap'.$i.'-'.$j;
											$err = 'Value overlap not allowed (overlap with : '.$lowcek.'-'.$upcek.')';
											$this->view->$errid = $err;
											$errid2 = 'overlap'.$i;
											$this->view->$errid2 = true;
											$error++;
	   									}
   									}
   									
								}
							}
							
							if ($low >= $up)
							{
								$errid = 'err'.$lowid;
								$err = 'Lower Boundary Range has to be less than Upper Boundary Range';
								$this->view->$errid = $err;
								$error++;
							}
							
							if($cek_low == false)
							{
								$errid = 'err'.$lowid;
								$err = 'Amount value has to be numeric';
								$this->view->$errid = $err;
								$error++;
							}
					
							if($cek_up == false)
							{
								$errid = 'err'.$upid;
								$err = 'Amount value has to be numeric';
								$this->view->$errid = $err;
								$error++;
							}
						}
						
						//$maxVal = '9 999 999 999 999.99';
						$maxVal = '9999999999999.99';
						
						if ($low == '' || $low == null)
						{
							$errid = 'err'.$lowid;
							$err = 'Lower Boundary Range should not be empty while the boundary is active';
							$this->view->$errid = $err;
							$error++;
							//die;
						}else{
							if($low > $maxVal){
								$errid = 'err'.$lowid;
								$err = 'Maximum Lower Boundary value is '.Application_Helper_General::displayCurrency($maxVal);
								$this->view->$errid = $err;
								$error++;
							}
						}
						
						if ($up == '' || $up == null)
						{
							$errid = 'err'.$upid;
							$err = 'Upper Boundary Range should not be empty while the boundary is active';
							$this->view->$errid = $err;
							$error++;
							//die;
						}else{
							if($up > $maxVal){
								$errid = 'err'.$upid;
								$err = 'Maximum Upper Boundary value is '.Application_Helper_General::displayCurrency($maxVal);
								$this->view->$errid = $err;
								$error++;
							}
						}
						
						if($grupcek == 0 && ($special == '' || $special == null))
						{
							$errid = 'errgrup'.$i;
							$err = 'Boundary must have group member(s)';
							$this->view->$errid = $err;
							$error++;
						}
					}
					else
					{
						$insertactive = '0';
					}

					
					if($ramount)
					{
						if($insertbound)
						{
							$insertarray[$i]['bound'] = $insertbound;
							$insertarray[$i]['low'] = $low;
							$insertarray[$i]['up'] = $up;
							$insertarray[$i]['ccy'] = $cur;
							$insertarray[$i]['active'] = $insertactive;
							$insertarray[$i]['group'] = $arraygroup;
							$insertarray[$i]['special'] = $special;
							$insertarray[$i]['row'] = $i;
						}
						else
						{
							if(($low != (1000*($i-1))+1 && $up != (1000*$i)) || $insertactive || $arraygroup)
							{
								$insertarray[$i]['bound'] = $insertbound;
								$insertarray[$i]['low'] = $low;
								$insertarray[$i]['up'] = $up;
								$insertarray[$i]['ccy'] = $cur;
								$insertarray[$i]['active'] = $insertactive;
								$insertarray[$i]['group'] = $arraygroup;
								$insertarray[$i]['special'] = $special;
								$insertarray[$i]['row'] = $i;
							}
						}
					}
					else
					{
						if(($low != (1000*($i-1))+1 && $up != (1000*$i)) || $insertactive || $arraygroup)
						{
							$insertarray[$i]['bound'] = $insertbound;
							$insertarray[$i]['low'] = $low;
							$insertarray[$i]['up'] = $up;
							$insertarray[$i]['ccy'] = $cur;
							$insertarray[$i]['active'] = $insertactive;
							$insertarray[$i]['group'] = $arraygroup;
							$insertarray[$i]['special'] = $special;
							$insertarray[$i]['row'] = $i;
						}	
					}
					unset($arraygroup);
				}

				if($error == 0)
				{
					$this->_db->beginTransaction();
					
					try
					{
					   	$changeInfo= "Edit Boundary";
						$changeType = $this->_changeType['code']['edit'];
						$masterTable = 'M_APP_BOUNDARY,M_APP_BOUNDARY_GROUP';
						$tempTable =  'TEMP_APP_BOUNDARY,TEMP_APP_BOUNDARY_GROUP';
						$keyField = $custid;
						$keyValue = $result['CUST_NAME'];
						$displayTableName = 'Approver Group Boundary';
					
						$changesId = $this->suggestionWaitingApproval($displayTableName, $changeInfo, $changeType,'',$masterTable,$tempTable,$keyField,$keyValue,$custid);
						
						$specialinsertid = 'S_'.$custid;
						
						foreach($insertarray as $insert)
						{	

							try{
							$data = array(
											'CHANGES_ID' 		=> $changesId,
											'CCY_BOUNDARY' 		=> $insert['ccy'],
											'BOUNDARY_ID'		=> $insert['bound'],
											'BOUNDARY_MIN'		=> $insert['low'],
											'BOUNDARY_MAX'		=> $insert['up'],
											'CUST_ID' 			=> $custid,
											'BOUNDARY_ISUSED'	=> $insert['active'],
											'ROW_INDEX'			=> $insert['row'],
										);
							$this->_db->insert('TEMP_APP_BOUNDARY',$data);										
							// echo 'aaa';die;
							
							foreach($insert['group'] as $key=>$val)
							{
								$datagroup = array(
													'CHANGES_ID' 	=> $changesId,
													'BOUNDARY_ID'	=> $insert['bound'],
													'GROUP_USER_ID'	=> $val,
													'ROW_INDEX'		=> $insert['row'],
													);
								$this->_db->insert('TEMP_APP_BOUNDARY_GROUP',$datagroup);
								// echo 'aaa1';die;
							}
							
							if($insert['special'])
							{
								
								$dataspecial = array(
													 'CHANGES_ID' 		=> $changesId,
													 'BOUNDARY_ID'		=> $insert['bound'],
													 'GROUP_USER_ID'	=> $specialinsertid,
													 'ROW_INDEX'		=> $insert['row'],
													);
								$this->_db->insert('TEMP_APP_BOUNDARY_GROUP',$dataspecial);
								// echo 'aaa3';die;
							}

							}catch(Exception $e){
								print_r($e);die;
							}
						}
						// Zend_Debug::dump($error); die;
						if($error == 0)
						{	
							//die;
							
							$this->_db->commit();
							
							//pengaturan url untuk button back
	                        //$this->setbackURL('/customer/view/index/cust_id/'.$custid); 
	                        Application_Helper_General::writeLog('UABG','Update Approver Boundary Group ('.$custid.')');
	                        $this->_redirect('/notification/submited/index');
							//$this->_redirect('/notification/success/index');
						}
					}
					
					catch(Exception $e)
					{
						//Zend_Debug::dump('rollback'); die;
						$this->_db->rollBack();
						$docErr = $this->language->_('An Error Occured. Please Try Again');	
						$this->view->error = $docErr;
					}
					
				}
			}		
		}
		if($cek)
		{			
			$docErr = "*".$this->language->_('No changes allowed for this record while awaiting approval for previous change')."";	
			$this->view->error = $docErr;
			$this->view->changestatus = "disabled";
		}
		Application_Helper_General::writeLog('UABG','View Update Approver Boundary Group ('.$custid.') page');	
	}
}
