<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'SGO/Extendedmodule/phpqrcode/qrlib.php';
require_once("Service/Account.php");

class eform_BgclosingmdController extends Application_Main
{

	protected $_moduleDB = 'RTF'; // masih harus diganti

	public function indexAction()
	{

		$this->_helper->_layout->setLayout('newlayout');
		$settings = new Settings();
		// BEGIN GLOBAL VARIABEL
		$config = Zend_Registry::get('config');
		$bgpublishType  = $config["bgpublish"]["type"]["desc"];
		$bgpublishCode  = $config["bgpublish"]["type"]["code"];
		$arrbgpublish   = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

		$bgcgType   = $config["bgcg"]["type"]["desc"];
		$bgcgCode   = $config["bgcg"]["type"]["code"];
		$arrbgcg 	= array_combine(array_values($bgcgCode), array_values($bgcgType));

		$Settings = new Settings();

		$toc_ind = $Settings->getSetting('ftemplate_bgclosingmd_ind');
		$this->view->toc_ind = $toc_ind;
		$toc_eng = $Settings->getSetting('ftemplate_bgclosingmd_eng');
		$this->view->toc_eng = $toc_eng;

		$allSetting = $settings->getAllSetting();
		$this->view->snkPelepasanMd = $allSetting['ftemplate_bgclosingmd_ind'];

		// END GLOBAL VARIABEL



		// BEGIN PENUTUPAN BANK GARANSI
		// BEGIN DETAIL BANK GARANSI
		$bgparam = $this->_getParam('bgnumb');
		$AESMYSQL = new Crypt_AESMYSQL();
		$decryption = urldecode($bgparam);
		$numb = $AESMYSQL->decrypt($decryption, $password);
		if (!empty($numb)) {
			$bgdata = $this->_db->select()
				->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
				->joinleft(array('B' => 'M_CUSTOMER'), 'A.CUST_ID = B.CUST_ID', array('B.CUST_NAME'))
				->joinleft(array('C' => 'M_BRANCH'), 'A.BG_BRANCH = C.BRANCH_CODE', array('C.BRANCH_NAME'))
				->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
				->where('A.BG_REG_NUMBER = ?', $numb)
				->query()->fetchAll();

			if (!empty($bgdata)) {
				$getBG = $bgdata['0'];

				if ($getBG['BG_NUMBER'] == '') {
					$getBG['BG_NUMBER'] = '-';
				}

				if ($getBG['BG_SUBJECT'] == '') {
					$getBG['BG_SUBJECT'] = '- no subject -';
				}

				$this->view->getBG = $getBG;

				$this->view->publishDesc = $arrbgpublish[$getBG['BG_PUBLISH']];
				if ($getBG['BG_PUBLISH'] == 1) {
					$getPaperPrint = $this->_db->select()
						->from('M_PAPER')
						->where('notes = ?', $getBG['BG_NUMBER'])
						->where('STATUS = 1')
						->query()->fetchAll();

					$totalKertas = count($getPaperPrint);

					if ($totalKertas == 1) {
						$this->view->nomorRefKertas = ' - Nomor Ref Kertas : ' . $getPaperPrint[$totalKertas - 1]['PAPER_ID'] . ' (' . $totalKertas . ' lembar)';
					} else {
						$this->view->nomorRefKertas = ' - Nomor Ref Kertas : ' . $getPaperPrint[0]['PAPER_ID'] . ' - ' . $getPaperPrint[$totalKertas - 1]['PAPER_ID'] . ' (' . $totalKertas . ' lembar)';
					}
				}

				// BEGIN NILAI BANK GARANSI

				$bgdetaildata = $this->_db->select()
					->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
					->where('A.BG_REG_NUMBER = ?', $getBG["BG_REG_NUMBER"])
					->query()->fetchAll();


				if (!empty($bgdetaildata)) {
					foreach ($bgdetaildata as $key => $value) {
						if ($value['PS_FIELDNAME'] == 'Currency') {
							$this->view->bgCurrency =   $value['PS_FIELDVALUE'];
						}

						if ($getBG['COUNTER_WARRANTY_TYPE'] == '3') {

							if ($value['PS_FIELDNAME'] == 'Insurance Name') {
								$cekInsuranceDetail = $this->_db->select()
									->from('M_CUSTOMER')
									->where('CUST_ID = ?', $value['PS_FIELDVALUE'])
									->query()->fetch();

								$this->view->insuraceName = $cekInsuranceDetail['CUST_NAME'];
							}

							if ($value['PS_FIELDNAME'] == 'Insurance Branch') {

								$insuranceBranch = $this->_db->select()
									->from("M_INS_BRANCH")
									->where("INS_BRANCH_CODE = ?", $value['PS_FIELDVALUE'])
									->query()->fetchAll();

								$this->view->insuranceBranch = $insuranceBranch[0]["INS_BRANCH_NAME"];
							}
						}
					}
				}

				$this->view->BG_AMOUNT  = $getBG['BG_AMOUNT'];
				$this->view->updateStart = Application_Helper_General::convertDate($getBG['TIME_PERIOD_START'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->updateEnd = Application_Helper_General::convertDate($getBG['TIME_PERIOD_END'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->bgClaimDate = Application_Helper_General::convertDate($getBG['BG_CLAIM_DATE'], $this->view->viewDateFormat, $this->view->defaultDateFormat);

				// END NILAI BANK GARANSI

				// BEGIN KONTRA GARANSI
				$this->view->counterWarrantyType = $getBG['COUNTER_WARRANTY_TYPE'];
				$this->view->counterWarrantyDesc = $arrbgcg[$getBG['COUNTER_WARRANTY_TYPE']];

				// END KONTRA GARANSI

				// BEGIN MARGINAL DEPOSIT PRINCIPAL
				$saveHoldAmount = null;

				$bgdatasplit = $this->_db->select()
					->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
					->joinLeft(['B' => 'M_CUSTOMER_ACCT'], 'A.ACCT = B.ACCT_NO AND B.CUST_ID = ' . $this->_db->quote($this->_custIdLogin) . '', ["M_ACCT_DESC" => "B.ACCT_DESC", "M_ACCT_TYPE" => "B.ACCT_TYPE", 'CUST_ID', 'CCY_ID'])
					->where('A.BG_REG_NUMBER = ?', $getBG['BG_REG_NUMBER'])
					->query()->fetchAll();

				foreach ($bgdatasplit as $key => $value) {
					if ($value['CUST_ID'] != $getBG['CUST_ID']) continue;
					if ($value["ACCT_DESC"]) continue;

					$saveHoldAmount += intval($value["AMOUNT"]);
				}

				$this->view->holdAmount = $saveHoldAmount;
				// END MARGINAL DEPOSIT PRINCIPAL

				// BEGIN PELEPASAN DANA
				$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

				$type = array('D', 'T', '20', '30', 'S', '10');
				$param = array('ACCT_TYPE' => $type);
				$AccArr = $CustomerUser->getAccountsBG($param);
				$getAllProduct = $this->_db->select()
					->from("M_PRODUCT")
					->query()->fetchAll();

				$newArr2 = [];
				foreach ($AccArr as $keyArr => $arrAcct) {

					$accountTypeCheck = false;
					$productTypeCheck = false;

					$svcAccount = new Service_Account($arrAcct['ACCT_NO'], null, null, null, null, null);
					$result = $svcAccount->inquiryAccountBalance();
					if ($result['response_code'] == '0000') {
						$accountType = $result['account_type'];
						$productType = $result['product_type'];
					} else {
						$result = $svcAccount->inquiryDeposito();

						$accountType = $result['account_type'];
						$productType = $result['product_type'];
					}

					foreach ($getAllProduct as $key => $value) {
						# code...
						if ($value['PRODUCT_CODE'] == $accountType) {
							$accountTypeCheck = true;
						};

						if ($value['PRODUCT_PLAN'] == $productType) {
							$productTypeCheck = true;
						};
						$productTypeCheck = true;
					}

					if ($accountTypeCheck && $productTypeCheck) $newArr2[] = $arrAcct;
				}

				$this->view->AccArr = $newArr2;
				// END PELEPASAN DANA

			}
		}
		// END DETAIL BANK GARANSI

		// END PENUTUPAN BANK GARANSI

		// BEGIN CONFIRM PAGE
		if ($this->_request->isPost()) {
			$datasession = $this->_request->getParams();

			$sessionNamespace = new Zend_Session_Namespace('bgclosingmd');
			$sessionNamespace->content = $datasession;

			$this->_redirect('/eform/bgclosingmd/confirm');
		}
		// END CONFIRM PAGE


	}

	public function confirmAction()
	{

		$this->_helper->_layout->setLayout('newlayout');
		$sessionNamespace = new Zend_Session_Namespace('bgclosingmd');
		$data = $sessionNamespace->content;

		$this->_helper->_layout->setLayout('newlayout');
		$settings = new Settings();
		// BEGIN GLOBAL VARIABEL
		$config = Zend_Registry::get('config');
		$bgpublishType  = $config["bgpublish"]["type"]["desc"];
		$bgpublishCode  = $config["bgpublish"]["type"]["code"];
		$arrbgpublish   = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

		$bgcgType   = $config["bgcg"]["type"]["desc"];
		$bgcgCode   = $config["bgcg"]["type"]["code"];
		$arrbgcg 	= array_combine(array_values($bgcgCode), array_values($bgcgType));

		// END GLOBAL VARIABEL

		// BEGIN PENUTUPAN BANK GARANSI
		// BEGIN DETAIL BANK GARANSI
		$bgparam = $data['bgnumb'];
		$AESMYSQL = new Crypt_AESMYSQL();
		$decryption = urldecode($bgparam);
		$numb = $AESMYSQL->decrypt($decryption, $password);
		if (!empty($numb)) {
			$bgdata = $this->_db->select()
				->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
				->joinleft(array('B' => 'M_CUSTOMER'), 'A.CUST_ID = B.CUST_ID', array('B.CUST_NAME'))
				->joinleft(array('C' => 'M_BRANCH'), 'A.BG_BRANCH = C.BRANCH_CODE', array('C.BRANCH_NAME'))
				->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
				->where('A.BG_REG_NUMBER = ?', $numb)
				->query()->fetchAll();


			if (!empty($bgdata)) {
				$getBG = $bgdata['0'];

				if ($getBG['BG_NUMBER'] == '') {
					$getBG['BG_NUMBER'] = '-';
				}

				if ($getBG['BG_SUBJECT'] == '') {
					$getBG['BG_SUBJECT'] = '- no subject -';
				}

				$this->view->getBG = $getBG;

				$this->view->publishDesc = $arrbgpublish[$getBG['BG_PUBLISH']];
				if ($getBG['BG_PUBLISH'] == 1) {
					$getPaperPrint = $this->_db->select()
						->from('M_PAPER')
						->where('notes = ?', $getBG['BG_NUMBER'])
						->where('STATUS = 1')
						->query()->fetchAll();

					$totalKertas = count($getPaperPrint);

					if ($totalKertas == 1) {
						$this->view->nomorRefKertas = ' - Nomor Ref Kertas : ' . $getPaperPrint[$totalKertas - 1]['PAPER_ID'] . ' (' . $totalKertas . ' lembar)';
					} else {
						$this->view->nomorRefKertas = ' - Nomor Ref Kertas : ' . $getPaperPrint[0]['PAPER_ID'] . ' - ' . $getPaperPrint[$totalKertas - 1]['PAPER_ID'] . ' (' . $totalKertas . ' lembar)';
					}
				}

				// BEGIN NILAI BANK GARANSI

				$bgdetaildata = $this->_db->select()
					->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
					->where('A.BG_REG_NUMBER = ?', $getBG["BG_REG_NUMBER"])
					->query()->fetchAll();


				if (!empty($bgdetaildata)) {
					foreach ($bgdetaildata as $key => $value) {
						if ($value['PS_FIELDNAME'] == 'Currency') {
							$this->view->bgCurrency =   $value['PS_FIELDVALUE'];
						}

						if ($getBG['COUNTER_WARRANTY_TYPE'] == '3') {

							if ($value['PS_FIELDNAME'] == 'Insurance Name') {

								$cekInsuranceDetail = $this->_db->select()
									->from('M_CUSTOMER')
									->where('CUST_ID = ?', $value['PS_FIELDVALUE'])
									->query()->fetch();

								$this->view->insuraceName =   $cekInsuranceDetail['CUST_NAME'];
							}

							if ($value['PS_FIELDNAME'] == 'Insurance Branch') {

								$insuranceBranch = $this->_db->select()
									->from("M_INS_BRANCH")
									->where("INS_BRANCH_CODE = ?", $value['PS_FIELDVALUE'])
									->query()->fetchAll();

								$this->view->insuranceBranch = $insuranceBranch[0]["INS_BRANCH_NAME"];
							}
						}
					}
				}

				$this->view->BG_AMOUNT  = $getBG['BG_AMOUNT'];
				$this->view->updateStart = Application_Helper_General::convertDate($getBG['TIME_PERIOD_START'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->updateEnd = Application_Helper_General::convertDate($getBG['TIME_PERIOD_END'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->bgClaimDate = Application_Helper_General::convertDate($getBG['BG_CLAIM_DATE'], $this->view->viewDateFormat, $this->view->defaultDateFormat);

				// END NILAI BANK GARANSI

				// BEGIN KONTRA GARANSI
				$this->view->counterWarrantyType = $getBG['COUNTER_WARRANTY_TYPE'];
				$this->view->counterWarrantyDesc = $arrbgcg[$getBG['COUNTER_WARRANTY_TYPE']];

				// END KONTRA GARANSI

				// BEGIN MARGINAL DEPOSIT PRINCIPAL
				$saveHoldAmount = null;

				$bgdatasplit = $this->_db->select()
					->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
					->joinLeft(['B' => 'M_CUSTOMER_ACCT'], 'A.ACCT = B.ACCT_NO AND B.CUST_ID = ' . $this->_db->quote($this->_custIdLogin) . '', ["M_ACCT_DESC" => "B.ACCT_DESC", "M_ACCT_TYPE" => "B.ACCT_TYPE", 'CUST_ID', 'CCY_ID'])
					->where('A.BG_REG_NUMBER = ?', $getBG['BG_REG_NUMBER'])
					->query()->fetchAll();

				foreach ($bgdatasplit as $key => $value) {
					if ($value['CUST_ID'] != $getBG['CUST_ID']) continue;
					if ($value["ACCT_DESC"]) continue;

					$saveHoldAmount += intval($value["AMOUNT"]);
				}

				$this->view->holdAmount = $saveHoldAmount;
				// END MARGINAL DEPOSIT PRINCIPAL

				// BEGIN PELEPASAN DANA
				$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

				$type = array('D', 'T', '20', '30', 'S', '10');
				$param = array('ACCT_TYPE' => $type);
				$AccArr = $CustomerUser->getAccountsBG($param);
				$getAllProduct = $this->_db->select()
					->from("M_PRODUCT")
					->query()->fetchAll();

				$newArr2 = [];
				foreach ($AccArr as $keyArr => $arrAcct) {

					$accountTypeCheck = false;
					$productTypeCheck = false;

					$svcAccount = new Service_Account($arrAcct['ACCT_NO'], null, null, null, null, null);
					$result = $svcAccount->inquiryAccountBalance();
					if ($result['response_code'] == '0000') {
						$accountType = $result['account_type'];
						$productType = $result['product_type'];
					} else {
						$result = $svcAccount->inquiryDeposito();

						$accountType = $result['account_type'];
						$productType = $result['product_type'];
					}

					foreach ($getAllProduct as $key => $value) {
						# code...
						if ($value['PRODUCT_CODE'] == $accountType) {
							$accountTypeCheck = true;
						};

						if ($value['PRODUCT_PLAN'] == $productType) {
							$productTypeCheck = true;
						};

						$productTypeCheck = true;
					}

					if ($accountTypeCheck && $productTypeCheck) $newArr2[] = $arrAcct;
				}

				$this->view->AccArr = $newArr2;
				// END PELEPASAN DANA

			}
		}
		// END DETAIL BANK GARANSI

		// END PENUTUPAN BANK GARANSI


		// BEGIN LIST ACCOUNT PELEPASAN DANA

		if (!empty($data['flselect_account_number'])) {
			$jumlahAccount = count($data['flselect_account_number']);
		} else {
			$jumlahAccount = count($data['flselect_account_manual']);
		}

		$htmlListAccount .= '<table id="fulltable">
								<thead>
									<tr>
										<th>Own Account </th>
										<th>Account Number</th>
										<th>Account Name</th>
										<th>Currency</th>
										<th>Nominal</th>
									</tr>
								</thead>
							<tbody id="tbody">';
		$arrOwn = array("0" => "T", "1" => "Y");
		for ($i = 0; $i < $jumlahAccount; $i++) {
			// if (!empty($data['flselect_account_number'])) {
			if ($data['flselect_own'][$i] == '1') {
				$accountNumber = $data['flselect_account_number'][$i];
				// get from service
				$serviceAccount = new Service_Account($data['flselect_account_number'][$i], null);
				$accountInfo = $serviceAccount->inquiryAccontInfo();
				$serviceCif = new Service_Account(null, null, null, null, null, $accountInfo["cif"]);
				$saveResult = $serviceCif->inquiryCIFAccount()["accounts"];
				$listAccount = array_search($data['flselect_account_number'][$i], array_column($saveResult, "account_number"));

				$accountName = $accountInfo["account_name"];
				$accountCurrency = $saveResult[$listAccount]["currency"];
				$amount = $data['flamount'][$i];
			} else {
				$accountNumber = $data['flselect_account_manual'][$i];
				$accountName = $data['flname_manual'][$i];
				$accountCurrency = "IDR";
				$amount = $data['flamount'][$i];
			}
			$htmlListAccount .= '<tr>
						<td>' . $arrOwn[$data['flselect_own'][$i]] . '</td>
						<td>' . $accountNumber . '</td>
						<td>' . $accountName . '</td>
						<td>' . $accountCurrency . '</td>
						<td>' . $amount . '</td>
					</tr>
					';
			$totalAmount += intval(str_replace(',', '', $amount));
		}

		$htmlListAccount .= '</tbody></table>';

		$this->view->listAccount = $htmlListAccount;
		$this->view->totalAmount = Application_Helper_General::displayMoney($totalAmount);
		// END LIST ACCOUNT PELEPASAN DANA

		// BEGIN SUBMIT PENUTUPAN BANK GARANSI
		if ($this->_request->isPost()) {
			$generateCloseRefNumber = $bgdata[0]['CLOSE_REF_NUMBER'] ?: $this->generateTransactionID("TK");

			while (true) {
				$exist = $this->_db->select()
					->from('TEMP_BANK_GUARANTEE_CLOSE')
					->where('CLOSE_REF_NUMBER = ?', $generateCloseRefNumber)
					->query()->fetch();

				$cekOnTBG = $this->_db->select()
					->from('T_BANK_GUARANTEE')
					->where('CLOSE_REF_NUMBER = ?', $generateCloseRefNumber)
					->query()->fetch();

				$cekOnSlip = $this->_db->select()
					->from('T_BG_PSLIP')
					->where('CLOSE_REF_NUMBER = ?', $generateCloseRefNumber)
					->query()->fetch();

				if (!$exist && !$cekOnTBG && !$cekOnSlip) break;

				$generateCloseRefNumber = $this->generateTransactionID("TK");
			}

			try {

				//$this->_db->beginTransaction();

				// CEK Principal memiliki workflow Persetujuan
				$select_cust_linefacility = $this->_db->select()
					->from('M_CUSTOMER')
					->where('CUST_ID = ?', $getBG['CUST_ID']);

				$getCustomer = $this->_db->fetchRow($select_cust_linefacility);

				if ($getCustomer['CUST_APPROVER'] == '1') {
					$SUGGESTION_STATUS = '2';
				} else {
					$SUGGESTION_STATUS = '3';
				}

				$this->_db->insert('TEMP_BANK_GUARANTEE_CLOSE', [
					"CLOSE_REF_NUMBER" => $generateCloseRefNumber,
					"BG_NUMBER" => $getBG['BG_NUMBER'],
					"CHANGE_TYPE" => 5,
					"SUGGESTION_STATUS" => $SUGGESTION_STATUS,
					"LASTUPDATED" => new Zend_Db_Expr("now()"),
					"LASTUPDATEDFROM" => "PRINCIPAL",
					"LASTUPDATEDBY" => $this->_userIdLogin,
				]);


				$this->_db->insert('TEMP_BANK_GUARANTEE_FILE_CLOSE', [
					"CLOSE_REF_NUMBER" => $generateCloseRefNumber,
					"FILE_NOTE" => 'Surat Permohonan Penutupan',
					"BG_FILE" => $data['lampiran'][0],
				]);

				$this->_db->insert('TEMP_BANK_GUARANTEE_FILE_CLOSE', [
					"CLOSE_REF_NUMBER" => $generateCloseRefNumber,
					"FILE_NOTE" => 'Surat Pernyataan Pekerjaan Selesai',
					"BG_FILE" => $data['lampiran'][1],
				]);

				$this->_db->insert('TEMP_BANK_GUARANTEE_FILE_CLOSE', [
					"CLOSE_REF_NUMBER" => $generateCloseRefNumber,
					"FILE_NOTE" => 'BAST Pekerjaan',
					"BG_FILE" => $data['lampiran'][2],
				]);


				for ($i = 0; $i < $jumlahAccount; $i++) {
					if ($data['flselect_own'][$i] == '1') {
						// if (!empty($data['flselect_account_number'])) {
						$accountNumber = $data['flselect_account_number'][$i];
						// get from service
						$serviceAccount = new Service_Account($data['flselect_account_number'][$i], null);
						$accountInfo = $serviceAccount->inquiryAccontInfo();
						$serviceCif = new Service_Account(null, null, null, null, null, $accountInfo["cif"]);
						$saveResult = $serviceCif->inquiryCIFAccount()["accounts"];
						$listAccount = array_search($data['flselect_account_number'][$i], array_column($saveResult, "account_number"));

						$accountName = $accountInfo["account_name"];
						$accountCurrency = $saveResult[$listAccount]["currency"];
						$amount = $data['flamount'][$i];
					} else {
						$accountNumber = $data['flselect_account_manual'][$i];
						$accountName = $data['flname_manual'][$i];
						$accountCurrency = "IDR";
						$amount = $data['flamount'][$i];
					}

					$this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', [
						"CLOSE_REF_NUMBER" => $generateCloseRefNumber,
						"ACCT_INDEX" => $i + 1,
						"PS_FIELDNAME" => 'Beneficiary is own account',
						"PS_FIELDVALUE" => $arrOwn[$data['flselect_own'][$i]],
					]);


					$this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', [
						"CLOSE_REF_NUMBER" => $generateCloseRefNumber,
						"ACCT_INDEX" => $i + 1,
						"PS_FIELDNAME" => 'Beneficiary Account CCY',
						"PS_FIELDVALUE" => $accountCurrency,
					]);

					$this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', [
						"CLOSE_REF_NUMBER" => $generateCloseRefNumber,
						"ACCT_INDEX" => $i + 1,
						"PS_FIELDNAME" => 'Beneficiary Account Number',
						"PS_FIELDVALUE" => $accountNumber,
					]);

					$this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', [
						"CLOSE_REF_NUMBER" => $generateCloseRefNumber,
						"ACCT_INDEX" => $i + 1,
						"PS_FIELDNAME" => 'Beneficiary Account Name',
						"PS_FIELDVALUE" => $accountName,
					]);

					$this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', [
						"CLOSE_REF_NUMBER" => $generateCloseRefNumber,
						"ACCT_INDEX" => $i + 1,
						"PS_FIELDNAME" => 'Transaction CCY',
						"PS_FIELDVALUE" => $accountCurrency,
					]);

					$this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', [
						"CLOSE_REF_NUMBER" => $generateCloseRefNumber,
						"ACCT_INDEX" => $i + 1,
						"PS_FIELDNAME" => 'Transaction Amount',
						"PS_FIELDVALUE" => str_replace('.00', '', Application_Helper_General::convertDisplayMoney($amount)),
					]);
				}

				$BG_REASON = "NoRef Pengajuan : " . $generateCloseRefNumber;
				$this->_db->insert('T_BANK_GUARANTEE_HISTORY_CLOSE', [
					"CLOSE_REF_NUMBER" => $generateCloseRefNumber,
					"BG_NUMBER" => $getBG['BG_NUMBER'],
					"CUST_ID" => $getBG['CUST_ID'],
					"DATE_TIME" => new Zend_Db_Expr("now()"),
					"USER_FROM" => "PRINCIPAL",
					"USER_LOGIN" => $this->_userIdLogin,
					"BG_REASON" => $BG_REASON,
					"HISTORY_STATUS" => 1,
				]);

				//$this->_db->commit();

				Application_Helper_General::writeLog('PPMD', "Pengajuan Pelepasan MD BG No. " . $getBG['BG_NUMBER'] . ", Subjek : " . $getBG['BG_SUBJECT'] . ", NoRef Pengajuan : " . $generateCloseRefNumber);

				unset($sessionNamespace->content);
				$this->setbackURL('/' . $this->_request->getModuleName());
				$this->_redirect('/notification/success');
			} catch (Exception $e) {
				$this->_db->rollBack();
				//Application_Log_GeneralLog::technicalLog($e);
			}
		}
		// END SUBMIT PENUTUPAN BANK GARANSI


	}


	public function generateTransactionID($kode)
	{

		$currentDate = date("ymd");
		$length = 4;
		$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[random_int(0, $charactersLength - 1)];
		}

		$trxId = $kode . $currentDate . $randomString;

		return $trxId;
	}

	public function inquirymarginaldepositAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$regNumber = $this->_getParam('reqNumber');


		$optHtml = '<table id="guarantedTransactions">
		<thead>
                                <th>Hold Seq</th>
                                <th>Nomor Rekening</th>
                                <th>Nama Rekening</th>
                                <th>Tipe</th>
                                <th>Valuta</th>
								<th>Nominal</th>
                            </thead>
                            <tbody>';

		$bgdatasplit = $this->_db->select()
			->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
			->joinLeft(['B' => 'M_CUSTOMER_ACCT'], 'A.ACCT = B.ACCT_NO AND B.CUST_ID = ' . $this->_db->quote($this->_custIdLogin) . '', ["M_ACCT_DESC" => "B.ACCT_DESC", "M_ACCT_TYPE" => "B.ACCT_TYPE", 'CUST_ID', 'CCY_ID'])
			->where('A.BG_REG_NUMBER = ?', $regNumber)
			->query()->fetchAll();

		$total = 0;
		$totalEscrow = 0;

		if (!empty($bgdatasplit)) {
			foreach ($bgdatasplit as $key => $row) {
				if (strtolower($row["M_ACCT_DESC"]) == "giro" || $row["M_ACCT_TYPE"] == "D" || strtolower($row["M_ACCT_TYPE"]) == "giro" || strtolower($row["ACCT_DESC"]) == "giro" || strtolower($row["ACCT_TYPE"]) == "giro") continue;
				// GET SERVICE 
				$serviceAccount = new Service_Account($row['ACCT'], null);
				$accountInfo = $serviceAccount->inquiryAccontInfo();
				$serviceCif = new Service_Account(null, null, null, null, null, $accountInfo["cif"]);
				$saveResult = $serviceCif->inquiryCIFAccount()["accounts"];
				$listAccount = array_search($row['ACCT'], array_column($saveResult, "account_number"));

				$optHtml .= '<tr>
						<td>' . $row['HOLD_SEQUENCE'] . '</td>
						<td>' . $row['ACCT'] . '</td>
						<td>' . $row['NAME'] . '</td>
						<td>' . $saveResult[$listAccount]["type_desc"] . " " . $saveResult[$listAccount]["product_type"] . '</td>
						<td>' . $saveResult[$listAccount]["currency"] . '</td>
						<td>' . Application_Helper_General::displayMoney($row['AMOUNT']) . '</td>
					</tr>
					';

				$total += $row['AMOUNT'];
				if (strtolower($row['ACCT_DESC']) == 'escrow') $totalEscrow += $row['AMOUNT'];
			}
		} else {
			$optHtml .= '<tr><td colspan="7" class="tbl-evencontent" align="center">--- ' . $this->language->_('No Data') . ' ---</td></tr>';
		}

		$optHtml .= '<input type="hidden" value="' . $total . '" id="total_amount_md">';
		$optHtml .= '<input type="hidden" value="' . $totalEscrow . '" id="total_escrow">';
		$optHtml .= '</tbody></table>';

		echo $optHtml;
	}


	public function inquirydepositoAction()
	{

		$app = Zend_Registry::get('config');
		$app = $app['app']['bankcode'];

		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$acct_no = $this->_getParam('acct_no');
		//echo $acct_no;

		$svcAccount = new Service_Account($acct_no, Application_Helper_General::getCurrNum('IDR'), $app, '1');
		$result = $svcAccount->inquiryDeposito('AB', TRUE);
		//var_dump($result);

		$data['data'] = false;
		is_array($result) ? $result :  $result = $data;
		echo json_encode($result);
	}

	public function inquiryaccountbalanceAction()
	{

		$app = Zend_Registry::get('config');
		$app = $app['app']['bankcode'];

		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$acct_no = $this->_getParam('acct_no');
		//echo $acct_no;

		$svcAccount = new Service_Account($acct_no, Application_Helper_General::getCurrNum('IDR'), $app, '1');
		$result = $svcAccount->inquiryAccountBalance('AB', TRUE);
		//var_dump($result);
		//die();
		$data['data'] = false;
		is_array($result) ? $result :  $result = $data;
		echo json_encode($result);
	}

	public function inquiryaccountinfoAction()
	{

		//integrate ke core
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();
		$acct_no = $this->_getParam('acct_no');
		$acct_name = $this->_getParam('acct_name');
		$svcAccount = new Service_Account($acct_no, null);
		$result = $svcAccount->inquiryAccontInfo('AI', TRUE);

		if ($result['response_desc'] == 'Success') //00 = success
		{
			$svcAccountCIF = new Service_Account($result['account_number'], null, null, null, null, $result['cif']);
			$result2 = $svcAccountCIF->inquiryCIFAccount();

			$filterBy = $result['account_number']; // or Finance etc.
			$new = array_filter($result2['accounts'], function ($var) use ($filterBy) {
				return ($var['account_number'] == $filterBy);
			});

			$singleArr = array();
			foreach ($new as $key => $val) {
				$singleArr = $val;
			}
		} else {
			header('Content-Type: application/json; charset=utf-8');
			echo json_encode($result);
			return 0;
		}

		if ($singleArr['type_desc'] == 'Deposito') {
			$svcAccountDeposito = new Service_Account($result['account_number'], null, null, null, null, null);
			$result3 = $svcAccountDeposito->inquiryDeposito();

			if ($result3["response_code"] != "0000") {
				header('Content-Type: application/json; charset=utf-8');
				echo json_encode(array_merge($result3, $singleArr, $result));
				return 0;
			}

			if ($result3["status"] != 1 && $result3["status"] != 4) {
				header('Content-Type: application/json; charset=utf-8');
				echo json_encode(array_merge($result3, $singleArr, $result));
				return 0;
			}

			$sqlRekeningJaminanExist = $this->_db->select()
				->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
				->where('A.ACCT = ?', $result['account_number'])
				->query()->fetchAll();

			if (count($sqlRekeningJaminanExist) <= 1) {
				$result3["check_exist_split"] = 0;
				// $result3["check_exist_split"] = (!empty($sqlRekeningJaminanExist)) ? 1 : 0;
			} else {
				$result3["check_exist_split"] = 1;
			}

			$get_product_type = current($new)["type"];

			$check_prod_type = $this->_db->select()
				->from("M_PRODUCT_TYPE")
				->where("PRODUCT_CODE = ?", $result3["account_type"])
				->query()->fetch();

			$result3["check_product_type"] = 1;
			if (!empty($check_prod_type)) {
				$result3["check_product_type"] = 1;
			}
		} else {
			$svcAccountBalance = new Service_Account($result['account_number'], null, null, null, null, null);
			$result3 = $svcAccountBalance->inquiryAccountBalance();

			if ($result3["status"] != 1 && $result3["status"] != 4) {
				header('Content-Type: application/json; charset=utf-8');
				echo json_encode($result3);
				return 0;
			}

			$get_product_type = current($new)["type"];
			$check_prod_type = $this->_db->select()
				->from("M_PRODUCT_TYPE")
				->where("PRODUCT_CODE = ?", $result3["account_type"])
				->query()->fetch();

			$result3["check_product_type"] = 1;
			if (!empty($check_prod_type)) {
				$result3["check_product_type"] = 1;
			}
		}

		$return = array_merge($result, $singleArr, $result3);

		$data['data'] = false;

		$return['not_match'] = false;

		if (strtolower($acct_name) != strtolower($return['account_name'])) {
			$return = array_diff_key($return, ['account_name' => '']);
			$return['not_match'] = true;
		}

		is_array($return) ? $return :  $return = $data;

		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($return);
	}

	private function getDetailInfoAcct($acctnumber)
	{
		$callService = new Service_Account($acctnumber, '');
		$resultAcctInfo = $callService->inquiryAccontInfo();

		$getCif = $resultAcctInfo['cif'];

		$callServiceCif = new Service_Account('', '', '', '', '', $getCif);
		$resultCifAccount = $callServiceCif->inquiryCIFAccount();

		$filterAcct = array_shift(array_filter($resultCifAccount['accounts'], function ($item) use ($acctnumber) {
			return ltrim($item['account_number'], '0') == ltrim($acctnumber, '0');
		}));

		return array_merge($resultAcctInfo, $filterAcct);
	}
}
