<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'SGO/Extendedmodule/phpqrcode/qrlib.php';
require_once("Service/Account.php");

class eform_BgclosingController extends Application_Main
{

	protected $_moduleDB = 'RTF'; // masih harus diganti

	public function indexAction()
	{

		$this->_helper->_layout->setLayout('newlayout');
		$settings = new Settings();
		// BEGIN GLOBAL VARIABEL
		$config = Zend_Registry::get('config');
		$bgpublishType  = $config["bgpublish"]["type"]["desc"];
		$bgpublishCode  = $config["bgpublish"]["type"]["code"];
		$arrbgpublish   = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

		$bgcgType   = $config["bgcg"]["type"]["desc"];
		$bgcgCode   = $config["bgcg"]["type"]["code"];
		$arrbgcg 	= array_combine(array_values($bgcgCode), array_values($bgcgType));

		// END GLOBAL VARIABEL

		// BEGIN PENUTUPAN BANK GARANSI
		// BEGIN DETAIL BANK GARANSI
		$bgparam = $this->_getParam('bgnumb');
		// fixing 27 april 2023
		$this->view->bgparam = $bgparam;
		// fixing 27 april 2023
		$AESMYSQL = new Crypt_AESMYSQL();
		$decryption = urldecode($bgparam);
		$numb = $AESMYSQL->decrypt($decryption, $password);
		if (!empty($numb)) {
			$bgdata = $this->_db->select()
				->from(array('A' => 'T_BANK_GUARANTEE'), array(
					'*',
					'INS_NAME' => new Zend_Db_Expr('(SELECT MC.CUST_NAME FROM M_CUSTOMER MC WHERE MC.CUST_ID = A.BG_INSURANCE_CODE LIMIT 1)')
				))
				->joinleft(array('B' => 'M_CUSTOMER'), 'A.CUST_ID = B.CUST_ID', array('B.CUST_NAME'))
				->joinleft(array('C' => 'M_BRANCH'), 'A.BG_BRANCH = C.BRANCH_CODE', array('C.BRANCH_NAME'))
				->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
				->where('A.BG_REG_NUMBER = ?', $numb)
				->query()->fetchAll();

			if (!empty($bgdata)) {
				$getBG = $bgdata['0'];

				if ($getBG['BG_NUMBER'] == '') {
					$getBG['BG_NUMBER'] = '-';
				}

				if ($getBG['BG_SUBJECT'] == '') {
					$getBG['BG_SUBJECT'] = '- no subject -';
				}

				$this->view->getBG = $getBG;

				$this->view->publishDesc = $arrbgpublish[$getBG['BG_PUBLISH']];
				if ($getBG['BG_PUBLISH'] == 1) {
					$getPaperPrint = $this->_db->select()
						->from('M_PAPER')
						->where('notes = ?', $getBG['BG_NUMBER'])
						->where('STATUS = 1')
						->query()->fetchAll();

					$totalKertas = count($getPaperPrint);

					$this->view->nomorRefKertas = ' - Nomor Ref Kertas : ' . $getPaperPrint[0]['PAPER_ID'] . ' - ' . $getPaperPrint[$totalKertas - 1]['PAPER_ID'] . ' (' . $totalKertas . ' lembar)';
				}

				// BEGIN NILAI BANK GARANSI

				$bgdetaildata = $this->_db->select()
					->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
					->where('A.BG_REG_NUMBER = ?', $getBG["BG_REG_NUMBER"])
					->query()->fetchAll();


				if (!empty($bgdetaildata)) {
					foreach ($bgdetaildata as $key => $value) {
						if ($value['PS_FIELDNAME'] == 'Currency') {
							$this->view->bgCurrency =   $value['PS_FIELDVALUE'];
						}

						if ($getBG['COUNTER_WARRANTY_TYPE'] == '3') {

							if ($value['PS_FIELDNAME'] == 'Insurance Name') {
								$this->view->insuraceName =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Insurance Branch') {

								$insuranceBranch = $this->_db->select()
									->from("M_INS_BRANCH")
									->where("INS_BRANCH_CODE = ?", $value['PS_FIELDVALUE'])
									->query()->fetchAll();

								$this->view->insuranceBranch = $insuranceBranch[0]["INS_BRANCH_NAME"];
							}
						}
					}
				}

				$this->view->BG_AMOUNT  = $getBG['BG_AMOUNT'];
				$this->view->updateStart = Application_Helper_General::convertDate($getBG['TIME_PERIOD_START'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->updateEnd = Application_Helper_General::convertDate($getBG['TIME_PERIOD_END'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->bgClaimDate = Application_Helper_General::convertDate($getBG['BG_CLAIM_DATE'], $this->view->viewDateFormat, $this->view->defaultDateFormat);

				// END NILAI BANK GARANSI

				// BEGIN KONTRA GARANSI
				$this->view->counterWarrantyType = $getBG['COUNTER_WARRANTY_TYPE'];
				$this->view->counterWarrantyDesc = $arrbgcg[$getBG['COUNTER_WARRANTY_TYPE']];

				// END KONTRA GARANSI

				// BEGIN MARGINAL DEPOSIT PRINCIPAL
				$saveHoldAmount = null;
				$saveEscrowAmount = null;

				$bgdatasplit = $this->_db->select()
					->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
					->joinLeft(['B' => 'M_CUSTOMER_ACCT'], 'A.ACCT = B.ACCT_NO AND B.CUST_ID = ' . $this->_db->quote($this->_custIdLogin) . '', ["M_ACCT_DESC" => "B.ACCT_DESC", "M_ACCT_TYPE" => "B.ACCT_TYPE", 'CUST_ID', 'CCY_ID'])
					->where('A.BG_REG_NUMBER = ?', $getBG['BG_REG_NUMBER'])
					->query()->fetchAll();

				// fixing 26 april 2023
				if (count($bgdatasplit) > 1) {
					foreach ($bgdatasplit as $key => $value) {
						if (strtolower($value["M_ACCT_DESC"]) == "giro" || $value["M_ACCT_TYPE"] == "D") {
							continue;
						};
						$serviceAccount = new Service_Account($value['ACCT'], null);
						$accountInfo = $serviceAccount->inquiryAccontInfo();
						$serviceCif = new Service_Account(null, null, null, null, null, $accountInfo["cif"]);
						$saveResult = $serviceCif->inquiryCIFAccount()["accounts"];
						$listAccount = array_search($value['ACCT'], array_column($saveResult, "account_number"));

						if ($saveResult[$listAccount]["product_type"] == 'J1') {
							continue;
						} else {
							$saveHoldAmount += floatval($value["AMOUNT"]);
						}
						// if ($value['CUST_ID'] != $getBG['CUST_ID']) continue;
						// if ($value["ACCT_DESC"]) continue;


						if (strtolower($value["ACCT_DESC"] == 'Escrow')) $saveEscrowAmount += floatval($value["AMOUNT"]);
					}
				} else {
					$saveHoldAmount += floatval($bgdatasplit[0]['AMOUNT']);
				}
				// fixing 26 april 2023

				// fixing 27 april 2023
				$toc_ind = $settings->getSetting('ftemplate_bg_ind');
				$this->view->toc_ind = $toc_ind;
				$toc_eng = $settings->getSetting('ftemplate_bg_eng');
				$this->view->toc_eng = $toc_eng;
				// fixing 27 april 2023
				
				$allSetting = $settings->getAllSetting();
				$this->view->snkPenutupan = $allSetting['ftemplate_bgclosing_ind'];

				$this->view->holdAmount = $saveHoldAmount;
				$this->view->escrowAmount = $saveEscrowAmount;
				// END MARGINAL DEPOSIT PRINCIPAL

				// BEGIN PELEPASAN DANA
				$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

				$type = array('D', 'S', 'T', '10', '20', '30');
				$param = array('ACCT_TYPE' => $type);
				$AccArr = $CustomerUser->getAccountsBG($param);
				$getAllProduct = $this->_db->select()
					->from("M_PRODUCT")
					->query()->fetchAll();

				$newArr2 = [];
				foreach ($AccArr as $keyArr => $arrAcct) {

					$accountTypeCheck = false;
					$productTypeCheck = false;

					$svcAccount = new Service_Account($arrAcct['ACCT_NO'], null, null, null, null, null);
					$result = $svcAccount->inquiryAccountBalance();
					if ($result['response_code'] == '0000') {
						$accountType = $result['account_type'];
						$productType = $result['product_type'];
					} else {
						$result = $svcAccount->inquiryDeposito();

						$accountType = $result['account_type'];
						$productType = $result['product_type'];
					}

					foreach ($getAllProduct as $key => $value) {
						# code...
						if ($value['PRODUCT_CODE'] == $accountType) {
							$accountTypeCheck = true;
						};

						if ($value['PRODUCT_PLAN'] == $productType) {
							$productTypeCheck = true;
						};
						$productTypeCheck = true;
					}

					if ($accountTypeCheck && $productTypeCheck) $newArr2[] = $arrAcct;
				}

				$this->view->AccArr = $newArr2;
				// END PELEPASAN DANA

			}
		}
		// END DETAIL BANK GARANSI

		// END PENUTUPAN BANK GARANSI

		// BEGIN CONFIRM PAGE
		if ($this->_request->isPost()) {
			$datasession = $this->_request->getParams();
			$attachmentDestination   = UPLOAD_PATH . '/document/closing/';
			//$adapter                = new Zend_File_Transfer_Adapter_Http();
			//$files 					= $adapter->getFileInfo();
			//var_dump($files);
			$files = $_FILES;
			$jumlahFile = count($files['uploadSource']['name']);

			for ($i = 0; $i < $jumlahFile; $i++) {
				$namaFile = $files['uploadSource']['name'][$i];
				$lokasiTmp = $files['uploadSource']['tmp_name'][$i];

				# kita tambahkan uniqid() agar nama gambar bersifat unik
				$date = date("dmy");
				$time = date("his");
				$namaBaru = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . uniqid() . '_' . $namaFile;

				$prosesUpload = move_uploaded_file($lokasiTmp, $attachmentDestination . $namaBaru);
				$datasession['lampiran'][$i] = $namaBaru;
			}

			$sessionNamespace = new Zend_Session_Namespace('bgclosing');
			$sessionNamespace->content = $datasession;

			$this->_redirect('/eform/bgclosing/confirm');
		}
		// END CONFIRM PAGE


	}

	public function confirmAction()
	{

		$this->_helper->_layout->setLayout('newlayout');
		$sessionNamespace = new Zend_Session_Namespace('bgclosing');
		$data = $sessionNamespace->content;

		$this->_helper->_layout->setLayout('newlayout');
		$settings = new Settings();
		// BEGIN GLOBAL VARIABEL
		$config = Zend_Registry::get('config');
		$bgpublishType  = $config["bgpublish"]["type"]["desc"];
		$bgpublishCode  = $config["bgpublish"]["type"]["code"];
		$arrbgpublish   = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

		$bgcgType   = $config["bgcg"]["type"]["desc"];
		$bgcgCode   = $config["bgcg"]["type"]["code"];
		$arrbgcg 	= array_combine(array_values($bgcgCode), array_values($bgcgType));

		// END GLOBAL VARIABEL

		// BEGIN PENUTUPAN BANK GARANSI
		// BEGIN DETAIL BANK GARANSI
		$bgparam = $data['bgnumb'];
		// fixing 26 april 2023
		$this->view->bgparam = $bgparam;
		// fixing 26 april 2023
		$AESMYSQL = new Crypt_AESMYSQL();
		$decryption = urldecode($bgparam);
		$numb = $AESMYSQL->decrypt($decryption, $password);
		if (!empty($numb)) {
			$bgdata = $this->_db->select()
				->from(array('A' => 'T_BANK_GUARANTEE'), array(
					'*',
					'INS_NAME' => new Zend_Db_Expr('(SELECT MC.CUST_NAME FROM M_CUSTOMER MC WHERE MC.CUST_ID = A.BG_INSURANCE_CODE LIMIT 1)')
				))
				->joinleft(array('B' => 'M_CUSTOMER'), 'A.CUST_ID = B.CUST_ID', array('B.CUST_NAME'))
				->joinleft(array('C' => 'M_BRANCH'), 'A.BG_BRANCH = C.BRANCH_CODE', array('C.BRANCH_NAME'))
				->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
				->where('A.BG_REG_NUMBER = ?', $numb)
				->query()->fetchAll();


			if (!empty($bgdata)) {
				$getBG = $bgdata['0'];

				if ($getBG['BG_NUMBER'] == '') {
					$getBG['BG_NUMBER'] = '-';
				}

				if ($getBG['BG_SUBJECT'] == '') {
					$getBG['BG_SUBJECT'] = '- no subject -';
				}

				$this->view->getBG = $getBG;

				$this->view->publishDesc = $arrbgpublish[$getBG['BG_PUBLISH']];
				if ($getBG['BG_PUBLISH'] == 1) {
					$getPaperPrint = $this->_db->select()
						->from('M_PAPER')
						->where('notes = ?', $getBG['BG_NUMBER'])
						->where('STATUS = 1')
						->query()->fetchAll();

					$totalKertas = count($getPaperPrint);

					$this->view->nomorRefKertas = ' - Nomor Ref Kertas : ' . $getPaperPrint[0]['PAPER_ID'] . ' - ' . $getPaperPrint[$totalKertas - 1]['PAPER_ID'] . ' (' . $totalKertas . ' lembar)';
				}

				// BEGIN NILAI BANK GARANSI

				$bgdetaildata = $this->_db->select()
					->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
					->where('A.BG_REG_NUMBER = ?', $getBG["BG_REG_NUMBER"])
					->query()->fetchAll();


				if (!empty($bgdetaildata)) {
					foreach ($bgdetaildata as $key => $value) {
						if ($value['PS_FIELDNAME'] == 'Currency') {
							$this->view->bgCurrency =   $value['PS_FIELDVALUE'];
						}

						if ($getBG['COUNTER_WARRANTY_TYPE'] == '3') {

							if ($value['PS_FIELDNAME'] == 'Insurance Name') {
								$this->view->insuraceName =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Insurance Branch') {

								$insuranceBranch = $this->_db->select()
									->from("M_INS_BRANCH")
									->where("INS_BRANCH_CODE = ?", $value['PS_FIELDVALUE'])
									->query()->fetchAll();

								$this->view->insuranceBranch = $insuranceBranch[0]["INS_BRANCH_NAME"];
							}
						}
					}
				}

				$this->view->BG_AMOUNT  = $getBG['BG_AMOUNT'];
				$this->view->updateStart = Application_Helper_General::convertDate($getBG['TIME_PERIOD_START'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->updateEnd = Application_Helper_General::convertDate($getBG['TIME_PERIOD_END'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->bgClaimDate = Application_Helper_General::convertDate($getBG['BG_CLAIM_DATE'], $this->view->viewDateFormat, $this->view->defaultDateFormat);

				// END NILAI BANK GARANSI

				// BEGIN KONTRA GARANSI
				$this->view->counterWarrantyType = $getBG['COUNTER_WARRANTY_TYPE'];
				$this->view->counterWarrantyDesc = $arrbgcg[$getBG['COUNTER_WARRANTY_TYPE']];

				// END KONTRA GARANSI

				// BEGIN MARGINAL DEPOSIT PRINCIPAL
				$saveHoldAmount = 0;

				$bgdatasplit = $this->_db->select()
					->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
					->joinLeft(['B' => 'M_CUSTOMER_ACCT'], 'A.ACCT = B.ACCT_NO AND B.CUST_ID = ' . $this->_db->quote($this->_custIdLogin) . '', ["M_ACCT_DESC" => "B.ACCT_DESC", "M_ACCT_TYPE" => "B.ACCT_TYPE", 'CUST_ID', 'CCY_ID'])
					->where('A.BG_REG_NUMBER = ?', $getBG['BG_REG_NUMBER'])
					->query()->fetchAll();

				// GET ESCROW
				$this->view->getEscrow = array_filter(array_column($bgdatasplit, 'ACCT_DESC'), function ($item) {
					return strtolower($item) == 'escrow';
				});

				foreach ($bgdatasplit as $key => $value) {
					// if ($value['CUST_ID'] != $getBG['CUST_ID']) continue;
					// if (strtolower($value["ACCT_DESC"]) == 'escrow') continue;
					if (strtolower($value["M_ACCT_DESC"]) == "giro" || $value["M_ACCT_TYPE"] == "D") {
						continue;
					};

					$serviceAccount = new Service_Account($value['ACCT'], null);
					$accountInfo = $serviceAccount->inquiryAccontInfo();
					$serviceCif = new Service_Account(null, null, null, null, null, $accountInfo["cif"]);
					$saveResult = $serviceCif->inquiryCIFAccount()["accounts"];
					$listAccount = array_search($value['ACCT'], array_column($saveResult, "account_number"));

					if ($saveResult[$listAccount]["product_type"] == 'J1') {
						continue;
					} else {
						$saveHoldAmount += floatval($value["AMOUNT"]);
					}
				}

				$this->view->holdAmount = $saveHoldAmount;
				// END MARGINAL DEPOSIT PRINCIPAL

				// BEGIN PELEPASAN DANA
				$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

				$type = array('D', 'T', '20', '30', 'S', '10');
				$param = array('ACCT_TYPE' => $type);
				$AccArr = $CustomerUser->getAccountsBG($param);
				$getAllProduct = $this->_db->select()
					->from("M_PRODUCT")
					->query()->fetchAll();

				$newArr2 = [];
				foreach ($AccArr as $keyArr => $arrAcct) {

					$accountTypeCheck = false;
					$productTypeCheck = false;

					$svcAccount = new Service_Account($arrAcct['ACCT_NO'], null, null, null, null, null);
					$result = $svcAccount->inquiryAccountBalance();
					if ($result['response_code'] == '0000') {
						$accountType = $result['account_type'];
						$productType = $result['product_type'];
					} else {
						$result = $svcAccount->inquiryDeposito();

						$accountType = $result['account_type'];
						$productType = $result['product_type'];
					}

					foreach ($getAllProduct as $key => $value) {
						# code...
						if ($value['PRODUCT_CODE'] == $accountType) {
							$accountTypeCheck = true;
						};

						if ($value['PRODUCT_PLAN'] == $productType) {
							$productTypeCheck = true;
						};
						$productTypeCheck = true;
					}

					if ($accountTypeCheck && $productTypeCheck) $newArr2[] = $arrAcct;
				}

				$this->view->AccArr = $newArr2;
				// END PELEPASAN DANA

			}
		}
		// END DETAIL BANK GARANSI

		// END PENUTUPAN BANK GARANSI

		// BEGIN LAMPIRAN DOKUMEN

		$htmlLampiran .= '<div class="col-sm-3">
                                <label for="" class="col-form-label">Surat Permohonan Penutupan</label>
                            </div>
                            <div class="col-sm-9">
							' . $data['lampiran'][0] . '
							</div>';

		$htmlLampiran .= '<div class="col-sm-3">
                                <label for="" class="col-form-label">Surat Pernyataan Pekerjaan Selesai</label>
                            </div>
                            <div class="col-sm-9">
							' . $data['lampiran'][1] . '
							</div>';

		$htmlLampiran .= '<div class="col-sm-3">
                                <label for="" class="col-form-label">BAST Pekerjaan</label>
                            </div>
                            <div class="col-sm-9">
							' . $data['lampiran'][2] . '
							</div>';

		$this->view->lampiranDokumen = $htmlLampiran;
		// END LAMPIRAN DOKUMEN

		// BEGIN LIST ACCOUNT PELEPASAN DANA

		if (!empty($data['flselect_account_number'])) {
			$jumlahAccount = count($data['flselect_account_number']);
		} else {
			$jumlahAccount = count($data['flselect_account_manual']);
		}
		$totalAmount = null;
		$htmlListAccount = '<table id="fulltable">
								<thead>
									<tr>
										<th>Own Account </th>
										<th>Account Number</th>
										<th>Account Name</th>
										<th>Currency</th>
										<th>Nominal</th>
									</tr>
								</thead>
							<tbody id="tbody">';
		$arrOwn = array("0" => "T", "1" => "Y");
		for ($i = 0; $i < $jumlahAccount; $i++) {
			if (!empty($data['flselect_account_number'][$i])) {
				$accountNumber = $data['flselect_account_number'][$i];
				// get from service
				$serviceAccount = new Service_Account($data['flselect_account_number'][$i], null);
				$accountInfo = $serviceAccount->inquiryAccontInfo();
				$serviceCif = new Service_Account(null, null, null, null, null, $accountInfo["cif"]);
				$saveResult = $serviceCif->inquiryCIFAccount()["accounts"];
				$listAccount = array_search($data['flselect_account_number'][$i], array_column($saveResult, "account_number"));

				$accountName = $accountInfo["account_name"];
				$accountCurrency = $saveResult[$listAccount]["currency"];
				$amount = $data['flamount'][$i];
			} else {
				// echo '<pre>';
				// var_dump($data['flselect_account_manual'][$i]);
				$accountNumber = $data['flselect_account_manual'][$i];
				$accountName = $data['flname_manual'][$i];
				$accountCurrency = "IDR";
				$amount = $data['flamount'][$i];
			}
			$htmlListAccount .= '<tr>
						<td>' . $arrOwn[$data['flselect_own'][$i]] . '</td>
						<td>' . $accountNumber . '</td>
						<td>' . $accountName . '</td>
						<td>' . $accountCurrency . '</td>
						<td>' . $amount . '</td>
					</tr>
					';
			$totalAmount += intval(str_replace(',', '', $amount));
		}
		// die;


		$htmlListAccount .= '</tbody></table>';

		$this->view->listAccount = $htmlListAccount;
		$this->view->totalAmount = Application_Helper_General::displayMoney($totalAmount);
		// END LIST ACCOUNT PELEPASAN DANA

		// BEGIN SUBMIT PENUTUPAN BANK GARANSI
		if ($this->_request->isPost()) {
			$generateCloseRefNumber = $this->generateTransactionID("PP");

			while (true) {
				$exist = $this->_db->select()
					->from('TEMP_BANK_GUARANTEE_CLOSE')
					->where('CLOSE_REF_NUMBER = ?', $generateCloseRefNumber)
					->query()->fetch();

				if (!$exist) break;

				$generateCloseRefNumber = $this->generateTransactionID("PP");
			}

			try {

				//$this->_db->beginTransaction();

				// CEK Principal memiliki workflow Persetujuan
				$select_cust_linefacility = $this->_db->select()
					->from('M_CUSTOMER')
					->where('CUST_ID = ?', $getBG['CUST_ID']);

				$getCustomer = $this->_db->fetchRow($select_cust_linefacility);

				if ($getCustomer['CUST_APPROVER'] == '1') {
					$SUGGESTION_STATUS = '2';
				} else {
					$SUGGESTION_STATUS = '3';
				}

				$this->_db->insert('TEMP_BANK_GUARANTEE_CLOSE', [
					"CLOSE_REF_NUMBER" => $generateCloseRefNumber,
					"BG_NUMBER" => $getBG['BG_NUMBER'],
					"CHANGE_TYPE" => 1,
					"SUGGESTION_STATUS" => $SUGGESTION_STATUS,
					"LASTUPDATED" => new Zend_Db_Expr("now()"),
					"LASTUPDATEDFROM" => "PRINCIPAL",
					"LASTUPDATEDBY" => $this->_userIdLogin,
				]);


				$this->_db->insert('TEMP_BANK_GUARANTEE_FILE_CLOSE', [
					"CLOSE_REF_NUMBER" => $generateCloseRefNumber,
					"FILE_NOTE" => 'Surat Permohonan Penutupan',
					"BG_FILE" => $data['lampiran'][0],
				]);

				$this->_db->insert('TEMP_BANK_GUARANTEE_FILE_CLOSE', [
					"CLOSE_REF_NUMBER" => $generateCloseRefNumber,
					"FILE_NOTE" => 'Surat Pernyataan Pekerjaan Selesai',
					"BG_FILE" => $data['lampiran'][1],
				]);

				$this->_db->insert('TEMP_BANK_GUARANTEE_FILE_CLOSE', [
					"CLOSE_REF_NUMBER" => $generateCloseRefNumber,
					"FILE_NOTE" => 'BAST Pekerjaan',
					"BG_FILE" => $data['lampiran'][2],
				]);


				for ($i = 0; $i < $jumlahAccount; $i++) {

					if ($arrOwn[$data['flselect_own'][$i]] == "T") {
						$accountNumber = $data['flselect_account_manual'][$i];
						$accountName = $data['flname_manual'][$i];
						$accountCurrency = "IDR";
						$amount = $data['flamount'][$i];
					} else {
						$accountNumber = $data['flselect_account_number'][$i];
						// get from service
						$serviceAccount = new Service_Account($data['flselect_account_number'][$i], null);
						$accountInfo = $serviceAccount->inquiryAccontInfo();
						$serviceCif = new Service_Account(null, null, null, null, null, $accountInfo["cif"]);
						$saveResult = $serviceCif->inquiryCIFAccount()["accounts"];
						$listAccount = array_search($data['flselect_account_number'][$i], array_column($saveResult, "account_number"));

						$accountName = $accountInfo["account_name"];
						$accountCurrency = $saveResult[$listAccount]["currency"];
						$amount = $data['flamount'][$i];
					}

					/*if (!empty($data['flselect_account_number'])) {
						
					} else {
						
					}*/

					$this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', [
						"CLOSE_REF_NUMBER" => $generateCloseRefNumber,
						"ACCT_INDEX" => $i + 1,
						"PS_FIELDNAME" => 'Beneficiary is own account',
						"PS_FIELDVALUE" => $arrOwn[$data['flselect_own'][$i]],
					]);


					$this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', [
						"CLOSE_REF_NUMBER" => $generateCloseRefNumber,
						"ACCT_INDEX" => $i + 1,
						"PS_FIELDNAME" => 'Beneficiary Account CCY',
						"PS_FIELDVALUE" => $accountCurrency,
					]);

					$this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', [
						"CLOSE_REF_NUMBER" => $generateCloseRefNumber,
						"ACCT_INDEX" => $i + 1,
						"PS_FIELDNAME" => 'Beneficiary Account Number',
						"PS_FIELDVALUE" => $accountNumber,
					]);

					$this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', [
						"CLOSE_REF_NUMBER" => $generateCloseRefNumber,
						"ACCT_INDEX" => $i + 1,
						"PS_FIELDNAME" => 'Beneficiary Account Name',
						"PS_FIELDVALUE" => $accountName,
					]);

					$this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', [
						"CLOSE_REF_NUMBER" => $generateCloseRefNumber,
						"ACCT_INDEX" => $i + 1,
						"PS_FIELDNAME" => 'Transaction CCY',
						"PS_FIELDVALUE" => $accountCurrency,
					]);

					$this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL_CLOSE', [
						"CLOSE_REF_NUMBER" => $generateCloseRefNumber,
						"ACCT_INDEX" => $i + 1,
						"PS_FIELDNAME" => 'Transaction Amount',
						"PS_FIELDVALUE" => str_replace('.00', '', Application_Helper_General::convertDisplayMoney($amount)),
					]);
				}

				$BG_REASON = "NoRef Pengajuan : " . $generateCloseRefNumber;
				$this->_db->insert('T_BANK_GUARANTEE_HISTORY_CLOSE', [
					"CLOSE_REF_NUMBER" => $generateCloseRefNumber,
					"BG_NUMBER" => $getBG['BG_NUMBER'],
					"CUST_ID" => $getBG['CUST_ID'],
					"DATE_TIME" => new Zend_Db_Expr("now()"),
					"USER_FROM" => "PRINCIPAL",
					"USER_LOGIN" => $this->_userIdLogin,
					"BG_REASON" => $BG_REASON,
					"HISTORY_STATUS" => 1,
				]);

				//$this->_db->commit();

				Application_Helper_General::writeLog('PABG', "Pengajuan Penutupan BG No. " . $getBG['BG_NUMBER'] . ", Subjek : " . $getBG['BG_SUBJECT'] . ", NoRef Pengajuan : " . $generateCloseRefNumber);

				unset($sessionNamespace->content);
				$this->setbackURL('/' . $this->_request->getModuleName());
				$this->_redirect('/notification/success');
			} catch (Exception $e) {
				$this->_db->rollBack();
				//Application_Log_GeneralLog::technicalLog($e);
			}
		}
		// END SUBMIT PENUTUPAN BANK GARANSI


	}


	public function generateTransactionID($kode)
	{

		$currentDate = date("ymd");
		$length = 4;
		$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[random_int(0, $charactersLength - 1)];
		}

		$trxId = $kode . $currentDate . $randomString;

		return $trxId;
	}

	public function inquirymarginaldepositAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$regNumber = $this->_getParam('reqNumber');


		$optHtml = '<table id="guarantedTransactions" class="table table-sm table-striped">
		<thead>
                                <th>Hold Seq</th>
                                <th>Nomor Rekening</th>
                                <th>Nama Rekening</th>
                                <th>Tipe</th>
                                <th>Valuta</th>
								<th>Nominal</th>
                            </thead>
                            <tbody>';

		$bgdatasplit = $this->_db->select()
			->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
			->joinLeft(['B' => 'M_CUSTOMER_ACCT'], 'A.ACCT = B.ACCT_NO AND B.CUST_ID = ' . $this->_db->quote($this->_custIdLogin) . '', ["M_ACCT_DESC" => "B.ACCT_DESC", "M_ACCT_TYPE" => "B.ACCT_TYPE", 'CUST_ID', 'CCY_ID'])
			->where('A.BG_REG_NUMBER = ?', $regNumber)
			->query()->fetchAll();

		$haveEscrow = false;
		$haveAcct = false;

		if (!empty($bgdatasplit)) {
			$haveAcct = true;
			foreach ($bgdatasplit as $key => $row) {
				if (strtolower($row["M_ACCT_DESC"]) == "giro" || $row["M_ACCT_TYPE"] == "D") {
					$haveEscrow = true;
					continue;
				};
				// GET SERVICE 
				$serviceAccount = new Service_Account($row['ACCT'], null);
				$accountInfo = $serviceAccount->inquiryAccontInfo();
				$serviceCif = new Service_Account(null, null, null, null, null, $accountInfo["cif"]);
				$saveResult = $serviceCif->inquiryCIFAccount()["accounts"];
				$listAccount = array_search($row['ACCT'], array_column($saveResult, "account_number"));
				// fixing 27 april 2023
				$holdSeq = !empty($row['HOLD_SEQUENCE']) ? $row['HOLD_SEQUENCE'] : '-';
				// fixing 27 april 2023
				if ($saveResult[$listAccount]["product_type"] == 'J1') {
					continue;
				}
				$optHtml .= '<tr>
						<td>' . $holdSeq . '</td>
						<td>' . $row['ACCT'] . '</td>
						<td>' . $row['NAME'] . '</td>
						<td>' . $saveResult[$listAccount]["type_desc"] . " " . $saveResult[$listAccount]["product_type"] . '</td>
						<td>' . $saveResult[$listAccount]["currency"] . '</td>
						<td>' . Application_Helper_General::displayMoney($row['AMOUNT']) . '</td>
					</tr>
					';
			}
		} else {
			$optHtml .= '<tr><td colspan="7" class="tbl-evencontent" align="center">--- ' . $this->language->_('No Data') . ' ---</td></tr>';
		}

		$optHtml .= '</tbody></table>';

		$optHtml .= '<input type="hidden" id="haveescrow" value="' . $haveEscrow . '">';
		$optHtml .= '<input type="hidden" id="haveacct" value="' . $haveAcct . '">';

		echo $optHtml;
	}

	public function mdfcoldtAction()
	{
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);

		$bgRegNumber = $this->_request->getParam('reqNumber') ?: null;
		$buttonescrow = $this->_request->getParam('buttonescrow') ?: null;

		if ($bgRegNumber == null) {
			header('Content-Type: application/json');
			echo json_encode(['result' => 'nothing']);
			return true;
		}

		$getBgData = $this->_db->select()
			->from('TEMP_BANK_GUARANTEE', ['CUST_ID'])
			->where('BG_REG_NUMBER = ?', $bgRegNumber)
			->query()->fetch();

		if (!$getBgData) {
			$getBgData = $this->_db->select()
				->from('T_BANK_GUARANTEE', ['CUST_ID'])
				->where('BG_REG_NUMBER = ?', $bgRegNumber)
				->query()->fetch();
		}

		$bgdatasplit = $this->_db->select()
			->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
			->joinLeft(['MCA' => 'M_CUSTOMER_ACCT'], 'MCA.ACCT_NO = A.ACCT AND MCA.CUST_ID = "' . $getBgData['CUST_ID'] . '"', ['ACCT_TYPE_MCA' => 'MCA.ACCT_TYPE', 'CCY_ID_MCA' => 'MCA.CCY_ID', 'ACCT_DESC_MCA' => 'MCA.ACCT_DESC'])
			->where('A.BG_REG_NUMBER = ?', $bgRegNumber)
			->query()->fetchAll();


		$bgdatasplit = array_filter($bgdatasplit, function ($acct) {

			if (strtolower($acct['ACCT_DESC']) != 'giro' && (strtolower($acct['ACCT_TYPE_MCA']) != '20' || strtolower($acct['ACCT_TYPE_MCA']) != 'd') && strtolower($acct['ACCT_DESC_MCA']) != 'giro' && (strtolower($acct['ACCT_TYPE']) != 'giro'))
				return true;
		});

		$mdAmount = 0;

		$htmlResult = "
        <table id=\"marginalDepositFCTable\" class=\"tableFile\">
            <thead>
                <th>" . $this->language->_('Hold Seq') . "</th>
                <th>" . $this->language->_('Nomor Rekening') . "</th>
                <th>" . $this->language->_('Nama Rekening') . "</th>
                <th>" . $this->language->_('Tipe') . "</th>
                <th>" . $this->language->_('Valuta') . "</th>
                <th>" . $this->language->_('Nominal') . "</th>
            </thead>
            <tbody>
            ";

		foreach ($bgdatasplit as $key => $value) {

			$callService = new Service_Account($value['ACCT'], '');
			$cekAcctType = strtolower($value['ACCT_DESC']) ?: (strtolower($value['ACCT_DESC_MCA']) ?: ((strtolower($value['ACCT_TYPE_MCA']) == 'T' || strtolower($value['ACCT_TYPE_MCA']) == '30') ? 'deposito' : ''));

			if (strtolower($value['ACCT_DESC']) == 'escrow') {
				$inqbalance = $callService->inquiryAccountBalance();
				// if (intval($inqbalance['available_balance']) == 0) {
				//     // hapus data atau ??
				//     continue;
				// } else $mdAmount += floatval($value['AMOUNT']);
				$mdAmount += floatval($value['AMOUNT']);
			} else {
				// jika deposito
				$inqbalance = $callService->inquiryDeposito();
				// if (strtolower($cekAcctType) == 'deposito') {
				if ($inqbalance['response_code'] == '0000' && strtolower($inqbalance['account_type']) == 't') {

					// if (strtolower($inqbalance['hold_description']) == 'n') {
					//     // hapus data atau ??
					//     continue;
					// } else $mdAmount += $value['AMOUNT'];
					$mdAmount += $value['AMOUNT'];
				}
				// end jika deposito

				// jika tabungan
				else {
					$inqbalance = $callService->inqLockSaving();

					$cekSaving = array_shift(array_filter($inqbalance['lock'], function ($sequence) use ($value) {
						return strval($value['HOLD_SEQUENCE']) == strval($sequence['sequence_number']);
					}));

					// if (!$cekSaving) {
					//     // hapus data atau
					//     continue;
					// } else $mdAmount += $value['AMOUNT'];
					$mdAmount += $value['AMOUNT'];
				}
				// end jika tabungan
			}

			$htmlResult .= "
            <tr>
                <td>" . ($value['HOLD_SEQUENCE'] ?: '-') . "</td>
                <td>" . $value['ACCT'] . "</td>
                <td>" . $value['NAME'] . "</td>
                <td>" . ($value['ACCT_DESC'] ?: $value['ACCT_TYPE'] ?: $value['ACCT_DESC_MCA'])  . "</td>
                <td>" . ((strtolower($value['ACCT_DESC']) == 'escrow') ? 'IDR' : ($value['CCY_ID'] ?: $value['CCY_ID_MCA'])) . "</td>
                <td align=\"center\">" . number_format($value['AMOUNT'], 2) . (strtolower($value['ACCT_DESC']) == 'escrow' && $buttonescrow ? '<button type="button" class="btnwhite ml-1" style="min-width: 20px !important;height: 20px !important;text-transform: inherit;color: #8282E6 !important" onclick="openwindow(`/eform/index/escrowdetail/bgnumb/${window.location.href.split(\'/\')[7]}`, 600, 400)">i</button>' : "") . "</td>
            </tr>";
		}

		if (!$bgdatasplit) {
			$htmlResult .= "
            <tr>
                <td colspan='6' class='text-center'>" . $this->language->_('Tidak ada marginal deposit') . "</td>
            </tr>";
		}

		$htmlResult .= "</tbody></table>";
		$htmlResult .= "<input type='hidden' value='" . $mdAmount . "' id='mdfceksis'></input>";

		echo $htmlResult;
	}


	public function inquirydepositoAction()
	{

		$app = Zend_Registry::get('config');
		$app = $app['app']['bankcode'];

		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$acct_no = $this->_getParam('acct_no');
		//echo $acct_no;

		$svcAccount = new Service_Account($acct_no, Application_Helper_General::getCurrNum('IDR'), $app, '1');
		$result = $svcAccount->inquiryDeposito('AB', TRUE);
		//var_dump($result);

		$data['data'] = false;
		is_array($result) ? $result :  $result = $data;
		echo json_encode($result);
	}

	public function inquiryaccountbalanceAction()
	{

		$app = Zend_Registry::get('config');
		$app = $app['app']['bankcode'];

		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$acct_no = $this->_getParam('acct_no');
		//echo $acct_no;

		$svcAccount = new Service_Account($acct_no, Application_Helper_General::getCurrNum('IDR'), $app, '1');
		$result = $svcAccount->inquiryAccountBalance('AB', TRUE);
		//var_dump($result);
		//die();
		$data['data'] = false;
		is_array($result) ? $result :  $result = $data;
		echo json_encode($result);
	}

	public function inquiryaccountinfoAction()
	{

		//integrate ke core
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();
		$acct_no = $this->_getParam('acct_no');
		$svcAccount = new Service_Account($acct_no, null);
		$result = $svcAccount->inquiryAccontInfo('AI', TRUE);

		if ($result['response_desc'] == 'Success') //00 = success
		{
			$svcAccountCIF = new Service_Account($result['account_number'], null, null, null, null, $result['cif']);
			$result2 = $svcAccountCIF->inquiryCIFAccount();

			$filterBy = $result['account_number']; // or Finance etc.
			$new = array_filter($result2['accounts'], function ($var) use ($filterBy) {
				return ($var['account_number'] == $filterBy);
			});

			$singleArr = array();
			foreach ($new as $key => $val) {
				$singleArr = $val;
			}
		}

		if ($singleArr['type_desc'] == 'Deposito') {
			$svcAccountDeposito = new Service_Account($result['account_number'], null, null, null, null, null);
			$result3 = $svcAccountDeposito->inquiryDeposito();
		} else {
			$svcAccountBalance = new Service_Account($result['account_number'], null, null, null, null, null);
			$result3 = $svcAccountBalance->inquiryAccountBalance();
		}
		$return = array_merge($result, $singleArr, $result3);

		$data['data'] = false;
		is_array($return) ? $return :  $return = $data;


		echo json_encode($return);
	}
}
