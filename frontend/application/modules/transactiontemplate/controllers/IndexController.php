<?php
require_once 'Zend/Controller/Action.php';
class transactiontemplate_IndexController extends Application_Main {
	
	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
		
		$arr = null;
		$viewFilter = null;
		$errors = null;
		
		$fields = array	(
							'TEMPLATE_NAME'  			=> array	(
																	'field' => 'T_NAME',
																	'label' => $this->language->_('Template Name'),
																	'sortable' => true
																),
							'PAYMENT_SUBJECT'  			=> array	(
																	'field' => 'TEMP_SUBJECT',
																	'label' => $this->language->_('Payment Subject'),
																	'sortable' => true
																),
							
							'SOURCE_ACCOUNT'  			=> array	(
																	'field' => 'TEMP_SOURCE',
																	'label' => $this->language->_('Source Account'),
																	'sortable' => true
																),
							'BENE_ACCOUNT'  		=> array	(
																	'field' => 'TEMP_BENE',
																	'label' => $this->language->_('Beneficiary Account'),
																	'sortable' => true
																	),	
							'TYPE'  		=> array	(
																	'field' => 'T_TYPE',
																	'label' => $this->language->_('Type'),
																	'sortable' => true
																	),	
							'AMOUNT'  					=> array	(
																	'field' => 'TEMP_AMOUNT',
																	'label' => $this->language->_('CCY / Amount'),
																	'sortable' => true
																	),	
								
						);
		$this->view->fields = $fields;


		$filterlist = array('SOURCE_ACCOUNT','TYPE','TEMPLATE_NAME','PAYMENT_SUBJECT','BENE_ACCOUNT');
		
		$this->view->filterlist = $filterlist;
	  
		
		$filterArr = array(	'filter' 	  	=> array('StringTrim','StripTags'),
							'TEMPLATE_NAME'   => array('StringTrim','StripTags'),
							'PAYMENT_SUBJECT'   	=> array('StringTrim','StripTags'),
							'SOURCE_ACCOUNT' 	=> array('StringTrim','StripTags'),
							'BENE_ACCOUNT' 	=> array('StringTrim','StripTags'),
							'TYPE' 	=> array('StringTrim','StripTags'),
							'AMOUNT' 	=> array('StringTrim','StripTags')
							
		);

		$dataParam = array('TEMPLATE_NAME','PAYMENT_SUBJECT','BENE_ACCOUNT');
		$dataParamValue = array();
		// print_r($this->_request->getParam('wherecol'));die;
		foreach ($dataParam as $dtParam)
		{
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}
				
			}

		}
		
		$zf_filter = new Zend_Filter_Input($filterArr,array(),$dataParamValue);
		// $filter = $zf_filter->getEscaped('filter');
		$filter 		= $this->_getParam('filter');

		// $zf_filter 	= new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		
		$page = $this->_getParam('page');
		$page = (Zend_Validate::is($page,'Digits'))? $page : 1;
		$sortBy  = $this->_getParam('sortby');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
		
		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		
		// $filter 	= $zf_filter->getEscaped('filter');
		$delete 	= $this->_getParam('delete');
		
		if($delete)
		{
			//echo("<br>ok<br>");
			$postreq_id	= $this->_request->getParam('req_id');
			if($postreq_id)
			{
				foreach ($postreq_id as $key => $value) 
				{
					if($postreq_id[$key]==0)
					{
						unset($postreq_id[$key]);
					}
					
				}
			}
			
			if($postreq_id == null)
			{
				$params['req_id'] = null;
			}
			else
			{
				$params['req_id'] = 1;
			}
			
			$validators = array	(
										'req_id' => 	array	(	
																	'NotEmpty',
																	'messages' => array	(
																							'Error File ID Submitted',
																						)
																),
									);
			$filtersVal = array	( 	
									'req_id' => array('StringTrim','StripTags')
								);
			$zf_filter_input = new Zend_Filter_Input($filtersVal,$validators,$params,$this->_optionsValidator);
			
			if($zf_filter_input->isValid())
			{			
				try
				{
					foreach ($postreq_id as  $key =>$value) 
					{		
						$T_ID =  $postreq_id[$key];
				
						$this->_db->beginTransaction();
						$where = array('T_ID = ?' => $T_ID);
						$this->_db->delete('M_TEMPLATE',$where);


						$where2 = array('TEMP_ID = ?' => $T_ID);
						$this->_db->delete('M_TEMPLATE_DATA',$where2);

						$this->_db->commit();
					}
					$id = implode(",", $postreq_id);
					Application_Helper_General::writeLog('HLUD','Delete Template. Template ID: ['.$id.']');
					$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
					$this->_redirect('/notification/success');
				}
				catch(Exception $e)
				{
					$this->_db->rollBack();
				}
			}
			else
			{
				$error 			= true;
				$errors 		= $zf_filter_input->getMessages();
				$req_idErr 		= (isset($errors['req_id']))? $errors['req_id'] : null;
				
				$this->_redirect("/transactiontemplate/index?error=true&req_idErr=$req_idErr&filter=Filter");
			}
			
			//$filter = 'Filter';
			
		}
		
		if($this->_getParam('error'))
		{
			$this->view->error 			= $this->_getParam('error');
			
			$this->view->req_idErr 		= $this->_getParam('req_idErr');
			
		}
		
		//if($filter)
		//{


		//select data template

		$select = $this->_db->select()
				->from(array('A' => 'M_TEMPLATE'),array('*'))
				->join(array('G' => 'M_TEMPLATE_DATA'), 'A.T_ID = G.TEMP_ID', array('G.*'));
		$select ->order('T_ID DESC');
		
		if($filter == TRUE)
		{
			$TEMPNAME   	= $this->_getParam('TEMPLATE_NAME');
			$PAYSUBJ   	= $this->_getParam('PAYMENT_SUBJECT');
			$SOURCEACCT    = $this->_getParam('SOURCE_ACCOUNT');
			$BENEACCT    	= $zf_filter->getEscaped('BENE_ACCOUNT');
			$TYPE    	= $zf_filter->getEscaped('T_TYPE');					
			$AMOUNT		= $zf_filter->getEscaped('AMOUNT');					
			
			if($SEARCH_TEXT)
			{
				$select->where("UPPER(A.T_NAME) LIKE ".$this->_db->quote('%'.$TEMPNAME.'%'));
			}
			
			if($PAYSUBJ)
			{
				$select->where("UPPER(G.TEMP_SUBJECT) LIKE ".$this->_db->quote('%'.$PAYSUBJ.'%'));
			}
			
			if($SOURCEACCT)
			{
				$select->where("UPPER(G.TEMP_SOURCE) LIKE ".$this->_db->quote('%'.$SOURCEACCT.'%'));
			}

			if($BENEACCT)
			{
				$select->where("UPPER(G.TEMP_BENE) LIKE ".$this->_db->quote('%'.$BENEACCT.'%'));
			}

			if($TYPE)
			{
				$select->where("UPPER(A.T_TYPE) LIKE ".$this->_db->quote('%'.$TYPE.'%'));
			}

			if($AMOUNT)
			{
				$select->where("UPPER(G.TEMP_AMOUNT) LIKE ".$this->_db->quote('%'.$AMOUNT.'%'));
			}
			// echo $select;die;
			$this->view->TEMPNAME 	= $TEMPNAME;
			$this->view->PAYSUBJ 	= $PAYSUBJ;
			$this->view->SOURCEACCT = $SOURCEACCT;
			$this->view->BENEACCT 	= $BENEACCT;
			$this->view->TYPE 		= $TYPE;
			$this->view->AMOUNT 	= $AMOUNT;
		}

		$select->order($sortBy.' '.$sortDir);
		$arr = $this->_db->fetchAll($select);

		// print_r($arr);die();

		$this->view->arr = $arr;
		$this->paging($arr);
		unset($dataParamValue['CREATED_END']);

		// print_r($dataParamValue);die();
		if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     
      }
	}
}