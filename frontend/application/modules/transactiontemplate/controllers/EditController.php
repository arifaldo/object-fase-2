<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';

class transactiontemplate_EditController extends Application_Main {
	
	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');
		$T_ID   	= $this->_getParam('ID');
		$this->view->T_ID = $T_ID;
		
		$filterArr = array(	'filter'=> array('StringTrim','StripTags'),
							'submit'=> array('StringTrim','StripTags'));
		
		$zf_filter 	= new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		
		$filter 	= $this->_getParam('filter');
		
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
			if($temp[0]=='F' || $temp[0]=='S'){
				if($temp[0]=='F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = ''; unset($temp[0]);
				foreach($temp as $value)
				{
					if(!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}	
		}

		$params = $this->_request->getParams();

    	$selectuser = $this->_db->select()
             ->from(array('A' => 'M_USER')); 
	    $selectuser->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin));
	    // echo $selectuser;die;
	    $this->view->dataact = $selectuser->query()->fetchAll();

	    $selectccy = $this->_db->select()
             ->from(array('A' => 'M_MINAMT_CCY'), array('CCY_ID'));
	    $this->view->dataccy = $selectccy->query()->fetchAll();

	

	    //select data template
		$select = $this->_db->select()
				->from(array('A' => 'M_TEMPLATE'),array('*'))
				->join(array('G' => 'M_TEMPLATE_DATA'), 'A.T_ID = G.TEMP_ID', array('G.*'))
				->where('A.T_ID = ?', (string) $T_ID );

		$data = $this->_db->fetchRow($select);

		$this->view->data = $data;

		// print_r($data);die();

		//inhouse

		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

		if ($data['TEMP_CROSS'] == 1) {
			$AccArr = $CustomerUser->getAccounts();
		}else{
			$param = array('CCY_IN'=>array("IDR","USD"));
			$AccArr = $CustomerUser->getAccounts($param);
		}

		$this->view->AccArr = $AccArr;

		$model = new purchasing_Model_Purchasing();

		$purposeArr = $model->getTranspurpose();

		$purposeList = array(''=>'-- Select Transaction Purpose --');
		foreach ($purposeArr as $key => $value ){
			$purposeList[$value['CODE']] = $value['DESCRIPTION'];
		}

		$this->view->TransPurposeArr = $purposeList;


		//domestic

		$paramSettingID = array('range_futuredate', //'auto_release', 'cut_off_time_inhouse'
								'cut_off_time_skn'   , 'cut_off_time_rtgs',
								'threshold_rtgs'     , 'threshold_lld');

		$settings 			= new Application_Settings();
		$settings->setSettings(null, $paramSettingID);
		$ccyList  			= $settings->setCurrencyRegistered();

		//for skn rtgs
		$lldTypeArr  		= $settings->getLLDDOMType();
		$lldCategoryArr  	= $settings->getLLDDOMCategory();
		$lldIdenticalArr  	= $settings->getLLDDOMIdentical();
		$lldRelationshipArr = $settings->getLLDDOMRelationship();
		$lldPurposeArr 		= $settings->getLLDDOMPurpose();
		$lldBeneIdentifArr 	= $settings->getLLDDOMBeneIdentification();
		$lldSenderIdentifArr = $settings->getLLDDOMSenderIdentification();
		$residentArr 		= array_combine($this->_citizenship["code"], $this->_citizenship["desc"]);
 		$citizenshipArr		= array("W" => "WNI", "N" => "WNA");

		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr2 	  = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR")));

		$anyValue = '-- '.$this->language->_('Select City'). ' --';
		$select = $this->_db->select()
				->from(array('A' => 'M_CITY'),array('*'));
		$select ->order('CITY_NAME ASC');
		$arr = $this->_db->fetchall($select);
		
		$cityCodeArr 			= array(''=> $anyValue);
		$cityCodeArr 			+= Application_Helper_Array::listArray($arr,'CITY_CODE','CITY_NAME');
		$this->view->cityCodeArr 	= $cityCodeArr;

		$this->view->AccArr2 			= $AccArr2;
		$this->view->transferTypeArr 	= array($this->_transfertype["desc"]["SKN"]  => $this->_transfertype["desc"]["SKN"],
												$this->_transfertype["desc"]["RTGS"] => $this->_transfertype["desc"]["RTGS"],
												'ONLINE' => 'ONLINE');
 		$this->view->citizenshipArr 	= $citizenshipArr;
		$this->view->residentArr 		= $residentArr;
		$this->view->lldCategoryArr 	= $lldCategoryArr;
		$this->view->lldIdenticalArr 	= $lldIdenticalArr;
		$this->view->lldRelationshipArr = $lldRelationshipArr;
		$this->view->lldPurposeArr 		= $lldPurposeArr;
		$this->view->lldBeneIdentifArr  = $lldBeneIdentifArr;
		$this->view->lldSenderIdentifArr  = $lldSenderIdentifArr;

		$this->view->IdentyArr 			= $lldIdenticalArr;
		$this->view->TransactorArr 		= $lldRelationshipArr;


		$submit1 = $params['submit1']; //if params inhouse
		$submit2 = $params['submit2']; //if params domestic
		$submit3 = $params['submit3']; //if params domestic

			if($this->_request->isPost())
			{
				if ($submit1) {
					$bene = $params['edit_tempbenef'];

					// jika ya cross curr
					if ($params['edit_tempcross'] == 1) {
						
						$purpose = $params['edit_temptranspose'];
						$exchange = $params['edit_tempexrate'];

						if ($exchange == 1) {
							$refnum = '';
						}else{
							$refnum = $params['edit_temprefnum'];
						}

						$custref = $params['edit_tempcustnum'];

						$ccy = $params['edit_tempccy'];

						$amount = $ccy." ".$params['edit_tempamount'];

					}else{

						$purpose = '';
						$exchange = '';
						$refnum = '';
						$custref = '';
						$amount = $params['edit_tempamount'];
					}

					//jika tidak
					if ($params['edit_tempnotif'] == 1) {
						$sms = '';
						$email = '';
					}else{
						$sms = $params['edit_tempsmsnotif'];
						$email = $params['edit_tempemailnotif'];	
					}

					foreach ($params['template'] as $key => $value) {
						$locked .= $value.",";
					}

					try
					{
						$this->_db->beginTransaction();
						$param = array(
								'T_NAME' 		=> $params['edit_tempname'],
								'T_TARGET' 		=> $params['user'][0],
						);	
						
						$where = array('T_ID = ?' => $T_ID);
						$query = $this->_db->update ( "M_TEMPLATE", $param, $where );


						if(!empty($bene)){
   								$select = $this->_db->select()
								->from(array('A' => 'M_BENEFICIARY'),array('*'));
								$select->where('A.BENEFICIARY_ACCOUNT = ?' , (string) $bene);
								$select->where('A.CUST_ID = ?' , (string) $this->_custIdLogin);
								$benedata = $this->_db->fetchall($select);				
								$benename = $benedata['0']['BENEFICIARY_NAME'];
   							}else{
   								$benedata = array();
   								$benename = '';
   							}


						$param2 = array(
								'TEMP_SUBJECT'	=> $params['edit_tempsubject'],
								'TEMP_CROSS'	=> $params['edit_tempcross'], 
								'TEMP_SOURCE' 	=> $params['edit_tempsourceacct'],
								'TEMP_BENE'		=> $bene,
								'TEMP_BENE_NAME'	=> $benename,
								'TEMP_AMOUNT'	=> $amount,
								'TEMP_PURPOSE'  => $purpose,
								'TEMP_EXCHANGE'	=> $exchange,
								'TEMP_REFNUM'	=> $refnum,
								'TEMP_CUSTREF'	=> $custref,
								'TEMP_NOTIF'	=> $params['edit_tempnotif'],
								'TEMP_SMS' => $sms,
								'TEMP_EMAIL' => $email,
								'TEMP_LOCKED' => $locked,
						);	

						$updateWhere['TEMP_ID = ?'] = (string)$T_ID;

   						$this->_db->update('M_TEMPLATE_DATA',$param2,$updateWhere);


						$this->_db->commit();
						Application_Helper_General::writeLog('HLUD','Edit Template. Template ID: ['.$T_ID.']');
						$this->setbackURL('/transactiontemplate');
						$this->_redirect('/notification/success/index');
					}
					catch(Exception $e)
					{
						$this->_db->rollBack();
					}	

				}
				elseif ($submit2) {
					$bene = $params['edit_tempbenef2'];
					$amount = $params['edit_tempamount2'];
					$bank = $params['edit_tempbankname'];
					$emaildom = $params['edit_tempemail'];

					if ($params['edit_temptratype'] == 'SKN' || $params['edit_temptratype'] == 'RTGS') {
						$beneaddress = $params['edit_tempbeneaddress'];
						$citizenship = $params['edit_tempcitizenship'];
						$nationality = $params['edit_tempnationality'];
						$benecategory = $params['edit_tempbenecategory'];
						$beneidtype = $params['edit_tempbeneidtype'];
						$beneidnumber = $params['edit_tempbeneidnumber'];
						$city = $params['edit_tempcity'];

					}else{
						$beneaddress = '';
						$citizenship = '';
						$nationality = '';
						$benecategory = '';
						$beneidtype = '';
						$beneidnumber = '';
						$city = '';
					}


					if ($params['edit_temptratype'] == 'SKN') {
						$type = 'SKN'; 
					}elseif ($params['edit_temptratype'] == 'RTGS'){
						$type = 'RTGS';
					}else{
						$type = 'ONLINE';
					}

					//jika tidak
					if ($params['edit_tempnotif2'] == 1) {
						$sms = '';
						$email = '';
					}else{
						$sms = $params['edit_tempsmsnotif2'];
						$email = $params['edit_tempemailnotif2'];	
					}

					foreach ($params['template2'] as $key => $value) {
						$locked .= $value.",";
					}

					try
					{
						$this->_db->beginTransaction();
						$param = array(
								'T_NAME' 		=> $params['edit_tempname2'],
								'T_TARGET' 		=> $params['user2'][0],
						);
						
						$where = array('T_ID = ?' => $T_ID);
						$query = $this->_db->update ( "M_TEMPLATE", $param, $where );


						if(!empty($bene)){
   								$select = $this->_db->select()
								->from(array('A' => 'M_BENEFICIARY'),array('*'));
								$select->where('A.BENEFICIARY_ACCOUNT = ?' , (string) $bene);
								$select->where('A.CUST_ID = ?' , (string) $this->_custIdLogin);
								$benedata = $this->_db->fetchall($select);				
								$benename = $benedata['0']['BENEFICIARY_NAME'];
   							}else{
   								$benedata = array();
   								$benename = '';
   							}


						$param2 = array(
								'TEMP_SUBJECT'	=> $params['edit_tempsubject2'],
								'TEMP_SOURCE' 	=> $params['edit_tempsourceacct2'],
								'TEMP_BENE'		=> $bene,
								'TEMP_BENE_NAME'	=> $benename,
								'TEMP_AMOUNT'	=> $amount,
								'TEMP_TRATYPE'		=> $type, 
								'TEMP_BENEADDRESS'  => $beneaddress,
								'TEMP_CITIZENSHIP'	=> $citizenship,
								'TEMP_NATIONALITY'	=> $nationality,
								'TEMP_BENECATEGORY'	=> $benecategory,
								'TEMP_BENEIDTYPE'	=> $beneidtype,
								'TEMP_BENEIDNUM'	=> $beneidnumber,
								'TEMP_BANK'			=> $bank,
								'TEMP_CITY'			=> $city,
								'TEMP_EMAIL_DOMESTIC'	=> $emaildom,

								'TEMP_NOTIF'	=> $params['edit_tempnotif2'],
								'TEMP_SMS' => $sms,
								'TEMP_EMAIL' => $email,
								'TEMP_LOCKED' => $locked,
						);	

						$updateWhere['TEMP_ID = ?'] = (string)$T_ID;

   						$this->_db->update('M_TEMPLATE_DATA',$param2,$updateWhere);


						$this->_db->commit();
						Application_Helper_General::writeLog('HLUD','Edit Template. Template ID: ['.$T_ID.']');
						$this->setbackURL('/transactiontemplate');
						$this->_redirect('/notification/success/index');
					}
					catch(Exception $e)
					{
						$this->_db->rollBack();
					}

				} elseif ($submit3) {

					$bene = $params['edit_tempbenef3'];
					$purpose = $params['edit_temptranspose3'];
					$ccy = $params['edit_tempccy3'];
					$amount = $ccy." ".$params['edit_tempamount3'];

					//jika tidak
					if ($params['edit_tempnotif3'] == 1) {
						$sms = '';
						$email = '';
					}else{
						$sms = $params['edit_tempsmsnotif3'];
						$email = $params['edit_tempemailnotif3'];	
					}

					foreach ($params['template3'] as $key => $value) {
						$locked .= $value.",";
					}

					try
					{
						$this->_db->beginTransaction();
						$param = array(
								'T_NAME' 		=> $params['edit_tempname3'],
								'T_TARGET' 		=> $params['user3'][0],
						);	
						
						$where = array('T_ID = ?' => $T_ID);
						$query = $this->_db->update ( "M_TEMPLATE", $param, $where );


						if(!empty($bene)){
   								$select = $this->_db->select()
								->from(array('A' => 'M_BENEFICIARY'),array('*'));
								$select->where('A.BENEFICIARY_ACCOUNT = ?' , (string) $bene);
								$select->where('A.CUST_ID = ?' , (string) $this->_custIdLogin);
								$benedata = $this->_db->fetchall($select);				
								$benename = $benedata['0']['BENEFICIARY_NAME'];
   							}else{
   								$benedata = array();
   								$benename = '';
   							}


						$param2 = array(
								'TEMP_SUBJECT'	=> $params['edit_tempsubject3'], 
								'TEMP_SOURCE' 	=> $params['edit_tempsourceacct3'],
								'TEMP_BENE'		=> $bene,
								'TEMP_BENE_NAME'	=> $benename,
								'TEMP_AMOUNT'	=> $amount,
								'TEMP_CHARGE_TYPE'	=> $params['edit_tempchargetype3'],
								'TEMP_PURPOSE'  => $purpose,
								'TEMP_MESSAGE'	=> $params['edit_tempmessage3'],
								'TEMP_IDENTY'	=> $params['edit_tempidentity3'],
								'TEMP_RELATIONSHIP'	=> $params['edit_temptransactor3'],
								'TEMP_BANK'	=> $params['edit_tempbankname3'],
								'TEMP_EMAIL_DOMESTIC'	=> $params['edit_tempemail3'],
								'TEMP_NOTIF'	=> $params['edit_tempnotif3'],
								'TEMP_SMS' => $sms,
								'TEMP_EMAIL' => $email,
								'TEMP_LOCKED' => $locked,
						);	

						$updateWhere['TEMP_ID = ?'] = (string)$T_ID;

   						$this->_db->update('M_TEMPLATE_DATA',$param2,$updateWhere);


						$this->_db->commit();
						Application_Helper_General::writeLog('HLUD','Edit Template. Template ID: ['.$T_ID.']');
						$this->setbackURL('/transactiontemplate');
						$this->_redirect('/notification/success/index');
					}
					catch(Exception $e)
					{
						$this->_db->rollBack();
					}	
				}
			}
		
	
		if(!$this->_request->isPost()){
			Application_Helper_General::writeLog('HLUD','Viewing Edit Help');
		}
						
	}

	public function sourceAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $type = $this->_getParam('type');

        $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

        if($type == 1){
        	$AccArr = $CustomerUser->getAccounts();
        }
        else{
        	$param = array('CCY_IN'=>'IDR');
			$AccArr = $CustomerUser->getAccounts($param);
        }
		

        echo '<option value="-"> -- Select Source Account -- </option>';
		foreach($AccArr as $key => $val){
			echo '<option value="'.$val['ACCT_NO'].'" ccy="'.$val['CCY_ID'].'"  '.$selected.' >'.$val['ACCT_NO'].' ['.$val['CCY_ID'].'] - '.$val['ACCT_NAME'].' / '.$val['ACCT_ALIAS_NAME'].' ('.$val['DESC'].')</option>';
		}

        echo $optHtml;
    }
}
