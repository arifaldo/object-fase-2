<?php
require_once 'Zend/Controller/Action.php';

class management_CompanydetailController extends Application_Main
{

  public function indexAction()
  {
    $this->_helper->layout()->setLayout('newlayout');

    // Get data M_CUSTOMER
    $compInfo = $this->_db->select()
      ->from(
        array('A' => 'M_CUSTOMER'),
        array('*')
      )
      ->joinLeft(
        array('B' => 'M_COMPANY_TYPE'),
        'B.COMPANY_TYPE_CODE = A.CUST_TYPE',
        array('COMPANY_TYPE_DESC')
      )
      ->joinLeft(
        array('C' => 'M_BUSINESS_ENTITY'),
        'C.BUSINESS_ENTITY_CODE = A.BUSINESS_TYPE',
        array('BUSINESS_ENTITY_DESC')
      )
      ->joinLeft(
        array('D' => 'M_DEBITUR'),
        'D.DEBITUR_CODE = A.DEBITUR_CODE',
        array('DEBITUR_DESC')
      )
      ->joinLeft(
        array('E' => 'M_CITYLIST'),
        'E.CITY_CODE = A.CUST_CITY',
        array('CITY_NAME')
      )
      ->joinLeft(
        array('F' => 'M_CREDIT_QUALITY'),
        'F.CODE = A.COLLECTIBILITY_CODE',
        array('DESCRIPTION')
      )
      ->joinLeft(
        array('Z' => 'M_COUNTRY'),
        'Z.COUNTRY_CODE = A.COUNTRY_CODE',
        array('COUNTRY_NAME')
      )
      ->where('A.CUST_ID = ?', $this->_custIdLogin);
    $compInfo = $this->_db->fetchRow($compInfo);
    $compInfo['COLLECTIBILITY_CODE'] = $compInfo['COLLECTIBILITY_CODE'] . ' - ' . $compInfo['DESCRIPTION'];
    $this->view->compInfo = $compInfo;
    // echo '<pre>';
    // print_r($compInfo);
    // echo '</pre><br>';

    // Get data M_CUSTOMER_ACCT
    $accountInfo = $this->_db->select()
      ->from(
        array('A' => 'M_CUSTOMER_ACCT'),
        array('*')
      )
      ->where('A.CUST_ID = ?', $this->_custIdLogin);
    $accountInfo = $this->_db->fetchAll($accountInfo);
    $this->view->accountInfo = $accountInfo;

    // Get data M_APP_GROUP_USER BY GROUP
    if ($compInfo['CUST_MODEL'] == 1) {
      $groupUser = $this->_db->select()
        ->from(
          array('A' => 'M_APP_GROUP_USER'),
          array('*')
        )
        ->where('A.CUST_ID = ?', $this->_custIdLogin)
        ->group('GROUP_USER_ID');
      $groupUser = $this->_db->fetchAll($groupUser);

      // Get data M_APP_GROUP_USER BY USER
      $selectUser = $this->_db->select()
        ->from(
          array('A' => 'M_APP_GROUP_USER'),
          array('*')
        )
        ->where('A.CUST_ID = ?', $this->_custIdLogin);
      $selectUser = $this->_db->fetchAll($selectUser);

      $alfabets = [''];
      $alpha = 'A';
      while ($alpha != 'Z') {
        $alfabets[] = $alpha;
        $alpha = chr(ord($alpha) + 1);
      }
      $alfabets[] = $alpha;
      unset($alfabets[0]);

      $userLists = '';
      foreach ($groupUser as $key => $value) {
        foreach ($selectUser as $key2 => $value2) {
          if ($value['GROUP_USER_ID'] == $value2['GROUP_USER_ID']) {
            if (empty($userLists)) {
              $userLists .= $value2['USER_ID'];
            } else {
              $userLists .= ', ' . $value2['USER_ID'];
            }
          }
        }
        $groupUser[$key]['USER'] = $userLists;

        $userLists = '';
        $special = 'S_' . $this->_custIdLogin;
        if ($value['GROUP_USER_ID'] == $special) {
          $groupUser[$key]['GID'] = 'SG';
        } else {
          $groupExp = explode('_', $value['GROUP_USER_ID']);
          $groupUser[$key]['GID'] = $alfabets[(int)$groupExp[2]];
        }
      }
      $this->view->groupUser = $groupUser;
    }

    // Get data M_APP_BOUNDARY
    $dataBoundary = $this->_db->select()
      ->from(
        array('A' => 'M_APP_BOUNDARY'),
        array('*')
      )
      ->joinLeft(
        array('B' => 'M_APP_BOUNDARY_GROUP'),
        'B.BOUNDARY_ID = A.BOUNDARY_ID',
        array('GROUP_USER_ID')
      )
      ->where('A.CUST_ID = ?', $this->_custIdLogin)
      ->group('A.BOUNDARY_ID');
    $dataBoundary = $this->_db->fetchAll($dataBoundary);
    foreach ($dataBoundary as $key => $row) {
      $policy = str_replace('AND', '<span style="color:blue">and</span>', $row['POLICY']);
      $policy = str_replace('OR', '<span style="color:blue">or</span>', $policy);
      $policy = str_replace('THEN', '<span style="color:blue">then</span>', $policy);
      $dataBoundary[$key]['POLICY'] = $policy;
    }
    $this->view->dataBoundary = $dataBoundary;

    $transferTypeArr = array_combine($this->_paymenttype["code"], $this->_paymenttype["desc"]);
    $transferTypeArr['23'] = 'Cash Pooling Other Bank';
    $this->view->transferTypeArr = $transferTypeArr;

    // SPECIAL OBLIGEE
    if ($compInfo['CUST_MODEL'] == 3) {
      $select = $this->_db->select()
        ->from(
          array('A' => 'M_CUST_SPOBLIGEE'),
          array('*')
        )
        ->joinLeft(
          array('C' => 'M_CUSTOMER'),
          'A.CUST_ID = C.CUST_ID',
          array('CUST_NAME')
        )
        ->where('A.CUST_ID = ?', $this->_custIdLogin);
      $specialObligee = $this->_db->fetchRow($select);

      if ($specialObligee) {
        // CUSTOMER ACCOUNT
        $select = $this->_db->select()
          ->from(
            array('A' => 'M_CUSTOMER_ACCT'),
            array('*')
          )
          ->where('A.ACCT_NO = ?', $specialObligee['SHARING_ACCT']);
        $getCustAcct = $this->_db->fetchRow($select);

        $sharingAcct = $getCustAcct['ACCT_NO'] . ' (' . $getCustAcct['CCY_ID'] . ')' . ' / ' . $getCustAcct['ACCT_NAME'] . ' / ' . $getCustAcct['ACCT_DESC'];

        $specialObligee['SHARING_ACCT'] = $sharingAcct;
        $this->view->specialObligee = $specialObligee;
      }
    } else {
      // LINE FACILITY
      $select = $this->_db->select()
        ->from(
          array('A' => 'M_CUST_LINEFACILITY'),
          array('*')
        )
        ->where('A.CUST_ID = ?', $this->_custIdLogin);
      $lineFacility = $this->_db->fetchRow($select);
      $this->view->lineFacility = $lineFacility;

      if ($lineFacility) {
        $get_linefacility = $this->_db->select()
          ->from("M_CUST_LINEFACILITY", ["CUST_ID", "PLAFOND_LIMIT", "FEE_PROVISION", "FEE_ADMIN", "FEE_STAMP", "CUST_SEGMENT"])
          ->where("ID = ?", $lineFacility['ID'])
          ->query()->fetchAll();
      }

      if ($get_linefacility[0]["CUST_SEGMENT"] == "4") {
        $check_all_detail = $this->_db->select()
          ->from("T_BANK_GUARANTEE_DETAIL")
          ->where("PS_FIELDNAME = ?", "Insurance Name")
          ->where("PS_FIELDVALUE = ?", $get_linefacility[0]["CUST_ID"])
          ->query()->fetchAll();

        $total_bgamount_on_risk = 0;

        if (count($check_all_detail) > 0) {
          $save_bg_reg_number = [];
          foreach ($check_all_detail as $value) {
            array_push($save_bg_reg_number, $value["BG_REG_NUMBER"]);
          }

          $get_bgamount_on_risks = $this->_db->select()
            ->from("T_BANK_GUARANTEE", ["BG_AMOUNT"])
            ->where("(BG_STATUS = 15) AND COUNTER_WARRANTY_TYPE = '3'")
            ->where("BG_REG_NUMBER IN (?)", $save_bg_reg_number)
            ->query()->fetchAll();

          foreach ($get_bgamount_on_risks as $get_bgamount_on_risk) {
            $total_bgamount_on_risk += $get_bgamount_on_risk["BG_AMOUNT"];
          }
        }

        $check_all_detail = $this->_db->select()
          ->from("TEMP_BANK_GUARANTEE_DETAIL")
          ->where("PS_FIELDNAME = ?", "Insurance Name")
          ->where("PS_FIELDVALUE = ?", $get_linefacility[0]["CUST_ID"])
          ->query()->fetchAll();

        $total_bgamount_on_temp = 0;

        if (count($check_all_detail) > 0) {

          $save_bg_reg_number = [];
          foreach ($check_all_detail as $value) {
            array_push($save_bg_reg_number, $value["BG_REG_NUMBER"]);
          }

          $get_bgamount_on_temps = $this->_db->select()
            ->from("TEMP_BANK_GUARANTEE", ["BG_AMOUNT"])
            ->where("COUNTER_WARRANTY_TYPE = '3'")
            ->where("BG_REG_NUMBER IN (?)", $save_bg_reg_number)
            ->where("BG_STATUS IN (5,6,7,10,14,17,20,21,22,23,24)")
            ->query()->fetchAll();

          foreach ($get_bgamount_on_temps as $key => $get_bgamount_on_temp) {
            $total_bgamount_on_temp += $get_bgamount_on_temp["BG_AMOUNT"];
          }
        }
        // echo $total_bgamount_on_risk;
        $this->view->current_limit = $get_linefacility[0]["PLAFOND_LIMIT"] - $total_bgamount_on_risk - $total_bgamount_on_temp;
        $this->view->limit_used = $total_bgamount_on_risk + $total_bgamount_on_temp;
      } else {
        $get_bgamount_on_risks = $this->_db->select()
          ->from("T_BANK_GUARANTEE", ["BG_AMOUNT"])
          ->where("(BG_STATUS = 15) AND COUNTER_WARRANTY_TYPE = '2' AND CUST_ID = " . $this->_db->quote($get_linefacility[0]["CUST_ID"]))
          ->query()->fetchAll();


        $total_bgamount_on_risk = 0;

        foreach ($get_bgamount_on_risks as $key => $get_bgamount_on_risk) {
          $total_bgamount_on_risk += $get_bgamount_on_risk["BG_AMOUNT"];
        }

        $total_bgamount_on_temp = 0;

        $get_bgamount_on_temps = $this->_db->select()
          ->from("TEMP_BANK_GUARANTEE", ["BG_AMOUNT"])
          ->where("COUNTER_WARRANTY_TYPE = '2' AND CUST_ID = " . $this->_db->quote($get_linefacility[0]["CUST_ID"]))
          ->where("BG_STATUS IN (1,2,3,5,6,7,14,17,20)")
          ->query()->fetchAll();

        foreach ($get_bgamount_on_temps as $key => $get_bgamount_on_temp) {
          $total_bgamount_on_temp += $get_bgamount_on_temp["BG_AMOUNT"];
        }


        $this->view->current_limit = $get_linefacility[0]["PLAFOND_LIMIT"] - $total_bgamount_on_risk - $total_bgamount_on_temp;
        $this->view->limit_used = $total_bgamount_on_risk + $total_bgamount_on_temp;
      }
    }

    // get all provision
    if ($compInfo['CUST_MODEL'] == '1' || $compInfo['CUST_MODEL'] == '2') {

      $data = $this->_db->select()
        ->from(array('A' => 'M_CUST_LINEFACILITY'), array('*'))
        ->joinLeft(
          array('B' => 'M_CUSTOMER'),
          'B.CUST_ID = A.CUST_ID',
          array('CUST_NAME', 'COLLECTIBILITY_CODE')
        )
        ->where('A.CUST_ID = ?', $compInfo['CUST_ID'])
        ->query()->fetch();

      $getCustomer = $this->_db->select()
        ->from("M_CUSTOMER")
        ->where("CUST_ID = ?", $compInfo['CUST_ID'])
        ->query()->fetch();

      $data["GRUP_BUMN"] = $getCustomer["GRUP_BUMN"];
      $this->view->data = $data;

      $get_all_provision = $this->_db->fetchAll(
        $this->_db->select()
          ->from("M_CUST_LINEFACILITY_DETAIL")
          ->where("CUST_ID = ?", $compInfo['CUST_ID'])
      );

      $this->view->get_all_provision = $get_all_provision;

      // $limit_available = (float)$data['PLAFOND_LIMIT'] - (float)$data['LIMIT_USED'];
      // $this->view->limit_available = $limit_available;

      // $segmentationArr = [
      //   1 => 'CORPORATE',
      //   2 => 'COMMERCIAL',
      //   3 => 'SME',
      //   4 => 'FINANCIAL INSTITUTION'
      // ];
      // $this->view->segmentationArr = $segmentationArr;

      // $getCreditQuality = $model->getCreditQuality();
      // $creditArr = Application_Helper_Array::listArray($getCreditQuality, 'CODE', 'DESCRIPTION');
      // $this->view->creditArr = $creditArr;

      // $getTempLinefacility = $model->getTempLinefacility($this->_custIdLogin);
      // if ($getTempLinefacility) {
      //   $this->view->is_update = true;
      // }

      // $config = Zend_Registry::get('config');

      // $entityCode = $config["bg"]["entity"]["code"];
      // $entityDesc = $config["bg"]["entity"]["desc"];
      // $entityArr  = array_combine(array_values($entityCode), array_values($entityDesc));
      // $this->view->entityArr = $entityArr;

      // $citizenshipCode = $config["bg"]["citizen"]["code"];
      // $citizenshipDesc = $config["bg"]["citizen"]["desc"];
      // $citizenshipArr  = array_combine(array_values($citizenshipCode), array_values($citizenshipDesc));
      // $this->view->citizenshipArr = $citizenshipArr;

      // $statusCode = $config["bg"]["statusowner"]["code"];
      // $statusDesc = $config["bg"]["statusowner"]["desc"];
      // $statusArr  = array_combine(array_values($statusCode), array_values($statusDesc));
      // $this->view->statusArr = $statusArr;


      // $select = $this->_db->select()
      //   ->from(
      //     array('A' => 'M_POSITION'),
      //     array('*')
      //   );
      // $position = $this->_db->fetchAll($select);
      // $positionArr = Application_Helper_Array::listArray($position, 'CODE', 'POSITION');
      // $this->view->positionArr = $positionArr;

      // $select = $this->_db->select()
      //   ->from(
      //     array('A' => 'BG_RELATIONSHIP_WITH_REPORTER'),
      //     array('*')
      //   );
      // $relationship    = $this->_db->fetchAll($select);
      // $relationshipArr = Application_Helper_Array::listArray($relationship, 'NO', 'RELATION_WITH_BANK');
      // $this->view->relationshipArr = $relationshipArr;

      // $select = $this->_db->select()
      //   ->from(
      //     array('A' => 'M_PRELIMINARY'),
      //     array('*')
      //   )
      //   ->where('A.CUST_ID = ?', $compInfo['CUST_ID']);
      // $datapre = $this->_db->fetchRow($select);

      // $datapre['CUST_NAME'] = $compInfo['CUST_NAME'];
      // $this->view->datapre = $datapre;


      // $select = $this->_db->select()
      //   ->from(
      //     array('A' => 'M_PRELIMINARY_MEMBER'),
      //     array('*')
      //   )
      //   ->joinLeft(["C" => "M_CITYLIST"], "C.CITY_CODE = A.KAB_CITY", ["C.CITY_NAME"])
      //   ->where('A.CUST_ID = ?', $compInfo['CUST_ID']);
      // $dataMember = $this->_db->fetchAll($select);
      // $this->view->dataMember = $dataMember;
    }

    $statusArr = [
      1 => 'Approved',
      2 => 'Terminated',
      3 => 'Expired',
      4 => 'Freeze Submission'
    ];
    $this->view->statusArr = $statusArr;

    // foreach($selectBoundary as $row){
    //   list($groupType,$groupName,$groupNumber) = explode("_", $row['GROUP_USER_ID']);
    //   if($groupType == 'N'){
    //     $name = 'Group '.trim($groupNumber,'0');
    //   }else{
    //     $name = 'Special Group';
    //   }

    //   $special = 'S_'.$this->_custIdLogin;
    //   // Get data M_APP_GROUP_USER
    //   $selectUser = $this->_db->select()
    //     ->from(
    //       array('A' => 'M_APP_GROUP_USER'),
    //       array('USER_ID')
    //     )
    //     ->where('A.GROUP_USER_ID = ?', $special);
    //   $selectUser = $this->_db->fetchAll($selectUser);

    //   $userLists = '';
    //   $policyExp = explode(' THEN ', $row['POLICY']);
    //   foreach($policyExp as $key => $value){
    //     $policyAND = explode(' AND ', $value);
    //     foreach($policyAND as $keyAND => $valueAND){
    //       if($valueAND == 'SG'){
    //         foreach($selectUser as $val){
    //           if(empty($userLists)){
    //             $userLists .= $val['USER_ID'];
    //           }else{
    //             $userLists .= ', '.$val['USER_ID'];
    //           }
    //         }
    //       }else{
    //         $policyOR = explode(' OR ', $value);
    //         foreach($policyOR as $keyOR => $valueOR){
    //           if($valueOR == 'SG'){
    //             foreach($selectUser as $val){
    //               if(empty($userLists)){
    //                 $userLists .= $val['USER_ID'];
    //               }else{
    //                 $userLists .= ', '.$val['USER_ID'];
    //               }
    //             }
    //           }else{
    //             $groupNo = sprintf("%02d", $alfabets[$valueOR]);
    //             $group = 'N_'.$this->_custIdLogin.'_'.$groupNo;
    //             // Get data M_APP_GROUP_USER
    //             $selectUser = $this->_db->select()
    //               ->from(
    //                 array('A' => 'M_APP_GROUP_USER'),
    //                 array('USER_ID')
    //               )
    //               ->where('A.GROUP_USER_ID = ?', $group);
    //             $selectUser = $this->_db->fetchAll($selectUser);
    //             foreach($selectUser as $val){
    //               if(empty($userLists)){
    //                 $userLists .= $val['USER_ID'];
    //               }else{
    //                 $userLists .= ', '.$val['USER_ID'];
    //               }
    //             }
    //           }
    //         }
    //       }
    //     }

    //     $userListsExp = explode(',', $userLists);
    //     $userLists = array_unique($userListsExp);
    //     $userListsNew = '';
    //     foreach($userLists as $keyList => $valueList){
    //       if($keyList == 0){
    //         $userListsNew .= $valueList;
    //       }else{
    //         $userListsNew .= ', '.$valueList;
    //       }
    //     }

    //     $policy = explode(' ', $row['POLICY']);
    //     foreach($policy as $keyPolicy => $valuePolicy){
    //       if($valuePolicy == 'AND'){
    //         $valpolicy = '<span style="color:blue;">and</span>';
    //       }
    //       elseif($valuePolicy == 'OR'){
    //         $valpolicy = '<span style="color:blue;">or</span>';
    //       }
    //       elseif($valuePolicy == 'THEN'){
    //         $valpolicy = '<span style="color:blue;">then</span>';
    //       }
    //     }
    //     echo '<pre>';
    //     print_r($userLists);
    //     echo '</pre><br>';
    //   }
    // }

    $printOption = $this->_request->getParam('printOption');
    $currentTab  = $this->_request->getParam('currentTab');
    $download    = $this->_request->getParam('download');

    // Print All Tab
    if ($printOption == 1) {
      // if($compInfo['CUST_MODEL'] == 3){
      $params = [
        'compInfo'        => $compInfo,
        'accountInfo'     => $accountInfo,
        'groupUser'       => $groupUser,
        'dataBoundary'    => $dataBoundary,
        'transferTypeArr' => $transferTypeArr,
        'specialObligee' => $specialObligee,
        'CUST_MODEL' => $compInfo['CUST_MODEL'],
        'lineFacility' => $lineFacility,
        'currentlimit' => $current_limit,
        'limit_used' => $limit_used

      ];
      // }else{
      //   $params = [
      //     'compInfo'        => $compInfo, 
      //     'accountInfo'     => $accountInfo, 
      //     'groupUser'       => $groupUser,
      //     'dataBoundary'    => $dataBoundary,
      //     'transferTypeArr' => $transferTypeArr,
      //     'lineFacility' => $lineFacility,
      //   ];
      // }
      $this->_forward('printcustomerdetailall', 'index', 'widget', $params);
    }

    // Print Current Tab
    // if($printOption == 2){
    //   $params = [
    //     'compInfo'        => $compInfo, 
    //     'accountInfo'     => $accountInfo, 
    //     'groupUser'       => $groupUser,
    //     'dataBoundary'    => $dataBoundary,
    //     'transferTypeArr' => $transferTypeArr,
    //     'currentTab'      => $currentTab,
    //       'CUST_MODEL' => $compInfo['CUST_MODEL'],
    //       'specialObligee' => $specialObligee,
    //     'lineFacility' => $lineFacility,
    //   ];
    //   $this->_forward('printcustomerdetailcurrent', 'index', 'widget', $params);
    // }

    // Download PKS File Line Facility
    if ($download == 'lineFacility') {
      $path = UPLOAD_PATH . '/document/submit/';
      $file = $lineFacility['PKS_FILE'];
      $this->_helper->download->file($file, $path . $file);
    }

    // Download PKS File Special Obligee
    if ($download == 'specialObligee') {
      $path = UPLOAD_PATH . '/document/submit/';
      $file = $specialObligee['PKS_FILE'];
      $this->_helper->download->file($file, $path . $file);
    }

    Application_Helper_General::writeLog('DAGL', 'Lihat Detail Perusahaan (' . $compInfo['CUST_NAME'] . ')');
  }
}
