<?php
require_once 'Zend/Controller/Action.php';

class bgfeesharing_IndexController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->_layout->setLayout('newlayout');

		$filter_clear = $this->_getParam('filter_clear');

		//get filtering param
		$filterlist = array('BG_NUMBER' => 'BG Number','BG_SUBJECT' => 'Subject', 'RECIPIENT_NAME' => 'Applicant Name', 'BRANCH_NAME' => 'Bank Branch','COUNTER_WARRANTY_TYPE' => 'Counter Type', 'TANGGAL' => 'Issued Date');

		$this->view->filterlist = $filterlist;

		$filterArr = array(
		  'BG_NUMBER'  => array('StringTrim', 'StripTags'),
		  'BG_SUBJECT'  => array('StringTrim', 'StripTags'),
		  'RECIPIENT_NAME'  => array('StringTrim', 'StripTags'),
		  //'BG_AMOUNT'   => array('StringTrim', 'StripTags'),
		  'BRANCH_NAME'   => array('StringTrim', 'StripTags'),
		  'COUNTER_WARRANTY_TYPE'    => array('StringTrim', 'StripTags'),
		  'TIME_PERIOD_START'  => array('StringTrim', 'StripTags'),
		  'TIME_PERIOD_END'   => array('StringTrim', 'StripTags')
		);

		$dataParam = array('BG_NUMBER', 'BG_SUBJECT', 'RECIPIENT_NAME', 'BRANCH_NAME','COUNTER_WARRANTY_TYPE','TIME_PERIOD_START','TIME_PERIOD_END');
		$dataParamValue = array();

		foreach ($dataParam as $dtParam) {
		  if (!empty($this->_request->getParam('wherecol'))) {
			$dataval = $this->_request->getParam('whereval');
			foreach ($this->_request->getParam('wherecol') as $key => $value) {
			  if ($dtParam == $value) {
				if (!empty($dataParamValue[$dtParam])) {
				  $dataval[$key] = $dataParamValue[$dtParam] . ',' . $dataval[$key];
				}
				$dataParamValue[$dtParam] = $dataval[$key];
			  }
			}
		  }
		}

		if($this->_request->getParam('createdate')){
		  $dataParamValue['TIME_PERIOD_START'] = $this->_request->getParam('createdate')[0];
		  $dataParamValue['TIME_PERIOD_END'] = $this->_request->getParam('createdate')[1];
		}

		$options = array('allowEmpty' => true);
		$validators = array(
		  'TIME_PERIOD_START'  => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
		  'TIME_PERIOD_END'   => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
		  'BG_NUMBER' => array(),
		  'BG_SUBJECT' => array(),
		  'RECIPIENT_NAME' => array(),
		  //'BG_AMOUNT' => array(),
		  'BRANCH_NAME' => array(),
		  'COUNTER_WARRANTY_TYPE'=> array()
		);

		$zf_filter  = new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);

		$fbgnumb    = $zf_filter->getEscaped('BG_NUMBER');
		$subject   = $zf_filter->getEscaped('BG_SUBJECT');
		$recipient   = $zf_filter->getEscaped('RECIPIENT_NAME');
		//$fbgmaount   = $zf_filter->getEscaped('BG_AMOUNT');
		$fbranchname   = $zf_filter->getEscaped('BRANCH_NAME');
		$fcounterType   = $zf_filter->getEscaped('COUNTER_WARRANTY_TYPE');
		//

		// $whereIn = [7, 20, 21, 22, 23];
		$whereIn = [15, 16];

		$select = $this->_db->select()
		->from(
			array('A' => 'T_BANK_GUARANTEE'),
			array(
				'BG_REG_NUMBER'			=> 'A.BG_REG_NUMBER',
				'CUST_ID'				=> 'A.CUST_ID',
				'CUST_NAME'				=> 'C.CUST_NAME',
				'BG_NUMBER'				=> 'A.BG_NUMBER',
				'BG_SUBJECT'			=> 'A.BG_SUBJECT',
				'BG_AMOUNT'				=> 'A.BG_AMOUNT',
				'BRANCH_NAME'			=> 'B.BRANCH_NAME',
				'COUNTER_WARRANTY_TYPE'	=> new Zend_Db_Expr("(
					CASE A.COUNTER_WARRANTY_TYPE
					WHEN '1' THEN 'Full Cover'
					WHEN '2' THEN 'Line Facility'
					WHEN '3' THEN 'Insurance'
					ELSE '-'
					END)"),
					// 'BG_ISSUED'				=> 'A.BG_ISSUED',
				'BG_INSURANCE_CODE'		=> 'A.BG_INSURANCE_CODE',
				'PROVISION_FEE'			=> 'A.PROVISION_FEE',
				'STAMP_FEE'				=> 'A.STAMP_FEE',
				"BG_ISSUED" => "A.BG_ISSUED"
			)
		)
		->joinLeft(
			array('B' => 'M_BRANCH'),
			'B.BRANCH_CODE = A.BG_BRANCH',
			array()
		)
		->joinLeft(
			array('SPO' => 'M_CUST_SPOBLIGEE'),
			'SPO.CUST_ID = A.SP_OBLIGEE_CODE',
			array(
				"FEE_SHARING" => "SPO.SHARING_FEE"
			)
		)
		->joinLeft(
			array('C' => 'M_CUSTOMER'),
			'C.CUST_ID = A.CUST_ID',
			array()
		)
		->where('A.SP_OBLIGEE_CODE = ?', $this->_custIdLogin)
		->where('A.BG_STATUS IN (?)', $whereIn);
		// ->where('A.BG_STATUS IN (?)', $whereIn);
		
		if ($fbgnumb) {
		  $select->where("BG_NUMBER LIKE " . $this->_db->quote('%' . $fbgnumb . '%'));
		}
		if ($subject) {
		  $select->where("BG_SUBJECT LIKE " . $this->_db->quote('%' . $subject . '%'));
		}
		if ($recipient) {
		  $select->where("RECIPIENT_NAME LIKE " . $this->_db->quote('%' . $recipient . '%'));
		}
		/*if ($fbgmaount) {
		  $select->where("BG_AMOUNT LIKE " . $this->_db->quote('%' . $fbgmaount . '%'));
		}*/
		if ($fbranchname) {
		  $select->where("BRANCH_NAME LIKE " . $this->_db->quote('%' . strtoupper($fbranchname) . '%'));
		}
		if ($fcounterType) {
		  $select->where("COUNTER_WARRANTY_TYPE = ?", $fcounterType);
		}
		if ($fstatus) {
		  $select->where("BG_STATUS = ?", $fstatus);
		}

		if (!empty($dataParamValue['TIME_PERIOD_START']) && !empty($dataParamValue['TIME_PERIOD_END'])) {

		  $select->where("BG_ISSUED >= ?", date('Y-m-d', strtotime($dataParamValue['TIME_PERIOD_START'])));
		  $select->where("BG_ISSUED >= ?", date('Y-m-d', strtotime($dataParamValue['TIME_PERIOD_END'])));
		}
	
		$select->order('A.BG_CREATED DESC');
		$data = $this->_db->fetchAll($select);
		$this->paging($data);

		// Get data M_BRANCH
		$select = $this->_db->select()
		->from(
			array('MB' => 'M_BRANCH'),
			array('*')
		);
		$dataBranch = $this->_db->fetchAll($select);
		$bankBranchArr = Application_Helper_Array::listArray($dataBranch, 'BRANCH_CODE', 'BRANCH_NAME');

		$counterTypeArr = [
			'1' => 'Full Cover',
			'2' => 'Line Facility',
			'3' => 'Insurance'
		];

		$this->view->filter 		= $filter;
		
		if (!empty($dataParamValue)) {

		  $this->view->createdStart = $dataParamValue['TIME_PERIOD_START'];
		  $this->view->createdEnd = $dataParamValue['TIME_PERIOD_END'];

		  foreach ($dataParamValue as $key => $value) {
			$duparr = explode(',', $value);
			if (!empty($duparr)) {

			  foreach ($duparr as $ss => $vs) {
				$wherecol[] = $key;
				$whereval[] = $vs;
			  }
			} else {
			  $wherecol[] = $key;
			  $whereval[] = $value;
			}

		  }
		  $this->view->wherecol     = $wherecol;
		  $this->view->whereval     = $whereval;
		}
	

		if ($data) {

			$dataAll = [];

			if(!empty($this->_getParam('csv')) || $this->_request->getParam('print') == 1){
				foreach ($data as $key => $row) {
					$subData = [];
					$subData['CUST_NAME'] = strtoupper($row['CUST_NAME']) ?: '-';
					$BG_NUMBER    = strtoupper($row['BG_NUMBER']) ?: '-';
					$BG_SUBJECT   = strtoupper($row['BG_SUBJECT']) ?: '- ' . $this->language->_('NO SUBJECT') . ' -';
					$subData['BG_NUMBER'] = $BG_NUMBER . ' / ' . $BG_SUBJECT;
					$subData['BG_AMOUNT'] = ($row['BG_AMOUNT'] != 0) ? 'IDR ' . Application_Helper_General::displayMoneyplain($row['BG_AMOUNT']) : '-';
					$subData['BRANCH_NAME'] = $row['BRANCH_NAME'] ?: '-';
					$subData['BG_ISSUED'] = $row['BG_ISSUED'] ? date('d M Y', strtotime($row['BG_ISSUED'])) : '-';
					$subData['PROVISION_FEE'] = ($row['PROVISION_FEE'] != 0) ? 'IDR ' . Application_Helper_General::displayMoneyplain($row['PROVISION_FEE']) : '-';
					$subData['FEE_SHARING'] = ($row['FEE_SHARING'] != 0) ? 'IDR ' . number_format(($row["FEE_SHARING"] / 100) * ($row["PROVISION_FEE"] + 0), 2) : '-';
					if (empty($row['BG_INSURANCE_CODE'])) {
						$subData['COUNTER_WARRANTY_TYPE'] = strtoupper($row['COUNTER_WARRANTY_TYPE']);
					} else {
						$subData['COUNTER_WARRANTY_TYPE'] = strtoupper($row['COUNTER_WARRANTY_TYPE']) . ' (' . $row['BG_INSURANCE_CODE'] . ')';
					}
					$dataAll[] = $subData;
				}
			}

			
			if ($this->_getParam('csv')) {

				$this->_helper->download->csv(array($this->language->_('Applicant Name'), $this->language->_('BG Number / Subjek Transaksi'), $this->language->_('BG Amount'), $this->language->_('Bank Cabang'), $this->language->_('Counter Type'), $this->language->_('Issued Date'), $this->language->_('Provision Charge'), $this->language->_('Fee Sharing')), $dataAll, null, 'BG Fee Sharing');
			} else if ($this->_request->getParam('print') == 1) {

				$fields = array(
					'custname'     => array(
						'field'    => 'CUST_NAME',
						'label'    => $this->language->_('Applicant Name'),
					),
					'number'     => array(
						'field'    => 'BG_NUMBER',
						'label'    => $this->language->_('BG Number / Subjek Transaksi'),
					),
					'bgamount'  => array(
						'field'    => 'BG_AMOUNT',
						'label'    => $this->language->_('BG Amount'),
					),
					'branch'  => array(
						'field'    => 'BRANCH_NAME',
						'label'    => $this->language->_('Branch'),
					),
					'countertype'  => array(
						'field'    => 'COUNTER_WARRANTY_TYPE',
						'label'    => $this->language->_('Counter Type'),
					),
					'bgisued'  => array(
						'field'    => 'BG_ISSUED',
						'label'    => $this->language->_('Issued Date'),
					),
					'provisionfee'  => array(
						'field'    => 'PROVISION_FEE',
						'label'    => $this->language->_('Provision Charge'),
					),
					'feesharing'  => array(
						'field'    => 'FEE_SHARING',
						'label'    => $this->language->_('Fee Sharing'),
					),
				);

				$this->_forward('print', 'index', 'widget', array('data_content' => $dataAll, 'data_caption' => 'BG Fee Sharing', 'data_header' => $fields));
			}
		}

		Application_Helper_General::writeLog('FSBG', 'View BG Fee Sharing');
	}
	
	public function getwarantyAction()
	  {
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$tblName = $this->_getParam('id');

		$optHtml = "<option value=''>-- ".$this->language->_('Please Select')." --</option>";

		$arrWarrantyType = array(
		  '1' => 'Full Cover',
		  '2' => 'Line Facility',
		  '3' => 'Insurance'
		);

		foreach($arrWarrantyType as $key => $row){
		  if($tblName==$key){
			$select = 'selected';
		  }else{
			$select = '';
		  }
		  $optHtml.="<option value='".$key."' ".$select.">".$row."</option>";
		}

		echo $optHtml;
	  }
}
