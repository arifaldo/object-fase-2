<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';
require_once 'General/CustomerUser.php';
require_once 'CMD/Validate/ValidateAccountBeneficiary.php';
require_once 'Service/Token.php'; //added new
require_once 'CMD/Validate/Validate.php';

class requestspecialrate_CreateController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti
	
	protected $_destinationUploadDir = '';
	protected $_listCCYValidate = '';

	public function initController()
	{       
		$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/'; 	
		$this->_listCCYValidate = Application_Helper_Array::simpleArray($this->getCcy(),'CCY_ID');  
		$selectCurrency = '-- '.$this->language->_('Select Currency'). '--';
		$listCcy = array(''=>$selectCurrency);
		if (count($this->getCcy()) == 1){ //remove useless value
			$listCcy = array();
		}
		$listCcy = array_merge($listCcy,Application_Helper_Array::listArray($this->getCcy(),'CCY_ID','CCY_ID'));
		$this->view->ccy = $listCcy;
        		
	}
	
	public function indexAction()
	{		

		$this->_helper->layout()->setLayout('newlayout');
		
		$Settings = new Settings();
		$privibenelinkage = $this->view->hasPrivilege('BLBU');
		$filter  = new Application_Filtering();
		$settings = new Application_Settings();
		$ccyList  = $settings->setCurrencyRegistered();		
		//Zend_Debug::dump($ccyList);die;
		
		$this->view->userIdLogin  = $this->_userIdLogin;
		
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr = $CustomerUser->getAccounts();
		$this->view->AccArr = $AccArr;
        
        $getCcy = $this->getCcy();
        $this->view->CCYData = $getCcy;
        // var_dump($CCYData);

        $this->view->request_id = $this->generateTransactionID();

        if(!$this->_request->isPost() && $this->_getParam('isback'))
		{
			$sessionNamespace = new Zend_Session_Namespace('requestspecialrate');
			$content = $sessionNamespace->content;			
			$this->fillParams($content['request_id'],$content['fromccy'],$content['toccy'],$content['amount']);
			//$this->view->ACBENEF_NAME = $content['BENEFICIARY_NAME'];
			unset($_SESSION['requestspecialrate']); 
		}
		
		if($this->_request->isPost() )
		{
			$REG_NUM 		= $this->_getParam('request_id');
			$FROM_CCY 		= $this->_getParam('fromccy');
			$TO_CCY 	= $this->_getParam('toccy');			
			$AMOUNT = $this->_getParam('amount');
			$CCY_AMOUNT  = $this->_getParam('amountccy');
			
	
			$dataValidate =  array(					
				'request_id' => $REG_NUM, 
				'fromccy' => $FROM_CCY, 
				'toccy' => $TO_CCY,
				'amount' => $AMOUNT,
				'amountccy' => $CCY_AMOUNT 
			);
			
			$resultValidate = $this->validateField($dataValidate);
						 
			//print_r($paramBene);die;
			 if($resultValidate===true){
			 	
			 	// $accData = $this->_db->select()
			    //    ->from('M_CUSTOMER_ACCT')
			    //    ->where('ACCT_NO = ?', $ACCTSRC)
			    //    ->where('CUST_ID = ?', (string)$this->_custIdLogin);
			       
			    // $accData = $this->_db->fetchRow($accData);
			    
			    // $ACCTSRC_NAME 	= $accData['ACCT_NAME'];
			    // $ACCTSRC_CCY 	= $accData['CCY_ID'];
                
				$content = array(
								'CUST_ID_LOGIN' 			=> $this->_custIdLogin,
								'USER_ID_LOGIN' 			=> $this->_userIdLogin,								
								'REG_NUM' 					=> $REG_NUM,
								'FROM_CCY' 					=> $FROM_CCY,
								'TO_CCY' 				    => $TO_CCY,
								'CCY_AMOUNT' 			    => $CCY_AMOUNT ,
								'AMOUNT' 				    => $AMOUNT
					);
					
					// var_dump($content);die;
					/*$content['paramAccount'] = $responseData;
					$content['payment'] = $payment;*/
					
			 //validasi hard token page 1 -- begin
					/*$message = $Settings->getSetting('token_confirm_message');
					$trans = array("[Response_Code]" => $responseCode);
					$message = strtr($message, $trans);
					
					if($tokenType == '1'){ //sms token
						if(empty($responseCodeReq)){
							$resultToken = FALSE;
							$this->view->error = true;
							$errMessage = 'Error : Response Token cannot be left blank.';
							$this->view->report_msg = $errMessage;
						}
						else
						{
							$token = new Service_Token(NULL,$this->_userIdLogin);
							$token->setMsisdn($usermobilephone);
							$token->setMessage($message);

							$res = $token->confirm();
							$resultToken = $res['ResponseCode'] == '0000';
						}
					}
					elseif($tokenType == '2'){ //hard token
						
						if(empty($responseCodeReq)){
							$resultToken = FALSE;
							$this->view->error = true;
							$errMessage = 'Error : Response Token cannot be left blank.';
							$this->view->report_msg = $errMessage;
						}
						else
						{
							
							$resHard = $HardToken->verifyHardToken($challengeCodeReq2.$challengeCodeReq, $responseCodeReq);
							$resultToken = $resHard['ResponseCode'] == '0000';
							
							//set user lock token gagal
							$CustUser = new CustomerUser($this->_custId, $this->_userIdLogin);							
							if ($resHard['ResponseCode'] != '0000'){
								$tokenFailed = $CustUser->setFailedTokenMustLogout();
								if ($tokenFailed)
									$this->_forward('home');
							}
						}
					}
					elseif($tokenType == '3'){ //mobile token
						$resultToken = TRUE;
					}
					else{$resultToken = TRUE;}*/
					//validasi hard token page 1 -- end	
					
					//if($resultToken == TRUE){	
						//print_r($content);die;
						$sessionNamespace = new Zend_Session_Namespace('requestspecialrate');
						$sessionNamespace->mode = 'Add';
						$sessionNamespace->content = $content;
						$this->_redirect('/requestspecialrate/create/confirm');
					//}
					/*else{
						//echo $resultToken."aaa"; die;
						$this->view->ACBENEF = $ACBENEF;
						$this->view->CURR_CODE = $CURR_CODE;
						$this->view->ACBENEF_EMAIL = $ACBENEF_EMAIL;
						
						if($resultToken == FALSE){
							if($res['ResponseCode'] == 'XT'){
								$errMessage = 'Error : Service Rejected';
								$this->view->resulttoken = $errMessage;
							}
							else{								
								$errMessage = 'Error : Invalid Token';
								$this->view->resulttoken = $errMessage;
							}
						}
					}*/
					
					/*$sessionNamespace = new Zend_Session_Namespace('requestspecialrate');
					$sessionNamespace->mode = 'Add';
					$sessionNamespace->content = $content;
					$this->_redirect('/predefinedbeneficiary/inhouseacct/confirm');*/
				
			}
			else
			{			
				 	
				$this->view->error = true;
				$docErr = $resultValidate->getMessages();
				$this->view->report_msg = $docErr;				
			}
		}
		if(!$this->_request->isPost()){
		Application_Helper_General::writeLog('BADA','Viewing Add Beneficiary Account');
		}

    }

    public function confirmAction()
	{
		
		$sessionNamespace = new Zend_Session_Namespace('requestspecialrate');
		$content = $sessionNamespace->content;
//		Zend_Debug::dump($content);
		//print_r($content);die;
		$mode = $sessionNamespace->mode;
		
		$this->view->mode = $mode;
		$this->fillParams($content['CUST_ID_LOGIN'],$content['USER_ID_LOGIN'],$content['REG_NUM'],$content['FROM_CCY'],$content['TO_CCY'],$content['AMOUNT'],$content['CCY_AMOUNT']);
		
		
		if($this->_custSameUser){
			// echo 'here1';
			$this->view->token = true;
			$userOnBehalf = $this->_userIdLogin;
			$tokenIdUser = $this->_db->select()
			->from(
				array('M_USER'),
				array('TOKEN_ID')
			)
			->where('USER_ID = ?',$userOnBehalf)
			->where('CUST_ID = ?',$this->_custIdLogin)
			->limit(1)
		;
		$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$tokenIdUser = $this->_db->fetchRow($tokenIdUser);
		$tokenIdUser = $tokenIdUser['TOKEN_ID'];

			$Token 			= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
			$challengeCode 	= $Token->generateChallengeCode();
			$this->view->userOnBehalf		= $userOnBehalf;
			$this->view->challengeCode		= $challengeCode;
			// $this->view->custsame 			= true;
		}
		
		//validasi token -- begin
		$select3 = $this->_db->select()
					 ->from(array('C' => 'M_USER'));
		$select3->where("USER_ID = ".$this->_db->quote($this->_userIdLogin));
		$data2 					= $this->_db->fetchRow($select3);
		$tokenIdUser 			= $data2['TOKEN_ID'];
		$tokenType 				= $data2['TOKEN_TYPE'];
		$usermobilephone 		= $data2['USER_MOBILE_PHONE'];
		$this->view->tokentype 	= $data2['TOKEN_TYPE'];

		
		if($this->_request->isPost() )
		{	
			
            // if($this->_getParam('submit')=='Back')
			// {
				
			// 	if($mode=='Add'){					
			// 		$this->_redirect('/requestspecialrate/create/index/isback/1');
			// 	}elseif ($mode=='Edit'){
			// 		$this->_redirect('/requestspecialrate/edit/index/benef_id/'.$content['BENEFICIARY_ID'].'/isback/1');	
			// 	}
			// }
			// else
			// {
				try 
				{
					//$Beneficiary = new Beneficiary();
					// $reqbook		= new requestbook_Model_Requestbook();
					$this->_db->beginTransaction();					
                    $data = array(	'CUST_ID' => $content['CUST_ID_LOGIN'],
                                    'RATE_REQUESTBY' => $content['USER_ID_LOGIN'],
                                    'ID_REQUEST' => $content['REG_NUM'],
                                    'FROM_CCY' => $content['FROM_CCY'],
                                    'TO_CCY' => $content['TO_CCY'],
                                    'CCY_AMOUNT' => $content['CCY_AMOUNT'],
                                    'AMOUNT_TRX' =>Application_Helper_General::convertDisplayMoney($content['AMOUNT']),
                                    'VALID_DATE' =>new Zend_Db_Expr("now()"),
                                    'RATE_REQUESTED' =>new Zend_Db_Expr("now()"),
                                    'STATUS' =>'2',
									'ACTION' => 'C'
                                );
                                // var_dump($data);die;
                    $this->_db->insert('T_REQ_SPCRATE',$data);
					
                    $historyInsert = array(
                        'ID_REQUEST'         => $content['REG_NUM'],
                        'CHANGES_FLAG'         => 'F',
                        'CUST_ID'           => $this->_custIdLogin,
                        'CREATEDBY'        => $this->_userIdLogin,
                        'RATE_DATETIME'    => new Zend_Db_Expr("now()"),
						'ACTION' => 'C'
                    );

                    $this->_db->insert('T_HISTORY_REQRATE', $historyInsert);
                    $this->_db->commit();
					
					$this->setbackURL('/'.$this->_request->getModuleName().'/index/');
					$this->_redirect('/notification/success');
				}
				catch(Exception $e) 
				{   
                    var_dump($e);die;
					$this->_db->rollBack();
					
				}
			// }
		}
	}

    private function fillParams($CUST_ID_LOGIN,$USER_ID_LOGIN,$REG_NUM,$FROM_CCY,$TO_CCY,$AMOUNT,$CCY_AMOUNT)
	{
		//if($BENEFICIARY_ID) $this->view->BENEFICIARY_ID = $BENEFICIARY_ID;
		$this->view->CUST_ID_LOGIN 	= $CUST_ID_LOGIN;
		$this->view->USER_ID_LOGIN 	= $USER_ID_LOGIN;
		$this->view->REG_NUM 		= $REG_NUM;
		$this->view->FROM_CCY 	= $FROM_CCY;
		$this->view->TO_CCY = $TO_CCY;
		$this->view->AMOUNT 		= $AMOUNT;
		$this->view->CCY_AMOUNT 		= $CCY_AMOUNT;
	}

    private function validateField($data){
											
		$filters = array(
			'request_id' 		=> array('StripTags','StringTrim'), 			
			'fromccy' 		=> array('StripTags','StringTrim'), 					
			'toccy'		=> array('StripTags','StringTrim'),
			'amount'		=> array('StripTags','StringTrim'),
			'amountccy'		=> array('StripTags','StringTrim')
		);
		
		$validators = array(
			'request_id' => array(
				'NotEmpty',
				/*'Digits',
				new Zend_Validate_StringLength(array('max'=>10,'min'=>10)),*/
				'messages' => array(
					'Source Account cannot be left blank. Please correct it.',
					/*'Beneficiary Account must be numbers. Please correct it.',
					'Beneficiary Account length must be 10 digits. Please correct it.',*/
					)											   					   
			),
						
			'fromccy' => 	array(	
				'NotEmpty',
				//new Zend_Validate_Alnum(),
				//new Zend_Validate_StringLength(array('min'=>3,'max'=>3)),
				//array('InArray', array('haystack' => $this->_listCCYValidate)),
				'messages' => array	(
					'Please correct it.',
					//'04',
					//'04',
					//'Currency is not on the database.'
				)
			),
			
			'toccy' => array(
				'NotEmpty',
				/*'Digits',
				new Zend_Validate_StringLength(array('max'=>10,'min'=>10)),*/
				'messages' => array(
					'Please correct it.',
					/*'Beneficiary Account must be numbers. Please correct it.',
					'Beneficiary Account length must be 10 digits. Please correct it.',*/
					)											   					   
			),

			'amountccy' => array(
				'NotEmpty',
				/*'Digits',
				new Zend_Validate_StringLength(array('max'=>10,'min'=>10)),*/
				'messages' => array(
					'Please correct it.',
					/*'Beneficiary Account must be numbers. Please correct it.',
					'Beneficiary Account length must be 10 digits. Please correct it.',*/
					)											   					   
			),

			'amount' => array(
				'NotEmpty',
				/*'Digits',
				new Zend_Validate_StringLength(array('max'=>10,'min'=>10)),*/
				'messages' => array(
					'Amount cannot be left blank. Please correct it.',
					/*'Beneficiary Account must be numbers. Please correct it.',
					'Beneficiary Account length must be 10 digits. Please correct it.',*/
					)											   					   
			),
		);
		
		$zf_filter_input = new Zend_Filter_Input($filters,$validators,$data);
		if($zf_filter_input->isValid()) {
			return true;
		}else{
			return $zf_filter_input;
		}
	}

    public function generateTransactionID(){

        $currentDate = date("Ymd");
        $seqNumber   = strtoupper(Application_Helper_General::str_rand(6));
        $trxId = 'C'.$currentDate.$seqNumber;

        return $trxId;
    }

}