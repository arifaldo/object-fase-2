<?php

class virtualpayment_ViewController extends Application_Main
{
	protected $_titleCaption = 'Virtual Payment';
	
	
	public function initController()
	{
		$this->view->titleCaption = $this->_titleCaption;
	}
	
	public function indexAction (){
		$this->view->modulename = $this->_request->getModuleName();
		$this->view->controllername = $this->_request->getControllerName();
		$this->view->actionname = $this->_request->getActionName();
		
		$this->view->user = $this->_userIdLogin;
		
		$paymentReff = $this->_getParam('reff');
		$history = $this->_getParam('history');
		$this->view->history = $history;
			
//		$select1 = $this->_db->select()
//			->from(array('XPD'=>'T_VP_DETAIL'))
//			->where('XPD.VP_NUMBER = ?', $paymentReff)
//			//->joinLeft(array('XBOU'=>'X_BILLER_OFFLINE_UPLOAD'), 'XBOU.INVOICE = XPD.INVOICE_NO')
//			->limit(1)
//		;
//		$data1 = $this->_db->fetchRow($select1);
//		//Zend_Debug::dump($data1);
//		$invoice_no = $data1['INVOICE_NO'];	
	
		
		$data = array();
		
		//$params = $this->_request->getParams();
//		$filter = array(
//				'*'	=> array('StringTrim','StripTags'),
//		);
//		$options = array('allowEmpty' => false);
//		$validators = array(
//			'id' => array('alnum','presence' => 'required'),
//		);
//		
//		$filter  = new Zend_Filter_Input($filter, $validators, $params, $options);
		
		//if ($filter->isValid()){
					
			//$id = $filter->getEscaped('id');
		if($history){
			$select = $this->_db->select()
				->from(array('X'=>'T_VP_TRANSACTION_HISTORY'))
				->where('X.VP_NUMBER = ?', $paymentReff)
				->joinLeft(array('B'=>'M_CUSTOMER'), 'X.CUST_ID = B.CUST_ID')
				->limit(1)
			;
			$data = $this->_db->fetchRow($select);
		}	
		else{		
			$select = $this->_db->select()
				->from(array('X'=>'T_VP_TRANSACTION'))
//				->where('X.VP_NUMBER = ?', $paymentReff)
				->where('X.VP_TRANSACTION_ID = ?', $paymentReff)
				->joinLeft(array('B'=>'M_CUSTOMER'), 'X.CUST_ID = B.CUST_ID')
				->limit(1)
			;
			$data = $this->_db->fetchRow($select);
		//}
		}
		
		$this->view->data = $data;	
		
		$effectiveDate = $data['VP_EFDATE'];			
		$daysadd = "+".$data['DUE_DATE']." days";	
		$Duedate = date('Y-m-d', strtotime($daysadd, strtotime($effectiveDate)));
		$this->view->Duedate = $Duedate;
		
		
		$optDay = array(
				//'' => '-- Please Select --',
				'' => 'No',
				'0' => $this->language->_('Monday'),
				'1' => $this->language->_('Tuesday'),
				'2' => $this->language->_('Wednesday'),
				'3' => $this->language->_('Thursday'),
				'4' => $this->language->_('Friday'),
				'5' => $this->language->_('Saturday'),
				'6' => $this->language->_('Sunday')
		);
		$this->view->optDay 		= $optDay;

		$optNotif = array(
				//'' => '-- Please Select --',
				'' => 'No',
				'0' => $this->language->_('15 Minutes'),
				'1' => $this->language->_('30 Minutes'),
				'2' => $this->language->_('1 Hours'),
				'3' => $this->language->_('2 Hours'),
				'4' => $this->language->_('1 Days'),
				'5' => $this->language->_('2 Days'),
				'6' => $this->language->_('1 Weeks'),
				'7' => $this->language->_('1 Months')
		);
		$this->view->optNotif 		= $optNotif;

		$optRepeat = array(
				//'' => '-- Please Select --',
				'0' => $this->language->_('None'),
				'1' => $this->language->_('Daily'),
				'2' => $this->language->_('Weekly'),
				'3' => $this->language->_('Monthly'),
				'4' => $this->language->_('Yearly')
		);
		$this->view->optRepeat 		= $optRepeat;

		$optVaPayment = array(
				//'' => '-- Please Select --',
				'1' => $this->language->_('Remain Open'),
				'2' => $this->language->_('Close')
		);
	
		$this->view->optVapPayment 		= $optVaPayment;

		$optVaType = array(
				//'' => '-- Please Select --',
				'0' => $this->language->_('No Inquiry'),
				'1' => $this->language->_('Inquiry')
		);
	
		$this->view->optVaType 		= $optVaType;


		$optPaid = array(
				//'' => '-- Please Select --',
				'N' => $this->language->_('No'),
				'Y' => $this->language->_('Yes')
		);
	
		$this->view->optPaid 		= $optPaid;

		
		$optSync = array(
				//'' => '-- Please Select --',
				'' => 'No',
				'1' => $this->language->_('Paid'),
				'2' => $this->language->_('Unpaid'),
				'3' => $this->language->_('Delete'),
		);

		$optStatus = array(
				//'' => '-- Please Select --',
				'' => 'No',
				'1' => $this->language->_('Open'),
				'2' => $this->language->_('Postpone'),
				'3' => $this->language->_('Expired'),
				'4' => $this->language->_('Close')
		);
		$this->view->optSync 		= $optSync;
		$this->view->optStatus 		= $optStatus;

		$pdf = $this->_getParam('pdf');
		$pdfTran = $this->_getParam('pdfTran');
		$cancelVirtualPayment = $this->_getParam('cancelVirtualPayment');
		
		
		//Virtual Payment Bill Detail
		$optStatus = array(
        //'' => '-- Please Select --',
                '1' => $this->language->_('No'),
                '0' => $this->language->_('Yes')
              );

              $optBill = array(
        //'' => '-- Please Select --',
                '0' => $this->language->_('Billing'),
                '1' => $this->language->_('No BIlling')
              );

              $optVA = array(
        //'' => '-- Please Select --',
                '0' => $this->language->_('Static'),
                '1' => $this->language->_('Dynamic')
              );

              $optdue = array(
        //'' => '-- Please Select --',
                '0' => $this->language->_('Open'),
                '1' => $this->language->_('Close')
              );



              $opttype = array(
        //'' => '-- Please Select --',
                '0' => $this->language->_('Close'),
                '1' => $this->language->_('Payment'),
                '2' => $this->language->_('Open Payment'),
                
              );

               $optpayment = array(
        //'' => '-- Please Select --',
                '0' => $this->language->_('Yes'),
                '1' => $this->language->_('No'),
                '2' => $this->language->_('Over'),
                
              );
          
      
      // die;
//       $select = $this->_db->select()
//                           ->from(array('a'=> 'M_CUSTOMER_BIN'))
//                           ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
//                           ->query()->fetch();
                           
	  					 $resultdata = $this->_db->select()
  	                         ->from(array('A' => 'M_CUSTOMER_BIN'),array('*'))
//						
							 ->joinLeft(array('C' => 'M_CUSTOMER'),'A.CUST_ID = C.CUST_ID',array('CUST_NAME','CUST_CIF'))
                           	 ->where('A.CUST_ID = ? ',$this->_custIdLogin)
                           	 ->where('A.CUST_BIN = ?','1234')
							 ->query()->fetchAll();
							 // echo $resultdata;die;
							
            // $resultdata = $this->getCustomer($this->_custIdLogin,'1234');
              if(!empty($resultdata)){
              	$resultdata = $resultdata['0'];
              }else{
              	$resultdata = array();
              }
              
              $this->view->stats_due   = $optdue[$resultdata['DUEDATE_STATUS']];
              $this->view->vp_max   = $resultdata['VP_MAX'];
              $this->view->vp_min   = $resultdata['VP_MIN'];
              $this->view->seller_fee   = isset($resultdata['SELLER_FEE']) ? $resultdata['SELLER_FEE'] : "-";
              $this->view->buyer_fee   = isset($resultdata['BUYER_FEE']) ? $resultdata['BUYER_FEE'] : "-";
              $this->view->cal_invo   = $optStatus[$resultdata['CAL_INVOICE']];
              $this->view->cal_depo   = $optStatus[$resultdata['CAL_DEPO']];
              $this->view->partial_type   = $resultdata['PARTIAL_TYPE'];
              $this->view->unique   = $optStatus[$resultdata['UNIQUE']];
              $this->view->partial_type   = $opttype[$resultdata['PARTIAL_TYPE']];
              $this->view->partial_payment   = $optpayment[$resultdata['PARTIAL_PAYMENT']];
              $this->view->va_type   = $optVA[$resultdata['VA_TYPE']];
              $this->view->amount_type   = $optBill[$resultdata['AMOUNT_TYPE']];
              $this->view->ccy   = $resultdata['CCY'];
              $this->view->cif   = $resultdata['CUST_CIF'];
              $this->view->bank   = $resultdata['BANK_NAME'];
              $this->view->branch   = $resultdata['BRANCH_NAME'];
              $this->view->account_alias   = $resultdata['ACCT_ALIAS_NAME'];
              $this->view->account   = $resultdata['ACCT_NO'];
              $this->view->account_name   = $resultdata['ACCT_NAME'];
              
           
		//Virtual Payment Bill Detail
		
			$selectAll = $this->_db->select()
//				->from(array('A' => 'T_VP_TRANSACTION'),array('VP_NUMBER','BANK_REFNO','CUST_ID','MU.CUST_NAME','CUST_BIN','INVOICE_NAME','INVOICE_DESC','CCY','AMOUNT','PAID_STATUS','DEVICE_ID','VP_CREATED','VP_PAID_DATE','VP_MODE'));
				->from(array('MU' => 'T_VP_TRANSACTION_DETAIL'),array());
//				->from(array('A' => 'T_VP_DETAIL'),array('A.VP_NUMBER','A.INSTITUTIONS_ID','A.INVOICE_NO','A.INVOICE_NAME','A.AMOUNT','A.VP_EFDATE','MB.PAID_STATUS','MB.PAID_FROM'));
			$selectAll->joinLeft(array('A'=>'T_VP_TRANSACTION'), 'A.VP_NUMBER = MU.VP_NUMBER AND A.VP_TRANSACTION_ID= MU.VP_TRANSACTION_ID', array('A.VP_TRANSACTION_ID','A.VP_NUMBER','A.INVOICE_NAME','A.INVOICE_DESC','A.CCY','MU.AMOUNT','MU.PS_NUMBER','A.CUST_BIN','MU.VP_CREATED','A.VP_STATUS','A.NOTIFICATION_SMS','A.NOTIFICATION_EMAIL','A.VP_EFDATE','A.DUE_DATE','A.PAID_STATUS','A.VP_CREATEDBY'));	
			$selectAll->joinLeft(array('B' => 'M_CUSTOMER_BIN'),'B.CUST_ID = A.CUST_ID',array('B.BIN_NAME'));
			$selectAll->joinLeft(array('C' => 'M_CUSTOMER'),'C.CUST_ID = A.CUST_ID',array('C.CUST_NAME','C.CUST_ID'));
			$selectAll->where('MU.VP_TRANSACTION_ID = ?', $paymentReff);
			// echo $select;die;
			$selectAll->group('MU.PS_NUMBER');
			$dataall = $this->_db->fetchAll($selectAll);
			$no = 0;
			$total = 0;
			foreach ($dataall as $key => $value) {
				$total = $total+$value['AMOUNT'];
				$no++;
			}
			$this->view->total = $no;
			$this->view->totalamount = $total;
//		if($pdf)
//		{
//			$datapdf = SGO_Helper_GeneralFunction::formatPurchaseNote($paramSession['PSNumber'], $paramSession);
//			$this->_helper->download->pdf(null,null,null,'Payment Report Detail',$datapdf);
//		}

		$this->view->pdf 		= ($pdf)? true: false;
		
		if($pdf)
		{
			$outputHTML = "<tr><td>".$this->view->render('/view/index.phtml')."</td></tr>";
			$this->_helper->download->pdf(null,null,null,'Virtual Payment Detail',$outputHTML);
			Application_Helper_General::writeLog('VVPD','Download PDF Virtual Payment Detail Report');
		}
		
		if($cancelVirtualPayment)
		{
			$paymentData = array(
									'PAID_STATUS' => 3,
									'VP_UPDATED' => new Zend_Db_Expr("now()")
								);

			$whereData 	 = array(
								 'VP_NUMBER = ?' => (string) $data['VP_NUMBER']
//								 'CUST_ID = ?'	 => (string) $this->_custIdLogin
								);

			$this->_db->update('T_VP_TRANSACTION', $paymentData, $whereData);
			
			////////////////////////////////////////////////////////////////////////
			
//			$paymentDataHistory = array(
//									'PAID_STATUS' => 3,
//									'VP_UPDATED' => new Zend_Db_Expr("now()")
//								);
//
//			$whereDataHistory 	 = array(
//								 'VP_NUMBER = ?' => (string) $data['VP_NUMBER']
////								 'CUST_ID = ?'	 => (string) $this->_custIdLogin
//								);
//
//			$this->_db->update('T_VP_TRANSACTION_HISTORY', $paymentDataHistory, $whereDataHistory);
//			
			
			
//			$this->_redirect($this->_backURL);
			$ns = new Zend_Session_Namespace('FVC');
			$ns->backURL = '/virtualpayment/detail';
			$this->_redirect('/notification/success/index');
		}
		
		if($this->_request->getParam('printtrxxx') == 1){
			$this->_forward('printtrxxx', 'index', 'widget', array('psnumber' => $paramSession['PSNumber'], 'param' => $paramSession));
		}
	
	}


	public function editAction (){
		$this->view->modulename = $this->_request->getModuleName();
		$this->view->controllername = $this->_request->getControllerName();
		$this->view->actionname = $this->_request->getActionName();
		
		$this->view->user = $this->_userIdLogin;
		
		$paymentReff = $this->_getParam('reff');
		$history = $this->_getParam('history');
		$this->view->history = $history;
			
//		$select1 = $this->_db->select()
//			->from(array('XPD'=>'T_VP_DETAIL'))
//			->where('XPD.VP_NUMBER = ?', $paymentReff)
//			//->joinLeft(array('XBOU'=>'X_BILLER_OFFLINE_UPLOAD'), 'XBOU.INVOICE = XPD.INVOICE_NO')
//			->limit(1)
//		;
//		$data1 = $this->_db->fetchRow($select1);
//		//Zend_Debug::dump($data1);
//		$invoice_no = $data1['INVOICE_NO'];	
	
		
		$data = array();
		
		//$params = $this->_request->getParams();
//		$filter = array(
//				'*'	=> array('StringTrim','StripTags'),
//		);
//		$options = array('allowEmpty' => false);
//		$validators = array(
//			'id' => array('alnum','presence' => 'required'),
//		);
//		
//		$filter  = new Zend_Filter_Input($filter, $validators, $params, $options);
		
		//if ($filter->isValid()){
					
			//$id = $filter->getEscaped('id');
		if($history){
			$select = $this->_db->select()
				->from(array('X'=>'T_VP_TRANSACTION_HISTORY'))
				->where('X.VP_NUMBER = ?', $paymentReff)
				->joinLeft(array('B'=>'M_CUSTOMER'), 'X.CUST_ID = B.CUST_ID')
				->limit(1)
			;
			$data = $this->_db->fetchRow($select);
		}	
		else{		
			$select = $this->_db->select()
				->from(array('X'=>'T_VP_TRANSACTION'))
				->where('X.VP_NUMBER = ?', $paymentReff)
				->joinLeft(array('B'=>'M_CUSTOMER'), 'X.CUST_ID = B.CUST_ID')
				->limit(1)
			;
			$data = $this->_db->fetchRow($select);
		//}
		}
		// print_r($data);die;
		$this->view->data = $data;	
		
		$optDay = array(
				//'' => '-- Please Select --',
				'' => 'No',
				'0' => $this->language->_('Monday'),
				'1' => $this->language->_('Tuesday'),
				'2' => $this->language->_('Wednesday'),
				'3' => $this->language->_('Thursday'),
				'4' => $this->language->_('Friday'),
				'5' => $this->language->_('Saturday'),
				'6' => $this->language->_('Sunday')
		);
		$this->view->optDay 		= $optDay;

		$optNotif = array(
				//'' => '-- Please Select --',
				'' => 'No',
				'0' => $this->language->_('15 Minutes'),
				'1' => $this->language->_('30 Minutes'),
				'2' => $this->language->_('1 Hours'),
				'3' => $this->language->_('2 Hours'),
				'4' => $this->language->_('1 Days'),
				'5' => $this->language->_('2 Days'),
				'6' => $this->language->_('1 Weeks'),
				'7' => $this->language->_('1 Months')
		);
		$this->view->optNotif 		= $optNotif;

		$optRepeat = array(
				//'' => '-- Please Select --',
				'0' => $this->language->_('None'),
				'1' => $this->language->_('Daily'),
				'2' => $this->language->_('Weekly'),
				'3' => $this->language->_('Monthly'),
				'4' => $this->language->_('Yearly')
		);
		$this->view->optRepeat 		= $optRepeat;

		$optVaPayment = array(
				//'' => '-- Please Select --',
				'1' => $this->language->_('Remain Open'),
				'2' => $this->language->_('Close')
		);
	
		$this->view->optVapPayment 		= $optVaPayment;

		$optVaType = array(
				//'' => '-- Please Select --',
				'0' => $this->language->_('No Inquiry'),
				'1' => $this->language->_('Inquiry')
		);
	
		$this->view->optVaType 		= $optVaType;

			$optPaid = array(
				//'' => '-- Please Select --',
				'N' => $this->language->_('No'),
				'Y' => $this->language->_('Yes')
		);
	
		$this->view->optPaid 		= $optPaid;


		$optSync = array(
				//'' => '-- Please Select --',
				'' => 'No',
				'1' => $this->language->_('Paid'),
				'2' => $this->language->_('Unpaid'),
				'3' => $this->language->_('Delete'),
		);

		$optStatus = array(
				//'' => '-- Please Select --',
				'' => 'No',
				'1' => $this->language->_('Open'),
				'2' => $this->language->_('Postpone'),
				'3' => $this->language->_('Close'),
				'3' => $this->language->_('Paid')
		);
		$this->view->optSync 		= $optSync;
		$this->view->optStatus 		= $optStatus;

		$pdf = $this->_getParam('pdf');
		$pdfTran = $this->_getParam('pdfTran');
		$cancelVirtualPayment = $this->_getParam('cancelVirtualPayment');
		
//		if($pdf)
//		{
//			$datapdf = SGO_Helper_GeneralFunction::formatPurchaseNote($paramSession['PSNumber'], $paramSession);
//			$this->_helper->download->pdf(null,null,null,'Payment Report Detail',$datapdf);
//		}

		$this->view->pdf 		= ($pdf)? true: false;
		
		if($pdf)
		{
			$outputHTML = "<tr><td>".$this->view->render('/view/index.phtml')."</td></tr>";
			$this->_helper->download->pdf(null,null,null,'Virtual Payment Detail',$outputHTML);
			Application_Helper_General::writeLog('VVPD','Download PDF Virtual Payment Detail Report');
		}

		if($this->_request->isPost() )
		{		

			$reff 				= $this->_getParam('reff');
			$remain_notif 					= $this->_getParam('remain_notif');	
			$NOTIFICATION_EMAIL 					= $this->_getParam('NOTIFICATION_EMAIL');	
			$NOTIFICATION_SMS 					= $this->_getParam('NOTIFICATION_SMS');	
			


			$paymentData = array(
									'NOTIFICATION_EMAIL' => $NOTIFICATION_EMAIL,
									'NOTIFICATION_SMS' => $NOTIFICATION_SMS,
									'REPEAT_EVERY' => $remain_notif,
									'VP_UPDATED' => new Zend_Db_Expr("now()"),
									'VP_UPDATEBY' => $this->_userIdLogin
								);

			$whereData 	 = array(
								 'VP_NUMBER = ?' => (string) $reff
//								 'CUST_ID = ?'	 => (string) $this->_custIdLogin
								);

			$this->_db->update('T_VP_TRANSACTION', $paymentData, $whereData);

			$this->_redirect('/notification/success/index');
			
			

		}
		
		if($this->_request->getParam('printtrxxx') == 1){
			$this->_forward('printtrxxx', 'index', 'widget', array('psnumber' => $paramSession['PSNumber'], 'param' => $paramSession));
		}
	
	}
	
}
