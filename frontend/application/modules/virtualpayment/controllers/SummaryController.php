<?php

class virtualpayment_SummaryController extends Application_Main
{
	protected $_titleCaption = 'Virtual Payment';
	
	
	public function initController()
	{
		$this->view->titleCaption = $this->_titleCaption;
	}
	
	public function indexAction (){
		
		$this->view->modulename = $this->_request->getModuleName();
		$this->view->controllername = $this->_request->getControllerName();
		$this->view->actionname = $this->_request->getActionName();
		
		$fields = array	(
				'VP_NUMBER'	=> array(
						'field'	=> 'VP_NUMBER',
						'label'	=> $this->language->_('Payment Ref').'#',
						'sortable'	=> true
				),
				'INSTITUTIONS_ID'	=> array(
						'field' => 'INSTITUTIONS_ID',
						'label' => $this->language->_('Institutions ID'),
						'sortable'	=> true
				),
				'INVOICE_NO'	=> array(
						'field' => 'INVOICE_NO',
						'label' => $this->language->_('Invoice No'),
						'sortable'	=> true
				),
				'INVOICE_NAME'	=> array(
						'field' => 'INVOICE_NAME',
						'label' => $this->language->_('Invoice Name'),
						'sortable'	=> true
				),
				'AMOUNT'	=> array(
						'field' => 'AMOUNT',
						'label' => $this->language->_('Amount'),
						'sortable'	=> true
				),
				'VP_EFDATE'	=> array(
						'field' => 'VP_EFDATE',
						'label' => $this->language->_('Transaction Date'),
						'sortable'	=> true
				),

				'PAID_STATUS'	=> array(
						'field' => 'PAID_STATUS',
						'label' => $this->language->_('Paid Status'),
						'sortable'	=> true
				),
				
				'INST_NAME'	=> array(
						'field' => 'INST_NAME',
						'label' => $this->language->_('Payment Type'),
						'sortable'	=> true
				),
		
		);
		$params 	= $this->_request->getParams();
		$field = Application_Helper_Array::simpleArray($fields, 'field');
		
		$sortBy 	= (array_key_exists($this->_getParam('sortby'), $fields) ? $fields[$this->_getParam('sortby')]['field'] : $field[0]);
		$sortDir 	= (in_array($this->_getParam('sortdir'), array('asc','desc')) ? $this->_getParam('sortdir') : 'desc');
		$page 		= (is_numeric($this->_getParam('page')) ? $this->_getParam('page') : 1);
		
		$stringParam = $params;
//		unset($stringParam['module']);
//		unset($stringParam['controller']);
//		unset($stringParam['action']);
//		unset($stringParam['filter']);
		
		$filter = array(
				'*'	=> array('StringTrim','StripTags'),
		);
		$options = array('allowEmpty' => true);
		
		$validators = array(
				'INST_ID'			=> array(),
				'INST_NAME'			=> array(),
		);
		
		$filter  = new Zend_Filter_Input($filter, $validators, $params, $options);
		
		
		$INST_ID = $this->_getParam('INST_ID');
		$VP_NUMBER = $this->_getParam('VP_NUMBER');
		$this->view->institutions_id = $INST_ID;
		$this->view->payment_ref = $VP_NUMBER;
		
		$optSync = array(
				//'' => '-- Please Select --',
				'' => 'No',
				'Y' => 'Yes',
				'N' => 'No',
		);
//		$optFrom = array(
//				//'' => '-- Please Select --',
//				'' => '-',
//		);
		$this->view->optSync 		= $optSync;
		
		$data = array();
		
		if ($filter->isValid()){
			foreach ($stringParam as $key => $val){
				$$key = $val;
				$this->view->{$key} = $val;
			}
		
//			$select = $this->_db->select()
//			->from(
//					array('D' => 'T_VP_DETAIL'),
//					array('VP_NUMBER','INSTITUTIONS_ID','INVOICE_NO','INVOICE_NAME','AMOUNT','VP_EFDATE','VP_STATUS')
//			)
//			
//			$select->joinLeft(
//					array('M' => 'M_INSTITUSI'),'D.INSTITUTIONS_ID = M.INST_ID')	
//					
//			->order($sortBy.' '.$sortDir);

			
			$select = $this->_db->select()
				->from(array('A' => 'T_VP_DETAIL'),array('A.VP_NUMBER','A.INSTITUTIONS_ID','A.INVOICE_NO','A.INVOICE_NAME','A.AMOUNT','A.VP_EFDATE','MB.PAID_STATUS'));
			$select->joinLeft(array('MU'=>'M_INSTITUSI'), 'A.INSTITUTIONS_ID = MU.INST_ID', array('MU.INST_NAME'));		
			$select->joinLeft(array('MB'=>'T_VP_TRANSACTION'), 'A.VP_NUMBER = MB.VP_NUMBER', array(''));		
			$select ->order($sortBy.' '.$sortDir);
			$select->where('A.CUST_ID = '.$this->_db->quote($this->_custIdLogin));
			
			
			//$select->where('INST_NAME like "%pajak%"');
			
			if (!empty($INST_ID)){
				$select->where('A.INSTITUTIONS_ID like '.$this->_db->quote('%'.$INST_ID.'%'));
			}
			
			if (!empty($VP_NUMBER)){
				$select->where('A.VP_NUMBER like '.$this->_db->quote('%'.$VP_NUMBER.'%'));
			}
				
			// 			echo $select;
			$data = $this->_db->fetchAll($select);
//			Zend_Debug::dump($data);
		}
		
		$this->view->fields 		= $fields;
		$this->view->sortBy 		= $sortBy;
		$this->view->sortDir 		= $sortDir;
		$this->view->page 			= $page;
		
		$this->view->query_string_params = $stringParam;
		$this->view->paginator 			= $data;
		$this->view->data	 			= $data;
		
		$this->paging($data);
	}
	
}
