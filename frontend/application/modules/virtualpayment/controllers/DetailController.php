<?php

class virtualpayment_DetailController extends Application_Main
{
	protected $_titleCaption = 'Virtual Payment';


	public function initController()
	{
		$this->view->titleCaption = $this->_titleCaption;
	}

	public function indexAction()
	{

		$this->_helper->layout()->setLayout('newlayout');
		$this->view->modulename = $this->_request->getModuleName();
		$this->view->controllername = $this->_request->getControllerName();
		$this->view->actionname = $this->_request->getActionName();

		$fields = array(
			'BIN'	=> array(
				'field'	=> 'CUST_BIN',
				'label'	=> $this->language->_('BIN'),
				'sortable'	=> true
			),
			'VP_NUMBER'	=> array(
				'field'	=> 'VP_NUMBER',
				'label'	=> $this->language->_('VA Number'),
				'sortable'	=> true
			),
			'CREATE_DATE'	=> array(
				'field' => 'CREATE_DATE',
				'label' => $this->language->_('Create Date'),
				'sortable'	=> true
			),
			'LAST_UPLOADED'	=> array(
				'field' => 'LAST_UPLOADED',
				'label' => $this->language->_('Last Uploaded'),
				'sortable'	=> true
			),
			'CCY'	=> array(
				'field' => 'CCY',
				'label' => $this->language->_('CCY'),
				'sortable'	=> true
			),
			'DOC_AMOUNT'	=> array(
				'field' => 'DOC_AMOUNT',
				'label' => $this->language->_('Doc Amount'),
				'sortable'	=> true
			),
			'PAYMENT'	=> array(
				'field' => 'PAYMENT',
				'label' => $this->language->_('Payment'),
				'sortable'	=> true
			),
			'INVALID'	=> array(
				'field' => 'INVALID',
				'label' => $this->language->_('Invalid'),
				'sortable'	=> true
			),
			'BALANCE'	=> array(
				'field' => 'BALANCE',
				'label' => $this->language->_('Balance'),
				'sortable'	=> true
			),
			'STATUS'	=> array(
				'field' => 'STATUS',
				'label' => $this->language->_('Status'),
				'sortable'	=> true
			),
			// 'PAID_DATE'	=> array(
			// 		'field' => 'VP_PAID_DATE',
			// 		'label' => $this->language->_('Paid Date'),
			// 		'sortable'	=> true
			// ),

		);
		$params 	= $this->_request->getParams();
		$field = Application_Helper_Array::simpleArray($fields, 'field');


		$filterlist = array("VP_TRANSACTION_ID", "VP_NUMBER", "INVOICE_NAME", "INVOICE_DESC", "CCY", "AMOUNT", "REMAIN_AMOUNT", "DEPOSIT", "PAYMENT", "VP_CREATED", "VP_CREATEDBY", "EFDATE", "INVALID", "PAID_STATUS", "FULL_PAID");

		$this->view->filterlist = $filterlist;

		//		$sortBy 	= (array_key_exists($this->_getParam('sortby'), $fields) ? $fields[$this->_getParam('sortby')]['field'] : $field[0]);
		$sortBy  		= ($this->_getParam('sortby')) ?  $this->_getParam('sortby') : ('VP_CREATED');

		$sortDir 	= (in_array($this->_getParam('sortdir'), array('asc', 'desc')) ? $this->_getParam('sortdir') : 'desc');
		$page 		= (is_numeric($this->_getParam('page')) ? $this->_getParam('page') : 1);

		$stringParam = $params;
		//		unset($stringParam['module']);
		//		unset($stringParam['controller']);
		//		unset($stringParam['action']);
		//		unset($stringParam['filter']);

		$filter = array(
			'*'	=> array('StringTrim', 'StripTags'),
		);
		$options = array('allowEmpty' => true);

		$validators = array(
			'INST_ID'			=> array(),
			'INST_NAME'			=> array(),
		);

		$filter  = new Zend_Filter_Input($filter, $validators, $params, $options);


		$CUST_BIN = $this->_getParam('CUST_BIN');
		$VP_NUMBER = $this->_getParam('VP_NUMBER');
		$this->view->cust_bin = $CUST_BIN;
		$this->view->vp_number = $VP_NUMBER;

		$uploadStart = $this->_getParam('uploadStart');
		$uploadEnd = $this->_getParam('uploadEnd');
		$payStart = $this->_getParam('payStart');
		$payEnd = $this->_getParam('payEnd');

		$paid_status = $this->_getParam('paid_status');
		$va_type = $this->_getParam('va_type');

		$csv 			= $this->_getParam('csv');
		$pdf 			= $this->_getParam('pdf');

		$this->view->paid_status = $paid_status;
		$this->view->va_type = $va_type;

		$optSync = array(
			//'' => '-- Please Select --',
			'' => 'No',
			'1' => $this->language->_('Paid'),
			'2' => $this->language->_('Unpaid'),
			'3' => $this->language->_('Delete'),
		);

		$optStatus = array(
			//'' => '-- Please Select --',
			'' => 'No',
			'1' => $this->language->_('Open'),
			'2' => $this->language->_('Postpone'),
			'3' => $this->language->_('Expired'),
			'4' => $this->language->_('Close')
		);

		$optSyncVpMode = array(
			//'' => '-- Please Select --',
			'0' => $this->language->_('Free Input'),
			'1' => $this->language->_('As Inquiry'),
		);

		//		$optFrom = array(
		//				//'' => '-- Please Select --',
		//				'' => '-',
		//		);
		$this->view->optStatus 		= $optStatus;
		$this->view->optSync 		= $optSync;
		$this->view->optSyncVpMode 	= $optSyncVpMode;
		//$this->view->optFrom 		= $optFrom;

		$data = array();

		//		if($filter == NULL && $clearfilter != 1){
		//			$fUpdatedStart 	= date("d/m/Y");
		//			$fUpdatedEnd 	= date("d/m/Y");
		//		}
		//		else{
		//		
		//			if($filter != NULL){
		//				$fUpdatedStart 	= $zf_filter->getEscaped('updatedStart');
		//				$fUpdatedEnd 	= $zf_filter->getEscaped('updatedEnd');
		//			}
		//			else if($clearfilter == 1){
		//				$fUpdatedStart 	= "";
		//				$fUpdatedEnd 	= "";
		//			}
		//		}


		//			$this->view->uploadStart = date("d/m/Y");;
		//			$this->view->uploadEnd = date("d/m/Y");

		if ($filter->isValid()) {
			foreach ($stringParam as $key => $val) {
				$$key = $val;
				$this->view->{$key} = $val;
			}

			//			$select = $this->_db->select()
			//			->from(
			//					array('D' => 'T_VP_DETAIL'),
			//					array('VP_NUMBER','INSTITUTIONS_ID','INVOICE_NO','INVOICE_NAME','AMOUNT','VP_EFDATE','VP_STATUS')
			//			)
			//			
			//			$select->joinLeft(
			//					array('M' => 'M_INSTITUSI'),'D.INSTITUTIONS_ID = M.INST_ID')	
			//					
			//			->order($sortBy.' '.$sortDir);

			$select = $this->_db->select()
				//				->from(array('A' => 'T_VP_TRANSACTION'),array('VP_NUMBER','BANK_REFNO','CUST_ID','MU.CUST_NAME','CUST_BIN','INVOICE_NAME','INVOICE_DESC','CCY','AMOUNT','PAID_STATUS','DEVICE_ID','VP_CREATED','VP_PAID_DATE','VP_MODE'));
				->from(array('A' => 'T_VP_TRANSACTION'), array());
			//				->from(array('A' => 'T_VP_DETAIL'),array('A.VP_NUMBER','A.INSTITUTIONS_ID','A.INVOICE_NO','A.INVOICE_NAME','A.AMOUNT','A.VP_EFDATE','MB.PAID_STATUS','MB.PAID_FROM'));
			$select->joinLeft(array('MU' => 'T_VP_TRANSACTION_DETAIL'), 'A.VP_TRANSACTION_ID = MU.VP_TRANSACTION_ID', array(
				'A.VP_TRANSACTION_ID', 'A.VP_NUMBER', 'A.INVOICE_NAME', 'A.INVOICE_DESC', 'A.CCY', 'A.AMOUNT',

				'A.REMAIN_AMOUNT', 'A.DEPOSIT', 'PAYMENT' => 'SUM(MU.AMOUNT)', 'A.VP_CREATED', 'A.VP_CREATEDBY', 'A.VP_EFDATE', 'A.DUE_DATE_TYPE', 'A.VP_STATUS', 'A.PAID_STATUS', 'A.DUE_DATE', 'A.VP_TIME'
			));
			//			$select->joinLeft(array('MB'=>'T_VP_TRANSACTION'), 'A.VP_NUMBER = MB.VP_NUMBER', array(''));		
			$select->order($sortBy . ' ' . $sortDir);
			$select->where('A.CUST_ID = ' . $this->_db->quote($this->_custIdLogin));


			//$select->where('INST_NAME like "%pajak%"');

			if (!empty($CUST_BIN)) {
				$select->where('A.CUST_BIN like ' . $this->_db->quote('%' . $CUST_BIN . '%'));
			}

			if (!empty($VP_NUMBER)) {
				$select->where('A.VP_NUMBER like ' . $this->_db->quote('%' . $VP_NUMBER . '%'));
			}

			if (!empty($va_type) || $va_type == '0') {
				$select->where('A.VP_MODE = ?', $va_type);
			}

			if (!empty($paid_status)) {
				$select->where('A.PAID_STATUS = ?', $paid_status);
			}

			if ($uploadStart) {
				$FormatDate 	= new Zend_Date($uploadStart, $this->_dateDisplayFormat);
				$updatedFrom   	= $FormatDate->toString($this->_dateDBFormat);
				$select->where('DATE(VP_CREATED) >= ?', $updatedFrom);
			}

			if ($uploadEnd) {
				$FormatDate 	= new Zend_Date($uploadEnd, $this->_dateDisplayFormat);
				$updatedTo   	= $FormatDate->toString($this->_dateDBFormat);
				$select->where('DATE(VP_CREATED) <= ?', $updatedTo);
			}

			if ($payStart) {
				$FormatDate 	= new Zend_Date($payStart, $this->_dateDisplayFormat);
				$createdFrom   	= $FormatDate->toString($this->_dateDBFormat);
				$select->where('DATE(VP_PAID_DATE) >= ?', $createdFrom);
			}

			if ($payEnd) {
				$FormatDate 	= new Zend_Date($payEnd, $this->_dateDisplayFormat);
				$createdTo   	= $FormatDate->toString($this->_dateDBFormat);
				$select->where('DATE(VP_PAID_DATE) <= ?', $createdTo);
			}

			$this->view->uploadStart = $uploadStart;
			$this->view->uploadEnd = $uploadEnd;
			$this->view->payStart = $payStart;
			$this->view->payEnd = $payEnd;
			$select->group('A.VP_TRANSACTION_ID');
			// echo $select;die;
			$data = $this->_db->fetchAll($select);
			// Zend_Debug::dump($data);
		}

		$this->view->fields 		= $fields;
		$this->view->sortBy 		= $sortBy;
		$this->view->sortDir 		= $sortDir;
		$this->view->page 			= $page;

		$this->view->query_string_params = $stringParam;
		$this->view->paginator 			= $data;
		$this->view->data	 			= $data;

		if ($csv || $pdf) {
			$arr = $this->_db->fetchAll($select);

			foreach ($arr as $key => $value) {
				//echo $key;
				$arr[$key]["VP_CREATED"] = Application_Helper_General::convertDate($value["VP_CREATED"], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
				$arr[$key]["AMOUNT"] = SGO_Helper_GeneralFunction::displayCurrency($value["AMOUNT"]);
				// if($value["VP_MODE"] == '0'){
				// 	$arr[$key]["VP_MODE"] = 'Free Input';	
				// }
				// else{
				// 	$arr[$key]["VP_MODE"] = 'As in query';	
				// }

				if ($value["PAID_STATUS"] == 'Y') {
					$arr[$key]["PAID_STATUS"] = 'Yes';
				} elseif ($value["PAID_STATUS"] == 'N') {
					$arr[$key]["PAID_STATUS"] = 'No';
				}


				if ($value['DUE_DATE_TYPE'] == '0') {

					$arr[$key]['DUE_DATE'] = 'No due date';
					// }
				} else {
					$effectiveDate = $value['VP_EFDATE'];
					$daysadd = "+" . $value['DUE_DATE'] . " days";
					$Duedate = date('Y-m-d', strtotime($daysadd, strtotime($effectiveDate)));

					$arr[$key]['DUE_DATE'] = substr(Application_Helper_General::convertDate($Duedate, $this->displayDateTimeFormat, $this->defaultDateFormat), 0, 11) . " " . $value['VP_TIME'];
				}


				$arr[$key]['VP_STATUS'] = $optStatus[$value['VP_STATUS']];
				// if($value['VP_STATUS']==''){

				// 		$arr[$key]['DUE_DATE_TYPE'] =
				// }
				unset($arr[$key]['VP_TIME']);
				unset($arr[$key]['DUE_DATE_TYPE']);

				//				$arr[$key]["PS_UPDATED"] = Application_Helper_General::convertDate($value["PS_UPDATED"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
				//				$arr[$key]["PS_EFDATE"] = Application_Helper_General::convertDate($value["PS_EFDATE"],$this->view->viewDateFormat,$this->view->defaultDateFormat);
				//				$arr[$key]["SOURCE_ACCOUNT_NAME"]= $value["SOURCE_ACCOUNT_NAME"].' / '.$value["SOURCE_ACCOUNT_ALIAS_NAME"];
				//				$arr[$key]["BENEFICIARY_ACCOUNT_NAME"]= $value["BENEFICIARY_ACCOUNT_NAME"].' / '.$value["BENEFICIARY_ALIAS_NAME"];
				//				$arr[$key]["BENEFICIARY_ACCOUNT_CCY"]= $value["BENEFICIARY_ACCOUNT_CCY"].' / '.$value["TRA_AMOUNT"].$persenLabel;
				//				$arr[$key]["PS_TYPE"]= $this->language->_($value["PS_TYPE"]).' ('.$value["TRANSFER_TYPE"].')';
				//				unset($arr[$key]["SOURCE_ACCOUNT_ALIAS_NAME"]);
				//				unset($arr[$key]["BENEFICIARY_ALIAS_NAME"]);
				//				unset($arr[$key]["TRA_AMOUNT"]);
				//				unset($arr[$key]["TRANSFER_TYPE"]);
				//				$arr[$key]["TRA_STATUS"]= $this->language->_($value["TRA_STATUS"]);

			}

			$header = Application_Helper_Array::simpleArray($fields, 'label');
			//Zend_Debug::dump($arr);die;
			if ($csv) {
				Application_Helper_General::writeLog('VVPD', 'Download CSV Virtual Payment Upload');
				//Zend_Debug::dump($arr);die;
				//array('Created Date','Last Updated Date','Payment Date', 'Payment Ref#', 'Source Account', 'Source Account Name / Alias', 'Beneficiary Account', 'Beneficiary Account Name / Alias', 'CCY / Amount', 'Transaction ID', 'Message', 'Additional Message', 'Status', 'Payment Type')
				$this->_helper->download->csv($header, $arr, null, 'Virtual Payment Upload');
			}

			if ($pdf) {
				Application_Helper_General::writeLog('VVPD', 'Download PDF Virtual Payment Upload');
				//array('Created Date','Last Updated Date','Payment Date', 'Payment Ref#', 'Source Account', 'Source Account Name / Alias', 'Beneficiary Account', 'Beneficiary Account Name / Alias', 'CCY / Amount', 'Transaction ID', 'Message', 'Additional Message', 'Status', 'Payment Type')
				$this->_helper->download->pdf($header, $arr, null, 'Virtual Payment Upload');
			}
		}
		Application_Helper_General::writeLog('VVPD', 'View Virtual Payment Upload');
		$this->paging($data);
	}
}
