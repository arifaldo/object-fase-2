<?php

class virtualpayment_HistoryController extends Application_Main
{
	protected $_titleCaption = 'Virtual Payment';
	
	
	public function initController()
	{
		$this->view->titleCaption = $this->_titleCaption;
	}
	
	public function indexAction (){
		$this->view->modulename = $this->_request->getModuleName();
		$this->view->controllername = $this->_request->getControllerName();
		$this->view->actionname = $this->_request->getActionName();
		
		$this->view->user = $this->_userIdLogin;
		
		$paymentReff = $this->_getParam('reff');
		$history = $this->_getParam('history');
		$id = $this->_getParam('id');
		$this->view->history = $history;
			
//		$select1 = $this->_db->select()
//			->from(array('XPD'=>'T_VP_DETAIL'))
//			->where('XPD.VP_NUMBER = ?', $paymentReff)
//			//->joinLeft(array('XBOU'=>'X_BILLER_OFFLINE_UPLOAD'), 'XBOU.INVOICE = XPD.INVOICE_NO')
//			->limit(1)
//		;
//		$data1 = $this->_db->fetchRow($select1);
//		//Zend_Debug::dump($data1);
//		$invoice_no = $data1['INVOICE_NO'];	
	
		
		$data = array();
		
		//$params = $this->_request->getParams();
//		$filter = array(
//				'*'	=> array('StringTrim','StripTags'),
//		);
//		$options = array('allowEmpty' => false);
//		$validators = array(
//			'id' => array('alnum','presence' => 'required'),
//		);
//		
//		$filter  = new Zend_Filter_Input($filter, $validators, $params, $options);
		
		//if ($filter->isValid()){
					
			//$id = $filter->getEscaped('id');
			$select = $this->_db->select()
				->from(array('X'=>'T_VP_TRANSACTION_HISTORY'))
				->where('X.VP_NUMBER = ?', $paymentReff)
				->where('X.ID = ?', $id)
				
//				->joinLeft(array('B'=>'M_INSTITUSI'), 'X.INSTITUTIONS_ID = B.INST_ID')
				->limit(1)
			;
			$data = $this->_db->fetchRow($select);
		$this->view->data = $data;	
		
		$optSync = array(
				//'' => '-- Please Select --',
				'' => 'No',
				'1' => $this->language->_('Paid'),
				'2' => $this->language->_('Unpaid'),
				'3' => $this->language->_('Delete'),
		);
		$this->view->optSync 		= $optSync;

		$pdf = $this->_getParam('pdf');
		$pdfTran = $this->_getParam('pdfTran');
		$cancelVirtualPayment = $this->_getParam('cancelVirtualPayment');
		
//		if($pdf)
//		{
//			$datapdf = SGO_Helper_GeneralFunction::formatPurchaseNote($paramSession['PSNumber'], $paramSession);
//			$this->_helper->download->pdf(null,null,null,'Payment Report Detail',$datapdf);
//		}

		$this->view->pdf 		= ($pdf)? true: false;
		
		if($pdf)
		{
			$outputHTML = "<tr><td>".$this->view->render('/view/index.phtml')."</td></tr>";
			$this->_helper->download->pdf(null,null,null,'Virtual Payment Detail',$outputHTML);
			Application_Helper_General::writeLog('VVPD','Download PDF Virtual Payment Detail Report');
		}
		
		if($cancelVirtualPayment)
		{
			$paymentData = array(
									'PAID_STATUS' => 3,
									'VP_UPDATED' => new Zend_Db_Expr("now()")
								);

			$whereData 	 = array(
								 'VP_NUMBER = ?' => (string) $data['VP_NUMBER']
//								 'CUST_ID = ?'	 => (string) $this->_custIdLogin
								);

			$this->_db->update('T_VP_TRANSACTION', $paymentData, $whereData);
			
			////////////////////////////////////////////////////////////////////////
			
//			$paymentDataHistory = array(
//									'PAID_STATUS' => 3,
//									'VP_UPDATED' => new Zend_Db_Expr("now()")
//								);
//
//			$whereDataHistory 	 = array(
//								 'VP_NUMBER = ?' => (string) $data['VP_NUMBER']
////								 'CUST_ID = ?'	 => (string) $this->_custIdLogin
//								);
//
//			$this->_db->update('T_VP_TRANSACTION_HISTORY', $paymentDataHistory, $whereDataHistory);
//			
			
			
//			$this->_redirect($this->_backURL);
			$ns = new Zend_Session_Namespace('FVC');
			$ns->backURL = '/virtualpayment/detail';
			$this->_redirect('/notification/success/index');
		}
		
		if($this->_request->getParam('printtrxxx') == 1){
			$this->_forward('printtrxxx', 'index', 'widget', array('psnumber' => $paramSession['PSNumber'], 'param' => $paramSession));
		}
	
	}
	
}
