<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/SinglePayment.php';
require_once 'CMD/Validate/ValidatePaymentMultiple.php';
require_once 'General/CustomerUser.php';

class virtualpayment_NewController extends Application_Main
{
	protected $_titleCaption = 'Virtual Payment';
	
	
	public function initController()
	{
		$this->view->titleCaption = $this->_titleCaption;
		$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';
		$this->_listCCYValidate = Application_Helper_Array::simpleArray($this->getCcy(),'CCY_ID');  	
	}
	
	
	public function indexAction (){
		$this->view->modulename = $this->_request->getModuleName();
		$this->view->controllername = $this->_request->getControllerName();
		$this->view->actionname = $this->_request->getActionName();
		
		$this->view->user = $this->_userIdLogin;
		
		$paymentReff = $this->_getParam('reff');
		$history = $this->_getParam('history');
		$this->view->history = $history;

		$selectBIN = $this->_db->select()
  	                         ->from(array('A' => 'M_CUSTOMER_BIN'),array('*'))
//							 ->joinLeft(array('B' => 'M_CUSTOMER'),'A.CUST_ID = B.CUST_ID AND A.CUST_ID = B.CUST_ID',array('*'));
							 ->joinLeft(array('C' => 'M_CUSTOMER'),'A.CUST_ID = C.CUST_ID',array('CUST_NAME'))
							 ->where('A.CUST_ID = ?',$this->_custIdLogin);
							 ;
		$binArr = $this->_db->fetchAll($selectBIN);					 
		$this->view->binArr = $binArr;
//		$select1 = $this->_db->select()
//			->from(array('XPD'=>'T_VP_DETAIL'))
//			->where('XPD.VP_NUMBER = ?', $paymentReff)
//			//->joinLeft(array('XBOU'=>'X_BILLER_OFFLINE_UPLOAD'), 'XBOU.INVOICE = XPD.INVOICE_NO')
//			->limit(1)
//		;
//		$data1 = $this->_db->fetchRow($select1);
//		//Zend_Debug::dump($data1);
//		$invoice_no = $data1['INVOICE_NO'];	
	
		



		  $optStatus = array(
        //'' => '-- Please Select --',
                '1' => $this->language->_('No'),
                '0' => $this->language->_('Yes')
              );

              $optBill = array(
        //'' => '-- Please Select --',
                '0' => $this->language->_('Billing'),
                '1' => $this->language->_('No BIlling')
              );

              $optVA = array(
        //'' => '-- Please Select --',
                '0' => $this->language->_('Static'),
                '1' => $this->language->_('Dynamic')
              );

              $optdue = array(
        //'' => '-- Please Select --',
                '0' => $this->language->_('Open'),
                '1' => $this->language->_('Close')
              );



              $opttype = array(
        //'' => '-- Please Select --',
                '0' => $this->language->_('Close'),
                '1' => $this->language->_('Payment'),
                '2' => $this->language->_('Open Payment'),
                
              );

               $optpayment = array(
        //'' => '-- Please Select --',
                '0' => $this->language->_('Yes'),
                '1' => $this->language->_('No'),
                '2' => $this->language->_('Over'),
                
              );
          
      
      // die;
//       $select = $this->_db->select()
//                           ->from(array('a'=> 'M_CUSTOMER_BIN'))
//                           ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$cust_id))
//                           ->query()->fetch();
                           
	  					 $resultdata = $this->_db->select()
  	                         ->from(array('A' => 'M_CUSTOMER_BIN'),array('*'))
//						
							 ->joinLeft(array('C' => 'M_CUSTOMER'),'A.CUST_ID = C.CUST_ID',array('CUST_NAME','CUST_CIF'))
                           	 ->where('A.CUST_ID = ? ',$this->_custIdLogin)
                           	 ->where('A.CUST_BIN = ?','1234')
							 ->query()->fetchAll();
							 // echo $resultdata;die;
							
            // $resultdata = $this->getCustomer($this->_custIdLogin,'1234');
              if(!empty($resultdata)){
              	$resultdata = $resultdata['0'];
              }else{
              	$resultdata = array();
              }
              // print_r($resultdata);die;
              $this->view->stats_due   = $optdue[$resultdata['DUEDATE_STATUS']];
              $this->view->vp_max   = $resultdata['VP_MAX'];
              $this->view->vp_min   = $resultdata['VP_MIN'];
              $this->view->seller_fee   = $resultdata['SELLER_FEE'];
              $this->view->buyer_fee   = $resultdata['BUYER_FEE'];
              $this->view->cal_invo   = $optStatus[$resultdata['CAL_INVOICE']];
              $this->view->cal_depo   = $optStatus[$resultdata['CAL_DEPO']];
              $this->view->partial_type   = $resultdata['PARTIAL_TYPE'];
              $this->view->unique   = $optStatus[$resultdata['UNIQUE']];
              $this->view->partial_type   = $opttype[$resultdata['PARTIAL_TYPE']];
              $this->view->partial_payment   = $optpayment[$resultdata['PARTIAL_PAYMENT']];
              $this->view->va_type   = $optVA[$resultdata['VA_TYPE']];
              $this->view->amount_type   = $optBill[$resultdata['AMOUNT_TYPE']];
              $this->view->ccy   = $resultdata['CCY'];
              $this->view->cif   = $resultdata['CUST_CIF'];
              $this->view->bank   = $resultdata['BANK_NAME'];
              $this->view->branch   = $resultdata['BRANCH_NAME'];
              $this->view->account_alias   = $resultdata['ACCT_ALIAS_NAME'];
              $this->view->account   = $resultdata['ACCT_NO'];
              $this->view->account_name   = $resultdata['ACCT_NAME'];

		$data = array();
		
		
		$optSync = array(
				//'' => '-- Please Select --',
				'' => 'No',
				'1' => $this->language->_('Paid'),
				'2' => $this->language->_('Unpaid'),
				'3' => $this->language->_('Delete'),
		);
		$this->view->optSync 		= $optSync;


		if($this->_request->isPost() )
		{
			$this->view->nominal 		= $this->_getParam('nominal');
			$this->view->due 		= $this->_getParam('due');
			$this->view->bin 		= $this->_getParam('bin');
			//$this->view->reminder 		= $this->_getParam('reminder');
			$this->view->ps_startdate = $this->_getParam('ps_startdate');
			//$repetition = $this->_getParam('repetition');
			$va = $this->_getParam('va');
			$nominal = $this->_getParam('nominal');
			$due = $this->_getParam('due');
			$bin = $this->_getParam('bin');
			$ps_startdate = $this->_getParam('ps_startdate');
			$paramsName['ps_startdate'] = $ps_startdate;
			$filtersName = array(
									'ps_startdate'		=> array('StringTrim'),
									
								);

			$validatorsName = array(
									'ps_startdate' => array(
															'NotEmpty',
															'messages' => array(
																					"Error : Start date cannot left blank"
																				)
														)
								);

			$zf_filter_input_name = new Zend_Filter_Input($filtersName, $validatorsName, $paramsName, $this->_optionsValidator);
			$adapter 	= new Zend_File_Transfer_Adapter_Http ();
				if(!$zf_filter_input_name->isValid()){
						$this->view->error = true;
						// $errors = array($adapter->getMessages());
						$this->view->errorMsg = 'Error : Start date cannot left blank';

				}else{
					// $errors = array($adapter->getMessages());
					// print_r($errors);die;
					$content = array(
					'nominal' => $nominal,
					'bin' => $bin,
					'va' => $va,
					'due' => $due,
					'ps_startdate' => $ps_startdate
					);
							
							$sessionNamespace = new Zend_Session_Namespace('beneficiaryAccountAddEdit');
							$sessionNamespace->mode = 'Add';
							$sessionNamespace->content = $content;
							$this->_redirect('/virtualpayment/new/next');



				}


			
						
			// print_r($this->_getParam('nominal'));die;

		}

		$pdf = $this->_getParam('pdf');
		$pdfTran = $this->_getParam('pdfTran');
		$cancelVirtualPayment = $this->_getParam('cancelVirtualPayment');
		
//		if($pdf)
//		{
//			$datapdf = SGO_Helper_GeneralFunction::formatPurchaseNote($paramSession['PSNumber'], $paramSession);
//			$this->_helper->download->pdf(null,null,null,'Payment Report Detail',$datapdf);
//		}

		$this->view->pdf 		= ($pdf)? true: false;
		
		if($pdf)
		{
			$outputHTML = "<tr><td>".$this->view->render('/view/index.phtml')."</td></tr>";
			$this->_helper->download->pdf(null,null,null,'Virtual Payment Detail',$outputHTML);
			Application_Helper_General::writeLog('VVPD','Download PDF Virtual Payment Detail Report');
		}
		
		if($cancelVirtualPayment)
		{
			$paymentData = array(
									'PAID_STATUS' => 3,
									'VP_UPDATED' => new Zend_Db_Expr("now()")
								);

			$whereData 	 = array(
								 'VP_NUMBER = ?' => (string) $data['VP_NUMBER']
//								 'CUST_ID = ?'	 => (string) $this->_custIdLogin
								);

			$this->_db->update('T_VP_TRANSACTION', $paymentData, $whereData);
			
			
			$ns = new Zend_Session_Namespace('FVC');
			$ns->backURL = '/virtualpayment/detail';
			$this->_redirect('/notification/success/index');
		}
		
		if($this->_request->getParam('printtrxxx') == 1){
			$this->_forward('printtrxxx', 'index', 'widget', array('psnumber' => $paramSession['PSNumber'], 'param' => $paramSession));
		}
	
	}

	public function nextAction()
	{
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$this->view->binArr = $CustomerUser->getCustomerBin();

		$data = $this->_db->fetchRow(
			$this->_db->select()
			->from(array('C' => 'M_USER'))
			->where("USER_ID = ".$this->_db->quote($this->_userIdLogin))
			->limit(1)
		);
		
		$this->view->userId				= $data['USER_ID'];
		
		$sessionNamespace = new Zend_Session_Namespace('beneficiaryAccountAddEdit');
		$content = $sessionNamespace->content;
		$mode = $sessionNamespace->mode;
		// print_r($content);die;
		$this->view->mode = $mode;
		$this->view->VA_OPEN_CLOSE = $content['nominal'];
		$this->view->VA 		   = $content['va'];
		
		
		
 		if($this->_request->isPost() )
		{	
			$this->view->cust_bin 		= $this->_getParam('cust_bin');
					
			$adapter1 					= new Zend_File_Transfer_Adapter_Http();
			$params 					= $this->_request->getParams();
			$sourceFileName 			= $adapter1->getFileName();
			$cust_bin 					= $content['bin'];

			$repettimes 				= $this->_getParam('repettimes');
			$repettype 					= $this->_getParam('repettype');
			$repetdate 					= $this->_getParam('repetdate');

			$repetev 					= $this->_getParam('repetev');
			$repeat 					= $this->_getParam('repetition');
			$repetun 					= $this->_getParam('repetun');
			$due 						= $this->_getParam('repetdate');
			$vp_duedate					= $this->_getParam('vp_duedate');
			$ps_startdate					= $content['ps_startdate'];
			$vp_payment					= $this->_getParam('vp_payment');
			$every_date					= $this->_getParam('every_date');
			
			// name="due"
			$duetype						= $this->_getParam('due');
			$validasi			= $this->_getParam('validasi');
			$jam			= $this->_getParam('jam');
			$remain_notif = $this->_getParam('remain_notif');
			$vatype 		= $this->_getParam('vatype');
			//$this->view->reminder 		= $this->_getParam('reminder');
			//$this->view->ps_startdate = $this->_getParam('ps_startdate');
			//$repetition = $this->_getParam('repetition');
			//$reminder = $this->_getParam('reminder');
			

			
			if($sourceFileName == null)
			{
				$sourceFileName = null;
			}
			else
			{
				$sourceFileName = substr(basename($adapter1->getFileName()), 0);
			}
			
			$paramsName['sourceFileName'] 	= $sourceFileName;
			$paramsName['cust_bin'] 	= $cust_bin;
			
			$filtersName = array(
									'sourceFileName'		=> array('StringTrim'),
									'cust_bin'		=> array('StringTrim'),
									'ps_startdate'		=> array('StringTrim'),
									'repettype'		=> array('StringTrim'),
									'repetdate'		=> array('StringTrim'),
									'repettimes'		=> array('StringTrim'),
									'vatype'		=> array('StringTrim'),
								);
			
			$validatorsName = array(
									'sourceFileName' => array(
															'NotEmpty',
															'messages' => array(
																					"Error : File size too big or left blank"
																				)
														),
									'cust_bin' => array(
															'NotEmpty',
															'messages' => array(
																					"Error : File BIN cannot left blank. Please selectt"
																				)
														),
									// 'cust_bin' => array(
									// 						'NotEmpty',
									// 						'messages' => array(
									// 												"Error : File BIN cannot left blank. Please selectt"
									// 											)
									// 					),
									'ps_startdate' => array(
															'NotEmpty',
															'messages' => array(
																					"Error : File BIN cannot left blank. Please selectt"
																				)
														),
									'repettype' => array(),
									'repetdate' => array(),
									'repettimes' => array(),
									'vatype' => array(),
																												
								);
			$zf_filter_input_name = new Zend_Filter_Input($filtersName, $validatorsName, $paramsName, $this->_optionsValidator);
			
			if($cust_bin == '')
			{
				$this->view->error4		= true;
			}
			else{
				if($zf_filter_input_name->isValid())
				{
					// die;
					$filter 	= new Application_Filtering();
					$adapter 	= new Zend_File_Transfer_Adapter_Http ();
					$validate   = new ValidatePaymentMultiple($this->_custIdLogin, $this->_userIdLogin);
					$max		= $this->getSetting('max_import_single_payment');
					
					$adapter->setDestination ( $this->_destinationUploadDir );
					$extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv'));
					$extensionValidator->setMessage(
														'Error: Extension file must be *.csv'
													);
					
					$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
					$sizeValidator->setMessage(
												'Error: File siz must not more than '.$this->getSetting('Fe_attachment_maxbyte')
											);
							
					$adapter->setValidators(array($extensionValidator,$sizeValidator));
						
					if ($adapter->isValid ()) 
					{
						$sourceFileName = $adapter->getFileName();
						$newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';
							
						$adapter->addFilter ( 'Rename',$newFileName  );
							
						if ($adapter->receive ())
						{
							$Csv = new Application_Csv ($newFileName,",");
							$csvData = $Csv->readAll ();
							$csvData2 = $Csv->readAll();
							
							
							
							@unlink($newFileName);
							//Zend_Debug::dump($csvData);die;
							//end
	
							$totalRecords2 = count($csvData2);
	
							if($totalRecords2)
								{
									for ($a= 1; $a<$totalRecords2; $a++ ){
										unset($csvData2[$a]);
									}
	//								unset($csvData2[1]);
	//								unset($csvData2[2]);
	//								unset($csvData2[3]);
									$totalRecords2 = count($csvData2);
							}
	
	
							foreach ( $csvData2 as $row )
							{
								$type =  trim($row[0]);
							}
	
							$totalRecords = count($csvData);
							if($totalRecords)
							{
								unset($csvData[0]);
								$totalRecords = count($csvData);
							}
							
							if ($totalRecords){
								if($totalRecords <= $max)
								{
									$no =0;

									foreach ( $csvData as $columns )
									{
										if(count($columns)==6)
										{
											if (strtoupper(trim($type)) == 'VA_NUMBER'){ //ini statis va(Open)
												$params['VP_NUMBER'] 			= trim($columns[0]);
												$params['INVOICE_NAME'] 		= trim($columns[1]);
												$params['INVOICE_DESC'] 		= trim($columns[2]);
												$params['AMOUNT'] 				= trim($columns[3]);
												$params['REMINDER_SMS'] 		= trim($columns[4]);
												$params['REMINDER_EMAIL'] 		= trim($columns[5]);
												
												$VP_NUMBER 			= $filter->filter($params['VP_NUMBER'],"VP_NUMBER");
												$INVOICE_NAME 		= $filter->filter($params['INVOICE_NAME'],"INVOICE_NAME");
												$INVOICE_DESC 		= $filter->filter($params['INVOICE_DESC'],"INVOICE_DESC");
												$REMINDER_SMS 		= $filter->filter($params['REMINDER_SMS'],"REMINDER_SMS");
												$REMINDER_EMAIL 		= $filter->filter($params['REMINDER_EMAIL'],"REMINDER_EMAIL");
												$CCY 				= 'IDR';
												
												$AMOUNT 			= $filter->filter($params['AMOUNT'],"AMOUNT");
												$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($AMOUNT);
												
												$paramPayment = array(
														"CATEGORY" 					=> 'VIRTUAL PAYMENT',
														"FROM" 						=> "I",				// F: Form, I: Import
														//"PS_NUMBER"					=> "",
														"VA_TYPE"					=> 'Open',
														"PS_EFDATE"					=> date('d/m/y'),
														"_dateFormat"				=> $this->_dateUploadFormat,
														"_dateDBFormat"				=> $this->_dateDBFormat,
												);
									
												$paramTrxArr[0] = array(
														"TRANSFER_TYPE" 			=> 'VP',
														"TRA_AMOUNT" 				=> $TRA_AMOUNT_num,
														"ACBENEF_CCY" 				=> strtoupper($CCY),
														"COMP_ID" 					=> $this->_custIdLogin,
														"VP_NUMBER" 				=> $VP_NUMBER,
														"INVOICE_NAME" 				=> $INVOICE_NAME,
														"INVOICE_DESC" 				=> $INVOICE_DESC,

														"REMINDER_SMS" 				=> $REMINDER_SMS,
														"REMINDER_EMAIL"			=> $REMINDER_EMAIL,

														"REPEAT" 					=> $repeat,
														"REPET_TYPE" 				=> $repettype,
														"REPET_DATE" 				=> $repetdate,
														"REPET_TIMES" 				=> $repettimes,
														"REPET_EVERY" 				=> $repetev,
														"START_DATE" 				=> $ps_startdate,
														"DUE_TYPE"					=> $duetype,					
														"CUST_BIN" 					=> $cust_bin,

														"DUE_DATE" 					=> $vp_duedate,
														"REMINDER_NOTIFICATION"		=> $remain_notif,
														"VP_EFDATE" 				=> $content['ps_startdate'],
														"VP_CREATEDBY"		 		=> $this->_userIdLogin,
														"VP_TIME" 					=> $jam,
														"VP_AFTER" 					=> $vatype,
														"VP_PAYMENT" 				=> $vp_payment,
														"VP_VALIDATE"				=> $validasi,
														
												);



			
												// $cust_bin 					= $this->_getParam('cust_bin');
												// $repettimes 				= $this->_getParam('repettimes');
												// $repettype 					= $this->_getParam('repettype');
												// $repetdate 					= $this->_getParam('repetdate');

												// $repetev 					= $this->_getParam('repetev');
												// $repeat 					= $this->_getParam('repetition');
												// $repetun 					= $this->_getParam('repetun');
												// $due 					= $this->_getParam('due');
												// $ps_startdate			= $this->_getParam('ps_startdate');

												// $schduler			= $this->_getParam('schduler');
												// $jam			= $this->_getParam('jam');
												// $remain_notif = $this->_getParam('remain_notif');
												// $vatype 		= $this->_getParam('vatype');
											
											}
											else{ //dinamis va (Close)
												// $params['VP_NUMBER'] 			= trim($columns[0]);
												$params['INVOICE_NAME'] 		= trim($columns[0]);
												$params['INVOICE_DESC'] 		= trim($columns[1]);
												
												$params['REMINDER_SMS'] 		= trim($columns[2]);
												$params['REMINDER_EMAIL'] 		= trim($columns[3]);
	//											$params['CCY'] 					= trim($columns[3]);
												
												
												$INVOICE_NAME 		= $filter->filter($params['INVOICE_NAME'],"INVOICE_NAME");
												$INVOICE_DESC 		= $filter->filter($params['INVOICE_DESC'],"INVOICE_DESC");
												$CCY 				= 'IDR';
												$AMOUNT 			= $filter->filter($params['AMOUNT'],"AMOUNT");
												$REMINDER_SMS 		= $filter->filter($params['REMINDER_SMS'],"REMINDER_SMS");
												$REMINDER_EMAIL 		= $filter->filter($params['REMINDER_EMAIL'],"REMINDER_EMAIL");
													
												$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($AMOUNT);
												
												$paramPayment = array(
														"CATEGORY" 					=> 'VIRTUAL PAYMENT',
														"FROM" 						=> "I",				// F: Form, I: Import
														//"PS_NUMBER"					=> "",
														"VA_TYPE"					=> 'Close',
														"PS_EFDATE"					=> date('d/m/y'),
														"_dateFormat"				=> $this->_dateUploadFormat,
														"_dateDBFormat"				=> $this->_dateDBFormat,
												);
									
												$paramTrxArr[0] = array(
														"TRANSFER_TYPE" 			=> 'VP',
														"TRA_AMOUNT" 				=> $TRA_AMOUNT_num,
														"ACBENEF_CCY" 				=> strtoupper($CCY),
														"COMP_ID" 					=> $this->_custIdLogin,
														"VP_NUMBER" 				=> '',
														"INVOICE_NAME" 				=> $INVOICE_NAME,
														"INVOICE_DESC" 				=> $INVOICE_DESC,

														"REMINDER_SMS" 				=> $REMINDER_SMS,
														"REMINDER_EMAIL"			=> $REMINDER_EMAIL,

														"REPEAT" 					=> $repeat,
														"REPET_TYPE" 				=> $repettype,
														"REPET_DATE" 				=> $repetdate,
														"REPET_TIMES" 				=> $repettimes,
														"START_DATE" 				=> $startdate,
														"VP_EFDATE" 				=> $ps_startdate,
														"CUST_BIN" 					=> $cust_bin,
														"DUE_DATE" 					=> $vp_duedate,
														"REMINDER_NOTIFICATION"		=> $remain_notif,
														"DUE_TYPE"					=> $duetype,
														
														// "REPEAT_UNTIL_DATE" 		=> $cust_bin,
														"VP_CREATEDBY"		 		=> $this->_userIdLogin,
														"VP_TIME" 					=> $jam,
														"VP_AFTER" 					=> $vatype,
														"VP_PAYMENT" 				=> $vp_payment,
														"VP_VALIDATE"				=> $validasi,
												);
											}
												
											$arr[$no]['paramPayment'] = $paramPayment;
											$arr[$no]['paramTrxArr'] = $paramTrxArr;
										}else if(count($columns)==5)
										{
											if (strtoupper(trim($type)) == 'VA_NUMBER'){ //ini statis va(Open)
												$params['VP_NUMBER'] 			= trim($columns[0]);
												$params['INVOICE_NAME'] 		= trim($columns[1]);
												$params['INVOICE_DESC'] 		= trim($columns[2]);
												// $params['AMOUNT'] 				= trim($columns[3]);
												$params['REMINDER_SMS'] 		= trim($columns[3]);
												$params['REMINDER_EMAIL'] 		= trim($columns[4]);
												
												$VP_NUMBER 			= $filter->filter($params['VP_NUMBER'],"VP_NUMBER");
												$INVOICE_NAME 		= $filter->filter($params['INVOICE_NAME'],"INVOICE_NAME");
												$INVOICE_DESC 		= $filter->filter($params['INVOICE_DESC'],"INVOICE_DESC");
												$REMINDER_SMS 		= $filter->filter($params['REMINDER_SMS'],"REMINDER_SMS");
												$REMINDER_EMAIL 		= $filter->filter($params['REMINDER_EMAIL'],"REMINDER_EMAIL");
												$CCY 				= 'IDR';
												
												$AMOUNT 			= $filter->filter($params['AMOUNT'],"AMOUNT");
												$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($AMOUNT);
												
												$paramPayment = array(
														"CATEGORY" 					=> 'VIRTUAL PAYMENT',
														"FROM" 						=> "I",				// F: Form, I: Import
														//"PS_NUMBER"					=> "",
														"VA_TYPE"					=> 'Open',
														"PS_EFDATE"					=> date('d/m/y'),
														"_dateFormat"				=> $this->_dateUploadFormat,
														"_dateDBFormat"				=> $this->_dateDBFormat,
												);
									
												$paramTrxArr[0] = array(
														"TRANSFER_TYPE" 			=> 'VP',
														"TRA_AMOUNT" 				=> $TRA_AMOUNT_num,
														"ACBENEF_CCY" 				=> strtoupper($CCY),
														"COMP_ID" 					=> $this->_custIdLogin,
														"VP_NUMBER" 				=> $VP_NUMBER,
														"INVOICE_NAME" 				=> $INVOICE_NAME,
														"INVOICE_DESC" 				=> $INVOICE_DESC,

														"REMINDER_SMS" 				=> $REMINDER_SMS,
														"REMINDER_EMAIL"			=> $REMINDER_EMAIL,

														"REPEAT" 					=> $repeat,
														"REPET_TYPE" 				=> $repettype,
														"REPET_DATE" 				=> $repetdate,
														"REPET_TIMES" 				=> $repettimes,
														"REPET_EVERY" 				=> $repetev,
														"START_DATE" 				=> $ps_startdate,
														"DUE_TYPE"					=> $duetype,					
														"CUST_BIN" 					=> $cust_bin,

														"DUE_DATE" 					=> $vp_duedate,
														"REMINDER_NOTIFICATION"		=> $remain_notif,
														"VP_EFDATE" 				=> $ps_startdate,
														"VP_CREATEDBY"		 		=> $this->_userIdLogin,
														"VP_TIME" 					=> $jam,
														"VP_AFTER" 					=> $vatype,
														"VP_PAYMENT" 				=> $vp_payment,
														"VP_VALIDATE"				=> $validasi,

														
												);

												// $cust_bin 					= $this->_getParam('cust_bin');
												// $repettimes 				= $this->_getParam('repettimes');
												// $repettype 					= $this->_getParam('repettype');
												// $repetdate 					= $this->_getParam('repetdate');

												// $repetev 					= $this->_getParam('repetev');
												// $repeat 					= $this->_getParam('repetition');
												// $repetun 					= $this->_getParam('repetun');
												// $due 					= $this->_getParam('due');
												// $ps_startdate			= $this->_getParam('ps_startdate');

												// $schduler			= $this->_getParam('schduler');
												// $jam			= $this->_getParam('jam');
												// $remain_notif = $this->_getParam('remain_notif');
												// $vatype 		= $this->_getParam('vatype');
											
											}
											else{ //dinamis va (Close)
												// $params['VP_NUMBER'] 			= trim($columns[0]);
												$params['INVOICE_NAME'] 		= trim($columns[0]);
												$params['INVOICE_DESC'] 		= trim($columns[1]);
												$params['AMOUNT']		 		= trim($columns[2]);
												$params['REMINDER_SMS'] 		= trim($columns[3]);
												$params['REMINDER_EMAIL'] 		= trim($columns[4]);
	//											$params['CCY'] 					= trim($columns[3]);
												
												
												$INVOICE_NAME 		= $filter->filter($params['INVOICE_NAME'],"INVOICE_NAME");
												$INVOICE_DESC 		= $filter->filter($params['INVOICE_DESC'],"INVOICE_DESC");
												$CCY 				= 'IDR';
												$AMOUNT 			= $filter->filter($params['AMOUNT'],"AMOUNT");
												$REMINDER_SMS 		= $filter->filter($params['REMINDER_SMS'],"REMINDER_SMS");
												$REMINDER_EMAIL 		= $filter->filter($params['REMINDER_EMAIL'],"REMINDER_EMAIL");
													
												$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($AMOUNT);
												
												$paramPayment = array(
														"CATEGORY" 					=> 'VIRTUAL PAYMENT',
														"FROM" 						=> "I",				// F: Form, I: Import
														//"PS_NUMBER"					=> "",
														"VA_TYPE"					=> 'Close',
														"PS_EFDATE"					=> date('d/m/y'),
														"_dateFormat"				=> $this->_dateUploadFormat,
														"_dateDBFormat"				=> $this->_dateDBFormat,
												);
									
												$paramTrxArr[0] = array(
														"TRANSFER_TYPE" 			=> 'VP',
														"TRA_AMOUNT" 				=> $TRA_AMOUNT_num,
														"ACBENEF_CCY" 				=> strtoupper($CCY),
														"COMP_ID" 					=> $this->_custIdLogin,
														"VP_NUMBER" 				=> '',
														"INVOICE_NAME" 				=> $INVOICE_NAME,
														"INVOICE_DESC" 				=> $INVOICE_DESC,

														"REMINDER_SMS" 				=> $REMINDER_SMS,
														"REMINDER_EMAIL"			=> $REMINDER_EMAIL,

														"REPEAT" 					=> $repeat,
														"REPET_TYPE" 				=> $repettype,
														"REPET_DATE" 				=> $repetdate,
														"REPET_TIMES" 				=> $repettimes,
														"START_DATE" 				=> $startdate,
														"VP_EFDATE" 				=> $ps_startdate,
														"CUST_BIN" 					=> $cust_bin,
														"DUE_DATE" 					=> $vp_duedate,
														"REMINDER_NOTIFICATION"		=> $remain_notif,
														"DUE_TYPE"					=> $duetype,
														
														// "REPEAT_UNTIL_DATE" 		=> $cust_bin,
														"VP_CREATEDBY"		 		=> $this->_userIdLogin,
														"VP_TIME" 					=> $jam,
														"VP_AFTER" 					=> $vatype,
														"VP_PAYMENT" 				=> $vp_payment,
														"VP_VALIDATE"				=> $validasi,
												);
											}
												
											$arr[$no]['paramPayment'] = $paramPayment;
											$arr[$no]['paramTrxArr'] = $paramTrxArr;
										}else if(count($columns)==4)
										{
											
											
												$params['INVOICE_NAME'] 		= trim($columns[0]);
												$params['INVOICE_DESC'] 		= trim($columns[1]);
												// $params['AMOUNT']		 		= trim($columns[2]);
												$params['REMINDER_SMS'] 		= trim($columns[2]);
												$params['REMINDER_EMAIL'] 		= trim($columns[3]);
	//											$params['CCY'] 					= trim($columns[3]);
												
												
												$INVOICE_NAME 		= $filter->filter($params['INVOICE_NAME'],"INVOICE_NAME");
												$INVOICE_DESC 		= $filter->filter($params['INVOICE_DESC'],"INVOICE_DESC");
												$CCY 				= 'IDR';
												$AMOUNT 			= $filter->filter($params['AMOUNT'],"AMOUNT");
												$REMINDER_SMS 		= $filter->filter($params['REMINDER_SMS'],"REMINDER_SMS");
												$REMINDER_EMAIL 		= $filter->filter($params['REMINDER_EMAIL'],"REMINDER_EMAIL");
													
												$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($AMOUNT);
												
												$paramPayment = array(
														"CATEGORY" 					=> 'VIRTUAL PAYMENT',
														"FROM" 						=> "I",				// F: Form, I: Import
														//"PS_NUMBER"					=> "",
														"VA_TYPE"					=> 'Close',
														"PS_EFDATE"					=> date('d/m/y'),
														"_dateFormat"				=> $this->_dateUploadFormat,
														"_dateDBFormat"				=> $this->_dateDBFormat,
												);
									
												$paramTrxArr[0] = array(
														"TRANSFER_TYPE" 			=> 'VP',
														"TRA_AMOUNT" 				=> $TRA_AMOUNT_num,
														"ACBENEF_CCY" 				=> strtoupper($CCY),
														"COMP_ID" 					=> $this->_custIdLogin,
														"VP_NUMBER" 				=> '',
														"INVOICE_NAME" 				=> $INVOICE_NAME,
														"INVOICE_DESC" 				=> $INVOICE_DESC,

														"REMINDER_SMS" 				=> $REMINDER_SMS,
														"REMINDER_EMAIL"			=> $REMINDER_EMAIL,

														"REPEAT" 					=> $repeat,
														"REPET_TYPE" 				=> $repettype,
														"REPET_DATE" 				=> $repetdate,
														"REPET_TIMES" 				=> $repettimes,
														"START_DATE" 				=> $startdate,
														"VP_EFDATE" 				=> $ps_startdate,
														"CUST_BIN" 					=> $cust_bin,
														"DUE_DATE" 					=> $vp_duedate,
														"REMINDER_NOTIFICATION"		=> $remain_notif,
														"DUE_TYPE"					=> $duetype,
														
														// "REPEAT_UNTIL_DATE" 		=> $cust_bin,
														"VP_CREATEDBY"		 		=> $this->_userIdLogin,
														"VP_TIME" 					=> $jam,
														"VP_AFTER" 					=> $vatype,
														"VP_PAYMENT" 				=> $vp_payment,
														"VP_VALIDATE"				=> $validasi,
												);
											
												
											$arr[$no]['paramPayment'] = $paramPayment;
											$arr[$no]['paramTrxArr'] = $paramTrxArr;
										}
										else
										{
											$this->view->error 		= true;
											break;
										}
										$no++;
									}
										
									if(!$this->view->error)
									{
										$resultVal	= $validate->checkCreateVp($arr);
										$payment 	= $validate->getPaymentInfo();
										$errorTrxMsg 	= $validate->getErrorTrxMsg();
											
										$content['payment'] = $payment;
										$content['arr'] 	= $arr;
										$content['errorTrxMsg'] 	= $errorTrxMsg;
										$sessionNamespace = new Zend_Session_Namespace('confirmImportCredit');
										$sessionNamespace->content = $content;
										$this->_redirect('/virtualpayment/import/confirm');
									}
								}
								else
								{
									$this->view->error2 = true;
									$this->view->max 	= $max;
								}
							}else{
								$this->view->error = true;
							}
						}
					}
					else
					{
						$this->view->error = true;
						$errors = array($adapter->getMessages());
						$this->view->errorMsg = $errors;
					}
				}
				else
				{
					$this->view->error3		= true;
				}
			}
		}
	}


	public function confirmAction()
	{
		$sessionNamespace = new Zend_Session_Namespace('confirmImportCredit');
		$data = $sessionNamespace->content;
		
		$vpNumber 	= array('');
//		$vpType 	= array('');
		foreach($data["arr"] as $row)
		{
			if($row['paramTrxArr'][0]['VP_NUMBER'] != ''){
				array_push($vpNumber, $row['paramTrxArr'][0]['VP_NUMBER']);
				$vpTypeP = 'Open';
			}
		}
		$vpType = (!empty($vpTypeP)?$vpTypeP:'');
		
		if(!$data["payment"]["countTrxCCY"])
		{
			$this->_redirect("/authorizationacl/index/nodata");
		}
		
		$this->view->countTrxCCY = $data["payment"]["countTrxCCY"];
		
		$this->view->sumTrxCCY = $data["payment"]["sumTrxCCY"];
		
		$totalSuccess = 0;
		foreach($data["payment"]["countTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$totalSuccess += $ccy['success'];
			}
		}
		
		$amountSuccess = 0;
		foreach($data["payment"]["sumTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$amountSuccess += $ccy['success'];
			}
		}
		
		$totalFailed = 0;
		foreach($data["payment"]["countTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$totalFailed += $ccy['failed'];
			}
		}
		
		$amountFailed = 0;
		foreach($data["payment"]["sumTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$amountFailed += $ccy['failed'];
			}
		}
		
		$cust_bin = 0;
		foreach($data["arr"] as $row)
		{
			$cust_bin =$row['paramTrxArr'][0]['CUST_BIN'];
		}
		
		if(count(array_unique($vpNumber))<count($vpNumber))
		{
//		    echo 'Array has duplicates';
//			$vals = array_count_values($vpNumber);
			$totalFailed = 1;
			$totalFailedVp = 1;
		}
		
		function showDups($vpNumber)
		{
		   $array_temp = array();
		   $i = 1;
		   foreach($vpNumber as $val)
		   {
		     if (!in_array($val, $array_temp))
		     {
		       $array_temp[] = $val;
		     }
		     else
		     {
		       echo '
		       <b><font color="red">'.$i++. '. Duplicate VA Number : ' . $val . ' in the file.</b></font><br />';
		     }
		   }
		}
		
		showDups($vpNumber);
		       
		$this->view->totalSuccess = $totalSuccess;
		$this->view->amountSuccess = $amountSuccess;
		$this->view->totalFailed = $totalFailed;
		$this->view->totalFailedVp = (!empty($totalFailedVp)?$totalFailedVp:'');
		
		$this->view->amountFailed = $amountFailed;
		$this->view->cust_bin = $cust_bin;
		
		$fields = array(
						'PaymentType'     			 => array(
														'field'    => '',
														'label'    => $this->language->_('BIN'),
														'sortable' => false
													),
//						'CCY'     			 => array(
//														'field'    => '',
//														'label'    => $this->language->_('CCY'),
//														'sortable' => false
//													),
						'Payment Success'     			 => array(
														'field'    => '',
														'label'    => $this->language->_('Payment Success'),
														'sortable' => false
													),
						'Total Amount Success'     			 => array(
														'field'    => '',
														'label'    => $this->language->_('Total Amount Success'),
														'sortable' => false
													),
						'Payment Failed'     			 => array(
														'field'    => '',
														'label'    => $this->language->_('Payment Failed'),
														'sortable' => false
													),
						'Total Amount Failed'     			 => array(
														'field'    => '',
														'label'    => $this->language->_('Total Amount Failed'),
														'sortable' => false
													),
                        );
		$this->view->fields = $fields;
		
		
			$selectCountUser = $this->_db->select()
		  	                   ->from('T_VP_COUNTER',array('*'))
		  	                   ->where('UPPER(CUST_ID)='.$this->_db->quote((string)$this->_custIdLogin));
//	  	                       ->where("SUBSTRING(VP_CREATED,1,10)= ?", $dateNow)
//	  	                       ->where("VP_MODE= ?", 1);
//	  	                       ->where("A.PAID_STATUS in (1,2)");	
		    $dataCountUser = $this->_db->fetchRow($selectCountUser);   

			$date_val	= date('Y-m-d');
			$dataCountUpdated = substr($dataCountUser['COUNTER_UPDATED'], 0,10);
			
		    $jumUser = $dataCountUser['COUNTER'];
			
			if($jumUser == 0 || $jumUser == ''){
				$lastCounter = 1;
				$totalLastData = $totalSuccess;
			}
			else{
				$lastCounter = $jumUser+1;
				$totalLastData = $totalSuccess+$jumUser;
			}
			
			$i = $lastCounter;
			
			if($date_val > $dataCountUpdated){
				$where = array('CUST_ID = ?' => $this->_custIdLogin);
				$this->_db->delete('T_VP_COUNTER',$where);
//				echo 'sudah lewat tanggal hapus counter';
			}
			else{
				if($vpType == 'Open'){
				}
				else{
					if ($totalLastData >= 999999 || $i >= 999999)
					{
						unset($_SESSION['confirmImportCredit']);
//						redirect(buildEncLink('warning_payment_overquota.php',''));
						$this->_redirect('/virtualpayment/warning');
					}
				}
			}
		// die('gere');
		if($this->_request->isPost() )
		{			
			if($this->_getParam('submit_cancel') == TRUE)
			{
				unset($_SESSION['confirmImportCredit']); 
				$this->_redirect('/virtualpayment/import');
			}
			// die;
			foreach($data["arr"] as $row)
			{
				// 
				$param['COMP_ID'] 			= $this->_userIdLogin;
				//$param['PS_EFDATE']  		= Application_Helper_General::convertDate($row['paramPayment']['PS_EFDATE'], $this->_dateDBFormat, $this->_dateUploadFormat);
//				$param['INSTITUTIONS_ID'] 	= $row['paramTrxArr'][0]['INSTITUTIONS_ID'];
				$param['VP_NUMBER']  		= $row['paramTrxArr'][0]['VP_NUMBER'];
				$param['INVOICE_NAME']  	= $row['paramTrxArr'][0]['INVOICE_NAME'];
				$param['INVOICE_DESC']		= $row['paramTrxArr'][0]['INVOICE_DESC'];
				$param['CCY'] 				= $row['paramTrxArr'][0]['ACBENEF_CCY'];
				$param['AMOUNT'] 			= $row['paramTrxArr'][0]['TRA_AMOUNT'];


				$param['REPET_DATE']		= $row['paramTrxArr'][0]['REPET_DATE'];
				$param['REPET_TIMES'] 			= $row['paramTrxArr'][0]['REPET_TIMES'];
				$param['START_DATE'] 			= $row['paramTrxArr'][0]['START_DATE'];
				$param['REPEAT'] 			= $row['paramTrxArr'][0]['REPEAT'];
				$param['REPET_TYPE'] 			= $row['paramTrxArr'][0]['REPET_TYPE'];

				$param['CUST_BIN'] 			= '12354';
				$param['TOTAL'] 			= $totalLastData;
//				$param['TOTAL_TEMP'] 		= $totalSuccess;
				$param['TOTAL_COUNTER'] 	= $i++;
				
//				$param['AREA'] 				= $row['paramTrxArr'][0]['AREA'];
				$param['BENEFICIARY_ACCOUNT'] = $row['paramTrxArr'][0]['BENEFICIARY_ACCOUNT'];
				$param['BENEFICIARY_NAME'] 	  = $row['paramTrxArr'][0]['BENEFICIARY_NAME'];

//				$param['_addBeneficiary'] 			= $row['paramPayment']['_addBeneficiary'];
//				$param['_beneLinkage'] 				= $row['paramPayment']['_beneLinkage'];
//				$param["_dateFormat"]				= $row['paramPayment']['_dateFormat'];
//				$param["_dateDBFormat"]				= $row['paramPayment']['_dateDBFormat'];
//				$param["_createPB"]					= $row['paramPayment']['_createPB'];
//				$param["_createDOM"]				= $row['paramPayment']['_createDOM'];
//				$param["_createREM"]				= $row['paramPayment']['_createREM'];
	  
				try 
				{
					$SinglePayment = new SinglePayment("", $this->_custIdLogin, $this->_userIdLogin);					
					//$result = $SinglePayment->createPayment($param);
					// Zend_Debug::dump($param);die;
					$result = $SinglePayment->createVirtualPayment($param);
					
				}
				catch(Exception $e)
				{
					Application_Helper_General::exceptionLog($e);
					$errr = 1;
				}
			}
			// die;
			unset($sessionNamespace->content);
			
			$this->_helper->getHelper('FlashMessenger')->addMessage('/virtualpayment/import/');
			
			if ($errr != 1)
			{	$this->_redirect('/notification/success/index');	}
			else
			{	$this->_redirect('/notification/success/index');	}	// TODO: what to do, if failed create payment
			
		}
		Application_Helper_General::writeLog('IFVP','Confirm Virtual Payment by Import File (CSV)');
	
	}
	
}
