<?php
require_once 'General/Settings.php';
require_once 'General/CustomerUser.php';
require_once 'General/Login.php';
require_once 'SGO/Helper/AES.php';

class directdebit_IndextestController extends Application_Main {

	public function getUserIP()
	{
	    $client  = @$_SERVER['HTTP_CLIENT_IP'];
	    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
	    $remote  = $_SERVER['REMOTE_ADDR'];
	
	    if(filter_var($client, FILTER_VALIDATE_IP))
	    {
	        $ip = $client;
	    }
	    elseif(filter_var($forward, FILTER_VALIDATE_IP))
	    {
	        $ip = $forward;
	    }
	    else
	    {
	        $ip = $remote;
	    }
	
	    return $ip;
	}
	
	public function get_browser_name($user_agent)
	{
	    if (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR/')) return 'Opera';
	    elseif (strpos($user_agent, 'Edge')) return 'Edge';
	    elseif (strpos($user_agent, 'Chrome')) return 'Chrome';
	    elseif (strpos($user_agent, 'Safari')) return 'Safari';
	    elseif (strpos($user_agent, 'Firefox')) return 'Firefox';
	    elseif (strpos($user_agent, 'MSIE') || strpos($user_agent, 'Trident/7')) return 'Internet Explorer';
	    
	    return 'Other';
	}
	


	public function indexAction() {
		$this->_helper->layout()->setLayout('newlayout');

					$select = $this->_db->select()
												->from(array('M_SETTING'), array('SETTING_VALUE'))
												->where('SETTING_ID = ?', 'femailtemplate_newuser');

						// return 

			        $FResetPass = $this->_db->fetchOne($select);

			        $datacreated = $this->_db->select()
										        ->from('M_USER')
										        ->where('USER_ID = ?', 'PTFE_BENY014')
										        ->where('CUST_ID = ?','FIF00A1')
										        ->query()
										        ->fetch(Zend_Db::FETCH_ASSOC);
			        // echo $datacreated;die;
			        $newPassword = substr(base64_decode($datacreated['USER_CLEARTEXT_PWD']),4, -4);
			        
			        $FResetPass = str_ireplace('[[user_fullname]]',$datacreated['USER_FULLNAME'],$FResetPass);
					$FResetPass = str_ireplace('[[comp_accid]]',$datacreated['CUST_ID'],$FResetPass);
					$FResetPass = str_ireplace('[[user_login]]',$datacreated['USER_ID'],$FResetPass);
					$FResetPass = str_ireplace('[[user_email]]',$datacreated['USER_EMAIL'],$FResetPass);
					$FResetPass = str_ireplace('[[user_cleartext_password]]',$newPassword,$FResetPass);
					$FResetPass = str_ireplace('[[master_bank_name]]',$templateEmailMasterBankName,$FResetPass);
					$FResetPass = str_ireplace('[[master_bank_email]]',$templateEmailMasterBankEmail,$FResetPass);
					$FResetPass = str_ireplace('[[master_bank_telp]]',$templateEmailMasterBankTelp,$FResetPass);
					//$FResetPass = str_ireplace('[[user_role]]',$user_role,$FResetPass);

					$emailArr[$emailCtr]['EMAIL'] = $datacreated['USER_EMAIL'];
					$emailArr[$emailCtr]['CONTENT'] = $FResetPass;
					$emailCtr++;

					Application_Helper_Email::queueEmail('benny@sgo.co.id','New Password Information',$FResetPass);
					// die;
		$set = $this->_getParam('lang');
		if($set){
			//// Start Multi Language
			$defaultlanguage=$set;
			$ns = new Zend_Session_Namespace('language');
			$sessionLanguage = $ns->langCode ;
			
			if(!is_null($sessionLanguage)){
				$setlang = $sessionLanguage;
			}else{
				$setlang = $locale->getLanguage();
			}
			if (!empty($setlang) && isset($setlang)) {
				$defaultlanguage=$setlang;
				$ns->langCode = $defaultlanguage;
			}
			Zend_Loader::loadClass('Zend_Translate');
			
			$frontendOptions = array(
				'cache_id_prefix' => 'MyLanguage',
				'lifetime' => 86400,
				'automatic_serialization' => true
			);
			$backendOptions = array('cache_dir' => APPLICATION_PATH.'/../../library/data/cache/language/frontend');
			$cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
			Zend_Translate::setCache($cache);
			
			$translate = new Zend_Translate('tmx', APPLICATION_PATH.'/../../library/data/languages/frontend/language.tmx', $defaultlanguage);
			// 		Zend_Debug::dump($translate);
			Zend_Registry::set('language', $translate);
			//// End Multi Language
		
			$lang = $set;
			$ns->langCode = $lang;
			$this->_redirect('/');
		}
		
		//---------------------- GENERATE CAPTCHA IMAGE  --------------------------------------
		$captcha = Application_Captcha::generateCaptcha ();
		$this->view->captchaId = $captcha ['id']; //returns the ID given to session &amp; image
		$this->view->captImgDir = $captcha ['imgDir'];
		//---------------------	END GENERATE CAPTCHA IMAGE -------------------------------------
		$confirmPage = $this->_getParam('confirmPage',1);
		$tokenAuth = $this->_getParam('tokenAuth',0);

		$challengeCode = NULL;
		$challengeCode = new Service_Token(NULL, NULL);
		$challengeCode = $challengeCode->generateChallengeCode();

		$sessionNameRand = new Zend_Session_Namespace('confirmTransactRand');
		if ($this->_request->isPost ()) {
			$token = '';

			$filters = array (
				'userId' => array ('StripTags', 'StringTrim', 'StringToUpper' ),
				'custId' => array ('StripTags', 'StringTrim', 'StringToUpper' ),
				'password' => array (),
				'challengeCode' => array ('StripTags', 'StringTrim', 'StringToUpper' ),
				'responseCode' => array ('StripTags', 'StringTrim', 'StringToUpper' ),
			);

			$validators = array(
				'userId'         => array(
					'NotEmpty',
//					'Alnum',
					'messages' => array(
						$this->language->_('User ID cannot be empty'),
//						$this->language->_('Invalid User ID Format'),
					),
				),
				'custId'         => array(
					'NotEmpty',
//					'Alnum',
					'messages' => array(
						$this->language->_('Customer ID cannot be empty'),
//						$this->language->_('Invalid Customer ID Format'),
					),
				),
				'password'         => array(
					'NotEmpty',
					'messages' => array(
						$this->language->_('Password cannot be empty'),
					),
				),
				'challengeCode'         => array(),
				'responseCode'         => array(
					'Digits',
					'messages' => array(
						$this->language->_('Invalid Response Token Format'),
					),
				),
			);

			$zf_filter_input = new Zend_Filter_Input ( $filters, $validators, $this->_getAllParams (), $this->_optionsValidator );
			// ========================== END FILTERING =======================================

			//================================BYPASSCAPCAY (|| true========================================//
			
			$sessionNameRand = new Zend_Session_Namespace('confirmTransactRand');	
			$sessionNameRand->randomTransact = $this->_getParam('randomTransact');
			if($this->_getParam('randomTransact') == $sessionNameRand->randomTransact){
				try {
					$passwordRand = $sessionNameRand->randomTransact; //yang hidden param random
					$blocksize = 256;  // can be 128, 192 or 256
					$custidEnc =  $this->_getParam('custId');
					$useridEnc =  $this->_getParam('userId');
					$authkeysEnc =   $this->_request->getParam('password');
// 					print_r($custidEnc);die;
				} 
				catch (Exception $e) {
					$custidEnc = '';
					$useridEnc = '';
					$authkeysEnc = '';
				}
			}
			else{
				$this->view->sessionExpired = $this->language->_('Session expired');
				$this->_redirect('/');
			}	
// 			print_r($custidEnc);die;
			$randomTransact = str_pad(mt_rand(0, 99999999), 8, '0', STR_PAD_LEFT);
			$this->view->randomTransact = $randomTransact;	
			$sessionNameRand->randomTransact = $randomTransact;	
			
			if($confirmPage == 1)
			{

				$compId = $this->_getParam('custId');
// 			    echo $cud;
				//cek company id dan user id match
// 				$compId = $custidEnc;//$this->_getParam('custId');
				$userIdPost = $useridEnc;//$this->_getParam('userId');
				$userStatusPost = 3;
				
				$dataCompId = $this->_db->select()
				->from(
					array('M_USER')
	//				array('CUST_TOKEN_AUTH' => "IFNULL(CUST_TOKEN_AUTH,'N')")
				)
				->where('CUST_ID = ?', $compId)
				->where('USER_ID = ?', $userIdPost)
				->where('USER_STATUS != ?', $userStatusPost)
				->limit(1)
				;
	           // echo $dataCompId;die;
				$dataComp = $this->_db->fetchRow($dataCompId);
				
				if(!$zf_filter_input->isValid())
				{
					$errors = '';
					$listMsg = $zf_filter_input->getMessages();
					foreach($listMsg as $key)
					{
						foreach($key as $msg)
						{
							$errors .= $msg.PHP_EOL;
						}
					}
				}
				elseif(!$dataComp){
					$errors = $this->language->_('Invalid Company ID or User ID');
					Application_Helper_General::writeLogAnonymous('FLGN','Failed Login into System Invalid Company ID or User ID' .$user_ip = $this->getUserIP(),$userId, $custid);
					
				}
				
					$custId = ($custidEnc);
					$tokenAuth = $this->_db->select()
					->from(
						array('M_CUSTOMER'),
						array('CUST_TOKEN_AUTH' => "IFNULL(CUST_TOKEN_AUTH,'N')")
					)
					->where('CUST_ID = ?', (string)$custId)
					->where('CUST_TOKEN_AUTH = ?', 'Y')
					->limit(1)
					;

					$tokenAuth = $this->_db->fetchRow($tokenAuth);
					$tokenAuth = (isset($tokenAuth['CUST_TOKEN_AUTH']) && $tokenAuth['CUST_TOKEN_AUTH'] == 'Y' ? 1 : 0);

				




				$cekpass 	= false;
				$userId = ($useridEnc);//($zf_filter_input->userId);
				$custId = ($custidEnc);//($zf_filter_input->custId);
				$blocksize = 256;
				$passwordRand = $sessionNameRand->randomTransact;
// 				$custId = SGO_Helper_AES::decrypt( $this->_getParam('custId'), $passwordRand, $blocksize );
				$pass = ($authkeysEnc);//($zf_filter_input->password);
				// print_r($tokenAuth);die;
				if ($tokenAuth == '1'){
					$responseCode = $zf_filter_input->getEscaped('responseCode');
					if (!(empty($responseCode))){
						$tokenIdUser = $this->_db->select()
						->from(
							array('M_USER'),
							array('TOKEN_ID')
						)
						->where('USER_ID = ?',$userId)
						->limit(1)
						;
						$tokenIdUser = $this->_db->fetchRow($tokenIdUser);
						$tokenIdUser = $tokenIdUser['TOKEN_ID'];

						if (!(empty($tokenIdUser))){
							$cekToken = new Service_Token($custId, $userId, $tokenIdUser);
							$cekToken = $cekToken->verify($zf_filter_input->getEscaped('challengeCode'), $responseCode);

							if ($cekToken['ResponseCode'] == '00'){
								$Login 		= new Login();
								$result   	= $Login->authenticate($custId, $userId.'@HOST'.$_SERVER['REMOTE_ADDR'], $pass);

								$cekpass	= ($result['responseCode'] == '00')? true: false;
								$errors		= ($result['responseCode'] == '00')?   "": $result['responseDesc'];
							}else{
								$CustUser = new CustomerUser($custId,$userId);
								$CustUser->setCustId($custId);
								$CustUser->setUserId($userId);

// 								$CustUser->setFailedTokenMustLogout();
								$CustUser->setUserFailToken();
								$CustUser->setUserLock();
								$CustUser->setUserLogout();

								$cekpass = false;
								$errors = 'Token '.$cekToken['ResponseDesc'];
							}
						}else{
							$cekpass = false;
							$errors = 'Token has not been enabled';
						}
					}else{
						$cekpass = false;
						$errors = 'Response token cannot be empty ';
					}
				}else{
					$tokenIdUser = $this->_db->select()
						->from(
							array('M_USER'),
							array('USER_FAILEDATTEMPT')
						)
						->where('USER_ID = ?',$userId)
						->limit(1)
						;
						$capcalock = $this->_db->fetchRow($tokenIdUser);
						if($capcalock['USER_FAILEDATTEMPT'] == '3'){
							$this->view->reqcap = 1;
						}
						
					if ($capcalock['USER_FAILEDATTEMPT'] >= 3 && Application_Captcha::validateCaptcha ( $this->_request->getPost ( 'captcha' ) ) === true ) {

						$Login 		= new Login();
						
// 						print_r($pass);die;
						$result   	= $Login->authenticate($custId, $userId.'@HOST'.$_SERVER['REMOTE_ADDR'], $pass);

						$cekpass	= ($result['responseCode'] == '00')? true: false;
						$errors		= ($result['responseCode'] == '00')?   "": $result['responseDesc'];
					}else if($capcalock['USER_FAILEDATTEMPT'] < 2) {
						// die('re');

						$Login 		= new Login();
						
// 						print_r($pass);die;
						$result   	= $Login->authenticate($custId, $userId.'@HOST'.$_SERVER['REMOTE_ADDR'], $pass);

						$cekpass	= ($result['responseCode'] == '00')? true: false;
						$errors		= ($result['responseCode'] == '00')?   "": $result['responseDesc'];
						
						
					}else if($capcalock['USER_FAILEDATTEMPT'] = 2) {
						// die;

						$Login 		= new Login();
						
// 						print_r($pass);die;
						$result   	= $Login->authenticate($custId, $userId.'@HOST'.$_SERVER['REMOTE_ADDR'], $pass);

						$cekpass	= ($result['responseCode'] == '00')? true: false;
						$errors		= ($result['responseCode'] == '00')?   "": $result['responseDesc'];
						$this->view->reqcap = 1;
						
						
					}else{
						$this->view->reqcap = 1;
						$errors = $this->language->_('Invalid Captcha');
						
						// $cekpass = true;
					}
				}

				if($cekpass){
					try{
						$this->_db->delete('TEMP_SESSION_USER',array(
								'CUST_ID = ?'=>$custId,
								'USER_ID = ?'=>$userId,
						));
						$this->_db->insert('TEMP_SESSION_USER',array(
								'CUST_ID'=>$custId,
								'USER_ID'=>$userId,
								'USER_LASTACTIVITY'=>new Zend_Db_expr('now()'),
								'TOKEN_WS'=>$token,
						));
					}catch(Exception $e){
						Application_Helper_General::exceptionLog($e);
					}

					$Settings = new Settings();
					$allSetting = $Settings->getAllSetting();

					$minDiff = $allSetting['timeoutafter'];

					$objSessionNamespace = new Zend_Session_Namespace( 'Zend_Auth' );
					$timeExprSession = 600 + $minDiff *60;//tambah 600 karena scheduler force logout 10 menit sekali
					$objSessionNamespace->setExpirationSeconds( $timeExprSession );
					// set keterangan login ke auth
					$CustomerUser	= new CustomerUser(strtoupper($custId), strtoupper($userId));
					$listAccount 	= $CustomerUser->getAccounts();
					$accountList 	= Application_Helper_Array::simpleArray($listAccount,'ACCT_NO');

					$listPrivilege = $CustomerUser->getPrivileges();
					$privilege = Application_Helper_Array::simpleArray($listPrivilege,'FPRIVI_ID');

					$privilege[] = 'systemPrivilege';

					// set keterangan login ke auth
					$auth = Zend_Auth::getInstance();
					$data = new stdClass();
					// print_r($result);die;
					$data->isCommLinkage = false;
					$data->userIdLogin    = $userId;
					$data->userNameLogin  = ucwords(strtolower($result['userNameLogin']));
					$data->custNameLogin  = ucwords(strtolower($result['custNameLogin']));
					$data->priviIdLogin   = $privilege;
					$data->allSetting     = $allSetting;
					// $data->userGroupLogin = $groupArr;
					$data->custIdLogin    = strtoupper($custId);
					$data->tokenIdLogin    = $result['tokenId'];
					$data->accountList    = $accountList;
					$data->lastLogin = $result['lastLogin'];
					$data->custNameLogin = $result['custNameLogin'];
					$data->custSameUser = $result['custSameUser'];

					// cek userSessionHash before login
					$userSessionHash	= $this->_db->fetchrow (
								$this->_db->SELECT ('SESSION_HASH')->FROM ( 'M_USER' )
								->WHERE ( 'USER_ID = ?', $userId )
								->WHERE ('CUST_ID =?',$custId)
								);
					//


					// ---------------------------------------- HASH -----------------------------------------------  //
					$hash = $this->hashKey($userId,$custId);
					$data->userHash     = $hash;
					
					$auth->getStorage()->write($data);
					// End set keterangan login ke auth
					//$browser = substr($_SERVER['HTTP_USER_AGENT'], 0,24);
					$browser = $_SERVER['HTTP_USER_AGENT'];
					$sessionHash = md5($_SERVER['REMOTE_ADDR'].$browser);
					
					//update USER_ISLOGIN
					$updateArr['USER_ISLOGIN'] = 1;
					$updateArr['USER_ISLOCKED'] = 0;
					$updateArr['USER_LASTLOGIN'] = new Zend_Db_Expr('now()');
					$updateArr['USER_LASTACTIVITY'] = new Zend_Db_Expr('now()');
					$updateArr['USER_FAILEDATTEMPT'] = 0;
					$updateArr['USER_ISNEW'] = 0;
					$updateArr['USER_HASH'] = $hash;
					$updateArr['SESSION_HASH'] = $sessionHash;
					$whereArr = array('CUST_ID = ?' => (string)$custId,
							'USER_ID = ?' => (string)$userId);
					$customerupdate = $this->_db->update('M_USER',$updateArr,$whereArr);
					
					
					if ($userSessionHash['SESSION_HASH'] != $sessionHash){
					
						//send email
						$EmailLoginNotif 				= $allSetting['femailtemplate_loginnotif'];
						$templateEmailMasterBankName	= $allSetting['master_bank_name'];
						$templateEmailMasterBankAppName = $Settings->getSetting('master_bank_app_name');
						$templateEmailMasterBankEmail	= $allSetting['master_bank_email'];
						$templateEmailMasterBankTelp	= $allSetting['master_bank_telp'];
	
						$userData	= $this->_db->fetchrow (
								$this->_db->SELECT ()->FROM ( 'M_USER' )
								->WHERE ( 'USER_ID = ?', $userId )
								->WHERE ('CUST_ID =?',$custId)
							);						
						
						$lastlogin = Application_Helper_General::convertDate($userData['USER_LASTLOGIN'],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);

						// masking user id
						$lengthUserId = strlen($userId);
						$maskchar = substr($userId,1,$lengthUserId-3);
						$lengthMaskchar = strlen($maskchar);
						$mask = str_repeat("*",$lengthMaskchar);
						$userIdMask = str_replace($maskchar, $mask, $userId)."<br>";
						// end masking						
						
						$data = array( '[[user_fullname]]' => $userData['USER_FULLNAME'],
									'[[user_login]]' => $userIdMask,
									'[[user_email]]' => $userData['USER_EMAIL'],
									'[[user_lastlogin]]' => $lastlogin,
									'[[user_agent]]' => $this->get_browser_name($_SERVER['HTTP_USER_AGENT']),//$browser,
									'[[master_bank_name]]' => $templateEmailMasterBankName,
									'[[master_bank_app_name]]' => $templateEmailMasterBankAppName,
									'[[master_bank_email]]' => $templateEmailMasterBankEmail,
									'[[master_bank_telp]]' 	=> $templateEmailMasterBankTelp
									);
	
						$EmailLoginNotif = strtr($EmailLoginNotif,$data);
						Application_Helper_Email::sendEmail($userData['USER_EMAIL'],'Login Notification',$EmailLoginNotif);					
						//end send email
					}
					

					//update latest transaction cache
					$this->latestTransaction($custId,$userId);
				// 	print_r($cekpass);
				// die;
					Application_Helper_General::writeLog('FLGN','Login Success '.$user_ip = $this->getUserIP());
					$this->_redirect ( '/home/dashboard' );
				}else{
					$confirmPage = 1;
				}
			}
			else
			{
			    
			}
		} // END REQUEST IS POST
		else{
			$randomTransact = str_pad(mt_rand(0, 99999999), 8, '0', STR_PAD_LEFT);
			$this->view->randomTransact = $randomTransact;	
			$sessionNameRand->randomTransact = $randomTransact;
		}
		
		$this->view->challengeCode = $challengeCode;
		$this->view->confirmPage = $confirmPage;
		$this->view->tokenAuth = $tokenAuth;

		if(isset($errors)){
			$this->view->errors = $errors;
		}

//		$this->view->userId = (isset($zf_filter_input->userId)) ? $zf_filter_input->userId : $this->_getParam('userId');
		$this->view->userId = (isset($useridEnc)) ? $useridEnc : $useridEnc;
		
//		$this->view->custId = (isset($zf_filter_input->custId)) ? $zf_filter_input->custId : $this->_getParam('custId');
		$this->view->custId = (isset($custidEnc)) ? $custidEnc : $custidEnc;
	}

	public function hashKey($userId,$custId) {
		do
		{
			$key = null;
			for($i=0;$i<3;$i++)
				$key .= rand(0,9);

			$browser = $_SERVER['HTTP_USER_AGENT'];
			$hash = md5($_SERVER['REMOTE_ADDR'].$browser.$key);
			$checkHash = null;
			// CHECK HASH
			$checkHash = $this->_db->fetchOne(
																			$this->_db->SELECT()
																								->FROM('M_USER' ,array('USER_HASH'))
																								->WHERE('USER_ID =?',$userId)
																								->WHERE('CUST_ID =?',$custId)
																								->WHERE('USER_HASH =?',$hash)
																			);
		}while(!empty($checkHash));

		return $hash;
	}
	
	private function latestTransaction($custId,$userId){
    	$arrPayType 	= array_combine($this->_paymenttype["code"],$this->_paymenttype["desc"]);

    	$casePayType = "(CASE P.PS_TYPE ";
  		foreach($arrPayType as $key => $val)	{ $casePayType .= " WHEN ".$key." THEN '".$val."'"; }
  		$casePayType .= " END)";

    	$frontendOptions = array ('lifetime' => 900, 
								  'automatic_serialization' => true );
		$backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/latesttrans/' ); // Directory where to put the cache files
		$cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );

		$cacheID = 'LT'.$custId.$userId;

		if(!$result = $cache->load($cacheID)) 
		{
			$select = $this->_db->select()
								->from(array('P' => 'T_PSLIP'), array())
								->join(array('T' => 'T_TRANSACTION'), 'P.PS_NUMBER = T.PS_NUMBER', array())
								->joinLeft(array('M' => 'M_SERVICE_PROVIDER'), 'P.PS_BILLER_ID = M.PROVIDER_ID', array('P.PS_NUMBER','PAYMENT_TYPE' => $casePayType,'T.BENEFICIARY_ACCOUNT','P.PS_CCY','P.PS_TOTAL_AMOUNT','M.PROVIDER_NAME'))
								->where('P.CUST_ID = ?', $custId)
								->where('P.PS_RELEASER_USER_LOGIN = ?', $userId)
								->where('P.PS_STATUS = 5')
								->where('T.TRA_STATUS = 3')
								->order('P.PS_CREATED DESC')
								->limit(5);

			$result = $this->_db->fetchAll($select);
			
			$cache->save($result,$cacheID);
		}
    }
	
	public function langAction() {
		$set = $this->_getParam('set');
	 
		//// Start Multi Language
		$defaultlanguage=$set;
		$ns = new Zend_Session_Namespace('language');
		$sessionLanguage = $ns->langCode;

		if(!is_null($sessionLanguage)){
			$setlang = $sessionLanguage;
		}else{
			$setlang = $locale->getLanguage();
		}
		if (!empty($setlang) && isset($setlang)) {
			$defaultlanguage=$setlang;
			$ns->langCode = $defaultlanguage;
		}
		Zend_Loader::loadClass('Zend_Translate');

		$frontendOptions = array(
			'cache_id_prefix' => 'MyLanguage',
			'lifetime' => 86400,
			'automatic_serialization' => true
		);
		$backendOptions = array('cache_dir' => APPLICATION_PATH.'/../../library/data/cache/language/frontend');
		$cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
		Zend_Translate::setCache($cache);

		$translate = new Zend_Translate('tmx', APPLICATION_PATH.'/../../library/data/languages/frontend/language.tmx', $defaultlanguage);
// 		Zend_Debug::dump($translate);
		Zend_Registry::set('language', $translate);
		//// End Multi Language

		$lang = $set;
		$ns->langCode = $lang;
		
	}

	public function logoutAction() {
		Application_Helper_General::writeLog('FLGT','Logout From System '.$user_ip = $this->getUserIP());
		if(Zend_Auth::getInstance()->hasIdentity()){
			$auth = Zend_Auth::getInstance()->getIdentity();
    	  	$this->_userIdLogin   = $auth->userIdLogin;
    	  	$this->_custIdLogin   = $auth->custIdLogin;
    	  	Zend_Auth::getInstance()->clearIdentity();
			$CustomerUser	= new CustomerUser(strtoupper($this->_custIdLogin),strtoupper($this->_userIdLogin));
			$CustomerUser->forceLogout();

			$forceBySession = $this->_request->getParam('session');

			if($forceBySession == '1'){
				$this->_redirect('/authorizationacl/index/disableuser');
			}
		}
		$this->_redirect('/');
		
//		$this->_redirector->gotoUrl('login/');
	}

	

}