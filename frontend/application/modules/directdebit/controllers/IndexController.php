<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';

class directdebit_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF'; // masih harus diganti

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr 	  = $CustomerUser->getAccounts(array());	// show acc in IDR only

		$this->view->AccArr = $AccArr;

		$model = new purchasing_Model_Purchasing();

		$purposeArr = $model->getTranspurpose();
		$purposeList = array(''=>'-- Select Transaction Purpose --');
		foreach ($purposeArr as $key => $value ){

			$purposeList[$value['CODE']] = $value['DESCRIPTION'];
		}

		$this->view->TransPurposeArr = $purposeList;
	}
	
	
}
