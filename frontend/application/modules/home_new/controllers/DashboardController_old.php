<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Settings.php';
require_once 'General/SystemBalance.php';
require_once 'General/CustomerUser.php';
// require_once 'SGO/Extendedmodule/FPDF/FPDI_Protection.php';


class home_DashboardController extends Application_Main {


    
    public function indexAction()
    {   


        $this->_helper->_layout->setLayout('newlayout');
        $frontendOptions = array ('lifetime' => 259200, 
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'BAL'.$this->_custIdLogin.$this->_userIdLogin;

        $setting = new Settings();              
        $enc_pass = $setting->getSetting('enc_pass');
        $enc_salt = $setting->getSetting('enc_salt');
        $sessionNamespace = new Zend_Session_Namespace('Tokenenc');          
        $pw_hash = md5($enc_salt.$enc_pass);
        $rand = $this->_userIdLogin.date('dHis').$pw_hash;
        $sessionNamespace->token    = $rand;
        $this->view->token = $sessionNamespace->token;

        // get pie BG Information

        $select2 = $this->_db->select()
                          ->from(array('BG' => 'V_PIEBGINFORMATION'),array('*'))
                          ->where('BG.CUST_ID ='.$this->_db->quote((string)$this->_custIdLogin));
        //$arr = $this->_db->fetchAll($select2);                          
		$arr = array();

        foreach ($arr as $key => $value)
        {
            $this->view->amountReview = $value['BG_AMOUNT_REVIEW'];
            $this->view->amountRelease = $value['BG_AMOUNT_RELEASE'];
            $this->view->amountApprove = $value['BG_AMOUNT_APPROVE'];
            $this->view->amountIssued = $value['BG_AMOUNT_ISSUED'];
            $this->view->dataReview = $value['COUNT_DATA_REVIEW'];
            $this->view->dataRelease = $value['COUNT_DATA_RELEASE'];
            $this->view->dataApprove = $value['COUNT_DATA_APPROVE'];
            $this->view->dataIssued = $value['COUNT_DATA_ISSUED'];
            
        }
        // end get pie BG Information

        // get pie BG Warranty

        // get pie Realisasi Anggaran
        $this->view->totalAnggaran2021 = (125917822000/1000000000);
        $this->view->realisasiSP2D2021 = (90177172000/1000000000);
        $this->view->sisaAnggaran2021 = (35740650000/1000000000);

        $this->view->totalAnggaran2020 = (106172192100/1000000000);
        $this->view->realisasiSP2D2020 = (105983172000/1000000000);
        $this->view->sisaAnggaran2020 = (189020100/1000000000);

        $this->view->totalAnggaran2019 = (99816521100/1000000000);
        $this->view->realisasiSP2D2019 = (65001862600/1000000000);
        $this->view->sisaAnggaran2019 = (34814658500/1000000000);
        /*$select2 = $this->_db->select()
                          ->from(array('BG' => 'V_PIEBGINFORMATION'),array('*'))
                          ->where('BG.CUST_ID ='.$this->_db->quote((string)$this->_custIdLogin));
        $arr = $this->_db->fetchAll($select2);                          

        foreach ($arr as $key => $value)
        {
            $this->view->amountReview = $value['BG_AMOUNT_REVIEW'];
            $this->view->amountRelease = $value['BG_AMOUNT_RELEASE'];
            $this->view->amountApprove = $value['BG_AMOUNT_APPROVE'];
            $this->view->amountIssued = $value['BG_AMOUNT_ISSUED'];
            $this->view->dataReview = $value['COUNT_DATA_REVIEW'];
            $this->view->dataRelease = $value['COUNT_DATA_RELEASE'];
            $this->view->dataApprove = $value['COUNT_DATA_APPROVE'];
            $this->view->dataIssued = $value['COUNT_DATA_ISSUED'];
            
        }*/
        // end get pie Realisasi Anggaran

        // get pie SP2D
        $this->view->totalNilaiKontrak2021 = (90177172000/1000000000);
        $this->view->totalNilaiDibayar2021 = (73118571900/1000000000);
        $this->view->menungguTinjauan2021 = (10208671000/1000000000);
        $this->view->menungguPersetujuan2021 = (3781281900/1000000000);
        $this->view->menungguRilis2021 = (3068647200/1000000000);

        $this->view->totalNilaiKontrak2020 = (105983172000/1000000000);
        $this->view->totalNilaiDibayar2020 = (105983172000/1000000000);
        $this->view->menungguTinjauan2020 = 0;
        $this->view->menungguPersetujuan2020 = 0;
        $this->view->menungguRilis2020 = 0;

        $this->view->totalNilaiKontrak2019 = (65001862600/1000000000);
        $this->view->totalNilaiDibayar2019 = (65001862600/1000000000);
        $this->view->menungguTinjauan2019 = 0;
        $this->view->menungguPersetujuan2019 = 0;
        $this->view->menungguRilis2019 = 0;

        
        /*$select3 = $this->_db->select()
                          ->from(array('BG' => 'V_PIEBGWARRANTY'),array('*'))
                          ->where('BG.CUST_ID ='.$this->_db->quote((string)$this->_custIdLogin));
        $arr3 = $this->_db->fetchAll($select3);                          

        foreach ($arr3 as $key3 => $value3)
        {
            $this->view->amountFullCover = $value3['BG_AMOUNT_FULLCOVER'];
            $this->view->amountLineFacility = $value3['BG_AMOUNT_LINEFACILITY'];
            $this->view->amountInsurance = $value3['BG_AMOUNT_INSURANCE'];
            $this->view->dataFullCover = $value3['COUNT_DATA_FULLCOVER'];
            $this->view->dataLineFacility = $value3['COUNT_DATA_LINEFACILITY'];
            $this->view->dataInsurance = $value3['COUNT_DATA_INSURANCE'];
            
        }*/
        // end get pie SP2D

        $select3 = $this->_db->select()
                          ->from(array('BG' => 'V_PIEBGWARRANTY'),array('*'))
                          ->where('BG.CUST_ID ='.$this->_db->quote((string)$this->_custIdLogin));
        //$arr3 = $this->_db->fetchAll($select3);                          
		
		$arr3 = array();
        /*echo "<pre>";
        var_dump($arr3);
        die();*/
        foreach ($arr3 as $key3 => $value3)
        {
            $this->view->amountFullCover = $value3['BG_AMOUNT_FULLCOVER'];
            $this->view->amountLineFacility = $value3['BG_AMOUNT_LINEFACILITY'];
            $this->view->amountInsurance = $value3['BG_AMOUNT_INSURANCE'];
            $this->view->dataFullCover = $value3['COUNT_DATA_FULLCOVER'];
            $this->view->dataLineFacility = $value3['COUNT_DATA_LINEFACILITY'];
            $this->view->dataInsurance = $value3['COUNT_DATA_INSURANCE'];
            
        }
        // end get pie BG Warranty
        
        // get theme color
        $theme = $this->_userTheme;
        $this->view->theme = $theme;
        
        $param = array('USER_ID'=>$userId, 'CUST_ID'=>$custId);
        $model = new accountstatement_Model_Accountstatement();     
        $userAcctInfo = $model->getCustomerAccountKurs($param);
        // print_r($userAcctInfo);die;
        $accountNo  = $userAcctInfo[0]['ACCT_NO'];
        //var_dump($accountNo); die;
        $accountType = $userAcctInfo[0]['ACCT_TYPE'];
        $accountCcy = $userAcctInfo[0]['CCY_ID'];
        $acct_data = $this->_db->fetchAll(
            $this->_db->select()->distinct()
                 ->from(array('A' => 'M_CUSTOMER_ACCT'))
                 ->join(array('C' => 'M_MAKERLIMIT'),'A.ACCT_NO = C.ACCT_NO',array())
                 ->join(array('D' => 'M_PRODUCT_TYPE'),'D.PRODUCT_CODE = A.ACCT_TYPE',array('D.PRODUCT_CODE','D.PRODUCT_NAME'))
                 ->joinLeft(array('B' => 'M_GROUPING'),'A.GROUP_ID = B.GROUP_ID',array('GROUP_NAME'))
                 ->where("A.ACCT_STATUS = 1")
                 ->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin))
                 ->where("C.USER_LOGIN = ".$this->_db->quote($this->_userIdLogin))
                 ->where("C.MAXLIMIT > 0")                   
                 ->order('B.GROUP_NAME DESC')
                 ->order('A.ORDER_NO ASC')
        );
         
        if(!$result = $cache->load($cacheID)) 
        //  if(true)    
        {
            // die;
            $data = $this->_db->fetchAll(
                        $this->_db->select()->distinct()
                             ->from(array('A' => 'M_CUSTOMER_ACCT'))
                             ->join(array('C' => 'M_MAKERLIMIT'),'A.ACCT_NO = C.ACCT_NO',array())
                             ->join(array('D' => 'M_PRODUCT_TYPE'),'D.PRODUCT_CODE = A.ACCT_TYPE',array('D.PRODUCT_CODE','D.PRODUCT_NAME'))
                             ->joinLeft(array('B' => 'M_GROUPING'),'A.GROUP_ID = B.GROUP_ID',array('GROUP_NAME'))
                             ->where("A.ACCT_STATUS = 1")
                             ->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin))
                             ->where("C.USER_LOGIN = ".$this->_db->quote($this->_userIdLogin))
                             ->where("C.MAXLIMIT > 0")                   
                             ->order('B.GROUP_NAME DESC')
                             ->order('A.ORDER_NO ASC')
                             ->GROUP('C.ACCT_NO')
                );

            $tempList = array();
            $acct = array();
            foreach($data as $key => $row ){ 
                // $systemBalance = new SystemBalance($this->_custIdLogin,$row['ACCT_NO'],Application_Helper_General::getCurrNum($row['CCY_ID']));
                // $systemBalance->setFlag(false);
                // $systemBalance->checkBalance();
                $balance = $this->_db->fetchAll(
                        $this->_db->select()->distinct()
                             ->from(array('A' => 'T_BALANCE'))
                             ->where("A.ACCT_NO = ?",$row['ACCT_NO'])
                             ->order('DATE(A.PLAFOND_END) DESC')
                            );
                //var_dump($balance);
                
                $acct[$key]['ACCT_NO'] = $row['ACCT_NO'];
                $acct[$key]['acct_name'] = $row['ACCT_ALIAS_NAME'];
                $acct[$key]['acct_ccy'] = $row['CCY_ID'];
                if(!empty($balance)){
                    $balance = $balance['0']['PLAFOND'];
                }else{
                  //  $balance = 0;
                  //demo 
                    $balance = rand(10000,100000000);
                }
                $acct[$key]['balance'] = $row['CCY_ID']." ".Application_Helper_General::displayMoney($balance);
                array_push($tempList, $arr);
            }
            //var_dump($acct);die;
            $save['templist'] = $acct;
            $result = $acct;
             $suggest = array();
             // var_dump($acct);die;
            foreach ($acct as $key => $value) {
                        $suggest[] = $value['ACCT_NO'];     
            }
            if(empty($suggest)){
                $suggest = '';
            }
            $statementavg = $this->_db->select()
                    ->FROM(array('T' => 'T_STATEMENT'), array('T.STAT_TYPE','T.STAT_DATE', 'TOTAL' => 'SUM(T.STAT_AMOUNT)','MONTH' => "CONCAT(MONTHNAME(T.STAT_DATE),' ',YEAR(T.STAT_DATE))"))
                    ->WHERE("T.ACCT_NO IN(?)", $suggest)
                    ->where("DATE(T.STAT_DATE) > DATE_ADD(NOW(), INTERVAL -4 MONTH)")
                    // ->GROUP("T.STAT_TYPE,T.STAT_DATE")
                    ->GROUP("YEAR(T.STAT_DATE), MONTH(T.STAT_DATE)")

                    ->order("T.STAT_DATE ASC");

            $dataavg = $this->_db->select()->FROM(array('a' => $statementavg),array('AVG' =>'AVG(TOTAL)','STAT_DATE'))
                                ->GROUP('a.STAT_DATE')
                                ->order("a.STAT_DATE ASC")
                                ->limit(4)
                                //echo $dataavg;die;
                                ->QUERY()
                        ->FETCHALL();
            $save['dataavg'] = $dataavg;

           

             $statement = $this->_db->select()
                    ->FROM(array('T' => 'T_STATEMENT'), array('T.STAT_TYPE','T.STAT_DATE', 'TOTAL' => 'SUM(T.STAT_AMOUNT)','MONTH' => "CONCAT(MONTHNAME(T.STAT_DATE),' ',YEAR(T.STAT_DATE))"))
                    ->WHERE("T.ACCT_NO IN(?)", $suggest)
                    ->WHERE("DATE(T.STAT_DATE) > DATE_ADD(NOW(), INTERVAL -4 MONTH)")
                    // ->GROUP("T.STAT_TYPE,T.STAT_DATE")
                    ->GROUP("T.`STAT_TYPE`,YEAR(T.STAT_DATE), MONTH(T.STAT_DATE)")
                    ->order("T.STAT_DATE ASC")
                    ->limit(8)
                    //echo $statement;die;
                    ->QUERY()
                    ->FETCHALL();
                    
            $save['statement'] = $statement;
            // var_dump($suggest);die;
            $dchartdata = $this->_db->select()
                    ->FROM(array('T' => 'T_BALANCE'), array('T.PLAFOND','T.ACCT_NO','T.CCY_ID'))
                    ->WHERE("T.ACCT_NO IN(?)", $suggest)
                   ->where("DATE(T.PLAFOND_END) = DATE(NOW())")
                    ->ORDER("T.PLAFOND_END DESC")
                    ->group("T.ACCT_NO")
                    // ->group("T.PLAFOND_END")
                    ->QUERY()
                    
                    ->FETCHALL();
                    // echo "<pre>";
                    // EC
            // var_dump($dchartdata);die;
          $totaldchart = $this->_db->select()
                    ->FROM(array('T' => 'T_BALANCE'), array('TOTAL'=>'SUM(T.PLAFOND)'))
                    ->WHERE("T.ACCT_NO IN(?)", $suggest)
                    ->where("DATE(T.PLAFOND_END) = DATE(NOW())")
                    ->QUERY()
                    ->FETCHALL();
            // var_dump($totaldchart);die;
            $cashin = array();
            $cashout = array();
            $everage = array();
            $month = array();
         if(!empty($statement)){
         foreach ($statement as $key => $value) {
            if($value['STAT_TYPE'] == 'C'){
                $cashin[] = (int)$value['TOTAL'];
                $in = $value['TOTAL'];
                $month[] = $value['MONTH'];
            }else if($value['STAT_TYPE'] == 'D'){
                $cashout[] = (int)$value['TOTAL'];
                $out = -$value['TOTAL'];
            }
         }

         }else{
            $cashin = array();
            $cashout = array();
            
            $month = array();
         }

         
         // var_dump($cashout);die;

         if(!empty($dataavg)){
         foreach ($dataavg as $key => $value) {
                $everage[] = (int)$value['AVG'];
         }
        }else{
            $everage = array();
        }


         $dchart = array();
         $total = 0;
         if(!empty($totaldchart)){
            $total = $totaldchart['0']['TOTAL'];
         }
        // var_dump($total);die;
         // var_dump($dchartdata);

                        

        // var_dump($dchart);die;


        $userId = $this->_userIdLogin;
        $model = new accountstatement_Model_Accountstatement();
        $userCIF = $this->_db->fetchRow(
                $this->_db->select()
                ->from(array('C' => 'M_USER'))
                ->join(array('G' => 'M_CUSTOMER'), 'C.CUST_ID = G.CUST_ID', array('G.CUST_CIF'))
                ->where("USER_ID = ".$this->_db->quote($this->_userIdLogin))
                ->limit(1)
        );
        //die('here');
        $param['userId'] = $userId;
        $userAcctInfo = $model->getCustAccount($param);
        $accountNo  = $userAcctInfo[0]['ACCT_NO'];
        $accountType = $userAcctInfo[0]['ACCT_TYPE'];
        $accountCcy = $userAcctInfo[0]['CCY_ID'];
        //print_r($accountCcy);
        //print_r($userCIF['CUST_CIF']);
        //print_r($accountType);
        //print_r($accountNo);
        //die;
        if(!empty($userAcctInfo)){
        $Account = new Account($accountNo, $accountCcy, $accountType, $userCIF['CUST_CIF'], $userId);
        $Account->setFlag(false);
        $resLoan = $Account->checkBalanceLoan();
        $accountListDataLoan = $resLoan['AccountList'];

        $resDeposit = $Account->checkBalanceDeposit();
        //print_r($resDeposit);die;

        // var_dump($resDeposit);
        // var_dump($resLoan);die;

        $accountListDataDeposit = $resDeposit['AccountList'];

//      $tempDeposit = array();
//      $tempDeposit[0]['DEPOSIT_NO'] = "20015001303311";
//      $tempDeposit[0]['PROD_TYPE'] = "My Depo";
//      $tempDeposit[0]['CCY_ID'] = "IDR";
//      $tempDeposit[0]['AVAIL_BAL'] = "20,000,000.00";

//      $tempDeposit[1]['DEPOSIT_NO'] = "20015001304344";
//      $tempDeposit[1]['PROD_TYPE'] = "My Depo 2";
//      $tempDeposit[1]['CCY_ID'] = "IDR";
//      $tempDeposit[1]['AVAIL_BAL'] = "35,000,000.00";

        $svcInquiry = new Service_Inquiry($this->_userIdLogin,$accountNo,$accountType);
        $resultKursEx = $svcInquiry->rateInquiry();



        $kurs = '';
        // var_dump($resultKursEx['DataList']);
            foreach ($resultKursEx['DataList'] as $val){
                if($val['currency']=='USD'){
                    $kurs = $val['sell'];
                }
            }

         if(!empty($dchartdata)){
            $percent = 0;
         foreach ($suggest as $keys => $val) {
            
            foreach ($dchartdata as $key => $value) {
                
                if($value['ACCT_NO'] == $val){
                        if($value['PLAFOND'] != 0){
                            if($value['CCY_ID'] == 'IDR'){
                                $rata = ((int)$value['PLAFOND']/(int)$total)*100;
                            }else{
                                // var_dump($kurs);
                                // var_dump($value['PLAFOND']);die;
                                $kurs = str_replace(',','',$kurs);
                              $rata = (((int)$value['PLAFOND']*$kurs)/(int)$total)*100;  
                            }
                        }else{
                            $rata = (int)0;
                        }
                    // var_dump($rata);
                         // if($keys<=4){
                            $dchart[$keys][0] = $value['ACCT_NO'];
                        $dchart[$keys][1] = $rata;
                        // var_dump($dchart);die;
                         // }else{
                         //    $dchart[5][0] = 'Others';
                         //    $ps = ((int)$value['PLAFOND']/(int)$total)*100;
                         //    $percent = $percent + ((int)$value['PLAFOND']/(int)$total)*100;
                         //    // echo "<pre>";
                         //    // print_r($percent);
                         //    $percent = $percent + ((int)$value['PLAFOND']/(int)$total)*100; 
                         //    $dchart[5][1] = $percent;               
                         // }
                }
                }
            }
            
            if(empty($dchart[$keys])){
                $dchart[$keys][] = $val;
                $dchart[$keys][] = (int)0;
            }

         }
         

        else{
            $dchart = array();
        }

        if(!empty($accountListDataDeposit)){
            $tempDeposit = $accountListDataDeposit;
//          print_r($tempDeposit);die;
            if (count($tempDeposit) > 0)
            {
                $datadeposit = array();
                $tdep = array();
                // $depodata = array();
                $totaldepo = 0;
                foreach ( $tempDeposit as $key=>$row ) {
                    if (isset($row ['accountNo'])){

                        $datadeposit[$key]['ACCT_NO'] = $row['accountNo'];
                        //                  $datadeposit[$key]['PRODUCT_NAME'] = $row['PRODUCT_NAME'];
                        $datadeposit[$key]['AVAILABLE_BALANCE'] = $row['amount'];
                        $kurs = '';
                        foreach ($resultKursEx['DataList'] as $val){
                            if($val['currency']==$row['currency']){
                                $kurs = $val['sell'];
                            }
                        }
                        $plain_amount = str_replace(',','',$row['amount']);
                        if(!empty($kurs)){
                            $kurs = str_replace(',','',$kurs);
                            $equivalen = $plain_amount*$kurs;

                        }else if($row['currency'] == 'IDR'){
                            $equivalen = str_replace(',','',$row['amount']);
                        }else{
                            $equivalen = 'N/A';
                        }


                        $datadeposit[$key]['EQUIVALEN'] = $equivalen;
                        // $depodata[$row['accountNo']] = $equivalen;
                        $totaldepo += $equivalen;

                        if($row['currency']=='IDR'){
                        $tdep[$row['currency']] += $plain_amount;
                        }else if($row['currency']=='USD'){
                        $tdep[$row['currency']] += $plain_amount;
                        }else if($row['currency']=='JPY'){
                        $tdep[$row['currency']] += $plain_amount;
                        }
                    }
                }
            }

        }else{
            $dataDeposit = array();
            $depodata = array();
        }
    }


        $this->view->totaldepo = $totaldepo;
        $this->view->tdep = $tdep;
//
        if(!empty($accountListDataLoan)){
            $tempLoan = $accountListDataLoan;
            if (count($tempLoan) > 0)
            {
                $dataloan = array();
                $tloan = array();
                $totalloan = 0;
                // $loandata = array();
             // print_r($tempLoan);die;
                foreach ( $tempLoan as $key=>$row ) {
                    if (isset($row ['accountNo'])){
                        //print_r($row ['productPlan']);
                        //print_r($product_name);die;
                        $dataloan[$key]['ACCT_NO']      = $row['accountNo'];
                        $kurs = '';
                        foreach ($resultKursEx['DataList'] as $val){
                            if($val['currency']==$row['currency']){
                                $kurs = $val['sell'];
                            }
                        }
                     $plain_amount = str_replace(',','',$row['outstanding']);
                        if($row['currency'] != 'IDR'){
                             $kurs = str_replace(',','',$kurs);
                            $equivalen = $plain_amount*$kurs;
                            // var_dump($plain_amount);
                            // var_dump($kurs);
                            // die('here');
                            // $equivalen = Application_Helper_General::displayMoney($equivalen);
                        }else if($row['currency'] == 'IDR'){
                            $equivalen = str_replace(',','',$row['outstanding']);
                        }else{
                            $equivalen = 0;
                        }

                        // $loandata[$row['accountNo']] = $equivalen;

                        $dataloan[$key]['EQUIVALEN'] = $equivalen;
                        $totalloan += $equivalen;
                        
                    }
                }
            }
        }else{
            $dataloan = array();
            $tempLoan = array();
            $loandata = array();
        }

        $this->view->totalloan = $totalloan;

        array_push($tloan,"");
        $this->view->tloan = $tloan;

        $totalamount = $total + $totaldepo+ $totalloan;

        $percentasset = ($total/$totalamount)*100;
        $percentdepo = ($totaldepo/$totalamount)*100;
        $percentloan = ($totalloan/$totalamount)*100;

        $this->view->totalasset = $total;
        $this->view->totaldepo = $total;
        $this->view->totalloan = $total;
        

        $loandata = array();
        // echo "<pre>";
        // var_dump($totalloan);
        // var_dump($dataloan);die;
        if(!empty($dataloan)){

            foreach ($dataloan as $key => $value) {
                // [$value['ACCT_NO']]
                $loandata[$key][] = $value['ACCT_NO'];
                $loandata[$key][] = ($value['EQUIVALEN']/$totaldepo)*100;
            }

        }
        $depodata = array();
        if(!empty($datadeposit)){

            foreach ($datadeposit as $key => $value) {
                $depodata[$key][] = $value['ACCT_NO'];
                $depodata[$key][] = ($value['EQUIVALEN']/$totaldepo)*100;
                // $depodata[$value['ACCT_NO']] = ($value['EQUIVALEN']/$totaldepo)*100;
            }

        }
        

        
        $save['loandata'] = $loandata;
        $save['depodata'] = $depodata;
        $save['percentasset'] = $percentasset;
        $save['percentloan'] = $percentloan;
        $save['percentdepo'] = $percentdepo;
        // var_dump($total);die;
        // die;
        // if(!empty($dchart)){
        //     foreach ($dchart as $key => $value) {
        //          $booltest = is_int($dchart[$key][1]);
        //             if(!$booltest){
        //                 $dchart[$key][0] = $dchart[$key][0];
        //                 $dchart[$key][1] = (int)0;
        //             }
                
        //     }
        // }
        
            if(empty($dchart)){
                $totalacct = count($suggest);
                $percent = (1/$totalacct)*100;

                foreach ($suggest as $key => $value) {
                            $dchart[$key][] = $value;
                            $dchart[$key][] = (int)$percent;
                }
            }
            $save['cevg'] = $everage;
            $save['cin'] = $cashin;
            $save['cout'] = $cashout;
            $save['dchart'] = $dchart;

            // $cache->save($notif,$cacheID);
            $selectkurs = $this->_db->select()
                            ->from('M_KURS', array('*'))
                            ->where('DATE(KURS_DATE) = DATE(NOW())');
            $kurslist  =  $this->_db->fetchAll($selectkurs);
            $save['kurslist'] = $kurslist;

            $selectkurs = $this->_db->select()
                            ->from('M_KURS', array('*'))
                            ->where("KURS_CCY IN ('USD', 'EURO', 'SGD', 'AUD', 'YEN')")
                            ->order('KURS_DATE DESC')
                            ->limit('5');
            $kursdata  =  $this->_db->fetchAll($selectkurs);
            $save['kursdata'] = $kursdata;


            $report =  $this->_db->fetchAll(
                        $this->_db->select()->distinct()
                             ->from(array('A' => 'T_REPORT_GENERATOR'),array('A.REPORT_NAME','A.ID'))
                             ->where("A.REPORT_CUST = ".$this->_db->quote($this->_custIdLogin))
                             ->where("A.REPORT_CREATEDBY = ".$this->_db->quote($this->_userIdLogin))
                     );

        
        // echo $report;die;
            $output = '';
            $secret_key = 'cmdemo';
            $secret_iv = 'democm';
                // hash
            $key = hash('sha256', $secret_key);
            
            $iv = substr(hash('sha256', $secret_iv), 0, 16);
            $encrypt_method = "AES-256-CBC";
            if(!empty($report)){
            foreach ($report as $keys => $value) {
                
                $string = $value['ID'];
                $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
                $output = base64_encode($output);
                $report[$keys]['CODE'] = $output;
            }


            // $cacheID = 'REPORT'.$this->_custIdLogin.$this->_userIdLogin;
            
            
            $reportId = Application_Helper_Array::simpleArray($report, "ID");


            $selectcolomn = $this->_db->select()
                                ->from('T_REPORT_COLOMN', array('COLM_NAME','COLM_FIELD','COLM_TYPE','COLM_REPORD_ID'))
                                ->where('COLM_REPORD_ID IN (?)', $reportId)
                                ->order('COLM_ID ASC');
            $colomnArr  =  $this->_db->fetchAll($selectcolomn);
             // echo $selectcolomn;die;
            
            
            }
            $save['report'] = $report;
            $save['colomn'] = $colomnArr;


            

            // $privi = $this->_priviId;
            $privi = $this->_priviId;
            $userLogin  = $this->_custIdLogin;

            if (!empty($privi))
            {

                $changeModulePrivilegeObj  = new Changemanagement_Model_Privilege();
                $listAutModuleArr = $changeModulePrivilegeObj->getAuthorizeModule();
                
                $whPriviID = "'".implode("','", $listAutModuleArr)."'";

                $selectChanges = $this->_db->select()
                                ->from(array('G'=>'T_GLOBAL_CHANGES'),array('DISPLAY_TABLENAME'))
                                ->where('(CHANGES_STATUS = '.$this->_db->quote('WA').' OR CHANGES_STATUS = '.$this->_db->quote('RR').')')
                                ->where('G.CHANGES_FLAG = ?', 'F')
                                ->where('G.CUST_ID = ?', $userLogin)
                                ->where('G.MODULE IN ('.$whPriviID.')')
                                ->order('DISPLAY_TABLENAME ASC')
                                ->query()->fetchAll();
                
                $waitingAutorization = array();
                foreach($selectChanges as $row)
                {
                    if(isset($waitingAutorization[$row['DISPLAY_TABLENAME']]))
                    {
                        $waitingAutorization[$row['DISPLAY_TABLENAME']] += 1;
                    }
                    else
                    {
                        $waitingAutorization[$row['DISPLAY_TABLENAME']] = 1;
                    }
                }
                $save['waitingAutorization'] = $waitingAutorization;
                    
                
                
                
            }


            $paramPayment = array("WA"              => true,
                              "ACCOUNT_LIST"    => $this->_accountList,
                              "_beneLinkage"    => $this->view->hasPrivilege('BLBU'),
                             );

        $CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
        
        
        $selectuser = $this->_db->select()
                                ->from(array('A'            => 'M_APP_GROUP_USER'),
                                   array('GROUP_USER_ID'    => 'A.GROUP_USER_ID',
                                         'USER_ID' => 'A.USER_ID',
                                         'CUST_ID' => 'A.CUST_ID'
                                        )
                                   )
                            ->join(array('B' => 'M_APP_BOUNDARY_GROUP'), 'A.GROUP_USER_ID = B.GROUP_USER_ID', array())
                            ->join(array('C' => 'M_APP_BOUNDARY'), 'C.BOUNDARY_ID = B.BOUNDARY_ID', array('BOUNDARY_MIN'    => 'C.BOUNDARY_MIN',
                                         'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
                                         'CCY_BOUNDARY' => 'C.CCY_BOUNDARY'
                                        ))
        ->where("A.USER_ID  = ?" , (string)$this->_userIdLogin)
        ->where('A.CUST_ID = ?', (string)$this->_custIdLogin);
        
        $datauser = $this->_db->fetchAll($selectuser);
        
        
        $rangesql = '';
        foreach ($datauser as $key => $value){
            $rangesql .= " (P.PS_TOTAL_AMOUNT BETWEEN '".$value['BOUNDARY_MIN']."' AND '".$value['BOUNDARY_MAX']."') ";
            if(!empty($datauser[$key+1])){
                $rangesql .= " OR ";
            }
        }
        $rangesql .= " OR ";
        foreach ($datauser as $key => $value){
            $rangesql .= " (P.PS_TYPE = '14' AND P.PS_REMAIN BETWEEN '".$value['BOUNDARY_MIN']."' AND '".$value['BOUNDARY_MAX']."') ";
            if(!empty($datauser[$key+1])){
                $rangesql .= " OR ";
            }
        }
        $rangesql .= " OR ";
        foreach ($datauser as $key => $value){
            $rangesql .= " (P.PS_TYPE = '15' AND P.PS_REMAIN BETWEEN '".$value['BOUNDARY_MIN']."' AND '".$value['BOUNDARY_MAX']."') ";
            if(!empty($datauser[$key+1])){
                $rangesql .= " OR ";
            }
        }
        
        $select   = $CustUser->getPayment($paramPayment);
        
        $select->where('P.PS_STATUS = ?', (string)$this->_paymentstatus["code"]["waitingforapproval"]);

        if(!empty($rangesql))
        {
            if(!empty($datauser)){
                $select->where($rangesql);
            }
            
            $select->where("P.PS_NUMBER NOT IN ('P20160921NIG0UTYHC','P20160920PMQ6Q1CEC') ");
            
        }

        $dataSQL = $this->_db->fetchAll($select);

        $save['dataSQL'] = $dataSQL;


            $cache->save($save,$cacheID);   
        }else{
            $data = $cache->load($cacheID);
            
            
            $result = $data['templist'];
            if(empty($result)){
                $data = $this->_db->fetchAll(
                        $this->_db->select()->distinct()
                             ->from(array('A' => 'M_CUSTOMER_ACCT'))
                             ->join(array('C' => 'M_MAKERLIMIT'),'A.ACCT_NO = C.ACCT_NO',array())
                             ->join(array('D' => 'M_PRODUCT_TYPE'),'D.PRODUCT_CODE = A.ACCT_TYPE',array('D.PRODUCT_CODE','D.PRODUCT_NAME'))
                             ->joinLeft(array('B' => 'M_GROUPING'),'A.GROUP_ID = B.GROUP_ID',array('GROUP_NAME'))
                             ->where("A.ACCT_STATUS = 1")
                             ->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin))
                             ->where("C.USER_LOGIN = ".$this->_db->quote($this->_userIdLogin))
                             ->where("C.MAXLIMIT > 0")                   
                             ->order('B.GROUP_NAME DESC')
                             ->order('A.ORDER_NO ASC')
                             ->GROUP('C.ACCT_NO')
                );

            $tempList = array();
            $acct = array();
            foreach($data as $key => $row ){ 
                // $systemBalance = new SystemBalance($this->_custIdLogin,$row['ACCT_NO'],Application_Helper_General::getCurrNum($row['CCY_ID']));
                // $systemBalance->setFlag(false);
                // $systemBalance->checkBalance();
                $balance = $this->_db->fetchAll(
                        $this->_db->select()->distinct()
                             ->from(array('A' => 'T_BALANCE'))
                             ->where("A.ACCT_NO = ?",$row['ACCT_NO'])
                             ->order('DATE(A.PLAFOND_END) DESC')
                            );
                //var_dump($balance);
                
                $acct[$key]['ACCT_NO'] = $row['ACCT_NO'];
                $acct[$key]['acct_name'] = $row['ACCT_ALIAS_NAME'];
                $acct[$key]['acct_ccy'] = $row['CCY_ID'];
                if(!empty($balance)){
                    $balance = $balance['0']['PLAFOND'];
                }else{
                    //$balance = 0;
                    $balance = rand(10000,100000000);
                }
                $acct[$key]['balance'] = $row['CCY_ID']." ".Application_Helper_General::displayMoney($balance);
                array_push($tempList, $arr);
            }
            $result = $acct;
            }
            
            $everage = $data['cevg'];
            $cashin = $data['cin'];
            $cashout = $data['cout'];
            $dchart = $data['dchart'];
            //$suggest = array();
            if(empty($data['cevg']) && !empty($suggest)){
                $statement = $this->_db->select()
                    ->FROM(array('T' => 'T_STATEMENT'), array('T.STAT_TYPE','T.STAT_DATE', 'TOTAL' => 'SUM(T.STAT_AMOUNT)','MONTH' => "CONCAT(MONTHNAME(T.STAT_DATE),' ',YEAR(T.STAT_DATE))"))
                    ->WHERE("T.ACCT_NO IN(?)", $suggest)
                    // ->GROUP("T.STAT_TYPE,T.STAT_DATE")
                    ->GROUP("T.`STAT_TYPE`,YEAR(T.STAT_DATE), MONTH(T.STAT_DATE)")
                    ->order("T.STAT_DATE ASC")
                    ->QUERY()
                    ->FETCHALL();
                    $dataavg = $this->_db->select()->FROM(array('a' => $statementavg),array('AVG' =>'AVG(TOTAL)','STAT_DATE'))
                                ->GROUP('a.STAT_DATE')
                                ->order("a.STAT_DATE ASC")
                                ->QUERY()
                        ->FETCHALL();
                 $cashin = array();
                $cashout = array();
                $everage = array();
                $month = array();
             if(!empty($statement)){
             foreach ($statement as $key => $value) {
                if($value['STAT_TYPE'] == 'C'){
                    $cashin[] = (int)$value['TOTAL'];
                    $in = $value['TOTAL'];
                    $month[] = $value['MONTH'];
                }else if($value['STAT_TYPE'] == 'D'){
                    $cashout[] = (int)$value['TOTAL'];
                    $out = -$value['TOTAL'];
                }
             }

             }else{
                $cashin = array();
                $cashout = array();
                
                $month = array();
             }

             
             // var_dump($cashout);die;

             if(!empty($dataavg)){
             foreach ($dataavg as $key => $value) {
                    $everage[] = (int)$value['AVG'];
             }
            }else{
                $everage = array();
            }       
                        
            }
            
            

            //change by DW
            // $forcenotif = $data['forcenotif'];
            // $notif = $data['notif'];
            // $forcenotif = $data['fnotif'];

            $kurslist = $data['kurslist'];
            $kursdata = $data['kursdata'];
            $report = $data['report'];
            $colomnArr = $data['colomn'];
            $hidewidget = $data['widget'];

            $loandata = $data['loandata'];
            $depodata = $data['depodata'];
            $percentasset = $data['percentasset'];
            $percentloan = $data['percentloan'];
            $percentdepo = $data['percentdepo'];

            $waitingAutorization = $data['waitingAutorization'];
            $dataSQL = $data['dataSQL'];
        }
        //$result = $acct;

        if(is_nan($percentasset)){
            $percentasset = 0;
        }

        if(is_nan($percentdepo)){
            $percentdepo = 0;
        }

        if(is_nan($percentloan)){
            $percentloan = 0;
        }
        // var_dump($loandata);
        $loandata = array(array(1=>6,0=>'Loan')); 
        $depodata = array(array(1=>11,0=>'Deposit')); 
        //demo
        $percentasset = 83;
        $percentdepo = 11;
        $percentloan = 6;
        
        
        $this->view->loandata = $loandata;
        $this->view->depodata = $depodata;
        $this->view->percentasset = $percentasset;
        $this->view->percentdepo = $percentdepo;
        $this->view->percentloan = $percentloan;
        // echo "<pre>";
        //  var_dump($loandata);
        //   var_dump($depodata);
        //   var_dump($dchart);

        if (!empty($privi))
            {
                $this->view->waitingAutorization = $waitingAutorization;
            }
        
        // $cache = $this->serviceLocator->get('BALFIRSTMED1AHAMDAN01');
        // $iterator = $cache->getIterator();
        // foreach($iterator as $key){
        //    echo $key;
        // }



        $this->view->accountList = $result;

        // print_r($result);
        



        $select = $this->_db->select()
                                ->from( 'M_NOTIFICATION',
                                        array('*'));
        $select->where("DATE (EFDATE) <= DATE(NOW())");
        #$select->where("TIME (EFDATE) <= TIME(NOW())");
        $select->where("DATE (EXP_DATE) >= DATE(NOW())");
        $select->where("TARGET IN ('1','3')");

        $notif = $this->_db->fetchAll($select);

        if(!empty($notif)){
            foreach ($notif as $key => $value) {
                $not[] = $value['ID'];
            }
        }
         
        $selectforce = $this->_db->select()
                            ->from( 'M_FORCENOTIF',
                                    array('NOTIF_ID'));
        $selectforce->where("USER_ID = ? ",$this->_userIdLogin);
        if(!empty($not)){
            $selectforce->where("NOTIF_ID IN (?)",$not);    
        }
        // echo $selectforce;die;
        $forcenotif = $this->_db->fetchAll($selectforce);

        if(!empty($notif)){
            $this->view->forcenotif = $forcenotif;
            $this->view->notif = $notif;    
        }
        // echo "<pre>";
        //   var_dump($dchart);die;
        $dchart = array(array(1=>83,0=>'CASA')); 
         $this->view->dchartdata = $dchart;
         $this->view->month = $month;
         $this->view->cashin = $cashin;
         $this->view->cashout = $cashout;
         $this->view->everage = $everage;
        $userId = $this->_userIdLogin;
        $custId = $this->_custIdLogin;
        $this->view->kurslist =$kursdata;
        $this->view->reportlist = $report;
        $this->view->colomnlist = $colomnArr;


        $selectwidget = $this->_db->select()
                            ->from('M_WIDGET', array('*'))
                            ->where('CUST_ID = ?', $this->_custIdLogin)
                            ->where('USER_ID = ?', $this->_userIdLogin);
                            
            $hidewidget  =  $this->_db->fetchAll($selectwidget);
            // $save['widget'] = $hidewidget;
        
        if(!empty($hidewidget)){
            foreach ($hidewidget as $key => $value) {
                $str = 'widget'.$value['WIDGET_ID'];
                $this->view->$str = 'hide';
            }
        }



        $this->view->waitingApproval = $dataSQL;


    }



     public function columnAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'cmdemo';
        $secret_iv = 'democm';
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        $string = $this->_getParam('report');


         $report = openssl_decrypt(base64_decode($tblName), $encrypt_method, $key, 0, $iv);
       
        $deleteWhere['ID = ?'] = $report;
        $deleteWherecolm['COLM_REPORD_ID = ?'] = $report;
        
        $this->_db->delete('T_REPORT_GENERATOR',$deleteWhere);
        $this->_db->delete('T_REPORT_COLOMN',$deleteWherecolm);
       

        echo $optHtml;
    }


    public function hidedashAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');
        $param = array(
                'USER_ID' => $this->_userIdLogin,
                'CUST_ID' => $this->_custIdLogin,
                'WIDGET_ID' => $tblName
            );
                        $this->_db->insert('M_WIDGET',$param);

        echo $optHtml;
    }


        public function savenotifAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');
        $param = array(
                'USER_ID' => $this->_userIdLogin,
                'NOTIF_ID' => $tblName,
                'CREATED' => new Zend_Db_Expr("now()")
            );
        $this->_db->insert('M_FORCENOTIF',$param);

        // echo $optHtml;
    }




    public function inquiryAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $id = $this->_getParam('id');
        $ccy = $this->_getParam('data');

        $systemBalance = new SystemBalance($this->_custIdLogin,$id,Application_Helper_General::getCurrNum($ccy));
                $systemBalance->setFlag(false);
                $systemBalance->checkBalance();
        $optHtml = $systemBalance->getEffectiveBalance();
         // $optHtml = '200000';

        echo $optHtml;
    }


    


    public function changethemeAction(){

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $color = $this->_getParam('color');

        $updateArr = array(
            'USER_THEME' => $color                            
        );
        $where['USER_ID = ?'] = $this->_userIdLogin;
        $query = $this->_db->update('M_USER',$updateArr,$where);

        if ($query) {

            //if success save to auth
            $auth = Zend_Auth::getInstance();
            $data = $auth->getStorage()->read();

            $data->_userTheme = $color;
            $auth->getStorage()->write($data);

            echo true;

        }
        else{
            echo false;
        }
    }




    public function totalbudgetrealizationAction(){
        
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $totalAnggaran2021 = (125917822000/1000000000);
        $realisasiSP2D2021 = (90177172000/1000000000);
        $sisaAnggaran2021 = (35740650000/1000000000);

        $totalAnggaran2020 = (106172192100/1000000000);
        $realisasiSP2D2020 = (105983172000/1000000000);
        $sisaAnggaran2020 = (189020100/1000000000);

        $totalAnggaran2019 = (99816521100/1000000000);
        $realisasiSP2D2019 = (65001862600/1000000000);
        $sisaAnggaran2019 = (34814658500/1000000000);

        // get pie SP2D
        $totalNilaiKontrak2021 = (90177172000/1000000000);
        $totalNilaiDibayar2021 = (73118571900/1000000000);
        $menungguTinjauan2021 = (10208671000/1000000000);
        $menungguPersetujuan2021 = (3781281900/1000000000);
        $menungguRilis2021 = (3068647200/1000000000);

        $totalNilaiKontrak2020 = (105983172000/1000000000);
        $totalNilaiDibayar2020 = (105983172000/1000000000);
        $menungguTinjauan2020 = 0;
        $menungguPersetujuan2020 = 0;
        $menungguRilis2020 = 0;

        $totalNilaiKontrak2019 = (65001862600/1000000000);
        $totalNilaiDibayar2019 = (65001862600/1000000000);
        $menungguTinjauan2019 = 0;
        $menungguPersetujuan2019 = 0;
        $menungguRilis2019 = 0;

        

            
        
        // $coba = (array(
        //     'abc' => "1",
        //     'abcd' => "2"
            
        // ));


        $coba = (array(
            'totalAnggaran2021' => $totalAnggaran2021,
            'realisasiSP2D2021' => $realisasiSP2D2021,
            'sisaAnggaran2021' => $sisaAnggaran2021,
            'totalAnggaran2020' => $totalAnggaran2020,
            'realisasiSP2D2020' => $realisasiSP2D2020,
            'sisaAnggaran2020' => $sisaAnggaran2020,
            'totalAnggaran2019' => $totalAnggaran2019,
            'realisasiSP2D2019' => $realisasiSP2D2019,
            'sisaAnggaran2019' => $sisaAnggaran2019,
            'totalNilaiKontrak2021' => $totalNilaiKontrak2021,
            'totalNilaiDibayar2021'=> $totalNilaiDibayar2021,
            'menungguTinjauan2021' => $menungguTinjauan2021,
            'menungguPersetujuan2021' => $menungguPersetujuan2021,
            'menungguRilis2021' => $menungguRilis2021,
    
            'totalNilaiKontrak2020' => $totalNilaiKontrak2020,
            'totalNilaiDibayar2020' => $totalNilaiDibayar2020,
            'menungguTinjauan2020' => $menungguTinjauan2020,
            'menungguPersetujuan2020' => $menungguPersetujuan2020,
            'menungguRilis2020' => $menungguRilis2020,
    
            'totalNilaiKontrak2019' => $totalNilaiKontrak2019,
            'totalNilaiDibayar2019' => $totalNilaiDibayar2019,
            'menungguTinjauan2019' => $menungguTinjauan2019,
            'menungguPersetujuan2019' => $menungguPersetujuan2019,
            'menungguRilis2019' => $menungguRilis2019
    
            
        ));

        echo $this->_helper->json($coba);exit;
    

        

       
    }

    public function totalbudgetAction(){

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $paramTahun = $this->_getParam('tahun');

        if($paramTahun == '2021' ){
            
			$RealisasiSP2D = (90177172000/1000000000);
			$arraychart[0]['y'] = $RealisasiSP2D;
			$arraychart[0]['name'] = 'Realisasi SP2D';
            $arraychart[0]['color'] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
			$arraychart[0]['nominal'] = Application_Helper_General::displayMoneyplain($RealisasiSP2D);
            
            $SisaAnggaran = (35740650000/1000000000);
            $arraychart[1]['y'] = $SisaAnggaran;
			$arraychart[1]['name'] = 'Sisa Anggaran';
			$arraychart[1]['nominal'] = Application_Helper_General::displayMoneyplain($SisaAnggaran);
            
			
		}

        if($paramTahun == '2020' ){
			
			$arraychart[0]['y'] = (105983172000/1000000000);
			$arraychart[0]['name'] = 'Realisasi SP2D';
            $arraychart[0]['color'] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
			$arraychart[0]['nominal'] = Application_Helper_General::displayMoneyplain(105983172000/1000000000);

            $arraychart[1]['y'] = (189020100/1000000000);
			$arraychart[1]['name'] = 'Sisa Anggaran';
			$arraychart[1]['nominal'] = Application_Helper_General::displayMoneyplain(189020100/1000000000);
            
			
		}

        if($paramTahun == '2019' ){
            
			$arraychart[0]['y'] = (65001862600/1000000000);
			$arraychart[0]['name'] = 'Realisasi SP2D';
            $arraychart[0]['color'] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
			$arraychart[0]['nominal'] = Application_Helper_General::displayMoneyplain(65001862600/1000000000);

            $arraychart[1]['y'] = (34814658500/1000000000);
			$arraychart[1]['name'] = 'Sisa Anggaran';
            $arraychart[0]['color'] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
			$arraychart[1]['nominal'] = Application_Helper_General::displayMoneyplain(34814658500/1000000000);
            
			
		}
					
        echo json_encode(array_values($arraychart));


    
    }

    function totalsp2dAction(){
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        
		$paramTahun = $this->_getParam('tahun');
		
		
		if($paramTahun == '2021' ){
            
            $this->view->totalNilaiKontrak2021 = (90177172000/1000000000);
            $this->view->totalNilaiDibayar2021 = (73118571900/1000000000);
            $this->view->menungguTinjauan2021 = (10208671000/1000000000);
            $this->view->menungguPersetujuan2021 = (3781281900/1000000000);
            $this->view->menungguRilis2021 = (3068647200/1000000000);

            $this->view->totalNilaiKontrak2020 = (105983172000/1000000000);
            $this->view->totalNilaiDibayar2020 = (105983172000/1000000000);
            $this->view->menungguTinjauan2020 = 0;
            $this->view->menungguPersetujuan2020 = 0;
            $this->view->menungguRilis2020 = 0;

            $this->view->totalNilaiKontrak2019 = (65001862600/1000000000);
            $this->view->totalNilaiDibayar2019 = (65001862600/1000000000);
            $this->view->menungguTinjauan2019 = 0;
            $this->view->menungguPersetujuan2019 = 0;
            $this->view->menungguRilis2019 = 0;


			$totalNilaiDibayar = (73118571900/1000000000);
			$arraychart[0]['y'] = $totalNilaiDibayar;
			$arraychart[0]['name'] = 'Total Nilai Dibayar';
            $arraychart[0]['color'] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
			$arraychart[0]['nominal'] = Application_Helper_General::displayMoneyplain($totalNilaiDibayar);
            
            $menungguTinjauan = (10208671000/1000000000);
            $arraychart[1]['y'] = $menungguTinjauan;
			$arraychart[1]['name'] = 'Menunggu Tinjauan';
			$arraychart[1]['nominal'] = Application_Helper_General::displayMoneyplain($menungguTinjauan);

            $menungguRilis = (3068647200/1000000000);
            $arraychart[2]['y'] = $menungguRilis;
			$arraychart[2]['name'] = 'Total Menunggu Persetujuan';
			$arraychart[2]['nominal'] = Application_Helper_General::displayMoneyplain($menungguRilis);

            $menungguPersetujuan = (3781281900/1000000000);
            $arraychart[3]['y'] = $menungguPersetujuan;
			$arraychart[3]['name'] = 'Menunggu Rilis';
			$arraychart[3]['nominal'] = Application_Helper_General::displayMoneyplain($menungguPersetujuan);
            
			
		}

        if($paramTahun == '2020' ){
			
			$totalNilaiDibayar = (105983172000/1000000000);
			$arraychart[0]['y'] = $totalNilaiDibayar;
			$arraychart[0]['name'] = 'Total Nilai Dibayar';
            $arraychart[0]['color'] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
			$arraychart[0]['nominal'] = Application_Helper_General::displayMoneyplain($totalNilaiDibayar);
            
            $menungguTinjauan = 0;
            $arraychart[1]['y'] = $menungguTinjauan;
			$arraychart[1]['name'] = 'Menunggu Tinjauan';
			$arraychart[1]['nominal'] = Application_Helper_General::displayMoneyplain($menungguTinjauan);

            $menungguRilis = 0;
            $arraychart[2]['y'] = $menungguRilis;
			$arraychart[2]['name'] = 'Total Menunggu Persetujuan';
			$arraychart[2]['nominal'] = Application_Helper_General::displayMoneyplain($menungguRilis);

            $menungguPersetujuan =0;
            $arraychart[3]['y'] = $menungguPersetujuan;
			$arraychart[3]['name'] = 'Menunggu Rilis';
			$arraychart[3]['nominal'] = Application_Helper_General::displayMoneyplain($menungguPersetujuan);
            
			
		}

        if($paramTahun == '2019' ){
            
			$totalNilaiDibayar = (65001862600/1000000000);
			$arraychart[0]['y'] = $totalNilaiDibayar;
			$arraychart[0]['name'] = 'Total Nilai Dibayar';
            $arraychart[0]['color'] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
			$arraychart[0]['nominal'] = Application_Helper_General::displayMoneyplain($totalNilaiDibayar);
            
            $menungguTinjauan = 0;
            $arraychart[1]['y'] = $menungguTinjauan;
			$arraychart[1]['name'] = 'Menunggu Tinjauan';
			$arraychart[1]['nominal'] = Application_Helper_General::displayMoneyplain($menungguTinjauan);

            $menungguRilis = 0;
            $arraychart[2]['y'] = $menungguRilis;
			$arraychart[2]['name'] = 'Total Menunggu Persetujuan';
			$arraychart[2]['nominal'] = Application_Helper_General::displayMoneyplain($menungguRilis);

            $menungguPersetujuan = 0;
            $arraychart[3]['y'] = $menungguPersetujuan;
			$arraychart[3]['name'] = 'Menunggu Rilis';
			$arraychart[3]['nominal'] = Application_Helper_General::displayMoneyplain($menungguPersetujuan);
            
			
		}
					
        echo json_encode(array_values($arraychart));

	}


    public function accountsAction(){

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $loandata = array(array(1=>6,0=>'Loan')); 
        $depodata = array(array(1=>11,0=>'Deposit')); 
        //demo
        $percentasset = 83;
        $percentdepo = 11;
        $percentloan = 6;


        $arraychart[0]['y'] = $percentasset;
		$arraychart[0]['name'] = 'CASA';
        $arraychart[0]['color'] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));

        $arraychart[1]['y'] = $percentdepo;
		$arraychart[1]['name'] = 'Loan';
        $arraychart[1]['color'] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));

        $arraychart[2]['y'] = $percentloan;
		$arraychart[2]['name'] = 'Deposito';
        $arraychart[2]['color'] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));

        echo json_encode(array_values($arraychart));
        




    
    }

    public function reportsAction(){

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        //$custId = $this->_custIdLogin;

        $report =  $this->_db->fetchAll(
            $this->_db->select()->distinct()
                 ->from(array('A' => 'T_REPORT_GENERATOR'),array('A.REPORT_NAME','A.ID'))
                 ->where("A.REPORT_CUST = ".$this->_db->quote($this->_custIdLogin))
                 ->where("A.REPORT_CREATEDBY = ".$this->_db->quote($this->_userIdLogin))
                //  ->where("A.REPORT_CUST = ? ",$this->_custIdLogin)
			    // ->where("A.REPORT_CREATEDBY = ? ",$this->_userIdLogin)
         );


         $output = '';
            $secret_key = 'cmdemo';
            $secret_iv = 'democm';
                // hash
            $key = hash('sha256', $secret_key);
            
            $iv = substr(hash('sha256', $secret_iv), 0, 16);
            $encrypt_method = "AES-256-CBC";
           

         echo json_encode(array_values($report));

    
    }

    public function approvalAction(){

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $paramPayment = array("WA"              => true,
                              "ACCOUNT_LIST"    => $this->_accountList,
                              "_beneLinkage"    => $this->view->hasPrivilege('BLBU'),
                             );

        $CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

        $select   = $CustUser->getPayment($paramPayment);

    
    }

    public function bgwarrantyAction(){

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $cover = 40;
        $facility = 27;
        $insurance = 9;
        


        $arraychart[0]['y'] = $cover;
		$arraychart[0]['name'] = 'Total BG Full Cover';
        $arraychart[0]['color'] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));

        $arraychart[1]['y'] = $facility;
		$arraychart[1]['name'] = 'Total BG Line Facility';
        $arraychart[1]['color'] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));

        $arraychart[2]['y'] = $insurance;
		$arraychart[2]['name'] = 'Total BG Insurance';
        $arraychart[2]['color'] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));

       

        echo json_encode(array_values($arraychart));

    
    }

    public function exchangeAction(){

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $selectkurs = $this->_db->select()
                            ->from('M_KURS', array('*'))
                            ->where("KURS_CCY IN ('USD', 'EURO', 'SGD', 'AUD', 'YEN')")
                            ->order('KURS_DATE DESC')
                            ->limit('5');
            $kursdata  =  $this->_db->fetchAll($selectkurs);

            echo json_encode(array_values($kursdata));

        

    
    }

    public function bguaranteeAction(){

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        // $select2 = $this->_db->select()
        //                   ->from(array('BG' => 'V_PIEBGINFORMATION'),array('*'))
        //                   ->where('BG.CUST_ID ='.$this->_db->quote((string)$this->_custIdLogin));
        // $arr = $this->_db->fetchAll($select2); 
        
        // $bgguar = array();

        // foreach ($arr as $key => $value)
        // {
        //     $bgguar['amountReview'] = $value['BG_AMOUNT_REVIEW'];
        //     $bgguar['amountRelease'] = $value['BG_AMOUNT_RELEASE'];
        //     $bgguar['amountApprove'] = $value['BG_AMOUNT_APPROVE'];
        //     $bgguar['amountIssued'] = $value['BG_AMOUNT_ISSUED'];
        //     $bgguar['dataReview'] = $value['COUNT_DATA_REVIEW'];
        //     $bgguar['dataRelease'] = $value['COUNT_DATA_RELEASE'];
        //     $bgguar['dataApprove'] = $value['COUNT_DATA_APPROVE'];
        //     $bgguar['dataIssued'] = $value['COUNT_DATA_ISSUED'];
            
        // }

        $review = 76;
        $release = 23;
        $approval = 8;
        $issued = 3;


        $arraychart[0]['y'] = $review;
		$arraychart[0]['name'] = 'Waiting for Review';
        $arraychart[0]['color'] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));

        $arraychart[1]['y'] = $release;
		$arraychart[1]['name'] = 'Waiting for Release';
        $arraychart[1]['color'] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));

        $arraychart[2]['y'] = $approval;
		$arraychart[2]['name'] = 'Waiting for Approval';
        $arraychart[2]['color'] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));

        $arraychart[3]['y'] = $issued;
		$arraychart[3]['name'] = 'Issued';
        $arraychart[3]['color'] = sprintf('#%06X', mt_rand(0, 0xFFFFFF));

        echo json_encode(array_values($arraychart));

    
    }




}



