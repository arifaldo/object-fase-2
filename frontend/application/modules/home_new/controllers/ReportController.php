<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Validate/Validate.php';

class home_ReportController extends Application_Main {


	
	public function indexAction()
    { 
    	$this->_helper->_layout->setLayout('newlayout');

    	$select = $this->_db->select()
    						->from('information_schema.tables', array('table_name'))
    						->where('table_schema = ?', 'bank_mayapada_cm_demo');

    	$tableList = $this->_db->fetchAll($select);

    	$this->view->tableList = $tableList;

        if($this->_request->isPost()){
            $filters    = array('tablename' => array('StringTrim','StripTags','HtmlEntities'),
                                'tablecols' => array('StringTrim','StripTags','HtmlEntities'),
                                'sortasc' => array('StringTrim','StripTags','HtmlEntities'),
                                'sortdesc' => array('StringTrim','StripTags','HtmlEntities'),
                                'datalimit' => array('StringTrim','StripTags','HtmlEntities'),
                                'wherecol' => array('StringTrim','StripTags','HtmlEntities'),
                                'whereopt' => array('StringTrim','StripTags','HtmlEntities'),
                                'whereval' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_name' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_email' => array('StringTrim','StripTags','HtmlEntities'),
                                'report_schedule' => array('StringTrim','StripTags','HtmlEntities')
            );

            $validators =  array('tablename'     => array(),
                                'tablecols'      => array(),
                                'sortasc'        => array(),
                                'sortdesc'       => array(),
                                'wherecol'       => array(),
                                'whereopt'       => array(),
                                'whereval'          => array('allowEmpty'=>true,
                                                            new Zend_Validate_Regex(array('pattern' => '/^[0-9A-Za-z\\s-_.]+$/')),
                                                            'messages' => array('Invalid report condition')
                                                    ),
                                'datalimit'         => array('allowEmpty' => true,
                                                            'Digits',
                                                            'messages' => array('Invalid data limit format')
                                                    ),
                                'report_name'       => array('NotEmpty',
                                                            array('StringLength',array('max'=>200)),
                                                            'messages' => array('Can not be empty',
                                                                            'Report name length cannot be more than 200',
                                                                        )
                                                    ),
                                'report_email'      => array('allowEmpty'=>true,
                                                            array('StringLength',array('max'=>128)),
                                                            'messages' => array(//'Can not be empty',
                                                                            'Email length cannot be more than 128',
                                                                        )
                                                    ),
                                'report_schedule'   => array('allowEmpty'=>true,
                                                            'Alpha',
                                                            'messages' => array(//'Can not be empty',
                                                                                'Invalid schedule'
                                                                        )
                                                    ),
            );

            $zf_filter = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

            $cek_multiple_email = true;

            if($zf_filter->report_email)
            {
                $validate = new Validate();
                $cek_multiple_email = $validate->isValidEmailMultiple($zf_filter->report_email);
            }

            if($zf_filter->isValid() && $cek_multiple_email == true)
            {
                
                $selectedCols = implode(",", $zf_filter->tablecols);
                $sortAsc = implode(",", $zf_filter->sortasc);
                $sortDesc = implode(",", $zf_filter->sortdesc);

                $whereCols = $zf_filter->wherecol;
                $whereOpts = $zf_filter->whereopt;
                $whereVals = $zf_filter->whereval;

                $optsArr = array("EQUAL" => "=",
                                "NOT EQUAL" => "<>",
                                "LESS THAN" => "<",
                                "GREATER THAN" => ">",
                                "LESS THAN OR EQUAL TO" => "<=",
                                "GREATER THAN OR EQUAL TO" => ">="
                );

                if(!empty($whereCols)){
                    $tempWhere = array();

                    foreach($whereCols as $key => $val){
                        $tempName = explode("-", $val);
                        $colName = $tempName[0];

                        if($whereOpts[$key] != "LIKE"){
                            $opt = $optsArr[$whereOpts[$key]];
                            $whereval = $whereVals[$key];
                        }
                        else{
                            $opt = $whereOpts[$key];
                            $whereval = "%".$wherevals[$key]."%";
                        }

                        $tempWhere[] = $colName." ".$opt." ".$whereval;  
                    }

                    $wheres = implode(";",$tempWhere);
                }

                if(empty($zf_filter->report_email)){
                    $schedule = null;
                }
                else{
                    if(empty($zf_filter->report_schedule))
                        $schedule = "weekly";
                    else
                        $schedule = strtolower($zf_filter->report_schedule);
                }

                if(empty($zf_filter->datalimit))
                    $limit = 0;
                else
                    $limit = $zf_filter->datalimit;
                
                $insArr = array("REPORT_NAME" => $zf_filter->report_name,
                                "REPORT_TABLE" => $zf_filter->tablename,
                                "REPORT_COLUMNS" => $selectedCols,
                                "REPORT_WHERE" => $wheres,
                                "REPORT_SORT_ASC" => $sortAsc,
                                "REPORT_SORT_DESC" => $sortDesc,
                                "REPORT_LIMIT" => $limit,
                                "REPORT_EMAIL" => $zf_filter->report_email,
                                "REPORT_SCHEDULE" => $schedule,
                                "REPORT_CUST" => $this->_custIdLogin,
                                "REPORT_CREATED" => new Zend_Db_Expr("GETDATE()"),
                                "REPORT_CREATEDBY" => $this->_userIdLogin
                );

                try{
                    $this->_db->beginTransaction();

                    $this->_db->insert('T_REPORT_GENERATOR',$insArr);

                    Application_Helper_General::writeLog('ADRG','New report has been added, Report Name : '.$zf_filter->report_name. ' Creator : '.$this->_custIdLogin." | ".$this->_userIdLogin);

                    $this->_db->commit();
                    $this->setbackURL('/home/report');
                    $this->_redirect('/notification/success/index');
                }
                catch(Exception $e){
                    $this->_db->rollBack();
                    $error_remark = $this->language->_('An Error Occured. Please Try Again');
                }

                if(isset($error_remark))
                {
                    Application_Helper_General::writeLog('ADRG','Add Report');
                    $this->_helper->getHelper('FlashMessenger')->addMessage('F');
                    $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
                    $this->_redirect('home/report');
                }
            }
            else{
                $this->view->tablename = ($zf_filter->isValid('tablename'))? $zf_filter->tablename : $this->_getParam('tablename');
                $this->view->tablecols = ($zf_filter->isValid('tablecols'))? $zf_filter->tablecols : $this->_getParam('tablecols');
                $this->view->sortasc = ($zf_filter->isValid('sortasc'))? $zf_filter->sortasc : $this->_getParam('sortasc');
                $this->view->sortdesc = ($zf_filter->isValid('sortdesc'))? $zf_filter->sortdesc : $this->_getParam('sortdesc');
                $this->view->datalimit = ($zf_filter->isValid('datalimit'))? $zf_filter->datalimit : $this->_getParam('datalimit');
                $this->view->wherecol = ($zf_filter->isValid('wherecol'))? $zf_filter->wherecol : $this->_getParam('wherecol');
                $this->view->whereopt = ($zf_filter->isValid('whereopt'))? $zf_filter->whereopt : $this->_getParam('whereopt'); 
                $this->view->whereval = ($zf_filter->isValid('whereval'))? $zf_filter->whereval : $this->_getParam('whereval');
                $this->view->report_name = ($zf_filter->isValid('report_name'))? $zf_filter->report_name : $this->_getParam('report_name');
                $this->view->report_email = ($zf_filter->isValid('report_email'))? $zf_filter->report_email : $this->_getParam('report_email');
                $this->view->report_schedule = ($zf_filter->isValid('report_schedule'))? $zf_filter->report_schedule : $this->_getParam('report_schedule');

                $error = $zf_filter->getMessages();
         
                $errorArray = null;
                foreach($error as $keyRoot => $rowError)
                {
                   foreach($rowError as $errorString)
                   {
                      $errorArray[$keyRoot] = $errorString;
                   }
                }
        
                if(isSet($cek_multiple_email) && $cek_multiple_email == false) $errorArray['report_email'] = 'Invalid email format';
        
                $this->view->error_msg = $errorArray;

                $tblName = ($zf_filter->isValid('tablename'))? $zf_filter->tablename : $this->_getParam('tablename');
                $select = $this->_db->select()
                                    ->from('information_schema.columns', array('DATA_TYPE','COLUMN_NAME'))
                                    ->where('table_schema = ?', 'bank_mayapada_cm_demo')
                                    ->where('table_name = ?', $tblName);
                
                $tempColumn = $this->_db->fetchAll($select);

                $this->view->columnList = $tempColumn;

                $tblCols = ($zf_filter->isValid('tablecols'))? $zf_filter->tablecols : $this->_getParam('tablecols');
                $tblAsc = ($zf_filter->isValid('sortasc'))? $zf_filter->sortasc : $this->_getParam('sortasc');
                $tblDesc = ($zf_filter->isValid('sortdesc'))? $zf_filter->sortdesc : $this->_getParam('sortdesc');

                $leftCols = array();
                $leftAsc = array();
                $leftDesc = array();
                foreach($tempColumn as $row){
                    if(!in_array($row['COLUMN_NAME'], $tblCols))
                        $leftCols[] = $row['COLUMN_NAME'];

                    if(!in_array($row['COLUMN_NAME'], $tblAsc))
                        $leftAsc[] = $row['COLUMN_NAME'];

                    if(!in_array($row['COLUMN_NAME'], $tblDesc))
                        $leftDesc[] = $row['COLUMN_NAME'];
                }

                $this->view->leftcols = $leftCols;
                $this->view->leftasc = $leftAsc;
                $this->view->leftdesc = $leftDesc;

                $whereCols = ($zf_filter->isValid('wherecol'))? $zf_filter->wherecol : $this->_getParam('wherecol');
                if(!empty($whereCols)){
                    $wherectr = count($whereCols)+1;
                }
                else{
                    $wherectr = 1;
                }

                $this->view->wherectr = $wherectr;
            }
        }
        else{
            $this->view->wherectr = 1;
        }
    }


    public function columnAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $select = $this->_db->select()
                            ->from('information_schema.columns', array('COLUMN_NAME'))
                            ->where('table_schema = ?', 'bank_mayapada_cm_demo')
                            ->where('table_name = ?', $tblName);
                            // echo $select;die;
        $data = $this->_db->fetchAll($select);
        foreach($data as $row){
            $optHtml.="<option value='".$row['COLUMN_NAME']."'>".$row['COLUMN_NAME']."</option>";
        }

        echo $optHtml;
    }

    public function wherecolumnAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

        $select = $this->_db->select()
                            ->from('information_schema.columns', array('DATA_TYPE','COLUMN_NAME'))
                            ->where('table_schema = ?', 'bank_mayapada_cm_demo')
                            ->where('table_name = ?', $tblName);

        $data = $this->_db->fetchAll($select);
        $optHtml = "<option value=''>-- ".$this->language->_('Any Value')." --</option>";
        foreach($data as $row){
            $optHtml.="<option value='".$row['COLUMN_NAME']."-".$row['DATA_TYPE']."'>".$row['COLUMN_NAME']."</option>";
        }

        echo $optHtml;
    }
}
