<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'SGO/Extendedmodule/Google/GoogleAuthenticator.php';
require_once 'SGO/Extendedmodule/phpqrcode/qrlib.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'SGO/Helper/Encryption.php';

class bgclosingworkflow_releasedetailController extends Application_Main
{

	protected $_moduleDB = 'RTF'; // masih harus diganti

	public function indexAction()
	{
		$this->_helper->_layout->setLayout('newlayout');
		$settings = new Settings();
		$conf = Zend_Registry::get('config');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$AESMYSQL = new Crypt_AESMYSQL();

		$system_type = $settings->getSetting('system_type');
		$stamp_fee = $settings->getSetting('stamp_fee');
		$adm_fee = $settings->getSetting('adm_fee');
		$claim_period = $settings->getSetting('max_claim_period');
		$getOtpTrx = $this->_db->select()->from("M_SETTING")->where("SETTING_ID = ?", "otp_exp_trx")->query()->fetchAll();
		$bankName = $conf['app']['bankname'];

		$allSetting = $settings->getAllSetting();
		$this->view->snkPenutupan = $allSetting['ftemplate_bgclosing_ind'];
		$this->view->snkPelepasanMd = $allSetting['ftemplate_bgclosingmd_ind'];
		$this->view->snkPenerbitanInd = $settings->getSetting('ftemplate_bg_ind');
		$this->view->snkPenerbitanIng = $settings->getSetting('ftemplate_bg_eng');

		$counterWarrantyType = $conf["bgcg"]["type"]["desc"];
		$counterWarrantyCode = $conf["bgcg"]["type"]["code"];
		$counterWarranty = array_combine(array_values($counterWarrantyCode), array_values($counterWarrantyType));

		$bgStatusType = $conf["bg"]["status"]['desc'];
		$bgStatusCode = $conf["bg"]["status"]["code"];
		$bgStatus = array_combine(array_values($bgStatusCode), array_values($bgStatusType));

		$ctdesc = $conf["bgclosing"]["changetype"]["desc"];
		$ctcode = $conf["bgclosing"]["changetype"]["code"];
		$suggestion_type = array_combine(array_values($ctcode), array_values($ctdesc));

		$bgclosinghistoryDesc   = $conf["bgclosinghistory"]["status"]["desc"];
		$bgclosinghistoryCode   = $conf["bgclosinghistory"]["status"]["code"];
		$arrbgclosinghistory 	= array_combine(array_values($bgclosinghistoryCode), array_values($bgclosinghistoryDesc));
		$this->view->arrbgclosinghistory = $arrbgclosinghistory;

		$usergoogleAuth = $this->_db->select()
			->from(["MU" => "M_USER"], ["MU.GOOGLE_CODE", "MU.USER_FAILEDTOKEN"])
			->where("CUST_ID = ?", $this->_custIdLogin)
			->where("USER_ID = ?", $this->_userIdLogin)
			->where("LTRIM(RTRIM(GOOGLE_CODE)) <> ?", "")
			->query()->fetchAll();

		if (!empty($usergoogleAuth)) {
			$maxtoken = $settings->getSetting("max_failed_token");
			$this->view->googleauth = true;
			$this->view->tokenfail = (int)$maxtoken;
			if ($usergoogleAuth['0']['USER_FAILEDTOKEN'] != '0') {
				$tokenfail = (int)$maxtoken - (int)($usergoogleAuth['0']['USER_FAILEDTOKEN'] + 1);
				$this->view->failedtoken = $usergoogleAuth['0']['USER_FAILEDTOKEN'];
				$this->view->tokenfail = $tokenfail;
			}
			// untuk di tanyakan
			if ($release) {
				$step = $this->_getParam('step');
			} else {
				$step = 3;
			}
			// untuk di tanyakan
		} else {
			// untuk di tanyakan
			$this->view->nogoauth = '1';
			if ($release) {
				$step = $this->_getParam('step');
			} else {
				$step = 3;
			}
			// untuk di tanyakan
		}

		$selectcomp = $this->_db->select()
			->from(array('A' => 'M_CUSTOMER'), array('*'))
			->joinRight(array('B' => 'M_CITYLIST'), 'A.CUST_CITY = B.CITY_CODE', array('B.CITY_NAME'))
			->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
			->query()->fetchAll();

		$get_cash_collateral = $this->_db->select()
			->from("M_CHARGES_OTHER", ["CHARGES_PCT", "CHARGES_ADM", "CHARGES_STAMP"])
			->where("CUST_ID = ?", "GLOBAL")
			->where("CHARGES_TYPE = ?", "10")
			->query()->fetchAll();

		$numb = $this->_getParam('bgnumb');

		$this->view->systemType = $system_type;
		$this->view->stamp_fee = $stamp_fee;
		$this->view->adm_fee = $adm_fee;
		$this->view->BG_CLAIM_PERIOD = $claim_period;
		$this->view->timeExpOtp = $getOtpTrx[0]["SETTING_VALUE"];
		$this->view->ProvFee = 2000000;
		$this->view->masterbankname = $bankName;
		$this->view->toc_ind = $toc_ind;
		$this->view->toc_eng = $toc_eng;
		$this->view->userid = $this->_userIdLogin;
		$this->view->custid = $this->_custIdLogin;
		$this->view->googleauth = true;
		$this->view->compinfo = $selectcomp[0];
		$this->view->cash_collateral = $get_cash_collateral[0];
		$this->view->bgnumb_enc = $numb;
		$this->view->suggestion_type = $suggestion_type;

		// decrypt numb
		$enc_pass = $settings->getSetting('enc_pass');
		$enc_salt = $settings->getSetting('enc_salt');

		$pw_hash = md5($enc_salt . $enc_pass);
		$rand = $this->_userIdLogin . date('dHis') . $pw_hash;
		$sessionNamespace->token = $rand;
		$password = $sessionNamespace->token;
		$BG_NUMBER = urldecode($numb);
		$BG_NUMBER = $AESMYSQL->decrypt($BG_NUMBER, $password);
		$numb = $BG_NUMBER;

		$this->view->token = $sessionNamespace->token;
		$this->view->numb = $numb;

		if (!empty($numb)) {
			$bgdata = $this->_db->select()
				->from(array('A' => 'T_BANK_GUARANTEE'), array('*', 'IS_AMENDMENT' => 'A.CHANGE_TYPE', 'BG_REG' => 'A.BG_REG_NUMBER'))
				->join(array('D' => 'TEMP_BANK_GUARANTEE_CLOSE'), 'A.BG_NUMBER = D.BG_NUMBER', ['CHANGE_TYPE_CLOSE' => 'D.CHANGE_TYPE', 'SUGGESTION_STATUS', 'CLOSE_REF_NUMBER' => 'D.CLOSE_REF_NUMBER'])
				->where('A.CUST_ID = ?', $this->_custIdLogin)
				->where('A.BG_NUMBER = ?', $numb)
				->where('D.SUGGESTION_STATUS IN (?)', ['3'])
				->query()->fetchAll();

			if (!empty($bgdata)) {
				$data = $bgdata['0'];

				$bgdatadetail = $this->_db->select()
					->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
					->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
					->where('A.BG_REG_NUMBER = ?', $data['BG_REG'])
					->query()->fetchAll();

				$checkProgressClose = $this->_db->select()
					->from('TEMP_BANK_GUARANTEE_CLOSE')
					->where('BG_NUMBER = ?', $data['BG_NUMBER'])
					->orWhere('BG_REG_NUMBER = ?', $data['BG_REG'])
					->query()->fetch();

				$this->view->dataClose = $checkProgressClose;

				if (!empty($bgdatadetail)) {
					foreach ($bgdatadetail as $key => $value) {
						if ($value['PS_FIELDNAME'] == 'Currency') {
							$this->view->bgCurrency =   $value['PS_FIELDVALUE'];
						}

						if ($data['COUNTER_WARRANTY_TYPE'] == '3') {
							if ($value['PS_FIELDNAME'] == 'Insurance Name') {
								$insuranceName = $this->_db->select()
									->from("M_CUSTOMER")
									->where("CUST_ID = ?", $value['PS_FIELDVALUE'])
									->query()->fetchAll();

								$this->view->insuraceName =  $insuranceName[0]['CUST_NAME'];
							}

							if ($value['PS_FIELDNAME'] == 'Insurance Branch') {
								$insuranceBranch = $this->_db->select()
									->from("M_INS_BRANCH")
									->where("INS_BRANCH_CODE = ?", $value['PS_FIELDVALUE'])
									->query()->fetchAll();

								$this->view->insuranceBranch = $insuranceBranch[0]["INS_BRANCH_NAME"];
							}
						}
					}
				}

				$closeDocument = $this->_db->select()
					->from(array('A' => 'TEMP_BANK_GUARANTEE_CLOSE'), array('*'))
					->join(array('B' => 'TEMP_BANK_GUARANTEE_FILE_CLOSE'), 'A.CLOSE_REF_NUMBER = B.CLOSE_REF_NUMBER')
					->where('A.CLOSE_REF_NUMBER = ?', $data['CLOSE_REF_NUMBER'])
					->query()->fetchAll();

				$this->view->closeDocument = $closeDocument;

				$policyBoundary = $this->findPolicyBoundary(38, $data['BG_AMOUNT']);
				$approverUserList = $this->findUserBoundary(38, $data['BG_AMOUNT']);
				$releaserList = $this->findUserPrivi('RLBG');

				$bgpublishType = $conf["bgpublish"]["type"]["desc"];
				$bgpublishCode = $conf["bgpublish"]["type"]["code"];
				$arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));

				$this->view->publishDesc = $arrbgpublish[$data['BG_PUBLISH']];

				if ($data['COUNTER_WARRANTY_TYPE'] == '3') {
					$this->view->isinsurance = true;
					$this->view->stamp_fee = $data['STAMP_FEE'];
					$this->view->adm_fee = $data['ADM_FEE'];
					$this->view->ProvFee = $data['PROVISION_FEE'];
				}
				$this->view->BG_CLAIM_DATE = $data['BG_CLAIM_DATE'];
				$this->view->policyBoundary = $policyBoundary;

				if ($data['BG_PUBLISH'] == 1) {
					$getPaperPrint = $this->_db->select()
						->from('M_PAPER')
						->where('notes = ?', $data['BG_NUMBER'])
						->where('STATUS = 1')
						->query()->fetchAll();
					$totalKertas = count($getPaperPrint);

					if ($totalKertas == 1) {
						$this->view->nomorRefKertas = ' - Nomor Ref Kertas : ' . $getPaperPrint[$totalKertas - 1]['PAPER_ID'] . ' (' . $totalKertas . ' lembar)';
					} else {
						$this->view->nomorRefKertas = ' - Nomor Ref Kertas : ' . $getPaperPrint[0]['PAPER_ID'] . ' - ' . $getPaperPrint[$totalKertas - 1]['PAPER_ID'] . ' (' . $totalKertas . ' lembar)';
					}
				}

				if ($data["COUNTER_WARRANTY_TYPE"] == "3") {
					$getInsuranceBranch = array_search("Insurance Branch", array_column($bgdatadetail, "PS_FIELDNAME"));
					$getInsuranceName = array_search("Insurance Name", array_column($bgdatadetail, "PS_FIELDNAME"));
					$getInsuranceBranch = $bgdatadetail[$getInsuranceBranch];
					$getInsuranceName = $bgdatadetail[$getInsuranceName];

					$insuranceBranch = $this->_db->select()
						->from("M_INS_BRANCH")
						->where("INS_BRANCH_CODE = ?", $getInsuranceBranch["PS_FIELDVALUE"])
						->query()->fetchAll();
					$this->view->insuranceBranch = $insuranceBranch[0]["INS_BRANCH_NAME"];

					// get linefacillity
					$paramLimit = array();
					$paramLimit['CUST_ID'] =  $getInsuranceName["PS_FIELDVALUE"];
					$paramLimit['COUNTER_WARRANTY_TYPE'] = $data['COUNTER_WARRANTY_TYPE'];
					$getLineFacility = Application_Helper_General::getLineFacility($paramLimit);
					$this->view->current_limit = $getLineFacility['currentLimit'];
					$this->view->max_limit =  $getLineFacility['plafondLimit'];
					$this->view->ticketSize =  $getLineFacility['ticketSize'];
					// end get linefacility
				}

				if ($data['COUNTER_WARRANTY_TYPE'] == '2') {
					// get linefacillity
					$paramLimit = array();
					$paramLimit['CUST_ID'] =  $this->_custIdLogin;
					$paramLimit['COUNTER_WARRANTY_TYPE'] = $data['COUNTER_WARRANTY_TYPE'];
					$getLineFacility = Application_Helper_General::getLineFacility($paramLimit);
					$this->view->current_limit = $getLineFacility['currentLimit'];
					$this->view->max_limit =  $getLineFacility['plafondLimit'];
					$this->view->ticketSize =  $getLineFacility['ticketSize'];
				}

				if ($data['COUNTER_WARRANTY_TYPE'] == '1') {
					// get linefacillity
					$paramLimit = array();
					$paramLimit['CUST_ID'] =  $this->_custIdLogin;
					$paramLimit['COUNTER_WARRANTY_TYPE'] = $data['COUNTER_WARRANTY_TYPE'];
					$getLineFacility = Application_Helper_General::getLineFacility($paramLimit);
					$this->view->current_limit = $getLineFacility['currentLimit'];
					$this->view->max_limit =  $getLineFacility['plafondLimit'];
					$this->view->ticketSize =  $getLineFacility['ticketSize'];
				}

				$selectHistory = $this->_db->select()
					->from('T_BANK_GUARANTEE_HISTORY_CLOSE')
					->where('CLOSE_REF_NUMBER = ?', $bgdata[0]['CLOSE_REF_NUMBER'])
					->where("BG_NUMBER = ?", $numb);
				$history = $this->_db->fetchAll($selectHistory);

				$historyStatusCode = $conf["history"]["status"]["code"];
				$historyStatusDesc = $conf["history"]["status"]["desc"];
				$historyStatusArr  = array_combine(array_values($historyStatusCode), array_values($historyStatusDesc));

				$this->view->dataHistory = $history;
				$this->view->historyStatusArr = $historyStatusArr;

				$getBoundary = $this->_db->select()
					->from('M_APP_BOUNDARY')
					->where('CUST_ID = ?', $data['CUST_ID'])
					->where('BOUNDARY_MIN <= \'' . $data['BG_AMOUNT'] . '\'')
					->where('BOUNDARY_MAX >= \'' . $data['BG_AMOUNT'] . '\'')
					->query()->fetch();

				$checkBoundary = strpos($getBoundary['POLICY'], 'THEN');
				if ($checkBoundary === false) $checkBoundary = strpos($getBoundary['POLICY'], 'AND');
				$checkCount = 1;
				$cust_approver = 1;
				foreach ($history as $row) {
					//if maker done
					if ($row['HISTORY_STATUS'] == 1) {
						$makerStatus = 'active';
						$makerIcon = '<i class="fas fa-check"></i>';
						$makerOngoing = '';
						if ($cust_reviewer == 0 && $cust_approver == 0) { //kalau tidak ada priv reviewer & approver
							$reviewerOngoing = '';
							$approverOngoing = '';
							$releaserOngoing = '';
						} else {
							$reviewerOngoing = '';
							$approverOngoing = '';
							$releaserOngoing = '';
						}
						$custlogin = $row['USER_LOGIN'];
						$selectCust	= $this->_db->select()
							->from('M_USER')
							->where("USER_ID = ?", $custlogin)
							->where("CUST_ID = ?", $row['CUST_ID']);
						$customer = $this->_db->fetchAll($selectCust);

						$custFullname = $customer[0]['USER_FULLNAME'];
						$custEmail 	  = $customer[0]['USER_EMAIL'];
						$custPhone	  = $customer[0]['USER_PHONE'];
						$makerApprovedBy = $custFullname;
						$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
						$align = 'align="center"';
						$marginRight = '';
						if ($cust_reviewer == 0 && $cust_approver == 0) {
							$align = '';
							$marginRight = 'style="margin-right: 15px;"';
						}
						$this->view->makerApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span ' . $marginRight . '>' . $custFullname . '</span></div>';
					}
					//if reviewer done

					//if approver done
					if (false) {
						$makerStatus = 'active';
						$approveStatus = 'active';
						$reviewStatus = 'active';
						$makerOngoing = '';
						$reviewerOngoing = '';
						$approverOngoing = 'ongoing';
						$releaserOngoing = '';

						$custlogin = $row['USER_LOGIN'];
						$selectuserapp	= $this->_db->select()
							->from(array('C' => 'T_APPROVAL'), array('*'))
							->where("C.PS_NUMBER = ?", $numb)
							->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin);
						$userapprove = $this->_db->fetchAll($selectuserapp);

						//tampung data user yang sudah approve
						if (!empty($userapprove) && empty($userid)) {
							foreach ($userapprove as $vl) {
								$userid[] = $vl['USER_ID'];
							}
						}
						$approveEfDate[] = date('d-M-Y', strtotime($row['DATE_TIME']));
					}

					//if approver done
					if ($checkBoundary === false) {
						if ($row['HISTORY_STATUS'] == 2) {
							$makerStatus = 'active';
							$makerIcon = '<i class="fas fa-check"></i>';
							$makerOngoing = '';
							if ($cust_reviewer == 0 && $cust_approver == 0) { //kalau tidak ada priv reviewer & approver
								$reviewerOngoing = '';
								$approverOngoing = '';
								$releaserOngoing = '';
							} else {
								$reviewerOngoing = '';
								$approverOngoing = '';
								$releaserOngoing = '';
							}
							$custlogin = $row['USER_LOGIN'];
							$selectCust	= $this->_db->select()
								->from('M_USER')
								->where("USER_ID = ?", $custlogin)
								->where("CUST_ID = ?", $row['CUST_ID']);
							$customer = $this->_db->fetchAll($selectCust);

							$custFullname = $customer[0]['USER_FULLNAME'];
							$custEmail 	  = $customer[0]['USER_EMAIL'];
							$custPhone	  = $customer[0]['USER_PHONE'];

							$approversApprovedBy = $custFullname;
							$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
							$align = 'align="center"';
							$marginRight = '';
							if ($cust_reviewer == 0 && $cust_approver == 0) {
								$align = '';
								$marginRight = 'style="margin-right: 15px;"';
							}
							$this->view->approversApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span ' . $marginRight . '>' . $custFullname . '</span></div>';
						}
					} else {
						if ($row['HISTORY_STATUS'] == 2) {
							$makerStatus = 'active';
							$makerIcon = '<i class="fas fa-check"></i>';
							$makerOngoing = '';
							if ($cust_reviewer == 0 && $cust_approver == 0) { //kalau tidak ada priv reviewer & approver
								$reviewerOngoing = '';
								$approverOngoing = '';
								$releaserOngoing = '';
							} else {
								$reviewerOngoing = '';
								$approverOngoing = '';
								$releaserOngoing = '';
							}
							$custlogin = $row['USER_LOGIN'];

							$selectCust	= $this->_db->select()
								->from('M_USER')
								->where("USER_ID = ?", $custlogin)
								->where("CUST_ID = ?", $row['CUST_ID']);
							$customer = $this->_db->fetchAll($selectCust);

							$custEmail 	  = $customer[0]['USER_EMAIL'];
							$custPhone	  = $customer[0]['USER_PHONE'];
							$approversApprovedBy = $custFullname;
							$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
							$align = 'align="center"';
							$marginRight = '';
							if ($checkCount === 1) {
								$getGroupUser = $this->_db->select()
									->from('M_APP_GROUP_USER')
									->where('USER_ID = ?', $customer[0]['USER_ID'])
									->query()->fetch();

								$arrayGroup = range('A', 'Z');
								$groupUser = $arrayGroup[intval(explode('_', $getGroupUser['GROUP_USER_ID'])[2]) - 1];
								$custFullname = $efdate . '<br><span>' . $customer[0]['USER_FULLNAME'] . ' (' . $groupUser . ')</span><br>';
								$checkCount++;
							} else {
								$getGroupUser = $this->_db->select()
									->from('M_APP_GROUP_USER')
									->where('USER_ID = ?', $customer[0]['USER_ID'])
									->query()->fetch();

								$arrayGroup = range('A', 'Z');
								$groupUser = $arrayGroup[intval(explode('_', $getGroupUser['GROUP_USER_ID'])[2]) - 1];
								$custFullname = $custFullname . $efdate . '<br><span>' . $customer[0]['USER_FULLNAME'] . ' (' . $groupUser . ')</span>';
								$checkCount = 1;
							}
							if ($cust_reviewer == 0 && $cust_approver == 0) {
								$align = '';
								$marginRight = 'style="margin-right: 15px;"';
							}
							$this->view->approversApprovedBy = '<div ' . $align . ' class="textTheme">' . $custFullname . '</span></div>';
						}
					}
					//if releaser done
					if (false) {
						$makerStatus = 'active';
						$approveIcon = '<i class="fas fa-check"></i>';
						$approveStatus = '';
						$reviewStatus = '';
						$releaseStatus = 'active';
						$releaseIcon = '<i class="fas fa-check"></i>';
						$makerOngoing = '';
						$reviewerOngoing = '';
						$approverOngoing = '';
						$releaserOngoing = '';
						$custlogin = $row['USER_LOGIN'];

						$selectCust	= $this->_db->select()
							->from('M_USER')
							->where("USER_ID = ?", $custlogin)
							->where("CUST_ID = ?", $row['CUST_ID']);
						$customer = $this->_db->fetchAll($selectCust);

						$custFullname = $customer[0]['USER_FULLNAME'];
						$releaserApprovedBy = $custFullname;
						$align = 'align="center"';
						$marginLeft = '';
						if ($cust_reviewer == 0 && $cust_approver == 0) {
							$align = '';
							$marginLeft = 'style="margin-left: 15px;"';
						}
						$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
						$this->view->releaserApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span ' . $marginLeft . '>' . $custFullname . '</span></div>';
					}

					if ($row['HISTORY_STATUS'] == 13) {
						$makerStatus = 'active';
						$insuranceIcon = '<i class="fas fa-check"></i>';
						$insuranceStatus = 'active';
						$reviewStatus = '';
						$makerOngoing = '';
						$reviewerOngoing = '';
						$approverOngoing = '';
						$custlogin = $row['USER_LOGIN'];

						$selectCust	= $this->_db->select()
							->from('M_USER')
							->where("USER_ID = ?", $custlogin);
						$customer = $this->_db->fetchAll($selectCust);

						$custFullname = $customer[0]['USER_FULLNAME'];
						$insuranceApprovedBy = $custFullname;
						$align = 'align="center"';
						$marginLeft = '';
						if ($cust_reviewer == 0 && $cust_approver == 0) {
							$align = '';
							$marginLeft = 'style="margin-left: 15px;"';
						}
						$efdate = date('d-M-Y', strtotime($row['DATE_TIME']));
						$this->view->insuranceApprovedBy = '<div ' . $align . ' class="textTheme">' . $efdate . '<br><span ' . $marginLeft . '>' . $custFullname . '</span></div>';
					}
				}

				//approvernamecircle jika sudah ada yang approve
				if (!empty($userid)) {
					$alphabet = array('01' => 'A', '02' => 'B', '03' => 'C', '04' => 'D', '05' => 'E', '06' => 'F', '07' => 'G', '08' => 'H', '09' => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z', 27 => 'SG');
					$flipAlphabet = array_flip($alphabet);
					$approvedNameList = array();
					$i = 0;
					foreach ($userid as $key => $value) {
						//select utk nama dan email
						$selectusername = $this->_db->select()
							->from(array('M_USER'), array('*'))
							->where("USER_ID = ?", (string) $value)
							->where("CUST_ID = ?", (string) $this->_custIdLogin);
						$username = $this->_db->fetchAll($selectusername);

						//select utk cek user berada di grup apa
						$selectusergroup	= $this->_db->select()
							->from(array('C' => 'M_APP_GROUP_USER'), array('*'))
							->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
							->where("C.USER_ID 	= ?", (string) $value);
						$usergroup = $this->_db->fetchAll($selectusergroup);

						$groupuserid = $usergroup[0]['GROUP_USER_ID'];
						$groupusername = $usergroup[0]['USER_ID'];
						$groupuseridexplode = explode("_", $groupuserid);
						if ($groupuseridexplode[0] == "S") {
							$usergroupid = "SG";
						} else {
							$usergroupid = $alphabet[$groupuseridexplode[2]];
						}
						array_push($approvedNameList, $username[0]['USER_FULLNAME']);
						$efdate = $approveEfDate[$i];
						$approverApprovedBy[] = '<div align="center" class="textTheme">' . $efdate . '<br>' . $username[0]['USER_FULLNAME'] . ' (' . $usergroupid . ')</div>';
						$i++;
					}
					$this->view->approverApprovedBy = $approverApprovedBy;

					//kalau sudah approve semua
					if (!$checkBoundary) {
						$releaserOngoing = 'ongoing';
					}
				}

				$selectsuperuser = $this->_db->select()
					->from(array('C' => 'T_APPROVAL'))
					->where("C.PS_NUMBER 	= ?", $numb)
					->where("C.GROUP 	= 'SG'");
				$superuser = $this->_db->fetchAll($selectsuperuser);

				if (!empty($superuser)) {
					$userid = $superuser[0]['USER_ID'];
					//select utk nama dan email
					$selectusername = $this->_db->select()
						->from(array('M_USER'), array('*'))
						->where("USER_ID = ?", (string) $userid)
						->where("CUST_ID = ?", (string) $this->_custIdLogin);
					$username = $this->_db->fetchAll($selectusername);
					$approverApprovedBy[] = '<div align="center" class="textTheme">' . $efdate . '<br>' . $username[0]['USER_FULLNAME'] . ' (' . $usergroupid . ')</div>';
					$approveStatus = 'active';
					$approverOngoing = '';
					$approveIcon = '<i class="fas fa-check"></i>';
					$releaserOngoing = 'ongoing';
				}

				$makerNameCircle = '<button id="makerCircle" style="cursor:default" class="btnCircleGroup ' . $makerStatus . ' ' . $makerOngoing . ' hovertext" disabled>' . $makerIcon . '</button>';
				foreach ($reviewerList as $key => $value) {
					$textColor = '';
					if ($value == $reviewerApprovedBy) {
						$textColor = 'text-white-50';
					}
					$reviewerListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value . '</p>';
				}

				$reviewerNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $reviewStatus . ' ' . $reviewerOngoing . ' hovertext" disabled>' . $reviewIcon . '</button>';
				foreach ($insuranceList as $key => $value) {
					$textColor = '';
					if ($value == $insuranceApprovedBy) {
						$textColor = 'text-white-50';
					}
					$insuranceListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value . '</p>';
				}

				$insuranceNameCircle = '<button style="cursor:default" class="btnCircleGroup ' . $insuranceStatus . ' ' . $insuranceOngoing . ' hovertext" disabled>' . $insuranceIcon . '</button>';
				$groupNameList = $approverUserList['GROUP_NAME'];
				unset($approverUserList['GROUP_NAME']);
				if ($approverUserList != '') {
					foreach ($approverUserList as $key => $value) {
						$approverListdata .= $key . ' (' . $groupNameList[$key] . ')' . '<br>';
						$i = 1;
						foreach ($value as $key2 => $value2) {
							$textColor = '';
							if (in_array($value2, $approvedNameList)) {
								$textColor = 'text-white-50';
							}
							if ($i == count($value)) {
								$approverListdata .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value2 . '</p><br>';
							} else {
								$approverListdata .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value2 . '</p>';
							}
							$i++;
						}
					}
				} else {
					$approverListdata = 'There is no Approver User';
				}

				$spandata = '';
				$approverNameCircle = '<button class="btnCircleGroup ' . $approveStatus . ' ' . $approverOngoing . ' hovertext" disabled>' . $approveIcon . ' ' . $spandata . '</button>';
				$releasenewNameCircle =  '<button class="btnCircleGroup hovertext" disabled>' . $spandata . '</button>';

				foreach ($releaserList as $key => $value) {
					$textColor = '';
					if ($value == $releaserApprovedBy) {
						$textColor = 'text-white-50';
					}
					$releaserListView .= '<p class="m-0 ' . $textColor . '" style="font-size: 13px">' . $value['USER_FULLNAME'] . '</p>';
				}
				$releaserNameCircle = '<button id="releaserCircle" class="btnCircleGroup ' . $releaseStatus . ' ' . $releaserOngoing . ' hovertext" disabled>' . $releaseIcon . ' <span class="hovertextcontent" style="left: -100px; text-align: center;">' . $releaserListView . '</span> </button>';

				$bgType = $conf["bg"]["type"]["desc"];
				$bgCode = $conf["bg"]["type"]["code"];
				$arrbgType = array_combine(array_values($bgCode), array_values($bgType));

				$this->view->policyBoundary = $policyBoundary;
				$this->view->releasenewNameCircle = $releasenewNameCircle;
				$this->view->makerNameCircle = $makerNameCircle;
				$this->view->reviewerNameCircle = $reviewerNameCircle;
				$this->view->insuranceNameCircle = $insuranceNameCircle;
				$this->view->approverNameCircle = $approverNameCircle;
				$this->view->releaserNameCircle = $releaserNameCircle;
				$this->view->makerStatus = $makerStatus;
				$this->view->approveStatus = $approveStatus;
				$this->view->reviewStatus = $reviewStatus;
				$this->view->releaseStatus = $releaseStatus;
				$this->view->arrbgType = $arrbgType;

				if (!empty($data['BG_BRANCH'])) {
					$selectbranch = $this->_db->select()
						->from(array('A' => 'M_BRANCH'), array('*'))
						->where('A.BRANCH_CODE = ?', $data['BG_BRANCH'])
						->query()->fetchAll();
					$this->view->branchname = $selectbranch[0]['BRANCH_NAME'];
				}

				$this->view->updateStart = Application_Helper_General::convertDate($data['TIME_PERIOD_START'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->updateEnd = Application_Helper_General::convertDate($data['TIME_PERIOD_END'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->TIME_PERIOD_START = $data['TIME_PERIOD_START'];
				$this->view->TIME_PERIOD_END = $data['TIME_PERIOD_END'];

				$BgType = $conf["bgclosing"]["status"]["desc"];
				$BgCode = $conf["bgclosing"]["status"]["code"];
				$arrStatus = array_combine(array_values($BgCode), array_values($BgType));
				$this->view->arrStatus = $arrStatus;

				$bgcgType = $conf["bgcg"]["type"]["desc"];
				$bgcgCode = $conf["bgcg"]["type"]["code"];
				$arrbgcg = array_combine(array_values($bgcgCode), array_values($bgcgType));
				$this->view->warranty_type_text = $arrbgcg[$data['COUNTER_WARRANTY_TYPE']];

				if ($data['BG_NUMBER'] == '') {
					$data['BG_NUMBER'] = '-';
				}
				if ($data['BG_SUBJECT'] == '') {
					$data['BG_SUBJECT'] = '- no subject -';
				}

				$this->view->BG_REG_NUMBER = $data['BG_REG'];
				$this->view->BG_NUMBER = $data['BG_NUMBER'];
				$this->view->BG_SUBJECT = $data['BG_SUBJECT'];
				$this->view->recipent_name = $data['RECIPIENT_NAME'];
				$this->view->address = $data['RECIPIENT_ADDRES'];
				$this->view->city = $data['RECIPIENT_CITY'];
				$this->view->contact_number = $data['RECIPIENT_CONTACT'];
				$this->view->contact_person = $data['RECIPIENT_CP'];
				$this->view->phone = $data['RECIPIENT_OFFICE_NUMBER'];
				$this->view->contact_email = $data['RECIPIENT_EMAIL'];
				$this->view->data = $data;
				$this->view->fileName = $data['FILE'];
				$this->view->bank_amount = $data['BG_AMOUNT'];
				$this->view->warranty_type = $data['COUNTER_WARRANTY_TYPE'];
				$this->view->account_number = $data['COUNTER_WARRANTY_ACCT_NO'];
				$this->view->account_name = $data['COUNTER_WARRANTY_ACCT_NAME'];
				$this->view->amount = $data['COUNTER_WARRANTY_AMOUNT'];
				$this->view->acct = $data['FEE_CHARGE_TO'];
				$this->view->status = $data['BG_STATUS'];
				$this->view->suggestionStatus = $data['SUGGESTION_STATUS'];
				$this->view->claim_period = $settings->getSetting('claim_period');
				$this->view->GT_DATE = Application_Helper_General::convertDate($data['GT_DOC_DATE'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
				$this->view->GT_NUMBER = $data['GT_DOC_NUMBER'];

				$bgdocType = $conf["bgdoc"]["type"]["desc"];
				$bgdocCode = $conf["bgdoc"]["type"]["code"];
				$arrbgdoc = array_combine(array_values($bgdocCode), array_values($bgdocType));
				$this->view->GT_TYPE = $arrbgdoc[$data['GT_DOC_TYPE']];

				$this->view->GT_OTHERS = $data['GT_DOC_OTHER'];
				$this->view->usage_purpose = $data['USAGE_PURPOSE'];
				$this->view->comment = $data['SERVICE'];

				if (!empty($data['BG_REJECT_NOTES']) || !empty($data['BG_CANCEL_NOTES'])) {
					$select = $this->_db->select()
						->from(
							array('a' => 'T_BANK_GUARANTEE_HISTORY'),
							array(
								'a.USER_LOGIN', 'u_name' => 'b.USER_FULLNAME', 'b_name' => 'c.BUSER_NAME',
								'a.DATE_TIME', 'a.BG_REASON', 'a.HISTORY_STATUS', 'a.BG_REG_NUMBER'
							)
						)
						->joinLeft(
							array('b' => 'M_USER'),
							'a.USER_LOGIN = b.USER_ID AND a.CUST_ID = b.CUST_ID',
							array()
						)
						->joinLeft(array('c' => 'M_BUSER'), 'a.USER_LOGIN = c.BUSER_ID', array())
						->where('a.BG_REG_NUMBER = ?', (string) $data['BG_REG'])
						->where('a.HISTORY_STATUS = ?', (string) $data['BG_STATUS'])
						->group('a.HISTORY_ID')
						->order('a.DATE_TIME');
					$result = $this->_db->fetchAll($select);

					if (!empty($result)) {
						$data['REASON'] = $result['0']['BG_REASON'];
					}
					$this->view->reqrepair = true;
					$this->view->reason = $data['REASON'] . '' . $data['BG_REJECT_NOTES'] . $data['BG_CANCEL_NOTES'];
				}

				if (!empty($bgdatadetail)) {
					foreach ($bgdatadetail as $key => $value) {
						if ($data['COUNTER_WARRANTY_TYPE'] == 3) {
							// if ($value['PS_FIELDNAME'] == 'Insurance Name') {
							// 	$this->view->insuranceName =   $value['PS_FIELDVALUE'];
							// }

							if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
								$this->view->PrincipalAgreement =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount') {
								$this->view->insurance_amount =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement Start Date') {
								$this->view->paDateStart =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Principal Agreement End Date') {
								$this->view->paDateEnd =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Insurance Branch') {
								$insBranchCode =   $value['PS_FIELDVALUE'];
							}
						} else {
							if ($value['PS_FIELDNAME'] == 'Plafond Owner') {
								$this->view->owner1 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner') {
								$this->view->amountowner1 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
								$this->view->owner2 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
								$this->view->amountowner2 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
								$this->view->owner3 =   $value['PS_FIELDVALUE'];
							}

							if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
								$this->view->amountowner3 =   $value['PS_FIELDVALUE'];
							}
						}
					}
				}

				$this->view->bankname = $conf['app']['bankname'];

				$download = $this->_getParam('download');
				if ($download) {
					$attahmentDestination = UPLOAD_PATH . '/document/submit/';
					$this->_helper->download->file($data['FILE'], $attahmentDestination . $data['FILE']);
				}

				// Marginal Deposit Eksisting ---------------
				$bgdatamdeks = $this->_db->select()
					->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
					->joinLeft(['B' => 'M_CUSTOMER_ACCT'], 'A.ACCT = B.ACCT_NO AND B.CUST_ID = ' . $this->_db->quote($this->_custIdLogin) . '', ["M_ACCT_DESC" => "B.ACCT_DESC", "M_ACCT_TYPE" => "B.ACCT_TYPE", 'CUST_ID', 'CCY_ID'])
					->where('A.BG_REG_NUMBER = ?', $data['BG_REG'])
					->query()->fetchAll();

				$mdeksisting = null;
				$saveEscrow = 0;
				foreach ($bgdatamdeks as $key => $value) {
					// if ($value['CUST_ID'] != $data['CUST_ID']) continue;
					// if ($value["ACCT_DESC"]) continue;
					// $mdeksisting += intval($value["AMOUNT"]);
					if (strtolower($value["M_ACCT_DESC"]) == "giro" || $value["M_ACCT_TYPE"] == "D" || strtolower($value["ACCT_DESC"]) == "giro" || strtolower($value["ACCT_TYPE"]) == "giro") continue;
					$mdeksisting += floatval($value["AMOUNT"]);
					if (strtolower($value["ACCT_DESC"] == 'Escrow')) $saveEscrow += floatval($value["AMOUNT"]);
				}

				$this->view->totaleksisting = $mdeksisting;
				$this->view->escrow = $saveEscrow;



				// Pelepasan dana ---------------
				$mdclose = $this->_db->select()
					->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL_CLOSE'), array('*'))
					->where('A.CLOSE_REF_NUMBER = ?', $data['CLOSE_REF_NUMBER'])
					->query()->fetchAll();

				$groupedArray = array();
				$totalClose = null;
				foreach ($mdclose as $row) {
					$index = $row['ACCT_INDEX'];
					if (!isset($groupedArray[$index])) {
						$groupedArray[$index] = array(
							'ownAccount' => '',
							'ccyAccount' => '',
							'numberAccount' => '',
							'nameAccount' => '',
							'ccyTransaction' => '',
							'amountTransaction' => '',
						);
					}
					if ($row['PS_FIELDNAME'] == 'Beneficiary is own account') {
						$groupedArray[$index]['ownAccount'] = $row['PS_FIELDVALUE'];
					} elseif ($row['PS_FIELDNAME'] == 'Beneficiary Account CCY') {
						$groupedArray[$index]['ccyAccount'] = $row['PS_FIELDVALUE'];
					} elseif ($row['PS_FIELDNAME'] == 'Beneficiary Account Number') {
						$groupedArray[$index]['numberAccount'] = $row['PS_FIELDVALUE'];
					} elseif ($row['PS_FIELDNAME'] == 'Beneficiary Account Name') {
						$groupedArray[$index]['nameAccount'] = $row['PS_FIELDVALUE'];
					} elseif ($row['PS_FIELDNAME'] == 'Transaction CCY') {
						$groupedArray[$index]['ccyTransaction'] = $row['PS_FIELDVALUE'];
					} elseif ($row['PS_FIELDNAME'] == 'Transaction Amount') {
						$groupedArray[$index]['amountTransaction'] = $row['PS_FIELDVALUE'];
						$totalClose += $row['PS_FIELDVALUE'];
					}
				}
				$groupedArray = array_values($groupedArray);
				$this->view->totalclose = $totalClose;
				$this->view->mdclose = $groupedArray;

				$download = $this->_getParam('certfinal');
				if ($download == 1) {
					$attahmentDestination = UPLOAD_PATH . '/document/submit/';
					return $this->_helper->download->file("BG_NO_" . $data["BG_NUMBER"] . ".pdf", $attahmentDestination . $data['DOCUMENT_ID'] . ".pdf");
				}

				$klaimErr = false;
				$cekKlaimStatus = $this->_db->select()
					->from('TEMP_BANK_GUARANTEE_CLOSE')
					->where('BG_NUMBER = ?', $data['BG_NUMBER'])
					->where('CHANGE_TYPE = 3')
					->where('SUGGESTION_STATUS NOT IN (7,11)')
					->query()->fetch();

				if (!empty($cekKlaimStatus)) {
					$klaimErr = true;
				}

				$this->view->klaimErr = $klaimErr;

				if ($this->_request->isPost()) {
					$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

					$release = $this->_getParam('release');
					$reject = $this->_getParam('reject');
					$repair = $this->_getParam('repair');

					$filter = new Application_Filtering();
					$tokenmerge = $this->_request->getPost('tokenmerge');
					$responseCode = $filter->filter($tokenmerge, "SELECTION");

					if ($release) {
						$getNamaPemohon = $this->_db->select()
							->from('M_CUSTOMER', ['CUST_NAME'])
							->where('CUST_ID = ?', $data['CUST_ID'])
							->query()->fetch();
						$getNamaPemohon = $getNamaPemohon['CUST_NAME'];

						if (!empty($usergoogleAuth)) {
							$pga = new PHPGangsta_GoogleAuthenticator();
							$settingObj = new Settings();
							$gduration = $settingObj->getSetting("google_duration");
							if (SGO_Helper_GeneralFunction::validateToken($numb, $this->_custIdLogin, $this->_userIdLogin, $responseCode)) {
								$this->_db->beginTransaction();
								$resultToken = $resHard['ResponseCode'] == '0000';
								$datatoken = array('USER_FAILEDTOKEN' => 0);
								$wheretoken =  array();
								$wheretoken['USER_ID = ?'] = $this->_userIdLogin;
								$wheretoken['CUST_ID = ?'] = $this->_custIdLogin;
								$this->_db->update('M_USER', $datatoken, $wheretoken);

								// Update data ke table TEMP_BANK_GUARANTEE_CLOSE
								$dataclose = [
									'LASTUPDATED' => new Zend_Db_Expr("now()"),
									'LASTUPDATEDFROM' => 'PRINCIPAL',
									'LASTUPDATEDBY' => $this->_userIdLogin,
								];

								if ($data['IS_AMENDMENT'] == '4') {
									$dataclose['SUGGESTION_STATUS'] = '10';
								} else {
									$dataclose['SUGGESTION_STATUS'] = '4';
								}

								$whereclose['CLOSE_REF_NUMBER = ?'] = $data['CLOSE_REF_NUMBER'];
								$this->_db->update('TEMP_BANK_GUARANTEE_CLOSE', $dataclose, $whereclose);

								if ($bgdata[0]['CHANGE_TYPE_CLOSE'] == '5') {
									$this->_db->delete('TEMP_BANK_GUARANTEE_CLOSE', [
										'CLOSE_REF_NUMBER = ?' => $bgdata[0]['CLOSE_REF_NUMBER']
									]);


									// INSERT PSLIP
									$this->_db->insert('T_BG_PSLIP', [
										'CLOSE_REF_NUMBER' => $bgdata[0]['CLOSE_REF_NUMBER'],
										'BG_NUMBER' => $bgdata[0]['BG_NUMBER'],
										'PS_STATUS' => '1',
										'TYPE' => '2',
									]);
								}

								// Insert data ke table T_BANK_GUARANTEE_HISTORY_CLOSE
								$historyInsert = [
									'DATE_TIME' => new Zend_Db_Expr("now()"),
									'CLOSE_REF_NUMBER' => $data['CLOSE_REF_NUMBER'],
									'BG_NUMBER' => $data['BG_NUMBER'],
									'CUST_ID' => $this->_custIdLogin,
									'USER_FROM' => 'PRINCIPAL',
									'USER_LOGIN' => $this->_userIdLogin,
									'BG_REASON' => 'NoRef Pengajuan : ' . $data['CLOSE_REF_NUMBER'] . '',
									'HISTORY_STATUS' => 3,
								];
								$this->_db->insert('T_BANK_GUARANTEE_HISTORY_CLOSE', $historyInsert);

								if ($bgdata[0]['COUNTER_WARRANTY_TYPE'] === '1') {
									$getAllBgroup = $this->_db->select()
										->from('M_BPRIVI_GROUP')
										->where('BPRIVI_ID  = ?', 'VCCS')
										->query()->fetchAll();
								} elseif ($bgdata[0]['COUNTER_WARRANTY_TYPE'] === '2') {
									$getAllBgroup = $this->_db->select()
										->from('M_BPRIVI_GROUP')
										->where('BPRIVI_ID  = ?', 'VNCS')
										->query()->fetchAll();
								} elseif ($bgdata[0]['COUNTER_WARRANTY_TYPE'] === '3') {
									$getAllBgroup = $this->_db->select()
										->from('M_BPRIVI_GROUP')
										->where('BPRIVI_ID  = ?', 'VNCS')
										->query()->fetchAll();
								}

								// TEMP BANK GUARANTEE UNDERLYING
								$getAllUnderlying = $this->_db->select()
									->from('T_BANK_GUARANTEE_UNDERLYING')
									->where('BG_REG_NUMBER = ?', $data['BG_REG_NUMBER'])
									->query()->fetchAll();

								$underlyingText = implode(', ', array_column($getAllUnderlying, 'DOC_NAME'));

								$underlyingForEmail = '
									<div style="display:inline-block;max-width: 394px;overflow:hidden;text-overflow: ellipsis;white-space: nowrap;">' . $underlyingText . '</div>
										<br>
									<div style="display:inline-block; margin-top: 5px">(Total : ' . count($getAllUnderlying) . ' Dokumen)</div>';

								if ($bgdata[0]['COUNTER_WARRANTY_TYPE'] === '1' || $bgdata[0]['COUNTER_WARRANTY_TYPE'] === '2') {
									$saveBgroup = array_column($getAllBgroup, 'BGROUP_ID');
									$getAllBuser = $this->_db->select()
										->from(['MB' => 'M_BUSER'], ['MB.BUSER_EMAIL', 'MB.BUSER_NAME'])
										->joinLeft(['MBG' => 'M_BGROUP'], 'MB.BGROUP_ID = MBG.BGROUP_ID', ['MBG.BGROUP_DESC'])
										->joinLeft(['MBR' => 'M_BRANCH'], 'MB.BUSER_BRANCH = MBR.ID', [])
										->where('MB.BGROUP_ID IN (?)', $saveBgroup)
										->where('MBR.BRANCH_CODE = ?', $bgdata[0]['BG_BRANCH'])
										->query()->fetchAll();

									$tipePengajuan = ($bgdata[0]['IS_AMENDMENT'] == 0 || empty($bgdata[0]['IS_AMENDMENT'])) ? 'New' : 'Amandemen';
									$allSetting = $settings->getAllSetting();
									foreach ($getAllBuser as $buser) {
										$getEmailTemplate = $allSetting['bemailtemplate_6A'];
										$dataEmail = [
											'[[title_change_type]]' => 'VERIFIKASI',
											'[[master_bank_name]]' => $allSetting["master_bank_name"],
											'[[close_ref_number]]' => $data['CLOSE_REF_NUMBER'],
											'[[bg_number]]' => $data['BG_NUMBER'],
											'[[contract_name]]' => $underlyingForEmail,
											'[[usage_purpose]]' => ucwords(strtolower($data['USAGE_PURPOSE_DESC'])),
											'[[cust_name]]' => $selectcomp[0]['CUST_NAME'],
											'[[recipient_name]]' => $data['RECIPIENT_NAME'],
											'[[bg_amount]]' => $this->view->bgCurrency . ' ' . number_format($data['BG_AMOUNT'], 2),
											'[[counter_warranty_type]]' => $counterWarranty[$data['COUNTER_WARRANTY_TYPE']],
											'[[change_type]]' => $suggestion_type[$data['CHANGE_TYPE_CLOSE']],
											'[[suggestion_status]]' => $arrStatus[$dataclose['SUGGESTION_STATUS']],
											'[[repair_notes]]' => '-',
										];
										$getEmailTemplate = strtr($getEmailTemplate, $dataEmail);

										if ($bgdata[0]['CHANGE_TYPE_CLOSE'] != '5') {
											Application_Helper_Email::sendEmail($buser['BUSER_EMAIL'], strval($arrStatus[$dataclose['SUGGESTION_STATUS']] . ' BG NO. ' . $data['BG_NUMBER']), $getEmailTemplate);
										}
									}
								}

								if ($bgdata[0]['COUNTER_WARRANTY_TYPE'] === '3') {
									$saveBgroup = array_column($getAllBgroup, 'BGROUP_ID');
									$getAllBuser = $this->_db->select()
										->from(['MB' => 'M_BUSER'], ['MB.BUSER_EMAIL', 'MB.BUSER_NAME'])
										->joinLeft(['MBG' => 'M_BGROUP'], 'MB.BGROUP_ID = MBG.BGROUP_ID', ['MBG.BGROUP_DESC'])
										->joinLeft(['MBR' => 'M_BRANCH'], 'MB.BUSER_BRANCH = MBR.ID', [])
										->where('MB.BGROUP_ID IN (?)', $saveBgroup)
										->where('MBR.BRANCH_CODE = ?', $bgdata[0]['BG_BRANCH'])
										->query()->fetchAll();

									$tipePengajuan = ($bgdata[0]['IS_AMENDMENT'] == 0 || empty($bgdata[0]['IS_AMENDMENT'])) ? 'New' : 'Amandemen';
									$allSetting = $settings->getAllSetting();
									foreach ($getAllBuser as $buser) {
										$getEmailTemplate = $allSetting['bemailtemplate_6A'];
										$dataEmail = [
											'[[title_change_type]]' => 'VERIFIKASI',
											'[[master_bank_name]]' => $allSetting["master_bank_name"],
											'[[close_ref_number]]' => $data['CLOSE_REF_NUMBER'],
											'[[bg_number]]' => $data['BG_NUMBER'],
											'[[contract_name]]' => $underlyingForEmail,
											'[[usage_purpose]]' => ucwords(strtolower($data['USAGE_PURPOSE_DESC'])),
											'[[cust_name]]' => $selectcomp[0]['CUST_NAME'],
											'[[recipient_name]]' => $data['RECIPIENT_NAME'],
											'[[bg_amount]]' => $this->view->bgCurrency . ' ' . number_format($data['BG_AMOUNT'], 2),
											'[[counter_warranty_type]]' => $counterWarranty[$data['COUNTER_WARRANTY_TYPE']],
											'[[change_type]]' => $suggestion_type[$data['CHANGE_TYPE_CLOSE']],
											'[[suggestion_status]]' => $arrStatus[$dataclose['SUGGESTION_STATUS']],
											'[[repair_notes]]' => '-',
										];
										$getEmailTemplate = strtr($getEmailTemplate, $dataEmail);
										if ($bgdata[0]['CHANGE_TYPE_CLOSE'] != '5') {
											Application_Helper_Email::sendEmail($buser['BUSER_EMAIL'], strval($arrStatus[$dataclose['SUGGESTION_STATUS']] . ' BG NO. ' . $data['BG_NUMBER']), $getEmailTemplate);
										}
									}
								}

								// if (intval($bgdata[0]['COUNTER_WARRANTY_TYPE']) === 3) {
								// 	$getInsranch = $this->_db->select()
								// 		->from('M_INS_BRANCH', ['INS_BRANCH_NAME', 'INS_BRANCH_EMAIL'])
								// 		->where('INS_BRANCH_CODE = ?', $insBranchCode)
								// 		->query()->fetch();

								// 	$tipePengajuan = ($bgdata[0]['IS_AMENDMENT'] == 0 || empty($bgdata[0]['IS_AMENDMENT'])) ? 'New' : 'Amandemen';
								// 	$allSetting = $settings->getAllSetting();
								// 	$getEmailTemplate = $allSetting['femailtemplate_ijinprinsip_notif'];
								// 	$data = [
								// 		'[[nama_cabang_asuransi]]' => $getInsranch['INS_BRANCH_NAME'],
								// 		'[[user_name]]' => $buser['BUSER_NAME'],
								// 		'[[group_name]]' => $buser['BGROUP_DESC'],
								// 		'[[master_bank_name]]' => $allSetting["master_bank_name"],
								// 		'[[master_bank_app_name]]' => $allSetting["master_bank_app_name"],
								// 		'[[bg_reg_number]]' => $bgdata[0]["BG_REG_NUMBER"],
								// 		'[[bg_subject]]' => $bgdata[0]["BG_SUBJECT"],
								// 		// '[[recipient_name]]' => $bgdata[0]["RECIPIENT_NAME"],
								// 		'[[recipient_name]]' => $getNamaPemohon,
								// 		'[[is_amandment]]' => $tipePengajuan,
								// 		'[[bg_status]]' => $bgStatus[8],
								// 		'[[counter_warranty_type]]' => $counterWarranty[$bgdata[0]['COUNTER_WARRANTY_TYPE']],
								// 	];
								// 	$getEmailTemplate = strtr($getEmailTemplate, $data);
								// 	Application_Helper_Email::sendEmail($getInsranch['INS_BRANCH_EMAIL'], 'Permintaan Permohonan Ijin Prinsip', $getEmailTemplate);
								// }

								// SEND EMAIL TO OBLIGEE
								$getEmailTemplate = $allSetting['bemailtemplate_1A'];

								// TEMP BANK GUARANTEE UNDERLYING
								$getAllUnderlying = $this->_db->select()
									->from('T_BANK_GUARANTEE_UNDERLYING')
									->where('BG_REG_NUMBER = ?', $data['BG_REG_NUMBER'])
									->query()->fetchAll();

								$underlyingText = implode(', ', array_column($getAllUnderlying, 'DOC_NAME'));

								$underlyingForEmail = '
									<div style="display:inline-block;max-width: 394px;overflow:hidden;text-overflow: ellipsis;white-space: nowrap;">' . $underlyingText . '</div>
										<br>
									<div style="display:inline-block; margin-top: 5px">(Total : ' . count($getAllUnderlying) . ' Dokumen)</div>';

								$dataEmail = [
									'[[recipient_cp]]' => $bgdata[0]['RECIPIENT_CP'],
									'[[recipient_name]]' => $bgdata[0]['RECIPIENT_NAME'],
									'[[bg_number]]' => $bgdata[0]['BG_NUMBER'],
									'[[contract_name]]' => $underlyingForEmail,
									'[[usage_purpose]]' => ucwords(strtolower($bgdata[0]['USAGE_PURPOSE_DESC'])),
									'[[cust_name]]' => $selectcomp[0]['CUST_NAME'],
									'[[recipient_name]]' => $bgdata[0]['RECIPIENT_NAME'],
									'[[bg_amount]]' => $this->view->bgCurrency . ' ' . number_format($bgdata[0]['BG_AMOUNT'], 2),
									'[[close_ref_number]]' => $data['CLOSE_REF_NUMBER'],
									'[[master_bank_email]]' => $allSetting["master_bank_email"],
									'[[master_bank_telp]]' => $allSetting["master_bank_telp"],
									'[[master_app_name]]' => $allSetting["master_bank_app_name"],
									'[[master_bank_name]]' => $allSetting["master_bank_name"],
								];
								$getEmailTemplate = strtr($getEmailTemplate, $dataEmail);
								if ($bgdata[0]['CHANGE_TYPE_CLOSE'] != '5') {
									Application_Helper_Email::sendEmail($bgdata[0]['RECIPIENT_EMAIL'], 'PEMBERITAHUAN PENGAJUAN PENUTUPAN BANK GARANSI', $getEmailTemplate);
								}

								$this->_db->commit();

								// Simpan log aktifitas user ke table T_FACTIVITY
								if ($data['IS_AMENDMENT'] == '1') Application_Helper_General::writeLog('RLBG', 'Rilis Pengajuan Penutupan untuk BG No : ' . $data['BG_NUMBER'] . ', Subjek : ' . $data['BG_SUBJECT'] . ', NoRef Pengajuan : ' . $data['CLOSE_REF_NUMBER'] . '');
								if ($data['IS_AMENDMENT'] == '2') Application_Helper_General::writeLog('RLBG', 'Rilis Pengajuan Pelepasan Marginal Deposit untuk BG No : ' . $data['BG_NUMBER'] . ', Subjek : ' . $data['BG_SUBJECT'] . ', NoRef Pengajuan : ' . $data['CLOSE_REF_NUMBER'] . '');

								$this->setbackURL('/' . $this->_request->getModuleName() . '/release/');
								$this->_redirect('/notification/success/index');
							} else {
								$tokenFailed = $CustUser->setLogToken(); //log token activity
								$error = true;
								$errorMsg[] = $this->language->_('Invalid Response Code');  //$verToken['ResponseDesc'];
								$this->view->popauth = true;
								if ($tokenFailed === true) {
									$this->_redirect('/default/index/logout');
								}
							}
						} else {
							if (SGO_Helper_GeneralFunction::validateToken($numb, $this->_custIdLogin, $this->_userIdLogin, $responseCode)) {
								$this->_db->beginTransaction();
								$resultToken = $resHard['ResponseCode'] == '0000';
								$datatoken = array('USER_FAILEDTOKEN' => 0);
								$wheretoken =  array();
								$wheretoken['USER_ID = ?'] = $this->_userIdLogin;
								$wheretoken['CUST_ID = ?'] = $this->_custIdLogin;
								$this->_db->update('M_USER', $datatoken, $wheretoken);

								if ($bgdata[0]['CHANGE_TYPE_CLOSE'] == '5') {
									$this->_db->delete('TEMP_BANK_GUARANTEE_CLOSE', [
										'CLOSE_REF_NUMBER = ?' => $bgdata[0]['CLOSE_REF_NUMBER']
									]);

									// INSERT PSLIP
									$this->_db->insert('T_BG_PSLIP', [
										'CLOSE_REF_NUMBER' => $bgdata[0]['CLOSE_REF_NUMBER'],
										'BG_NUMBER' => $bgdata[0]['BG_NUMBER'],
										'PS_STATUS' => '1',
										'TYPE' => '2',
									]);
								}

								// Update data ke table TEMP_BANK_GUARANTEE_CLOSE
								$dataclose = [
									'LASTUPDATED' => new Zend_Db_Expr("now()"),
									'LASTUPDATEDFROM' => 'PRINCIPAL',
									'LASTUPDATEDBY' => $this->_userIdLogin,
								];

								if ($data['IS_AMENDMENT'] == '4') {
									$dataclose['SUGGESTION_STATUS'] = '10';
								} else {
									$dataclose['SUGGESTION_STATUS'] = '4';
								}

								$whereclose['CLOSE_REF_NUMBER = ?'] = $data['CLOSE_REF_NUMBER'];
								$this->_db->update('TEMP_BANK_GUARANTEE_CLOSE', $dataclose, $whereclose);

								// Insert data ke table T_BANK_GUARANTEE_HISTORY_CLOSE
								$historyInsert = [
									'DATE_TIME' => new Zend_Db_Expr("now()"),
									'CLOSE_REF_NUMBER' => $data['CLOSE_REF_NUMBER'],
									'BG_NUMBER' => $data['BG_NUMBER'],
									'CUST_ID' => $this->_custIdLogin,
									'USER_FROM' => 'PRINCIPAL',
									'USER_LOGIN' => $this->_userIdLogin,
									'BG_REASON' => 'NoRef Pengajuan : ' . $data['CLOSE_REF_NUMBER'] . '',
									'HISTORY_STATUS' => 3,
								];
								$this->_db->insert('T_BANK_GUARANTEE_HISTORY_CLOSE', $historyInsert);

								if ($bgdata[0]['COUNTER_WARRANTY_TYPE'] === '1') {
									$getAllBgroup = $this->_db->select()
										->from('M_BPRIVI_GROUP')
										->where('BPRIVI_ID  = ?', 'VCCS')
										->query()->fetchAll();
								} elseif ($bgdata[0]['COUNTER_WARRANTY_TYPE'] === '2') {
									$getAllBgroup = $this->_db->select()
										->from('M_BPRIVI_GROUP')
										->where('BPRIVI_ID  = ?', 'VNCS')
										->query()->fetchAll();
								} elseif ($bgdata[0]['COUNTER_WARRANTY_TYPE'] === '3') {
									$getAllBgroup = $this->_db->select()
										->from('M_BPRIVI_GROUP')
										->where('BPRIVI_ID  = ?', 'VNCS')
										->query()->fetchAll();
								}

								// TEMP BANK GUARANTEE UNDERLYING
								$getAllUnderlying = $this->_db->select()
									->from('T_BANK_GUARANTEE_UNDERLYING')
									->where('BG_REG_NUMBER = ?', $data['BG_REG_NUMBER'])
									->query()->fetchAll();

								$underlyingText = implode(', ', array_column($getAllUnderlying, 'DOC_NAME'));

								$underlyingForEmail = '
									<div style="display:inline-block;max-width: 394px;overflow:hidden;text-overflow: ellipsis;white-space: nowrap;">' . $underlyingText . '</div>
										<br>
									<div style="display:inline-block; margin-top: 5px">(Total : ' . count($getAllUnderlying) . ' Dokumen)</div>';

								if ($bgdata[0]['COUNTER_WARRANTY_TYPE'] === '1' || $bgdata[0]['COUNTER_WARRANTY_TYPE'] === '2') {
									$saveBgroup = array_column($getAllBgroup, 'BGROUP_ID');
									$getAllBuser = $this->_db->select()
										->from(['MB' => 'M_BUSER'], ['MB.BUSER_EMAIL', 'MB.BUSER_NAME'])
										->joinLeft(['MBG' => 'M_BGROUP'], 'MB.BGROUP_ID = MBG.BGROUP_ID', ['MBG.BGROUP_DESC'])
										->joinLeft(['MBR' => 'M_BRANCH'], 'MB.BUSER_BRANCH = MBR.ID', [])
										->where('MB.BGROUP_ID IN (?)', $saveBgroup)
										->where('MBR.BRANCH_CODE = ?', $bgdata[0]['BG_BRANCH'])
										->query()->fetchAll();

									$tipePengajuan = ($bgdata[0]['IS_AMENDMENT'] == 0 || empty($bgdata[0]['IS_AMENDMENT'])) ? 'New' : 'Amandemen';
									$allSetting = $settings->getAllSetting();
									foreach ($getAllBuser as $buser) {
										$getEmailTemplate = $allSetting['bemailtemplate_6A'];
										$dataEmail = [
											'[[title_change_type]]' => 'VERIFIKASI',
											'[[master_bank_name]]' => $allSetting["master_bank_name"],
											'[[close_ref_number]]' => $data['CLOSE_REF_NUMBER'],
											'[[bg_number]]' => $data['BG_NUMBER'],
											'[[contract_name]]' => $underlyingForEmail,
											'[[usage_purpose]]' => ucwords(strtolower($data['USAGE_PURPOSE_DESC'])),
											'[[cust_name]]' => $selectcomp[0]['CUST_NAME'],
											'[[recipient_name]]' => $data['RECIPIENT_NAME'],
											'[[bg_amount]]' => $this->view->bgCurrency . ' ' . number_format($data['BG_AMOUNT'], 2),
											'[[counter_warranty_type]]' => $counterWarranty[$data['COUNTER_WARRANTY_TYPE']],
											'[[change_type]]' => $suggestion_type[$data['CHANGE_TYPE_CLOSE']],
											'[[suggestion_status]]' => $arrStatus[$dataclose['SUGGESTION_STATUS']],
											'[[repair_notes]]' => '-',
										];
										$getEmailTemplate = strtr($getEmailTemplate, $dataEmail);
										if ($bgdata[0]['CHANGE_TYPE_CLOSE'] != '5') {
											Application_Helper_Email::sendEmail($buser['BUSER_EMAIL'], strval($arrStatus[$dataclose['SUGGESTION_STATUS']] . ' BG NO. ' . $data['BG_NUMBER']), $getEmailTemplate);
										}
									}
								}

								if ($bgdata[0]['COUNTER_WARRANTY_TYPE'] === '3') {
									$saveBgroup = array_column($getAllBgroup, 'BGROUP_ID');
									$getAllBuser = $this->_db->select()
										->from(['MB' => 'M_BUSER'], ['MB.BUSER_EMAIL', 'MB.BUSER_NAME'])
										->joinLeft(['MBG' => 'M_BGROUP'], 'MB.BGROUP_ID = MBG.BGROUP_ID', ['MBG.BGROUP_DESC'])
										->joinLeft(['MBR' => 'M_BRANCH'], 'MB.BUSER_BRANCH = MBR.ID', [])
										->where('MB.BGROUP_ID IN (?)', $saveBgroup)
										->where('MBR.BRANCH_CODE = ?', $bgdata[0]['BG_BRANCH'])
										->query()->fetchAll();

									$tipePengajuan = ($bgdata[0]['IS_AMENDMENT'] == 0 || empty($bgdata[0]['IS_AMENDMENT'])) ? 'New' : 'Amandemen';
									$allSetting = $settings->getAllSetting();
									foreach ($getAllBuser as $buser) {
										$getEmailTemplate = $allSetting['bemailtemplate_6A'];
										$dataEmail = [
											'[[title_change_type]]' => 'VERIFIKASI',
											'[[master_bank_name]]' => $allSetting["master_bank_name"],
											'[[close_ref_number]]' => $data['CLOSE_REF_NUMBER'],
											'[[bg_number]]' => $data['BG_NUMBER'],
											'[[contract_name]]' => $underlyingForEmail,
											'[[usage_purpose]]' => ucwords(strtolower($data['USAGE_PURPOSE_DESC'])),
											'[[cust_name]]' => $selectcomp[0]['CUST_NAME'],
											'[[recipient_name]]' => $data['RECIPIENT_NAME'],
											'[[bg_amount]]' => $this->view->bgCurrency . ' ' . number_format($data['BG_AMOUNT'], 2),
											'[[counter_warranty_type]]' => $counterWarranty[$data['COUNTER_WARRANTY_TYPE']],
											'[[change_type]]' => $suggestion_type[$data['CHANGE_TYPE_CLOSE']],
											'[[suggestion_status]]' => $arrStatus[$dataclose['SUGGESTION_STATUS']],
											'[[repair_notes]]' => '-',
										];
										$getEmailTemplate = strtr($getEmailTemplate, $dataEmail);
										if ($bgdata[0]['CHANGE_TYPE_CLOSE'] != '5') {
											Application_Helper_Email::sendEmail($buser['BUSER_EMAIL'], strval($arrStatus[$dataclose['SUGGESTION_STATUS']] . ' BG NO. ' . $data['BG_NUMBER']), $getEmailTemplate);
										}
									}
								}

								// if (intval($bgdata[0]['COUNTER_WARRANTY_TYPE']) === 3) {
								// 	$getInsranch = $this->_db->select()
								// 		->from('M_INS_BRANCH', ['INS_BRANCH_NAME', 'INS_BRANCH_EMAIL'])
								// 		->where('INS_BRANCH_CODE = ?', $insBranchCode)
								// 		->query()->fetch();

								// 	$tipePengajuan = ($bgdata[0]['IS_AMENDMENT'] == 0 || empty($bgdata[0]['IS_AMENDMENT'])) ? 'New' : 'Amandemen';
								// 	$allSetting = $settings->getAllSetting();
								// 	$getEmailTemplate = $allSetting['femailtemplate_ijinprinsip_notif'];
								// 	$data = [
								// 		'[[nama_cabang_asuransi]]' => $getInsranch['INS_BRANCH_NAME'],
								// 		'[[user_name]]' => $buser['BUSER_NAME'],
								// 		'[[group_name]]' => $buser['BGROUP_DESC'],
								// 		'[[master_bank_name]]' => $allSetting["master_bank_name"],
								// 		'[[master_bank_app_name]]' => $allSetting["master_bank_app_name"],
								// 		'[[bg_reg_number]]' => $bgdata[0]["BG_REG_NUMBER"],
								// 		'[[bg_subject]]' => $bgdata[0]["BG_SUBJECT"],
								// 		// '[[recipient_name]]' => $bgdata[0]["RECIPIENT_NAME"],
								// 		'[[recipient_name]]' => $getNamaPemohon,
								// 		'[[is_amandment]]' => $tipePengajuan,
								// 		'[[bg_status]]' => $bgStatus[8],
								// 		'[[counter_warranty_type]]' => $counterWarranty[$bgdata[0]['COUNTER_WARRANTY_TYPE']],
								// 	];
								// 	$getEmailTemplate = strtr($getEmailTemplate, $data);
								// 	Application_Helper_Email::sendEmail($getInsranch['INS_BRANCH_EMAIL'], 'Permintaan Permohonan Ijin Prinsip', $getEmailTemplate);
								// }

								// SEND EMAIL TO OBLIGEE
								$getEmailTemplate = $allSetting['bemailtemplate_1A'];

								// TEMP BANK GUARANTEE UNDERLYING
								$getAllUnderlying = $this->_db->select()
									->from('T_BANK_GUARANTEE_UNDERLYING')
									->where('BG_REG_NUMBER = ?', $data['BG_REG_NUMBER'])
									->query()->fetchAll();

								$underlyingText = implode(', ', array_column($getAllUnderlying, 'DOC_NAME'));

								$underlyingForEmail = '
									<div style="display:inline-block;max-width: 394px;overflow:hidden;text-overflow: ellipsis;white-space: nowrap;">' . $underlyingText . '</div>
										<br>
									<div style="display:inline-block; margin-top: 5px">(Total : ' . count($getAllUnderlying) . ' Dokumen)</div>';

								$dataEmail = [
									'[[recipient_cp]]' => $bgdata[0]['RECIPIENT_CP'],
									'[[recipient_name]]' => $bgdata[0]['RECIPIENT_NAME'],
									'[[bg_number]]' => $bgdata[0]['BG_NUMBER'],
									'[[contract_name]]' => $underlyingForEmail,
									'[[usage_purpose]]' => ucwords(strtolower($bgdata[0]['USAGE_PURPOSE_DESC'])),
									'[[cust_name]]' => $selectcomp[0]['CUST_NAME'],
									'[[recipient_name]]' => $bgdata[0]['RECIPIENT_NAME'],
									'[[bg_amount]]' => $this->view->bgCurrency . ' ' . number_format($bgdata[0]['BG_AMOUNT'], 2),
									'[[close_ref_number]]' => $data['CLOSE_REF_NUMBER'],
									'[[master_bank_email]]' => $allSetting["master_bank_email"],
									'[[master_bank_telp]]' => $allSetting["master_bank_telp"],
									'[[master_app_name]]' => $allSetting["master_bank_app_name"],
									'[[master_bank_name]]' => $allSetting["master_bank_name"],
								];
								$getEmailTemplate = strtr($getEmailTemplate, $dataEmail);
								if ($bgdata[0]['CHANGE_TYPE_CLOSE'] != '5') {
									Application_Helper_Email::sendEmail($bgdata[0]['RECIPIENT_EMAIL'], 'PEMBERITAHUAN PENGAJUAN PENUTUPAN BANK GARANSI', $getEmailTemplate);
								}

								$this->_db->commit();

								// Simpan log aktifitas user ke table T_FACTIVITY
								if ($data['IS_AMENDMENT'] == '1') Application_Helper_General::writeLog('RLBG', 'Rilis Pengajuan Penutupan untuk BG No : ' . $data['BG_NUMBER'] . ', Subjek : ' . $data['BG_SUBJECT'] . ', NoRef Pengajuan : ' . $data['CLOSE_REF_NUMBER'] . '');
								if ($data['IS_AMENDMENT'] == '2') Application_Helper_General::writeLog('RLBG', 'Rilis Pengajuan Pelepasan Marginal Deposit untuk BG No : ' . $data['BG_NUMBER'] . ', Subjek : ' . $data['BG_SUBJECT'] . ', NoRef Pengajuan : ' . $data['CLOSE_REF_NUMBER'] . '');

								$this->setbackURL('/' . $this->_request->getModuleName() . '/release/');
								$this->_redirect('/notification/success/index');
							} else {
								$tokenFailed = $CustUser->setLogToken(); //log token activity
								$error = true;
								$errorMsg[] = $this->language->_('Invalid Response Code');  //$verToken['ResponseDesc'];
								$this->view->popauth = true;
								if ($tokenFailed === true) {
									$this->_redirect('/default/index/logout');
								}
							}
						}
						if ($error) {
							$this->view->popauth = true;
							$this->view->errorMsg = $errorMsg[0];
						}
					}

					if ($reject) {
						$notes = $this->_getParam('PS_REASON_REJECT');

						// Update data ke table TEMP_BANK_GUARANTEE_CLOSE
						$dataclose = [
							'SUGGESTION_STATUS' => '7',
							'LASTUPDATED' => new Zend_Db_Expr("now()"),
							'LASTUPDATEDFROM' => 'PRINCIPAL',
							'LASTUPDATEDBY' => $this->_userIdLogin,
						];
						$whereclose['CLOSE_REF_NUMBER = ?'] = $data['CLOSE_REF_NUMBER'];
						$this->_db->update('TEMP_BANK_GUARANTEE_CLOSE', $dataclose, $whereclose);

						// Insert data ke table T_BANK_GUARANTEE_HISTORY_CLOSE
						$historyInsert = [
							'DATE_TIME' => new Zend_Db_Expr("now()"),
							'CLOSE_REF_NUMBER' => $data['CLOSE_REF_NUMBER'],
							'BG_NUMBER' => $data['BG_NUMBER'],
							'CUST_ID' => $this->_custIdLogin,
							'USER_FROM' => 'PRINCIPAL',
							'USER_LOGIN' => $this->_userIdLogin,
							'BG_REASON' => $notes,
							'HISTORY_STATUS' => 7,
						];
						$this->_db->insert('T_BANK_GUARANTEE_HISTORY_CLOSE', $historyInsert);

						// Simpan log aktifitas user ke table T_FACTIVITY
						if ($data['IS_AMENDMENT'] == '1') Application_Helper_General::writeLog('RLBG', 'Penolakan Pengajuan Penutupan untuk BG No : ' . $data['BG_NUMBER'] . ', Subjek : ' . $data['BG_SUBJECT'] . ', NoRef Pengajuan : ' . $data['CLOSE_REF_NUMBER'] . '');
						if ($data['IS_AMENDMENT'] == '2') Application_Helper_General::writeLog('RLBG', 'Penolakan Pengajuan Pelepasan Marginal Deposit untuk BG No : ' . $data['BG_NUMBER'] . ', Subjek : ' . $data['BG_SUBJECT'] . ', NoRef Pengajuan : ' . $data['CLOSE_REF_NUMBER'] . '');

						$this->setbackURL('/' . $this->_request->getModuleName() . '/release/');
						$this->_redirect('/notification/success/index');
					}

					if ($repair) {
						$notes = $this->_getParam('PS_REASON_REPAIR');

						// Update data ke table TEMP_BANK_GUARANTEE_CLOSE
						$dataclose = [
							'SUGGESTION_STATUS' => '8',
							'LASTUPDATED' => new Zend_Db_Expr("now()"),
							'LASTUPDATEDFROM' => 'PRINCIPAL',
							'LASTUPDATEDBY' => $this->_userIdLogin,
						];
						$whereclose['CLOSE_REF_NUMBER = ?'] = $data['CLOSE_REF_NUMBER'];
						$this->_db->update('TEMP_BANK_GUARANTEE_CLOSE', $dataclose, $whereclose);

						// Insert data ke table T_BANK_GUARANTEE_HISTORY_CLOSE
						$historyInsert = [
							'DATE_TIME' => new Zend_Db_Expr("now()"),
							'CLOSE_REF_NUMBER' => $data['CLOSE_REF_NUMBER'],
							'BG_NUMBER' => $data['BG_NUMBER'],
							'CUST_ID' => $this->_custIdLogin,
							'USER_FROM' => 'PRINCIPAL',
							'USER_LOGIN' => $this->_userIdLogin,
							'BG_REASON' => $notes,
							'HISTORY_STATUS' => 5,
						];
						$this->_db->insert('T_BANK_GUARANTEE_HISTORY_CLOSE', $historyInsert);

						// Simpan log aktifitas user ke table T_FACTIVITY
						if ($data['IS_AMENDMENT'] == '1') Application_Helper_General::writeLog('RLBG', 'Permintaan Perbaikan Pengajuan Penutupan untuk BG No : ' . $data['BG_NUMBER'] . ', Subjek : ' . $data['BG_SUBJECT'] . ', NoRef Pengajuan : ' . $data['CLOSE_REF_NUMBER'] . '');
						if ($data['IS_AMENDMENT'] == '2') Application_Helper_General::writeLog('RLBG', 'Permintaan Perbaikan Pengajuan Pelepasan Marginal Deposit untuk BG No : ' . $data['BG_NUMBER'] . ', Subjek : ' . $data['BG_SUBJECT'] . ', NoRef Pengajuan : ' . $data['CLOSE_REF_NUMBER'] . '');

						$this->setbackURL('/' . $this->_request->getModuleName() . '/release/');
						$this->_redirect('/notification/success/index');
					}

					$back = $this->_getParam('back');
					if ($back) {
						$this->_redirect('/bgclosingworkflow/release');
					}
				}

				// Simpan log aktifitas user ke table T_FACTIVITY
				if ($data['IS_AMENDMENT'] == '1') Application_Helper_General::writeLog('RLBG', $this->language->_('Lihat Detail Permintaan Rilis Pengajuan Penutupan untuk BG No') . ' : ' . $data['BG_NUMBER'] . ', Subjek : ' . $data['BG_SUBJECT'] . ', NoRef Pengajuan : ' . $data['CLOSE_REF_NUMBER']);
				if ($data['IS_AMENDMENT'] == '2') Application_Helper_General::writeLog('RLBG', $this->language->_('Lihat Detail Permintaan Rilis Pelepasan Marginal Deposit untuk BG No') . ' : ' . $data['BG_NUMBER'] . ', Subjek : ' . $data['BG_SUBJECT'] . ', NoRef Pengajuan : ' . $data['CLOSE_REF_NUMBER']);
			}
		}
	}


	public function findPolicyBoundary($transfertype, $amount)
	{
		$selectuser	= $this->_db->select()
			->from(array('C' => 'M_APP_BOUNDARY'), array(
				'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
				'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
				'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
				'C.TRANSFER_TYPE',
				'C.POLICY'
			))
			->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
			->where("C.BOUNDARY_MIN 	<= ?", $amount)
			->where("C.BOUNDARY_MAX 	>= ?", $amount);

		$datauser = $this->_db->fetchAll($selectuser);
		return $datauser[0]['POLICY'];
	}

	//return tombol jika blm ada yg approve
	public function findUserBoundary($transfertype, $amount)
	{
		$selectuser	= $this->_db->select()
			->from(array('C' => 'M_APP_BOUNDARY'), array(
				'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
				'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
				'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
				'C.TRANSFER_TYPE',
				'C.POLICY'
			))
			->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
			->where("C.BOUNDARY_MIN 	<= ?", $amount)
			->where("C.BOUNDARY_MAX 	>= ?", $amount);

		$datauser = $this->_db->fetchAll($selectuser);

		$command = str_replace('(', '', $datauser[0]['POLICY']);
		$command = str_replace(')', '', $command);
		$command = $command . ' SG';
		$list = explode(' ', $command);

		$alphabet = array('01' => 'A', '02' => 'B', '03' => 'C', '04' => 'D', '05' => 'E', '06' => 'F', '07' => 'G', '08' => 'H', '09' => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z', 27 => 'SG');

		$flipAlphabet = array_flip($alphabet);

		foreach ($list as $row => $data) {
			foreach ($alphabet as $key => $value) {
				if ($data == $value) {
					$groupuser[] = $flipAlphabet[$data];
				}
			}
		}

		$uniqueGroupUser = array_unique($groupuser);

		foreach ($uniqueGroupUser as $key => $value) {
			if ($value == '27') {
				$selectGroupName	= $this->_db->select()
					->from(array('C' => 'M_APP_GROUP_USER'), array('C.GROUP_NAME'))
					->where("C.GROUP_USER_ID LIKE ?", '%S_' . $this->_custIdLogin . '%');
			} else {
				$selectGroupName	= $this->_db->select()
					->from(array('C' => 'M_APP_GROUP_USER'), array('C.GROUP_NAME'))
					->where("C.GROUP_USER_ID LIKE ?", '%' . $this->_custIdLogin . '_' . $value . '%');
			}

			$groupNameList = $this->_db->fetchAll($selectGroupName);

			array_unique($groupNameList[0]);

			$uniqueGroupName[$value] = $groupNameList[0]['GROUP_NAME'];
		}

		foreach ($uniqueGroupName as $row => $data) {
			foreach ($alphabet as $key => $value) {
				if ($row == $key) {
					$newUniqueGroupName[$value] = $data;
				}
			}
		}

		foreach ($groupuser as $key => $value) {

			//if special group
			if ($value == 27) {
				$likecondition = "S_%";
			} else {
				$likecondition = "%" . $this->_custIdLogin . "_" . $value . "%";
			}

			$selectgroup = $this->_db->select()
				->from(array('C' => 'M_APP_GROUP_USER'), array(
					'USER_ID'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.GROUP_USER_ID LIKE ?", (string) $likecondition);

			$group_user = $this->_db->fetchAll($selectgroup);

			$groups[][$alphabet[$value]] = $group_user;
		}
		//	echo '<pre>';
		//var_dump($groups);
		$tempGroup = array();
		foreach ($groups as $key => $value) {

			foreach ($value as $data => $values) {

				foreach ($values as $row => $val) {
					$userid = $val['USER_ID'];

					$selectusername = $this->_db->select()
						->from(array('M_USER'), array(
							'*'
						))
						->where("CUST_ID = ?", (string) $this->_custIdLogin)
						->where("USER_ID = ?", (string) $userid);
					//echo $selectusername;echo ' ';
					$username = $this->_db->fetchAll($selectusername);

					if (!in_array($data, $tempGroup)) {
						$userlist[$data][] = $username[0]['USER_FULLNAME'];
					}
				}

				array_push($tempGroup, $data);

				// $approverbtn[][$data] = '<button class="btnCircleGroup hovertext" style="margin-right: 1%; margin-top: 12%;" disabled>'.$data.'
				// 	<span class="hovertextcontent" style="padding-left: 15px;">'.$userlist.'</span></button>';
			}
		}

		$userlist['GROUP_NAME'] = $newUniqueGroupName;
		return $userlist;
	}

	public function findUserPrivi($privID)
	{
		$selectuser	= $this->_db->select()
			->from(array('A' => 'M_FPRIVI_USER'))
			->joinLeft(array('B' => 'M_USER'), 'A.FUSER_ID = CONCAT(B.CUST_ID,B.USER_ID)', array('USER_FULLNAME'))
			->where("A.FPRIVI_ID 	= ?", (string) $privID)
			->where("B.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->order('B.USER_FULLNAME ASC');

		//echo $selectuser;die();
		return $datauser = $this->_db->fetchAll($selectuser);
	}

	public function sendtokenAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$numb = $this->_getParam('id');
		$userId = $this->_userIdLogin;

		$token = SGO_Helper_GeneralFunction::generateToken($userId);

		$selectQuery = "SELECT USER_EMAIL FROM M_USER
                 WHERE CUST_ID = " . $this->_db->quote($this->_custIdLogin) . " AND USER_ID = " . $this->_db->quote($this->_userIdLogin) . " ";
		$userData =  $this->_db->fetchAll($selectQuery);
		$userData = $userData['0'];

		$bgdata = $this->_db->select()
			->from(array('A' => 'T_BANK_GUARANTEE'), array('*', 'IS_AMENDMENT' => 'A.CHANGE_TYPE', 'BG_REG' => 'A.BG_REG_NUMBER'))
			->join(array('D' => 'TEMP_BANK_GUARANTEE_CLOSE'), 'A.BG_NUMBER = D.BG_NUMBER')
			->where('A.CUST_ID = ?', $this->_custIdLogin)
			->where('A.BG_REG_NUMBER = ?', $numb)
			->where('D.SUGGESTION_STATUS IN (?)', ['3'])
			->query()->fetchAll();
		$data = $bgdata['0'];

		$bgdatadetail = $this->_db->select()
			->from(array('A' => 'T_BANK_GUARANTEE_DETAIL'), array('*'))
			->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
			->where('A.BG_REG_NUMBER = ?', $numb)
			->query()->fetchAll();

		if (!empty($bgdatadetail)) {
			foreach ($bgdatadetail as $key => $value) {
				if ($value['PS_FIELDNAME'] == 'Currency') {
					$currency['currency'] = $value['PS_FIELDVALUE'];
				}
			}
		}

		$Settings = new Settings();
		$allSetting = $Settings->getAllSetting();
		$EmailLoginNotif = $allSetting['femailtemplate_token'];
		$templateEmailMasterBankName = $allSetting['master_bank_name'];
		$templateEmailMasterBankAppName = $allSetting['master_bank_app_name'];
		$templateEmailMasterBankEmail = $allSetting['master_bank_email'];
		$templateEmailMasterBankTelp = $allSetting['master_bank_telp'];
		$templateEmailMasterBankWapp = $allSetting['master_bank_wapp'];

		$getOtpTrx = $this->_db->select()
			->from("M_SETTING")
			->where("SETTING_ID = ?", "otp_exp_trx");
		$getOtpTrx = $this->_db->fetchRow($getOtpTrx);

		$timeExpiredToken = new DateTime("now");
		$timeExpiredToken->add(new DateInterval("PT" . strval($getOtpTrx["SETTING_VALUE"]) . "M"));
		$stamp = $timeExpiredToken->format('Y-m-d H:i:s');
		$timeExpiredToken = $timeExpiredToken->format("H:i:s");

		$listtoken = "SELECT * FROM M_LIST_TOKEN WHERE BG_REG_NUMBER = '" . $data['BG_REG'] . "' AND CUST_ID = " . $this->_db->quote($this->_custIdLogin) . " AND USER_ID = " . $this->_db->quote($this->_userIdLogin) . " ";
		$listtokenData =  $this->_db->fetchAll($listtoken);

		if ($listtokenData) {
			$this->_db->delete("M_LIST_TOKEN", "M_LIST_TOKEN.BG_REG_NUMBER = '" . $data['BG_REG'] . "' AND CUST_ID = '" . $this->_custIdLogin . "' AND USER_ID = '" . $this->_userIdLogin . "'");
		}

		$encryption = new SGO_Helper_Encryption();
		$encryptToken = $encryption->encrypt($token);

		$listToken = array(
			'EXPIRY_TIMESTAMP' => $stamp,
			'BG_REG_NUMBER' => $data['BG_REG'],
			'CUST_ID' => $this->_custIdLogin,
			'USER_ID' => $this->_userIdLogin,
			'TOKEN' => $encryptToken,
		);

		$this->_db->insert('M_LIST_TOKEN', $listToken);

		$datatemp = array(
			'[[comp_code]]' => $this->_custIdLogin,
			'[[user_login]]' => $this->_userIdLogin,
			'[[register_number]]' => $data['BG_REG'],
			'[[token]]' => $token,
			'[[timeExpiredToken]]' => $timeExpiredToken,
			'[[dateExpired]]' => date("d-M-Y"),
			'[[cust_name]]' => $this->_custNameLogin,
			'[[subject]]' => $data['BG_SUBJECT'],
			'[[currency]]' => $currency['currency'],
			'[[amount]]' => number_format($data['BG_AMOUNT']),
			'[[master_bank_name]]' => $templateEmailMasterBankName,
			'[[master_bank_app_name]]' => $templateEmailMasterBankAppName,
			'[[master_bank_email]]' => $templateEmailMasterBankEmail,
			'[[master_bank_telp]]' 	=> $templateEmailMasterBankTelp,
			'[[master_bank_wapp]]' 	=> $templateEmailMasterBankWapp
		);

		$EmailLoginNotif = strtr($EmailLoginNotif, $datatemp);
		if (!empty($userData['USER_EMAIL'])) {
			Application_Helper_Email::sendEmail($userData['USER_EMAIL'], 'Token Notification', $EmailLoginNotif);
		}
		return true;
	}

	public function validatetokenAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$userId = $this->_getParam('userid');
		$custId = $this->_getParam('custid');
		$bgregnum = $this->_getParam('id');

		$inputtoken1 = $this->_getParam('inputtoken1');
		$inputtoken2 = $this->_getParam('inputtoken2');
		$inputtoken3 = $this->_getParam('inputtoken3');
		$inputtoken4 = $this->_getParam('inputtoken4');
		$inputtoken5 = $this->_getParam('inputtoken5');
		$inputtoken6 = $this->_getParam('inputtoken6');

		$responseCode = $inputtoken1 . $inputtoken2 . $inputtoken3 . $inputtoken4 . $inputtoken5 . $inputtoken6;

		$filter = new Application_Filtering();
		$responseCode = $filter->filter($responseCode, "SELECTION");
		$result = SGO_Helper_GeneralFunction::validateToken($bgregnum, $custId, $userId, $responseCode);
		$return = array();
		if ($result == "tokenSuccess") {
			$return['return'] = "tokenSuccess";
		} else {
			$settingObj = new Settings();
			$maxtoken = $settingObj->getSetting("max_failed_token");

			$selectQuery = "SELECT GOOGLE_CODE,USER_FAILEDTOKEN FROM M_USER
					 WHERE CUST_ID = " . $this->_db->quote($custId) . " AND USER_ID = " . $this->_db->quote($userId) . "";
			$usergoogleAuth =  $this->_db->fetchAll($selectQuery);

			$tokensubmit = ($usergoogleAuth['0']['USER_FAILEDTOKEN']) + 1;
			$tokenfail = (int)$maxtoken - (int)($usergoogleAuth['0']['USER_FAILEDTOKEN'] + 1);

			$this->_db->beginTransaction();
			$datatoken = array(
				'USER_FAILEDTOKEN' => $tokensubmit
			);

			$wheretoken =  array();
			$wheretoken['USER_ID = ?'] = $userId;
			$wheretoken['CUST_ID = ?'] = $custId;
			$this->_db->update('M_USER', $datatoken, $wheretoken);
			$this->_db->commit();

			$return['return'] = "tokenFailed";
			$return['tokenfail'] = $tokenfail;

			if ($tokensubmit == $maxtoken) {
				$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
				$tokenFailed = $CustUser->setLogToken(); //log token activity
				$return['redirect'] = true;
			}
		}
		echo json_encode($return);
	}

	public function checktypebglfAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$get_offer_type = $this->_request->getParam("offertype");
		$warranty_type = $this->_request->getParam("warranty_type");

		if ($warranty_type == 2) {
			$cust_id = $this->_request->getParam("cust_id");

			$check_grup_bumn = $this->_db->select()
				->from("M_CUSTOMER")
				->where("CUST_STATUS = 1")
				->where("CUST_ID = ?", $cust_id)
				->query()->fetchAll();

			$check_charges_bg = $this->_db->select()
				->from("M_CHARGES_BG")
				->where("CHARGES_ID LIKE ? ", '' . ($check_grup_bumn[0]['GRUP_BUMN'] == 1 ? 'LFBUMN' : 'LFNONBUMN') . '%')
				->where("CHARGES_NAME LIKE ?", '' . 'LF ' . $get_offer_type . '%')
				->query()->fetchAll();

			$check_lf_detail = $this->_db->select()
				->from("M_CUST_LINEFACILITY_DETAIL")
				->where("OFFER_TYPE = ?", $get_offer_type)
				//->where("FLAG = 1")
				->where("GRUP_BUMN = ?", $check_grup_bumn[0]['GRUP_BUMN'])
				->where("CUST_ID = ?", $cust_id)
				->query()->fetchAll();
			$check_lf_detail[0]['ACTIVE'] = $check_charges_bg[0]['ACTIVE'];
		}

		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($check_lf_detail[0]);
	}

	public function checktypebginsAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$get_offer_type = $this->_request->getParam("offertype");
		$warranty_type = $this->_request->getParam("warranty_type");

		if ($warranty_type == 3) {
			$cust_id = $this->_request->getParam("cust_id");

			$check_grup_bumn = $this->_db->select()
				->from("M_CUSTOMER")
				->where("CUST_STATUS = 1")
				->where("CUST_ID = ?", $cust_id)
				->query()->fetchAll();

			$check_charges_bg = $this->_db->select()
				->from("M_CHARGES_BG")
				->where("CHARGES_ID LIKE ? ", '' . ($check_grup_bumn[0]['GRUP_BUMN'] == 1 ? 'INSBUMN' : 'INSNONBUMN') . '%')
				->where("CHARGES_NAME LIKE ?", '' . 'ASURANSI ' . $get_offer_type . '%')
				->query()->fetchAll();

			$check_lf_detail = $this->_db->select()
				->from("M_CUST_LINEFACILITY_DETAIL")
				->where("OFFER_TYPE = ?", $get_offer_type)
				//->where("FLAG = 1")
				->where("GRUP_BUMN = ?", $check_grup_bumn[0]['GRUP_BUMN'])
				->where("CUST_ID = ?", $cust_id)
				->query()->fetchAll();
			$check_lf_detail[0]['ACTIVE'] = $check_charges_bg[0]['ACTIVE'];
		}

		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($check_lf_detail[0]);
	}

	public function inquirymarginaldepositAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$regNumber = $this->_getParam('reqNumber');

		$optHtml = '<table class="table table-sm mb-1" style="font-size: 1.2em;" id="guarantedTransactions">
						<thead style="background-color: #c0c2c4;">
							<td><i>' . $this->language->_('Hold Seq') . '</i></td>
							<td><i>' . $this->language->_('Nomor Rekening') . '</i></td>
							<td><i>' . $this->language->_('Nama Rekening') . '</i></td>
							<td><i>' . $this->language->_('Tipe') . '</i></td>
							<td><i>' . $this->language->_('Valuta') . '</i></td>
							<td><i>' . $this->language->_('Nominal') . '</i></td>
						</thead>
						<tbody>';

		$bgdatasplit = $this->_db->select()
			->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
			->joinLeft(['B' => 'M_CUSTOMER_ACCT'], 'A.ACCT = B.ACCT_NO AND B.CUST_ID = ' . $this->_db->quote($this->_custIdLogin) . '', ["M_ACCT_DESC" => "B.ACCT_DESC", "M_ACCT_TYPE" => "B.ACCT_TYPE", 'CUST_ID', 'CCY_ID'])
			->where('A.BG_REG_NUMBER = ?', $regNumber)
			->query()->fetchAll();

		if (!empty($bgdatasplit)) {
			foreach ($bgdatasplit as $key => $row) {
				if (strtolower($row["M_ACCT_DESC"]) == "giro" || $row["M_ACCT_TYPE"] == "D" || strtolower($row["ACCT_DESC"]) == "giro" || strtolower($row["ACCT_TYPE"]) == "giro") continue;
				// GET SERVICE 
				$serviceAccount = new Service_Account($row['ACCT'], null);
				$accountInfo = $serviceAccount->inquiryAccontInfo();
				$serviceCif = new Service_Account(null, null, null, null, null, $accountInfo["cif"]);
				$saveResult = $serviceCif->inquiryCIFAccount()["accounts"];
				$listAccount = array_search($row['ACCT'], array_column($saveResult, "account_number"));

				$row['HOLD_SEQUENCE'] ? $holdSequence = $row['HOLD_SEQUENCE'] : $holdSequence = '-';
				$optHtml .= '<tr class="table-secondary">
					<td>' . $holdSequence . '</td>
					<td>' . $row['ACCT'] . '</td>
					<td>' . $row['NAME'] . '</td>
					<td>' . $saveResult[$listAccount]["type_desc"] . " " . $saveResult[$listAccount]["product_type"] . '</td>
					<td>' . $saveResult[$listAccount]["currency"] . '</td>
					<td>' . Application_Helper_General::displayMoney($row['AMOUNT']) . '</td>
				</tr>';
			}
		} else {
			$optHtml .= '<tr><td colspan="7" class="tbl-evencontent" align="center">--- ' . $this->language->_('No Data') . ' ---</td></tr>';
		}
		$optHtml .= '</tbody></table>';
		$optHtml .= '<p style="font-size: 1.2em;" class="mb-1"><font class="text-danger">*</font><i>' . $this->language->_("Rekening Tipe Deposito dan Saving akan dilakukan pelepasan dana ditahan (tanpa pemindahan dana)") . '</i></p>';
		echo $optHtml;
	}

	public function inquiryaccountinfoAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();
		$app = Zend_Registry::get('config');
		$app = $app['app']['bankcode'];
		$acct_no = $this->_getParam('acct_no');
		$svcAccount = new Service_Account($acct_no, Application_Helper_General::getCurrNum('IDR'), $app, '1');
		$result = $svcAccount->inquiryAccontInfo('AI', TRUE);
		$data['data'] = false;
		is_array($result) ? $result :  $result = $data;
		echo json_encode($result);
	}

	public function checkvalidasiAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();
		$bgNumber = $this->_getParam('bgNumber');

		$data = $this->_db->select()
			->from('TEMP_BANK_GUARANTEE_CLOSE')
			->where('BG_NUMBER = ?', $bgNumber)
			->where('CHANGE_TYPE = 3')
			->where('SUGGESTION_STATUS NOT IN (7,11)')
			->query()->fetch();

		if (!empty($data)) {
			$return['status'] = true;
		} else {
			$return['status'] = false;
		}

		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($return);
	}

	public function showfinaldocAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		// decrypt numb

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$this->view->token = $sessionNamespace->token;

		$AESMYSQL = new Crypt_AESMYSQL();

		$BG_NUMBER     = urldecode($this->_getParam('bgnumb'));

		$BG_NUMBER = $AESMYSQL->decrypt($BG_NUMBER, $password);

		$file = $this->_request->getParam("file");

		$get_document_id = $this->_db->select()
			->from("T_BANK_GUARANTEE", ["DOCUMENT_ID"])
			->where("BG_NUMBER = ?", $BG_NUMBER)
			->query()->fetch();

		$get_file_name = $get_document_id["DOCUMENT_ID"] . ".pdf";

		header("Content-type: application/pdf");
		header("Content-Disposition: inline; filename=" . $get_file_name);
		// @readfile('/app/bg/library/data/uploads/document/submit/' . $get_file_name);
		@readfile(UPLOAD_PATH . '/document/submit/' . $get_file_name);
	}

	public function showcertificateAction()
	{
		$this->_helper->layout()->disableLayout();

		$AESMYSQL = new Crypt_AESMYSQL();
		$bgnumbDecode = urldecode($this->_getParam('bgnumb'));
		$bgnumbDecrypt = $AESMYSQL->decrypt($bgnumbDecode);

		$this->view->bgnumbDecode = $bgnumbDecode;

		$getDocId = $this->_db->select()
			->from('T_BANK_GUARANTEE', ['DOCUMENT_ID', 'BG_NUMBER'])
			->where('BG_NUMBER = ?', $bgnumbDecrypt)
			->query()->fetch();

		$this->view->bgNumb = $getDocId['BG_NUMBER'];

		$this->render('showcertificate');
	}

	public function showcertAction()
	{
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);

		$AESMYSQL = new Crypt_AESMYSQL();
		$bgnumbDecode = urldecode($this->_getParam('bgnumb'));
		$bgnumbDecrypt = $AESMYSQL->decrypt($bgnumbDecode);

		if (!$bgnumbDecrypt) {
			header('Content-type: application/json');
			echo json_encode(['message' => 'file tidak ditemukan']);
			return false;
		}

		$getDocId = $this->_db->select()
			->from('T_BANK_GUARANTEE', ['DOCUMENT_ID', 'BG_NUMBER'])
			->where('BG_NUMBER = ?', $bgnumbDecrypt)
			->query()->fetch();

		$pathFile = UPLOAD_PATH . '/document/submit/';
		$getFile = $pathFile . '/' . $getDocId['DOCUMENT_ID'] . '.pdf';

		if (!file_exists($getFile)) {
			header('Content-type: application/json');
			echo json_encode(['message' => 'file tidak ditemukan']);
			return false;
		}

		header("Content-type: application/pdf");
		header("Content-Disposition: inline; filename=BG_" . $getDocId['BG_NUMBER'] . '.pdf' . "");

		readfile($getFile);
		return true;
	}

	public function downloadcertificateAction()
	{
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);

		$AESMYSQL = new Crypt_AESMYSQL();
		$bgnumbDecode = urldecode($this->_getParam('bgnumb'));
		$bgnumbDecrypt = $AESMYSQL->decrypt($bgnumbDecode);

		if (!$bgnumbDecrypt) {
			header('Content-type: application/json');
			echo json_encode(['message' => 'file tidak ditemukan']);
			return false;
		}

		$getDocId = $this->_db->select()
			->from('T_BANK_GUARANTEE', ['DOCUMENT_ID', 'BG_NUMBER'])
			->where('BG_NUMBER = ?', $bgnumbDecrypt)
			->query()->fetch();

		$pathFile = UPLOAD_PATH . '/document/submit/';
		$getFile = $pathFile . '/' . $getDocId['DOCUMENT_ID'] . '.pdf';

		if (!file_exists($getFile)) {
			header('Content-type: application/json');
			echo json_encode(['message' => 'file tidak ditemukan']);
			return false;
		}

		header("Content-type: application/pdf");
		header("Content-Disposition: attachtment; filename=BG_" . $getDocId['BG_NUMBER'] . '.pdf' . "");

		readfile($getFile);
		return true;
	}
}
