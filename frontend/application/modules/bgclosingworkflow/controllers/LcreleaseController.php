<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class eformworkflow_LcreleaseController extends Application_Main 
{

    protected $_moduleDB = 'RTF'; // masih harus diganti
    
    public function indexAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->systemType = $system_type;

        $Settings = new Settings();

        $toc = $Settings->getSetting('ftemplate_bg');
        $this->view->toc = $toc;

        $conf = Zend_Registry::get('config');
        $this->_bankName = $conf['app']['bankname'];
        $this->view->masterbankname = $this->_bankName;

        $this->CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
        
        $selectQuery    = "SELECT GOOGLE_CODE,USER_FAILEDTOKEN FROM M_USER
                 WHERE CUST_ID = " . $this->_db->quote($this->_custIdLogin) . " AND USER_ID = " . $this->_db->quote($this->_userIdLogin) . " AND LTRIM(RTRIM(GOOGLE_CODE)) <> ''";
        // echo $selectQuery;
        $usergoogleAuth =  $this->_db->fetchAll($selectQuery);

        // var_dump($usergoogleAuth);die;
        if (!empty($usergoogleAuth)) {
            $this->view->googleauth = true;
            //var_dump($usergoogleAuth['0']['USER_FAILEDTOKEN']);
            $settingObj = new Settings();
            $maxtoken = $settingObj->getSetting("max_failed_token");
            $this->view->tokenfail = (int)$maxtoken-1;
            if($usergoogleAuth['0']['USER_FAILEDTOKEN'] != '0'){
                //die;
                $this->view->failedtoken = $usergoogleAuth['0']['USER_FAILEDTOKEN'];
                
                
                
                $tokenfail = (int)$maxtoken - (int)($usergoogleAuth['0']['USER_FAILEDTOKEN']+1);
                $this->view->tokenfail = $tokenfail;
            }
            if ($release) {
                $step = $this->_getParam('step');
            } else {
                $step = 3;
            }
        }
        else{
            $this->view->nogoauth = '1';
            if ($release) {
                $step = $this->_getParam('step');
            } else {
                $step = 3;
            }
        }

        

        $this->view->googleauth = true;

        $selectcomp = $this->_db->select()
                             ->from(array('A' => 'M_CUSTOMER'),array('*'))
                           //  ->joinLeft(array('B' => 'M_COUNTRY'),'A.COUNTRY_CODE = B.COUNTRY_CODE',array('COUNTRY_NAME'))
                             ->where('A.CUST_ID ='.$this->_db->quote((string)$this->_custIdLogin))
                             ->query()->fetchAll();

        $this->view->compinfo = $selectcomp[0];
        
        $complist = $this->_db->fetchAll(
            $this->_db->select()
                 ->from(array('A' => 'M_USER'),array('CUST_ID'))
               
                 ->where("A.USER_ID = ? ", $this->_userIdLogin)
        );	
        // echo $complist;;die;
        // var_dump($complist);die;
        $comp = "'";
        // print_r($complist);die;
        foreach ($complist as $key => $value) {
            $comp .= "','".$value['CUST_ID']."','";
        }
        $comp .= "'";

        $acctlist = $this->_db->fetchAll(
            $this->_db->select()
                    ->from(array('A' => 'M_APIKEY'))
                    ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
                    ->join(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME', 'B.BANK_CODE'))
                    // ->where('A.ACCT_STATUS = ?','5')
                    ->where("A.CUST_ID IN (".$comp.")")
                    ->order('A.APIKEY_ID ASC')
                    // echo $acctlist;
        );

        $listbankArr = array();
        foreach ($acctlist as $key => $value) {
            $listbank = array(
                                "BANK_NAME" => $value['BANK_NAME'],
                                "BANK_CODE" => $value['BANK_CODE']
            );
            array_push($listbankArr, $listbank);
        }

        $listbankArr = array_unique($listbankArr, SORT_REGULAR);
        $this->view->listBank = $listbankArr;

        // echo "<pre>";
        // var_dump($acctlist);

        $account = array();
		foreach ($acctlist as $key => $value) {
			$account[$value['ID']][$value['FIELD']] = $value['VALUE'];
			$account[$value['ID']]['BANK_CODE'] = $value['BANK_CODE'];
			$account[$value['ID']]['SENDER_ID'] = $value['SENDER_ID'];
			$account[$value['ID']]['AUTH_USER'] = $value['AUTH_USER'];
			$account[$value['ID']]['AUTH_PASS'] = $value['AUTH_PASS'];
			$account[$value['ID']]['BANK_NAME'] = $value['BANK_NAME'];
			$account[$value['ID']]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
		}
		// echo "<pre>";
		// var_dump($account);die;
		// $acct = array();
		$i = 0;
		foreach ($account as $key => $value) {

			$acct[$i]['ACCT_NO'] = $value['account_number'];
			$acct[$i]['ACCT_BANK'] = $value['BANK_NAME'];
			$acct[$i]['BANK_CODE'] = $value['BANK_CODE'];
			$acct[$i]['BANK_NAME'] = $value['BANK_NAME'];
			$acct[$i]['ACCT_NAME'] = $value['account_name'];
			$i++;
        }
    
        $this->view->sourceAcct = $acct;
        
        $accData = $this->_db->select()
					->from('M_CUSTOMER_ACCT')
					->where('CUST_ID IN ('.$comp.')');

        $accData = $this->_db->fetchAll($accData);
        foreach($accData as $key => $value){
            $accdata[$i]['ACCT_NO'] = $value['ACCT_NO'];
            $accdata[$i]['ACCT_NAME'] = $value['ACCT_NAME'];
            $accdata[$i]['ACCT_BANK'] = $this->_bankName;
        }
        $this->view->AccArr = $accdata;

        // $paramac = array(
		// 	'CCY_IN' => 'IDR'
		// );
		// $AccArr 	  				= $this->CustomerUser->getAccounts($paramac);
        // $this->view->AccArr 		= $AccArr;
        
        $numb = $this->_getParam('LC_NUMBER');

        if(!empty($numb)){

            $lcdata = $this->_db->select()
                             ->from(array('A' => 'T_LC'),array('*'))
                             ->where('A.LC_REG_NUMBER = ?',$numb)
                             ->query()->fetchAll();

            if(!empty($lcdata)){

                $data = $lcdata['0'];

                $this->view->expiryDate = Application_Helper_General::convertDate($data['LC_EXPDATE'],$this->view->viewDateFormat,$this->view->defaultDateFormat);
                $this->view->lastShipment = Application_Helper_General::convertDate($data['LC_LASTSHIP_DATE'],$this->view->viewDateFormat,$this->view->defaultDateFormat);
                
                $arrcredit_type = array(
                    1 => 'Letter of Credit',
                    2 => 'Surat Kredit Berdokumen Dalam Negeri'
                );
                
                $this->view->credit_type_text = $arrcredit_type[$data['LC_CREDIT_TYPE']];

                foreach($listbankArr as $key => $value){
                    $arrconfirming_bank[$value['BANK_CODE']] = $value['BANK_NAME'];
                }
                // var_dump($arrconfirming_bank);
                $this->view->confirming_bank_text = $arrconfirming_bank[$data['LC_CONFIRMING']];
                
                $arrtransferable = array(
                    1 => 'Transferable',
                    2 => 'Non Transferable'
                );
                
                $this->view->transferable_text = $arrtransferable[$data['LC_TRANSFERABLE']];
                
                $arrconfirmation = array(
                    1 => 'Without',
                    2 => 'May Add',
                    3 => 'Confirmed'
                );
                
                $this->view->confirmation_text = $arrconfirmation[$data['LC_CONFIRMATION']];
                
                $arradvBank = array(
                    1 => 'PHILLIP BANK - SWIFT: HDSBKHHPP',
                );
                
                $this->view->advBank_text = $arradvBank[$data['LC_ADVISING']];
                
                $arrlcBy = array(
                    1 => 'Negotation',
                    2 => 'Acceptance',
                    3 => 'Confirmed',
                    4 => 'Payment'
                );
                
                $this->view->lcBy_text = $arrlcBy[$data['LC_BY']];
                
                $arrdifered_payment = array(
                    1 => 'delivery of goods but not exceeding the expiry date',
                    2 => 'proof of delivery goods but not exceeding the expired date'
                );
                
                $this->view->difered_payment_text = $arrdifered_payment[$data['LC_DOC_AFTER']];
                
                $arrtrade_terms = array(
                    0 => '---',
                    1 => 'EXW',
                    2 => 'FCA',
                    3 => 'CPT',
                    4 => 'CIP',
                    5 => 'DAT',
                    6 => 'DAP',
                    7 => 'DDP',
                    8 => 'FAS',
                    9 => 'FOB',
                    10 => 'CFR',
                    11 => 'CIF'
                );
                
                $this->view->trade_terms_text = $arrtrade_terms[$data['LC_TRADEIN']];
                
                $arrtenor = array(
                    1 => 'Sight',
                    2 => 'Usance',
                    3 => 'Other'
                );
                
                $this->view->tenor_text = $arrtenor[$data['LC_TENOR']];
                
                $arrconfirmationCharge = array(
                    1 => 'Not Applicable'            
                );
                
                $this->view->confirmationCharge_text = $arrconfirmationCharge[$data['LC_CONFIRM']];
                
                $arrcharge = array(
                    1 => 'Applicant',
                    2 => 'Beneficiary',
                );
                
                $this->view->issuance_text = $arrcharge[$data['LC_ISSUANCE']];
                $this->view->discrepancy_text = $arrcharge[$data['LC_DISCREPANCY']];
                $this->view->chargesOutside_text = $arrcharge[$data['LC_CHARGESOUTSIDE']];

                $this->view->lc_number = $data['LC_REG_NUMBER'];
                $this->view->benef_name = $data['LC_BENEF_NAME'];
                $this->view->benef_address = $data['LC_BENEF_ADDRESS'];
                $this->view->bank_amount = Application_Helper_General::displayMoneyplain($data['LC_AMOUNT']);
                $this->view->ccyid = $data['LC_CCY'];
                $this->view->tolerance = $data['LC_TOLERANCE_TYPE'];
                $this->view->toleranceMax = Application_Helper_General::displayMoneyplain($data['LC_TOLERANCE_MAX']);
                $this->view->toleranceMin = Application_Helper_General::displayMoneyplain($data['LC_TOLERANCE_MIN']);
                $this->view->expiryPlace = $data['LC_EXP_PLACE'];
                $this->view->shipmentPeriod = $data['LC_SHIP_PERIOD'];
                $this->view->tenor = $data['LC_TENOR'];
                $this->view->partial_shipment = $data['LC_TENORPARTIAL'];
                $this->view->trans_shipment = $data['LC_TENORTRANSHIP'];
                $this->view->confirming_bank = $data['LC_CONFIRMING'];
                $this->view->placeCharge = $data['LC_PLACETALK'];
                $this->view->portLoading = $data['LC_PORTLOAD'];
                $this->view->placeDestination = $data['LC_PLACEFINAL'];
                $this->view->portofDischarge = $data['LC_PORTDISCHARGE'];
                $this->view->availablewith = $data['LC_AVAIL_TYPE'];
                $this->view->otherAvailable = $data['LC_AVAIL_OTHER'];
                $this->view->documentPeriod = $data['LC_DOC_PERIOD'];
                $this->view->descGoods = $data['LC_DESC_GOOD'];
                $this->view->contractNo = $data['LC_CONTACT'];
                $this->view->hsCode = $data['LC_HSCODE'];
                $this->view->countryOrigin = $data['LC_COUNTRY'];
                $this->view->goodsVolume = $data['LC_VOLUME'];
                $this->view->unitPrice = Application_Helper_General::displayMoneyplain($data['LC_UNITAMOUNT']);
                $this->view->ccyid2 = $data['LC_CCYUNIT'];
                $this->view->trade = $data['LC_TRADETYPE'];
                $this->view->trade_terms = $data['LC_TRADEIN'];
                $this->view->tradeOthers = $data['LC_TRADEOTHER'];
                $this->view->comment = $data['LC_ADD_CONDITION'];
                $this->view->chargetoAccount = $data['LC_CHARGEACCOUNT'];

                if(!empty($data['LC_DOCREQ'])){
                    $docreq = (explode(",",$data['LC_DOCREQ']));

                    foreach($docreq as $key => $val){
                        $str = 'checkp'.$val;
                        //var_dump($str);
                        $this->view->$str =  'checked';
                    }

                    $detaillc = $this->_db->select()
                             ->from(array('A' => 'T_LC_DETAIL'),array('*'))
                             ->where('A.LC_NUMBER = ?',$numb)
                             ->query()->fetchAll();

                    // var_dump($detaillc);

                    foreach($detaillc as $key => $val){
                        $str = 'docReq'.$val['DOCREQ_INDEX'];
                        $this->view->$str =  $val['DOCREQ_VAL'];
                    }
                    
                }

                $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

        
                    $param = array('CCY_IN' => 'IDR','ACCT_NO'=>$data['LC_CHARGEACCOUNT']);
                    $AccArr = $CustomerUser->getAccounts($param);
                
                    
                if(!empty($AccArr)){
                    $this->view->src_name = $AccArr['0']['ACCT_NAME'];
                }
                
                $download = $this->_getParam('download');
                              //print_r($edit);die;
                if($download){
                    $attahmentDestination = UPLOAD_PATH . '/document/submit/';
                    $this->_helper->download->file($data['FILE'],$attahmentDestination.$data['FILE']);    
                }

                if($this->_request->isPost() )
                {
                    $CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
                    $approve = $this->_getParam('release');
                    $LC_REG_NUMBER = $this->_getParam('lcnumb');
                    $reject = $this->_getParam('reject');
                    $repair = $this->_getParam('repair');
                    $filter             = new Application_Filtering();
                    $inputtoken1        = $this->_getParam('inputtoken1');
                    $inputtoken2        = $this->_getParam('inputtoken2');
                    $inputtoken3        = $this->_getParam('inputtoken3');
                    $inputtoken4        = $this->_getParam('inputtoken4');
                    $inputtoken5        = $this->_getParam('inputtoken5');
                    $inputtoken6        = $this->_getParam('inputtoken6');

                    $responseCode       = $inputtoken1 . $inputtoken2 . $inputtoken3 . $inputtoken4 . $inputtoken5 . $inputtoken6;

                    $responseCode       = $filter->filter($responseCode, "SELECTION");
                        //print_r($edit);die;
                        //var_dump($approve);die;
                    if($approve){
                        
                        if (!empty($usergoogleAuth)) {

                            $pga = new PHPGangsta_GoogleAuthenticator();
                            // var_dump($usergoogleAuth['0']['GOOGLE_CODE']);
                                //var_dump($responseCode);die;
                                $settingObj = new Settings();
                                $gduration = $settingObj->getSetting("google_duration");
                                if ($pga->verifyCode($usergoogleAuth['0']['GOOGLE_CODE'], $responseCode, $gduration)) {
                                $resultToken = $resHard['ResponseCode'] == '0000';
                                $datatoken = array(
                                                                    'USER_FAILEDTOKEN' => 0
                                                                );

                                                                $wheretoken =  array();
                                                                $wheretoken['USER_ID = ?'] = $this->_userIdLogin;
                                                                $wheretoken['CUST_ID = ?'] = $this->_custIdLogin;
                                                                $data = $this->_db->update('M_USER',$datatoken,$wheretoken);
                                
                                $data = array ('LC_STATUS' => '4');
                                $where['LC_REG_NUMBER = ?'] = $LC_REG_NUMBER;
                                $this->_db->update('T_LC',$data,$where);

                                // $notes = $this->_getParam('PS_REASON_REJECT');
                                // $historyInsert = array(
                                //     'DATE_TIME'         => new Zend_Db_Expr("now()"),
                                //     'LC_REG_NUMBER'         => $LC_REG_NUMBER,
                                //     'CUST_ID'           => $this->_custIdLogin,
                                //     'USER_LOGIN'        => $this->_userIdLogin,
                                //     'HISTORY_STATUS'    => 4
                                //     //  'BG_REASON'         => $notes,
                                // );

                                // $this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);

                                $this->setbackURL('/'.$this->_request->getModuleName().'/release/');
                                $this->_redirect('/notification/success/index');
                                                                
                                } else {
                                $tokenFailed = $CustUser->setLogToken(); //log token activity

                                $error = true;
                                $errorMsg[] = $this->language->_('Invalid Response Code');  //$verToken['ResponseDesc'];
                                $this->view->popauth = true;
                                if ($tokenFailed === true) {
                                    $this->_redirect('/default/index/logout');
                                }
                                }
                            
                            //$resultToken = $resHard['ResponseCode'] == '0000';
                        }
                        
                        if($error){
                                $this->view->popauth                = true;
                                //var_dump($errorMsg);
                                $this->view->errorMsg           = $errorMsg[0];
                        }
                        
                        
                    }
                    
                    if($reject){
                        $data = array ('LC_STATUS' => '11');
                        $where['LC_REG_NUMBER = ?'] = $LC_REG_NUMBER;
                        $this->_db->update('T_LC',$data,$where);



                        // $notes = $this->_getParam('PS_REASON_REJECT');
                        //     $historyInsert = array(
                        //             'DATE_TIME'         => new Zend_Db_Expr("now()"),
                        //             'LC_REG_NUMBER'         => $LC_REG_NUMBER,
                        //             'CUST_ID'           => $this->_custIdLogin,
                        //             'USER_LOGIN'        => $this->_userIdLogin,
                        //             'HISTORY_STATUS'    => 11,
                        //             'BG_REASON'         => $notes,
                        //         );

                        //         $this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);

                        $this->setbackURL('/'.$this->_request->getModuleName().'/release/');
                        $this->_redirect('/notification/success/index');
                        
                    }


                    if($repair){
                        //die('here');
                        $data = array ('LC_STATUS' => '10');
                        $where['LC_REG_NUMBER = ?'] = $LC_REG_NUMBER;
                        $this->_db->update('T_LC',$data,$where);

                            // $notes = $this->_getParam('PS_REASON_REPAIR');
                            // $historyInsert = array(
                            //         'DATE_TIME'         => new Zend_Db_Expr("now()"),
                            //         'LC_REG_NUMBER'         => $LC_REG_NUMBER,
                            //         'CUST_ID'           => $this->_custIdLogin,
                            //         'USER_LOGIN'        => $this->_userIdLogin,
                            //         'HISTORY_STATUS'    => 10,
                            //         'BG_REASON'         => $notes,
                            //     );

                            //     $this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);

                        $this->setbackURL('/'.$this->_request->getModuleName().'/approve/');
                        $this->_redirect('/notification/success/index');
                        
                    }


                    $back = $this->_getParam('back');
                    if($back){
                        $this->_redirect('/eformworkflow/release');
                    }
                    
                    
                }
                
            }
            
        }

    }
}