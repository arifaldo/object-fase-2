<?php


require_once 'Zend/Controller/Action.php';

class Viewownactivity_FrontEndController extends Application_Main
{
	/**
	 * The default action - show the home page
	 */
	//protected $_groupStatusCode  = array();

	// BackEnd Activity Log -> berisi daftar aktifitas yang dilakukan oleh BackEnd user.
	public function beActivityAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$setting = new Settings();
		$system_type    = $setting->getSetting('system_type');

		$activity = $this->_db->select()->distinct()
		->from(array('A' => 'M_FPRIVILEGE'), array('FPRIVI_ID', 'FPRIVI_DESC', 'FPRIVI_MODE'))
		->joinLeft(['MFP' => 'M_FPRIVI_USER'], 'A.FPRIVI_ID = MFP.FPRIVI_ID', [''])
		->order('FPRIVI_DESC ASC');

		$auth = Zend_Auth::getInstance()->getIdentity();

		if ($auth->userHeadQuarter == "NO") {
			$activity->where('MFP.FUSER_ID = ?', join([$auth->custIdLogin, $auth->userIdLogin])); // Add Bahri
		}

		$activity = $activity->query()->fetchAll();

		// $login = array('BPRIVI_ID' => 'BLGN', 'BPRIVI_DESC' => 'Login');
		// $logout = array('BPRIVI_ID' => 'BLGT', 'BPRIVI_DESC' => 'Logout');
		// $changepass = array('BPRIVI_ID' => 'CHOP', 'BPRIVI_DESC' => 'Change My Password');

		$login = array('FPRIVI_ID' => 'FLGN', 'FPRIVI_DESC' => 'Login');
		$logout = array('FPRIVI_ID' => 'FLGT', 'FPRIVI_DESC' => 'Logout');
		$changepass = array('FPRIVI_ID' => 'CHMP', 'FPRIVI_DESC' => 'Change My Password');
		$widget = array('FPRIVI_ID' => 'MWDG', 'FPRIVI_DESC' => 'Widget');
		$account = array('FPRIVI_ID' => 'MACC', 'FPRIVI_DESC' => 'Akun Saya');
		array_unshift($activity, $account);

		array_unshift($activity, $changepass);
		array_unshift($activity, $logout);
		array_unshift($activity, $login);
		array_unshift($activity, $widget);

		$tableDtl = array();
		foreach ($activity as $key => $value) {
			if ($system_type == 2) {

				if ($value['FPRIVI_MODE'] == '2' || $value['FPRIVI_MODE'] == '0') {
					$tableDtl[$key] = $value;
				}
			} else if ($system_type == 1) {

				if ($value['FPRIVI_MODE'] == '1' || $value['FPRIVI_MODE'] == '0') {
					$tableDtl[$key] = $value;
				}
			} else {

				if ($value['FPRIVI_MODE'] == '1' || $value['FPRIVI_MODE'] == '0' || $value['FPRIVI_MODE'] == '2') {
					$tableDtl[$key] = $value;
				} else {
					$tableDtl[$key] = $value;
				}
			}
		}

		$activityarr = Application_Helper_Array::listArray($tableDtl, 'FPRIVI_ID', 'FPRIVI_DESC');
		asort($activityarr);

		$this->view->activity = $activityarr;

		$fields = array(
			'datetime' 	   => array(
				'field'    => 'LOG_DATE',
				'label'    => $this->language->_('Date/Time'),
				'sortable' => true
			),
			'userID'       => array(
				'field'    => 'USER_ID',
				'label'    => $this->language->_('User ID'),
				'sortable' => true
			),
			// 'username' 	   => array('field'    => 'USER_NAME',
			//                 	    'label'    => $this->language->_('Name'),
			//                 	    'sortable' => true),
			'activity'		=> array(
				'field'    => 'ACTION_DESC',
				'label'    => $this->language->_('Activity Type'),
				'sortable' => true
			),
			'description'     => array(
				'field'    => 'ACTION_FULLDESC',
				'label'    => $this->language->_('Description'),
				'sortable' => true
			),
		);

		$fields1 = array(
			'datetime' 	   => array(
				'field'    => 'LOG_DATE',
				'label'    => $this->language->_('Date/Time'),
				'sortable' => true
			),
			'userID'       => array(
				'field'    => 'USER_ID',
				'label'    => $this->language->_('User ID'),
				'sortable' => true
			),
			'username' 	   => array(
				'field'    => 'USER_NAME',
				'label'    => $this->language->_('User Name'),
				'sortable' => true
			),
			'activity'		=> array(
				'field'    => 'ACTION_DESC',
				'label'    => $this->language->_('Activity Type'),
				'sortable' => true
			),
			'description'     => array(
				'field'    => 'ACTION_FULLDESC',
				'label'    => $this->language->_('Description'),
				'sortable' => true
			),
		);

		//get page, sortby, sortdir
		$page = $this->_getParam('page');
		//$sortBy = $this->_getParam('sortby');
		//$sortDir = $this->_getParam('sortdir');
		$sortBy  = $this->_getParam('sortby', 'datetime');
		$sortDir = $this->_getParam('sortdir', 'desc');

		$getCSV = $this->_getParam('csv');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page, 'Digits') && ($page > 0)) ? $page : 1;

		$sortBy = (Zend_Validate::is(
			$sortBy,
			'InArray',
			array(array_keys($fields))
		)) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is(
			$sortDir,
			'InArray',
			array('haystack' => array('asc', 'desc'))
		)) ? $sortDir : 'asc';

		//get filtering param
		$filters = array(
			'filter' 	   	=>  array('StringTrim', 'StripTags'),
			'fUserID'   	=>  array('StringTrim', 'StripTags', 'StringToUpper'),
			'fDateFrom' 	=>  array('StringTrim', 'StripTags'),
			'fDateTo'   	=>  array('StringTrim', 'StripTags'),
			'moduleDesc'   =>  array('StringTrim', 'StripTags', 'StringToUpper'),
			'actionDesc'   =>  array('StringTrim', 'StripTags', 'StringToUpper'),
			'keyValue'   	=>  array('StringTrim', 'StripTags', 'StringToUpper'),
		);

		$optVldtr   = array('breakChainOnFailure' => false);
		$validators = array(
			'filter'	  => array('allowEmpty' => true),
			'fUserID'	  => array('allowEmpty' => true),
			'fDateFrom' => array(
				new Zend_Validate_Date($this->_dateDisplayFormat),
				'allowEmpty' => true,
				'messages' => array("From date format must be '{$this->_dateDisplayFormat}'")
			),
			'fDateTo'   => array(
				new Zend_Validate_Date($this->_dateDisplayFormat),
				'allowEmpty' => true,
				'messages' => array("To date format must be '{$this->_dateDisplayFormat}'")
			),
			'moduleDesc' => array('allowEmpty' => true),
			'actionDesc' => array('allowEmpty' => true),
			'keyValue' 	 => array('allowEmpty' => true),
		);

		$zf_filter = new Zend_Filter_Input($filters, $validators, $this->getRequest()->getParams(), $optVldtr);

		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;

		$filter		 = ($zf_filter->filter)     ? $zf_filter->filter     : $this->_request->getParam('filter');

		$filter_clear = $this->_getParam('clearfilter');
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');

		if ($filter == TRUE) {
			$fUserID     = html_entity_decode(($zf_filter->fUserID)    ? $zf_filter->fUserID     : $this->_request->getParam('fUserID'));
			$fDateFrom 	 = html_entity_decode($zf_filter->getEscaped('fDateFrom'));
			$fDateTo     = html_entity_decode($zf_filter->getEscaped('fDateTo'));
			$active = $this->_request->getParam('SEARCH_ACTIVITY');

			$this->view->fUserID	 = $fUserID;
			$this->view->active = $active;
		}


		$select = $this->_db->select()
			->from(array('A' => 'T_FACTIVITY'), array('*'))
			->joinLeft(array('F' => 'M_FPRIVILEGE'), 'F.FPRIVI_ID = A.ACTION_DESC', array(
				'FPRIVI_DESC' => new Zend_Db_Expr("(CASE A.ACTION_DESC 
WHEN 'FLGN' THEN 'Login' 
WHEN 'FLGT' THEN 'Logout' 
WHEN 'CHMP' THEN 'Change My Password' 
WHEN 'MWDG' THEN 'Widget' 
WHEN 'MACC' THEN 'Akun Saya' 
WHEN 'RFPW' THEN 'Reset Forgot Password' 
ELSE F.FPRIVI_DESC 
END)"),
			))
			->where('A.USER_ID = ?', $this->_userIdLogin)
			->where('A.CUST_ID = ?', $this->_custIdLogin)
			->order('A.LOG_DATE DESC');

		if ($filter == null) {
			$fDateFrom = (date("d/m/Y"));
			$fDateTo = (date("d/m/Y"));
		}
		//die;

		$fDateFrommax = (date("d/m/Y", strtotime("-3 month")));
		$this->view->fDateFrommax 	 = $fDateFrommax;

		if ($filter_clear == TRUE) {
			$fDateFrom = '';
			$fDateTo = '';
			$this->view->fDateFrom   = $fDateFrom;
			$this->view->fDateTo 	 = $fDateTo;
		}

		if ($filter == TRUE) {
			$this->view->fDateFrom   = $fDateFrom;
			$this->view->fDateTo 	 = $fDateTo;
			if (!empty($fDateFrom)) {
				$FormatDate = new Zend_Date($fDateFrom, $this->_dateDisplayFormat);
				$fDateFrom  = $FormatDate->toString($this->_dateDBFormat);
			}

			if (!empty($fDateTo)) {
				$FormatDate = new Zend_Date($fDateTo, $this->_dateDisplayFormat);
				$fDateTo    = $FormatDate->toString($this->_dateDBFormat);
			}

			if (!empty($active)) {
				$select->where("A.ACTION_DESC = ? ", $active);
			}

			if (!empty($fDateFrom) && empty($fDateTo)) {
				$select->where("DATE(A.LOG_DATE) >= " . $this->_db->quote($fDateFrom));
			}

			if (empty($fDateFrom) && !empty($fDateTo)) {
				$select->where("DATE(A.LOG_DATE) <= " . $this->_db->quote($fDateTo));
			}

			if (!empty($fDateFrom) && !empty($fDateTo)) {
				$select->where("DATE(A.LOG_DATE) between " . $this->_db->quote($fDateFrom) . " and " . $this->_db->quote($fDateTo));
			}
		}
		//die;
		if ($filter == TRUE) {

			// var_dump($filter);die;
			//where clauses        	
			// if($fUserID)
			// $select->where("UPPER(A.USER_ID) LIKE ".$this->_db->quote('%'.$fUserID.'%'));

			// if($active)
			// $select->where("UPPER(A.ACTION_DESC) LIKE ".$this->_db->quote('%'.$active.'%')); 
			$active = $this->_request->getParam('SEARCH_ACTIVITY');

			if (!empty($fDateFrom)) {
				$FormatDate = new Zend_Date($fDateFrom, $this->_dateDisplayFormat);
				$fDateFrom  = $FormatDate->toString($this->_dateDBFormat);
			}

			if (!empty($fDateTo)) {
				$FormatDate = new Zend_Date($fDateTo, $this->_dateDisplayFormat);
				$fDateTo    = $FormatDate->toString($this->_dateDBFormat);
			}

			if (!empty($fDateFrom) && empty($fDateTo)) {
				$select->where("DATE(A.LOG_DATE) >= " . $this->_db->quote($fDateFrom));
			}

			if (empty($fDateFrom) && !empty($fDateTo)) {
				$select->where("DATE(A.LOG_DATE) <= " . $this->_db->quote($fDateTo));
			}

			if (!empty($fDateFrom) && !empty($fDateTo)) {
				$select->where("DATE(A.LOG_DATE) between " . $this->_db->quote($fDateFrom) . " and " . $this->_db->quote($fDateTo));
			}

			if (!empty($active)) {
				$select->where("A.ACTION_DESC = ? ", $active);
			}
		}
		// echo $select;die;
		$select->order($sortBy . ' ' . $sortDir);
		$data = $this->_db->fetchAll($select);

		// echo $select;die;
		// $select->order($sortBy.' '.$sortDir);
		// $data = $this->_db->fetchAll($select);


		$this->paging($select);
		$this->view->fields 	 = $fields;
		$this->view->filter 	 = $filter;
		//Zend_Debug::dump($data);die;

		if ($csv || $pdf || $this->_request->getParam('print')) {
			$header = array($this->language->_('Date/Time'), $this->language->_('User ID'), $this->language->_('User Name'), $this->language->_('Activity Type'), $this->language->_('Description'));

			foreach ($data as $key => $row) {
				$data[$key]['LOG_DATE'] = Application_Helper_General::convertDate($row['LOG_DATE'], $this->view->displayDateTimeFormat, $this->view->defaultDateFormat);
			}

			if ($csv) {

				// if($filter == TRUE){
				$selectcomp = $this->_db->select()
					->from('M_CUSTOMER', array('value' => 'CUST_ID', 'CUST_NAME'))
					->where('CUST_ID = ?', $this->_custIdLogin);

				$selectcomp = $this->_db->fetchAll($selectcomp);
				$headerData[] = $selectcomp[0]['CUST_NAME'];
				$newData[] = array('My Activity');

				if (!empty($fDateFrom) && !empty($fDateTo)) {
					$newData[] = array('From : ' . Application_Helper_General::convertDate($fDateFrom, 'dd/MM/yyyy') . ' to : ' . Application_Helper_General::convertDate($fDateTo, 'dd/MM/yyyy'));
				} else if (!empty($fDateFrom)) {
					$newData[] = array('From : ' . Application_Helper_General::convertDate($fDateFrom, 'dd/MM/yyyy') . ' to : -');
				}

				if ($filter == TRUE) {
					$newData[] = array('Activity Type : ' . $activityarr[$data[0]['ACTION_DESC']]);
				}

				if ($filter == null) {
					$newData[] = array('Activity Type : ');
				}

				$newData[] = array(' ');
				$newData[] = array($this->language->_('Date/Time'), $this->language->_('User ID'), $this->language->_('User Name'), $this->language->_('Activity Type'), $this->language->_('Description'));
				// }

				foreach ($data as $p => $pTrx) {
					$paramTrx = array(
						"LOG_DATE" => date("d-m-Y H:i:s", strtotime($pTrx['LOG_DATE'])),
						"USER_ID"  				=> $pTrx['USER_ID'],
						"USER_NAME" 			=> $pTrx['USER_NAME'],
						"ACTION_DESC"  			=> $pTrx['FPRIVI_DESC'],
						"ACTION_FULLDESC"  		=> $pTrx['ACTION_FULLDESC'],
					);
					$newData[] = $paramTrx;
				}

				$datenow = date("Ymd");
				$this->_helper->download->csv($headerData, $newData, null, 'Own Activity_' . $this->_userIdLogin . '_' . $datenow);
			} else if ($pdf) {
				$datenow = date("Ymd");
				$this->_helper->download->pdf($header, $data, null, 'Own Activity_' . $this->_userIdLogin . '_' . $datenow);
			} else if ($this->_request->getParam('print')) {

				$setting = new Settings();
				$master_bank_app_name = $setting->getSetting('master_bank_app_name');
				$master_bank_name = $setting->getSetting('master_bank_name');
				$masterbank = $master_bank_app_name . ' - ' . $master_bank_name;


				$selectcomp = $this->_db->select()
					->from('M_CUSTOMER', array('value' => 'CUST_ID', 'CUST_NAME'))
					->where('CUST_ID = ?', $this->_custIdLogin);

				$selectcomp = $this->_db->fetchAll($selectcomp);
				$newData[] = $selectcomp[0]['CUST_NAME'];
				$newData[] = 'My Activity';

				if (!empty($fDateFrom) && !empty($fDateTo)) {
					$newData[] = array('From : ' . Application_Helper_General::convertDate($fDateFrom, 'dd/MM/yyyy') . ' to : ' . Application_Helper_General::convertDate($fDateTo, 'dd/MM/yyyy'));
				} else if (!empty($fDateFrom)) {
					$newData[] = array('From : ' . Application_Helper_General::convertDate($fDateFrom, 'dd/MM/yyyy') . ' to : -');
				}

				// $newData[] = 'From : ' . Application_Helper_General::convertDate($fDateFrom, 'dd/MM/yyyy') . ' to : ' . Application_Helper_General::convertDate($fDateTo, 'dd/MM/yyyy');

				if ($filter == TRUE) {
					$newData[] = 'Activity Type : ' . $data[0]['FPRIVI_DESC'];
				}

				if ($filter == null) {
					$newData[] = 'Activity Type : ';
				}

				$newData[] = array(' ');

				$tempfilter = 1;

				$this->_forward('print', 'index', 'widget', array('data_content' => $data, 'data_caption' => $masterbank, 'data_header' => $fields1, 'data_filter' => $newData, 'filter' => $tempfilter));
			}
		}
		// end if if($zf_filter->isValid())
	}
}




// ==============================BACKUP===================
// <?php


// require_once 'Zend/Controller/Action.php';

// class Viewownactivity_FrontEndController extends Application_Main 
// {
// 	/**
// 	 * The default action - show the home page
// 	 */
// 	//protected $_groupStatusCode  = array();

// 	// BackEnd Activity Log -> berisi daftar aktifitas yang dilakukan oleh BackEnd user.
// 	public function beActivityAction() 
// 	{	
// 		$this->_helper->layout()->setLayout('newlayout');

// 		$setting = new Settings();
//         $system_type    = $setting->getSetting('system_type');

// 		$activity = $this->_db->select()->distinct()
// 								->from(array('A' => 'M_FPRIVILEGE'),array('FPRIVI_ID', 'FPRIVI_DESC', 'FPRIVI_MODE'))
// 								->order('FPRIVI_DESC ASC')
// 				 				->query()->fetchAll();

// 		$login = array('BPRIVI_ID'=>'BLGN','BPRIVI_DESC'=>'Login');
// 		$logout = array('BPRIVI_ID'=>'BLGT','BPRIVI_DESC'=>'Logout');
// 		$changepass = array('BPRIVI_ID'=>'CHOP','BPRIVI_DESC'=>'Change My Password');
// 		array_unshift($activity,$changepass);
// 		array_unshift($activity,$logout);
// 		array_unshift($activity,$login);

// 		$tableDtl = array();
// 		foreach ($activity as $key => $value) {
// 			if ($system_type == 2) {

//                 if ($value['FPRIVI_MODE'] == '2' || $value['FPRIVI_MODE'] == '0') {
//                  	$tableDtl[$key] = $value;
//                 }

//             }else if($system_type == 1){

//                 if ($value['FPRIVI_MODE'] == '1' || $value['FPRIVI_MODE'] == '0') {
//                  	$tableDtl[$key] = $value;
//                 }

//             }else{

//             	if ($value['FPRIVI_MODE'] == '1' || $value['FPRIVI_MODE'] == '0' || $value['FPRIVI_MODE'] == '2') {
//                  	$tableDtl[$key] = $value;
//                 }
//             }
// 		}

// 		$activityarr = Application_Helper_Array::listArray($tableDtl,'FPRIVI_ID','FPRIVI_DESC');
// 		asort($activityarr);
// 		//Zend_Debug::dump($activityarr);die;

// 		$this->view->activity=$activityarr;


// 		$fields = array('datetime' 	   => array('field'    => 'LOG_DATE',
//                                         	    'label'    => $this->language->_('Date/Time'),
//                                         	    'sortable' => true),
//                         'userID'       => array('field'    => 'USER_ID',
//                                         	    'label'    => $this->language->_('User ID'),
//                                         	    'sortable' => true),
//                         // 'username' 	   => array('field'    => 'USER_NAME',
//                         //                 	    'label'    => $this->language->_('Name'),
//                         //                 	    'sortable' => true),
// 						'activity'		=> array('field'    => 'ACTION_DESC',
// 											    'label'    => $this->language->_('Activity Type'),
//                                         	    'sortable' => true),
// 						'description'     => array('field'    => 'ACTION_FULLDESC',
// 											    'label'    => $this->language->_('Description'),
//                                         	    'sortable' => true),
//             );

// 		$fields1 = array('datetime' 	   => array('field'    => 'LOG_DATE',
//                                         	    'label'    => $this->language->_('Date/Time'),
//                                         	    'sortable' => true),
//                         'userID'       => array('field'    => 'USER_ID',
//                                         	    'label'    => $this->language->_('User ID'),
//                                         	    'sortable' => true),
//                         'username' 	   => array('field'    => 'USER_NAME',
//                                         	    'label'    => $this->language->_('User Name'),
//                                         	    'sortable' => true),
// 						'activity'		=> array('field'    => 'ACTION_DESC',
// 											    'label'    => $this->language->_('Activity Type'),
//                                         	    'sortable' => true),
// 						'description'     => array('field'    => 'ACTION_FULLDESC',
// 											    'label'    => $this->language->_('Description'),
//                                         	    'sortable' => true),
//             );

//         //get page, sortby, sortdir
//         $page = $this->_getParam('page');		
//         //$sortBy = $this->_getParam('sortby');
//         //$sortDir = $this->_getParam('sortdir');
//         $sortBy  = $this->_getParam('sortby','datetime');
// 		$sortDir = $this->_getParam('sortdir','desc');

//         $getCSV = $this->_getParam('csv');

//         //validate parameters before passing to view and query
//         $page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;

//         $sortBy = (Zend_Validate::is($sortBy,'InArray',
//         	array(array_keys($fields))
//             ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

//         $sortDir = (Zend_Validate::is($sortDir,'InArray',
//         	array('haystack'=>array('asc','desc'))
//             ))? $sortDir : 'asc';

// 		//get filtering param
//         $filters = array('filter' 	   	=>  array('StringTrim', 'StripTags'),
//                          'fUserID'   	=>  array('StringTrim', 'StripTags', 'StringToUpper'),
//                          'fDateFrom' 	=>  array('StringTrim', 'StripTags'),
//                          'fDateTo'   	=>  array('StringTrim', 'StripTags'),
//         				 'moduleDesc'   =>  array('StringTrim', 'StripTags', 'StringToUpper'),
//         				 'actionDesc'   =>  array('StringTrim', 'StripTags', 'StringToUpper'),
//         				 'keyValue'   	=>  array('StringTrim', 'StripTags', 'StringToUpper'),
//             );

//         $optVldtr   = array('breakChainOnFailure' => false);
//         $validators = array(
//        					'filter'	  => array('allowEmpty' => true),
//         				'fUserID'	  => array('allowEmpty' => true),
// 						'fDateFrom' => array(new Zend_Validate_Date($this->_dateDisplayFormat), 
//         									 'allowEmpty' => true,
// 											 'messages' => array("From date format must be '{$this->_dateDisplayFormat}'")													   					   
// 											 ),
// 						'fDateTo'   => array(new Zend_Validate_Date($this->_dateDisplayFormat),
// 											 'allowEmpty' => true, 
// 											 'messages' => array("To date format must be '{$this->_dateDisplayFormat}'" )													   					   
// 											 ),
// 						'moduleDesc' => array('allowEmpty' => true),
// 						'actionDesc' => array('allowEmpty' => true),					 
// 						'keyValue' 	 => array('allowEmpty' => true),
// 							);

//         $zf_filter = new Zend_Filter_Input($filters, $validators, $this->getRequest()->getParams(), $optVldtr);

// 	      	$this->view->currentPage = $page;
// 	        $this->view->sortBy = $sortBy;
// 	        $this->view->sortDir = $sortDir;

// 	        $filter		 = ($zf_filter->filter)     ? $zf_filter->filter     : $this->_request->getParam('filter');

// 	        $filter_clear = $this->_getParam('clearfilter');
// 	        $csv = $this->_getParam('csv');
// 			$pdf = $this->_getParam('pdf');

// 	        if($filter == TRUE)
//         	{	
// 				$fUserID     = html_entity_decode(($zf_filter->fUserID)    ? $zf_filter->fUserID     : $this->_request->getParam('fUserID'));
// 				$fDateFrom 	 = html_entity_decode($zf_filter->getEscaped('fDateFrom'));
// 				$fDateTo     = html_entity_decode($zf_filter->getEscaped('fDateTo'));
// 				$active = $this->_request->getParam('SEARCH_ACTIVITY');

// 	            $this->view->fUserID	 = $fUserID;
// 				$this->view->active = $active;
//         	}

// 			$select = $this->_db->select()
// 						->from (array('A' => 'T_FACTIVITY'),array())
// 						->joinleft (array('B' => 'M_FPRIVILEGE'),'A.ACTION_DESC = B.FPRIVI_ID', 
// 								array('LOG_DATE' 	    => 'A.LOG_DATE', 
// 									  'USER_ID' 	    => 'A.USER_ID', 
// 									  'USER_NAME'       => 'A.USER_NAME', 
// 									  'ACTION_DESC' 	=> new Zend_Db_Expr("(CASE A.ACTION_DESC 
// 									        	  								   	WHEN 'BLGN' THEN 'Login' 
// 									        	  									WHEN 'BLGT' THEN 'Logout' 
// 									        	  									WHEN 'CHOP' THEN 'Change My Password' 
// 									        	  									ELSE B.FPRIVI_DESC 
// 									        	  									END)"), 
// 									  'ACTION_FULLDESC'	=> 'A.ACTION_FULLDESC'
// 								      ))
// 						->where('USER_ID LIKE ?', $this->_userIdLogin);

// 	        if($filter == null)
// 			{	
// 				$fDateFrom = (date("d/m/Y"));
// 				$fDateTo = (date("d/m/Y"));
// 			}
// 			//die;

// 			$fDateFrommax = (date("d/m/Y",strtotime("-3 month")));
// 			$this->view->fDateFrommax 	 = $fDateFrommax;

// 			if($filter_clear == TRUE){
// 				$fDateFrom = '';
// 				$fDateTo = '';
// 				$this->view->fDateFrom   = $fDateFrom;
// 		        $this->view->fDateTo 	 = $fDateTo;
// 			}

// 			if($filter == TRUE || $filter == null)
// 	        {
// 	        	$this->view->fDateFrom   = $fDateFrom;
// 		        $this->view->fDateTo 	 = $fDateTo;
// 	        	if(!empty($fDateFrom))
// 	            {
// 	            	$FormatDate = new Zend_Date($fDateFrom, $this->_dateDisplayFormat);
// 					$fDateFrom  = $FormatDate->toString($this->_dateDBFormat);	
// 	            }

// 	            if(!empty($fDateTo))
// 	            {
// 	            	$FormatDate = new Zend_Date($fDateTo, $this->_dateDisplayFormat);
// 					$fDateTo    = $FormatDate->toString($this->_dateDBFormat);
// 	            }

// 	            if(!empty($fDateFrom) && empty($fDateTo))
// 	            $select->where("DATE(A.LOG_DATE) >= ".$this->_db->quote($fDateFrom));

// 	            if(empty($fDateFrom) && !empty($fDateTo))
// 	            $select->where("DATE(A.LOG_DATE) <= ".$this->_db->quote($fDateTo));

// 	            if(!empty($fDateFrom) && !empty($fDateTo))
// 	            $select->where("DATE(A.LOG_DATE) between ".$this->_db->quote($fDateFrom)." and ".$this->_db->quote($fDateTo));
// 	        }
// 			//die;
// 	        if($filter == TRUE)
// 	        {
// 	        	//where clauses        	
// 	            if($fUserID)
// 	            $select->where("UPPER(A.USER_ID) LIKE ".$this->_db->quote('%'.$fUserID.'%'));

// 	            if($active)
// 	            $select->where("UPPER(A.ACTION_DESC) LIKE ".$this->_db->quote('%'.$active.'%'));  
// 			}
// 			// echo $select;die;
// 			$select->order($sortBy.' '.$sortDir);
// 			$data = $this->_db->fetchAll($select);

// 			$this->paging($select);
// 		    $this->view->fields 	 = $fields;
// 		    $this->view->filter 	 = $filter;
// 			//Zend_Debug::dump($data);die;

// 			if($csv || $pdf || $this->_request->getParam('print'))
// 			{
// 				$header = array($this->language->_('Date/Time'),$this->language->_('User ID'),$this->language->_('User Name'),$this->language->_('Activity Type'),$this->language->_('Description'));

// 				foreach($data as $key=>$row)
// 				{
// 					$data[$key]['LOG_DATE'] = Application_Helper_General::convertDate($row['LOG_DATE'],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);					
// 				}

// 				if($csv)
// 				{
// 					// if($filter == TRUE){
// 						$selectcomp = $this->_db->select()
// 					            ->from('M_CUSTOMER', array('value' => 'CUST_ID','CUST_NAME'))   
// 					            ->where('CUST_ID = ?', $this->_custIdLogin);

// 						$selectcomp = $this->_db->fetchAll($selectcomp);
// 						$headerData[] = $selectcomp[0]['CUST_NAME'];
// 						$newData[] = array('My Activity');
// 						$newData[] = array('From : '.Application_Helper_General::convertDate($fDateFrom,'dd/MM/yyyy').' to : '.Application_Helper_General::convertDate($fDateTo,'dd/MM/yyyy'));

// 						if($filter == TRUE){
// 							$newData[] = array('Activity Type : '.$data[0]['ACTION_DESC']);
// 						}

// 						if($filter == null){
// 							$newData[] = array('Activity Type : ');
// 						}

// 						$newData[] = array(' ');
// 						$newData[] = array($this->language->_('Date/Time'),$this->language->_('User ID'),$this->language->_('User Name'),$this->language->_('Activity Type'),$this->language->_('Description'));
// 					// }

// 					foreach ($data as $p => $pTrx){
// 						$paramTrx = array(	
// 											"LOG_DATE"  			=> Application_Helper_General::convertDate($pTrx['LOG_DATE'],$this->displayDateTimeFormat,$this->defaultDateFormat),
// 											"USER_ID"  				=> $pTrx['USER_ID'],
// 											"USER_NAME" 			=> $pTrx['USER_NAME'],
// 											"ACTION_DESC"  			=> $pTrx['ACTION_DESC'],
// 											"ACTION_FULLDESC"  		=> $pTrx['ACTION_FULLDESC'],
// 									);
// 						$newData[] = $paramTrx;
// 					}

// 					$datenow = date("Ymd");
// 					$this->_helper->download->csv($headerData,$newData,null,'Own Activity_'.$this->_userIdLogin.'_'.$datenow);  
// 				}
// 				else if($pdf)
// 				{
// 					$datenow = date("Ymd");
// 					$this->_helper->download->pdf($header,$data,null,'Own Activity_'.$this->_userIdLogin.'_'.$datenow);    
// 				}
// 				else if($this->_request->getParam('print')){

// 					$setting = new Settings();
// 					$master_bank_app_name = $setting->getSetting('master_bank_app_name');
// 					$master_bank_name = $setting->getSetting('master_bank_name');
// 					$masterbank = $master_bank_app_name . ' - ' . $master_bank_name; 


// 					$selectcomp = $this->_db->select()
// 					            ->from('M_CUSTOMER', array('value' => 'CUST_ID','CUST_NAME'))   
// 					            ->where('CUST_ID = ?', $this->_custIdLogin);

// 					$selectcomp = $this->_db->fetchAll($selectcomp);
// 					$newData[] = $selectcomp[0]['CUST_NAME'];
// 					$newData[] = 'My Activity';
// 					$newData[] = 'From : '.Application_Helper_General::convertDate($fDateFrom,'dd/MM/yyyy').' to : '.Application_Helper_General::convertDate($fDateTo,'dd/MM/yyyy');

// 					if($filter == TRUE){
// 						$newData[] = 'Activity Type : '.$data[0]['ACTION_DESC'];
// 					}

// 					if($filter == null){
// 						$newData[] = 'Activity Type : ';
// 					}

// 					$tempfilter = 1;

// 					$this->_forward('print', 'index', 'widget', array('data_content' => $data, 'data_caption' => $masterbank, 'data_header' => $fields1, 'data_filter' => $newData, 'filter' => $tempfilter));
// 				}


// 			}	
// // end if if($zf_filter->isValid())
//  	}
// }