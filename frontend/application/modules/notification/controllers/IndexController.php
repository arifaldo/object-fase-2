<?php
require_once 'Zend/Controller/Action.php';

class notification_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF';

	public function indexAction()
	{
		
	}
	
	public function releaseAction()
	{
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		$this->view->PS_NUMBER = $temp[0];
		
		$sessionNamespace = new Zend_Session_Namespace('URL_CP_WR');
		$this->view->backURL = (!empty($sessionNamespace->URL)) ? 
									   $sessionNamespace->URL : '/paymentworkflow/waitingrelease/index';
		
		
	}
}
