<?php
require_once 'Zend/Controller/Action.php';

class token_IndexController extends Application_Main {

	public function initController()
	{
		$this->_helper->layout->setLayout('newlayout');
	}

	public function indexAction(){

		$userId = $this->_userIdLogin;
		$custId = $this->_custIdLogin;

		$token = new Service_Token($custId, $userId);
		// echo "<pre>";
		// var_dump($token);
		$res = $token->tokenHardwareList(array());
		// var_dump($res);die;
		$resData = (isset($res['ResponseData']) && count($res['ResponseData']) ? $res['ResponseData'] : array());
		$tokenData = $this->view->tokenData = $resData;
		
		$this->view->custId = $custId;
		$this->view->paginator = $resData;

	}
}