<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';

class exchangerate_RateController extends Application_Main
{
	public function indexAction() 
	{ 
		$userId = $this->_userIdLogin;
		$custId = $this->_custIdLogin;

//		$token = new Service_Token($custId, $userId);
//		$res = $token->tokenHardwareList(array());
//		$resData = (isset($res['ResponseData']) && count($res['ResponseData']) ? $res['ResponseData'] : array());
		
		$this->view->tokenData = $resData;
		//Zend_Debug::dump($resData);
		$dateNow = date("Y-m-d H:i:s");
		
		$this->view->dateNow = $dateNow;

	    $fields = array('C.CUST_ID'   		=> array('field'    => 'C.CUST_ID',
	                                        		 'label'    => $this->language->_('KURS'),
	                                        		 'sortable' => true),
	    
	                    'CUST_NAME' 		=> array('field'    => 'CUST_NAME',
	                                        		 'label'    => $this->language->_('JUAL'),
	                                        		 'sortable' => true),
	    
	                    'USER_ID'     		=> array('field'    => 'USER_ID',
	                                        		 'label'    => $this->language->_('BELI'),
	                                       		  	 'sortable' => true),
	    
	    			
	                   );
	    
	    //validasi page, jika input page bukan angka               
	    $page = $this->_getParam('page');
	    $csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');
    
    
	    $page = (Zend_Validate::is($page,'Digits'))? $page : 1;
	    
	    //validasi sort, jika input sort bukan ASC atau DESC
	    $sortBy  = $this->_getParam('sortby');
	    $sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
	    $sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
	
	    $filterArr = array(	'filter' => array('StripTags','StringTrim'),
	                       	'custid'    => array('StripTags','StringTrim','StringToUpper'),
	                       	'custname'  => array('StripTags','StringTrim','StringToUpper'),
	    					'username'  => array('StripTags','StringTrim','StringToUpper'),
	    					'userid'  => array('StripTags','StringTrim','StringToUpper'),
	                       	'status' => array('StripTags','StringTrim')
	                      );
	                      
	    $zf_filter = new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
	    $filter = $zf_filter->getEscaped('filter');
	    
	    $this->view->currentPage = $page;
	    $this->view->sortBy = $sortBy;
	    $this->view->sortDir = $sortDir;
	    
    
    // proses pengambilan data filter,display all,sorting
    
//        $select2 = $this->_db->select()
//					        ->from(array('B' => 'M_USER'),array())
//					        ->join(array('C' => 'M_CUSTOMER'), 'B.CUST_ID = C.CUST_ID',array(	'C.CUST_ID',
//				        																	 	'C.CUST_NAME',
//				        																	 	'B.USER_ID',
//				        																	 	'B.USER_FULLNAME',
//				        																		'B.TOKEN_ID',
//				        																	 	'B.USER_EMAIL',
//				        																	 	'B.USER_PHONE',
//				        																	 	'B.USER_CREATED',
//				        																	 	'B.USER_SUGGESTED',
//				        																	 	'B.USER_UPDATED')); 
		$select2 = array(
			'USD' => $this->language->_('USD'),
			'SGD' => $this->language->_('SGD'),
			'EUR' => $this->language->_('EUR'),
			'AUD' => $this->language->_('AUD'),
		)	; 

		
//		
//        if($filter==TRUE)
//        {
//			$custid     = html_entity_decode($zf_filter->getEscaped('custid'));
//			$custname   = html_entity_decode($zf_filter->getEscaped('custname'));
//			$userid     = html_entity_decode($zf_filter->getEscaped('userid'));
//			$username   = html_entity_decode($zf_filter->getEscaped('username'));
//			$status  = $zf_filter->getEscaped('status');
//			if($custid)$select2->where('UPPER(C.CUST_ID) LIKE '.$this->_db->quote('%'.strtoupper($custid).'%'));
//			if($custname)$select2->where('UPPER(CUST_NAME) LIKE '.$this->_db->quote('%'.strtoupper($custname).'%'));
//			if($userid)$select2->where('UPPER(USER_ID) LIKE '.$this->_db->quote('%'.strtoupper($userid).'%'));
//			if($username)$select2->where('UPPER(USER_FULLNAME) LIKE '.$this->_db->quote('%'.strtoupper($username).'%'));
//			if($status)$select2->where('USER_STATUS=?',$status);
//			//echo $status; die;
//			
//			$this->view->custid = $custid;
//			$this->view->custname = $custname;
//			$this->view->userid = $userid;
//			$this->view->username = $username;
//			$this->view->status = $status;
//        }
        //$select2->order($sortBy.' '.$sortDir);
	    $this->paging($select2);
	    //die;
	    $this->view->fields = $fields;
	    $this->view->filter = $filter;
     //insert log
	} 
}