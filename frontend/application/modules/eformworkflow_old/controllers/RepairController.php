<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class eformworkflow_RepairController extends Application_Main
{

  protected $_moduleDB = 'RTF'; // masih harus diganti

  public function indexAction()
  {
    $this->_helper->_layout->setLayout('newlayout');

    $settings = new Settings();
    $system_type = $settings->getSetting('system_type');
    $this->view->systemType = $system_type;

    $conf = Zend_Registry::get('config');
    $this->_bankName = $conf['app']['bankname'];
    $this->view->masterbankname = $this->_bankName;

    $this->CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

    $fields = array(
      'regno'             => array(
        'field'    => 'BG_REG_NUMBER',
        'label'    => $this->language->_('Reg Number / Subject'),
      ),
      'type'              => array(
        'field'    => 'TIME_PERIOD_START',
        'label'   => $this->language->_('Type'),
      ),
      'bank_branch'       => array(
        'field'    => 'BRANCH_NAME',
        'label'    => $this->language->_('Bank Branch'),
      ),
      'counter_type'     => array(
        'field'    => 'COUNTER_WARRANTY_TYPE',
        'label'    => $this->language->_('Counter Type'),
      ),
      'bgamount'         => array(
        'field'    => 'BG_AMOUNT',
        'label'    => $this->language->_('BG Amount')
      ),
      'repair_note'     => array(
        'field'    => 'REPAIR_NOTE',
        'label'    => $this->language->_('Repair Notes'),
      ),
      'request_by'      => array(
        'field' => 'REQUEST_BY',
        'label' => $this->language->_('Request By'),
      ),
    );





    // filter ----------------------------------------------------------

    // $filterlist = array(
    //   "BG_NUMBER" => 'REG NUMBER',
    //   'BG_SUBJECT' => "SUBJECT",
    //   "AMOUNT" => "AMOUNT"
    // );

    $filterlist = array('REG_NUMBER', 'BG_SUBJECT', 'BRANCH_NAME', 'COUNTER_TYPE', 'REQUEST_BY');

    $this->view->filterlist = $filterlist;

    $dataParam = array("REG_NUMBER", "BG_SUBJECT", "BRANCH_NAME", "COUNTER_TYPE", "REQUEST_BY");
    $dataParamValue = array();

    foreach ($dataParam as $dtParam) {
      if (!empty($this->_request->getParam('wherecol'))) {
        $dataval = $this->_request->getParam('whereval');
        foreach ($this->_request->getParam('wherecol') as $key => $value) {
          if ($dtParam == $value) {

            if (empty($dataParamValue[$dtParam])) {
              $dataParamValue[$dtParam] = [];
            }
            array_push($dataParamValue[$dtParam], $dataval[$key]);
          }
        }
      }
    }

    $options = array('allowEmpty' => true);
    $validators = array(
      'REG_NUMBER' => array(),
      'BG_SUBJECT' => array(),
      'BRANCH_NAME' => array(),
      'COUNTER_TYPE' => array(),
      'AMOUNT' => array(),
      'REQUEST_BY' => array(),
    );

    $zf_filter  = new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);

    $fRegNumber = $zf_filter->getEscaped('REG_NUMBER');
    $fSubject = $zf_filter->getEscaped('BG_SUBJECT');
    $fBranchName = $zf_filter->getEscaped('BRANCH_NAME');
    $fCounterType = $zf_filter->getEscaped('COUNTER_TYPE');
    $fAmount = $zf_filter->getEscaped('AMOUNT');
    $fRequestBy = $zf_filter->getEscaped('REQUEST_BY');


    $REPAIR_NOTE = $this->_db->select()
      ->from(
        array('D' => 'T_BANK_GUARANTEE_HISTORY'),
        array(
          'BG_REASON' => 'D.BG_REASON'
        )
      )
      // ->where('D.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
      // ->where('D.HISTORY_STATUS = 3')
      ->where('D.HISTORY_STATUS IN (3, 11, 19)')
      ->where("D.BG_REG_NUMBER = A.BG_REG_NUMBER")
      ->limit(1)
      ->order('D.DATE_TIME DESC');

    $REQUEST_BY = $this->_db->select()
      ->from(
        array('D' => 'T_BANK_GUARANTEE_HISTORY'),
        array(
          'USER_LOGIN'      => 'D.USER_LOGIN'
        )
      )
      // ->where('D.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
      // ->where('D.HISTORY_STATUS = 3')
      ->where('D.HISTORY_STATUS IN (3, 11)')
      ->where("D.BG_REG_NUMBER = A.BG_REG_NUMBER")
      ->limit(1)
      ->order('D.DATE_TIME DESC');

    if ($fRequestBy) {
      $REQUEST_BY->where("D.USER_LOGIN LIKE " . $this->_db->quote('%' . $fRequestBy[0] . '%'));
    }

    $selectbgArr = $this->_db->select()
      ->from(
        array('A' => 'TEMP_BANK_GUARANTEE'),
        array(
          'REG_NUMBER'                    => 'BG_REG_NUMBER',
          'SUBJECT'                       => 'BG_SUBJECT',
          'RECIPIENT_NAME'                => 'RECIPIENT_NAME',
          'TIME_PERIOD_START'             => 'TIME_PERIOD_START',
          'TIME_PERIOD_END'               => 'TIME_PERIOD_END',
          'COUNTER_WARRANTY_TYPE'         => 'COUNTER_WARRANTY_TYPE',
          'COUNTER_WARRANTY_ACCT_NO'      => 'COUNTER_WARRANTY_ACCT_NO',
          'COUNTER_WARRANTY_ACCT_NAME'    => 'COUNTER_WARRANTY_ACCT_NAME',
          'IS_AMENDMENT'                  => 'A.CHANGE_TYPE',
          'AMOUNT'                        => 'BG_AMOUNT',
          // 'FULLNAME'                      => 'T.USER_FULLNAME',
          'BRANCH_NAME'                   => 'C.BRANCH_NAME',
          // 'REQUEST_BY'                    =>'D.USER_LOGIN',
          // 'REPAIR_NOTE'                   =>'D.BG_REASON', 
          'REPAIR_NOTE'                   => new Zend_Db_Expr('(' . $REPAIR_NOTE . ')'),
          'REQUEST_BY'                     => new Zend_Db_Expr('(' . $REQUEST_BY . ')'),
        )
      )
      ->join(array('C' => 'M_BRANCH'), 'A.BG_BRANCH = C.BRANCH_CODE')
      ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
      ->where('A.BG_STATUS IN (4, 11, 19)')
      ->order('A.BG_CREATED DESC');

    $selectbg = $selectbgArr;

    $selectlc = $this->_db->select()
      ->from(array('A' => 'T_LC'), array(
        'REG_NUMBER' => 'LC_REG_NUMBER',
        'SUBJECT' => 'LC_CREDIT_TYPE',
        'CREATED' => 'LC_CREATED',
        'CCYID' => 'LC_CCY',
        'TIME_PERIOD_END' => 'LC_EXPDATE',
        'CREATEDBY' => 'LC_CREATEDBY',
        'AMOUNT' => 'LC_AMOUNT',
        'FULLNAME' => 'T.USER_FULLNAME'
      ))
      ->join(array('T' => 'M_USER'), 'A.LC_CREATEDBY = T.USER_ID')
      ->where('A.LC_CUST =' . $this->_db->quote((string)$this->_custIdLogin))
      ->where('A.LC_STATUS = 4')
      ->order('A.LC_CREATED DESC');

    if ($fRegNumber) {
      $selectbg->where("A.BG_REG_NUMBER LIKE " . $this->_db->quote('%' . $fRegNumber[0] . '%'));
    }
    if ($fSubject) {
      $selectbg->where("A.BG_SUBJECT LIKE " . $this->_db->quote('%' . $fSubject[0] . '%'));
    }
    if ($fBranchName) {
      $selectbg->where("BRANCH_NAME LIKE " . $this->_db->quote('%' . strtoupper($fBranchName[0]) . '%'));
    }
    if ($fAmount) {
      $selectbg->where("A.BG_AMOUNT = " . $this->_db->quote($fAmount[0]));
    }

    if ($fCounterType) {
      $selectbg->where("A.COUNTER_WARRANTY_TYPE = ?", $fCounterType[0]);
    }

    // -----------------------------------------------------------------

	
    $selectbg = $this->_db->fetchAll($selectbg);
    $selectlc = $this->_db->fetchAll($selectlc);

    // echo '<pre>';
    // print_r($selectbg);
    $result = array_merge($selectbg, $selectlc);

    $setting = new Settings();
    $enc_pass = $setting->getSetting('enc_pass');
    $enc_salt = $setting->getSetting('enc_salt');
    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');
    $pw_hash = md5($enc_salt . $enc_pass);
    $rand = $this->_userIdLogin . date('dHis') . $pw_hash;
    $sessionNamespace->token   = $rand;
    $this->view->token = $sessionNamespace->token;

    foreach ($result as $key => $value) {
      $get_reg_number = $value["REG_NUMBER"];

      $AESMYSQL = new Crypt_AESMYSQL();
      $rand = $this->token;

      $encrypted_payreff = $AESMYSQL->encrypt($get_reg_number, $rand);
      $encpayreff = urlencode($encrypted_payreff);

      $result[$key]["REG_NUMBER_ENCRYPTED"] = $encpayreff;
    }

    $this->paging($result);

    $conf = Zend_Registry::get('config');

    $this->view->bankname = $conf['app']['bankname'];

    $config = Zend_Registry::get('config');
    $BgType = $config["bg"]["status"]["desc"];
    $BgCode = $config["bg"]["status"]["code"];

    $arrStatus = array_combine(array_values($BgCode), array_values($BgType));

    $this->view->arrStatus = $arrStatus;

    $arrType = array(
      1 => 'Standart',
      2 => 'Custom'
    );

    $arrLang = array(
      1 => 'Indonesian',
      2 => 'English',
      3 => 'Bilingual'
    );
    $this->view->langArr = $arrLang;
    $this->view->formatArr = $arrType;
    $this->view->fields = $fields;

    if (!empty($dataParamValue)) {
      $this->view->createdStart = $dataParamValue['TIME_PERIOD_START'];
      $this->view->createdEnd = $dataParamValue['TIME_PERIOD_END'];

      foreach ($dataParamValue as $key => $value) {
        $duparr = explode(',', $value);
        if (!empty($duparr)) {
          foreach ($duparr as $ss => $vs) {
            $wherecol[] = $key;
            $whereval[] = $vs[0];
          }
        } else {
          $wherecol[] = $key;
          $whereval[] = $value[0];
        }
      }

      $this->view->wherecol     = $wherecol;
      $this->view->whereval     = $whereval;
    }
  }

  public function getcountertypeAction()
  {
    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout()->disableLayout();

    $tblName = $this->_getParam('id');

    $optHtml = "<option value=''>-- " . $this->language->_('Please Select') . " --</option>";

    $arrWarrantyType = array(
      '1' => 'FC',
      '2' => 'LF',
      '3' => 'INS'
    );

    foreach ($arrWarrantyType as $key => $row) {
      if ($tblName == $key) {
        $select = 'selected';
      } else {
        $select = '';
      }
      $optHtml .= "<option value='" . $key . "' " . $select . ">" . $row . "</option>";
    }

    echo $optHtml;
  }
}
