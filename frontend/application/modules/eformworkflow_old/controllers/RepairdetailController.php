<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';
require_once("Service/Account.php");

class eformworkflow_repairdetailController extends Application_Main
{

    protected $_moduleDB = 'RTF'; // masih harus diganti

    public function indexAction()
    {
        $this->_helper->_layout->setLayout('newlayout');

        $settings = new Settings();
        $system_type = $settings->getSetting('system_type');
        $this->view->systemType = $system_type;

        $conf = Zend_Registry::get('config');
        $this->_bankName = $conf['app']['bankname'];
        $this->view->masterbankname = $this->_bankName;

        $selectcomp = $this->_db->select()
            ->from(array('A' => 'M_CUSTOMER'), array('*'))
            //  ->joinLeft(array('B' => 'M_COUNTRY'),'A.COUNTRY_CODE = B.COUNTRY_CODE',array('COUNTRY_NAME'))
            ->joinRight(array('B' => 'M_CITYLIST'), 'A.CUST_CITY = B.CITY_CODE', array('B.CITY_NAME'))
            ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
            ->query()->fetchAll();
        // echo '<pre>';
        // print_r($selectcomp);
        $this->view->compinfo = $selectcomp[0];
        $this->view->owner1 = $selectcomp[0]["CUST_NAME"];
        $numb = $this->_getParam('bgnumb');

        // decrypt numb
        $setting = new Settings();
        $enc_pass = $setting->getSetting('enc_pass');
        $enc_salt = $setting->getSetting('enc_salt');
        $sessionNamespace = new Zend_Session_Namespace('Tokenenc');
        $pw_hash = md5($enc_salt . $enc_pass);
        $rand = $this->_userIdLogin . date('dHis') . $pw_hash;
        $sessionNamespace->token     = $rand;
        $this->view->token = $sessionNamespace->token;

        $sessionNamespace = new Zend_Session_Namespace('Tokenenc');
        $password = $sessionNamespace->token;
        $this->view->token = $sessionNamespace->token;


        $AESMYSQL = new Crypt_AESMYSQL();

        $BG_NUMBER     = urldecode($this->_getParam('bgnumb'));

        $BG_NUMBER = $AESMYSQL->decrypt($BG_NUMBER, $password);

        $numb = $BG_NUMBER;

        $get_ccy_exist = $this->_db->select()
            ->from("M_MINAMT_CCY", ["CCY_ID"])
            ->query()->fetchAll();

        $this->view->ccy_exist = $get_ccy_exist;

        $Settings = new Settings();

        $toc_ind = $Settings->getSetting('ftemplate_bg_ind');
        $this->view->toc_ind = $toc_ind;
        $toc_eng = $Settings->getSetting('ftemplate_bg_eng');
        $this->view->toc_eng = $toc_eng;

        $params = $this->_request->getParams();
        if (!empty($numb)) {
            $bgdata = $this->_db->select()
                ->from(array('A' => 'TEMP_BANK_GUARANTEE'), array('*'))
                ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
                ->where('A.BG_REG_NUMBER = ?', $numb)
                ->where('A.BG_STATUS IN (4, 11, 19)')
                ->query()->fetchAll();

            $bgdatadetail = $this->_db->select()
                ->from(array('A' => 'TEMP_BANK_GUARANTEE_DETAIL'), array('*'))
                ->where('A.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
                ->where('A.BG_REG_NUMBER = ?', $numb)
                ->query()->fetchAll();

            $get_others_attachment = $this->_db->select()
                ->from("TEMP_BANK_GUARANTEE_FILE")
                ->where("BG_REG_NUMBER = ?", $numb)
                ->query()->fetchAll();

            $this->view->get_others_attachment = $get_others_attachment;


            $this->view->currency = $bgdatadetail[array_search("Currency", array_column($bgdatadetail, "PS_FIELDNAME"))]["PS_FIELDVALUE"];
            //$datas = $this->_request->getParams();
            // var_dump($datas);

            // echo "<BR><code>bgdata = $bgdata</code><BR>"; die;

            if (!empty($bgdata)) {

                // get cash collateral

                $get_cash_collateral = $this->_db->select()
                    ->from("M_CHARGES_OTHER", ["CHARGES_PCT", "CHARGES_ADM", "CHARGES_STAMP"])
                    ->where("CUST_ID = ?", "GLOBAL")
                    ->where("CHARGES_TYPE = ?", "10")
                    ->query()->fetchAll();

                $this->view->cash_collateral = $get_cash_collateral[0];

                // end get cash collateral

                $REPAIR_NOTE = $this->_db->select()
                    ->from(
                        array('D' => 'T_BANK_GUARANTEE_HISTORY'),
                        array(
                            'BG_REASON'      => 'D.BG_REASON'
                        )
                    )
                    ->where('D.HISTORY_STATUS IN (4, 11, 19)')
                    ->where("D.BG_REG_NUMBER = ?", $numb)
                    ->limit(1)
                    ->order('D.DATE_TIME DESC')
                    ->query()->fetchAll();

                $this->view->reason = $REPAIR_NOTE[0]["BG_REASON"];

                // get linefacillity
                $paramLimit = array();

                $paramLimit['CUST_ID'] =  $this->_custIdLogin;
                $paramLimit['COUNTER_WARRANTY_TYPE'] = 2;
                $getLineFacility = Application_Helper_General::getLineFacility($paramLimit);


                $this->view->current_limit = $getLineFacility['currentLimit'];
                $this->view->max_limit =  $getLineFacility['plafondLimit'];

                //$this->view->linefacility = $get_linefacility[0];

                // end get linefacility

                $bgdatasplit = $this->_db->select()
                    ->from(array('A' => 'TEMP_BANK_GUARANTEE_SPLIT'), array('*'))
                    ->joinLeft(["MCA" => "M_CUSTOMER_ACCT"], "MCA.ACCT_NO = A.ACCT", ["ACCT_TYPE2" => "MCA.ACCT_TYPE", "ACCT_DESC2" => "MCA.ACCT_DESC"])
                    ->where('MCA.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin))
                    ->where('A.BG_REG_NUMBER = ?', $numb)
                    ->query()->fetchAll();
                $this->view->bgdatasplit = $bgdatasplit;

                $data = $bgdata['0'];
                $this->view->data = $data;


                $this->view->bankFormatNumber = $data["BG_FORMAT"];

                if ($data['IS_AMENDMENT'] == 1) {
                    $this->view->suggestion_type = 'Change';
                } else if ($data['IS_AMENDMENT'] == 0) {
                    $this->view->suggestion_type = 'New';
                }

                if (!empty($data['BG_BRANCH'])) {
                    $selectbranch = $this->_db->select()
                        ->from(array('A' => 'M_BRANCH'), array('*'))
                        ->where('A.BRANCH_CODE = ?', $data['BG_BRANCH'])
                        ->query()->fetchAll();
                    //var_dump($selectbranch[0]['BRANCH_NAME']);die;
                    $this->view->branchname = $selectbranch[0]['BRANCH_NAME'];
                }

                $this->view->updateStart = Application_Helper_General::convertDate($data['TIME_PERIOD_START'], $this->view->viewDateFormat, $this->view->defaultDateFormat);
                $this->view->updateEnd = Application_Helper_General::convertDate($data['TIME_PERIOD_END'], $this->view->viewDateFormat, $this->view->defaultDateFormat);

                $this->view->TIME_PERIOD_START = $data['TIME_PERIOD_START'];
                $this->view->TIME_PERIOD_END = $data['TIME_PERIOD_END'];


                // $arrStatus = array(
                //     '1' => 'Waiting for review',
                //     '2' => 'Waiting for approve',
                //     '3' => 'Waiting to release',
                //     '4' => 'Waiting for bank approval',
                //     '5' => 'Issued',
                //     '6' => 'Expired',
                //     '7' => 'Canceled',
                //     '8' => 'Claimed by applicant',
                //     '9' => 'Claimed by recipient',
                //     '10' => 'Request Repair',
                //     '11' => 'Reject'
                // );

                //$config    		= Zend_Registry::get('config');
                $config = Zend_Registry::get('config');
                $BgType         = $config["bg"]["status"]["desc"];
                $BgCode         = $config["bg"]["status"]["code"];

                $arrStatus = array_combine(array_values($BgCode), array_values($BgType));

                $this->view->arrStatus = $arrStatus;


                $comp = "'" . $this->_custIdLogin . "'";
                $acctlist = $this->_db->fetchAll(
                    $this->_db->select()
                        ->from(array('A' => 'M_APIKEY'))
                        ->join(array('C' => 'M_APICREDENTIAL'), 'A.APIKEY_ID = C.ID', array('*'))
                        ->join(array('B' => 'M_BANKTABLE'), 'B.BANK_CODE = A.BANK_CODE', array('B.BANK_NAME'))
                        // ->where('A.ACCT_STATUS = ?','5')
                        ->where("A.CUST_ID IN (" . $comp . ")")
                        ->order('A.APIKEY_ID ASC')
                    // echo $acctlist;
                );

                $account = array();
                foreach ($acctlist as $key => $value) {
                    $account[$value['ID']][$value['FIELD']] = $value['VALUE'];
                    $account[$value['ID']]['BANK_CODE'] = $value['BANK_CODE'];
                    $account[$value['ID']]['SENDER_ID'] = $value['SENDER_ID'];
                    $account[$value['ID']]['AUTH_USER'] = $value['AUTH_USER'];
                    $account[$value['ID']]['AUTH_PASS'] = $value['AUTH_PASS'];
                    $account[$value['ID']]['BANK_NAME'] = $value['BANK_NAME'];
                    $account[$value['ID']]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
                }
                // echo "<pre>";
                // var_dump($account);die;
                // $acct = array();
                $i = 0;
                foreach ($account as $key => $value) {

                    $acct[$i]['ACCT_NO'] = $value['account_number'];
                    $acct[$i]['ACCT_BANK'] = $value['BANK_NAME'];
                    $acct[$i]['BANK_CODE'] = $value['BANK_CODE'];
                    $acct[$i]['BANK_NAME'] = $value['BANK_NAME'];
                    $acct[$i]['ACCT_NAME'] = $value['account_name'];
                    $i++;
                }

                // $this->view->sourceAcct = $acct;

                // echo "<code>acct start =========</code>";
                // echo '<pre>'; 
                // print_r($acct);
                // echo "<code>acct end=========</code>";

                $arrBankFormat = array(
                    1 => 'Bank Standard',
                    2 => 'Special Format (with bank approval)'
                );

                $this->view->bankFormat = $arrBankFormat[$data['BG_FORMAT']];

                $arrLang = array(
                    1 => 'Indonesian',
                    2 => 'English',
                    3 => 'Billingual',
                );

                $this->view->BG_LANGUAGE = $data['BG_LANGUAGE'];
                $this->view->languagetext = $arrLang[$data['BG_LANGUAGE']];

                $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

                // $param = array('CCY_IN' => 'IDR', 'ACCT_NO' => $data['FEE_CHARGE_TO']);
                // $param = array('CCY_IN' => 'IDR');
                $param = array('CCY_IN in (?)' => ['IDR', 'USD']);
                $AccArr = $CustomerUser->getAccountsBG($param);
                //}
                if (!empty($AccArr)) {
                    foreach ($AccArr as $i => $value) {

                        $AccArr[$i]['ACCT_BANK'] = $this->_bankName;
                    }
                }
                $this->view->AccArr = $AccArr;

                $type = array('D', 'S', '20', '10');
                $param = array('ACCT_TYPE' => $type);
                //var_dump($param);die;
                //$param = array('ACCT_TYPE' => "20");
                $AccArr2 = $CustomerUser->getAccountsBG($param);
                $this->view->AccArr2 = $AccArr2;

                // preliminary Member
                $selectMP = $this->_db->select()
                    ->from(array('MP' => 'M_PRELIMINARY_MEMBER'), array('*'));
                $selectMP->where('MP.CUST_ID =' . $this->_db->quote((string)$this->_custIdLogin));

                $preliminaryMemberArr = $this->_db->fetchAll($selectMP);
                $this->view->preliminaryMemberArr = $preliminaryMemberArr;

                $select_cust_linefacility = $this->_db->select()
                    ->from(
                        array('A' => 'M_CUST_LINEFACILITY'),
                        array('*')
                    )
                    ->joinLeft(
                        array('B' => 'M_CUSTOMER'),
                        'B.CUST_ID = A.CUST_ID',
                        array('CUST_NAME', 'COLLECTIBILITY_CODE')
                    )
                    ->where("A.CUST_ID = ?", $this->_custIdLogin);
                //->where();
                // $select_cust_linefacility->where("DATE(NOW()) between DATE(A.PKS_START_DATE) AND DATE(A.PKS_EXP_DATE)");
                // ->where("A.STATUS in (2,3,4)");
                //echo $select_cust_linefacility;
                $select_cust_linefacility = $this->_db->fetchRow($select_cust_linefacility);

                if (!empty($select_cust_linefacility)) {
                    $this->view->statusLF = $select_cust_linefacility["STATUS"];
                }

                //usd
                $paramUSD = array('CCY_IN' => 'USD');
                $AccArrUSD = $CustomerUser->getAccountsBG($paramUSD);
                if (!empty($AccArrUSD)) {
                    foreach ($AccArrUSD as $iUSD => $valueUSD) {
                        $AccArrUSD[$iUSD]['ACCT_BANK'] = $this->_bankName;
                    }
                }

                $this->view->AccArrUSD = $AccArrUSD;


                if (!empty($AccArr)) {
                    $this->view->src_name = $AccArr['0']['ACCT_NAME'];
                }

                $arrWaranty = array(
                    1 => 'Full Cover, Savings Account (Giro / Time Deposit / Savings) or Cash Deposit (MD)',
                    2 => 'Indirect Credit Facility (Non Cash Loan) / Bank Guarantee Ceiling',
                    3 => 'Insurance'

                );

                $this->view->warranty_type_text = $arrWaranty[$data['COUNTER_WARRANTY_TYPE']];

                if (!empty($data['USAGE_PURPOSE'])) {
                    $data['USAGE_PURPOSE'] = explode(',', $data['USAGE_PURPOSE']);
                    foreach ($data['USAGE_PURPOSE'] as $key => $val) {
                        $str = 'checkp' . $val;
                        //var_dump($str);
                        $this->view->$str =  'checked';
                    }
                }

                if ($data['BG_NUMBER'] == '') {
                    $data['BG_NUMBER'] = '-';
                }
                if ($data['BG_SUBJECT'] == '') {
                    $data['BG_SUBJECT'] = '- no subject -';
                }


                // echo '<pre>';
                // print_r($conf);

                // BG TYPE
                $bgType         = $conf["bg"]["type"]["desc"];
                $bgCode         = $conf["bg"]["type"]["code"];

                $arrbgType = array_combine(array_values($bgCode), array_values($bgType));

                foreach ($arrbgType as $key => $val) {
                    $arrbgType_new[$key] =  array(
                        'ID' => $key,
                        'VALUE' => $val,
                    );
                }
                $list_bg_purpose                = array('' => ' --- ' . $this->language->_('Choose One') . ' ---');
                $list_bg_purpose                += Application_Helper_Array::listArray($arrbgType_new, 'ID', 'VALUE');
                $this->view->bgpurposeArr       = $list_bg_purpose;
                $this->view->BG_PURPOSE = $data['USAGE_PURPOSE'];

                //BG Document Type
                $bgdocType         = $conf["bgdoc"]["type"]["desc"];
                $bgdocCode         = $conf["bgdoc"]["type"]["code"];

                $arrbgdoc = array_combine(array_values($bgdocCode), array_values($bgdocType));

                foreach ($arrbgdoc as $key => $val) {
                    $arrbgdoc_new[$key] =  array(
                        'ID' => $key,
                        'VALUE' => $val,
                    );
                }
                $list_document_type                = array('' => ' --- ' . $this->language->_('Choose One') . ' ---');
                $list_document_type                += Application_Helper_Array::listArray($arrbgdoc_new, 'ID', 'VALUE');
                $this->view->documenttypeArr     = $list_document_type;
                $this->view->GT_DOC_TYPE =  $data['GT_DOC_TYPE'];


                //BG Counter Guarantee Type
                $bgcgType         = $conf["bgcg"]["type"]["desc"];
                $bgcgCode         = $conf["bgcg"]["type"]["code"];

                $arrbgcg = array_combine(array_values($bgcgCode), array_values($bgcgType));

                foreach ($arrbgcg as $key => $val) {
                    $arrbgcg_new[$key] =  array(
                        'ID' => $key,
                        'VALUE' => $val,
                    );
                }
                $list_warranty_type                = array('' => ' --- ' . $this->language->_('Choose One') . ' ---');
                $list_warranty_type                += Application_Helper_Array::listArray($arrbgcg_new, 'ID', 'VALUE');
                $this->view->warrantytypeArr     = $list_warranty_type;
                // $this->view->COUNTER_WARRANTY_TYPE = $data['COUNTER_WARRANTY_TYPE'];


                $this->view->currencyArr = array(
                    '1' => $this->language->_('IDR'),
                    '2' => $this->language->_('USD'),
                );

                // BG Publishing Form
                $bgpublishType         = $conf["bgpublish"]["type"]["desc"];
                $bgpublishCode         = $conf["bgpublish"]["type"]["code"];

                $arrbgpublish = array_combine(array_values($bgpublishCode), array_values($bgpublishType));
                foreach ($arrbgpublish as $key => $val) {
                    $arrbgpublish_new[$key] =  array(
                        'ID' => $key,
                        'VALUE' => $val,
                    );
                }
                $list_bg_publist                = array('' => ' --- ' . $this->language->_('Any Value') . ' ---');
                $list_bg_publist                += Application_Helper_Array::listArray($arrbgpublish_new, 'ID', 'VALUE');
                $this->view->BG_PUBLISH_ARR     = $list_bg_publist;



                $Settings = new Settings();
                $claim_period = $Settings->getSetting('max_claim_period');

                $this->view->maxCalendarLimit = $claim_period;

                // echo '<pre>';
                // print_r($list_bg_publist); 
                $this->view->BG_PUBLISH = $data['BG_PUBLISH'];

                $this->view->BG_FORMAT = $data['BG_FORMAT'];

                $this->view->BG_REG_NUMBER = $data['BG_REG_NUMBER'];
                $this->view->BG_NUMBER = $data['BG_NUMBER'];
                $this->view->BG_AMOUNT = $data['BG_AMOUNT'];

                $this->view->BG_SUBJECT = $data['BG_SUBJECT'];

                $this->view->BG_CLAIM_PERIOD = $data['BG_CLAIM_PERIOD'];
                $this->view->BG_UNDERLYING_DOC = $data['BG_UNDERLYING_DOC'];

                $this->view->CUST_CP = $data['CUST_CP'];
                $this->view->CUST_EMAIL = $data['CUST_EMAIL'];
                $this->view->CUST_CP = $data['CUST_CP'];
                $this->view->CUST_EMAIL = $data['CUST_EMAIL'];
                $this->view->CUST_CONTACT_NUMBER = $data['CUST_CONTACT_NUMBER'];


                $this->view->recipent_name = $data['RECIPIENT_NAME'];
                $this->view->address = $data['RECIPIENT_ADDRES'];
                $this->view->city = $data['RECIPIENT_CITY'];

                $this->view->RECIPIENT_OFFICE_NUMBER = $data['RECIPIENT_OFFICE_NUMBER'];
                $this->view->RECIPIENT_CP = $data['RECIPIENT_CP'];
                $this->view->RECIPIENT_CONTACT = $data['RECIPIENT_CONTACT'];
                $this->view->RECIPIENT_EMAIL = $data['RECIPIENT_EMAIL'];

                $this->view->SERVICE = $data['SERVICE'];
                $this->view->GT_DOC_NUMBER = $data['GT_DOC_NUMBER'];
                $this->view->GT_DOC_DATE = $data['GT_DOC_DATE'];

                $this->view->BG_CLAIM_PERIOD = $data['BG_CLAIM_PERIOD'];
                $this->view->BG_UNDERLYING_DOC = $data['BG_UNDERLYING_DOC'];

                // $this->view->contact_number = $data['RECIPIENT_CONTACT'];
                $this->view->comment = $data['GUARANTEE_TRANSACTION'];

                $this->view->fileName = $data['FILE'];
                $this->view->bank_amount = $data['BG_AMOUNT'];
                $this->view->warranty_type = $data['COUNTER_WARRANTY_TYPE'];
                $this->view->account_number = $data['COUNTER_WARRANTY_ACCT_NO'];
                $this->view->account_name = $data['COUNTER_WARRANTY_ACCT_NAME'];

                $this->view->amount = $data['COUNTER_WARRANTY_AMOUNT'];
                $this->view->acct = $data['FEE_CHARGE_TO'];
                $this->view->status = $data['BG_STATUS'];
                $this->view->TIME_PERIOD_START = $data['TIME_PERIOD_START'];
                $this->view->TIME_PERIOD_END = $data['TIME_PERIOD_END'];


                if ($data['COUNTER_WARRANTY_TYPE'] == 3) {

                    $getDataInsurance = $this->_db->select()
                        ->from("TEMP_BANK_GUARANTEE", ["BG_INSURANCE_CODE", "BG_BRANCH"])
                        ->where("COUNTER_WARRANTY_TYPE = '3'")
                        ->where("BG_REG_NUMBER = ? ", $BG_NUMBER)
                        ->query()
                        ->fetchAll();

                    $this->view->resInsuranceCode = $getDataInsurance[0]['BG_INSURANCE_CODE'];
                    $this->view->resInsuranceBranch = $getDataInsurance[0]['BG_BRANCH'];
                } elseif ($data['COUNTER_WARRANTY_TYPE'] == 1) {
                    $getFullConver = $this->_db->select()
                        ->from("TEMP_BANK_GUARANTEE_SPLIT", ["*"])
                        ->where("BG_REG_NUMBER = ? ", $BG_NUMBER)
                        ->query()
                        ->fetchAll();

                    $this->view->getFullConver = $getFullConver;
                }

                if ($data['BG_STATUS'] == '7' ||  $data['BG_STATUS'] == '10' || !empty($data['BG_REJECT_NOTES']) || !empty($data['BG_CANCEL_NOTES'])) {
                    $selectQuery  = "SELECT
                        a.USER_LOGIN,
                        b.`USER_FULLNAME` AS u_name,
                        c.`BUSER_NAME` AS b_name,
                        a.DATE_TIME,
                        a.BG_REASON,
                        a.HISTORY_STATUS,
                        a.BG_REG_NUMBER
                        
                        
                        FROM
                        T_BANK_GUARANTEE_HISTORY AS a
                        LEFT JOIN M_USER AS b ON a.`USER_LOGIN` = b.`USER_ID` AND a.`CUST_ID` = b.`CUST_ID`
                        LEFT JOIN `M_BUSER` AS c ON a.`USER_LOGIN` = c.`BUSER_ID`
                        WHERE a.BG_REG_NUMBER = " . $this->_db->quote((string) $data['BG_REG_NUMBER']) . " AND a.HISTORY_STATUS = " . $this->_db->quote((string) $data['BG_STATUS']) . " GROUP BY HISTORY_ID ORDER BY DATE_TIME";
                    $result =  $this->_db->fetchAll($selectQuery);
                    if (!empty($result)) {
                        $data['REASON'] = $result['0']['BG_REASON'];
                    }
                    $this->view->reqrepair = true;

                    // $this->view->reason = $data['REASON'] . '' . $data['BG_REJECT_NOTES'] . $data['BG_CANCEL_NOTES'];
                }

                if (!empty($bgdatadetail)) {
                    foreach ($bgdatadetail as $key => $value) {

                        if ($data['COUNTER_WARRANTY_TYPE'] == 3) {

                            if ($value['PS_FIELDNAME'] == 'Insurance Name') {
                                $this->view->insuranceName =   $value['PS_FIELDVALUE'];

                                $getInsuranceCustomer = $this->_db->select()
                                    ->from(["A" => "M_CUSTOMER"], ["CUST_ID", "CUST_NAME"])
                                    ->joinLeft(["MCLF" => "M_CUST_LINEFACILITY"], "MCLF.CUST_ID = A.CUST_ID")
                                    ->where("A.CUST_ID = ?", $value["PS_FIELDVALUE"])
                                    // ->where("A.CUST_MODEL = '2'")
                                    // ->where("A.CUST_STATUS = '1'")
                                    // ->where("MCLF.CUST_SEGMENT = ?", "4")
                                    // ->where("MCLF.STATUS = ?", "1")
                                    ->query()->fetchAll();

                                $simpanInsurance = [];

                                $simpanInsurance[0] = ["key" => "-", "value" => "-- Choose one --"];

                                foreach ($getInsuranceCustomer as $key => $customer) {
                                    $getId = $customer["CUST_ID"];
                                    $getName = $customer["CUST_NAME"];
                                    if ($customer["TICKET_SIZE"] == "1") {
                                        $ticket_size = floatval((10 / 100) * $customer["PLAFOND_LIMIT"]);
                                    } else {
                                        $ticket_size = floatval($customer["PLAFOND_LIMIT"]);
                                    }
                                    array_push($simpanInsurance, ["key" => $getId, "value" => $getName, "ticket_size" => $ticket_size]);
                                }

                                $this->view->simpanInsurance = $simpanInsurance;
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement') {
                                $this->view->PrincipalAgreement =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount') {
                                $this->view->insurance_amount =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Principal Agreement Date') {
                                $this->view->paDate =   $value['PS_FIELDVALUE'];
                            }
                        } else {

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 1') {
                                $this->view->owner1 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 1') {
                                $this->view->amountowner1 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 2') {
                                $this->view->owner2 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 2') {
                                $this->view->amountowner2 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Plafond Owner 3') {
                                $this->view->owner3 =   $value['PS_FIELDVALUE'];
                            }

                            if ($value['PS_FIELDNAME'] == 'Amount Owner 3') {
                                $this->view->amountowner3 =   $value['PS_FIELDVALUE'];
                            }
                        }
                    }
                }



                $conf = Zend_Registry::get('config');
                $this->view->bankname = $conf['app']['bankname'];



                //echo '<pre>';
                //var_dump($data);

                $download = $this->_getParam('download');
                //print_r($edit);die;
                if ($download) {
                    $attahmentDestination = UPLOAD_PATH . '/document/submit/';
                    $this->_helper->download->file($data['FILE'], $attahmentDestination . $data['FILE']);
                }

                if ($this->_request->isPost()) {
                    $approve = $this->_getParam('approve');
                    $BG_NUMBER = $this->_getParam('bgnumb');
                    $reject = $this->_getParam('reject');
                    $repair = $this->_getParam('repair');

                    $datadump = $this->_request->getParams();
                    $newamount = str_replace(',', '', $datadump['BG_AMOUNT']);


                    // if ($datadump['currency_type'] == '1') {
                    //     $selectbankamount = $this->_db->select()
                    //         ->from(array('M_MINAMT_CCY'), array('MIN_TX_AMT'))
                    //         ->where('CCY_ID = ?', 'IDR');
                    // } else {
                    //     $selectbankamount = $this->_db->select()
                    //         ->from(array('M_MINAMT_CCY'), array('MIN_TX_AMT'))
                    //         ->where('CCY_ID = ?', 'USD');
                    // }

                    $selectbankamount = $this->_db->select()
                        ->from(array('M_MINAMT_CCY'), array('MIN_TX_AMT'))
                        ->where('CCY_ID = ?', $datadump['currency_type']);

                    $databankamount = $this->_db->fetchRow($selectbankamount);



                    //$datas = $this->_request->getParams();
                    //echo '<pre>';
                    //var_dump($datas);die;

                    $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
                    if (count($temp) > 1) {
                        if ($temp[0] == 'F' || $temp[0] == 'S') {
                            if ($temp[0] == 'F')
                                $this->view->error = 1;
                            else
                                $this->view->success = 1;
                            $msg = '';
                            unset($temp[0]);
                            foreach ($temp as $value) {
                                if (!is_array($value))
                                    $value = array($value);
                                $msg .= $this->view->formErrors($value);
                            }
                            $this->view->report_msg = $msg;
                        }
                    }
                    $attahmentDestination     = UPLOAD_PATH . '/document/submit/';
                    $errorRemark             = null;
                    $adapter                 = new Zend_File_Transfer_Adapter_Http();
                    $fileExt                 = "jpeg,jpg,pdf";

                    $sourceFileName = $adapter->getFileName("uploadSource");

                    if ($sourceFileName == null) {
                        $sourceFileName = null;
                        $fileType = null;
                    } else {
                        $sourceFileName = substr(basename($adapter->getFileName("uploadSource")), 0);
                        // echo "<code>sourceFileName = $sourceFileName</code>"; die;	
                        if ($_FILES["uploadSource"]["type"]) {
                            $adapter->setDestination($attahmentDestination);
                            $maxFileSize             = $this->getSetting('Fe_attachment_maxbyte');
                            $fileType = $adapter->getMimeType("uploadSource");
                            $fileInfo = $adapter->getFileInfo("uploadSource");
                            $size = $_FILES["uploadSource"]["size"];
                        } else {
                            $fileType = null;
                            $size = null;
                        }
                    }


                    $filters = array(
                        'CUST_CP'                   => array('StripTags', 'StringTrim'),
                        'CUST_CONTACT_NUMBER'       => array('StripTags', 'StringTrim'),
                        'CUST_EMAIL'                => array('StripTags', 'StringTrim'),
                        'BG_PURPOSE'                => array('StripTags', 'StringTrim'),
                        'BG_AMOUNT'                 => array('StripTags', 'StringTrim'),
                        'updatedate'                => array('StripTags', 'StringTrim'),
                        'RECIPIENT_CP'              => array('StripTags', 'StringTrim'),
                        'RECIPIENT_CONTACT'         => array('StripTags', 'StringTrim'),
                        'RECIPIENT_EMAIL'           => array('StripTags', 'StringTrim'),
                        'SERVICE'                   => array('StripTags', 'StringTrim'),
                        'GT_DOC_TYPE'               => array('StripTags', 'StringTrim'),
                        'GT_DOC_NUMBER'             => array('StripTags', 'StringTrim'),
                        'GT_DOC_DATE'               => array('StripTags', 'StringTrim'),
                        'COUNTER_WARRANTY_TYPE'     => array('StripTags', 'StringTrim'),
                        'acct'                      => array('StripTags', 'StringTrim'),
                        'BG_PUBLISH'                => array('StripTags', 'StringTrim'),
                        'BG_FORMAT'                 => array('StripTags', 'StringTrim'),
                        'BG_LANGUAGE'               => array('StripTags', 'StringTrim'),

                    );


                    if ($newamount < $databankamount['MIN_TX_AMT']) {
                        $error_msg[] = $this->language->_('Error') . ': ' . $this->language->_('Amount cannot be lower than ') . $databankamount['MIN_TX_AMT'] . '.';
                        $this->view->error      = true;
                        $this->view->error_msg = $error_msg;


                        $validators =  array(
                            'CUST_CP'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'CUST_CONTACT_NUMBER'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'CUST_EMAIL'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'BG_PURPOSE'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'BG_AMOUNT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'updatedate'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'RECIPIENT_CP'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'RECIPIENT_CONTACT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'RECIPIENT_EMAIL'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'SERVICE'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'GT_DOC_TYPE'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'GT_DOC_NUMBER'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'GT_DOC_DATE'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'COUNTER_WARRANTY_TYPE'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'acct'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'BG_PUBLISH'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'BG_FORMAT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),

                        );

                        if ($this->_request->getPost("BG_FORMAT") == 1) {
                            $validators['language'] = array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            );
                        }

                        $zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);
                    } else {

                        $validators =  array(
                            'CUST_CP'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'CUST_CONTACT_NUMBER'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'CUST_EMAIL'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'BG_PURPOSE'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'BG_AMOUNT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'updatedate'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'RECIPIENT_CP'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'RECIPIENT_CONTACT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'RECIPIENT_EMAIL'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'SERVICE'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'GT_DOC_TYPE'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'GT_DOC_NUMBER'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'GT_DOC_DATE'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'COUNTER_WARRANTY_TYPE'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'acct'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'BG_PUBLISH'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),
                            'BG_FORMAT'      => array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            ),

                        );

                        if ($this->_request->getPost("BG_FORMAT") == 1) {
                            $validators['language'] = array(
                                'NotEmpty',
                                array('StringLength', array('min' => 1, 'max' => 50)),
                                'messages' => array(
                                    'Can not be empty',
                                    'invalid'
                                )
                            );
                        }

                        $zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);

                        if ($zf_filter_input->isValid()) {

                            // echo '<pre>';
                            // print_r($params['flselect_account_manual']);die;
                            // $dd = $zf_filter_input->COUNTER_WARRANTY_TYPE;
                            // echo "<code>dd = $dd</code>"; die;
                            if ($zf_filter_input->COUNTER_WARRANTY_TYPE == '1') {

                                if (!empty($params['flselect_account_manual'])) {
                                    $splitArr = array();
                                    foreach ($params['flselect_account_manual'] as $ky => $vl) {
                                        if ($vl == '') {
                                            $nameAcct = explode('|', $params['flselect_account_number'][$ky]);
                                            $splitArr[$ky]['acct'] = $nameAcct[0];
                                        } else {
                                            $splitArr[$ky]['acct'] = $vl;
                                        }

                                        if ($params['flname_manual'][$ky] == '') {
                                            $splitArr[$ky]['acct_name'] = $params['flname'][$ky];
                                        } else {
                                            $splitArr[$ky]['acct_name'] = $params['flname_manual'][$ky];
                                        }

                                        $splitArr[$ky]['amount'] = $params['flamount'][$ky];
                                        $splitArr[$ky]['flag'] = $params['flselect_own'][$ky];
                                        $splitArr[$ky]['bank_code'] = '999';


                                        // echo "<code>masuk = $params</code>"; 
                                    }
                                    $this->view->acctSplit = $splitArr;
                                }
                            }

                            if ($params['currency_type'] == "IDR") {

                                // echo "<code>masuk1 = $data</code><BR>"; die;
                                // echo '<pre>';
                                // print_r($params['flselect_own']);die;
                                // foreach($params['flselect_own'] as $i => $val)
                                // {
                                //     if($val == "1")
                                //     {
                                //         $arr_select_account_number = explode("|",$select_account_number[$i]);
                                //         $account_number .= $arr_select_account_number[0].',';
                                //         $account_name .= $arr_select_account_number[1].',';
                                //         $amount .= $params['flamount'][$i].',';
                                //     }
                                //     else if($val == "2")
                                //     {
                                //         $account_number .= $select_account_number[$i].',';
                                //         $account_name .= $flname_manual[$i].',';
                                //         $amount .= $params['flamount'][$i].',';
                                //     }
                                // }

                                // echo "<code>account_number = $account_number</code><BR>"; die;
                                // if($params['select_own'] == "1")
                                // {
                                // echo "<code>masuk1 = $data</code><BR>"; die;
                                $select_account_number = $params['flselect_account_number'];
                                $account_number = '';
                                $account_name = '';
                                $amount = '';
                                for ($i = 0; $i <= count($params['flselect_account_number']); $i++) {
                                    $arr_select_account_number = explode("|", $select_account_number[$i]);
                                    $account_number .= $arr_select_account_number[0] . ',';
                                    $account_name .= $arr_select_account_number[1] . ',';
                                    $amount .= $params['flamount'][$i] . ',';
                                }
                                $account_number = substr($account_number, 0, -1);
                                $account_name = substr($account_name, 0, -1);
                                $amount = substr($amount, 0, -1);

                                // }
                                // else
                                // {
                                //     // echo "<code>masuk2 = $data</code><BR>"; die;
                                //     $select_account_number = $params['flselect_account_manual'];
                                //     $flname_manual = $params['flname_manual'];

                                //     $account_number = '';
                                //     $account_name = '';
                                //     $amount = '';
                                //     for($i=0;$i<=count($params['flselect_account_manual']);$i++){
                                //         $account_number .= $select_account_number[$i].',';
                                //         $account_name .= $flname_manual[$i].',';
                                //         $amount .= $params['flamount'][$i].',';
                                //     }
                                //     $account_number = substr($account_number, 0, -1);
                                //     $account_name = substr($account_name, 0, -1);
                                //     $amount = substr($amount, 0, -1);
                                // }


                                $acct = $params['acct'];
                            }
                            // elseif ($params['currency_type'] == "2") {
                            //     $select_account_numberUSD = $params['select_account_numberUSD'];
                            //     for ($i = 0; $i <= count($params['select_account_numberUSD']); $i++) {
                            //         $arr_account_numberUSD = explode("|", $select_account_numberUSD[$i]);
                            //         $account_number .= $arr_account_numberUSD[0] . ',';;
                            //         $account_name .= $arr_account_numberUSD[1] . ',';;
                            //     }

                            //     $acct = $params['acctUSD'];
                            // }

                            $fileTypeMessage = explode('/', $fileType);
                            $fileType =  $fileTypeMessage[1];
                            $extensionValidator = new Zend_Validate_File_Extension(array(false, $fileExt));
                            $extensionValidator->setMessage("Extension file must be *.pdf");

                            $maxFileSize = "1024000";
                            $size = number_format($size);

                            $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $maxFileSize));
                            $sizeValidator->setMessage("File size is exceeding 1,024,000 byte(s), uploaded file is $size byte(s)");

                            $adapter->setValidators(array($extensionValidator, $sizeValidator));

                            if (!empty($fileInfo['uploadSource']["name"]) || empty($fileInfo['uploadSource']["name"])) {

                                if ($adapter->isValid("uploadSource")) {

                                    if ($adapter->receive("uploadSource")) {
                                        // echo "<code>masuk = $data</code>"; die;	
                                        $file_name = $fileInfo['uploadSource']["name"];
                                        // unlink($attahmentDestination.$data['BG_UNDERLYING_DOC']); 

                                    } else {
                                        $errmsgs = $adapter->getMessages();
                                        foreach ($errmsgs as $key => $val) {
                                            $errfield = "error_" . $fieldname;
                                            $errorArray[$errfield] = $val;
                                        }
                                        $this->view->error = true;
                                        $this->view->err_msg = $errmsgs;
                                        // echo '<pre>';
                                        // echo "<code>errors1 = $errors</code>"; die;	
                                        // print_r($errmsgs);die;
                                    }
                                } else {
                                    $this->view->error = true;
                                    $errors = $this->language->_('Extension file must be *.jpg /.jpeg / .pdf');
                                    $this->view->err_msg = $errors;
                                    // echo '<pre>';
                                    // echo "<code>errors2 new= $errors</code>"; die;	
                                    // print_r($errors);die;
                                }
                            } else {
                                $file_name = '';
                            }

                            // check file sebelumnya 

                            $attahmentDestination = UPLOAD_PATH . '/document/submit/';
                            $files = $adapter->getFileInfo();
                            $othersAttachment = [];

                            foreach (explode(",", $datadump["save_others_attachment"]) as $key => $value) {
                                if (array_search($value, array_column($get_others_attachment, "BG_FILE")) !== false && (!empty($get_others_attachment))) {
                                    array_push($othersAttachment, $value);
                                }
                            }

                            $specialFormatFile = "";
                            foreach ($files as $file => $fileInfo) {
                                if ($file == "uploadSource") continue;

                                if ($file == "specialFormatFile") {
                                    $sizeFile = $fileInfo['size'] + 0;
                                    if ($sizeFile == 0) continue;

                                    $fileName = $fileInfo["name"];
                                    $adapter->setDestination($attahmentDestination);

                                    $date = date("dmy");
                                    $time = date("his");

                                    $newFileName2 = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . uniqid() . "_" . $fileName;

                                    $adapter->addFilter('Rename', $newFileName2, $file);
                                    // $adapter->receive($file);
                                    move_uploaded_file($_FILES["specialFormatFile"]["tmp_name"], $attahmentDestination . $newFileName2);
                                    $specialFormatFile = $newFileName2;
                                    continue;
                                }

                                $sizeFile = $fileInfo['size'] + 0;
                                if ($sizeFile == 0) continue;

                                $fileName = $fileInfo["name"];
                                $adapter->setDestination($attahmentDestination);

                                $date = date("dmy");
                                $time = date("his");

                                $newFileName2 = $date . "_" . $time . "_" . $this->_custIdLogin . "_" . uniqid() . "_" . $fileName;

                                $adapter->addFilter('Rename', $newFileName2, $file);
                                $adapter->receive($file);
                                array_push($othersAttachment, $newFileName2);
                            }

                            if (count($othersAttachment) > 0) {

                                $this->_db->beginTransaction();
                                $this->_db->delete("TEMP_BANK_GUARANTEE_FILE", ["BG_REG_NUMBER = ?" => $numb]);

                                foreach ($othersAttachment as $key => $value) {
                                    $this->_db->insert("TEMP_BANK_GUARANTEE_FILE", [
                                        "BG_REG_NUMBER" => $numb,
                                        "BG_FILE" => $value,
                                        "INDEX" => $key
                                    ]);
                                }

                                $this->_db->commit();

                                // jika file ingin dihapus

                                // $checkOnDb = $this->_db->select()
                                // ->from(["A" => "TEMP_BANK_GUARANTEE_FILE"], ["*"])
                                // ->where("BG_REG_NUMBER = '$numb'")
                                // ->query()->fetchAll();

                                // if(count($checkOnDb) > 0) {
                                //     foreach ($checkOnDb as $key => $value) {
                                //         $getFileName = $value["BG_FILE"];
                                //         unlink($attahmentDestination.$getFileName);
                                //     }
                                // }

                                // end jika file ingin dihapus 
                            }

                            // end check file sebelumnya

                            try {
                                $this->_db->beginTransaction();
                                if ($zf_filter_input->BG_FORMAT == 1) {
                                    $BG_LANGUAGE = $zf_filter_input->language;
                                } else {
                                    $BG_LANGUAGE = 0;
                                }

                                if ($repair) {
                                    if ($zf_filter_input->BG_FORMAT != 2) {
                                        $specialFormatFile = "";
                                    }

                                    $UpdateArr = array(
                                        // 'BG_REG_NUMBER'                     => $data['BG_REG_NUMBER'],   
                                        'CUST_CP'                           => $zf_filter_input->CUST_CP,
                                        'CUST_CONTACT_NUMBER'               => $zf_filter_input->CUST_CONTACT_NUMBER,
                                        'CUST_EMAIL'                        => $zf_filter_input->CUST_EMAIL,
                                        'BG_STATUS'                         => 2,
                                        'USAGE_PURPOSE'                     => $zf_filter_input->BG_PURPOSE,
                                        'BG_AMOUNT'                         => Application_Helper_General::convertDisplayMoney($zf_filter_input->BG_AMOUNT),
                                        'PROVISION_FEE' => Application_Helper_General::convertDisplayMoney($this->_getParam("provision_fee_input")),
                                        'ADM_FEE' => Application_Helper_General::convertDisplayMoney($this->_getParam("administration_fee_input")),
                                        'STAMP_FEE' => Application_Helper_General::convertDisplayMoney($this->_getParam("stamp_fee_input")),
                                        'RECIPIENT_CP'                      => $zf_filter_input->RECIPIENT_CP,
                                        'RECIPIENT_CONTACT'                 => $zf_filter_input->RECIPIENT_CONTACT,
                                        'RECIPIENT_EMAIL'                   => $zf_filter_input->RECIPIENT_EMAIL,

                                        'SERVICE'                           => $zf_filter_input->SERVICE,
                                        'GT_DOC_TYPE'                       => $zf_filter_input->GT_DOC_TYPE,
                                        'GT_DOC_NUMBER'                     => $zf_filter_input->GT_DOC_NUMBER,
                                        'GT_DOC_DATE'                       => $zf_filter_input->GT_DOC_DATE,
                                        'TIME_PERIOD_START'                 => $zf_filter_input->updatedate[0],
                                        'TIME_PERIOD_END'                   => $zf_filter_input->updatedate[1],
                                        'BG_CLAIM_DATE' =>   date('Y-m-d', strtotime('+' . $claim_period . ' days', strtotime($zf_filter_input->updatedate[1]))),
                                        'COUNTER_WARRANTY_TYPE'             => $zf_filter_input->COUNTER_WARRANTY_TYPE,
                                        'BG_PUBLISH'                        => $zf_filter_input->BG_PUBLISH,
                                        'BG_FORMAT'                         => $zf_filter_input->BG_FORMAT,
                                        'BG_LANGUAGE'                       => $BG_LANGUAGE,
                                        'COUNTER_WARRANTY_ACCT_NO'          => rtrim($account_number, ","),
                                        'COUNTER_WARRANTY_ACCT_NAME'        => rtrim($account_name, ","),
                                        'COUNTER_WARRANTY_AMOUNT'           => Application_Helper_General::convertDisplayMoney(rtrim($amount, ",")),
                                        'FEE_CHARGE_TO'                     => $zf_filter_input->acct,

                                        'BG_UNDERLYING_DOC'                 => $file_name,
                                        'BG_CREATED'                        => new Zend_Db_Expr('now()'),
                                        'BG_CREATEDBY'                      => $this->_userIdLogin,
                                        'BG_UPDATED'                        => new Zend_Db_Expr('now()'),
                                        'BG_UPDATEDBY'                      => $this->_userIdLogin,
                                        // 'IS_AMENDMENT'                      => 0,
                                        'SPECIAL_FORMAT_DOC' => $specialFormatFile

                                    );

                                    $whereArr['BG_REG_NUMBER = ?'] = $numb;
                                    // echo '<pre>';
                                    // print_r($UpdateArr);die;
                                    $this->_db->update('TEMP_BANK_GUARANTEE', $UpdateArr, $whereArr);

                                    if ($zf_filter_input->COUNTER_WARRANTY_TYPE == '1') {
                                        // echo "<code>COUNTER_WARRANTY_TYPE_TXT = 1</code>"; die;	   
                                        $this->_db->delete('TEMP_BANK_GUARANTEE_SPLIT', ['BG_REG_NUMBER = ?' => $numb]);

                                        foreach ($splitArr as $ky => $vl) {
                                            $tmparrDetail = array(
                                                'BG_REG_NUMBER'         => $numb,
                                                'ACCT'              => $vl['acct'],
                                                'BANK_CODE'         => $vl['bank_code'],
                                                'NAME'              => $vl['acct_name'],
                                                'AMOUNT'            => Application_Helper_General::convertDisplayMoney(rtrim($vl['amount'], ",")),
                                                'FLAG'              => $vl['flag']
                                            );
                                            // echo '<pre>';
                                            // print_r($tmparrDetail);die;

                                            $this->_db->insert('TEMP_BANK_GUARANTEE_SPLIT', $tmparrDetail);
                                        }
                                    }

                                    if ($zf_filter_input->COUNTER_WARRANTY_TYPE == '3') {
                                        // echo "<code>COUNTER_WARRANTY_TYPE_TXT = 1</code>"; die;	   
                                        $this->_db->delete('TEMP_BANK_GUARANTEE_DETAIL', ['BG_REG_NUMBER = ?' => $numb]);

                                        $arrDetail = array(
                                            'BG_REG_NUMBER' => $data['BG_REG_NUMBER'],
                                            'CUST_ID' => $data['CUST_ID'],
                                            'USER_ID' =>  $this->_userIdLogin,
                                            'PS_FIELDNAME' => 'Insurance Name',
                                            'PS_FIELDTYPE' => 1,
                                            'PS_FIELDVALUE' => $this->_request->getParam("insuranceName")
                                        );
                                        $this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL', $arrDetail);

                                        $arrDetail = array(
                                            'BG_REG_NUMBER' => $data['BG_REG_NUMBER'],
                                            'CUST_ID' => $data['CUST_ID'],
                                            'USER_ID' =>  $this->_userIdLogin,
                                            'PS_FIELDNAME' => 'Insurance Branch',
                                            'PS_FIELDTYPE' => 1,
                                            'PS_FIELDVALUE' => $this->_request->getParam("insuranceBranch")
                                        );
                                        $this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL', $arrDetail);

                                        // if ($this->_request->getParam("currency_type") == "1") {
                                        //     $Currency = "IDR";
                                        // } elseif ($data['currency_type'] == "2") {
                                        //     $Currency = "USD";
                                        // }

                                        $arrDetail = array(
                                            'BG_REG_NUMBER' => $data['BG_REG_NUMBER'],
                                            'CUST_ID' => $data['CUST_ID'],
                                            'USER_ID' =>  $this->_userIdLogin,
                                            'PS_FIELDNAME' => 'Currency',
                                            'PS_FIELDTYPE' => 1,
                                            'PS_FIELDVALUE' => $this->_request->getParam("currency_type")
                                        );

                                        $this->_db->insert('TEMP_BANK_GUARANTEE_DETAIL', $arrDetail);
                                    }
                                }

                                $historyInsert = array(
                                    'DATE_TIME'         => new Zend_Db_Expr("now()"),
                                    'BG_REG_NUMBER'     => $numb,
                                    'CUST_ID'           => $this->_custIdLogin,
                                    'USER_LOGIN'        => $this->_userIdLogin,
                                    'HISTORY_STATUS'    => 1
                                );

                                $this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);
								
								$this->_db->delete('T_APPROVAL', ['PS_NUMBER = ?' => $numb]);
								
                                $this->_db->commit();
                                // $this->setbackURL('/'.$this->_request->getModuleName().'/');
                                $this->setbackURL('/' . $this->_request->getModuleName() . '/repair/');
                                $this->_redirect('/notification/success');
                            } catch (Exception $e) {

                                echo '<pre>';
                                print_r($e);
                                die;
                                // echo "<code>masuk3 = $data</code><BR>"; die;	
                                $this->_db->rollBack();
                                $this->view->errorMsg = array(array($this->language->_('An Error Occured. Please Try Again.')));
                            }
                        } else {
                            // echo "<code>masuk4 = $data</code><BR>"; die;	
                            $error = $zf_filter_input->getMessages();
                            // $error = true;



                            //format error utk ditampilkan di view html 
                            $errorArray = null;
                            foreach ($error as $keyRoot => $rowError) {
                                foreach ($rowError as $errorString) {
                                    $errorArray[$keyRoot] = $errorString;
                                }
                            }
                            $this->view->report_msg = $errorArray;
                            // echo '<pre>';
                            // print_r($errorArray);die;
                        }
                        //print_r($edit);die;
                        if ($approve) {
                            $data = array('BG_STATUS' => '3');
                            $where['BG_REG_NUMBER = ?'] = $BG_NUMBER;
                            $this->_db->update('T_BANK_GUARANTEE', $data, $where);

                            $historyInsert = array(
                                'DATE_TIME'         => new Zend_Db_Expr("now()"),
                                'BG_REG_NUMBER'         => $BG_NUMBER,
                                'CUST_ID'           => $this->_custIdLogin,
                                'USER_LOGIN'        => $this->_userIdLogin,
                                'HISTORY_STATUS'    => 3
                            );

                            $this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);

                            $this->setbackURL('/' . $this->_request->getModuleName() . '/review/');
                            $this->_redirect('/notification/success/index');
                        }

                        if ($reject) {
                            $data = array('BG_STATUS' => '11');
                            $where['BG_REG_NUMBER = ?'] = $BG_NUMBER;
                            $this->_db->update('T_BANK_GUARANTEE', $data, $where);

                            $notes = $this->_getParam('PS_REASON_REJECT');
                            $historyInsert = array(
                                'DATE_TIME'         => new Zend_Db_Expr("now()"),
                                'BG_REG_NUMBER'         => $BG_NUMBER,
                                'CUST_ID'           => $this->_custIdLogin,
                                'USER_LOGIN'        => $this->_userIdLogin,
                                'HISTORY_STATUS'    => 11,
                                'BG_REASON'         => $notes,
                            );

                            $this->_db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);

                            $this->setbackURL('/' . $this->_request->getModuleName() . '/review/');
                            $this->_redirect('/notification/success/index');
                        }
                    }
                    // $back = $this->_getParam('back');
                    // if($back)
                    // {
                    //     $this->_redirect('/eformworkflow/review');
                    // }


                }
            }
        }
    }

    public function cobaAction()
    {
        $total = 100;

        $Date1 = date("Y-m-d H:i:s");
        $date = new DateTime($Date1);

        while ($total > 0) {
            $date->modify('+1 day');
            $Date2 = $date->format('Y-m-d');
            $check_weekend = date("N", strtotime($Date2));

            if ($check_weekend < 6) {
                $total--;
            }
        }

        $this->_db->update("T_BANK_GUARANTEE", [
            "BG_CG_DEADLINE" => $Date2
        ], [
            "BG_REG_NUMBER = ?" => "992616101F4201"
        ]);

        var_dump($Date2);
        die();

        $test = new Service_Account('1601300020088', '');
        $info = $test->inquiryAccontInfo();
        $girosaving = $test->inquiryAccountBalance();
        $deposito = $test->inquiryDeposito();

        echo "<pre>";
        var_dump($info, $girosaving, $deposito);
        echo "</pre>";
        die();


        // $path_file = "/app/bg/library/data/temp/2022-07-28-85b4ce22-13ac-41e3-bf72-1731abe2f95f.pdf";
        $path_file = "/app/bg/library/data/temp/baru123.pdf";
        // $path_file = "/app/bg/library/data/temp/testing123.pdf";
        $filecontent = file_get_contents($path_file);

        $command = "gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.6  -dPDFSETTINGS=/prepress -dNOPAUSE -dQUIET -dBATCH -sOutputFile=/app/bg/library/data/temp/baru123.pdf " . $path_file . "";

        // gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dNOPAUSE -dQUIET -dBATCH -sOutputFile=new-pdf1.5.pdf original.pdf
        // $p_result =  exec($command);

        var_dump($filecontent);
        die();

        if (preg_match("/^%PDF-1.5/", $filecontent)) {
            echo "Valid pdf";
        } else {
            echo "In Valid pdf";
        }
        // var_dump(file_exists($path_file));
        die();

        die();
        $svcCekStatus = new SGO_Soap_ClientUser();
        $svcCekStatus->callapi("inqaccount", null, [
            "account_number" => "101400135552"
            // "ref_trace" => "220728026908",
            // "trans_code" => "8774"
        ]);

        var_dump($svcCekStatus->getResult());
        die();

        // die("Hello");

        // $this->_db->update("T_BANK_GUARANTEE_SPLIT", [
        //     "AMOUNT" => round(12000000, 2)
        // ], [
        //     "BG_REG_NUMBER = '149616101F4201' AND ACCT_DESC = 'Escrow'"
        // ]);

        // die();
        // $get_temp_bank_guarantee = $this->_db->select()
        //     ->from("TEMP_BANK_GUARANTEE")
        //     ->where('DOCUMENT_ID = ? ', "96d69090-efe5-4e3b-b58f-65614bae9714")
        //     ->query()->fetch();
        // // ->where('DOCUMENT_ID = ? ', "96a9d028-47a6-4e29-b0f6-b8e2af140967");

        // unset($get_temp_bank_guarantee["NTU_COUNTING"]);
        // if (array_key_exists("SAVE_LINK", $get_temp_bank_guarantee)) {
        //     unset($get_temp_bank_guarantee["SAVE_LINK"]);
        // }

        // // $currentDate = date("ymd");
        // // $seqNumber   = mt_rand(1111, 9999);
        // // $bg_number_gen = strval($get_temp_bank_guarantee["BG_BRANCH"]) . strval($currentDate) . strval($get_temp_bank_guarantee["USAGE_PURPOSE"]) . strval($seqNumber);

        // $get_temp_bank_guarantee["BG_ISSUED"] = new Zend_Db_Expr("now()");

        // if ($get_temp_bank_guarantee["COUNTER_WARRANTY_TYPE"] == 3) {
        //     $get_detail_insurance = $this->_db->select()
        //         ->from("TEMP_BANK_GUARANTEE_DETAIL")
        //         ->where("BG_REG_NUMBER = ?", $get_temp_bank_guarantee["BG_REG_NUMBER"])
        //         ->where("PS_FIELDNAME = ?", "Insurance Name")
        //         ->query()->fetch();

        //     $get_detail_lf = $this->_db->select()
        //         ->from("M_CUST_LINEFACILITY")
        //         ->where("CUST_ID = ?", $get_detail_insurance["PS_FIELDVALUE"])
        //         ->query()->fetch();


        //     $get_temp_bank_guarantee["BG_CG_DEADLINE"] = date("Y-m-d", strtotime(date("Y-m-d") . " + " . ($get_detail_lf["DOC_DEADLINE"] - 1) . " days"));
        // }

        // $get_temp_bank_guarantee["BG_STATUS"] = "15";
        // $get_temp_bank_guarantee["COMPLETION_STATUS"] = "2";

        // $get_temp_bank_guarantee["REBATE_STATUS"] = $get_temp_bank_guarantee["REDEBATE_STATUS"];
        // $get_temp_bank_guarantee["REBATE_TYPE"] = $get_temp_bank_guarantee["REDEBATE_TYPE"];
        // $get_temp_bank_guarantee["REBATE_AMOUNT"] = $get_temp_bank_guarantee["REDEBATE_AMOUNT"];
        // unset($get_temp_bank_guarantee["REDEBATE_STATUS"]);
        // unset($get_temp_bank_guarantee["REDEBATE_TYPE"]);
        // unset($get_temp_bank_guarantee["REDEBATE_AMOUNT"]);
        // unset($get_temp_bank_guarantee["IS_TRANSFER_COA"]);
        // unset($get_temp_bank_guarantee["TRANSFER_COA_TIMEOUT"]);
        // unset($get_temp_bank_guarantee["IS_VALID"]);

        // $this->_db->insert("T_BANK_GUARANTEE", $get_temp_bank_guarantee);

        // die();
        // $svctemp = new Service_Account("1601306666109", "IDR");
        // $save_name = $svctemp->inquiryAccontInfo();
        // $balance = $svctemp->inquiryAccountBalance();

        // $svctemp1 = new Service_Account("1601306666109", "IDR");
        // $save_name1 = $svctemp1->inquiryAccontInfo()["account_name"];
        // $balance1 = $svctemp1->inquiryAccountBalance();

        // // var_dump($balance);
        // // die();

        // $temp_param = [
        //     "SOURCE_ACCOUNT_NUMBER" => "1601306666109",
        //     "SOURCE_ACCOUNT_NAME" => $save_name1,
        //     "BENEFICIARY_ACCOUNT_NUMBER" => "4801300004657",
        //     "BENEFICIARY_ACCOUNT_NAME" => $save_name,
        //     "PHONE_NUMBER_FROM" => "08123123",
        //     "AMOUNT" => "10000",
        //     "PHONE_NUMBER_TO" => "0825312312",
        //     "RATE1" => "10000000",
        //     "RATE2" => "10000000",
        //     "RATE3" => "10000000",
        //     "RATE4" => "10000000",
        //     "DESCRIPTION" => "Pemindahan Dana",
        //     "DESCRIPTION_DETAIL" => "Biaya Asuransi",

        // ];

        // $temp_service =  new Service_TransferWithin($temp_param);
        // $temp_result = $temp_service->sendTransfer();

        // var_dump($temp_result, $balance, $balance1);
        // die();

        // $checkQuota = new SGO_Soap_ClientUser();
        // $checkQuota->callapi("checkquota", null);
        // var_dump($checkQuota->getResult());
        // die();

        // $app      = Zend_Registry::get('config');
        // $bankCode = $app['app']['bankcode'];


        // $svcAccount_temp = new Service_Account("101400135170", "IDR", $bankCode, null, null, null, "EBG19");
        // // $temp_lock_deposito = $svcAccount_temp->lockDeposito(["balance" => "100000"]);
        // $temp_lock_deposito = $svcAccount_temp->inquiryDeposito();

        // var_dump($temp_lock_deposito);
        // die();


        $svcTransferCoa = new SGO_Soap_ClientUser();
        // $svcTransferCoa->callapi("inquiryrate", null, [
        //     "currency" => "IDR"
        // ]);


        $svcTransferCoa->callapi("transfercoa", null, [
            "source_account_currency" => "IDR",
            "source_account_number" => "1601306666109",
            "source_amount" => "600000", //debit amount
            "beneficiary_account_currency" => "IDR",
            "beneficiary_account_number" => "0",
            "branch_code" => "161",
            "beneficiary_amount" => "3000000", //credit amount
            "fee1" => "2000000",
            "fee2" => "1000000",
            "rate1" => "10000000",
            "rate2" => "10000000",
            "rate3" => "10000000",
            "rate4" => "10000000",
            "rate5" => "10000000",
            "remark1" => "Provision Fee",
            "remark2" => "Administration Fee",
            "remark3" => "Stamp Fee",
            "gl_account1" => "467115496250", //467115496250
            "gl_account2" => "46794840", //46794840
            "gl_account3" => "189991081011", //189991081011
            "message" => "Transfer COA"
        ]);


        header("Content-type: application/json");
        echo $svcTransferCoa->getResultPure();
        die();
    }


    public function getstatuslfAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        $custid = $this->_getParam('custid');

        // get linefacillity

        $get_linefacility = $this->_db->select()
            ->from("M_CUST_LINEFACILITY")
            ->where("CUST_ID = ?", $custid)
            ->query()->fetchAll();

        //$result[];//json_encode(["linefacility" => $get_linefacility]);
        //$result = $get_linefacility;

        echo json_encode($get_linefacility[0]); // json_encode($get_linefacility,JSON_UNESCAPED_SLASHES);
    }


    public function inquirydepositoAction()
    {

        $app = Zend_Registry::get('config');
        $app = $app['app']['bankcode'];

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $acct_no = $this->_getParam('acct_no');
        //echo $acct_no;

        $svcAccount = new Service_Account($acct_no, Application_Helper_General::getCurrNum('IDR'), $app, '1');
        $result = $svcAccount->inquiryDeposito('AB', TRUE);
        //var_dump($result);

        $data['data'] = false;
        is_array($return) ? $return :  $return = $data;
        echo json_encode($result);
    }

    public function inquiryaccountbalanceAction()
    {

        $app = Zend_Registry::get('config');
        $app = $app['app']['bankcode'];

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $acct_no = $this->_getParam('acct_no');
        //echo $acct_no;

        $svcAccount = new Service_Account($acct_no, Application_Helper_General::getCurrNum('IDR'), $app, '1');
        $result = $svcAccount->inquiryAccountBalance('AB', TRUE);
        //var_dump($result);
        //die();
        $data['data'] = false;
        is_array($return) ? $return :  $return = $data;
        echo json_encode($result);
    }

    public function inquiryaccountinfoAction()
    {

        //integrate ke core
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        $acct_no = $this->_getParam('acct_no');
        $svcAccount = new Service_Account($acct_no, null);
        $result = $svcAccount->inquiryAccontInfo('AI', TRUE);

        if ($result['response_desc'] == 'Success') //00 = success
        {
            $svcAccountCIF = new Service_Account($result['account_number'], null, null, null, null, $result['cif']);
            $result2 = $svcAccountCIF->inquiryCIFAccount();

            $filterBy = $result['account_number']; // or Finance etc.
            $new = array_filter($result2['accounts'], function ($var) use ($filterBy) {
                return ($var['account_number'] == $filterBy);
            });

            $singleArr = array();
            foreach ($new as $key => $val) {
                $singleArr = $val;
            }
        }

        if ($singleArr['type_desc'] == 'Deposito') {
            $svcAccountDeposito = new Service_Account($result['account_number'], null, null, null, null, null);
            $result3 = $svcAccountDeposito->inquiryDeposito();
        } else {
            $svcAccountBalance = new Service_Account($result['account_number'], null, null, null, null, null);
            $result3 = $svcAccountBalance->inquiryAccountBalance();
        }
        $return = array_merge($result, $singleArr, $result3);

        $data['data'] = false;
        is_array($return) ? $return :  $return = $data;


        echo json_encode($return);
    }
}
