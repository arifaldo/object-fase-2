<?php
class Practice_Bootstrap extends Zend_Application_Module_Bootstrap
{
    protected function _initView()
    {
        $router = new Zend_Controller_Router_Rewrite();
        $request =  new Zend_Controller_Request_Http();
        $request->setParam('test', 'coba');
        $router->route($request);

        $module = $request->getModuleName();
        $contoller = $request->getControllerName();
        $action = $request->getActionName();

        // $module = ($module == 'default') ? 'home' : $module;

        $view = new Zend_View();
        // $view->headTitle(' : ' . $module);

        return $view;
    }
}
