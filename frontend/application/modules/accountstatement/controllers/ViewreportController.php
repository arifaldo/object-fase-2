<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
class accountstatement_ViewreportController extends Application_Main
{
	protected $_moduleDB = 'RTF';
	protected $_acctArr = array();
	protected $_cache;
	
	public function initController()
	{
		$Customer = new CustomerUser($this->_custIdLogin,$this->_userIdLogin);
		$this->_acctArr = $Customer->getAccounts();	
		$this->view->AccArr =  $this->_acctArr;
		$frontendOptions = array(
            'cache_id_prefix' 			=> 'test', 
            'lifetime' 					=> 3600,
            'automatic_serialization' 	=> true
        );

		$backendOptions = array('cache_dir' => LIBRARY_PATH.'/data/cache/transactionHistory');
		$this->_cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
		$set = new Settings();
		$range_statement = $set->getSettingVal('range_statement');
		$this->view->range_statement = $range_statement;		
	}
	
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$this->view->PERIOD 		= 'today';
		Application_Helper_General::writeLog('ACDT', 'Viewing Transaction Inquiry');
		$hisData = $this->_db->fetchAll(
					$this->_db->select()
					->from('T_HISTORY_STATEMENT',array('*'))
					->where('HIS_USER_ID = '.$this->_db->quote($this->_userIdLogin))
					->where('HIS_TYPE = ?','1')
					->order('ID DESC')
					->limit(5)
				);
		$this->view->historyData = $hisData;

		$hisDataFile = $this->_db->fetchAll(
					$this->_db->select()
					->from('T_HISTORY_STATEMENT',array('*'))
					->where('HIS_USER_ID = '.$this->_db->quote($this->_userIdLogin))
					->where('HIS_TYPE = ?','2')
					->order('ID DESC')
					->limit(5)
				);
		$this->view->historyDataFile = $hisDataFile;

		$hisDatastate = $this->_db->fetchAll(
					$this->_db->select()
					->from('T_HISTORY_STATEMENT',array('*'))
					->where('HIS_USER_ID = '.$this->_db->quote($this->_userIdLogin))
					->where('HIS_TYPE = ?','3')
					->order('ID DESC')
					->limit(5)
				);
		$this->view->historyDatastate = $hisDatastate;
		$this->view->DATE_FROM = date('d/m/Y');
		 
		
		if($this->_getParam('statement')){
			
			$account  = $this->_getParam('ACCTSRCT');

		$isNew = (empty($page) && empty($csv) && empty($pdf))? true: false;	// isNew = true, then inquiry to host
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;

		$filterArr = array	(
								'ACCTSRCT'		=> 	array('StringTrim','StripTags')
							);
		$listAccValidate = Application_Helper_Array::simpleArray($this->_acctArr,'ACCT_NO');
		$validators = array(
								'periodtype' => array(
													'NotEmpty',
													array('InArray', array('haystack' => array('today', 'history'))),
													'messages' => array($this->language->_('Error: You must select Period'),
																		//'Error: You must select Period',
																		//'Error: Period must be today or history',
																		$this->language->_('Error: Period must be today or history'),
																		),
												),
								'ACCTSRCT' => array(
													'NotEmpty',
													array('InArray', array('haystack' => $listAccValidate)),
													'messages' => array($this->language->_('Error: You must select account'),
																		//'Error: You must select account',
																		$this->language->_('Error: You dont have right to this account'),
																		//'Error: You dont have right to this account',
																	),
												)
							);
//echo $account;die;
		$zf_filter_input = new Zend_Filter_Input($filterArr,$validators,$this->_request->getParams(),$this->_optionsValidator);
	
		$period = $zf_filter_input->periodtype;
		
		$dateFrom = '';
		$dateTo   = '';
		if ($period == 'history')
		{
			$dateFrom = $this->_getParam('history_from');
			$dateTo   = $this->_getParam('history_to');
			if(!empty($dateFrom))
			{
				$this->view->DATE_FROM		=$dateFrom;
				$FormatDate = new Zend_Date($dateFrom, $this->_dateDisplayFormat);
				$dateFromView  = $FormatDate->toString($this->_dateViewFormat);
				$this->view->DATE_FROM_view	= $dateFromView;
				$dateFrom  = $FormatDate->toString($this->_dateDBFormat);
			}
			if(!empty($dateTo))
			{
				$this->view->DATE_TO		= $dateTo;
				$FormatDate = new Zend_Date($dateTo, $this->_dateDisplayFormat);
				$dateToView    = $FormatDate->toString($this->_dateViewFormat);
				$this->view->DATE_TO_view	= $dateToView;
				$dateTo    = $FormatDate->toString($this->_dateDBFormat);
			}
		}
		else{
			$FormatDate = Zend_Date::now();
			$dateNow    = $FormatDate->toString($this->_dateViewFormat);
			$this->view->DATE_FROM_view = $dateNow;
			$this->view->DATE_TO_view = $dateNow;
		}

		$this->view->ACCTSRCT 		= $zf_filter_input->ACCTSRCT;
		$this->view->PERIOD 		= $period;

		if (!empty($back))
		{
			$this->render( 'index' );
		}
		else
		{
			$monthnow = ltrim(date('m'), '0');
			$yearnow = date('Y');

			$monthVal   = sprintf("%02d", $this->_getParam('monthVal'));
			$yearVal   = $this->_getParam('yearVal');



			$datestart = date($yearVal.'-'.$monthVal.'-01');
			$dateend = date($yearVal.'-'.$monthVal.'-t');
			
		    // $this->_redirect('/accountstatement/statement');
			// $file = file_get_contents('/files/');
			// file_put_contents($files, $file);
			// $daat = fopen("/files/".$files, 'r');
			// print_r($daat);die;
						// file_put_contents($files, fopen("/files/".$files, 'r'));
			// print_r($monthnow);
			// print_r($monthVal);die;
			$errorMsg = "";
			if($monthVal>=$monthnow && $yearVal>=$yearnow){
				$errorMsg = $this->language->_('Error: e-Statement not yet available for selected period');
			}elseif ($period == 'history' && (empty($dateFrom) || empty($dateTo)))
			{
				$errorMsg = $this->language->_('Error: History date range has not been chosen yet');
			}
			elseif ($period == 'history')
			{
				$dateFromDB = Application_Helper_General::convertDate($dateFrom, $this->_dateDBFormat, $this->_dateDisplayFormat);
				$dateToDB   = Application_Helper_General::convertDate($dateTo, $this->_dateDBFormat, $this->_dateDisplayFormat);
				$dateHistory = Application_Helper_General::convertDate((date("d/m/Y")), $this->_dateDBFormat, $this->_dateDisplayFormat);
//				$dateDiff	= Application_Helper_General::date_diff_days($dateFromDB, $dateHistory);
				$dateDiff	= Application_Helper_General::date_diff_days($dateFromDB, $dateToDB);

				$maxInq 	= date('Y-m-d', strtotime('-89 days', strtotime(date('Y-m-d'))));
				/*
				if (strtotime($dateFromDB) > strtotime($dateToDB))
				{
					$errorMsg = $this->language->_('Error: From Date must not be greater than To Date');
				}
				else
				if (strtotime($dateToDB) > strtotime(date('Y-m-d')))
				{
					//$errorMsg = $this->language->_('Error: History period must be less than today');
					$errorMsg = $this->language->_('Error: End date greater than today date');
				}
				else */
				if ($dateDiff > 31)
				{
					$errorMsg = $this->language->_('Error: Account statement are retained for the last 31 days');
				}
				elseif ($dateFromDB < $maxInq)
				{
					$errorMsg = $this->language->_('Error: Account statement are retained for the last 90 days');
				}
			}



			if($zf_filter_input->isValid() && $errorMsg == "")
			{



					$data =  new Service_Account($zf_filter_input->ACCTSRCT,'IDR');
					$dataInquiry = $data->accountInquiry('AB',FALSE); 
					$dataAccountType = $dataInquiry['AccountType'];
					// print_r($datestart);echo " ";
					// print_r($dateend);
					$dataStatement = $data->accountStatementHistory($datestart, $dateend, $isMobile = FALSE, $dataAccountType); 


					// $sessionData->Detail 			= $dataStatement['Detail'];	
					// $sessionData->OpeningBalance 	= $dataStatement['OpeningBalance'];	
					// $sessionData->ClosingBalance 	= $dataStatement['ClosingBalance'];	
					// $sessionData->HoldAmount 		= $dataInquiry['HoldAmount'];	

					// $dataStatement['Detail'] 			= $sessionData->Detail;
					$dataStatement['OpeningBalance'] 	= $dataStatement['OpeningBalance'];	
					$dataStatement['ClosingBalance'] 	= $dataStatement['ClosingBalance'];	
					$dataInquiry['HoldAmount'] 			= $dataInquiry['HoldAmount'];	
				
				//echo "<pre>"; print_r($dataStatement);	die();
				
					$dataDetails = array();
					$totalDebet = "";
					$totalKredit = "";

					foreach ($dataStatement['Detail'] as $dt => $datadtl) {
						if ($datadtl->DBCR == 'D')
						{
							$dataDetails[$dt]['amount']  = Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['type']  = "DB";
							$totalDebet = $totalDebet + $datadtl->Amount;
						}
						else
						{
							$dataDetails[$dt]['amount']  =  Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['type']   = "CR";

							$totalKredit = $totalKredit + $datadtl->Amount;
						}

						$dataDetails[$dt]['ccy']   = Application_Helper_General::getCurrCode($datadtl->CCY);
						$dataDetails[$dt]['desc']   = $datadtl->Description;
						$dataDetails[$dt]['date']   = $datadtl->Date;
						$dataDetails[$dt]['balance']   = Application_Helper_General::displayMoney($datadtl->Balance);

						
// 							if( $dt == 0 )
// 							{
// //								$dataStatement['Detail'][$dt]->Balance = (int) $dataStatement['ClosingBalance'];
// 								$dataStatement['Detail'][$dt]->Balance = $dataStatement['ClosingBalance'];
// 							}
// 							else
// 							{
// 								$credit = ($dataStatement['Detail'][$dt-1]->DBCR == 'C') ? $dataStatement['Detail'][$dt-1]->Amount : 0;
// 								$debet = ($dataStatement['Detail'][$dt-1]->DBCR == 'D') ? $dataStatement['Detail'][$dt-1]->Amount : 0;
// //								$dataStatement['Detail'][$dt]->Balance = (int) ($dataStatement['Detail'][$dt-1]->Balance + $debet - $credit);
// 								$dataStatement['Detail'][$dt]->Balance = ($dataStatement['Detail'][$dt-1]->Balance + $debet - $credit);
// 							}
							
							
							$credit = ($dataStatement['Detail'][$dt]->DBCR == 'C') ? $dataStatement['Detail'][$dt]->Amount : 0;
							$debet = ($dataStatement['Detail'][$dt]->DBCR == 'D') ? $dataStatement['Detail'][$dt]->Amount : 0;
							$openingBalance = $dataStatement['Detail'][$dt]->Balance  + $debet - $credit;
					}

					$dataStatement['Detail'] = array_reverse($dataStatement['Detail'], true); //di sorting
					

			$this->view->resultdata = $dataDetails;


			$this->view->openbalance = $openingBalance;
			$this->view->totalcredit = $totalKredit;
			$this->view->totaldebet = $totalDebet;
			$selectCust = $this->_db->select()
					 ->from(array('C' => 'M_CUSTOMER'));
					$selectCust->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin));
					$dataCust = $this->_db->fetchRow($selectCust);

				$custname=$dataCust['CUST_NAME'];

				$this->view->cust = $dataCust;
				$this->view->acct = $zf_filter_input->ACCTSRCT;
				$this->view->hal = '1/1';
				$this->view->period = $monthVal.'-'.$yearVal;
			
			// echo '<pre>';
					// print_r($this->_request->getParams());die;
					// print_r($dataStatement);die;
			// die;

				
					// /files/<?=$file

			// $file = "logs/{$session->username}.txt";

		    // if(!file_exists($file)) die("I'm sorry, the file doesn't seem to exist.");

		    // $type = filetype($file);
		    // print_r($file);die;
		    // Get a date and timestamp
		    $today = date("F j, Y, g:i a");
		    $time = time();

// header("Content-type:application/pdf");
		      // header("Pragma: public");
        // header("Expires: 0");
        // header("Accept-Ranges: bytes");
        // header("Connection: keep-alive");
        // header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        // header("Cache-Control: public");
        // header("Content-type: application/zip");
        // header("Content-Description: File Transfer");
$month = array("",$this->language->_("January"),
                        $this->language->_("February"),
                        $this->language->_("March"),
                        $this->language->_("April"),
                        $this->language->_("May"),
                        $this->language->_("June"),
                        $this->language->_("July"),
                        $this->language->_("August"),
                        $this->language->_("September"),
                        $this->language->_("October"),
                        $this->language->_("November"),
                        $this->language->_("December"));

$name = $zf_filter_input->ACCTSRCT."".$month[$monthVal]."".$yearVal.".pdf";
$src = 'xxxxxxx'.substr($zf_filter_input->ACCTSRCT, -4);
$namefile = 'eStatement_'.$src."_".$yearVal."_".$monthVal;
			
					$outputHTML = "<tr><td>".$this->view->render($this->view->controllername.'/pdf.phtml')."</td></tr>";
			// Zend_Debug::dump($outputHTML);die;

				$this->_helper->download->pdfstatement(null,null,null,$namefile,$outputHTML,true,$namefile);
				$file = LIBRARY_PATH.'/data/temp/'.$namefile.'.pdf';	
			// var_dump($file);die;
   while (ob_get_level()) {
			    ob_end_clean();
			}
		    header('Content-Description: File Transfer');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        // header('Content-Disposition: attachment; filename =' . $filename);
		    header("Content-Disposition: attachment;filename=".$namefile.".pdf");
		    header("Content-Transfer-Encoding: binary");
		    // header('Pragma: no-cache');
		    header('Expires: 0');
		    // Send the file contents.
		    set_time_limit(0);
		    // echo $file;die;
		    readfile($file);

				$dateFromDB = '01-'.$monthVal.'-'.$yearVal;
					$d = new DateTime( $dateFromDB );
					$date = $d->format( 't' );
					$dateToDB = $date.'-'.$monthVal.'-'.$yearVal;


				$ccy 		= $accData['CCY_ID'];
				$ACCT_TYPE 	= $accData['ACCT_TYPE'];

				//echo "<pre>"; print_r($dataStatement);	die();

				$dataDetails = array();
				$totalDebet = "";
				$totalKredit = "";

				// Send Email
				$selectCust = $this->_db->select()
					 ->from(array('C' => 'M_CUSTOMER'));
					$selectCust->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin));
					$dataCust = $this->_db->fetchRow($selectCust);

				$custname=$dataCust['CUST_NAME'];

				$this->view->cust = $dataCust;
				$this->view->acct = $zf_filter_input->ACCTSRCT;
				$this->view->hal = '1/1';
				$this->view->period = $monthVal.'-'.$yearVal;

				foreach($this->_acctArr as $key=>$row){
					if ($row['ACCT_NO'] == $zf_filter_input->ACCTSRCT){
						$email = $row['EMAIL'];
						$accountNo = $row['ACCT_NO']." [".$row['CCY_ID']."] - ".$row['ACCT_NAME']." / ".$row['ACCT_ALIAS_NAME']." (".$row['DESC'].")";
					}
				}

				$inserthistory = array(
		                      'HIS_ACCT'    => $zf_filter_input->ACCTSRCT,
		                      'HIS_MONTH'    => $monthVal,
		                      'HIS_YEAR'    => $yearVal,
		                      'HIS_USER_ID'    => $this->_userIdLogin,
		                      'HIS_FILE'    => NULL,
		                      'HIS_TYPE'    => '3'
		                   		 );
	             $this->_db->insert('T_HISTORY_STATEMENT',$inserthistory);


				/*$FEmailEstatement = $set->getSetting('femailtemplate_estatement');

				$data = array( '[[user_fullname]]' => $isi['USER_FULLNAME'],
								'[[comp_accid]]' => $isi['CUST_ID'],
								'[[user_login]]' => $isi['USER_ID'],
								'[[user_email]]' => $isi['USER_EMAIL'],
								'[[master_bank_name]]' => $templateEmailMasterBankName,
								'[[master_bank_telp]]' => $templateEmailMasterBankTelp,
								'[[master_bank_email]]' => $templateEmailMasterBankEmail

								);

				$FEmailEstatement = strtr($FEmailEstatement,$data);*/
	


				$FEmailEstatement = "Yang Terhormat $custname,
									<br><br>
									Berikut ini adalah e-Statement Rekening Anda $accountNo periode bulan $month[$monthVal] $yearVal.

									<br><br>
									Simpan alamat email eStatement@bankpermata.com pada Address Book email Anda agar informasi e-Statement tidak masuk ke dalam Junk Mail/Spam.";


				//$status = Application_Helper_Email::sendEmailAttachement($isi['USER_EMAIL'],'Reset Password Information',$FEmailResetPass,$file);

				$status = Application_Helper_Email::sendEmailAttachement($email,'E-Statement',$FEmailEstatement,$file,$name);


				//end send email

				if ($csv || $pdf) {
					$dataDetailsOri = $dataStatement['Detail'];
				} else {
					$this->paging($dataStatement['Detail']);
					$dataDetailsOri = $this->view->paginator;
				}

				$date_language = $this->language->_('Date');
				$description_language = $this->language->_('Description');
				$ccy_language = $this->language->_('CCY');
				$debet_language = $this->language->_('Debet');
				$credit_language = $this->language->_('Credit');
				$amount_language = $this->language->_('Amount');
				$balance_language = $this->language->_('Balance');

				//$header = array($date_language, $description_language, $ccy_language, $debet_language, $credit_language);


				if ($period == 'history')
				{
					//$header[] = 'Balance';
				}

				$dataDetails = array();
				//Zend_Debug::dump($dataDetailsOri); die;
				$totalDebet = "";
				$totalKredit = "";


				$this->view->ccy 			= $ccy;
				$this->view->header			= $header;
				$this->view->query_string_params = $stringParam;
				$this->view->pdf 			= ($pdf)? true: false;
				$this->updateQstring();


				// if(!empty($pdf))
				// {

				// 	$logDesc .= '. Print PDF';
				// 	Application_Helper_General::writeLog('ACDT', $logDesc);
				// 	$outputHTML = "<tr><td>".$this->view->render($this->view->controllername.'/view.phtml')."</td></tr>";
				// 	$this->_helper->download->pdf(null,null,null,'&nbsp;',$outputHTML);
				// }

				Application_Helper_General::writeLog('ACDT', $logDesc);
			}
			else
			{
				$this->view->error 		= true;
				$errors 				= $zf_filter_input->getMessages();

				$this->view->ACCTSRCERR		= (isset($errors['ACCTSRCT']))? reset($errors['ACCTSRCT']) : null;

				if ($this->view->ACCTSRCERR == null)
				{	$this->view->ACCTSRCERR = $errorMsg;	}

				$this->view->ACCTSRCT 		= $account;
				$this->view->PERIOD 		= $period;
				
				// $this->_redirect('/accountstatement/viewreport');
				$this->render( 'index' );
			}
		}
			
			
		}
		
		
		
		
	}

	public function viewAction(){
	
		$hisData = $this->_db->fetchAll(
					$this->_db->select()
					->from('T_HISTORY_STATEMENT',array('*'))
					->where('HIS_USER_ID = '.$this->_db->quote($this->_userIdLogin))
					->where('HIS_TYPE = ?','1')
					->order('ID DESC')
					->limit(5)
				);
		$this->view->historyData = $hisData;

		$hisDataFile = $this->_db->fetchAll(
					$this->_db->select()
					->from('T_HISTORY_STATEMENT',array('*'))
					->where('HIS_USER_ID = '.$this->_db->quote($this->_userIdLogin))
					->where('HIS_TYPE = ?','2')
					->order('ID DESC')
					->limit(5)
				);
		$this->view->historyDataFile = $hisDataFile;

		$hisDatastate = $this->_db->fetchAll(
					$this->_db->select()
					->from('T_HISTORY_STATEMENT',array('*'))
					->where('HIS_USER_ID = '.$this->_db->quote($this->_userIdLogin))
					->where('HIS_TYPE = ?','3')
					->order('ID DESC')
					->limit(5)
				);
		$this->view->historyDatastate = $hisDatastate;



		$this->_helper->_layout->setLayout('newlayout');
		$set = new Settings();
		$acct_stat_layout = $set->getSettingVal('acct_stat_layout');
		$this->view->acct_stat_layout = $acct_stat_layout;		
		$listAccValidate = Application_Helper_Array::simpleArray($this->_acctArr,'ACCT_NO');

		$page     = $this->_getParam('page');
		$back     = $this->_getParam('back');
		$csv      = $this->_getParam('csv');
		$pdf      = $this->_getParam('pdf');
		$account  = $this->_getParam('ACCTSRC');
		
		if(empty($account)){
			$account  = $this->_getParam('ACCTSRCS');	
		}
		// ACCTSRCS
		// print_r($account);
		$download = $this->_getParam('download');
		$filetype  = $this->_getParam('filetype');
		if(!empty($filetype)){
			$download = true;
		}

		$isNew = (empty($page) && empty($csv) && empty($pdf))? true: false;	// isNew = true, then inquiry to host
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
		
		$filterArr = array	( 	
								'ACCTSRC'		=> 	array('StringTrim','StripTags')
							);
		 // print_r($this->_request->getParams());
		$dataparam['ACCTSRC'] = $account;
									// print_r($dataparam);die;
		$validators = array(
								'periodtype' => array(
									'allowEmpty' => TRUE
													// 'NotEmpty',
													// array('InArray', array('haystack' => array('today', 'history'))),
													// 'messages' => array($this->language->_('Error: You must select Period'),
													// 					//'Error: You must select Period',
													// 					//'Error: Period must be today or history',
													// 					$this->language->_('Error: Period must be today or history'),
													// 					),
												),			
								'ACCTSRC' => array(
													'NotEmpty',
													array('InArray', array('haystack' => $listAccValidate)),
													'messages' => array($this->language->_('Error: You must select account'),
																		//'Error: You must select account',
																		$this->language->_('Error: You dont have right to this account'),
																		//'Error: You dont have right to this account',
																	),
												)		
							);
		
		// print_r($this->_request->getParams());die;
		$zf_filter_input = new Zend_Filter_Input($filterArr,$validators,$dataparam,$this->_optionsValidator);
		
		$period = 'history';
		// print_r($this->_request->getParams());die;
		$dateFrom = '';
		$dateTo   = '';
		if ($period == 'history')
		{
			$dateFrom = $this->_getParam('history_from');
			$dateTo   = $this->_getParam('history_to');
			// print_r($dateFrom);
			// print_r($dateTo);die;

			if(!empty($dateFrom))
			{
				$this->view->DATE_FROM		=$dateFrom;
				$FormatDate = new Zend_Date($dateFrom, $this->_dateDisplayFormat);
				$dateFromView  = $FormatDate->toString($this->_dateViewFormat);
				$this->view->DATE_FROM_view	= $dateFromView;
				$dateFrom  = $FormatDate->toString($this->_dateDBFormat);
			}
			if(!empty($dateTo))
			{
				$this->view->DATE_TO		= $dateTo;
				$FormatDate = new Zend_Date($dateTo, $this->_dateDisplayFormat);
				$dateToView    = $FormatDate->toString($this->_dateViewFormat);
				$this->view->DATE_TO_view	= $dateToView;
				$dateTo    = $FormatDate->toString($this->_dateDBFormat);
			}
		}
		else{
			$FormatDate = Zend_Date::now();
			$dateNow    = $FormatDate->toString($this->_dateViewFormat);
			$this->view->DATE_FROM_view = $dateNow;
			$this->view->DATE_TO_view = $dateNow;
		}
		
		$this->view->ACCTSRC 		= $zf_filter_input->ACCTSRC;
		$this->view->PERIOD 		= $period;

		if (!empty($back))
		{
			$this->render( 'index' );
		}
		else
		{
			$errorMsg = "";
			if ($period == 'history' && (empty($dateFrom) || empty($dateTo)))
			{
				$errorMsg = $this->language->_('Error: History date range has not been chosen yet');
			}
			elseif ($period == 'history')
			{
				$dateFromDB = Application_Helper_General::convertDate($dateFrom, $this->_dateDBFormat, $this->_dateDisplayFormat);
				$dateToDB   = Application_Helper_General::convertDate($dateTo, $this->_dateDBFormat, $this->_dateDisplayFormat);
				$dateHistory = Application_Helper_General::convertDate((date("d/m/Y")), $this->_dateDBFormat, $this->_dateDisplayFormat);
//				$dateDiff	= Application_Helper_General::date_diff_days($dateFromDB, $dateHistory);
				$dateDiff	= Application_Helper_General::date_diff_days($dateFromDB, $dateToDB);
				
				$maxInq 	= date('Y-m-d', strtotime('-89 days', strtotime(date('Y-m-d'))));
				/*
				if (strtotime($dateFromDB) > strtotime($dateToDB))
				{
					$errorMsg = $this->language->_('Error: From Date must not be greater than To Date');
				}
				else 
				if (strtotime($dateToDB) > strtotime(date('Y-m-d')))
				{
					//$errorMsg = $this->language->_('Error: History period must be less than today');
					$errorMsg = $this->language->_('Error: End date greater than today date');
				}
				else */
				if ($dateDiff > 31)
				{
					$errorMsg = $this->language->_('Error: Account statement are retained for the last 31 days');
				}
				elseif ($dateFromDB < $maxInq)
				{
					$errorMsg = $this->language->_('Error: Account statement are retained for the last 90 days');
				}
			}
			
			if($zf_filter_input->isValid() && $errorMsg == "")
			{
				$accData = $this->_db->fetchRow(
					$this->_db->select()
					->from('M_CUSTOMER_ACCT',array('ACCT_NAME', 'ACCT_ALIAS_NAME','ACCT_TYPE', 'CCY_ID'))
					->where('ACCT_NO = '.$this->_db->quote($zf_filter_input->ACCTSRC))
				);
				
				$ccy 		= $accData['CCY_ID'];
				$ACCT_TYPE 	= $accData['ACCT_TYPE'];


				if(!empty($ccy)){
				$currCode 	= Application_Helper_General::getCurrNum($ccy);
				}
				
				$data =  new Service_Account($zf_filter_input->ACCTSRC,$currCode);
				$dataInquiry = $data->accountInquiry('AB',FALSE); 
				$dataAccountType = $dataInquiry['AccountType'];
				
			    if ($period == 'today')
			    	$dataStatement = $data->accountStatementToday($isMobile = FALSE, $dataAccountType); 
				else
					$dataStatement = $data->accountStatementHistory($dateFromDB, $dateToDB, $isMobile = FALSE, $dataAccountType); 

				// echo '<pre>';
				// print_r($dataInquiry);
				// print_r($dataStatement);die;

				if($dataInquiry['ResponseCode'] == '00' && $dataStatement['ResponseCode'] == '00'){
					$this->view->messageResponse = '1';
					$filetype  = $this->_getParam('filetype');
					$directlink = $this->_getParam('directlink');
					// print_r($this->_request->getParams());die;
					if($directlink =='1'){
					if(empty($filetype)){
						$histype = '1';
						$inserthistory = array(
	                      'HIS_ACCT'    =>  $zf_filter_input->ACCTSRC,
	                      'HIS_DATE_START'    => $dateFrom,
	                      'HIS_DATE_END'    => $dateTo,
	                      'HIS_USER_ID'    => $this->_userIdLogin,
	                      'HIS_FILE'    => $filetype,
	                      'HIS_TYPE'    => $histype
	                   		 ); 
					}else{
						$histype = '2';
						$inserthistory = array(
	                      'HIS_ACCT'    =>  $zf_filter_input->ACCTSRC,
	                      'HIS_DATE_START'    => $dateFrom,
	                      'HIS_DATE_END'    => $dateTo,
	                      'HIS_USER_ID'    => $this->_userIdLogin,
	                      'HIS_FILE'    => $filetype,
	                      'HIS_TYPE'    => $histype
	                   		 ); 
					}
					 
                     $this->_db->insert('T_HISTORY_STATEMENT',$inserthistory);
                 	}else{
                 		$inserthistory = array(
	                      'HIS_ACCT'    =>  $zf_filter_input->ACCTSRC,
	                      'HIS_DATE_START'    => $dateFrom,
	                      'HIS_DATE_END'    => $dateTo,
	                      'HIS_USER_ID'    => $this->_userIdLogin,
	                      'HIS_FILE'    => NULL,
	                      'HIS_TYPE'    => '1'
	                   		 );
                 		$this->_db->insert('T_HISTORY_STATEMENT',$inserthistory);
                 	}

				}
				elseif ($dataStatement['ResponseCode'] =='45'){
					$this->view->messageError = $this->language->_('No Transaction History');
					$this->view->messageResponse = '0';
				}
				else{
					$this->view->messageError = $this->language->_('Error : Cant Continue the Transaction, Please Retry in a few moment');
					$this->view->messageResponse = '0';
				}	
						

				// echo "<pre>"; print_r($dataStatement);	die();
				
				$dataDetails = array();
				$totalDebet = "";
				$totalKredit = "";
								
				foreach($dataStatement['Detail'] as $dt => $datadtl)
				{

					if($acct_stat_layout == '1'){
						if ($datadtl->DBCR == 'D')
						{
							$dataDetails[$dt]['debet'] 	 = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['credit']  = "";
							$totalDebet = $totalDebet + $datadtl->Amount;
						}
						else
						{
							$dataDetails[$dt]['debet']   = "";
							$dataDetails[$dt]['credit']  = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$totalKredit = $totalKredit + $datadtl->Amount;
						}
						
							if( $dt == 0 )
							{
//								$dataStatement['Detail'][$dt]->Balance = (int) $dataStatement['ClosingBalance'];
								$dataStatement['Detail'][$dt]->Balance = $dataStatement['ClosingBalance'];
							}
							else
							{
								$credit = ($dataStatement['Detail'][$dt-1]->DBCR == 'C') ? $dataStatement['Detail'][$dt-1]->Amount : 0;
								$debet = ($dataStatement['Detail'][$dt-1]->DBCR == 'D') ? $dataStatement['Detail'][$dt-1]->Amount : 0;
//								$dataStatement['Detail'][$dt]->Balance = (int) ($dataStatement['Detail'][$dt-1]->Balance + $debet - $credit);
								$dataStatement['Detail'][$dt]->Balance = ($dataStatement['Detail'][$dt-1]->Balance + $debet - $credit);
							}
							
							
							$credit = ($dataStatement['Detail'][$dt]->DBCR == 'C') ? $dataStatement['Detail'][$dt]->Amount : 0;
							$debet = ($dataStatement['Detail'][$dt]->DBCR == 'D') ? $dataStatement['Detail'][$dt]->Amount : 0;
							$openingBalance = $dataStatement['Detail'][$dt]->Balance  + $debet - $credit;
					}
					elseif($acct_stat_layout == '2'){
						if ($datadtl->DBCR == 'D')
						{
							$dataDetails[$dt]['amount']  = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['type']  = "DB";
							$totalDebet = $totalDebet + $datadtl->Amount;
						}
						else
						{
							$dataDetails[$dt]['amount']  = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['type']   = "CR";
							$totalKredit = $totalKredit + $datadtl->Amount;
						}
						
							if( $dt == 0 )
							{
//								$dataStatement['Detail'][$dt]->Balance = (int) $dataStatement['ClosingBalance'];
								$dataStatement['Detail'][$dt]->Balance = $dataStatement['ClosingBalance'];
							}
							else
							{
								$credit = ($dataStatement['Detail'][$dt-1]->DBCR == 'C') ? $dataStatement['Detail'][$dt-1]->Amount : 0;
								$debet = ($dataStatement['Detail'][$dt-1]->DBCR == 'D') ? $dataStatement['Detail'][$dt-1]->Amount : 0;
//								$dataStatement['Detail'][$dt]->Balance = (int) ($dataStatement['Detail'][$dt-1]->Balance + $debet - $credit);
								$dataStatement['Detail'][$dt]->Balance = ($dataStatement['Detail'][$dt-1]->Balance + $debet - $credit);
							}
							
							
							$credit = ($dataStatement['Detail'][$dt]->DBCR == 'C') ? $dataStatement['Detail'][$dt]->Amount : 0;
							$debet = ($dataStatement['Detail'][$dt]->DBCR == 'D') ? $dataStatement['Detail'][$dt]->Amount : 0;
							$openingBalance = $dataStatement['Detail'][$dt]->Balance  + $debet - $credit;
					}
					elseif($acct_stat_layout == '1R'){
						if ($datadtl->DBCR == 'D')
						{
							$dataDetails[$dt]['debet'] 	 = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['credit']  = "";
							$totalDebet = $totalDebet + $datadtl->Amount;
						}
						else
						{
							$dataDetails[$dt]['debet']   = "";
							$dataDetails[$dt]['credit']  = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$totalKredit = $totalKredit + $datadtl->Amount;
						}
						
//						$balance = $datadtl->Balance;
//						$dataDetails[$dt]['balance'] = ($csv)? $balance: Application_Helper_General::displayMoney($balance);

						if( $dt == 0 )
						{
//							$dataStatement['Detail'][$dt]->Balance = (int) $dataStatement['ClosingBalance'];
							$dataStatement['Detail'][$dt]->Balance = $dataStatement['ClosingBalance'];
						}
						else
						{
							$credit = ($dataStatement['Detail'][$dt-1]->DBCR == 'C') ? $dataStatement['Detail'][$dt-1]->Amount : 0;
							$debet = ($dataStatement['Detail'][$dt-1]->DBCR == 'D') ? $dataStatement['Detail'][$dt-1]->Amount : 0;
//							$dataStatement['Detail'][$dt]->Balance = (int) ($dataStatement['Detail'][$dt-1]->Balance + $debet - $credit);
							$dataStatement['Detail'][$dt]->Balance = ($dataStatement['Detail'][$dt-1]->Balance + $debet - $credit);
						}
						
						
						$credit = ($dataStatement['Detail'][$dt]->DBCR == 'C') ? $dataStatement['Detail'][$dt]->Amount : 0;
						$debet = ($dataStatement['Detail'][$dt]->DBCR == 'D') ? $dataStatement['Detail'][$dt]->Amount : 0;
						$openingBalance = $dataStatement['Detail'][$dt]->Balance  + $debet - $credit;

					}
					elseif($acct_stat_layout == '2R'){
						if ($datadtl->DBCR == 'D')
						{
							$dataDetails[$dt]['amount']  = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['type']  = "DB";
							$totalDebet = $totalDebet + $datadtl->Amount;
							//$totalBalance = $dataAvalilableBalance - $datadtl->Amount;
						}
						else
						{
							$dataDetails[$dt]['amount']  = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['type']   = "CR";
							$totalKredit = $totalKredit + $datadtl->Amount;
							//$totalBalance = $dataAvalilableBalance + $datadtl->Amount;
						}
						
						
//						$typeTransaction = $datadtl->DBCR;
//						$dataAmount = $datadtl->Amount;
//						
//						
//						if($typeTransaction == 'D'){
//							$debetAmount = $dataAmount;
//							
//
//						}
//						elseif($typeTransaction == 'C'){
//							$creditAmount = $dataAmount;
//						}
						
//						$totBalance = ($dataStatement['ClosingBalance']-$totalKredit+$totalDebet);
//						$tot = $totBalance + $debetAmount - $creditAmount;
						
						if( $dt == 0 )
						{
//							$dataStatement['Detail'][$dt]->Balance = (int) $dataStatement['ClosingBalance'];
							$dataStatement['Detail'][$dt]->Balance = $dataStatement['ClosingBalance'];
						}
						else
						{
							$credit = ($dataStatement['Detail'][$dt-1]->DBCR == 'C') ? $dataStatement['Detail'][$dt-1]->Amount : 0;
							$debet = ($dataStatement['Detail'][$dt-1]->DBCR == 'D') ? $dataStatement['Detail'][$dt-1]->Amount : 0;
//							$dataStatement['Detail'][$dt]->Balance = (int) ($dataStatement['Detail'][$dt-1]->Balance + $debet - $credit);
							$dataStatement['Detail'][$dt]->Balance = ($dataStatement['Detail'][$dt-1]->Balance + $debet - $credit);
						}
						
						
						$credit = ($dataStatement['Detail'][$dt]->DBCR == 'C') ? $dataStatement['Detail'][$dt]->Amount : 0;
						$debet = ($dataStatement['Detail'][$dt]->DBCR == 'D') ? $dataStatement['Detail'][$dt]->Amount : 0;
						$openingBalance = $dataStatement['Detail'][$dt]->Balance  + $debet - $credit;
						
						
//						$dataStatement['Detail'][$dt]->Balance = $tot;
						//$runningBalance = ($totBalance-$debetAmount)+$creditAmount;
						//$balance = $datadtl->Balance;
//						$balance = $tot;
						
//						$dataDetails[$dt]['balance'] = ($csv)? $balance: Application_Helper_General::displayMoney($balance);
						
					}
				
					
					if ($period == 'history')
					{	
//						$balanceSign = substr($datadtl->Balance, 0, 1);
//						$balance	 = substr($datadtl->Balance, 1);
//						if ($balanceSign != '+')
//			   			{	$balance = (-1) * $balance;		}
			   			
			   			$balance = $datadtl->Balance;
	   			
						//$dataDetails[$dt]['balance'] = ($csv)? $balance: Application_Helper_General::displayMoney($balance);		
					}
		
				}
				
				$dataStatement['Detail'] = array_reverse($dataStatement['Detail'], true); //di sorting
				
				// 
				// if($download)
				
				if ($csv || $pdf) {
					// print_r($dataStatement);
					$dataDetailsOri = $dataStatement['Detail'];
				} else {
					$this->paging($dataStatement['Detail']);	
					$dataDetailsOri = $this->view->paginator;
				}

				$date_language = $this->language->_('Date');
				$description_language = $this->language->_('Description');
				$ccy_language = $this->language->_('CCY');
				$debet_language = $this->language->_('Debet');
				$credit_language = $this->language->_('Credit');
				$amount_language = $this->language->_('Amount');
				$balance_language = $this->language->_('Balance');
				
				//$header = array($date_language, $description_language, $ccy_language, $debet_language, $credit_language);
				if($acct_stat_layout == '1'){
					$header = array($date_language, $description_language, $debet_language, $credit_language);
				}
				elseif($acct_stat_layout == '2'){
					$header = array($date_language, $description_language, $amount_language);
				}
				elseif($acct_stat_layout == '1R'){
					$header = array($date_language, $description_language, $debet_language, $credit_language, $balance_language);
				}
				elseif($acct_stat_layout == '2R'){
					$header = array($date_language, $description_language, $amount_language, '', $balance_language);
				}
				
				if ($period == 'history')
				{	
					//$header[] = 'Balance';		
				}
				
				$dataDetails = array();
				//Zend_Debug::dump($dataDetailsOri); die;
				$totalDebet = "";
				$totalKredit = "";
				foreach($dataDetailsOri as $dt => $datadtl)
				{
					$limiter = ($csv)? " ": "<br>";
					$univDesc  = (!empty($datadtl->UniversalDescription1))? $limiter.$datadtl->UniversalDescription1: "";
					$univDesc .= (!empty($datadtl->UniversalDescription2))? $limiter.$datadtl->UniversalDescription2: "";
					$univDesc .= (!empty($datadtl->UniversalDescription3))? $limiter.$datadtl->UniversalDescription3: "";
					$univDesc .= (!empty($datadtl->UniversalDescription4))? $limiter.$datadtl->UniversalDescription4: "";
					//$dataDetails[$dt]['datetime'] 	 = Application_Helper_General::convertDate($datadtl->Date)." ".$datadtl->Time;
					//$dataDetails[$dt]['datetime'] 	 = Application_Helper_General::convertDate(strtotime($datadtl->Datetime), $this->_dateTimeDisplayFormat);
					
					$dataDetails[$dt]['datetime'] 	 = $datadtl->Datetime;//Application_Helper_General::convertDate($datadtl->Datetime, $this->_dateDisplayFormat);
					$dataDetails[$dt]['description'] = $datadtl->Description.$univDesc;
					$dataDetails[$dt]['ccy'] 		 = $ccy;
										
					if($acct_stat_layout == '1'){
						if ($datadtl->DBCR == 'D')
						{
							$dataDetails[$dt]['debet'] 	 = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['credit']  = "";
							$totalDebet = $totalDebet + $datadtl->Amount;
						}
						else
						{
							$dataDetails[$dt]['debet']   = "";
							$dataDetails[$dt]['credit']  = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$totalKredit = $totalKredit + $datadtl->Amount;
						}
					}
					elseif($acct_stat_layout == '2'){
						if ($datadtl->DBCR == 'D')
						{
							$dataDetails[$dt]['amount']  = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['type']  = "DB";
							$totalDebet = $totalDebet + $datadtl->Amount;
						}
						else
						{
							$dataDetails[$dt]['amount']  = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['type']   = "CR";
							$totalKredit = $totalKredit + $datadtl->Amount;
						}
					}
					elseif($acct_stat_layout == '1R'){
						if ($datadtl->DBCR == 'D')
						{
							$dataDetails[$dt]['debet'] 	 = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['credit']  = "";
							$totalDebet = $totalDebet + $datadtl->Amount;
						}
						else
						{
							$dataDetails[$dt]['debet']   = "";
							$dataDetails[$dt]['credit']  = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$totalKredit = $totalKredit + $datadtl->Amount;
						}
						$balance = $datadtl->Balance;
						$dataDetails[$dt]['balance'] = ($csv)? $balance: Application_Helper_General::displayMoney($balance);

					}
					elseif($acct_stat_layout == '2R'){
						if ($datadtl->DBCR == 'D')
						{
							$dataDetails[$dt]['amount']  = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['type']  = "DB";
							$totalDebet = $totalDebet + $datadtl->Amount;
						}
						else
						{
							$dataDetails[$dt]['amount']  = ($csv)? $datadtl->Amount: Application_Helper_General::displayMoney($datadtl->Amount);
							$dataDetails[$dt]['type']   = "CR";
							$totalKredit = $totalKredit + $datadtl->Amount;
						}
						$balance = $datadtl->Balance;
						$dataDetails[$dt]['balance'] = ($csv)? $balance: Application_Helper_General::displayMoney($balance);
					}
					
					if ($period == 'history')
					{	
//						$balanceSign = substr($datadtl->Balance, 0, 1);
//						$balance	 = substr($datadtl->Balance, 1);
//						if ($balanceSign != '+')
//			   			{	$balance = (-1) * $balance;		}
			   			
			   			$balance = $datadtl->Balance;
	   			
						//$dataDetails[$dt]['balance'] = ($csv)? $balance: Application_Helper_General::displayMoney($balance);		
					}
					//echo $datadtl->Amount."<br>";
				}
				//echo $totalDebet; die;
				$stringParam = array('ACCTSRC'		=> $zf_filter_input->ACCTSRC,
									 'periodtype'	=> $period,
									 'history_from'	=> $dateFrom,
									 'history_to'	=> $dateTo,
								    );
				
				$this->view->dataDetails 	= $dataDetails;
				$this->view->accData 		= $accData;
				$this->view->openingBalance = $openingBalance;
				$this->view->closingBalance = $dataStatement['ClosingBalance'];
				$this->view->holdAmount 	= $dataInquiry['HoldAmount'];
				$this->view->totalDebet 	= $totalDebet == 0 ? 0.00 : $totalDebet;
				$this->view->totalKredit 	= $totalKredit == 0 ? 0.00 : $totalKredit;
				
				$this->view->ccy 			= $ccy;
				$this->view->header			= $header;
				$this->view->query_string_params = $stringParam;
				$this->view->pdf 			= ($pdf)? true: false;
				$this->updateQstring();
				
				$logDesc = 'Account: '.$zf_filter_input->ACCTSRC;

				if($download){
					// var_dump($download);die;
					$filetype  = $this->_getParam('filetype');
					$directlink = $this->_getParam('directlink');
					// print_r($directlink);
					// var_dump($directlink);die;
						if($directlink =='0'){
						if(empty($filetype)){
							// die('here');
							$histype = '1';
							$inserthistory = array(
		                      'HIS_ACCT'    => $zf_filter_input->ACCTSRC,
		                      'HIS_DATE_START'    => $dateFrom,
		                      'HIS_DATE_END'    => $dateTo,
		                      'HIS_USER_ID'    => $this->_userIdLogin,
		                      'HIS_FILE'    => $filetype,
		                      'HIS_TYPE'    => $histype
		                   		 ); 
						}else{
							// die('here1');
							$histype = '2';
							$inserthistory = array(
		                      'HIS_ACCT'    => $zf_filter_input->ACCTSRC,
		                      'HIS_DATE_START'    => $dateFrom,
		                      'HIS_DATE_END'    => $dateTo,
		                      'HIS_USER_ID'    => $this->_userIdLogin,
		                      'HIS_FILE'    => $filetype,
		                      'HIS_TYPE'    => $histype
		                   		 ); 
						}
						// print_r($inserthistory);
						 // print_r($this->_request->getParams());die;
	                     $this->_db->insert('T_HISTORY_STATEMENT',$inserthistory);
	                 }
					// $csv  = $this->_getParam('ACCTSRC');

				if($filetype =='2')
				{
					$logDesc .= '. Export to CSV';
					
					// Set Information Data
					$n = count($dataDetails);
						  $dataDetails[$n] = array("");
					$n++; $dataDetails[$n] = array($this->language->_('Information'));
					$n++; $dataDetails[$n] = array($this->language->_('Account'), $zf_filter_input->ACCTSRC.' ('.$accData['ACCT_NAME'].' '.$accData['ACCT_ALIAS_NAME'].' / '.$ccy.')');
					//$n++; $dataDetails[$n] = array("Opening Balance", $ccy.' '.Application_Helper_General::displayMoney($this->view->openingBalance));
					$n++; $dataDetails[$n] = array($this->language->_('Opening Balance'), $ccy.' '.Application_Helper_General::displayMoney($this->view->openingBalance));
					$n++; $dataDetails[$n] = array($this->language->_('Closing Balance'), $ccy.' '.Application_Helper_General::displayMoney($this->view->closingBalance));
					$n++; $dataDetails[$n] = array($this->language->_('Total Debit'), $ccy.' '.Application_Helper_General::displayMoney($this->view->totalDebet));
					$n++; $dataDetails[$n] = array($this->language->_('Total Credit'), $ccy.' '.Application_Helper_General::displayMoney($this->view->totalKredit));
					if ($period == 'today')
					{
					//$n++; $dataDetails[$n] = array($this->language->_('Hold Amount'), $ccy.' '.Application_Helper_General::displayMoney($this->view->holdAmount));
					}
					$n++; $dataDetails[$n] = array($this->language->_('Start Date'), $this->view->DATE_FROM_view);
					$n++; $dataDetails[$n] = array($this->language->_('End Date'), $this->view->DATE_TO_view);
					// print_r($dataDetails);die;
					$src = 'xxxxxxx'.substr($zf_filter_input->ACCTSRC, -4);
					$filename = 'Account Statement_'.$src;
					$this->_helper->download->csv($header,$dataDetails,null,$filename);  
					// exit();
				}
				elseif($filetype =='1' )
				{
					$logDesc .= '. Print PDF';
					// print_r($logDesc);die;
					Application_Helper_General::writeLog('ACDT', $logDesc);
					$outputHTML = "<tr><td>".$this->view->render($this->view->controllername.'/view.phtml')."</td></tr>";
					$src = 'xxxxxxx'.substr($zf_filter_input->ACCTSRC, -4);
					$filename = 'Account Statement_'.$src;
					$this->_helper->download->pdf(null,null,null,$filename,$outputHTML);   
					// echo $outputHTML;
					// exit();
					// die; 
				}else if($filetype == '3'){
					$this->updateQstring();
				// if ($mt) {
					$dataDetailsOri = $dataStatement['Detail'];
					// print_r($dataDetails);die;
					$startDate = date("Ymd", strtotime($dateFrom));
					$dualstartDate = date("ymd", strtotime($dateTo));
					$handle = fopen($startDate.".txt", "w");
					
					$str = "{1:003BANK PERMATA}{2:I940".strtoupper($accData['ACCT_NAME'])."}"."{4:0-";
					// print_r($startDate);die;
				    fwrite($handle, "{1:003BANK PERMATA}{2:I940".strtoupper($accData['ACCT_NAME'])."}"."{4:0-");
				    $randomNum = substr(str_shuffle("0123456789"), 0, 6);
				    fwrite($handle, ":20:".$startDate."".$randomNum);
				    $str .= ":20:".$startDate."".$randomNum;
				    fwrite($handle, ":25:".$zf_filter_input->ACCTSRC);
				    $str .= ":25:".$zf_filter_input->ACCTSRC;
				    fwrite($handle, ":28C:00001");
				    $str .= ":28C:00001";
				    if ($period == 'today'){
				    	fwrite($handle, ":60F:C".$dualstartDate."".$accData['CCY_ID']."".$openingBalance);	
				    	$str .= ":60F:C".$dualstartDate."".$accData['CCY_ID']."".$openingBalance;
				    }else{
				    	fwrite($handle, ":60M:C".$dualstartDate."".$accData['CCY_ID']."".$openingBalance);	
				    	$str .= ":60M:C".$dualstartDate."".$accData['CCY_ID']."".$openingBalance;
				    }
				    $trxdate = '';
				    // var_dump($dataDetails);die;
				    foreach ($dataDetails as $key => $value) {
				    	
				    	$trxdate = date("ymd", strtotime($value['datetime']));
				    	if($value['type'] == 'DB'){
				    		$value['type'] = 'D';
				    	}else{
				    		$value['type'] = 'C';
				    	}
				    	$str .= ":61:".$startDate."".$value['type']."".$value['amount']."NTRF";
				    	fwrite($handle, ":61:".$startDate."".$value['type']."".$value['amount']."NTRF");
				    	fwrite($handle, ":86:".str_pad($value['description'],76,' ')."");
				    	$str .= ":86:".str_pad($value['description'],76,' ')."";
				    }
				    if ($period == 'today'){
				    	fwrite($handle, ":62F:C".$dualstartDate."IDR".$dataStatement['ClosingBalance']."}");
				    	$str .= ":62F:C".$dualstartDate."IDR".$dataStatement['ClosingBalance']."}";
				    }else{
				    	fwrite($handle, ":62F:C".$dualendDate."IDR".$dataStatement['ClosingBalance']."}");
				    	$str .= ":62F:C".$dualendDate."IDR".$dataStatement['ClosingBalance']."}";
				    }
				    // echo "<pre>";
				    // var_dump($str);die;
				    fwrite($handle, $str);
				    fclose($handle);
				    $src = 'xxxxxxx'.substr($zf_filter_input->ACCTSRC, -4);
					$filename = 'Account Statement_'.$src;
				    header('Content-Type: application/octet-stream');
				    header('Content-Disposition: attachment; filename='.basename($filename.'.txt'));
				    header('Expires: 0');
				    header('Cache-Control: must-revalidate');
				    header('Pragma: public');
				    header('Content-Length: ' . filesize($filename.'.txt'));
				    readfile($startDate.'.txt');
				    exit;
					// startDate


				// }
				}

				}
				// var_dump();die;
				Application_Helper_General::writeLog('ACDT', $logDesc);
			}
			else
			{
				$this->view->error 		= true;
				$errors 				= $zf_filter_input->getMessages();
				// print_r($errors);die;
				$this->view->ACCTSRCERR		= (isset($errors['ACCTSRC']))? reset($errors['ACCTSRC']) : null;
				
				if ($this->view->ACCTSRCERR == null)
				{	$this->view->ACCTSRCERR = $errorMsg;	}
				
				$this->view->ACCTSRC 		= $account;
				
				$this->render( 'index' );
			}
		}
		
		
	
	}

	public function virtualAction(){
		$this->_helper->layout()->setLayout('popup');
		// $cacheData = new Zend_Session_Namespace('TRX_INQ');
		$cachePID = $this->_custIdLogin.$this->_userIdLogin;
		$cacheData = $this->_cache->load($cachePID);
		$this->_cache->clean(Zend_Cache::CLEANING_MODE_OLD);

		$page     		= $this->_getParam('page');
		$id     		= $this->_getParam('id',0);
		$csv      		= $this->_getParam('csv');
		$pdf      		= $this->_getParam('pdf');
		$isNew 			= (empty($page) && empty($csv) && empty($pdf))? true: false;	// isNew = true, then inquiry to host
		$page 			= (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
		$this->view->id = $id = (Zend_Validate::is($id,'Digits') && ($id >= 0))? $id : null;
		$dateFrom 		= str_replace( '-','/',$this->_getParam('history_from'));
		$dateTo   		= str_replace( '-','/',$this->_getParam('history_to'));

		$vaData['VIRTUAL_ACCOUNT_NAME'] = 'N/A';
		$vaData['providerCode'] 						= 'N/A';
		$vaData['providerName'] 						= 'N/A';
		$vaData['providerAddr'] 							= 'N/A';
		$vaData['CCY'] 										= 'N/A';

		$cacheID = 'VA'.$this->_custIdLogin.$this->_userIdLogin;
		if($cacheData !== false){
			if( $id !== null && is_array($cacheData->Header) && is_array($cacheData->dataStatement) ){
				$data['Header'] = $cacheData->Header;
				$data['Detail'] = $cacheData->dataStatement['Detail'];

				if( isset($data['Detail'][$id]) ){
					$vaData 			+= $data['Detail'][$id];
					// $transDesc 	= trim($vaData['transDesc']);
					$vaData['VA']	= substr( trim($vaData['transDesc']) ,0,16);
					
					$this->view->ACCTSRC 		= $ACCTSRC = $data['Header']['ACCTSRC'];
					$this->view->DATE_FROM	= $dateFrom = (empty($dateFrom))? Application_Helper_General::convertDate(date('Y-m-d')): Application_Helper_General::convertDate($dateFrom);
					$this->view->DATE_TO		= $dateTo = (empty($dateTo))	? Application_Helper_General::convertDate(date('Y-m-d')): Application_Helper_General::convertDate($dateTo);
					
					$this->view->DATE_FROM_view	= (empty($dateFrom))? Application_Helper_General::convertDate(date('Y-m-d'), $this->_dateViewFormat): Application_Helper_General::convertDate($dateFrom, $this->_dateViewFormat);
					$this->view->DATE_TO_view	= (empty($dateTo))	? Application_Helper_General::convertDate(date('Y-m-d'), $this->_dateViewFormat): Application_Helper_General::convertDate($dateTo, $this->_dateViewFormat);

					{
						$errorMsg = "";

						$dateFromDB = Application_Helper_General::convertDate($dateFrom, $this->_dateDBFormat, $this->_dateDisplayFormat);
						$dateToDB   = Application_Helper_General::convertDate($dateTo, $this->_dateDBFormat, $this->_dateDisplayFormat);

						{

							$ccy 		= $data['Header']['CCY_ID'];
							$currCode = Application_Helper_General::getCurrNum($ccy);
							$dataStatement['Detail'] = array();
							if ($isNew === true)	
							{
								$this->_cache->remove($cacheID);
								$service =  new Service_Account($ACCTSRC,$currCode,$cacheData->dataStatement['AccountType']);

								$dataStatement = $service->virtualAccountStatementHistory($dateFromDB, $dateToDB,$vaData['VA'],$vaData['balanceAmt']);
								// $cacheVaData = new Zend_Session_Namespace('TRX_VAINQ');
								$cacheVaData = $this->_cache->load($cacheID);
								$cacheVaData->Detail 			= $dataStatement;	
								$this->_cache->save($cacheVaData);
							}else{
								// $cacheVaData = new Zend_Session_Namespace('TRX_VAINQ');
								$cacheVaData = $this->_cache->load($cacheID);
								$dataStatement	= $cacheVaData->Detail;
							}

							if ($csv || $pdf) {
								$dataDetailsOri = $dataStatement['Detail'];
							} else {
								$this->paging($dataStatement['Detail']);	
								$dataDetailsOri = $this->view->paginator;
							}
							$header = array('Transaction Date', 'Posting Date', 'Transaction Code', 'Debet (IDR)', 'Credit (IDR)' ,'Reference Number');

							$stringParam = array(
								'ACCTSRC'		=> $ACCTSRC,
								'history_from'	=> $dateFrom,
								'history_to'	=> $dateTo,
							);

							$cacheVaData = $this->_cache->load($cacheID);
							$cacheVaData->Header 			= $stringParam;
							$this->_cache->save($cacheVaData);
							$this->view->dataDetails 	= $dataStatement['Detail'];
							if(count($dataStatement['Detail']) > 0)
							{
								$vaData['VIRTUAL_ACCOUNT_NAME'] = $dataStatement['Detail'][0]['VIRTUAL_ACCOUNT_NAME'];
								$vaData['providerCode'] 	= $dataStatement['providerCode'];
								$vaData['providerName'] 	= $dataStatement['providerName'];
								$vaData['providerAddr'] 	= $dataStatement['providerAddr'];
								$vaData['CCY'] 					= $dataStatement['CCY'];
							}
							$this->view->data 			= $data['Header'];
							$this->view->header			= $header;
							$this->view->query_string_params = $stringParam;
							$this->view->pdf 			= ($pdf)? true: false;
							$this->updateQstring();
							if($csv)
							{
								$logDesc = 'Download CSV Virtual Account Transaction History ['.$dateFrom.' - '.$dateTo.' ].';
								$n = 0;
								$n++; $dataStatement['Header'][$n] = array("Information");
								$n++; $dataStatement['Header'][$n] = array("Account", $ACCTSRC.' ('.$data['Header']['ACCT_NAME'].' '.$data['Header']['ACCT_ALIAS_NAME'].' / '.$ccy.')');
								$n++; $dataStatement['Header'][$n] = array("Virtual Account", $vaData['VA'].' ('.$vaData['VIRTUAL_ACCOUNT_NAME'].' / '.$vaData['CCY'].')');
								$n++; $dataStatement['Header'][$n] = array("Provider Code", $vaData['providerCode']);
								$n++; $dataStatement['Header'][$n] = array("Provider Name", $vaData['providerName']);
								$n++; $dataStatement['Header'][$n] = array("Provider Address", $vaData['providerAddr']);
								$n++; $dataStatement['Header'][$n] = array("Start Date", $this->view->DATE_FROM_view);
								$n++; $dataStatement['Header'][$n] = array("End Date", $this->view->DATE_TO_view);
								$n++; $dataStatement['Header'][$n] = array("");

								$dataStatement['Header'] = array_merge($dataStatement['Header'] , array($header));
								$n++;$n++;$dataStatement['Header'][$n] = array("");

								$td = $tc = 0;
								$content = array();
								foreach($dataStatement['Detail'] as $row)
								{
									$credit = ($row['balanceSign'] == 'C') ? $row['balanceAmt'] : 0;
									$debet = ($row['balanceSign'] == 'D') ? $row['balanceAmt'] : 0;
									$td += $debet;
									$tc += $credit;
									$n++;

									$His = str_split($row['TRANSACTION_TIME'],2);
									$His[] = '00';
									$His = implode(':',$His);
									
									$content[$n] = array(
										Application_Helper_General::convertDate($row['TRANSACTION_DATE'], $this->_dateViewFormat).' '.$His,
										Application_Helper_General::convertDate($row['POSTING_DATE'], $this->_dateViewFormat),
										$row['BVTransCode'],
										Application_Helper_General::displayMoney($debet),
										Application_Helper_General::displayMoney($credit),
										$row['BVTraceNo']
									);
								}
								$n++;
								$content[$n] = array(
									"",
									"",
									"Total",
									Application_Helper_General::displayMoney($td),
									Application_Helper_General::displayMoney($tc),
									"",
								);
								
								$dataStatement['Detail'] = array_merge($dataStatement['Header'] , $content);

								$this->_helper->download->csv(array(),$dataStatement['Detail'],null,'VirtualAccountTransactionInquiry');  
							}
							else if(!empty($pdf))
							{
								$logDesc = 'Download PDF Virtual Account Transaction History ['.$dateFrom.' - '.$dateTo.' ].';
								$outputHTML = "<tr><td>".$this->view->render($this->view->controllername.'/view.phtml')."</td></tr>";
								$this->_helper->download->pdf(null,null,null,'Virtual Account Transaction Inquiry',$outputHTML);    
							}
							else
							{
								$logDesc = 'View Virtual Account Transaction History ['.$dateFrom.' - '.$dateTo.' ].';
							}
							
							$logDesc .= 'Account : '.$ACCTSRC.'. Virtual Account : '.$vaData['VA'];
							Application_Helper_General::writeLog('ACDT', $logDesc);
						}
					}
				}
			}
			else
			{
				$this->view->errMsg = 'Invalid Virtual Account';
			}
		}
		else
		{
			$this->view->errMsg = 'No Data Found';
		}
		$this->view->va 				= $vaData;
	}




	public function deleteAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

     

//      if ($priviBeneLinkage == true)
//      {
           		 $this->_db->beginTransaction();
				
				$where['ID = ?'] = $tblName;
				$this->_db->delete('T_HISTORY_STATEMENT',$where);
				
				
				$this->_db->commit();
				
				Application_Helper_General::writeLog('GRAM','Deleting History Statement'.$tblName);

				$hisDatastate = $this->_db->fetchAll(
					$this->_db->select()
					->from('T_HISTORY_STATEMENT',array('*'))
					->where('HIS_USER_ID = '.$this->_db->quote($this->_userIdLogin))
					->where('HIS_TYPE = ?','3')
					->order('ID DESC')
					->limit(5)
				);

				 $month = array("",$this->language->_("January"),
                        $this->language->_("February"),
                        $this->language->_("March"),
                        $this->language->_("April"),
                        $this->language->_("May"),
                        $this->language->_("June"), 
                        $this->language->_("July"),
                        $this->language->_("August"),
                        $this->language->_("September"),
                        $this->language->_("October"),
                        $this->language->_("November"),
                        $this->language->_("December"));

				// $this->view->historyDatastate = $hisDatastate;
				$optHtml = '';
				if(!empty($hisDatastate)){
					foreach ($hisDatastate as $key => $value) {
						$optHtml .= '<div class="page-bar"></div><div class="form-group row">';
						$optHtml .= '<label class="control-label col-md-4">'.$value['HIS_ACCT'].'</label><label class="control-label col-md-6"><i class="far fa-calendar-alt"></i>&nbsp'.$month[(int)$value['HIS_MONTH']].'  - '.$value['HIS_YEAR'].'</label><div class="col-md-2">';
						$optHtml .= '<a href="javascript:;" onclick="directsstate('.$value['HIS_ACCT'].','.$value['HIS_MONTH'].','.$value['HIS_YEAR'].')"><i class="fa fa-eye" title="View"></i></a>';
						$optHtml .= '<a href="javascript:;" value="'.$value['ID'].'" onclick="deletestate(this)"><i class="fa fa-trash" title="Remove"></i></a></div></div>';
						$optHtml .= '';
						$optHtml .= '';
					}
				}
	
          echo $optHtml;
    }


    public function refreshstateAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        // $tblName = $this->_getParam('id');

     

//      if ($priviBeneLinkage == true)
//      {
           		 

				$hisDatastate = $this->_db->fetchAll(
					$this->_db->select()
					->from('T_HISTORY_STATEMENT',array('*'))
					->where('HIS_USER_ID = '.$this->_db->quote($this->_userIdLogin))
					->where('HIS_TYPE = ?','3')
					->order('ID DESC')
					->limit(5)
				);

				 $month = array("",$this->language->_("January"),
                        $this->language->_("February"),
                        $this->language->_("March"),
                        $this->language->_("April"),
                        $this->language->_("May"),
                        $this->language->_("June"), 
                        $this->language->_("July"),
                        $this->language->_("August"),
                        $this->language->_("September"),
                        $this->language->_("October"),
                        $this->language->_("November"),
                        $this->language->_("December"));

				// $this->view->historyDatastate = $hisDatastate;
				$optHtml = '';
				if(!empty($hisDatastate)){
					foreach ($hisDatastate as $key => $value) {
						$optHtml .= '<div class="page-bar"></div><div class="form-group row">';
						$optHtml .= '<label class="control-label col-md-4">'.$value['HIS_ACCT'].'</label><label class="control-label col-md-6"><i class="far fa-calendar-alt"></i>&nbsp'.$month[(int)$value['HIS_MONTH']].'  - '.$value['HIS_YEAR'].'</label><div class="col-md-2">';
						$optHtml .= '<a href="javascript:;" onclick="directsstate('.$value['HIS_ACCT'].','.$value['HIS_MONTH'].','.$value['HIS_YEAR'].')"><i class="fa fa-eye" title="View"></i></a>';
						$optHtml .= '<a href="javascript:;" value="'.$value['ID'].'" onclick="deletestate(this)"><i class="fa fa-trash" title="Remove"></i></a></div></div>';
						$optHtml .= '';
						$optHtml .= '';
					}
				}
	
          echo $optHtml;
    }


    public function refreshfileAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        // $tblName = $this->_getParam('id');

     

//      if ($priviBeneLinkage == true)
//      {
           		 

				$hisDatastate = $this->_db->fetchAll(
					$this->_db->select()
					->from('T_HISTORY_STATEMENT',array('*'))
					->where('HIS_USER_ID = '.$this->_db->quote($this->_userIdLogin))
					->where('HIS_TYPE = ?','2')
					->order('ID DESC')
					->limit(5)
				);

				// $this->view->historyDatastate = $hisDatastate;
				$optHtml = '';
				if(!empty($hisDatastate)){
					foreach ($hisDatastate as $key => $value) {
						$optHtml .= '<div class="page-bar"></div><div class="form-group row">';
						$optHtml .= '<label class="control-label col-md-4">'.$value['HIS_ACCT'].'</label><label class="control-label col-md-6"><i class="far fa-calendar-alt"></i>&nbsp;'.Application_Helper_General::convertDate($value['HIS_DATE_START'], 'dd MMMM y').' - '.Application_Helper_General::convertDate($value['HIS_DATE_END'], 'dd MMMM y').'</label><div class="col-md-2">';
						$optHtml .= '<a href="javascript:;" onclick="directfile('.$value['HIS_ACCT'].','.$value['HIS_DATE_START'].','.$value['HIS_DATE_END'].','.$value['HIS_FILE'].')"><i class="fa fa-file-download" title="Download"></i></a>';
						$optHtml .= '<a href="javascript:;" value="'.$value['ID'].'" onclick="deletefile(this)"><i class="fa fa-trash" title="Remove"></i></a></div></div>';
						$optHtml .= '';
						$optHtml .= '';
					}
				}
	
          echo $optHtml;
    }


    public function deletefileAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

     

//      if ($priviBeneLinkage == true)
//      {
           		 $this->_db->beginTransaction();
				
				$where['ID = ?'] = $tblName;
				$this->_db->delete('T_HISTORY_STATEMENT',$where);
				
				
				$this->_db->commit();
				
				Application_Helper_General::writeLog('GRAM','Deleting History Statement'.$tblName);

				$hisDatastate = $this->_db->fetchAll(
					$this->_db->select()
					->from('T_HISTORY_STATEMENT',array('*'))
					->where('HIS_USER_ID = '.$this->_db->quote($this->_userIdLogin))
					->where('HIS_TYPE = ?','2')
					->order('ID DESC')
					->limit(5)
				);

				// $this->view->historyDatastate = $hisDatastate;
				$optHtml = '';
				if(!empty($hisDatastate)){
					foreach ($hisDatastate as $key => $value) {
						$optHtml .= '<div class="page-bar"></div><div class="form-group row">';
						$optHtml .= '<label class="control-label col-md-4">'.$value['HIS_ACCT'].'</label><label class="control-label col-md-6"><i class="far fa-calendar-alt"></i>&nbsp;'.Application_Helper_General::convertDate($value['HIS_DATE_START'], 'dd MMMM y').' - '.Application_Helper_General::convertDate($value['HIS_DATE_END'], 'dd MMMM y').'</label><div class="col-md-2">';
						$optHtml .= '<a href="javascript:;" onclick="directfile('.$value['HIS_ACCT'].','.$value['HIS_DATE_START'].','.$value['HIS_DATE_END'].','.$value['HIS_FILE'].')"><i class="fa fa-file-download" title="Download"></i></a>';
						$optHtml .= '<a href="javascript:;" value="'.$value['ID'].'" onclick="deletefile(this)"><i class="fa fa-trash" title="Remove"></i></a></div></div>';
						$optHtml .= '';
						$optHtml .= '';
					}
				}
				// $optHtml .= '';
				
	
          echo $optHtml;
    }


    public function deleteviewAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $tblName = $this->_getParam('id');

     

//      if ($priviBeneLinkage == true)
//      {
           		 $this->_db->beginTransaction();
				
				$where['ID = ?'] = $tblName;
				$this->_db->delete('T_HISTORY_STATEMENT',$where);
				
				
				$this->_db->commit();
				
				Application_Helper_General::writeLog('GRAM','Deleting History Statement'.$tblName);

				$hisDatastate = $this->_db->fetchAll(
					$this->_db->select()
					->from('T_HISTORY_STATEMENT',array('*'))
					->where('HIS_USER_ID = '.$this->_db->quote($this->_userIdLogin))
					->where('HIS_TYPE = ?','1')
					->order('ID DESC')
					->limit(5)
				);

				// $this->view->historyDatastate = $hisDatastate;
				$optHtml = '';
				if(!empty($hisDatastate)){
					foreach ($hisDatastate as $key => $value) {
						$optHtml .= '<div class="page-bar"></div><div class="form-group row">';
						$optHtml .= '<label class="control-label col-md-4">'.$value['HIS_ACCT'].'</label><label class="control-label col-md-6"><i class="far fa-calendar-alt"></i>&nbsp;'.Application_Helper_General::convertDate($value['HIS_DATE_START'], 'dd MMMM y').' - '.Application_Helper_General::convertDate($value['HIS_DATE_END'], 'dd MMMM y').'</label><div class="col-md-2">';
						$optHtml .= '<a href="javascript:;" onclick="direct('.$value['HIS_ACCT'].','.$value['HIS_DATE_START'].','.$value['HIS_DATE_END'].')"><i class="fa fa-eye" title="View"></i></a>';
						$optHtml .= '<a href="javascript:;" value="'.$value['ID'].'" onclick="deleteview(this)"><i class="fa fa-trash" title="Remove"></i></a></div></div>';
						$optHtml .= '';
						$optHtml .= '';
					}
				}
				
				
	
          echo $optHtml;
    }

}
