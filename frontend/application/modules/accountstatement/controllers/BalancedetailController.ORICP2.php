<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/SystemBalance.php';
class accountstatement_BalancedetailController extends Application_Main
{
	protected $_moduleDB = 'RTF'; // masih harus diganti

	public function indexAction()
	{ 
		$fields = array(
						'acct'  => array('field' => 'ACCT_NO',
											   'label' => 'Account Number',
											   'sortable' => true),
						'acct_name'  => array('field' => 'ACCT_NAME',
											   'label' => 'Account Name',
											   'sortable' => true),
						'ccy'   => array('field'    => 'CCY_ID',
											  'label'    => 'CCY',
											  'sortable' => true),
						'status'   => array('field'    => '',
											  'label'    => 'Account Status',
											  'sortable' => true),
						'available'   => array('field'    => '',
											  'label'    => 'Available Balance',
											  'sortable' => true),
						'clearing' => array('field'    => '',
											  'label'    => 'Yesterday Clearing',
											  'sortable' => true),
						'effective'   => array('field'    => '',
											  'label'    => 'Effective Balance',
											  'sortable' => true)
				);
				
		$sortBy  = $this->_getParam('sortby','doc_no');
		$sortDir = $this->_getParam('sortdir','asc');
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';
			
		$custInfo = $this->_db->fetchRow(
					$this->_db->select()
						->from(array('C' => 'M_CUSTOMER'))
						->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin))
				);
		$this->view->custname	= $custInfo['CUST_NAME'];
		$this->view->custadd	= $custInfo['CUST_ADDRESS'];
		$this->view->custcity	= $custInfo['CUST_CITY'];
		$this->view->custprov	= $custInfo['CUST_PROVINCE'];
		$this->view->custzip	= $custInfo['CUST_ZIP'];
		$this->view->custphone	= $custInfo['CUST_PHONE'];
		$this->view->custfax	= $custInfo['CUST_FAX'];
										  
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		$data = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'M_CUSTOMER_ACCT'))
						 ->join(array('C' => 'M_MAKERLIMIT'),'A.ACCT_NO = C.ACCT_NO',array())
						 ->joinLeft(array('B' => 'M_GROUPING'),'A.GROUP_ID = B.GROUP_ID',array('GROUP_NAME'))
						 ->where("A.ACCT_STATUS = 1")
						 ->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin))
						 ->where("C.USER_LOGIN = ".$this->_db->quote($this->_userIdLogin))
						 ->where("C.MAXLIMIT > 0")
						 ->order('B.GROUP_NAME DESC')
						 ->order('A.ORDER_NO ASC')
				);
				
		$isGroup = false;
		$isSystemBalance = false;
		$isTolerance = false;
		if(count($data))
		{
			foreach($data as $row)
			{
				if($row['GROUP_ID'])
				{
					$isGroup = true;
				}
				if($row['ISSYSTEMBALANCE'])
				{
					$isSystemBalance = true;
				}
				
				if($row['ISPLAFONDDATE'] && strtotime($row['PLAFOND_START']) <= strtotime(date('Y-m-d')) && strtotime($row['PLAFOND_END']) >= strtotime(date('Y-m-d'))  )
				{
					$isTolerance = true;
				}
				
			}
		}
		
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');
		
		$this->view->isGroup = $isGroup;
		$this->view->isSystemBalance = $isSystemBalance;
		$this->view->isTolerance = $isTolerance;
		$this->view->custId = $this->_custIdLogin;
		$this->view->resultdata = $data;
		$this->view->fields = $fields;
		
		$logDesc = 'Viewing';
	if($csv)
		{
			$arr = array();
			
			if(!$isSystemBalance){
				$header = array(
					'Account No',
					'Account Alias',
					'Account Name',
					'CCY',
					'Type',
					'Status',
					'Available Balance',
					'Yesterday Clearing',
					'Effective Balance'
				);
			}else{
				if($isTolerance){
					$header = array(
						'Account No',
						'Account Alias',
						'Account Name',
						'CCY',
						'Type',
						'Status',
						'Available Balance',
						'Plafond',
						'Outstanding',
						'Yesterday Clearing',
						'Effective Balance',
						'Tolerance',
						'Tolerance Start Date',
						'Tolerance End Date',
						'Maker Amount Hold',
						'System Balance',
					);
				}else{
					$header = array(
						'Account No',
						'Account Alias',
						'Account Name',
						'CCY',
						'Type',
						'Status',
						'Available Balance',
						'Plafond',
						'Outstanding',
						'Yesterday Clearing',
						'Effective Balance',
						'Tolerance',
						'Maker Amount Hold',
						'System Balance',
					);
				}
				
				
			}
			if(!$isGroup)
			{
				$arr[0] = $header;
				$i = 1;
				$arr_value_tot_group =  array();
				$arr_display_tot_group =  array();
				foreach($data as $row)
				{
					
					$systemBalance = new SystemBalance($row['CUST_ID'],$row ['ACCT_NO'],Application_Helper_General::getCurrNum($row ['CCY_ID']));
					$systemBalance->checkBalance();
					
					
					if(isset($arr_value_tot_group[$row ['CCY_ID']])) {
			          $arr_value_tot_group[$row ['CCY_ID']] += $systemBalance->getAvailableBalance();
			        }
			        else {
			          $arr_value_tot_group[$row ['CCY_ID']] = $systemBalance->getAvailableBalance();
			        }
					$arr_display_tot_group[$row ['CCY_ID']] = $row ['CCY_ID']." ". Application_Helper_General::displayMoney($arr_value_tot_group[$row ['CCY_ID']]);
					$accountStatusModified =($row['FREEZE_STATUS']!='09')?$systemBalance->getCoreAccountStatusDesc():'Freezed';
					if(!$isSystemBalance){				
						$arr[$i][] = $row['ACCT_NO'];
						$arr[$i][] = $row['ACCT_ALIAS_NAME'];
						$arr[$i][] = $systemBalance->getCoreAccountName();
						$arr[$i][] = $row['CCY_ID'];
						$arr[$i][] = $row ['ACCT_DESC'];
						$arr[$i][] = $accountStatusModified;
						$arr[$i][] = $systemBalance->getAvailableBalance();
						$arr[$i][] = $systemBalance->getYesterdayClearing();
						$arr[$i][] = $systemBalance->getEffectiveBalance();
					}else{
						$arr[$i][] = $row['ACCT_NO'];
						$arr[$i][] = $row['ACCT_ALIAS_NAME'];
						$arr[$i][] = $systemBalance->getCoreAccountName();
						$arr[$i][] = $row['CCY_ID'];
						$arr[$i][] = $row ['ACCT_DESC'];
						$arr[$i][] = $accountStatusModified;
						$arr[$i][] = $systemBalance->getAvailableBalance();
						$arr[$i][] = $systemBalance->getCorePlafond();
						$arr[$i][] = $systemBalance->getCoreOutstandingPlafond();
						$arr[$i][] = $systemBalance->getYesterdayClearing();
						$arr[$i][] = $systemBalance->getEffectiveBalance();
						
						$plafondStart = $row ['PLAFOND_START'] ? Application_Helper_General::convertDate($row ['PLAFOND_START'],$this->displayDateFormat,$this->defaultDateFormat) : 'N/A';
						$plafondEnd  = $row ['PLAFOND_END'] ? Application_Helper_General::convertDate($row ['PLAFOND_END'],$this->displayDateFormat,$this->defaultDateFormat) : 'N/A';
						$sysBal = ($systemBalance->getSystemBalance()=='N/A') ? $systemBalance->getSystemBalance() : Application_Helper_General::displayMoney($systemBalance->getSystemBalance());
						$tolerance = ($systemBalance->getTolerance()=='N/A') ? $systemBalance->getTolerance() : Application_Helper_General::displayMoney($systemBalance->getTolerance());
						$arr[$i][] = $tolerance;
						if($isTolerance){
							$arr[$i][] = $plafondStart;
							$arr[$i][] = $plafondEnd;
						}
						
						if($systemBalance->systemBalanceIsActive()){
							$makerAmountHold = Application_Helper_General::displayMoney($systemBalance->getMakerAmountHold());
						}else{
							$makerAmountHold = 'N/A';
						}
						
						
						
						$arr[$i][] = $makerAmountHold;
						$arr[$i][] = $sysBal;
						
					}
					$i++;
				}
				$display_tot_group = implode("; ",$arr_display_tot_group);
				$arr[$i][] = 'TOTAL AVAILABLE BALANCE : '.$display_tot_group;	
			}
			else
			{
				$i = 0;
				$group = null;
				
				$resultdataGroup =  array();
				foreach ($data as $rowGroup){
					$resultdataGroup[$rowGroup['GROUP_NAME']][] = $rowGroup; 
				}
				$i = 0;
				$group = null;
				$arr_value_tot_group = array();
				foreach ( $resultdataGroup as $groupname=>$rowPerGroup ) 
				{
					$i++;
					$td_css = ($i % 2 == 0) ? 'tbl-evencontent' : 'tbl-oddcontent';
					$groupname = ($groupname) ? $groupname : 'OTHERS';
					$arr[$i][] = $groupname;
					$i++;
					$arr[$i][] = $header;
					
						if(count($rowPerGroup) > 0){
							$arr_value_tot_group =  array();
							$arr_display_tot_group =  array();
							foreach ($rowPerGroup as $row){
							$i++;	
							$systemBalance = new SystemBalance($this->_custIdLogin,$row ['ACCT_NO'],Application_Helper_General::getCurrNum($row ['CCY_ID']));
							$systemBalance->checkBalance();
								
								if(isset($arr_value_tot_group[$row ['CCY_ID']])) {
									$arr_value_tot_group[$row ['CCY_ID']] += $systemBalance->getAvailableBalance();
								}else{
									$arr_value_tot_group[$row ['CCY_ID']] = $systemBalance->getAvailableBalance();
								}
								$arr_display_tot_group[$row ['CCY_ID']] = $row ['CCY_ID']." ". Application_Helper_General::displayMoney($arr_value_tot_group[$row ['CCY_ID']]);
							$accountStatusModified =($row['FREEZE_STATUS']!='09')?$systemBalance->getCoreAccountStatusDesc():'Freezed';
							if(!$isSystemBalance){				
								$arr[$i][] = $row['ACCT_NO'];
								$arr[$i][] = $row['ACCT_ALIAS_NAME'];
								$arr[$i][] = $systemBalance->getCoreAccountName();
								$arr[$i][] = $row['CCY_ID'];
								$arr[$i][] = $row ['ACCT_DESC'];
								$arr[$i][] = $accountStatusModified;
								$arr[$i][] = $systemBalance->getAvailableBalance();
								$arr[$i][] = $systemBalance->getYesterdayClearing();
								$arr[$i][] = $systemBalance->getEffectiveBalance();
							}else{
								$arr[$i][] = $row['ACCT_NO'];
								$arr[$i][] = $row['ACCT_ALIAS_NAME'];
								$arr[$i][] = $systemBalance->getCoreAccountName();
								$arr[$i][] = $row['CCY_ID'];
								$arr[$i][] = $row ['ACCT_DESC'];
								$arr[$i][] = $accountStatusModified;
								$arr[$i][] = $systemBalance->getAvailableBalance();
								$arr[$i][] = $systemBalance->getCorePlafond();
								$arr[$i][] = $systemBalance->getCoreOutstandingPlafond();
								$arr[$i][] = $systemBalance->getYesterdayClearing();
								$arr[$i][] = $systemBalance->getEffectiveBalance();
								
								$plafondStart = $row ['PLAFOND_START'] ? Application_Helper_General::convertDate($row ['PLAFOND_START'],$this->view->displayDateFormat,$this->view->defaultDateFormat) : 'N/A';
								$plafondEnd  = $row ['PLAFOND_END'] ? Application_Helper_General::convertDate($row ['PLAFOND_END'],$this->view->displayDateFormat,$this->view->defaultDateFormat) : 'N/A';
								$sysBal = ($systemBalance->getSystemBalance()=='N/A') ? $systemBalance->getSystemBalance() : Application_Helper_General::displayMoney($systemBalance->getSystemBalance());

								
								$tolerance = $row ['PLAFOND'] ? Application_Helper_General::displayMoney($row ['PLAFOND']) : 'N/A';
								$plafondStart = $row ['PLAFOND_START'] ? Application_Helper_General::convertDate($row ['PLAFOND_START'],$this->displayDateFormat,$this->defaultDateFormat) : 'N/A';
								$plafondEnd  = $row ['PLAFOND_END'] ? Application_Helper_General::convertDate($row ['PLAFOND_END'],$this->displayDateFormat,$this->defaultDateFormat) : 'N/A';
								$sysBal = ($systemBalance->getSystemBalance()=='N/A') ? $systemBalance->getSystemBalance() : Application_Helper_General::displayMoney($systemBalance->getSystemBalance());
								$tolerance = ($systemBalance->getTolerance()=='N/A') ? $systemBalance->getTolerance() : Application_Helper_General::displayMoney($systemBalance->getTolerance());
										
								
								$arr[$i][] = $tolerance;
								if($isTolerance){
									$arr[$i][] = $plafondStart;
									$arr[$i][] = $plafondEnd;
								}
								
								if($systemBalance->systemBalanceIsActive()){
									$makerAmountHold = Application_Helper_General::displayMoney($systemBalance->getMakerAmountHold());
								}else{
									$makerAmountHold = 'N/A';
								}
								$arr[$i][] = $makerAmountHold;
								$arr[$i][] = $sysBal;
								
							}
								
							}
						}
						$i++;	
						$display_tot_group = implode("; ",$arr_display_tot_group);	
						$arr[$i][] = 'TOTAL AVAILABLE BALANCE : '.$display_tot_group;
					
				} 
										
			}
				$logDesc = 'Export to CSV';
				$this->_helper->download->csv(array(),$arr,null,'Balance Detail');  
		
		}
		
		if($pdf){
			Application_Helper_General::writeLog('BAIQ','Print PDF');
			$outputHTML = "<tr><td>".$this->view->render($this->view->controllername.'/pdf.phtml')."</td></tr>";
			$this->_helper->download->pdf(null,null,null,'Balance Detail',$outputHTML);
		}
		
		Application_Helper_General::writeLog('BAIQ',$logDesc);
	}
	
	
}
