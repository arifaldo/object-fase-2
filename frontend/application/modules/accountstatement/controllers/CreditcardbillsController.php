<?php
require_once 'Zend/Controller/Action.php';

class accountstatement_CreditcardbillsController extends Application_Main
{
	protected $_moduleDB = 'RTF'; // masih harus diganti
	
	/*public function initModel()
	{
		
		$this->param['USER_ID'] 		= $this->_userIdLogin;
		$this->CustomerUser 			= new CustomerUser($this->_userIdLogin);
	}*/
	
	
	public function indexAction()
	{
		$model = new accountstatement_Model_Accountstatement();
		$this->view->ccArr = $model->getUserAccount(array('acctType' => 11,'userId' =>$this_userIdLogin));
		$data = array();

		if($this->_request->isPost()){
			$filters = array(
			                 'cc_number'     => array('StringTrim','StripTags'),
							);

			$validators = array(
								'cc_number'    => array(
															'NotEmpty',
															'messages' => array(
																              $this->language->_('Error: You must select card no'),
														                      )
														   ),
							   );
		
							   
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

			if($zf_filter_input->isValid()){
				//inquiry bills here
			}
			else{
				foreach($validators as $key=>$value)
			  	{
					$this->view->$key = $this->_getParam($key);
			  	}
		  	    
		  	    $error = $zf_filter_input->getMessages();

		  	    foreach($error as $keyRoot => $rowError)
		        {
		           foreach($rowError as $errorString)
		           {
		              	$keyname = "error_" . $keyRoot;
						$this->view->$keyname = $this->language->_($errorString);
		           }
		        }
			}
		}

		//Application_Helper_General::writeLog('BAIQ',$logDesc);
		//$this->view->resultdata = $data;
	}
	
}
