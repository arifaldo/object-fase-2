<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
class accountstatement_CreditcarddetailController extends Application_Main
{
	protected $_moduleDB = 'RTF'; // masih harus diganti
	/*public function initModel()
	{
		
		$this->param['USER_ID'] 		= $this->_userIdLogin;
		$this->CustomerUser 			= new CustomerUser($this->_userIdLogin);
	}*/
	
	
	public function indexAction()
	{
		/*$fields = array(
						'acct'  => array('field' => 'ACCT_NO',
											   'label' => $this->language->_('Account Number'),
											   'sortable' => true),
						'acct_name'  => array('field' => 'ACCT_NAME',
											   'label' => $this->language->_('Account Name'),
											   'sortable' => true),
						'ccy'   => array('field'    => 'CCY_ID',
											  'label'    => $this->language->_('CCY'),
											  'sortable' => true),
						'status'   => array('field'    => '',
											  'label'    => $this->language->_('Account Status'),
											  'sortable' => true),
						'available'   => array('field'    => '',
											  'label'    => $this->language->_('Available Balance'),
											  'sortable' => true),
						'clearing' => array('field'    => '',
											  'label'    => $this->language->_('Yesterday Clearing'),
											  'sortable' => true),
						'effective'   => array('field'    => '',
											  'label'    => $this->language->_('Effective Balance'),
											  'sortable' => true)
				);*/
		$model = new accountstatement_Model_Accountstatement();
		$custInfo = $model->getUser($this->_userIdLogin);
		$temp = $model->getUserAccount(array('userId'=>$this->_userIdLogin));
		$CustomerUser 			= new CustomerUser($this->_userIdLogin);
		$userData = $CustomerUser->getUser($this->_userIdLogin);
			
		$this->view->tokentype 		= $userData['TOKEN_TYPE'];
		$this->view->tokenIdUser 	= $userData['TOKEN_ID'];
		$tokenIdUser 				= $userData['TOKEN_ID'];
		$tokenType 					= $userData['TOKEN_TYPE'];
		$userMobilePhone 			= trim($userData['USER_MOBILE_PHONE']);
		$USER_ISLOCKED 				= $userData['USER_ISLOCKED'];
		//added new hard token
		$HardToken 						= new Service_Token($this->_custIdLogin, $this->_userIdLogin, $tokenIdUser);
		$challengeCode 					= $HardToken->generateChallengeCode();
		$this->view->challengeCode 		= $challengeCode;
		$this->view->challengeCodeReq 	= $challengeCode;
		
		/*$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');
		$totalAvailableBalance = 0;
		if (count ( $temp ) > 0) 
		{
			$data = array();
			foreach ( $temp as $key=>$row ) {
				$Account = new Account($row ['ACCT_NO'],Application_Helper_General::getCurrNum($row ['CCY_ID']));
				$Account->setFlag(false);
				$Account->checkBalance();
				$data[$key]['ACCT_NO'] = $row ['ACCT_NO'];
				$data[$key]['ACCT_ALIAS_NAME'] = $row ['ACCT_ALIAS_NAME'];
				$data[$key]['ACCT_NAME'] = $Account->getCoreAccountName();
				$data[$key]['CCY_ID'] = $row ['CCY_ID'];
				$data[$key]['ACCT_DESC'] = $row ['ACCT_DESC'];
				$data[$key]['FREEZE_STATUS'] = $accountStatusModified =($row['FREEZE_STATUS']!='09')?$Account->getCoreAccountStatusDesc():'Freezed';
//				$data[$key]['AVAILABLE_BALANCE'] = $Account->getAvailableBalance();
				$data[$key]['AVAILABLE_BALANCE'] = Application_Helper_General::displayMoney($Account->getAvailableBalance());
						
//				Zend_Debug::dump($Account->getAvailableBalance());
				$totalAvailableBalance = $totalAvailableBalance + $Account->getAvailableBalance();
			}
		}

		$logDesc = 'Viewing';
		
		if($csv || $pdf){
			$arr = $data;
			$header = array(
				$this->language->_('Deposit Number'),
				$this->language->_('On Behalf Of'),
				$this->language->_('CCY'),
				$this->language->_('Time Period'),
				$this->language->_('Amount'),
				$this->language->_('Due Date'),
			);
		}
		if($csv)
		{
			$logDesc = 'Export to CSV';
			$this->_helper->download->csv($header,$arr,null,$this->language->_('Deposit Detail'));  
		}
		
		if($pdf){
			$logDesc = 'Export to PDF';
			$this->_helper->download->pdf($header,$arr,null,$this->language->_('Deposit Detail'));  
		}
		
		
		if($this->_request->getParam('print') == 1){
			$arr = $data;
			$fields = array(
						'ACCT_NO'  => array('field' => 'ACCT_NO',
											   'label' => $this->language->_('Account Number'),
											   'sortable' => true),
						'ACCT_NAME'  => array('field' => 'ACCT_NAME',
											   'label' => $this->language->_('Account Name'),
											   'sortable' => true),
						'CCY_ID'   => array('field'    => 'CCY_ID',
											  'label'    => $this->language->_('CCY'),
											  'sortable' => true),
			
						'ACCT_DESC'   => array('field'    => 'ACCT_DESC',
											  'label'    => $this->language->_('Type'),
											  'sortable' => true),
			
						'FREEZE_STATUS'   => array('field'    => 'FREEZE_STATUS',
											  'label'    => $this->language->_('Status'),
											  'sortable' => true),
						'AVAILABLE_BALANCE'   => array('field'    => 'AVAILABLE_BALANCE',
											  'label'    => $this->language->_('Available Balance'),
											  'sortable' => true)
			);
			
//            $data = $arr;//$this->_dbObj->fetchAll($select);
            $this->_forward('print', 'index', 'widget', array('data_content' => $arr, 'data_caption' => 'Deposit Inquiry', 'data_header' => $fields));
        }
		
		
		Application_Helper_General::writeLog('BAIQ',$logDesc);
		$this->view->userId = $this->_userIdLogin;
		$this->view->resultdata = $data;
		$this->view->fields = $fields;
		$this->view->totalAvailableBalance = $totalAvailableBalance;*/
	}

	public function successAction(){
		
	}
	
	
}
