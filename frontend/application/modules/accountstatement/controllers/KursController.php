<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Account.php';
class accountstatement_KursController extends Application_Main
{
	protected $_moduleDB = 'RTF'; // masih harus diganti

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$app = Zend_Registry::get('config');
		$app = $app['app']['bankcode'];
				
		$userId = $this->_userIdLogin;
		$custId = $this->_custIdLogin;
		
		$param = array('USER_ID'=>$userId, 'CUST_ID'=>$custId);
		
		$model = new accountstatement_Model_Accountstatement();		

		$userAcctInfo = $model->getCustomerAccountKurs($param);
//die;		
//print_r($userAcctInfo );die;
		$accountNo 	= $userAcctInfo[0]['ACCT_NO'];
		$accountType = $userAcctInfo[0]['ACCT_TYPE'];
		$accountCcy = $userAcctInfo[0]['CCY_ID'];
				
		$svcInquiry = new Service_Inquiry($userId, $accountNo, $accountType, $accountCcy);		
		$result		= $svcInquiry->rateInquiry();

		$fields = array(
						'ccy'  => array('field' => '',
											   'label' => $this->language->_('CCY'),
											   'sortable' => false),
						'sell'  => array('field' => '',
											   'label' => $this->language->_('Sell'),
											   'sortable' => false),
						'buy'   => array('field'    => '',
											  'label'    => $this->language->_('Buy'),
											  'sortable' => false),
				);

		$data = array();
		
		$data = $result['DataList'];
		
		//print_r($result);die;
		Application_Helper_General::writeLog('ERIQ',$logDesc);

		$this->view->userId = $this->_userIdLogin;
		$this->view->resultdata = $data;
		$this->view->fields = $fields;
	}
	
}
