<?php

class customerservice_CreateController extends Application_Main
{
	public function indexAction()
	{
			$this->_helper->layout()->setLayout('newlayout');
		$pleaseSelect = '-- '.$this->language->_('Please Select'). ' --';
		$questioncategory 		= $this->_questioncategory;

		$questioncategoryArray 	= array( '' => $pleaseSelect);
		$questioncategoryArray += array_combine(array_values($questioncategory['code']),array_values($questioncategory['desc']));


		foreach($questioncategoryArray as $key => $value){ if($key != 5) $optpayStatusRaw[$key] = $this->language->_($value); }

		$optPayType = $optpayStatusRaw;
		$this->view->questioncategoryArray 	= $optPayType;

		//$this->view->questioncategoryArray 		= $questioncategoryArray;

		//Zend_Debug::dump($questioncategory['desc']);

		if($this->_getParam('submit') == true)
		{
			$filters = 	array(
									'category'	=> 	array('StringTrim','StripTags'),
									'question'	=> 	array('StringTrim','StripTags'),
								);
			$validator = array	(
									'category' 	=> 	array	(
																array	(
																			'NotEmpty',
																			array('inArray',$questioncategory['code']),
																		),
																'messages' => array	(
																						$this->language->_('Error').": ".$this->language->_('Category has not fill yet').".",
																						$this->language->_('Error').": ".$this->language->_('Category has not been exist').".",
																					)
															),
									'question'	=> 	array	(
																'NotEmpty',
																'messages' => array	(
																						$this->language->_('Error').": ".$this->language->_('Question has not fill yet').".",
																					)
															),
								);

			$param['category']	= $this->_getParam('category');
			$param['question']	= $this->_getParam('question');

			$zf_filter_input = new Zend_Filter_Input($filters, $validator, $param, $this->_optionsValidator);

			if($zf_filter_input->isValid())
			{
				try
				{
					$arrayInsert = array(
											'USER_ID' 			=> $this->_userIdLogin,
											'QUESTION_DATE' 	=> new Zend_Db_Expr('GETDATE()'),
											'QUESTION_CATEGORY' => $param['category'],
											'QUESTION_Q' 		=> $param['question'],
										);
					$insert = $this->_db->insert('CUSTOMER_SERVICES',$arrayInsert);

					Application_Helper_General::writeLog('CSCQ','Customer Service Create Question');
		 			//Application_Helper_General::writeLog('CSVQ','Customer Service View Question');
		 			//$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
					//$this->_redirect('/notification/success/index/result/cs');

		 			$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
					$this->_redirect('/notification/success');


				}
				catch(Exception $e)
				{
					$this->view->failErr = "Data Fail To Insert";
					$this->view->category = $param['category'];
					$this->view->question = $param['question'];
				}
			}
			else
			{
				$errors 				 = $zf_filter_input->getMessages();

				$this->view->categoryErr = (isset($errors['category']))? $errors['category'] : null;
				$this->view->questionErr = (isset($errors['question']))? $errors['question'] : null;

				$this->view->category = $param['category'];
				$this->view->question = $param['question'];
			 	//Application_Helper_General::writeLog('CSVQ','Customer Service View Question');

			}

			//Application_Helper_General::writeLog('BAIQ',$logDesc);
			//Application_Helper_General::writeLog('CSAD','User has been Added, User ID : '.$zf_filter_input->user_id. ' User Name : '.$user_data['USER_FULLNAME'].' Change id : '.$change_id);
		 	//Application_Helper_General::writeLog('CSCQ','Customer Service Create Question');
		 	//Application_Helper_General::writeLog('CSVQ','Customer Service View Question');
		}
		Application_Helper_General::writeLog('CSCQ','Customer Service Create Question');
	}
}
