<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/SinglePayment.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';

class rdntrans_RtgskseiController extends Application_Main
{
	protected $_moduleDB = 'RTF';

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		
		$futureDate = date('d/m/Y', strtotime('+1 days', strtotime(date('Y-m-d')))); 

		$anyValue = '-- '.$this->language->_('Select City'). ' --';
		$select = $this->_db->select()
				->from(array('A' => 'M_CITY'),array('*'));
		$select ->order('CITY_NAME ASC');
		$arr = $this->_db->fetchall($select);
		
		$cityCodeArr 			= array(''=> $anyValue);
		$cityCodeArr 			+= Application_Helper_Array::listArray($arr,'CITY_CODE','CITY_NAME');
		$this->view->cityCodeArr 	= $cityCodeArr;
		
		
		$Settings = new Settings();
		$address_mandatory = $Settings->getSettingNew('address_mandatory');
		$this->view->address_mandatory = $address_mandatory; 
		
		// Add by Hamdan
		$tranferdatetype = $this->_request->getParam('tranferdatetype');
		$this->view->tranferdatetype = $tranferdatetype;
		
		$date_val	= date('d/m/Y');
		$PS_EFDATE_VAL = $this->_request->getParam('PS_EFDATE');
		$this->view->paymentDate = $PS_EFDATE_VAL;
		if($PS_EFDATE_VAL > $date_val){
			$pendingfuturedate = $this->language->_('Future Date Transfer');
			$this->view->paymentType = $pendingfuturedate;
		}
		elseif ($PS_EFDATE_VAL == $date_val){
			$todaytranfer = $this->language->_('Today Transfer');
			$this->view->paymentType = $todaytranfer;
		}
		else{
			$pendingfuturedate = $this->language->_('Future Date Transfer');
			$this->view->paymentType = $pendingfuturedate;
		}

		if($this->_custSameUser){
			// echo 'here1';
			$this->view->token = true;
			$userOnBehalf = $this->_userIdLogin;
			$tokenIdUser = $this->_db->select()
			->from(
				array('M_USER'),
				array('TOKEN_ID')
			)
			->where('USER_ID = ?',$userOnBehalf)
			->where('CUST_ID = ?',$this->_custIdLogin)
			->limit(1)
		;

		$tokenIdUser = $this->_db->fetchRow($tokenIdUser);
		$tokenIdUser = $tokenIdUser['TOKEN_ID'];

			$Token 			= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
			$challengeCode 	= $Token->generateChallengeCode();
			$this->view->userOnBehalf		= $userOnBehalf;
			$this->view->challengeCode		= $challengeCode;
		}
		
		
		$PS_PERIODIC_EVERY = $this->_request->getParam('PERIODIC_EVERY');
		$PS_PERIODIC_EVERY_DATE = $this->_request->getParam('PERIODIC_EVERYDATE');
		$PS_ENDDATE_VAL = ($this->_request->getParam('PS_ENDDATEPERIODIC'))?$this->_request->getParam('PS_ENDDATEPERIODIC'):date('d/m/Y');
		$this->view->endDatePeriodic = $PS_ENDDATE_VAL;		
				
		$jenisTransfer =  $this->_request->getParam('tranferdatetype');
		$jenisPeriodic =  $this->_request->getParam('tranferdateperiodictype');
		
		//die ($jenisTransfer) ;
		if ($jenisTransfer == '1'){
			$todaytranfer = $this->language->_('Today Transfer');
			$this->view->paymentType = $todaytranfer;
		}elseif ($jenisTransfer == '2'){
			$pendingfuturedate = $this->language->_('Future Date Transfer');
			$this->view->paymentType = $pendingfuturedate;
		}elseif ($jenisTransfer == '3'){
			$periodictranfer = $this->language->_('Periodic Transfer');
			$this->view->paymentType = $periodictranfer;
			
			if ($jenisPeriodic == '5'){
				$this->view->periodicValue = $PS_PERIODIC_EVERY;
			}elseif ($jenisPeriodic == '6'){
				$this->view->periodicValue = $PS_PERIODIC_EVERY_DATE;
			}
		}
		// End
		
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}
    	}

    	$USE_CONFIRM_PAGE = true;
		
    	$periodicEveryArr = array(
    								'1' => $this->language->_('Monday'), 
    								'2' => $this->language->_('Tuesday'), 
    								'3' => $this->language->_('Wednesday'), 
    								'4' => $this->language->_('Thursday'), 
    								'5' => $this->language->_('Friday'), 
    								'6' => $this->language->_('Saturday'), 
    								'7' => $this->language->_('Sunday'),);
		$periodicEveryDateArr = range(1,31);
    	
    	// Set variables needed in view
    	$paramSettingID = array('range_futuredate', //'auto_release', 'cut_off_time_inhouse'
								'cut_off_time_kse'   , 'cut_off_time_kse',
								'threshold_rtgs'     , 'threshold_lld');

		$settings 			= new Application_Settings();
		$settings->setSettings(null, $paramSettingID);							// Zend_Registry => 'APPSETTINGS'
		$ccyList  			= $settings->setCurrencyRegistered();				// Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
		$lldTypeArr  		= $settings->getLLDDOMType();
		$lldCategoryArr  	= $settings->getLLDDOMCategory();
		$lldIdenticalArr  	= $settings->getLLDDOMIdentical();
		$lldRelationshipArr = $settings->getLLDDOMRelationship();
		$lldPurposeArr 		= $settings->getLLDDOMPurpose();
		$lldBeneIdentifArr 	= $settings->getLLDDOMBeneIdentification();
		$lldSenderIdentifArr = $settings->getLLDDOMSenderIdentification();
		$residentArr 		= array_combine($this->_citizenship["code"], $this->_citizenship["desc"]);
 		$citizenshipArr		= array("W" => "WNI", "N" => "WNA");

		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr 	  = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR"), "ISRDN_TRX" => "1"));	// show acc in IDR only and ISRDN_TRX = 1

		// print_r($AccArr);die();

		$filter 		= new Application_Filtering();
		$PS_NUMBER 		= $filter->filter($this->_getParam('PS_NUMBER'), "PS_NUMBER");
		$PS_EFDATE 		= Application_Helper_General::convertDate($this->getCurrentDate());
		$isConfirmPage 	= $this->_getParam('confirmPage');

		$process 	= $this->_getParam('process');
		//var_dump($process);die;
		$submitBtn 	= ($this->_request->isPost() )? true: false;

		// get ps_number data for repair payment
    	if (!empty($PS_NUMBER) && !$this->_request->isPost())
    	{
    		$paramList = array("WA" 			=> false,
							   "ACCOUNT_LIST" 	=> $this->_accountList,
							   "_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
							 );
    		$select   = $CustomerUser->getPayment($paramList, false);	// not distinct, cause text cannot be selected as DISTINCT, it's not comparable
			$select->where('P.PS_NUMBER = ?' , (string) $PS_NUMBER);

			$select->columns(array(	"tra_message"			=> "T.TRA_MESSAGE",
									"tra_refno"				=> "T.TRA_ADDITIONAL_MESSAGE",
									"acbenef_email"			=> "T.BENEFICIARY_EMAIL",
									"acbenef_address"		=> "T.BENEFICIARY_ADDRESS",
 									"acbenef_citizenship"	=> "T.BENEFICIARY_CITIZENSHIP",
									"acbenef_resident"		=> "T.BENEFICIARY_RESIDENT",
									"bank_name"				=> "T.BENEFICIARY_BANK_NAME",
									//"bank_city"				=> "T.BENEFICIARY_BANK_CITY",
									"clr_code"				=> "T.CLR_CODE",
									"lld_desc"				=> "T.LLD_DESC",
									"lld_code"				=> "T.LLD_CODE",
									"transfer_type"			=> "T.TRANSFER_TYPE",
									//"acbenef_bankname"		=> "T.BENEFICIARY_ACCOUNT_NAME",
									//"acbenef_alias"			=> "T.BENEFICIARY_ALIAS_NAME",
							  	    "acbenef_cat"				=> "T.BENEFICIARY_CATEGORY",
								    "acbenef_id_type"		=> "T.BENEFICIARY_ID_TYPE",
								    "acbenef_id_number"		=> "T.BENEFICIARY_ID_NUMBER",
								    "acbenef_city_code"		=> "T.BENEFICIARY_CITY_CODE",
								)
							 );

			$pslipData 			= $this->_db->fetchRow($select);
			if (!empty($pslipData))
			{
				//tambahn pentest
				$PS_EFDATE_ORI  		= date("d/m/Y",strtotime($pslipData['efdate']));
				if($PS_EFDATE_ORI > date('d/m/Y')){
					$TransferDate 	=  "2"; //future
					$PS_EFDATE = $PS_EFDATE_ORI;	
				}else{
					$TransferDate	=  "1"; //imediate
					$PS_EFDATE = date('d/m/Y');
				}
				
				$PS_SUBJECT 		= $pslipData['paySubj'];
				// $PS_EFDATE  		= $pslipData['efdate'];
//				$PS_EFDATE  		= date("d/m/Y",strtotime($pslipData['efdate']));

				$TRA_AMOUNT  		= Application_Helper_General::displayMoney($pslipData['amount']);
				$TRA_MESSAGE 		= $pslipData['tra_message'];
				$TRA_REFNO 			= $pslipData['tra_refno'];
				$ACCTSRC 			= $pslipData['accsrc'];
				$ACBENEF  			= $pslipData['acbenef'];
				$ACBENEF_CCY  		= $pslipData['acbenef_ccy'];
				$ACBENEF_BANKNAME	= $pslipData['acbenef_bankname'];
				$ACBENEF_ALIAS  	= $pslipData['acbenef_alias'];
				$ACBENEF_EMAIL  	= $pslipData['acbenef_email'];
				$ACBENEF_ADDRESS  	= $pslipData['acbenef_address'];
 				$ACBENEF_CITIZENSHIP= $pslipData['acbenef_citizenship'];
				$ACBENEF_RESIDENT	= $pslipData['acbenef_resident'];
				$BANK_NAME			= $pslipData['bank_name'];
//				$BANK_CITY			= $pslipData['bank_city'];
				$CLR_CODE			= $pslipData['clr_code'];
				$LLD_DESC			= $pslipData['lld_desc'];
				$LLD_CODE			= $pslipData['lld_code'];
				
				$LLD_CATEGORY		= $pslipData['acbenef_cat'];
				$LLD_BENEIDENTIF	= $pslipData['acbenef_id_type'];
				$LLD_BENENUMBER		= $pslipData['acbenef_id_number'];
				$CITY_CODE			= $pslipData['acbenef_city_code'];
				
				$PS_STATUS 			= $pslipData['PS_STATUS'];
				$PS_TYPE  			= $pslipData['PS_TYPE'];
				$TRANSFER_TYPE  	= $pslipData['transfer_type'];	// 1, 2

				// Payment Status is not Request Repair
				if ($PS_STATUS != $this->_paymentstatus["code"]["requestrepair"])
				{
					$this->_helper->getHelper('FlashMessenger')->addMessage("F");
					$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Payment Status has changed.");
					$this->_redirect('/paymentworkflow/requestrepair/index');
				}
				elseif ($PS_TYPE != $this->_paymenttype["code"]["domestic"])
				{
					$this->_helper->getHelper('FlashMessenger')->addMessage("F");
					$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Payment Type is invalid.");
					$this->_redirect('/paymentworkflow/requestrepair/index');
				}

				$trfTypeArr = array_combine($this->_transfertype["code"], $this->_transfertype["desc"]);	// Array([2] => SKN, [1] => RTGS)
				$TRANSFER_TYPE = $trfTypeArr[$TRANSFER_TYPE];	// SKN, RTGS

				if (!empty($LLD_CODE))
				{
					$LLD_CODEarr = Application_Helper_General::stringFormatToArray($LLD_CODE);
					$LLD_IDENTICAL 			= (!empty($LLD_CODEarr["ID"]))? $LLD_CODEarr["ID"]: "";
					$LLD_RELATIONSHIP 		= (!empty($LLD_CODEarr["TR"]))? $LLD_CODEarr["TR"]: "";
					$LLD_PURPOSE 			= (!empty($LLD_CODEarr["TP"]))? $LLD_CODEarr["TP"]: "";
					$LLD_DESCRIPTION 		= (!empty($LLD_CODEarr["TD"]))? $LLD_CODEarr["TD"]: "";
					$LLD_SENDERIDENTIF 		= (!empty($LLD_CODEarr["ST"]))? $LLD_CODEarr["ST"]: "";
					$LLD_SENDERNUMBER 		= (!empty($LLD_CODEarr["SN"]))? $LLD_CODEarr["SN"]: "";
				}
			}
			else	// ps_number is invalid, or not belong to customer
			{
				$this->_helper->getHelper('FlashMessenger')->addMessage("F");
				$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Payment Ref# is invalid.");
				$this->_redirect('/paymentworkflow/requestrepair/index');
			}
    	}

		if (!empty($PS_NUMBER) && !$this->view->hasPrivilege('PRPP'))
		{
			$this->_helper->getHelper('FlashMessenger')->addMessage("F");
			$this->_helper->getHelper('FlashMessenger')->addMessage("Error: You don't have privilege to repair payment.");
			$this->_redirect('/paymentworkflow/requestrepair/index');
		}
		
		//tambahn pentest
		$tranferdatetype = $this->_request->getParam('tranferdatetype');
		$this->view->tranferdatetype = $tranferdatetype;
		
		if (!empty($pslipData))	
		{
			if($TransferDate == '1'){
				$this->view->PS_EFDATEFUTURE = $futureDate;
			}
			elseif ($tranferdatetype =='3'){
				$PS_EFDATE = date('d/m/Y', strtotime('+1 days', strtotime(date('Y-m-d')))); 
				$this->view->PS_EFDATEFUTURE = $PS_EFDATE;
				$PS_EFDATE = $PS_EFDATE_ORII;//						
			}
			else{
				$this->view->PS_EFDATEFUTURE = $PS_EFDATE;
			}
		}
		else{
			$this->view->PS_EFDATEFUTURE = $futureDate;
		}
		
		//$TRANSFER_TYPE ='SKN';
		$sessionNameRand = new Zend_Session_Namespace('confirmTransactRand');
		if($this->_request->isPost() )
		{
			$sessionNameRand = new Zend_Session_Namespace('confirmTransactRand');
				
			if($this->_getParam('randomTransact') == $sessionNameRand->randomTransact){
				try {
					$passwordRand = $sessionNameRand->randomTransact; //yang hidden param random
					$blocksize = 256;  // can be 128, 192 or 256
					$ACBENEF =   $this->_request->getParam('ACBENEF');
					$ACCTSRC =   $this->_request->getParam('ACCTSRC');
					$TRA_AMOUNT =   $this->_request->getParam('TRA_AMOUNT');;
				} 
				catch (Exception $e) {
					$ACBENEF = '';
					$ACCTSRC = '';
					$TRA_AMOUNT = '';
				}
			
				$TRANSFER_TYPE 		= $filter->filter($this->_request->getParam('TRANSFER_TYPE')	, "SELECTION");
				$PS_SUBJECT 		= $filter->filter($this->_request->getParam('PS_SUBJECT')		, "PS_SUBJECT");
				$PS_EFDATE 			= $filter->filter($this->_request->getParam('PS_EFDATE')		, "PS_DATE");
				$TRA_AMOUNT 		= $filter->filter($TRA_AMOUNT		, "AMOUNT");
				$TRANSFER_FEE 		= $filter->filter($this->_request->getParam('TRANSFER_FEE')		, "TRANSFER_FEE");
				$TRA_MESSAGE 		= $filter->filter($this->_request->getParam('TRA_MESSAGE')		, "TRA_MESSAGE");
				$TRA_REFNO 			= $filter->filter($this->_request->getParam('TRA_REFNO')		, "TRA_REFNO");
				$ACCTSRC 			= $filter->filter($ACCTSRC			, "ACCOUNT_NO");
//				$ACBENEF 			= $filter->filter($ACBENEF			, "ACCOUNT_NO");
				
				//$ACBENEF_BANKNAME 	= $filter->filter($this->_request->getParam('ACBENEF_BANKNAME')	, "ACCOUNT_NAME");
				
				if($TRANSFER_TYPE == 'ONLINE'){
					$ACBENEF_BANKNAME 	= $filter->filter($this->_request->getParam('ACBENEF_BANKNAME_ONLINE')	, "ACCOUNT_NAME");
				}
				else{
					$ACBENEF_BANKNAME 	= $filter->filter($this->_request->getParam('ACBENEF_BANKNAME')	, "ACCOUNT_NAME");
				}
				
					
				$ACBENEF_ALIAS 		= $filter->filter($this->_request->getParam('ACBENEF_ALIAS')	, "ACCOUNT_ALIAS");
				$ACBENEF_EMAIL 		= $filter->filter($this->_request->getParam('ACBENEF_EMAIL')	, "EMAIL");
				$ACBENEF_CCY 		= $filter->filter($this->_request->getParam('CURR_CODE')		, "SELECTION");
				$ACBENEF_ADDRESS	= $filter->filter($this->_request->getParam('ACBENEF_ADDRESS')	, "ADDRESS");
	 			
				//$ACBENEF_CITIZENSHIP= $filter->filter($this->_request->getParam('ACBENEF_CITIZENSHIP'), "SELECTION");
				//$ACBENEF_RESIDENT	= $filter->filter($this->_request->getParam('ACBENEF_RESIDENT') , "SELECTION");
				//$this->view->ACBENEF_CITIZENSHIP= (isset($ACBENEF_RESIDENT))	? $ACBENEF_RESIDENT	: 'R';
				
				$ACBENEF_RESIDENT= $filter->filter($this->_request->getParam('ACBENEF_RESIDENT'), "SELECTION");
				
				$this->view->ACBENEF_RESIDENT= (isset($ACBENEF_RESIDENT))	? $ACBENEF_RESIDENT	: 'R'; 
				
				$ACBENEF_CITIZENSHIP= $filter->filter($this->_request->getParam('ACBENEF_CITIZENSHIP'), "SELECTION");
				$this->view->ACBENEF_CITIZENSHIP= (isset($ACBENEF_CITIZENSHIP))	? $ACBENEF_CITIZENSHIP	: 'W';
				
				$CITY_CODE= $filter->filter($this->_request->getParam('CITY_CODE'), "SELECTION");
				$this->view->CITY_CODE= (isset($CITY_CODE))	? $CITY_CODE	: ''; 
				
				$BANK_NAME			= $filter->filter($this->_request->getParam('BANK_NAME')		, "BANK_NAME");
	//			$BANK_CITY			= $filter->filter($this->_request->getParam('BANK_CITY')		, "ADDRESS");
				$CLR_CODE			= $filter->filter($this->_request->getParam('CLR_CODE')			, "BANK_CODE");
				$TRANSFER_TYPE 		= $filter->filter($this->_request->getParam('TRANSFER_TYPE')	, "SELECTION");
				$LLD_CATEGORY 		= $filter->filter($this->_request->getParam('LLD_CATEGORY')		, "LLD_CODE");
				$LLD_IDENTICAL 		= $filter->filter($this->_request->getParam('LLD_IDENTICAL')	, "LLD_CODE");
				$LLD_RELATIONSHIP 	= $filter->filter($this->_request->getParam('LLD_RELATIONSHIP')	, "LLD_CODE");
				$LLD_PURPOSE 		= $filter->filter($this->_request->getParam('LLD_PURPOSE')		, "LLD_CODE");
				$LLD_DESCRIPTION 	= $filter->filter($this->_request->getParam('LLD_DESCRIPTION')	, "LLD_DESC");
				$LLD_BENEIDENTIF 	= $filter->filter($this->_request->getParam('LLD_BENEIDENTIF')	, "LLD_CODE");
				$LLD_BENENUMBER 	= $filter->filter($this->_request->getParam('LLD_BENENUMBER')	, "LLD_CODE");
				$LLD_SENDERIDENTIF 	= $filter->filter($this->_request->getParam('LLD_SENDERIDENTIF'), "LLD_CODE");
				$LLD_SENDERNUMBER 	= $filter->filter($this->_request->getParam('LLD_SENDERNUMBER')	, "LLD_CODE");
				
	
				$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);
				$TRANSFER_FEE_num 	= Application_Helper_General::convertDisplayMoney($TRANSFER_FEE);
				
				$PERIODIC_EVERY 	= $filter->filter($this->_request->getParam('PERIODIC_EVERY')	, "SELECTION");
				$PERIODIC_EVERYDATE = $filter->filter($this->_request->getParam('PERIODIC_EVERYDATE')	, "SELECTION");
				$TrfDateType 		= $filter->filter($this->_request->getParam('tranferdatetype')	, "SELECTION");				
				$TrfPeriodicType 	= $filter->filter($this->_request->getParam('tranferdateperiodictype')	, "SELECTION");
				$PS_ENDDATE			= $filter->filter($this->_request->getParam('PS_ENDDATEPERIODIC')		, "PS_DATEPERIODIC");
				
				$PS_EVERY_PERIODIC_UOM 	= $TrfPeriodicType; //trim($this->_request->getParam('PS_EVERY_PERIODIC_UOM'));
				$START_DATE 			= $filter->filter(date('d/m/Y')	, "PS_DATE");
				$EXPIRY_DATE 			= $filter->filter($this->_request->getParam('PS_ENDDATEPERIODIC')	, "PS_DATE");
	
				$filter->__destruct();
				unset($filter);
	
				// post submit payment
				if ($submitBtn)
				{
//					die('hger');
					//cek periodik				
					if ($TrfDateType=='3'){
											
						// NextDate														
						if ($PS_EVERY_PERIODIC_UOM ==5){ //every day
							
							$dateNow = mktime(0,0,0,date("n"),date("j"),date("Y"));
							$every = (int)$PERIODIC_EVERY;
							$d = date("w",$dateNow);
							
							$addDay = $every;
							if ($every > $d){
								$addDay = $addDay;
							}else{
								$addDay = $addDay + 7;
							}
							$nextDate  =  (int)$addDay - (int)$d;
							$NEXT_DATE = date("Y-m-d",strtotime("+$nextDate day"));
							$PERIODIC_EVERY_VAL = $PERIODIC_EVERY;
							
						}elseif ($PS_EVERY_PERIODIC_UOM ==6){ //every date
							
							$dateNextMonth = mktime(0,0,0,date("n"),date("j")+1,date("Y"));
							
							$dateNow = date("j");
							$maxDays=date('t', $dateNextMonth);
							//echo $maxDays; die;
							$every = (int)$PERIODIC_EVERYDATE;
	
							if ($every > $dateNow){
								$addMonth = 0;
							}else{
								$addMonth = 1; 
							}
							
							if ($maxDays >=  $every){
								$every = $every;
							}else{
								$every = $maxDays;
							}
							
							$nextDate = mktime(0,0,0,date("n")+$addMonth,$every,date("Y"));
							$NEXT_DATE = date("Y-m-d",$nextDate);
							$PERIODIC_EVERY_VAL = $PERIODIC_EVERYDATE;
						}
						//echo $NEXT_DATE."<br>";
						$END_DATE = join('-',array_reverse(explode('/',$EXPIRY_DATE))); 
						$PS_EFDATE = date("d/m/Y", strtotime($NEXT_DATE)); //date_format($NEXT_DATE, 'd-m-Y');
						$PS_EFDATE_ORII = date("d/m/Y", strtotime($NEXT_DATE)); //date_format($NEXT_DATE, 'd-m-Y');
						//echo $END_DATE;
						 
										
					}else{
						$PS_EFDATE = $PS_EFDATE;
					}
	
					//cek date kosong
					if(!$PS_EFDATE){
	
						$error_msg		 		= 'Payment Date can not be left blank.';
						$this->view->error 		= true;
						$this->view->ERROR_MSG	= $error_msg;
						
					}elseif($TrfDateType=='3' && $TrfPeriodicType==5 && empty($PERIODIC_EVERY)){
						$errorMsg		 		= $this->language->_('Period can not be left blank.');
						$this->view->error 		= true;
						$this->view->ERROR_MSG	= $errorMsg;
						
						$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
						$sessionNameConfrim->benefAcct = $ACBENEF;
						$sessionNameConfrim->sourceAcct = $ACCTSRC;
						$sessionNameConfrim->traAmount = $TRA_AMOUNT_num;
					
					}elseif($TrfDateType=='3' && $TrfPeriodicType==6 && empty($PERIODIC_EVERYDATE)){
						$errorMsg		 		= $this->language->_('Period can not be left blank.');
						$this->view->error 		= true;
						$this->view->ERROR_MSG	= $errorMsg;
						
						$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
						$sessionNameConfrim->benefAcct = $ACBENEF;
						$sessionNameConfrim->sourceAcct = $ACCTSRC;
						$sessionNameConfrim->traAmount = $TRA_AMOUNT_num;
					
					}elseif($TrfDateType=='3' && strtotime($NEXT_DATE) > strtotime($END_DATE)){
						$errorMsg		 		= $this->language->_('End Date must grather than'). " ". $NEXT_DATE;
						$this->view->error 		= true;
						$this->view->ERROR_MSG	= $errorMsg;
						
						$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
						$sessionNameConfrim->benefAcct = $ACBENEF;
						$sessionNameConfrim->sourceAcct = $ACCTSRC;
						$sessionNameConfrim->traAmount = $TRA_AMOUNT_num;
	
					}
					else{
	
						$validateDateFormat = new Zend_Validate_Date(array('format' => $this->_dateDisplayFormat));
						if(!$validateDateFormat->isValid($PS_EFDATE))
						{
							$error_msg = 'Invalid Payment Date Format. Payment Date Format must be dd/MM/yyyy';
							$this->view->error 		= true;
							$this->view->ERROR_MSG	= $error_msg;
	
						}
						else{
						//new kebutuhan pentest (revisi security)
							if($isConfirmPage != 1){
								$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
								$sessionNameConfrim->benefAcct = $ACBENEF;
								$sessionNameConfrim->sourceAcct = $ACCTSRC;
								$sessionNameConfrim->traAmount = $TRA_AMOUNT_num;
							}
							else{
								$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
								$sessionNameConfrim->benefAcct;	
								$sessionNameConfrim->sourceAcct;	
								$sessionNameConfrim->traAmount;
							}
							
							$paramPayment = array(	"CATEGORY" 					=> "SINGLE DOM",
													"FROM" 						=> "F",				// F: Form, I: Import
													"PS_NUMBER"					=> $PS_NUMBER,
													"PS_SUBJECT"				=> $PS_SUBJECT,
													"PS_EFDATE"					=> $PS_EFDATE,
													"END_DATE"					=> $EXPIRY_DATE,
													"_dateFormat"				=> $this->_dateDisplayFormat,
													"_dateDBFormat"				=> $this->_dateDBFormat,
													"_addBeneficiary"			=> $this->view->hasPrivilege('BADA'),	// privi BADA (Add Beneficiary)
													"_beneLinkage"				=> $this->view->hasPrivilege('BLBU'),	// privi BLBU (Linkage Beneficiary User)
													"_createPB"					=> false,								// cannot create PB trx
													"_createDOM"				=> $this->view->hasPrivilege('CDFT'),	// privi CDFT (Create Domestic Fund Transfer)
													"_createREM"				=> false,								// cannot create REM trx
													"ISRDN"      				=> "1",
													
												 );
	
							$paramTrxArr[0] = array("TRANSFER_TYPE" 			=> $TRANSFER_TYPE,
													"TRA_AMOUNT" 				=> $sessionNameConfrim->traAmount,
													"TRA_MESSAGE" 				=> $TRA_MESSAGE,
													"TRA_REFNO" 				=> $TRA_REFNO,
													"ACCTSRC" 					=> $sessionNameConfrim->sourceAcct,
													"ACBENEF" 					=> $sessionNameConfrim->benefAcct,
													"ACBENEF_CCY" 				=> $ACBENEF_CCY,
													"ACBENEF_EMAIL" 			=> $ACBENEF_EMAIL,
	
												// for Beneficiary data, except (bene CCY and email), must be passed by reference
													"ACBENEF_BANKNAME" 			=> &$ACBENEF_BANKNAME,
													"ReffId" 					=> &$ReffId,
													"ACBENEF_ALIAS" 			=> &$ACBENEF_ALIAS,
	 												"ACBENEF_CITIZENSHIP" 		=> &$ACBENEF_RESIDENT,		// W:WNI, N: WNA
													"ACBENEF_RESIDENT" 			=> &$ACBENEF_CITIZENSHIP,			// 1/R: RESIDENT, 2/NR: NON-RESIDENT
													"ACBENEF_ADDRESS1" 			=> &$ACBENEF_ADDRESS,
												//	"ACBENEF_ADDRESS2" 			=> &$ACBENEF_ADDRESS2,
												//	"ACBENEF_ADDRESS3" 			=> &$ACBENEF_ADDRESS3,
	
												//	"ORG_DIR" 					=> $ORG_DIR,
													"BANK_CODE" 				=> $CLR_CODE,
												//	"BANK_NAME" 				=> $BANK_NAME,
												//	"BANK_BRANCH" 				=> $BANK_BRANCH,
												//	"BANK_ADDRESS1" 			=> $BANK_ADDRESS1,
												//	"BANK_ADDRESS2" 			=> $BANK_ADDRESS2,
												//	"BANK_ADDRESS3" 			=> $BANK_ADDRESS3,
	
													"LLD_IDENTICAL" 			=> $LLD_IDENTICAL,
												//	"LLD_CITIZENSHIP"			=> $LLD_CITIZENSHIP,
													"LLD_CATEGORY" 				=> $LLD_CATEGORY,
													"LLD_RELATIONSHIP" 			=> $LLD_RELATIONSHIP,
													"LLD_PURPOSE" 				=> $LLD_PURPOSE,
													"LLD_DESCRIPTION" 			=> $LLD_DESCRIPTION,
													"LLD_BENEIDENTIF" 			=> $LLD_BENEIDENTIF,
													"LLD_BENENUMBER" 			=> $LLD_BENENUMBER,
													"LLD_SENDERIDENTIF" 		=> $LLD_SENDERIDENTIF,
													"LLD_SENDERNUMBER" 			=> $LLD_SENDERNUMBER,
													"CITY_CODE" 				=> $CITY_CODE, //yang akan dikirimkan ke vlink field 3
													"KSIE"						=> "1",
												 );
	
	
							if($isConfirmPage != 1){
								$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
								$sessionNameConfrim->ACBENEF_BANKNAME = $paramTrxArr[0]['ACBENEF_BANKNAME'];
							}
							else{
								$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
								$sessionNameConfrim->ACBENEF_BANKNAME;	
							}
							
							//$userDailyLimit = $CustomerUser->getUserDailyLimit();
	
							//Zend_Registry::set('USER_DAILY_LIMIT', $userDailyLimit);
							//Zend_Registry::set('MAKER_LIMIT' 	 , array());
							//Zend_Registry::set('TOTAMT_TODAYCCY' , array());
							
							if($TRANSFER_TYPE == 'ONLINE'){
								//ambil data m_user ->ambil sourceAccount, acctType, statusnya aktif=1
								if($ACCTSRC != ''){
									$acctStatus = 1;
									$dataAcct = $this->_db->fetchRow(
										$this->_db->select()
										->from(array('C' => 'M_CUSTOMER_ACCT'))
										->where("CUST_ID = ?",$this->_custIdLogin)
										->where("ACCT_STATUS = ?",$acctStatus)
										->where("ACCT_NO = ?",$ACCTSRC)
										->limit(1)
										);
		//							$ACCT_NO = (!empty($dataAcct['ACCT_NO'])?$dataAcct['ACCT_NO']:'');
									$ACCT_TYPE = (!empty($dataAcct['ACCT_TYPE'])?$dataAcct['ACCT_TYPE']:'');
											
									//////////////////
									$paramTrxArr[0]["sourceAccountType"] 	 = $ACCT_TYPE;
								}
							}
									
							$resWs = array();
							$validate   = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin);
							$validate->setFlagConfirmPage(($isConfirmPage == 1)?TRUE:FALSE); //\tujuan untuk set supaya jangan manggil inquiry lg di page ke 2
							//echo '<pre>';
							//var_dump($paramPayment);
							//var_dump($paramTrxArr);die;
							$resultVal	= $validate->checkCreate($paramPayment, $paramTrxArr, $resWs);
							$sourceAccountType = $resWs['accountType'];
							$infoWarning = $resWs['infoHoliday']['infoWarning']; //permintaan mayapada hari libur dikasih warning saja
							$reffId = $paramTrxArr[0]['ReffId'];
							if($isConfirmPage != 1){
								$this->view->reffIdSend = $reffId;
								$this->view->sourceAcctTypeGet = $sourceAccountType;
							}
							else{
								$reffIdGet 		= $this->_getParam('reffId');
								$this->view->reffIdSend = $reffIdGet;
								
								$sourceAcctTypeGet 		= $this->_getParam('sourceAcctType');
								$this->view->sourceAcctTypeGet = $sourceAcctTypeGet;
							}
	
							if($validate->isError() === false)	// payment data is valid
							{
								$payment 		= $validate->getPaymentInfo();
	
								if ($USE_CONFIRM_PAGE == true && $isConfirmPage == false)
								{
									$isConfirmPage 	= true;
									$ACCTSRC_CCY   	= $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["CCY_ID"];
									$ACCTSRC_NAME   = $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["ACCT_NAME"];
									//$ACCTSRC_ALIAS  = $payment["acctsrcArr"][$ACCTSRC]["ACCT_ALIAS"];
									$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
									//$AccArr 	  = $CustomerUser->getAccounts();
									$AccArr 	  = $CustomerUser->getAccounts(array("ISRDN_TRX" => "1"));
									
									foreach($AccArr as $val){
										if($val['ACCT_NO']==$sessionNameConfrim->sourceAcct){
											$ACCTSRC_ALIAS = $val['ACCT_ALIAS_NAME'];
											$ACCTSRC_NAME  = $val['ACCT_NAME'];
											$ACCTSRC_TYPE  = $val['ACCT_TYPE'];
										}
									}
									
									$ACCTSRC_TYPE   = $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["ACCT_TYPE"];									
									$ACCTSRC_view 	= Application_Helper_General::viewAccount($sessionNameConfrim->sourceAcct, $ACCTSRC_CCY, $ACCTSRC_NAME, $ACCTSRC_ALIAS, $ACCTSRC_TYPE);

	
									$validate->__destruct();
									unset($validate);
	
									require_once 'General/Charges.php';
									$trfType		= $TRANSFER_TYPE;
									$chargesObj 	= Charges::factory($this->_custIdLogin, $trfType);
									$paramCharges 	= array("accsrc" => $sessionNameConfrim->sourceAcct, "transferType" => $trfType);
									$chargesAMT 	= $chargesObj->getCharges($paramCharges);
									$chargesCCY 	= $ACCTSRC_CCY;
								}
								else
								{
									$validate->__destruct();
									unset($validate);
	
									$ACCTSRC_CCY   	= $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["CCY_ID"];
									if($TRANSFER_FEE_num == ''){
										$TRANSFER_FEE_num = 0;
									}
									$param = array();
									$param['PS_SUBJECT'] 					= $PS_SUBJECT;
									$param['PS_CCY'] 						= $ACCTSRC_CCY;
									$param['PS_EFDATE'] 					= Application_Helper_General::convertDate($PS_EFDATE, $this->_dateDBFormat, $this->_dateDisplayFormat);
									$param['TRA_AMOUNT'] 					= $sessionNameConfrim->traAmount;
									$param['TRANSFER_FEE'] 					= $TRANSFER_FEE_num;
									$param['TRA_MESSAGE'] 					= $TRA_MESSAGE;
									$param['TRA_REFNO'] 					= $TRA_REFNO;
									$param['SOURCE_ACCOUNT'] 				= $sessionNameConfrim->sourceAcct;
									$param['BENEFICIARY_ACCOUNT'] 			= $sessionNameConfrim->benefAcct;
									$param['BENEFICIARY_ACCOUNT_CCY'] 		= $ACBENEF_CCY;
									$param['BENEFICIARY_ACCOUNT_NAME'] 		= $ACBENEF_BANKNAME;
									$param['BENEFICIARY_ALIAS_NAME'] 		= $ACBENEF_ALIAS;
									$param['BENEFICIARY_BANK_NAME'] 		= $BANK_NAME;
									$param['BENEFICIARY_EMAIL'] 			= $ACBENEF_EMAIL;
									$param['BENEFICIARY_ADDRESS'] 			= $ACBENEF_ADDRESS;
									$param['BENEFICIARY_CITIZENSHIP'] 		= $ACBENEF_CITIZENSHIP;
									$param['BENEFICIARY_RESIDENT'] 			= $ACBENEF_RESIDENT;
									$param['CLR_CODE']		 				= $CLR_CODE;
									$param['TRANSFER_TYPE']		 			= $TRANSFER_TYPE;
									$param['LLD_CATEGORY']		 			= $LLD_CATEGORY;
									$param['LLD_IDENTICAL']		 			= $LLD_IDENTICAL;
									$param['LLD_RELATIONSHIP']		 		= $LLD_RELATIONSHIP;
									$param['LLD_PURPOSE']		 			= $LLD_PURPOSE;
									$param['LLD_DESCRIPTION']		 		= $LLD_DESCRIPTION;
									$param['LLD_BENEIDENTIF']		 		= $LLD_BENEIDENTIF;
									$param['LLD_BENENUMBER']		 		= $LLD_BENENUMBER;
									$param['LLD_SENDERIDENTIF']		 		= $LLD_SENDERIDENTIF;
									$param['LLD_SENDERNUMBER']		 		= $LLD_SENDERNUMBER ;
									$param['CITY_CODE']		 				= $CITY_CODE;
									$param['_addBeneficiary'] 				= $paramPayment["_addBeneficiary"];
									$param['_beneLinkage'] 					= $paramPayment["_beneLinkage"];
									$param['_priviCreate'] 					= 'CDFT';
									$param['sourceAccountType'] 			= $sourceAcctTypeGet;
									$param['ReffId'] 						= $reffIdGet;
									$param['ISRDN'] 						= $paramPayment["ISRDN"];
									
									
									try
									{
										$SinglePayment = new SinglePayment($PS_NUMBER, $this->_custIdLogin, $this->_userIdLogin);
										if (!empty($PS_NUMBER))
										{	$SinglePayment->isRepair = true;	}
										
										//ada by Hamdan for Periodic
										if( $TrfDateType == 3 ){
																													
											// Insert T_PERIODIC
	//										$param['PS_PERIODIC_TRANSFER'] = 1;
											$START_DATE = join('-',array_reverse(explode('/',$START_DATE)));
											$EXPIRY_DATE = join('-',array_reverse(explode('/',$EXPIRY_DATE)));
												
											$insertPeriodic = array(
													'PS_EVERY_PERIODIC' 	=> (int)$PERIODIC_EVERY_VAL,
													'PS_PERIODIC_NUMBER'	=> $this->generateTransactionID(),
													'PS_EVERY_PERIODIC_UOM' => $PS_EVERY_PERIODIC_UOM, // 1: daily, 2: weekly, 3: monthly, 4: yearly, 5: every day of, 6: every date of
													'PS_PERIODIC_STARTDATE' => $START_DATE,
													'PS_PERIODIC_ENDDATE'	=> $EXPIRY_DATE,
													'PS_PERIODIC_NEXTDATE'	=> $NEXT_DATE,
													'PS_PERIODIC_STATUS' 	=> 2,	// 2 INPROGRESS KALO BELUM BERAKHIR, 1 COMPLETE KALO SUDAH HABIS END DATE, 0 PERIODIC INI DI CANCEL
													'USER_NAME' 				=> $this->_userIdLogin,
													'PS_CREATED' 			=> new Zend_Db_Expr("GETDATE()"),
												);
											//Zend_Debug::dump($insertPeriodic); die;
											$this->_db->insert('T_PERIODIC', $insertPeriodic);
											$psPeriodicID =  $this->_db->lastInsertId();
											$param['PS_PERIODIC'] = $psPeriodicID;
													
													
											// Insert T_PERIODIC_DETAIL
											$select	= $this->_db->select()
												->from(array('A'	 		=> 'M_CUSTOMER_ACCT'),
													array(
												   		'ACCT_NAME' 	=> 'A.ACCT_NAME',
												   		'ACCT_ALIAS' 	=> 'A.ACCT_ALIAS_NAME',
												   		'ACCT_CCY' 	=> 'A.CCY_ID',
													)
												)
												->where("A.CUST_ID 			 = ?", $this->_custIdLogin)
												->where("A.ACCT_NO		 	 = ?", $param['SOURCE_ACCOUNT']);
											
											$accsrc = $this->_db->fetchRow($select);
											
											$sourceAccountName 		=  $accsrc['ACCT_NAME'];
											$sourceAccountAliasName =  $accsrc['ACCT_ALIAS'];
											$sourceAccountCCY 		=  $accsrc['ACCT_CCY'];
													
											//Zend_Debug::dump($param); die;
											$insertPeriodicDetail = array(
														'PS_PERIODIC' 				=> $psPeriodicID,
														'SOURCE_ACCOUNT' 			=> $param['SOURCE_ACCOUNT'], // 1: daily, 2: weekly, 3: monthly, 4: yearly, 5: every day of, 6: every date of
														'SOURCE_ACCOUNT_CCY' 		=> $sourceAccountCCY,
														'SOURCE_ACCOUNT_NAME' 		=> $sourceAccountName,
														'SOURCE_ACCOUNT_TYPE' 		=> $param['sourceAccountType'],
														'SOURCE_ACCOUNT_BANK_CODE' 	=> "",
														'SOURCE_ACCOUNT_BANK_NAME' 	=> "",
														'BENEFICIARY_ACCOUNT' 		=> $param['BENEFICIARY_ACCOUNT'],
														'BENEFICIARY_ACCOUNT_CCY' 	=> $param['BENEFICIARY_ACCOUNT_CCY'],
														'BENEFICIARY_ACCOUNT_NAME' 	=> $param['BENEFICIARY_ACCOUNT_NAME'],
														'BENEFICIARY_EMAIL' 		=> $param['BENEFICIARY_EMAIL'],
														'BENEFICIARY_BANK_CODE' 	=> "",
														'BENEFICIARY_BANK_NAME' 	=> "",
														'TRA_AMOUNT' 				=> $param['TRA_AMOUNT'],
														'TRA_MESSAGE' 				=> $param['TRA_MESSAGE'],														
														'TRANSFER_TYPE' 			=> 0,	// 0 : Inhouse, 1: RTGS, 2: SKN														
													);
												$this->_db->insert('T_PERIODIC_DETAIL', $insertPeriodicDetail);
										}
										// End
										
										$result = $SinglePayment->createPaymentDomestic($param);
	
										$ns = new Zend_Session_Namespace('FVC');
										//$ns->backURL = '/'.$this->view->modulename.'/'.$this->view->controllername.'/index';
	
										if (!empty($PS_NUMBER))
											$ns->backURL = '/paymentworkflow/requestrepair/index/m/1';
										else
											$ns->backURL = '/'.$this->view->modulename.'/'.$this->view->controllername.'/index';
	
	
										if ($result === true)
										{	$this->_redirect('/notification/success/index');	}
										else
										{	$this->_redirect('/notification/success/index');	}	// TODO: what to do, if failed create payment
									}
									catch(Exception $e)
									{
										//print_r($e);die;
										Application_Helper_General::exceptionLog($e);
									}
								}
							}
							else
							{
								$errorMsg 		= $validate->getErrorMsg();
								$errorTrxMsg 	= $validate->getErrorTrxMsg();	// array
								var_dump($errorTrxMsg);
								var_dump($errorMsg);die;
	
								$validate->__destruct();
								unset($validate);
	
								$errMessage 	= (!empty($errorMsg))? $errorMsg: reset(reset(reset($errorTrxMsg)));
	
								$this->view->error 		= true;
								$this->view->ERROR_MSG	= $errMessage;
							}
						}
					}
				}
				else{
					//tambahn pentest
					$TRA_AMOUNT  		= $this->_getParam('TRA_AMOUNT');
					
					$PS_EFDATE_ORI_now  		= $this->_getParam('PS_EFDATE');
					if($PS_EFDATE_ORI_now > date('d/m/Y')){
						$TransferDate 	=  "2";
					}else{
						$TransferDate	=  "1";
//						$PS_EFDATE = date('d/m/Y', strtotime('+1 days', strtotime(date('Y-m-d')))); 
						$PS_EFDATE = date('d/m/Y'); 
					}
					
					$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
					$isConfirmPage 	= false;
				}
			}
			else{
				$this->view->sessionExpired = $this->language->_('Session expired');
				$this->_redirect('/rdntrans/rtgsksei');
			}
			$randomTransact = str_pad(mt_rand(0, 99999999), 8, '0', STR_PAD_LEFT);
			$this->view->randomTransact = $randomTransact;	
			$sessionNameRand->randomTransact = $randomTransact;	
			
			//tambahn pentest
			if($tranferdatetype =='1'){
				$this->view->PS_EFDATE 			= $PS_EFDATE;
			}
			elseif ($tranferdatetype =='3'){
				$PS_EFDATE = date('d/m/Y', strtotime('+1 days', strtotime(date('Y-m-d')))); 
				$this->view->PS_EFDATEFUTURE = $PS_EFDATE;
				$PS_EFDATE = $PS_EFDATE_ORII;//						
			}
			else{
				$this->view->PS_EFDATEFUTURE = $PS_EFDATE;						
			}

		}
		else
		{
			$randomTransact = str_pad(mt_rand(0, 99999999), 8, '0', STR_PAD_LEFT);
			$this->view->randomTransact = $randomTransact;	
			$sessionNameRand->randomTransact = $randomTransact;
		}
		
		$this->view->infoWarning = $infoWarning;

		$TRA_MESSAGE_len = (isset($TRA_MESSAGE))? strlen($TRA_MESSAGE): 0;
		$TRA_REFNO_len 	 = (isset($TRA_REFNO))  ? strlen($TRA_REFNO)  : 0;

		$TRA_MESSAGE_len = 120 - $TRA_MESSAGE_len;
		$TRA_REFNO_len 	 = 200 - $TRA_REFNO_len;

//		$this->view->COT_SKN			= Application_Helper_General::getRegistrySetting("cut_off_time_skn"	, "00:00:00");
//		$this->view->COT_RTGS			= Application_Helper_General::getRegistrySetting("cut_off_time_rtgs", "00:00:00");
//		$this->view->THRESHOLD_LLD		= Application_Helper_General::getRegistrySetting("threshold_lld"	, 0);

		$settingObj = new Settings();
//		$this->view->COT_SKN			= $settingObj->getSetting("cut_off_time_skn"	, "00:00:00");
		$this->view->COT_KSE			= $settingObj->getSetting("cut_off_time_kse", "00:00:00");
		$this->view->COT_BI				= $settingObj->getSetting("cut_off_time_bi", "00:00:00");
		$this->view->THRESHOLD_LLD		= $settingObj->getSetting("threshold_lld"	, 0);

		$this->view->AccArr 			= $AccArr;
		$this->view->transferTypeArr 	= array(//$this->_transfertype["desc"]["SKN"]  => $this->_transfertype["desc"]["SKN"],
												$this->_transfertype["desc"]["RTGS"] => $this->_transfertype["desc"]["RTGS"],
												);
 		$this->view->citizenshipArr 	= $citizenshipArr;
		$this->view->residentArr 		= $residentArr;
		$this->view->lldCategoryArr 	= $lldCategoryArr;
		$this->view->lldIdenticalArr 	= $lldIdenticalArr;
		$this->view->lldRelationshipArr = $lldRelationshipArr;
		$this->view->lldPurposeArr 		= $lldPurposeArr;
		$this->view->lldBeneIdentifArr  = $lldBeneIdentifArr;
		$this->view->lldSenderIdentifArr  = $lldSenderIdentifArr;

		$this->view->PS_NUMBER 			= (isset($PS_NUMBER))			? $PS_NUMBER			: '';
		$this->view->PS_SUBJECT 		= (isset($PS_SUBJECT))			? $PS_SUBJECT			: '';
		$this->view->PS_EFDATE 			= $PS_EFDATE;

		$this->view->TransferDate		= (isset($TransferDate))		? $TransferDate			: '1';
		
		// $this->view->PS_EFDATE 			= (isset($PS_EFDATE))			? $PS_EFDATE			: $this->getCurrentDate();
		//$this->view->TRA_AMOUNT 		= (isset($TRA_AMOUNT))			? $TRA_AMOUNT			: '';
		//$this->view->TRA_AMOUNT 		= $this->_request->getParam('TRA_AMOUNT');

		if($PS_NUMBER){
			$this->view->TRA_AMOUNT 		= (isset($TRA_AMOUNT))			? $TRA_AMOUNT			: '';
		}
		else{
//			$this->view->TRA_AMOUNT 		= $this->_request->getParam('TRA_AMOUNT');
			$traamount = Application_Helper_General::displayMoney($sessionNameConfrim->traAmount);
			$this->view->TRA_AMOUNT 		= (isset($traamount))			? $traamount: '';
		}

		$this->view->TRA_MESSAGE 		= (isset($TRA_MESSAGE))			? $TRA_MESSAGE			: '';
		$this->view->TRA_REFNO 			= (isset($TRA_REFNO))			? $TRA_REFNO			: '';
		$this->view->TRA_MESSAGE_len	= $TRA_MESSAGE_len;
		$this->view->TRA_REFNO_len		= $TRA_REFNO_len;

		$this->view->ACCTSRC_view		= $ACCTSRC_view;
		//$this->view->ACBENEF_BANKNAME	= (isset($ACBENEF_BANKNAME))	? $ACBENEF_BANKNAME		: '';
		
		if (!empty($pslipData))	
		{
			$this->view->ACCTSRC 			= (isset($ACCTSRC))				? $ACCTSRC				: '';
			$this->view->ACBENEF 			= (isset($ACBENEF))				? $ACBENEF				: '';
		}
		else{
			$this->view->ACCTSRC 			= (isset($sessionNameConfrim->sourceAcct))				? $sessionNameConfrim->sourceAcct				: '';
			$this->view->ACBENEF 			= (isset($sessionNameConfrim->benefAcct))				? $sessionNameConfrim->benefAcct				: '';
		}
		
		
		if($TRANSFER_TYPE == 'ONLINE'){
			if (!empty($pslipData))	
			{
				$this->view->ACBENEF_BANKNAME_ONLINE	= (isset($ACBENEF_BANKNAME))	? $ACBENEF_BANKNAME		: '';
			}
			else{
				$this->view->ACBENEF_BANKNAME_ONLINE	= (isset($sessionNameConfrim->ACBENEF_BANKNAME))	? $sessionNameConfrim->ACBENEF_BANKNAME		: '';
			}
				$this->view->trType	= 'ONLINE';
		}
		else{
			if (!empty($pslipData))	
			{
				$this->view->ACBENEF_BANKNAME	= (isset($ACBENEF_BANKNAME))	? $ACBENEF_BANKNAME		: '';
			}
			else{
//				$this->view->ACBENEF_BANKNAME	= (isset($sessionNameConfrim->ACBENEF_BANKNAME))	? $sessionNameConfrim->ACBENEF_BANKNAME		: '';
				
				if($tranferdatetype == 3){
					$this->view->ACBENEF_BANKNAME	= $this->_getParam('ACBENEF_BANKNAME');
				}
				else{
					$this->view->ACBENEF_BANKNAME	= (isset($sessionNameConfrim->ACBENEF_BANKNAME))	? $sessionNameConfrim->ACBENEF_BANKNAME		: '';
				}
			}
				$this->view->trType	= 'OTHER';
		}
		
		$this->view->ACBENEF_ALIAS 		= (isset($ACBENEF_ALIAS))		? $ACBENEF_ALIAS		: '';
		$this->view->ACBENEF_EMAIL 		= (isset($ACBENEF_EMAIL))		? $ACBENEF_EMAIL		: '';
		$this->view->CURR_CODE 			= (isset($ACBENEF_CCY))			? $ACBENEF_CCY			: '';
		$this->view->ACBENEF_ADDRESS 	= (isset($ACBENEF_ADDRESS))		? $ACBENEF_ADDRESS		: '';
 		$this->view->ACBENEF_CITIZENSHIP= (isset($ACBENEF_CITIZENSHIP))	? $ACBENEF_CITIZENSHIP	: '';
 		$this->view->CITY_CODE			= (isset($CITY_CODE))			? $CITY_CODE	: ''; 
			//print_r($ACBENEF_RESIDENT);die;
		if($ACBENEF_RESIDENT=='W'){
			$ACBENEF_RESIDENT = 'R';
		}else{
			$ACBENEF_RESIDENT = 'NR';
		}
		$this->view->ACBENEF_RESIDENT	= (isset($ACBENEF_RESIDENT))	? $ACBENEF_RESIDENT		: 'R';
		//print_r($ACBENEF_CITIZENSHIP);
		//print_r($ACBENEF_RESIDENT);die;
		$this->view->BANK_NAME			= (isset($BANK_NAME))			? $BANK_NAME			: '';
//		$this->view->BANK_CITY			= (isset($BANK_CITY))			? $BANK_CITY			: '';

		$this->view->TRANSFER_TYPE 		= (isset($TRANSFER_TYPE))		? $TRANSFER_TYPE		: "RTGS";
		$this->view->CLR_CODE 			= (isset($CLR_CODE))			? $CLR_CODE				: "";   // "0140601";  // 0000000
		$this->view->LLD_CATEGORY 		= (isset($LLD_CATEGORY))		? $LLD_CATEGORY			: "";
		$this->view->LLD_IDENTICAL 		= (isset($LLD_IDENTICAL))		? $LLD_IDENTICAL		: "";
		$this->view->LLD_RELATIONSHIP 	= (isset($LLD_RELATIONSHIP))	? $LLD_RELATIONSHIP		: "";
		$this->view->LLD_PURPOSE 		= (isset($LLD_PURPOSE))			? $LLD_PURPOSE			: "";
		$this->view->LLD_DESCRIPTION 	= (isset($LLD_DESCRIPTION))		? $LLD_DESCRIPTION		: "";
		$this->view->LLD_BENEIDENTIF 	= (isset($LLD_BENEIDENTIF))		? $LLD_BENEIDENTIF		: "";
		$this->view->LLD_BENENUMBER 	= (isset($LLD_BENENUMBER))		? $LLD_BENENUMBER		: "";
		$this->view->LLD_SENDERIDENTIF 	= (isset($LLD_SENDERIDENTIF))	? $LLD_SENDERIDENTIF	: "";
		$this->view->LLD_SENDERNUMBER 	= (isset($LLD_SENDERNUMBER))	? $LLD_SENDERNUMBER		: "";
		$this->view->CHARGES_AMT 		= (isset($chargesAMT))			? Application_Helper_General::displayMoney($chargesAMT)	: '';
		$this->view->CHARGES_CCY 		= (isset($chargesCCY))			? $chargesCCY			: '';

		$this->view->confirmPage		= $isConfirmPage;
		//var_dump($isConfirmPage);
		//var_dump($USE_CONFIRM_PAGE);
		$this->view->useConfirmPage		= $USE_CONFIRM_PAGE;
		
		$this->view->PERIODIC_EVERY 	= (isset($PERIODIC_EVERY))		? $PERIODIC_EVERY		: '0';
		$this->view->PERIODIC_EVERYDATE = (isset($PERIODIC_EVERYDATE))	? $PERIODIC_EVERYDATE	: '0';
		//print_r($TrfDateType);die;
		if($TrfDateType=='IMEDIATE'){$TrfDateType = '1';}
		$this->view->TrfDateType 		= (isset($TrfDateType))			? $TrfDateType			: '1';
		$this->view->TrfPeriodicType 	= (isset($TrfPeriodicType))		? $TrfPeriodicType		: ''; //$TrfPeriodicType;
		
		$this->view->periodicEveryArr  	= $periodicEveryArr;
		$this->view->periodicEveryDateArr  = $periodicEveryDateArr;

		if ($isConfirmPage == '1')
		{
		}
		else{
			Application_Helper_General::writeLog('CDFT','Viewing Create Single Payment Domestic');
		}
	}

	public function generateTransactionID(){

		$currentDate = date("Ymd");
		$seqNumber	 = strtoupper(Application_Helper_General::str_rand(5));
		$trxId = 'PR'.$currentDate.$this->_custIdLogin.$seqNumber;

		return $trxId;
	}
}
