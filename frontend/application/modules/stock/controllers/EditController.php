<?php

require_once 'Zend/Controller/Action.php';

class stock_EditController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti
	
	
    public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
	    //pengaturan url untuk button back
	    $this->setbackURL('/'.$this->_request->getModuleName().'/index'); 
	
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
		
    	
    	$this->view->report_msg = array();

    	
		if(!$this->_request->isPost())
		{
			$stock_code = $this->_getParam('stock_code');
			$cust = $this->_custIdLogin;
			
			if($stock_code)
			{
			  $resultdata = $this->_db->fetchRow(
								$this->_db->select()
									 ->from(array('M_STOCK')) 
									 ->where("STOCK_CODE =?", $stock_code)
									 ->where("CUST_ID =?", $cust)
							                   );	
			  if($resultdata)
			  {
			  	// print_r($resultdata);die;
			        $this->view->stock_code      = $resultdata['STOCK_CODE'];
					$this->view->stock_name      = $resultdata['STOCK_NAME'];
					
			  } 
			}
			else
			{
			   $error_remark = 'Stock Code not found';
			
			   $this->_helper->getHelper('FlashMessenger')->addMessage('F');
			   $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
			   //$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName());
			   $this->_redirect('/'.$this->_request->getModuleName().'/index');	
			}
		}
		else
		{
			$filters = array(
							 'stock_code' => array('StringTrim','StripTags','StringToUpper'),
							 'stock_name' => array('StringTrim','StripTags','StringToUpper')
							 
							);

			$validators = array(
								'stock_code' => array('NotEmpty',
			                                         new Zend_Validate_StringLength(array('max'=>12)),
													 'messages' => array(
																         $this->language->_('Can not be empty'),
																         $this->language->_('Data too long (max 12 chars)'),
			                                                             )
													),
								'stock_name'      => array('NotEmpty',
													 new Zend_Validate_StringLength(array('max'=>105)),
													 'messages' => array(
																		$this->language->_('Can not be empty'),
																         $this->language->_('Data too long (max 105 chars)'),
																         )
														)
							   );
		
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			// die;
			if($zf_filter_input->isValid())
			{  
				$content = array(
								'STOCK_NAME' 	 => $zf_filter_input->stock_name
						       );

				try 
				{
				    //-----insert--------------
					$this->_db->beginTransaction();

					$whereArr  = array('STOCK_CODE = ?'=>$zf_filter_input->stock_code,
						'CUST_ID = ?'=>$this->_custIdLogin


				);
					$this->_db->update('M_STOCK',$content,$whereArr);
					// print_r($content);
					// print_r($whereArr);die;
					$this->_db->commit();

					foreach(array_keys($filters) as $field)
						$this->view->$field = $zf_filter_input->getEscaped($field);

					Application_Helper_General::writeLog('RBUD','Edit Remittance Bank. Bank Code : ['.$zf_filter_input->bank_code.']');
					$this->view->success = true;
					$this->view->report_msg = array();
					
					$this->_redirect('/notification/success/index');
				}
				catch(Exception $e) 
				{
					//rollback changes
					$this->_db->rollBack();

				    foreach(array_keys($filters) as $field)
						$this->view->$field = $zf_filter_input->getEscaped($field);
					$errorMsg = 'exception';
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
				}
			}
			else
			{  
				$this->view->error = true;

				foreach(array_keys($filters) as $field)
						$this->view->$field = ($zf_filter_input->isValid($field))? $zf_filter_input->getEscaped($field) : $this->_getParam($field);

				$this->view->countryArr = $selectcountry;

				$error = $zf_filter_input->getMessages();

				//format error utk ditampilkan di view html 
                $errorArray = null;
		        foreach($error as $keyRoot => $rowError)
		        {
		           foreach($rowError as $errorString)
		           {
		              $errorArray[$keyRoot] = $errorString;
		           }
		        }
        
		        $this->view->succes = false;
                $this->view->report_msg = $errorArray;
			}
		}
	}

}