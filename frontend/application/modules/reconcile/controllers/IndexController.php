<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/SystemBalance.php';
require_once 'Crypt/AESMYSQL.php';

require_once 'General/Settings.php';
require_once 'SGO/Extendedmodule/PHPExcel.php';
require_once 'SGO/Extendedmodule/PHPExcel/IOFactory.php';

class reconcile_IndexController extends Application_Main
{

	protected $_destinationUploadDir = '';
    protected $_maxRow = '';

	public function initController(){

        if (!$this->view->hasPrivilege('RRDL') && !$this->view->hasPrivilege('RRUL')) {
            $this->_redirect('/authorizationacl/index/index');
        }

		$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';

        $setting = new Settings();
        $this->_maxRow = $setting->getSetting('max_import_bulk');
	}

	public function indexAction()
	{
		$setting = new Settings();			  	
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
		$pw_hash = md5($enc_salt.$enc_pass);
		$rand = $this->_userIdLogin.date('dHis').$pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;
		$password = $sessionNamespace->token; 

		$this->_helper->layout()->setLayout('newlayout');   

        $this->view->compCode = $this->_custIdLogin;

        // $selectBank = $this->_db->select()
        //              ->distinct()
        //              ->from(array('R' => 'M_RECON_ADAPTER'), 'R.BANK_CODE')
        //              ->join(array('B' => 'M_BANK_TABLE'), 'R.BANK_CODE = B.BANK_CODE', 'B.BANK_NAME');

        // $dataBank = $this->_db->fetchAll($selectBank);

        // foreach ($dataBank as $key => $value) {
        //     $opt .= '<option value="'.$value['BANK_CODE'].'">'.$value['BANK_NAME'].'</option>'; 
        // }

        // $this->view->bankOpt = $opt;

        $selectAdapter = $this->_db->select()
                     ->distinct()
                     ->from(array('R' => 'M_RECON_ADAPTER'), 'R.ADAPTER_NAME');

        $adapterData = $this->_db->fetchAll($selectAdapter);

        foreach ($adapterData as $key => $value) {
            $opt .= '<option value="'.$value['ADAPTER_NAME'].'">'.$value['ADAPTER_NAME'].'</option>'; 
        }

        $this->view->adapterOpt = $opt;

        $select2    = $this->_db->select()
            ->from( array('R' => 'TEMP_RECON'),array('*'))
            ->order("UPLOADED_DATE DESC")
            ->where( "UPLOADED_BY = ?", $this->_custIdLogin.$this->_userIdLogin);

        $this->paging($select2);

		if($this->_request->isPost() )
        {
        	$param = $this->_request->getParams();

            $delete     = $this->_getParam('delete');

            if ($delete) {

                $reconid = $this->_request->getParam('recon_id');

                $validators = array (
                                        'reconid' => array   (
                                                                'NotEmpty',
                                                                'messages' => array (
                                                                                        'Error File ID Submitted',
                                                                                    )
                                                            ),
                                    );

                $filtersVal = array ( 'reconid' => array('StringTrim','StripTags'));

                $zf_filter_input = new Zend_Filter_Input($filtersVal,$validators,$params,$this->_optionsValidator);
                    $success = false;
                foreach ($reconid as $key => $value) {
                    if(!empty($value)){
                        $success = true;
                    }
                }


                if($zf_filter_input->isValid() && $success)
                {

                    try
                    {
                        foreach ($reconid as $key => $value)
                        {

                            $id = $value;

                            //unlink files
                            $select = $this->_db->select()
                                        ->from(array('H' => 'TEMP_RECON'),array('*'))
                                        ->join(array('R' => 'M_RECON_ADAPTER'), 'R.ADAPTER_NAME = H.ADAPTER_NAME', array('ADAPTERNAME' => 'R.ADAPTER_NAME'));

                            $select->where('H.ID = '. $this->_db->quote($id));

                            $reconData = $this->_db->fetchAll($select);

                            $statement_file_path = $reconData[0]['STATEMENT_FILE'];
                            $report_file_path = $reconData[0]['REPORT_FILE'];
                            @unlink($statement_file_path);
                            @unlink($report_file_path);

                            $this->_db->beginTransaction();

                            $where['ID = ?'] = $id;
                            $result =  $this->_db->delete('TEMP_RECON',$where);

                            Application_Helper_General::writeLog('RRUL','Delete Reconciliation Report. Adapter Profile: '.$reconData[0]['ADAPTERNAME'].', Reconcile File Name: '.$reconData[0]['ADAPTER_NAME']);

                            $this->_db->commit();
                        }
                    }
                    catch(Exception $e)
                    {
                        $this->_db->rollBack();
                    }
                    $this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
                    $this->_redirect('/notification/success/index');
                }
                else
                {
                    $error          = true;
                    $errors         = $zf_filter_input->getMessages();
                    $profileiderr   = (isset($errors['profileiderr']))? $errors['profileiderr'] : null;
                }
            }
            else{
                $adapter = new Zend_File_Transfer_Adapter_Http();

                $adapter->setDestination ( $this->_destinationUploadDir );
                $extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv'));
                $extensionValidator->setMessage(
                    $this->language->_('Error').': '.$this->language->_('Extension file must be').' *.csv'
                );

                $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
                $sizeValidator->setMessage(
                    'Error: File exceeds maximum size'
                );

                $adapter->setValidators ( array (
                    $extensionValidator,
                    $sizeValidator,
                ));

                $files = $adapter->getFileName();

                $fileNames = $adapter->getFileInfo();

                foreach ($files as $key => $value) {

                    $newFileName = $value . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

                    $adapter->addFilter ( 'Rename',$newFileName  );

                    if ($adapter->isValid ($key)){

                        if ($adapter->receive($key)) {

                            $filePath[] = $newFileName;

                            //PARSING CSV HERE
                            $data[] = $this->_helper->parser->parseCSV($newFileName);     
                        }

                    }
                    else
                    {   
                        $this->view->error = true;
                        foreach($adapter->getMessages() as $key=>$val)
                        {
                            if($key=='fileUploadErrorNoFile')
                                $error_msg[0] = $this->language->_('Error').': '.$this->language->_('File cannot be left blank. Please correct it').'.';
                            else
                                $error_msg[0] = $val;
                            break;
                        }
                        $errors = $this->displayError($error_msg);
                        $this->view->report_msg = $errors;
                    }   
                }

                if (count($files) < 2) {
                    $this->view->error = true;
                    $error_msg[] = 'Please upload both file';
                    $errors = $this->displayError($error_msg);
                    $this->view->report_msg = $errors;
                }

                if(empty($errors)){
                   
                    // $bankCode = $param['bank'];
                    $adapterName = $param['adapterName'];
                    $reconName = $param['reconname'];

                    //insert to TEMP_RECON db
                    $insertArr = array(
                        'ADAPTER_NAME' => $adapterName,
                        'RECON_NAME' => $reconName,
                        'STATEMENT_FILE' => $filePath[0],
                        'STATEMENT_FILE_NAME' => $fileNames['statement']['name'],
                        'REPORT_FILE'   => $filePath[1],
                        'REPORT_FILE_NAME' => $fileNames['vareport']['name'],
                        'STATEMENT_FILE_DATA' => json_encode($data[0]),
                        'REPORT_FILE_DATA'   => json_encode($data[1]),
                        'UPLOADED_DATE' => new Zend_Db_Expr('now()'),
                        'UPLOADED_BY'   => $this->_custIdLogin.$this->_userIdLogin,
                        'FLAG'       => 0
                    );

                    $this->_db->insert('TEMP_RECON', $insertArr);

                    Application_Helper_General::writeLog('RRUL','Upload Reconciliation Report. Adapter Profile: '.$adapterName.', Reconcile File Name: '.$reconName);
                    $this->setbackURL('/'.$this->_request->getModuleName());
                    $this->_redirect('/notification/success');
                }
            }

         //    $bankCode = $param['bank'];

	        // $select = $this->_db->select()
         //        ->from(array('R' => 'M_RECON_ADAPTER'),array('*'))
         //        ->join(array('D' => 'M_RECON_ADAPTER_DETAIL'), 'R.ADAPTER_ID = D.ADAPTER_ID', array('*'))
         //        ->where('R.BANK_CODE = ?', (string)$bankCode);

        	// $reconData = $this->_db->fetchAll($select); 

        	// $statementStartRow  = $reconData[0]['STATEMENT_START_FROM_ROW'];
        	// $reportStartRow 	= $reconData[0]['REPORT_START_FROM_ROW'];

        	// $statementHeaderData = $data[0][$statementStartRow-1];
         //    $reportHeaderData = $data[1][$reportStartRow-1];

        	// $statementData = $data[0];
         //    $reportData = $data[1];

            //--------------unset data from above start row---------------------
            // if (!empty($statementStartRow)) {
            //     for($i=0; $i<$statementStartRow;$i++){
            //         unset($statementData[$i]);
            //     }
            // }

            // if (!empty($reportStartRow)) {
            //     for($i=0; $i<$reportStartRow;$i++){
            //         unset($reportData[$i]);
            //     }
            // }

            //---------get rid of empty spaces between column on statement data---------
            // $statementEmptySpaces = array();
            // foreach ($statementHeaderData as $key => $value) {
            //     if (empty($value)) {
            //         array_push($statementEmptySpaces, $key);
            //         unset($statementHeaderData[$key]);
            //     }
            // }
            // foreach ($statementData as $key => $value) {
            //     foreach ($statementEmptySpaces as $spaces) {
            //         unset($statementData[$key][$spaces]);
            //     }
            // }

            //---------get rid of empty spaces between column on report data---------
            // $reportEmptySpaces = array();
            // foreach ($reportHeaderData as $key => $value) {
            //     if (empty($value)) {
            //         array_push($reportEmptySpaces, $key);
            //         unset($reportHeaderData[$key]);
            //     }
            // }
            // foreach ($reportData as $key => $value) {
            //     foreach ($reportEmptySpaces as $spaces) {
            //         unset($reportData[$key][$spaces]);
            //     }
            // }

            // //remove empty array
            // foreach ($statementData as $key => $value) {
            //     $limit = count($value) / 2;

            //     $count = 0;
            //     foreach ($value as $key2 => $value2) {
            //         if (empty($value2)) {
            //             $count++;
            //         }
            //     }

            //     if ($count >= $limit) {
            //         unset($statementData[$key]);
            //     }
            // }

            //  //remove empty array
            // foreach ($reportData as $key => $value) {
            //     $limit = count($value) / 2;

            //     $count = 0;
            //     foreach ($value as $key2 => $value2) {
            //         if (empty($value2)) {
            //             $count++;
            //         }
            //     }

            //     if ($count >= $limit) {
            //         unset($reportData[$key]);
            //     }
            // }
        	
        	//--------------------------------------------------sync data with adapter setting---------------------------------------- 

        	// foreach ($reconData as $key => $value) {
        	// 	foreach ($statementData as $key2 => $value2) {

                    
         //            $skip = false;

         //            //if selected column more than 1, then concat string
         //            $columnValue = '';
         //            if (strpos($value['STATEMENT_COLUMN'],',') !== false) {

         //                $explodeColumnValue = explode(',', $value['STATEMENT_COLUMN']);

         //                foreach ($explodeColumnValue as $explodeKey => $explodeValue) {
         //                    $columnValue .= $value2[$explodeValue];
         //                }
         //            }
         //            else{
         //                $columnValue = $value2[$value['STATEMENT_COLUMN']];
         //            }

         //            //if equal date, covert to standard date format
         //            if ($value['OPERATOR'] == 'equaldate') {
         //                $val = Application_Helper_General::convertDate($columnValue, 'dd-mm-yy', 'dd-mm-yy');
         //            }

         //            //if equal amount, check if amount is 0 or empty, then it is a debet column, not credit, so skip
         //            else if($value['OPERATOR'] == 'equalamount'){

         //                if (empty($columnValue) || intval($columnValue) == '0') {
         //                    $skip = true;
         //                }
         //                else{
         //                    $val = $this->removeSpaces($columnValue);
         //                }
         //            }
         //            else{
         //                 $val = $this->removeSpaces($columnValue);
         //            }

         //            if (!$skip) {
         //                $newStatementData[$key2][$value['HEADER_NAME']] = array(
         //                    'value' => $val,
         //                    'operator' => $value['OPERATOR']
         //                );
         //            }
        	// 	}

        	// 	foreach ($reportData as $key2 => $value2) {

         //            //if selected column more than 1, then concat string
         //            $columnValue = '';
         //            if (strpos($value['REPORT_COLUMN'],',') !== false) {

         //                $explodeColumnValue = explode(',', $value['REPORT_COLUMN']);

         //                foreach ($explodeColumnValue as $explodeKey => $explodeValue) {
         //                    $columnValue .= $value2[$explodeValue];
         //                }
         //            }
         //            else{
         //                $columnValue = $value2[$value['REPORT_COLUMN']];
         //            }

        	// 		$newReportData[$key2][$value['HEADER_NAME']] = array(
        	// 			'value' => ($value['OPERATOR'] == 'equaldate') ? Application_Helper_General::convertDate($columnValue, 'dd-mm-yy', 'dd-mm-yy') : $this->removeSpaces($columnValue),
        	// 			'operator' => $value['OPERATOR']
        	// 		);
        	// 	}
        	// }

         //    //--------------------------------------------------end sync data with adapter setting---------------------------------------- 

         //    //limit recon data Ex: (date, amount, desc), then limit will be 3
         //    $limit = count($reconData);

        	// $result = $this->reconciliate($newStatementData, $newReportData, $limit);

         //    //generate csv
         //    $this->generateReconFile($statementHeaderData ,$statementData, $reportHeaderData, $reportData, $result);
        }
	}

    public function generatecsvAction(){

        $id = $this->_request->getParam('id');
        $type = $this->_request->getParam('type');

        $select = $this->_db->select()
            ->from(array('R' => 'TEMP_RECON'),array('*'))
            ->where('R.ID = ?', (string)$id);

        $tempReconData = $this->_db->fetchAll($select); 

        $adapterName = $tempReconData[0]['ADAPTER_NAME'];

        $select = $this->_db->select()
            ->from(array('R' => 'M_RECON_ADAPTER'),array('*'))
            ->join(array('D' => 'M_RECON_ADAPTER_DETAIL'), 'R.ADAPTER_ID = D.ADAPTER_ID', array('*'))
            ->where('R.ADAPTER_NAME = ?', (string)$adapterName);

        $reconData = $this->_db->fetchAll($select); 

        $statementStartRow  = $reconData[0]['STATEMENT_START_FROM_ROW'];
        $reportStartRow  = $reconData[0]['REPORT_START_FROM_ROW'];

        $statementFileData = json_decode($tempReconData[0]['STATEMENT_FILE_DATA'], 1);
        $reportFileData = json_decode($tempReconData[0]['REPORT_FILE_DATA'], 1);

        $statementHeaderData = $statementFileData[$statementStartRow-1];
        $reportHeaderData = $reportFileData[$reportStartRow-1];

        $data = json_decode($tempReconData[0]['RECON_RESULT'], 1);

        $statementMatchData = $data['statementmatchdata'];
        $reportMatchData = $data['reportmatchdata'];
        $statementDiffData = $data['statementdiff'];
        $reportDiffData = $data['reportdiff'];


        $statementData = $statementFileData;
        $reportData = $reportFileData;

         //--------------unset data from above start row---------------------
        if (!empty($statementStartRow)) {
            for($i=0; $i<$statementStartRow;$i++){
                unset($statementData[$i]);
            }
        }

        if (!empty($reportStartRow)) {
            for($i=0; $i<$reportStartRow;$i++){
                unset($reportData[$i]);
            }
        }

        // ---------get rid of empty spaces between column on statement data---------
        $statementEmptySpaces = array();
        foreach ($statementHeaderData as $key => $value) {
            if (empty($value)) {
                array_push($statementEmptySpaces, $key);
                unset($statementHeaderData[$key]);
            }
        }
        foreach ($statementData as $key => $value) {
            foreach ($statementEmptySpaces as $spaces) {
                unset($statementData[$key][$spaces]);
            }
        }

        // ---------get rid of empty spaces between column on report data---------
        $reportEmptySpaces = array();
        foreach ($reportHeaderData as $key => $value) {
            if (empty($value)) {
                array_push($reportEmptySpaces, $key);
                unset($reportHeaderData[$key]);
            }
        }
        foreach ($reportData as $key => $value) {
            foreach ($reportEmptySpaces as $spaces) {
                unset($reportData[$key][$spaces]);
            }
        }

        //remove empty array
        foreach ($statementData as $key => $value) {
            $limit = count($value) / 2;

            $count = 0;
            foreach ($value as $key2 => $value2) {
                if (empty($value2)) {
                    $count++;
                }
            }

            if ($count >= $limit) {
                unset($statementData[$key]);
            }
        }

         //remove empty array
        foreach ($reportData as $key => $value) {
            $limit = count($value) / 2;

            $count = 0;
            foreach ($value as $key2 => $value2) {
                if (empty($value2)) {
                    $count++;
                }
            }

            if ($count >= $limit) {
                unset($reportData[$key]);
            }
        }

        //-----------------------------------match data---------------------------------------------------

        if ($type == 'match') {
            // $headerData[] = 'Here is your statement match data';
            // $csvData[] = $statementHeaderData;

            $headerData[] = $statementHeaderData;

            foreach ($statementData as $key => $value) {
                if (in_array($key, $statementMatchData)) {
                    $csvData[] = $value;
                }
            }

            //space
            $csvData[] = '';
            $csvData[] = '';
            $csvData[] = '';

            // $csvData[] = array('Here is your report match data');
            $csvData[] = $reportHeaderData;

            $csvData = array();
            foreach ($reportData as $key => $value) {
                if (in_array($key, $reportMatchData)) {
                    array_push($csvData, $value);
                }
            }

            $this->_helper->download->csv($headerData,$csvData,null,'Match Data', 1);   
            Application_Helper_General::writeLog('RRDL','Download Reconciliation Report. Adapter Profile: '.$adapterName.', Reconcile File Name: '.$tempReconData[0]['RECON_NAME'].', Report Type: Match Data');
        }

        //-----------------------------------statement diff data--------------------------------------------------------
        else if($type == 'statement'){
            // $leftHeader[] = 'Statement diff data';
            // $leftCsvData[] = $statementHeaderData;

            $leftHeader[] = $statementHeaderData;

            $leftCsvData = array();
            foreach ($statementData as $key => $value) {
                if (in_array($key, $statementDiffData)) {
                    array_push($leftCsvData, $value);
                }
            }

            $this->_helper->download->csv($leftHeader,$leftCsvData,null,'Statement Diff Data', 1);    
            Application_Helper_General::writeLog('RRDL','Download Reconciliation Report. Adapter Profile: '.$adapterName.', Reconcile File Name: '.$tempReconData[0]['RECON_NAME'].', Report Type: Statement Diff'); 
        }

        // -----------------------------------right data--------------------------------------------------------
        else{
            // $rightHeader[] = 'Report diff data';
            // $rightCsvData[] = $reportHeaderData;

            $rightHeader[] = $reportHeaderData;
            // $rightCsvData[] = $reportHeaderData;

            $rightCsvData = array();
            foreach ($reportData as $key => $value) {
                if (in_array($key, $reportDiffData)) {
                    array_push($rightCsvData, $value);
                }
            }

            $this->_helper->download->csv($rightHeader,$rightCsvData,null,'Report Diff Data', 1);     
            Application_Helper_General::writeLog('RRDL','Download Reconciliation Report. Adapter Profile: '.$adapterName.', Reconcile File Name: '.$tempReconData[0]['RECON_NAME'].', Report Type: Report Diff');
        }
    }

    //reconciliate data
	public function reconciliate($statementData, $reportData, $limit){

        //the larger array will be leftdata
		// if (count($statementData) > count($reportData)) {
		// 	$leftData = $statementData;
		// 	$rightData = $reportData;
		// }
		// else{
		// 	$leftData = $reportData;
		// 	$rightData = $statementData;
		// }

        $leftData = $statementData;
        $rightData = $reportData;

		foreach ($leftData as $leftKey => $leftValue) {

            foreach ($rightData as $rightKey => $rightValue) {
                
                $match = 0;
                foreach ($leftValue as $leftKey2 => $leftValue2) {
                        
                    if ($leftValue2['operator'] == 'equal' || $leftValue2['operator'] == 'equaldate' || $leftValue2['operator'] == 'equalamount') {

                        if ($leftValue2['value'] == $rightValue[$leftKey2]['value']) {
                           $match++; 
                        }

                    }
                    else if($leftValue2['operator'] == 'contain' || $leftValue2['operator'] == 'containinverted'){

                        $inverted = false;
                        if ($leftValue2['operator'] == 'containinverted') {
                            $inverted = true;
                        }

                        if ($this->compareContain($leftValue2['value'], $rightValue[$leftKey2]['value'], $inverted)) {
                            $match++;
                        }

                    }
                }

                //if all match, then add to matchData
                if ($match == $limit) {
                    $statementMatchData[] = $leftKey;
                    $reportMatchData[] = $rightKey;
                    
                    unset($leftData[$leftKey]);
                    unset($rightData[$rightKey]);
                }
            }
        }

        $statementDiffData = array_keys($leftData);
        $reportDiffData = array_keys($rightData);

        $result = array(
            'statementmatchdata' => $statementMatchData,
            'reportmatchdata' => $reportMatchData,
            'statementdiff' => $statementDiffData,
            'reportdiff' => $reportDiffData
        );


        return $result;
	}

    public function compareContain($string1, $string2, $inverted){

        // $string1 = 'ATMLTRBCA000810142111591234500027TRFPRIMA FROM';
        // $string2 = '111591234500027B11EMERALDLANDATMLTRBCA000810142111591234500027';

        $string1 = preg_replace('/\D/', '', $string1);
        $string2 = preg_replace('/\D/', '', $string2);

        // echo $string1.' | '.$string2;die();

        $haystack = $string2;
        $needle = $string1;
        //if inverted is true
        if ($inverted) {
            $haystack = $string1;
            $needle = $string2;
        }

        $substr = substr($needle, 0, 15); //utk kasus va, ambil 15 huruf terdepan

        if (strpos($haystack, $substr) !== false) {
            return true;
        }
        else{
            return false;
        }

    }

	public function removeSpaces($string){
		return str_replace(' ', '', $string);
	}


    public function generateReconFile($statementHeaderData, $statementData, $reportHeaderData, $reportData, $data){

        $statementMatchData = $data['statementmatchdata'];
        $reportMatchData = $data['reportmatchdata'];
        $statementDiffData = $data['statementdiff'];
        $reportDiffData = $data['reportdiff'];


        //-----------------------------------match data---------------------------------------------------
        $headerData[] = 'Here is your statement match data';
        $csvData[] = $statementHeaderData;

        foreach ($statementData as $key => $value) {
            if (in_array($key, $statementMatchData)) {
                $csvData[] = $value;
            }
        }

        //space
        $csvData[] = '';
        $csvData[] = '';
        $csvData[] = '';

        $csvData[] = array('Here is your report match data');
        $csvData[] = $reportHeaderData;

        foreach ($reportData as $key => $value) {
            if (in_array($key, $reportMatchData)) {
                $csvData[] = $value;
            }
        }

        $this->_helper->download->csv($headerData,$csvData,null,'Match Data', 1);      

        //-----------------------------------end match data---------------------------------------------------

        //-----------------------------------left data--------------------------------------------------------

        // $leftHeader[] = 'Statement diff data';
        // $leftCsvData[] = $statementHeaderData;

        // foreach ($statementData as $key => $value) {
        //     if (in_array($key, $statementDiffData)) {
        //         $leftCsvData[] = $value;
        //     }
        // }

        // $this->_helper->download->csv($leftHeader,$leftCsvData,null,'Statement Diff Data', 1);     

        //-----------------------------------end left data--------------------------------------------------------

        //-----------------------------------right data--------------------------------------------------------

        // $rightHeader[] = 'Report diff data';
        // $rightCsvData[] = $reportHeaderData;

        // foreach ($reportData as $key => $value) {
        //     if (in_array($key, $reportDiffData)) {
        //         $rightCsvData[] = $value;
        //     }
        // }

        // $this->_helper->download->csv($rightHeader,$rightCsvData,null,'Report Diff Data', 1);     

        //-----------------------------------end right data--------------------------------------------------------
    }
}
 