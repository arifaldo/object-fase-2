<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class marjinaldeposit_IndexController extends Application_Main
{

  protected $_moduleDB = 'RTF'; // masih harus diganti

  public function indexAction()
  {
    $this->_helper->_layout->setLayout('newlayout');

    $settings = new Settings();
    $system_type = $settings->getSetting('system_type');
    $this->view->systemType = $system_type;

    $conf = Zend_Registry::get('config');

    $fields = array(
      'norek'     => array(
        'field'    => 'MD_ACCT',
        'label'    => $this->language->_('No Rekening'),
      ),
      'namarek'     => array(
        'field'    => 'MD_ACCT_NAME',
        'label'    => $this->language->_('Nama Rekening'),
      ),
      'type'  => array(
        'field'    => 'MD_ACCT_TYPE',
        'label'    => 'Tipe Rekening',
      ),
      'matauang'  => array(
        'field'    => 'MD_ACCT_CCY',
        'label'    => $this->language->_('Mata Uang'),
      ),
      'nominal' => array(
        'field' => 'GUARANTEE_AMOUNT',
        'label' => $this->language->_('Nominal'),
      )
    );

    $lastupdates = date('d M Y H:i:s');

    unset($_SESSION['lastupdate']);

    Application_Helper_General::writeLog('VIMD', $this->language->_('Lihat Daftar Marginal Deposit Asuransi'));

    $sessionNamespace = new Zend_Session_Namespace('lastupdate');
    $sessionNamespace->content = $lastupdates;
    $datalastupdates = $sessionNamespace->content;
    $this->view->datalastupdates = $datalastupdates;


    $sumselect = $this->_db->select()
      ->from(array('A' => 'M_MARGINALDEPOSIT_DETAIL'), array("totalamount"   => "sum(A.GUARANTEE_AMOUNT)"))
      ->joinLeft(array('M' => 'M_CUSTOMER_ACCT'), 'A.MD_ACCT = M.ACCT_NO', array('M.ACCT_NAME'))
      ->where('A.CUST_ID = ?', $this->_custIdLogin);


    $select = $this->_db->select()
      ->from(array('A' => 'M_MARGINALDEPOSIT_DETAIL'), array('*'))
      ->joinLeft(array('M' => 'M_CUSTOMER_ACCT'), 'A.MD_ACCT = M.ACCT_NO', array('M.ACCT_NAME'))
      ->where('A.CUST_ID = ?', $this->_custIdLogin);


    $filterlist = array('MD_ACCT', 'ACCT_NAME', 'MD_ACCT_TYPE', 'MD_ACCT_CCY', 'GUARANTEE_AMOUNT');

    $this->view->filterlist = $filterlist;

    $dataParam = array('MD_ACCT', 'ACCT_NAME', 'MD_ACCT_TYPE', 'MD_ACCT_CCY', 'GUARANTEE_AMOUNT');

    $dataParamValue = array();

    foreach ($dataParam as $dtParam) {
      if (!empty($this->_request->getParam('wherecol'))) {
        $dataval = $this->_request->getParam('whereval');
        foreach ($this->_request->getParam('wherecol') as $key => $value) {
          if ($dtParam == $value) {

            if (empty($dataParamValue[$dtParam])) {
              $dataParamValue[$dtParam] = [];
            }
            array_push($dataParamValue[$dtParam], $dataval[$key]);
          }
        }
      }
    }

    $options = array('allowEmpty' => true);
    $validators = array(
      'MD_ACCT' => array(),
      'ACCT_NAME' => array(),
      'MD_ACCT_TYPE' => array(),
      'MD_ACCT_CCY' => array(),
      'GUARANTEE_AMOUNT' => array(),
    );

    $zf_filter  = new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);

    $fMdAcct = $zf_filter->getEscaped('MD_ACCT');
    $fAcctName = $zf_filter->getEscaped('ACCT_NAME');
    $fType = $zf_filter->getEscaped('MD_ACCT_TYPE');
    $fAcctCcy = $zf_filter->getEscaped('MD_ACCT_CCY');
    $fGurantee = $zf_filter->getEscaped('GUARANTEE_AMOUNT');

    if ($fMdAcct) {
      $select->where("MD_ACCT LIKE " . $this->_db->quote('%' . $fMdAcct[0] . '%'));
    }
    if ($fAcctName) {
      $select->where("ACCT_NAME LIKE " . $this->_db->quote('%' . $fAcctName[0] . '%'));
    }
    if ($fType) {
      $select->where("MD_ACCT_TYPE LIKE " . $this->_db->quote('%' . $fType[0] . '%'));
    }
    if ($fAcctCcy) {
      $select->where("MD_ACCT_CCY = " . $this->_db->quote($fAcctCcy[0]));
    }
    if ($fGurantee) {
      $select->where("GUARANTEE_AMOUNT LIKE " . $this->_db->quote('%' . strtoupper($fGurantee[0]) . '%'));
    }

    $select = $select->query()->fetchAll();
    $sumamount = $this->_db->fetchRow($sumselect);
    $this->view->totalamount = $sumamount;

    $this->paging($select);

    $conf = Zend_Registry::get('config');

    // unset($fields['insurance_branch_ccy']);
    $this->view->fields = $fields;
    $this->view->filter = $filter;

    if (!empty($dataParamValue)) {

      $this->view->createdStart = $dataParamValue['TIME_PERIOD_START'];
      $this->view->createdEnd = $dataParamValue['TIME_PERIOD_END'];

      foreach ($dataParamValue as $key => $value) {
        $duparr = explode(',', $value);
        if (!empty($duparr)) {
          foreach ($duparr as $ss => $vs) {
            $wherecol[] = $key;
            $whereval[] = $vs[0];
          }
        } else {
          $wherecol[] = $key;
          $whereval[] = $value[0];
        }
      }

      $this->view->wherecol     = $wherecol;
      $this->view->whereval     = $whereval;
    }


    $data = [];
    if (!empty($this->_getParam('csv')) || $this->_request->getParam('print') == 1) {

      foreach ($select as $key => $row) {
        $subData = [];
        $subData['MD_ACCT'] = $row['MD_ACCT'];
        $subData['MD_ACCT_NAME'] = $row['MD_ACCT_NAME'];
        $subData['MD_ACCT_TYPE'] = $row['MD_ACCT_TYPE'];
        $subData['MD_ACCT_CCY'] = $row['MD_ACCT_CCY'];
        $subData['GUARANTEE_AMOUNT'] = 'IDR ' . Application_Helper_General::displayMoneyplain($row['GUARANTEE_AMOUNT']);

        $data[] = $subData;
      }
    }

    if ($this->_getParam('csv')) {

      $this->_helper->download->csv(array($this->language->_('Nomor Rekening'), $this->language->_('Nama Rekening'), $this->language->_('Tipe Rekening'), $this->language->_('Mata Uang')), $data, null, 'Marjinal Deposit');
    } else if ($this->_request->getParam('print') == 1) {

      $fields = array(
        'norek'     => array(
          'field'    => 'MD_ACCT',
          'label'    => $this->language->_('No Rekening'),
        ),
        'namarek'     => array(
          'field'    => 'MD_ACCT_NAME',
          'label'    => $this->language->_('Nama Rekening'),
        ),
        'type'  => array(
          'field'    => 'MD_ACCT_TYPE',
          'label'    => 'Tipe Rekening',
        ),
        'matauang'  => array(
          'field'    => 'MD_ACCT_CCY',
          'label'    => $this->language->_('Mata Uang'),
        ),
        'nominal' => array(
          'field' => 'GUARANTEE_AMOUNT',
          'label' => $this->language->_('Nominal'),
        )
      );

      $this->_forward('print', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'Marjinal Deposit', 'data_header' => $fields));
    }
  }
}
