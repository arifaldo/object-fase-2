<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/SystemBalance.php';
require_once 'Crypt/AESMYSQL.php';

class airlines_EditdepositController extends airlines_Model_Airlines
{

	public function indexAction() 
	{

		$this->_helper->layout()->setLayout('newlayout');

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
	    $password = $sessionNamespace->token; 
	    $this->view->token = $sessionNamespace->token;

		$AESMYSQL = new Crypt_AESMYSQL();
		$airlines = $AESMYSQL->decrypt($this->_getParam('airlines'), $password);

		$this->view->airlines = $airlines;
		$this->view->airlinesName = $this->getAirlines('AIRLINE_NAME', array('AIRLINE_CODE' => $airlines))[0]['AIRLINE_NAME'];

		$this->view->compName = $this->_custNameLogin;		

		//get airlines data
		$airlinesList = $this->getAirlines();
		foreach ($airlinesList as $key => $value) {
			$airlinesArr[$value['AIRLINE_CODE']] = $value['AIRLINE_NAME'];
		}


		$airlinesData = $this->getAirlinesSettings($airlines);

		$this->view->balance_cycle = $airlinesData[0]['BALANCE_CYCLE'];
		$this->view->warning1 = $airlinesData[0]['WARN_LEVEL1'];
		$this->view->warning2 = $airlinesData[0]['WARN_LEVEL2'];
		$this->view->min_balance = $airlinesData[0]['MIN_BALANCE'];
		$this->view->amount_topup = $airlinesData[0]['AMOUNT_TOPUP'];

		$transaction_info = json_decode($airlinesData[0]['TRANSACTION_INFO'], 1);

		$this->view->transaction_info = $transaction_info;
		$this->view->acct_counter = count($transaction_info);
		$this->view->dividedby = $transaction_info[0]['type'];

		if (!$this->_getParam('submit')) {
			foreach ($transaction_info as $key => $value) {
				$source_acct_alias[] = $this->getAccountAlias($value['bankcode'], $value['sourceaccount']);
				$source_acct[] = $value['sourceaccount'];
				$bank_code[] = $value['bankcode'];
				$travelagent_id[] = $value['travelagentid'];
				$dividedval[] = $value['value'];
			}

			$this->view->source_acct_alias = $source_acct_alias;
			$this->view->source_acct = $source_acct;
			$this->view->bank_code = $bank_code;
			$this->view->travelagent = $travelagent_id;
			$this->view->dividedval = $dividedval;
		}

		$this->view->status = $airlinesData[0]['STATUS'];

		//get account list data
    	$account = $this->getAccountList();
        $this->view->account = $account;
        $this->view->account_count = count($account);

        //get deposit data
        $depositData = $this->getAirlinesDeposit();


        foreach ($depositData as $key => $value) {

    		$depositDataArr[$value['AIRLINE_CODE']][] = array(
    			'TRAVELAGENT_ID' => $value['TRAVELAGENT_ID'],
    			'DEPOSIT_ID'	 =>  $value['DEPOSIT_ID'],
    			'BANK_NAME'	 	 =>  $value['BANK_NAME'],
    			'ACCT_NO' 		 =>  $value['ACCT_NO'],
    			'VIRTUAL_ACCOUNT'=>  $value['VIRTUAL_ACCOUNT'],
    			'IS_ACTIVE' 	 =>  $value['IS_ACTIVE']
    		);

    		$travelAgentArr[$value['TRAVELAGENT_ID']] = $value['BANK_CODE'];
        }

		$this->view->depositData = $depositDataArr;
		$this->view->travelAgentData = $travelAgentArr;


		$cek = $this->isRequestChange($airlines);
		if(!$cek){

			if ($this->_getParam('submit')) {
					
				$filters = array(
			        'acct_counter' => array('StringTrim','StripTags'),
			        'min_balance' => array('StringTrim','StripTags'),
			        'amount_topup' => array('StringTrim','StripTags'),
			        'balance_cycle' => array('StringTrim','StripTags'),
			        'status' => array('StringTrim','StripTags')
				);

				$validators = array(
					'acct_counter' => array(
		            				'NotEmpty',
									'messages' => array(
												    $this->language->_('Can not be empty'),
		                                          )
								),
					'min_balance'  => array(
									'NotEmpty',
									'messages' => array(
													$this->language->_('Minimum balance Can not be empty')
												  )
								),
					'amount_topup'  	=> array(
									'NotEmpty',
									'messages' => array(
													$this->language->_('Auto topup amount Can not be empty')
												  )
								),
					'balance_cycle'  	=> array(
									'NotEmpty',
									'messages' => array(
													$this->language->_('Check every Can not be empty')
												  )
								),
					'status'  	=> array(
									'NotEmpty',
									'messages' => array(
													$this->language->_('Status Can not be empty')
												  )
								)
				);

				$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);

				$params = $this->_request->getParams();

				if($zf_filter_input->isValid())
				{	
					$counter = $zf_filter_input->acct_counter;

					$dividedby = $params['dividedby'];

					if ($dividedby == 0) {
						$dividedby = 'p';
					}
					else{
						$dividedby = 'f';
					}

					$dividedval = $params['dividedval'];

					$source_acct = $params['source_acct'];
					$source_acct_alias = $params['source_acct_alias'];	
					$bank_code = $params['bank_code'];
					$travelagent_id = $params['travelagent'];

					if (!empty($source_acct)) {
						//validasi source account
						$source_acct_counter = 1;
						$source_acct_unique = [];
						foreach ($source_acct as $value) {

							if (in_array($value, $source_acct_unique)) {

								$error = true;
								$error_msg[] = 'Cannot choose same Account';
							}
							$source_acct_unique[] = $value;
						}	
					}
					else{
						$error = true;
						$error_msg[] = 'Source account cannot be left blank';
					}

					if(count(array_filter($travelagent_id)) != count($source_acct)){
						$error = true;
						$error_msg[] = 'Travel Agent ID cannot be left blank';
					}

					//if percent then check if reach 100
					if($dividedby == 'p'){
						if (array_sum($dividedval) < 100) {
							$error = true;
							$error_msg[] = 'Percentage not reach 100';
						}
					}
					//if fix then check if reach autop topup amount
					else{
						$dividedSum = 0;
						foreach ($dividedval as $key => $value) {
							$dividedSum += Application_Helper_General::convertDisplayMoney($value);
						}

						if ($dividedSum < Application_Helper_General::convertDisplayMoney($zf_filter_input->amount_topup)) {
							$error = true;
							$error_msg[] = 'Fix amount not reach amount topup';
						}
					}
								

					if ($error !== true) {

						$transaction_info = '[';
						for ($i=1; $i <= $counter; $i++) { 

							if ($i != $counter) {
								$comma = ',';
							}
							else{
								$comma = '';
							}

							if ($dividedby == 'f') {
								$dividedvalue = Application_Helper_General::convertDisplayMoney($dividedval[$i]);
							}
							else{
								$dividedvalue = $dividedval[$i];
							}
							
							$transaction_info .= '{"sourceaccount": "'.$source_acct[$i].'", "bankcode": "'.$bank_code[$i].'", "travelagentid": "'.$travelagent_id[$i].'", "type": "'.$dividedby.'", "value": "'.$dividedvalue.'"}'.$comma;
							
						}
						$transaction_info .= ']';
						
						if ($error !== true) {

							$data = array(
								'CUST_ID'				=> $this->_custIdLogin,
								'AIRLINE_CODE'			=> $airlines,
								'TRANSACTION_INFO'		=> $transaction_info,
								'WARN_LEVEL1'			=> Application_Helper_General::convertDisplayMoney($params['warning1']),
								'WARN_LEVEL2'			=> Application_Helper_General::convertDisplayMoney($params['warning2']),
								'MIN_BALANCE' 			=> Application_Helper_General::convertDisplayMoney($zf_filter_input->min_balance),
								'AMOUNT_TOPUP' 			=> Application_Helper_General::convertDisplayMoney($zf_filter_input->amount_topup),
								'BALANCE_CYCLE' 		=> $zf_filter_input->balance_cycle,
								'UPDATED_BY' 			=> $this->_userIdLogin,
								'UPDATED_DATE' 			=> date('Y-m-d H:i:s'),
								'STATUS' 				=> $zf_filter_input->status
							);

							//input db
							$this->_db->beginTransaction();

							try {

								$change_id = $this->suggestionWaitingApproval('Airline Setting','Edit airline setting',strtoupper($this->_changeType['code']['edit']),null,'M_AIRLINES_SETTINGS','TEMP_AIRLINES_SETTINGS',$airlines,$airlinesArr[$airlines],$this->_custIdLogin);

								$data['CHANGES_ID'] = $change_id;

								$this->_db->insert('TEMP_AIRLINES_SETTINGS',$data);
								$successUpdate = true;
								
							} catch (Exception $e) {
								print_r($e);die();
								$successUpdate = false;
								$this->_db->rollBack();
								
								foreach(array_keys($filters) as $field)
									$this->view->$field = $zf_filter_input->getEscaped($field);

								$errorMsg = 'exeption';
								$this->_helper->getHelper('FlashMessenger')->addMessage('F');
								$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
							}

							$this->_db->commit();	

							if ($successUpdate) {
								$this->setbackURL('/'.$this->_request->getModuleName());
								$this->_redirect('/notification/submited');
							}
							else{
								$this->view->error = true;
								$error_msg[] = 'Failed to update data';
								$this->view->error_msg = $error_msg;
							}
							
						}else{
							$this->view->error = true;
							$this->view->error_msg = $error_msg;
						}

					}
					else{
						$this->view->error = true;
						$this->view->error_msg = $error_msg;
					}
				}
				else{

					$this->view->error = true;

					$error = $zf_filter_input->getMessages();

					$errorArray = null;
			        foreach($error as $keyRoot => $rowError)
			        {
			           foreach($rowError as $errorString)
			           {
			              $errorArray[$keyRoot] = $errorString;
			           }
			        }

	                $this->view->error_msg = $errorArray;
				}

				foreach(array_keys($filters) as $field)
					$this->view->$field = $this->_getParam($field);

				$this->view->source_acct_alias = $params['source_acct_alias'];
				$this->view->source_acct = $params['source_acct'];
				$this->view->bank_code = $params['bank_code'];
				$this->view->travelagent = $params['travelagent'];
				$this->view->dividedby = $params['dividedby'];
				$this->view->dividedval = $params['dividedval'];
				$this->view->warning1 = $params['warning1'];
				$this->view->warning2 = $params['warning2'];
				$this->view->remainingDivided = $params['remainingDivided'];
			}

		}
		else{	
			$this->view->changestatus = true;
		}
	}

	public function accountsuggestionAction()
	{
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $airlines = $_GET['airlines'];

        //get deposit data
        $depositData = $this->getAirlinesDeposit();

        foreach ($depositData as $key => $value) {

    		$depositDataArr[$value['AIRLINE_CODE']][] = array(
    			'BANK_CODE' => $value['BANK_CODE']
    		);
        }

        foreach ($depositDataArr[$airlines] as $key => $value) {
        	$bankArr[] = $value['BANK_CODE'];
        }
        
        $account = $this->getAccountList();

        foreach ($account as $key => $value) {

        	if (empty($value['account_currency'])) {
        		$ccy = 'IDR';
        	}
        	else{
        		$ccy = $value['account_currency'];
        	}

        	if (!empty($value['account_alias'])) {
        		$account[$key]['ALIAS'] =  $value['account_number'].' - '.$value['BANK_NAME'].' ('.$ccy.') - '.$value['account_alias'];
        	}
        	else{
        		$account[$key]['ALIAS'] =  $value['account_number'].' - '.$value['BANK_NAME'].' ('.$ccy.')';
        	}
        }

        $searchTerm = $_GET['term'];

        $accountData = array();
        foreach ($account as $key => $value) {

        	if (strpos(strtolower($value['ALIAS']), strtolower($searchTerm)) !== false && in_array($value['BANK_CODE'], $bankArr)) {
        		$data['id']    = $value['account_number'];
		        $data['value'] = $value['ALIAS'];
		        $data['bankcode'] = $value['BANK_CODE'];
		        $data['label'] = '
		        <a href="javascript:void(0);" style="text-decoration:none;">
		            <img class="img-fluid" src="'.$value['BANK_IMG'].'" width="70"/>
		            <span>  '.$value['ALIAS'].'</span>
		        </a>';
		        array_push($accountData, $data);
        	}
        }

        echo json_encode($accountData);
	}
}
