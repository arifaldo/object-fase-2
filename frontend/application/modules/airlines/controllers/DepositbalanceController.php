<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/SystemBalance.php';

class airlines_DepositBalanceController extends Application_Main
{

	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');

		$airlinesarr = array(
			'QZ' => 'airasia.png',
			'QG' => 'citilink.png'
		);

		$this->view->airlinesarr = $airlinesarr;


		$select = $this->_db->select()
					->from(array('A' => 'M_AIRLINES_SETTINGS'),array('*'));

		$airlinesdata = $this->_db->fetchall($select);

		$this->view->data = $airlinesdata;

		//get cache
		$frontendOptions = array ('lifetime' => 600,
                                  'automatic_serialization' => true );
        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
        $cacheID = 'AIRLINESBAL'.$this->_custIdLogin;

        $data = $cache->load($cacheID);

        if(!empty($data)){

        	$balancearr = $data['balancearr'];
          	
        }else{

        	$clientUser  =  new SGO_Soap_ClientUser();

			$balancearr = array();
			foreach ($airlinesdata as $key => $value) {

				$paramObj = array(
					'airline_code' => $value['AIRLINE_CODE'],
					'AUTH_USER' => 'travel',
					'AUTH_PASS' => 'test'
				);

				$success = $clientUser->callapi('airlinesDepositInquiry',$paramObj,'airlines/agent/info');
			
				if($clientUser->isTimedOut()){
					$returnStruct = array(
							'ResponseCode'	=>'XT',
							'ResponseDesc'	=>'Service Timeout',					
							'Cif'			=>'',
							'AccountList'	=> '',
					);
				}else{
					$result  = $clientUser->getResult();

					if($result->error_code == '0000'){

						$balancearr[$value['AIRLINE_CODE']] = $result->account_info->available_credit;
					}
				}
			}

			$save['balancearr'] = $balancearr;
            $cache->save($save,$cacheID);
        }

        $this->view->balancearr = $balancearr;
	}

	public function deleteairlineAction(){

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $airlinecode = $this->_getParam('airlinecode');

        $where['AIRLINE_CODE = ?'] = $airlinecode;
		$delete = $this->_db->delete('M_AIRLINES_SETTINGS',$where);

		if ($delete) {
			echo true;
		}
		else{
			echo false;
		}
    }

    public function checkbalanceAction(){

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $airlinecode = $this->_getParam('airlinecode');

        $clientUser  =  new SGO_Soap_ClientUser();

      	$paramObj = array(
			'airline_code' => $airlinecode,
			'AUTH_USER' => 'travel',
			'AUTH_PASS' => 'test'
		);

		$success = $clientUser->callapi('airlinesDepositInquiry',$paramObj,'airlines/agent/info');
	
		if($clientUser->isTimedOut()){
			
			echo 'N/A';

		}else{
			$result  = $clientUser->getResult();

			if($result->error_code == '0000'){

				//get cache
				$frontendOptions = array ('lifetime' => 600,
		                                  'automatic_serialization' => true );
		        $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
		        $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
		        $cacheID = 'AIRLINESBAL'.$this->_custIdLogin;

		        $data = $cache->load($cacheID);

		        if (!empty($data)) {

		        	$data['balancearr'][$airlinecode] = $result->account_info->available_credit;
		        	$cache->save($data,$cacheID);
		        	
		        }
		        else{

		        }

				echo $result->account_info->available_credit;
			}
		}
    }
}
