<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';
class linkagebeneficiary_ManageController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti
	protected $benefcode;
	protected $benefdesc;

	public function initController()
	{
		$listCcy = array(''=>'-- '.$this->language->_('Select Currency') .'--');
		$listCcy = array_merge($listCcy,Application_Helper_Array::listArray($this->getCcy(),'CCY_ID','CCY_ID'));
		$this->view->ccyArr = $listCcy;

		$usersArr = $this->_db->fetchAll(
								$this->_db->select()
									 ->from(array('M_FPRIVI_USER'),array('FUSER_ID'))
									 ->where("FUSER_ID LIKE ?", $this->_custIdLogin.'%')
									 ->where("FPRIVI_ID=?", 'BLBU')
							);
				$datauser = array();
		foreach($usersArr as $key => $val){
			$datauser[$key]['USER_ID'] = str_replace($this->_custIdLogin,'',$val['FUSER_ID']);
		}	
		
		
		$listUser = array(''=>'-- '. $this->language->_('Select User'). ' --');
		$listUser = array_merge($listUser,Application_Helper_Array::listArray($datauser,'USER_ID','USER_ID'));
		
		
		
		$this->view->userArr = $listUser;

		//Zend_Debug::dump($this->_beneftype);

		$listType = array(''=>'-- '.$this->language->_('All').' --');
		$listType += Application_Helper_Array::globalvarArray($this->_beneftype);
		foreach($listType as $key => $value){ if($key != 3) $optpayStatusRaw[$key] = $this->language->_($value); }
		//$this->view->typeArr = $listType;
		$this->view->typeArr = $optpayStatusRaw;

		$this->benefcode = array_flip($this->_beneftype['code']);
		$this->benefdesc = $this->_beneftype['desc'];
		$this->view->benefcode = $this->benefcode;
		$this->view->benefdesc = $this->benefdesc;

	}

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$fields = array(
						// 'user_id'   => array('field'    => 'USER_ID',
						// 					  'label'    => $this->language->_('User ID'),
						// 					  'sortable' => true),
						'user_name'   => array('field'    => 'USER_FULLNAME',
											  'label'    => $this->language->_('User Name'),
											  'sortable' => true),
						'benef_bank'  => array('field' => 'C.BENEFICIARY_ACCOUNT',
											   'label' => $this->language->_('Beneficiary Bank'),
											   'sortable' => true),
						'benef_acct'  => array('field' => 'C.BENEFICIARY_ACCOUNT',
											   'label' => $this->language->_('Beneficiary Account'),
											   'sortable' => true),
						'benef_name'  => array('field' => 'C.BENEFICIARY_NAME',
											   'label' => $this->language->_('Beneficiary Name'),
											   'sortable' => true),
						// 'alias'  => array('field' => 'BENEFICIARY_ALIAS',
						// 					   'label' => $this->language->_('Alias Name'),
						// 					   'sortable' => true),
						// 'ccy'   => array('field'    => 'CURR_CODE',
						// 					  'label'    => $this->language->_('CCY'),
						// 					  'sortable' => true),
						'benef_type'   => array('field'    => 'BENEFICIARY_TYPE',
											  'label'    => $this->language->_('Type'),
											  'sortable' => true)
				);

		$filterlist = array("USER_ID",'USER_FULLNAME','BENEFICIARY_ACCOUNT','BENEFICIARY_NAME','CURR_CODE','BENEFICIARY_TYPE');
		
		$this->view->filterlist = $filterlist;
		//get page, sortby, sortdir
		$pdf	 = $this->_getParam('pdf');
		$csv	 = $this->_getParam('csv');
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','doc_no');
		$sortDir = $this->_getParam('sortdir','asc');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits'))? $page : 1;

		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';


		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	
							'USER_ID' 	  	=> array('StringTrim','StripTags'),
							'USER_FULLNAME' 	=> array('StringTrim','StripTags'),
							'BENEFICIARY_ACCOUNT'    => array('StringTrim','StripTags'),
							'BENEFICIARY_NAME'    => array('StringTrim','StripTags','StringToUpper'),
							'CURR_CODE'    		=> array('StringTrim','StripTags','StringToUpper'),
							'BENEFICIARY_TYPE'    => array('StringTrim','StripTags')
		);
		$dataParam = array("USER_ID",'USER_FULLNAME','BENEFICIARY_ACCOUNT','BENEFICIARY_NAME','CURR_CODE','BENEFICIARY_TYPE');
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{
			
			// print_r($dtParam);die;
			if(!empty($this->_request->getParam('whereco'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('whereco') as $key => $value) {
						if($dtParam==$value){
							if(!empty($dataParamValue[$dtParam])){
								$dataval[$key] = $dataParamValue[$dtParam] .','.$dataval[$key]; 
							}
							
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}
				
			}
				
			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}
		// var_dump($dataParamValue);

		$options = array('allowEmpty' => true);
		$validators = array(
							'USER_ID' 	  	=> array(),
							'USER_FULLNAME' 	=> array(),
							'BENEFICIARY_ACCOUNT'    => array(),
							'BENEFICIARY_NAME'    => array(),
							'CURR_CODE'    		=> array(),
							'BENEFICIARY_TYPE'    => array()
		);	

		$zf_filter = new Zend_Filter_Input($filterArr,$validators,$dataParamValue,$options);
		// $filter = $zf_filter->getEscaped('filter');
		$filter 		= $this->_getParam('filter');

		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;

		$caseScheme = "(CASE BENEFICIARY_TYPE ";
		foreach($this->benefcode as $key=>$val)
		{
			$caseScheme .= " WHEN ".$key." THEN '".$this->benefdesc[$val]."' ";
		}
		$caseScheme .= " END)";

		$select = $this->_db->select()->distinct()
					   ->from(	array('A' => 'M_BENEFICIARY_USER'),array())
					   ->join(	array('B' => 'M_USER'),'A.USER_ID = B.USER_ID AND A.CUST_ID = B.CUST_ID',
								array('USER_ID','USER_FULLNAME'))
					   ->join(	array('C' => 'M_BENEFICIARY'),'A.BENEFICIARY_ID = C.BENEFICIARY_ID',
								array('BENEFICIARY_ACCOUNT','BENEFICIARY_NAME','BENEFICIARY_ALIAS','BANK_NAME','CURR_CODE','BENEFICIARY_TYPE'=>$caseScheme));
		$select->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin));

		// if($filter == TRUE || $pdf || $this->_request->getParam('print'))
		// {
			$fUserId = $zf_filter->getEscaped('USER_ID');
			$fUserName = $zf_filter->getEscaped('USER_FULLNAME');
			$fAcct = $zf_filter->getEscaped('BENEFICIARY_ACCOUNT');
			$fName = $zf_filter->getEscaped('BENEFICIARY_NAME');
			$fCcy = $zf_filter->getEscaped('CURR_CODE');
			$fType = $zf_filter->getEscaped('BENEFICIARY_TYPE');

	        // if($fUserId)$select->where('UPPER(A.USER_ID) = '.$this->_db->quote(strtoupper($fUserId)));
	        // if($fUserName)$select->where('UPPER(USER_FULLNAME) LIKE '.$this->_db->quote('%'.strtoupper($fUserName).'%'));
	        // if($fAcct)$select->where('C.BENEFICIARY_ACCOUNT LIKE '.$this->_db->quote('%'.$fAcct.'%'));
	        // if($fName)$select->where('UPPER(BENEFICIARY_NAME) LIKE '.$this->_db->quote('%'.strtoupper($fName).'%'));
	        // if($fCcy)$select->where('UPPER(CURR_CODE) = '.$this->_db->quote(strtoupper($fCcy)));
					// if($fType)$select->where('BENEFICIARY_TYPE = ?',$fType);
			if($fUserId)
			{	
				$fUserIdarr = explode(',', $fUserId);
				$select->where("UPPER(A.USER_ID)  in (?)",$fUserIdarr );	
			}
			if($fUserName)
			{	
				$fUserNamearr = explode(',', $fUserName);
				$select->where("UPPER(USER_FULLNAME)  in (?)",$fUserNamearr );	
			}
			if($fAcct)
			{	
				$fAcctarr = explode(',', $fAcct);
				$select->where("C.BENEFICIARY_ACCOUNT  in (?)",$fAcctarr );	
			}
			if($fName)
			{	
				$fNamearr = explode(',', $fName);
				$select->where("UPPER(BENEFICIARY_NAME)  in (?)",$fNamearr );	
			}
			if($fCcy)
			{	
				$fCcyarr = explode(',', $fCcy);
				$select->where("UPPER(CURR_CODE)  in (?)",$fCcyarr );	
			}
			if($fType)
			{	
				$fTypearr = explode(',', $fType);
				$select->where("BENEFICIARY_TYPE  in (?)",$fTypearr );	
			}

			$this->view->user_id = $fUserId;
			$this->view->user_name= $fUserName;
			$this->view->benef_acct = $fAcct;
			$this->view->benef_name = $fName;
			$this->view->ccy = $fCcy;
			$this->view->benef_type = $fType;
		// }
		//die("$select");
	    $select->order($sortBy.' '.$sortDir);
		$arr = $this->_db->fetchAll($select);
		$this->paging($arr);

		//new - begin
		$data_val = array();
		$i=0;
		foreach ($arr as $data) {
			
			$data['USER_FULLNAME'] = $data['USER_FULLNAME'].' ('.$data['USER_ID'].')';
			$data['BANK_NAME'] = $this->language->_($data['BANK_NAME']);
			$data['BENEFICIARY_TYPE'] = $this->language->_($data['BENEFICIARY_TYPE']);
			unset($data['BENEFICIARY_ALIAS']);
			unset($data['CURR_CODE']);
			unset($data['USER_ID']);

			$paramTrx = array(	"USER_FULLNAME"  		=> $data['USER_FULLNAME'],
								"BANK_NAME"  			=> $data['BANK_NAME'],
								"BENEFICIARY_ACCOUNT"  	=> $data['BENEFICIARY_ACCOUNT'],
								"BENEFICIARY_NAME"  	=> $data['BENEFICIARY_NAME'],
								"BENEFICIARY_TYPE" 		=> $data['BENEFICIARY_TYPE'],
							);

			/*echo "<pre>";
			var_dump($paramTrx);*/

			array_push($data_val, $paramTrx);
			//$data_val[$i++] = $data;
		}

		$filterlistdata = array("Filter");
			foreach($dataParamValue as $fil => $val){
					$paramTrx = $fil . " - " . $val;
					array_push($filterlistdata, $paramTrx);
		}
		// die();
		//new - end
//		Zend_Debug::dump($data_val);
//		die;

		$this->view->fields = $fields;
		$this->view->filter = $filter;

		if($pdf)
		{


			$this->_helper->download->pdf(array($this->language->_('User Name'),$this->language->_('Beneficiary Bank'),$this->language->_('Beneficiary Account'),$this->language->_('Beneficiary Account Name'), $this->language->_('Type')),$data_val,null,'Beneficiary Linkage');
		}

		if ($csv) {
			$header  = Application_Helper_Array::simpleArray($fields, "label");
			$listable = array_merge_recursive(array($header), $data_val);
			$this->_helper->download->csv($filterlistdata,$listable,null,'Beneficiary Linkage');

		}

		if($this->_request->getParam('print') == 1){
            $data = $arr;//$this->_dbObj->fetchAll($select);
            $this->_forward('print', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'Pairing Beneficiary', 'data_header' => $fields));
				}
		if (!empty($dataParamValue)) {
			// $this->view->createdStart = $dataParamValue['PS_CREATED'];
			// $this->view->createdEnd = $dataParamValue['PS_CREATED_END'];
		//	$this->view->efdateStart = $dataParamValue['PS_EFDATE'];
		//	$this->view->efdateEnd = $dataParamValue['PS_EFDATE_END'];

			// unset($dataParamValue['PS_CREATED_END']);
			// unset($dataParamValue['PS_EFDATE_END']);

			foreach ($dataParamValue as $key => $value) {
				$duparr = explode(',',$value);
							if(!empty($duparr)){
								
								foreach($duparr as $ss => $vs){
									$wherecol[]	= $key;
									$whereval[] = $vs;
								}
							}else{
									$wherecol[]	= $key;
									$whereval[] = $value;
							}
			}
			$this->view->wherecol     = $wherecol;
			$this->view->whereval     = $whereval;
				
		}
	//   if(!empty($this->_request->getParam('wherecol'))){
	// 	$this->view->wherecol			= $this->_request->getParam('wherecol');
	// }

	// if(!empty($this->_request->getParam('whereopt'))){
	// 	$this->view->whereopt			= $this->_request->getParam('whereopt');
	// }

	// if(!empty($this->_request->getParam('whereval'))){
	// 	$this->view->whereval			= $this->_request->getParam('whereval');
	// }
			
				$app = Zend_Registry::get('config');
 		$appBankname = $app['app']['bankname'];
		$this->view->app_bank = $appBankname;
			
		//$this->_helper->download->csv(array('Group ID','Group Name','Status'),null,$select,'download group');
		Application_Helper_General::writeLog('BMBU',"Viewing Manage Linkage User");
	}

	public function managelinkbenefAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$in = null;
		$cektemp = '';
		$fields = array(
						'benef_bank'  => array('field' => 'BANK_NAME',
											   'label' => $this->language->_('Bank Name'),
											   'sortable' => true),
						'benef_acct'  => array('field' => 'BENEFICIARY_ACCOUNT',
											   'label' => $this->language->_('Beneficiary Account'),
											   'sortable' => true),
						// 'ccy'   => array('field'    => 'CURR_CODE',
						//  					  'label'    => $this->language->_('CCY'),
						// 					  'sortable' => true),
						'benef_name'  => array('field' => 'BENEFICIARY_NAME',
											   'label' => $this->language->_('Beneficiary Name'),
											   'sortable' => true),
						// 'alias'  => array('field' => 'BENEFICIARY_ALIAS',
						// 					   'label' => $this->language->_('Beneficiary Alias'),
						// 					   'sortable' => true),
						'benef_type'  => array('field' => 'BENEFICIARY_TYPE',
											   'label' => $this->language->_('Type'),
											   'sortable' => true)
				);

		//get page, sortby, sortdir
		//$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','doc_no');
		$sortDir = $this->_getParam('sortdir','asc');

		//validate parameters before passing to view and query
		//$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;

		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';

		//$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;

		if($this->_getParam('filter'))
		{
			$userId = $this->_getParam('user_id');
			//Zend_Debug::dump($this->_getAllParams());die;
			//echo("id = ".$userId);
			$this->view->user_id = $userId;

			$cektemp = $this->_db->select()
									->from(array('M'=>'T_BENEFICIARY_USER'),array('SUGGEST_ID'))
								->where('M.USER_ID =?',$userId)
								->where('M.CUST_ID =?',$this->_custIdLogin);
			$cektemp = $this->_db->fetchRow($cektemp);

			if($cektemp)
			{
				$this->view->disabled = 'disabled';
				$this->view->errtemp = '<br /><br /><i>*'.$this->language->_('No changes allowed for this record while awaiting approval for previous change').'</i>';
			}

			$select	= $this->_db->select()
								->from	(
											array('M'=>'M_BENEFICIARY_USER'),
											array(
													'BENEFICIARY_ID'=>'M.BENEFICIARY_ID'
												)
										)
								->where('M.USER_ID =?',$userId);
			//die($select);
			$arr = $this->_db->fetchAll($select);
			$in = "(";
			$x = 0;
			foreach($arr as $row)
			{
				if($x==1){$in .= ",";}
				$in .= "'".$row['BENEFICIARY_ID']."'";
				$x=1;
			}
			$in .= ")";
			if($x == 0)
			{
				$in = null;
			}
			//die($select);
		}

		if($this->_getParam('submit') && !$cektemp)
		{
			$filters = array	(
								'user_id'		=> 	array('StringTrim','StripTags'),
								'benef'		=> 	array('StringTrim','StripTags'),
							);
			$clause = "SUGGEST_STATUS = '3'";

			$validators = array	(
									'user_id' 			=> 	array	(
																		'NotEmpty',
																		array('Db_NoRecordExists',array('table'=>'T_BENEFICIARY_USER',
																										'field'=>'USER_ID',
																										// 'include' => array('field' => 'CUST_ID','value' => $this->_custIdLogin),
																										// 'exclude' => array('field' => 'SUGGEST_STATUS','value' => '3')
																										'exclude' => "CUST_ID = '".$this->_custIdLogin."' and SUGGEST_STATUS != '3'"
																										)),
																		'messages' => array	(
																								$this->language->_('Error: User ID has not been choosen yet.'),
																								$this->language->_('Error: No changes allowed for this record while awaiting approval for previous change.'),
																							)
																	),
									'benef' 			=> 	array	(
																		new Zend_Validate_GreaterThan(0),
																		'messages' => array	(
																								$this->language->_('Error: Beneficiary Account Has not been Choosen yet.'),
																							)
																	),

								);
			$postBenef_id	= $this->getRequest()->getParam('benef_id');
			$postBenef_acc 	= $this->getRequest()->getParam('benef_acc');
			//Zend_Debug::dump($validators);die;
			$params['user_id'] = $this->_getParam('user_id');

			foreach ($postBenef_id as $key => $value)
			{
				if($postBenef_id[$key]==0)
				{
					unset($postBenef_id[$key]);
					unset($postBenef_acc[$key]);
				}
			}

			if($postBenef_id == null)
			{
				$params['benef'] = 0;
			}
			else
			{
				$params['benef'] = 1;
			}
			//Zend_Debug::dump($params);die;
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$params,$this->_optionsValidator);

			if($zf_filter_input->isValid())
			{
				try
				{
					$now = Zend_Date::now()->toString("d-MMM-yyyy");

					$param = array 	(
								"SUGGEST_USER" =>$this->_userIdLogin ,
								"CUST_ID" =>$this->_custIdLogin ,
								"USER_ID" => $params['user_id'],
								"SUGGEST_DATE" => new Zend_Db_Expr('now()'),
								"SUGGEST_STATUS" => 0,
							);
					//Zend_Debug::dump($param);die;
					$select = $this->_db->insert('T_BENEFICIARY_USER',$param);
					//die("id = ".$select);
					$masterId = $this->_db->fetchOne ("SELECT @@identity");
					$no =1;

					foreach ($postBenef_id as $key => $value)
					{
						$param = array 	(
									"USER_ID" => $params['user_id'],
									"BENEFICIARY_ID" => $postBenef_acc[$key],
									"SUGGEST_ID" => $masterId,
								);

						$select = $this->_db->select()
											->from(	array('TB' => 'TEMP_BENEFICIARY_USER'),
													array('SUGGEST_ID'))
											->where('TB.USER_ID = ?' , $SUGGEST_ID);

						$this->_db->insert('TEMP_BENEFICIARY_USER',$param);
						$no++;
					}

					Application_Helper_General::writeLog('BMBU',"Add Pairing Beneficiary for User=".$this->_userIdLogin."; for Company= ".$params['user_id']);

					$this->setbackURL('/'.$this->_request->getModuleName().'/manage/index/');
					$this->_redirect('/notification/submited/index');
				}
				catch(Exception $e)
				{
					$this->_db->rollBack();
				}

			}
			else
			{
				$this->view->error 			= true;
				$errors 					= $zf_filter_input->getMessages();

				$this->view->user_idErr 	= (isset($errors['user_id']))? $errors['user_id'] : null;
				$this->view->user_id 		= $params['user_id'];

				$this->view->benefErr 		= (isset($errors['benef']))? $errors['benef'] : null;
			}
		}


		if($in != null && $this->_getParam('user_id'))
		{//echo("in = $in");die;
			$sqlA = "SELECT 
							BANK_NAME,
							BENEFICIARY_ACCOUNT,
							BENEFICIARY_ID,
							BENEFICIARY_NAME,
							CURR_CODE,
							BENEFICIARY_ALIAS,
							BENEFICIARY_TYPE,
							1 AS 'CHECK'
					FROM 	M_BENEFICIARY
					WHERE	CUST_ID = ".$this->_db->quote($this->_custIdLogin).
					"AND		BENEFICIARY_ID IN".$in;

			$sqlB = "SELECT 
							BANK_NAME,
							BENEFICIARY_ACCOUNT,
							BENEFICIARY_ID,
							BENEFICIARY_NAME,
							CURR_CODE,
							BENEFICIARY_ALIAS,
							BENEFICIARY_TYPE,
							0 AS 'CHECK'
					FROM 	M_BENEFICIARY
					WHERE	CUST_ID = ".$this->_db->quote($this->_custIdLogin).
					"AND	BENEFICIARY_ID NOT IN".$in;

			$select = $this->_db->select()->distinct()->union(array($sqlA, $sqlB));
			//Zend_Debug::dump($select->query()->fetchAll());die;
			//die($select);
			$this->view->check = true;
		}
		else
		{
			$select = $this->_db->select()
					   ->from(array('M_BENEFICIARY'));
			$select->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin));
			$this->view->check = false;
		}
		$select->order($sortBy.' '.$sortDir);
		//echo $select;die;
		$arr = $select->query()->fetchAll();
		$this->view->data = $arr;
		//Zend_Debug::dump($arr);die;
		$this->paging($arr);
		$this->view->fields = $fields;
		
		$app = Zend_Registry::get('config');
 		$appBankname = $app['app']['bankname'];
		$this->view->app_bank = $appBankname;
		Application_Helper_General::writeLog('BMBU',"Manage Linkage User");
	}
}
