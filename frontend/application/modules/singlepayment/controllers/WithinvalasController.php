<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/SinglePayment.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'General/CustomerUser.php';
require_once 'Service/Token.php'; //added new
require_once 'SGO/Helper/AES.php';
require_once 'Crypt/AESMYSQL.php';
class singlepayment_WithinvalasController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti

	public function indexAction()
	{
		$futureDate = date('d/m/Y', strtotime('+1 days', strtotime(date('Y-m-d')))); 
		
				
		$set = new Settings();
		$beneficiary_acct_keyin = $set->getSetting('beneficiary_acct_keyin');
		$this->view->beneficiary_acct_keyin = $beneficiary_acct_keyin;

		$this->view->TransferDate = '1';
		
		$tranferdatetype = $this->_request->getParam('tranferdatetype');
		$this->view->tranferdatetype = $tranferdatetype;

		$date_val	= date('d/m/Y');
		$PS_EFDATE_VAL = $this->_request->getParam('PS_EFDATE');
		$this->view->paymentDate = $PS_EFDATE_VAL;
		
		//update tambahan periodic - begin
		$PS_PERIODIC_EVERY = $this->_request->getParam('PERIODIC_EVERY');
		$PS_PERIODIC_EVERY_DATE = $this->_request->getParam('PERIODIC_EVERYDATE');
		$PS_ENDDATE_VAL = ($this->_request->getParam('PS_ENDDATEPERIODIC'))?$this->_request->getParam('PS_ENDDATEPERIODIC'):date('d/m/Y');
		$this->view->endDatePeriodic = $PS_ENDDATE_VAL;	
		//update tambahan periodic - end
		
		if ($PS_EFDATE_VAL == $date_val){
			$todaytranfer = $this->language->_('Today Transfer');
			$this->view->paymentType = $todaytranfer;
		}
		else{
			$pendingfuturedate = $this->language->_('Future Date Transfer');
			$this->view->paymentType = $pendingfuturedate;
		}

		//update tambahan periodic - begin
		$jenisTransfer =  $this->_request->getParam('tranferdatetype');
		$jenisPeriodic =  $this->_request->getParam('tranferdateperiodictype');
		
		if ($jenisTransfer == '1'){
			$todaytranfer = $this->language->_('Today Transfer');
			$this->view->paymentType = $todaytranfer;
		}elseif ($jenisTransfer == '2'){
			$pendingfuturedate = $this->language->_('Future Date Transfer');
			$this->view->paymentType = $pendingfuturedate;
		}elseif ($jenisTransfer == '3'){
			$periodictranfer = $this->language->_('Periodic Transfer');
			$this->view->paymentType = $periodictranfer;
			
			if ($jenisPeriodic == '5'){
				$this->view->periodicValue = $PS_PERIODIC_EVERY;
			}elseif ($jenisPeriodic == '6'){
				$this->view->periodicValue = $PS_PERIODIC_EVERY_DATE;
			}
		}
		//update tambahan periodic - end

		$select3 = $this->_db->select()
					 ->from(array('C' => 'M_USER'));
		$select3->where("USER_ID = ".$this->_db->quote($this->_userIdLogin));
		$data2 = $this->_db->fetchRow($select3);
		
		$this->view->userId		= $data2['USER_ID'];
		$this->view->usermobilephone	= $data2['USER_MOBILE_PHONE'];
// 		$this->view->tokentype = $data2['TOKEN_TYPE'];
// 		$this->view->tokenIdUser = $data2['TOKEN_ID'];
// 		$tokenIdUser = $data2['TOKEN_ID'];
// 		$tokenType = $data2['TOKEN_TYPE'];

		//added new hard token
// 		$HardToken = new Service_Token($this->_custIdLogin, $this->_userIdLogin, $tokenIdUser);
// 		$challengeCode = $HardToken->generateChallengeCode();
// 		$this->view->challengeCode = $challengeCode;

		$challengeCodeSub = substr($challengeCode, 0,2);
		$this->view->challengeCodeReq = $challengeCodeSub;

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}
    	}

    	$USE_CONFIRM_PAGE = true;
    	$isResultPage 	  = false;
    	$payReff 		  = '';
    	
    	$periodicEveryArr = array(
    								'1' => $this->language->_('Monday'), 
    								'2' => $this->language->_('Tuesday'), 
    								'3' => $this->language->_('Wednesday'), 
    								'4' => $this->language->_('Thursday'), 
    								'5' => $this->language->_('Friday'), 
    								'6' => $this->language->_('Saturday'), 
    								'7' => $this->language->_('Sunday'),);
		$periodicEveryDateArr = range(1,31);

    	// Set variables needed in view
		$settings = new Application_Settings();
		$ccyList  = $settings->setCurrencyRegistered();	
		$lldIdenticalArr  	= $settings->getLLDDOMIdentical();
		$lldRelationshipArr = $settings->getLLDDOMRelationship();
		$lldCategoryArr = $settings->getLLDDOMCategory();
		// Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
// 		print_r($ccyList);die;
		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr 	  = $CustomerUser->getAccounts();
		
		$model = new purchasing_Model_Purchasing();
	
		$purposeArr = $model->getTranspurpose();

		$purposeList = array(''=>'-- Select Transaction Purpose --');
		foreach ($purposeArr as $key => $value ){
			
			$purposeList[$value['CODE']] = $value['DESCRIPTION'];
		}

// 		print_r($purposeList);
// 		die;
		$identyArr = $model->getIdenty();
//	print_r($data2);die;
		$identyList = array();
		foreach ($identyArr as $key => $value ){
			// 			print_r($purposeArr);
			$identyList[$value['sm_key2']] = $value['sm_text1'];
		}
		
		
		$transactorArr = $model->getTransactor();
		$transRelationList = array();
		foreach ($transactorArr as $key => $value ){
			// 			print_r($purposeArr);
			$transRelationList[$value['sm_key2']] = $value['sm_text1'];
		}
// 		print_r($lldRelationshipArr);die;
		
		$this->view->TransactorArr = $lldRelationshipArr;
		$this->view->IdentyArr = $lldIdenticalArr;
		$this->view->TransPurposeArr = $purposeList;
		$this->view->CategoryArr = $lldCategoryArr;
		
		$typeOfSelect = array(
										'd' => 'Day(s)',
										'm' => 'Month(s)',
										'y' => 'Year(s)',
									);

		$filter 		= new Application_Filtering();
		// $PS_NUMBER 		= $filter->filter($this->_getParam('PS_NUMBER'), "PS_NUMBER");


		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');  
	     $password = $sessionNamespace->token; 
	     // $this->view->token = $sessionNamespace->token;


		$AESMYSQL = new Crypt_AESMYSQL();
		$PS_NUMBER 			= urldecode($filter->filter($this->_getParam('PS_NUMBER'), "PS_NUMBER"));
		$PS_NUMBER = $AESMYSQL->decrypt($PS_NUMBER, $password);

		// $PS_EFDATE 		= Application_Helper_General::convertDate($this->getCurrentDate(),'dd/MM/yyyy');
		$PS_EFDATE 		= date('d/m/Y');
		$isConfirmPage 	= $this->_getParam('confirmPage');
		$pdf 	= $this->_getParam('pdf');

		if($this->_getParam('process') == 'back'){
			Zend_Session::namespaceUnset('TW');
		}
		$sessionNamespace 					= new Zend_Session_Namespace('TW');
		$process 	= $this->_getParam('process');

		$submitBtn 	= ($this->_request->isPost() && $process == "submit")? true: false;

		if (!empty($PS_NUMBER) && !$this->_request->isPost())
    	{
    		$paramList = array("WA" 			=> false,
							   "ACCOUNT_LIST" 	=> $this->_accountList,
							   "_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
							 );
    		$select   = $CustomerUser->getPayment($paramList, false);	// not distinct, cause text cannot be selected as DISTINCT, it's not comparable
			$select->where('P.PS_NUMBER = ?' , (string) $PS_NUMBER);

			$select->columns(array(	"tra_message"			=> "T.TRA_MESSAGE",
									"TRA_ADDMESSAGE"				=> "T.TRA_ADDITIONAL_MESSAGE",
									"acbenef_email"			=> "T.BENEFICIARY_EMAIL",
									"acbenef_address"		=> "T.BENEFICIARY_ADDRESS",
									"acbenef_citizenship"	=> "T.BENEFICIARY_CITIZENSHIP", //WNI WNA W / R
									"acbenef_resident"		=> "T.BENEFICIARY_RESIDENT",
									"bank_name"				=> "T.BENEFICIARY_BANK_NAME",
									"bank_city"				=> "T.BENEFICIARY_BANK_CITY",
									"clr_code"				=> "T.CLR_CODE",
									"lld_desc"				=> "T.LLD_DESC",
									"lld_code"				=> "T.LLD_CODE",
									"transfer_type"			=> "T.TRANSFER_TYPE",
									"acbenef_category"		=> "T.BENEFICIARY_CATEGORY",
									"acbenef_identity_type" => "T.BENEFICIARY_ID_TYPE",
									"acbenef_identity_num"	=> "T.BENEFICIARY_ID_NUMBER",
									"acbenef_address1"		=> "T.BENEFICIARY_BANK_ADDRESS1",
									"acbenef_address2"		=> "T.BENEFICIARY_BANK_ADDRESS2",
									"acbenef_pob"			=> "T.POB_NUMBER",
									"acbenef_country"		=> "T.BENEFICIARY_BANK_COUNTRY",
									"lld_identity"			=> "T.LLD_IDENTITY",
									"lld_trans_purpose"		=> "T.LLD_TRANSACTION_PURPOSE",
									"lld_trans_rel"			=> "T.LLD_TRANSACTOR_RELATIONSHIP"
									//"acbenef_bankname"		=> "T.BENEFICIARY_ACCOUNT_NAME",
									//"acbenef_alias"			=> "T.BENEFICIARY_ALIAS_NAME",
							  	   )
							 );

			$pslipData 			= $this->_db->fetchRow($select);
			//print_r($pslipData);die;
			if (!empty($pslipData))
			{
				$PS_SUBJECT 		= $pslipData['paySubj'];
				// $PS_EFDATE  		= $pslipData['efdate'];
				$PS_EFDATE  		= date("d/m/Y",strtotime($pslipData['efdate']));
				

				$TRA_AMOUNT  		= Application_Helper_General::displayMoney($pslipData['amount']);
				$TRA_MESSAGE 		= $pslipData['tra_message'];
				$TRA_ADDMESSAGE 			= $pslipData['TRA_ADDMESSAGE'];
				$ACCTSRC 			= $pslipData['accsrc'];
				$ACCTSRC_CCY		= $pslipData['accsrc_ccy'];
				
				$ACBENEF  			= $pslipData['acbenef'];
				$BENEF_CURRENCY  		= $pslipData['acbenef_ccy'];
				$ACBENEF_CCY  		= $pslipData['acbenef_ccy'];

				$CURR_CODE			= $pslipData['ccy'];
				$ACBENEF_BANKNAME	= $pslipData['acbenef_bankname'];
				$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
								$sessionNameConfrim->benefAcct = $ACBENEF;
								$sessionNameConfrim->sourceAcct = $ACCTSRC;
								$sessionNameConfrim->ACBENEF_BANKNAME = $ACBENEF_BANKNAME;
								$sessionNameConfrim->traAmount = $TRA_AMOUNT;
				$ACBENEF_ALIAS  	= $pslipData['acbenef_alias'];
				$ACBENEF_EMAIL  	= $pslipData['acbenef_email'];
				$ACBENEF_ADDRESS  	= $pslipData['acbenef_address'];
				$BENEFICIARY_RESIDENT = $pslipData['acbenef_resident'];
				$BENEFICIARY_CITIZENSHIP = $pslipData['acbenef_citizenship'];
				//print_r($BENEFICIARY_RESIDENT);die;
				//$ACBENEF_CITIZENSHIP = ($ACBENEF_CITIZENSHIP_CODE == "R")? 'RESIDENT' : 'NON RESIDENT';
				//$ACBENEF_NATIONALITY = ($ACBENEF_NATIONALITY_CODE == "W")? 'WNI' : 'WNA';
				$BANK_NAME			= $pslipData['bank_name'];
				$BANKNAME			= $pslipData['bank_name'];
				$BANK_CITY			= $pslipData['bank_city'];
				$CLR_CODE			= $pslipData['clr_code'];
				$LLD_DESC			= $pslipData['lld_desc'];
				$LLD_CODE			= $pslipData['lld_code'];
				$SWIFT_CODE			= $pslipData['SWIFT_CODE'];
				$PS_STATUS 			= $pslipData['PS_STATUS'];
				$PS_TYPE  			= $pslipData['PS_TYPE'];
				$TRANSFER_TYPE  	= $pslipData['transfer_type'];	// 1, 2

				$BENEFICIARY_CATEGORY = $lldCategoryArr[$pslipData['acbenef_category']];
				$BENEFICIARY_ID_TYPE = $pslipData['acbenef_identity_type'];
				$ACBENEF_IDENTITY_TYPE_DISP = $lldBeneIdentifArr[$BENEFICIARY_ID_TYPE];
				$BENEFICIARY_ID_NUMBER  = $pslipData['acbenef_identity_num'];
				
				$ACBENEF_BANK_ADD1 = $pslipData['acbenef_address1'];
				$ACBENEF_BANK_ADD2 = $pslipData['acbenef_address2'];
				$ACBENEF_POBNUMB   = $pslipData['acbenef_pob'];
				$country_code   = $pslipData['acbenef_country'];

				$LLD_IDENTITY  = $pslipData['lld_identity'];

				$LLD_TRANSACTOR_RELATIONSHIP = $pslipData['lld_trans_rel'];
				//print_r($pslipData);die;
				$LLD_TRANSACTION_PURPOSE = $pslipData['lld_trans_purpose'];

				//get country name
			/*	$getCountry = $this->_db->select()
								->from(array('M_COUNTRY'))
								->where('COUNTRY_CODE = ?', $country_code);

				$getCountry = $this->_db->fetchRow($getCountry);
				$ACBENEF_COUNTRY = $getCountry['COUNTRY_NAME'];


*/
//print_r($PS_TYPE);die;				

				// Payment Status is not Request Repair
				if ($PS_STATUS != $this->_paymentstatus["code"]["requestrepair"])
				{
					$this->_helper->getHelper('FlashMessenger')->addMessage("F");
					$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Payment Status has changed.");
					$this->_redirect('/paymentworkflow/requestrepair/index');
				}
				elseif ($PS_TYPE != $this->_paymenttype["code"]["within"])
				{
					$this->_helper->getHelper('FlashMessenger')->addMessage("F");
					$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Payment Type is invalid.");
					$this->_redirect('/paymentworkflow/requestrepair/index');
				}

				$trfTypeArr = array_combine($this->_transfertype["code"], $this->_transfertype["desc"]);	// Array([2] => SKN, [1] => RTGS)
				$trfTypeArr += array('ONLINE' => 'ONLINE');
				$TRANSFER_TYPE = $trfTypeArr[$TRANSFER_TYPE];	// SKN, RTGS

				

				
			}
			else	// ps_number is invalid, or not belong to customer
			{
				$this->_helper->getHelper('FlashMessenger')->addMessage("F");
				$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Payment Ref# is invalid.");
				$this->_redirect('/paymentworkflow/requestrepair/index');
			}
    	}

		if (!empty($PS_NUMBER) && !$this->view->hasPrivilege('PRPP'))
		{
			$this->_helper->getHelper('FlashMessenger')->addMessage("F");
			$this->_helper->getHelper('FlashMessenger')->addMessage("Error: You don't have privilege to repair payment.");
			$this->_redirect('/paymentworkflow/requestrepair/index');
		}

		//tambahan pentest
		$tranferdatetype = $this->_request->getParam('tranferdatetype');
		$this->view->tranferdatetype = $tranferdatetype;
		
		$this->view->PS_EFDATEFUTURE = $futureDate;
		
		$sessionNameRand = new Zend_Session_Namespace('confirmTransactRand');
			
		$this->view->kurs = Application_Helper_General::convertDisplayMoney($kurs);
		
		if($this->_request->isPost() )
		{
			$sessionNameRand = new Zend_Session_Namespace('confirmTransactRand');
				
			if($this->_getParam('randomTransact') == $sessionNameRand->randomTransact){
//					echo'masuk';
				try {
					//die;
			
					//print_r($this->_request->getParam('ACCTSRC'));
					$passwordRand = $sessionNameRand->randomTransact; //yang hidden param random
					//print_r($passwordRand);
					$blocksize = 256;  // can be 128, 192 or 256
					$ACBENEF =  SGO_Helper_AES::decrypt( $this->_request->getParam('ACBENEF'), $passwordRand, $blocksize );
					$ACCTSRC =  SGO_Helper_AES::decrypt( $this->_request->getParam('ACCTSRC'), $passwordRand, $blocksize );
					$TRA_AMOUNT =  SGO_Helper_AES::decrypt( $this->_request->getParam('TRA_AMOUNT'), $passwordRand, $blocksize );
					$responseCodeReq =  SGO_Helper_AES::decrypt( $this->_request->getParam('responseCodeReq'), $passwordRand, $blocksize );
					$responseCodeEncrpty =  SGO_Helper_AES::decrypt( $this->_request->getParam('responseCode'), $passwordRand, $blocksize );
					//print_r($ACCTSRC);
					//die;
					
				} 
				catch (Exception $e) {
					//die('gere');
					$ACBENEF = '';
					$ACCTSRC = '';
					$TRA_AMOUNT = '';
					$responseCodeReq = '';
					$responseCodeEncrpty = '';
				}
				
				$TRANS_PURPOSECODE = $this->_getParam('TRANSPURPOSE_SELECT');
				//print_r($TRANS_PURPOSE);die;
				$this->view->error = false;
				$LLD_IDENTITY = $this->_request->getParam('LLD_IDENTITY_SELECT');
				$LLD_TRANSACTOR_RELATIONSHIP = $this->_request->getParam('LLD_TRANSACTOR_RELATIONSHIP_SELECT');
// 				print_r($LLD_IDENTITY);
// 				print_r($LLD_TRANSACTOR_RELATIONSHIP);die;
				$LLD_TRANSACTION_PURPOSE = $this->_request->getParam('TRANSPURPOSE_SELECT');
				$BENEFICIARY_CITIZENSHIP = $this->_request->getParam('ACBENEF_CITIZENSHIP');
				$BENEFICIARY_RESIDENT = $this->_request->getParam('ACBENEF_NATION');
				$BENEFICIARY_CATEGORY = $this->_request->getParam('ACBENEF_CATEGORY');
				$BENEFICIARY_ID_TYPE  = $this->_request->getParam('ACBENEF_IDENTY');
				$BENEFICIARY_ID_NUMBER 	= $this->_request->getParam('ACBENEF_IDENTYNUM');
				$PS_SUBJECT = $this->_request->getParam('PS_SUBJECT');
			
				$TRANSPURPOSE 			= $this->_request->getParam('TRANSPURPOSE_SELECT');
				$ACBENEF_CURRENCY1		= $this->_request->getParam('ACBENEF_CURRENCY');
				$ACBENEF_CURRENCY		= $this->_request->getParam('CURR_CODE_SEL');
				$BENEF_CURRENCY			= $this->_request->getParam('ACBENEF_CURRENCY');
				
				$ACCTSRC_CURRECY		= $this->_request->getParam('ACCTSRC_CCY');
				$ACCTSRC_CURRECY2		= $this->_request->getParam('ACCTSRC_CCY2');
				
				$this->view->ACCTSRC_CCY = $ACCTSRC_CURRECY;
				$this->view->ACCTSRC_CCY2 = $ACCTSRC_CURRECY2;
				$this->view->CURR_CODE_SEL = $ACBENEF_CURRENCY; 
				
				/*if($resultKurs['ResponseCode']=='00'){

					foreach ($resultKurs['DataList'] as $key => $val){
						if($val['Kurs']==$ACBENEF_CURRENCY){
							$kurs = $val['Sell'];
// 							print_r($val);
						}else if($ACBENEF_CURRENCY == 'IDR'){
							$kurs = $val['Buy'];
						}
					}
// 					die;
				}else{
					$kurs = 'N/A';
				}
				$this->view->kurs = Application_Helper_General::convertDisplayMoney($kurs);*/
				
				$payDateType 		= $filter->filter($this->_request->getParam('payDateType')	, "SELECTION");
				$PS_EFDATE 			= $filter->filter($this->_request->getParam('PS_EFDATE')		, "PS_DATE");
				$TRA_AMOUNT 		= $filter->filter($TRA_AMOUNT		, "AMOUNT");
				$TRA_MESSAGE 		= $filter->filter($this->_request->getParam('TRA_MESSAGE')		, "TRA_MESSAGE");
				$TRA_ADDMESSAGE 			= $filter->filter($this->_request->getParam('TRA_ADDMESSAGE')		, "TRA_ADDMESSAGE");
				$ACCTSRC 			= $filter->filter($ACCTSRC			, "ACCOUNT_NO");
				$ACBENEF 			= $filter->filter($ACBENEF			, "ACCOUNT_NO"); 
				
				$ACBENEF_BANKNAME 	= $filter->filter($this->_request->getParam('ACBENEF_BANKNAME')	, "ACCOUNT_NAME");

				$ACBENEF_ALIAS 		= $filter->filter($this->_request->getParam('ACBENEF_ALIAS')	, "ACCOUNT_ALIAS");
				$ACBENEF_EMAIL 		= $filter->filter($this->_request->getParam('ACBENEF_EMAIL')	, "EMAIL");
				$ACBENEF_CCY 		= $filter->filter($this->_request->getParam('CURR_CODE_SEL')		, "SELECTION");
				$PERIODIC_EVERY 	= $filter->filter($this->_request->getParam('PERIODIC_EVERY')	, "SELECTION");
				$PERIODIC_EVERYDATE 	= $filter->filter($this->_request->getParam('PERIODIC_EVERYDATE')	, "SELECTION");
				$TrfDateType 		= $filter->filter($this->_request->getParam('tranferdatetype')	, "SELECTION");				
				$TrfPeriodicType 	= $filter->filter($this->_request->getParam('tranferdateperiodictype')	, "SELECTION");
				$PS_ENDDATE		= $filter->filter($this->_request->getParam('PS_ENDDATEPERIODIC')		, "PS_DATEPERIODIC");
//				$AGREEMENT		= $filter->filter($this->_request->getParam('agreement')		, "agreement");
				
// 				$LLD_TRANSACTOR_RELATIONSHIP		= $filter->filter($this->_request->getParam('LLD_TRANSACTOR_RELATIONSHIP')		, "LLD_TRANSACTOR_RELATIONSHIP");
// 				$LLD_IDENTITY		= $filter->filter($this->_request->getParam('LLD_IDENTITY')		, "LLD_IDENTITY");
				$AGREEMENT		= $filter->filter($this->_request->getParam('agreement')		, "agreement");
					

				$challengeCodeReq2 	= $filter->filter($this->_request->getParam('challengeCodeReq2'), "challengeCodeReq2");
				$challengeCodeReq 	= $filter->filter($this->_request->getParam('challengeCodeReq')	, "challengeCodeReq");
				
				$responseCodeReq 	= $filter->filter($responseCodeReq	, "responseCodeReq");
				$random = str_pad(mt_rand(0, 9), 2, '0', STR_PAD_LEFT);
				//$this->view->challengeCodeReq1 = substr($this->_request->getParam('challengeCodeReq'), '2','4');

//				$this->view->challengeCodeReq1 = $random.substr($challengeCodeReq, -4);
				$this->view->challengeCodeReq1 = substr($challengeCodeReq, -4);
			
				$PS_EVERY_PERIODIC_UOM 	= $TrfPeriodicType; //trim($this->_request->getParam('PS_EVERY_PERIODIC_UOM'));
				//$PS_EVERY_PERIODIC 		= trim($this->_request->getParam('PS_EVERY_PERIODIC'.$PS_EVERY_PERIODIC_UOM));
				$START_DATE 			= $filter->filter(date('d/m/Y')	, "PS_DATE");
				//$EXPIRY_DATE 			= $filter->filter($this->_request->getParam('EXPIRY_DATE')	, "PS_DATE");
				$EXPIRY_DATE 			= $filter->filter($this->_request->getParam('PS_ENDDATEPERIODIC')	, "PS_DATE");

				
				$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);
				if($this->_getParam('process') == 'back'){
					$PS_NUMBER = '';
					$PS_SUBJECT = '';
					$TRA_AMOUNT = '';
					$TRA_MESSAGE = '';
					$TRA_ADDMESSAGE = '';
					$TRA_MESSAGE_len = '';
					$TRA_ADDMESSAGE_len = '';
					$ACCTSRC = '';
					$ACCTSRC_view = '';
					$ACBENEF = '';
					$ACBENEF_BANKNAME = '';
					$ACBENEF_ALIAS = '';
					$ACBENEF_EMAIL = '';
					$CURR_CODE = '';
					$CHARGES_AMT = '';
					$CHARGES_CCY = '';
					$PS_EVERY_PERIODIC = '';
					$PS_EVERY_PERIODIC_UOM = '';
					$START_DATE = '';
					$EXPIRY_DATE = '';
					$PERIODIC_EVERY = '0';
					$PERIODIC_EVERYDATE = '0';
					$TrfDateType = '1';
					$TrfPeriodicType = '0';
				}
//die;
			if(empty($sessionNamespace->psNumber))
			{
				// post submit payment
				if ($submitBtn)
				{
					//die;
						$errorMsg = '';
						if( $payDateType == 1 ){ // TRANSFER NOW
							$PS_EFDATE			= date('d/m/Y');
						}
						else if( $payDateType == 2 ){ // TRANSFER AT
							$PS_EFDATE			= $filter->filter($this->_request->getParam('PS_EFDATE')		, "PS_DATE");								
							
						}
							
					
					//die;
					$filter->__destruct();
					unset($filter);
					//cek date kosong
					if(!$PS_EFDATE){
						$errorMsg	 			= 'Payment Date can not be left blank.';
						$this->view->error 		= true;
						$this->view->ERROR_MSG	= $errorMsg;
					}
					else{

						$validateDateFormat = new Zend_Validate_Date(array('format' => $this->_dateDisplayFormat));
						if(!$validateDateFormat->isValid($PS_EFDATE))
						{
							$errorMsg = 'Invalid Payment Date Format. Payment Date Format must be dd/MM/yyyy';
							$this->view->error 		= true;
							$this->view->ERROR_MSG	= $errorMsg;
						}
						else {
							//new kebutuhan pentest (revisi security)
							if($isConfirmPage != 1){
								$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
								$sessionNameConfrim->benefAcct = $ACBENEF;
								$sessionNameConfrim->sourceAcct = $ACCTSRC;
								
								$sessionNameConfrim->traAmount = $TRA_AMOUNT_num;
							}
							else{
								
								$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
								$sessionNameConfrim->benefAcct;	
								$sessionNameConfrim->sourceAcct;	
								$sessionNameConfrim->traAmount;
							}
							
							$accsrcData = $model->getBeneficiaryByAccount($sessionNameConfrim->sourceAcct);
							
							
							$paramPayment = array(	"CATEGORY" 					=> "SINGLE PB",
													"FROM" 						=> "F",				// F: Form, I: Import
													"PS_NUMBER"					=> $PS_NUMBER,
													"PS_SUBJECT"				=> '',
													"PS_EFDATE"					=> $PS_EFDATE,
													"_dateFormat"				=> $this->_dateDisplayFormat,
													"_dateDBFormat"				=> $this->_dateDBFormat,
													"_addBeneficiary"			=> $this->view->hasPrivilege('BADA'),		// privi BADA (Add Beneficiary)
													"_beneLinkage"				=> $this->view->hasPrivilege('BLBU'),		// privi BLBU (Linkage Beneficiary User)
													"_createPB"					=> $this->view->hasPrivilege('CRSP'),		// privi CRSP (Create Single Payment)
													"_createDOM"				=> false,									// cannot create DOM trx
													"_createREM"				=> false,									// cannot create REM trx
												 );
							$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
							$AccArr 	  = $CustomerUser->getAccounts(array("ACCT_TYPE" => array('10','20')));	// show acc in IDR only
							//print_r($ACBENEF_CURRENCY);die;
							//die;
							$srcData = $this->_db->select()
							->from(array('M_CUSTOMER_ACCT'),array('ACCT_NO','ACCT_TYPE'))
							->where('ACCT_NO = ?', $ACCTSRC)
							->limit(1)
							;
							$acsrcData = $this->_db->fetchRow($srcData);
							if(!empty($acsrcData)){
								$accsrc = $acsrcData['ACCT_NO'];
								$accsrctype = $acsrcData['ACCT_TYPE'];
							}
							//print_r($AccArr);die('here');
							$svcInquiry = new Service_Inquiry($this->_userIdLogin,$accsrc,$accsrctype);
							$resultKursEx = $svcInquiry->rateInquiry();
								
							//rate inquiry for display
							$kurssell = '';
							$kurs = '';
							$book = '';				
							if($resultKursEx['ResponseCode']=='00'){
								$kursList = $resultKursEx['DataList'];
								$kurssell = '';
								$kurs = '';
								
								foreach($kursList as $row){
							
											if($row["currency"] == 'USD'){
												//print_r($row);die;												$row["buy"] = str_replace(',','',$row["buy"]);
												$row["sell"] = str_replace(',','',$row["sell"]);
												$row["book"] = str_replace(',','',$row["book"]);							
												$kurssell = $row["sell"];
												$book = $row["book"];
												if($ACCTSRC_CURRECY=='IDR'){
												$kurs = $row["sell"];
												}else{
												$kurs = $row["buy"];

												}
											}
								}		
								
							}else{
								
									$errMessage 	= 'Invalid Account';
									//$errorMsg 		= (!empty($errorMsg)) ? $errorMsg : $validate->getErrorMsg();
									//$errorTrxMsg 	= $validate->getErrorTrxMsg();	// array
									//$validate->__destruct();
									//unset($validate);
									//if(!empty($errorMsg) || !empty($errorTrxMsg))//{
									//	$errMessage 	= (!empty($errorMsg))? $errorMsg: reset(reset(reset($errorTrxMsg)));
								
									$this->view->error 		= true;
									$this->view->ERROR_MSG	= $errMessage;
								
									// 								if(empty($responseCodeReq)){}else{
									// 									$this->view->ERROR_MSG_TOKEN = 'Error : Invalid Token';
									// 								}
								
							}
							//print_r($book);die;
							$this->view->kurs = Application_Helper_General::convertDisplayMoney($kurs);
							if($kurs != 'N/A'){
// 								print_r($kurs);die;
								
									$plain_kurs = str_replace(',','', $kurs);
									$traAmountEq 						= $sessionNameConfrim->traAmount*$plain_kurs;
								
								
							}else{
								$traAmountEq 						= 'N/A';
							}

							$paramTrxArr[0] = array("TRANSFER_TYPE" 			=> "PB",
													"TRA_AMOUNT" 				=> $sessionNameConfrim->traAmount,
													"TRA_AMOUNTEQ" 				=> $traAmountEq,
													"TRA_MESSAGE" 				=> $TRA_MESSAGE,
													"TRA_ADDMESSAGE" 				=> $TRA_ADDMESSAGE,
													"ACCTSRC" 					=> $sessionNameConfrim->sourceAcct,
													"ACBENEF" 					=> $sessionNameConfrim->benefAcct,
													"ACBENEF_CCY" 				=> $ACCTSRC_CURRECY,
													"ACBENEF_CCY2" 				=> $ACCTSRC_CURRECY2,
													"ACBENEF_BANKNAME" 			=> &$ACBENEF_BANKNAME,
													
												// for Beneficiary data, except (bene CCY and email), must be passed by reference
													"ACBENEF_ALIAS" 			=> &$ACBENEF_ALIAS,
													"ACBENEF_EMAIL" 			=> $ACBENEF_EMAIL,
													"LLD_IDENTITY"				=> $LLD_IDENTITY,
													"LLD_TRANSACTOR_RELATIONSHIP"				=> $LLD_TRANSACTOR_RELATIONSHIP,
													"LLD_TRANSACTION_PURPOSE"				=> $LLD_TRANSACTION_PURPOSE,
													"BENEFICIARY_CITIZENSHIP"	 =>$BENEFICIARY_CITIZENSHIP,
													"BENEFICIARY_RESIDENT"   	 => $BENEFICIARY_RESIDENT,
													"BENEFICIARY_CATEGORY"		 => $BENEFICIARY_CATEGORY,
													"BENEFICIARY_ID_TYPE"		 => $BENEFICIARY_ID_TYPE,
													"BENEFICIARY_ID_NUMBER"		 => $BENEFICIARY_ID_NUMBER,
													"BENEFICIARY_CCY"			 => $BENEF_CURRENCY,
													"ACBENEF_CURRENCY"			 => $ACBENEF_CURRENCY,
													"ACCTSRC_ID_TYPE"			 => $accsrcData['0']['BENEFICIARY_ID_TYPE'],
													"ACCTSRC_ID_NUMBER"			 => $accsrcData['0']['BENEFICIARY_ID_NUMBER'],
													"ACCTSRC_CITIZENSHIP"		 => $accsrcData['0']['BENEFICIARY_CITIZENSHIP'],
													"ACCTSRC_RESIDENT'"			 => $accsrcData['0']['BENEFICIARY_RESIDENT'],
													"ACCTSRC_CATEGORY"			 => $accsrcData['0']['BENEFICIARY_CATEGORY'],
													"AGREEMENT"					 => $AGREEMENT,
																			 );
// 							print_r($paramTrxArr);die;
							if($isConfirmPage != 1){
								$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
								$sessionNameConfrim->ACBENEF_BANKNAME = $paramTrxArr[0]['ACBENEF_BANKNAME'];
							}
							else{
								$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
								$sessionNameConfrim->ACBENEF_BANKNAME;	
							}
							
							$paramSettingID = array('range_futuredate', 'auto_release_payment');
							
							$settings->setSettings(null, $paramSettingID);	// Zend_Registry => 'APPSETTINGS'

							$validate   = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin);
							$resAcct = array();
							$validate->setFlagConfirmPage(($isConfirmPage == 1)?TRUE:FALSE); //tujuan untuk set supaya jangan manggil inquiry lg di page ke 2
 							//print_r($paramTrxArr);die;
							$resultVal	= $validate->checkCreateValas($paramPayment, $paramTrxArr, $resAcct);
							//Zend_Debug::dump($resultVal); die;							
 						//	print_r($resultVal);die;
							if(!empty($resultVal)){
						
								$accsrcDetail['ACCTSRC_CITIZENSHIP'] = $resultVal['ACSRC']['Citizenship'];
								$accsrcDetail['ACCTSRC_RESIDENT'] = $resultVal['ACSRC']['National'];
								$accsrcDetail['ACCTSRC_CATEGORY'] = $resultVal['ACSRC']['Category'];
								$accsrcDetail['ACCTSRC_ID_NUMBER'] = $resultVal['ACSRC']['IdentificationNumber'];
								$accsrcDetail['ACCTSRC_ID_TYPE'] = $resultVal['ACSRC']['IdentificationType'];
								// 								print_r($resultVal['BENE']);die;
								$BENEFICIARY_CITIZENSHIP = $resultVal['BENE']['0']['Citizenship'];
								$BENEFICIARY_RESIDENT = $resultVal['BENE']['0']['National'];
								
								$BENEFICIARY_CATEGORY = $resultVal['BENE']['0']['Category'];
								$BENEFICIARY_ID_NUMBER = $resultVal['BENE']['0']['IdentificationNumber'];
								$BENEFICIARY_ID_TYPE = $resultVal['BENE']['0']['IdentificationType'];
								
								$KURS_CCY = $resultVal['KURS_CCY'];
							}

							$sourceAccountType = $resAcct['productType'];
								
								if($isConfirmPage != 1){
									$this->view->sourceAcctTypeGet = $sourceAccountType;
								}
								else{
									$sourceAcctTypeGet 		= $this->_getParam('sourceAcctType');
									$this->view->sourceAcctTypeGet = $sourceAcctTypeGet;
								}
								
// 							Zend_Debug::dump($validate->getErrorMsg());
							//validasi hard token page 1 -- begin
							//die;
							//if($validate->isError() === false && $this->view->error === false )	// payment data is valid
							if($validate->isError() === false &&  $this->view->error === false)	// payment data is valid - new added
							{	
								//die;
								$payment 		= $validate->getPaymentInfo();

								if ($USE_CONFIRM_PAGE == true && $isConfirmPage == false)
								{
									
									// notif kurs
									if ($ACCTSRC_CURRECY2 == "USD" && $BENEF_CURRENCY == "USD"){
										
									}else{
										
										// get e-rate for notif kurs
										/*$svcInquiry = new Service_Inquiry();
										$resultKurs		= $svcInquiry->rateInquiry();
								 		//print_r($resultKurs);die;
										
										if($resultKurs['ResponseCode']=='00'){
								 			
											//$this->view->kurslist = json_encode($resultKurs['DataList']);
											//$kurslist = json_encode($resultKurs['DataList']['USD']);
																																	
											foreach ($resultKurs['DataList'] as $key => $val){
												if ($val['currency'] == "USD"){
													$kursbuy = $val['buy'];
													$kurssell = $val['sell'];
												}
												
											}
										}else{
											$kursbuy = 'N/A';
											$kurssell = 'N/A';
										}
										
															
									    if($ACCTSRC_CURRECY2=="USD" && $BENEF_CURRENCY == 'IDR'){
									    	$kurs_ccy = $kursbuy;
										}else if($ACCTSRC_CURRECY2=="IDR" && $BENEF_CURRENCY == 'USD'){
											$kurs_ccy = $kurssell;
										}*/											
										$KURS_CCY = Application_Helper_General::convertDisplayMoney($kurs);
										//$KURS_CCY = money_format('%i',(int)$kurs);
										//print_r($kurs);die;
										echo "
									      <script type=\"text/javascript\">
									      	var textdesc = \"e-Rate to be used for this transaction is IDR ".Application_Helper_General::convertDisplayMoney($kurs)." \";
	    									var textalert = textdesc;
									       	alert(textalert);		    										
									      </script>
									     ";
										// end get e-rate for notif kurs
										
									}// notif kurs
									
									$isConfirmPage 	= true;
									$ACCTSRC_CCY   	= $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["CCY_ID"];
									$ACCTSRC_NAME   = $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["ACCT_NAME"];
									$ACCTSRC_ALIAS  = $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["ACCT_ALIAS"];
									$ACCTSRC_TYPE   = $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["ACCT_TYPE"];
									$ACCTSRC_view 	= Application_Helper_General::viewAccount($sessionNameConfrim->sourceAcct, $ACCTSRC_CCY, $ACCTSRC_NAME, $ACCTSRC_ALIAS, $ACCTSRC_TYPE);

									$validate->__destruct();
									unset($validate);

									require_once 'General/Charges.php';
									$trfType		= "PB";
									$chargesObj 	= Charges::factory($this->_userIdLogin, $trfType);
									$paramCharges 	= array("accsrc" => $sessionNameConfrim->sourceAcct, "transferType" => $trfType);
									$chargesAMT 	= $chargesObj->getCharges($paramCharges);
									$chargesCCY 	= $ACCTSRC_CCY;
								}
								else
								{									
									
									$validate->__destruct();
									unset($validate);
									
									$ACCTSRC_CCY   	= $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["CCY_ID"];
									$ACCTSRC_NAME   = $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["ACCT_NAME"];
									$ACCTSRC_ALIAS  = $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["ACCT_ALIAS"];
									$ACCTSRC_TYPE   = $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["ACCT_TYPE"];
									$ACCTSRC_view 	= Application_Helper_General::viewAccount($sessionNameConfrim->sourceAcct, $ACCTSRC_CCY, $ACCTSRC_NAME, $ACCTSRC_ALIAS, $ACCTSRC_TYPE);

									
									$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
									
									$param = array();
									$param['PS_SUBJECT'] 					= '';
									$param['PS_CCY'] 						= $ACBENEF_CURRENCY;
									$param['PS_EFDATE'] 					= Application_Helper_General::convertDate($PS_EFDATE, $this->_dateDBFormat, $this->_dateDisplayFormat);
									$param['EXPIRY_DATE'] 					= Application_Helper_General::convertDate($EXPIRY_DATE, $this->_dateDBFormat, $this->_dateDisplayFormat);
//									$param['TRA_AMOUNT'] 					= $TRA_AMOUNT_num;
									$param['TRA_AMOUNT'] 					= $sessionNameConfrim->traAmount;
									$param['TRA_MESSAGE'] 					= $TRA_MESSAGE;
									$param['TRA_ADDMESSAGE'] 					= $TRA_ADDMESSAGE;
									$param['SOURCE_ACCOUNT_NAME'] 			= $ACCTSRC;
									$param['SOURCE_ACCOUNT'] 				= $sessionNameConfrim->sourceAcct;
									$param['SOURCE_ACCOUNT_CCY'] 			= $ACCTSRC_CURRECY;
									$param['LLD_IDENTITY'] 					= (isset($LLD_IDENTITY))		? $LLD_IDENTITY		: '';
									$param['LLD_TRANSACTION_PURPOSE'] 		= (isset($LLD_TRANSACTION_PURPOSE))		? $LLD_TRANSACTION_PURPOSE		: '';
									$param['LLD_TRANSACTOR_RELATIONSHIP'] 	= (isset($LLD_TRANSACTOR_RELATIONSHIP))		? $LLD_TRANSACTOR_RELATIONSHIP		: '';
									
									$param['BENEFICIARY_ACCOUNT'] 			= $sessionNameConfrim->benefAcct; //ambil dari session
									$param['BENEFICIARY_ACCOUNT_CCY'] 		= $BENEF_CURRENCY;
									$param['BENEFICIARY_ACCOUNT_NAME'] 		= $ACBENEF_BANKNAME;
									$param['BENEFICIARY_ALIAS_NAME'] 		= $ACBENEF_ALIAS;
									$param['BENEFICIARY_EMAIL'] 			= $ACBENEF_EMAIL;
									$param['BOOK_RATE_SELL'] 				= $book;
									$param['_addBeneficiary'] 				= $paramPayment["_addBeneficiary"];
									$param['_beneLinkage'] 					= $paramPayment["_beneLinkage"];
									$param['_priviCreate'] 					= 'CRSP';
									if($kurs != 'N/A'){
										$plain_kurs = str_replace(',','', $kurs);
// 										print_r($plain_kurs);die;
										$traAmountEq 						= $sessionNameConfrim->traAmount*$plain_kurs;
									}else{
										$traAmountEq 						= 'N/A';
									}
									
									$param['TRA_AMOUNTEQ'] 					= $traAmountEq;
									$param['sourceAccountType'] 			= $sourceAcctTypeGet;
									//print_r($param);die;
							//token validasi here added agung
//print_r($responseCodeEncrpty);die;
//							if ($filter->isValid()){
				
// 							if(!empty($responseCodeEncrpty)){	
// 								if($tokenType == '1')
// 								{
// 									$responseCode = $filter->getEscaped('responseCode');
// 									//echo'smstoken';
// 								}
// 								elseif($tokenType == '2')
// 								{
// 									$responseCode = $responseCodeEncrpty;
// 									//echo'hardtoken';
// 								}
// 								elseif($tokenType == '3')
// 								{
// 									//echo'mobiletoken';
// 								}
// 								else{}

								$userData = $this->_db->select()
								->from(array('M_USER'),array('USER_ID','USER_MOBILE_PHONE'))
								->where('USER_ID = ?', $this->_userIdLogin)
								->limit(1)
								;
								$userData = $this->_db->fetchRow($userData);
								$userMobilePhone = trim($userData['USER_MOBILE_PHONE']);
								//if (isset($userMobilePhone) && !(empty($userMobilePhone))){

									$Settings = new Settings();
									$message = $Settings->getSetting('token_confirm_message');

									$trans = array("[Response_Code]" => $responseCode);
									$message = strtr($message, $trans);

// 									$token = new Service_Token(NULL,$this->_userIdLogin);
// 									$token->setMsisdn($userData['USER_MOBILE_PHONE']);
// 									$token->setMessage($message);

// 									if($tokenType == '1'){ //sms token
// 										$res = $token->confirm();
// 										$resultToken = $res['ResponseCode'] == '0000';
// 									}
// 									elseif($tokenType == '2'){ //hard token
// 										$resHard = $HardToken->validateOtp($responseCode);
// 										$resultToken = $resHard['ResponseCode'] == '0000';

// 										//set user lock token gagal
// 										$CustUser = new CustomerUser($this->_userIdLogin);
// 										if ($resHard['ResponseCode'] != '0000'){
// 											$tokenFailed = $CustUser->setFailedTokenMustLogout($resHard['ResponseTO']);
// 											if ($tokenFailed)
// 												$this->_forward('home');
// 										}
// 									}
// 									elseif($tokenType == '3'){ //mobile token
// 									}
// 									else{}
									//Zend_Debug::dump($resultToken);
									//Zend_Debug::dump($resHard['ResponseCode']);
// 									if ($resultToken)
// 									{
										try
										{
											if( $tranferdatetype == 3 ){
												// Insert T_PERIODIC //update tambahan periodic
												$param['PS_PERIODIC_TRANSFER'] = 1;

												$START_DATE = join('-',array_reverse(explode('/',$START_DATE)));
												$EXPIRY_DATE = join('-',array_reverse(explode('/',$EXPIRY_DATE)));
												$insertPeriodic = array(
														'PS_EVERY_PERIODIC' 	=> (int)$PERIODIC_EVERY_VAL,
														'PS_PERIODIC_NUMBER'	=> $this->generateTransactionID(),
														'PS_EVERY_PERIODIC_UOM' => $PS_EVERY_PERIODIC_UOM, // 1: daily, 2: weekly, 3: monthly, 4: yearly, 5: every day of, 6: every date of
														'PS_PERIODIC_STARTDATE' => $START_DATE,
														'PS_PERIODIC_ENDDATE'	=> $EXPIRY_DATE,
														'PS_PERIODIC_NEXTDATE'	=> $NEXT_DATE,
														'PS_PERIODIC_STATUS' 	=> 2,	// 2 INPROGRESS KALO BELUM BERAKHIR, 1 COMPLETE KALO SUDAH HABIS END DATE, 0 PERIODIC INI DI CANCEL
														'USER_ID' 				=> $this->_userIdLogin,
														'PS_CREATED' 			=> new Zend_Db_Expr("GETDATE()"),
													);
//												Zend_Debug::dump($insertPeriodic); die;												
												$this->_db->insert('T_PERIODIC', $insertPeriodic);
												$psPeriodicID =  $this->_db->lastInsertId();
												$param['PS_PERIODICID'] = $psPeriodicID;
												
												// Insert T_PERIODIC_DETAIL //update tambahan periodic
												$select	= $this->_db->select()
													->from(
														array('A'	 		=> 'M_USER_ACCT'),
														array(
															'ACCT_NAME' 	=> 'A.ACCT_NAME',
															'ACCT_ALIAS' 	=> 'A.ACCT_ALIAS_NAME',
															'ACCT_CCY' 	=> 'A.CCY_ID',
														)
													)
													->where("A.USER_ID 			 = ?", $this->_userIdLogin)
													->where("A.ACCT_NO		 	 = ?", $param['SOURCE_ACCOUNT'])
												;
												$accsrc = $this->_db->fetchRow($select);
										
												$sourceAccountName 		=  $accsrc['ACCT_NAME'];
												$sourceAccountAliasName =  $accsrc['ACCT_ALIAS'];
												$sourceAccountCCY 		=  $accsrc['ACCT_CCY'];
												
//												Zend_Debug::dump($param); die;
												$insertPeriodicDetail = array(
														'PS_PERIODIC' 				=> $psPeriodicID,
														'SOURCE_ACCOUNT' 			=> $param['SOURCE_ACCOUNT'], // 1: daily, 2: weekly, 3: monthly, 4: yearly, 5: every day of, 6: every date of
														'SOURCE_ACCOUNT_CCY' 		=> $sourceAccountCCY,
														'SOURCE_ACCOUNT_NAME' 		=> $sourceAccountName,
														'SOURCE_ACCOUNT_TYPE' 		=> $param['sourceAccountType'],
														'SOURCE_ACCOUNT_BANK_CODE' 	=> "",
														'SOURCE_ACCOUNT_BANK_NAME' 	=> "",
														'BENEFICIARY_ACCOUNT' 		=> $param['BENEFICIARY_ACCOUNT'],
														'BENEFICIARY_ACCOUNT_CCY' 	=> $param['BENEFICIARY_ACCOUNT_CCY'],
														'BENEFICIARY_ACCOUNT_NAME' 	=> $param['BENEFICIARY_ACCOUNT_NAME'],
														'BENEFICIARY_EMAIL' 		=> $param['BENEFICIARY_EMAIL'],
														'BENEFICIARY_BANK_CODE' 	=> "",
														'BENEFICIARY_BANK_NAME' 	=> "",
														'TRA_AMOUNT' 				=> $param['TRA_AMOUNT'],
														'TRA_MESSAGE' 				=> $param['TRA_MESSAGE'],														
														'TRANSFER_TYPE' 			=> 0,	// 0 : Inhouse, 1: RTGS, 2: SKN														
													);
												$this->_db->insert('T_PERIODIC_DETAIL', $insertPeriodicDetail);
											}

											//{
												//$SinglePayment = new SinglePayment($PS_NUMBER, $this->_userIdLogin);
												$SinglePayment = new SinglePayment($PS_NUMBER,$this->_custIdLogin,$this->_userIdLogin);
												// if (!empty($PS_NUMBER))
												// {	$SinglePayment->isRepair = true;	}

												if(!empty($PS_NUMBER))
												{
													$SinglePayment->isRepair = true;
												}
													$acct_data = $model->getSourceAccount($sessionNameConfrim->sourceAcct);
													$bene_data = $model->getBeneficiaryByAccount($param['BENEFICIARY_ACCOUNT']);
// 													print_r($bene_data);die;
													if(empty($resultVal['BENE']['0']['accountName'])){
														// 			echo 'here';
														// 			die;ACCTSRC_RESIDENT
														$param['BENEFICIARY_CITIZENSHIP'] = $this->_request->getParam('ACBENEF_CITIZENSHIP_CODE');
														$param['BENEFICIARY_RESIDENT'] = $this->_request->getParam('ACBENEF_NATION_CODE');
														$param['BENEFICIARY_CATEGORY'] = $this->_request->getParam('ACBENEF_CATEGORY_CODE');
														$param['BENEFICIARY_ID_TYPE']  = $this->_request->getParam('ACBENEF_IDENTY');
														$param['BENEFICIARY_ID_NUMBER'] = $this->_request->getParam('ACBENEF_IDENTYNUM');
														// 			print_r($BENEFICIARY_ID_NUMBER);
														$param['ACCT_CITIZENSHIP'] = $this->_request->getParam('ACCTSRC_CITIZENSHIP_CODE');
														
														$param['ACCT_RESIDENT'] = $this->_request->getParam('ACCTSRC_RESIDENT_CODE');
														$param['ACCT_CATEGORY'] = $this->_request->getParam('ACCTSRC_CATEGORY_CODE');
														$param['ACCT_ID_TYPE'] = $this->_request->getParam('ACCTSRC_ID_TYPE');
														$param['ACCT_ID_NUMBER'] = $this->_request->getParam('ACCTSRC_ID_NUMBER');
														// 			print_r($accsrcDetail);
													
													}
													$param['PS_SUBJECT'] = $PS_SUBJECT;
													$param['ACCT_NAME'] = $acct_data['0']['ACCT_NAME'];
													$param['ACCT_CCY'] = $acct_data['0']['CCY_ID'];
													$param['USER_BANK_CODE'] = $bene_data['0']['USER_BANK_CODE'];
													$param['PLAIN_KURS'] = $plain_kurs;
													$resWs = array();
 													//print_r($param);die;
													$result = $SinglePayment->createPaymentWithinValas($param, $resWs);

													$ns = new Zend_Session_Namespace('FVC');
													$ns->backURL = '/'.$this->view->modulename.'/'.$this->view->controllername.'/index';
//													$ACCTSRC_view 	= Application_Helper_General::viewAccount($ACCTSRC, $ACCTSRC_CCY, $ACCTSRC_NAME, $ACCTSRC_ALIAS, $ACCTSRC_TYPE);
													$ACCTSRC_view 	= Application_Helper_General::viewAccount($param['SOURCE_ACCOUNT'], $ACCTSRC_CCY, $ACCTSRC_NAME, $ACCTSRC_ALIAS, $ACCTSRC_TYPE);
													
													$sessionNamespace->lldidenty 	= $LLD_IDENTITY;
													$sessionNamespace->lldrelation 	= $LLD_TRANSACTOR_RELATIONSHIP;
													
													$sessionNamespace->psNumber 	= $result;
													$sessionNamespace->ACCTSRC_view 	= $ACCTSRC_view;
													$sessionNamespace->isConfirmPage 	= $isConfirmPage = true;
													$sessionNamespace->isResultPage 	= $isResultPage = true;
													$payReff = $PS_NUMBER = $result;
													/*Zend_Debug::dump($payReff);
													Zend_Debug::dump($result);
													die;*/
													
													/* if($PS_EFDATE_VAL == $date_val){
														if($resWs['ResponseCode'] == '9999' || $resWs['ResponseCode'] == 'XT'){
															$this->view->messageSuccess = $this->language->_('Your Transaction Is Suspect, Please Contact Customer Care');
														}
														elseif($resWs['ResponseCode'] == '0000' || $resWs['ResponseCode'] == '00'){
															$this->view->messageSuccess = $this->language->_('Your Transfer Is Success');
														}
														else{
															$this->view->messageSuccess = $this->language->_('Your Transaction Is Failed');
														}
													}
													else{ //pending future date
														$this->view->messageSuccess = $this->language->_('Your Transfer Is Success');
													} */
													if ($result === true){
														$this->_redirect('/notification/success/index');
													}else{ //// TODO: what to do, if failed create payment
														//$this->_redirect('/notification/error/index');
													}
												

										}
										catch(Exception $e)
										{
											// Zend_Debug::dump($e);die;
											//$result = 'Code : '.$e->getCode().', Message : '.$e->getMessage();
											//echo "tes 123: ".$result;
										}
// 									}else{
// 										$errMessage 	= 'Invalid Token';

// 										$this->view->error 		= true;
// 										$this->view->ERROR_MSG	= $errMessage;
// 										$this->view->responseCode = $responseCode;
// 									}

									/* FORCE LOGOUT */
									$locked = $this->_db->select()
									->from(array('M_USER'),array('USER_ISLOCKED'))
									->where('USER_ID = ?', $this->_userIdLogin)
									->limit(1);
									$locked = $this->_db->fetchRow($locked);
									$locked = $locked['USER_ISLOCKED'];

									if (isset($locked) && $locked == '1'){
										$CustomerUser->forceLogout();
										$this->redirect('/default/index/logout');
									}
								/*}
								else{
									$errMessage 	= 'Record Not Found';
									$this->view->error 		= true;
									$this->view->ERROR_MSG	= $errMessage;
									$this->view->responseCode = $responseCode;
								}*/
// 							}
// 							else{
// 								$errMessage 	= 'Response Token cannot be left blank';
// 								$this->view->error 		= true;
// 								$this->view->ERROR_MSG	= $errMessage;
// 								$this->view->responseCode = $responseCode;
// 							}

						   }
						}
						else
							{
								$errMessage 	= '';
								$errorMsg 		= (!empty($errorMsg)) ? $errorMsg : $validate->getErrorMsg();
								$errorTrxMsg 	= $validate->getErrorTrxMsg();	// array
								$validate->__destruct();
								unset($validate);
								if(!empty($errorMsg) || !empty($errorTrxMsg))//{
									$errMessage 	= (!empty($errorMsg))? $errorMsg: reset(reset(reset($errorTrxMsg)));

								$this->view->error 		= true;
								$this->view->ERROR_MSG	= $errMessage;
								
// 								if(empty($responseCodeReq)){}else{
// 									$this->view->ERROR_MSG_TOKEN = 'Error : Invalid Token';
// 								}
							}
						}
					}
				}
				else{
					//tambahn pentest
					$this->_redirect('/singlepayment/withinvalas');
					$isConfirmPage 	= false;
				}
			}
			else{
				
				$ACCTSRC_view 	= $sessionNamespace->ACCTSRC_view;
				$payReff = $PS_NUMBER = $sessionNamespace->psNumber;
				$isConfirmPage 	= $sessionNamespace->isConfirmPage;
				$isResultPage 	= $sessionNamespace->isResultPage;
			}
			
			}
			else{
//				echo "<script type='text/javascript'>alert('Session expired')</script>";
				$this->view->sessionExpired = $this->language->_('Session expired');
				$this->_redirect('/singlepayment/withinvalas');
			}
		
			
			$randomTransact = str_pad(mt_rand(0, 99999999), 8, '0', STR_PAD_LEFT);
			$this->view->randomTransact = $randomTransact;	
			$sessionNameRand->randomTransact = $randomTransact;	
			
		//tambahn pentest
			if($tranferdatetype =='1'){
				$this->view->PS_EFDATE 			= $PS_EFDATE;
			}
			elseif ($tranferdatetype =='3'){
				$PS_EFDATE = date('d/m/Y', strtotime('+1 days', strtotime(date('Y-m-d')))); 
				$this->view->PS_EFDATEFUTURE = $PS_EFDATE;
				$PS_EFDATE = $PS_EFDATE_ORI;//						
			}
			else{
				$this->view->PS_EFDATEFUTURE = $PS_EFDATE;						
			}
		}
		else
		{
			$randomTransact = str_pad(mt_rand(0, 99999999), 8, '0', STR_PAD_LEFT);
			$this->view->randomTransact = $randomTransact;	
			$sessionNameRand->randomTransact = $randomTransact;
			
			Zend_Session::namespaceUnset('TW');
		}

		$TRA_MESSAGE_len = (isset($TRA_MESSAGE))? strlen($TRA_MESSAGE): 0;
		$TRA_ADDMESSAGE_len 	 = (isset($TRA_ADDMESSAGE))  ? strlen($TRA_ADDMESSAGE)  : 0;

		$TRA_MESSAGE_len = 140 - $TRA_MESSAGE_len;
		$TRA_ADDMESSAGE_len 	 = 200 - $TRA_ADDMESSAGE_len;
		//Zend_Debug::dump($AccArr);die;
		$this->view->AccArr 			= $AccArr;
		
		$this->view->ccyArr 			= $ccyList;
		$this->view->typeOfSelect 			= $typeOfSelect;
		$this->view->PS_NUMBER 			= (isset($PS_NUMBER))			? $PS_NUMBER			: '';
		$this->view->PS_SUBJECT 		= (isset($PS_SUBJECT))			? $PS_SUBJECT			: '';
		$this->view->PS_EFDATE 			= (isset($PS_EFDATE))			? $PS_EFDATE			: '';

		// $this->view->PS_EFDATE 			= (isset($PS_EFDATE))			? $PS_EFDATE			: $this->getCurrentDate();
//		$this->view->TRA_AMOUNT 		= (isset($TRA_AMOUNT))			? $this->_request->getParam('TRA_AMOUNT'): '';
		$traamount = Application_Helper_General::displayMoney($sessionNameConfrim->traAmount);
		$this->view->TRA_AMOUNT 		= (isset($traamount))			? $traamount: '';
		
		if($kurs!='N/A'){
// 			print_r($sessionNameConfrim->traAmount);
// 			print_r($kurs);die;
			$plain_kurs = str_replace(',','', $kurs);
			$eqamount = $sessionNameConfrim->traAmount*$plain_kurs;
			$this->view->eqamount = Application_Helper_General::displayMoney($eqamount);
		}else{
			$eqamount = 'N/A';
			$this->view->eqamount = 'N/A';
		} 
		$this->view->TRA_AMOUNT_PLAIN 		= (isset($sessionNameConfrim->traAmount))			? $sessionNameConfrim->traAmount: '';
		
		$this->view->TRA_MESSAGE 		= (isset($TRA_MESSAGE))			? $TRA_MESSAGE			: '';
		$this->view->TRA_ADDMESSAGE 			= (isset($TRA_ADDMESSAGE))			? $TRA_ADDMESSAGE			: '';
		$this->view->TRA_MESSAGE_len	= $TRA_MESSAGE_len;
		$this->view->TRA_ADDMESSAGE_len		= $TRA_ADDMESSAGE_len;

//		$this->view->ACCTSRC 			= (isset($ACCTSRC))				? $ACCTSRC				: '';
		$this->view->ACCTSRC 			= (isset($sessionNameConfrim->sourceAcct))				? $sessionNameConfrim->sourceAcct				: $ACCTSRC;
		$this->view->ACCTSRC_view		= (isset($ACCTSRC_view))		? $ACCTSRC_view			: '';
//		$this->view->ACBENEF 			= (isset($ACBENEF))				? $ACBENEF				: '';
		$this->view->ACBENEF 			= (isset($sessionNameConfrim->benefAcct))				? $sessionNameConfrim->benefAcct				: '';
		$this->view->BENEFICIARY_ACCOUNT_CCY 		= (isset($ACBENEF_ALIAS))		? $ACBENEF_ALIAS		: '';
		if(!empty($ACCTSRC)){
		
	       $accData = $this->_db->select()
	       ->from('M_CUSTOMER_ACCT')
	       ->where('ACCT_NO = ?', $ACCTSRC)
	       ->where('CUST_ID = ?', (string)$this->_custIdLogin);
	       
	       $accData = $this->_db->fetchRow($accData);
	      // print_r($accData);die;
	       $ACCTSRC_CCY = $accData['CCY_ID'];
	   	
		$this->view->ACCTSRC_CCY = (isset($ACCTSRC_CCY))				? $ACCTSRC_CCY				: '';
		$this->view->ACCTSRC_CCY2 = (isset($ACCTSRC_CCY))				? $ACCTSRC_CCY				: '';
		}
		$this->view->ACBENEF_BANKNAME	= (isset($sessionNameConfrim->ACBENEF_BANKNAME))	? $sessionNameConfrim->ACBENEF_BANKNAME		: '';
		$this->view->ACBENEF_ALIAS 		= (isset($ACBENEF_ALIAS))		? $ACBENEF_ALIAS		: '';
		$this->view->ACBENEF_EMAIL 		= (isset($ACBENEF_EMAIL))		? $ACBENEF_EMAIL		: '';
		$this->view->CURR_CODE 			= (isset($ACBENEF_CCY))			? $ACBENEF_CCY			: '';
		$this->view->CHARGES_AMT 		= (isset($chargesAMT))			? Application_Helper_General::displayMoney($chargesAMT)	: '';
		$this->view->CHARGES_CCY 		= (isset($chargesCCY))			? $chargesCCY			: '';
		$this->view->payDateType 		= 2;
		$this->view->PERIODIC_EVERY 	= (isset($PERIODIC_EVERY))		? $PERIODIC_EVERY		: '0';
		$this->view->PERIODIC_EVERYDATE = (isset($PERIODIC_EVERYDATE))	? $PERIODIC_EVERYDATE	: '0';
		$this->view->TrfDateType 		= (isset($TrfDateType))			? $TrfDateType			: '1';
		$this->view->TrfPeriodicType 	= (isset($TrfPeriodicType))		? $TrfPeriodicType		: ''; //$TrfPeriodicType;
		
		$this->view->LLD_IDENTITY 	= (isset($LLD_IDENTITY))		? $LLD_IDENTITY		: '';
		if(!empty($sessionNamespace->lldrelation)){
			$LLD_TRANSACTOR_RELATIONSHIP = $sessionNamespace->lldrelation;
		}
		
		if(!empty($sessionNamespace->lldidenty)){
			$LLD_IDENTITY = $sessionNamespace->lldidenty;
		}
		
		if(!empty($LLD_IDENTITY)){
		//$LLD_IDENTITYTXT = $model->getTransactorById($LLD_IDENTITY,'IDENTICAL');
		//$LLD_TRANSACTOR_RELATIONSHIPTXT = $model->getTransactorById($LLD_TRANSACTOR_RELATIONSHIP,'RELATIONSHIP');
		}

		//$this->view->LLD_IDENTITYTXT 	= (isset($LLD_IDENTITYTXT))		? $LLD_IDENTITYTXT['0']['sm_text1']		: '';
		$this->view->LLD_TRANSACTOR_RELATIONSHIP 	= (isset($LLD_TRANSACTOR_RELATIONSHIP))		? $LLD_TRANSACTOR_RELATIONSHIP		: '';
		
		//$this->view->LLD_TRANSACTOR_RELATIONSHIPTXT 	= (isset($LLD_TRANSACTOR_RELATIONSHIPTXT))		? $LLD_TRANSACTOR_RELATIONSHIPTXT['0']['sm_text1']		: '';
		$prebenemodel 			= new predefinedbeneficiary_Model_Predefinedbeneficiary();
// 		if(!empty($LLD_TRANSACTION_PURPOSE)){
// 			$purposedata = $prebenemodel->getPurposeCode($LLD_TRANSACTION_PURPOSE);
// 			if(!empty($purposedata)){
// 				$LLD_TRANSACTION_PURPOSE = $purposedata['0']['DESCRIPTION'];
// 			}
// 		}
		
		$this->view->LLD_HI 	=  'none';
		if($ACBENEF_CURRENCY =='USD'){
// 			print_r('here');die;
			if($sessionNameConfrim->traAmount>=10000){
				$this->view->LLD_HI  = '-';
// 				print_r('here');die;
			}else{
// 				print_r($sessionNameConfrim->traAmount);
// 				print_r('here1');
				$this->view->LLD_HI  = 'none';
			}
		}

	//	if(empty($resultVal['BENE']['0']['accountName'])){
		if(empty($BENEFICIARY_CITIZENSHIP)){
// 			echo 'here';
// 			die;ACCTSRC_RESIDENT
			$BENEFICIARY_CITIZENSHIP = $this->_request->getParam('ACBENEF_CITIZENSHIP');
			$BENEFICIARY_RESIDENT = $this->_request->getParam('ACBENEF_NATION');
			$BENEFICIARY_CATEGORY = $this->_request->getParam('ACBENEF_CATEGORY');
			$BENEFICIARY_ID_TYPE  = $this->_request->getParam('ACBENEF_IDENTY');
			$BENEFICIARY_ID_NUMBER 	= $this->_request->getParam('ACBENEF_IDENTYNUM');
			// 			print_r($BENEFICIARY_ID_NUMBER);
			$accsrcDetail['ACCTSRC_CITIZENSHIP'] = $this->_request->getParam('ACCTSRC_CITIZENSHIP');
			$accsrcDetail['ACCTSRC_RESIDENT'] = $this->_request->getParam('ACCTSRC_RESIDENT');
			$accsrcDetail['ACCTSRC_CATEGORY'] = $this->_request->getParam('ACCTSRC_CATEGORY');
			$accsrcDetail['ACCTSRC_ID_TYPE'] = $this->_request->getParam('ACCTSRC_ID_TYPE');
			$accsrcDetail['ACCTSRC_ID_NUMBER'] = $this->_request->getParam('ACCTSRC_ID_NUMBER');
 //			print_r($accsrcDetail);
//die;				
		}
		
// 		print_r($accsrcDetail);die;
		$this->view->LLD_TRANSACTION_PURPOSE 	= (isset($LLD_TRANSACTION_PURPOSE))		? $LLD_TRANSACTION_PURPOSE		: '';
		
		$this->view->ACBENEF_CITIZENSHIP_CODE 	= (isset($BENEFICIARY_CITIZENSHIP))		? $BENEFICIARY_CITIZENSHIP		: '';
		$this->view->ACBENEF_CATEGORY_CODE 	= (isset($BENEFICIARY_CATEGORY))		? $BENEFICIARY_CATEGORY		: '';
		$this->view->ACBENEF_NATION_CODE 	= $BENEFICIARY_RESIDENT;
			$temp_c = $accsrcDetail['ACCTSRC_CITIZENSHIP'];
			$temp_r = $accsrcDetail['ACCTSRC_RESIDENT'];
		
		if(!empty($temp_c)){
			//print_r($temp_c);
			if($temp_c=='R' || $temp_c=='Resident'){
				$accsrcDetail['ACCTSRC_RESIDENT'] = 'Resident';
			}else{
				$accsrcDetail['ACCTSRC_RESIDENT'] = 'Non Resident';
			}
		}else{
			$accsrcDetail['ACCTSRC_RESIDENT'] = '-';
		}
		
		if(!empty($temp_r)){
			if($temp_r=='W' || $temp_r=='WNI'){
				$accsrcDetail['ACCTSRC_CITIZENSHIP'] = 'WNI';
			}else{
				$accsrcDetail['ACCTSRC_CITIZENSHIP'] = 'WNA';
			}
		}else{
			$accsrcDetail['ACCTSRC_CITIZENSHIP'] = '-';
		}
//print_r($accsrcDetail);die;
// 		print_r($BENEFICIARY_RESIDENT);die;
		if(!empty($BENEFICIARY_CATEGORY)){
				
		
		}else{
			$BENEFICIARY_CATEGORY = '-';
		}
		

		$this->view->ACBENEF_IDENTY 	= (isset($BENEFICIARY_ID_TYPE))		? $BENEFICIARY_ID_TYPE		: '';
		
		$this->view->ACBENEF_IDENTYNUM 	= (isset($BENEFICIARY_ID_NUMBER))		? $BENEFICIARY_ID_NUMBER		: '';
// 		$this->view->ACBENEF_CURRENCY 	= (isset($ACBENEF_CURRENCY))		? $ACBENEF_CURRENCY		: '';
		$this->view->ACBENEF_CURRENCY 	= (isset($BENEF_CURRENCY))		? $BENEF_CURRENCY		: '';
		$this->view->TRANSPURPOSE 	= (isset($LLD_TRANSACTION_PURPOSE))		? $LLD_TRANSACTION_PURPOSE		: '';
//print_r($TRANS_PURPOSECODE);die;
		$model = new purchasing_Model_Purchasing();
		
		$purposeArr = $model->getTranspurpose();
		$purposeList = array(''=>'-- Select Transaction Purpose --');
		foreach ($purposeArr as $key => $value ){
			
			$purposeList[$value['CODE']] = $value['DESCRIPTION'];
		}
//print_r($purposeList);die;
		if(!empty($TRANS_PURPOSECODE))
			$this->view->LLD_TRANSACTION_PURPOSETEXT = $purposeList[$TRANS_PURPOSECODE];
		$this->view->TRANSPURPOSE_view 	= (isset($LLD_TRANSACTION_PURPOSE))		? $LLD_TRANSACTION_PURPOSE		: '';
		$settingObj = new Settings();
		$this->view->LIMITLLD		= $settingObj->getSetting("threshold_lld_remittance"	, 0);
		$this->view->LIMITLLDMONEY		= Application_Helper_General::displayMoney($settingObj->getSetting("threshold_lld_remittance"	, 0));
// 		print_r($accsrcDetail);die;
		$this->view->ACCTSRC_CITIZENSHIP_CODE 	= (isset($accsrcDetail['ACCTSRC_CITIZENSHIP']))		? $accsrcDetail['ACCTSRC_CITIZENSHIP']		: '';
		$this->view->ACCTSRC_RESIDENT_CODE 	= (isset($accsrcDetail['ACCTSRC_RESIDENT']))		? $accsrcDetail['ACCTSRC_RESIDENT']		: '';
		$this->view->ACCTSRC_CATEGORY_CODE 	= (isset($accsrcDetail['ACCTSRC_CATEGORY']))		? $accsrcDetail['ACCTSRC_CATEGORY']		: '';
		$temp_bc = $BENEFICIARY_CITIZENSHIP;
		$temp_br = $BENEFICIARY_RESIDENT;
		if(!empty($temp_br)){
		//print_r($temp_br);die;
			if($temp_br=='R' || $temp_br=='Resident' || $temp_br=='W'){
				$BENEFICIARY_RESIDENT = 'Resident';
			}else if($temp_br=='-'){
				$BENEFICIARY_RESIDENT = '-';
			}else{
				$BENEFICIARY_RESIDENT = 'Non Resident';
			}
		}else{
			$BENEFICIARY_RESIDENT = '-';
		}
 		//print_r($BENEFICIARY_RESIDENT);die;
		if(!empty($temp_bc)){
		//print_r($temp_bc);die;
 		//	echo 'here';
			if($temp_bc=='W' || $temp_bc=='WNI' || $temp_bc=='R'){
				$BENEFICIARY_CITIZENSHIP = 'WNI';
			}else if($temp_bc=='-'){
				$BENEFICIARY_CITIZENSHIP = '-';
			}else{
				$BENEFICIARY_CITIZENSHIP = 'WNA';
			}
		}else{
// 			echo 'here2';
			$BENEFICIARY_CITIZENSHIP = '-';
		}
		//die;
		if(!empty($accsrcDetail['ACCTSRC_CATEGORY'])){
			
			
			
		}else if($accsrcDetail['ACCTSRC_CATEGORY']=='-'){
			$accsrcDetail['ACCTSRC_CATEGORY'] = '';
		}else{
			$accsrcDetail['ACCTSRC_CATEGORY'] = '';
		}
				$this->view->ACBENEF_CITIZENSHIP 	= (isset($BENEFICIARY_CITIZENSHIP))		? $BENEFICIARY_CITIZENSHIP		: '';
		$this->view->ACBENEF_CATEGORY 	= (isset($BENEFICIARY_CATEGORY))		? $BENEFICIARY_CATEGORY		: '';
		$this->view->ACBENEF_NATION 	= $BENEFICIARY_RESIDENT;
		$this->view->ACCTSRC_ID_TYPE 	= (isset($accsrcDetail['ACCTSRC_ID_TYPE']))		? $accsrcDetail['ACCTSRC_ID_TYPE']		: '';
		$this->view->ACCTSRC_ID_NUMBER 	= (isset($accsrcDetail['ACCTSRC_ID_NUMBER']))		? $accsrcDetail['ACCTSRC_ID_NUMBER']		: '';
		$this->view->ACCTSRC_CITIZENSHIP 	= (isset($accsrcDetail['ACCTSRC_CITIZENSHIP']))		? $accsrcDetail['ACCTSRC_CITIZENSHIP']		: '';
		$this->view->ACCTSRC_RESIDENT 	= (isset($accsrcDetail['ACCTSRC_RESIDENT']))		? $accsrcDetail['ACCTSRC_RESIDENT']		: '';
		$this->view->ACCTSRC_CATEGORY 	= (isset($accsrcDetail['ACCTSRC_CATEGORY']))		? $accsrcDetail['ACCTSRC_CATEGORY']		: '';
		
		
		$this->view->periodicEveryArr  	= $periodicEveryArr;
		$this->view->periodicEveryDateArr  = $periodicEveryDateArr;
		
		
		
		if($this->_getParam('process') != 'back'){
			$this->view->PS_EVERY_PERIODIC1 		= (($this->_getParam('PS_EVERY_PERIODIC1')))			? $this->_getParam('PS_EVERY_PERIODIC1')			: 0;
			$this->view->PS_EVERY_PERIODIC5 		= (($this->_getParam('PS_EVERY_PERIODIC5')))			? $this->_getParam('PS_EVERY_PERIODIC5')			: 1;
			$this->view->PS_EVERY_PERIODIC6 		= (($this->_getParam('PS_EVERY_PERIODIC6')))			? $this->_getParam('PS_EVERY_PERIODIC6')			: 0;
			$this->view->payDateType 		= (isset($payDateType))			? $payDateType			: 2;
		}

		if(isset($PS_EVERY_PERIODIC_UOM)){ if($PS_EVERY_PERIODIC_UOM) $temps = $PS_EVERY_PERIODIC_UOM; };
		$this->view->PS_EVERY_PERIODIC_UOM 		= (isset($temps))	?  $temps : 1;
		$this->view->EXPIRY_DATE 		= (isset($EXPIRY_DATE))			? $EXPIRY_DATE			: '';

		if (isset($PS_NUMBER) && $pdf == 1)
		{
			$isConfirmPage = true;
			$isResultPage  = true;
			$payReff = $PS_NUMBER;
		}

		$this->view->confirmPage		= $isConfirmPage;
		$this->view->resultPage			= $isResultPage;
		$this->view->payReff			= $payReff;
		$this->view->useConfirmPage		= $USE_CONFIRM_PAGE;

		if (isset($PS_NUMBER) && $pdf == 1)
		{
			$outputHTML = "<tr><td>".$this->view->render($this->view->controllername.'/index.phtml')."</td></tr>";
			$this->_helper->download->pdf(null,null,null,'Payment',$outputHTML);
		}
		if($this->_request->getParam('printtrx') == 1){
//			$data = $this->_getParam('payReff');//$this->_dbObj->fetchAll($select);
            $this->_forward('printtrxvalas', 'index', 'widget', array('data_caption' => 'Transfer', 'data_header' => 'In House (Valas)'));
		} 
		Application_Helper_General::writeLog('CRSP','Transfer In House');

	}


	public function generateTransactionID(){

		$currentDate = date("Ymd");
		$seqNumber	 = strtoupper(Application_Helper_General::str_rand(5));
		$trxId = 'PR'.$currentDate.$this->_custIdLogin.$seqNumber;

		return $trxId;
	}
	 
}
