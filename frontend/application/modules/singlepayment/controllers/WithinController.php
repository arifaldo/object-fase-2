<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/SinglePayment.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/CustomerUser.php';
require_once 'CMD/Payment.php';
require_once 'CMD/Beneficiary.php';
require_once 'SGO/Extendedmodule/Google/GoogleAuthenticator.php';
require_once 'Crypt/AESMYSQL.php';
class singlepayment_WithinController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if (count($temp) > 1) {
			if ($temp[0] == 'F' || $temp[0] == 'S') {
				if ($temp[0] == 'F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = '';
				unset($temp[0]);
				foreach ($temp as $value) {
					if (!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}
		}

		
		$Settings = new Settings();
		$this->view->maxdate = $Settings->getSetting('range_futuredate');
		$thesoshold = $Settings->getSetting("threshold_lld_remittance", 		"0");
		$this->view->thesoshold = $thesoshold;
		$acco_no = $this->_request->getParam('ACCT_NO');
		$this->view->acco_no = $acco_no;


		// $this->view->hidetoken = true;

		$selectuser = $this->_db->select()
			->from(array('A' => 'M_USER'));
		$selectuser->where("A.CUST_ID = " . $this->_db->quote($this->_custIdLogin));
		// echo $selectuser;die;
		$this->view->dataact = $selectuser->query()->fetchAll();
		
		
		
		
		
		

		$selectccy = $this->_db->select()
			->from(array('A' => 'M_MINAMT_CCY'), array('CCY_ID'));
		// ->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin))
		// ->where("A.USER_LOGIN = ".$this->_db->quote($this->_userIdLogin))
		// ->GROUP("A.CCY_ID");
		// ->group('A.ACCT_NO')
		$this->view->dataccy = $selectccy->query()->fetchAll();


		$validate   = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin);

		$futureDate = date('d/m/Y', strtotime('+1 days', strtotime(date('Y-m-d'))));
		// var_dump($this->_custSameUser);die;
		if ($this->_custSameUser) {
			// echo 'here1';
			$this->view->token = true;
			$userOnBehalf = $this->_userIdLogin;
			$tokenIdUser = $this->_db->select()
				->from(
					array('M_USER'),
					array('TOKEN_ID', 'GOOGLE_CODE')
				)
				->where('USER_ID = ?', $userOnBehalf)
				->where('CUST_ID = ?', $this->_custIdLogin)
				->limit(1);
			$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
			$tokenIdUser = $this->_db->fetchRow($tokenIdUser);
			$tokenGoogle = $tokenIdUser['GOOGLE_CODE'];
			// var_dump($tokenIdUser);
			// var_dump($tokendata);
			$tokenIdUser = $tokenIdUser['TOKEN_ID'];

			$Token 			= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
			$challengeCode 	= $Token->generateChallengeCode();
			$this->view->userOnBehalf		= $userOnBehalf;
			$this->view->challengeCode		= $challengeCode;
			// var_dump($tokendata);
			if (!empty($tokenGoogle)) {
				// die('here');
				$this->view->googleauth = true;
			}
		}

		$this->view->googleauth = true;
		
		
		$tokenIdUser = $this->_db->select()
			->from(
				array('M_USER'),
				array('TOKEN_ID','TOKEN_TYPE')
			)
			->where('USER_ID = ?', $this->_userIdLogin)
			->where('CUST_ID = ?', $this->_custIdLogin)
			->limit(1);

		$tokenIdUser = $this->_db->fetchRow($tokenIdUser);
		//var_dump($userOnBehalf);
		//die('here');
		$tokenType = $tokenIdUser['TOKEN_TYPE'];
		if($tokenIdUser['TOKEN_TYPE'] == '6'){
			$this->view->googleauth = true;
			//var_dump($usergoogleAuth['0']['USER_FAILEDTOKEN']);
			$settingObj = new Settings();
			$maxtoken = $settingObj->getSetting("max_failed_token");
			$this->view->tokenfail = (int)$maxtoken-1;
			if($this->_custSameUser){
			$this->view->divchlcode = true;
			}else{
			$this->view->divchlcode = false;	
			}
			$this->view->token = true;
			$this->view->googleauth = false;
			$random_number = random_int(10000000, 99999999);
			//$this->view->chlcode = $random_number;
			$this->view->chlcode = chunk_split($random_number, 4, ' ');
			if($usergoogleAuth['0']['USER_FAILEDTOKEN'] != '0'){
				//die;
				$this->view->failedtoken = $usergoogleAuth['0']['USER_FAILEDTOKEN'];
				
				
				
				$tokenfail = (int)$maxtoken - (int)($usergoogleAuth['0']['USER_FAILEDTOKEN']+1);
				$this->view->tokenfail = $tokenfail;
			}
			//echo '<pre>';
			//var_dump($params);
			//var_dump($release);die;
			
		}
		$tokenIdUser = $tokenIdUser['TOKEN_ID'];


		$crossCurr = $this->_request->getParam('CROSS_CURR');
		if (empty($crossCurr))
			$crossCurr = 2; //inhouse biasa

		if ($crossCurr == 1) { //valas
			$set = new Settings();
			$beneficiary_acct_keyin = $set->getSetting('beneficiary_acct_keyin');
			$this->view->beneficiary_acct_keyin = $beneficiary_acct_keyin;
		} else {
			$PS_PERIODIC_EVERY = $this->_request->getParam('PERIODIC_EVERY');
			$PS_PERIODIC_EVERY_DATE = $this->_request->getParam('PERIODIC_EVERYDATE');
			$PS_ENDDATE_VAL = ($this->_request->getParam('PS_ENDDATEPERIODIC')) ? $this->_request->getParam('PS_ENDDATEPERIODIC') : date('d/m/Y');
			$this->view->endDatePeriodic = $PS_ENDDATE_VAL;

			$periodicEveryArr = array(
				'1' => $this->language->_('Monday'),
				'2' => $this->language->_('Tuesday'),
				'3' => $this->language->_('Wednesday'),
				'4' => $this->language->_('Thursday'),
				'5' => $this->language->_('Friday'),
				'6' => $this->language->_('Saturday'),
				'7' => $this->language->_('Sunday'),
			);
			$periodicEveryDateArr = range(1, 28);
			$periodicEveryDateArr['31'] = $this->language->_('End of month');
			// $periodicEveryDateArr['31'] = '30';
		}
		// var_dump($periodicEveryDateArr);die;

		//cek privilage
		$selectpriv = $this->_db->select()
			->from(array('A' => 'M_FPRIVI_USER'), array('FPRIVI_ID'))
			->where('A.FUSER_ID = ?', (string) $this->_custIdLogin . $this->_userIdLogin)
			->query()->fetchAll();

		foreach ($selectpriv as $key => $value) {
			foreach ($value as $row => $data) {
				if ($data == 'MTSP') {
					$usermtsp = true;
				}
				if ($data == 'UTSP') {
					$userutsp = true;
				}
			}
		}

		



		/*$select = $this->_db->select()
				->from(array('A' => 'M_TEMPLATE'),array('*'))
				->join(array('G' => 'M_TEMPLATE_DATA'), 'A.T_ID = G.TEMP_ID', array('G.*'));
				$select->where('A.T_GROUP = ?' , (string) $this->_custIdLogin);
		$select ->order('T_ID ASC');*/

		// echo $select;die;


		// add by Hamdan
		$tranferdatetype = $this->_request->getParam('tranferdatetype');
		$this->view->tranferdatetype = $tranferdatetype;


		$date_val	= date('d/m/Y');

		if ($tranferdatetype == '1') {
			$PS_EFDATE_VAL = $this->_request->getParam('PS_EFDATE');
			$this->view->paymentDate = $PS_EFDATE_VAL;
		} else {
			$PS_EFDATE_VAL = $this->_request->getParam('PS_FUTUREDATE');
			$this->view->paymentDate = $PS_EFDATE_VAL;
		}

		if ($PS_EFDATE_VAL > $date_val) {
			$pendingfuturedate = $this->language->_('Future Date Transfer');
			$this->view->paymentType = $pendingfuturedate;
		} elseif ($PS_EFDATE_VAL == $date_val) {
			$todaytranfer = $this->language->_('Today Transfer');
			$this->view->paymentType = $todaytranfer;
		} else {
			$pendingfuturedate = $this->language->_('Future Date Transfer');
			$this->view->paymentType = $pendingfuturedate;
		}

		$jenisTransfer =  $this->_request->getParam('tranferdatetype');
		$jenisPeriodic =  $this->_request->getParam('tranferdateperiodictype');

		if ($jenisTransfer == '1') {
			$todaytranfer = $this->language->_('Today Transfer');
			$this->view->paymentType = $todaytranfer;
		} elseif ($jenisTransfer == '2') {
			$pendingfuturedate = $this->language->_('Future Date Transfer');
			$this->view->paymentType = $pendingfuturedate;
		} elseif ($jenisTransfer == '3') {
			$periodictranfer = $this->language->_('Periodic Transfer');
			$this->view->paymentType = $periodictranfer;

			if ($jenisPeriodic == '5') {
				$this->view->periodicValue = $PS_PERIODIC_EVERY;
			} elseif ($jenisPeriodic == '6') {
				$this->view->periodicValue = $PS_PERIODIC_EVERY_DATE;
			}
		}

		$USE_CONFIRM_PAGE = true;
		$isResultPage 	  = false;
		$payReff 		  = '';


		// Set variables needed in view
		$settings = new Application_Settings();
		
		$ccyList  = $settings->setCurrencyRegistered();

		$lldIdenticalArr  	= $settings->getLLDDOMIdentical();
		$lldRelationshipArr = $settings->getLLDDOMRelationship();
		$lldCategoryArr = $settings->getLLDDOMCategory();

		$model = new purchasing_Model_Purchasing();

		$purposeArr = $model->getTranspurpose();

		$purposeList = array('' => '-- Select Transaction Purpose --');
		foreach ($purposeArr as $key => $value) {
			$purposeList[$value['CODE']] = $value['DESCRIPTION'];
		}

		$identyArr = $model->getIdenty();
		$identyList = array();
		foreach ($identyArr as $key => $value) {
			$identyList[$value['sm_key2']] = $value['sm_text1'];
		}


		$transactorArr = $model->getTransactor();
		$transRelationList = array();
		foreach ($transactorArr as $key => $value) {
			$transRelationList[$value['sm_key2']] = $value['sm_text1'];
		}

		$this->view->TransactorArr = $lldRelationshipArr;
		$this->view->IdentyArr = $lldIdenticalArr;
		$this->view->TransPurposeArr = $purposeList;
		$this->view->CategoryArr = $lldCategoryArr;

		if ($this->_getParam('process') == 'back') {
			Zend_Session::namespaceUnset('TW');
		}

		$sessionNamespace = new Zend_Session_Namespace('TW');

		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		
		if ($crossCurr == 1) {
			$AccArr 	  = $CustomerUser->getAccounts();
		} else {
			$param = array('CCY_IN' => array("IDR", "USD"));
			$AccArr = $CustomerUser->getAccounts($param);
		}
		// End
		



		$filter 		= new Application_Filtering();
		// $PS_NUMBER 		= $filter->filter($this->_getParam('PS_NUMBER'), "PS_NUMBER");

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		// $this->view->token = $sessionNamespace->token;


		$AESMYSQL = new Crypt_AESMYSQL();
		$this->view->encps_number = $this->_getParam('PS_NUMBER');
		$PS_NUMBER 			= urldecode($filter->filter($this->_getParam('PS_NUMBER'), "PS_NUMBER"));
		$PS_NUMBER = $AESMYSQL->decrypt($PS_NUMBER, $password);

		$PS_EFDATE 		= Application_Helper_General::convertDate($this->getCurrentDate());
		$isConfirmPage 	= $this->_getParam('confirmPage');

		$process 	= $this->_getParam('process');
		$submitBtn 	= ($this->_request->isPost() && $process == "submit") ? true : false;


		// get ps_number data for repair payment
		if (!empty($PS_NUMBER) && !$this->_request->isPost()) {

			$paramList = array(
				"WA" 			=> false,
				"ACCOUNT_LIST" 	=> $this->_accountList,
				"_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
			);
			$select   = $CustomerUser->getPayment($paramList, false);
			$select->where('P.PS_NUMBER = ?', (string) $PS_NUMBER);

			if ($crossCurr == 1) {
				$select->columns(
					array(
						"tra_message"		=> "T.TRA_MESSAGE",
						"TRA_ADDMESSAGE"		=> "T.TRA_ADDITIONAL_MESSAGE",
						"acbenef_email"			=> "T.BENEFICIARY_EMAIL",
						"acbenef_address"		=> "T.BENEFICIARY_ADDRESS",
						"acbenef_citizenship"	=> "T.BENEFICIARY_CITIZENSHIP", //WNI WNA W / R
						"acbenef_resident"		=> "T.BENEFICIARY_RESIDENT",
						"bank_name"				=> "T.BENEFICIARY_BANK_NAME",
						"bank_city"				=> "T.BENEFICIARY_BANK_CITY",
						"clr_code"				=> "T.CLR_CODE",
						"lld_desc"				=> "T.LLD_DESC",
						"lld_code"				=> "T.LLD_CODE",
						"transfer_type"			=> "T.TRANSFER_TYPE",
						"acbenef_category"		=> "T.BENEFICIARY_CATEGORY",
						"acbenef_identity_type" => "T.BENEFICIARY_ID_TYPE",
						"acbenef_identity_num"	=> "T.BENEFICIARY_ID_NUMBER",
						"acbenef_address1"		=> "T.BENEFICIARY_BANK_ADDRESS1",
						"acbenef_address2"		=> "T.BENEFICIARY_BANK_ADDRESS2",
						"acbenef_pob"			=> "T.POB_NUMBER",
						"acbenef_country"		=> "T.BENEFICIARY_BANK_COUNTRY",
						"lld_identity"			=> "T.LLD_IDENTITY",
						"lld_trans_purpose"		=> "T.LLD_TRANSACTION_PURPOSE",
						"lld_trans_rel"			=> "T.LLD_TRANSACTOR_RELATIONSHIP"
						//"acbenef_bankname"		=> "T.BENEFICIARY_ACCOUNT_NAME",
						//"acbenef_alias"			=> "T.BENEFICIARY_ALIAS_NAME",
					)
				);
			} else {
				$select->columns(
					array(
						"tra_message"		=> "T.TRA_MESSAGE",
						"tra_refno"			=> "T.TRA_REFNO",
						"acbenef_email"		=> "T.BENEFICIARY_EMAIL",
						"acbenef_bankname"	=> "T.BENEFICIARY_ACCOUNT_NAME",
						"acbenef_alias"		=> "T.BENEFICIARY_ALIAS_NAME",
					)
				);
			}

			$pslipData 	= $this->_db->fetchRow($select);

			if (!empty($pslipData)) {
				//tambahn pentest
				$PS_EFDATE_ORI  	= date("d/m/Y", strtotime($pslipData['efdate']));
				$PS_SUBJECT 		= $pslipData['paySubj'];

				if ($PS_EFDATE_ORI > date('d/m/Y')) {
					$TransferDate 	=  "2"; //future
					$PS_EFDATE = $PS_EFDATE_ORI;
				} else {
					$TransferDate	=  "1"; //imediate
					$PS_EFDATE = date('d/m/Y', strtotime('+1 days', strtotime(date('Y-m-d'))));
				}
				//echo '<pre>';
				//var_dump($pslipData);die;
				$TRA_AMOUNT  		= Application_Helper_General::displayMoney($pslipData['amount']);
				$TRA_MESSAGE 		= $pslipData['tra_message'];
				$PS_NOTIF			= $pslipData['ps_notif'];
				$PS_EMAIL			= $pslipData['ps_email'];
				$PS_SMS				= $pslipData['ps_sms'];
				$ACCTSRC 			= $pslipData['accsrc'];
				$ACBENEF  			= $pslipData['acbenef'];
				$ACBENEF_CCY  		= $pslipData['acbenef_ccy'];
				$ACBENEF_BANKNAME	= $pslipData['acbenef_bankname'];
				$ACBENEF_ALIAS  	= $pslipData['acbenef_alias'];
				$ACBENEF_EMAIL  	= $pslipData['acbenef_email'];
				$PS_STATUS 			= $pslipData['PS_STATUS'];
				$PS_TYPE  			= $pslipData['PS_TYPE'];

				if ($crossCurr == 1) {
					$TRA_ADDMESSAGE 	= $pslipData['TRA_ADDMESSAGE'];
					$ACCTSRC_CCY		= $pslipData['accsrc_ccy'];
					$BENEF_CURRENCY  	= $pslipData['acbenef_ccy'];
					$CURR_CODE			= $pslipData['ccy'];

					$ACBENEF_ADDRESS  	= $pslipData['acbenef_address'];
					$BENEFICIARY_RESIDENT = $pslipData['acbenef_resident'];
					$BENEFICIARY_CITIZENSHIP = $pslipData['acbenef_citizenship'];

					$BANK_NAME			= $pslipData['bank_name'];
					$BANKNAME			= $pslipData['bank_name'];
					$BANK_CITY			= $pslipData['bank_city'];
					$CLR_CODE			= $pslipData['clr_code'];
					$LLD_DESC			= $pslipData['lld_desc'];
					$LLD_CODE			= $pslipData['lld_code'];
					$SWIFT_CODE			= $pslipData['SWIFT_CODE'];
					$TRANSFER_TYPE  	= $pslipData['transfer_type'];	// 1, 2

					$BENEFICIARY_CATEGORY = $lldCategoryArr[$pslipData['acbenef_category']];
					$BENEFICIARY_ID_TYPE = $pslipData['acbenef_identity_type'];
					$ACBENEF_IDENTITY_TYPE_DISP = $lldBeneIdentifArr[$BENEFICIARY_ID_TYPE];
					$BENEFICIARY_ID_NUMBER  = $pslipData['acbenef_identity_num'];

					$ACBENEF_BANK_ADD1 = $pslipData['acbenef_address1'];
					$ACBENEF_BANK_ADD2 = $pslipData['acbenef_address2'];
					$ACBENEF_POBNUMB   = $pslipData['acbenef_pob'];
					$country_code   = $pslipData['acbenef_country'];

					$LLD_IDENTITY  = $pslipData['lld_identity'];

					$LLD_TRANSACTOR_RELATIONSHIP = $pslipData['lld_trans_rel'];
					$LLD_TRANSACTION_PURPOSE = $pslipData['lld_trans_purpose'];

					$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
					$sessionNameConfrim->benefAcct = $ACBENEF;
					$sessionNameConfrim->sourceAcct = $ACCTSRC;
					$sessionNameConfrim->ACBENEF_BANKNAME = $ACBENEF_BANKNAME;
					$sessionNameConfrim->traAmount = $TRA_AMOUNT;
				} else {
					$TRA_REFNO 			= $pslipData['tra_refno'];
				}

				// Payment Status is not Request Repair
				if ($PS_STATUS != $this->_paymentstatus["code"]["requestrepair"]) {
					$this->_helper->getHelper('FlashMessenger')->addMessage("F");
					$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Payment Status has changed.");
					$this->_redirect('/paymentworkflow/requestrepair/index');
				} elseif ($PS_TYPE != $this->_paymenttype["code"]["within"])	// 0: PB
				{
					$this->_helper->getHelper('FlashMessenger')->addMessage("F");
					$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Payment Type is invalid.");
					$this->_redirect('/paymentworkflow/requestrepair/index');
				}
			} else	// ps_number is invalid, or not belong to customer
			{
				$this->_helper->getHelper('FlashMessenger')->addMessage("F");
				$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Payment Ref# is invalid.");
				$this->_redirect('/paymentworkflow/requestrepair/index');
			}
		}

		if (!empty($PS_NUMBER) && !$this->view->hasPrivilege('PRPP')) {
			$this->_helper->getHelper('FlashMessenger')->addMessage("F");
			$this->_helper->getHelper('FlashMessenger')->addMessage("Error: You don't have privilege to repair payment.");
			$this->_redirect('/paymentworkflow/requestrepair/index');
		}

		//tambahn pentest
		$tranferdatetype = $this->_request->getParam('tranferdatetype');
		$this->view->tranferdatetype = $tranferdatetype;

		if (!empty($pslipData)) {
			if ($tranferdatetype == '1') {
				$this->view->PS_EFDATEFUTURE = $PS_EFDATE;
			} elseif ($tranferdatetype == '3') {
				$PS_EFDATE = date('d/m/Y', strtotime('+1 days', strtotime(date('Y-m-d'))));
				$this->view->PS_EFDATEFUTURE = $PS_EFDATE;
				$PS_EFDATE = $PS_EFDATE_ORII; //
			} else {
				$this->view->PS_EFDATEFUTURE = $PS_EFDATE;
			}
		} else {
			$this->view->PS_EFDATEFUTURE = $futureDate;
		}

		$sessionNameRand = new Zend_Session_Namespace('confirmTransactRand');


		if ($this->_request->isPost()) {

			$CROSS_CURR = $crossCurr;
			// print_r($crossCurr);die;
			$sessionNameRand = new Zend_Session_Namespace('confirmTransactRand');

			if ($this->_getParam('randomTransact') == $sessionNameRand->randomTransact) {
				try {
					$passwordRand = $sessionNameRand->randomTransact; //yang hidden param random
					$blocksize = 256;  // can be 128, 192 or 256
					$ACBENEF =  SGO_Helper_AES::decrypt($this->_request->getParam('ACBENEF'), $passwordRand, $blocksize);
					$ACCTSRC =  SGO_Helper_AES::decrypt($this->_request->getParam('ACCTSRC'), $passwordRand, $blocksize);
					$TRA_AMOUNT =  SGO_Helper_AES::decrypt($this->_request->getParam('TRA_AMOUNT'), $passwordRand, $blocksize);
				} catch (Exception $e) {
					$ACBENEF = '';
					$ACCTSRC = '';
					$TRA_AMOUNT = '';
				}

				// $CROSS_CURR 		= $filter->filter($this->_request->getParam('CROSS_CURR')	, "SELECTION");
				$PS_SUBJECT 		= $filter->filter($this->_request->getParam('PS_SUBJECT'), "PS_SUBJECT");
				// print_r($CROSS_CURR);die;
				$this->view->error = false;

				$SAVEBENE = $this->_getParam('save_bene');

				if ($CROSS_CURR == 1) {
					$TRANS_PURPOSECODE = $this->_getParam('TRANSPURPOSE_SELECT');
					$LLD_IDENTITY = $this->_request->getParam('LLD_IDENTITY_SELECT');
					$LLD_TRANSACTOR_RELATIONSHIP = $this->_request->getParam('LLD_TRANSACTOR_RELATIONSHIP_SELECT');
					$LLD_TRANSACTION_PURPOSE = $this->_request->getParam('TRANSPURPOSE_SELECT');
					$BENEFICIARY_CITIZENSHIP = $this->_request->getParam('ACBENEF_CITIZENSHIP');
					$BENEFICIARY_RESIDENT = $this->_request->getParam('ACBENEF_NATION');
					$BENEFICIARY_CATEGORY = $this->_request->getParam('ACBENEF_CATEGORY');
					$BENEFICIARY_ID_TYPE  = $this->_request->getParam('ACBENEF_IDENTY');
					$BENEFICIARY_ID_NUMBER 	= $this->_request->getParam('ACBENEF_IDENTYNUM');

					$TRANSPURPOSE 			= $this->_request->getParam('TRANSPURPOSE_SELECT');
					$TRA_PURPOSE 			= $this->_request->getParam('TRANSPURPOSE_DESC');
					// "TRA_PURPOSE"				=> $TRA_PURPOSE,  
					$ACBENEF_CURRENCY1		= $this->_request->getParam('TRA_CCY');
					$ACBENEF_CURRENCY		= $this->_request->getParam('TRA_CCY');
					$BENEF_CURRENCY			= $this->_request->getParam('TRA_CCY');

					$ACCTSRC_CURRECY		= $this->_request->getParam('ACCTSRC_CCY');
					$ACCTSRC_CURRECY2		= $this->_request->getParam('ACCTSRC_CCY2');
					$ACBENEF_CURRENCY1		= $ACBENEF_CURRENCY1;
					$ACBENEF_CURRENCY		= $ACBENEF_CURRENCY1;
					$BENEF_CURRENCY			= $ACBENEF_CURRENCY1;
					$TREASURY_NUM			= $this->_request->getParam('TREASURY_NUM');
					$DOC_ID			= $this->_request->getParam('DOC_ID');
					
					$EXRATE  				= $this->_request->getParam('exrate');
					$this->view->EXRATE = $EXRATE;



					$TRA_NOTIF		= $this->_request->getParam('notif');
					$TRA_EMAIL		= $this->_request->getParam('email_notif');
					$TRA_SMS		= $this->_request->getParam('sms_notif');

					$this->view->ACCTSRC_CCY = $ACCTSRC_CURRECY;
					$this->view->ACCTSRC_CCY2 = $ACCTSRC_CURRECY2;
					$this->view->CURR_CODE_SEL = $ACBENEF_CURRENCY;

					$TRA_ADDMESSAGE = $filter->filter($this->_request->getParam('TRA_ADDMESSAGE'), "TRA_ADDMESSAGE");
				} else {
					$TRA_REFNO 			= $filter->filter($this->_request->getParam('TRA_REFNO'), "TRA_REFNO");
					$TRANSFER_FEE 		= $filter->filter($this->_request->getParam('TRANSFER_FEE'), "TRANSFER_FEE");
					$TRANSFER_FEE_num 	= Application_Helper_General::convertDisplayMoney($TRANSFER_FEE);
					$TRA_NOTIF		= $this->_request->getParam('notif');
					$TRA_EMAIL		= $this->_request->getParam('email_notif');
					$TRA_SMS		= $this->_request->getParam('sms_notif');
				}
				// echo 'here';
				// var_dump($TRA_NOTIF);
				// echo '---';

				$PS_EFDATE 			= $filter->filter($this->_request->getParam('PS_EFDATE'), "PS_DATE");

				//tambahan pentest
				if ($PS_NUMBER) {
					if ($this->_getParam('tranferdatetype') == "2") {
						$PS_EFDATE = $this->_getParam('PS_EFDATE');
					} else {
						$PS_EFDATE = date('d/m/Y');
					}
				}
				 

				$TRA_AMOUNT 		= $filter->filter($TRA_AMOUNT, "AMOUNT");
				$TRA_MESSAGE 		= $filter->filter($this->_request->getParam('TRA_MESSAGE'), "TRA_MESSAGE");

				$ACCTSRC 			= $filter->filter($ACCTSRC, "ACCOUNT_NO");
				$ACBENEF 			= $filter->filter($ACBENEF, "ACCOUNT_NO");
				$ACBENEF_BANKNAME 	= $filter->filter($this->_request->getParam('ACBENEF_BANKNAME'), "ACCOUNT_NAME");
				$ACBENEF_ALIAS 		= $filter->filter($this->_request->getParam('ACBENEF_ALIAS'), "ACCOUNT_ALIAS");
				$ACBENEF_EMAIL 		= $filter->filter($this->_request->getParam('ACBENEF_EMAIL'), "EMAIL");
				$ACBENEF_CCY 		= $filter->filter($this->_request->getParam('CURR_CODE'), "SELECTION");
				$TrfDateType 		= $filter->filter($this->_request->getParam('tranferdatetype'), "SELECTION");
				$PERIODIC_EVERY 	= $filter->filter($this->_request->getParam('PERIODIC_EVERY'), "SELECTION");
				$PERIODIC_EVERYDATE = $filter->filter($this->_request->getParam('PERIODIC_EVERYDATE'), "SELECTION");
				$TrfPeriodicType 	= $this->_request->getParam('tranferdateperiodictype');
				// var_dump($tr)
				$PS_ENDDATE			= $filter->filter($this->_request->getParam('PS_ENDDATEPERIODIC'), "PS_DATEPERIODIC");
				$PS_EVERY_PERIODIC_UOM 	= $TrfPeriodicType;

				$START_DATE 			= $filter->filter(date('d/m/Y'), "PS_DATE");
				$EXPIRY_DATE 			= $filter->filter($this->_request->getParam('PS_ENDDATEPERIODIC'), "PS_DATE");

				$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);
				$TRA_CCY			= $filter->filter($this->_request->getParam('TRA_CCY'), "TRA_CCY");

				$filter->__destruct();
				unset($filter);

				// if($this->_getParam('process') == 'back' && $CROSS_CURR == 1){
				// 	$PS_NUMBER = '';
				// 	$PS_SUBJECT = '';
				// 	$TRA_AMOUNT = '';
				// 	$TRA_MESSAGE = '';
				// 	$TRA_ADDMESSAGE = '';
				// 	$TRA_MESSAGE_len = '';
				// 	$TRA_ADDMESSAGE_len = '';
				// 	$ACCTSRC = '';
				// 	$ACCTSRC_view = '';
				// 	$ACBENEF = '';
				// 	$ACBENEF_BANKNAME = '';
				// 	$ACBENEF_ALIAS = '';
				// 	$ACBENEF_EMAIL = '';
				// 	$CURR_CODE = '';
				// 	$CHARGES_AMT = '';
				// 	$CHARGES_CCY = '';
				// 	$PS_EVERY_PERIODIC = '';
				// 	$PS_EVERY_PERIODIC_UOM = '';
				// 	$START_DATE = '';
				// 	$EXPIRY_DATE = '';
				// 	$PERIODIC_EVERY = '0';
				// 	$PERIODIC_EVERYDATE = '0';
				// 	$TrfDateType = '1';
				// 	$TrfPeriodicType = '0';
				// }

				// post submit payment
				// $submitBtn = true;
				// print_r($submitBtn);die;
				if ($submitBtn) {

					if ($CROSS_CURR == 2) {
						//cek periodik
						
						if ($TrfDateType=='3'){
											
						$repetition = $this->_getParam('repetition');
						$efDate = $this->_getParam('PS_EFDATE1');
						$PS_EFDATE 			= $this->_request->getParam('PS_EFDATE1');
						//daily
						if ($repetition == 1) {
							$repeatOn = $this->_getParam('report_day');
							$endDate = $this->_getParam('PS_ENDDATEPERIODIC');
						}
						//weekly
						else if ($repetition == 2) {
							$repeatOn = $this->_getParam('report_day');
							$endDate = $this->_getParam('PS_ENDDATEPERIODIC');

							if (empty($repeatOn)) {
								$this->view->error = true;
								$confirmPage = false;
								$this->view->error_msg = 'Repeat On Be Selected';
							}
						}
						//monthly
						else if ($repetition == 3) {
							$repeatEvery = $this->_getParam('selectrepeat');
							// $repeatOn = $this->_getParam('PS_REPEATON');
							$endDate = $this->_getParam('PS_ENDDATEPERIODIC');

							if (empty($repeatEvery)) {
								$this->view->error = true;
								$confirmPage = false;
								$this->view->error_msg = 'Repeat Every Must Be Selected';
							}
							// if (empty($repeatOn)) {
							// 	$this->view->error = true;
							// 	$confirmPage = false;
							// 	$error_msg[] = 'Repeat On Be Selected';
							// }
						}

						if (empty($endDate)) {
							$this->view->error = true;
							$confirmPage = false;
							$this->view->error_msg = 'End Date Must Be Selected';
						}

						//periodic
						$this->view->efDate = $efDate;
						$this->view->repetition = $repetition;
						$this->view->endDate = $endDate;
						$this->view->repeatEvery = $repeatEvery;
						$this->view->repeatOn = $repeatOn;
						$this->view->endDate = $endDate;
						$this->view->repeat_day = $repeatOn;


					}
						
						/*
						if ($TrfDateType == '3') {

							// NextDate
							if ($PS_EVERY_PERIODIC_UOM == 5) { //every day

								$dateNow = mktime(0, 0, 0, date("n"), date("j"), date("Y"));
								$every = (int) $PERIODIC_EVERY;
								$d = date("w", $dateNow);

								$addDay = $every;
								if ($every > $d) {
									$addDay = $addDay;
								} else {
									$addDay = $addDay + 7;
								}
								$nextDate  =  (int) $addDay - (int) $d;
								$NEXT_DATE = date("Y-m-d", strtotime("+$nextDate day"));
								$PERIODIC_EVERY_VAL = $PERIODIC_EVERY;
							} elseif ($PS_EVERY_PERIODIC_UOM == 6) { //every date

								$dateNextMonth = mktime(0, 0, 0, date("n"), date("j") + 1, date("Y"));

								$dateNow = date("j");
								$maxDays = date('t', $dateNextMonth);
								//echo $maxDays; die;
								$every = (int) $PERIODIC_EVERYDATE;

								if ($every > $dateNow) {
									$addMonth = 0;
								} else {
									$addMonth = 1;
								}

								if ($maxDays >=  $every) {
									$every = $every;
								} else {
									$every = $maxDays;
								}

								$nextDate = mktime(0, 0, 0, date("n") + $addMonth, $every, date("Y"));
								$NEXT_DATE = date("Y-m-d", $nextDate);
								$PERIODIC_EVERY_VAL = $PERIODIC_EVERYDATE;
							}
							// var_dump($PS_EVERY_PERIODIC_UOM);
							$END_DATE = join('-', array_reverse(explode('/', $EXPIRY_DATE)));
							$PS_EFDATE = date("d/m/Y", strtotime($NEXT_DATE)); //date_format($NEXT_DATE, 'd-m-Y');
							$PS_EFDATE_ORII = date("d/m/Y", strtotime($NEXT_DATE)); //date_format($NEXT_DATE, 'd-m-Y');
							// print_r($PS_EFDATE);die;
						} */
						else if($TrfDateType=='2'){
						$PS_EFDATE 			= $this->_request->getParam('PS_EFDATE');
						//var_dump($PS_EFDATE);
						$PS_EFDATE = $PS_EFDATE;
						}
						else {
							$PS_EFDATE = $PS_EFDATE;
						}
					}

					//cek date kosong
					if (!$PS_EFDATE) {
						$error_msg	 			= 'Payment Date can not be left blank.';
						$this->view->error 		= true;
						$this->view->ERROR_MSG	= $error_msg;
					} elseif ($TrfDateType == '3' && $TrfPeriodicType == 5 && empty($PERIODIC_EVERY)) {
						$errorMsg		 		= $this->language->_('Period can not be left blank.');
						$this->view->error 		= true;
						$this->view->ERROR_MSG	= $errorMsg;

						$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
						$sessionNameConfrim->benefAcct = $ACBENEF;
						$sessionNameConfrim->sourceAcct = $ACCTSRC;
						$sessionNameConfrim->traAmount = $TRA_AMOUNT_num;
					} elseif ($TrfDateType == '3' && $TrfPeriodicType == 6 && empty($PERIODIC_EVERYDATE)) {
						$errorMsg		 		= $this->language->_('Period can not be left blank.');
						$this->view->error 		= true;
						$this->view->ERROR_MSG	= $errorMsg;

						$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
						$sessionNameConfrim->benefAcct = $ACBENEF;
						$sessionNameConfrim->sourceAcct = $ACCTSRC;
						$sessionNameConfrim->traAmount = $TRA_AMOUNT_num;
					} elseif ($TrfDateType == '3' && strtotime($NEXT_DATE) > strtotime($END_DATE)) {
						$errorMsg		 		= $this->language->_('End Date must be greater than') . " " . $NEXT_DATE;
						$this->view->error 		= true;
						$this->view->ERROR_MSG	= $errorMsg;

						$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
						$sessionNameConfrim->benefAcct = $ACBENEF;
						$sessionNameConfrim->sourceAcct = $ACCTSRC;
						$sessionNameConfrim->traAmount = $TRA_AMOUNT_num;
					} else {

						// die('here');
						$validateDateFormat = new Zend_Validate_Date(array('format' => $this->_dateDisplayFormat));
						if (!$validateDateFormat->isValid($PS_EFDATE)) {
							// die('here');
							$error_msg = 'Invalid Payment Date Format. Payment Date Format must be dd/MM/yyyy';
							$this->view->error 		= true;
							$this->view->ERROR_MSG	= $error_msg;
						} else {
							//new kebutuhan pentest (revisi security)
							$resultToken = true;
							
							if ($isConfirmPage != 1) {
								//var_dump($this->_custSameUser);die;
								if ($this->_custSameUser) {
									// var_dump($this->view->hasPrivilege('PRLP'));
									if (!$this->view->hasPrivilege('PRLP')) {
										// die('here');
										$errMessage = $this->language->_("Error: You don't have privilege to release payment");
										$this->view->error = true;
										$this->view->ERROR_MSG = $this->language->_("Error: You don't have privilege to release payment");
									} else {
										// die('sini');

										$challengeCode		= $this->_getParam('challengeCode');

										$inputtoken1 		= $this->_getParam('inputtoken1');
										$inputtoken2 		= $this->_getParam('inputtoken2');
										$inputtoken3 		= $this->_getParam('inputtoken3');
										$inputtoken4 		= $this->_getParam('inputtoken4');
										$inputtoken5 		= $this->_getParam('inputtoken5');
										$inputtoken6 		= $this->_getParam('inputtoken6');
										$inputtoken7 		= $this->_getParam('inputtoken7');
										$inputtoken8 		= $this->_getParam('inputtoken8');

										$responseCode		= $inputtoken1 . $inputtoken2 . $inputtoken3 . $inputtoken4 . $inputtoken5 . $inputtoken6. $inputtoken7 . $inputtoken8;
										//var_dump($tokenType);
					//die('here');
										if($tokenType == 6){
						
													$select3 = $this->_db->select()
														 ->from(array('C' => 'M_USER'));
													$select3->where("USER_ID = ".$this->_db->quote($this->_userIdLogin));
													$select3->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin));
													$data2 = $this->_db->fetchRow($select3);
											
													$request = array();
													$request['mod'] = 'prosesCR';
													$request['tokenid'] = $data2['TOKEN_ID'];
													$request['otp'] = $responseCode;
													$request['chl'] = $this->_request->getParam('chlcode');
													
													$request['token'] = 'mcodex';
													$clientUser  =  new SGO_Soap_ClientUser();
													$success = $clientUser->callapi('token',$request);
													$result  = $clientUser->getResult();
													//var_dump($result);die;
													
													if($result != 'CORRECT'){
														$tokenFailed = $CustUser->setLogToken(); //log token activity

														$error = true;
														$this->view->error = true;
														$this->view->popauth = true;
														$errorMsg[] = $this->language->_('Invalid Token Auth Code');	//$verToken['ResponseDesc'];
														$this->view->ERROR_MSG =  $this->language->_('Invalid Token Auth Code');

														if ($tokenFailed === true) {
															$this->_redirect('/default/index/logout');
														}
													}else{
														//$step = $step+1;
														$tokensuccess = true;
													}
													
													//die;
										
									}else{

										$HardToken 	= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
										$resHard = $HardToken->verifyHardToken($challengeCode, $responseCode);
										$resultToken = $resHard['ResponseCode'] == '0000';

										if ($resHard['ResponseCode'] != '0000') {
											$tokenFailed = $CustUser->setLogToken(); //log token activity

											$this->view->error = true;
											$errorMsg[] = $this->language->_('Invalid Response Code');	//$verToken['ResponseDesc'];

											if ($tokenFailed === true) {
												$this->_redirect('/default/index/logout');
											}
										}
									}
									}
								}
								$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
								$sessionNameConfrim->benefAcct = $ACBENEF;
								$sessionNameConfrim->sourceAcct = $ACCTSRC;
								$sessionNameConfrim->traAmount = $TRA_AMOUNT_num;
							} else {
								if ($this->_custSameUser) {
									// var_dump($tokenGoogle);
									// die('here');

									$inputtoken1 		= $this->_getParam('inputtoken1');
									$inputtoken2 		= $this->_getParam('inputtoken2');
									$inputtoken3 		= $this->_getParam('inputtoken3');
									$inputtoken4 		= $this->_getParam('inputtoken4');
									$inputtoken5 		= $this->_getParam('inputtoken5');
									$inputtoken6 		= $this->_getParam('inputtoken6');
									$inputtoken7 		= $this->_getParam('inputtoken7');
									$inputtoken8 		= $this->_getParam('inputtoken8');

									$responseCode		= $inputtoken1 . $inputtoken2 . $inputtoken3 . $inputtoken4 . $inputtoken5 . $inputtoken6. $inputtoken7 . $inputtoken8;

									// $HardToken 		= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
									// $resHard = $HardToken->validateOtp($responseCode);
									// $resultToken = $resHard['ResponseCode'] == '0000';
									// // print_r($resHard);die;
									// if ($resHard['ResponseCode'] != '00'){
									// 	$tokenFailed = $CustUser->setLogToken(); //log token activity

									// 	$this->view->error = true;
									// 	$errorMsg[] = $this->language->_('Invalid Response Code');	//$verToken['ResponseDesc'];

									// 	if ($tokenFailed === true)
									// 	{
									// 		$this->_redirect('/default/index/logout');
									// 	}
									// }
									
									if($tokenType == 6){
						
								$select3 = $this->_db->select()
									 ->from(array('C' => 'M_USER'));
								$select3->where("USER_ID = ".$this->_db->quote($this->_userIdLogin));
								$select3->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin));
								$data2 = $this->_db->fetchRow($select3);
						
								$request = array();
								$request['mod'] = 'prosesOTP';
								$request['tokenid'] = $data2['TOKEN_ID'];
								$request['otp'] = $responseCode;
								//$request['chl'] = $this->_request->getParam('chlcode');
								
								$request['token'] = 'mcodex';
								$clientUser  =  new SGO_Soap_ClientUser();
								$success = $clientUser->callapi('token',$request);
								$result  = $clientUser->getResult();
								
								if($result != 'CORRECT'){
									$tokenFailed = $CustUser->setLogToken(); //log token activity
									
									$error = true;
									$this->view->error = true;
									$this->view->popauth = true;
									$errorMsg[] = $this->language->_('Invalid Response Code');	//$verToken['ResponseDesc'];
									$this->view->ERROR_MSG = $this->language->_('Invalid Response Code');	//$verToken['ResponseDesc'];

									if ($tokenFailed === true) {
										$this->_redirect('/default/index/logout');
									}
								}else{
									//$step = $step+1;
									$tokensuccess = true;
								}
								
								//die;
					
							}else{
									if (!empty($tokenGoogle)) {

										$pga = new PHPGangsta_GoogleAuthenticator();
										// var_dump($data2['GOOGLE_CODE']);
										$setting 		= new Settings();
										$google_duration 	= $setting->getSetting('google_duration');
										$resultcapca = $pga->verifyCode($tokenGoogle, $responseCode, $google_duration);
										// var_dump($resultcapca);
										//  	var_dump($responseCode);
										//  	var_dump($tokenGoogle);die;
										if ($resultcapca) {
											$resultToken = $resHard['ResponseCode'] == '0000';
										} else {
											$tokenFailed = $CustUser->setLogToken(); //log token activity

											$error = true;
											$this->view->error = true;
											$errorMsg[] = $this->language->_('Invalid Google Auth Code');	//$verToken['ResponseDesc'];
											$this->view->ERROR_MSG =  $this->language->_('Invalid Google Auth Code');

											// if ($tokenFailed === true)
											// {
											// 	$this->_redirect('/default/index/logout');
											// }
										}
									} else {

										$Token 		= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
										$verToken 	= $Token->verify($challengeCode, $responseCode);

										if ($verToken['ResponseCode'] != '00') {
											$tokenFailed = $CustUser->setLogToken(); //log token activity

											$error = true;
											$this->view->error = true;
											$errorMsg[] = $this->language->_('Invalid Response Code');	//$verToken['ResponseDesc'];
											$this->view->ERROR_MSG =  $this->language->_('Invalid Response Code');

											if ($tokenFailed === true) {
												$this->_redirect('/default/index/logout');
											}
										}
									}
									
									
							}	
									
								}
								// die('here1');
								$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
								$sessionNameConfrim->benefAcct;
								$sessionNameConfrim->sourceAcct;
								$sessionNameConfrim->traAmount;
							}
							// print_r($CROSS_CURR);die;
							if ($CROSS_CURR == 2) {
								$PS_EFDATE 		= Application_Helper_General::convertDate($PS_EFDATE);
								$srcData = $this->_db->select()
									->from(array('M_CUSTOMER_ACCT'), array('CCY_ID'))
									->where('ACCT_NO = ?', $ACCTSRC)
									->limit(1);
								$acsrcData = $this->_db->fetchRow($srcData);
								$paramPayment = array(
									"CATEGORY" 					=> "SINGLE PB",
									"FROM" 						=> "F",				// F: Form, I: Import
									"PS_NUMBER"					=> $PS_NUMBER,
									"PS_SUBJECT"				=> $PS_SUBJECT,
									"PS_EFDATE"					=> $PS_EFDATE,
									"_dateFormat"				=> $this->_dateDisplayFormat,
									"_dateDBFormat"				=> $this->_dateDBFormat,
									"_addBeneficiary"			=> $this->view->hasPrivilege('BADA'), // privi BADA (Add Beneficiary)
									"_beneLinkage"				=> $this->view->hasPrivilege('BLBU'), // privi BLBU (Linkage Beneficiary User)
									"_createPB"					=> 1, // privi CRSP (Create Single Payment)
									"_createDOM"				=> false, // cannot create DOM trx
									"_createREM"				=> false, // cannot create REM trx
									"CROSS_CURR"				=> $CROSS_CURR,
									"TRA_CCY"					=> $acsrcData['CCY_ID'],
								);

								$paramTrxArr[0] = array(
									"TRANSFER_TYPE" 		=> "PB",
									"TRA_AMOUNT" 				=> $sessionNameConfrim->traAmount,
									"TRANSFER_FEE" 				=> $TRANSFER_FEE_num,
									"TRA_MESSAGE" 				=> $TRA_MESSAGE,
									"TRA_REFNO" 				=> $TRA_REFNO,
									"ACCTSRC" 					=> $sessionNameConfrim->sourceAcct,
									"ACBENEF" 					=> $sessionNameConfrim->benefAcct,
									"ACBENEF_CCY" 				=> $ACBENEF_CCY,
									"ACBENEF_BANKNAME" 			=> &$ACBENEF_BANKNAME,
									// for Beneficiary data, except (bene CCY and email), must be passed by reference
									"ACBENEF_ALIAS" 			=> &$ACBENEF_ALIAS,
									"ACBENEF_EMAIL" 			=> $ACBENEF_EMAIL,
									"PS_EMAIL"					=> $TRA_EMAIL,
									"PS_NOTIF"					=> $TRA_NOTIF,
									"PS_SMS"					=> $TRA_SMS,
								);
							} else {

								$accsrcData = $model->getBeneficiaryByAccount($sessionNameConfrim->sourceAcct);
								$srcData = $this->_db->select()
									->from(array('M_CUSTOMER_ACCT'), array('CCY_ID'))
									->where('ACCT_NO = ?', $ACCTSRC)
									->limit(1);
								$acsrcData = $this->_db->fetchRow($srcData);
								$paramPayment = array(
									"CATEGORY" 					=> "SINGLE PB",
									"FROM" 						=> "F",				// F: Form, I: Import
									"PS_NUMBER"					=> $PS_NUMBER,
									"PS_SUBJECT"				=> '',
									"PS_EFDATE"					=> $PS_EFDATE,
									"_dateFormat"				=> $this->_dateDisplayFormat,
									"_dateDBFormat"				=> $this->_dateDBFormat,
									"_addBeneficiary"			=> $this->view->hasPrivilege('BADA'), // privi BADA (Add Beneficiary)
									"_beneLinkage"				=> $this->view->hasPrivilege('BLBU'), // privi BLBU (Linkage Beneficiary User)
									"_createPB"					=> 1, // privi CRSP (Create Single Payment)
									"_createDOM"				=> false, // cannot create DOM trx
									"_createREM"				=> false, // cannot create REM trx
									"CROSS_CURR"				=> $CROSS_CURR,
									"TRA_CCY"					=> $acsrcData['CCY_ID'],
									"PS_CCY"					=> $TRA_CCY,
								);

								//$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
								//$AccArr 	  = $CustomerUser->getAccounts(array("ACCT_TYPE" => array('10','20')));	// show acc in IDR only

								$srcData = $this->_db->select()
									->from(array('M_CUSTOMER_ACCT'), array('ACCT_NO', 'ACCT_TYPE'))
									->where('ACCT_NO = ?', $ACCTSRC)
									->limit(1);

								$acsrcData = $this->_db->fetchRow($srcData);
								if (!empty($acsrcData)) {
									$accsrc = $acsrcData['ACCT_NO'];
									$accsrctype = $acsrcData['ACCT_TYPE'];
								}
											
								if($sessionNameConfrim->traAmount >= $thesoshold){
								
								if(empty($DOC_ID)){
									$this->view->error = true;
												$confirmPage = false;
												$errMessage = 'Document Underlying cannot be blank';
												$this->view->error_msg = 'Document Underlying cannot be blank';
								}else if( ($ACCTSRC_CURRECY == 'USD' && $BENEF_CURRENCY == 'IDR') || ($ACCTSRC_CURRECY == 'IDR' && $BENEF_CURRENCY == 'USD')){
								$selectud= $this->_db->select()
												->from(
													array('A'	 		=> 'T_UNDERLYING'),
													array(
														'DOC_STATUS' 	=> 'A.DOC_STATUS',
														'CCY' 	=> 'A.CCY',
														'AMOUNT' 	=> 'A.AMOUNT'
													)
												)
												//->where("A.CCY		 	 = ?", 'USD')
												->where("A.CUST_ID		 	 = ?", $this->_custIdLogin)
												//->where("A.AMOUNT	= ? ",$sessionNameConfrim->traAmount)
												->where("A.DOC_ID		 	 = ?", $DOC_ID);
											//echo $selectteasury;
											
											$docud = $this->_db->fetchAll($selectud);
											//var_dump($docud);die;
											if(empty($docud)){
												$this->view->error = true;
												$confirmPage = false;
												$errMessage = 'Document Underlying Not Valid';
												$this->view->error_msg = 'Document Underlying Not Valid';
											}else if($docud['0']['CCY']!='USD'){
												$this->view->error = true;
												$confirmPage = false;
												$errMessage = 'Document Underlying Not Matching Transaction';
												$this->view->error_msg = 'Document Underlying Not Matching Transaction';
											}else if($docud['0']['AMOUNT']!=$sessionNameConfrim->traAmount){
												$this->view->error = true;
												$confirmPage = false;
												$errMessage = 'Document Underlying Not Matching Transaction';
												$this->view->error_msg = 'Document Underlying Not Matching Transaction';
											}else if($docud['0']['DOC_STATUS']=='1'){
												$this->view->error = true;
												$confirmPage = false;
												$errMessage = 'Document Underlying is still in the bank checking process';
												$this->view->error_msg = 'Document Underlying is still in the bank checking process';
											}else if($docud['0']['DOC_STATUS']=='3'){
												$this->view->error = true;
												$confirmPage = false;
												$errMessage = 'Document Underlying still need to be repaired';
												$this->view->error_msg = 'Document Underlying still need to be repaired';
											}else if($docud['0']['DOC_STATUS']=='4'){
												$this->view->error = true;
												$confirmPage = false;
												$errMessage = 'Document Underlying is not Approved by Bank';
												$this->view->error_msg = 'Document Underlying is not Approved by Bank';
											}else{
												$confirmPage = true;
											}
											
								}
								}
								//var_dump($TREASURY_NUM);
								if ($EXRATE == '1') {
									//special rate
									$svcInquiry = new Service_Inquiry($this->_userIdLogin, $accsrc, $accsrctype);
									$resultKursEx = $svcInquiry->rateInquiry();

									//rate inquiry for display
									$kurssell = '';
									$kurs = '';
									$book = '';
									if ($resultKursEx['ResponseCode'] == '00') {
										$kursList = $resultKursEx['DataList'];
										$kurssell = '';
										$kurs = '';

										foreach ($kursList as $row) {
											if ($row["currency"] == 'USD') {
												$row["sell"] = str_replace(',', '', $row["sell"]);
												$row["book"] = str_replace(',', '', $row["book"]);
												$kurssell = $row["sell"];
												$book = $row["book"];
												if ($ACCTSRC_CURRECY == 'IDR') {
													$kurs = $row["sell"];
												} else {
													$kurs = $row["buy"];
												}
											}
										}
									} else {
										// die('here1');
										$errMessage 	= 'Invalid Account';
										$this->view->error 		= true;
										$this->view->ERROR_MSG	= $errMessage;
									}
									// print_r($kurs);die;
									$this->view->kurs = Application_Helper_General::convertDisplayMoney($kurs);
									if ($kurs != 'N/A') {
										$plain_kurs = str_replace(',', '', $kurs);
										$traAmountEq = $sessionNameConfrim->traAmount * $plain_kurs;
									} else {
										$traAmountEq = 'N/A';
									}
								} else {
										
									$srcData = $this->_db->select()
									->from(array('M_BENEFICIARY'), array('CURR_CODE'))
									->where('BENEFICIARY_ACCOUNT = ?', $ACBENEF)
									->where('CUST_ID = ?', $this->_custIdLogin)
									->limit(1);
								$benef = $this->_db->fetchRow($srcData);
									
									$selectteasury	= $this->_db->select()
												->from(
													array('A'	 		=> 'T_REQ_SPCRATE'),
													array(
														'DEAL_RATE' 	=> 'A.DEAL_RATE'
													)
												)
												->where("A.FROM_CCY 			 = ?", $ACCTSRC_CURRECY)
												->where("A.TO_CCY		 	 = ?", $benef['CURR_CODE'])
												->where("A.CUST_ID		 	 = ?", $this->_custIdLogin)
												->where("A.TREASURY_REFF		 	 = ?", $TREASURY_NUM);
											//echo $selectteasury;die; 
											$ratedeal = $this->_db->fetchAll($selectteasury);
											if(empty($ratedeal)){
												$this->view->error = true;
											$confirmPage = false;
											$errMessage = 'Treasury code not valid';
											$this->view->error_msg = 'Treasury code not valid';
											}else if($ratedeal[0]['AMOUNT_TRX'] != $sessionNameConfrim->traAmount){
												$this->view->error = true;
											$confirmPage = false;
											$errMessage = 'Transaction Amount not valid';
											$this->view->error_msg = 'Transaction Amount not valid';
											}else{
											//var_dump($ratedeal);die;
											$kurs = $ratedeal[0]['DEAL_RATE'];
											$kursUSD = $ratedeal[0]['DEAL_RATE'];
											}

									$this->view->kurs = Application_Helper_General::convertDisplayMoney($kurs);
									$this->view->kursUSD = Application_Helper_General::convertDisplayMoney($kursUSD);
									if ($kurs != 'N/A' && $kursUSD !=  'N/A') {
										$plain_kurs = str_replace(',', '', $kurs);
										// $plain_kurs = str_replace(',','', $kurs);
										$traAmountEq = ($sessionNameConfrim->traAmount * $kursUSD) * $plain_kurs;
										// var_dump($sessionNameConfrim->traAmount);
										// var_dump($kursUSD);
										// var_dump($plain_kurs);echo '--';
										// print_r($traAmountEq);die;
									} else {
										$traAmountEq = 'N/A';
									}
								}
								// var_dump($kursUSD);
								// echo 'kurs';
								// var_dump($kurs);

								$paramTrxArr[0] = array(
									"TRANSFER_TYPE" 		=> "PB",
									"TRA_AMOUNT" 				=> $sessionNameConfrim->traAmount,
									"TRA_AMOUNTEQ" 				=> $traAmountEq,
									"TRA_MESSAGE" 				=> $TRA_MESSAGE,
									"TRA_ADDMESSAGE" 			=> $TRA_ADDMESSAGE,
									"ACCTSRC" 					=> $sessionNameConfrim->sourceAcct,
									"ACBENEF" 					=> $sessionNameConfrim->benefAcct,
									"ACBENEF_CCY" 				=> $ACCTSRC_CURRECY,
									"ACBENEF_CCY2" 				=> $ACCTSRC_CURRECY2,
									"ACBENEF_BANKNAME" 			=> &$ACBENEF_BANKNAME,

									// for Beneficiary data, except (bene CCY and email), must be passed by reference
									"ACBENEF_ALIAS" 			=> &$ACBENEF_ALIAS,
									"ACBENEF_EMAIL" 			=> $ACBENEF_EMAIL,
									"LLD_IDENTITY"				=> $LLD_IDENTITY,
									"LLD_TRANSACTOR_RELATIONSHIP" => $LLD_TRANSACTOR_RELATIONSHIP,
									"LLD_TRANSACTION_PURPOSE"	 => $LLD_TRANSACTION_PURPOSE,
									"TRA_PURPOSE"				=> $TRA_PURPOSE,
									"BENEFICIARY_CITIZENSHIP"	 => $BENEFICIARY_CITIZENSHIP,
									"BENEFICIARY_RESIDENT"   	 => $BENEFICIARY_RESIDENT,
									"BENEFICIARY_CATEGORY"		 => $BENEFICIARY_CATEGORY,
									"BENEFICIARY_ID_TYPE"		 => $BENEFICIARY_ID_TYPE,
									"BENEFICIARY_ID_NUMBER"		 => $BENEFICIARY_ID_NUMBER,
									"BENEFICIARY_CCY"			 => $BENEF_CURRENCY,
									"ACBENEF_CURRENCY"			 => $ACBENEF_CURRENCY,
									"ACCTSRC_ID_TYPE"			 => $accsrcData['0']['BENEFICIARY_ID_TYPE'],
									"ACCTSRC_ID_NUMBER"			 => $accsrcData['0']['BENEFICIARY_ID_NUMBER'],
									"ACCTSRC_CITIZENSHIP"		 => $accsrcData['0']['BENEFICIARY_CITIZENSHIP'],
									"ACCTSRC_RESIDENT'"			 => $accsrcData['0']['BENEFICIARY_RESIDENT'],
									"ACCTSRC_CATEGORY"			 => $accsrcData['0']['BENEFICIARY_CATEGORY'],
									"AGREEMENT"					 => $AGREEMENT,
									"PS_EMAIL"					=> $TRA_EMAIL,
									"PS_NOTIF"					=> $TRA_NOTIF,
									"PS_SMS"					=> $TRA_SMS,
									"TREASURY_NUM"				=> $TREASURY_NUM,
									"DOC_ID"					=> $DOC_ID

								);
							}
							// print_r($SAVEBENE);echo '<-here';die;
							if ($isConfirmPage != 1) {
								$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
								$sessionNameConfrim->ACBENEF_BANKNAME = $paramTrxArr[0]['ACBENEF_BANKNAME'];
							} else {
								$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
								$sessionNameConfrim->ACBENEF_BANKNAME;
							}
							if (empty($sessionNameConfrim->SAVEBENE)) {
								$sessionNameConfrim->SAVEBENE = $SAVEBENE;
							}


							$paramSettingID = array('range_futuredate', 'auto_release_payment');

							$settings->setSettings(null, $paramSettingID);	// Zend_Registry => 'APPSETTINGS'



							$validate->setFlagConfirmPage(($isConfirmPage == 1) ? TRUE : FALSE); //tujuan untuk set supaya jangan manggil inquiry lg di page ke 2
							// echo "<pre>";
							// print_r($paramTrxArr);

							// print_r($paramPayment);die();

							if (!$this->view->error) { 
								if ($CROSS_CURR == 1) {
									// if($isConfirmPage != 1){
									$resAcct = array();
									// print_r($paramPayment);
									// print_r($paramTrxArr);die;
									$resultVal	= $validate->checkCreateValas($paramPayment, $paramTrxArr, $resAcct);
									$sessionNameConfrim->ACBENEF = $resAcct;
									// }
								} else {
									$resWs = array();
									// echo "<pre>";
									// var_dump($paramPayment);
									// var_dump($paramTrxArr);die;
									$resultVal	= $validate->checkCreate($paramPayment, $paramTrxArr, $resWs);
									// print_r($resWs);die;
									$sessionNameConfrim->ACBENEF = $resWs;
								}
							}

							if ($TRA_EMAIL) {
								$validateemail = new validate;
								$cek_multiple_email = $validateemail->isValidEmailMultiple($TRA_EMAIL);
							} else {
								$cek_multiple_email = true;
							}


							// $resultVal	= true;
							if (!empty($resultVal) && $CROSS_CURR == 1 && $cek_multiple_email) {

								$accsrcDetail['ACCTSRC_CITIZENSHIP'] = $resultVal['ACSRC']['Citizenship'];
								$accsrcDetail['ACCTSRC_RESIDENT'] = $resultVal['ACSRC']['National'];
								$accsrcDetail['ACCTSRC_CATEGORY'] = $resultVal['ACSRC']['Category'];
								$accsrcDetail['ACCTSRC_ID_NUMBER'] = $resultVal['ACSRC']['IdentificationNumber'];
								$accsrcDetail['ACCTSRC_ID_TYPE'] = $resultVal['ACSRC']['IdentificationType'];

								$BENEFICIARY_CITIZENSHIP = $resultVal['BENE']['0']['Citizenship'];
								$BENEFICIARY_RESIDENT = $resultVal['BENE']['0']['National'];

								$BENEFICIARY_CATEGORY = $resultVal['BENE']['0']['Category'];
								$BENEFICIARY_ID_NUMBER = $resultVal['BENE']['0']['IdentificationNumber'];
								$BENEFICIARY_ID_TYPE = $resultVal['BENE']['0']['IdentificationType'];

								$KURS_CCY = $resultVal['KURS_CCY'];
							}

							if ($CROSS_CURR == 1) {
								$sourceAccountType = $resWs['ACCT_TYPE'];
							} else {
								$sourceAccountType = $resWs['ACCT_TYPE'];
							}
							// print_r($resWs);
							// print_r($sourceAccountType);

							if ($isConfirmPage != 1) {
								$this->view->sourceAcctTypeGet = $sourceAccountType;
							} else {
								$sourceAcctTypeGet 		= $this->_getParam('sourceAcctType');
								$this->view->sourceAcctTypeGet = $sourceAcctTypeGet;
							}
							$sourceAcctTypeGet = $sourceAccountType;
							// print_r($sourceAcctTypeGet);die;
							// print_r('gere');die;
							// var_dump($this->view->error);
							// var_dump($validate->getErrorTrxMsg());die;
							if ($validate->isError() === false && $this->view->error === false)	// payment data is valid
							{
								// die('here');
								$payment 		= $validate->getPaymentInfo();
								// var_dump($USE_CONFIRM_PAGE);
								// var_dump($isConfirmPage);die;
								if ($USE_CONFIRM_PAGE == true && $isConfirmPage == false) {

									// echo 'sini';
									if ($CROSS_CURR == 1) {
										if ($ACCTSRC_CURRECY2 == "USD" && $BENEF_CURRENCY == "USD") {
										} else {

											$KURS_CCY = Application_Helper_General::convertDisplayMoney($kurs);
											$KURS_USD = Application_Helper_General::convertDisplayMoney($kursUSD);
											// die('here');
											echo "
										      <script type=\"text/javascript\">
										      	var textdesc = \"Rate to be used for this transaction is IDR " . Application_Helper_General::convertDisplayMoney($kurs) . " rate USD " . $KURS_USD . " \";
		    									var textalert = textdesc;
										       	alert(textalert);
										      </script>
										     ";
											// end get e-rate for notif kurs

										} // notif kurs
									}

									$isConfirmPage 	= true;

									// echo $isConfirmPage;die();
									$ACCTSRC_CCY   	= $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["CCY_ID"];
									$ACCTSRC_NAME   = $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["ACCT_NAME"];
									// print_r($payment);
									if ($CROSS_CURR == 1) {
										$ACCTSRC_ALIAS  = $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["ACCT_ALIAS"];
										$ACCTSRC_view 	= Application_Helper_General::viewAccount($sessionNameConfrim->sourceAcct, $ACCTSRC_CCY, $ACCTSRC_NAME, $ACCTSRC_ALIAS, $ACCTSRC_TYPE);
									} else {
										$ACCTSRC_view 	= Application_Helper_General::viewAccount($sessionNameConfrim->sourceAcct, $ACCTSRC_CCY, $ACCTSRC_NAME, "", $ACCTSRC_TYPE);
									}
									// print_r($ACCTSRC_view);die;
									$ACCTSRC_TYPE   = $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["ACCT_TYPE"];


									$validate->__destruct();
									unset($validate);

									require_once 'General/Charges.php';
									$trfType		= "PB";
									if($CROSS_CURR != '1'){
									$chargesObj 	= Charges::factory($this->_custIdLogin, $trfType);
									$paramCharges 	= array("accsrc" => $sessionNameConfrim->sourceAcct, "transferType" => $trfType ); 
									$chargesAMT 	= $chargesObj->getCharges($paramCharges); 
									}else{
										//var_dump($ACCTSRC_CCY);
										//var_dump($ACCTSRC_CCY);die;
										if($BENEF_CURRENCY == $ACCTSRC_CCY){
											$chargesObj 	= Charges::factory($this->_custIdLogin, $trfType);
											$paramCharges 	= array("accsrc" => $sessionNameConfrim->sourceAcct, "transferType" => $trfType ,"ccy" => $BENEF_CURRENCY);
											
											$chargesAMT 	= $chargesObj->getCharges($paramCharges); 
											//var_dump($chargesAMT);die;
										}else{
											$chargesAMT = 0;
										}
										
										
									}
									$chargesCCY 	= $ACCTSRC_CCY;
								} else {
									$validate->__destruct();
									unset($validate);
									// echo '<pre>';
									// print_r($sessionNameConfrim);die;
									// print_r($TRA_NOTIF);die;
									$ACCTSRC_CCY   	= $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["CCY_ID"];

									if ($CROSS_CURR == 1) {
										$ACCTSRC_NAME   = $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["ACCT_NAME"];
										$ACCTSRC_ALIAS  = $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["ACCT_ALIAS"];
										$ACCTSRC_TYPE   = $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["ACCT_TYPE"];
										$ACCTSRC_view 	= Application_Helper_General::viewAccount($sessionNameConfrim->sourceAcct, $ACCTSRC_CCY, $ACCTSRC_NAME, $ACCTSRC_ALIAS, $ACCTSRC_TYPE);


										$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');

										$param = array();
										$param['PS_SUBJECT'] 					= '';
										if($ACBENEF_CURRENCY == 'USD' || $ACCTSRC_CCY == 'USD'){
											$PS_CCY = 'USD';
										}else{
											$PS_CCY = 'IDR';
										}
										$param['PS_CCY'] 						= $PS_CCY;
										$param['PS_EFDATE'] 					= Application_Helper_General::convertDate($PS_EFDATE, $this->_dateDBFormat, $this->_dateDisplayFormat);
										$param['EXPIRY_DATE'] 					= Application_Helper_General::convertDate($EXPIRY_DATE, $this->_dateDBFormat, $this->_dateDisplayFormat);
										$param['TRA_AMOUNT'] 					= $sessionNameConfrim->traAmount;
										$param['TRA_MESSAGE'] 					= $TRA_MESSAGE;
										$param['TRA_ADDMESSAGE'] 					= $TRA_ADDMESSAGE;
										$param['SOURCE_ACCOUNT_NAME'] 			= $ACCTSRC;
										$param['SOURCE_ACCOUNT'] 				= $sessionNameConfrim->sourceAcct;
										$param['SOURCE_ACCOUNT_CCY'] 			= $ACCTSRC_CURRECY;
										$param['LLD_IDENTITY'] 					= (isset($LLD_IDENTITY))		? $LLD_IDENTITY		: '';
										$param['LLD_TRANSACTION_PURPOSE'] 		= (isset($LLD_TRANSACTION_PURPOSE))		? $LLD_TRANSACTION_PURPOSE		: '';
										$param['LLD_TRANSACTOR_RELATIONSHIP'] 	= (isset($LLD_TRANSACTOR_RELATIONSHIP))		? $LLD_TRANSACTOR_RELATIONSHIP		: '';

										$param['$TRA_PURPOSE'] 					= $TRA_PURPOSE;

										$param['BENEFICIARY_ACCOUNT'] 			= $sessionNameConfrim->benefAcct; //ambil dari session
										$param['BENEFICIARY_ACCOUNT_CCY'] 		= $BENEF_CURRENCY;
										$param['BENEFICIARY_ACCOUNT_NAME'] 		= $ACBENEF_BANKNAME;
										$param['BENEFICIARY_ALIAS_NAME'] 		= $ACBENEF_ALIAS;
										$param['BENEFICIARY_EMAIL'] 			= $ACBENEF_EMAIL;
										$param['BOOK_RATE_SELL'] 				= $book;
										$param['_addBeneficiary'] 				= $paramPayment["_addBeneficiary"];
										$param['_beneLinkage'] 					= $paramPayment["_beneLinkage"];
										$param['_priviCreate'] 					= 'CRSP';

										if ($kurs != 'N/A') {
											$plain_kurs = str_replace(',', '', $kurs);
											$traAmountEq 						= $sessionNameConfrim->traAmount * $plain_kurs;
										} else {
											$traAmountEq 						= 'N/A';
										}

										$param['TRA_AMOUNTEQ'] 					= $traAmountEq;
										$param['sourceAccountType'] 			= $sourceAcctTypeGet;
										$param['PS_EMAIL'] = $TRA_EMAIL;
										$param['PS_NOTIF'] = $TRA_NOTIF;
										$param['PS_SMS'] = $TRA_SMS;
										$param['TREASURY_NUM'] = $TREASURY_NUM;
										$param['DOC_ID']	= $DOC_ID;
										// die('here2');
										// print_r($param);die;
									} else {
										$ACCTSRC_NAME   = $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["ACCT_NAME"];
										$ACCTSRC_ALIAS  = $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["ACCT_ALIAS"];
										$ACCTSRC_TYPE   = $payment["acctsrcArr"][$sessionNameConfrim->sourceAcct]["ACCT_TYPE"];
										// print_r($sessionNameConfrim->ACBENEF['ACBENEF']);
										// print_r($sessionNameConfrim->traAmount);die();
										$param = array();
										
										$PS_CCY = 'IDR';
										
										$param['PS_SUBJECT'] 					= $PS_SUBJECT;
										$param['PS_CCY'] 						= $PS_CCY;
										$param['PS_EFDATE'] 					= Application_Helper_General::convertDate($PS_EFDATE, $this->_dateDBFormat, $this->_dateDisplayFormat);
										$param['TRA_AMOUNT'] 					= $sessionNameConfrim->traAmount;
										$param['TRANSFER_FEE'] 					= $TRANSFER_FEE_num;
										$param['TRA_MESSAGE'] 					= $TRA_MESSAGE;
										$param['TRA_REFNO'] 					= $TRA_REFNO;
										$param['SOURCE_ACCOUNT'] 				= $sessionNameConfrim->sourceAcct;
										$param['BENEFICIARY_ACCOUNT'] 			= $sessionNameConfrim->benefAcct;
										$param['BENEFICIARY_ACCOUNT_CCY'] 		= $ACBENEF_CCY;
										$param['BENEFICIARY_ACCOUNT_NAME'] 		= $sessionNameConfrim->ACBENEF_BANKNAME;
										$param['BENEFICIARY_ALIAS_NAME'] 		= $ACBENEF_ALIAS;
										$param['BENEFICIARY_EMAIL'] 			= $ACBENEF_EMAIL;
										$param['_addBeneficiary'] 				= $paramPayment["_addBeneficiary"];
										$param['_beneLinkage'] 					= $paramPayment["_beneLinkage"];
										$param['_priviCreate'] 					= 'CRSP';
										$param['sourceAccountType'] 			= $sourceAcctTypeGet;
										$param['PS_SAVEBENE']	= $sessionNameConfrim->SAVEBENE;
										$param['RATE'] 							= '0';
										$param['RATE_BUY'] 						= '0';
										$param['BOOKRATE'] 						= '0';
										$param['BOOKRATE_BUY'] 					= '0';
										$param['TRA_AMOUNTEQ'] = '0';
										$param['PS_EMAIL'] = $TRA_EMAIL;
										$param['PS_NOTIF'] = $TRA_NOTIF;
										$param['PS_SMS'] = $TRA_SMS;
										// print_r($param);die;
									}

									// print_r($param);
									// die('here');

									// $sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
									// echo '<pre>';
									// print_r($sessionNameConfrim);die;
									try {
										$SinglePayment = new SinglePayment($PS_NUMBER, $this->_custIdLogin, $this->_userIdLogin);

										if (!empty($PS_NUMBER)) {
											$SinglePayment->isRepair = true;
										}


										if ($TrfDateType == 3) {
											//$repetition = $sessionNameConfrim->repetition;

											//daily
											if ($repetition == 1) {
												
												//$repeatOn = $sessionNameConfrim->report_day;
												$repeatOn = $this->_getParam('report_day');
												$endDate = $this->_getParam('PS_ENDDATEPERIODIC');
												//$endDate = $sessionNameConfrim->endDate;
												$efDate = $this->_getParam('PS_EFDATE1');

												//get from start date
												$nextDate = DateTime::createFromFormat('d/m/Y', $efDate);
												$nextDate = $nextDate->format('Y-m-d');

												$dataday = $repeatOn;
																	$arrday = array(
																		'0' => 'sunday',
																		'1' => 'monday',
																		'2' => 'tuesday',
																		'3' => 'wednesday',
																		'4' => 'thursday',
																		'5' => 'friday',
																		'6' => 'saturday'
																	);

																	// get number of day in a week of startdate
																	$datenumb = date("w", strtotime($nextDate));
											//var_dump($nextDate);						
											//var_dump($datenumb);die;												
																		foreach ($dataday as $key => $value) {
																			if($value== '0' || $value == '6'){
																					unset($dataday[$key]);
																			}
																		}
																			//var_dump($efDate);
																			//var_dump($dataday);
																		if (!empty($dataday)) {
																			$nextDateArr = array();
																		foreach ($dataday as $key => $value) {

																			if ($datenumb == 6 || $nextdatenumb == 6) {
																				$nextdatenumb = 0;
																			//	continue;
																			}
																			else{
																				$nextdatenumb++;
																			}

																			
																			//else if($nextdatenumb == $value){
																				$string = 'next ' . $arrday[$value];
																				//var_dump($string);die;
																				$nextDate = DateTime::createFromFormat('d/m/Y', $efDate);
																				$nextDate->modify($string);
																				$nextDate = $nextDate->format('Y-m-d');
																				
																				$nextDatenow = DateTime::createFromFormat('d/m/Y', $efDate);
																				$nextDatenow = $nextDatenow->format('Y-m-d');
																				$datenumb = date("w", strtotime($nextDatenow));
																				//var_dump($nextDatenow);
																				//var_dump($datenumb);
																				//var_dump($value);
																				//echo '-';
																				if ($datenumb == $value) {
																					$nextDateArr[] = $nextDatenow;
																				//	var_dump($nextDate);die('1');
																					break;

																				}
																				$nextDateArr[] = $nextDate;
																				//var_dump($nextDate);die('2');
																			//	break;
																		//	}
																		//	}
																			
																			
																			
																		}
																		//var_dump($nextDateArr);die;
																		if(!empty($nextDateArr)){
																				usort($nextDateArr, function($a, $b) {
																						$dateTimestamp1 = strtotime($a);
																						$dateTimestamp2 = strtotime($b);

																						return $dateTimestamp1 < $dateTimestamp2 ? -1: 1;
																					});
																			}
																			//if(!empty($nextDateArr['1'])){
																			//	$nextDate = $nextDateArr['1'];
																		//	}else{
																				$nextDate = $nextDateArr['0'];
																		//	}
																			
																			//$param['PS_EFDATE'] = $nextDateArr['0'];
																	}
																	
												
											}
											//weekly
											else if ($repetition == 2) {

												$repeatOn = $this->_getParam('report_day');
												$endDate = $this->_getParam('PS_EFDATE1');

												$repeatEvery = $repeatOn['0'];

												//get from start date
												$nextDate = DateTime::createFromFormat('d/m/Y', $efDate);
												$nextDate = $nextDate->format('Y-m-d');

												$dataday = $repeatOn;
												$arrday = array(
													'0' => 'sunday',
													'1' => 'monday',
													'2' => 'tuesday',
													'3' => 'wednesday',
													'4' => 'thursday',
													'5' => 'friday',
													'6' => 'saturday'
												);

												// get number of day in a week of startdate
												$datenumb = date("w", strtotime($nextDate));

												if (!empty($dataday)) {
													foreach ($dataday as $key => $value) {

														if ($datenumb == $value) {
															$nextDate = $nextDate;
															break;
														}
														else {
															$string = 'next ' . $arrday[$dataday[0]];
															// var_dump($string);die();
															$nextDate = DateTime::createFromFormat('d/m/Y', $efDate);
															$nextDate->modify($string);
															$nextDate = $nextDate->format('Y-m-d');

															break;
														}
													}
												}
											}
											//monthly
											else if ($repetition == 3) {
												$repeatEvery = $this->_getParam('selectrepeat');
												// $repeatOn = $this->_getParam('PS_REPEATON');
												$endDate = $this->_getParam('PS_ENDDATEPERIODIC');
												//$repeatEvery = $sessionNameConfrim->repeatEvery;
												//$endDate = $sessionNameConfrim->endDate;

												//get from start date
												$nextDate = DateTime::createFromFormat('d/m/Y', $this->_getParam('PS_EFDATE1'));
												$nextDate = $nextDate->format('Y-m-d');

												// get date of startdate
												$datenumb = date("d", strtotime($nextDate));
											
												if($repeatEvery == 'last'){
												
												$nextDate = date("Y-m-t", strtotime($nextDate));
												
												//die;
												}else{
												
													if ($datenumb == $repeatEvery) {
														$nextDate = $nextDate;
													}
													else if($datenumb < $repeatEvery){
														$nextDateExplode = explode('-', $nextDate);

														$nextDateExplode[2] = $repeatEvery;

														$nextDate = implode('-', $nextDateExplode);
													}
													else if($datenumb > $repeatEvery) {

														$nextDateExplode = explode('-', $nextDate);

														$nextDateExplode[2] = $repeatEvery;

														$nextDate = implode('-', $nextDateExplode);

														$nextDate = date('Y-m-d', strtotime("+1 month", strtotime($nextDate)));
													}
												}
											}
										}else{
											
											$nextDate = $PS_EFDATE;
										}
										
										if ($TrfDateType == 3) {
											// echo 'here';
											//$efDate = $this->_getParam('PS_EFDATE1');
											$start = $this->_getParam('PS_EFDATE1');
											$STR_DATE = DateTime::createFromFormat('d/m/Y', $start);
											$START_DATE = $STR_DATE->format('Y-m-d');

											$EXPIRY_DATE = DateTime::createFromFormat('d/m/Y', $this->_getParam('PS_ENDDATEPERIODIC'));
											$expDate = $EXPIRY_DATE->format('Y-m-d'); 

											if (!empty($TrfDateType == 3 && !empty($this->_getParam('PS_ENDDATEPERIODIC')))) {
												$EF_DATE = DateTime::createFromFormat('d/m/Y', $this->_getParam('PS_ENDDATEPERIODIC'));
												$endDate = $EF_DATE->format('Y-m-d');
												// var_dump($endDate);die;
											}

											if (empty($this->_getParam('PS_ENDDATEPERIODIC'))) {
												$endDate = join('-', array_reverse(explode('/', date('d/m/Y'))));
												$EF_DATE = DateTime::createFromFormat('d/m/Y', $endDate);
												$endDate = $EF_DATE->format('Y-m-d');
											}
										}

										// add by hamdan
										if ($TrfDateType == 3) { 
 
											// Insert T_PERIODIC//
											$START_DATE = join('-', array_reverse(explode('/', $START_DATE)));
											$EXPIRY_DATE = join('-', array_reverse(explode('/', $EXPIRY_DATE)));
											//$EF_DATE = DateTime::createFromFormat('d/m/Y', $endDate);
											//$endDate = $EF_DATE->format('Y-m-d');
											$insertPeriodic = array(
												'PS_EVERY_PERIODIC' 	=> (int) $repeatEvery,
												'PS_PERIODIC_NUMBER'	=> $this->generateTransactionID(),
												'PS_EVERY_PERIODIC_UOM' => $repetition, // 1: daily, 2: weekly, 3: monthly, 4: yearly, 5: every day of, 6: every date of
												'PS_PERIODIC_STARTDATE' => $START_DATE,
												'PS_PERIODIC_ENDDATE'	=> $endDate,
												'PS_PERIODIC_NEXTDATE'	=> Application_Helper_General::convertDate($nextDate, $this->_dateDBFormat, $this->_dateDisplayFormat),
												'PS_PERIODIC_STATUS' 	=> 2,	// 2 INPROGRESS KALO BELUM BERAKHIR, 1 COMPLETE KALO SUDAH HABIS END DATE, 0 PERIODIC INI DI CANCEL
												'USER_ID' 				=> $this->_userIdLogin,
												'PS_CREATED' 			=> new Zend_Db_Expr("GETDATE()"),
											);
											  
											//Zend_Debug::dump($insertPeriodic); die('ghe'); 
											$this->_db->insert('T_PERIODIC', $insertPeriodic);
											$psPeriodicID =  $this->_db->lastInsertId();
											$param['PS_PERIODIC'] = $psPeriodicID;


											// Insert T_PERIODIC_DETAIL
											$select	= $this->_db->select()
												->from(
													array('A'	 		=> 'M_CUSTOMER_ACCT'),
													array(
														'ACCT_NAME' 	=> 'A.ACCT_NAME',
														'ACCT_ALIAS' 	=> 'A.ACCT_ALIAS_NAME',
														'ACCT_CCY' 	=> 'A.CCY_ID',
													)
												)
												->where("A.CUST_ID 			 = ?", $this->_custIdLogin)
												->where("A.ACCT_NO		 	 = ?", $param['SOURCE_ACCOUNT']);

											$accsrc = $this->_db->fetchRow($select);

											$sourceAccountName 		=  $accsrc['ACCT_NAME'];
											$sourceAccountAliasName =  $accsrc['ACCT_ALIAS'];
											$sourceAccountCCY 		=  $accsrc['ACCT_CCY'];
											// echo "<pre>";
											// print_r($param);die;
											$insertPeriodicDetail = array(
												'PS_PERIODIC' 				=> $psPeriodicID,
												'SOURCE_ACCOUNT' 			=> $param['SOURCE_ACCOUNT'], // 1: daily, 2: weekly, 3: monthly, 4: yearly, 5: every day of, 6: every date of
												'SOURCE_ACCOUNT_CCY' 		=> $sourceAccountCCY,
												'SOURCE_ACCOUNT_NAME' 		=> $sourceAccountName,
												'SOURCE_ACCOUNT_TYPE' 		=> $param['sourceAccountType'],
												'SOURCE_ACCOUNT_BANK_CODE' 	=> "",
												'SOURCE_ACCOUNT_BANK_NAME' 	=> "",
												'BENEFICIARY_ACCOUNT' 		=> $param['BENEFICIARY_ACCOUNT'],
												'BENEFICIARY_ACCOUNT_CCY' 	=> $param['BENEFICIARY_ACCOUNT_CCY'],
												'BENEFICIARY_ACCOUNT_NAME' 	=> $param['BENEFICIARY_ACCOUNT_NAME'],
												'BENEFICIARY_EMAIL' 		=> $param['BENEFICIARY_EMAIL'],
												'BENEFICIARY_BANK_CODE' 	=> "",
												'BENEFICIARY_BANK_NAME' 	=> "",
												'TRA_AMOUNT' 				=> $param['TRA_AMOUNT'],
												'TRA_MESSAGE' 				=> $param['TRA_MESSAGE'],
												'TRANSFER_TYPE' 			=> 0,	// 0 : Inhouse, 1: RTGS, 2: SKN
											);
											$this->_db->insert('T_PERIODIC_DETAIL', $insertPeriodicDetail);
											
											$report_day = $this->_getParam('report_day');
											//var_dump($data);die;
											if (!empty($report_day)) {

												$filter_day = array_unique($report_day);
												foreach ($filter_day as $keyday => $valday) {
													$insertPeriodicday = array(
														'PERIODIC_ID' => $psPeriodicID,
														'DAY_ID'		=> $valday
													);

													$this->_db->insert('T_PERIODIC_DAY', $insertPeriodicday);
												}
											}
										}
										// end

										if($TrfDateType == 3){
											$param['PS_EFDATE'] 	= Application_Helper_General::convertDate($nextDate, $this->_dateDBFormat, $this->_dateDisplayFormat);
										}

										if ($CROSS_CURR == 1) {
											$acct_data = $model->getSourceAccount($sessionNameConfrim->sourceAcct);
											$bene_data = $model->getBeneficiaryByAccount($param['BENEFICIARY_ACCOUNT']);

											if (empty($resultVal['BENE']['0']['accountName'])) {

												$param['BENEFICIARY_CITIZENSHIP'] = $this->_request->getParam('ACBENEF_CITIZENSHIP_CODE');
												$param['BENEFICIARY_RESIDENT'] = $this->_request->getParam('ACBENEF_NATION_CODE');
												$param['BENEFICIARY_CATEGORY'] = $this->_request->getParam('ACBENEF_CATEGORY_CODE');
												$param['BENEFICIARY_ID_TYPE']  = $this->_request->getParam('ACBENEF_IDENTY');
												$param['BENEFICIARY_ID_NUMBER'] = $this->_request->getParam('ACBENEF_IDENTYNUM');
												$param['ACCT_CITIZENSHIP'] = $this->_request->getParam('ACCTSRC_CITIZENSHIP_CODE');
												$param['ACCT_RESIDENT'] = $this->_request->getParam('ACCTSRC_RESIDENT_CODE');
												$param['ACCT_CATEGORY'] = $this->_request->getParam('ACCTSRC_CATEGORY_CODE');
												$param['ACCT_ID_TYPE'] = $this->_request->getParam('ACCTSRC_ID_TYPE');
												$param['ACCT_ID_NUMBER'] = $this->_request->getParam('ACCTSRC_ID_NUMBER');
											}

											$param['PS_SUBJECT'] = $PS_SUBJECT;
											$param['ACCT_NAME'] = $acct_data['0']['ACCT_NAME'];
											$param['ACCT_CCY'] = $acct_data['0']['CCY_ID'];
											$param['USER_BANK_CODE'] = $bene_data['0']['USER_BANK_CODE'];
											$param['PLAIN_KURS'] = $plain_kurs;
											$param['PS_SAVEBENE']	= $sessionNameConfrim->SAVEBENE;
											//echo '<pre>';
											//var_dump($param);die;
											$resWs = array();
											$result = $SinglePayment->createPaymentWithinValas($param, $resWs);

											$ns = new Zend_Session_Namespace('FVC');
											$ns->backURL = '/' . $this->view->modulename . '/' . $this->view->controllername . '/index';
											$ACCTSRC_view 	= Application_Helper_General::viewAccount($param['SOURCE_ACCOUNT'], $ACCTSRC_CCY, $ACCTSRC_NAME, $ACCTSRC_ALIAS, $ACCTSRC_TYPE);

											$sessionNamespace->lldidenty 	= $LLD_IDENTITY;
											$sessionNamespace->lldrelation 	= $LLD_TRANSACTOR_RELATIONSHIP;

											$sessionNamespace->psNumber 	= $result;
											$sessionNamespace->ACCTSRC_view 	= $ACCTSRC_view;
											$sessionNamespace->isConfirmPage 	= $isConfirmPage = true;
											$sessionNamespace->isResultPage 	= $isResultPage = true;
											$payReff = $PS_NUMBER = $result;
										} else {
											//echo '<pre>';
											 //print_r($param);die;
											$result = $SinglePayment->createPaymentWithin($param);
											$ACCTSRC_view 	= Application_Helper_General::viewAccount($param['SOURCE_ACCOUNT'], $ACCTSRC_CCY, $ACCTSRC_NAME, $ACCTSRC_ALIAS, $ACCTSRC_TYPE);

											$ns = new Zend_Session_Namespace('FVC');

											if (!empty($PS_NUMBER))
												$ns->backURL = '/paymentworkflow/requestrepair/index/m/1';
											else
												$ns->backURL = '/' . $this->view->modulename . '/' . $this->view->controllername . '/index';
										}

										// die;

										// var_dump('here');die;
										if ($this->_custSameUser) {

											$paramSQL = array(
												"WA" 				=> false,
												"ACCOUNT_LIST" 	=> $this->_accountList,
												"_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
											);

											// get payment query
											$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
											$select   = $CustUser->getPayment($paramSQL);
											$select->where('P.PS_NUMBER = ?', (string) $result);
											// echo $select;
											$pslip = $this->_db->fetchRow($select);
											$settingObj = new Settings();
											$setting = array(
												"COT_SKN"  			=> $settingObj->getSetting("cut_off_time_skn", 		"00:00:00"),
												"COT_RTGS" 			=> $settingObj->getSetting("cut_off_time_rtgs", 	"00:00:00"),
												"COT_BULK" 			=> $settingObj->getSetting("cut_off_time_bulkpayments", 	"00:00:00"),
												"COT_BI" 				=> $settingObj->getSetting("cut_off_time_bi", 	"00:00:00"),
												"COT_REMIT"			=> $settingObj->getSetting("cut_off_time_remittance", "00:00:00"),
												'start_remit'			=> $settingObj->getSetting("starting_time_remittance", "00:00:00"),
												"range_futuredate" 	=> $settingObj->getSetting("range_futuredate", 		"0"),
												"auto_release" 		=> $settingObj->getSetting("auto_release_payment",	"0"),
												"_dateFormat" 			=> $this->_dateDisplayFormat,
												"_dateDBFormat" 		=> $this->_dateDBDefaultFormat,
												"_transfertype" 		=> array_flip($this->_transfertype["code"]),
											);

											$isBackDated  = ($pslip['efdate'] < date('Y-m-d'));
											$isFutureDate = ($pslip['efdate'] > date('Y-m-d'));

											$caseTransferType 	= Application_Helper_General::caseArray($this->_transfertype);
											$caseTransferStatus = Application_Helper_General::caseArray($this->_transferstatus);

											$app = Zend_Registry::get('config');
											$appBankname = $app['app']['bankname'];

											$selectTrx = $this->_db->select()
												->from(
													array('TT' => 'T_TRANSACTION'),
													array(
														'ACCTSRC'				=> 'TT.SOURCE_ACCOUNT',
														'ACCTSRC_CCY'			=> 'TT.SOURCE_ACCOUNT_CCY',
														'ACCTSRC_NAME'			=> new Zend_Db_Expr("
																							CASE WHEN TT.SOURCE_ACCOUNT_ALIAS_NAME is null THEN TT.SOURCE_ACCOUNT_NAME
																								 ELSE CONCAT(TT.SOURCE_ACCOUNT_NAME )
																							END"),
														//'ACCTSRC_ALIAS'		=> 'TT.SOURCE_ACCOUNT_ALIAS_NAME',
														'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
														'ACBENEF_NAME'			=> new Zend_Db_Expr("
																								CONCAT(TT.BENEFICIARY_ACCOUNT_NAME )"),
														'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
														'ACBENEF_EMAIL'			=> 'TT.BENEFICIARY_EMAIL',
														'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
														'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',
														'TRA_REFNO'				=> 'TT.TRA_REFNO',
														'TRANSFER_TYPE'			=> 'TT.TRANSFER_TYPE',
														'TRANSFER_TYPE_disp'	=> new Zend_Db_Expr("CASE TT.TRANSFER_TYPE $caseTransferType ELSE 'N/A' END"),
														'TRA_STATUS'			=> 'TT.TRA_STATUS',
														'TRA_STATUS_disp'		=> new Zend_Db_Expr("CASE TT.TRA_STATUS $caseTransferStatus ELSE 'N/A' END"),
														'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
														'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
														'TRA_REMAIN'			=> 'TT.TRA_REMAIN',
														'EQUIVALENT_AMOUNT_IDR'	=> 'TT.EQUIVALENT_AMOUNT_IDR',
														'CLR_CODE'				=> 'TT.CLR_CODE',
														'TT.RATE',
														'TT.PROVISION_FEE',
														'TT.NOSTRO_NAME',
														'TT.FULL_AMOUNT_FEE',
														'C.PS_CCY', 'C.CUST_ID',
														'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
														'EQUIVALEN_USD'			=> 'TT.EQUIVALENT_AMOUNT_USD',
														'EQUIVALEN_IDR'			=> 'TT.EQUIVALENT_AMOUNT_IDR',
														'BANK_NAME'				=> new Zend_Db_Expr("
																								CASE WHEN TT.TRANSFER_TYPE = '0' THEN '" . $appBankname . "'
																								WHEN TT.TRANSFER_TYPE = '9' THEN CONCAT('" . $appBankname . "',' - ' ,TT.BENEFICIARY_BANK_NAME)
																								 WHEN TT.TRANSFER_TYPE = '10' THEN CONCAT('" . $appBankname . "',' - ',TT.BENEFICIARY_BANK_NAME)
																									 ELSE TT.BENEFICIARY_BANK_NAME
																								END"),
														'BALANCE_TYPE'			=> new Zend_Db_Expr("(SELECT BALANCE_TYPE
					        																FROM T_PERIODIC_DETAIL Y
					        																inner join T_PSLIP Z
					        																on Y.PS_PERIODIC = Z.PS_PERIODIC
					        																where
					        																Z.PS_NUMBER = TT.PS_NUMBER limit 1)"),
													)
												)
												->joinLeft(array('C' => 'T_PSLIP'), 'C.PS_NUMBER = TT.PS_NUMBER', array())
												->where('TT.PS_NUMBER = ?', $result);
											// echo $selectTrx;
											$paramTrxArr = $this->_db->fetchAll($selectTrx);

											$validate  	  = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin, $result);
											$paramPayment = array_merge($pslip, $setting);
											// echo '<pre>';
											// print_r($paramPayment);
											// print_r($paramTrxArr);
											// die;
											$check 		  = $validate->checkRelease($paramPayment, $paramTrxArr);
											$infoWarnOri = (!empty($check['infoWarning']) ? '*) ' . $check['infoWarning'] : '');
											$sessionNameConfrim->infoWarnOri = $infoWarnOri;

											if ($validate->isError() === true) {
												$error = true;
												$errorMsg = array_merge($errorMsg, $check["errorMsg"]);
											}

											$Payment = new Payment($result, $this->_custIdLogin, $this->_userIdLogin);
											// if ($this->_hasPriviReleasePayment){
											$resultRelease = $Payment->releasePayment();
											// print_r($resultRelease);
											$this->view->ps_numb = $result;
											$this->view->hidetoken = true;
											if ($resultRelease['status'] == '00') {
												$ns = new Zend_Session_Namespace('FVC');
												$ns->backURL = $this->view->backURL;
												$this->view->releaseresult = true;
												// $this->_redirect('/notification/success/index');
											} else {
												$this->view->releaseresult = false;
												$this->_helper->getHelper('FlashMessenger')->addMessage($result);
												//					$this->_helper->getHelper('FlashMessenger')->addMessage($resultRelease['status']);
												//					$this->_helper->getHelper('FlashMessenger')->addMessage($resultRelease['errorMessage']);
												$this->_redirect('/notification/index/release');
											}
											// }
										}



										// var_dump($result);die;
										// print_r($sessionNameConfrim->SAVEBENE);die;
										if ($result === true) {
											if (!empty($sessionNameConfrim->SAVEBENE)) {

												$content 						= array();
												$content['BENEFICIARY_ACCOUNT'] = $sessionNameConfrim->ACBENEF['ACBENEF']['BENEFICIARY_ACCOUNT'];
												$content['BENEFICIARY_NAME'] = $sessionNameConfrim->ACBENEF['ACBENEF']['BENEFICIARY_NAME'];
												$content['CURR_CODE'] = $sessionNameConfrim->ACBENEF['ACBENEF']['CURR_CODE'];
												$content['BENEFICIARY_RESIDENT'] = $sessionNameConfrim->ACBENEF['ACBENEF']['BENEFICIARY_RESIDENT'];
												$content['BENEFICIARY_ID_NUMBER'] = $sessionNameConfrim->ACBENEF['ACBENEF']['BENEFICIARY_ID_NUMBER'];
												$content['BENEFICIARY_CITIZENSHIP'] = $sessionNameConfrim->ACBENEF['ACBENEF']['BENEFICIARY_CITIZENSHIP'];
												$content['BENEFICIARY_ID_TYPE'] = $sessionNameConfrim->ACBENEF['ACBENEF']['BENEFICIARY_ID_TYPE'];
												$content['BENEFICIARY_CATEGORY'] = $sessionNameConfrim->ACBENEF['ACBENEF']['BENEFICIARY_CATEGORY'];
												$content['BENEFICIARY_EMAIL'] = '';
												$content['BENEFICIARY_ALIAS'] = '';
												$content['BENEFICIARY_CREATED'] = new Zend_Db_Expr("now()");
												$content['BENEFICIARY_CREATEDBY'] = $this->_userIdLogin;
												$content['BENEFICIARY_UPDATED'] = new Zend_Db_Expr("now()");
												$content['BENEFICIARY_UPDATEDBY'] = $this->_userIdLogin;
												$content['BENEFICIARY_SUGGESTED'] = new Zend_Db_Expr("now()");
												$content['BENEFICIARY_UPDATEDBY'] = $this->_userIdLogin;
												$content['CUST_ID'] 			  = $this->_custIdLogin;
												$content['BENEFICIARY_TYPE'] = '1';
												$content['BENEFICIARY_UPDATEDBY'] = $this->_userIdLogin;
												$content['USER_ID_LOGIN'] = $this->_userIdLogin;
												$content['CUST_ID_LOGIN'] = $this->_custIdLogin;
												// $content['PS_NUMBER'] = $result;
												// print_r($content);die;

												$selectUsers = $this->_db->select()
													->from(array('MAB' => 'M_BENEFICIARY'))
													->join(array('MABG' => 'M_BENEFICIARY_USER'), 'MAB.BENEFICIARY_ID = MABG.BENEFICIARY_ID', array('MAB.BENEFICIARY_ACCOUNT'))
													->where('MABG.USER_ID = ?', $this->_userIdLogin)
													->where('MABG.CUST_ID = ?', $this->_custIdLogin)
													->where('MAB.BENEFICIARY_ACCOUNT = ?', $sessionNameConfrim->ACBENEF['ACBENEF']['BENEFICIARY_ACCOUNT'])
													// echo $selectUser;die;
													->query()->fetchall();
												// print_r($selectUsers);die;
												if (empty($selectUsers)) {
													$Beneficiary = new Beneficiary();
													// $content
													$add = $Beneficiary->add($content);
												}
											}


											$this->_redirect('/notification/success/index');
										} else { //// TODO: what to do, if failed create payment
											// die('here');
											if (!$this->_custSameUser) {
												$this->_redirect('/notification/success/index');
											}
											// 										$this->_redirect('/notification/error/index');
										}
									} catch (Exception $e) {
										Application_Helper_General::exceptionLog($e);
									}
								}
							} else {

								$errorMsg 		= $validate->getErrorMsg();
								// print_r($errorMsg);die;
								// die('here');
								try {
									$validate   = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin);
									$errorTrxMsg 	= $validate->getErrorTrxMsg();	// array
									// $errorTrxMsg 	= array();	// array
								} catch (Exception $e) {
									// print_r($e);die;
									// Application_Helper_General::exceptionLog($e);
								}
								// print_r($errMessage);die;

								// $validate->__destruct();
								unset($validate);
								if (empty($errMessage)) {
									$errMessage 	= (!empty($errorMsg)) ? $errorMsg : reset(reset(reset($errorTrxMsg)));
								}
								if (!empty($errMessage)) {
									$this->view->error 		= true;
									$this->view->ERROR_MSG	= $errMessage;
								}

								// die('here');
								// print_r($errMessage);die;


								// print_r($errMessage);die;
							}
						}
					}
				} else {
					// die('here');
					//tambahn pentest
					$TRA_AMOUNT  		= $this->_getParam('TRA_AMOUNT');

					$PS_EFDATE_ORI_now  		= $this->_getParam('PS_EFDATE');
					if ($PS_EFDATE_ORI_now > date('d/m/Y')) {
						$TransferDate 	=  "2";
					} else {
						$TransferDate	=  "1";
						$PS_EFDATE = date('d/m/Y', strtotime('+1 days', strtotime(date('Y-m-d'))));
					}

					$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');
					$isConfirmPage 	= false;
				}
			} else {
				$this->view->sessionExpired = $this->language->_('Session expired');
				$this->_redirect('/singlepayment/within');
			}

			$randomTransact = str_pad(mt_rand(0, 99999999), 8, '0', STR_PAD_LEFT);
			$this->view->randomTransact = $randomTransact;
			$sessionNameRand->randomTransact = $randomTransact;

			//tambahn pentest
			if ($tranferdatetype == '1') {
				$this->view->PS_EFDATE 			= $PS_EFDATE;
			} elseif ($tranferdatetype == '3') {
				$PS_EFDATE = date('d/m/Y', strtotime('+1 days', strtotime(date('Y-m-d'))));
				$this->view->PS_EFDATEFUTURE = $PS_EFDATE;
				$PS_EFDATE = $PS_EFDATE_ORII; //
			} else {
				$this->view->PS_EFDATEFUTURE = $PS_EFDATE;
			}
		} else {
			// die('here2');
			$randomTransact = str_pad(mt_rand(0, 99999999), 8, '0', STR_PAD_LEFT);
			$this->view->randomTransact = $randomTransact;
			$sessionNameRand->randomTransact = $randomTransact;

			if ($crossCurr == 1)
				Zend_Session::namespaceUnset('TW');
		}

		$TRA_MESSAGE_len = (isset($TRA_MESSAGE)) ? strlen($TRA_MESSAGE) : 0;
		$TRA_REFNO_len 	 = (isset($TRA_REFNO))  ? strlen($TRA_REFNO)  : 0;

		$TRA_MESSAGE_len = 100 - $TRA_MESSAGE_len;
		$TRA_REFNO_len 	 = 200 - $TRA_REFNO_len;
		if($ACBENEF_CURRENCY == 'USD' || $ACCTSRC_CCY == 'USD'){
											$PS_CCY = 'USD';
										}else{
											$PS_CCY = 'IDR';
										}
		$this->view->AccArr 			= $AccArr;
		$this->view->ccyArr 			= $ccyList;
		$this->view->PS_NUMBER 			= (isset($PS_NUMBER))			? $PS_NUMBER			: '';
		$this->view->PS_SUBJECT 		= (isset($PS_SUBJECT))			? $PS_SUBJECT			: '';
		$this->view->TRA_CCY	 		= (isset($PS_CCY))				? $PS_CCY			: '';
		$this->view->PS_EFDATE 			= $PS_EFDATE;
		$this->view->PS_NOTIF 			= $PS_NOTIF;
		$this->view->PS_EMAIL 			= $PS_EMAIL;
		$this->view->PS_SMS 			= $PS_SMS;
		$this->view->TransferDate		= (isset($TransferDate))		? $TransferDate			: '1';

		if ($crossCurr == 1) {
			$traamount = Application_Helper_General::displayMoney($sessionNameConfrim->traAmount);
			$this->view->TRA_AMOUNT 		= (isset($traamount))			? $traamount : '';

			if ($kurs != 'N/A') {
				$plain_kurs = str_replace(',', '', $kurs);
				// $plain_kurs = str_replace(',','', $kursUSD);
				// var_dump($plain_kurs);
				// var_dump($kursUSD);die;
				$eqamount = ($sessionNameConfrim->traAmount) * $plain_kurs;
				$this->view->eqamount = Application_Helper_General::displayMoney($eqamount);
			} else {
				$eqamount = 'N/A';
				$this->view->eqamount = 'N/A';
			}
			$this->view->TRA_AMOUNT_PLAIN 		= (isset($sessionNameConfrim->traAmount))			? $sessionNameConfrim->traAmount : '';


			$this->view->TRA_ADDMESSAGE 	= (isset($TRA_ADDMESSAGE))			? $TRA_ADDMESSAGE			: '';
			$this->view->TRA_ADDMESSAGE_len		= $TRA_ADDMESSAGE_len;

			$this->view->BENEFICIARY_ACCOUNT_CCY = (isset($ACBENEF_ALIAS))		? $ACBENEF_ALIAS		: '';
			if (!empty($ACCTSRC)) {

				$accData = $this->_db->select()
					->from('M_CUSTOMER_ACCT')
					->where('ACCT_NO = ?', $ACCTSRC)
					->where('CUST_ID = ?', (string) $this->_custIdLogin);

				$accData = $this->_db->fetchRow($accData);
				$ACCTSRC_CCY = $accData['CCY_ID'];

				$this->view->ACCTSRC_CCY = (isset($ACCTSRC_CCY))				? $ACCTSRC_CCY				: '';
				$this->view->ACCTSRC_CCY2 = (isset($ACCTSRC_CCY))				? $ACCTSRC_CCY				: '';
			}
		} else {

			if ($PS_NUMBER) {
				$this->view->TRA_AMOUNT 		= (isset($TRA_AMOUNT))			? $TRA_AMOUNT			: '';
			} else {
				$traamount = Application_Helper_General::displayMoney($sessionNameConfrim->traAmount);
				$this->view->TRA_AMOUNT 		= (isset($traamount))			? $traamount : '';
			}

			$this->view->TRA_REFNO 			= (isset($TRA_REFNO))			? $TRA_REFNO			: '';
			$this->view->TRA_REFNO_len		= $TRA_REFNO_len;
		}
		
		$this->view->TRA_MESSAGE 		= (isset($TRA_MESSAGE))			? $TRA_MESSAGE			: '';
		// var_dump($TRA_NOTIF);
		$this->view->notif 		= (isset($TRA_NOTIF))			? $TRA_NOTIF			: '';
		$this->view->sms_notif 		= (isset($TRA_SMS))			? $TRA_SMS			: '';
		$this->view->email_notif 		= (isset($TRA_EMAIL))			? $TRA_EMAIL			: '';



		$this->view->TRA_MESSAGE_len	= $TRA_MESSAGE_len;


		if (!empty($pslipData)) {
			$this->view->ACCTSRC 			= (isset($ACCTSRC))				? $ACCTSRC				: '';
			$this->view->ACBENEF 			= (isset($ACBENEF))				? $ACBENEF				: '';
			$this->view->ACBENEF_BANKNAME	= (isset($ACBENEF_BANKNAME))	? $ACBENEF_BANKNAME		: '';
		} else {
			$this->view->ACCTSRC 			= (isset($sessionNameConfrim->sourceAcct))				? $sessionNameConfrim->sourceAcct				: '';
			$this->view->ACBENEF 			= (isset($sessionNameConfrim->benefAcct))				? $sessionNameConfrim->benefAcct				: '';

			if ($tranferdatetype == 3) {
				$this->view->ACBENEF_BANKNAME	= $this->_getParam('ACBENEF_BANKNAME');
			} else {
				$this->view->ACBENEF_BANKNAME	= (isset($sessionNameConfrim->ACBENEF_BANKNAME))	? $sessionNameConfrim->ACBENEF_BANKNAME		: '';
			}
		}

		$this->view->ACCTSRC_view		= (isset($ACCTSRC_view))		? $ACCTSRC_view			: '';


		$this->view->ACBENEF_ALIAS 		= (isset($ACBENEF_ALIAS))		? $ACBENEF_ALIAS		: '';
		$this->view->ACBENEF_EMAIL 		= (isset($ACBENEF_EMAIL))		? $ACBENEF_EMAIL		: '';
		$this->view->CURR_CODE 			= (isset($ACBENEF_CCY))			? $ACBENEF_CCY			: '';
		$this->view->CHARGES_AMT 		= (isset($chargesAMT))			? Application_Helper_General::displayMoney($chargesAMT)	: '';
		$this->view->CHARGES_CCY 		= (isset($chargesCCY))			? $chargesCCY			: '';
		$this->view->PERIODIC_EVERY 	= (isset($PERIODIC_EVERY))		? $PERIODIC_EVERY		: '0';
		$this->view->PERIODIC_EVERYDATE = (isset($PERIODIC_EVERYDATE))	? $PERIODIC_EVERYDATE	: '0';
		$this->view->TrfDateType 		= (isset($TrfDateType))			? $TrfDateType			: '1';
		$this->view->TrfPeriodicType 	= (isset($TrfPeriodicType))		? $TrfPeriodicType		: ''; //$TrfPeriodicType;
		$this->view->CROSS_CURR 		= (isset($CROSS_CURR))	? $CROSS_CURR	: '2';
		// var_dump($CROSS_CURR);die;
		// print_r($this->view->CROSS_CURR);
		$this->view->LLD_HI =  'none';

		if ($crossCurr == 1) {
			$this->view->LLD_IDENTITY 	= (isset($LLD_IDENTITY))		? $LLD_IDENTITY		: '';

			if (!empty($sessionNamespace->lldrelation)) {
				$LLD_TRANSACTOR_RELATIONSHIP = $sessionNamespace->lldrelation;
			}

			if (!empty($sessionNamespace->lldidenty)) {
				$LLD_IDENTITY = $sessionNamespace->lldidenty;
			}

			$this->view->LLD_TRANSACTOR_RELATIONSHIP 	= (isset($LLD_TRANSACTOR_RELATIONSHIP))		? $LLD_TRANSACTOR_RELATIONSHIP		: '';

			$prebenemodel 			= new predefinedbeneficiary_Model_Predefinedbeneficiary();

			if ($ACBENEF_CURRENCY == 'USD') {
				if ($sessionNameConfrim->traAmount >= 10000) {
					$this->view->LLD_HI  = '-';
				} else {
					$this->view->LLD_HI  = 'none';
				}
			}

			if (empty($BENEFICIARY_CITIZENSHIP)) {
				$BENEFICIARY_CITIZENSHIP = $this->_request->getParam('ACBENEF_CITIZENSHIP');
				$BENEFICIARY_RESIDENT = $this->_request->getParam('ACBENEF_NATION');
				$BENEFICIARY_CATEGORY = $this->_request->getParam('ACBENEF_CATEGORY');
				$BENEFICIARY_ID_TYPE  = $this->_request->getParam('ACBENEF_IDENTY');
				$BENEFICIARY_ID_NUMBER 	= $this->_request->getParam('ACBENEF_IDENTYNUM');
				$accsrcDetail['ACCTSRC_CITIZENSHIP'] = $this->_request->getParam('ACCTSRC_CITIZENSHIP');
				$accsrcDetail['ACCTSRC_RESIDENT'] = $this->_request->getParam('ACCTSRC_RESIDENT');
				$accsrcDetail['ACCTSRC_CATEGORY'] = $this->_request->getParam('ACCTSRC_CATEGORY');
				$accsrcDetail['ACCTSRC_ID_TYPE'] = $this->_request->getParam('ACCTSRC_ID_TYPE');
				$accsrcDetail['ACCTSRC_ID_NUMBER'] = $this->_request->getParam('ACCTSRC_ID_NUMBER');
			}

			$this->view->LLD_TRANSACTION_PURPOSE 	= (isset($LLD_TRANSACTION_PURPOSE))		? $LLD_TRANSACTION_PURPOSE		: '';

			$this->view->ACBENEF_CITIZENSHIP_CODE 	= (isset($BENEFICIARY_CITIZENSHIP))		? $BENEFICIARY_CITIZENSHIP		: '';
			$this->view->ACBENEF_CATEGORY_CODE 	= (isset($BENEFICIARY_CATEGORY))		? $BENEFICIARY_CATEGORY		: '';
			$this->view->ACBENEF_NATION_CODE 	= $BENEFICIARY_RESIDENT;

			$temp_c = $accsrcDetail['ACCTSRC_CITIZENSHIP'];
			$temp_r = $accsrcDetail['ACCTSRC_RESIDENT'];

			if (!empty($temp_c)) {
				if ($temp_c == 'R' || $temp_c == 'Resident') {
					$accsrcDetail['ACCTSRC_RESIDENT'] = 'Resident';
				} else {
					$accsrcDetail['ACCTSRC_RESIDENT'] = 'Non Resident';
				}
			} else {
				$accsrcDetail['ACCTSRC_RESIDENT'] = '-';
			}

			if (!empty($temp_r)) {
				if ($temp_r == 'W' || $temp_r == 'WNI') {
					$accsrcDetail['ACCTSRC_CITIZENSHIP'] = 'WNI';
				} else {
					$accsrcDetail['ACCTSRC_CITIZENSHIP'] = 'WNA';
				}
			} else {
				$accsrcDetail['ACCTSRC_CITIZENSHIP'] = '-';
			}

			if (empty($BENEFICIARY_CATEGORY))
				$BENEFICIARY_CATEGORY = '-';



			$this->view->ACBENEF_IDENTY 	= (isset($BENEFICIARY_ID_TYPE))		? $BENEFICIARY_ID_TYPE		: '';
			$this->view->ACBENEF_IDENTYNUM 	= (isset($BENEFICIARY_ID_NUMBER))		? $BENEFICIARY_ID_NUMBER		: '';
			$this->view->ACBENEF_CURRENCY 	= (isset($BENEF_CURRENCY))		? $BENEF_CURRENCY		: '';
			$this->view->TRANSPURPOSE 	= (isset($LLD_TRANSACTION_PURPOSE))		? $LLD_TRANSACTION_PURPOSE		: '';


			if (!empty($TRANS_PURPOSECODE))
				$this->view->LLD_TRANSACTION_PURPOSETEXT = $purposeList[$TRANS_PURPOSECODE];

			$this->view->TRANSPURPOSE_view 	= (isset($LLD_TRANSACTION_PURPOSE))		? $LLD_TRANSACTION_PURPOSE		: '';

			$this->view->ACCTSRC_CITIZENSHIP_CODE 	= (isset($accsrcDetail['ACCTSRC_CITIZENSHIP']))		? $accsrcDetail['ACCTSRC_CITIZENSHIP']		: '';
			$this->view->ACCTSRC_RESIDENT_CODE 	= (isset($accsrcDetail['ACCTSRC_RESIDENT']))		? $accsrcDetail['ACCTSRC_RESIDENT']		: '';
			$this->view->ACCTSRC_CATEGORY_CODE 	= (isset($accsrcDetail['ACCTSRC_CATEGORY']))		? $accsrcDetail['ACCTSRC_CATEGORY']		: '';
			$temp_bc = $BENEFICIARY_CITIZENSHIP;
			$temp_br = $BENEFICIARY_RESIDENT;
			if (!empty($temp_br)) {
				if ($temp_br == 'R' || $temp_br == 'Resident' || $temp_br == 'W') {
					$BENEFICIARY_RESIDENT = 'Resident';
				} else if ($temp_br == '-') {
					$BENEFICIARY_RESIDENT = '-';
				} else {
					$BENEFICIARY_RESIDENT = 'Non Resident';
				}
			} else {
				$BENEFICIARY_RESIDENT = '-';
			}

			if (!empty($temp_bc)) {
				if ($temp_bc == 'W' || $temp_bc == 'WNI' || $temp_bc == 'R') {
					$BENEFICIARY_CITIZENSHIP = 'WNI';
				} else if ($temp_bc == '-') {
					$BENEFICIARY_CITIZENSHIP = '-';
				} else {
					$BENEFICIARY_CITIZENSHIP = 'WNA';
				}
			} else {
				$BENEFICIARY_CITIZENSHIP = '-';
			}

			if (!empty($accsrcDetail['ACCTSRC_CATEGORY'])) {
			} elseif ($accsrcDetail['ACCTSRC_CATEGORY'] == '-') {
				$accsrcDetail['ACCTSRC_CATEGORY'] = '';
			} else {
				$accsrcDetail['ACCTSRC_CATEGORY'] = '';
			}

			$this->view->ACBENEF_CITIZENSHIP 	= (isset($BENEFICIARY_CITIZENSHIP))		? $BENEFICIARY_CITIZENSHIP		: '';
			$this->view->ACBENEF_CATEGORY 	= (isset($BENEFICIARY_CATEGORY))		? $BENEFICIARY_CATEGORY		: '';
			$this->view->ACBENEF_NATION 	= $BENEFICIARY_RESIDENT;
			$this->view->ACCTSRC_ID_TYPE 	= (isset($accsrcDetail['ACCTSRC_ID_TYPE']))		? $accsrcDetail['ACCTSRC_ID_TYPE']		: '';
			$this->view->ACCTSRC_ID_NUMBER 	= (isset($accsrcDetail['ACCTSRC_ID_NUMBER']))		? $accsrcDetail['ACCTSRC_ID_NUMBER']		: '';
			$this->view->ACCTSRC_CITIZENSHIP 	= (isset($accsrcDetail['ACCTSRC_CITIZENSHIP']))		? $accsrcDetail['ACCTSRC_CITIZENSHIP']		: '';
			$this->view->ACCTSRC_RESIDENT 	= (isset($accsrcDetail['ACCTSRC_RESIDENT']))		? $accsrcDetail['ACCTSRC_RESIDENT']		: '';
			$this->view->ACCTSRC_CATEGORY 	= (isset($accsrcDetail['ACCTSRC_CATEGORY']))		? $accsrcDetail['ACCTSRC_CATEGORY']		: '';
		}

		$settingObj = new Settings();
		$this->view->LIMITLLD		= $settingObj->getSetting("threshold_lld_remittance", 0);
		$this->view->LIMITLLDMONEY		= Application_Helper_General::displayMoney($settingObj->getSetting("threshold_lld_remittance", 0));

		$this->view->periodicEveryArr  	= $periodicEveryArr;
		$this->view->periodicEveryDateArr  = $periodicEveryDateArr;

		$this->view->confirmPage		= $isConfirmPage;
		$this->view->useConfirmPage		= $USE_CONFIRM_PAGE;
		$this->view->TRANSPURPOSE_DESC	= $TRA_PURPOSE;
		$this->view->resultPage			= $isResultPage;
		$this->view->payReff			= $payReff;
		$this->view->TREASURY_NUM		= $TREASURY_NUM;
		$this->view->DOC_ID		= $DOC_ID;

		if ($isConfirmPage == '1') {
			// Zend_Debug::Dump($sessionNameConfrim);die();
		} else {
			Application_Helper_General::writeLog('CRSP', 'Viewing Create Single Payment In House');
		}

		// Zend_Debug::Dump($sessionNameConfrim);die();

		// if ($this->_getParam('backBtn')){
		//    	$sessionNameConfrim = new Zend_Session_Namespace('confirmTransact');

		//    	Zend_Debug::Dump($sessionNameConfrim);die();
		//    }

		/*------------------------------------approval matrix data--------------------------------------------------------*/

		$cust_id = $this->_custIdLogin;

		$selectUsergroup = $this->_db->select()
			->from(array('M_APP_GROUP_USER'), array('*'))
			->where('CUST_ID = ?', $cust_id)
			->group('GROUP_USER_ID')
			// echo $selectUser;die;
			->query()->fetchall();

		$selectUsers = $this->_db->select()
			->from(array('M_APP_GROUP_USER'), array('*'))
			->where('CUST_ID = ?', $cust_id)
			// echo $selectUser;die;
			->query()->fetchall();
		// print_r($selectUser);die;

		$userlists = '';
		foreach ($selectUsergroup as $key => $value) {
			foreach ($selectUsers as $no => $val) {
				if ($val['GROUP_USER_ID'] == $value['GROUP_USER_ID']) {
					if (empty($userlists))
						$userlists .= $val['USER_ID'];
					else
						$userlists .= ', ' . $val['USER_ID'];
				}
			}
			$selectUsergroup[$key]['USER'] .= $userlists;
			$userlists = '';
			$spesials = 'S_' . $cust_id;
			if ($value['GROUP_USER_ID'] == $spesials) {
				$selectUsergroup[$key]['GID'] .= 'SG';
			} else {
				$groups = explode('_', $value['GROUP_USER_ID']);
				$alphabets = array(1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'E', 6 => 'F', 7 => 'G', 8 => 'H', 9 => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z');

				// $cust = explode('_', $value['GROUP_USER_ID'])
				$selectUsergroup[$key]['GID'] .= $alphabets[(int) $groups[2]];
			}
		}
		$this->view->selectUsergroup = $selectUsergroup;








		$selects = $this->_db->select()
			->from(array('MAB' => 'M_APP_BOUNDARY'))
			->join(array('MABG' => 'M_APP_BOUNDARY_GROUP'), 'MAB.BOUNDARY_ID = MABG.BOUNDARY_ID', array('GROUP_USER_ID'))
			->where('MAB.CUST_ID = ?', (string) $cust_id)
			->group('BOUNDARY_ID');
		// echo $select;
		$result = $this->_db->fetchAll($selects);

		$dataArr = array();
		// $alf = 'A';
		// $alf++;
		// ++$alf;
		// print_r((int)$alf);
		// $nogroup = sprintf("%02d", 1);
		// print_r($result);die;
		foreach ($result as $row) {
			list($grouptype, $groupname, $groupnum) = explode("_", $row['GROUP_USER_ID']);
			if ($grouptype == 'N')
				$name = 'Group ' . trim($groupnum, '0');
			else
				$name = 'Special Group';

			$selectUser = $this->_db->select()
				->from(array('M_APP_GROUP_USER'), array('USER_ID'))
				->where('GROUP_USER_ID = ?', $row['GROUP_USER_ID'])
				// echo $selectUser;die;
				->query()->fetchall();

			$userlist = '';

			$policy = explode(' AND ', $row['POLICY']);


			foreach ($policy as $key => $value) {
				if ($value == 'SG') {
					$group = 'S_' . $cust_id;
					$selectUser = $this->_db->select()
						->from(array('M_APP_GROUP_USER'), array('USER_ID'))
						->where('GROUP_USER_ID = ?', $group)
						// echo $selectUser;die;
						->query()->fetchall();
					foreach ($selectUser as $val) {
						if (empty($userlist))
							$userlist .= $val['USER_ID'];
						else
							$userlist .= ', ' . $val['USER_ID'];
					}
				} else {



					$alphabet = array("A" => 1, "B" => 2, "C" => 3, "D" => 4, "E" => 5, "F" => 6, "G" => 7, "H" => 8, "I" => 9, "J" => 10, "K" => 11, "L" => 12, "M" => 13, "N" => 14, "O" => 15, "P" => 16, "Q" => 17, "R" => 18, "S" => 19, "T" => 20, "U" => 21, "V" => 22, "W" => 23, "X" => 24, "Y" => 25, "Z" => 26,);

					$policyor = explode(' OR ', $value);
					// print_r($policyor);die;
					foreach ($policyor as $numb => $valpol) {
						if ($valpol == 'SG') {
							$group = 'S_' . $cust_id;
							$selectUser = $this->_db->select()
								->from(array('M_APP_GROUP_USER'), array('USER_ID'))
								->where('GROUP_USER_ID = ?', $group)
								// echo $selectUser;die;
								->query()->fetchall();
							foreach ($selectUser as $val) {
								if (empty($userlist)) {
									$userlist .= $val['USER_ID'];
								} else {
									$userlist .= ', ' . $val['USER_ID'];
								}
							}
						} else {
							$nogroup = sprintf("%02d", $alphabet[$valpol]);
							// print_r($valpol);
							$group = 'N_' . $cust_id . '_' . $nogroup;
							$selectUser = $this->_db->select()
								->from(array('M_APP_GROUP_USER'), array('USER_ID'))
								->where('GROUP_USER_ID = ?', $group)
								// echo $selectUser;die;
								->query()->fetchall();
							// print_r($selectUser);
							foreach ($selectUser as $val) {
								if (empty($userlist))
									$userlist .= $val['USER_ID'];
								else
									$userlist .= ', ' . $val['USER_ID'];
							}
						}
					}
				}
			}
			// print_r($userlist);die;
			// $nogroup = sprintf("%02d", $key+1);

			// foreach($selectUser as $val){
			//  if(empty($userlist))
			//    $userlist .= $val['USER_ID'];
			//  else
			//    $userlist .= ', '.$val['USER_ID'];
			// }

			$arrTraType     = array_combine($this->_paymenttype["code"], $this->_paymenttype["desc"]);

			$dataArr[] = array(
				'TRANSFER_TYPE' => $arrTraType[$row['TRANSFER_TYPE']],
				'CURRENCY' => $row['CCY_BOUNDARY'],
				'BOUNDARY' => Application_Helper_General::displayMoney($row['BOUNDARY_MIN']) . " - " . Application_Helper_General::displayMoney($row['BOUNDARY_MAX']),
				'GROUP_NAME' => $row['POLICY'],
				'USERS' => $userlist
			);
		}
		// print_r($dataArr);die;
		/* print_r($row);die();*/

		$this->view->dataBoundary = $dataArr;

		/*$selectpriv = $this->_db->select()
                                  ->from(array('M_FPRIVI_USER'),array('FPRIVI_ID'))
                                  ->where('FUSER_ID = ?', $this->_custIdLogin.$this->_userIdLogin)
                                  ->where('FPRIVI_ID = "UTSP"')
                                  ->query()->fetchall();

                  print_r($selectpriv);die();*/

		if ($this->view->hasPrivilege('UTSP')) {
			$templatePriv = 1;
		} else {
			$templatePriv = 0;
		}

		if ($this->view->hasPrivilege('CRSP')) {
			$priv = 1;
		} else {
			$priv = 0;
		}

		if ($this->view->hasPrivilege('MTSP')) {
			$usetemp = 1;
		} else {
			$usetemp = 0;
		}

		// $usetemp = 0;
		// $templatePriv = 1;
		// $priv = 0;
		// echo $priv;
		// echo $templatePriv;die;

		// print_r($templatePriv);die();

		$this->view->usetemplate = $usetemp;
		$this->view->btntemplatePriv = $priv;
		$this->view->templatePriv = $templatePriv;

		/*----------------------------------end approval matrix-----------------------------------------------*/


		//Recrate Ongoing
		if ($this->_request->getParam('recreate')) {
			$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
			$filter       = new Application_Filtering();
			$AESMYSQL = new Crypt_AESMYSQL();
			$password = $sessionNamespace->token;
			$PS_NUMBER = urldecode($filter->filter($this->_getParam('recreate'), "recrate"));
			$PS_NUMBER = $AESMYSQL->decrypt($PS_NUMBER, $password);

			$recreate  = $this->_db->select()
				->from(array('P' => 'T_PSLIP'))
				->joinLeft(array('T' => 'T_TRANSACTION'), 'P.PS_NUMBER = T.PS_NUMBER')
				->where('P.PS_NUMBER = ?', $PS_NUMBER);
			$recreate = $this->_db->fetchRow($recreate);
			// echo '<pre>';
			// var_dump($recreate['TRA_AMOUNT']);die;

			$this->view->PS_SUBJECT = $recreate['PS_SUBJECT'];
			$this->view->TRA_AMOUNT = $recreate['TRA_AMOUNT'];
			$this->view->ACCTSRC = $recreate['SOURCE_ACCOUNT'];
			$this->view->ACBENEF = $recreate['BENEFICIARY_ACCOUNT'];
			$this->view->ACBENEF_BANKNAME = $recreate['BENEFICIARY_ACCOUNT_NAME'];
			$this->view->ACBENEF_EMAIL = $recreate['BENEFICIARY_EMAIL'];
			$this->view->TRA_MESSAGE = $recreate['TRA_MESSAGE'];

			$this->view->notif = $recreate['PS_NOTIF'];
			$this->view->email_notif = $recreate['PS_EMAIL'];
			$this->view->sms_notif = $recreate['PS_SMS'];
		}
		/////////////////
	}

	public function sourceAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$type = $this->_getParam('type');

		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);

		if ($type == 1) {
			$AccArr = $CustomerUser->getAccounts();
		} else {
			$param = array('CCY_IN' => 'IDR');
			$AccArr = $CustomerUser->getAccounts($param);
		}


		echo '<option value="-"> -- Select Source Account -- </option>';
		foreach ($AccArr as $key => $val) {
			echo '<option value="' . $val['ACCT_NO'] . '" ccy="' . $val['CCY_ID'] . '"  ' . $selected . ' >' . $val['ACCT_NO'] . ' [' . $val['CCY_ID'] . '] - ' . $val['ACCT_NAME'] . ' / ' . $val['ACCT_ALIAS_NAME'] . ' (' . $val['DESC'] . ')</option>';
		}

		echo $optHtml;
	}


	public function templateaddAction()
	{
		// die('here');
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$template = $this->_getParam('template');
		$subject = $this->_getParam('subject');
		$source = $this->_getParam('source');
		$bene = $this->_getParam('bene');
		$amount = str_replace(",", "", $this->_getParam('amount'));
		$notif = $this->_getParam('notif');
		$cross = $this->_getParam('cross');
		$transpose = $this->_getParam('transpose');
		$exrate = $this->_getParam('exrate');
		$refnum = $this->_getParam('refnum');
		$custnum = $this->_getParam('custnum');
		$email = $this->_getParam('email');
		$sms = $this->_getParam('sms');
		$target = $this->_getParam('target');
		$locked = $this->_getParam('locked');

		$this->_db->beginTransaction();
		$insertArr = array(
			'T_NAME' 			=> $template,
			'T_CREATEDBY' 		=> $this->_userIdLogin,
			'T_CREATED' 		=> new Zend_Db_Expr("now()"),
			'T_GROUP'			=> $this->_custIdLogin,
			'T_TARGET'			=> $target,
			'T_TYPE'			=> "1",
		);

		$inserttemplate = $this->_db->insert('M_TEMPLATE', $insertArr);
		$lastId = $this->_db->lastInsertId();

		if (!empty($bene)) {
			$select = $this->_db->select()
				->from(array('A' => 'M_BENEFICIARY'), array('*'));
			$select->where('A.BENEFICIARY_ACCOUNT = ?', (string) $bene);
			$select->where('A.CUST_ID = ?', (string) $this->_custIdLogin);
			$benedata = $this->_db->fetchall($select);
			$benename = $benedata['0']['BENEFICIARY_NAME'];
		} else {
			$benedata = array();
			$benename = '';
		}

		// print_r($insertArr);die;

		$insertData = array(
			'TEMP_ID' 				=> $lastId,
			'TEMP_SUBJECT' 			=> $subject,
			'TEMP_SOURCE' 			=> $source,
			'TEMP_BENE' 			=> $bene,
			'TEMP_BENE_NAME' 		=> $benename,
			'TEMP_CROSS' 			=> $cross,
			'TEMP_AMOUNT' 			=> $amount,
			'TEMP_NOTIF' 			=> $notif,
			'TEMP_EMAIL' 			=> $email,
			'TEMP_SMS' 				=> $sms,
			'TEMP_PURPOSE' 			=> $transpose,
			'TEMP_EXCHANGE' 		=> $exrate,
			'TEMP_REFNUM' 			=> $refnum,
			'TEMP_CUSTREF' 			=> $custnum,
			'TEMP_LOCKED'			=> $locked,
		);

		$inserttemplatedata = $this->_db->insert('M_TEMPLATE_DATA', $insertData);

		$this->_db->commit();

		if ($inserttemplate && $inserttemplatedata) {
			echo "success";
		} else {
			echo "failed";
		}


		// echo '<span>te</span>';



		//approval list controller
		//  		$groupDesc = array( '01' => 'Group 1',
		//                        '02' => 'Group 2',
		//                        '03' => 'Group 3',
		//                        '04' => 'Group 4',
		//                        '05' => 'Group 5',
		//                        '06' => 'Group 6',
		//                        '07' => 'Group 7',
		//                        '08' => 'Group 8',
		//   						'09' => 'Group 9',
		//                        '10' => 'Group 10',
		//                        'S'  => 'Special Group');

		//    $this->view->group_desc = $groupDesc;


		// for($i=1;$i<11;$i++)
		// {
		// 	$groupuserid = 'N_'.$this->_custIdLogin.'_0'.$i;
		// 	if(strlen($i) == 2)
		// 	{
		// 		$groupuserid = 'N_'.$this->_custIdLogin.'_'.$i;
		// 	}
		// 	$selectgroup = $this->_db->select()
		// 			       	->from(array('A' => 'M_APP_GROUP_USER'),array('USER_ID'));
		// 	$selectgroup -> where("A.CUST_ID LIKE ".$this->_db->quote($this->_custIdLogin));
		// 	$selectgroup -> where("A.GROUP_USER_ID LIKE ".$this->_db->quote($groupuserid));
		// 	$resultgroup = $selectgroup->query()->FetchAll();
		// 	$groupuseridview = 'group'.$i;
		// 	$this->view->$groupuseridview = $resultgroup;
		// }

		// $specialid = 'S_'.$this->_custIdLogin;
		// $selectspecial = $this->_db->select()
		// 			       		->from(array('A' => 'M_APP_GROUP_USER'),array('USER_ID'));
		// $selectspecial -> where("A.CUST_ID LIKE ".$this->_db->quote($this->_custIdLogin));
		// $selectspecial -> where("A.GROUP_USER_ID LIKE ".$this->_db->quote($specialid));
		// $resultspecial = $selectspecial->query()->FetchAll();
		// $this->view->special = $resultspecial;

		//   $authorizationGroupUser = $this->_db->select()
		//                                  ->from(array('B'=>'M_APP_BOUNDARY'),
		// 					                    array('BOUNDARY_ID','CCY_BOUNDARY', 'BOUNDARY_MIN', 'BOUNDARY_MAX'))
		// 					             ->join(array('BG' => 'M_APP_BOUNDARY_GROUP'),'B.BOUNDARY_ID = BG.BOUNDARY_ID',array('BOUNDARY_GROUP_ID','GROUP_USER_ID'))
		// 					             ->join(array('GU' => 'M_APP_GROUP_USER'),'BG.GROUP_USER_ID = GU.GROUP_USER_ID',array('USER_ID'))
		// 					             ->join(array('U'  => 'M_USER'),'U.USER_ID = GU.USER_ID',array('USER_FULLNAME'))
		// 					             ->where('U.CUST_ID = ?',(string)$this->_custIdLogin)
		// 					             ->where('B.CUST_ID = ?',(string)$this->_custIdLogin)
		// 					             ->order(array('CCY_BOUNDARY','BOUNDARY_MIN'.' '.'ASC'))
		// 					             ->query()->fetchAll();

		// /*Zend_Debug::dump($authorizationGroupUser);
		// die;	*/

		// $authorizationMatrix = array();
		//    $groupArray = array();
		//    $groupUserArray = array();

		// foreach($authorizationGroupUser as $row)
		// {
		//    //MERANGKUM DATA DARI APP BOUNDARY
		//    $authorizationMatrix[$row['BOUNDARY_ID']]['BOUNDARY_MIN'] = $row['BOUNDARY_MIN'];
		//    $authorizationMatrix[$row['BOUNDARY_ID']]['BOUNDARY_MAX'] = $row['BOUNDARY_MAX'];
		//    $authorizationMatrix[$row['BOUNDARY_ID']]['CCY_BOUNDARY'] = $row['CCY_BOUNDARY'];

		//    //example : N_CUSTCHRIS_01 & S_CUSTCHRIS
		//    $explodeGroup = explode('_',$row['GROUP_USER_ID']);

		//    //MERANGKUM DATA UTK BOUNDARY GROUP, tetapi masih ada yg redudant
		//    if($explodeGroup[0] == 'N')
		//    {
		//        $keyGroup   = $explodeGroup[2];
		//        $group_desc = $this->language->_('Group').' ' . (int)$keyGroup;
		//    }
		//    else if($explodeGroup[0] == 'S')
		//    {
		//        $keyGroup = 'S';
		//        $group_desc = $this->language->_('Special');
		//    }

		//    $groupArray[$row['BOUNDARY_ID']][]  = $group_desc;


		//    //MERANGKUM DATA UTK GROUP USER
		//    $groupUserArray[$keyGroup][] = $row['USER_FULLNAME'] . ' ('.$row['USER_ID'].')';

		// }

		// //men-distinct group agar tidak ada yang sama
		// $groupArray_unique = array();
		// foreach($groupArray as $key => $row)
		// {
		//    $groupArray_unique[$key] = array_unique($groupArray[$key]);
		// }

		//    //men-distinct group user agar tidak ada yang sama
		// $groupUserArray_unique = array();
		// foreach($groupUserArray as $key => $row)
		// {
		//    $groupUserArray_unique[$key] = array_unique($row);
		// }

		// $this->view->authorization_matrix = $authorizationMatrix;
		//       $this->view->boundary_group       = $groupArray_unique;
		// $this->view->group_user           = $groupUserArray_unique;

	}
	
	
	public function templaterefreshAction()
	{
		// die('here');
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();
		$selectpriv = $this->_db->select()
			->from(array('A' => 'M_FPRIVI_USER'), array('FPRIVI_ID'))
			->where('A.FUSER_ID = ?', (string) $this->_custIdLogin . $this->_userIdLogin)
			->query()->fetchAll();

		foreach ($selectpriv as $key => $value) {
			foreach ($value as $row => $data) {
				if ($data == 'MTSP') {
					$usermtsp = true;
				}
				if ($data == 'UTSP') {
					$userutsp = true;
				}
			}
		}

		if (!empty($usermtsp)) {

			$select = $this->_db->select()
				->from(array('A' => 'M_TEMPLATE'), array('*'))
				->join(array('G' => 'M_TEMPLATE_DATA'), 'A.T_ID = G.TEMP_ID', array('G.*'));
			$select->where('A.T_GROUP = ?', (string) $this->_custIdLogin);
			$select->where('A.T_TYPE = "1"');
			$select->order('T_ID ASC');

			$templateArr = $this->_db->fetchall($select);
			$this->view->templatearr = $templateArr;
		} else if (!empty($userutsp)) {
			$select = $this->_db->select()
				->from(array('A' => 'M_TEMPLATE'), array('*'))
				->join(array('G' => 'M_TEMPLATE_DATA'), 'A.T_ID = G.TEMP_ID', array('G.*'));
			$select->where('A.T_GROUP = ?', (string) $this->_custIdLogin)
				->where('A.T_TARGET LIKE ?', (string) '%' . $this->_userIdLogin . '%')
				->where('A.T_TYPE = "1"')
				->order('T_ID ASC');

			$templateArr = $this->_db->fetchall($select);
			$this->view->templatearr = $templateArr;
		}

		if (!empty($usermtsp) && !empty($userutsp)) {
			$select = $this->_db->select()
				->from(array('A' => 'M_TEMPLATE'), array('*'))
				->join(array('G' => 'M_TEMPLATE_DATA'), 'A.T_ID = G.TEMP_ID', array('G.*'));
			$select->where('A.T_GROUP = ?', (string) $this->_custIdLogin);
			$select->where('A.T_TYPE = "1"');
			$select->order('T_ID ASC');

			$templateArr = $this->_db->fetchall($select);
			$this->view->templatearr = $templateArr;
		}
		
		//echo $select;
		
		if ($templateArr) {
                  foreach ($templateArr as $key => $value) {
                    /*print_r($value);die;*/
                    echo '<div class="row">';
                    echo '<div class="alert alert-primary" role="alert" style="width: 90%">';
                    /*  echo '<a href="#" id="templatelink" data-subject="'.$value['TEMP_SUBJECT'].'" data-bene="'.$value['TEMP_BENE'].'" data-source="'.$value['TEMP_SOURCE'].'" data-amount="'.$value['TEMP_AMOUNT'].'" data-cross="'.$value['TEMP_CROSS'].'" data-notif="'.$value['TEMP_NOTIF'].'" data-benename="'.$value['TEMP_BENE_NAME'].'" class="alert-link templatelink">'.$value['T_NAME'].'</a> <span style="float:right">*/
                    echo '<a href="#"  id="edittemplatelink" data-tempid="' . $value['T_ID'] . '" data-tempname="' . $value['T_NAME'] . '" data-tempsubject="' . $value['TEMP_SUBJECT'] . '" data-tempbene="' . $value['TEMP_BENE'] . '" data-tempsource="' . $value['TEMP_SOURCE'] . '" data-tempamount="' . $value['TEMP_AMOUNT'] . '" data-tempcross="' . $value['TEMP_CROSS'] . '" data-tempnotif="' . $value['TEMP_NOTIF'] . '" data-tempbenename="' . $value['TEMP_BENE_NAME'] . '" data-temppurpose="' . $value['TEMP_PURPOSE'] . '"  data-tempexchange="' . $value['TEMP_EXCHANGE'] . '"  data-temperefnum="' . $value['TEMP_REFNUM'] . '"  data-tempcustref="' . $value['TEMP_CUSTREF'] . '" data-tempemail="' . $value['TEMP_EMAIL'] . '" data-tempsms="' . $value['TEMP_SMS'] . '" data-templocked="' . $value['TEMP_LOCKED'] . '" class="alert-link edittemplatelink">' . $value['T_NAME'] . '</a> <span style="float:right">';

                    // btn delete
                    // <a href="#" id="removetemp" onclick="deleteForm('.$value['T_ID'].')" ><i class="fa fa-trash pl-1 pr-1"></i></a></span>
                    echo '</div>';
                    echo '</div>';
					echo '<script>';
					//echo 'alert()';
					/*echo "function edittemp(){document.getElementById('edittemplatediv').style.display = 'block';$('.templatelink').show();$('.closeBtn').css('margin-right', '25%');document.getElementById('edittempname').value = $(this).attr('data-tempname');document.getElementById('edittempsubject').value = $(this).attr('data-tempsubject');document.getElementById('edittempbenef').value = $(this).attr('data-tempbene');var amount = $(this).attr('data-tempamount');var amountsplit = amount.split(" ");if (amountsplit.length > 1) {  var amount = amountsplit[1];var ccy = amountsplit[0];} else {var amount = $(this).attr('data-tempamount');var ccy = '';}
document.getElementById('edittempamount').value = amount;
      document.getElementById('hiddenid').value = $(this).attr('data-tempid');
      document.getElementById('hiddenbenename').value = $(this).attr('data-tempbenename');
      document.getElementById('locked').value = $(this).attr('data-templocked');


      document.getElementById('edittempsourceacct').value = $(this).attr('data-tempsource');

      if ($(this).attr('data-tempcross') == '2') {

        document.getElementById('edittempnocross').checked = true;
        $('.tempvalas').hide();
        $('#currinhouse').show();
      } else {
        document.getElementById('edittempyescross').checked = true;
        $('.tempvalas').show();
        $('#currinhouse').hide();

        document.getElementById('edittemptranspose').value = $(this).attr('data-temppurpose');

        if ($(this).attr('data-tempexchange') == '1') {
          document.getElementById('edittempcounterrate').checked = true;
        } else {
          document.getElementById('edittempspecialrate').checked = true;
        }

        document.getElementById('edittemprefnum').value = $(this).attr('data-temperefnum');
        document.getElementById('edittempcustnum').value = $(this).attr('data-tempcustref');
        document.getElementById('edittempccy').value = ccy;
      }


      if ($(this).attr('data-tempnotif') == '1') {
        // radiobtn = document.getElementById('notif');
        // radiobtn.checked = true
        document.getElementById('edittempnonotif').checked = true;
        $('#edittemptremailnotif').hide();
        $('#edittemptrsmsnotif').hide();
      } else {
        // radiobtn = document.getElementById('notif');
        // radiobtn.checked = true;
        document.getElementById('edittempyesnotif').checked = true;
        $('#edittemptremailnotif').show();
        $('#edittemptrsmsnotif').show();

        document.getElementById('edittempemailnotif').value = $(this).attr('data-tempemail');
        document.getElementById('edittempsmsnotif').value = $(this).attr('data-tempsms');
      }


      //disabled field kalau ada yg di lock
      var locked = $(this).attr('data-templocked');

      var lockedsplit = locked.split(",");


      for (i = 0; i < lockedsplit.length - 1; i++) {

        if (lockedsplit[i] == 'subject') {
          var subject = true;
        } else if (lockedsplit[i] == 'cross') {
          var cross = true;
        } else if (lockedsplit[i] == 'source') {
          var source = true;
        } else if (lockedsplit[i] == 'purpose') {
          var purpose = true;
        } else if (lockedsplit[i] == 'bene') {
          var bene = true;
        } else if (lockedsplit[i] == 'amount') {
          var amountlock = true;
        } else if (lockedsplit[i] == 'exrate') {
          var exrate = true;
        } else if (lockedsplit[i] == 'notif') {
          var notif = true;
        } else if (lockedsplit[i] == 'notifexpand') {
          var notifexpand = true;
        }
      }

      document.getElementById('edittempsubject').disabled = subject;
      document.getElementById('edittempnocross').disabled = cross;
      document.getElementById('edittempyescross').disabled = cross;
      document.getElementById('edittempsourceacct').disabled = source;
      document.getElementById('edittemptranspose').disabled = purpose;
      document.getElementById('edittempbenef').disabled = bene;
      document.getElementById('edit_tempaccountadd').disabled = bene;
      document.getElementById('edittempccy').disabled = amountlock;
      document.getElementById('edittempamount').disabled = amountlock;
      document.getElementById('edittempcounterrate').disabled = exrate;
      document.getElementById('edittempspecialrate').disabled = exrate;
      document.getElementById('edittemprefnum').disabled = exrate;
      document.getElementById('edittempcustnum').disabled = exrate;
      document.getElementById('edittempnonotif').disabled = notif;
      document.getElementById('edittempyesnotif').disabled = notif;
      document.getElementById('edittempemailnotif').disabled = notifexpand;
      document.getElementById('edittempsmsnotif').disabled = notifexpand;
 
		
	}";  */
	echo "$('.edittemplatelink').click(function(){document.getElementById('edittemplatediv').style.display='block',$('.templatelink').show(),$('.closeBtn').css('margin-right','25%'),document.getElementById('edittempname').value=$(this).attr('data-tempname'),document.getElementById('edittempsubject').value=$(this).attr('data-tempsubject'),document.getElementById('edittempbenef').value=$(this).attr('data-tempbene');var e=(t=$(this).attr('data-tempamount')).split(' ');if(e.length>1)var t=e[1],d=e[0];else t=$(this).attr('data-tempamount'),d='';document.getElementById('edittempamount').value=t,document.getElementById('hiddenid').value=$(this).attr('data-tempid'),document.getElementById('hiddenbenename').value=$(this).attr('data-tempbenename'),document.getElementById('locked').value=$(this).attr('data-templocked'),document.getElementById('edittempsourceacct').value=$(this).attr('data-tempsource'),'2'==$(this).attr('data-tempcross')?(document.getElementById('edittempnocross').checked=!0,$('.tempvalas').hide(),$('#currinhouse').show()):(document.getElementById('edittempyescross').checked=!0,$('.tempvalas').show(),$('#currinhouse').hide(),document.getElementById('edittemptranspose').value=$(this).attr('data-temppurpose'),'1'==$(this).attr('data-tempexchange')?document.getElementById('edittempcounterrate').checked=!0:document.getElementById('edittempspecialrate').checked=!0,document.getElementById('edittemprefnum').value=$(this).attr('data-temperefnum'),document.getElementById('edittempcustnum').value=$(this).attr('data-tempcustref'),document.getElementById('edittempccy').value=d),'1'==$(this).attr('data-tempnotif')?(document.getElementById('edittempnonotif').checked=!0,$('#edittemptremailnotif').hide(),$('#edittemptrsmsnotif').hide()):(document.getElementById('edittempyesnotif').checked=!0,$('#edittemptremailnotif').show(),$('#edittemptrsmsnotif').show(),document.getElementById('edittempemailnotif').value=$(this).attr('data-tempemail'),document.getElementById('edittempsmsnotif').value=$(this).attr('data-tempsms'));var m=$(this).attr('data-templocked').split(',');for(i=0;i<m.length-1;i++)if('subject'==m[i])var a=!0;else if('cross'==m[i])var n=!0;else if('source'==m[i])var l=!0;else if('purpose'==m[i])var s=!0;else if('bene'==m[i])var c=!0;else if('amount'==m[i])var o=!0;else if('exrate'==m[i])var u=!0;else if('notif'==m[i])var p=!0;else if('notifexpand'==m[i])var r=!0;document.getElementById('edittempsubject').disabled=a,document.getElementById('edittempnocross').disabled=n,document.getElementById('edittempyescross').disabled=n,document.getElementById('edittempsourceacct').disabled=l,document.getElementById('edittemptranspose').disabled=s,document.getElementById('edittempbenef').disabled=c,document.getElementById('edit_tempaccountadd').disabled=c,document.getElementById('edittempccy').disabled=o,document.getElementById('edittempamount').disabled=o,document.getElementById('edittempcounterrate').disabled=u,document.getElementById('edittempspecialrate').disabled=u,document.getElementById('edittemprefnum').disabled=u,document.getElementById('edittempcustnum').disabled=u,document.getElementById('edittempnonotif').disabled=p,document.getElementById('edittempyesnotif').disabled=p,document.getElementById('edittempemailnotif').disabled=r,document.getElementById('edittempsmsnotif').disabled=r});";
					echo '</script>';
                  }
                } else {
                  echo '<div class="row">';
                  echo '<div class="alert alert-primary" role="alert" style="width: 90%">';
                  echo "<h6>----No Data----</h6>";
                  echo '</div>';
                  echo '</div>';
         }

		// $this->_db->commit();
	}

	public function templateeditAction()
	{
		// die('here');
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();
		$id = $this->_getParam('id');
		$template = $this->_getParam('template');
		$subject = $this->_getParam('subject');
		$source = $this->_getParam('source');
		$bene = $this->_getParam('bene');
		$amount = str_replace(",", "", $this->_getParam('amount'));
		$notif = $this->_getParam('notif');
		$cross = $this->_getParam('cross');
		$transpose = $this->_getParam('transpose');
		$exrate = $this->_getParam('exrate');
		$refnum = $this->_getParam('refnum');
		$custnum = $this->_getParam('custnum');
		$email = $this->_getParam('email');
		$sms = $this->_getParam('sms');

		// $this->_db->beginTransaction();
		$updateArr = array(
			'T_NAME' 			=> $template,
			'T_CREATEDBY' 		=> $this->_userIdLogin,
			'T_CREATED' 		=> new Zend_Db_Expr("now()"),
			'T_GROUP'			=> $this->_custIdLogin,
		);

		$updateWhere['T_ID = ?'] = (string) $id;
		$this->_db->update('M_TEMPLATE', $updateArr, $updateWhere);

		if (!empty($bene)) {
			$select = $this->_db->select()
				->from(array('A' => 'M_BENEFICIARY'), array('*'));
			$select->where('A.BENEFICIARY_ACCOUNT = ?', (string) $bene);
			$select->where('A.CUST_ID = ?', (string) $this->_custIdLogin);
			$benedata = $this->_db->fetchall($select);
			$benename = $benedata['0']['BENEFICIARY_NAME'];
		} else {
			$benedata = array();
			$benename = '';
		}

		$updateData = array(
			'TEMP_SUBJECT' 			=> $subject,
			'TEMP_SOURCE' 			=> $source,
			'TEMP_BENE' 			=> $bene,
			'TEMP_BENE_NAME' 		=> $benename,
			'TEMP_CROSS' 			=> $cross,
			'TEMP_AMOUNT' 			=> $amount,
			'TEMP_NOTIF' 			=> $notif,
			'TEMP_EMAIL' 			=> $email,
			'TEMP_SMS' 				=> $sms,
			'TEMP_PURPOSE' 			=> $transpose,
			'TEMP_EXCHANGE' 		=> $exrate,
			'TEMP_REFNUM' 			=> $refnum,
			'TEMP_CUSTREF' 			=> $custnum,
		);

		// $myJSON = json_encode($updateData);

		// die($myJSON);

		$updateWhere['TEMP_ID = ?'] = (string) $id;

		$this->_db->update('M_TEMPLATE_DATA', $updateData, $updateWhere);

		// $this->_db->commit();
	}



	public function templatedeleteAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$template = $this->_getParam('template');
		// $subject = $this->_getParam('subject');
		// $source = $this->_getParam('source');
		// $bene = $this->_getParam('bene');
		// $amount = $this->_getParam('amount');
		// $notif = $this->_getParam('notif');
		// $cross = $this->_getParam('cross');

		$this->_db->beginTransaction();
		// $insertArr = array(
		// 	'T_NAME' 			=> $template,
		// 	'T_CREATEDBY' 		=> $this->_userIdLogin,
		// 	'T_CREATED' 		=> new Zend_Db_Expr("now()"),
		// 	'T_GROUP'			=> $this->_custIdLogin,
		// 	);

		$where = array('TEMP_ID = ?' => $template);
		$this->_db->delete('M_TEMPLATE_DATA', $where);
		// $this->_db->insert('M_TEMPLATE',$insertArr);
		// 		$lastId = $this->_db->lastInsertId();

		// 		if(!empty($bene)){
		// 			$select = $this->_db->select()
		// ->from(array('A' => 'M_BENEFICIARY'),array('*'));
		// $select->where('A.BENEFICIARY_ACCOUNT = ?' , (string) $bene);
		// $select->where('A.CUST_ID = ?' , (string) $this->_custIdLogin);
		// $benedata = $this->_db->fetchall($select);
		// $benename = $benedata['0']['BENEFICIARY_NAME'];
		// 		}else{
		// 			$benedata = array();
		// 			$benename = '';
		// 		}



		// $insertData = array(
		// 					'TEMP_ID' 				=> $lastId,
		// 		'TEMP_SUBJECT' 			=> $subject,
		// 		'TEMP_SOURCE' 			=> $source,
		// 		'TEMP_BENE' 			=> $bene,
		// 		'TEMP_BENE_NAME' 		=> $benename,
		// 		'TEMP_CROSS' 			=> $cross,
		// 		'TEMP_AMOUNT' 			=> $amount,
		// 		'TEMP_NOTIF' 			=> $notif,

		// 				);

		// 		$this->_db->insert('M_TEMPLATE_DATA',$insertData);

		$this->_db->commit();
		// echo '<span>te</span>';



		//approval list controller
		//  		$groupDesc = array( '01' => 'Group 1',
		//                        '02' => 'Group 2',
		//                        '03' => 'Group 3',
		//                        '04' => 'Group 4',
		//                        '05' => 'Group 5',
		//                        '06' => 'Group 6',
		//                        '07' => 'Group 7',
		//                        '08' => 'Group 8',
		//   						'09' => 'Group 9',
		//                        '10' => 'Group 10',
		//                        'S'  => 'Special Group');

		//    $this->view->group_desc = $groupDesc;


		// for($i=1;$i<11;$i++)
		// {
		// 	$groupuserid = 'N_'.$this->_custIdLogin.'_0'.$i;
		// 	if(strlen($i) == 2)
		// 	{
		// 		$groupuserid = 'N_'.$this->_custIdLogin.'_'.$i;
		// 	}
		// 	$selectgroup = $this->_db->select()
		// 			       	->from(array('A' => 'M_APP_GROUP_USER'),array('USER_ID'));
		// 	$selectgroup -> where("A.CUST_ID LIKE ".$this->_db->quote($this->_custIdLogin));
		// 	$selectgroup -> where("A.GROUP_USER_ID LIKE ".$this->_db->quote($groupuserid));
		// 	$resultgroup = $selectgroup->query()->FetchAll();
		// 	$groupuseridview = 'group'.$i;
		// 	$this->view->$groupuseridview = $resultgroup;
		// }

		// $specialid = 'S_'.$this->_custIdLogin;
		// $selectspecial = $this->_db->select()
		// 			       		->from(array('A' => 'M_APP_GROUP_USER'),array('USER_ID'));
		// $selectspecial -> where("A.CUST_ID LIKE ".$this->_db->quote($this->_custIdLogin));
		// $selectspecial -> where("A.GROUP_USER_ID LIKE ".$this->_db->quote($specialid));
		// $resultspecial = $selectspecial->query()->FetchAll();
		// $this->view->special = $resultspecial;

		//   $authorizationGroupUser = $this->_db->select()
		//                                  ->from(array('B'=>'M_APP_BOUNDARY'),
		// 					                    array('BOUNDARY_ID','CCY_BOUNDARY', 'BOUNDARY_MIN', 'BOUNDARY_MAX'))
		// 					             ->join(array('BG' => 'M_APP_BOUNDARY_GROUP'),'B.BOUNDARY_ID = BG.BOUNDARY_ID',array('BOUNDARY_GROUP_ID','GROUP_USER_ID'))
		// 					             ->join(array('GU' => 'M_APP_GROUP_USER'),'BG.GROUP_USER_ID = GU.GROUP_USER_ID',array('USER_ID'))
		// 					             ->join(array('U'  => 'M_USER'),'U.USER_ID = GU.USER_ID',array('USER_FULLNAME'))
		// 					             ->where('U.CUST_ID = ?',(string)$this->_custIdLogin)
		// 					             ->where('B.CUST_ID = ?',(string)$this->_custIdLogin)
		// 					             ->order(array('CCY_BOUNDARY','BOUNDARY_MIN'.' '.'ASC'))
		// 					             ->query()->fetchAll();

		// /*Zend_Debug::dump($authorizationGroupUser);
		// die;	*/

		// $authorizationMatrix = array();
		//    $groupArray = array();
		//    $groupUserArray = array();

		// foreach($authorizationGroupUser as $row)
		// {
		//    //MERANGKUM DATA DARI APP BOUNDARY
		//    $authorizationMatrix[$row['BOUNDARY_ID']]['BOUNDARY_MIN'] = $row['BOUNDARY_MIN'];
		//    $authorizationMatrix[$row['BOUNDARY_ID']]['BOUNDARY_MAX'] = $row['BOUNDARY_MAX'];
		//    $authorizationMatrix[$row['BOUNDARY_ID']]['CCY_BOUNDARY'] = $row['CCY_BOUNDARY'];

		//    //example : N_CUSTCHRIS_01 & S_CUSTCHRIS
		//    $explodeGroup = explode('_',$row['GROUP_USER_ID']);

		//    //MERANGKUM DATA UTK BOUNDARY GROUP, tetapi masih ada yg redudant
		//    if($explodeGroup[0] == 'N')
		//    {
		//        $keyGroup   = $explodeGroup[2];
		//        $group_desc = $this->language->_('Group').' ' . (int)$keyGroup;
		//    }
		//    else if($explodeGroup[0] == 'S')
		//    {
		//        $keyGroup = 'S';
		//        $group_desc = $this->language->_('Special');
		//    }

		//    $groupArray[$row['BOUNDARY_ID']][]  = $group_desc;


		//    //MERANGKUM DATA UTK GROUP USER
		//    $groupUserArray[$keyGroup][] = $row['USER_FULLNAME'] . ' ('.$row['USER_ID'].')';

		// }

		// //men-distinct group agar tidak ada yang sama
		// $groupArray_unique = array();
		// foreach($groupArray as $key => $row)
		// {
		//    $groupArray_unique[$key] = array_unique($groupArray[$key]);
		// }

		//    //men-distinct group user agar tidak ada yang sama
		// $groupUserArray_unique = array();
		// foreach($groupUserArray as $key => $row)
		// {
		//    $groupUserArray_unique[$key] = array_unique($row);
		// }

		// $this->view->authorization_matrix = $authorizationMatrix;
		//       $this->view->boundary_group       = $groupArray_unique;
		// $this->view->group_user           = $groupUserArray_unique;

	}

	public function generateTransactionID(){

		$currentDate = date("Ymd");
		$seqNumber	 = strtoupper(Application_Helper_General::str_rand(5));
		$trxId = 'PR'.$currentDate.$this->_custIdLogin.$seqNumber;

		return $trxId;
	}
}
