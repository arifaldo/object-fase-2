<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/SinglePayment.php';
require_once 'CMD/Validate/ValidatePaymentMultiple.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Extendedmodule/Google/GoogleAuthenticator.php';

class singlepayment_ImportbatchController extends Application_Main
{
	protected 	$_moduleDB 	= 'RTF';
	protected	$_rowNum 	= 0;
	protected	$_errmsg 	= null;
	protected	$params		= null;

	public function initController()
	{
		$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';
		$this->_listCCYValidate = Application_Helper_Array::simpleArray($this->getCcy(),'CCY_ID');
	}

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		if($this->_request->isPost() )
		{
			$adapter1 					= new Zend_File_Transfer_Adapter_Http();
			$params 					= $this->_request->getParams();
			$sourceFileName 			= $adapter1->getFileName();

			if($sourceFileName == null)
			{
				$sourceFileName = null;
			}
			else
			{
				$sourceFileName = substr(basename($adapter1->getFileName()), 0);
			}

			$paramsName['sourceFileName'] 	= $sourceFileName;

			$filtersName = array(
									'sourceFileName'		=> array('StringTrim'),
								);

			$validatorsName = array(
									'sourceFileName' => array(
															'NotEmpty',
															'messages' => array(
																					"Error : File size too big or left blank"
																				)
														),
								);
			$zf_filter_input_name = new Zend_Filter_Input($filtersName, $validatorsName, $paramsName, $this->_optionsValidator);

			if($zf_filter_input_name->isValid())
			{
				$filter 	= new Application_Filtering();
				$adapter 	= new Zend_File_Transfer_Adapter_Http ();
// 				print_r($this->_custIdLogin);
// 				print_r($this->_userIdLogin);die;
				$validate   = new ValidatePaymentMultiple($this->_custIdLogin, $this->_userIdLogin);
				$max		= $this->getSetting('max_import_single_payment');

				$adapter->setDestination ( $this->_destinationUploadDir );
				$extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv'));
				$extensionValidator->setMessage(
													'Error: Extension file must be *.csv'
												);

				$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
				$sizeValidator->setMessage(
											'Error: File siz must not more than '.$this->getSetting('Fe_attachment_maxbyte')
										);

				$adapter->setValidators(array($extensionValidator,$sizeValidator));

				if ($adapter->isValid ())
				{
					$sourceFileName = $adapter->getFileName();
					$newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

					$adapter->addFilter ( 'Rename',$newFileName  );

					if ($adapter->receive ())
					{
						$Csv = new Application_Csv ($newFileName,",");
						$csvData = $Csv->readAll ();

						@unlink($newFileName);

						$totalRecords = count($csvData);
						if($totalRecords)
						{
							unset($csvData[0]);
							$totalRecords = count($csvData);
						}

						if ($totalRecords){
							if($totalRecords <= $max)
							{
								$no =0;
								// var_dump($csvData); die;
								foreach ( $csvData as $columns )
								{
									if(count($columns)==10)
									{
										$params['PAYMENT SUBJECT'] 			= trim($columns[0]);
										$params['SOURCE ACCT NO'] 			= trim($columns[1]);
										$params['BENEFICIARY ACCT NO'] 		= trim($columns[2]);
//										$params['BENEFICIARY NAME'] 		= trim($columns[3]);
										$params['CCY'] 						= trim($columns[3]);
										$params['AMOUNT'] 					= trim($columns[4]);
										$params['MESSAGE'] 					= trim($columns[5]);
										$params['ADDITIONAL MESSAGE'] 		= trim($columns[6]);
//										$params['EMAIL ADDRESS'] 			= trim($columns[8]);
										$params['PAYMENT DATE'] 			= trim($columns[7]);
										$params['TRANSFER TYPE'] 			= trim($columns[8]);
										$params['BANK CODE'] 				= trim($columns[9]);
//										$params['BANK NAME'] 				= trim($columns[12]);
										// 									$params['BANK CITY'] 				= trim($columns[13]);
//										$params['CITIZENSHIP'] 				= trim($columns[13]);

										$PS_SUBJECT 		= $filter->filter($params['PAYMENT SUBJECT'],"PS_SUBJECT");
										$PS_EFDATE 			= $filter->filter($params['PAYMENT DATE'],"PS_DATE");
										$TRA_AMOUNT 		= $filter->filter($params['AMOUNT'],"AMOUNT");
										$TRA_MESSAGE 		= $filter->filter($params['MESSAGE'],"TRA_MESSAGE");
										$TRA_REFNO 			= $filter->filter($params['ADDITIONAL MESSAGE'],"TRA_REFNO");
										$ACCTSRC 			= $filter->filter($params['SOURCE ACCT NO'],"ACCOUNT_NO");
										$ACBENEF 			= $filter->filter($params['BENEFICIARY ACCT NO'],"ACCOUNT_NO");
										$ACBENEF_BANKNAME 	= $filter->filter($params['BENEFICIARY NAME'],"ACCOUNT_NAME");
//										$ACBENEF_ALIAS 		= $filter->filter($params['BENEFICIARY NAME'],"ACCOUNT_ALIAS");
//										$ACBENEF_EMAIL 		= $filter->filter($params['EMAIL ADDRESS'],"EMAIL");
										$ACBENEF_CCY 		= $filter->filter($params['CCY'],"SELECTION");
//										$ACBENEF_CITIZENSHIP= $filter->filter($params['CITIZENSHIP'], "SELECTION");
										// 									$BANK_CITY			= $filter->filter($params['BANK CITY'], "ADDRESS");
										$CLR_CODE			= $filter->filter($params['BANK CODE'], "BANK_CODE");
										$TRANSFER_TYPE 		= $filter->filter($params['TRANSFER TYPE'], "SELECTION");
//										$BANK_NAME 			= $filter->filter($params['BANK NAME'],"BANK_NAME");

										$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

										if($TRANSFER_TYPE == 'RTGS'){
											$chargeType = '1';
											$select = $this->_db->select()
															->from('M_CHARGES_OTHER',array('*'))
															->where("CUST_ID = ?",$this->_custIdLogin)
															->where("CHARGES_TYPE = ?",$chargeType);
											$resultSelecet = $this->_db->FetchAll($select);
											$chargeAmt = $resultSelecet['0']['CHARGES_AMT'];

											//$param['TRANSFER_FEE'] = $chargeAmt;
										}
										else if($TRANSFER_TYPE == 'SKN'){
											$chargeType1 = '2';
											$select1 = $this->_db->select()
															->from('M_CHARGES_OTHER',array('*'))
															->where("CUST_ID = ?",$this->_custIdLogin)
															->where("CHARGES_TYPE = ?",$chargeType1);
											$resultSelecet1 = $this->_db->FetchAll($select1);
											$chargeAmt = $resultSelecet1['0']['CHARGES_AMT'];

											//$param['TRANSFER_FEE'] = $chargeAmt1;
										}
										else{
											$chargeAmt = '0';
											//$param['TRANSFER_FEE'] = $chargeAmt2;
										}


										$paramPayment = array(
												"CATEGORY" 					=> "SINGLE PAYMENT",
												"FROM" 						=> "I",				// F: Form, I: Import
												"PS_NUMBER"					=> "",
												"PS_SUBJECT"				=> $PS_SUBJECT,
												"PS_EFDATE"					=> $PS_EFDATE,
												"_dateFormat"				=> $this->_dateUploadFormat,
												"_dateDBFormat"				=> $this->_dateDBFormat,
												"_addBeneficiary"			=> $this->view->hasPrivilege('BADA'),	// privi BADA (Add Beneficiary)
												"_beneLinkage"				=> $this->view->hasPrivilege('BLBU'),	// privi BLBU (Linkage Beneficiary User)
												"_createPB"					=> $this->view->hasPrivilege('CRIP'),								// cannot create PB trx
												"_createDOM"				=> $this->view->hasPrivilege('CRDI'),	// privi CDFT (Create Domestic Fund Transfer)
												"_createREM"				=> $this->view->hasPrivilege('CRIR'),								// cannot create REM trx
										);

										$paramTrxArr[0] = array(
												"TRANSFER_TYPE" 			=> $TRANSFER_TYPE,
												"TRA_AMOUNT" 				=> $TRA_AMOUNT_num,
												"TRANSFER_FEE" 				=> $chargeAmt,
												"TRA_MESSAGE" 				=> $TRA_MESSAGE,
												"TRA_REFNO" 				=> $TRA_REFNO,
												"ACCTSRC" 					=> $ACCTSRC,
												"ACBENEF" 					=> $ACBENEF,
												"ACBENEF_CCY" 				=> $ACBENEF_CCY,
												"ACBENEF_EMAIL" 			=> $ACBENEF_EMAIL,
												"ACBENEF_BANKNAME" 			=> $ACBENEF_BANKNAME,
//												"ACBENEF_ALIAS" 			=> $ACBENEF_ALIAS,
												"ACBENEF_CITIZENSHIP" 		=> $ACBENEF_CITIZENSHIP,		// 1/R: RESIDENT, 2/NR: NON-RESIDENT
												// 																"ACBENEF_ADDRESS1" 			=> $BANK_CITY,
//												"ACBENEF_RESIDENT" 			=> $ACBENEF_RESIDENT,
												"BENEFICIARY_RESIDENT" 		=> $BENEFICIARY_RESIDENT,
												"BANK_CODE" 				=> $CLR_CODE,
//												"BENEFICIARY_BANK_NAME"		=> $BANK_NAME,
//												"LLD_IDENTICAL" 			=> "",
//												"LLD_CATEGORY" 				=> "",
//												"LLD_RELATIONSHIP" 			=> "",
//												"LLD_PURPOSE" 				=> "",
//												"LLD_DESCRIPTION" 			=> "",
												"BENEFICIARY_ID_NUMBER" 	=> $BENEFICIARY_ID_NUMBER,
												"BENEFICIARY_ID_TYPE" 		=> $BENEFICIARY_ID_TYPE,
												"BENEFICIARY_CITY_CODE" 	=> $BENEFICIARY_CITY_CODE,
												"BENEFICIARY_CATEGORY" 		=> $BENEFICIARY_CATEGORY,
												"BANK_NAME" 	=> $BANK_NAME,

										);

										$arr[$no]['paramPayment'] = $paramPayment;
										$arr[$no]['paramTrxArr'] = $paramTrxArr;
									}
									else
									{
										$this->view->error 		= true;
										break;
									}
									$no++;
								}

								if(!$this->view->error)
								{
									$resWs = array();
									$err 	= array();

									$resultVal	= $validate->checkCreate($arr, $resWs);
									$payment 	= $validate->getPaymentInfo();
									$errorTrxMsg 	= $validate->getErrorTrxMsg();

									//Zend_Debug::dump($resWs);die;
									$i = 0;
									foreach($resWs as $key=>$dataAcctType){
										//Zend_Debug::dump($dataAcctType);
										$arr[$key]['paramTrxArr'][0]['ACCOUNT_TYPE'] = $dataAcctType['accountType'];
									}


									//die;

									$sourceAccountType 	= $resWs['accountType'];

									$content['payment'] = $payment;
									$content['arr'] 	= $arr;
									$content['errorTrxMsg'] 	= $errorTrxMsg;
									$content['sourceAccountType'] 	= $sourceAccountType;

									$sessionNamespace = new Zend_Session_Namespace('confirmImportCredit');
									$sessionNamespace->content = $content;

									$this->_redirect('/singlepayment/import/confirm');
								}

							}
							else
							{
								$this->view->error2 = true;
								$this->view->max 	= $max;
							}
						}else{
							$this->view->error = true;
						}
					}
				}
				else
				{
					$this->view->error = true;
					$errors = array($adapter->getMessages());
					$this->view->errorMsg = $errors;
				}
			}
			else
			{
				$this->view->error3		= true;
			}
		}
		Application_Helper_General::writeLog('CRDI','Viewing Create Single Payment Domestic by Import File (CSV)');
		Application_Helper_General::writeLog('CRIP','Viewing Create Single Payment In House by Import File (CSV)');
	}

	public function confirmAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$sessionNamespace = new Zend_Session_Namespace('confirmImportCreditBatch');
		$data = $sessionNamespace->content;

		if($this->_custSameUser){
			$this->view->token = true;

			$selectQuery	= "SELECT GOOGLE_CODE,USER_FAILEDTOKEN FROM M_USER
                 WHERE CUST_ID = " . $this->_db->quote($this->_custIdLogin) . " AND USER_ID = " . $this->_db->quote($this->_userIdLogin) . " AND LTRIM(RTRIM(GOOGLE_CODE)) <> ''";
			// echo $selectQuery;
			$usergoogleAuth =  $this->_db->fetchAll($selectQuery);

			// var_dump($usergoogleAuth);die; 
			if (!empty($usergoogleAuth)) {
				$this->view->googleauth = true;
				//var_dump($usergoogleAuth['0']['USER_FAILEDTOKEN']);
				$settingObj = new Settings();
				$maxtoken = $settingObj->getSetting("max_failed_token");
				$this->view->tokenfail = (int)$maxtoken-1;
				if($usergoogleAuth['0']['USER_FAILEDTOKEN'] != '0'){
					//die;  
					$this->view->failedtoken = $usergoogleAuth['0']['USER_FAILEDTOKEN'];
					
					
					
					$tokenfail = (int)$maxtoken - ((int)$usergoogleAuth['0']['USER_FAILEDTOKEN']+1);
					$this->view->tokenfail = $tokenfail;
				}
				if ($release) {
					$step = $this->_getParam('step');
				} else {
					$step = 3;
				}
			}
			else{
				$this->view->nogoauth = '1';
				if ($release) {
					$step = $this->_getParam('step');
				} else {
					$step = 3;
				}
			}

			

			$this->view->googleauth = true;
		}



		// var_dump($data); die;
		// if(!$data["payment"]["countTrxCCY"])
		// {
		// 	$this->_redirect("/authorizationacl/index/nodata");
		// }

		$this->view->countTrxCCY = $data["payment"]["countTrxCCY"];

		$this->view->sumTrxCCY = $data["payment"]["sumTrxCCY"];

		$totalSuccess = 0;
		foreach($data["payment"]["countTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$totalSuccess += $ccy['success'];
			}
		}

		$amountSuccess = 0;
		foreach($data["payment"]["sumTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$amountSuccess += $ccy['success'];
			}
		}

		$totalFailed = 0;
		foreach($data["payment"]["countTrxCCY"] as $row)
		{
			// var_dump($data["payment"]["countTrxCCY"]); die;
			foreach($row as $ccy)
			{
				$totalFailed += $ccy['failed'];
			}
		}

		$amountFailed = 0;
		foreach($data["payment"]["sumTrxCCY"] as $row)
		{
			foreach($row as $ccy)
			{
				$amountFailed += $ccy['failed'];
			}
		}

		$this->view->totalSuccess = $totalSuccess;
		$this->view->amountSuccess = $amountSuccess;
		$this->view->totalFailed = $totalFailed;
		$this->view->amountFailed = $amountFailed;

		$this->view->PS_FILEID = $data['paramPayment']['PSFILEID'];
		$this->view->PS_CCY = $data['paramPayment']['PS_CCY'];

		$uploadDate = date("Y-m-d");
	    $createDate = date("Y-m-d H:i:s");
	    $this->view->uploadDate = $uploadDate;

	    $countData['TRANSACTION_DATA'] = array();
		foreach($data['paramTrxArray'] as $row)
		{
			$countData['TRANSACTION_DATA'][] = array(

					'PS_CCY' 					=> $row['ACBENEF_CCY'],
					'SOURCE_ACCOUNT'			=> $row['ACCTSRC'],
					'BENEFICIARY_ACCOUNT' 		=> $row['ACBENEF'],
					'BENEFICIARY_ACCOUNT_CCY' 	=> $row['ACBENEF_CCY'],
					'BENEFICIARY_ACCOUNT_NAME' 	=> $row['ACBENEF_BANKNAME'],
	//				$param['BENEFICIARY_ALIAS_NAME'] 	= $row['paramTrxArr'][0]['ACBENEF_ALIAS'];
					'BENEFICIARY_ALIAS_NAME' 	=> $row['ACBENEF_BANKNAME'],
					'BENEFICIARY_EMAIL' 		=> $row['ACBENEF_EMAIL'],
					'BENEFICIARY_ADDRESS' 		=> $row['ACBENEF_ADDRESS1'],
					'BENEFICIARY_CITIZENSHIP' 	=> $row['ACBENEF_CITIZENSHIP'],
					'CLR_CODE' 					=> $row['BANK_CODE'],
					//	$param['BENEFICIARY_BANK_NAME'] 	= $row['paramTrxArr'][0]['BENEFICIARY_BANK_NAME'];
					'TRANSFER_TYPE' 			=> $row['TRANSFER_TYPE'],
					'TRA_AMOUNT' 				=> $row['TRA_AMOUNT'],
					'TRANSFER_FEE' 				=> $row['TRANSFER_FEE'],
					'TRA_MESSAGE' 				=> $row['TRA_MESSAGE'],
					'TRA_REFNO' 				=> $row['TRA_REFNO'],
					'sourceAccountType'			=> $row['ACCOUNT_TYPE'],
					'BENEFICIARY_BANK_NAME' 	=> $row['BANK_NAME'],
					'LLD_CATEGORY' 				=> $row['BENEFICIARY_CATEGORY'],
					'CITY_CODE' 				=> $row['BENEFICIARY_CITY_CODE'],
					'LLD_BENEIDENTIF' 			=> $row['BENEFICIARY_ID_TYPE'],
					'LLD_BENENUMBER' 			=> $row['BENEFICIARY_ID_NUMBER'],
					'PS_SMS' 					=> $row['PS_SMS'],
					'PS_EMAIL' 					=> $row['PS_EMAIL'],
			);

		}

		$totalAmountData = 0;

		if(is_array($countData['TRANSACTION_DATA']))
	 	{
	     	foreach ($countData['TRANSACTION_DATA'] as $key => $paramTransaction)
	     	{
	     		$totalAmountData = $totalAmountData + $paramTransaction['TRA_AMOUNT'];
	     		
	     	}
	    }

	    $this->view->totalAmountData = $totalAmountData;

		$transactionCount 	= count($countData['TRANSACTION_DATA']);

		$this->view->countData = $transactionCount;

		$fields = array(
						'PaymentType'     			 => array(
														'field'    => '',
														'label'    => $this->language->_('Payment Type'),
														'sortable' => false
													),
						'CCY'     			 => array(
														'field'    => '',
														'label'    => $this->language->_('CCY'),
														'sortable' => false
													),
						'Payment Success'     			 => array(
														'field'    => '',
														'label'    => $this->language->_('Payment Success'),
														'sortable' => false
													),
						'Total Amount Success'     			 => array(
														'field'    => '',
														'label'    => $this->language->_('Total Amount Success'),
														'sortable' => false
													),
						'Payment Failed'     			 => array(
														'field'    => '',
														'label'    => $this->language->_('Payment Failed'),
														'sortable' => false
													),
						'Total Amount Failed'     			 => array(
														'field'    => '',
														'label'    => $this->language->_('Total Amount Failed'),
														'sortable' => false
													),
                        );
		$this->view->fields = $fields;

		if($this->_request->isPost() )
		{
			if($this->_getParam('submit_cancel') == TRUE)
			{
				unset($_SESSION['confirmImportCreditBatch']);
				$this->_redirect('/newbatchpayment/');
			}



			if($this->_custSameUser){
										if(!$this->view->hasPrivilege('PRLP')){
											// die('here');
											
											$errMessage = $this->language->_("Error: You don't have privilege to release payment");
											$this->view->error = true;
											$this->view->ERROR_MSG = $this->language->_("Error: You don't have privilege to release payment");
										}else{
											
											///google auth
											$challengeCode		= $this->_getParam('challengeCode');

											$inputtoken1 		= $this->_getParam('inputtoken1');
											$inputtoken2 		= $this->_getParam('inputtoken2');
											$inputtoken3 		= $this->_getParam('inputtoken3');
											$inputtoken4 		= $this->_getParam('inputtoken4');
											$inputtoken5 		= $this->_getParam('inputtoken5');
											$inputtoken6 		= $this->_getParam('inputtoken6');

											$responseCode		= $inputtoken1.$inputtoken2.$inputtoken3.$inputtoken4.$inputtoken5.$inputtoken6;


											$select3 = $this->_db->select()
												 ->from(array('C' => 'M_USER'));
											$select3->where("USER_ID = ".$this->_db->quote($this->_userIdLogin));
											// $select3->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin));
											$data2 = $this->_db->fetchRow($select3);

											// $code = $param['googleauth'];
		

											$pga = new PHPGangsta_GoogleAuthenticator();
									    	 //var_dump($data2['GOOGLE_CODE']);
									    	 //var_dump($code);
									    	 //print_r($responseCode);die();
											$setting 		= new Settings();
											$google_duration 	= $setting->getSetting('google_duration');
									        if($pga->verifyCode($data2['GOOGLE_CODE'], $responseCode, $google_duration))
									        {
									        	$datatoken = array(
															'USER_FAILEDTOKEN' => 0
														);

														$wheretoken =  array();
														$wheretoken['USER_ID = ?'] = $this->_userIdLogin;
														$wheretoken['CUST_ID = ?'] = $this->_custIdLogin;
														$data = $this->_db->update('M_USER',$datatoken,$wheretoken);
									        	$resultToken = $resHard['ResponseCode'] == '0000';
									        	$tokenAuth = true;
									        }else{
									        	$tokenFailed = $CustomerUser->setLogToken();
									        	$tokenAuth = false;	
									        	$this->view->popauth = true;
									        	if ($tokenFailed === true) {
										 		$this->_redirect('/default/index/logout');
										 	}
									        }
										}
										//var_dump($tokenAuth);
					if($tokenAuth){
						$param['sameuser'] = 1;
						$checktoken = true;
						//$result = $BulkPayment->createPaymentBatch($param,$paymentRef);
					}else{
						$checktoken = false;
						$this->view->error = true;
						// $docErr = $this->displayError($zf_filter->getMessages());
						// print_r($docErr);die;
						$this->view->tokenError = true;
						$docErr = 'Invalid Token';
						$this->view->report_msg = $docErr;
					}					
										
				}else{
					//echo '<pre>';
					//var_dump($param);die; 
					$checktoken = true;
					//$result = $BulkPayment->createPaymentBatch($param,$paymentRef);
				}



			

			if($checktoken){
				$i = rand(1,99);
				$param['PS_SUBJECT'] 		= 'Many To Many '.date("Ymd").sprintf("%04s", $i);
				$param['BS_ID'] 			= $data['paramPayment']['BS_ID'];
				$param['PS_FILEID']  		= $data['paramPayment']['PSFILEID'];
				$param['PS_FILE'] 	 		= $data['paramPayment']['PS_FILE'];
				$param['PS_EFDATE']  		= $data['paramPayment']['PS_EFDATE'];
				$param['_addBeneficiary'] 	= $data['paramPayment']['_addBeneficiary'];
				$param['_beneLinkage'] 		= $data['paramPayment']['_beneLinkage'];
				$param["_dateFormat"]		= $data['paramPayment']['_dateFormat'];
				$param["_dateDBFormat"]		= $data['paramPayment']['_dateDBFormat'];
				$param["_createPB"]			= $data['paramPayment']['_createPB'];
				$param["_createDOM"]		= $data['paramPayment']['_createDOM'];
				$param["_createREM"]		= $data['paramPayment']['_createREM'];

				$param['PS_TYPE'] 			= $this->_paymenttype['code']['bulkpayment'];

				$param['TRANSACTION_DATA'] = array();
				foreach($data['paramTrxArray'] as $row)
				{
					$param['TRANSACTION_DATA'][] = array(
							'PS_SUBJECT'				=> $row['PS_SUBJECT'],
							'PS_CCY' 					=> $row['ACBENEF_CCY'],
							'SOURCE_ACCOUNT'			=> $row['ACCTSRC'],
							'BENEFICIARY_ACCOUNT' 		=> $row['ACBENEF'],
							'BENEFICIARY_ACCOUNT_CCY' 	=> $row['ACBENEF_CCY'],
							'BENEFICIARY_ACCOUNT_NAME' 	=> $row['ACBENEF_BANKNAME'],
			//				$param['BENEFICIARY_ALIAS_NAME'] 	= $row['paramTrxArr'][0]['ACBENEF_ALIAS'];
							'BENEFICIARY_ALIAS_NAME' 	=> $row['ACBENEF_BANKNAME'],
							'BENEFICIARY_EMAIL' 		=> $row['ACBENEF_EMAIL'],
							'BENEFICIARY_ADDRESS' 		=> $row['ACBENEF_ADDRESS1'],
							'BENEFICIARY_CITIZENSHIP' 	=> $row['ACBENEF_CITIZENSHIP'],
							'CLR_CODE' 					=> $row['BANK_CODE'],
							//	$param['BENEFICIARY_BANK_NAME'] 	= $row['paramTrxArr'][0]['BENEFICIARY_BANK_NAME'];
							'TRANSFER_TYPE' 			=> $row['TRANSFER_TYPE'],
							'TRA_AMOUNT' 				=> $row['TRA_AMOUNT'],
							'TRANSFER_FEE' 				=> $row['TRANSFER_FEE'],
							'TRA_MESSAGE' 				=> $row['TRA_MESSAGE'],
							'TRA_REFNO' 				=> $row['TRA_REFNO'],
							'sourceAccountType'			=> $row['ACCOUNT_TYPE'],
							'BENEFICIARY_BANK_NAME' 	=> $row['BANK_NAME'],
							'LLD_CATEGORY' 				=> $row['BENEFICIARY_CATEGORY'],
							'CITY_CODE' 				=> $row['BENEFICIARY_CITY_CODE'],
							'LLD_BENEIDENTIF' 			=> $row['BENEFICIARY_ID_TYPE'],
							'LLD_BENENUMBER' 			=> $row['BENEFICIARY_ID_NUMBER'],
							'PS_SMS' 					=> $row['PS_SMS'],
							'PS_EMAIL' 					=> $row['PS_EMAIL'],
					);

				}

			
						$SinglePayment = new SinglePayment("", $this->_custIdLogin, $this->_userIdLogin);
						$result = $SinglePayment->createPaymentBatch($param);
						//$result = $SinglePayment->createPaymentBatchTest($param);

						if($result)
						{
						//awal result

							$sqlTransaction = $this->_db->select()
												->from(array('A' => 'TEMP_BULKTRANSACTION'),array())
												->join(array('B' => 'TEMP_BULKPSLIP'),'A.BS_ID = B.BS_ID',array('A.*','B.*'))
												->where("A.BS_ID = ".$this->_db->quote($result));
							$resultType = $this->_db->fetchAll($sqlTransaction);

							foreach($resultType as $row){

								$where = array('TRANSACTION_ID = ?' => $row['TRANSACTION_ID']);
								$this->_db->delete('TEMP_BULKTYPE', $where);

								$insertArrDetail 					= array();
								$insertArrDetail['TRANSACTION_ID'] 	= $row['TRANSACTION_ID'];
								$insertArrDetail['PS_TYPE'] 		= $row['TRANSFER_TYPE'];
								$insertArrDetail['PS_CATEGORY'] 	= $row['PS_CATEGORY'];

								$this->_db->insert('TEMP_BULKTYPE', $insertArrDetail);

							}

						

							unset($sessionNamespace->content);
							$this->_redirect('/notification/success/index');
						//akhir result
						}
						else
						{
							$this->view->error 		= true;
							$error_msg[0] = 'Error: Transaction failed';
							$this->view->report_msg	= $this->displayError($error_msg);
							$this->_redirect('/singlepayment/importbatch/confirm');
						}

					// }
					// catch(Exception $e)
					// {
					// 	Application_Helper_General::exceptionLog($e);
					// 	$errr = 1;
					// }

				//akhir
				//}
				//die();
				// unset($sessionNamespace->content);

				//$this->_helper->getHelper('FlashMessenger')->addMessage('/'.$this->view->modulename.'/'.$this->view->controllername.'/index');

				// if ($errr != 1)
				// {	$this->_redirect('/notification/success/index');	}
				// else
				// {	$this->_redirect('/notification/success/index');	}	
				// TODO: what to do, if failed create payment


			}else{
				$this->view->error = true;
				$error_msg[0] = 'Error: Invalid Token';
				$this->view->report_msg	= $this->displayError($error_msg);
				$this->_redirect('/singlepayment/importbatch/confirm');
			}

		}
	}

}
