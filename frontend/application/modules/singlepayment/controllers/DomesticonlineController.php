<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/SinglePayment.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'General/CustomerUser.php';
class singlepayment_DomesticonlineController extends Application_Main
{
	protected $_moduleDB = 'RTF';

	public function indexAction()
	{
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}
    	}

    	$USE_CONFIRM_PAGE = true;

    	$periodicEveryArr = array(
    								'1' => $this->language->_('Monday'), 
    								'2' => $this->language->_('Tuesday'), 
    								'3' => $this->language->_('Wednesday'), 
    								'4' => $this->language->_('Thursday'), 
    								'5' => $this->language->_('Friday'), 
    								'6' => $this->language->_('Saturday'), 
    								'7' => $this->language->_('Sunday'),);
		$periodicEveryDateArr = range(1,31);

    	// Set variables needed in view
    	$paramSettingID = array('range_futuredate', //'auto_release', 'cut_off_time_inhouse'
								//'cut_off_time_skn'   , 'cut_off_time_rtgs',
								//'threshold_rtgs'     , 'threshold_lld'
    							);

		$settings 			= new Application_Settings();
		$settings->setSettings(null, $paramSettingID);							// Zend_Registry => 'APPSETTINGS'
		$ccyList  			= $settings->setCurrencyRegistered();				// Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'

		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr 	  = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR")));	// show acc in IDR only
		
		$tranferdatetype = $this->_request->getParam('tranferdatetype');
		if(empty($tranferdatetype)){$tranferdatetype=1;}
		$this->view->tranferdatetype = $tranferdatetype;
		

		$filter 		= new Application_Filtering();
		$PS_NUMBER 		= $filter->filter($this->_getParam('PS_NUMBER'), "PS_NUMBER");
		$PS_EFDATE 		= Application_Helper_General::convertDate($this->getCurrentDate());
		$isConfirmPage 	= $this->_getParam('confirmPage');

		$process 	= $this->_getParam('process');
		$submitBtn 	= ($this->_request->isPost() && $process == "submit")? true: false;

		// get ps_number data for repair payment
    	if (!empty($PS_NUMBER) && !$this->_request->isPost())
    	{
    		$paramList = array("WA" 			=> false,
							   "ACCOUNT_LIST" 	=> $this->_accountList,
							   "_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
							 );
    		$select   = $CustomerUser->getPayment($paramList, false);	// not distinct, cause text cannot be selected as DISTINCT, it's not comparable
			$select->where('P.PS_NUMBER = ?' , (string) $PS_NUMBER);

			$select->columns(array(	"tra_message"			=> "T.TRA_MESSAGE",
									"tra_refno"				=> "T.TRA_REFNO",
									"acbenef_email"			=> "T.BENEFICIARY_EMAIL",
									"bank_name"				=> "T.BENEFICIARY_BANK_NAME",
									"bank_code"				=> "T.BANK_CODE",
									"transfer_type"			=> "T.TRANSFER_TYPE",
									//"acbenef_bankname"		=> "T.BENEFICIARY_ACCOUNT_NAME",
									//"acbenef_alias"			=> "T.BENEFICIARY_ALIAS_NAME",
							  	   )
							 );

			$pslipData 			= $this->_db->fetchRow($select);

			if (!empty($pslipData))
			{
				$PS_SUBJECT 		= $pslipData['paySubj'];
				$PS_EFDATE  		= date("d/m/Y",strtotime($pslipData['efdate']));

				$TRA_AMOUNT  		= Application_Helper_General::displayMoney($pslipData['amount']);
				$TRA_MESSAGE 		= $pslipData['tra_message'];
//				$TRA_REFNO 			= $pslipData['tra_refno'];
				$ACCTSRC 			= $pslipData['accsrc'];
				$ACBENEF  			= $pslipData['acbenef'];
				$ACBENEF_CCY  		= $pslipData['acbenef_ccy'];
				$ACBENEF_BANKNAME	= $pslipData['acbenef_bankname'];
				$ACBENEF_ALIAS  	= $pslipData['acbenef_alias'];
				$ACBENEF_EMAIL  	= $pslipData['acbenef_email'];
				$BANK_NAME			= $pslipData['bank_name'];
				$BANK_CODE			= $pslipData['bank_code'];

				$PS_STATUS 			= $pslipData['PS_STATUS'];
				$PS_TYPE  			= $pslipData['PS_TYPE'];
				$TRANSFER_TYPE  	= $pslipData['transfer_type'];	// 5

				// Payment Status is not Request Repair
				if ($PS_STATUS != $this->_paymentstatus["code"]["requestrepair"])
				{
					$this->_helper->getHelper('FlashMessenger')->addMessage("F");
					$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Payment Status has changed.");
					$this->_redirect('/paymentworkflow/requestrepair/index');
				}
				elseif ($PS_TYPE != $this->_paymenttype["code"]["domestic"])
				{
					$this->_helper->getHelper('FlashMessenger')->addMessage("F");
					$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Payment Type is invalid.");
					$this->_redirect('/paymentworkflow/requestrepair/index');
				}
				elseif ($TRANSFER_TYPE != $this->_transfertype["code"]["ONLINE"])
				{
					$this->_helper->getHelper('FlashMessenger')->addMessage("F");
					$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Transfer Type is invalid.");
					$this->_redirect('/paymentworkflow/requestrepair/index');
				}

				$trfTypeArr = array_combine($this->_transfertype["code"], $this->_transfertype["desc"]);	// Array([2] => SKN, [1] => RTGS)
				$TRANSFER_TYPE = $trfTypeArr[$TRANSFER_TYPE];	// ONLINE
			}
			else	// ps_number is invalid, or not belong to customer
			{
				$this->_helper->getHelper('FlashMessenger')->addMessage("F");
				$this->_helper->getHelper('FlashMessenger')->addMessage("Error: Payment Ref# is invalid.");
				$this->_redirect('/paymentworkflow/requestrepair/index');
			}
    	}

		if (!empty($PS_NUMBER) && !$this->view->hasPrivilege('PRPP'))
		{
			$this->_helper->getHelper('FlashMessenger')->addMessage("F");
			$this->_helper->getHelper('FlashMessenger')->addMessage("Error: You don't have privilege to repair payment.");
			$this->_redirect('/paymentworkflow/requestrepair/index');
		}

		if($this->_request->isPost() )
		{
			$PS_SUBJECT 		= $filter->filter($this->_request->getParam('PS_SUBJECT')		, "PS_SUBJECT");
			$PS_EFDATE 			= $filter->filter($this->_request->getParam('PS_EFDATE')		, "PS_DATE");
			$TRA_AMOUNT 		= $filter->filter($this->_request->getParam('TRA_AMOUNT')		, "AMOUNT");
			$TRA_MESSAGE 		= $filter->filter($this->_request->getParam('TRA_MESSAGE')		, "TRA_MESSAGE");
//			$TRA_REFNO 			= $filter->filter($this->_request->getParam('TRA_REFNO')		, "TRA_REFNO");
			$ACCTSRC 			= $filter->filter($this->_request->getParam('ACCTSRC')			, "ACCOUNT_NO");
			$ACBENEF 			= $filter->filter($this->_request->getParam('ACBENEF')			, "ACCOUNT_NO");
			$ACBENEF_BANKNAME 	= $filter->filter($this->_request->getParam('ACBENEF_BANKNAME')	, "ACCOUNT_NAME");
			$ACBENEF_ALIAS 		= $filter->filter($this->_request->getParam('ACBENEF_ALIAS')	, "ACCOUNT_ALIAS");
			$ACBENEF_EMAIL 		= $filter->filter($this->_request->getParam('ACBENEF_EMAIL')	, "EMAIL");
			$ACBENEF_CCY 		= $filter->filter($this->_request->getParam('CURR_CODE')		, "SELECTION");
			$BANK_NAME			= $filter->filter($this->_request->getParam('BANK_NAME')		, "BANK_NAME");
			$BANK_CODE			= $filter->filter($this->_request->getParam('BANK_CODE')		, "BANK_CODE");
			$TRANSFER_TYPE		= "ONLINE";

			$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

			$filter->__destruct();
			unset($filter);

			// post submit payment
			if ($submitBtn)
			{

				//cek date kosong
				if(!$PS_EFDATE){

					$error_msg		 		= 'Payment Date can not be left blank.';
					$this->view->error 		= true;
					$this->view->ERROR_MSG	= $error_msg;

				}
				else{

					$validateDateFormat = new Zend_Validate_Date(array('format' => $this->_dateDisplayFormat));
					if(!$validateDateFormat->isValid($PS_EFDATE))
					{
						$error_msg = 'Invalid Payment Date Format. Payment Date Format must be dd/MM/yyyy';
						$this->view->error 		= true;
						$this->view->ERROR_MSG	= $error_msg;

					}
					else{

						$paramPayment = array(	"CATEGORY" 					=> "SINGLE DOM",
												"FROM" 						=> "F",				// F: Form, I: Import
												"PS_NUMBER"					=> $PS_NUMBER,
												"PS_SUBJECT"				=> $PS_SUBJECT,
												"PS_EFDATE"					=> $PS_EFDATE,
												"_dateFormat"				=> $this->_dateDisplayFormat,
												"_dateDBFormat"				=> $this->_dateDBFormat,
												"_addBeneficiary"			=> $this->view->hasPrivilege('BADA'),	// privi BADA (Add Beneficiary)
												"_beneLinkage"				=> $this->view->hasPrivilege('BLBU'),	// privi BLBU (Linkage Beneficiary User)
												"_createPB"					=> false,								// cannot create PB trx
												"_createDOM"				=> true, //$this->view->hasPrivilege('CROL'),	// privi CROL (Create Domestic Online)
												"_createREM"				=> false,								// cannot create REM trx
											 );

						$paramTrxArr[0] = array("TRANSFER_TYPE" 			=> $TRANSFER_TYPE,
												"TRA_AMOUNT" 				=> $TRA_AMOUNT_num,
												"TRA_MESSAGE" 				=> $TRA_MESSAGE,
												"TRA_REFNO" 				=> "",
												"ACCTSRC" 					=> $ACCTSRC,
												"ACBENEF" 					=> $ACBENEF,
												"ACBENEF_CCY" 				=> $ACBENEF_CCY,
												"ACBENEF_EMAIL" 			=> $ACBENEF_EMAIL,

											// for Beneficiary data, except (bene CCY and email), must be passed by reference
												"ACBENEF_BANKNAME" 			=> &$ACBENEF_BANKNAME,
												"ACBENEF_ALIAS" 			=> &$ACBENEF_ALIAS,
												"ACBENEF_CITIZENSHIP" 		=> "",			// W:WNI, N: WNA
												"ACBENEF_RESIDENT" 			=> "",			// 1/R: RESIDENT, 2/NR: NON-RESIDENT
												"ACBENEF_ADDRESS1" 			=> "",

												"BANK_CODE" 				=> $BANK_CODE,

												"LLD_IDENTICAL" 			=> "",
												"LLD_CATEGORY" 				=> "",
												"LLD_RELATIONSHIP" 			=> "",
												"LLD_PURPOSE" 				=> "",
												"LLD_DESCRIPTION" 			=> "",
											 );

						$validate   = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin);
						$resultVal	= $validate->checkCreate($paramPayment, $paramTrxArr);

						if($validate->isError() === false)	// payment data is valid
						{
							$payment 		= $validate->getPaymentInfo();

							if ($USE_CONFIRM_PAGE == true && $isConfirmPage == false)
							{
								$isConfirmPage 	= true;
								$ACCTSRC_CCY   	= $payment["acctsrcArr"][$ACCTSRC]["CCY_ID"];
								$ACCTSRC_NAME   = $payment["acctsrcArr"][$ACCTSRC]["ACCT_NAME"];
								$ACCTSRC_ALIAS  = $payment["acctsrcArr"][$ACCTSRC]["ACCT_ALIAS"];
								$ACCTSRC_TYPE   = $payment["acctsrcArr"][$ACCTSRC]["ACCT_TYPE"];
								$ACCTSRC_view 	= Application_Helper_General::viewAccount($ACCTSRC, $ACCTSRC_CCY, $ACCTSRC_NAME, $ACCTSRC_ALIAS, $ACCTSRC_TYPE);

								$validate->__destruct();
								unset($validate);

								require_once 'General/Charges.php';
								$trfType		= $TRANSFER_TYPE;
								$chargesObj 	= Charges::factory($this->_custIdLogin, $trfType);
								$paramCharges 	= array("accsrc" => $ACCTSRC, "transferType" => $trfType);
								$chargesAMT 	= 0; //$chargesObj->getCharges($paramCharges);
								$chargesCCY 	= $ACCTSRC_CCY;
							}
							else
							{
								$validate->__destruct();
								unset($validate);

								$ACCTSRC_CCY   	= $payment["acctsrcArr"][$ACCTSRC]["CCY_ID"];

								$param = array();
								$param['PS_SUBJECT'] 					= $PS_SUBJECT;
								$param['PS_CCY'] 						= $ACCTSRC_CCY;
								$param['PS_EFDATE'] 					= Application_Helper_General::convertDate($PS_EFDATE, $this->_dateDBFormat, $this->_dateDisplayFormat);
								$param['TRA_AMOUNT'] 					= $TRA_AMOUNT_num;
								$param['TRA_MESSAGE'] 					= $TRA_MESSAGE;
								$param['TRA_REFNO'] 					= "";
								$param['SOURCE_ACCOUNT'] 				= $ACCTSRC;
								$param['BENEFICIARY_ACCOUNT'] 			= $ACBENEF;
								$param['BENEFICIARY_ACCOUNT_CCY'] 		= $ACBENEF_CCY;
								$param['BENEFICIARY_ACCOUNT_NAME'] 		= $ACBENEF_BANKNAME;
								$param['BENEFICIARY_ALIAS_NAME'] 		= $ACBENEF_ALIAS;
								$param['BENEFICIARY_BANK_NAME'] 		= $BANK_NAME;
								$param['BENEFICIARY_EMAIL'] 			= $ACBENEF_EMAIL;
								$param['CLR_CODE']		 				= $BANK_CODE;
								$param['TRANSFER_TYPE']		 			= $TRANSFER_TYPE;
								$param['_addBeneficiary'] 				= $paramPayment["_addBeneficiary"];
								$param['_beneLinkage'] 					= $paramPayment["_beneLinkage"];
								$param['_priviCreate'] 					= 'CROL';

								try
								{
									$SinglePayment = new SinglePayment($PS_NUMBER, $this->_custIdLogin, $this->_userIdLogin);
									if (!empty($PS_NUMBER))
									{	$SinglePayment->isRepair = true;	}

									$result = $SinglePayment->createPaymentDomestic($param);

									$ns = new Zend_Session_Namespace('FVC');
									$ns->backURL = '/'.$this->view->modulename.'/'.$this->view->controllername.'/index';

									if ($result === true)
									{	$this->_redirect('/notification/success/index');	}
									else
									{	$this->_redirect('/notification/success/index');	}	// TODO: what to do, if failed create payment
								}
								catch(Exception $e)
								{
									Application_Helper_General::exceptionLog($e);
								}
							}
						}
						else
						{
							$errorMsg 		= $validate->getErrorMsg();
							$errorTrxMsg 	= $validate->getErrorTrxMsg();	// array

							$validate->__destruct();
							unset($validate);

							$errMessage 	= (!empty($errorMsg))? $errorMsg: reset(reset(reset($errorTrxMsg)));

							$this->view->error 		= true;
							$this->view->ERROR_MSG	= $errMessage;
						}
					}
				}
			}
			else
				$isConfirmPage 	= false;

		}

		$TRA_MESSAGE_len = (isset($TRA_MESSAGE))? strlen($TRA_MESSAGE): 0;
//		$TRA_REFNO_len 	 = (isset($TRA_REFNO))  ? strlen($TRA_REFNO)  : 0;

		$TRA_MESSAGE_len = 120 - $TRA_MESSAGE_len;
//		$TRA_REFNO_len 	 = 200 - $TRA_REFNO_len;
		if($PS_EFDATE == ''){
			$PS_EFDATE = Application_Helper_General::convertDate($this->getCurrentDate());
		}
		$this->view->AccArr 			= $AccArr;
		$this->view->PS_NUMBER 			= (isset($PS_NUMBER))			? $PS_NUMBER			: '';
		$this->view->PS_SUBJECT 		= (isset($PS_SUBJECT))			? $PS_SUBJECT			: '';
		$this->view->PS_EFDATE 			= (isset($PS_EFDATE)) ? $PS_EFDATE			: Application_Helper_General::convertDate($this->getCurrentDate());
		$date_val	= date('d/m/Y');
		if ($PS_EFDATE == $date_val){
			$todaytranfer = $this->language->_('Today Transfer');
			$this->view->paymentType = $todaytranfer;
		}
		else{
			$pendingfuturedate = $this->language->_('Future Date Transfer');
			$this->view->paymentType = $pendingfuturedate;
		}
		$date_val	= date('d/m/Y');
		$PS_EFDATE_VAL = $this->_request->getParam('PS_EFDATE');
		$this->view->paymentDate = $PS_EFDATE_VAL;
		// $dadte 			= (isset($PS_EFDATE)) ? $PS_EFDATE			: Application_Helper_General::convertDate($this->getCurrentDate());
		// print_r($dadte);
		// print_r();die;
		$this->view->TRA_AMOUNT 		= (isset($TRA_AMOUNT))			? $TRA_AMOUNT			: '';
		$this->view->TRA_MESSAGE 		= (isset($TRA_MESSAGE))			? $TRA_MESSAGE			: '';
//		$this->view->TRA_REFNO 			= (isset($TRA_REFNO))			? $TRA_REFNO			: '';
		$this->view->TRA_MESSAGE_len	= $TRA_MESSAGE_len;
//		$this->view->TRA_REFNO_len		= $TRA_REFNO_len;

		$this->view->ACCTSRC 			= (isset($ACCTSRC))				? $ACCTSRC				: '';
		$this->view->ACCTSRC_view		= (isset($ACCTSRC_view))		? $ACCTSRC_view			: '';
		$this->view->ACBENEF 			= (isset($ACBENEF))				? $ACBENEF				: '';
		$this->view->ACBENEF_BANKNAME	= (isset($ACBENEF_BANKNAME))	? $ACBENEF_BANKNAME		: '';
		$this->view->ACBENEF_ALIAS 		= (isset($ACBENEF_ALIAS))		? $ACBENEF_ALIAS		: '';
		$this->view->ACBENEF_EMAIL 		= (isset($ACBENEF_EMAIL))		? $ACBENEF_EMAIL		: '';
		$this->view->CURR_CODE 			= (isset($ACBENEF_CCY))			? $ACBENEF_CCY			: '';
		$this->view->BANK_NAME			= (isset($BANK_NAME))			? $BANK_NAME			: '';

		$this->view->BANK_CODE 			= (isset($BANK_CODE))			? $BANK_CODE			: "";
		$this->view->CHARGES_AMT 		= (isset($chargesAMT))			? Application_Helper_General::displayMoney($chargesAMT)	: '';
		$this->view->CHARGES_CCY 		= (isset($chargesCCY))			? $chargesCCY			: '';

		$this->view->confirmPage		= $isConfirmPage;
		$this->view->useConfirmPage		= $USE_CONFIRM_PAGE;
		$this->view->periodicEveryArr  	= $periodicEveryArr;
		$this->view->periodicEveryDateArr  = $periodicEveryDateArr;
	}
}
