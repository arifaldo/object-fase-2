<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/SinglePayment.php';
require_once 'CMD/Validate/ValidateAccountSource.php';
require_once 'General/CustomerUser.php';
require_once 'General/Settings.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'SGO/Extendedmodule/Google/GoogleAuthenticator.php';
class singlepayment_VirtualaccountController extends Application_Main
{
	public function initModel()
	{
		$this->model 					= new payment_Model_Payment();
		$this->param['SERVICE_TYPE'] 	= 16;
		$this->param['PROVIDER_TYPE'] 	= 1;
		$this->param['fetch'] 			= 'fetchAll';
		$this->param['USER_ID'] 		= $this->_userIdLogin;
	}

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$sessionNamespace 		= new Zend_Session_Namespace('phone');
		$paramSession 			= $sessionNamespace->paramSession;

		$this->view->radioCheck	= 1;
		$this->view->userIdLogin  = $this->_userIdLogin;

		/*$CustomerUser 			= new CustomerUser($this->_userIdLogin);
		$AccArr 	  			= $CustomerUser->getAccounts();*/

		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr 	  = $CustomerUser->getAccounts();

		$this->view->AccArr 	= $AccArr;
		$anyValue = '-- ' . $this->language->_('Any Value') . ' --';
		$arr 						= $this->model->getProviderId(16, 1);
		$providerArr 				= array('' => $anyValue);
		$providerArr 				+= Application_Helper_Array::listArray($arr, 'PROVIDER_ID', 'PROVIDER_NAME');

		$this->view->providerArr 	= $providerArr;

		$list 						= $this->model->cekListVA($this->param);
		$this->view->paymentList 	= $list;


		$settings = new Application_Settings();
		$ccyList  = $settings->setCurrencyRegistered();
		$this->view->ccyArr 			= $ccyList;

		//added token
		$userData = $this->_db->select()
			->from(array('M_USER'), array('USER_ID', 'TOKEN_TYPE', 'TOKEN_ID', 'USER_MOBILE_PHONE'))
			->where('USER_ID = ?', $this->_userIdLogin)
			->limit(1);
		$userData = $this->_db->fetchRow($userData);

		//added token type
		$this->view->tokentype = $userData['TOKEN_TYPE'];
		$this->view->tokenIdUser = $userData['TOKEN_ID'];
		$tokenIdUser = $userData['TOKEN_ID'];
		$tokenType = $userData['TOKEN_TYPE'];
		$userMobilePhone = trim($userData['USER_MOBILE_PHONE']);

		if ($this->_custSameUser) {
			// echo 'here1';
			$this->view->token = true;
			$userOnBehalf = $this->_userIdLogin;
			$tokenIdUser = $this->_db->select()
				->from(
					array('M_USER'),
					array('TOKEN_ID', 'GOOGLE_CODE')
				)
				->where('USER_ID = ?', $userOnBehalf)
				->where('CUST_ID = ?', $this->_custIdLogin)
				->limit(1);

			$tokenIdUser = $this->_db->fetchRow($tokenIdUser);
			$tokenIdUser = $tokenIdUser['TOKEN_ID'];
			$tokenGoogle = $tokenIdUser['GOOGLE_CODE'];

			$Token 			= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
			$challengeCode 	= $Token->generateChallengeCode();
			$this->view->userOnBehalf		= $userOnBehalf;
			$this->view->challengeCode		= $challengeCode;
			if (!empty($tokenGoogle)) {
				// die('here');
				$this->view->googleauth = true;
			}
		}

		$this->view->googleauth = true;

		$cust_id = $this->_custIdLogin;

		$selectUsergroup = $this->_db->select()
			->from(array('M_APP_GROUP_USER'), array('*'))
			->where('CUST_ID = ?', $cust_id)
			->group('GROUP_USER_ID')
			// echo $selectUser;die;
			->query()->fetchall();

		$selectUsers = $this->_db->select()
			->from(array('M_APP_GROUP_USER'), array('*'))
			->where('CUST_ID = ?', $cust_id)
			// echo $selectUser;die;
			->query()->fetchall();
		// print_r($selectUser);die;

		$userlists = '';
		foreach ($selectUsergroup as $key => $value) {
			foreach ($selectUsers as $no => $val) {
				if ($val['GROUP_USER_ID'] == $value['GROUP_USER_ID']) {
					if (empty($userlists))
						$userlists .= $val['USER_ID'];
					else
						$userlists .= ', ' . $val['USER_ID'];
				}
			}
			$selectUsergroup[$key]['USER'] .= $userlists;
			$userlists = '';
			$spesials = 'S_' . $cust_id;
			if ($value['GROUP_USER_ID'] == $spesials) {
				$selectUsergroup[$key]['GID'] .= 'SG';
			} else {
				$groups = explode('_', $value['GROUP_USER_ID']);
				$alphabets = array(1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'E', 6 => 'F', 7 => 'G', 8 => 'H', 9 => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z');

				// $cust = explode('_', $value['GROUP_USER_ID'])
				$selectUsergroup[$key]['GID'] .= $alphabets[(int) $groups[2]];
			}
		}
		$this->view->selectUsergroup = $selectUsergroup;

		//added new hard token
		$HardToken = new Service_Token($this->_custIdLogin, $this->_userIdLogin, $tokenIdUser);
		$challengeCode = $HardToken->generateChallengeCode();
		$this->view->challengeCode = $challengeCode;
		$this->view->challengeCodeReq = $challengeCode;
		// print_r($this->_request->getParams());die;
		if ($this->_getParam('submitBtn') == TRUE) {
			$msg = array();
			$param = array();
			$param['SERVICE_TYPE'] = '16';
			$param['PROVIDER_TYPE'] = 1; //1
			$param['operator']		= $arr[0]['PROVIDER_ID']; //$this->_getParam('operator');
			//$param['orderId']		= $this->_getParam('orderId');
			$param['ACCTSRC']		= $this->_getParam('ACCTSRC');
			$param['cek']			= $this->_getParam('cek');
			$param['radioCheck']	= $this->_getParam('radioCheck');
			$param['payment']		= $this->_getParam('payment');
			$param['namaperusahaan']	= $this->_getParam('namaperusahaan');

			$full = $this->_getParam('full');



			$param['amount']		= $this->_getParam('amount');
			$param['novirtual'] 	= $this->_getParam('orderId');
			$this->view->novir 		= $param['novirtual'];
			// print_r($this->_request->getParams());
			$sendProvider = new SinglePayment(null, $this->_userIdLogin);
			$resProvider = $sendProvider->createPaymentPurchase($param, $msg);
			// print_r($resProvider);die;
			$sgo_product_code_data	= $this->model->getSgoProductCode($param['operator']);
			$sgo_product_code = isset($sgo_product_code_data['0']['SGO_PRODUCT_CODE']);

			$listProviderName = $this->model->getProviderName($param['operator'], 1);
			$providerName = $listProviderName['PROVIDER_NAME'];

			if ($param['radioCheck'] == 1) {
				$param['orderId'] 			= $this->_getParam('orderId');
				$ACBENEF					= $param['orderId'];
				$this->view->orderId		= $param['orderId'];
			} else if ($param['radioCheck'] == 2) {
				$param['orderId'] 			= $this->_getParam('novirtual');
				$ACBENEF					= $param['novirtual'];
				//$this->view->orderId		= $param['novirtual'];
			}

			$VPData = $this->_db->select()
				->from(array('T' => 'T_VP_TRANSACTION'), array('INVOICE_NAME', 'INVOICE_DESC', 'AMOUNT', 'VP_NUMBER'))
				->join(array('G' => 'M_CUSTOMER'), 'T.CUST_ID = G.CUST_ID', array('G.CUST_NAME'))
				->where('VP_NUMBER = ?', $param['orderId'])
				->where('VP_STATUS = ?', '1')
				->limit(1);
			$VirtualData = $this->_db->fetchRow($VPData);


			$param['productCode'] = $sgo_product_code; //STCKAI
			$biller = new Service_Biller();

			//added roki (hardcode)
			if ($param['radioCheck'] == 1) {
				$paramSession['orderId']			= $param['orderId'];
			} else if ($param['radioCheck'] == 2) {
				$paramSession['orderId']			= $param['novirtual'];
			}

			$charges = $this->getCharges($param['operator']);
			if ($charges) {
				$fee = $charges[$detailCustomer['CCY_ID']]['CHARGES_AMT'];
			} else {
				$fee = 0;
			}

			$paramSession['operator']			= $param['operator'];
			$paramSession['ACCTSRC']			= $param['ACCTSRC'];
			$sessionNamespace->paramSession = $paramSession;
			$this->_redirect('/singlepayment/virtualaccount/next');
			//

			//if($phone == TRUE)
			// die('masuk');
			if (count($msg) < 1) {

				//begin
				//validasi hard token page 1 -- begin
				$filter_acct 	= new Application_Filtering();
				$challengeCodeReq = $filter_acct->filter($this->_getParam('challengeCodeReq'), "challengeCodeReq");
				$responseCodeReq  = $filter_acct->filter($this->_getParam('responseCodeReq'), "responseCodeReq");

				if ($full) {
					$param['amount']	 	= Application_Helper_General::convertDisplayMoney($VirtualData['AMOUNT']);
				} else {
					$param['amount']	 	= Application_Helper_General::convertDisplayMoney($this->_getParam('amount'));
				}
				// add hamdan, validasi VA
				$paramPayment = array(
					"CATEGORY" 					=> "TRANSFER VA",
					"FROM" 						=> "F",				// F: Form, I: Import
					"PS_NUMBER"					=> '',
					"PS_SUBJECT"				=> '',
					"PS_EFDATE"					=> date('d/m/Y'),
					"_dateFormat"				=> $this->_dateDisplayFormat,
					"_dateDBFormat"				=> $this->_dateDBFormat,
					"_addBeneficiary"			=> $this->view->hasPrivilege('BADA'),		// privi BADA (Add Beneficiary)
					"_beneLinkage"				=> $this->view->hasPrivilege('BLBU'),		// privi BLBU (Linkage Beneficiary User)
					"_createPB"					=> $this->view->hasPrivilege('CRSP'),		// privi CRSP (Create Single Payment)
					"_createDOM"				=> false,									// cannot create DOM trx
					"_createREM"				=> false,									// cannot create REM trx
					"TRA_CCY"					=> 'IDR'
				);
				$paramTrxArr[0] 				= array(
					"TRANSFER_TYPE" 		=> "PB",
					"TRA_AMOUNT" 				=> $param['amount'],
					"TRA_MESSAGE" 				=> '',
					"TRA_REFNO" 				=> '',
					"ACCTSRC" 					=> $param['ACCTSRC'],
					"ACBENEF" 					=> $ACBENEF,
					"ACBENEF_CCY" 				=> 'IDR',
					"ACBENEF_BANKNAME" 			=> &$ACBENEF_BANKNAME,
					"ACBENEF_ALIAS" 			=> &$ACBENEF_ALIAS,
					"ACBENEF_EMAIL" 			=> &$ACBENEF_EMAIL,
					"INVOICE_DESC"				=> $VirtualData['INVOICE_DESC'],
					"INVOICE_NAME"				=> $VirtualData['INVOICE_NAME'],
					"AMOUNT"					=> $param['amount'],
					"CUST_NAME"					=> $VirtualData['CUST_NAME'],
				);
				//end VA

				//validasi hard token page 1 -- begin
				/*if($tokenType == '1'){ //sms token
					$resultToken = TRUE;
				}
				elseif($tokenType == '2'){ //hard token
					if(empty($responseCodeReq)){
						$errM = 'Error : Response Token cannot be left blank.';
						$this->view->ERROR_MSG_N = $errM;
					}
					else{
						$resHard = $HardToken->verifyHardToken($challengeCodeReq, $responseCodeReq);
						$resultToken = $resHard['ResponseCode'] == '0000';
						//set user lock token gagal
						if ($resHard['ResponseCode'] != '0000')
						{
							$CustUser = new CustomerUser($this->_userIdLogin);
							$tokenFailed = $CustUser->setFailedTokenMustLogout();
						}
					}
				}
				elseif($tokenType == '3'){ //mobile token
					$resultToken = TRUE;
				}
				else{$resultToken = TRUE;}*/
				//validasi hard token page 1 -- end

				//if($validate->isError() === false && $resultToken === true){

				//if($resultToken == TRUE){

				if ($this->_custSameUser) {
					if ($tokenType == '2') { //hard token
						$challengeCodeReq		= $this->_getParam('challengeCode');

						$inputtoken1 		= $this->_getParam('inputtoken1');
						$inputtoken2 		= $this->_getParam('inputtoken2');
						$inputtoken3 		= $this->_getParam('inputtoken3');
						$inputtoken4 		= $this->_getParam('inputtoken4');
						$inputtoken5 		= $this->_getParam('inputtoken5');
						$inputtoken6 		= $this->_getParam('inputtoken6');

						$responseCodeReq		= $inputtoken1 . $inputtoken2 . $inputtoken3 . $inputtoken4 . $inputtoken5 . $inputtoken6;

						if (empty($responseCodeReq)) {
							$errM = 'Error : Response Token cannot be left blank.';
							$this->view->ERROR_MSG_N = $errM;
						} else {
							$resHard = $HardToken->verifyHardToken($challengeCodeReq, $responseCodeReq);
							$resultToken = $resHard['ResponseCode'] == '0000';
							//set user lock token gagal
							if ($resHard['ResponseCode'] != '0000') {
								$CustUser = new CustomerUser($this->_userIdLogin);
								$tokenFailed = $CustUser->setFailedTokenMustLogout();
							}
						}
					}
				} else {
					$resultToken = '0000';
				}
				$paramSettingID = array('range_futuredate', 'auto_release_payment');
				$settings->setSettings(null, $paramSettingID);	// Zend_Registry => 'APPSETTINGS'
				$resAcct = array();
				$validate   = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin);
				$resultVal	= $validate->checkCreate($paramPayment, $paramTrxArr, $resAcct);
				$sourceAccountType = $resAcct['accountType'];
				//echo $ACBENEF_BANKNAME."ggg"; die;
				if ($validate->isError() == false && $resultToken == '0000') {
					// die('here');
					$amount = $param['amount'];

					$detailCustomer	= $this->model->getCustomerData($this->param);
					$validateACCTSRC = new ValidateAccountSource($param['ACCTSRC'], $this->_custIdLogin, $this->_userIdLogin);

					$this->param['ACCT_NO'] = $param['ACCTSRC'];
					$charges = $this->getCharges($param['operator']);
					if ($charges) {
						$fee = $charges[$detailCustomer['CCY_ID']]['CHARGES_AMT'];
					} else {
						$fee = 0;
					}
					if ($amount  == '') {
						$amountGet = '0.00';
					} else {
						$amountGet = $amount;
					}

					$params['amount']	=  $param['amount'];
					/*
								TO DO GET AMOUNT FROM BILLER
							*/
					$total 				= $params['amount'] + $fee;
					$check 				= $validateACCTSRC->check($total);

					if ($check == true) {

						$this->param['ACCT_NO'] = $param['ACCTSRC'];
						$detailCustomer	= $this->model->getCustomerData($this->param);

						$paramsearch['PROVIDER_ID'] 		= $param['operator'];
						$paramsearch['fetch'] 				= 'fetchRow';
						$arr 								= $this->model->getProvider($paramsearch);
						if (isset($param['cek'])) {
							$paramsearch['USER_ID'] 		= $this->_userIdLogin;
							$paramsearch['REF_NO'] 			= $param['orderId'];
							$result 						= $this->model->cekList($paramsearch);

							if (!$result) {
								$paramSession['cek']		= $param['cek'];
							}
						}

						$paramSession['operator']			= $param['operator'];
						$paramSession['operatorNama']		= $providerName;

						if ($param['radioCheck'] == 1) {
							$paramSession['orderId']			= $param['orderId'];
						} else if ($param['radioCheck'] == 2) {
							$paramSession['orderId']			= $param['novirtual'];
						}

						$charges = $this->getCharges($param['operator']);
						if ($charges) {
							$fee = $charges[$detailCustomer['CCY_ID']]['CHARGES_AMT'];
						} else {
							$fee = 0;
						}

						$paramSession['operator']			= $param['operator'];
						$paramSession['ACCTSRC']			= $param['ACCTSRC'];
						$paramSession['ccy']				= $detailCustomer['CCY_ID'];
						$paramSession['ACCT_NAME']			= $customer_name_ori;
						$paramSession['ACCT_NAME_DATA']		= $detailCustomer['ACCT_NAME'];
						$paramSession['ACCT_ALIAS_NAME']	= $detailCustomer['ACCT_ALIAS_NAME'];
						$paramSession['ACCT_TYPE']			= $detailCustomer['ACCT_TYPE'];
						$paramSession['FREEZE_STATUS']		= $detailCustomer['FREEZE_STATUS'];
						$paramSession['MAXLIMIT']			= $detailCustomer['MAXLIMIT'];
						$paramSession['amount']				= $params['amount'];
						$paramSession['fee']				= $fee;
						$paramSession['total']				= $total;
						$paramSession['sgoProductCode']		= $param['productCode'];
						$paramSession['amount']				= $param['amount'];
						$paramSession['ACBENEF_BANKNAME']	= $ACBENEF_BANKNAME;


						$paramSession["INVOICE_DESC"]				= $VirtualData['INVOICE_DESC'];
						$paramSession["INVOICE_NAME"]				= $VirtualData['INVOICE_NAME'];
						$paramSession["CUST_NAME"]					= $VirtualData['CUST_NAME'];
						$paramSession['BENEFICIARY_ACCOUNT']		= $paramSession['orderId'];
						$paramSession['BENEFICIARY_ACCOUNT_NAME']	= $ACBENEF_BANKNAME;
						$paramSession['BENEFICIARY_ALIAS_NAME']		= $ACBENEF_BANKNAME;
						$paramSession['BENEFICIARY_EMAIL']			= '';
						$paramSession['cek'] 						= $param['cek'];
						$paramSession['sourceAccountType'] 			= $sourceAccountType;

						$sessionNamespace->paramSession = $paramSession;

						if ($res['ResponseCode'] != '00') {
							$this->_redirect('/singlepayment/virtualaccount/nextlast');
						} else {

							$this->view->operator				= $param['operator'];
							$this->view->ACCTSRC				= $param['ACCTSRC'];
							$this->view->cek					= $param['cek'];
							$this->view->radioCheck				= $param['radioCheck'];
							$this->view->amount					= $param['amount'];

							$this->view->ERROR_MSG_N = $res['ResponseDesc'];
						}
					} else {
						$errMsg 			= $validateACCTSRC->getErrorMsg();
						$error				= true;
					}
				} else {
					$errMessage 	= '';
					$errorMsg 		= (!empty($errorMsg)) ? $errorMsg : $validate->getErrorMsg();
					$errorTrxMsg 	= $validate->getErrorTrxMsg();	// array
					$validate->__destruct();
					unset($validate);
					if (!empty($errorMsg) || !empty($errorTrxMsg)) //{
						$errMessage 	= (!empty($errorMsg)) ? $errorMsg : reset(reset(reset($errorTrxMsg)));

					$this->view->error 		= true;
					$this->view->ERROR_MSG	= $errMessage;
					$this->view->ACCTSRC				= $param['ACCTSRC'];
					$this->view->amount		= $param['amount'];
				}
				/*}
				else{

						$this->view->xACCTSRC 				= (isset($errMsg))? $errMsg : null;

						$this->view->operator				= $param['operator'];
						$this->view->ACCTSRC				= $param['ACCTSRC'];
						$this->view->cek					= $param['cek'];
						$this->view->radioCheck				= $param['radioCheck'];
						$this->view->amount					= $param['amount'];
						//$this->view->ERROR_MSG_N = $errM;

						if(empty($responseCodeReq)){}else{
							$this->view->ERROR_MSG_N = 'Error : Invalid Token APPLI 2';
						}

				}*/
				//end
			} else {

				$errors 	= $msg;
				$error		= true;
			}

			if (isset($error)) {

				$this->view->error 					= $error;
				$this->view->operatorErr 			= (isset($errors['operator'])) ? $errors['operator'] : null;
				$this->view->orderIdErr 			= (isset($errors['orderId'])) ? $errors['orderId'] : null;
				$this->view->paymentErr 			= (isset($errors['novirtual'])) ? $errors['novirtual'] : null;
				$this->view->amountErr 				= (isset($errors['amount'])) ? $errors['amount'] : null;
				$this->view->ACCTSRCErr 			= (isset($errors['ACCTSRC'])) ? $errors['ACCTSRC'] : null;
				$this->view->xACCTSRC 				= (isset($errMsg)) ? $errMsg : null;

				$this->view->operator				= $param['operator'];
				$this->view->ACCTSRC				= $param['ACCTSRC'];
				$this->view->cek					= $param['cek'];
				$this->view->radioCheck				= $param['radioCheck'];
				$this->view->amount					= $param['amount'];
				$this->view->novirtual				= $this->_getParam['novirtual'];
				$this->view->paymentList			= $list;

				if ($param['radioCheck'] == 1) {
					//$this->view->orderId					= $param['orderId'];
				} else if ($param['radioCheck'] == 2) {

					//$this->view->orderId					= $param['novirtual'];
				}
			}
		} else if ($this->_getParam('submit') == true) {
			$this->cancel();
		} else {
			unset($_SESSION['virtualaccount']);
		}
		Application_Helper_General::writeLog('CRVA', 'Viewing Create Single Payment Virtual Account');
	}

	public function nextAction()
	{
		$this->view->userId	= $this->_userIdLogin;
		$SinglePayment 		= new SinglePayment(null, $this->_userIdLogin);
		$sessionNamespace 	= new Zend_Session_Namespace('phone');
		$paramSession 		= $sessionNamespace->paramSession;


		$paramSession['berita'] = $this->_getParam('berita');
		$paramSession['berita2'] = $this->_getParam('berita2');

		$TRA_MESSAGE_len = (isset($paramSession['berita'])) ? strlen($paramSession['berita']) : 0;
		$TRA_MESSAGE_len = 100 - $TRA_MESSAGE_len;
		$this->view->TRA_MESSAGE_len	= $TRA_MESSAGE_len;

		//table
		$fields = array(
			'va_id' => array(
				'field'    => 'VA_ID',
				'label'    => $this->language->_('VA ID'),
				'sortable' => false
			),

			'desc'     => array(
				'field'    => 'DESC',
				'label'    => $this->language->_('Description'),
				'sortable' => false
			),

			'ref_number'   => array(
				'field'    => 'REF_NUMBER',
				'label'    => $this->language->_('Ref Number'),
				'sortable' => false
			),

			'exp_time'     => array(
				'field'    => 'EXP_TIME',
				'label'    => $this->language->_('Expired Time'),
				'sortable' => false
			),

			'ccy'    => array(
				'field'  => 'CCY',
				'label'    => $this->language->_('CCY'),
				'sortable' => false
			),
			'amount'    => array(
				'field'  => 'AMOUNT',
				'label'    => $this->language->_('Amount'),
				'sortable' => false
			),
			'charge'    => array(
				'field'  => 'CHARGE',
				'label'    => $this->language->_('Charge'),
				'sortable' => false
			)
		);

		//validasi sort, jika input sort bukan ASC atau DESC
		$sortBy  = $this->_getParam('sortby');
		$sortBy  = (Zend_Validate::is($sortBy, 'InArray', array(array_keys($fields)))) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir, 'InArray', array('transaction_time' => array('asc', 'desc')))) ? $sortDir : 'asc';

		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		$this->view->fields = $fields;
		//

		// add hamdan
		$detailCustomer	= $this->model->getCustomerData(array('ACCT_NO' => $paramSession['ACCTSRC']));
		// echo "<pre>";
		// var_dump($detailCustomer);
		// die;
		$CustCCY = $detailCustomer['CCY_ID'];
		$CustName = $detailCustomer['ACCT_NAME'];
		$CustAliasName = $detailCustomer['ACCT_ALIAS_NAME'];
		$CustDesc = $detailCustomer['ACCT_TYPE'];

		$this->view->CustCCY = $CustCCY;
		$this->view->CustName = $CustName;
		$this->view->CustAliasName = $CustAliasName;
		$this->view->CustDesc = $CustDesc;
		// end hamdan

		//added token
		$userData = $this->_db->select()
			->from(array('M_USER'), array('USER_ID', 'TOKEN_TYPE', 'TOKEN_ID', 'USER_MOBILE_PHONE'))
			->where('USER_ID = ?', $this->_userIdLogin)
			->limit(1);
		$userData = $this->_db->fetchRow($userData);
		//added token type
		$this->view->tokentype = $userData['TOKEN_TYPE'];
		$this->view->tokenIdUser = $userData['TOKEN_ID'];
		$tokenIdUser = $userData['TOKEN_ID'];
		$tokenType = $userData['TOKEN_TYPE'];
		$userMobilePhone = trim($userData['USER_MOBILE_PHONE']);

		//add hardtoken
		//$HardToken = new Service_Token($this->_custIdLogin, $this->_userIdLogin, $tokenIdUser);


		if ($this->_custSameUser) {
			// echo 'here1';
			$this->view->token = true;
			$userOnBehalf = $this->_userIdLogin;
			$tokenIdUser = $this->_db->select()
				->from(
					array('M_USER'),
					array('TOKEN_ID')
				)
				->where('USER_ID = ?', $userOnBehalf)
				->where('CUST_ID = ?', $this->_custIdLogin)
				->limit(1);

			$tokenIdUser = $this->_db->fetchRow($tokenIdUser);
			$tokenIdUser = $tokenIdUser['TOKEN_ID'];

			$this->view->hidetoken = true;
			$Token 			= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
			$challengeCode 	= $Token->generateChallengeCode();
			$this->view->userOnBehalf		= $userOnBehalf;
			$this->view->challengeCode		= $challengeCode;
		}
		//Approval matirx
		$cust_id = $this->_custIdLogin;

		$selectUsergroup = $this->_db->select()
			->from(array('M_APP_GROUP_USER'), array('*'))
			->where('CUST_ID = ?', $cust_id)
			->group('GROUP_USER_ID')
			// echo $selectUser;die;
			->query()->fetchall();

		$selectUsers = $this->_db->select()
			->from(array('M_APP_GROUP_USER'), array('*'))
			->where('CUST_ID = ?', $cust_id)
			// echo $selectUser;die;
			->query()->fetchall();
		// print_r($selectUser);die;
		$userlists = '';
		foreach ($selectUsergroup as $key => $value) {
			foreach ($selectUsers as $no => $val) {
				if ($val['GROUP_USER_ID'] == $value['GROUP_USER_ID']) {
					if (empty($userlists))
						$userlists .= $val['USER_ID'];
					else
						$userlists .= ', ' . $val['USER_ID'];
				}
			}
			$selectUsergroup[$key]['USER'] .= $userlists;
			$userlists = '';
			$spesials = 'S_' . $cust_id;
			if ($value['GROUP_USER_ID'] == $spesials) {
				$selectUsergroup[$key]['GID'] .= 'SG';
			} else {
				$groups = explode('_', $value['GROUP_USER_ID']);
				$alphabets = array(1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'E', 6 => 'F', 7 => 'G', 8 => 'H', 9 => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z');

				// $cust = explode('_', $value['GROUP_USER_ID'])
				$selectUsergroup[$key]['GID'] .= $alphabets[(int) $groups[2]];
			}
		}
		$this->view->selectUsergroup = $selectUsergroup;
		//////

		// if (!$paramSession) {
		// 	$this->cancel();
		// }
		// print_r($this->_request->getParams());die;
		if ($this->_getParam('submit') == TRUE) {

			//rocki hardcode
			$PS_NUMBER 		= $SinglePayment->generatePaymentID(1);
			$paramSession['PS_NUMBER'] = $PS_NUMBER;
			$paramSession['AMOUNT'] = $this->_getParam('amount');
			$paramSession['date'] = date('d/m/Y');

			$sessionNamespace->paramSession = $paramSession;
			$this->_redirect('/singlepayment/virtualaccount/confirm');
			////

			$validator = new Payment(null, $this->_custIdLogin, $this->_userIdLogin);
			$createLimit = $validator->createLimit($paramSession['amount'], $paramSession['ccy'], $paramSession['ACCTSRC'], $msg1);

			$PS_NUMBER 		= "P" . $SinglePayment->generatePaymentID(1);
			$PS_EFDATE		= $this->model->date();

			$SinglePayment 	= new SinglePayment($PS_NUMBER, $this->_userIdLogin);

			$setting 	= new Settings();
			$bank_code 	= $setting->getSetting('bank_code');

			//			if(isset($paramSession['cek']))
			//			{
			//				$paramcek['cek'] = $paramSession['cek'];
			//				$param +=$paramcek;
			//			}
			$sessionNamespace->paramSession = $paramSession;
			$this->_redirect('/singlepayment/virtualaccount/nextlast');
		} else if ($this->_getParam('submit1') == true) {
			$this->_redirect('/singlepayment/virtualaccount/index');
		}

		$this->view->paramSession		= $paramSession;
	}

	public function nextlastAction()
	{
		$app = Zend_Registry::get('config');
		$app = $app['app']['bankcode'];

		$this->view->userId	= $this->_userIdLogin;
		$SinglePayment 		= new SinglePayment(null, $this->_userIdLogin);
		$sessionNamespace 	= new Zend_Session_Namespace('phone');
		$paramSession 		= $sessionNamespace->paramSession;

		$PS_EFDATE 				= date('d/m/Y');
		$this->view->PS_EFDATE 	= (isset($PS_EFDATE)) ? $PS_EFDATE : '';

		//added token
		/*$userData = $this->_db->select()
		->from(array('M_USER'),array('USER_ID','TOKEN_TYPE','TOKEN_ID','USER_MOBILE_PHONE'))
		->where('USER_ID = ?', $this->_userIdLogin)
		->limit(1)
		;
		$userData = $this->_db->fetchRow($userData);
		//added token type
		$this->view->tokentype = $userData['TOKEN_TYPE'];
		$this->view->tokenIdUser = $userData['TOKEN_ID'];
		$tokenIdUser = $userData['TOKEN_ID'];
		$tokenType = $userData['TOKEN_TYPE'];
		$userMobilePhone = trim($userData['USER_MOBILE_PHONE']);*/

		if ($this->_custSameUser) {
			// echo 'here1';
			$this->view->token = true;
			$userOnBehalf = $this->_userIdLogin;
			$tokenIdUser = $this->_db->select()
				->from(
					array('M_USER'),
					array('TOKEN_ID')
				)
				->where('USER_ID = ?', $userOnBehalf)
				->where('CUST_ID = ?', $this->_custIdLogin)
				->limit(1);

			$tokenIdUser = $this->_db->fetchRow($tokenIdUser);
			$tokenIdUser = $tokenIdUser['TOKEN_ID'];

			$this->view->hidetoken = true;
			$Token 			= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
			$challengeCode 	= $Token->generateChallengeCode();
			$this->view->userOnBehalf		= $userOnBehalf;
			$this->view->challengeCode		= $challengeCode;
		}

		// add hamdan
		$detailCustomer	= $this->model->getCustomerData(array('ACCT_NO' => $paramSession['ACCTSRC']));
		$CustCCY = $detailCustomer['CCY_ID'];
		$CustName = $detailCustomer['ACCT_NAME'];
		$CustAliasName = $detailCustomer['ACCT_ALIAS_NAME'];
		$CustDesc = $detailCustomer['ACCT_TYPE'];

		$this->view->CustCCY = $CustCCY;
		$this->view->CustName = $CustName;
		$this->view->CustAliasName = $CustAliasName;
		$this->view->CustDesc = $CustDesc;
		// end hamdan

		$paramPayment = array(
			"CATEGORY" 					=> "SINGLE PB",
			"FROM" 						=> "F",				// F: Form, I: Import
			"PS_NUMBER"					=> '',
			"PS_SUBJECT"				=> '',
			"PS_EFDATE"					=> date('d/m/Y'),
			"_dateFormat"				=> $this->_dateDisplayFormat,
			"_dateDBFormat"				=> $this->_dateDBFormat,
			"_addBeneficiary"			=> $this->view->hasPrivilege('BADA'),		// privi BADA (Add Beneficiary)
			"_beneLinkage"				=> $this->view->hasPrivilege('BLBU'),		// privi BLBU (Linkage Beneficiary User)
			"_createPB"					=> $this->view->hasPrivilege('CRSP'),		// privi CRSP (Create Single Payment)
			"_createDOM"				=> false,									// cannot create DOM trx
			"_createREM"				=> false,									// cannot create REM trx
		);

		//add hardtoken
		//$HardToken = new Service_Token($this->_custIdLogin, $this->_userIdLogin, $tokenIdUser);

		// if (!$paramSession) {
		// 	$this->cancel();
		// }

		if ($this->_getParam('submit1') == TRUE) {
			$validator = new Payment(null, $this->_custIdLogin, $this->_userIdLogin);
			$createLimit = $validator->createLimit($paramSession['amount'], $paramSession['ccy'], $paramSession['ACCTSRC'], $msg1);
			$PS_NUMBER 		= $SinglePayment->generatePaymentID(1);
			//$filter 		= new Application_Filtering();
			//$PS_NUMBER 		= $filter->filter($this->_getParam('PS_NUMBER'), "PS_NUMBER");
			$PS_EFDATE		= $this->model->date();

			$SinglePay 	= new SinglePayment($PS_NUMBER, $this->_custIdLogin, $this->_userIdLogin);


			$setting 	= new Settings();
			$bank_code 	= $setting->getSetting('bank_code');

			$param = array();
			$param['PS_SUBJECT'] 					= '';
			$param['PS_CCY'] 						= $paramSession['ccy'];
			$param['PS_EFDATE'] 					= Application_Helper_General::convertDate($PS_EFDATE, $this->_dateDBFormat, $this->_dateDisplayFormat);
			//$param['EXPIRY_DATE'] 					= Application_Helper_General::convertDate($EXPIRY_DATE, $this->_dateDBFormat, $this->_dateDisplayFormat);
			$param['TRA_AMOUNT'] 					= $paramSession['amount'];
			$param['TRA_MESSAGE'] 					= $paramSession['berita'];
			$param['TRA_REFNO'] 					= $paramSession['berita2'];
			$param['SOURCE_ACCOUNT'] 				= $paramSession['ACCTSRC'];
			$param['BENEFICIARY_ACCOUNT'] 			= $paramSession['BENEFICIARY_ACCOUNT']; //'100291000022';
			$param['BENEFICIARY_ACCOUNT_CCY'] 		= $paramSession['ccy'];
			$param['BENEFICIARY_ACCOUNT_NAME'] 		= $paramSession['BENEFICIARY_ACCOUNT_NAME'];
			$param['BENEFICIARY_ALIAS_NAME'] 		= $paramSession['BENEFICIARY_ACCOUNT_NAME'];
			$param['BENEFICIARY_EMAIL'] 			= '';
			$param['_addBeneficiary'] 				= $paramPayment["_addBeneficiary"];
			$param['_beneLinkage'] 					= $paramPayment["_beneLinkage"];
			$param['_priviCreate'] 					= 'CRVA';
			$param['cek'] 							= $paramSession['cek'];
			$param['PS_TYPE'] 						= '0';
			$param['PROVIDER_ID'] 					= $paramSession['operator'];
			$param['PROVIDER_ID'] 					= $paramSession['operator'];
			$param['VA'] 							= '6';
			$param['sourceAccountType'] 			= $paramSession['sourceAccountType'];
			$param['VA_NUMBER'] 					= $paramSession['BENEFICIARY_ACCOUNT'];

			//token validasi here added agung
			/*$filters = array('*' => array('HtmlEntities', 'StripTags', 'StringTrim'));
				$validators = array(
					'responseCode' => array(
						'notEmpty',
						'alnum',
						'presence' => 'required',
					)
				);*/

			//$filter = new Zend_Filter_Input($filters, $validators, $this->_getAllParams(), array('breakChainOnFailure' => true));

			if ($this->_custSameUser) {

				$inputtoken1 		= $this->_getParam('inputtoken1');
				$inputtoken2 		= $this->_getParam('inputtoken2');
				$inputtoken3 		= $this->_getParam('inputtoken3');
				$inputtoken4 		= $this->_getParam('inputtoken4');
				$inputtoken5 		= $this->_getParam('inputtoken5');
				$inputtoken6 		= $this->_getParam('inputtoken6');

				$responseCode		= $inputtoken1 . $inputtoken2 . $inputtoken3 . $inputtoken4 . $inputtoken5 . $inputtoken6;

				// $HardToken 		= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
				// $resHard = $HardToken->validateOtp($responseCode);
				// $resultToken = $resHard['ResponseCode'] == '0000';
				// // print_r($resHard);die;
				// if ($resHard['ResponseCode'] != '00'){
				// 	$tokenFailed = $CustUser->setLogToken(); //log token activity

				// 	$this->view->error = true;
				// 	$errorMsg[] = $this->language->_('Invalid Response Code');	//$verToken['ResponseDesc'];

				// 	if ($tokenFailed === true)
				// 	{
				// 		$this->_redirect('/default/index/logout');
				// 	}
				// }

				$Token 		= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
				$verToken 	= $Token->verify($challengeCode, $responseCode);

				if ($verToken['ResponseCode'] != '00') {
					$tokenFailed = $CustUser->setLogToken(); //log token activity

					$error = true;
					$errorMsg[] = $this->language->_('Invalid Response Code');	//$verToken['ResponseDesc'];

					if ($tokenFailed === true) {
						$this->_redirect('/default/index/logout');
					}
				}
			}


			if ($createLimit == TRUE) {

				//$return = $SinglePay->createVirtualAccount($param);
				// print_r($param);die;
				$return = $SinglePay->createPaymentWithin($param);
				$sessionNamespace->psNumber 	= $return;
				$payReff = $PS_NUMBER = $return;

				if ($paramSession['cek'] == 1) {
					try {
						$select	= $this->_db->select()
							->from(array('TPL' => 'T_PAY_LIST'));
						$select->where("TPL.USER_BANK_CODE = ? ", $app);
						$select->where("TPL.USER_ID = ? ", $this->_userIdLogin);
						$select->where("TPL.REF_NO = ? ", $param['BENEFICIARY_ACCOUNT']);
						$select->where("TPL.PROVIDER_ID = ? ", $param['PROVIDER_ID']);
						$tpaylist = $this->_db->fetchAll($select);

						if (!$tpaylist) {
							$arrTlist = array(
								'USER_ID' 			=> $this->_userIdLogin,
								'USER_BANK_CODE' 	=> $app,
								'PROVIDER_ID' 		=> $param['PROVIDER_ID'],
								'REF_NO' 			=> $param['BENEFICIARY_ACCOUNT'],
								'ACCT_NAME' 		=> $param['BENEFICIARY_ACCOUNT_NAME'],
							);
							$this->_db->insert('T_PAY_LIST', $arrTlist);
						}
					} catch (Exception $e) {
						Application_Log_GeneralLog::backgroundLog(Zend_Debug::dump($e, __FILE__ . __FUNCTION__, FALSE), array());
						//Zend_Debug::dump($e->getMessage());
						//						$this->_db->rollBack();
						//						$result = false;
						Application_Helper_General::exceptionLog($e);
						//$result = 'Code : '.$e->getCode().', Message : '.$e->getMessage();
					}
				}

				if ($return) {

					$paramSession['PS_NUMBER'] 		= $payReff;
					$paramSession['date']			= $PS_EFDATE;
					$paramSession['paymentMessage']	= 'Payment Success';
					$sessionNamespace->paramSession = $paramSession;
					$ns->backURL = '/' . $this->view->modulename . '/' . $this->view->controllername . '/index';
					//$this->_redirect('/singlepayment/virtualaccount/confirm');
					$this->_redirect('/notification/success/index');
					//$ns->backURL = '/'.$this->view->modulename.'/'.$this->view->controllername.'/index';

				}
			} else {
				$error_msg	 			= $msg1;
				$this->view->error 		= true;
				$this->view->ERROR_MSG	= $error_msg;
			}
		} else if ($this->_getParam('submit') == true) {
			$this->_redirect('/singlepayment/virtualaccount/next');
		}

		$this->view->paramSession			= $paramSession;
	}

	public function confirmAction()
	{
		$pdf = $this->_getParam('pdf');
		$sessionNamespace 					= new Zend_Session_Namespace('phone');
		$this->view->ERROR_MSG_N			= $paramSession['paymentMessage'];
		$paramSession 						= $sessionNamespace->paramSession;

		// if (!$paramSession) {
		// 	$this->cancel();
		// }

		if ($this->_getParam('submit') == TRUE) {
			///rocki hardcode
			$this->_redirect('/notification/success/index');

			////
			$this->_redirect('/singlepayment/virtualaccount/index');
		}

		$sessionNamespace->paramSession 	= $paramSession;
		$this->view->paramSession			= $paramSession;

		// add hamdan
		$detailCustomer	= $this->model->getCustomerData(array('ACCT_NO' => $paramSession['ACCTSRC']));
		$CustCCY = $detailCustomer['CCY_ID'];
		$CustName = $detailCustomer['ACCT_NAME'];
		$CustAliasName = $detailCustomer['ACCT_ALIAS_NAME'];
		$CustDesc = $detailCustomer['ACCT_TYPE'];

		$this->view->CustCCY = $CustCCY;
		$this->view->CustName = $CustName;
		$this->view->CustAliasName = $CustAliasName;
		$this->view->CustDesc = $CustDesc;
		$this->view->PS_NUMBER = $paramSession['PS_NUMBER'];

		// end hamdan
		if ($pdf) {
			$datapdf = SGO_Helper_GeneralFunction::formatPurchaseNote($paramSession['PSNumber'], $paramSession);
			$this->_helper->download->pdf(null, null, null, 'Payment Report Detail', $datapdf);
		}
	}

	public function cancel()
	{
		unset($_SESSION['virtualaccount']);
		$this->_redirect("/home/index");
	}
}
