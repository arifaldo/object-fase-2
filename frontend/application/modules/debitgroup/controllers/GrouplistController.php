<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Grouping.php';
class accountgroup_GrouplistController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti

	public function initController()
	{
		$listCcy = array(''=>'-- Select Currency --');
		$listCcy = array_merge($listCcy,Application_Helper_Array::listArray($this->getCcy(),'CCY_ID','DESCRIPTION'));
		$this->view->ccy = $listCcy;
	}

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$fields = array(
						'group_name'  => array('field' => 'GROUP_NAME',
											   'label' => $this->language->_('Group Name'),
											   'sortable' => true),
						'created_by'  => array('field' => 'CREATEDBY',
											   'label' => $this->language->_('Created By'),
											   'sortable' => true),
						'created_date'  => array('field' => 'CREATE_DATETIME',
											   'label' => $this->language->_('Created Date and Time'),
											   'sortable' => true),
						'status'   => array('field'    => 'STATUS',
											  'label'    => $this->language->_('Status'),
											  'sortable' => true),
						'action'   => array('field'    => 'GROUP_ISREQUESTDELETE',
											  'label'    => $this->language->_('Action'),
											  'sortable' => false)
				);
		$filterlist = array('GROUP_NAME','CREATEDBY');

		$this->view->filterlist = $filterlist;

		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','doc_no');
		$sortDir = $this->_getParam('sortdir','asc');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;

		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';


		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	'filter' 	  	=> array('StringTrim','StripTags'),
							'GROUP_NAME' 	=> array('StringTrim','StripTags','StringToUpper'),
							'CREATEDBY'    => array('StringTrim','StripTags','StringToUpper')
		);

		$dataParam = array("GROUP_NAME","CREATEDBY");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{

			// print_r($dtParam);die;
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}

			}

			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}

		$zf_filter = new Zend_Filter_Input($filterArr,array(),$dataParamValue);
		// $filter = $zf_filter->getEscaped('filter');
		$filter 		= $this->_getParam('filter');

		$alpha = $this->_getParam('alpha');

		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		$this->view->alpha = $alpha;

		$listStatus = array( '0' => 'Not Approved',
							 '1' => 'Approved'
						   );

		$caseStatus = "(CASE GROUP_ISAPPROVED ";
		foreach($listStatus as $key=>$val)
		{
			$caseStatus .= " WHEN '".$key."' THEN '".$val."' ";
		}
		$caseStatus .= " END)";

		$select = $this->_db->select()
					   ->from(array('A' => 'M_GROUPING'),array('*','STATUS'=>$caseStatus));
		$select->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin));
		if($alpha) $select->where("GROUP_NAME LIKE ".$this->_db->quote($alpha.'%'));

		if($filter == TRUE)
		{
			$fName = $zf_filter->getEscaped('GROUP_NAME');
			$fBy = $zf_filter->getEscaped('CREATEDBY');
			// print_r($fName);die;
	        if($fName)$select->where('UPPER(GROUP_NAME) LIKE '.$this->_db->quote('%'.strtoupper($fName).'%'));
	        if($fBy)$select->where('UPPER(CREATEDBY) LIKE '.$this->_db->quote('%'.strtoupper($fBy).'%'));
			// echo $select;die;
			$this->view->group_name = $fName;
			$this->view->created_by = $fBy;
		}

	    $select->order($sortBy.' '.$sortDir);
		$this->paging($select);
		Application_Helper_General::writeLog('GRGG','Viewing Group List');
		$this->view->fields = $fields;
	 if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;

      }
		$this->view->filter = $filter;
	}

	public function addgrouplistAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		if($this->_request->isPost() )
		{
			$filters = array( 	'GROUPNAME' 	=> array('StringTrim','StripTags','StringToUpper'));

			$validators = array('GROUPNAME' => array(	'NotEmpty',
															//new Zend_Validate_StringLength(array('max'=>50)),
															'Alnum',
															'messages' => array(
																$this->language->_('Error: Group Name cannot be left blank. Please correct it.'),
																$this->language->_('Error: Wrong Group Name format. Please correct it.'),
																)
														));

			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			//Zend_Debug::Dump($this->_request->getParams());die;
			if($zf_filter_input->isValid())
			{
				$content = array(
							'CUST_ID_LOGIN' 			=> $this->_custIdLogin,
							'USER_ID_LOGIN' 			=> $this->_userIdLogin,
							'GROUP_NAME' 				=> $zf_filter_input->GROUPNAME
								);
				try
				{

					//-----insert benef--------------
					$this->_db->beginTransaction();
					$Grouping = new Grouping();
					$Grouping->add($content);
					$this->_db->commit();

					Application_Helper_General::writeLog('GRGG','Adding Group');

					$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
					$this->_redirect('/notification/success');
				}
				catch(Exception $e)
				{
					//rollback changes
					$this->_db->rollBack();

					$this->fillParam($zf_filter_input);
					$errorMsg = $this->getErrorRemark('82');
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);

					Application_Helper_General::writeLog('GRGG','Roll Back Adding Group');
				}
			}
			else
			{
				$this->view->error = true;
				$this->fillParam($zf_filter_input);
				$docErr = $this->displayError($zf_filter_input->getMessages());
				$this->view->report_msg = $docErr;
				Application_Helper_General::writeLog('GRGG','Invalid Data Adding Group');
			}

			$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
			if(count($temp)>1){
				if($temp[0]=='F' || $temp[0]=='S'){
					if($temp[0]=='F')
						$this->view->error = 1;
					else
						$this->view->success = 1;
					$msg = ''; unset($temp[0]);
					foreach($temp as $value)
					{
						if(!is_array($value))
							$value = array($value);
						$msg .= $this->view->formErrors($value);
					}
					$this->view->report_msg = $msg;
				}
			}
		}
	}

	public function editgrouplistAction()
	{
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}
    	}

		if(!$this->_request->isPost())
		{
			$group_id = $this->_getParam('group_id');
			$group_id = (Zend_Validate::is($group_id,'Digits'))? $group_id : null;
			if($group_id)
			{
			  $resultdata = $this->_db->fetchRow(
								$this->_db->select()
									 ->from(array('M_GROUPING'))
									 ->where("GROUP_ID=?", $group_id)
							);
			  if($resultdata)
			  {
					$this->view->GROUP_ID = $resultdata['GROUP_ID'];
					$this->view->GROUPNAME 	= $resultdata['GROUP_NAME'];
			   }
			}
			else
			{
			   $error_remark = 'Error: Group ID does not exist.';

			   $this->_helper->getHelper('FlashMessenger')->addMessage('F');
			   $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
			   $this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName());
			}
			Application_Helper_General::writeLog('GRGG','Viewing Edit Group');
		}
		else
		{
			$filters = array( 	'GROUP_ID' => array('StringTrim','StripTags'),
								'GROUPNAME' => array('StringTrim','StripTags','StringToUpper'));

			$validators = array('GROUP_ID' => array(	'NotEmpty',
														//'Digits',
														'messages' => array(
															$this->language->_('Error: Group ID does not exist.'),
															//$this->getErrorRemark('04','Group ID')
															)
													),
								'GROUPNAME' => array(	'NotEmpty',
															//new Zend_Validate_StringLength(array('max'=>50)),
										'Alnum',
															'messages' => array(
																$this->language->_('Error: Group Name cannot be left blank. Please correct it.'),
																$this->language->_('Error: Wrong Group Name format. Please correct it.'),
																//$this->getErrorRemark('04','Group Name')
																)
														));

			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			//Zend_Debug::Dump($this->_request->getParams());die;
			if($zf_filter_input->isValid())
			{
				$content = array(
							'CUST_ID_LOGIN' 	=> $this->_custIdLogin,
							'USER_ID_LOGIN' 	=> $this->_userIdLogin,
							'GROUP_ID' 			=> $zf_filter_input->GROUP_ID,
							'GROUP_NAME' 		=> $zf_filter_input->GROUPNAME
								);

				try
				{
					//-----insert group--------------
					$this->_db->beginTransaction();
					$Grouping = new Grouping();
					$Grouping->update($content);
					$this->_db->commit();
					$this->fillParam($zf_filter_input);
					Application_Helper_General::writeLog('GRGG','Editing Group');

					$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
					$this->_redirect('/notification/success');
				}
				catch(Exception $e)
				{
					//rollback changes
					$this->_db->rollBack();
					$this->fillParam($zf_filter_input);
					$errorMsg ='RollBack'.$e;
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
					Application_Helper_General::writeLog('GRGG','RollBack Edit Group');
				}
			}
			else
			{
				$this->view->error = true;
				$this->fillParam($zf_filter_input);
				$docErr = $this->displayError($zf_filter_input->getMessages());
				$this->view->report_msg = $docErr;
				//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $docErr);
			}
		}
	}

	public function deleteAction()
	{
		if(!$this->_request->isPost())
		{
			$group_id = $this->_getParam('group_id');
			$group_id = (Zend_Validate::is($group_id,'Digits'))? $group_id : null;
			if($group_id)
			{
			  $resultdata = $this->_db->fetchRow(
								$this->_db->select()
									 ->from(array('M_GROUPING'))
									 ->where("GROUP_ID=?", $group_id)
							);
			  if($resultdata)
			  {
					$this->view->GROUP_ID = $resultdata['GROUP_ID'];
					$this->view->GROUP_NAME = $resultdata['GROUP_NAME'];
					$this->view->STATUS = $resultdata['GROUP_ISAPPROVED'];
			   }
			}
			else
			{
			   $error_remark = 'Error: Group ID does not exist.';

			   $this->_helper->getHelper('FlashMessenger')->addMessage('F');
			   $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
			}
		}
		else
		{
			$filters = array(  	'GROUP_ID' => array('StringTrim','StripTags'),
								'STATUS' => array('StringTrim','StripTags'));

			$validators = array('GROUP_ID' => array(	'NotEmpty',
															'Digits',
															'messages' => array(
																'Error: Group ID does not exist.',
																'Error: Wrong Format Group ID.')
														),
								'STATUS' 		=> array(	'NotEmpty',
															'Digits',
															'messages' => array(
																'Error: Status cannot be left blank.',
																'Error: Wrong Format Status.')
														));

			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			if($zf_filter_input->isValid())
			{
				try
				{
					//-----delete--------------
					$this->_db->beginTransaction();
					$Grouping = new Grouping();
					if($zf_filter_input->STATUS)
						$Grouping->suggestDelete($zf_filter_input->GROUP_ID);
					else
						$Grouping->delete($zf_filter_input->GROUP_ID);
					$this->_db->commit();

					Application_Helper_General::writeLog('GRGG','Deleting Group');

					$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
					$this->_redirect('/notification/success');
				}
				catch(Exception $e)
				{
					//rollback changes
					$this->_db->rollBack();
					$errorMsg = 'Error: Database Process Failed.';
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);

					Application_Helper_General::writeLog('GRGG','Roll Back Deleting Group');
				}
			}
			else
			{
				$this->view->error = true;
				$resultdata = $this->_db->fetchRow(
								$this->_db->select()
									 ->from(array('M_GROUPING'))
									 ->where("GROUP_ID=?", $zf_filter_input->GROUP_ID)
							);
				if($resultdata)
				{
					$this->view->GROUP_ID = $resultdata['GROUP_ID'];
					$this->view->GROUP_NAME = $resultdata['GROUP_NAME'];
					$this->view->STATUS = $resultdata['GROUP_ISAPPROVED'];
				}
				$docErr = $this->displayError($zf_filter_input->getMessages());
				$this->view->report_msg = $docErr;
				Application_Helper_General::writeLog('GRGG','Invalid Data Deleting Group');
			}
		}
	}

	private function fillParam($zf_filter_input)
	{
		if(isset($zf_filter_input->GROUP_ID))$this->view->GROUP_ID = ($zf_filter_input->isValid('GROUP_ID')) ? $zf_filter_input->GROUP_ID : $this->_getParam('GROUP_ID');
		$this->view->GROUPNAME = ($zf_filter_input->isValid('GROUPNAME')) ? $zf_filter_input->GROUPNAME : $this->_getParam('GROUPNAME');
	}

	public function suggestdeleteAction()
	{
		$filters = array( 'GROUP_ID' => array('StringTrim','StripTags'));
		$validators = array('GROUP_ID' => array(	'NotEmpty',
														'Digits',
														'messages' => array(
															$this->getErrorRemark('01','Group ID'),
															$this->getErrorRemark('04','Group ID'))
													));

		$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
		if($zf_filter_input->isValid())
		{
			$Grouping = new Grouping();
			$Grouping->suggestDelete($zf_filter_input->GROUP_ID);
		}
		Application_Helper_General::writeLog('GRGG','Suggesting Delete Group');

		$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
		$this->_redirect('/notification/success');
	}
}
