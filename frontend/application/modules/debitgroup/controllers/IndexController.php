<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/SourceAccount.php';
class debitgroup_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti

	public function indexAction()
	{

		$this->_helper->layout()->setLayout('newlayout');

		$fields = array(
						'vanumber'  => array('field' => 'VA_NUMBER',
											   'label' => $this->language->_('VA Number'),
											   'sortable' => true),
						'group'  => array('field' => 'GROUP_NAME',
											   'label' => $this->language->_('Card Group'),
											   'sortable' => true),
						'debitnumber'   => array('field'    => 'DEBIT_NUMBER',
											  'label'    => $this->language->_('Debit Card Number'),
											  'sortable' => true),
						'holdername'  => array('field' => 'DEBIT_NAME',
											   'label' => $this->language->_('Card Holder Name'),
											   'sortable' => true),
						'holderemail'  => array('field' => 'DEBIT_EMAIL',
											   'label' => $this->language->_('Card Holder Email'),
											   'sortable' => true)
				);

				
		$filterlist = array('VA_NUMBER','GROUP_NAME','DEBIT_NUMBER');

		$this->view->filterlist = $filterlist;
		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby');
		$sortDir = $this->_getParam('sortdir');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;

        $sortBy = (Zend_Validate::is($sortBy,'InArray',
														array(array_keys($fields))
														))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

        $sortDir = (Zend_Validate::is($sortDir,'InArray',
														array('haystack'=>array('asc','desc'))
														))? $sortDir : 'asc';


		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	'filter' 	=> array('StringTrim','StripTags'),
							'VA_NUMBER'    => array('StringTrim','StripTags'),
							'GROUP_NAME'    	=> array('StringTrim','StripTags','StringToUpper'),
							//'alias' 	=> array('StringTrim','StripTags','StringToUpper'),
							'DEBIT_NUMBER'     => array('StringTrim','StripTags')
		);

		$dataParam = array('VA_NUMBER','GROUP_NAME','DEBIT_NUMBER');
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{

			// print_r($dtParam);die;
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}

			}

			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}


		$selecttemp = $this->_db->select()->from(array('A' => 'TEMP_CUSTOMER_ACCT_GROUP'),array('A.ACCT_NO'))
					   ->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin));

		$datatemp = $this->_db->fetchAll($selecttemp);
		if(!empty($datatemp)){
			$this->view->disabledmanage = true;
			$this->view->disabled_msg = 'Group Account waiting for approval';
		}

		$zf_filter = new Zend_Filter_Input($filterArr,array(),$dataParamValue);
		// $filter = $zf_filter->getEscaped('filter');
		$filter 		= $this->_getParam('filter');
		$pdf = $this->_getParam('pdf');

		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;

		

		$select = $this->_db->select()
					   ->from(array('A' => 'T_DEBITCARD'))
					   ->joinleft(array('I' => 'T_CUST_DEBIT'),'A.REG_NUMBER=I.REG_NUMBER',array('VA_NUMBER'))
					   ->joinleft(array('C' => 'M_CUSTOMER'),'C.CUST_ID=A.CUST_ID',array('CUST_NAME','CUST_EMAIL'))
					   ->joinLeft(array('G' => 'T_DEBIT_GROUP'),'A.DEBIT_NUMBER = G.DEBIT_NUMBER',array())
					   ->joinLeft(array('D' => 'M_DEBITGROUP'),'D.GROUP_ID = G.GROUP_ID',array('GROUP_NAME'))
					   ->joinleft(array('F' => 'M_USER_DEBIT'),'F.USER_DEBITNUMBER=A.DEBIT_NUMBER',array('USER_ID'))
					   ->joinleft(array('H' => 'M_USER'),'F.USER_ID=H.USER_ID',array('USER_FULLNAME','USER_EMAIL'));
		$select->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin));

	    $select->order($sortBy.' '.$sortDir);



		if($filter == TRUE || $pdf || $this->_request->getParam('print'))
		{
			$fNumber = $zf_filter->getEscaped('VA_NUMBER');
			$fName = $zf_filter->getEscaped('GROUP_NAME');
			//$fAlias = $zf_filter->getEscaped('alias');
			$fGroup = $zf_filter->getEscaped('ACCT_GROUP');

	        if($fNumber)$select->where('VA_NUMBER LIKE '.$this->_db->quote('%'.strtoupper($fNumber).'%'));
	        if($fName)$select->where('UPPER(GROUP_NAME) LIKE '.$this->_db->quote('%'.strtoupper($fName).'%'));
	        //if($fAlias)$select->where('UPPER(ACCT_ALIAS_NAME) LIKE '.$this->_db->quote('%'.strtoupper($fAlias).'%'));
	        if($fGroup)$select->where('UPPER(DEBIT_NUMBER) LIKE '.$this->_db->quote('%'.strtoupper($fGroup).'%'));

			$this->view->number = $fNumber;
			$this->view->name = $fName;
			$this->view->alias = $fAlias;
			$this->view->group = $fGroup;

		}
		else $this->view->favorit = false;

	    if($pdf)
		{
			$sortBy  = $this->_getParam('sortby');
			$sortDir = $this->_getParam('sortdir');
			$select->order($sortBy.' '.$sortDir);

			$data = $this->_db->fetchAll($select);

			foreach($data as $key=>$row)
			{

				if(!$data[$key]['ACCT_ALIAS_NAME']) $data[$key]['ACCT_ALIAS_NAME'] = 'N/A';
				if(!$data[$key]['GROUP_NAME']) $data[$key]['GROUP_NAME'] = 'N/A';
			}
			Application_Helper_General::writeLog('GRAM','Downloading PDF Account Group List');
			$this->_helper->download->pdf(array($this->language->_('Account Number'),$this->language->_('Account Name'),$this->language->_('CCY'),$this->language->_('Alias Name'),$this->language->_('Account Group')),$data,null,$this->language->_('Account Group'));
		}

		// if($this->_request->getParam('print') == 1){
		// 	$data = $this->_db->fetchAll($select);

  //           $this->_forward('print', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'Account Group', 'data_header' => $fields));
  //       }

        if($this->_request->getParam('print') == 1){

        	$data = $this->_db->fetchAll($select);
        	
			$this->_forward('print', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'Account Group', 'data_header' => $fields));
		}

		$select->order($sortBy.' '.$sortDir);
		
		$data = $this->_db->fetchAll($select);
		//$this->paging($select);
		
		if(!empty($data)){
			foreach($data as $key => $row){
						if($val['GROUP_ID'] == $row['GROUP_ID']){
							if(!empty($row['USER_FULLNAME'])){
								$data[$key]['DEBIT_NAME'] = $row['USER_FULLNAME'];
							}else{
								$data[$key]['DEBIT_NAME'] = $row['CUST_NAME'];
							}	
							
							if(!empty($row['USER_EMAIL'])){
								$data[$key]['DEBIT_EMAIL'] = $row['USER_EMAIL'];
							}else{
								$data[$key]['DEBIT_EMAIL'] = $row['CUST_EMAIL'];
							}
						//$data[$key][$val['GROUP_ID']][] = $row;
						}
					}
		}
		
		//echo '<pre>';
		//var_dump($data);die;
		
		
		
		$this->view->dataact = $data;
		
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		$this->view->modulename = $this->_request->getModuleName();
		
		 if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;

      }
		//$this->_helper->download->csv(array('Group ID','Group Name','Status'),null,$select,'download group');
		Application_Helper_General::writeLog('GRAM','Viewing Account Group');
	}

	public function manageacctgroupAction()
	{

		$this->_helper->layout()->setLayout('newlayout');

		$select = $this->_db->select()
					        ->from(array('A' => 'T_CUST_DEBIT'))
							->join(array('D' => 'M_CUSTOMER'), 'D.CUST_ID = A.CUST_ID',array('D.CUST_NAME'))
							->where('A.CUST_ID = ?',$this->_custIdLogin);
							//->where('A.REG_NUMBER = ?',$id);
				//echo $select;			
		$data = $this->_db->fetchAll($select);
		$this->view->dataselect = $data;


		$fields = array(
						'number'  => array('field' => 'DEBIT_NUMBER',
											   'label' => $this->language->_('Account Number'),
											   'sortable' => false),
						'name'  => array('field' => 'ACCT_NAME',
											   'label' => $this->language->_('Account Name'),
											   'sortable' => false),
						'ccy'   => array('field'    => 'CCY_ID',
											  'label'    => $this->language->_('CCY'),
											  'sortable' => false),
						/*'alias'  => array('field' => 'ACCT_ALIAS_NAME',
											   'label' => $this->language->_('Alias Name'),
											   'sortable' => false),*/
						'group'  => array('field' => 'GROUP_NAME',
											   'label' => $this->language->_('Account Group'),
											   'sortable' => false)
				);

		//get sortby, sortdir
		$sortBy  = $this->_getParam('sortby','doc_no');
		$sortDir = $this->_getParam('sortdir','asc');

		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';

		$groupArr = $this->_db->select()
								->from(array('M_DEBITGROUP'), array('GROUP_ID', 'GROUP_NAME'))
								->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin))
								->where("GROUP_ISAPPROVED = 1")
								->order('GROUP_ID ASC')
								->query()->fetchAll();
		$listGroup = array(''=>'');
		$listGroup += Application_Helper_Array::listArray($groupArr,'GROUP_ID','GROUP_NAME');
		$this->view->group = $listGroup;

		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;

		$select = $this->_db->select()
					   ->from(array('A' => 'T_DEBITCARD'))
					   ->joinleft(array('I' => 'T_CUST_DEBIT'),'A.REG_NUMBER=I.REG_NUMBER',array('VA_NUMBER'))
					   ->joinleft(array('C' => 'M_CUSTOMER'),'C.CUST_ID=A.CUST_ID',array('CUST_NAME','CUST_EMAIL'))
					   ->joinLeft(array('G' => 'T_DEBIT_GROUP'),'A.DEBIT_NUMBER = G.DEBIT_NUMBER',array())
					   ->joinLeft(array('D' => 'M_DEBITGROUP'),'D.GROUP_ID = G.GROUP_ID',array('GROUP_NAME'))
					   ->joinleft(array('F' => 'M_USER_DEBIT'),'F.USER_DEBITNUMBER=A.DEBIT_NUMBER',array('USER_ID'))
					   ->joinleft(array('H' => 'M_USER'),'F.USER_ID=H.USER_ID',array('USER_FULLNAME','USER_EMAIL'));
		$select->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin));

	    
		

	    $selectgroup = $this->_db->select()
					   ->from(array('A' => 'T_DEBIT_GROUP'),array('acct' => 'GROUP_CONCAT(A.DEBIT_NUMBER SEPARATOR ",")','G.GROUP_NAME'))
					   ->joinLeft(array('G' => 'M_DEBITGROUP'),'A.GROUP_ID = G.GROUP_ID',array('G.GROUP_ID'));

		//6 may 2019 : tambah and G.cust_id = _custidlogin
		if(!empty($this->getRequest()->getParam('va'))){
		$selectgroup->where("VA_NUMBER = ? ",$this->getRequest()->getParam('va'));
		}
		$selectgroup->where("A.CUST_ID = ".$this->_db->quote($this->_custIdLogin));
		$selectgroup->where("G.GROUP_ID IS NOT NULL");

		// $selectgroup->where("");
		$selectgroup->group("G.GROUP_ID");



	    // echo $selectgroup;

	    /*print_r($select->query()->fetchAll());die();*/
		$this->view->data = $selectgroup->query()->fetchAll();
		//echo $select;
		//var_dump($select->query()->fetchAll());die;
		
		if(!empty($this->getRequest()->getParam('va'))){
			$select->where("I.VA_NUMBER = ?",$this->getRequest()->getParam('va'));
			$this->view->va = $this->getRequest()->getParam('va');
			//echo $select;die;
			$select->group("A.DEBIT_NUMBER");
			$select->order($sortBy.' '.$sortDir);
			$data = $select->query()->fetchAll();
			
			$selecttemp = $this->_db->select()
					   ->from(array('A' => 'T_DEBIT_GROUP_CHANGES'),array())
					   ->joinLeft(array('G' => 'TEMP_DEBIT_GROUP'),'G.SUGEST_ID = A.SUGGEST_ID',array('G.GROUP_ID'))
					   ->joinLeft(array('B' => 'M_DEBITGROUP'),'B.GROUP_ID = G.GROUP_ID',array())
					   ->where('B.VA_NUMBER = ?',$this->getRequest()->getParam('va'));
			$datatemp = $selecttemp->query()->fetchAll();
			if(!empty($datatemp)){
				$this->view->errorchange = true;
				$this->view->error = true;
				$this->view->report_msg = 'No changes allowed for this record while awaiting approval for previous change.';
			}
					   
		}else{
			$data = array();
		}
		
		if(!empty($data)){
			foreach($data as $key => $row){
						if($val['GROUP_ID'] == $row['GROUP_ID']){
							if(!empty($row['USER_FULLNAME'])){
								$data[$key]['DEBIT_NAME'] = $row['USER_FULLNAME'];
							}else{
								$data[$key]['DEBIT_NAME'] = $row['CUST_NAME'];
							}	
							
							if(!empty($row['USER_EMAIL'])){
								$data[$key]['DEBIT_EMAIL'] = $row['USER_EMAIL'];
							}else{
								$data[$key]['DEBIT_EMAIL'] = $row['CUST_EMAIL'];
							}
						//$data[$key][$val['GROUP_ID']][] = $row;
						}
					}
		}
		
		//echo '<pre>';
		//var_dump($data);die;
		
		
		
		$this->view->dataact = $data;
		$this->view->fields = $fields;

		if($this->_request->isPost() && !empty($this->getRequest()->getParam('submit')) )
		{
			$param = $this->getRequest()->getParams();
			//$alias = $this->getRequest()->getParam('alias');
			$group = $this->getRequest()->getParam('groupname');
			$color = $this->getRequest()->getParam('color');
	
			//echo "<pre>";
			//var_dump($param);die;
			//var_dump($group);
			//var_dump($color);die;

			$this->_setParam('validate_issuggest',$this->_custIdLogin);
			$filters    = array('validate_issuggest' => array('StripTags','StringTrim','StringToUpper'));
			$validators = array('validate_issuggest' => array());
												//);
			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			if($zf_filter_input->isValid())
			{
				// echo "<pre>";
				// print_r($this->_request->getParams());die;
				// die;
				try
				{

					$this->_db->beginTransaction();

					$suggestArr = array(
										'CUST_ID'			=> $this->_custIdLogin,
										'SUGGEST_USER' 		=> $this->_userIdLogin,
										'SUGGEST_DATE' 		=> new Zend_Db_Expr("now()"),
										'SUGGEST_STATUS'	=> 0
									);
					$this->_db->insert('T_DEBIT_GROUP_CHANGES',$suggestArr);

					$lastSuggestId = $this->_db->fetchOne('select @@identity');
					//var_dump($lastSuggestId);die;
					// print_r($group);
					// print_r($alias);

					foreach ($group as $key => $value) {
						if(!empty($value)){
						$insertArr = array(
											'CUST_ID'			=> $this->_custIdLogin,
											'GROUP_NAME' 			=> $value,
											'GROUP_ISAPPROVED' 		=> '1',
											'GROUP_ISREQUESTDELETE'			=> '0',
											'CREATEDBY'			=> $this->_userIdLogin,
											'CREATED' 	=> new Zend_Db_Expr('now()'),
											'VA_NUMBER' 		=> $param['vanumb'],
										);
						$this->_db->insert('M_DEBITGROUP',$insertArr);
						}
					}
					// echo "<pre>";
					// print_r($color);die;
					foreach ($color as $key => $value) {
						$listacc = explode(',', $value);
						if(!empty($group[$key])){
						$groupid = $this->_db->fetchRow(
								$this->_db->select()
									->from(array('M_DEBITGROUP'), array('GROUP_ID'))
									->where("GROUP_NAME = ".$this->_db->quote((string)$group[$key]))
									->where("CUST_ID = ?",$this->_custIdLogin)
								);
						//var_dump($listacc);	
						foreach ($listacc as $no => $val) {
								
								$valMod = explode("-", $val);
								 //var_dump($valMod);


								$number = trim($valMod[0]);
								$insertArr = array(
											'DEBIT_NUMBER' 			=> $number,
											'SUGEST_ID' 		=> $lastSuggestId,
											'CUST_ID'			=> $this->_custIdLogin,
											//'DEBIT_NUMBER' 			=> $val,
											'SUGESTEDBY' 		=> $this->_userIdLogin,
											'SUGESTED'			=> new Zend_Db_Expr('now()'),
											'GROUP_ID'			=> $groupid['GROUP_ID']
										);
								//		echo '<pre>';
								//var_dump($insertArr);
								$this->_db->insert('TEMP_DEBIT_GROUP',$insertArr);


								}
							}
					}
				
				
					//die;


					$this->_db->commit();

					Application_Helper_General::writeLog('GRAM','Managing Account Group');

					$this->setbackURL('/debitgroup/');
					$this->_redirect('/notification/submited');
				}
				catch(Exception $e)
				{
					print_r($e);die;
					//rollback changes
					$this->_db->rollBack();

					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);

					Application_Helper_General::writeLog('GRAM','Roll Back Managing Account Group');
				}
			}
			else
			{
				$this->view->error = true;
				$docErr = $this->displayError($zf_filter_input->getMessages());
				// print_r($zf_filter_input->getMessages())die;
				$this->view->report_msg = $docErr;
				Application_Helper_General::writeLog('GRAM','Invalid Data Managing Account Group');
			}

			$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
			if(count($temp)>1){
				if($temp[0]=='F' || $temp[0]=='S'){
					if($temp[0]=='F')
						$this->view->error = 1;
					else
						$this->view->success = 1;
					$msg = ''; unset($temp[0]);
					foreach($temp as $value)
					{
						if(!is_array($value))
							$value = array($value);
						$msg .= $this->view->formErrors($value);
					}
					$this->view->report_msg = $msg;
				}
			}
		}
	    Application_Helper_General::writeLog('GRAM','Viewing Manage Account Group');
	}
}
