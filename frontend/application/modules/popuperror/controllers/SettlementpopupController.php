<?php

/**
 * IndexController
 * 
 * @author Konrad B. Pratomo
 * @version 
 */

require_once 'Zend/Controller/Action.php';

class Popuperror_SettlementpopupController extends Application_Main {
	/**
	 * The default action - show the home page
	 */
	public function indexAction() {
		$this->_helper->layout()->setLayout('popup');
		
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
		
		$key = $this->_request->getParam('id');
		$ccy = $this->_request->getParam('ccy');
		
		$sessionNamespace = new Zend_Session_Namespace('confirmSettlement');
		$data = $sessionNamespace->content;
		
		$errorMsg[0] = 'Error: '.$data['errorTrxMsg']['PB'][$ccy][$key];
		
		/*Zend_Debug::dump($errorArr);die;
		$errorMsg = array();
		
		if(isset($errorArr[$key]['TRX'][0]['ACBENEF']))
			$errorMsg = array_merge($errorMsg,array('Error: '.$errorArr[$key]['TRX'][0]['ACBENEF']));
		if(isset($errorArr[$key]['TRX'][0]['ACBENEF_ALIAS']))
			$errorMsg = array_merge($errorMsg,array('Error: '.$errorArr[$key]['TRX'][0]['ACBENEF_ALIAS']));
		if(isset($errorArr[$key]['TRX'][0]['AMOUNT']))
			$errorMsg = array_merge($errorMsg,array('Error: '.$errorArr[$key]['TRX'][0]['AMOUNT']));
		if(isset($errorArr[$key]['PS_EFDATE']))
			$errorMsg = array_merge($errorMsg,array('Error: '.$errorArr[$key]['PS_EFDATE']));
		
		// $errorMsg = $errorArr['PB'][];
		*/
		$this->view->error 		= true;
		$this->view->report_msg	= $this->displayError($errorMsg);
	}

}

