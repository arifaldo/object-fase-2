<?php

require_once 'Zend/Controller/Action.php';

class Popuperror_DocumentimportController extends Application_Main {

	public function indexAction() {
		$this->_helper->layout()->setLayout('popup');
		
		$err_msg = null;
	
		$sessionNamespace = new Zend_Session_Namespace('importDocFile');		
		$data			 = $sessionNamespace->rawData['ERROR_MESSAGE'];
		$errorMode   = $data['ERRORMODE'];
		unset($data['ERRORMODE']);

		if(is_array($data))
		{
			if($errorMode == 1)
			{
				foreach($data as $counter => $row)
				{
					if(is_array($row))
					{
						foreach($row as $err)
						{
							if(is_array($err))
							{
								foreach($err as $msg)
									$errMsg[($counter+1)][] = $this->language->_('Error Line').' '.($counter+1).' : '.$msg;
							}
						}
					}
				}
			}
			elseif($errorMode == 2)
			{
				foreach($data as $grup)
				{
					if(is_array($grup['rowNum']) && is_array($grup['errorMessage']))
					{
						foreach($grup['rowNum'] as $row)
						{
							foreach($grup['errorMessage'] as $msg)
							{
								$errMsg[$row][] = $this->language->_('Error Line').' '.($row).' : '.$msg;
							}
						}
					}
				}
			}

	// SORT ERROR MESSAGE BERDASARKAN ERROR LINE
			ksort($errMsg);
			foreach($errMsg as $listMsg)
			{
				foreach($listMsg as $msg)
				$err_msg[] = $msg;
			}
		}

		$this->view->report_msg	= $err_msg;
	}

}

