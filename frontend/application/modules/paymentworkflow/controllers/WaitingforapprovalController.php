<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';

class paymentworkflow_WaitingforapprovalController extends Application_Main
{
	protected $_moduleDB = 'RTF';

	public function indexAction()
	{

		$this->_helper->layout()->setLayout('newlayout');

		$setting = new Settings();			  	
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
		$pw_hash = md5($enc_salt.$enc_pass);
		$rand = $this->_userIdLogin.date('dHis').$pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;
		
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if (count($temp) > 1) {
			if ($temp[0] == 'F' || $temp[0] == 'S') {
				if ($temp[0] == 'F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = '';
				unset($temp[0]);
				foreach ($temp as $value) {
					if (!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}
		}

		$payType = Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
		$trfType = array_combine($this->_transfertype["code"], $this->_transfertype["desc"]);

		foreach ($payType as $key => $value) {
			$filterPayType[$key] = $this->language->_($value);
		}
		foreach ($trfType as $key => $value) {
			if ($key != 3 && $key != 4) $filterTrfType[$key] = $this->language->_($value);
		}


		$fields = array(
			'payReff'  		=> array(
				'field' 	=> 'payReff',
				'label' 	=> $this->language->_('Payment Ref') . '#',
				'sortable' => true
			),
			'paySubj'  		=> array(
				'field' 	=> 'paySubj',
				'label' 	=> $this->language->_('Subject'),
				'sortable' => true
			),
			'created'  		=> array(
				'field' 	=> 'created',
				'label' 	=> $this->language->_('Created Date'),
				'sortable' => true
			),
			'efdate'  		=> array(
				'field' 	=> 'efdate',
				'label' 	=> $this->language->_('Payment Date'),
				'sortable' => true
			),
			'updated'  		=> array(
				'field' 	=> 'updated',
				'label' 	=> $this->language->_('Last Updated'),
				'sortable' => true
			),
			'accsrc'  		=> array(
				'field' 	=> 'accsrc',
				'label' 	=> $this->language->_('Source Account'),
				'sortable' => true
			),
			/*'accsrc_name'  	=> array('field' 	=> 'accsrc_name',
												 'label' 	=> $this->language->_('Source Account Name'),
												 'sortable' => true),*/
			'acbenef'  		=> array(
				'field' 	=> 'acbenef',
				'label' 	=> $this->language->_('Beneficiary Account'),
				'sortable' => true
			),
			/*'acbenef_name'  => array('field' 	=> 'acbenef_name',
												 'label'	=> $this->language->_('Beneficiary Name'),
												 'sortable' => true),*/
			'numtrx'  		=> array(
				'field' 	=> 'numtrx',
				'label' 	=> '#' . $this->language->_('Trans'),
				'sortable' => true
			),
			'amount'  			=> array(
				'field' 	=> 'amount',
				'label' 	=> $this->language->_('Amount'),
				'sortable' => true
			),
			/*'amount'  		=> array('field' 	=> 'amount',
												 'label' 	=> $this->language->_('Amount'),
												 'sortable' => true),*/
			'payType'  		=> array(
				'field'	=> 'payType',
				'label' 	=> $this->language->_('Payment Type'),
				'sortable' => true
			),
		);

		$filterlist = array('PS_SUBJECT','SOURCE_ACCOUNT','BENEFICIARY_ACCOUNT', 'PS_EFDATE', 'PS_NUMBER', 'PS_TYPE', 'TRANSFER_TYPE');

		$this->view->filterlist = $filterlist;


		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby', 'created');
		$sortDir = $this->_getParam('sortdir', 'desc');
		//		echo "<pre>";
		//		print_r($this->_request->getParams());
		//		echo $sortBy."<br>";
		//		echo $sortDir."<br>";

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page, 'Digits') && ($page > 0)) ? $page : 1;

		$sortBy = (Zend_Validate::is(
			$sortBy,
			'InArray',
			array(array_keys($fields))
		)) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is(
			$sortDir,
			'InArray',
			array('haystack' => array('asc', 'desc'))
		)) ? $sortDir : 'asc';

		$filterArr = array(
			'PS_NUMBER'  		=> array('StringTrim', 'StripTags', 'StringToUpper'),
			'PS_TYPE'  		=> array('StringTrim', 'StripTags', 'StringToUpper'),
			'TRANSFER_TYPE'  		=> array('StringTrim', 'StripTags'),
			'PS_SUBJECT'   => array('StringTrim', 'StripTags'),
			'SOURCE_ACCOUNT'     => array('StringTrim', 'StripTags'),
			'BENEFICIARY_ACCOUNT'     => array('StringTrim', 'StripTags'),
			'PS_EFDATE'   => array('StringTrim', 'StripTags'),
			'PS_EFDATE_END'     => array('StringTrim', 'StripTags'),
		);

		// if POST value not null, get post, else get param
		$dataParam = array("PS_NUMBER", "PS_TYPE", "TRANSFER_TYPE", "PS_SUBJECT", "SOURCE_ACCOUNT","BENEFICIARY_ACCOUNT", "PS_EFDATE", "PS_EFDATE_END");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam) {
			$dataPost = $this->_request->getPost($dtParam);
			$dataParamValue[$dtParam] = ($dataPost != null) ? $dataPost : $this->_getParam($dtParam);
		}


		$dataParam = array("PS_NUMBER", "PS_TYPE", "TRANSFER_TYPE","PS_SUBJECT","SOURCE_ACCOUNT","BENEFICIARY_ACCOUNT");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam) {

			// print_r($dtParam);die;
			if (!empty($this->_request->getParam('wherecol'))) {
				$dataval = $this->_request->getParam('whereval');
				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if ($dtParam == $value) {
						if(!empty($dataParamValue[$dtParam])){
								$dataval[$key] = $dataParamValue[$dtParam] .','.$dataval[$key]; 
							}
						$dataParamValue[$dtParam] = $dataval[$key];
					}
				}
			}

			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}
		// if (!empty($this->_request->getParam('createdate'))) {
		// 	$createarr = $this->_request->getParam('createdate');
		// 	$dataParamValue['PS_CREATED'] = $createarr[0];
		// 	$dataParamValue['PS_CREATED_END'] = $createarr[1];
		// }


		if (!empty($this->_request->getParam('efdate'))) {
			$efdatearr = $this->_request->getParam('efdate');
			$dataParamValue['PS_EFDATE'] = $efdatearr[0];
			$dataParamValue['PS_EFDATE_END'] = $efdatearr[1];
		}

		// The default is set so all fields allow an empty string
		$options = array('allowEmpty' => true);
		$validators = array(
			'PS_NUMBER'		=> array(),
			'PS_TYPE' 		=> array(),
			'TRANSFER_TYPE' 		=> array(array('InArray', array('haystack' => array_keys($filterTrfType)))),
			'PS_SUBJECT'   	=> array(),
			'SOURCE_ACCOUNT'     	=> array(),
			'BENEFICIARY_ACCOUNT'     	=> array(),
			'PS_EFDATE'   	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
			'PS_EFDATE_END'     	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),
		);

		$zf_filter  = new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);
		$filter 	= $this->_getParam('filter');
		$csv 		= $this->_getParam('csv');
		$pdf 		= $this->_getParam('pdf');
		$fromMenu	= $this->_getParam('m');

		$fPaymentReff = $zf_filter->getEscaped('PS_NUMBER');
		$fPaymentType = $zf_filter->getEscaped('PS_TYPE');
		$fTrfType 	  = $zf_filter->getEscaped('TRANSFER_TYPE');
		$fSubject = $zf_filter->getEscaped('PS_SUBJECT');
		$fSource   = $zf_filter->getEscaped('SOURCE_ACCOUNT');
		$fBenef   = $zf_filter->getEscaped('BENEFICIARY_ACCOUNT');
		$fPayDateFrom = $zf_filter->getEscaped('PS_EFDATE');
		$fPayDateTo   = $zf_filter->getEscaped('PS_EFDATE_END');
		$fPaymentStart 	= $zf_filter->getEscaped('PS_EFDATE');
		$fPaymentEnd 	= $zf_filter->getEscaped('PS_EFDATE_END');
		//var_dump($fPaymentStart);
		//var_dump($fPaymentEnd);

		$paramPayment = array(
			"WA" 				=> true,
			"ACCOUNT_LIST" 	=> $this->_accountList,
			"_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
		);

		// get payment query
		$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);


		$selectuser	= $this->_db->select()->distinct()
			->from(
				array('A'	 		=> 'M_APP_GROUP_USER'),
				array(
					'GROUP_USER_ID' 	=> 'A.GROUP_USER_ID',
					'USER_ID' => 'A.USER_ID',
					
					'CUST_ID' => 'A.CUST_ID'
				)
			)
			->join(array('B' => 'M_APP_BOUNDARY_GROUP'), 'A.GROUP_USER_ID = B.GROUP_USER_ID', array())
			->join(array('C' => 'M_APP_BOUNDARY'), 'C.BOUNDARY_ID = B.BOUNDARY_ID', array(
				'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
				'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
				'TRANSFER_TYPE' => 'C.TRANSFER_TYPE',
				'CCY_BOUNDARY' => 'C.CCY_BOUNDARY'
			))
			->where("A.USER_ID 	= ?", (string) $this->_userIdLogin)
			->where("A.CUST_ID 	= ?", (string) $this->_custIdLogin);;
		// echo $selectuser;
		$datauser = $this->_db->fetchAll($selectuser);
		$isSpecial = true;
		if(!empty($datauser)){
		$group = explode('_',$datauser['0']['GROUP_USER_ID']);
		if($group['0'] == 'S'){
			//var_dump($group['0']);
			$isSpecial = false;
		}
		}else{
			$isSpecial = false;
		}

		 		//echo "<pre>";
		 		//print_r($datauser);
/*
		$rangesql = '';
		foreach ($datauser as $key => $value) {
			$rangesql .= " (P.PS_TOTAL_AMOUNT BETWEEN '" . $value['BOUNDARY_MIN'] . "' AND '" . $value['BOUNDARY_MAX'] . "') ";
			if (!empty($datauser[$key + 1])) {
				$rangesql .= " OR ";
			}
		}
	*/	
	
		//$rangesql .= " OR ";
	//	var_dump($isSpecial);
		$rangesql = '';
		if($isSpecial){
		foreach ($datauser as $key => $value) {
			$rangesql .= " (P.PS_TYPE = '".$value['TRANSFER_TYPE']."' AND P.PS_REMAIN BETWEEN '" . $value['BOUNDARY_MIN'] . "' AND '" . $value['BOUNDARY_MAX'] . "') ";
			if (!empty($datauser[$key + 1])) {
				$rangesql .= " OR ";
			}
		}
		if(!empty($datauser)){
		$rangesql .= " OR ";
		}
		foreach ($datauser as $key => $value) {
			$rangesql .= " (P.PS_TYPE = '".$value['TRANSFER_TYPE']."' AND T.TRA_AMOUNT BETWEEN '" . $value['BOUNDARY_MIN'] . "' AND '" . $value['BOUNDARY_MAX'] . "') ";
			if (!empty($datauser[$key + 1])) {
				$rangesql .= " OR ";
			}
		}
		
			if(!empty($datauser)){
		$rangesql .= " OR ";
		$rangesql .= " (P.PS_TYPE = '19' OR P.PS_TYPE = '20' OR P.PS_TYPE = '23' )";
			}
		
		}
		
		// print_r($paramPayment);die;

		$select   = $CustUser->getPayment($paramPayment);

		$select->where('P.PS_STATUS = ?', (string) $this->_paymentstatus["code"]["waitingforapproval"]);

		if ($fromMenu == 1) {
			// $defaultFromDate = Application_Helper_General::addDate(date('Y-m-d'), 0, -1);	// 1 month
			// $fCreatedFrom 	 = ($fCreatedFrom)? $fCreatedFrom: Application_Helper_General::convertDate($defaultFromDate);
			// $fCreatedTo   	 = ($fCreatedTo  )? $fCreatedTo  : Application_Helper_General::convertDate(date('Y-m-d'));
		}

		// Filter Data
		if ($fSubject) {
			$fSubjectArr = explode(',', $fSubject);
			$select->where("UPPER(P.PS_SUBJECT)  in (?)",$fSubjectArr );
		}

		if ($fSource) {
			$fSourceArr = explode(',', $fSource);
			$select->where("T.SOURCE_ACCOUNT  in (?)",$fSourceArr );
		}

		if ($fBenef) {
			$fBenefArr = explode(',', $fBenef);
			$select->where("T.BENEFICIARY_ACCOUNT  in (?)",$fBenefArr );
		}

		if ($fPayDateFrom) {
			$FormatDate 	= new Zend_Date($fPayDateFrom, $this->_dateDisplayFormat);
			$payDateFrom   	= $FormatDate->toString($this->_dateDBFormat);
			$select->where('DATE(P.PS_EFDATE) >= ?', $payDateFrom);
		}

		if ($fPayDateTo) {
			$FormatDate 	= new Zend_Date($fPayDateTo, $this->_dateDisplayFormat);
			$payDateTo  	= $FormatDate->toString($this->_dateDBFormat);
			$select->where('DATE(P.PS_EFDATE) <= ?', $payDateTo);
		}

		if ($fPaymentReff) {
			//$select->where("UPPER(P.PS_NUMBER) LIKE " . $this->_db->quote('%' . $fPaymentReff . '%'));
			$fPaymentReffArr = explode(',', $fPaymentReff);
			$select->where("UPPER(P.PS_NUMBER)  in (?)",$fPaymentReffArr );
		}

		if ($fPaymentType) {
			//$fPayType 	 	= $fPaymentType;
			//$select->where("P.PS_TYPE in (?) ", $fPayType);
			$fPayType = explode(',', $fPaymentType);
			$select->where("P.PS_TYPE in (?) ", $fPayType);
		}
		// 		print_r($rangesql);
		if (!empty($rangesql)) {
			//if (!empty($datauser)) {
				$select->where($rangesql);
			//}
			$select->where("P.PS_NUMBER NOT IN ('P20160921NIG0UTYHC','P20160920PMQ6Q1CEC') ");
		}

		if ($fTrfType != "") {
			$select->where("(select count(PS_NUMBER) from T_TRANSACTION where PS_NUMBER = P.PS_NUMBER and TRANSFER_TYPE = ?) > 0", (string) $fTrfType);
		}
		//		{	$select->where("T.TRANSFER_TYPE = ? ", (string)$fTrfType);		}
		$select->where("P.PS_STATUS = 1");
		// $select->order($sortBy . ' ' . $sortDir);
		 //echo "<pre>";
		//echo $select;die;
		// 		Zend_Debug::dump($this->_request->getParams());
		// die;
		// 		
		$dirdeb = array(27,28);
		$select->where("P.PS_TYPE NOT IN (?) ", $dirdeb);
		$select->orWhere("P.PS_STATUS = '1' AND P.PS_TYPE IN (38) AND P.CUST_ID = ? ",$this->_custIdLogin);

		$select->group("T.PS_NUMBER");
	//	echo $select;
		$dataSQL = $this->_db->fetchAll($select);
	//	echo '<pre>';
	//	var_dump($dataSQL);
		
		//die;
		$newData = array();
		$no = 0;
		if(!empty($dataSQL)){
				foreach($dataSQL as $keyv => $val){
					
					$selectapprover	= $this->_db->select()
						->from(array('C' => 'T_APPROVAL'), array(
							'USER_ID'
						))
						->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
						->where("C.USER_ID 	= ?", (string) $this->_userIdLogin)
						->where("C.PS_NUMBER = ?", (string) $val['PS_NUMBER']);
					// ->where("C.GROUP = ?" , (string)$value);
					// echo $selectapprover;die;
					$usergroup = $this->_db->fetchAll($selectapprover);
					// print_r($usergroup);die;
					if (!empty($usergroup)) {
						// die;
						// $this->view->pdf = true;
						//$this->view->validbtn = false;
					} else {
					//	if (!empty($pslip)) {
							if ($val['PS_TYPE'] == '4' || $val['PS_TYPE'] == '5' || $val['PS_TYPE'] == '11') {
								$PS_TYPE = '18';
							} else {
								$PS_TYPE = $val['PS_TYPE'];
							}
						//	echo '<pre>';
						//	var_dump($val);die;
							$boundary = $this->validatebtn($PS_TYPE, $val['amount'], $val['ccy'], $val['PS_NUMBER']);
					//	}

						// print_r($boundary);die;
						if ($boundary) {
						
							$newData[$no] = $val;
							$no++;
							//$this->view->validbtn = false;
						} 
					}
					
				}
			
			
		}
		
		

		if ($csv || $pdf || $this->_request->getParam('print')) {
			$header  = Application_Helper_Array::simpleArray($fields, "label");
		} else {
			$this->paging($newData);
			$dataSQL = $this->view->paginator;
		}

		// arrange data for viewing in html, csv, pdf
		$data = array();

		$selectbank = $this->_db->select()
			->from(array('C' => 'M_BANK_TABLE'), array('*'));

		$databank = $this->_db->fetchAll($selectbank);

		foreach ($databank as $key => $value) {
			$newDataBank[$value['BANK_CODE']] = $value['BANK_NAME'];
		}
		// echo '<pre>';
		// print_r($dataSQL);die; 
		$app = Zend_Registry::get('config');
		$app = $app['app']['bankname'];

		foreach ($newData as $d => $dt) {

			if (!$csv && !$pdf && !$this->_request->getParam('print')) {
				$data[$d]["balanceType"] 		= $dt["BALANCE_TYPE"];
				$data[$d]["allowMultiAction"] = ($dt["PS_CATEGORY"] == "SINGLE PAYMENT") ? true : false;
			}


			$persenLabel = $dt["BALANCE_TYPE"] == '2' ? ' %' : '';
			/*print_r($dt);die;*/
			foreach ($fields as $key => $field) {
				$value = $dt[$key];
				if ($key == 'acbenef' && ($dt['PS_TYPE'] == '12' || $dt['PS_TYPE'] == '16' || $dt['PS_TYPE'] == '17')) {
					$value = '-';
				} elseif ($key == "acbenef" && $value != "-") {
					if (!empty($dt['BANK_CODE'])) {
						$benefBankName = $newDataBank[$dt['BANK_CODE']];
					} else if (!empty($dt['BENEF_ACCT_BANK_CODE'])) {
						$benefBankName = $newDataBank[$dt['BENEF_ACCT_BANK_CODE']];
					}else if (!empty($dt['CLR_CODE'])) {
						$benefBankName = $newDataBank[$dt['CLR_CODE']];
					}else if (!empty($dt['BENEFICIARY_BANK_NAME'])) {
						$benefBankName = $dt['BENEFICIARY_BANK_NAME'];
					} else {
						$benefBankName = $app;
					}


					if($dt['PS_TYPE'] == '4' || $dt['PS_TYPE'] == '25' || $dt['PS_TYPE'] == '26' || $dt['PS_TYPE'] == '38'){
						$value = '-';
					}else{
						if (empty($dt["acbenef_alias"]) || $dt["acbenef_alias"] == '-') {
							$value = $dt['acbenef'] . ' (' . $dt['acbenef_ccy'] . ')' .  " / ".$benefBankName ." / " . $dt['acbenef_name'] . " (S)";
						} else if($dt['PS_TYPE'] == '23'){
							$value = '-';
						}else {
							$value = $dt['acbenef'] . ' (' . $dt['acbenef_ccy'] . ')' .  " / ".$benefBankName ." / " . $dt['acbenef_alias'] . " (A)";
						}
					}
				}
				/*if ($key == "amount" && $csv)
				{	
					
					if( $dt['PS_TYPE']=='14' || $dt['PS_TYPE']=='15'){

						$value = Application_Helper_General::displayMoney($dt['PS_REMAIN']);
					}else{
						$value = Application_Helper_General::displayMoney($value);
					}
				}
				elseif ($key == "amount" && !$csv)
				{
					if( $dt['PS_TYPE']=='14' || $dt['PS_TYPE']=='15' ){

						$value = Application_Helper_General::displayMoney($dt['PS_REMAIN']);
					}else{
						$value = Application_Helper_General::displayMoney($value);
					}	
				}*/
				
				if ($key == 'amount' && ($dt['PS_TYPE'] == '19' || $dt['PS_TYPE'] == '20' || $dt['PS_TYPE'] == '23')) {
					$value = '-';
				} elseif ($key == "updated") {
					$value = Application_Helper_General::convertDate($value, $this->_dateTimeDisplayFormat);
				} elseif ($key == "created") {
					$value = Application_Helper_General::convertDate($value, $this->_dateTimeDisplayFormat);
				} elseif ($key == "efdate") {
					$value = Application_Helper_General::convertDate($value, $this->_dateViewFormat);
				} elseif (($key == "accsrc") && $value != "-") {
					/*$value = $value."[".$dt[$key.'_ccy']."]";*/
					if (empty($dt['SOURCE_ACCT_BANK_CODE'])) {
						$sourceBankName = $app;
					} else {
						$sourceBankName = $newDataBank[$dt['SOURCE_ACCT_BANK_CODE']];
					}

					if (empty($dt["accsrc_alias"]) | $dt["accsrc_alias"] == '-') {
						$value = $dt['accsrc'] . ' (' .$dt['accsrc_ccy']  . ')' . " / ".$sourceBankName." / " . $dt['accsrc_name'] . " (S)";
					} else {

						$value = $dt['accsrc'] . ' (' .$dt['accsrc_ccy']  . ')' . " / ".$sourceBankName." / " . $dt['accsrc_alias'] . " (A)";
					}
				} elseif ($key == "amount" && $value != "-") {
					if ($dt['PS_TYPE'] == '4' && $dt['accsrc_ccy'] == 'USD') {
						$value = "IDR " . Application_Helper_General::displayMoney($dt['amount']) . ' (' . $dt['accsrc_ccy'] . ' ' . Application_Helper_General::displayMoney($dt['EQUIVALEN_IDR']) . ')';
					}else if($dt['PS_TYPE'] == '23' || $dt['PS_TYPE'] == '19' || $dt['PS_TYPE'] == '20' || $dt['PS_TYPE'] == '15' || $dt['PS_TYPE'] == '14'){
						$value = '-';
					}else if($dt['PS_TYPE'] == '11'){
						//var_dump($dt);
						
						$value = $dt['ccy'] . " " . Application_Helper_General::displayMoney($dt['PS_TOTAL_AMOUNT']);
					} else {
						$value = $dt['ccy'] . " " . Application_Helper_General::displayMoney($dt['TRA_AMOUNT']);
					}
				} else if ($key == 'payType' && $value != '-') {
					// if ($dt['TRANSFER_TYPE'] == 0) {
					// 	if ($dt['SOURCE_ACCT_BANK_CODE'] == $dt['BENEF_ACCT_BANK_CODE']) {
					// 		//$value = $dt['payType'] . ' - ' . 'PB';
					// 		$value = $dt['payType'];
					// 	} else {
					// 		//$value = $dt['payType'] . ' - ' . 'ONLINE';
					// 		$value = $dt['payType'];
					// 	}
					// } else if ($dt['TRANSFER_TYPE'] == 1) {
					// 	// $value = $dt['payType'] . ' - ' . 'SKN';
					// 	$value = $dt['payType'];
					// } else if ($dt['TRANSFER_TYPE'] == 2) {
					// 	// $value = $dt['payType'] . ' - ' . 'RTGS';
					// 	$value = $dt['payType'];
					// }

					$dt['TRANSFER_TYPE1'] = $trfType[$dt['TRANSFER_TYPE']];

                    $tra_type	= array("0" => "Online", "2" => "SKN", "1" => "RTGS");

                    $tra_type1 = $tra_type[$dt['TRANSFER_TYPE']];
                    
                    if ($dt['PS_TYPE'] == '19') {
                         $payType = 'CP Same Bank Remains';
                    }else if ($dt['PS_TYPE'] == '20') {
                         $payType = 'CP Same Bank Maintains';
                    }else if ($dt['PS_TYPE'] == '23') {
                         $payType = 'CP Others Remains - '.$tra_type1;
                    }else if ($dt['PS_TYPE'] == '21') {
                         $payType = 'MM - '.$tra_type1;
                    }else if($dt['PS_TYPE'] == '16' || $dt['PS_TYPE'] == '17'){
						$type = $this->_db->fetchRow(
							$this->_db->select()
								->from(array('T_PSLIP_DETAIL'), array('PS_FIELDVALUE'))
								->where('PS_NUMBER = ?' , $dt['PS_NUMBER'])
								->where('PS_FIELDNAME = ?' , 'Type Of Transaction ')
						);
						if(!empty($type)){
							$typeArr = explode(' ',$type['PS_FIELDVALUE']);
							$payType = $typeArr['1'].' - '.$typeArr['0'];
						}else{
						//var_dump($type);die;
						$payType = $dt['payType']; 
						}
					}else if($dt['PS_TYPE'] == '25' || $dt['PS_TYPE'] == '4' || $dt['PS_TYPE'] == '26' || $dt['PS_TYPE'] == '28' || $dt['PS_TYPE'] == '3' || $dt['PS_TYPE'] == '27'){
						$payType = $dt['payType'];
					}elseif($dt['PS_TYPE'] == '38'){
						$payType = 'Inhouse - BG';
					}else {
                        $payType = $dt['payType'] . ' - ' . $dt['TRANSFER_TYPE1'];
                    }

                    $value = $payType;
				}

				$value = ($value == "" && !$csv) ? "&nbsp;" : $value;

				$data[$d][$key] = $this->language->_($value);
			}
		}

		$filterlistdata = array("Filter");
			foreach($dataParamValue as $fil => $val){
					$paramTrx = $fil . " - " . $val;
					array_push($filterlistdata, $paramTrx);
		}
		$this->view->$filterlistdata = $filterlistdata;

		if ($csv) {
			$filterlistdatax = $this->_request->getParam('data_filter');
			$listable = array_merge_recursive(array($header), $data);
			$this->_helper->download->csv($filterlistdatax, $listable, null, 'List Waiting Approval');
			Application_Helper_General::writeLog('PAPV', 'Export CSV Waiting Approval');
		} elseif ($pdf) {
			$this->_helper->download->pdf($header, $data, null, 'List Waiting Approval');
			Application_Helper_General::writeLog('PAPV', 'Export PDF Waiting Approval');
		} elseif ($this->_request->getParam('print')) {
			$filterlistdatax = $this->_request->getParam('data_filter');
			$this->_forward('printtable', 'index', 'widget', array('data_content' => $data, 'data_caption' => 'List Waiting Approval', 'data_header' => $fields, 'data_filter' => $filterlistdatax));
		} else {
			$stringParam = array(
				'PS_NUMBER'		=> $fPaymentReff,
				'PS_TYPE'		=> $fPaymentType,
				'TRANSFER_TYPE'		=> $fTrfType,
				'PS_SUBJECT'	=> $fSubject,
				'SOURCE_ACCOUNT'	=> $fSource,
				'BENEFICIARY_ACCOUNT'	=> $fBenef,
				'PS_EFDATE'	=> $fPayDateFrom,
				'PS_EFDATE_END'	=> $fPayDateTo,
			);

			$this->view->filterPayType 	= $filterPayType;
			$this->view->filterTrfType 	= $filterTrfType;
			$this->view->payReff 		= $fPaymentReff;
			$this->view->payType 		= $fPaymentType;
			$this->view->trfType 		= $fTrfType;
			$this->view->createdFrom 	= $fCreatedFrom;
			$this->view->createdTo 		= $fCreatedTo;
			$this->view->payDateFrom 	= $fPayDateFrom;
			$this->view->payDateTo 		= $fPayDateTo;
			$this->view->query_string_params = $stringParam;
			$this->updateQstring();
			if (!empty($this->_request->getParam('wherecol'))) {
				$this->view->wherecol			= $this->_request->getParam('wherecol');
			}

			if (!empty($this->_request->getParam('whereopt'))) {
				$this->view->whereopt			= $this->_request->getParam('whereopt');
			}

			if (!empty($this->_request->getParam('whereval'))) {
				$this->view->whereval			= $this->_request->getParam('whereval');
			}

			// print_r($data);die;
			$this->view->data 				= $data;
			$this->view->fields 			= $fields;
			$this->view->paymentStart 	= $fPaymentStart;
			$this->view->paymentEnd 	= $fPaymentEnd;
			$this->view->filter 			= $filter;
			$this->view->sortBy 			= $sortBy;
			$this->view->sortDir 			= $sortDir;
			$this->view->allowMultiApprove 	= ($this->view->hasPrivilege('PAPV') && count($data) > 0);
			$this->view->allowMultiReject  	= ($this->view->hasPrivilege('PRJT') && count($data) > 0);

			// set URL
			$URL = $this->view->url(array(
				'module'	 => $this->view->modulename,
				'controller' => $this->view->controllername,
				'action'	 => 'index',
				'page'		 => $page,
				'sortBy' 	 => $this->view->sortBy,
				'sortDir' 	 => $this->view->sortDir
			), null, true) . $this->view->qstring;

			$sessionNamespace = new Zend_Session_Namespace('URL_CP_WA');
			$sessionNamespace->URL = $URL;

			if (!empty($dataParamValue)) {
				// $this->view->createdStart = $dataParamValue['PS_CREATED'];
				// $this->view->createdEnd = $dataParamValue['PS_CREATED_END'];
				$this->view->efdateStart = $dataParamValue['PS_EFDATE'];
				$this->view->efdateEnd = $dataParamValue['PS_EFDATE_END'];

				unset($dataParamValue['PS_CREATED_END']);
				unset($dataParamValue['PS_EFDATE_END']);

				foreach ($dataParamValue as $key => $value) {
					$duparr = explode(',',$value);
								if(!empty($duparr)){
									
									foreach($duparr as $ss => $vs){
										$wherecol[]	= $key;
										$whereval[] = $vs;
									}
								}else{
										$wherecol[]	= $key;
										$whereval[] = $value;
								}
				}
				$this->view->wherecol     = $wherecol;
				$this->view->whereval     = $whereval;
				 
			}

			$this->view->filterlistdata = $filterlistdata;

			Application_Helper_General::writeLog('PAPV', 'Viewing Waiting Approval');
		}
	}
	
	public function validatebtn($transfertype, $amount, $ccy, $psnumb)
	{
		//die;

		if ($transfertype == '19' || $transfertype == '20' || $transfertype == '23' || $transfertype == '14' || $transfertype == '15' || $transfertype == '30') {
			if($transfertype == '19' || $transfertype == '20' || $transfertype == '14' || $transfertype == '15'){
						$transfertype = array('15');
				}else{ 
					$transfertype = array('30');
				}
			$selectuser	= $this->_db->select()
				->from(array('C' => 'M_APP_BOUNDARY'), array(
					'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
					'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
					'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
					'C.TRANSFER_TYPE',
					'C.POLICY'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.TRANSFER_TYPE 	IN (?)",  $transfertype);
			// ->where("C.BOUNDARY_MIN 	<= ?" , $amount)
			// ->where("C.BOUNDARY_MAX 	>= ?" , $amount);

		}else if($transfertype == '11' || $transfertype == '25'){
			$transfertype = array('11');
			$selectuser	= $this->_db->select()
				->from(array('C' => 'M_APP_BOUNDARY'), array(
					'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
					'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
					'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
					'C.TRANSFER_TYPE',
					'C.POLICY'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.TRANSFER_TYPE 	IN (?)",  $transfertype);
		} else {
			$selectuser	= $this->_db->select()
				->from(array('C' => 'M_APP_BOUNDARY'), array(
					'BOUNDARY_MIN' 	=> 'C.BOUNDARY_MIN',
					'BOUNDARY_MAX' => 'C.BOUNDARY_MAX',
					'CCY_BOUNDARY' => 'C.CCY_BOUNDARY',
					'C.TRANSFER_TYPE',
					'C.POLICY'
				))
				->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
				->where("C.TRANSFER_TYPE 	= ?", (string) $transfertype)
				->where("C.BOUNDARY_MIN 	<= ?", $amount)
				->where("C.BOUNDARY_MAX 	>= ?", $amount);
		}

		// echo $selectuser;
		
		$datauser = $this->_db->fetchAll($selectuser);
		 if(empty($datauser)){
			 
			 return true;
			 
		 }
		 
		$selectusergroup	= $this->_db->select()
			->from(array('C' => 'M_APP_GROUP_USER'), array(
				'*'
			))
			->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
			->where("C.USER_ID 	= ?", (string) $this->_userIdLogin);

		$usergroup = $this->_db->fetchAll($selectusergroup);

		


		$this->view->boundarydata = $datauser;
		// print_r($this->view->boundarydata);die;
		if (!empty($usergroup)) {
			$cek = false;

			foreach ($usergroup as $key => $value) {
				$group = explode('_', $value['GROUP_USER_ID']);
				$alphabet = array(1 => 'A', 2 => 'B', 3 => 'C', 4 => 'D', 5 => 'E', 6 => 'F', 7 => 'G', 8 => 'H', 9 => 'I', 10 => 'J', 11 => 'K', 12 => 'L', 13 => 'M', 14 => 'N', 15 => 'O', 16 => 'P', 17 => 'Q', 18 => 'R', 19 => 'S', 20 => 'T', 21 => 'U', 22 => 'V', 23 => 'W', 24 => 'X', 25 => 'Y', 26 => 'Z');
				$groupalfa = $alphabet[(int) $group[2]];
				// print_r($groupalfa);echo '-';
				$usergroup[$key]['GROUP'] = $groupalfa;
				
				// print_r($datauser);die;
				foreach ($datauser as $nub => $val) {
					$command = str_replace('(', '', $val['POLICY']);
					$command = str_replace(')', '', $command);
					$list = explode(' ', $command);

					//var_dump($list);
					foreach ($list as $row => $data) {
						
						if ($data == $groupalfa) {
							$cek = true;
							// die('ter');
							break;
						}
					}
				}
			}
			//die;
			
			//var_dump($group);die;
			
			if ($group[0] == 'S') {
				$cek = true;
				return true;
			}
			// echo $cek;
			// print_r($cek);die;
			if (!$cek) {
				// die('here');
				return false;
			}
		}
		

		if ($cek) {




			$command = ' ' . $datauser['0']['POLICY'] . ' ';
			$command = strtoupper($command);
		
			$cleanCommand = trim(preg_replace('/\s+/', ' ', $command));
			
			//transform to php logical operator syntak
			$translate = array(
				'AND' => '&&',
				'OR' => '||',
				'THEN' => 'THEN$',
				'A' => '$A',
				'B' => '$B',
				'C' => '$C',
				'D' => '$D',
				'E' => '$E',
				'F' => '$F',
				'G' => '$G',
				'H' => '$H',
				'I' => '$I',
				'J' => '$J',
				'K' => '$K',
				'L' => '$L',
				'M' => '$M',
				'N' => '$N',
				'O' => '$O',
				'P' => '$P',
				'Q' => '$Q',
				'R' => '$R',
				// 'S' => '$S',
				'T' => '$T',
				'U' => '$U',
				'V' => '$V',
				'W' => '$W',
				'X' => '$X',
				'Y' => '$Y',
				'Z' => '$Z',
				'SG' => '$SG',
			);

			$phpCommand =  strtr($cleanCommand, $translate);
			//var_dump($phpCommand);die;
			$param = array(
				'0' => '$A',
				'1' => '$B',
				'2' => '$C',
				'3' => '$D',
				'4' => '$E',
				'5' => '$F',
				'6' => '$G',
				'7' => '$H',
				'8' => '$I',
				'9' => '$J',
				'10' => '$K',
				'11' => '$L',
				'12' => '$M',
				'13' => '$N',
				'14' => '$O',
				'15' => '$P',
				'16' => '$Q',
				'17' => '$R',
				// '18' => '$S',
				'19' => '$T',
				'20' => '$U',
				'21' => '$V',
				'22' => '$W',
				'23' => '$X',
				'24' => '$Y',
				'25' => '$Z',
				'26' => '$SG',
			);
			// print_r($phpCommand);die;
			

			$command = str_replace('(', ' ', $val['POLICY']);
			$command = str_replace(')', ' ', $command);
			$list = explode(' ', $command);
			// print_r($list);die;
			// var_dump($command)

			$thendata = explode('THEN', $command);
			// print_r($thendata);echo '<br>';die;
			$cthen = count($thendata);
			// print_r($thendata);die;
			$secondcommand = str_replace('(', '', trim($thendata[0]));
			$secondcommand = str_replace(')', '', $secondcommand);
			$secondcommand = str_replace('AND', '', $secondcommand);
			$secondcommand = str_replace('OR', '', $secondcommand);
			$secondlist = explode(' ', $secondcommand);
			// print_r($secondlist);die;
			// print_r($grouplist);die;
			// print_r($thendata[$i]);die;
			// return true;
			if (!empty($secondlist)) {
				foreach ($usergroup as $key => $value) {
					foreach ($secondlist as $row => $thenval) {
						// print_r($value['GROUP']);echo ' == ';echo $thenval;echo '<br/>';
						if (trim($value['GROUP']) == trim($thenval)) {
							$thengroup = true;
							$grouplist[] = trim($thenval);
							//die('here');
						}
					}
				}
			}

			//var_dump($cthen);
			if ($cthen >= 2) {
				foreach ($usergroup as $key => $value) {
							// print_r($value);
					foreach ($thendata as $row => $thenval) {
						// echo '|';print_r($thenval);echo '==';
						// print_r($value['GROUP']);echo '|';echo '<br/>';
						// $thengroup = true;
						$newsecondcommand = str_replace('(', '', trim($thenval));
						$newsecondcommand = str_replace(')', '', $newsecondcommand);
							$newsecondcommand = str_replace('AND', '', $newsecondcommand);
						$newsecondcommand = str_replace('OR', '', $newsecondcommand);
							$newsecondlist = explode(' ', $newsecondcommand);
						//var_dump($newsecondcommand);
						if (in_array(trim($value['GROUP']), $newsecondlist)) {
						//if (trim($value['GROUP']) == trim($thenval)) {
							$thengroup = true;
							$grouplist[] = trim($thenval);
							//die('here');
						}
					}
				}
			}
			//var_dump($grouplist);die;
			//var_dump($thengroup);die;
			// var_dump($thengroup);die;
			// // print_r($group);die;
			// // echo $thengroup;die;
			//echo '<pre>';
			//var_dump($thengroup);
			//var_dump($thendata);
			//print_r($thendata);echo '<br/>';die('here');
			if ($thengroup == true) {

				 
				for ($i = 1; $i <= $cthen; ++$i) {
					$oriCommand = $phpCommand;
					//echo $oriCommand;die;
					$indno = $i;
					//echo $oriCommand;echo '<br>';

					for ($a = $cthen - $indno; $a >= 1; --$a) {

						if ($i > 1) {
							$replace = 'THEN$ $' . trim($thendata[$a + 1]);
						} else {
							$replace = 'THEN$ $' . trim($thendata[$a]);
						}

						$oriCommand = str_replace($replace, "", $oriCommand);
					}


					//print_r($thendata);echo '<br>';die();

					//die;
					// if($i == 3){
					// echo 'command : ';echo $oriCommand;echo '<br/>';
					// }
					//print_r($oriCommand);echo '<br>';
					//print_r($list);echo '<br>';die;
					
					
					$result = $this->generate($oriCommand, $list, $param, $psnumb,$groupalfa,$thengroup);
					// print_r($i);
					//var_dump($result);
					  //echo 'result-';var_dump($result);echo '-';die;
					if ($result) {
						// die;
						// echo $thendata[$i+1];die('eere');
						// print_r($i);
						// print_r($thendata);die;



						$replace = 'THEN$ $' . trim($thendata[$i + 1]);
						// var_dump($replace);die;
						// print_r($i);
						if (!empty($thendata[$i + 1])) {
							//die;
							$oriCommand = str_replace($replace, "", $phpCommand);
						} else {
							// die;
							$thirdcommand = str_replace('(', '', trim($thendata[$i - 1]));
							$thirdcommand = str_replace(')', '', $thirdcommand);
							$thirdcommand = str_replace('AND', '', $thirdcommand);
							$thirdcommand = str_replace('OR', '', $thirdcommand);
							$thirdlist = explode(' ', $thirdcommand);
	//						var_dump($secondlist);
	//						var_dump($grouplist);
//							die;

							if (!empty($secondlist)) {
								foreach ($grouplist as $key => $valg) {
									foreach ($secondlist as $row => $value) {
										if ($value == $valg) {
											//echo 'sini';
											return false;
										}
									}
								}
							}
							$oriCommand = $phpCommand;
						}
						// print_r($thendata[$i]);die;
						// if($i == 3){
						// echo $oriCommand;die;
						// echo '<br/>';	
						// }
						// print_r($i);
						 //echo $oriCommand;
						$result = $this->generate($oriCommand, $list, $param, $psnumb,$groupalfa,$thengroup);
						 //var_dump($result);echo '<br/>';
						if (!$result) {
							// die;
							//print_r($groupalfa);
							//print_r($thendata[$i]);
							
							if($groupalfa == trim($thendata[$i])){
								//echo 'heer';
								return true;
							}else{
								
								//return true;
							}
							/*
							foreach ($grouplist as $key => $valg) {
								 
								if (trim($valg) == trim($thendata[$i])) {
									// die;
									// print_r($i);
									// print_r($thendata);
									// print_r($thendata[$i]);die;
									if ($thendata[$i + 1] == $valg) {
										return true;
									} else {
										// die('here');
										return false;
									}
								}
							} */
						} else {
							
							//return false;
								
							// $result = $this->generate($phpCommand,$list,$param,$psnumb);
							// print_r($phpCommand);
							// if($result){}
							// die('here');
						}
						// var_dump($result);
						// die;


					} else {
						 //die('here');
						$secondcommand = str_replace('(', '', trim($thendata[$i - 1]));
						$secondcommand = str_replace(')', '', $secondcommand);
						$secondcommand = str_replace('AND', '', $secondcommand);
						$secondcommand = str_replace('OR', '', $secondcommand);
						$secondlist = explode(' ', $secondcommand);
						//var_dump($i);
						// var_dump($thendata);
						 //print_r($grouplist);
						 //die;
						 //if($groupalfa == $gro)
							 $approver = array();
			 
							foreach ($list as $key => $value) {
								if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
									$selectapprover	= $this->_db->select()
										->from(array('C' => 'T_APPROVAL'), array(
											'USER_ID'
										))
										->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
										// ->where("C.USER_ID 	= ?" , (string)$this->_userIdLogin)
										->where("C.PS_NUMBER = ?", (string) $psnumb)
										->where("C.GROUP = ?", (string) $value);
								//	 echo $selectapprover;
									$usergroup = $this->_db->fetchAll($selectapprover);
									// print_r($usergroup);
									$approver[$value] = $usergroup;
								}
							} 
							//var_dump($groupalfa);
							//var_dump($secondlist[0]);die;
							//foreach($approver as $app => $valpp){
								if(!empty($thendata[$i])){
									if(empty($approver[$secondlist[0]]) && $groupalfa != $secondlist['0']){
										//die('gere');
										return false;
									}
								}
							
							//echo '<pre>';
							//var_dump($thendata);
							//var_dump($grouplist);
							//var_dump($approver);
							//var_dump($groupalfa);
							//die;
							 foreach($grouplist as $key => $vg){
								 $newgroupalpa = str_replace('AND', '', $vg);
									$newgroupalpa = str_replace('OR', '', $newgroupalpa);
									$groupsecondlist = explode(' ', $newgroupalpa);
									 //var_dump($newgroupalpa);
									 //&& empty($approver[$groupalfa])
								 if(in_array($groupalfa,$groupsecondlist)  ){
									 // echo 'gere';die;
									 return true;
									 //die('ge');
								 }
								 
								 if($vg == $groupalfa  && count($approver[$groupalfa]) > 0){
									 // echo 'gere';die;
									 return true;
									 //die('ge');
								 }
								 
							 }
							
							// var_dump($approver[$groupalfa]);
							// var_dump($grouplist);
							 //var_dump($groupalfa);die;
							 
							 //echo 'ger';die;
						//print_r($secondlist);die;
					//	 return true;
						if (!empty($secondlist)) {
							foreach ($grouplist as $key => $valg) {
								foreach ($secondlist as $row => $value) {
									if ($value == $valg) {
										
										if(empty($thendata[1])){
											//die;
												return true;
										}
										//else{
										//	return false;
									//	}
									}
								}
							}
						}
						 
					//	echo 'here';die;
						 $secondresult = $this->generate($thendata[$i-1],$list,$param,$psnumb,$groupalfa,$thengroup);
						// var_dump($secondresult);
						 //print_r($thendata[$i-1]);
						 //die;
						 //if()
						//	 echo '<pre>';
						 //var_dump($grouplist);die;
						foreach ($grouplist as $key => $valgroup) {
							//print_r($valgroup);
							//var_dump($thendata[$i]); 
							
							
							if (trim($valg) == trim($thendata[$i])) {
								$cekgroup = false;
								 //die('here');
								 if($secondresult){
										return false;
								 }else{
									 return true;
								 }
								
								//break;
							}
							
							//else if (trim($valg) == trim($thendata[$i - 1])) {
						//		$cekgroup = false;
								// die('here');
							//	return false;
						//	}
						}
						//die;
						//if (!$cekgroup) {
							// die('here');
							//return false;
						//}
					}

					//		    echo '<br/>';
					//	${$command} = $oriCommand;
				}
			} else if (!empty($thendata) && $thengroup == false) {
				
				//var_dump($groupalfa)die;
				foreach ($thendata as $ky => $vlue) {
					$newsecondcommand = str_replace('(', '', trim($vlue));
					$newsecondcommand = str_replace(')', '', $newsecondcommand);
					$newsecondcommand = str_replace('AND', '', $newsecondcommand);
					$newsecondcommand = str_replace('OR', '', $newsecondcommand);
					$newsecondlist = explode(' ', $newsecondcommand);
					if ($newsecondlist['0'] == $groupalfa) {
						return true;
					}
				}
				return false;
			}
			
			


			$approver = array();
			// print_r($list);die;  	
			foreach ($list as $key => $value) {
				if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
					$selectapprover	= $this->_db->select()
						->from(array('C' => 'T_APPROVAL'), array(
							'USER_ID'
						))
						->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
						// ->where("C.USER_ID 	= ?" , (string)$this->_userIdLogin)
						->where("C.PS_NUMBER = ?", (string) $psnumb)
						->where("C.GROUP = ?", (string) $value);
				//	 echo $selectapprover;
					$usergroup = $this->_db->fetchAll($selectapprover);
					// print_r($usergroup);
					$approver[$value] = $usergroup;
				}
			}
			//die;
			



			// print_r($phpCommand);die;
			foreach ($param as $url) {
				if (strpos($phpCommand, $url) !== FALSE) {
					$ta = substr_count($phpCommand, $url);
					// print_r($list);die;

					if (!empty($approver)) {
						// print_r($approver);die;
						foreach ($list as $key => $value) {
							if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
								foreach ($approver[$value] as $row => $val) {
									// print_r($approver);die;
									if (!empty($val)) {
										$values = 'G' . $value;
										${$values}[$row + 1] = true;
										// print_r($B);
									}

									// print_r($val);
								}
							}
						}
					}

					for ($i = 1; $i <= $ta; $i++) {
						// print_r($list);die;
						foreach ($list as $key => $value) {
							if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
								$values = 'G' . $value;
								// print_r(${$values});
								if (empty(${$values}[$i])) {
									${$values}[$i] = false;
								}
								// if(${$value}[$i])
							}
						}
						// print_r($phpCommand);die;
						$numb = $i;
						$label = str_replace('$', '$G', $url);

						$replace = $label . '[' . $numb . ']';

						$alf = str_replace('$', '', $url);
						$values = 'G' . $alf;
						// print_r($values);die;
						if (${$values}[$i] == $replace) {
							$phpCommand = $this->str_replace_first($url, 'true', $phpCommand, 1);
							// print_r($phpCommand);
						} else {
							$phpCommand = $this->str_replace_first($url, 'false', $phpCommand, 1);
							// print_r($phpCommand);die;
						}
						// }
						// }

					}
					// print_r($GB);die;

				}
			}

			$keywords = preg_split("/[\s,]+/", $cleanCommand);
			$result =  false;
			$thendata = explode('THEN$',$phpCommand);
			//var_dump($thendata);die;
			if(!empty($thendata['1'])){
					$phpCommand = '';
				foreach($thendata as $tkey => $tval){
					$phpCommand .= '(';
					$phpCommand .= $tval.')';
					if(!empty($thendata[$tkey+1])){
					$phpCommand .= ' && ';
					}
				}
			}else{
				
				$phpCommand = str_replace('THEN$', '&&', $phpCommand);
			
			}
			//var_dump($phpCommand);
			if (!empty($phpCommand)) {
				eval('$result = ' . "$phpCommand;");
			} else {
				return false;
			}
			//var_dump($result);die;
			if(!$result){ 
			
				return true;
			}
			// die('here2');
			 //var_dump ($result);die;
			//return $result;
		} else {
			// die('here');
			return true;
		}
	}
	
	



	public function generate($command, $list, $param, $psnumb,$group,$thengroup)
	{

		$phpCommand = $command;

		// echo $command;die;

		$approver = array();
		 
		$count_list = array_count_values($list);
		//print_r($count_list);
		foreach ($list as $key => $value) {
			if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
				$selectapprover	= $this->_db->select()
					->from(array('C' => 'T_APPROVAL'), array(
						'USER_ID'
					))
					->where("C.CUST_ID 	= ?", (string) $this->_custIdLogin)
					// ->where("C.USER_ID 	= ?" , (string)$this->_userIdLogin)
					->where("C.PS_NUMBER = ?", (string) $psnumb)
					->where("C.GROUP = ?", (string) $value);
				// echo $selectapprover;
				$usergroup = $this->_db->fetchAll($selectapprover);
				// print_r($usergroup);
				$approver[$value] = $usergroup;
			}
		}
		//var_dump($param);die;
		//var_dump($group);
		foreach($approver as $appval){
			$totaldata = count($approver[$group]);
			$totalgroup = $count_list[$group];
			//var_dump($totaldata);
			//var_dump($totalgroup);
			if($totalgroup == $totaldata && $totalgroup != 0){
				
				return false;
			}
		}//die;
		//die;
		 

		

		 
		foreach ($param as $url) {
			
			if (strpos($phpCommand, $url) !== FALSE) {
				$ta = substr_count($phpCommand, $url);
				// print_r($list);die;

				if (!empty($approver)) {
					// print_r($approver);die;
					foreach ($list as $key => $value) {
						if ($value != 'AND' && $value != 'OR' && $value != 'THEN' && $value != '') {
							foreach ($approver[$value] as $row => $val) {
								// print_r($approver);die;
								if (!empty($val)) {
									$values = 'G' . $value;
									${$values}[$row + 1] = true;
									// print_r($B);
								}

								// print_r($val);
							}
						}
					}
				}


				// print_r($approver);die;

				for ($i = 1; $i <= $ta; $i++) {

					foreach ($list as $key => $value) {
						if (!empty($value)) {
							if ($value != 'AND' && $value != 'OR' && $value != 'THEN') {
								$values = 'G' . $value;
								// print_r(${$values});
								if (empty(${$values}[$i])) {
									${$values}[$i] = false;
								}
								// if(${$value}[$i])
							}
						}
					}


					$numb = $i;
					$label = str_replace('$', '$G', $url);
					// print_r($phpCommand);die('here');
					$replace = $label . '[' . $numb . ']';

					$alf = str_replace('$', '', $url);
					$values = 'G' . $alf;

					if (${$values}[$i] == $replace) {
						$phpCommand = $this->str_replace_first($url, 'true', $phpCommand, 1);
						// print_r($phpCommand);
					} else {
						$phpCommand = $this->str_replace_first($url, 'false', $phpCommand, 1);
						// print_r($phpCommand);die;
					}
					// }
					// }

				}
				// print_r($GB);die;

			}
		}
		
		$keywords = preg_split("/[\s,]+/", $cleanCommand);
		$result =  false;
		$phpCommand = str_replace('THEN$', '&&', $phpCommand);
		//print_r($phpCommand);echo '<br/>';

		if (!empty($phpCommand)) {
			eval('$result = ' . "$phpCommand;");
			//var_dump($thengroup);
			// var_dump($result);
			 
			if ($result) {
//var_dump($thengroup);die;
				if($thengroup){
						return true;
				}else{
					return false;
				}
				
			} else {
				
				if($thengroup){
						return false;
				}else{
					
					return true;
				}
			}
			// return $result;
		} else {
			return false;
		}

		// var_dump ($result);die;




	}
	
	function str_replace_first($from, $to, $content, $row)
			{
				$from = '/' . preg_quote($from, '/') . '/';
				return preg_replace($from, $to, $content, $row);
			}

	
	
}
