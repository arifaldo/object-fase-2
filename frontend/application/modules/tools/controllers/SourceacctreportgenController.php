<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Payment.php';
class tools_SourceacctreportgenController extends Application_Main {
	
	public function initController(){
	}
	
	public function indexAction() 
	{
		$cust_id = $this->_getParam('custID');
		

        $accData = $this->_db->select()
					->from(array('M_CUSTOMER_ACCT'), array('*'))
					->where('CUST_ID = '.$this->_db->quote((string)$this->_custIdLogin))
                    ->query()->fetchAll();

		$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$accArr 	  = $CustomerUser->getAccounts();
		
		
		$acctlist = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'M_APIKEY'))
						 ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
						  ->join(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
						 // ->where('A.ACCT_STATUS = ?','5')
						 ->where("A.CUST_ID = ? ",$this->_custIdLogin)
						 ->order('A.APIKEY_ID ASC')
						 // echo $acctlist;
				);
						 // echo $acctlist;die;
		 //echo '<pre>';
		// echo $acctlist;
		 //print_r($acctlist);die;
		 $masterData = $this->_db->fetchAll(
            $this->_db->select()
              ->from(array('A' => 'M_MAKERLIMIT'),array('USER_LOGIN','ACCT_NO','ACCT_NAME','ACCT_CCY','CUST_ID','MAXLIMIT'))
              ->joinLeft(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
              ->where('A.CUST_ID = ?', $this->_custIdLogin)
              ->where('A.USER_LOGIN = ?', $this->_userIdLogin)
              ->where('A.MAXLIMIT > 0')
            //  ->where('A.MAXLIMIT_OPEN = 1')
          );
		//echo '<pre>';
		//var_dump($accData);
		//var_dump($masterData);die;
		$account = array();
		foreach ($acctlist as $key => $value) {
			
			if($value['FIELD'] == 'account_number'){
				foreach ($masterData as $newkey => $valuemaster) {
				  if($value['VALUE'] == $valuemaster['ACCT_NO']){
				    $account[$value['ID']][$value['FIELD']] = $value['VALUE'];
				    $account[$value['ID']]['BANK_CODE'] = $value['BANK_CODE'];
					$account[$value['ID']]['SENDER_ID'] = $value['SENDER_ID'];
					$account[$value['ID']]['AUTH_USER'] = $value['AUTH_USER'];
					$account[$value['ID']]['AUTH_PASS'] = $value['AUTH_PASS'];
					$account[$value['ID']]['BANK_NAME'] = $value['BANK_NAME'];
					$account[$value['ID']]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
				  }
				}
			}
		}
		// echo "<pre>";
		// var_dump($account);die;
		 $acct = array();
			$i = 0;
		foreach ($account as $key => $value) {
			
			$acct[$i]['CUST_ID'] = $value['BANK_NAME'];			
			$acct[$i]['ACCT_NO'] = $value['account_number'];
			$acct[$i]['ACCT_ALIAS_NAME'] = $value['account_alias'];
			//$acct[$i]['ACCT_BANK'] = $value['BANK_NAME'];
			$acct[$i]['CCY_ID'] = $value['account_currency'];
			//$acct[$i]['BANK_CODE'] = $value['BANK_CODE'];
			//$acct[$i]['BANK_NAME'] = $value['BANK_NAME'];
			$acct[$i]['ACCT_NAME'] = $value['account_name'];
			$acct[$i]['ACCT_DESC'] = '';
			//$acct[$i]['SENDER_ID'] = $value['SENDER_ID'];
			//$acct[$i]['AUTH_USER'] = $value['AUTH_USER'];
			//$acct[$i]['AUTH_PASS'] = $value['AUTH_PASS'];
			//$acct[$i]['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
			$i++;
		}
		//$no = 0;
		$app = Zend_Registry::get('config');
		$appBankname = $app['app']['bankname'];
		$acctArr = array();
		foreach($accData as $key => $val){
			foreach ($masterData as $newkey => $valuemaster) {
				
				if($val['ACCT_NO'] == $valuemaster['ACCT_NO']){
					
				$acctArr[$key]['CUST_ID'] = $appBankname;
				$acctArr[$key]['ACCT_NO'] = $val['ACCT_NO'];
				//$acctArr[$key]['ACCT_BANK'] = $value['BANK_NAME'];
				$acctArr[$key]['ACCT_NAME'] = $val['ACCT_NAME'];
				$acctArr[$key]['ACCT_ALIAS_NAME'] = $val['ACCT_ALIAS_NAME'];
				$acctArr[$key]['ACCT_DESC'] = $val['ACCT_DESC'];
				$acctArr[$key]['CCY_ID'] = $val['CCY_ID'];
				}
			}
			
		}
		
		
		
		$data = array_merge($acct,$acctArr);
	//	echo '<pre>';
	//	var_dump($acct);
		
		//var_dump($acctArr);
		//var_dump($data);die;
		/*
		$masterData = $this->_db->fetchAll(
            $this->_db->select()
              ->from(array('A' => 'M_MAKERLIMIT'),array('USER_LOGIN','ACCT_NO','ACCT_NAME','ACCT_CCY','CUST_ID','MAXLIMIT'))
              ->joinLeft(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
              ->where('A.CUST_ID = ?', $this->_custIdLogin)
              ->where('A.USER_LOGIN = ?', $this->_userIdLogin)
              ->where('A.MAXLIMIT > 0')
              ->where('A.MAXLIMIT_OPEN = 1')
          );
		  //echo '<pre>';
		  //var_dump($masterData);die;
		  $dataArr = array();
		  $datano = 0;
		foreach($data as $value){
			
				foreach ($masterData as $newkey => $valuemaster) {
				  if($value['ACCT_NO'] == $valuemaster['ACCT_NO']){
					  $dataArr[$datano] = $value; 
					  $datano++;
				  }
				}
			
		} 
		*/		
		// echo "<pre>";
		// var_dump($data);
		// die;
		$this->view->listAcct =  $data;
		$this->view->dateTimeDisplayFormat  =  $this->_dateTimeDisplayFormat;

	}
}

