<?php
//require_once 'General/CustomerUser.php';

class tools_ServiceController extends Application_Main
{
	public function initController(){
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender();
	}

	public function userAction()
    {
    	$SoapServer 	= new SGO_Soap_Server();

		$wsdl 	= $this->_getParam('wsdl');
		$wsdl	= ($wsdl == 1)? true: false;

		try {
			$SoapServer->call('ServerUser', $wsdl);
		} catch(Exception $e) {
			echo "Error :".$e->getMessage();
		}
    }

	public function coreAction()
    {
    	$SoapServer 	= new SGO_Soap_Server();

		$wsdl 	= $this->_getParam('wsdl');
		$wsdl	= ($wsdl == 1)? true: false;

		try {
			$SoapServer->call('ServerCore', $wsdl);
		} catch(Exception $e) {
			echo "Error :".$e->getMessage();
		}
    }

	public function tokenAction()
    {
    	$SoapServer 	= new SGO_Soap_Server();

		$wsdl 	= $this->_getParam('wsdl');
		$wsdl	= ($wsdl == 1)? true: false;

		try {
			$SoapServer->call('ServerToken', $wsdl);
		} catch(Exception $e) {
			echo "Error :".$e->getMessage();
		}
    }

	public function dataAction()
    {
    	$SoapServer 	= new SGO_Soap_Server();

		$wsdl 	= $this->_getParam('wsdl');
		$wsdl	= ($wsdl == 1)? true: false;

		try {
			$SoapServer->call('ServerData', $wsdl);
		} catch(Exception $e) {
			echo "Error :".$e->getMessage();
		}
    }

}
