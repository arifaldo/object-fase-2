<?php

require_once 'Zend/Controller/Action.php';
require_once 'CMD/Payment.php';
class tools_SpecialrequestController extends Application_Main {
	
	public function initController(){
	}
	
	public function indexAction() 
	{
		$paymentRef = $this->_getParam('numreq');
		

        $srdata = $this->_db->select()
                             ->from(array('A' => 'T_HISTORY_REQRATE'),array('*'))
                             ->where('A.ID_REQUEST = ?',$paymentRef)
                             ->query()->fetchAll();

		$this->view->paymentHistory =  $srdata;
		$this->view->dateTimeDisplayFormat  =  $this->_dateTimeDisplayFormat;
	}
}

