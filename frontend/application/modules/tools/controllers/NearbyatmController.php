<?php

require_once 'Zend/Controller/Action.php';

class tools_NearbyatmController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
	   $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		 if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
	
    	$bankaddress1 = $this->language->_('ATM Address');
		
		$cityname = $this->language->_('City Name');
	
		
	    $fields = array(
	    
						'atm_address'      => array('field' => 'ATM_ADDRESS',
											      'label' => $bankaddress1,
											      'sortable' => true),
						'city_name'      => array('field' => 'ATM_CITY',
											      'label' => $cityname,
											      'sortable' => true)
				      );

   		$filterlist = array('ATM_ADDRESS','ATM_CITY');
		
		$this->view->filterlist = $filterlist;

		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','ATM_ADDRESS');
		$sortDir = $this->_getParam('sortdir','asc');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;

		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';

		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	'filter' 	  	 => array('StringTrim','StripTags'),
							'ATM_ADDRESS'  => array('StringTrim','StripTags'),
							'ATM_CITY'      => array('StringTrim','StripTags'),
							
		);

		$dataParam = array("ATM_ADDRESS","ATM_CITY");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{
			
			// print_r($dtParam);die;
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}
				
			}
				
			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}
// print_r($dataParamValue);die;
		$zf_filter = new Zend_Filter_Input($filterArr,array(),$dataParamValue);
		// $filter = $zf_filter->getEscaped('filter');
		$filter 		= $this->_getParam('filter');

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$csv = $this->_getParam('csv');
		$pdf = $this->_getParam('pdf');	
		//$filter_lg = $this->_getParam('filter');	
		
		//$tempfields = $fields;
		//unset($tempfields['country_name']);
		//$getData = Application_Helper_Array::SimpleArray($tempfields,'field');

		$select = $this->_db->select()
					        ->from(array('A' => 'M_ATM'),array('ATM_ADDRESS','ATM_CITY'));
					        // ->join(array('B' => 'M_COUNTRY'),'A.country_code = B.COUNTRY_CODE', array());


		//if($filter == 'Set Filter' || $filter == 'Mengatur Filter' || $csv || $pdf) 
		if($filter == true || $csv || $pdf || $this->_request->getParam('print')) 
		{  
			//$tempfields['country_name'] = $fields['country_name'];
		    $header = Application_Helper_Array::simpleArray($fields,'label');

			
			$fatm_address     = $zf_filter->getEscaped('ATM_ADDRESS');
			$fcity    = $zf_filter->getEscaped('ATM_CITY');

	        
	        if($fatm_address)     $select->where('UPPER(ATM_ADDRESS) LIKE '.$this->_db->quote('%'.strtoupper($fatm_address).'%'));
	        if($fcity)    $select->where('UPPER(ATM_CITY) LIKE '.$this->_db->quote('%'.strtoupper($fcity).'%'));
			
			$this->view->fatm_address  = $fatm_address;
			$this->view->fcity    = $fcity;
			// unset($_GET['wherecol']);
			// unset($_GET['whereval']);

			
		}

		//$this->view->success = true;
		$select->order($sortBy.' '.$sortDir);  
		
		$select = $this->_db->fetchall($select);

		//--------konfigurasicsv dan pdf---------
	    if($csv)
	    {		
	    	foreach ($select as $key => $val ){
	    		// unset($select[$key]['country_code']);
	    	}
				$this->_helper->download->csv($header,$select,null,'ATM BANK');
				Application_Helper_General::writeLog('RBLS','Download CSV ATM Bank');
		}
		else if($pdf)
		{
				$this->_helper->download->pdf($header,$select,null,'ATM BANK');
				Application_Helper_General::writeLog('RBLS','Download PDF ATM Bank');
		}
		else if($this->_request->getParam('print') == 1){
             $this->_forward('print', 'index', 'widget', array('data_content' => $select, 'data_caption' => 'ATM Bank', 'data_header' => $fields));
       	}
		else
		{		
				Application_Helper_General::writeLog('RBLS','View ATM Bank');
		}	
		//-------END konfigurasicsv dan pdf------------

		$this->paging($select);
	if(!empty($dataParamValue)){
			foreach ($dataParamValue as $key => $value) {
				$wherecol[]	= $key;
				$whereval[] = $value;
			}
        $this->view->wherecol     = $wherecol;
        $this->view->whereval     = $whereval;
     
      }

    

      
		$this->view->fields = $fields;
		$this->view->filter = $filter;

	}

}