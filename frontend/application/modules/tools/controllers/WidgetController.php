<?php

require_once 'Zend/Controller/Action.php';


class tools_WidgetController extends Application_Main
{


	protected $_moduleDB = 'RTF'; //masih harus diganti

	public function initController()
	{
		$getSession = new Zend_Session_Namespace('cust_model');
		if (intval($getSession->model) !== 1) {
			$this->_redirect('/home/dashboard');
		}
	}

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if (count($temp) > 1) {
			if ($temp[0] == 'F' || $temp[0] == 'S') {
				if ($temp[0] == 'F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = '';
				unset($temp[0]);
				foreach ($temp as $value) {
					if (!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}
		}


		$arrWid = array(
			'2' => 'Top Account',
			'7' => 'Credit Card',
			'5' => 'Exchange Rate',
			'4' => 'Report',
			'8' => 'Bank Guarantee',
			'9' => 'Budget Realization',
		);

		$fields = array(
			'bank_name'      => array(
				'field' => 'ID',
				'label' => $this->language->_('Widget'),
				'sortable' => true
			),
			/* 	'city'           => array('field' => 'CITY_CODE',
											      'label' => 'City',
											      'sortable' => true), */
			'clearing_code'  => array(
				'field' => 'CLR_CODE',
				'label' => $this->language->_('Action'),
				'sortable' => true
			),
			/*'swift_code'     => array('field' => 'SWIFT_CODE',
											      'label' => 'Swift Code',
											      'sortable' => true)*/
		);


		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby', 'doc_no');
		$sortDir = $this->_getParam('sortdir', 'asc');

		$csv = $this->_getParam('csv');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page, 'Digits') && ($page > 0)) ? $page : 1;

		$sortBy = (Zend_Validate::is(
			$sortBy,
			'InArray',
			array(array_keys($fields))
		)) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is(
			$sortDir,
			'InArray',
			array('haystack' => array('asc', 'desc'))
		)) ? $sortDir : 'asc';



		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;

		// $this->view->cityArr = array_merge(array(''=>'--- select city ---'),$this->getCity());


		$select = $this->_db->select()
			->from(array('A' => 'M_WIDGET'), array(
				'*'
			))
			->where('A.CUST_ID = ?', $this->_custIdLogin)
			->where('A.USER_ID = ?', $this->_userIdLogin);

		//$select->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin));



		//$this->view->success = true;
		$select->order($sortBy . ' ' . $sortDir);

		$select = $this->_db->fetchall($select);

		$header = Application_Helper_Array::simpleArray($fields, 'label');

		//--------konfigurasicsv dan pdf---------
		if ($csv) {
			$this->_helper->download->csv($header, $select, null, 'bank_table');  //array('Bank Name','Location','Clearing Code')
			Application_Helper_General::writeLog('VDBT', 'Export CSV Domestic Bank Table');
		} else {
			Application_Helper_General::writeLog('MWDG', 'Lihat Widget');
		}
		//-------END konfigurasicsv dan pdf------------



		$this->paging($select);
		$this->view->fields = $fields;
		$this->view->filter = $filter;

		//$this->_helper->download->csv(array('Group ID','Group Name','Status'),null,$select,'download group');



	}


	public function removewidgetAction()
	{
		// 		ini_set('display_errors', 1);
		// ini_set('display_startup_errors', 1);
		// error_reporting(E_ALL);

		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$id = $this->_getParam('id');

		//      $this->_helper->_layout->setLayout('newlayout'); 
		//       $frontendOptions = array ('lifetime' => 259200, 
		//                                 'automatic_serialization' => true );
		//       $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
		//       $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
		//       $cacheID = 'BAL'.$this->_custIdLogin.$this->_userIdLogin;

		// $result = $cache->load($cacheID);

		// foreach ($account as $key => $request) {
		# code...

		$where['WIDGET_ID = ?'] = $id;
		$where['CUST_ID = ?'] = $this->_custIdLogin;
		$where['USER_ID = ?'] = $this->_userIdLogin;
		//var_dump($where);
		$this->_db->delete('M_WIDGET', $where);

		Application_Helper_General::writeLog('MWDG', 'Aktifkan Widget Bank Garansi');


		//$balance = $this->_db->fetchAll(
		//	$this->_db->select()
		//		 ->from(array('A' => 'M_WIDGET'),array('*'))


		//		 ->where("A.CUST_ID = ? ",$this->_custIdLogin)
		//		 ->where("A.USER_ID = ? ",$this->_userIdLogin)

		// ->where("A.ACCT_NO = ? ",$request['account_number'])
		// ->where("A.BANK_CODE = ? ",$request['BANK_CODE'])

		//);
		return true;

		// var_dump($balance);
		// if(!empty($balance)){
		// // print_r($balance);die;
		// $date=date_create($balance['0']['DONE']);
		// $dateupdate = date_format($date,"d-M-Y H:i:s");

		// 	echo $dateupdate.','.$balance['0']['USER_ID'];
		// }

	}


	// private function getCity()
	// {
	//     $select = $this->_db->select()
	//                        ->distinct()
	// 				       ->from(array('A' => 'M_DOMESTIC_BANK_TABLE'),array('CITY_CODE'))
	// 				       ->where('CITY_CODE !='.$this->_db->quote(''))
	// 				       ->order(array('CITY_CODE ASC'))
	// 				       ->query()->fetchAll();
	//
	// 	$city_arr = array();
	// 	foreach($select as $row)
	// 	{
	// 	   $city_arr[trim($row['CITY_CODE'])] = $row['CITY_CODE'];
	// 	}
	//
	// 	//$result = Application_Helper_Array::listArray($select,'CITY','CITY');
	//
	//       return $city_arr;
	// }



}
