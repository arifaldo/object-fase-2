<?php

require_once 'Zend/Controller/Action.php';


class tools_OthersController extends Application_Main {

	public function indexAction()
	{
			$this->_helper->layout()->setLayout('newlayout');
	    $pdf = $this->_getParam('pdf');


	    $groupDesc = array( '01' => 'Group 1',
	                        '02' => 'Group 2',
	                        '03' => 'Group 3',
	                        '04' => 'Group 4',
	                        '05' => 'Group 5',
	                        '06' => 'Group 6',
	                        '07' => 'Group 7',
	                        '08' => 'Group 8',
	   						          '09' => 'Group 9',
	                        '10' => 'Group 10',
	                        'S'  => 'Special Group');

	    $this->view->group_desc = $groupDesc;


		for($i=1;$i<11;$i++)
		{
			$groupuserid = 'N_'.$this->_custIdLogin.'_0'.$i;
			if(strlen($i) == 2)
			{
				$groupuserid = 'N_'.$this->_custIdLogin.'_'.$i;
			}
			$selectgroup = $this->_db->select()
					       	->from(array('A' => 'M_APP_GROUP_USER'),array('USER_ID'));
			$selectgroup -> where("A.CUST_ID LIKE ".$this->_db->quote($this->_custIdLogin));
			$selectgroup -> where("A.GROUP_USER_ID LIKE ".$this->_db->quote($groupuserid));
			$resultgroup = $selectgroup->query()->FetchAll();
			$groupuseridview = 'group'.$i;
			$this->view->$groupuseridview = $resultgroup;
		}

		$specialid = 'S_'.$this->_custIdLogin;
		$selectspecial = $this->_db->select()
					       		->from(array('A' => 'M_APP_GROUP_USER'),array('USER_ID'));
		$selectspecial -> where("A.CUST_ID LIKE ".$this->_db->quote($this->_custIdLogin));
		$selectspecial -> where("A.GROUP_USER_ID LIKE ".$this->_db->quote($specialid));
		$resultspecial = $selectspecial->query()->FetchAll();
		$this->view->special = $resultspecial;

	   $authorizationGroupUser = $this->_db->select()
		                                 ->from(array('B'=>'M_APP_BOUNDARY'),
							                    array('BOUNDARY_ID','CCY_BOUNDARY', 'BOUNDARY_MIN', 'BOUNDARY_MAX'))
							             ->join(array('BG' => 'M_APP_BOUNDARY_GROUP'),'B.BOUNDARY_ID = BG.BOUNDARY_ID',array('BOUNDARY_GROUP_ID','GROUP_USER_ID'))
							             ->join(array('GU' => 'M_APP_GROUP_USER'),'BG.GROUP_USER_ID = GU.GROUP_USER_ID',array('USER_ID'))
							             ->join(array('U'  => 'M_USER'),'U.USER_ID = GU.USER_ID',array('USER_FULLNAME'))
							             ->where('U.CUST_ID = ?',(string)$this->_custIdLogin)
							             ->where('B.CUST_ID = ?',(string)$this->_custIdLogin)
							             ->order(array('CCY_BOUNDARY','BOUNDARY_MIN'.' '.'ASC'))
							             ->query()->fetchAll();

		/*Zend_Debug::dump($authorizationGroupUser);
		die;	*/

		$authorizationMatrix = array();
	    $groupArray = array();
	    $groupUserArray = array();

		foreach($authorizationGroupUser as $row)
		{
		   //MERANGKUM DATA DARI APP BOUNDARY
		   $authorizationMatrix[$row['BOUNDARY_ID']]['BOUNDARY_MIN'] = $row['BOUNDARY_MIN'];
		   $authorizationMatrix[$row['BOUNDARY_ID']]['BOUNDARY_MAX'] = $row['BOUNDARY_MAX'];
		   $authorizationMatrix[$row['BOUNDARY_ID']]['CCY_BOUNDARY'] = $row['CCY_BOUNDARY'];

		   //example : N_CUSTCHRIS_01 & S_CUSTCHRIS
		   $explodeGroup = explode('_',$row['GROUP_USER_ID']);

		   //MERANGKUM DATA UTK BOUNDARY GROUP, tetapi masih ada yg redudant
		   if($explodeGroup[0] == 'N')
		   {
		       $keyGroup   = $explodeGroup[2];
		       $group_desc = $this->language->_('Group').' ' . (int)$keyGroup;
		   }
		   else if($explodeGroup[0] == 'S')
		   {
		       $keyGroup = 'S';
		       $group_desc = $this->language->_('Special');
		   }

		   $groupArray[$row['BOUNDARY_ID']][]  = $group_desc;


		   //MERANGKUM DATA UTK GROUP USER
		   $groupUserArray[$keyGroup][] = $row['USER_FULLNAME'] . ' ('.$row['USER_ID'].')';

		}

		//men-distinct group agar tidak ada yang sama
		$groupArray_unique = array();
		foreach($groupArray as $key => $row)
		{
		   $groupArray_unique[$key] = array_unique($groupArray[$key]);
		}

	    //men-distinct group user agar tidak ada yang sama
		$groupUserArray_unique = array();
		foreach($groupUserArray as $key => $row)
		{
		   $groupUserArray_unique[$key] = array_unique($row);
		}

		$this->view->authorization_matrix = $authorizationMatrix;
        $this->view->boundary_group       = $groupArray_unique;
		$this->view->group_user           = $groupUserArray_unique;

		/*Zend_Debug::dump($groupUserArray_unique);
		die;*/

		//----------------- PDF ----------------------
	   

  		$this->view->pdf_flag  = 'yes';
		//--------------- END PDF -------------------

  		/*----------------------------------------------new approval matrix get data-----------------------------------*/

  		$cust_id = $this->_custIdLogin;
  		
  		 $selectUsergroup = $this->_db->select()
                  ->from(array('M_APP_GROUP_USER'),array('*'))
                  ->where('CUST_ID = ?', $cust_id)
                  ->group('GROUP_USER_ID')
                  // echo $selectUser;die;
                  ->query()->fetchall();

                $selectUsers = $this->_db->select()
                            ->from(array('M_APP_GROUP_USER'),array('*'))
                            ->where('CUST_ID = ?', $cust_id)
                            // echo $selectUser;die;
                            ->query()->fetchall();
                  // print_r($selectUser);die;
                  
                  $userlists = '';
                  foreach ($selectUsergroup as $key => $value) {
                      foreach ($selectUsers as $no => $val) {
                        if($val['GROUP_USER_ID'] == $value['GROUP_USER_ID']){
                        if(empty($userlists))
                          $userlists .= $val['USER_ID'];
                        else
                          $userlists .= ', '.$val['USER_ID'];
                      }
                          
                      }
                      $selectUsergroup[$key]['USER'] .= $userlists;
                      $userlists = '';
                      $spesials = 'S_'.$cust_id;
                      if($value['GROUP_USER_ID']==$spesials){
                          $selectUsergroup[$key]['GID'] .= 'SG';
                      }else{
                        $groups = explode('_', $value['GROUP_USER_ID']);
                      $alphabets = array(1=>'A',2=>'B',3=>'C',4=>'D',5=>'E',6=>'F',7=>'G',8=>'H',9=>'I',10=>'J',11=>'K',12=>'L',13=>'M',14=>'N',15=>'O',16=>'P',17=>'Q',18=>'R',19=>'S',20=>'T',21=>'U',22=>'V',23=>'W',24=>'X',25=>'Y',26=>'Z');
                      
                        // $cust = explode('_', $value['GROUP_USER_ID'])
                        $selectUsergroup[$key]['GID'] .= $alphabets[(int)$groups[2]];
                      }
                      
                  }
                  $this->view->selectUsergroup = $selectUsergroup;








              $select = $this->_db->select()
                          ->from(array('MAB'=>'M_APP_BOUNDARY'))
                          ->join(array('MABG'=>'M_APP_BOUNDARY_GROUP'), 'MAB.BOUNDARY_ID = MABG.BOUNDARY_ID', array('GROUP_USER_ID'))
                          ->where('MAB.CUST_ID = ?', (string)$cust_id)
                          ->group('BOUNDARY_ID');

                  $result = $this->_db->fetchAll($select);

                  $dataArr = array();
                  // $alf = 'A';
                  // $alf++;
                  // ++$alf;
                  // print_r((int)$alf);
                  // $nogroup = sprintf("%02d", 1);

                  foreach($result as $row){
                    list($grouptype,$groupname,$groupnum) = explode("_",$row['GROUP_USER_ID']);
                    if($grouptype == 'N')
                      $name = 'Group '.trim($groupnum,'0');
                    else
                      $name = 'Special Group';

                    $selectUser = $this->_db->select()
                                ->from(array('M_APP_GROUP_USER'),array('USER_ID'))
                                ->where('GROUP_USER_ID = ?', $row['GROUP_USER_ID'])
                                // echo $selectUser;die;
                                ->query()->fetchall();

                    $userlist = '';

                    // $policy = explode(' AND ', $row['POLICY']);



                    //  echo "<pre>";
                    // echo $row['POLICY'];
                    $command = str_replace('(', '', $row['POLICY']);
                    $command = str_replace(')', '', $command);
                    $list = explode(' ', $command);
                    // var_dump($list);

                    $alphabet = array('01'=>'A','02'=>'B','03'=>'C','04'=>'D','05'=>'E','06'=>'F','07'=>'G','08'=>'H','09'=>'I',10=>'J',11=>'K',12=>'L',13=>'M',14=>'N',15=>'O',16=>'P',17=>'Q',18=>'R',19=>'S',20=>'T',21=>'U',22=>'V',23=>'W',24=>'X',25=>'Y',26=>'Z',27=>'SG');

                    $flipAlphabet = array_flip($alphabet);

                    foreach ($list as $rows => $data) {
                      foreach ($alphabet as $key => $value) {
                        if ($data == $value ) {
                          $groupuser[] = $flipAlphabet[$data];
                        }
                      }
                    }
                    // var_dump($groupuser);
                    foreach ($groupuser as $key => $value) {

                      //if special group
                      if ($value == 27) {
                        $likecondition = "%S_%";
                      }
                      else{
                        $likecondition = "%_".$value."%";
                      }

                      $selectgroup = $this->_db->select()
                                ->from(array('C' => 'M_APP_GROUP_USER'), array(
                                       'USER_ID'
                                      ))
                      ->where("C.CUST_ID  = ?" , (string)$this->_custIdLogin)
                      ->where("C.GROUP_USER_ID LIKE ?", (string)$likecondition);

                      $group_user = $this->_db->fetchAll($selectgroup);

                      $groups[][$alphabet[$value]] = $group_user;
                    }
                    // if($row['POLICY'] == '(( A AND A ) OR B) THEN C'){
                      
                    // var_dump($groups);
                    // die;
                    // }
                   



                    $policy = array_unique($groupuser);

                // echo "<pre>";
                    // var_dump($policy);

                    foreach ($policy as $key => $value) {
                      if($value == 'SG'){
                        $group = 'S_'.$cust_id;
                        $selectUser = $this->_db->select()
                                ->from(array('M_APP_GROUP_USER'),array('USER_ID'))
                                ->where('GROUP_USER_ID = ?', $group)
                                // echo $selectUser;die;
                                ->query()->fetchall();  
                        foreach($selectUser as $val){
                          if(empty($userlist))
                            $userlist .= $val['USER_ID'];
                          else
                            $userlist .= ', '.$val['USER_ID'];
                        }                         
                      }else{



                        $alphabet = array("A"=>1,"B"=>2,"C"=>3,"D"=>4,"E"=>5,"F"=>6,"G"=>7,"H"=>8,"I"=>9,"J"=>10,"K"=>11,"L"=>12,"M"=>13,"N"=>14,"O"=>15,"P"=>16,"Q"=>17,"R"=>18,"S"=>19,"T"=>20,"U"=>21,"V"=>22,"W"=>23,"X"=>24,"Y"=>25,"Z"=>26,);

                        $policyor = explode(' OR ', $value);
                        // print_r($policyor);die;
                        // foreach ($policyor as $numb => $valpol) {
                          if($valpol == 'SG'){
                        $group = 'S_'.$cust_id;
                        $selectUser = $this->_db->select()
                                ->from(array('M_APP_GROUP_USER'),array('USER_ID'))
                                ->where('GROUP_USER_ID = ?', $group)
                                // echo $selectUser;die;
                                ->query()->fetchall();  
                        foreach($selectUser as $val){
                          if(empty($userlist)){
                            $userlist .= $val['USER_ID'];
                          }
                          else{
                            $userlist .= ', '.$val['USER_ID'];
                          }
                        }
                      }else{
                          // $nogroup = sprintf("%02d", $alphabet[$valpol]);
                          // print_r($valpol);
                          $group = 'N_'.$cust_id.'_'.$value;
                          $selectUser = $this->_db->select()
                                  ->from(array('M_APP_GROUP_USER'),array('USER_ID'))
                                  ->where('GROUP_USER_ID = ?', $group)
                                  // echo $selectUser;die;
                                  ->query()->fetchall();  
                                // print_r($selectUser);  
                          foreach($selectUser as $val){
                            if(empty($userlist))
                              $userlist .= $val['USER_ID'];
                            else
                              $userlist .= ', '.$val['USER_ID'];
                          }
                        }
                      // }
                        

                      }
                    }
                    // print_r($policy);die;
                    // $nogroup = sprintf("%02d", $key+1);

                    // foreach($selectUser as $val){
                    //  if(empty($userlist))
                    //    $userlist .= $val['USER_ID'];
                    //  else
                    //    $userlist .= ', '.$val['USER_ID'];
                    // }
                    // echo "<pre>";
                    // var_dump($row);
                    $arrTraType     = array_combine($this->_paymenttype["code"],$this->_paymenttype["desc"]);

                    $dataArr[] = array( 
                              'TRANSFER_TYPE' => $arrTraType[$row['TRANSFER_TYPE']],
                              'CURRENCY' => $row['CCY_BOUNDARY'],
                              'BOUNDARY' => Application_Helper_General::displayMoney($row['BOUNDARY_MIN'])." - ".Application_Helper_General::displayMoney($row['BOUNDARY_MAX']),
                              'GROUP_NAME' => $row['POLICY'],
                              'USERS' => $userlist);
                  }

                 /* print_r($row);die();*/

                  $this->view->dataBoundary = $dataArr;

			/*-----------------------------------------end new approval matrix get data-----------------------------------*/

      /*---------------------------------------------userlimit get data--------------------------------------*/

        $selectcomp = $this->_db->select()
                             ->from('M_CUSTOMER')
                             ->where('CUST_ID ='.$this->_db->quote((string)$cust_id))
                             ->query()->fetchAll();

        $this->view->compinfo = $selectcomp;



       $selectlimit = $this->_db->select()
                             ->from(array('A' => 'M_MAKERLIMIT'),array('USER_LOGIN','MAXLIMIT','MAKERLIMIT_STATUS'))
               ->joinLeft(array('B' => 'M_USER'),'A.USER_LOGIN = B.USER_ID AND A.CUST_ID = B.CUST_ID',array('USER_FULLNAME'))
               ->joinLeft(array('C' => 'M_CUSTOMER_ACCT'),'A.ACCT_NO = C.ACCT_NO',array('ACCT_NO','CCY_ID','ACCT_NAME')) // ADDED BY SUHANDI
               ->where('A.MAXLIMIT > 0')  
                   ->where('A.MAKERLIMIT_STATUS <> 3')                                                            
                   //->where('C.ACCT_STATUS <> 3')                                                            
                             ->where('UPPER(B.CUST_ID)='.$this->_db->quote((string)$cust_id))
                             ->where('UPPER(A.CUST_ID)='.$this->_db->quote((string)$cust_id))
                             ->query()->fetchAll();

        $this->view->status_type = $this->_masterglobalstatus;
        $this->view->userlimit = $selectlimit;

       /* print_r($selectlimit);die();*/


      /*---------------------------------------------end userlimit get data--------------------------------------*/

      /*---------------------------------------------get privilage data--------------------------------------*/

       $selectcust = $this->_db->select()
                             ->from('M_CUSTOMER')
                             ->joinLeft(array('A'=>'M_USER'),'M_CUSTOMER.CUST_ID = A.CUST_ID', array('TOKEN_ID'))
                             ->where('M_CUSTOMER.CUST_ID ='.$this->_db->quote((string)$this->_custIdLogin))
                             ->where('A.USER_ID ='.$this->_db->quote((string)$this->_userIdLogin))
                             ->query()->fetchAll();


        $this->view->custinfo = $selectcust;

      /*---------------------------------------------end privilage data--------------------------------------*/

       if($pdf)
        {
         $outputHTML = "<tr><td>".$this->view->render('others/pdf.phtml')."</td></tr>";
         // echo $outputHTML;die;
         $this->_helper->download->pdf(null,null,null,'Transaction Workflow',$outputHTML);
      }

		/*Zend_Debug::dump( $this->view->boundary_group);
		die;*/
  		Application_Helper_General::writeLog('DAGL',"Viewing Approval Matrix");
	}
}
