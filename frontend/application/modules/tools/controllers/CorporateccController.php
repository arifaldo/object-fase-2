<?php

require_once 'Zend/Controller/Action.php';


class tools_CorporateccController extends Application_Main {

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

		$setting = new Settings();			  	
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
		$pw_hash = md5($enc_salt.$enc_pass);
		$rand = $this->_userIdLogin.date('dHis').$pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;


		$fields = array	(	'card'  					=> array	(
																		'field' => 'card',
																		'label' => $this->language->_('Card'),
																		'sortable' => true
																),
							'valid'  					=> array	(
																		'field' => 'valid',
																		'label' => $this->language->_('Valid'),
																		'sortable' => true
																	),
							'status'  					=> array	(
																		'field' => 'status',
																		'label' => $this->language->_('Status'),
																		'sortable' => true
																	),	
							'ccy'  		=> array('field' 	=> 'ccy',
												 'label' 	=> $this->language->_('CCY'),
												 'sortable' => true),

							'creditlimit'  		=> array('field' 	=> 'creditlimit',
												 'label' 	=> $this->language->_('Credit Limit'),
												 'sortable' => true),

							'withdraw'  		=> array('field' 	=> 'withdraw',
												 'label' 	=> $this->language->_('Cash Limit Withdraw'),
												 'sortable' => true),

							'outstanding' 					 => array	(
																		'field' => 'outstanding',
																		'label' => $this->language->_('Outstanding Limit'),
																		'sortable' => true
																	),
							'amount'  			=> array('field' 	=> 'amount',
												 'label' 	=> $this->language->_('Amount'),
												 'sortable' => true),
							
							'remainingcredit'  					=> array	(
																		'field' => 'remainingcredit',
																		'label' => $this->language->_('Remaining Credit'),
																		'sortable' => true
																	),
							'remainingcash'  					=> array	(
																		'field' => 'remainingcash',
																		'label' => $this->language->_('Remaining Cash Withdraw'),
																		'sortable' => true
																	),
							'totaldebt'  					=> array	(
																		'field'	=> 'totaldebt',
																		'label' 	=> $this->language->_('Total Debt'),
																		'sortable' => true
																	),
						);

		$filterlist = array('PS_VALID',"CARD",'STATUS','WDR_LIMIT');
		
		$this->view->filterlist = $filterlist;

		unset($fields['card']);
		$this->view->fields = $fields;

		if($this->_request->getParam('print') == 1){
			$this->_forward('print', 'index', 'widget', array('data_content' => '', 'data_caption' => 'Payment Report', 'data_header' => $fields));
		}
	}

	public function detailAction()
	{
		$this->_helper->layout()->setLayout('newpopup');

		$card_number = $this->_getParam('card_number');

		$this->view->card_number = $card_number;
	}

	public function addAction()
	{
		$this->_helper->layout()->setLayout('newlayout');

	}
}
