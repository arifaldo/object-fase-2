<?php

require_once 'Zend/Controller/Action.php';
require_once 'Crypt/AESMYSQL.php';


class tools_PgpkeymanagementController extends Application_Main {
	
	public function initController(){
		$this->_destinationUploadDir = UPLOAD_PATH . '/pgpkey/';
		$this->_listCCYValidate = Application_Helper_Array::simpleArray($this->getCcy(),'CCY_ID');  	
	}
	
	public function indexAction() 
	{
		
		$this->_helper->layout()->setLayout('newlayout');
		
		$this->view->params = $this->_request->getParams();
		
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1)
		{
      		if($temp[0]=='F' || $temp[0]=='S')
			{
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
		
		$arr = null;
		$viewFilter = null;
		
		$select = $this->_db->select()->distinct()
			->from(array('A' => 'M_USER'),
				array('USER_ID', 'USER_FULLNAME', 'CUST_ID'))
				->where("CUST_ID LIKE ".$this->_db->quote($this->_custIdLogin))
				->order('USER_FULLNAME ASC')
				 ->query() ->fetchAll();
				 
       	$userlist = array(""=>'-- '.$this->language->_('Any Value') .'--');
		$userlist +=Application_Helper_Array::listArray($select,'USER_ID','USER_FULLNAME');
		$this->view->listCustId = $userlist;
		
		$fields = array	(
							'FileName'  			=> array	(
																	'field' => 'FILE_NAME_PUB',
																	'label' => $this->language->_('File Public Key'),
																	'sortable' => true
																),
							'Filepassphrase'  		=> array	(
																	'field' => 'FILE_NAME_PRIV',
																	'label' => $this->language->_('File Private Key'),
																	'sortable' => true
																),
							'Uploaded By'  			=> array	(
																	'field' => 'USER_FULLNAME',
																	'label' => $this->language->_('Uploaded By'),
																	'sortable' => true
																),	
							'Upload Date and Time'  => array	(
																	'field' => 'FILE_UPLOADED_TIME',
																	'label' => $this->language->_('Uploaded Date and Time'),
																	'sortable' => true
																),
							'delete'   				=> array	(
																	'field'    => 'BENEFICIARY_ISREQUEST_DELETE',
																	'label'    => $this->language->_('Delete'),
																	'sortable' => false
																)
						);
		$this->view->fields = $fields;
		
		$page    = $this->_getParam('page');		
        $sortBy  = $this->_getParam('sortby','');
        $sortDir = $this->_getParam('sortdir','asc');
		
        $page 		= (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
        $sortBy 	= (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir 	= (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';
				
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$filterArr = array(	'filter' 	  	=> array('StringTrim','StripTags'),
							'SEARCH_TEXT'   	=> array('StringTrim','StripTags','StringToUpper'),
							'UPDATED_BY' 	  	=> array('StringTrim','StripTags'),
							//'UPDATE_START' 	  	=> array('StringTrim','StripTags'),
							//'UPDATE_END' 	  	=> array('StringTrim','StripTags'),
		);
		
		$zf_filter 	= new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		
		$filter 	= $zf_filter->getEscaped('filter');

		$select = $this->_db->select()
							->distinct()
							->from(	
									array('TFS'=>'T_FILE_PGPKEY'),
									array(
											'FILE_ID'			=>'TFS.FILE_ID',
											'FILE_NAME_PUB'		=>'TFS.FILE_NAME_PUB',
											'FILE_NAME_PRIV'	=>'TFS.FILE_NAME_PRIV',
											'FILE_PASSPHRASE'	=>'TFS.FILE_PASSPHRASE',
											'FILE_UPLOADED_TIME'=>'TFS.FILE_UPLOADED_TIME',
											'FILE_SYSNAME_PUB'	=>'TFS.FILE_SYSNAME_PUB',
											'FILE_SYSNAME_PRIV'	=>'TFS.FILE_SYSNAME_PRIV',
											'USER_LOGIN'		=>'TFS.USER_LOGIN',
											'USER_FULLNAME'		=>'MU.USER_FULLNAME',
											'FILE_DOWNLOADED' 	=>'TFS.FILE_DOWNLOADED',
										))
							->join	(
											array('MU' => 'M_USER'),
											'MU.USER_ID = TFS.USER_LOGIN',
											array()
										)
							->where('MU.CUST_ID =?',$this->_custIdLogin)
							->where('TFS.CUST_ID =?',$this->_custIdLogin);

		$FILE_ID   	= $this->_getParam('FILE_ID');
		$KEY   		= $this->_getParam('KEY');
		if($FILE_ID && $KEY)
		{
			
			$select->where('FILE_ID =?',$FILE_ID);
			$data = $this->_db->fetchRow($select);
			
			$attahmentDestination = $this->_destinationUploadDir; //UPLOAD_PATH . '/document/pgpkey/';
			if ($KEY == "PUB"){
				$this->_helper->download->file($data['FILE_NAME_PUB'],$attahmentDestination.$data['FILE_SYSNAME_PUB']);
			}
			else{
				$this->_helper->download->file($data['FILE_NAME_PRIV'],$attahmentDestination.$data['FILE_SYSNAME_PRIV']);
			}
						
			$updateArr = array();
			$updateArr['FILE_DOWNLOADED'] = $data['FILE_DOWNLOADED']+1;
			$updateArr['FILE_DOWNLOADEDBY'] = $this->_userIdLogin;
			
			$whereArr = array('FILE_ID = ?'=>$FILE_ID);
			
			$fileupdate = $this->_db->update('T_FILE_PGPKEY',$updateArr,$whereArr);
			//echo "file downloaded: ".$data['FILE_DOWNLOADED'];
			Application_Helper_General::writeLog('VDEL','Download File Sharing report '.$data['FILE_NAME_PUB']);
		}
		else{
			Application_Helper_General::writeLog('VDEL','View File Sharing report');
		}
		
		if($filter == null)
		{	
			$DATE_START = NULL;//(date("d/m/Y"));
			$DATE_END = NULL;//(date("d/m/Y"));
			$this->view->DATE_START  = $DATE_START;
			$this->view->DATE_END  = $DATE_END;
		//Zend_Debug::dump($this->view->fDateFrom); die;
		}
		
		if($filter == TRUE)
		{
			$SEARCH_TEXT   	= $this->_getParam('SEARCH_TEXT');
			$UPLOADED_BY    = $this->_getParam('UPLOADED_BY');
			//$DATE_START    	= $this->_getParam('DATE_START');
			//$DATE_END		= $this->_getParam('DATE_END');
			
			if($SEARCH_TEXT)
			{
				$select->where("UPPER(FILE_NAME_PUB) LIKE ".$this->_db->quote('%'.$SEARCH_TEXT.'%')." || UPPER(FILE_NAME_PRIV) LIKE ".$this->_db->quote('%'.$SEARCH_TEXT.'%'));
				$this->view->SEARCH_TEXT = $SEARCH_TEXT;
			}
			
			if($UPLOADED_BY)
			{
				$select->where("UPPER(MU.USER_ID) = ".$this->_db->quote($UPLOADED_BY));
				$this->view->UPLOADED_BY = $UPLOADED_BY;
			}
		}
		
		if($filter == null || $filter == TRUE)
		{
			if($DATE_START)
			{
				$FormatDate 	= new Zend_Date($DATE_START, $this->_dateDisplayFormat);
				$from   	= $FormatDate->toString($this->_dateDBFormat);	
				if($from)
				{
					$select->where('DATE(FILE_UPLOADED_TIME) >= '.$this->_db->quote($from));
					
				}
				$this->view->DATE_START = $DATE_START;
			}
			
			if($DATE_END)
			{
				$FormatDate 	= new Zend_Date($DATE_END, $this->_dateDisplayFormat);
				$to   	= $FormatDate->toString($this->_dateDBFormat);
				if($to)
				{
					$select->where('DATE(FILE_UPLOADED_TIME) <= '.$this->_db->quote($to));
					
				}
				$this->view->DATE_END = $DATE_END;
			}
		}
//		if(!$this->_request->isPost()){
//		Application_Helper_General::writeLog('VDEL','View File Sharing report');
//		}
		$select->order($sortBy.' '.$sortDir);
		// echo $select;die;
		//DIE($select);
		//$arr = $this->_db->fetchAll($select);
		$this->paging($select);
	}
	
	public function addpubkeyAction() 
	{
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
		
		$maxFileSize = "1024"; //"1024000"; //1024
		//~ $fileExt = "csv,dbf";
		$fileExt = "key";
		
		if($this->_request->isPost())
		{
			$attahmentDestination 	= $this->_destinationUploadDir;//UPLOAD_PATH . '/document/submit/';
			$errorRemark 			= null;
			$adapter 				= new Zend_File_Transfer_Adapter_Http();
			
			$params = $this->_request->getParams();
			$sourceFileName = $adapter->getFileName(); 
			if($sourceFileName == null){
			
				$sourceFileName = null;
				$fileType = null;
			}
			else
			{
			
				$sourceFileName = substr(basename($adapter->getFileName()), 0);
				if($_FILES["document"]["type"])
				{
					$adapter->setDestination($attahmentDestination);
					$fileType 				= $adapter->getMimeType();
					$size 					= $_FILES["document"]["size"];
				}
				else{
					$fileType = null;
					$size = null;
				}
			}
			$paramsName['sourceFileName'] 	= $sourceFileName;
			
			$helpTopic 	= $this->_getParam('helpTopic');
			if($helpTopic == null)	$helpTopic = null;
			
			$paramsName['helpTopic']  	= $helpTopic;
			$passphrase 				= $this->_getParam('passphrase');
			$paramsName['passphrase']  = $passphrase;

			$filtersName = array(
									'sourceFileName'	=> array('StringTrim'),
									'passphrase'		=> array('StringTrim'),
								);
			
			$validatorsName = array(
									'sourceFileName' => array(
															'NotEmpty',
															'messages' => array(
																					$this->language->_('Error : File cannot be left blank')
																				)
														),
									'passphrase' => array(
															'NotEmpty',
															'messages' => array(
																					$this->language->_('Error: passphrase cannot be left blank')
																				)
														),
								);
			$zf_filter_input_name = new Zend_Filter_Input($filtersName, $validatorsName, $paramsName, $this->_optionsValidator);
			
			if($sourceFileName == NULL){
				
				$this->view->error 				= true;
				$errors 						= $zf_filter_input_name->getMessages();
				$this->view->sourceFileNameErr 	= (isset($errors['sourceFileName']))? $errors['sourceFileName'] : null;
				
			}
			elseif($passphrase == NULL){
				
			
				$this->view->error 				= true;
				$errors 						= $zf_filter_input_name->getMessages();
				$this->view->passphraseErr 	= (isset($errors['passphrase']))? $errors['passphrase'] : null;
				
			}
			else{
			
				if($zf_filter_input_name->isValid())
				{
					$fileTypeMessage = explode('/',$fileType);
					if (isset($fileTypeMessage[1])){
						$fileType =  $fileTypeMessage[1];
						$extensionValidator = new Zend_Validate_File_Extension(array(false, $fileExt));
						$extensionValidator->setMessage("Extension file must be *.key");
						
						$size 			= number_format($size);
						$sizeTampil 	= $size/1000;
						//$sizeValidator 	= new Zend_Validate_File_Size(array('min' => 1, 'max' => $maxFileSize));
						// $sizeValidator->setMessage("File size is exceeding 1024 Kb, uploaded file is $sizeTampil byte(s)");
							
						$adapter->setValidators(array($extensionValidator));
							
						if($adapter->isValid())
						{
							$date = date("dmy");
							$time = date("his");
						
							$newFileName = $date."_".$time."_".$this->_custIdLogin."_". $sourceFileName;
							$adapter->addFilter('Rename', $newFileName);
						
							if($adapter->receive())
							{
								try
								{
									$this->_db->beginTransaction();
									$insertArr = array(
											'CUST_ID'				=> $this->_custIdLogin,
											'USER_LOGIN'			=> $this->_userIdLogin,
											'FILE_NAME' 			=> $sourceFileName,
											'FILE_PASSPHRASE'		=> $passphrase,
											'FILE_UPLOADED_TIME' 	=> new Zend_Db_Expr('now()'),
											'FILE_DOWNLOADED'		=> '0',
											//'FILE_DELETED'			=> '0',
											'FILE_DOWNLOADEDBY'		=> '',
											//'FILE_DELETEDBY'		=> '',
											//'FILE_ISEMAILED'		=> '0',
											'FILE_SYSNAME'			=> $newFileName,
											'FILE_PUBLIC'			=> '1',
									);
						
									$this->_db->insert('T_FILE_PGPKEY', $insertArr);
									$this->_db->commit();
						
									Application_Helper_General::writeLog('DELI',"Successfully upload ".$newFileName);
						
									$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
									$this->_redirect('/notification/success');
								}
								catch(Exception $e)
								{
									$this->_db->rollBack();
									Application_Log_GeneralLog::technicalLog($e);
								}
							}
						}
						else
						{
							$this->view->error = true;
							$errors = array($adapter->getMessages());
							$this->view->errorMsg = $errors;
						}
					}else{
						$this->view->error = true;
						$errors = array(array('FileType'=>'Error: Unknown File Type'));
						$this->view->errorMsg = $errors;
					}
				}
			}
		}
		
		//$FILE_passphrase_len = (isset($passphrase))? strlen($passphrase): 0;
		//$FILE_passphrase_len = 128 - $FILE_passphrase_len;
		
		//$TRA_REFNO_len 	 = 200 - $TRA_REFNO_len;
		
		//$this->view->FILE_passphrase_len		= $FILE_passphrase_len;
		
		
		$this->view->max_file_size = $maxFileSize; //'1024';
		$this->view->file_extension = $fileExt;
		Application_Helper_General::writeLog('DELI',"Viewing File Sharing");
	}
	
	public function addprivkeyAction() 
	{
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
		
		$maxFileSize = "1024"; //"1024000"; //1024
		//~ $fileExt = "csv,dbf";
		$fileExt = "key";
		
		if($this->_request->isPost())
		{
			$attahmentDestination 	= $this->_destinationUploadDir;//UPLOAD_PATH . '/document/submit/';
			$errorRemark 			= null;
			$adapter 				= new Zend_File_Transfer_Adapter_Http();
			
			$params = $this->_request->getParams();
			$sourceFileName = $adapter->getFileName(); 
			if($sourceFileName == null){
			
				$sourceFileName = null;
				$fileType = null;
			}
			else
			{
			
				$sourceFileName = substr(basename($adapter->getFileName()), 0);
				if($_FILES["document"]["type"])
				{
					$adapter->setDestination($attahmentDestination);
					$fileType 				= $adapter->getMimeType();
					$size 					= $_FILES["document"]["size"];
				}
				else{
					$fileType = null;
					$size = null;
				}
			}
			$paramsName['sourceFileName'] 	= $sourceFileName;
			
			$helpTopic 	= $this->_getParam('helpTopic');
			if($helpTopic == null)	$helpTopic = null;
			
			$paramsName['helpTopic']  	= $helpTopic;
			$description 				= $this->_getParam('description');
			$paramsName['description']  = $description;

			$filtersName = array(
									'sourceFileName'	=> array('StringTrim'),
									'description'		=> array('StringTrim'),
								);
			
			$validatorsName = array(
									'sourceFileName' => array(
															'NotEmpty',
															'messages' => array(
																					$this->language->_('Error : File cannot be left blank')
																				)
														),
									'description' => array(
															'NotEmpty',
															'messages' => array(
																					$this->language->_('Error: description cannot be left blank')
																				)
														),
								);
			$zf_filter_input_name = new Zend_Filter_Input($filtersName, $validatorsName, $paramsName, $this->_optionsValidator);
			
			if($sourceFileName == NULL){
				
				$this->view->error 				= true;
				$errors 						= $zf_filter_input_name->getMessages();
				$this->view->sourceFileNameErr 	= (isset($errors['sourceFileName']))? $errors['sourceFileName'] : null;
				
			}
			elseif($description == NULL){
				
			
				$this->view->error 				= true;
				$errors 						= $zf_filter_input_name->getMessages();
				$this->view->descriptionErr 	= (isset($errors['description']))? $errors['description'] : null;
				
			}
			else{
			
				if($zf_filter_input_name->isValid())
				{
					$fileTypeMessage = explode('/',$fileType);
					if (isset($fileTypeMessage[1])){
						$fileType =  $fileTypeMessage[1];
						$extensionValidator = new Zend_Validate_File_Extension(array(false, $fileExt));
						$extensionValidator->setMessage("Extension file must be *.key");
						
						$size 			= number_format($size);
						$sizeTampil 	= $size/1000;
						//$sizeValidator 	= new Zend_Validate_File_Size(array('min' => 1, 'max' => $maxFileSize));
						// $sizeValidator->setMessage("File size is exceeding 1024 Kb, uploaded file is $sizeTampil byte(s)");
							
						$adapter->setValidators(array($extensionValidator));
							
						if($adapter->isValid())
						{
							$date = date("dmy");
							$time = date("his");
						
							$newFileName = $date."_".$time."_".$this->_custIdLogin."_". $sourceFileName;
							$adapter->addFilter('Rename', $newFileName);
						
							if($adapter->receive())
							{
								try
								{
									$this->_db->beginTransaction();
									$insertArr = array(
											'CUST_ID'				=> $this->_custIdLogin,
											'USER_LOGIN'			=> $this->_userIdLogin,
											'FILE_NAME_PRIV' 		=> $sourceFileName,
											'FILE_PASSPHRASE'		=> $description,
											'FILE_UPLOADED_TIME' 	=> new Zend_Db_Expr('now()'),
											'FILE_DOWNLOADED'		=> '0',
											//'FILE_DELETED'		=> '0',
											'FILE_DOWNLOADEDBY'		=> '',
											//'FILE_DELETEDBY'		=> '',
											//'FILE_ISEMAILED'		=> '0',
											'FILE_SYSNAME_PRIV'		=> $newFileName,
											//'FILE_PRIVATE'		=> '1',
									);
						
									$this->_db->insert('T_FILE_PGPKEY', $insertArr);
									$this->_db->commit();
						
									Application_Helper_General::writeLog('DELI',"Successfully upload ".$newFileName);
						
									$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
									$this->_redirect('/notification/success');
								}
								catch(Exception $e)
								{
									$this->_db->rollBack();
									Application_Log_GeneralLog::technicalLog($e);
								}
							}
						}
						else
						{
							$this->view->error = true;
							$errors = array($adapter->getMessages());
							$this->view->errorMsg = $errors;
						}
					}else{
						$this->view->error = true;
						$errors = array(array('FileType'=>'Error: Unknown File Type'));
						$this->view->errorMsg = $errors;
					}
				}
			}
		}
		
		//$FILE_passphrase_len = (isset($passphrase))? strlen($passphrase): 0;
		//$FILE_passphrase_len = 128 - $FILE_passphrase_len;
		
		//$TRA_REFNO_len 	 = 200 - $TRA_REFNO_len;
		
		//$this->view->FILE_passphrase_len		= $FILE_passphrase_len;
		
		
		$this->view->max_file_size = $maxFileSize; //'1024';
		$this->view->file_extension = $fileExt;
		Application_Helper_General::writeLog('DELI',"Viewing File Sharing");
	}
	
	public function deleteAction()
	{		
		
			$file_id = $this->_getParam('file_id');
			$file_id = (Zend_Validate::is($file_id,'Digits'))? $file_id : null;
			if($file_id)
			{
				
					$select = $this->_db->select()
							->distinct()
							->from(	
									array('TFS'=>'T_FILE_PGPKEY'),
									array(
											'FILE_ID'			=>'TFS.FILE_ID',
											'FILE_SYSNAME_PUB'		=>'TFS.FILE_SYSNAME_PUB',
											'FILE_SYSNAME_PRIV'	=>'TFS.FILE_SYSNAME_PRIV',											
										))
							->join	(
											array('MU' => 'M_USER'),
											'MU.USER_ID = TFS.USER_LOGIN',
											array()
										)
							->where('MU.CUST_ID =?',$this->_custIdLogin)
							->where('TFS.CUST_ID =?',$this->_custIdLogin);
					$select->where('FILE_ID =?',$file_id);
					$data = $this->_db->fetchRow($select);
					
					$attahmentDestination = $this->_destinationUploadDir;
					
					$fileKeyPub = $attahmentDestination.$data['FILE_SYSNAME_PUB'];
					
					@unlink($fileKeyPub);
					
					$fileKeyPriv = $attahmentDestination.$data['FILE_SYSNAME_PRIV'];
					@unlink($fileKeyPriv);
												
					try 
					{
						//-----delete--------------
						$this->_db->beginTransaction();
						
						$where['FILE_ID = ?'] = $file_id;
						$this->_db->delete('T_FILE_PGPKEY',$where);
						
						$this->_db->commit();
						
						//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), null);
						$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
						$this->_redirect('/notification/success');
					}
					catch(Exception $e) 
					{
						//rollback changes
						$this->_db->rollBack();
						$this->fillParam($zf_filter_input);
						$errorMsg = $this->getErrorRemark('82');
						$this->_helper->getHelper('FlashMessenger')->addMessage('F');
						$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
						
						//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $errorMsg);
						//Application_Log_GeneralLog::technicalLog($e);
					}
				
			}
			else
			{
			   $error_remark = 'Error: Beneficiary ID cannot be left blank.';
			
			   $this->_helper->getHelper('FlashMessenger')->addMessage('F');
			   $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
			}
		
	}
	
	public function addallkeyAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
		
    	$select = $this->_db->select()
							->distinct()
							->from(	
									array('TFS'=>'T_FILE_PGPKEY'),
									array(
											'FILE_ID'			=>'TFS.FILE_ID',
											'FILE_NAME_PUB'		=>'TFS.FILE_NAME_PUB',
											'FILE_NAME_PRIV'	=>'TFS.FILE_NAME_PRIV',
											'FILE_PASSPHRASE'	=>'TFS.FILE_PASSPHRASE',
											'FILE_UPLOADED_TIME'=>'TFS.FILE_UPLOADED_TIME',
											'FILE_SYSNAME_PUB'	=>'TFS.FILE_SYSNAME_PUB',
											'FILE_SYSNAME_PRIV'	=>'TFS.FILE_SYSNAME_PRIV',
											'USER_LOGIN'		=>'TFS.USER_LOGIN',
											'USER_FULLNAME'		=>'MU.USER_FULLNAME',
											'FILE_DOWNLOADED' 	=>'TFS.FILE_DOWNLOADED',
										))
							->join	(
											array('MU' => 'M_USER'),
											'MU.USER_ID = TFS.USER_LOGIN',
											array()
										)
							->where('MU.CUST_ID =?',$this->_custIdLogin)
							->where('TFS.CUST_ID =?',$this->_custIdLogin);
		$data = $this->_db->fetchRow($select);
		// echo $select;
		// print_r($data);die;

		if(!empty($data)){
			// print_r($data['FILE_NAME_PUB']);die;
			$this->view->pubkey = $data['FILE_NAME_PUB'];
			$this->view->privkey = $data['FILE_NAME_PRIV'];
			$this->view->dateupload = Application_Helper_General::convertDate($data['FILE_UPLOADED_TIME'],$this->displayDateTimeFormat,$this->defaultDateFormat);
			$this->view->dateuploadby = $data['USER_LOGIN'];
		}

		// $params = $this->_request->getParams();
		// print_r($params);die;

		$downloadfile = $this->_getParam('filter');
		$fileid = $this->_getParam('fileid');
		if($downloadfile == 'download')
		{
			// print_r($data);die;
				// $this->_destinationUploadDir = 
			$attahmentDestination = UPLOAD_PATH . '/pgpkey/';
			// echo($attahmentDestination);die;
			if($fileid == 'pub'){
				if (file_exists($attahmentDestination.$data['FILE_SYSNAME_PUB']))
				{	$this->_helper->download->file($data['FILE_NAME_PUB'],$attahmentDestination.$data['FILE_SYSNAME_PUB']);	
					Application_Helper_General::writeLog('HLDL','Download Help '.$data['FILE_NAME_PUB']);
				}
				else
				{	echo "<script type='text/javascript'>alert('File Not Found');</script>";	
					
				}

			}else if($fileid == 'priv'){
				if (file_exists($attahmentDestination.$data['FILE_SYSNAME_PRIV']))
				{	$this->_helper->download->file($data['FILE_NAME_PRIV'],$attahmentDestination.$data['FILE_SYSNAME_PRIV']);	
					Application_Helper_General::writeLog('HLDL','Download Help '.$data['FILE_NAME_PRIV']);
				}
				else
				{	echo "<script type='text/javascript'>alert('File Not Found');</script>";	
					
				}

			}else{
				echo "<script type='text/javascript'>alert('File Not Found');</script>";	
			}

			
		}


		$maxFileSize = "1024"; //"1024000"; //1024
		//~ $fileExt = "csv,dbf";
		$fileExt = "key";
		
		if($this->_request->isPost())
		{
			$attahmentDestination 	= $this->_destinationUploadDir; //UPLOAD_PATH . '/pgpkey/';
			$errorRemark 			= null;
			$adapter 				= new Zend_File_Transfer_Adapter_Http();
						
			$params = $this->_request->getParams();
			$sourceFileName = $adapter->getFileName();			
			$countfile =  count($sourceFileName);			
			$files  = $adapter->getFileInfo();
			
						
			if ($countfile == 2){
				$sourceFileNamePub = $sourceFileName['pubkeyfile'];
				$sourceFileNamePriv = $sourceFileName['privkeyfile'];
			}elseif ($countfile == 1){
				if (!empty($files['pubkeyfile']['name'])){
					$sourceFileNamePub = $sourceFileName;
				}elseif (!empty($files['privkeyfile']['name'])){
					$sourceFileNamePriv = $sourceFileName;
				}
			}			
			
			if($sourceFileNamePub == null && $sourceFileNamePriv == null){
				$sourceFileNamePub = null;
				$sourceFileNamePriv = null;
				$fileTypePub = null;
			}
			elseif($sourceFileNamePub == null){			
				$sourceFileNamePub = null;
				$fileTypePub = null;
			}
			elseif($sourceFileNamePriv == null){
				$sourceFileNamePriv = null;
				$fileTypePriv = null;
			}
			else
			{
				
				$sourceFileNamePub 	= substr(basename($sourceFileNamePub), 0);
				$sourceFileNamePriv = substr(basename($sourceFileNamePriv), 0);
				
								
				if($_FILES["pubkeyfile"]["type"])
				{
					
					$adapter->setDestination($attahmentDestination);
					$fileType 				= $adapter->getMimeType();
					
					$fileTypePub 			= $fileType['pubkeyfile'];
					$fileTypePriv 			= $fileType['privkeyfile'];
					$sizePub 				= $_FILES["pubkeyfile"]["size"];
					$sizePriv 				= $_FILES["privkeyfile"]["size"];
				}
				else{
					$fileTypePub = null;
					$sizePub = null;
					$fileTypePriv = null;
					$sizePriv = null;
				}
			}

			$paramsName['sourceFileNamePub'] 	= $sourceFileNamePub;
			$paramsName['sourceFileNamePriv'] 	= $sourceFileNamePriv;
			
			
			$passphrase 				= $this->_getParam('passphrase');
			$paramsName['passphrase']  	= $passphrase;

			$filtersName = array(
									'sourceFileNamePub'	=> array('StringTrim'),
									'sourceFileNamePriv'	=> array('StringTrim'),
									'passphrase'	=> array('StringTrim'),
								);
			
			$validatorsName = array(
									'sourceFileNamePub' => array(
															'NotEmpty',
															'messages' => array(
																					$this->language->_('Error : Public Key File cannot be left blank')
																				)
														),
									'sourceFileNamePriv' => array(
															'NotEmpty',
															'messages' => array(
																					$this->language->_('Error : Private Key File cannot be left blank')
																				)
														),
									'passphrase' => array(
															
															'NotEmpty',
															array('StringLength',array('min'=>8,'max'=>9999)),
															'messages' => array(
																					$this->language->_('Error: passphrase cannot be left blank'),
																					$this->language->_('Error: Passphrase length cannot be less than 8'),																					
																				)
														),
								);
			$zf_filter_input_name = new Zend_Filter_Input($filtersName, $validatorsName, $paramsName, $this->_optionsValidator);
			
			
			if($sourceFileNamePub == NULL || $sourceFileNamePriv == NULL){
				
				$this->view->error 				= true;
				$errors 						= $zf_filter_input_name->getMessages();				
				$this->view->sourceFileNameErr 	= (isset($errors['sourceFileNamePub']))? $errors['sourceFileNamePub'] : $errors['sourceFileNamePriv'];
				//$this->view->sourceFileNamePrivErr 	= (isset($errors['sourceFileNamePriv']))? $errors['sourceFileNamePriv'] : null;
				
			}			
			elseif($passphrase == NULL){
				
				$this->view->error 				= true;
				$errors 						= $zf_filter_input_name->getMessages();				
				$this->view->passphraseErr 	= (isset($errors['passphrase']))? $errors['passphrase'] : null;
				
			}
			elseif(strlen($passphrase) < 8){
				$this->view->error 				= true;
				$errors 						= $zf_filter_input_name->getMessages();
				
				$this->view->passphraseErr 	= (isset($errors['passphrase']))? $errors['passphrase'] : null;
			}
			else{

			
				if($zf_filter_input_name->isValid())
				{
					
					//$password = $this->_getParam('password');
					$userId = $this->_userIdLogin;
					$custId = $this->_custIdLogin;

					$key = md5((string)$userId.(string)$custId);
					
					$encrypted_passphrase = Crypt_AESMYSQL::encrypt($passphrase, $key);
					$encryptedNewPassphrase = md5($encrypted_passphrase);
					
					$fileTypeMessagePub = explode('/',$fileTypePub);
					$fileTypeMessagePriv = explode('/',$fileTypePriv);
					
					if (isset($fileTypeMessagePub[1]) || isset($fileTypeMessagePriv[1])){
						
						$fileTypePub =  $fileTypeMessagePub[1];
						$extensionValidator = new Zend_Validate_File_Extension(array(false, $fileExt));
						$extensionValidator->setMessage("Extension file must be *.key");
						
						$sizePub 			= number_format($sizePub);
						$sizeTampil 		= $sizePub/1000;
						//$sizeValidator 	= new Zend_Validate_File_Size(array('min' => 1, 'max' => $maxFileSize));
						// $sizeValidator->setMessage("File size is exceeding 1024 Kb, uploaded file is $sizeTampil byte(s)");
							
						$adapter->setValidators(array($extensionValidator));
							
						if($adapter->isValid())
						{
														
							$date = date("dmy");
							$time = date("his");
						
							$newFileNamePub = $date."_".$time."_".$this->_custIdLogin."_". $sourceFileNamePub;
							$newFileNamePriv = $date."_".$time."_".$this->_custIdLogin."_". $sourceFileNamePriv;
							
							//$adapter->addFilter('Rename', $newFileNamePub);
							//$adapter->addFilter('Rename', $newFileNamePriv);
							
						
							if($adapter->receive())
							{
								rename($this->_destinationUploadDir.$sourceFileNamePub, $this->_destinationUploadDir.$newFileNamePub);
								rename($this->_destinationUploadDir.$sourceFileNamePriv, $this->_destinationUploadDir.$newFileNamePriv);

								try
								{
									$this->_db->beginTransaction();
									$insertArr = array(
											'CUST_ID'				=> $this->_custIdLogin,
											'USER_LOGIN'			=> $this->_userIdLogin,
											'FILE_NAME_PUB' 		=> $sourceFileNamePub,
											'FILE_NAME_PRIV' 		=> $sourceFileNamePriv,
											'FILE_PASSPHRASE'		=> $encryptedNewPassphrase,
											'FILE_UPLOADED_TIME' 	=> new Zend_Db_Expr('now()'),
											'FILE_DOWNLOADED'		=> '0',
											//'FILE_DELETED'			=> '0',
											'FILE_DOWNLOADEDBY'		=> '',
											//'FILE_DELETEDBY'		=> '',
											//'FILE_ISEMAILED'		=> '0',
											'FILE_SYSNAME_PUB'		=> $newFileNamePub,
											'FILE_SYSNAME_PRIV'		=> $newFileNamePriv,
									);
						
									$this->_db->insert('T_FILE_PGPKEY', $insertArr);
									$this->_db->commit();
									Application_Helper_General::writeLog('DELI',"Successfully upload ".$newFileName);
									
									$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/addallkey/');
									$this->_redirect('/notification/success');
																
									
								}
								catch(Exception $e)
								{
									$this->_db->rollBack();
									Application_Log_GeneralLog::technicalLog($e);
								}
							}
							
						}
						else
						{
							$this->view->error = true;
							$errors = array($adapter->getMessages());
							$this->view->errorMsg = $errors;
						}
					}else{
						$this->view->error = true;
						$errors = array(array('FileType'=>'Error: Unknown File Type'));
						$this->view->errorMsg = $errors;
					}
				}
			}
		}
		
		//$FILE_passphrase_len = (isset($passphrase))? strlen($passphrase): 0;
		//$FILE_passphrase_len = 128 - $FILE_passphrase_len;
		
		
		//$this->view->FILE_passphrase_len		= $FILE_passphrase_len;
		
		
		$this->view->max_file_size = $maxFileSize; //'1024';
		$this->view->file_extension = $fileExt;
		Application_Helper_General::writeLog('DELI',"Viewing File Sharing");
	}
}

