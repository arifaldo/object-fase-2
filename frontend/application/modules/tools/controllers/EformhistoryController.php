<?php


require_once 'CMD/Payment.php';
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'SGO/Helper/AES.php';
require_once 'General/Settings.php';
require_once 'Crypt/AESMYSQL.php';

class tools_EformhistoryController extends Application_Main
{

	public function initController()
	{
	}

	public function indexAction()
	{
		$paymentRef = $this->_getParam('bgnumb');

		//$Payment = new 	Payment($paymentRef);
		//$listHistory =  $Payment->getHistory();

		$selectQuery	= "SELECT
		  a.USER_LOGIN,
		  b.`USER_FULLNAME` AS u_name,
		  c.`BUSER_NAME` AS b_name,
		  a.DATE_TIME,
		  a.BG_REASON,
		  a.HISTORY_STATUS,
		  a.BG_REG_NUMBER
		  
		  
		FROM
		  T_BANK_GUARANTEE_HISTORY AS a
		  LEFT JOIN M_USER AS b ON a.`USER_LOGIN` = b.`USER_ID` AND a.`CUST_ID` = b.`CUST_ID`
		  LEFT JOIN `M_BUSER` AS c ON a.`USER_LOGIN` = c.`BUSER_ID`
		WHERE a.BG_REG_NUMBER = " . $this->_db->quote((string) $paymentRef) . " GROUP BY HISTORY_ID ORDER BY DATE_TIME";
		$result =  $this->_db->fetchAll($selectQuery);

		//$trfStatus 		= array_combine($this->_transferstatus["code"], $this->_transferstatus["desc"]);
		//echo '<pre>';
		//var_dump($this->_transferstatus);
		foreach ($result as $key => $value) {


			# code...
		}
		$config = Zend_Registry::get('config');

		$historyStatusCode = $config["history"]["status"]["code"];
		$historyStatusDesc = $config["history"]["status"]["desc"];
		$historyStatusArr  = array_combine(array_values($historyStatusCode), array_values($historyStatusDesc));

		$this->view->historyStatusArr   = $historyStatusArr;


		$this->view->paymentHistory =  $result;
		$this->view->dateTimeDisplayFormat  =  $this->_dateTimeDisplayFormat;
	}
}
