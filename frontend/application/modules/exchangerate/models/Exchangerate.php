<?php
class exchangerate_Model_Exchangerate
{
 	protected $_db;

    // constructor
 
 	public function __construct() {
	     $config = array(
	       'host'      	=> 'localhost',
	       'username'  	=> 'root',
	       'password'  	=> '590livedb',
	       'dbname'    	=> 'bankmayapada',
	       'port'  		=> '3306'
	     );
	     $this->_db = new Zend_Db_Adapter_Pdo_Mysql($config);
    }
    
	public function getData()
	{
		$result = $this->_db->fetchAll(
				$this->_db->select()
					->from(array('C' => 'GLDCRT'),array('*'))
					//->where("USER_ID = ".$this->_db->quote($userId))
			);
		return $result;
		
	}
	
	public function getDataUpdate()
	{
		$result = $this->_db->fetchRow(
				$this->_db->select()
					->from(array('C' => 'GLDCRT_UPDATED'),array('*'))
					//->where("USER_ID = ".$this->_db->quote($userId))
			);
		return $result;
		
	}
	
	public function getDataInterestRate($tipe = null)
	{
		
		$result = $this->_db->fetchAll(
				$this->_db->select()
					->from(array('C' => 'INTEREST_RATE'),array('*'))
					->where("TIPE = ?",$tipe)
			);
		
		return $result;
		
	}
	
	public function getDataInterestRateOther()
	{		
		$result = $this->_db->fetchAll(
				$this->_db->select()
					->from(array('C' => 'INTEREST_RATE'),array('*'))
					//->where("TIPE <> ".$paramna[TABUNGANKU]. " AND TIPE <> ".$paramna[MYDOLLAR]. " AND TIPE <> ".$paramna[DEPOSITO])
					->order("URUTAN")
			);
		
		return $result;
		
	}

	public function getCustomerAccount($param)
	{
		
		$select = $this->_db->select()
				->from(array('A' => 'M_CUSTOMER_ACCT'))
				->join(array('C' => 'M_MAKERLIMIT'),'A.ACCT_NO = C.ACCT_NO',array())
				->join(array('D' => 'M_PRODUCT_TYPE'),'D.PRODUCT_CODE = A.ACCT_TYPE',array('D.PRODUCT_CODE','D.PRODUCT_NAME'))
				->joinLeft(array('B' => 'M_GROUPING'),'A.GROUP_ID = B.GROUP_ID',array('GROUP_NAME'))
				->where("A.ACCT_STATUS = 1")
				->where("C.MAXLIMIT > 0");				
				
				if(isset($param['USER_ID'])) $select->where("C.USER_LOGIN = ".$this->_db->quote($param['USER_ID']));
				if(isset($param['CUST_ID'])) $select->where("A.CUST_ID = ".$this->_db->quote($param['CUST_ID']));
				 				
				$select->order('B.GROUP_NAME DESC');
				$select->order('A.ORDER_NO ASC');
				$select->limit(1);	
			
//		echo $select; die;
		$result = $this->_db->fetchAll($select);

		return $result;
	}
	
	public function getDataInterestRateUpdate()
	{
		$result = $this->_db->fetchRow(
				$this->_db->select()
					->from(array('C' => 'INTEREST_RATE_UPDATED'),array('*'))
					//->where("USER_ID = ".$this->_db->quote($userId))
			);
		return $result;
		
	}
}
    