<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';
require_once 'General/CustomerUser.php';

class outgoingcheque_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF';
	
	
	
	public function indexAction()
	{	

		$this->_helper->layout()->setLayout('newlayout');
		
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}	
		
		$CustomerUser 	= new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$AccArr = $CustomerUser->getAccounts();
		$this->view->AccArr = $AccArr;
		
		
		$ProductTypeArr = array(
		    '1' => $this->language->_('Cheque'),
		    '2' => $this->language->_('Bilyet Giro'),
		    );
		$this->view->ProdType 			= $ProductTypeArr;
		
		
		$StatusArr = array(
		    '1' => $this->language->_('Active'),
		    '2' => $this->language->_('Draw'),
		    '3' => $this->language->_('Ready'),
		);
		$this->view->StatusArr 			= $StatusArr;
		
		$opt[""] = "-- " .$this->language->_('Please Select'). " --";
		
		$payType 	= Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
		//$optpaytype 	= $this->setPaymentType($this->_paymenttype,$this->_transfertype);
		$arrPayStatus	= array_combine($this->_paymentstatus["code"], $this->_paymentstatus["desc"]);
		$trfType 		= array_combine($this->_transfertype["code"], $this->_transfertype["desc"]);
		$arrAccount 	= $CustomerUser->getAccounts();
		
		foreach($payType as $key => $value){ 
// 			if($key != 3) $optpaytypeRaw[$key] = $this->language->_($value);
				
			 $optpaytypeRaw[$key] = $this->language->_($value); 
		}
		//foreach($trfType as $key => $value){ if($key != 3 && $key != 4) $filterTrfType[$key] = $this->language->_($value); }
		foreach($trfType as $key => $value){ $filterTrfType[$key] = $this->language->_($value); }
		foreach($arrPayStatus as $key => $value){ if($key != 3) $optpayStatusRaw[$key] = $this->language->_($value); }
		
		if(is_array($arrAccount) && count($arrAccount) > 0){
			foreach($arrAccount as $key => $value){
				
				$val 		= $arrAccount[$key]["ACCT_NO"];
				$ccy 		= $arrAccount[$key]["CCY_ID"];
				$acctname 	= $arrAccount[$key]["ACCT_NAME"];
				//$acctalias 	= $arrAccount[$key]["ACCT_ALIAS_NAME"];
				$accttype 	= ($arrAccount[$key]["ACCT_TYPE"] == '10') ? 'SAVING': 'GIRO';	// 10 : saving, 20 : giro;
				
				$arrAccountRaw[$val] = $val.' ['.$ccy.'] '.$acctname.' ('.$accttype.')';
				
			}
		}
		else { $arrAccountRaw = array();}
		
		
		unset ($arrPayStatus[10]);
		unset ($arrPayStatus[15]);
		unset ($arrPayStatus[16]);
		
		$optPayType 	= $opt + $optpaytypeRaw;
		
		//$optPayStatus 	= $opt + $arrPayStatus;
		$optPayStatus 	= $opt + $optpayStatusRaw;
		unset($optPayStatus[10]);
		unset($optPayStatus[15]);
		unset($optPayStatus[16]);
		
		//hilangkan payment type payroll, Sweep In, Sweep Out
		$optPayTypePayroll = "11,11";
		$optPayTypeSweepIn = "14,14";
		$optPayTypeSweepOut = "15,15";
		$optPayTypeCredit = "6,4";
		$optPayTypeDebit = "7,5";
		//unset($optPayType[$optPayTypePayroll]);
		//unset($optPayType[$optPayTypeSweepIn]);
		//unset($optPayType[$optPayTypeSweepOut]);
		
		//unset($optPayType[$optPayTypeDebit]);
		//unset($optPayType[$optPayTypeCredit]);
		
		$optarrAccount 	= $opt + $arrAccountRaw;
		
		$this->view->optPayType 	= $optPayType;
		$this->view->optPayStatus 	= $optPayStatus;
		$this->view->optarrAccount 	= $optarrAccount;		
		$fields = array	(	                        
			'posting'  					=> array	(
		    'field' => 'PS_POSTING',
		    'label' => $this->language->_('Posting Date'),
		    'sortable' => true
			),
		    'disburse'  					=> array	(
		        'field' => 'PS_DISBURSE',
		        'label' => $this->language->_('Disburse Date'),
		        'sortable' => true
		    ),
		    'chbgno'  => array	(
				'field' => 'WARKAT_NO',
				'label' => $this->language->_('CH/BG No'),
				'sortable' => true
			),
		    'description'  					=> array	(
		        'field' => 'DESCRIPTION',
		        'label' => $this->language->_('Description'),
		        'sortable' => true
		    ),
		    'amount' 				=> array	(
		        'field' => 'AMOUNT',
		        'label' => $this->language->_('Amount'),
		        'sortable' => true
		    ),
		    'runningbalance'  					=> array	(
		        'field' => 'RUNNING_BALANCE',
		        'label' => $this->language->_('Running Balance'),
		        'sortable' => true
		    ),
		    'status'  			=> array	(
		        'field' => 'WARKAT_STATUS',
		        'label' => $this->language->_('Status'),
		        'sortable' => true
		    ),
		    'action'        			=> array	(
		        'field' => '',
		        'label' => $this->language->_('Action'),
		        'sortable' => true
		    ),
		    	
		);

		$filterlist = array('SOURCE_ACCOUNT','CH/BG','PRODUCT_TYPE','PS_STATUS');

		$this->view->filterlist = $filterlist;
		
		//get page, sortby, sortdir
		$page    		= $this->_getParam('page');
		$sortBy  		=($this->_getParam('sortby')) ?  $this->_getParam('sortby') : ('updated');
		$sortDir 		= $this->_getParam('sortdir');
		$filter 		= $this->_getParam('filter');
		$clearfilter	= $this->_getParam('clearfilter');
		
		$csv 			= $this->_getParam('csv');
		$pdf 			= $this->_getParam('pdf');
		
		$this->view->sortBy			= $sortBy;
		$this->view->sortDir 		= $sortDir;
		$this->view->filter			= $filter;
		$this->view->clearfilter	= $clearfilter;
		
		//validate parameters before passing to view and query
		$page 		= (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
		$sortBy 	= (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir 	= (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'desc';	
		
		$this->view->currentPage = $page;
		
		$filterArr = array(	
							'SOURCE_ACCOUNT' 		=> array('StringTrim','StripTags'),
							'PRODUCT_TYPE' 		=> array('StringTrim','StripTags','StringToUpper'),
							'chfrom'	=> array('StringTrim','StripTags'),
							'chend' 	=> array('StringTrim','StripTags'),
							'STATUS' 	=> array('StringTrim','StripTags'),
							
						
		);

		$dataParam = array("SOURCE_ACCOUNT","PRODUCT_TYPE","chfrom","chend","STATUS");
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{
			// print_r($dtParam);die;
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}

			}

			if ($dtParam == 'chfrom') {
				$dataParamValue[$dtParam] = $this->_request->getParam('chfrom');
			}

			if ($dtParam == 'chend') {
				$dataParamValue[$dtParam] = $this->_request->getParam('chend');
			}
		}

		// print_r($dataParamValue);die();
		
		// The default is set so all fields allow an empty string		
		$options = array('allowEmpty' => true);
		$validators = array(
						'SOURCE_ACCOUNT' 		=> array(array('InArray', array('haystack' => array_keys($optarrAccount)))),	// $filter!
						'PRODUCT_TYPE'		=> array(),
            		    'chfrom'		=> array(),
            		    'chend'		=> array(),
		                'STATUS'		=> array(),
						);
		
		$zf_filter  = new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options);
		
		if ($zf_filter->isValid()) {
	     	$filter = TRUE;
	    }
		
		$fAcctsrc 		= $zf_filter->getEscaped('SOURCE_ACCOUNT');
		
		$fProductType 	= $zf_filter->getEscaped('PRODUCT_TYPE');
		$fChStart = $zf_filter->getEscaped('chfrom');
		$fChend 	= $zf_filter->getEscaped('chend');
		$fStatus 	= $zf_filter->getEscaped('STATUS');
		
		
		


		$select	= $this->_db->select()
		->from(array('P'	 		=> 'T_CHEQUE_LIST'),
		    array('P.*')
		);


		if (!empty($fChStart) || !empty($fChend)) {	
			$select->where('P.WARKAT_NO BETWEEN '.$fChStart.' AND '.$fChend);
		}

		if($fAcctsrc)
		{	
	    	$select->where("P.SOURCE_ACCOUNT = ? ", $fAcctsrc);
	    }
//		{	$select->where("T.SOURCE_ACCOUNT LIKE ".$this->_db->quote('%'.$fAcctsrc.'%'));	}	// ACCSRC ?????
		
		if($fPaymentReff)
		{	$select->where("UPPER(P.PS_NUMBER) LIKE ".$this->_db->quote('%'.$fPaymentReff.'%'));	}	
		

		$select->where("P.OUTGOING_STATUS = 1");
		$select->where("P.CUST_ID = ? ",$this->_custIdLogin);
		
		// echo $select;die;		
		
// 		$select->order($sortBy.' '.$sortDir);
//         echo $select;

		if($filter || $clearfilter)
			$dataSQL = $this->_db->fetchAll($select);
		else
			$dataSQL = null;


		if ($csv || $pdf)
		{	
			$fieldExport = $fields;
			unset($fieldExport["action"]);
			$header  = Application_Helper_Array::simpleArray($fieldExport, "label");		}
		else
		{
			// $this->paging($dataSQL);	
			// $dataSQL = $this->view->paginator;
		}
		
		$data = array();
 	//	echo '<pre>';
 	//	print_r($dataSQL);die;
		foreach ($dataSQL as $d => $dt)
		{
							
			foreach ($fields as $key => $field)
			{
				$value 	   = $dt[$key];
				$PSSTATUS  = $dt["PS_STATUS"];
// 				$PSNUMBER  = $dt["payReff"];
// 				$payStatus = $dt["payStatus"];
				
						
				if ($key == "posting")		{ $value = Application_Helper_General::convertDate($dt["PS_POSTING"],$this->view->displayDateTimeFormat); }
				elseif ($key == "disburse")		{ $value = Application_Helper_General::convertDate($dt["PS_DISBURSE"],$this->_dateViewFormat,$this->view->defaultDateFormat); }
				elseif ($key == "sheets")		{ $value = $dt["NO_OF_SHEETS"]; }
				elseif ($key == "amount")		{ $value = $dt["AMOUNT"]; }
				elseif ($key == "runningbalance")		{ $value = $dt["RUNNING_BALANCE"]; }
				elseif ($key == "action")		{ $value = $dt["PS_NUMBER"]; }
				elseif ($key == "status")		{ $value = $dt["WARKAT_STATUS"]; }
				elseif ($key == "description")		{ $value = $dt["DESCRIPTION"]; }
				elseif ($key == "chbgno")		{ $value = $dt["WARKAT_NO"]; }
// 				elseif ($key == "updated")		{ $value = Application_Helper_General::convertDate($value,$this->view->displayDateTimeFormat); }
				// elseif ($key == "accsrc_name")	{			
					// $alias_name  = (!empty($dt["accsrc_alias"]) && $dt["accsrc_alias"] != "-")? " / ".$dt["accsrc_alias"]: "";
					// $value		 = trim($value).$alias_name;
				// }
				// elseif ($key == "acbenef_name"){			
					// $alias_name  = (!empty($dt["acbenef_alias"]) && $dt["acbenef_alias"] != "-")? " / ".$dt["acbenef_alias"]: "";
					// $value		 = trim($value).$alias_name;
				// }
				
				$value = ($value == "" && !$csv)? "&nbsp;": $value;
				$data[$d][$key] = $this->language->_($value);
			}
		
		}
// 		print_r($data);die;
		$this->paging($data);

		$dataExport = $data;
		$statusArr = array("1" => "RECLEARING", "2" => "CASH", "3" => "CANCEL");

		foreach($dataExport as $key => $val){
			$dataExport[$key]["status"] = $statusArr[$dataExport[$key]["status"]];
			unset($dataExport[$key]["action"]);
		}
		
		if($csv)
		{
			$this->_helper->download->csv($header,$dataExport,null,'List View Payment');  
			Application_Helper_General::writeLog('DARC','Export CSV View Payment');
		}
		elseif($pdf)
		{
			$this->_helper->download->pdf($header,$dataExport,null,'List View Payment');  
			Application_Helper_General::writeLog('DARC','Export PDF View Payment');
		}
		else
		{	
			$stringParam = array(
								'updatedStart' 	=> $fUpdatedStart,
								'updatedEnd'	=> $fUpdatedEnd,
								'createdStart'	=> $fCreatedStart,
								'createdEnd'	=> $fCreatedEnd,		
								'paymentStart'	=> $fPaymentStart,		
								'paymentEnd'	=> $fPaymentEnd,		
								'ACCTSRC'		=> $fAcctsrc,	
								'payReff'		=> $fPaymentReff,
								'paymentStatus'	=> $fPaymentStatus,		
								'paymentType'	=> $fPaymentType,	
								'transferType'	=> $fTransferType,
								'clearfilter'	=> $clearfilter,
							
								);
			/*
			$stringParam = array('payReff'		=> $fPaymentReff,
								 'payType'		=> $fPaymentType,
								 'trfType'		=> $fTrfType,
								 'createdFrom'	=> $fCreatedFrom,
								 'createdTo'	=> $fCreatedTo,
								 'payDateFrom'	=> $fPayDateFrom,
								 'payDateTo'	=> $fPayDateTo,
							    );	
			*/
			
			//echo "fPaymentStatus: $fPaymentStatus";
			//echo "fAcctsrc: $fAcctsrc";
			//echo "fPaymentType: $fPaymentType";
			
			// $this->view->filterPayType 		= $filterPayType;
			// $this->view->optfilterPayStatus 	= $optfilterPayStatus;
			// $this->view->CustomerArr 		= $CustomerArr;
			
			
			$this->view->optPayType 	= $optPayType;
			$this->view->optPayStatus 	= $optPayStatus;
			$this->view->optarrAccount 	= $optarrAccount;
		
			
			$this->view->updatedStart 	= $fUpdatedStart;
			$this->view->updatedEnd 	= $fUpdatedEnd;
			$this->view->createdStart 	= $fCreatedStart;
			$this->view->createdEnd 	= $fCreatedEnd;
			$this->view->paymentStart 	= $fPaymentStart;
			$this->view->paymentEnd 	= $fPaymentEnd;
			$this->view->ACCTSRC 		= $fAcctsrc;
			$this->view->sourceAccount 		= $dataSQL['0']['SOURCE_ACCOUNT'];
// 			print_r($dataSQL);die;
			$this->view->sourceAccountName    =   $dataSQL['0']['SOURCE_ACCOUNT_NAME'];
			$this->view->sourceCCY    =   $dataSQL['0']['SOURCE_ACCOUNT_CCY'];
			$this->view->productType    =   $dataSQL['0']['PRODUCT_TYPE'];
			$this->view->chbgnumber    =   $dataSQL['0']['NO_OF_SHEETS'];
			$this->view->datetime    =   Application_Helper_General::convertDate($dataSQL['0']['PS_ACTIVATED'],$this->view->displayDateTimeFormat);
			
			
			$this->view->payReff 		= $fPaymentReff;
			$this->view->paymentStatus 	= $fPaymentStatus;
			$this->view->paymentType 	= $fPaymentType;
			$this->view->transferType 	= $fTransferType;
			
			//$this->view->query_string_params = $stringParam;
			
			$this->view->data 				= $data;
			$this->view->fields 			= $fields;
			$this->view->filter 			= $filter;
	        $this->view->sortBy 			= $sortBy;
			$this->view->sortDir 			= $sortDir;
			
			$this->view->clearfilter 		= $clearfilter;
			
			// $this->view->currentPage 		= $page;
			// $this->view->paginator 			= $data;
			
			Application_Helper_General::writeLog('DARC','View Payment');
			
			//$this->view->sortBy = $sortBy;
			//$this->view->sortDir = $sortDir;
			
		}

		if($this->_request->getParam('print') == 1){
			$arr = $dataSQL;
			$fieldPrint = $fields;
			unset($fieldPrint['action']);
			foreach($arr as $key => $val){
				unset($arr[$key]["PS_NUMBER"]);
				unset($arr[$key]["CUST_ID"]);
				unset($arr[$key]["NO_OF_SHEETS"]);

				$warkatno = $arr[$key]["WARKAT_NO"];
				unset($arr[$key]["WARKAT_NO"]);

				$statusArr = array("1" => "RECLEARING", "2" => "CASH", "3" => "CANCEL");
				$status = $statusArr[$arr[$key]["WARKAT_STATUS"]];
				unset($arr[$key]["WARKAT_STATUS"]);

				$disburse = Application_Helper_General::convertDate($arr[$key]["PS_DISBURSE"],$this->view->displayDateFormat);
				unset($arr[$key]["PS_DISBURSE"]);
				unset($arr[$key]["PS_ACTIVATED"]);
				

				$arr[$key]["PS_POSTING"] = Application_Helper_General::convertDate($arr[$key]["PS_POSTING"],$this->view->displayDateTimeFormat);
				$arr[$key]["PS_DISBURSE"] = $disburse;

				$arr[$key]["WARKAT_NO"] = $warkatno;
					
				unset($arr[$key]["WARKAT_SERI"]);
				unset($arr[$key]["SOURCE_ACCOUNT"]);
				unset($arr[$key]["SOURCE_ACCOUNT_NAME"]);
				unset($arr[$key]["SOURCE_ACCOUNT_CCY"]);
				unset($arr[$key]["PRODUCT_TYPE"]);
				unset($arr[$key]["PS_UPDATEDBY"]);
				unset($arr[$key]["OUTGOING_STATUS"]);
				unset($arr[$key]["END_STATUS"]);

				$arr[$key]["WARKAT_STATUS"] = $status;
			}
			$this->_forward('print', 'index', 'widget', array('data_content' => $arr, 'data_caption' => 'Incoming', 'data_header' => $fieldPrint));
		}
		
	}
}