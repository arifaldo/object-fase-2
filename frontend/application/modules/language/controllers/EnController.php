<?php
class language_EnController extends Application_Main {
	
	public function indexAction(){
		$ns = new Zend_Session_Namespace('language');
		
//		if($this->_request->isPost()){
//			$lang = $this->_request->getPost("lang");
			$lang = 'en';
					$data['USER_LANG'] = 'en';
					$where['USER_ID = ?'] = $this->_userIdLogin;
					$this->_db->update('M_USER',$data,$where);
					
					Application_Helper_General::writeLog('CLAN','Change Language: Set language to '.$lang);
					$ns->langCode = $lang;
			$this->_redirect('/home/dashboard');
//		}
		
		$this->view->lang		= $ns->langCode;
		Application_Helper_General::writeLog('CLAN','Change Language');
	}
}
