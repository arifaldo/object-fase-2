<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/BulkPayment.php';
require_once 'CMD/Validate/ValidatePaymentSingle.php';
require_once 'General/CustomerUser.php';
require_once 'General/Customer.php';
require_once 'General/Settings.php';
require_once 'General/Charges.php';
require_once 'CMD/SinglePayment.php';
require_once 'CMD/Validate/ValidatePaymentMultiple.php';

require_once 'SGO/Extendedmodule/PHPExcel.php';
require_once 'SGO/Extendedmodule/PHPExcel/IOFactory.php';

class newbatchpayment_documentController extends Application_Main{
  
  protected $_moduleDB = 'RTF'; //masih harus diganti

  protected $_destinationUploadDir = '';
  protected $_maxRow = '';

  public function initController()
  {
    $this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';

    $setting = new Settings();
    $this->_maxRow = $setting->getSetting('max_import_bulk');
  }

  public function indexAction()
  { 
   

    $this->_helper->layout()->setLayout('popup');
    
    $this->view->suggestionType = $this->_suggestType;
   
      $change_id = $this->_getParam('BS_ID');

  	  $select = $this->_db->select()
                             // ->from('TEMP_BULKPSLIP',array('*'))
                        ->from(array('A' => 'TEMP_BULKPSLIP'),array())
                        ->join(array('B' => 'TEMP_BULKTRANSACTION'),'A.BS_ID = B.BS_ID',array('A.*','B.SOURCE_ACCOUNT','B.BENEFICIARY_ACCOUNT','B.BENEFICIARY_ACCOUNT_NAME','B.BENEFICIARY_ACCOUNT_CCY'))
                        ->where('A.BS_ID = ?',$change_id);
                            
      $resultdata = $this->_db->fetchAll($select);

      $conf = Zend_Registry::get('config');
      $paymentType = $conf['payment']['type'];
      $paymentTypeFlip = array_flip($paymentType['code']);

      $this->view->paymentType = $paymentType;
      $this->view->paymentTypeFlip = $paymentTypeFlip;

      $frontendOptions = array(
          'lifetime' => 86400,
          'automatic_serialization' => true
        );
      $backendOptions = array('cache_dir' => LIBRARY_PATH . '/data/cache/latesttrans/'); // Directory where to put the cache files
      $cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);

      $cacheID = 'USERLIST';

      $userlist = $cache->load($cacheID);
      $this->view->pdfUserlist  = $userlist;

      $this->view->bs_id            = $resultdata[0]['BS_ID'];
      $this->view->ps_subject       = $resultdata[0]['PS_SUBJECT'];
      $this->view->upload_by        = $resultdata[0]['UPLOADBY'];
      $this->view->ef_date          = $resultdata[0]['PS_EFDATE'];
      $this->view->total_amount     = $resultdata[0]['PS_TOTAL_AMOUNT'];
      $this->view->success_amount   = $resultdata[0]['PS_SUCCESS_AMOUNT'];
      $this->view->failed_amount    = $resultdata[0]['PS_FAILED_AMOUNT']; 
      $this->view->ps_type          = $resultdata[0]['PS_TYPE'];  
      $this->view->ps_ccy           = $resultdata[0]['PS_CCY'];    
      $this->view->ps_suc           = $resultdata[0]['PS_SUCCESS_TXAMOUNT'];  
      $this->view->ps_tot           = $resultdata[0]['PS_TXCOUNT'];  
      $this->view->ps_fai           = $resultdata[0]['PS_FAILED_TXAMOUNT'];  
      $this->view->ps_uploaded      = $resultdata[0]['PS_UPLOADED']; 
      $this->view->validation       = $resultdata[0]['VALIDATION'];
      $this->view->source_acct      = $resultdata[0]['SOURCE_ACCOUNT']; 
      $this->view->file_id          = $resultdata[0]['FILE_ID'];
      $this->view->beneficiary_acct = $resultdata[0]['BENEFICIARY_ACCOUNT'];
      $this->view->beneficiary_name = $resultdata[0]['BENEFICIARY_ACCOUNT_NAME'];
      $this->view->beneficiary_ccy  = $resultdata[0]['BENEFICIARY_ACCOUNT_CCY'];

      $downloadURL2 = $this->view->url(array('module'=>'newbatchpayment','controller'=>'document','action'=>'downloadtrx2','txt'=>'1','BS_ID'=>$change_id),null,true);
      
      $this->view->download2 = $this->view->formButton('download','download',array('class'=>'inputbtn', 'onclick'=>"window.location = ".$this->_db->quote($downloadURL2).";")); 

      //reupload
      $select = $this->_db->select()
              ->from(array('C' => 'M_CUST_ADAPTER_PROFILE'),array('*'))
              ->join(array('A' => 'M_ADAPTER_PROFILE'), 'C.ADAPTER_PROFILE_ID = A.PROFILE_ID', '*')
              ->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'A.PROFILE_ID = D.PROFILE_ID', '*')
              ->where("CUST_ID = ?",$this->_custIdLogin)
              ->where("TRF_TYPE = ?", 'payroll');
      $adapterDataPayroll = $this->_db->FetchAll($select);

      $select = $this->_db->select()
              ->from(array('C' => 'M_CUST_ADAPTER_PROFILE'),array('*'))
              ->join(array('A' => 'M_ADAPTER_PROFILE'), 'C.ADAPTER_PROFILE_ID = A.PROFILE_ID', '*')
              ->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'A.PROFILE_ID = D.PROFILE_ID', '*')
              ->where("CUST_ID = ?",$this->_custIdLogin)
              ->where("TRF_TYPE = ?", 'manytomany');
      $adapterDataMtm = $this->_db->FetchAll($select);

      $select = $this->_db->select()
              ->from(array('C' => 'M_CUST_ADAPTER_PROFILE'),array('*'))
              ->join(array('A' => 'M_ADAPTER_PROFILE'), 'C.ADAPTER_PROFILE_ID = A.PROFILE_ID', '*')
              ->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'A.PROFILE_ID = D.PROFILE_ID', '*')
              ->where("CUST_ID = ?",$this->_custIdLogin)
              ->where("TRF_TYPE = ?", 'emoney');
      $adapterDataEmoney = $this->_db->FetchAll($select);

      $select = $this->_db->select()
              ->from(array('C' => 'M_CUST_ADAPTER_PROFILE'),array('*'))
              ->join(array('A' => 'M_ADAPTER_PROFILE'), 'C.ADAPTER_PROFILE_ID = A.PROFILE_ID', '*')
              ->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'A.PROFILE_ID = D.PROFILE_ID', '*')
              ->where("CUST_ID = ?",$this->_custIdLogin)
              ->where("TRF_TYPE = ?", 'onetomany');
      $adapterDataOtm = $this->_db->FetchAll($select);

      $select = $this->_db->select()
              ->from(array('C' => 'M_CUST_ADAPTER_PROFILE'),array('*'))
              ->join(array('A' => 'M_ADAPTER_PROFILE'), 'C.ADAPTER_PROFILE_ID = A.PROFILE_ID', '*')
              ->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'A.PROFILE_ID = D.PROFILE_ID', '*')
              ->where("CUST_ID = ?",$this->_custIdLogin)
              ->where("TRF_TYPE = ?", 'manytoone');
      $adapterDataMto = $this->_db->FetchAll($select);

      if($this->_request->isPost())
      {
      
        $filter = new Application_Filtering();
        $confirm = false;
        $error_msg[] = "";
        
        $BULK_TYPE  = $this->_request->getParam('ps_type');
        
        if($BULK_TYPE == '4'){
        //awal
          
              $BS_ID  = $this->_request->getParam('bs_id');

              // $where = array(
              //        'BS_ID = ?' => $BS_ID
              //      );
              // $this->_db->delete('TEMP_BULKTRANSACTION',$where);

             // $extension = 'txt';
               if (!empty($adapterDataOtm)) {
                  $extension = $adapterDataOtm[0]['FILE_FORMAT'];
                }
                else{
                  $extension = 'txt'; 
                }

              $fileName = $adapterDataOtm[0]['FILE_PATH'];
              $fixLength = $adapterDataOtm[0]['FIXLENGTH'];
              $fixLengthType = $adapterDataOtm[0]['FIXLENGTH_TYPE'];
              $fixLengthHeader = $adapterDataOtm[0]['FIXLENGTH_HEADER_ORDER'];
              $fixLengthHeaderName = $adapterDataOtm[0]['FIXLENGTH_HEADER_NAME'];
              $fixLengthContent = $adapterDataOtm[0]['FIXLENGTH_CONTENT_ORDER'];
              //$delimitedWith = $adapterDataOtm[0]['DELIMITED_WITH'];
              $delimitedWith = '|';

              $PS_SUBJECT   = $filter->filter($this->_request->getParam('ps_subject'), "PS_SUBJECT");
              $PS_EFDATE    = $filter->filter($this->_request->getParam('ef_date'), "PS_DATE");
              $ACCTSRC    = $filter->filter($this->_request->getParam('source_acct'), "ACCOUNT_NO");
              $PSFILEID     = $filter->filter($this->_request->getParam('file_id'), "FILE_ID");

              $paramSettingID = array('range_futuredate', 'auto_release_payment');

              $settings = new Application_Settings();
              $settings->setSettings(null, $paramSettingID);       // Zend_Registry => 'APPSETTINGS'
              $ccyList = $settings->setCurrencyRegistered();    // Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
              $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
              $AccArr = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR")));  // show acc in IDR only

              $adapter = new Zend_File_Transfer_Adapter_Http();
              $this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';
              $adapter->setDestination ( $this->_destinationUploadDir );
              $extensionValidator = new Zend_Validate_File_Extension(array(false, $extension));
              $extensionValidator->setMessage(
                                'Error: Extension file must be *.'.$extension
                              );

              $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
              $sizeValidator->setMessage(
                'Error: File exceeds maximum size'
              );

              $adapter->setValidators ( array (
                $extensionValidator,
                $sizeValidator,
              ));

              if ($adapter->isValid ())
              {

                          $srcData = $this->_db->select()
                          ->from(array('M_CUSTOMER_ACCT'),array('ACCT_NO','ACCT_TYPE','CCY_ID'))
                          ->where('ACCT_NO = ?', $ACCTSRC)
                          ->limit(1);

                          $acsrcData = $this->_db->fetchRow($srcData);

                          if(!empty($acsrcData)){
                            $accsrc = $acsrcData['ACCT_NO'];
                            $accsrctype = $acsrcData['ACCT_TYPE'];
                            $accsccy = $acsrcData['CCY_ID'];
                          }

                          $svcInquiry = new Service_Inquiry($this->_userIdLogin,$ACCTSRC,$acsrcData['ACCT_TYPE']);
                          $resultKursEx = $svcInquiry->rateInquiry();

                          //rate inquiry for display
                          $kurssell = '';
                          $kurs = '';
                          $book = '';
                          if($resultKursEx['ResponseCode']=='00'){
                            $kursList = $resultKursEx['DataList'];
                            $kurssell = '';
                            $kurs = '';

                            foreach($kursList as $row){
                              if($row["currency"] == 'USD'){
                                $row["sell"] = str_replace(',','',$row["sell"]);
                                $row["book"] = str_replace(',','',$row["book"]);
                                $row["buy"] = str_replace(',','',$row["buy"]);
                                $kurssell = $row["sell"];
                                $book = $row["book"];
                                if($ACCTSRC_CURRECY=='IDR'){
                                  $kurs = $row["sell"];
                                }else{
                                  $kurs = $row["buy"];
                                }
                              }
                            }

                          }

                $sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
                $NUMBER = Rand();
                  $random = md5($NUMBER);
                $newFileName = $adapter->getFileName () . '-' .$random.'.txt';
            
                $fileName = substr($newFileName,85);

                $adapter->addFilter ( 'Rename',$newFileName  );

                if ($adapter->receive ())
                { 
                  $data = $this->convertFileToArray($newFileName, $extension, $delimitedWith);
                  //var_dump($adapterDataOtm);die;
                  //@unlink($newFileName);
                  //end
                  
                  $dateNow = date("dmY");
                  $dateUpload = $data[0][2];
                  if ($dateUpload != $dateNow) {
                    
                    $error_msg[] = 'Error: Wrong file_id/date.Please check your file and reupload again.';
                    $this->view->error    = true;
                    $this->view->report_msg = $this->displayError($error_msg);

                  }else{

                    $psefdatenow = date("Y-m-d");

                    if (!empty($adapterDataOtm)) {
                      //unset header if not fixlength
                      if ($fixLength != 1) {
                        unset($data[0]);
                        unset($data[1]);  
                      }
                    }
                    else{
                      //unset defaults
                      unset($data[0]);
                      unset($data[1]);
                     
                    }

                    $totalRecords = count($data);

                    //proses convert ke order yg benar
                    if($totalRecords)
                    {

                      foreach ($adapterDataOtm as $key => $value) {
                        $headerOrder[] = $value['HEADER_CONTENT'];
                      }

                      foreach ($data as $datakey => $datavalue) {

                        if (!empty($headerOrder)) {
                          foreach ($headerOrder as $key => $value) {

                            $headerOrderArr = explode(',', $value);

                            if (count($headerOrderArr) > 1) {
                              $i = 0;
                              $contentStr = '';
                              foreach ($headerOrderArr as $key2 => $value2) {

                                if ($i != count($headerOrderArr)) {
                                  $contentStr .= $data[$datakey][$value2].' ';
                                }
                                $i++;
                              } 
                              $newData[$datakey][] = $contentStr;
                            }
                            else{
                              $newData[$datakey][] = $data[$datakey][$value];
                            }
                          } 
                        }
                        else{
                          $newData[$datakey][] = null;
                        }
                      }
                    }

                    //check mandatory yang bo setup saat buat business adapter profile
                    $mandatoryCheck = $this->validateField($newData, $adapterDataOtm);

                    //auto mapping agar isi field file sesuai dengan yang di setup di m_mapping bo
                         
                    //$fixData = $this->autoMapping($newData, $adapterDataOtm);
                    $fixData = $data;
                    if($totalRecords)
                    {
                      if($totalRecords <= $this->_maxRow)
                      {
                        //die('here');
                        $rowNum = 0;

                        $paramPayment = array(  "CATEGORY"        => "BULK CREDITT",
                                                "FROM"            => "I",
                                                "PS_NUMBER"       => "",
                                                "BS_ID"           => $BS_ID,
                                                "PS_SUBJECT"      => $PS_SUBJECT,
                                                "PS_EFDATE"       => Application_Helper_General::convertDate($psefdatenow, $this->_dateDisplayFormat),
                                                "PSFILEID"        => $PSFILEID,
                                                "PS_FILE"         => $fileName,
                                                "_dateFormat"     => $this->_dateDisplayFormat,
                                                "_dateDBFormat"   => $this->_dateDBFormat,
                                                "_addBeneficiary" => $this->view->hasPrivilege('BADA'),
                                                "_beneLinkage"    => $this->view->hasPrivilege('BLBU'),
                                                "_createPB"       => $this->view->hasPrivilege('CBPW'),
                                                "_createDOM"      => $this->view->hasPrivilege('CBPI'),
                                                "_createREM"      => false,
                                                );

                        $paramTrxArr = array();
                         
                        foreach ( $fixData as $row )
                        {
                           //print_r(count($row));
                          // if(count($row)==8)
                          // {
                            $rowNum++;
                            $benefAcct  = trim($row[1]);
                            $ccy    = 'IDR';
                            $amount   = trim($row[2]);
                            $purpose  = '';
                            $message  = trim($row[5]);
                            $addMessage = trim($row[6]);
                            $type     = trim($row[3]);
                            $bankCode   = trim($row[4]);
                            $cust_ref   = '';

                            //$TRA_SMS          = trim($row[7]);
                            $TRA_SMS  = '';
                            $TRA_EMAIL  = '';
                            $REFRENCE   = '';

                            $fullDesc = array(
                              'BENEFICIARY_ACCOUNT'    => $benefAcct,
                              'BENEFICIARY_ACCOUNT_CCY'    => $ccy,
                              'TRA_AMOUNT'         => $amount,
                              'TRA_MESSAGE'          => $message,
                              'TRA_PURPOSE'          => $purpose,
                              'REFNO'            => $addMessage,
                              'TRANSFER_TYPE'        => $type,
                              'CLR_CODE'           => $bankCode,
                              'CUST_REF'           => $cust_ref,
                            );
                            // print_r($fullDesc);die;

                            $filter = new Application_Filtering();

                            $TRA_AMOUNT     = $filter->filter($amount, "AMOUNT");
                            $TRA_MESSAGE    = $filter->filter($message, "TRA_MESSAGE");
                            $TRA_REFNO      = $filter->filter($addMessage, "TRA_REFNO");
                            $ACBENEF      = $filter->filter($benefAcct, "ACCOUNT_NO");
                            $ACBENEF_EMAIL    = $filter->filter($email, "EMAIL");
                            $ACBENEF_CCY    = $filter->filter($ccy, "SELECTION");
                            $ACBENEF_ADDRESS  = $filter->filter($bankCity, "ADDRESS");
                            $CLR_CODE     = $filter->filter($bankCode, "BANK_CODE");
                            $TRANSFER_TYPE    = $filter->filter($type, "SELECTION");
                            $TRANS_PURPOSE    = $filter->filter($purpose, "SELECTION");
                            $CUST_REF       = $filter->filter($cust_ref, "CUST_REF");
                            $TRA_AMOUNT_num   = Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

                            if($TRANSFER_TYPE == 'RTGS'){
                              $chargeType = '1';
                              $select = $this->_db->select()
                                      ->from('M_CHARGES_OTHER',array('*'))
                                      ->where("CUST_ID = ?",$this->_custIdLogin)
                                      ->where("CHARGES_TYPE = ?",$chargeType);
                              $resultSelecet = $this->_db->FetchAll($select);
                              $chargeAmt = isset($resultSelecet['0']['CHARGES_AMT']);
                            }
                            else if($TRANSFER_TYPE == 'SKN'){
                              $chargeType1 = '2';
                              $select1 = $this->_db->select()
                                      ->from('M_CHARGES_OTHER',array('*'))
                                      ->where("CUST_ID = ?",$this->_custIdLogin)
                                      ->where("CHARGES_TYPE = ?",$chargeType1);
                              $resultSelecet1 = $this->_db->FetchAll($select1);
                              $chargeAmt = isset($resultSelecet1['0']['CHARGES_AMT']);
                            }
                            else{
                              $chargeAmt = '0';
                            }

                            $filter->__destruct();
                            unset($filter);
                            if($accsccy != 'IDR' ){
                              // print_r($kurs);
                              // print_r($TRA_AMOUNT_num);die;
                              $TRA_AMOUNT_NET = $TRA_AMOUNT_num;
                              $TRA_AMOUNT_EQ  =  $TRA_AMOUNT_num/$kurs;
                            }else{
                              $TRA_AMOUNT_NET = $TRA_AMOUNT_num;
                              $TRA_AMOUNT_EQ = $TRA_AMOUNT_num;
                            }

                            $paramTrx = array("TRANSFER_TYPE"       => $TRANSFER_TYPE,
                                      "TRA_AMOUNT"        => $TRA_AMOUNT_NET,
                                      "TRA_AMOUNTEQ"        => $TRA_AMOUNT_EQ,
                                      "TRANSFER_FEE"        => $chargeAmt,
                                      "TRA_MESSAGE"         => $TRA_MESSAGE,
                                      "TRA_REFNO"         => $TRA_REFNO,
                                      "ACCTSRC"           => $ACCTSRC,
                                      "ACBENEF"           => $ACBENEF,
                                      "ACBENEF_CCY"         => $ACBENEF_CCY,
                                      "ACBENEF_EMAIL"       => $ACBENEF_EMAIL,
                                      "BENEFICIARY_RESIDENT"    => $BENEFICIARY_RESIDENT,
                                      "ACBENEF_CITIZENSHIP"     => $ACBENEF_CITIZENSHIP,
                                      "BANK_CODE"         => $CLR_CODE,
                                      "BENEFICIARY_ID_NUMBER"   => $BENEFICIARY_ID_NUMBER,
                                      "BENEFICIARY_ID_TYPE"     => $BENEFICIARY_ID_TYPE,
                                      "BENEFICIARY_CITY_CODE"   => $BENEFICIARY_CITY_CODE,
                                      "BENEFICIARY_CATEGORY"    => $BENEFICIARY_CATEGORY,
                                      "BANK_NAME"         => $BANK_NAME,
                                      "TRANS_PURPOSE"       => $TRANS_PURPOSE,
                                      "PS_NOTIF"          => $TRA_NOTIF,
                                      "CUST_REF"          => $CUST_REF,
                                      "PS_SMS"          => $TRA_SMS,
                                      "PS_EMAIL"          => $TRA_EMAIL,
                                      "REFRENCE"          => $REFRENCE,

                                     );
                            if($accsccy != 'IDR' ){
                              $paramTrx['RATE']     = $kurssell;
                              $paramTrx['RATE_BUY']   = $kurs;
                              $paramTrx['BOOKRATE']   = $book;
                            }
                          
                            array_push($paramTrxArr,$paramTrx);
                            
                          // }
                          // else
                          // {
                          // // die('here');
                          //  $error_msg[] = $this->language->_('Error').': '.$this->language->_('Wrong File Format').'.';
                          //  $this->view->error    = true;
                          //  $this->view->report_msg = $this->displayError($error_msg);
                            
                            
                          //  break;
                          // }
                        }
                      }

                      else
                      {
                        $error_msg[] = 'Error: The number of rows to be imported should not more than '.$this->_maxRow.'.';
                        $this->view->error    = true;
                        $this->view->report_msg = $this->displayError($error_msg);
                      }
                      
                      $confirm = true;

                    }
                    else
                    {
                      $error_msg[] = $this->language->_('Error').': '.$this->language->_('Wrong File Format').'.';
                      $this->view->error    = true;
                      $this->view->report_msg = $this->displayError($error_msg);
                    }
                    
                  }

                }
              }
              else
              {
                // print_r($adapter->getMessages());die;
                $this->view->error = true;
                foreach($adapter->getMessages() as $key=>$val)
                {
                  if($key=='fileUploadErrorNoFile')
                    $error_msg[] = $this->language->_('Error').': '.$this->language->_('File cannot be left blank. Please correct it').'.';
                  else
                    $error_msg[] = $val;
                  break;
                }
                $errors = $this->displayError($error_msg);
                $this->view->report_msg = $errors;
              }

        //akhir
        }else if($BULK_TYPE == '5'){
        //awal
              $BS_ID  = $this->_request->getParam('bs_id');

               if (!empty($adapterDataMto)) {
                  $extension = $adapterDataMto[0]['FILE_FORMAT'];
                }
                else{
                  $extension = 'txt'; 
                }

              $fileName = $adapterDataMto[0]['FILE_PATH'];
              $fixLength = $adapterDataMto[0]['FIXLENGTH'];
              $fixLengthType = $adapterDataMto[0]['FIXLENGTH_TYPE'];
              $fixLengthHeader = $adapterDataMto[0]['FIXLENGTH_HEADER_ORDER'];
              $fixLengthHeaderName = $adapterDataMto[0]['FIXLENGTH_HEADER_NAME'];
              $fixLengthContent = $adapterDataMto[0]['FIXLENGTH_CONTENT_ORDER'];
              //$delimitedWith = $adapterDataMto[0]['DELIMITED_WITH'];
              $delimitedWith = '|';

              $PS_SUBJECT         = $filter->filter($this->_request->getParam('ps_subject'), "PS_SUBJECT");
              $PS_EFDATE          = $filter->filter($this->_request->getParam('ef_date'), "PS_DATE");
              $ACBENEF            = $filter->filter($this->_request->getParam('beneficiary_acct'), "ACCOUNT_NO");
              $ACBENEF_BANKNAME   = $filter->filter($this->_request->getParam('beneficiary_name'), "ACCOUNT_NAME");
              $ACBENEF_ALIAS      = $filter->filter($this->_request->getParam('ACBENEF_ALIAS'), "ACCOUNT_ALIAS");
              $ACBENEF_CCY        = $filter->filter($this->_request->getParam('beneficiary_ccy'), "SELECTION");
              $PSFILEID           = $filter->filter($this->_request->getParam('file_id'), "FILE_ID");

              $minLen = 10;
              $maxLen = 20;
              $error_msg[] = "";

                  $paramSettingID = array('range_futuredate', 'auto_release_payment');

                  $settings = new Application_Settings();
                  $settings->setSettings(null, $paramSettingID);       // Zend_Registry => 'APPSETTINGS'
                  $ccyList = $settings->setCurrencyRegistered();    // Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
                  $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
                  $AccArr = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR")));  // show acc in IDR only

                  $adapter = new Zend_File_Transfer_Adapter_Http();

                  $adapter->setDestination ( $this->_destinationUploadDir );

                  $extensionValidator = new Zend_Validate_File_Extension(array(false, $extension));
                  $extensionValidator->setMessage(
                    'Error: Extension file must be *.'.$extension
                  );

                  // $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
                  // $sizeValidator->setMessage(
                  //  'File exceeds maximum size'
                  // );

                  // $adapter->setValidators ( array (
                  //  $extensionValidator,
                  //  $sizeValidator,
                  // ));

                  if ($adapter->isValid ())
                  {
                    // die('ger');
                    $sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
                    $NUMBER = Rand();
                      $random = md5($NUMBER);
                    $newFileName = $adapter->getFileName () . '-' .$random.'.txt';
                    
                    // $NUMBER = Rand();
                //      $random = md5($NUMBER);
                    // $fileName = $random. '-' .$sourceFileName. '.tmp';
                    $fileName = substr($newFileName,85);

                    $adapter->addFilter ('Rename',$newFileName);

                    if ($adapter->receive ())
                    {
                      $data = $this->convertFileToArray($newFileName, $extension, $delimitedWith);

                      $dateNow = date("dmY");
                      $dateUpload = $data[0][2];
                      if ($dateUpload != $dateNow) {
                        
                        $error_msg[] = 'Error: Wrong file_id/date.Please check your file and reupload again.';
                        $this->view->error    = true;
                        $this->view->report_msg = $this->displayError($error_msg);

                      }else{
                          $psefdatenow = date("Y-m-d");
                    
                          //@unlink($newFileName);
                      
                          if (!empty($adapterDataMto)) {
                            //unset header if not fixlength
                            if ($fixLength != 1) {
                              unset($data[0]);
                              unset($data[1]);  
                            }
                          }
                          else{
                            //unset defaults
                            unset($data[0]);
                            unset($data[1]);
                            
                          }

                          $totalRecords = count($data);

                          //proses convert ke order yg benar
                          if($totalRecords)
                          {

                            foreach ($adapterDataMto as $key => $value) {
                              $headerOrder[] = $value['HEADER_CONTENT'];
                            }

                            foreach ($data as $datakey => $datavalue) {

                              if (!empty($headerOrder)) {
                                foreach ($headerOrder as $key => $value) {

                                  $headerOrderArr = explode(',', $value);

                                  if (count($headerOrderArr) > 1) {
                                    $i = 0;
                                    $contentStr = '';
                                    foreach ($headerOrderArr as $key2 => $value2) {

                                      if ($i != count($headerOrderArr)) {
                                        $contentStr .= $data[$datakey][$value2].' ';
                                      }
                                      $i++;
                                    } 
                                    $newData[$datakey][] = $contentStr;
                                  }
                                  else{
                                    $newData[$datakey][] = $data[$datakey][$value];
                                  }
                                } 
                              }
                              else{
                                $newData[$datakey][] = null;
                              }
                            }
                          }

                          //check mandatory yang bo setup saat buat business adapter profile
                          $mandatoryCheck = $this->validateField($newData, $adapterDataMto);

                          //auto mapping agar isi field file sesuai dengan yang di setup di m_mapping bo
                          //$fixData = $this->autoMapping($newData, $adapterDataMto);
                        //  $fixData = $data;
                          //var_dump($data);die('here');
                          //if(empty($fixData)){
                            $fixData = $data;
                            $totalRecords = count($data);
                          //}
                          if($totalRecords)
                          {
                            if($totalRecords <= $this->_maxRow)
                            {
                              // die('here');
                              $rowNum = 0;

                              $paramPayment = array( "CATEGORY"         => "BULK DEBET",
                                                     "FROM"             => "I",
                                                     "PS_NUMBER"        => "",
                                                     "BS_ID"            => $BS_ID,
                                                     "PS_SUBJECT"       => $PS_SUBJECT,
                                                     "PS_EFDATE"        => Application_Helper_General::convertDate($psefdatenow, $this->_dateDisplayFormat),
                                                     "PSFILEID"         => $PSFILEID,
                                                     "PS_FILE"          => $fileName,
                                                     "_dateFormat"      => $this->_dateDisplayFormat,
                                                     "_dateDBFormat"    => $this->_dateDBFormat,
                                                     "_addBeneficiary"  => $this->view->hasPrivilege('BADA'), // privi BADA (Add Beneficiary)
                                                     "_beneLinkage"     => $this->view->hasPrivilege('BLBU'), // privi BLBU (Linkage Beneficiary User)
                                                     "_createPB"        => $this->view->hasPrivilege('IPMO'), // privi IPMO (Create Bulk Debet Payment by Import File (CSV))
                                                     "_createDOM"       => false,        // cannot create DOM trx
                                                     "_createREM"       => false,        // cannot create REM trx
                                                    );

                              $paramTrxArr = array();
                              
                              foreach ( $fixData as $key=>$row)
                              {
                                // var_dump(count($row));
                                // if(count($row)==5)
                                // {
                                  // die('here');
                                  $rowNum++;
                                  $sourceAcct   = trim($row[1]);
                                  $ccy          = "IDR";
                                  $amount       = trim($row[2]);
                                  $purpose      = '';
                                  $message      = trim($row[3]);
                                  $cust_ref     = '';
                                  $addMessage   = trim($row[4]);

                                  // if(!empty($row[4]) || !empty($row[5])){
                                    $TRA_NOTIF      = '2';
                                  // }else{
                                  //  $TRA_NOTIF      = '1';
                                  // }
                                  $TRA_SMS        = '';
                                  $TRA_EMAIL        = '';
                                  $REFRENCE       = '';


                                  $filter = new Application_Filtering();

                                  $TRA_AMOUNT     = $filter->filter($amount, "AMOUNT");
                                  $TRA_MESSAGE    = $filter->filter($message, "TRA_MESSAGE");
                                  $TRA_REFNO      = $filter->filter($addMessage, "TRA_REFNO");
                                  // $ACCTSRC       = $filter->filter($sourceAcct, "ACCOUNT_NO");
                                  $ACCTSRC      = $sourceAcct;
                                  $TRANSFER_TYPE    = 'PB';

                                  $TRA_AMOUNT_num   = Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

                                  $filter->__destruct();
                                  unset($filter);

                                  $paramTrx = array(  "TRANSFER_TYPE"   => $TRANSFER_TYPE,
                                            "TRA_AMOUNT"    => $TRA_AMOUNT_num,
                                            "TRA_MESSAGE"     => $TRA_MESSAGE,
                                            "TRA_REFNO"     => $TRA_REFNO,
                                            "ACCTSRC"       => $ACCTSRC,
                                            "ACBENEF"       => $ACBENEF,
                                            "ACBENEF_CCY"     => $ACBENEF_CCY,
                                            "ACBENEF_EMAIL"   => '',

                                          // for Beneficiary data, except (bene CCY and email), must be passed by reference
                                            "ACBENEF_BANKNAME"      => &$ACBENEF_BANKNAME,
                                            "ACBENEF_ALIAS"       => &$ACBENEF_ALIAS,
                                            "PS_NOTIF"          => $TRA_NOTIF,
                                            "CUST_REF"          => $cust_ref,
                                            "PS_SMS"          => $TRA_SMS,
                                            "PS_EMAIL"          => $TRA_EMAIL,
                                            "REFRENCE"          => $REFRENCE,
                                          //  "ACBENEF_CITIZENSHIP"     => $ACBENEF_CITIZENSHIP,    // 1/R: RESIDENT, 2/NR: NON-RESIDENT
                                          //  "ACBENEF_ADDRESS1"      => $ACBENEF_ADDRESS,
                                          //  "ACBENEF_ADDRESS2"      => &$ACBENEF_ADDRESS2,
                                          //  "ACBENEF_ADDRESS3"      => &$ACBENEF_ADDRESS3,

                                          //  "ORG_DIR"           => $ORG_DIR,
                                          //  "BANK_CODE"         => $CLR_CODE,
                                          //  "BANK_NAME"         => $BANK_NAME,
                                          //  "BANK_BRANCH"         => $BANK_BRANCH,
                                          //  "BANK_ADDRESS1"       => $BANK_ADDRESS1,
                                          //  "BANK_ADDRESS2"       => $BANK_ADDRESS2,
                                          //  "BANK_ADDRESS3"       => $BANK_ADDRESS3,
                                           );

                                  array_push($paramTrxArr,$paramTrx);
                                // }
                                // else
                                // {
                                //  $error_msg[] = $this->language->_('Wrong File Format').'';
                                //  $this->view->error    = true;
                                //  $this->view->report_msg = $this->displayError($error_msg);
                                //  break;
                                // }
                              }
                            }
                            // kalo jumlah trx lebih dari setting
                            else
                            {
                              // die('here1');
                              $error_msg[] = 'The number of rows to be imported should not more than '.$this->_maxRow.'.';
                              $this->view->error    = true;
                              $this->view->report_msg = $this->displayError($error_msg);
                            }
                            
                            $confirm = true;

                          }
                          else //kalo total record = 0
                          {
                            //$error_msg[] = 'Wrong File Format. There is no data on csv File.';
                            $error_msg[] = $this->language->_('Wrong File Format').'.';
                            $this->view->error    = true;
                            $this->view->report_msg = $this->displayError($error_msg);
                          }

                      }
                    }
                  }
                  else
                  {
                    // print_r($adapter->getMessages());die;
                    foreach($adapter->getMessages() as $key=>$val)
                    {
                      if($key=='fileUploadErrorNoFile'){
                        $error_msg[] = $this->language->_('File cannot be left blank. Please correct it').'.';
                        $this->view->error    = true;
                        $this->view->report_msg = $this->displayError($error_msg);
                      }else{
                        $error_msg[] = $val;
                        $this->view->error    = true;
                        $this->view->report_msg = $this->displayError($error_msg);
                      break;
                      }
                    }
                  }

        //akhir     
        }else if($BULK_TYPE == '11'){
        //awal
           
            //$extension = 'txt';
             if (!empty($adapterDataPayroll)) {
                  $extension = $adapterDataPayroll[0]['FILE_FORMAT'];
                }
                else{
                  $extension = 'txt'; 
                }

            $fileName = $adapterDataPayroll[0]['FILE_PATH'];
            $fixLength = $adapterDataPayroll[0]['FIXLENGTH'];
            $fixLengthType = $adapterDataPayroll[0]['FIXLENGTH_TYPE'];
            $fixLengthHeader = $adapterDataPayroll[0]['FIXLENGTH_HEADER_ORDER'];
            $fixLengthHeaderName = $adapterDataPayroll[0]['FIXLENGTH_HEADER_NAME'];
            $fixLengthContent = $adapterDataPayroll[0]['FIXLENGTH_CONTENT_ORDER'];
            //$delimitedWith = $adapterDataPayroll[0]['DELIMITED_WITH'];
            $delimitedWith = '|';

            $PS_SUBJECT   = $filter->filter($this->_request->getParam('ps_subject'), "PS_SUBJECT");
            $PS_EFDATE    = $filter->filter($this->_request->getParam('ef_date'), "PS_DATE");
            $ACCTSRC      = $filter->filter($this->_request->getParam('source_acct'), "ACCOUNT_NO");
            $PSFILEID     = $filter->filter($this->_request->getParam('file_id'), "FILE_ID"); 
            $BS_ID        = $this->_request->getParam('bs_id');

            $paramSettingID = array('range_futuredate', 'auto_release_payment');

            $settings = new Application_Settings();
            $settings->setSettings(null, $paramSettingID);       // Zend_Registry => 'APPSETTINGS'
            $ccyList = $settings->setCurrencyRegistered();    // Zend_Registry => 'CCYNUM_LIST', 'MINAMT_LIST'
            $CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
            $AccArr = $CustomerUser->getAccounts(array("CCY_IN" => array("IDR")));  // show acc in IDR only

            $adapter = new Zend_File_Transfer_Adapter_Http();

            $adapter->setDestination ( $this->_destinationUploadDir );
            $extensionValidator = new Zend_Validate_File_Extension(array(false, $extension));
            $extensionValidator->setMessage(
              'Error: Extension file must be *.'.$extension
            );

            $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
            $sizeValidator->setMessage(
              'Error: File exceeds maximum size'
            );

            $adapter->setValidators ( array (
              $extensionValidator,
              $sizeValidator,
            ));

            if ($adapter->isValid ())
            {
              $sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
              $NUMBER = Rand();
              $random = md5($NUMBER);
              $newFileName = $adapter->getFileName () . '-' .$random.'.txt';
              
              $fileName = substr($newFileName,85);

              $adapter->addFilter ('Rename',$newFileName);

              if ($adapter->receive ())
              {
                  $data = $this->convertFileToArray($newFileName, $extension, $delimitedWith);

                  $dateNow = date("dmY");
                  $dateUpload = $data[0][2];
                  if ($dateUpload != $dateNow) {
                    
                    $error_msg[] = 'Error: Wrong file_id/date.Please check your file and reupload again.';
                    $this->view->error    = true;
                    $this->view->report_msg = $this->displayError($error_msg);

                  }else{
                      $psefdatenow = date("Y-m-d");

                      //@unlink($newFileName);

                      if (!empty($adapterDataPayroll)) {
                        //unset header if not fixlength
                        if ($fixLength != 1) {
                          unset($data[0]);
                          unset($data[1]);  
                        }
                      }
                      else{
                        
                        unset($data[0]);
                        unset($data[1]);              
                      } 

                      $totalRecords = count($data);
                      
                      //proses convert ke order yg benar
                      if($totalRecords)
                      {

                        foreach ($adapterDataPayroll as $key => $value) {
                          $headerOrder[] = $value['HEADER_CONTENT'];
                        }

                        foreach ($data as $datakey => $datavalue) {

                          if (!empty($headerOrder)) {
                            foreach ($headerOrder as $key => $value) {

                              $headerOrderArr = explode(',', $value);

                              if (count($headerOrderArr) > 1) {
                                $i = 0;
                                $contentStr = '';
                                foreach ($headerOrderArr as $key2 => $value2) {

                                  if ($i != count($headerOrderArr)) {
                                    $contentStr .= $data[$datakey][$value2].' ';
                                  }
                                  $i++;
                                } 
                                $newData[$datakey][] = $contentStr;
                              }
                              else{
                                $newData[$datakey][] = $data[$datakey][$value];
                              }
                            } 
                          }
                          else{
                            $newData[$datakey][] = null;
                          }
                        }
                      }
                      
                      $mandatoryCheck = $this->validateField($newData, $adapterDataPayroll);

                      //auto mapping agar isi field file sesuai dengan yang di setup di m_mapping bo
                      $fixData = $this->autoMapping($newData, $adapterDataPayroll);
                      
                      if(empty($fixData)){
                        $fixData = $data; 
                        $totalRecords = count($data);
                      }
                      
                      if($totalRecords)
                      {
                        if($totalRecords <= $this->_maxRow)
                        {
                          $rowNum = 0;

                          $paramPayment = array(  "CATEGORY"        => "BULK PAYROLL",
                                                  "FROM"            => "I",
                                                  "PS_NUMBER"       => "",
                                                  "BS_ID"           => $BS_ID,
                                                  "PS_SUBJECT"      => $PS_SUBJECT,
                                                  "PSFILEID"        => $PSFILEID,
                                                  "PS_EFDATE"       => Application_Helper_General::convertDate($psefdatenow, $this->_dateDisplayFormat),
                                                  "PS_FILE"         => $fileName,
                                                  "_dateFormat"     => $this->_dateDisplayFormat,
                                                  "_dateDBFormat"   => $this->_dateDBFormat,
                                                  "_addBeneficiary" => $this->view->hasPrivilege('BADA'), // privi BADA (Add Beneficiary)
                                                  "_beneLinkage"    => $this->view->hasPrivilege('BLBU'), // privi BLBU (Linkage Beneficiary User)
                                                  "_createPB"       => $this->view->hasPrivilege('CBPW'), // privi CBPW (Create Bulk Credit Payment PB by Import File (CSV))
                                                  "_createDOM"      => $this->view->hasPrivilege('CBPI'), // privi CBPI (Create Bulk Credit Payment Domestic by Import File (CSV))
                                                  "_createREM"      => false,        // cannot create REM trx
                                                  );

                          $paramTrxArr = array();
                          // Zend_Debug::dump($fixData); die;

                          foreach ( $fixData as $row )
                          {
                            // if(count($row)==4)
                            // {
                              // var_dump($row);die;
                              $rowNum++;
                              $benefAcct    = trim($row[1]);
        //                      $benefName    = trim($row[1]);
                              $ccy      = "IDR";
                              $amount     = trim($row[2]);
                              $message    = trim($row[3]);
                              $reference    = '';
                              $sms_notif    = '';
                              $email_notif  = '';
                              $addMessage   = trim($row[4]);
        //                      $email      = trim($row[6]);
        //                      $phoneNumber  = trim($row[7]);
                              $type       = 'PB';
        //                      $bankCode     = trim($row[6]);
                              //$bankName     = trim($row[10]);
          //                    $bankCity = trim($row[10]);
        //                      $benefAdd   = trim($row[9]);
        //                      $citizenship  = strtoupper(trim($row[10]));
                              //$resident = strtoupper(trim($row[11]));

                              /*
                               * Change parameter into document
                               */
                              $fullDesc = array(
                                'BENEFICIARY_ACCOUNT'     => $benefAcct,
                                'BENEFICIARY_NAME'      => '',
                                'BENEFICIARY_ACCOUNT_CCY'   => $ccy,
                                'TRA_AMOUNT'        => $amount,
                                'TRA_MESSAGE'       => $message,
                                'REFNO'           => $addMessage,
                                'SMS_NOTIF'         => $sms_notif,
                                'EMAIL_NOTIF'         => $email_notif,
                                'BENEFICIARY_EMAIL'     => '',
                                'BENEFICIARY_MOBILE_PHONE_NUMBER' => '',
                                'TRANSFER_TYPE'       => 'PB',
                                //'CLR_CODE'          => $bankCode,
                                //'BENEFICIARY_BANK_NAME'   => $bankName,
          //                      'BENEFICIARY_CITY' => $bankCity,
                                'BENEFICIARY_ADDRESS'   => '',
                                'BENEFICIARY_CITIZENSHIP'   => ''
                                //'BENEFICIARY_RESIDENT' => $resident
                              );

                              $filter = new Application_Filtering();

                              $SMS_NOTIF    = $filter->filter($sms_notif, "SMS");
                              $EMAIL_NOTIF    = $filter->filter($email_notif, "EMAIL");

                              $TRA_AMOUNT     = $filter->filter($amount, "AMOUNT");
                              $TRA_MESSAGE    = $filter->filter($message, "TRA_MESSAGE");
                              $TRA_REFNO      = $filter->filter($addMessage, "TRA_REFNO");
                              $ACBENEF      = $filter->filter($benefAcct, "ACCOUNT_NO");
                              $ACBENEF_BANKNAME   = $filter->filter('', "ACCOUNT_NAME");
                              $ACBENEF_ALIAS    = $filter->filter('', "ACCOUNT_ALIAS");
                              $ACBENEF_EMAIL    = $filter->filter('', "EMAIL");
                              $ACBENEF_PHONE    = $filter->filter('', "MOBILE_PHONE_NUMBER");
                              $ACBENEF_CCY    = $filter->filter($ccy, "SELECTION");
                              $ACBENEF_ADDRESS  = $filter->filter('', "ADDRESS");
                              $ACBENEF_CITIZENSHIP= $filter->filter('', "SELECTION");
                              //$ACBENEF_RESIDENT= $filter->filter($resident, "SELECTION");
                              //$BANK_NAME      = $filter->filter($bankName, "BANK_NAME");
                              //$BANK_CITY      = $filter->filter($bankCity, "ADDRESS");
                              //$CLR_CODE     = $filter->filter($bankCode, "BANK_CODE");
                              $TRANSFER_TYPE    = $filter->filter($type, "SELECTION");

                              $TRA_AMOUNT_num   = Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

                              if($TRANSFER_TYPE == 'RTGS'){
                                $chargeType = '1';
                                $select = $this->_db->select()
                                        ->from('M_CHARGES_OTHER',array('*'))
                                        ->where("CUST_ID = ?",$this->_custIdLogin)
                                        ->where("CHARGES_TYPE = ?",$chargeType);
                                $resultSelecet = $this->_db->FetchAll($select);
                                $chargeAmt = $resultSelecet['0']['CHARGES_AMT'];
                              }
                              else if($TRANSFER_TYPE == 'SKN'){
                                $chargeType1 = '2';
                                $select1 = $this->_db->select()
                                        ->from('M_CHARGES_OTHER',array('*'))
                                        ->where("CUST_ID = ?",$this->_custIdLogin)
                                        ->where("CHARGES_TYPE = ?",$chargeType1);
                                $resultSelecet1 = $this->_db->FetchAll($select1);
                                $chargeAmt = $resultSelecet1['0']['CHARGES_AMT'];
                              }
                              else{
                                $chargeAmt = '0';
                              }

                              $filter->__destruct();
                              unset($filter);

                              $paramTrx = array("TRANSFER_TYPE"       => $TRANSFER_TYPE,
                                        "TRA_AMOUNT"        => $TRA_AMOUNT_num,
                                        "TRANSFER_FEE"        => $chargeAmt,
                                        "TRA_MESSAGE"         => $TRA_MESSAGE,
                                        "TRA_REFNO"         => $TRA_REFNO,
                                        "ACCTSRC"           => $ACCTSRC,
                                        "ACBENEF"           => $ACBENEF,
                                        "ACBENEF_CCY"         => $ACBENEF_CCY,
                                        "ACBENEF_EMAIL"       => $ACBENEF_EMAIL,
                                        "ACBENEF_PHONE"       => $ACBENEF_PHONE,
                                        "SMS_NOTIF"         => $SMS_NOTIF,
                                        "EMAIL_NOTIF"       => $EMAIL_NOTIF,
                                      // for Beneficiary data, except (bene CCY and email), must be passed by reference
                                        "ACBENEF_BANKNAME"      => $ACBENEF_BANKNAME,
                                        "ACBENEF_ALIAS"       => $ACBENEF_ALIAS,
                                        "ACBENEF_CITIZENSHIP"     => $ACBENEF_CITIZENSHIP,    // W: WNI, N: WNA
                                      //  "ACBENEF_RESIDENT"      => $ACBENEF_RESIDENT,   // R: RESIDENT, NR: NON-RESIDENT
                                        "ACBENEF_ADDRESS1"      => $ACBENEF_ADDRESS,
                                        "REFERENCE"         => $reference,
                                      //  "ACBENEF_ADDRESS2"      => &$ACBENEF_ADDRESS2,
                                      //  "ACBENEF_ADDRESS3"      => &$ACBENEF_ADDRESS3,

                                      //  "ORG_DIR"           => $ORG_DIR,
                                        //"BANK_CODE"         => $CLR_CODE,
                                      //  "BANK_NAME"         => $BANK_NAME,
                                      //  "BANK_BRANCH"         => $BANK_BRANCH,
                                      //  "BANK_ADDRESS1"       => $BANK_ADDRESS1,
                                      //  "BANK_ADDRESS2"       => $BANK_ADDRESS2,
                                      //  "BANK_ADDRESS3"       => $BANK_ADDRESS3,
                                       );

                              array_push($paramTrxArr,$paramTrx);
                          }
                        }
                        // kalo jumlah trx lebih dari setting
                        else
                        {
                          $error_msg[] = 'Error: The number of rows to be imported should not more than '.$this->_maxRow.'.';
                          $this->view->error    = true;
                          $this->view->report_msg = $this->displayError($error_msg);
                        }

                        $confirm = true;

                      }
                      else //kalo total record = 0
                      {
                        $error_msg[] = $this->language->_('Error').': '.$this->language->_('Wrong File Formatss').'.';
                        $this->view->error    = true;
                        $this->view->report_msg = $this->displayError($error_msg);
                      }

                  }
              //  
              }

            }
            else
            {
              $this->view->error = true;
              foreach($adapter->getMessages() as $key=>$val)
              {
                if($key=='fileUploadErrorNoFile')
                  $error_msg[] = $this->language->_('Error').': '.$this->language->_('File cannot be left blank. Please correct it').'.';
                else
                  $error_msg[] = $val;
                break;
              }
              $errors = $this->displayError($error_msg);
              $this->view->report_msg = $errors;
            }

        //akhir
        }else if($BULK_TYPE == '22'){
        //awal
            
            //$extension = 'txt';
            if (!empty($adapterDataMtm)) {
                  $extension = $adapterDataMtm[0]['FILE_FORMAT'];
                }
                else{
                  $extension = 'txt'; 
                }

            $fileName = $adapterDataMtm[0]['FILE_PATH'];
            $fixLength = $adapterDataMtm[0]['FIXLENGTH'];
            $fixLengthType = $adapterData[0]['FIXLENGTH_TYPE'];
            $fixLengthHeader = $adapterDataMtm[0]['FIXLENGTH_HEADER_ORDER'];
            $fixLengthHeaderName = $adapterDataMtm[0]['FIXLENGTH_HEADER_NAME'];
            $fixLengthContent = $adapterDataMtm[0]['FIXLENGTH_CONTENT_ORDER'];
            //$delimitedWith = $adapterDataMtm[0]['DELIMITED_WITH'];
            $delimitedWith = '|';
            $PSFILEID     = $filter->filter($this->_request->getParam('file_id'), "FILE_ID"); 
            $BS_ID        = $this->_request->getParam('bs_id');

                $filter   = new Application_Filtering();
                $adapter  = new Zend_File_Transfer_Adapter_Http ();

                $max    = $this->getSetting('max_import_single_payment');

                $adapter->setDestination ( $this->_destinationUploadDir );
                $extensionValidator = new Zend_Validate_File_Extension(array(false, $extension));
                $extensionValidator->setMessage(
                                  'Error: Extension file must be *.'.$extension
                                );

                $sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
                $sizeValidator->setMessage(
                              'Error: File siz must not more than '.$this->getSetting('Fe_attachment_maxbyte')
                            );

                $adapter->setValidators(array($extensionValidator,$sizeValidator));
                // die('here');
                if ($adapter->isValid ())
                {
                  
                  $sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
                  $NUMBER = Rand();
                    $random = md5($NUMBER);
                  $newFileName = $adapter->getFileName () . '-' .$random.'.txt';
                  
                  $fileName = substr($newFileName,85);

                  $adapter->addFilter ( 'Rename',$newFileName  );

                  if ($adapter->receive ())
                  {

                    $data = $this->convertFileToArray($newFileName, $extension, $delimitedWith);

                    $dateNow = date("dmY");
                    $dateUpload = $data[0][2];
                    if ($dateUpload != $dateNow) {
                      
                      $error_msg[] = 'Error: Wrong file_id/date.Please check your file and reupload again.';
                      $this->view->error    = true;
                      $this->view->report_msg = $this->displayError($error_msg);

                    }else{
                        $psefdatenow = date("Y-m-d");

                        //@unlink($newFileName);

                        if (!empty($adapterDataMtm)) {
                          //unset header if not fixlength
                          if ($fixLength != 1) {
                            unset($data[0]);  
                            unset($data[1]);
                          }
                        }
                        else{
                          //unset defaults
                          unset($data[0]);
                          unset($data[1]);
                         
                        }

                        $totalRecords = count($data);

                        //proses convert ke order yg benar
                        if($totalRecords)
                        {

                          foreach ($adapterDataMtm as $key => $value) {
                            $headerOrder[] = $value['HEADER_CONTENT'];
                          }

                          foreach ($data as $datakey => $datavalue) {

                            if (!empty($headerOrder)) {
                              foreach ($headerOrder as $key => $value) {

                                $headerOrderArr = explode(',', $value);

                                if (count($headerOrderArr) > 1) {
                                  $i = 0;
                                  $contentStr = '';
                                  foreach ($headerOrderArr as $key2 => $value2) {

                                    if ($i != count($headerOrderArr)) {
                                      $contentStr .= $data[$datakey][$value2].' ';
                                    }
                                    $i++;
                                  } 
                                  $newData[$datakey][] = $contentStr;
                                }
                                else{
                                  $newData[$datakey][] = $data[$datakey][$value];
                                }
                              } 
                            }
                            else{
                              $newData[$datakey][] = null;
                            }
                          }

                          //check mandatory yang bo setup saat buat business adapter profile
                          $mandatoryCheck = $this->validateField($newData, $adapterDataMtm);

                          //auto mapping agar isi field file sesuai dengan yang di setup di m_mapping bo
                          $fixData = $this->autoMapping($newData, $adapterDataMtm);

                        }

                        if(empty($fixData)){
                          $fixData = $data;
                          $totalRecords = count($data);
                        }
                        
                        if ($totalRecords && empty($mandatoryCheck)){
                          if($totalRecords <= $max)
                          {
                            $no =0;
                            $paramTrxArray = array();

                            foreach ( $fixData as $columns )
                            {
                              // if(count($columns)==14)
                              // {
                                $params['PAYMENT_SUBJECT']      = trim($columns[1]);
                                $params['SOURCE_ACCT_NO']       = trim($columns[2]);
                                $params['CCY']            = trim('IDR');
                                $params['BENEFICIARY_ACCT_NO']    = trim($columns[3]);
                                $params['BENEFICIARY_ACCT_CCY']   = trim('IDR');
                                $params['AMOUNT']           = trim($columns[4]);
                                $params['MESSAGE']          = trim($columns[8]);
                                $params['TRANS_PURPOSE']      = '';
                                $params['ADDITIONAL_MESSAGE']     = trim($columns[9]);
                                // $origDate = trim($columns[8]);
                                // $date = str_replace('/', '-', $origDate );
                                // $date2 = date_create($date);
                                $params['PAYMENT_DATE']       = trim($columns[7]);
                                $params['TRANSFER_TYPE']      = trim($columns[5]);
                                $params['BANK_CODE']        = trim($columns[6]);
                                $params['CUST_REF']         = '';
                                // if(!empty($columns[12]) || !empty($columns[13])){
                                //  $params['TRA_NOTIF']      = '2';
                                // }else{
                                //  $params['TRA_NOTIF']      = '1';
                                // }
                                $params['BENEFICIARY_NAME']       = '';
                                $params['BENEFICIARY_NAME']       = '';
                                $params['TRA_NOTIF']        = '';
                                $params['PS_SMS']         = '';
                                $params['PS_EMAIL']         = '';
                                $params['TREASURY_NUM']       = '';
                                // $params['LLD_TRANSACTION_PURPOSE']  = trim($columns[14]);
                                $params['LLD_TRANSACTION_PURPOSE']  = '';

                                $PS_SUBJECT     = $filter->filter($params['PAYMENT_SUBJECT'],"PS_SUBJECT");
                                $PS_EFDATE      = $filter->filter($params['PAYMENT_DATE'],"PS_DATE");
                                $TRA_AMOUNT     = $filter->filter($params['AMOUNT'],"AMOUNT");
                                $TRA_MESSAGE    = $filter->filter($params['MESSAGE'],"TRA_MESSAGE");
                                $TRA_REFNO      = $filter->filter($params['ADDITIONAL_MESSAGE'],"TRA_REFNO");
                                $ACCTSRC      = $filter->filter($params['SOURCE_ACCT_NO'],"ACCOUNT_NO");
                                $ACBENEF      = $filter->filter($params['BENEFICIARY_ACCT_NO'],"ACCOUNT_NO");
                                $ACBENEF_BANKNAME   = $filter->filter($params['BENEFICIARY_NAME'],"ACCOUNT_NAME");
                                $ACBENEF_CCY    = $filter->filter($params['CCY'],"SELECTION");
                                $CLR_CODE     = $filter->filter($params['BANK_CODE'], "BANK_CODE");
                                $TRANSFER_TYPE    = $filter->filter($params['TRANSFER_TYPE'], "SELECTION");
                                $CUST_REF       = $filter->filter($params['CUST_REF'], "SELECTION");
                                $BENEFICIARY_ACCT_CCY     = $filter->filter($params['BENEFICIARY_ACCT_CCY'], "BENEFICIARY_ACCT_CCY");
                                $TRA_NOTIF      = $filter->filter($params['TRA_NOTIF'], "TRA_NOTIF");
                                $TRA_SMS      = $filter->filter($params['PS_SMS'], "PS_SMS");
                                $TRA_EMAIL      = $filter->filter($params['PS_EMAIL'], "PS_EMAIL");
                                $TREASURY_NUM   = $filter->filter($params['TREASURY_NUM'], "TREASURY_NUM");
                                $LLD_TRANSACTION_PURPOSE    = $filter->filter($params['LLD_TRANSACTION_PURPOSE'], "LLD_TRANSACTION_PURPOSE");

                                $TRA_AMOUNT_num   = Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

                                if($TRANSFER_TYPE == 'RTGS'){
                                  $chargeType = '1';
                                  $select = $this->_db->select()
                                          ->from('M_CHARGES_OTHER',array('*'))
                                          ->where("CUST_ID = ?",$this->_custIdLogin)
                                          ->where("CHARGES_TYPE = ?",$chargeType);
                                  $resultSelecet = $this->_db->FetchAll($select);
                                  $chargeAmt = $resultSelecet['0']['CHARGES_AMT'];

                                  //$param['TRANSFER_FEE'] = $chargeAmt;
                                }
                                else if($TRANSFER_TYPE == 'SKN'){
                                  $chargeType1 = '2';
                                  $select1 = $this->_db->select()
                                          ->from('M_CHARGES_OTHER',array('*'))
                                          ->where("CUST_ID = ?",$this->_custIdLogin)
                                          ->where("CHARGES_TYPE = ?",$chargeType1);
                                  $resultSelecet1 = $this->_db->FetchAll($select1);
                                  $chargeAmt = $resultSelecet1['0']['CHARGES_AMT'];

                                  //$param['TRANSFER_FEE'] = $chargeAmt1;
                                }
                                else{
                                  $chargeAmt = '0';
                                  //$param['TRANSFER_FEE'] = $chargeAmt2;
                                }

                                if($BENEFICIARY_ACCT_CCY != $ACBENEF_CCY){
                                  $CROSS_CURR = '2';
                                }else{
                                  $CROSS_CURR = '1';
                                }

                                $paramPayment = array(
                                    "CATEGORY"          => "SINGLE PAYMENT",
                                    "FROM"              => "I",       // F: Form, I: Import
                                    "PS_NUMBER"         => "",
                                    "PS_SUBJECT"        => $PS_SUBJECT,
                                    "BS_ID"             => $BS_ID,
                                    "PS_EFDATE"         => $psefdatenow,
                                    "PSFILEID"          => $PSFILEID,
                                    "PS_FILE"           => $fileName,
                                    "_dateFormat"       => 'yyyy-MM-dd',
                                    "_dateDBFormat"     => $this->_dateDBFormat,
                                    "_addBeneficiary"   => $this->view->hasPrivilege('BADA'), // privi BADA (Add Beneficiary)
                                    "_beneLinkage"      => $this->view->hasPrivilege('BLBU'), // privi BLBU (Linkage Beneficiary User)
                                    "_createPB"         => $this->view->hasPrivilege('CRIP'),               // cannot create PB trx
                                    "_createDOM"        => $this->view->hasPrivilege('CRDI'), // privi CDFT (Create Domestic Fund Transfer)
                                    "_createREM"        => $this->view->hasPrivilege('CRIR'),               // cannot create REM trx
                                    "TRA_CCY"           => $BENEFICIARY_ACCT_CCY,
                                    "CROSS_CURR"        => $CROSS_CURR
                                );

                                $paramTrxArr[0] = array(
                                    "TRANSFER_TYPE"       => $TRANSFER_TYPE,
                                    "TRA_AMOUNT"          => $TRA_AMOUNT_num,
                                    "TRANSFER_FEE"        => $chargeAmt,
                                    "TRA_MESSAGE"         => $TRA_MESSAGE,
                                    "TRA_REFNO"           => $TRA_REFNO,
                                    "ACCTSRC"             => $ACCTSRC,
                                    "ACBENEF"             => $ACBENEF,
                                    "ACBENEF_CCY"         => $ACBENEF_CCY,
                                    "ACBENEF_EMAIL"       => $ACBENEF_EMAIL,
                                    "ACBENEF_BANKNAME"    => $ACBENEF_BANKNAME,
            //                        "ACBENEF_ALIAS"     => $ACBENEF_ALIAS,
                                    "ACBENEF_CITIZENSHIP" => $ACBENEF_CITIZENSHIP,    // 1/R: RESIDENT, 2/NR: NON-RESIDENT
                                    //                                "ACBENEF_ADDRESS1"      => $BANK_CITY,
            //                        "ACBENEF_RESIDENT"      => $ACBENEF_RESIDENT,
                                    "CUST_REF"             => $CUST_REF,
                                    "BENEFICIARY_RESIDENT" => $BENEFICIARY_RESIDENT,
                                    "BANK_CODE"            => $CLR_CODE,
            //                        "BENEFICIARY_BANK_NAME"   => $BANK_NAME,
            //                        "LLD_IDENTICAL"       => "",
            //                        "LLD_CATEGORY"        => "",
            //                        "LLD_RELATIONSHIP"      => "",
            //                        "LLD_PURPOSE"         => "",
            //                        "LLD_DESCRIPTION"       => "",
                                    "LLD_TRANSACTION_PURPOSE" => $LLD_TRANSACTION_PURPOSE,
                                    "BENEFICIARY_ID_NUMBER"   => $BENEFICIARY_ID_NUMBER,
                                    "BENEFICIARY_ID_TYPE"     => $BENEFICIARY_ID_TYPE,
                                    "BENEFICIARY_CITY_CODE"   => $BENEFICIARY_CITY_CODE,
                                    "BENEFICIARY_ACCT_CCY"    => $BENEFICIARY_ACCT_CCY,
                                    "PS_NOTIF"                => $TRA_NOTIF,
                                    "PS_SMS"                  => $TRA_SMS,
                                    "PS_EMAIL"                => $TRA_EMAIL,
                                    "REFERENCE"               => $TREASURY_NUM,

                                    "BANK_NAME"   => $BANK_NAME,

                                );

                                $arr[$no]['paramPayment'] = $paramPayment;
                                $arr[$no]['paramTrxArr'] = $paramTrxArr;

                                $paramTrx = array(
                                    "PS_SUBJECT"        => $PS_SUBJECT,
                                    "TRANSFER_TYPE"       => $TRANSFER_TYPE,
                                    "TRA_AMOUNT"        => $TRA_AMOUNT_num,
                                    "TRANSFER_FEE"        => $chargeAmt,
                                    "TRA_MESSAGE"         => $TRA_MESSAGE,
                                    "TRA_REFNO"         => $TRA_REFNO,
                                    "ACCTSRC"           => $ACCTSRC,
                                    "ACBENEF"           => $ACBENEF,
                                    "ACBENEF_CCY"         => $ACBENEF_CCY,
                                    "ACBENEF_EMAIL"       => $ACBENEF_EMAIL,
                                    "ACBENEF_BANKNAME"      => $ACBENEF_BANKNAME,
            //                        "ACBENEF_ALIAS"       => $ACBENEF_ALIAS,
                                    "ACBENEF_CITIZENSHIP"     => $ACBENEF_CITIZENSHIP,    // 1/R: RESIDENT, 2/NR: NON-RESIDENT
                                    //                                "ACBENEF_ADDRESS1"      => $BANK_CITY,
            //                        "ACBENEF_RESIDENT"      => $ACBENEF_RESIDENT,
                                    "CUST_REF"          => $CUST_REF,
                                    "BENEFICIARY_RESIDENT"    => $BENEFICIARY_RESIDENT,
                                    "BANK_CODE"         => $CLR_CODE,
            //                        "BENEFICIARY_BANK_NAME"   => $BANK_NAME,
            //                        "LLD_IDENTICAL"       => "",
            //                        "LLD_CATEGORY"        => "",
            //                        "LLD_RELATIONSHIP"      => "",
            //                        "LLD_PURPOSE"         => "",
            //                        "LLD_DESCRIPTION"       => "",
                                    "LLD_TRANSACTION_PURPOSE" => $LLD_TRANSACTION_PURPOSE,
                                    "BENEFICIARY_ID_NUMBER"   => $BENEFICIARY_ID_NUMBER,
                                    "BENEFICIARY_ID_TYPE"     => $BENEFICIARY_ID_TYPE,
                                    "BENEFICIARY_CITY_CODE"   => $BENEFICIARY_CITY_CODE,
                                    "BENEFICIARY_ACCT_CCY"    => $BENEFICIARY_ACCT_CCY,
                                    "PS_NOTIF"        => $TRA_NOTIF,
                                    "PS_SMS"          => $TRA_SMS,
                                    "PS_EMAIL"        => $TRA_EMAIL,
                                    "REFERENCE"         => $TREASURY_NUM,

                                    "BANK_NAME"   => $BANK_NAME,

                                );

                                array_push($paramTrxArray,$paramTrx);
                              // }
                              // else
                              // {
                              //  // die('ge');
                              //  $this->view->error    = true;
                              //  break;
                              // }
                              $no++;
                            }

                            if(!$this->view->error)
                            {
                             
                              $i = 0;
                              foreach($resWs as $key=>$dataAcctType){
                                //Zend_Debug::dump($dataAcctType);
                                $arr[$key]['paramTrxArr'][0]['ACCOUNT_TYPE'] = $dataAcctType['accountType'];
                              }

                              $sourceAccountType  = $resWs['accountType'];

                              // echo "<pre>";
                              // var_dump($arr);
                              // die();

                              $content['payment'] = $payment;
                              $content['arr']   = $arr;
                              $content['errorTrxMsg']   = $errorTrxMsg;
                              $content['sourceAccountType']   = $sourceAccountType;
                              $content['paramPayment'] = $paramPayment;
                              $content['paramTrxArray'] = $paramTrxArray;

                              $sessionNamespace = new Zend_Session_Namespace('confirmImportCreditBatch');
                              $sessionNamespace->content = $content;

                              $this->_redirect('/singlepayment/importbatch/confirm');
                            }

                          }
                          else
                          {
                            // die('here');
                            $this->view->error2 = true;
                            $this->view->max  = $max;
                          }
                        }else{
                          // die('here1');
                          $this->view->error = true;

                          if (!empty($adapter->getMessages())) {
                            $error_msg = array($adapter->getMessages());
                          }

                          if (!empty($mandatoryCheck)) {
                            foreach ($mandatoryCheck as $key => $value) {
                              array_push($error_msg, $value.' Field Cannot be left blank');
                            }
                          }

                          $this->view->report_msg = $this->displayError($error_msg);
                        }

                    }
   
                  }
                }
                else
                {
                  // die('here3');
                  $this->view->error = true;
                  $error_msg = array($adapter->getMessages());
                  $this->view->report_msg = $this->displayError($error_msg);
                }

        //akhir
        }

        if($confirm)
        {
          $content['paramPayment'] = $paramPayment;
          $content['paramTrxArr']  = $paramTrxArr;
          $content['errorTrxMsg']  = $errorTrxMsg;
          $content['payment'] = $payment;
          if($BULK_TYPE=='4'){
            $content['sourceAccountType'] = $sourceAccountType;
            $sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
            $sessionNamespace->content = $content;
            $this->_redirect('/multicredit/bulkbatch/confirm');

          }else if($BULK_TYPE=='5'){
            $sessionNamespace = new Zend_Session_Namespace('confirmBulkDebet');
            $sessionNamespace->content = $content;
            $this->_redirect('/multidebet/bulkbatch/confirm');
            
          }else if($BULK_TYPE=='11'){
            $sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
            $sessionNamespace->content = $content;
            $this->_redirect('/payrollpayment/bulk/confirm');
          }
        }
        
      }

    }

    public function downloadAction(){

      $this->_helper->viewRenderer->setNoRender();
      $this->_helper->layout()->disableLayout();

      $PS_FILE = $this->_getParam('PS_FILE');

       $file = LIBRARY_PATH.'/data/uploads/document/temp/'.$PS_FILE;

       $filetype=filetype($file);

       $filename=basename($file);

       header ("Content-Type: ".$filetype);

       header ("Content-Length: ".filesize($file));

       header ("Content-Disposition: attachment; filename=".$filename);

       readfile($file);
      
    }

    public function downloadtrx2Action()
    {
      $BS_ID      = trim(strip_tags($this->_getParam('BS_ID')));
      
      $select = $this->_db->select()
                ->from( array('P' => 'TEMP_BULKPSLIP'),
                          array('PS_TYPE'         => 'P.PS_TYPE',
                                'PS_TXCOUNT'      => 'P.PS_TXCOUNT',
                                'PS_TOTAL_AMOUNT' => 'P.PS_TOTAL_AMOUNT',
                                'PS_CCY'          => 'P.PS_CCY'))
                ->where('P.BS_ID =? ',$BS_ID);

      $pslip = $this->_db->fetchRow($select);

      if (!empty($pslip))
      {
        
        $uploadDate = date("dmY");
        $pscount    = $pslip['PS_TXCOUNT'];
        $psamount   = $pslip['PS_TOTAL_AMOUNT'];
        $psccy      = $pslip['PS_CCY'];

        if ($pslip["PS_TYPE"] == 4){
          
          $this->downloadonetomany($BS_ID,$uploadDate,$pscount,$psamount,$psccy);

        }
        elseif ($pslip["PS_TYPE"] == 5) {
          
          $this->downloadmanytoone($BS_ID,$uploadDate,$pscount,$psamount,$psccy);

        }elseif ($pslip["PS_TYPE"] == 11) {
          
          $this->downloadpayroll($BS_ID,$uploadDate,$pscount,$psamount,$psccy);
          
        }elseif ($pslip["PS_TYPE"] == 22) {
          
          $this->downloadmanytomany($BS_ID,$uploadDate,$pscount,$psamount,$psccy);

        }
        
      }
      else
      {
        $data = array();
        $data[0][0] = "Invalid Payment Number";
        $this->_helper->download->txt(array(),$data,null,$BS_ID);
        Application_Helper_General::writeLog('RPPY','Download TXT Document Upload Detail');
      }
    }

    protected function downloadonetomany($BS_ID,$uploadDate,$pscount,$psamount,$psccy)
    {
      
      $totalamount = explode('.',$psamount);
      
      $datakey = array(
                  "0" => '0',
                  "1" => '12345abcde',
                  "2" => $uploadDate,
                  "3" => $pscount,
                  "4" => $totalamount[0],
                  "5" => $psccy, 
                  "6" => '',
                );
      $headerData[] = $datakey;
      
      $select = $this->_db->select()
                ->from( array(  'TT' => 'TEMP_BULKTRANSACTION'),
                    array(
                        'BENEFICIARY_ACCOUNT'    => 'TT.BENEFICIARY_ACCOUNT',
                        'TRA_AMOUNT'             => 'TT.TRA_AMOUNT',
                        'TRANSFER_TYPE'          => 'TT.TRANSFER_TYPE',
                        'CLR_CODE'               => 'TT.CLR_CODE',
                        'TRA_MESSAGE'            => 'TT.TRA_MESSAGE',
                        'TRA_ADDITIONAL_MESSAGE' => 'TT.TRA_ADDITIONAL_MESSAGE',
                        'ERROR_DESC'             => 'TT.ERROR_DESC',
                        )
                    )
               
                ->where('TT.BS_ID = ?', $BS_ID);

      $data = $this->_db->fetchAll($select);

      $paramTrx1 = array(
                          "0" => '0',
                          "1" => 'BENEFICIARY ACCT (16)',
                          "2" => 'AMOUNT',
                          "3" => 'TRANSFER TYPE',
                          "4" => 'BANK CODE',
                          "5" => 'MESSAGE (30)',
                          "6" => 'ADDITIONAL MESSAGE (16)',
                          "7" => 'ERROR DESC',
                          "8" => '',
                  );
      $newData[] = $paramTrx1;
      $no = 1;

      foreach ($data as $p => $pTrx)
      {

        $conf = Zend_Registry::get('config');
        $transferType = $conf['transfer']['type'];
        $transferTypeFlip = array_flip($transferType['code']);
        $descTransferType = $transferType['desc'][$transferTypeFlip[$pTrx['TRANSFER_TYPE']]];

        $traamount = explode('.', $pTrx['TRA_AMOUNT']);
        $TRA_AMOUNT = $traamount[0];

        $paramTrx = array(  
                  "0" => $no,
                  "1" => $pTrx['BENEFICIARY_ACCOUNT'],
                  "2" => $TRA_AMOUNT,
                  "3" => $descTransferType,
                  "4" => $pTrx['CLR_CODE'],
                  "5" => $pTrx['TRA_MESSAGE'],
                  "6" => $pTrx['TRA_ADDITIONAL_MESSAGE'],
                  "7" => $pTrx['ERROR_DESC'],
                  "8" => '',
                );

        $newData[] = $paramTrx;
        
      }

      $this->_helper->download->txtBatch($headerData,$newData,null,$BS_ID);
      Application_Helper_General::writeLog('RPPY','Download TXT Document Upload Detail');  
      
    }

    protected function downloadmanytoone($BS_ID,$uploadDate,$pscount,$psamount,$psccy)
    {
      $totalamount = explode('.',$psamount);
      $datakey = array(
                  "0" => '0',
                  "1" => 'many2one01',
                  "2" => $uploadDate,
                  "3" => $pscount,
                  "4" => $totalamount[0],
                  "5" => $psccy,
                  "6" => '', 
                );

      $headerData[] = $datakey;
      
      $select = $this->_db->select()
                ->from( array(  'TT' => 'TEMP_BULKTRANSACTION'),
                    array(
                        'SOURCE_ACCOUNT'         => 'TT.SOURCE_ACCOUNT',
                        'TRA_AMOUNT'             => 'TT.TRA_AMOUNT',
                        'TRA_MESSAGE'            => 'TT.TRA_MESSAGE',
                        'TRA_ADDITIONAL_MESSAGE' => 'TT.TRA_ADDITIONAL_MESSAGE',
                        'ERROR_DESC'             => 'TT.ERROR_DESC',
                        )
                    )
     
                ->where('TT.BS_ID = ?', $BS_ID);

      $data = $this->_db->fetchAll($select);

      $paramTrx1 = array(
                          "0" => '0',
                          "1" => 'SOURCE ACCT (16)',
                          "2" => 'AMOUNT',
                          "3" => 'MESSAGE (30)',
                          "4" => 'ADDITIONAL MESSAGE (16)',
                          "5" => 'ERROR DESC',
                          "6" => '',
                  );
      
      $newData[] = $paramTrx1; 
      $no = 1;

      foreach ($data as $p => $pTrx)
      {

        $traamount = explode('.', $pTrx['TRA_AMOUNT']);
        $TRA_AMOUNT = $traamount[0];

        $paramTrx = array(  
                  "0" => $no,
                  "1" => $pTrx['SOURCE_ACCOUNT'],
                  "2" => $TRA_AMOUNT,
                  "3" => $pTrx['TRA_MESSAGE'],
                  "4" => $pTrx['TRA_ADDITIONAL_MESSAGE'],
                  "5" => $pTrx['ERROR_DESC'],
                  "6" => '',
                );

        $newData[] = $paramTrx;
        
      }

      $this->_helper->download->txtBatch($headerData,$newData,null,$BS_ID);
      Application_Helper_General::writeLog('RPPY','Download TXT Document Upload Detail');  
      
    }

    protected function downloadpayroll($BS_ID,$uploadDate,$pscount,$psamount,$psccy)
    {
      $totalamount = explode('.',$psamount);
      $datakey = array(
                        "0" => '0',
                        "1" => '8ulkTr4n5f3r',
                        "2" => $uploadDate,
                        "3" => $pscount,
                        "4" => $totalamount[0],
                        "5" => $psccy, 
                        "6" => '',
                      );

      $headerData[] = $datakey;
      
      $select = $this->_db->select()
                ->from( array(  'TT' => 'TEMP_BULKTRANSACTION'),
                    array(
                        'BENEFICIARY_ACCOUNT'    => 'TT.BENEFICIARY_ACCOUNT',
                        'TRA_AMOUNT'             => 'TT.TRA_AMOUNT',
                        'TRA_MESSAGE'            => 'TT.TRA_MESSAGE',
                        'TRA_ADDITIONAL_MESSAGE' => 'TT.TRA_ADDITIONAL_MESSAGE',
                        'ERROR_DESC'             => 'TT.ERROR_DESC',
                        )
                    )
    
                ->where('TT.BS_ID = ?', $BS_ID);

      $data = $this->_db->fetchAll($select);

      $paramTrx1 = array(
                          "0" => '0',
                          "1" => 'BENEFICIARY ACCT (16)',
                          "2" => 'AMOUNT',
                          "3" => 'MESSAGE (30)',
                          "4" => 'ADDITIONAL MESSAGE (16)',
                          "5" => 'ERROR DESC',
                          "6" => '',
                  );
      $newData[] = $paramTrx1;
      $no = 1;

      foreach ($data as $p => $pTrx)
      {

        $traamount = explode('.', $pTrx['TRA_AMOUNT']);
        $TRA_AMOUNT = $traamount[0];

        $paramTrx = array(  
                  "0" => $no,
                  "1" => $pTrx['BENEFICIARY_ACCOUNT'],
                  "2" => $TRA_AMOUNT,
                  "3" => $pTrx['TRA_MESSAGE'],
                  "4" => $pTrx['TRA_ADDITIONAL_MESSAGE'],
                  "5" => $pTrx['ERROR_DESC'],
                  "6" => '',
                );

        $newData[] = $paramTrx;
        
      }

      $this->_helper->download->txtBatch($headerData,$newData,null,$BS_ID);
      Application_Helper_General::writeLog('RPPY','Download TXT Document Upload Detail');  
      
    }

    protected function downloadmanytomany($BS_ID,$uploadDate,$pscount,$psamount,$psccy)
    {
      $totalamount = explode('.',$psamount);
      $datakey = array(
                        "0" => '0',
                        "1" => 'm2m0000001',
                        "2" => $uploadDate,
                        "3" => $pscount,
                        "4" => $totalamount[0],
                        "5" => $psccy, 
                        "6" => '',
                      );

      $headerData[] = $datakey;
      
      $select = $this->_db->select()
                ->from( array(  'TT' => 'TEMP_BULKTRANSACTION'),
                    array(
                        'PS_SUBJECT'             => 'TT.PS_SUBJECT',
                        'SOURCE_ACCOUNT'         => 'TT.SOURCE_ACCOUNT',
                        'BENEFICIARY_ACCOUNT'    => 'TT.BENEFICIARY_ACCOUNT',
                        'TRA_AMOUNT'             => 'TT.TRA_AMOUNT',
                        'TRANSFER_TYPE'          => 'TT.TRANSFER_TYPE',
                        'CLR_CODE'               => 'TT.CLR_CODE',
                        'PS_EFDATE'              => 'C.PS_EFDATE',
                        'TRA_MESSAGE'            => 'TT.TRA_MESSAGE',
                        'TRA_ADDITIONAL_MESSAGE' => 'TT.TRA_ADDITIONAL_MESSAGE',
                        'ERROR_DESC'             => 'TT.ERROR_DESC',
                        )
                    )
                ->joinLeft( array(  'C' => 'TEMP_BULKPSLIP' ),'TT.BS_ID = C.BS_ID',array())
                ->where('TT.BS_ID = ?', $BS_ID);

      $data = $this->_db->fetchAll($select);

      $paramTrx1 = array(
                          "0" => '0',
                          "1" => 'PAYMENT SUBJECT (50)',
                          "2" => 'SOURCE ACCT (16)',
                          "3" => 'BENEFICIARY ACCT (16)',
                          "4" => 'AMOUNT',
                          "5" => 'TRANSFER TYPE',
                          "6" => 'BANK CODE',
                          "7" => 'PAYMENT DATE',
                          "8" => 'MESSAGE (30)',
                          "9"  => 'ADDITIONAL MESSAGE (16)',
                          "10"  => 'ERROR DESC',
                          "11"  => '',
                  );
      $newData[] = $paramTrx1;
      $no = 1;

      foreach ($data as $p => $pTrx)
      {

        $conf = Zend_Registry::get('config');
        $transferType = $conf['transfer']['type'];
        $transferTypeFlip = array_flip($transferType['code']);
        $descTransferType = $transferType['desc'][$transferTypeFlip[$pTrx['TRANSFER_TYPE']]];

        $traamount = explode('.', $pTrx['TRA_AMOUNT']);
        $TRA_AMOUNT = $traamount[0];

        $paramTrx = array(  
                  "0" => $no,
                  "1" => $pTrx['PS_SUBJECT'],
                  "2" => $pTrx['SOURCE_ACCOUNT'],
                  "3" => $pTrx['BENEFICIARY_ACCOUNT'],
                  "4" => $TRA_AMOUNT,
                  "5" => $descTransferType,
                  "6" => $pTrx['CLR_CODE'],
                  "7" => $pTrx['PS_EFDATE'],
                  "8" => $pTrx['TRA_MESSAGE'],
                  "9" => $pTrx['TRA_ADDITIONAL_MESSAGE'],
                  "10" => $pTrx['ERROR_DESC'],
                  "11" => '',
                );

        $newData[] = $paramTrx;
        
      }

      $this->_helper->download->txtBatch($headerData,$newData,null,$BS_ID);
      Application_Helper_General::writeLog('RPPY','Download TXT Document Upload Detail');  
      
    }

    private function parseCSV($fileName){
      $csvData = false;
      try {
          $Csv = new Application_Csv (  $fileName, $separator = "," );
          $csvData = $Csv->readAll ();
          // var_dump($csvData);die;
        } catch ( Exception $e ) {
          echo nl2br ( $e->getTraceAsString () );
        }
        return $csvData;
    }

    private function convertFileToArray($newFileName, $extension, $delimitedWith){

      $file_contents = file_get_contents($newFileName);

          //if csv occured
          if ($extension === 'csv') {

              if (!empty($delimitedWith)) {
                  $data = $this->_helper->parser->parseCSV($newFileName, $delimitedWith);
              }
              //if fix length
              else if ($fixLength == 1) {

                $fileContents = file($newFileName);

                $contentOrder = $fixLengthContent;

                // if with header
                  if ($fixLengthType == 1) {
                      //karena yg diambil hanya order dri row ke 2 saja
                      $startArrIndex = 1;
                      $surplusIndex = 0;
                  }
                  else if ($fixLengthType == 3) {
                      //karena yg diambil hanya order dri row ke 1 saja
                      $startArrIndex = 0;
                      $surplusIndex = 1;
                  }

                  $contentOrderArr = explode(',', $contentOrder);

                  if (count($contentOrderArr) > 1) {
                      foreach ($fileContents as $key => $value) {
                          if ($key >= $startArrIndex) {
                              foreach ($contentOrderArr as $key2 => $value2) {
                                  //first order, startIndex from 0
                                  if ($key2 == 0) {
                                      $startIndex = 0;
                                      $endIndex = (int) $value2 + 1;

                                      $data[$key + $surplusIndex][] = substr($value,$startIndex,$endIndex);
                                  }
                                  else{
                                      $startIndex = (int) ($contentOrderArr[$key2 - 1] + 1);
                                      $endIndex = (int) ($value2 - ($startIndex - 1));

                                      $data[$key + $surplusIndex][] = substr($value,$startIndex,$endIndex);
                                  }
                              }       
                          }   
                      }
                  }
              }
              else{
                  $data = $this->_helper->parser->parseCSV($newFileName);
              }
          }
          //if txt occured
          else if ($extension === 'txt') {
            $delimitedWith = '|';
             $lines = file($newFileName);

             $checkMt940 = false;
             $checkMt101 = false;
             foreach ($lines as $line) {
                 if (strpos($line, '{1:') !== false) {
                     $checkMt940 = true;
                 }
                 else if (strpos($line, ':20:') !== false) {
                     $checkMt101 = true;
                 }
             }

             //if mt940 format
             if ($checkMt940) {
                 
                  $data = $this->_helper->parser->mt940($newFileName);
                  $mtFile = true;
             }
             else if($checkMt101){
                  $data = $this->_helper->parser->mt101($newFileName);
                  $mtFile = true;
             }
             else{
                //parse csv jg bs utk txt
                  if (!empty($delimitedWith)) {
                      $data = $this->_helper->parser->parseCSV($newFileName, $delimitedWith);
                  }
                  //if fix length
                  else if ($fixLength == 1) {

                      $fileContents = file($newFileName);

                  $contentOrder = $fixLengthContent;

                  // if with header
                    if ($fixLengthType == 1) {
                        //karena yg diambil hanya order dri row ke 2 saja
                        $startArrIndex = 1;
                        $surplusIndex = 0;
                    }
                    else if ($fixLengthType == 3) {
                        //karena yg diambil hanya order dri row ke 1 saja
                        $startArrIndex = 0;
                        $surplusIndex = 1;
                    }

                    $contentOrderArr = explode(',', $contentOrder);

                    if (count($contentOrderArr) > 1) {
                        foreach ($fileContents as $key => $value) {
                            if ($key >= $startArrIndex) {
                                foreach ($contentOrderArr as $key2 => $value2) {
                                    //first order, startIndex from 0
                                    if ($key2 == 0) {
                                        $startIndex = 0;
                                        $endIndex = (int) $value2 + 1;

                                        $data[$key + $surplusIndex][] = substr($value,$startIndex,$endIndex);
                                    }
                                    else{
                                        $startIndex = (int) ($contentOrderArr[$key2 - 1] + 1);
                                        $endIndex = (int) ($value2 - ($startIndex - 1));

                                        $data[$key + $surplusIndex][] = substr($value,$startIndex,$endIndex);
                                    }
                                }       
                            }   
                        }
                    }                   
                  }
                  else{
                    $data = $this->_helper->parser->parseCSV($newFileName, '|');
                  }
             }
          }
          //if json occured
          else if ($extension === 'json') {

              $datajson = json_decode($file_contents, 1);
              $i = 0;
              foreach ($datajson as $key => $value) {
                  if ($i == 0) {
                      $data[$i] = array_keys($value);
                      $data[$i + 1] = array_values($value);
                  }
                  else{
                      $data[$i+1] = array_values($value);
                  }

                  $i++;
              }
          }
          //if xml occured
          else if($extension === 'xml'){

              $xml = (array) simplexml_load_string($file_contents);

               $i = 0;
              foreach ($xml as $key => $value) {
                  foreach ($value as $key2 => $value2) {
                      if ($i == 0) {
                          $data[$i] = array_keys((array)$value2);
                          $data[$i + 1] = array_values((array)$value2);
                      }
                      else{
                          $data[$i+1] = array_values((array)$value2);
                      }

                      $i++;
                  }
              }

              for($i=0; $i<count($data); $i++){
                  if ($i > 0) {
                      foreach ($data[$i] as $key => $value) {
                          if (empty($value)) {
                              $data[$i][$key] = null;
                          }
                      }
                  }
              }

              // print_r($data);die();
          }
          else if($extension === 'xls' || $extension === 'xlsx'){
              try {
                  $inputFileType = IOFactory::identify($newFileName);
                  $objReader = IOFactory::createReader($inputFileType);
                  $objPHPExcel = $objReader->load($newFileName);
              } catch(Exception $e) {
                  die('Error loading file "'.pathinfo($newFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
              }

              $sheet = $objPHPExcel->getSheet(0);
              $highestRow = $sheet->getHighestRow();
              $highestColumn = $sheet->getHighestColumn();
               
              for ($row = 1; $row <= $highestRow; $row++){                        
                  $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row);

                  $data[$row - 1] = $rowData[0];
              }
          }

          return $data;
    }

    //validate mandatory
    private function validateField($data, $adapterData){

      foreach ($adapterData as $key => $value) {
        if ($value['MANDATORY'] == 1) {
          $mandatoryArr[] = $value['HEADER_INDEX'];
        }
      }

      $field = array();
      foreach ($data as $key => $value) {

        foreach ($mandatoryArr as $key2 => $value2) {
          if(empty($value[$value2])){
            $field[] = $adapterData[$value2]['HEADER_NAME'];
          }
        }
      }
      return $field;
    }

    //validate 
    private function autoMapping($data, $adapterData){

      foreach ($adapterData as $key => $value) {
        if (!empty($value['HEADER_FUNCTION'])) {

          $mappingTag = explode('_', $value['HEADER_FUNCTION']);

          $functionField[] = array(
            'headerIndex' => $value['HEADER_INDEX'],
            'mappingTag' => $mappingTag[0]
          );
        }
      }

      $newData = array();
      foreach ($functionField as $key => $value) {

        if (!empty($newData)) {

          foreach ($newData as $newdatakey => $newdatavalue) {
            //select m_mapping
            $select = $this->_db->select()
                ->from('M_MAPPING',array('*'))
                ->where("TAG = ?", $value['mappingTag'])
                ->where("TEXT LIKE ?", $newdatavalue[$value['headerIndex']]);
            $mappingData = $this->_db->fetchRow($select);

            if (!empty($mappingData)) {
              $newdatavalue[$value['headerIndex']] = $mappingData['MAPPING_TO'];

            }
            $newData[$newdatakey] = $newdatavalue;
          }
        }
        else{

          foreach ($data as $datakey => $datavalue) {
            //select m_mapping
            $select = $this->_db->select()
                ->from('M_MAPPING',array('*'))
                ->where("TAG = ?", $value['mappingTag'])
                ->where("TEXT LIKE ?", $datavalue[$value['headerIndex']]);
            $mappingData = $this->_db->fetchRow($select);

            if (!empty($mappingData)) {
              $datavalue[$value['headerIndex']] = $mappingData['MAPPING_TO'];

            }
            $newData[$datakey] = $datavalue;
          }
        }
      }

      return $newData;
    }

}