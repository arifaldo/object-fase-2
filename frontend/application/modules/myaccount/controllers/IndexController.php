<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/SystemBalance.php';
require_once 'Crypt/AESMYSQL.php';
require_once 'General/CustomerUser.php';

class myaccount_IndexController extends myaccount_Model_Myaccount
{

	private $minLengthPassword;
	private $maxLengthPassword;
	private $userData;

	public function initController()
	{
		$settings =  new Settings();

		$this->minLengthPassword = $settings->getSetting('minfpassword');
		$this->maxLengthPassword = $settings->getSetting('maxfpassword');

		//get user data                        
		$resultData = $this->getUserData($this->_custIdLogin, $this->_userIdLogin);
		foreach ($resultData as $key => $value) {
			if (empty($value)) {
				$resultData[$key] = '-';
			}
		}

		$this->userData = $resultData;
	}

	public function indexAction()
	{

		$setting = new Settings();
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$pw_hash = md5($enc_salt . $enc_pass);
		$rand = $this->_userIdLogin . date('dHis') . $pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;
		$password = $sessionNamespace->token;

		$this->_helper->layout()->setLayout('newlayout');

		$this->view->minlengthpass = $this->minLengthPassword;
		$this->view->maxlengthpass = $this->maxLengthPassword;

		$this->view->compName = $this->_custNameLogin;

		$this->view->userData = $this->userData;

		//-------------------------------------------user privilege-------------------------------------------------------------
		$fuser_id  = $this->_custIdLogin . $this->_userIdLogin;
		$this->view->custidlogin = $this->_custIdLogin;
		$this->view->useridlogin = $this->_userIdLogin;


		$system_type = $setting->getSetting('system_type');

		$privilege = $this->_db->select()
			->from('M_FPRIVI_USER')
			->where('FUSER_ID =' . $this->_db->quote($fuser_id))
			->query()->fetchAll();
		$this->view->priviAll = Application_Helper_Array::listArray($this->getPrivileges($system_type), 'FPRIVI_ID', 'FPRIVI_DESC');
		$priviList  = $this->_db->select()
			->from(array('u' => 'M_FPRIVI_USER'))
			->where('UPPER(u.FUSER_ID)=' . $this->_db->quote((string)$fuser_id))
			->query()->fetchAll();

		$priviListArr = Application_Helper_Array::simpleArray($priviList, 'FPRIVI_ID');
		$this->view->priviList = $priviListArr;

		if (count($privilege) > 0) {
			$priviListArr = Application_Helper_Array::simpleArray($privilege, 'FPRIVI_ID');
			$this->view->priviview = $priviListArr;
		}

		//modifikasi privilege agar bisa ditampilkan sesuai golongan
		$this->view->getModuleDescArr = $this->getModuleDescArr();

		$privilege_final = array();
		$privilege_final_modif = array();

		$system_type = $setting->getSetting('system_type');

		$this->view->priviAll = Application_Helper_Array::listArray($this->getPrivileges($system_type), 'FPRIVI_ID', 'FPRIVI_DESC');

		$getprivilege = $this->getprivilege($this->_custIdLogin, $this->_userIdLogin);

		foreach ($getprivilege as $row) {

			$fprivi_moduleid = trim($row['FPRIVI_MODULEID']);
			$privilege_final[$fprivi_moduleid][] = $row;
			$privilege_final_modif = $this->modifPrivi($privilege_final);
		}

		$this->view->fprivilege = $privilege_final_modif;
		$this->view->template   = Application_Helper_Array::listArray($this->getTemplate(), 'FTEMPLATE_ID', 'FTEMPLATE_DESC');

		$priviTemplate = array();
		foreach ($this->getPriviTemplate() as $row) {
			$priviTemplate[$row['FTEMPLATE_ID']][$row['FPRIVI_ID']] = $row['FPRIVI_DESC'];
		}

		$this->view->priviTemplate = $priviTemplate;

		$flag_template = 0;
		$user_role     = '';
		$template_id   = '';
		if (count($priviTemplate) > 0) {
			foreach ($priviTemplate as $key => $row) {
				$count_row = count($row);
				$count_priviListArr = count($priviListArr);
				if ($count_row == $count_priviListArr) {
					$arr_diff = array_diff_key(array_flip($priviListArr), $row);
					if (count($arr_diff) == 0) {
						$template_id = $key;
						$flag_template = 1;
						break;
					}
				}
			}
		}

		if ($flag_template == 0) $user_role = 'Custom';
		else if ($flag_template == 1) {
			$select = $this->_db->select()
				->from('M_FTEMPLATE')
				->where('UPPER(FTEMPLATE_ID)=' . $this->_db->quote($template_id))
				->query()->fetch();
			$user_role = $select['FTEMPLATE_DESC'];
		}

		$this->view->user_role = $user_role;

		//-------------------------------------------end user privilege-------------------------------------------------------------

		//-------------------------------------------Daily maker limit-------------------------------------------------------------

		$dailyMakerLimit = $this->_db->select()
			->from('M_DAILYLIMIT', array('*'))
			->where('USER_LOGIN = ?', $this->_userIdLogin)
			->where('CUST_ID = ?', $this->_custIdLogin)
			->query()->fetchAll();

		$this->view->dailyMakerLimit = $dailyMakerLimit;


		//-------------------------------------------End Daily maker limit-------------------------------------------------------------

		//-------------------------------------------Transaction limit-------------------------------------------------------------

		$transactionLimit1 = $this->_db->select()
			->from(array('A' => 'M_CUSTOMER_ACCT'), array('ACCT_NO', 'ACCT_NAME', 'ACCT_ALIAS_NAME', 'CCY_ID', 'MAXLIMIT' => '(SELECT MAXLIMIT FROM M_MAKERLIMIT WHERE USER_LOGIN=\'' . $this->_userIdLogin . '\' AND CUST_ID=\'' . $this->_custIdLogin . '\' AND ACCT_NO=A.ACCT_NO AND MAKERLIMIT_STATUS = 1)'))
			->where('UPPER(A.CUST_ID)=' . $this->_db->quote(strtoupper($this->_custIdLogin)))
			->where('A.ACCT_STATUS <> 3')
			->query()->fetchAll();

		$transactionLimit = $this->_db->select()
			->from(array('M' => 'M_MAKERLIMIT'), array('*'))
			->joinLeft(array('MU' => 'M_USER'), 'MU.USER_ID=M.USER_LOGIN AND MU.CUST_ID=M.CUST_ID', array('MU.USER_FULLNAME'))
			->joinLeft(array('D' => 'M_BANK_TABLE'), 'M.BANK_CODE = D.BANK_CODE', array('D.BANK_NAME'))
			->where('M.MAXLIMIT != ?', '0.00')
			->where('UPPER(M.CUST_ID)=' . $this->_db->quote(strtoupper($this->_custIdLogin)))
			->where('UPPER(M.USER_LOGIN)=' . $this->_db->quote(strtoupper($this->_userIdLogin)))
			->order('D.BANK_NAME ASC')
			->query()->fetchAll();

		$this->view->transactionLimit = $transactionLimit;

		$getUserDetail = $this->_db->select()
			->from('M_USER', [
				'USER_ID',
				'USER_FULLNAME'
			])
			->where('USER_ID = ?', $this->_userIdLogin)
			->where('CUST_ID = ?', $this->_custIdLogin)
			->query()->fetch();


		$this->view->userDetail = $getUserDetail;

		Application_Helper_General::writeLog('MACC', 'Lihat Akun Saya');

		//-------------------------------------------End Transaction limit-------------------------------------------------------------

	}

	public function changephoneAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$phone = $this->_getParam('phone');
		$pass = $this->_getParam('pass');

		$curPass =  $this->userData['USER_PASSWORD'];

		$combine = ((string)$this->_userIdLogin . (string)$this->_custIdLogin);
		$key = md5($combine);
		$encrypt = new Crypt_AESMYSQL();
		$password = md5($encrypt->encrypt($pass, $key));

		if ($curPass != $password) {
			echo "invalidpass";
		} else {
			try {
				$where['CUST_ID = ?'] = $this->_custIdLogin;
				$where['USER_ID = ?'] = $this->_userIdLogin;
				$updateArr = array('USER_PHONE' => $phone);
				$this->_db->update('M_USER', $updateArr, $where);

				Application_Helper_General::writeLog('COHP', 'Change My Phone, Company Code : ' . $this->_custIdLogin . ', User ID : ' . $this->_userIdLogin . ' to ' . $phone);

				echo 'success';
			} catch (Exception $e) {
				echo 'failed';
			}
		}
	}

	public function changeemailAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$email = $this->_getParam('email');
		$pass = $this->_getParam('pass');

		$curPass =  $this->userData['USER_PASSWORD'];

		$combine = ((string)$this->_userIdLogin . (string)$this->_custIdLogin);
		$key = md5($combine);
		$encrypt = new Crypt_AESMYSQL();
		$password = md5($encrypt->encrypt($pass, $key));

		$select = $this->_db->select()
			->from('M_USER', array('USER_EMAIL', 'USER_ID', 'USER_FULLNAME'))
			->where('UPPER(CUST_ID)=' . $this->_db->quote((string)$this->_custIdLogin))
			->where('USER_EMAIL = ?', $email)
			->where('USER_STATUS != 3');

		$data = $this->_db->fetchRow($select);

		$checkDuplicateEmail = false;
		if (empty($data)) {

			$checkDuplicateEmail = true;
		} else {
			if ($data['USER_ID'] == $this->_userIdLogin) {
				$checkDuplicateEmail = true;
			}
		}

		if ($curPass != $password) {
			echo "invalidpass";
		} else if (!$checkDuplicateEmail) {
			echo "duplicateemail";
		} else {
			try {
				$where['CUST_ID = ?'] = $this->_custIdLogin;
				$where['USER_ID = ?'] = $this->_userIdLogin;
				$updateArr = array('USER_EMAIL' => $email);
				$this->_db->update('M_USER', $updateArr, $where);

				Application_Helper_General::writeLog('MACC', 'Change My Email, Company Code : ' . $this->_custIdLogin . ', User ID : ' . $this->_userIdLogin . ' to ' . $email);

				// $this->_db->insert('T_FACTIVITY', array(
				// 	'LOG_DATE'         => new Zend_Db_Expr('now()'),
				// 	'CUST_ID'           => $this->_custIdLogin,
				// 	'USER_ID'           => $this->_userIdLogin,
				// 	'USER_NAME'           => $data['USER_FULLNAME'],
				// 	'ACTION_DESC'       => 'COEM',
				// 	'ACTION_FULLDESC'   => 'Change My Email, Company Code : ' . $this->_custIdLogin . ', User ID : ' . $this->_userIdLogin . ' to ' . $email,
				// ));
				echo 'success';
				// $this->view->success_mail = 'Change Email Success';
				// $errDesc['result'] = 'success';
				$this->view->successmail = true;
				$this->view->success_mail = 'Change Email Success';
			} catch (Exception $e) {
				echo 'failed';
			}
		}
	}

	public function changepassAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$oldpass = $this->_getParam('curpass');
		$newpass = $this->_getParam('newpass');

		$errDesc = array();
		$validator = new Zend_Validate_Alnum();

		if ((strlen($newpass) < $this->minLengthPassword) || (strlen($newpass) > $this->maxLengthPassword))
			$errDesc['newpass'] = $this->language->_('Error: Minimum char') . " " . $this->minLengthPassword . " " . $this->language->_('and maximum char') . " " . $this->maxLengthPassword . " " . $this->language->_('for New Password length') . ".";
		elseif (Application_Helper_General::checkPasswordStrength($newpass) < 3)
			$errDesc['newpass'] = $this->language->_("Error: New Password must containt at least one uppercase character, one lowercase character and one number");
		if ($newpass == $oldpass) {
			$errDesc['newpass'] = $this->language->_("Error: Password must be different from old password");
		}

		// elseif (! ($validator->isValid($newpass)) ) {
		// 	$errDesc['newpass'] = $this->language->_("Error: New Password may not contain whitespaces or special characters");
		// }


		$CustomerUser =  new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$result = array();
		$failed = 0;
		if (count($errDesc) > 0) $failed = 1;
		$result = $CustomerUser->changePassword($oldpass, $newpass, $failed);

		if ($result == true) {

			// $this->_db->insert('T_FACTIVITY', array(
			// 	'LOG_DATE'         => new Zend_Db_Expr('now()'),
			// 	'CUST_ID'           => $this->_custIdLogin,
			// 	'USER_ID'           => $this->_userIdLogin,
			// 	'USER_NAME'           => $data['USER_FULLNAME'],
			// 	'ACTION_DESC'       => 'COPW',
			// 	'ACTION_FULLDESC'   => 'Change My Password, Company Code : ' . $this->_custIdLogin . ', User ID : ' . $this->_userIdLogin,
			// ));

			Application_Helper_General::writeLog('MACC', 'Change My Password, Company Code : ' . $this->_custIdLogin . ', User ID : ' . $this->_userIdLogin);
		}

		if ((is_array($result)  && $result !== true) ||  $failed == 1) {
			if (count($result) > 0) {
				foreach ($result as $key => $value) {
					$errDesc['errorMsg'][] = $value;
				}
			}

			$errDesc['result'] = 'failed';
		} else {

			$errDesc['result'] = 'success';
			$this->view->successpass = true;
			$this->view->success_pass = 'Change Password Success';
		}

		if (isset($errDesc['newpass'])) {
			$errDesc['errorMsg'][] = $errDesc['newpass'];
		}

		unset($errDesc['newpass']);
		unset($errDesc['oldpass']);

		echo json_encode($errDesc);
	}

	public function changependingtaskAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		$value = $this->_getParam('value');

		try {
			$where['CUST_ID = ?'] = $this->_custIdLogin;
			$where['USER_ID = ?'] = $this->_userIdLogin;
			$updateArr = array('USER_PENDING_NOTIF' => $value);
			$this->_db->update('M_USER', $updateArr, $where);

			echo 'success';
		} catch (Exception $e) {
			echo 'failed';
		}
	}

	public function sendtokenAction()
	{
		$this->_helper->viewRenderer->setNoRender();
		$this->_helper->layout()->disableLayout();

		// $tokenType = $this->_getParam('id');
		$userId = $this->_userIdLogin;

		// var_dump($userId);die;	

		$token = SGO_Helper_GeneralFunction::generateToken($userId);
		$selectQuery    = "SELECT USER_EMAIL,USER_PHONE FROM M_USER
		WHERE CUST_ID = " . $this->_db->quote($this->_custIdLogin) . " AND USER_ID = " . $this->_db->quote($this->_userIdLogin) . " ";
		// echo $selectQuery;
		$userData =  $this->_db->fetchAll($selectQuery);
		$userData = $userData['0'];

		$Settings = new Settings();
		$allSetting = $Settings->getAllSetting();
		$smsTemplate 				= $allSetting['sms_token'];
		$datatemp = array(
			'[[token]]' => $token
		);
		$smstokenNotif = strtr($smsTemplate, $datatemp);
		//var_dump($userData['USER_EMAIL'])
		//Application_Helper_Email::sendEmail($userData['USER_EMAIL'], 'Token Notification', $smstokenNotif);
		// echo $smstokenNotif;
		$sms = new Service_SMS($userData['USER_PHONE'], $smstokenNotif);
		$sms->OutgoingSMS();

		// var_dump($smstokenNotif);die;
		echo $smstokenNotif;
		return true;
		//echo $optHtml;
	}
}
