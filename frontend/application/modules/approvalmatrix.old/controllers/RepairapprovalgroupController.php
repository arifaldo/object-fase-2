<?php

require_once 'Zend/Controller/Action.php';

class approvalmatrix_RepairapprovalgroupController extends approvalmatrix_Model_Customer
{
  protected $_moduleDB = 'RTF'; //masih harus diganti

  public function indexAction()
  {
    // $this->_helper->layout()->setLayout('newlayout');
    $this->_helper->layout()->setLayout('popup');
    $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
    if (count($temp) > 1) {
      if ($temp[0] == 'F' || $temp[0] == 'S') {
        if ($temp[0] == 'F')
          $this->view->error = 1;
        else
          $this->view->success = 1;
        $msg = '';
        unset($temp[0]);
        foreach ($temp as $value) {
          if (!is_array($value))
            $value = array($value);
          $msg .= $this->view->formErrors($value);
        }
        $this->view->report_msg = $msg;
      }
    }

    $change_id = $this->_getParam('changes_id');
    $change_id = (Zend_Validate::is($change_id, 'Digits')) ? $change_id : 0;

    $this->view->change_id = $change_id;



    // $select->order($sortBy.' '.$sortDir); 

    /* $selectcust =  $this->_db->select()
                  ->from( 'M_CUSTOMER',array('CUST_ID','CUST_ID'));
                  //->where('PASSWORD = \''.$v_password_old.'\'');
        
        // echo $v_password;
      $cust = $this->_db->fetchAll($selectcust);  
      $arrcust = Application_Helper_Array::listArray($cust,'CUST_ID','CUST_ID');

      $this->view->arrcust = $arrcust;*/

    // $cust_id = strtoupper($this->_getParam('cust_id'));
    $cust_id = strtoupper($this->_custIdLogin);
    $cust_id = (Zend_Validate::is($cust_id, 'Alnum') && Zend_Validate::is($cust_id, 'StringLength', array('min' => 1, 'max' => $this->_custIdLength))) ? $cust_id : null;



    $select = $this->_db->select()
      ->from(array('MAB' => 'TEMP_APP_GROUP_USER'))
      // ->join(array('MABG'=>'M_APP_BOUNDARY_GROUP'), 'MAB.BOUNDARY_ID = MABG.BOUNDARY_ID', array('GROUP_USER_ID'))
      ->where('MAB.CHANGES_ID = ?', $change_id)
      ->where('MAB.CUST_ID = ?', (string)$cust_id);

    $result = $this->_db->fetchAll($select);

    $listgroup = $this->_db->select()
      ->from(array('MAB' => 'TEMP_APP_GROUP_USER'))
      // ->join(array('MABG'=>'M_APP_BOUNDARY_GROUP'), 'MAB.BOUNDARY_ID = MABG.BOUNDARY_ID', array('GROUP_USER_ID'))
      ->where('MAB.CUST_ID = ?', (string)$cust_id)
      ->where('MAB.CHANGES_ID = ?', $change_id)
      ->group('MAB.GROUP_USER_ID');
    $resultlist = $this->_db->fetchAll($listgroup);

    // temuan to juni
    try {
      $listPriviUser = $this->_db->select()
        ->from(array('MFU' => 'M_FPRIVI_USER'), ['MFU.FUSER_ID'])
        ->where('MFU.FUSER_ID LIKE (?)', '%' . $cust_id . '%')
        ->where('MFU.FPRIVI_ID = ?', 'APBG')
        ->query()->fetchAll(Zend_Db::FETCH_COLUMN);
  
      $lisUser = array_map(function ($val) use ($cust_id) {
        return str_replace($cust_id, '', $val);
      }, $listPriviUser);

    } catch (\Throwable $th) {
      //throw $th;
    }
    // temuan to juni

    foreach ($resultlist as $key => $value) {
      // temuan to juni
      $selectUser = $this->_db->select()
        ->from(array('TAGU' => 'TEMP_APP_GROUP_USER'), 'TAGU.USER_ID')
        ->where('TAGU.GROUP_USER_ID = ?', $value['GROUP_USER_ID'])
        ->where('TAGU.CHANGES_ID = ?', $change_id)
        ->where('TAGU.USER_ID IN (?)', $lisUser ?: '')
        ->query()->fetchAll(Zend_Db::FETCH_COLUMN);

      $userlist = implode(',', $selectUser);

      $resultlist[$key]['USERLIST'] = $userlist;
      // temuan to juni

      // $selectUser = $this->_db->select()
      //   ->from(array('TEMP_APP_GROUP_USER'), array('USER_ID'))
      //   ->where('GROUP_USER_ID = ?', $value['GROUP_USER_ID'])
      //   ->where('CHANGES_ID = ?', $change_id)
      //   // echo $selectUser;die;
      //   ->query()->fetchall();
      // foreach ($selectUser as $val) {
      //   if (empty($userlist))
      //     $userlist .= $val['USER_ID'];
      //   else
      //     $userlist .= ', ' . $val['USER_ID'];
      // }

      // $resultlist[$key]['USERLIST'] = $userlist;
      // $userlist = '';
    }

    $this->view->listgroup  = $resultlist;

    if ($result) {
      $this->view->data = $result;
      foreach ($result as $key => $value) {
        $special = 'S_' . $cust_id;
        if ($value['GROUP_USER_ID'] == $special) {
          $this->view->spname = $value['GROUP_NAME'];
          $selectUser = $this->_db->select()
            ->from(array('TEMP_APP_GROUP_USER'), array('USER_ID'))
            ->where('GROUP_USER_ID = ?', $special)
            ->where('CHANGES_ID = ?', $change_id)
            // echo $selectUser;die;
            ->query()->fetchall();
          $userlist = '';
          foreach ($selectUser as $val) {
            if (empty($userlist))
              $userlist .= $val['USER_ID'];
            else
              $userlist .= ',' . $val['USER_ID'];
          }
          $this->view->splist = $userlist;
        }
      }
    }





    $cust_id = (Zend_Validate::is($cust_id, 'Alnum') && Zend_Validate::is($cust_id, 'StringLength', array('min' => 1, 'max' => $this->_custIdLength))) ? $cust_id : null;
    // print_r($cust_id);die;
    $selectuser = $this->_db->select()
      ->from(array('A' => 'M_USER'))
      ->join(array('B' => 'M_FPRIVI_USER'), 'CONCAT(A.`CUST_ID`,A.`USER_ID`) = B.FUSER_ID', array('*'))
      ->where('A.USER_STATUS != ?', 3);;
    $selectuser->where("B.FPRIVI_ID =  ?", 'APBG');
    $selectuser->where("A.CUST_ID = " . $this->_db->quote($cust_id));
    //$selectuser->where("A.USER_STATUS !=  ?",'3');
    // echo $selectuser;die;
    $this->view->dataact = $selectuser->query()->fetchAll();

    $submit = $this->_getParam('submit');
    $reset = $this->_getParam('reset');

    // $select2 = $this->_db->select()
    //           ->from('TEMP_APP_BOUNDARY');
    // $select2 -> where("CUST_ID LIKE ".$this->_db->quote($cust_id));
    // // echo $select2;die;
    // $cek = $select2->query()->FetchAll();


    //     $cektrx = $this->_db->select()
    //           ->from('T_PSLIP');
    // $cektrx -> where("CUST_ID = ? ",$cust_id);
    // $cektrx -> where("PS_STATUS = ? ",'1');
    // // echo $select2;die;
    // $cektrx = $cektrx->query()->FetchAll();  

    // if(!empty($cektrx) || !empty($cek)){
    //   $this->view->error = true;
    //   $this->view->error_msg  = 'Cannot add/edit record before previous transaction is approved';
    // }

    // print($cek);die;
    // if(!$cek)
    // {
    // print_r($submit);
    // die;
    if ($submit) {
      //   if($submit && $this->view->hasPrivilege('BMUD'))
      // {
      // $cust = $this->_getParam('cust_id');

      $data = $this->_request->getParams();

      $content = array();
      $content['sg_name'] = $data['sg_group'];
      $content['sg_user'] = $data['sg_user'];
      $content['cust_id'] = $this->_custIdLogin;
      $content['g_list'] = $data['group'];
      $content['g_id'] = $data['groupid'];
      $content['g_user'] = $data['user'];
      $content['change_id'] = $change_id;

      $sessionNamespace = new Zend_Session_Namespace('approvalgroup');
      // $sessionNamespace->mode = 'Add';
      $sessionNamespace->content = $content;
      //echo '<pre>';
      //print_r($content);die;
      // $this->_redirect('/directdebit/index/confirm');
      $url = '/approvalmatrix/repairapprovalpolicy';
      $this->_redirect($url);
      // Application_Helper_General::writeLog('BMUD','Update Boundary Member ('.$custid.')');
    }
    // }
    // if($cek)
    // {
    //   $docErr = "*".$this->language->_('No changes allowed for this record while awaiting approval for previous change')."";
    //   $this->view->error = $docErr;
    //   $this->view->changestatus = "disabled";
    // }


    if ($cust_id) {
      $resultdata = $this->getCustomer($cust_id);
      // print_r($resultdata);die;
      $listGroup = $this->_db->fetchAll(
        $this->_db->select()
          ->from('TEMP_APP_GROUP_USER', array('USER_ID', 'GROUP_NAME'))
          ->where('GROUP_USER_ID=' . $this->_db->quote('S_' . $cust_id))
          ->where('CHANGES_ID = ?', $change_id)
          ->where('CUST_ID=' . $this->_db->quote($cust_id))
      );
      // print_r($listGroup);die;
      $listuser = '';
      if (!empty($listGroup)) {
        foreach ($listGroup as $key => $value) {
          if ($value['USER_ID'] == '') {
            continue;
          }
          $listuser .= $value['USER_ID'] . ',';
        }
        $listuser = rtrim($listuser, ",");
        // echo "#";
        // print_r($listuser);  
        $this->view->splist = $listuser;
        $this->view->spname = $listGroup['0']['GROUP_NAME'];
      }
      // print_r($listGroup);die;

      if ($resultdata['CUST_ID']) {
        $this->view->cust_id      = strtoupper($resultdata['CUST_ID']);
        $this->view->cust_name    = $resultdata['CUST_NAME'];
        $this->view->cust_cif     = $resultdata['CUST_CIF'];
        $this->view->cust_type    = $resultdata['CUST_TYPE'];
        $this->view->cust_workfield  = $resultdata['CUST_WORKFIELD'];
        $this->view->cust_address = $resultdata['CUST_ADDRESS'];
        $this->view->cust_city    = $resultdata['CUST_CITY'];
        $this->view->cust_zip     = $resultdata['CUST_ZIP'];
        $this->view->cust_province = $resultdata['CUST_PROVINCE'];
        $this->view->country_code  = $resultdata['COUNTRY_CODE'];
        $this->view->cust_contact  = $resultdata['CUST_CONTACT'];
        $this->view->cust_phone    = $resultdata['CUST_PHONE'];
        $this->view->cust_ext      = $resultdata['CUST_EXT'];
        $this->view->cust_fax      = $resultdata['CUST_FAX'];
        $this->view->cust_email    = $resultdata['CUST_EMAIL'];
        $this->view->cust_website  = $resultdata['CUST_WEBSITE'];
        $this->view->cust_status   = strtoupper($resultdata['CUST_STATUS']);

        $this->view->cust_charges_status  = $resultdata['CUST_CHARGES_STATUS'];
        $this->view->cust_admfee_status   = $resultdata['CUST_MONTHLYFEE_STATUS'];
        $this->view->cust_token_auth   = $resultdata['CUST_TOKEN_AUTH'];

        $this->view->cust_created     = $resultdata['CUST_CREATED'];
        $this->view->cust_createdby   = $resultdata['CUST_CREATEDBY'];
        $this->view->cust_suggested   = $resultdata['CUST_SUGGESTED'];
        $this->view->cust_suggestedby = $resultdata['CUST_SUGGESTEDBY'];
        $this->view->cust_updated     = $resultdata['CUST_UPDATED'];
        $this->view->cust_updatedby   = $resultdata['CUST_UPDATEDBY'];

        $this->view->cust_limit_idr     = Application_Helper_General::displayMoney($resultdata['CUST_LIMIT_IDR']);
        $this->view->cust_limit_usd   = Application_Helper_General::displayMoney($resultdata['CUST_LIMIT_USD']);



        //--------------------------------------------------Bank Account--------------------------------------------
        $this->view->bankAccount     = $this->getCustomerAcct($cust_id);
        $this->view->benefBankAcc    = $this->getBenefBankAcc($cust_id);
        $this->view->userAccount     = $this->getUserAcct($cust_id);
        $this->view->userLimit       = $this->getUserLimit($cust_id);
        $this->view->userDailyLimit  = $this->getUserDailyLimit($cust_id);
        // $this->view->userTempDailyLimit  = $this->getUserTempDailyLimit($cust_id);

        $getAppGroup = $this->getAppGroup($cust_id);
        $getAppGroupArray = null;
        foreach ($getAppGroup as $row) {
          $getAppGroupArray[$row['GROUP_USER_ID']][] = $row['USER_ID'];
        }
        $this->view->appGroup = $getAppGroupArray;


        $getAppBoundaryGroup = $this->getAppBoundary($cust_id);
        $boundaryGroup = null;
        foreach ($getAppBoundaryGroup as $row) {
          //example : N_CUSTCHRIS_01 & S_CUSTCHRIS
          $explodeGroup = explode('_', $row['GROUP_USER_ID']);

          if ($explodeGroup[0] == 'N')      $group_desc = 'Group ' . (int)$explodeGroup[2];
          else if ($explodeGroup[0] == 'S') $group_desc = 'Special Group';


          $boundaryGroup[$row['BOUNDARY_ID']]['GROUP_USER_ID'][] = $group_desc;
          $boundaryGroup[$row['BOUNDARY_ID']]['BOUNDARY_MIN']    = $row['BOUNDARY_MIN'];
          $boundaryGroup[$row['BOUNDARY_ID']]['BOUNDARY_MAX']    = $row['BOUNDARY_MAX'];
          $boundaryGroup[$row['BOUNDARY_ID']]['CCY_BOUNDARY']    = $row['CCY_BOUNDARY'];
        }
        $this->view->appBoundary = $boundaryGroup;

        $this->view->benefUser   = $this->getBenefUser($cust_id);

        //----------------------------------------------- END Bank Account------------------------------------------



        $result = $this->getTempCustomerId($cust_id);

        if ($result)  $temp = 0;
        else  $temp = 1;

        $this->view->cust_temp = $temp;
      } else $cust_id = null;
    } // End if cust_id == true



    if (!$cust_id) {
      $error_remark = 'Invalid Cust ID';
      //insert log
      try {
        $this->_db->beginTransaction();

        Application_Helper_General::writeLog('CCLS', 'View Detail Customer [Invalid Cust ID]');

        $this->_db->commit();
      } catch (Exception $e) {
        $this->_db->rollBack();
      }
      $this->_helper->getHelper('FlashMessenger')->addMessage('F');
      $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
      $this->_redirect($this->_helper->url->url(array('module' => $this->_request->getModuleName(), 'controller' => 'index', 'action' => 'index')));
    }

    $this->view->cust_id = $cust_id;
    $this->view->status_type = $this->_masterglobalstatus;
    $this->view->modulename = $this->_request->getModuleName();

    //insert log
    try {
      $this->_db->beginTransaction();

      //  Application_Helper_General::writeLog('CCLS','View Customer Detail ['.$cust_id.']');

      $this->_db->commit();
    } catch (Exception $e) {
      $this->_db->rollBack();
    }
  }
}
