<?php

require_once 'Zend/Controller/Action.php';

class virtualpaymentreport_EcollectionreportController extends Application_Main
{

  public function initController()
  {
   
  }

  public function indexAction()
  {

    $setting = new Settings();          
    $enc_pass = $setting->getSetting('enc_pass');
    $enc_salt = $setting->getSetting('enc_salt');
    $sessionNamespace = new Zend_Session_Namespace('Tokenenc');      
    $pw_hash = md5($enc_salt.$enc_pass);
    $rand = $this->_userIdLogin.date('dHis').$pw_hash;
    $sessionNamespace->token  = $rand;
    $this->view->token = $sessionNamespace->token;

    $this->_helper->layout()->setLayout('newlayout');

    $temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
    if (count($temp) > 1) {
      if ($temp[0] == 'F' || $temp[0] == 'S') {
        if ($temp[0] == 'F')
          $this->view->error = 1;
        else
          $this->view->success = 1;
        $msg = '';
        unset($temp[0]);
        foreach ($temp as $value) {
          if (!is_array($value))
            $value = array($value);
          $msg .= $this->view->formErrors($value);
        }
        $this->view->customer_msg = $msg;
      }
    }


    // proses pengambilan data filter,display all,sorting
    $select = array();
    $select = $this->_db->select()
      ->from(array('A' => 'M_CUSTOMER_BIN'), array('*'))
      //               ->joinLeft(array('B' => 'M_CUSTOMER'),'A.CUST_ID = B.CUST_ID AND A.CUST_ID = B.CUST_ID',array('*'));
      ->joinLeft(array('C' => 'M_CUSTOMER'), 'A.CUST_ID = C.CUST_ID', array(
        'CUST_NAME',
        'COMPANY' => new Zend_Db_Expr("CONCAT(C.CUST_NAME , ' (' , C.CUST_ID , ')  ' )")
      ));

    $filter     = $this->_getParam('filter');

    //saat pertama kali klik, lgsg filter by date today               
    if ($filter == '') {
      /*$today = date('d/m/Y');
            
            $this->view->latestSuggestionFrom = $today;
          $this->view->latestSuggestionTo   = $today;
            
          //konversi date agar dapat dibandingkan
          $latestSuggestionFrom   = (Zend_Date::isDate($today,$this->_dateDisplayFormat))?
                         new Zend_Date($today,$this->_dateDisplayFormat):
                         false;
      
          $latestSuggestionTo     = (Zend_Date::isDate($today,$this->_dateDisplayFormat))?
                         new Zend_Date($today,$this->_dateDisplayFormat):
                         false;
             
             if($latestSuggestionFrom)  $select->where("CONVERT(date,CUST_SUGGESTED) >= CONVERT(DATE,".$this->_db->quote($latestSuggestionFrom->toString($this->_dateDBFormat)).")");
         if($latestSuggestionTo)    $select->where("CONVERT(date,CUST_SUGGESTED) <= CONVERT(DATE,".$this->_db->quote($latestSuggestionTo->toString($this->_dateDBFormat)).")");*/
    }

    $fields = array(
          'va_number'     => array(
            'field'    => 'VA_NUMBER',
            'label'    => $this->language->_('VA_NUMBER'),
            'sortable' => true
          ),
          'va_name'   => array(
            'field'    => 'VA_NUMBER',
            'label'    => $this->language->_('VA_NAME'),
            'sortable' => true
          ),
          'ccy' => array(
            'field'    => 'ccy',
            'label'    => $this->language->_('CCY'),
            'sortable' => true
          ),
          'bill_amount' => array(
            'field'    => 'BILL_AMOUNT',
            'label'    => $this->language->_('Bill Amount'),
            'sortable' => true
          ),
          'paid_amount'     => array(
            'field'    => 'PAID_AMOUNT',
            'label'    => $this->language->_('Paid Amount'),
            'sortable' => true
          ),
          'expired_date'    => array(
            'field'  => 'EXPIRED_DATE',
            'label'    => $this->language->_('Expired Date'),
            'sortable' => true
          ),
          'last_updated'   => array(
            'field'    => 'LAST_UPDATED',
            'label'    => $this->language->_('Last Updated'),
            'sortable' => true
          ),
          'status'   => array(
            'field'    => 'STATUS',
            'label'    => $this->language->_('Status'),
            'sortable' => true
          ),
          'desc'   => array(
            'field'    => 'DESC',
            'label'    => $this->language->_('Desc'),
            'sortable' => true
          )
        ); 
        $filterlist = array('VA_NUMBER', 'VA_NAME', 'CCY', 'PAID_AMOUNT', 'STATUS', 'DESC', 'LAST_UPDATED', 'EXPIRED_DATE');

        $filterArr = array(
          'filter'  => array('StripTags', 'StringTrim'),
          'VA_NUMBER'  => array('StripTags', 'StringTrim'),
          'VA_NAME'  => array('StripTags', 'StringTrim'),
          'CCY'  => array('StripTags', 'StringTrim'),
          'PAID_AMOUNT'  => array('StripTags', 'StringTrim'),
          'LAST_UPDATED'   => array('StripTags', 'StringTrim', 'StringToUpper'),
          'LAST_UPDATED_END'       => array('StripTags', 'StringTrim'),
          'EXPIRED_DATE'   => array('StripTags', 'StringTrim', 'StringToUpper'),
          'EXPIRED_DATE_END'       => array('StripTags', 'StringTrim'),
          'STATUS'  => array('StripTags', 'StringTrim'),
          'DESC'  => array('StripTags', 'StringTrim'),
        );

        $validators = array(
          'filter' => array(),
          'VA_NUMBER'    => array(),
          'VA_NAME'    => array(),
          'CCY'  => array(),
          'PAID_AMOUNT'  => array(),
          'LAST_UPDATED' => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
          'LAST_UPDATED_END'     => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
          'EXPIRED_DATE' => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
          'EXPIRED_DATE_END'     => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
          'STATUS' => array(),
          'DESC' => array(),
        );

        $dataParam = array('VA_NUMBER', 'VA_NAME', 'CCY', 'PAID_AMOUNT', 'STATUS', 'DESC', 'LAST_UPDATED', 'EXPIRED_DATE');

    if ($filter == TRUE) {

      $vaType = $this->_getParam('vatype');

      //if va type static
      if ($vaType != '0') {
         $fields = array(
          'va_number'     => array(
            'field'    => 'VA_NUMBER',
            'label'    => $this->language->_('VA_NUMBER'),
            'sortable' => true
          ),
          'doc'   => array(
            'field'    => 'DOC',
            'label'    => $this->language->_('Doc#'),
            'sortable' => true
          ),
          'ccy' => array(
            'field'    => 'ccy',
            'label'    => $this->language->_('CCY'),
            'sortable' => true
          ),

          'total_bill_amount' => array(
            'field'    => 'TOTAL_BILL_AMOUNT',
            'label'    => $this->language->_('Total Bill Amount'),
            'sortable' => true
          ),
          'paid_amount'     => array(
            'field'    => 'PAID_AMOUNT',
            'label'    => $this->language->_('Paid Amount'),
            'sortable' => true
          ),
          'settled_w_remark'    => array(
            'field'  => 'SETTLED_WITH_REMARK',
            'label'    => $this->language->_('Settled With Remark'),
            'sortable' => true
          ),
          'unsettled_amount'   => array(
            'field'    => 'UNSETTLED_AMOUNT',
            'label'    => $this->language->_('Unsettled Amount'),
            'sortable' => true
          ),
          'credit_available'   => array(
            'field'    => 'CREDIT_AVAILABLE',
            'label'    => $this->language->_('Credit Available'),
            'sortable' => true
          ),
          'in_billing'   => array(
            'field'    => 'IN_BILLING',
            'label'    => $this->language->_('In Billing'),
            'sortable' => true
          ),
          'last_updated'   => array(
            'field'    => 'LAST_UPDATED',
            'label'    => $this->language->_('Last Updated'),
            'sortable' => true
          ),
          'status'   => array(
            'field'    => 'STATUS',
            'label'    => $this->language->_('Status'),
            'sortable' => true
          )
        ); 
        $filterlist = array('VA_NUMBER', 'CCY', 'PAID_AMOUNT', 'SETTLED_WITH_REMARK', 'UNSETTLED_AMOUNT', 'CREDIT_AVAILABLE', 'IN_BILLING', 'STATUS', 'LAST_UPDATED');

        $filterArr = array(
          'filter'  => array('StripTags', 'StringTrim'),
          'VA_NUMBER'  => array('StripTags', 'StringTrim'),
          'CCY'  => array('StripTags', 'StringTrim'),
          'PAID_AMOUNT'  => array('StripTags', 'StringTrim'),
          'SETTLED_WITH_REMARK'  => array('StripTags', 'StringTrim'),
          'UNSETTLED_AMOUNT'  => array('StripTags', 'StringTrim'),
          'CREDIT_AVAILABLE'  => array('StripTags', 'StringTrim'),
          'IN_BILLING'  => array('StripTags', 'StringTrim'),
          'LAST_UPDATED'   => array('StripTags', 'StringTrim', 'StringToUpper'),
          'LAST_UPDATED_END'       => array('StripTags', 'StringTrim'),
          'STATUS'  => array('StripTags', 'StringTrim'),
        );

        $validators = array(
          'filter' => array(),
          'VA_NUMBER'    => array(),
          'CCY'  => array(),
          'PAID_AMOUNT'  => array(),
          'SETTLED_WITH_REMARK'  => array(),
          'UNSETTLED_AMOUNT'  => array(),
          'CREDIT_AVAILABLE'  => array(),
          'IN_BILLING'  => array(),
          'LAST_UPDATED' => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
          'LAST_UPDATED_END'     => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
          'STATUS' => array(),
        );

        $dataParam = array('VA_NUMBER', 'CCY', 'PAID_AMOUNT', 'SETTLED_WITH_REMARK', 'UNSETTLED_AMOUNT', 'CREDIT_AVAILABLE', 'IN_BILLING', 'STATUS', 'LAST_UPDATED');
      }

    //validasi page, jika input page bukan angka               
    $page = $this->_getParam('page');

    $page = (Zend_Validate::is($page, 'Digits')) ? $page : 1;

    //validasi sort, jika input sort bukan ASC atau DESC
    $sortBy  = $this->_getParam('sortby');
    $sortBy  = (Zend_Validate::is($sortBy, 'InArray', array(array_keys($fields)))) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
    $sortDir = $this->_getParam('sortdir');
    $sortDir = (Zend_Validate::is($sortDir, 'InArray', array('haystack' => array('asc', 'desc')))) ? $sortDir : 'asc';

    
    $dataParamValue = array();
    $clean2 = array_diff($this->_request->getParam('wherecol'), $dataParam);
    $dataParam = array_diff($this->_request->getParam('wherecol'), $clean2);
    // $clean2 = array_diff( $this->_request->getParam('wherecol'),$dataParam); 
    // $dataParam = array_diff( $this->_request->getParam('wherecol'),$clean2); 
    // print_r($dataParam);die;

    // print_r($output);die;
    // print_r($this->_request->getParam('wherecol'));
    foreach ($dataParam as $no => $dtParam) {

      if (!empty($this->_request->getParam('wherecol'))) {
        $dataval = $this->_request->getParam('whereval');
        // print_r($dataval);
        $order = 0;
        foreach ($this->_request->getParam('wherecol') as $key => $value) {
          if ($value == "LAST_UPDATED" || $value == "EXPIRED_DATE") {
            $order--;
          }
          if ($dtParam == $value) {
            $dataParamValue[$dtParam] = $dataval[$order];
          }
          $order++;
        }
      }
    }
    // print_r($dataParamValue);
    // die; 
    if (!empty($this->_request->getParam('updatedate'))) {
      $datearr = $this->_request->getParam('updatedate');
      $dataParamValue['LAST_UPDATED'] = $datearr[0];
      $dataParamValue['LAST_UPDATED_END'] = $datearr[1];
    }

    if (!empty($this->_request->getParam('expdate'))) {
      $datearr = $this->_request->getParam('expdate');
      $dataParamValue['EXPIRED_DATE'] = $datearr[0];
      $dataParamValue['EXPIRED_DATE_END'] = $datearr[1];
    }

    $zf_filter = new Zend_Filter_Input($filterArr, $validator, $dataParamValue);

    $this->view->currentPage = $page;
    $this->view->sortBy = $sortBy;
    $this->view->sortDir = $sortDir;


//tinggal ubah sesuai filter
      $cid       = html_entity_decode($zf_filter->getEscaped('COMP_ID'));
      $cname     = html_entity_decode($zf_filter->getEscaped('COMP_NAME'));
      $bin      = html_entity_decode($zf_filter->getEscaped('BIN'));
      $status    = html_entity_decode($zf_filter->getEscaped('Status'));

      $latestSuggestionFrom   = html_entity_decode($zf_filter->getEscaped('BIN_SUGGESTED'));
      $latestSuggestionTo     = html_entity_decode($zf_filter->getEscaped('BIN_SUGGESTED_END'));
      $latestSuggestor        = html_entity_decode($zf_filter->getEscaped('BIN_SUGGESTEDBY'));

      $latestApprovalFrom     = html_entity_decode($zf_filter->getEscaped('BIN_UPDATED'));
      $latestApprovalTo       = html_entity_decode($zf_filter->getEscaped('BIN_UPDATED_END'));
      $latestApprover         = html_entity_decode($zf_filter->getEscaped('BIN_UPDATEDBY'));

      $latestCreatedFrom     = html_entity_decode($zf_filter->getEscaped('BIN_CREATED'));
      $latestCreatedTo       = html_entity_decode($zf_filter->getEscaped('BIN_CREATED_END'));
      $latestCreated         = html_entity_decode($zf_filter->getEscaped('BIN_CREATED'));


      //konversi date agar dapat dibandingkan
      $latestSuggestionFrom   = (Zend_Date::isDate($latestSuggestionFrom, $this->_dateDisplayFormat)) ?
        new Zend_Date($latestSuggestionFrom, $this->_dateDisplayFormat) :
        false;

      $latestSuggestionTo     = (Zend_Date::isDate($latestSuggestionTo, $this->_dateDisplayFormat)) ?
        new Zend_Date($latestSuggestionTo, $this->_dateDisplayFormat) :
        false;

      $latestApprovalFrom     = (Zend_Date::isDate($latestApprovalFrom, $this->_dateDisplayFormat)) ?
        new Zend_Date($latestApprovalFrom, $this->_dateDisplayFormat) :
        false;

      $latestApprovalTo       = (Zend_Date::isDate($latestApprovalTo, $this->_dateDisplayFormat)) ?
        new Zend_Date($latestApprovalTo, $this->_dateDisplayFormat) :
        false;


      $latestCreatedFrom     = (Zend_Date::isDate($latestCreatedFrom, $this->_dateDisplayFormat)) ?
        new Zend_Date($latestCreatedFrom, $this->_dateDisplayFormat) :
        false;

      $latestCreatedTo       = (Zend_Date::isDate($latestCreatedTo, $this->_dateDisplayFormat)) ?
        new Zend_Date($latestCreatedTo, $this->_dateDisplayFormat) :
        false;

      //if($cid)            $select->where('UPPER(CUST_ID) LIKE '.$this->_db->quote('%'.strtoupper($cid).'%'));
      if ($cid)              $select->where('UPPER(A.CUST_ID)=' . $this->_db->quote(strtoupper($cid)));
      if ($bin)              $select->where('UPPER(CUST_BIN) LIKE ' . $this->_db->quote('%' . strtoupper($bin) . '%'));
      if ($cname)            $select->where('UPPER(CUST_NAME) LIKE ' . $this->_db->quote('%' . strtoupper($cname) . '%'));
      if ($status)           $select->where('CUST_BIN_STATUS=?', $status);
      if ($latestSuggestor)  $select->where('UPPER(BIN_SUGGESTEDBY) LIKE ' . $this->_db->quote('%' . strtoupper($latestSuggestor) . '%'));
      if ($latestApprover)   $select->where('UPPER(BIN_UPDATEDBY) LIKE ' . $this->_db->quote('%' . strtoupper($latestApprover) . '%'));

      if ($latestSuggestionFrom)  $select->where("DATE(BIN_SUGGESTED) >= DATE(" . $this->_db->quote($latestSuggestionFrom->toString($this->_dateDBFormat)) . ")");
      if ($latestSuggestionTo)    $select->where("DATE(BIN_SUGGESTED) <= DATE(" . $this->_db->quote($latestSuggestionTo->toString($this->_dateDBFormat)) . ")");
      if ($latestApprovalFrom)    $select->where("DATE(BIN_UPDATED) >= DATE(" . $this->_db->quote($latestApprovalFrom->toString($this->_dateDBFormat)) . ")");
      if ($latestApprovalTo)      $select->where("DATE(BIN_UPDATED) <= DATE(" . $this->_db->quote($latestApprovalTo->toString($this->_dateDBFormat)) . ")");
      if ($latestCreatedFrom)    $select->where("DATE(BIN_CREATED) >= DATE(" . $this->_db->quote($latestCreatedFrom->toString($this->_dateDBFormat)) . ")");
      if ($latestCreatedTo)      $select->where("DATE(BIN_CREATED) <= DATE(" . $this->_db->quote($latestCreatedTo->toString($this->_dateDBFormat)) . ")");


      $this->view->cid     = $cid;
      $this->view->cname   = $cname;
      $this->view->bin    = $bin;
      $this->view->status  = $status;
      $this->view->latestSuggestor  = $latestSuggestor;
      $this->view->latestApprover   = $latestApprover;

      if ($latestSuggestionFrom)  $this->view->latestSuggestionFrom = $latestSuggestionFrom->toString($this->_dateDisplayFormat);
      if ($latestSuggestionTo)    $this->view->latestSuggestionTo   = $latestSuggestionTo->toString($this->_dateDisplayFormat);
      if ($latestApprovalFrom)    $this->view->latestApprovalFrom   = $latestApprovalFrom->toString($this->_dateDisplayFormat);
      if ($latestApprovalTo)      $this->view->latestApprovalTo     = $latestApprovalTo->toString($this->_dateDisplayFormat);
    }
    //utk sorting 
    $select->order($sortBy . ' ' . $sortDir);

    // END proses pengambilan data filter,display all,sorting

    // $data = $this->_db->fetchAll($select);

    // foreach ($data as $key => $value) {
    //   $tempCustomerId = $this->getTempCustomerId($value['CUST_ID'], $value['CUST_BIN'], TRUE);
    //   //if ada di temp
    //   if ($tempCustomerId) {
    //     $data[$key]['TEMP_STATUS'] = 1;
    //   }
    //   else{
    //     $data[$key]['TEMP_STATUS'] = 0;
    //   }
    // }

    //   echo "<pre>";
    // print_r($data);die();

    $data = array();

    $this->paging($data);
    $this->view->fields = $fields;
    $this->view->filter = $filter;
    $this->view->vatype = $vaType;
    $this->view->filterlist = $filterlist;
    $this->view->statusCode = array_flip($this->_masterglobalstatus['code']);
    $this->view->statusDesc = $this->_masterglobalstatus['desc'];
    $this->view->modulename = $this->_request->getModuleName();


    // $arr = $this->_db->fetchAll($select);

    if($this->_request->getParam('print') == 1){

      foreach($arr as $key=>$row)
        {
          
          $statusCode = array_flip($this->_masterglobalstatus['code']);
          $statusDesc = $this->_masterglobalstatus['desc'];

          $arr[$key]['BIN_UPDATED']     = Application_Helper_General::convertDate($row['BIN_UPDATED'],"dd MMM yyyy HH:mm:ss ") . ' (' . $row['BIN_UPDATEDBY'] . ')';
          $arr[$key]['BIN_SUGGESTED']   = Application_Helper_General::convertDate($row['BIN_SUGGESTED'],"dd MMM yyyy HH:mm:ss ") . ' (' . $row['BIN_SUGGESTEDBY'] . ')';
          $arr[$key]['MASTER_ACCOUNT']  = $row['ACCT_NO'].' ('.$row['CCY'].') -'.$row['ACCT_NAME'];
          $arr[$key]['CUST_CITY']       = $row['CUST_BIN'];
          $binType                      = array('1' => 'Cash In', '2' => 'Cash Out');
          $arr[$key]['BIN_TYPE']        = (empty($row['BIN_TYPE']) ? '-' : $binType[$row['BIN_TYPE']] );
          $arr[$key]['CUST_STATUS']     = $statusDesc[$statusCode[$row['CUST_BIN_STATUS']]];
          
        }

      $this->_forward('print', 'index', 'widget', array('data_content' => $arr, 'data_caption' => 'Bin Setup', 'data_header' => $fields));
    }

    //insert log
    try {
      $this->_db->beginTransaction();

      Application_Helper_General::writeLog('BNLS', 'View Customer BIN Setup Customer BIN List');

      $this->_db->commit();
    } catch (Exception $e) {
      $this->_db->rollBack();
    }

    if (!empty($dataParamValue)) {
      $this->view->updateStart = $dataParamValue['LAST_UPDATED'];
      $this->view->updateEnd = $dataParamValue['LAST_UPDATED_END'];
      $this->view->expiredStart = $dataParamValue['EXPIRED_DATE'];
      $this->view->expiredEnd = $dataParamValue['EXPIRED_DATE_END'];

      unset($dataParamValue['LAST_UPDATED_END']);
      unset($dataParamValue['EXPIRED_DATE_END']);
      foreach ($dataParamValue as $key => $value) {
        $wherecol[] = $key;
        $whereval[] = $value;
      }

      // print_r($whereval);die;
    } else {
      $wherecol = array();
      $whereval = array();
    }

    $this->view->wherecol     = $wherecol;
    $this->view->whereval     = $whereval;
  }

  public function custbinAction(){
    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout()->disableLayout();

    $custId = $this->_getParam('custId');

    $select = $this->_db->select()
        ->from(array('B' => 'M_CUSTOMER_BIN'),array('*'))
        ->where('B.CUST_ID = ?', (string)$custId)
        ->query()->fetchAll();

    $html = '';
    foreach ($select as $key => $value) {
      $html .= '<option value="'.$value['CUST_BIN'].'">'.$value['CUST_BIN'].'</option>';
    }

    echo $html;

  }
}
