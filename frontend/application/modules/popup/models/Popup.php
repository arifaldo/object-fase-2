<?php

Class popup_Model_Popup
{
    protected $_db;
   
    public function __construct()
    { 
		$this->_config 	= Zend_Registry::get('config');
		$this->_db 		= Zend_Db_Table::getDefaultAdapter();
    }
    
    public function getSettingById($id)
    {
    	$data = $this->_db->select()
    	->from(array('A' => 'M_SETTING'),array('SETTING_ID','SETTING_VALUE'))
    	->where('MODULE_ID = ?' , 'GNS')
    	->where('SETTING_ID = ?' , $id)
    	->query()->FetchAll();
    
    	return $data;
    }

	public function getBeneficiaries($param)
	{
		$select	= $this->_db->select()
							->from(array('B' => 'M_BENEFICIARY'));
		$select->columns(array('BENEFICIARY_CITIZENSHIP_disp'=> new Zend_Db_Expr("CASE B.BENEFICIARY_CITIZENSHIP	
																						WHEN 'R' THEN 'Resident' 
																						WHEN 'NR' THEN 'Non Resident'
																						ELSE ''	END"),
							   'BENEFICIARY_TYPE_disp'		=> new Zend_Db_Expr("CASE B.BENEFICIARY_TYPE
																						WHEN '1' THEN 'Within' 
																						WHEN '2' THEN 'Domestic'
																						WHEN '3' THEN 'Remittance' END"),
							   'ISFAVORITE_disp'			=> new Zend_Db_Expr("CASE B.ISFAVORITE 	
																						WHEN '0' THEN 'No' 
																						WHEN '1' THEN 'Yes'	END"),
							   'BENEFICIARY_BANKSTATUS_disp'=> new Zend_Db_Expr("CASE B.BENEFICIARY_BANKSTATUS 
							   															WHEN '0' THEN 'No' 
																						WHEN '1' THEN 'Yes'	
																						WHEN '2' THEN 'Failed'	END"),
							   'BENEFICIARY_ISAPPROVE_disp'	=> new Zend_Db_Expr("CASE B.BENEFICIARY_ISAPPROVE 
							   															WHEN '0' THEN 'Not Approved' 
																						WHEN '1' THEN 'Approved'	END"),
								'BANK_CODE'	=>	'BENEFICIARY_BANK_CODE',
							   'ACTION_disp'	=> new Zend_Db_Expr("CASE WHEN BENEFICIARY_ISREQUEST_DELETE = 1 THEN 'Delete'
							   											  WHEN BENEFICIARY_ISAPPROVE 		= 0 THEN 'Add'  END"),
																		  
							  )
						);
		$select->where("B.BENEFICIARY_ISAPPROVE = 1");
		if(isset($param['user_id'])) $select->where("B.USER_ID = ?", (string)$param['user_id']);
		if(isset($param['tempType']))
		{
			$select->where("B.BENEFICIARY_TYPE = ?", (string) $param['tempType']);
			// if($param['tempType'] != 8){
				// $select->join(array('K' => 'M_DOMESTIC_BANK_TABLE'), 'B.CLR_CODE = K.CLR_CODE and B.BANK_NAME = K.BANK_NAME', array());
			// }
		}
		if(isset($param['fAlias']))$select->where('UPPER(B.BENEFICIARY_ALIAS) LIKE '.$this->_db->quote('%'.strtoupper($param['fAlias']).'%'));
		if(isset($param['fAcct']))$select->where('B.BENEFICIARY_ACCOUNT LIKE '.$this->_db->quote('%'.strtoupper($param['fAcct']).'%'));
		if(isset($param['fName']))$select->where('UPPER(B.BENEFICIARY_NAME) LIKE '.$this->_db->quote('%'.strtoupper($param['fName']).'%'));
		if(isset($param['fFav']))$select->where('B.ISFAVORITE=1');
		if(isset($param['sortBy']) && isset($param['sortDir']))	$select->order($sortBy.' '.$sortDir);
		return $select;
	}
}