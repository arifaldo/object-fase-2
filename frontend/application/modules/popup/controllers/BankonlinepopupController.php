<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/SinglePayment.php';
class popup_BankonlinepopupController extends Application_Main
{
	 protected $_moduleDB = 'RTF'; //masih harus diganti

	public function initController()
	{       
	
		$this->_helper->layout()->setLayout('popup');
	}

	public function indexAction() 
	{	
		$app = Zend_Registry::get('config');
		$app = $app['app']['bankcode'];
		
		$bankname = $this->language->_('Bank Name');
		$bankcode = $this->language->_('Bank Code');
	    $fields = array(
						'BANK_OL_NAME'      => array('field' => 'BANK_OL_NAME',
											      'label' => $bankname,
											      'sortable' => true),
						'BANK_OL_CODE'           => array('field' => 'BANK_OL_CODE',
											      'label' => $bankcode,
											      'sortable' => true),
	                  );
				
		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','doc_no');
		$sortDir = $this->_getParam('sortdir','asc');
		
		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';

		$filterArr = array(	
										'filter' 	  	 => array('StringTrim','StripTags'),
										'BANK_OL_NAME'      => array('StringTrim','StripTags'),
										'BANK_OL_CODE'     => array('StringTrim','StripTags')
									);
		
		
		$zf_filter = new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		$filter = $zf_filter->getEscaped('filter');

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$select = $this->_db->select()
					        ->from(array('A' => 'M_DOMESTIC_ONLINE_BANK_TABLE'));
		$select->where('A.BANK_OL_CODE != ?', $app);   

		//if($filter == 'Set Filter')
		if($filter == TRUE)
		{
			$fbank_name   	  = $zf_filter->getEscaped('BANK_OL_NAME');
			$fbank_code       = $zf_filter->getEscaped('BANK_OL_CODE');

	        if($fbank_name)	$select->where('BANK_0L_NAME LIKE '.$this->_db->quote('%'.strtoupper($fbank_name).'%'));
	        if($fbank_code)		$select->where('BANK_0L_CODE LIKE '.$this->_db->quote('%'.strtoupper($fbank_code).'%'));
			
			$this->view->BANK_OL_NAME     = $fbank_name;
			$this->view->BANK_OL_CODE          = $fbank_code;
			
		}		

	    $select->order($sortBy.' '.$sortDir);   
		$this->paging($select);
		$this->view->fields = $fields;
		$this->view->filter = $filter;
	}

}
