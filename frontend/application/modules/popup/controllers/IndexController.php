<?php
require_once 'Zend/Controller/Action.php';

class popup_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti
	
	public function initController()
	{     
		$this->_helper->layout()->setLayout('popup');
	}

	public function indexAction() 
	{
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}	
    	}
	}
	
	public function multiapproveAction() 
	{
		$line = (int) $this->_request->getParam('line');
		
		$sessionNamespace = new Zend_Session_Namespace('confirmMultiApprove');
		$boundary = $sessionNamespace->boundary[$line];
		$errorMsg = (!empty($sessionNamespace->errorMsg[$line])) ? $sessionNamespace->errorMsg[$line]: "";
		$this->view->boundary 		= $boundary;
		
		if ($errorMsg)
		{
			$this->view->popupTitle	= "Error message list";
			$this->view->error 		= true;
			$this->view->report_msg	= $errorMsg;
		}
	}
	
	public function multireleaseAction() 
	{
		$line = (int) $this->_request->getParam('line');
		
		$sessionNamespace = new Zend_Session_Namespace('confirmMultiRelease');
		$errorMsg = (!empty($sessionNamespace->errorMsg[$line])) ? $sessionNamespace->errorMsg[$line]: "";

		$errorSuspect = (!empty($sessionNamespace->errorSuspect[$line])) ? $sessionNamespace->errorSuspect[$line]: "";

		if ($errorMsg || $errorSuspect)
		{
			if (!is_array($errorMsg))	$errorMsg = array($errorMsg);
			if (!is_array($errorSuspect))	$errorSuspect = array($errorSuspect);
			
			
			$this->view->popupTitle	= "Error message list";
			$this->view->error 		= true;
			$this->view->report_msg	= implode("<br><br>", $errorMsg);
			$this->view->errorsuspect 		= true;
			
			$this->view->report_suspect	= implode("<br><br>", $errorSuspect);
		}
		
		$this->render('index');
	}

	public function multiapprovedocAction() 
	{
		$line = (int) $this->_request->getParam('line');
		
		$sessionNamespace = new Zend_Session_Namespace('confirmMultiApproveDoc');
		$errorMsg = (!empty($sessionNamespace->errorMsg[$line])) ? $sessionNamespace->errorMsg[$line]: "";
		
		if ($errorMsg)
		{
			$this->view->popupTitle	= "Error message list";
			$this->view->error 		= true;
			$this->view->report_msg	= implode("<br><br>", $errorMsg);
		}
		
		$this->render('index');
	}
}
