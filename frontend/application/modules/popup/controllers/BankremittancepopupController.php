<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/SinglePayment.php';
class popup_BankremittancepopupController extends Application_Main
{
	 protected $_moduleDB = 'RTF'; //masih harus diganti

	public function initController()
	{       
	
		$this->_helper->layout()->setLayout('popup');
	}

	public function indexAction() 
	{	
		$app = Zend_Registry::get('config');
		$app = $app['app']['bankcode'];
		
		/* list country */
		$country = $this->_db->select()->distinct()
								->from(array('A' => 'M_COUNTRY'),array('COUNTRY_CODE', 'COUNTRY_NAME'))
								->joinLeft(array('O' => 'M_OFAC'),'A.COUNTRY_CODE = O.country_code', array())
								->where('O.country_code IS NULL')
								->order('COUNTRY_NAME ASC')
				 				->query()->fetchAll();
				 				
		$countryarr = Application_Helper_Array::listArray($country,'COUNTRY_CODE','COUNTRY_NAME');
		asort($countryarr);	
		$this->view->country=$countryarr;
		/* end list country */
		
		$bankname = $this->language->_('Bank Name');
		$bankcode = $this->language->_('Bank Code');
		$bank_address1 = $this->language->_('Bank Address 1');
		$bank_address2 = $this->language->_('Bank Address 2');
		$city_name = $this->language->_('City Name');
		$country = $this->language->_('Country');
		$pob_number = $this->language->_('POB Number');
		
	    $fields = array(						
						'bank_code'           => array('field' => 'bank_code',
											      'label' => $bankcode,
											      'sortable' => true),
	    				'bank_name'      => array('field' => 'bank_name',
											      'label' => $bankname,
											      'sortable' => true),
	    				'bank_address1'           => array('field' => 'bank_address1',
											      'label' => $bank_address1,
											      'sortable' => true),
	    				'bank_address2'           => array('field' => 'bank_address2',
											      'label' => $bank_address2,
											      'sortable' => true),
	    				'city_name'           => array('field' => 'city_name',
											      'label' => $city_name,
											      'sortable' => true),
	    				'country_code'           => array('field' => 'country_code',
											      'label' => $country,
											      'sortable' => true),
	    				'pob_number'           => array('field' => 'pob_number',
											      'label' => $pob_number,
											      'sortable' => true),
	                  );
				
		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','doc_no');
		$sortDir = $this->_getParam('sortdir','asc');
		
		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';

		$filterArr = array(	
										'filter' 	  	 => array('StringTrim','StripTags'),
										'bank_name'      => array('StringTrim','StripTags'),
										'bank_code'    	 => array('StringTrim','StripTags'),
										'"SEARCH_COUNTRY"'   => array('StringTrim','StripTags')
									);
		
		
		$zf_filter = new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		$filter = $zf_filter->getEscaped('filter');

		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;

		$getcurr = $this->_getParam('currency');
		$cursess = new Zend_Session_Namespace('curr');

		if(!empty($getcurr)){
			$cursess->currency = $getcurr;
			$currency = $getcurr;
		}
		else{
			$curtemp = $cursess->currency;
			if(!empty($curtemp))
				$currency = $curtemp;
		}


		if(empty($currency)){
			$currency = 'USD';
		}

		
		$select2 = $this->_db->select()
							//->distinct()
					        ->from(array('A' => 'M_BANK_REMITTANCE'),array())
					        ->join(array('B' => 'M_COUNTRY'),'A.COUNTRY_CODE = B.COUNTRY_CODE',array('A.bank_code',
						        																		'A.bank_name',
						        																		'A.bank_address1',
					        																			'A.bank_address2',
					        																			'A.city_name',
					        																			'A.pob_number',
						        																		'B.COUNTRY_NAME',
						        																		'A.country_code'))
					        //->joinLeft(array('C' => 'M_OFAC'),'B.COUNTRY_CODE = C.country_code',array())
					        ->join(array('MB' => 'M_MEMBER_BANK'),'SUBSTRING(A.bank_code,1,LENGTH(A.bank_code)-3) = MB.BANK_CODE', array())
					         ->where('MB.CCY = ?', $currency);
					        //->where('C.country_code IS NULL');
					        
					        
		if($filter == TRUE)
		{
			$fbank_name   	  = $zf_filter->getEscaped('bank_name');
			$fbank_code       = $zf_filter->getEscaped('bank_code');
			$fcountry_code    = $zf_filter->getEscaped('SEARCH_COUNTRY');

	        if($fbank_name)		$select2->where('A.bank_name LIKE '.$this->_db->quote('%'.strtoupper($fbank_name).'%'));
	        if($fbank_code)		$select2->where('A.bank_code LIKE '.$this->_db->quote('%'.strtoupper($fbank_code).'%'));
	        if($fcountry_code)	$select2->where('A.country_code LIKE '.$this->_db->quote('%'.strtoupper($fcountry_code).'%'));
			
			$this->view->bank_name     = $fbank_name;
			$this->view->bank_code     = $fbank_code;
			$this->view->country_code  = $fcountry_code;
			
		}		

		//echo $select2;die;
		
	    $select2->order($sortBy.' '.$sortDir); 	    
	    $arr = $this->_db->fetchAll($select2);  
		$this->paging($arr);
		
		$this->view->fields = $fields;
		$this->view->filter = $filter;
	}

}
