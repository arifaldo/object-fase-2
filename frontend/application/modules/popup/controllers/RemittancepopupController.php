<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';
require_once 'Service/Token.php'; //added new

class popup_RemittancepopupController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti
	
	public function initController()
	{       
		$listCcy = array(''=>'-- Select Currency --');
		$listCcy = array_merge($listCcy,Application_Helper_Array::listArray($this->getCcy(),'CCY_ID','DESCRIPTION'));
		$this->view->ccy = $listCcy;
		$this->_helper->layout()->setLayout('popup');
	}

	public function indexAction()
	{
		$aliasname = $this->language->_('Alias Name');
		$beneficiaryaccount = $this->language->_('Beneficiary Account');
		$beneficiaryaccountname = $this->language->_('Beneficiary Account Name');
		$emailaddress = $this->language->_('Email Address');
		$favorite= $this->language->_('Favorite');
		$checkedbybank= $this->language->_('Checked by Bank');
		$createdate= $this->language->_('Created Date');
		$status= $this->language->_('Status');
		$ccy= $this->language->_('CCY');
		
		//added new hard token
		//$HardToken = new Service_Token($this->_custIdLogin, $this->_userIdLogin, $tokenIdUser);
		//$challengeCode = $HardToken->generateChallengeCode1();
		
		//$challengeCodeSub = substr($challengeCode, 0,2);
		//$this->view->challengeCodeReq = $challengeCode;
		
		
		$fields = array(
						/*'alias'  => array('field' => 'BENEFICIARY_ALIAS',
											   'label' => $aliasname,
											   'sortable' => true),*/
						'benef_acct'  => array('field' => 'BENEFICIARY_ACCOUNT',
											   'label' => $beneficiaryaccount,
											   'sortable' => true),
						'benef_name'  => array('field' => 'BENEFICIARY_NAME',
											   'label' => $beneficiaryaccountname,
											   'sortable' => true),
						'email'  => array('field' => 'BENEFICIARY_EMAIL',
											   'label' => $emailaddress,
											   'sortable' => true),
						'ccy'   => array('field'    => 'CURR_CODE',
											  'label'    => $ccy,
											  'sortable' => true),
						'favorite'   => array('field'    => 'ISFAVORITE',
											  'label'    => $favorite,
											  'sortable' => true),
						'checked'   => array('field'    => 'BENEFICIARY_ISAPPROVE',
											  'label'    => $checkedbybank,
											  'sortable' => true),
						'date'   => array('field'    => 'BENEFICIARY_CREATED',
											  'label'    => $createdate,
											  'sortable' => true),
						'status'   => array('field'    => 'BENEFICIARY_BANKSTATUS',
											  'label'    => $status,
											  'sortable' => true),
				);
				
		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$getcurr    = $this->_getParam('currency');

		$currsess = new Zend_Session_Namespace('curr');
		if(!empty($getcurr)){
			$currsess->currency = $getcurr;
			$currency = $getcurr;
		}
		else{
			$currtemp = $currsess->currency;
			if(!empty($currtemp))
				$currency = $currtemp;
		}
		
		$sortBy  = $this->_getParam('sortby','favorite');
		$sortDir = $this->_getParam('sortdir','desc');
		$id_box = $this->_getParam('id_box');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;
		
		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';
	  

		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	'filter' 	  	=> array('StringTrim','StripTags'),
							'alias' 	  	=> array('StringTrim','StripTags'),
							'benef_acct'    => array('StringTrim','StripTags','StringToUpper'),
							'benef_name'    => array('StringTrim','StripTags'),
							'favorit'     	=> array('StringTrim','StripTags')
		);
		
		$zf_filter = new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		$filter = $zf_filter->getEscaped('filter');

		$this->view->currentPage = $page;
		$this->view->sortBy = $param['sortBy'] = $sortBy;
		$this->view->sortDir = $param['sortDir'] = $sortDir;
		$this->view->id_box = $id_box;

		//if($filter == 'Set Filter')
		if($filter == true)
		{
			$fAlias = $zf_filter->getEscaped('alias');
			$fAcct = $zf_filter->getEscaped('benef_acct');
			$fName = $zf_filter->getEscaped('benef_name');
			$fFav = $zf_filter->getEscaped('favorit');
			
			if($fAlias) $param['fAlias'] = $fAlias;
	        if($fAcct) $param['fAcct'] = $fAcct;
	        if($fName) $param['fName'] = $fName;
	        if($fFav==1)  $param['fFav'] = $fFav;
			
			$this->view->alias = $fAlias;
			$this->view->benef_acct = $fAcct;
			$this->view->benef_name = $fName;
			if($fFav==1)$this->view->favorit = true;
			else $this->view->favorit = false;
		}
		else $this->view->favorit = false;

		$this->view->curr = $getcurr;
		$model = new predefinedbeneficiary_Model_Predefinedbeneficiary();
		$param['currency'] = $currency;
		$param['user_id'] = $this->_userIdLogin;
		$param['cust_id'] = $this->_custIdLogin;
		$param['beneLinkage'] = $this->view->hasPrivilege('BLBU');
		$param['payType'] = $this->_beneftype["code"]["remittance"];
		$select   = $model->getBeneficiariesRemitPopup($param);
// 		echo "<pre>";
// 		print_r($select);die;
		$settingObj = new Settings();
		$this->view->LIMITLLD		= $settingObj->getSetting("threshold_lld_remittance"	, 0);
		$this->paging($select);
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		$this->view->dateTimeDisplayFormat = $this->_dateTimeDisplayFormat;
		Application_Helper_General::writeLog('CRSP','Show Account Book');

	}
	
	
	private function fillParam($zf_filter_input)
	{
		if(isset($zf_filter_input->BENEFICIARY_ID))$this->view->BENEFICIARY_ID = ($zf_filter_input->isValid('BENEFICIARY_ID')) ? $zf_filter_input->BENEFICIARY_ID : $this->_getParam('BENEFICIARY_ID'); 
		$this->view->ACBENEF_ALIAS = ($zf_filter_input->isValid('ACBENEF_ALIAS')) ? $zf_filter_input->ACBENEF_ALIAS : $this->_getParam('ACBENEF_ALIAS'); 
		$this->view->ACBENEF = ($zf_filter_input->isValid('ACBENEF')) ? $zf_filter_input->ACBENEF : $this->_getParam('ACBENEF');
		$this->view->CURR_CODE = ($zf_filter_input->isValid('CURR_CODE')) ? $zf_filter_input->CURR_CODE : $this->_getParam('CURR_CODE');
		$this->view->ACBENEF_EMAIL = ($zf_filter_input->isValid('ACBENEF_EMAIL')) ? $zf_filter_input->ACBENEF_EMAIL : $this->_getParam('ACBENEF_EMAIL');
	}

}
