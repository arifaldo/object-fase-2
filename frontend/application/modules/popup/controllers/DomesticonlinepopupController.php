<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/CustomerUser.php';

class popup_DomesticonlinepopupController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti

	public function initController()
	{
		$listCcy = array(''=>'-- Select Currency --');
		$listCcy = array_merge($listCcy,Application_Helper_Array::listArray($this->getCcy(),'CCY_ID','DESCRIPTION'));
		$this->view->ccy = $listCcy;
		$this->_helper->layout()->setLayout('popup');
	}

	public function indexAction()
	{
		$fields = array(
			'benef_acct'  => array('field' => 'BENEFICIARY_ACCOUNT',
								   'label' => 'Account No',
								   'sortable' => true),
			'alias'  => array('field' => 'BENEFICIARY_ALIAS',
								   'label' => 'Alias Name',
								   'sortable' => true),
			'benef_name'  => array('field' => 'BENEFICIARY_NAME',
								   'label' => 'Account Name',
								   'sortable' => true),
			'ccy'   => array('field'    => 'CURR_CODE',
								  'label'    => 'CCY',
								  'sortable' => true),
			'bank_name'   => array('field'    => 'BANK_NAME',
								  'label'    => 'Bank Name',
								  'sortable' => true),
			'bank_code'   => array('field'    => 'BANK_CODE',
								  'label'    => 'Bank Code',
								  'sortable' => true)
		);

		//get page, sortby, sortdir
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','doc_no');
		$sortDir = $this->_getParam('sortdir','asc');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;

		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';


		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	'filter' 	  	=> array('StringTrim','StripTags'),
							'alias' 	  	=> array('StringTrim','StripTags'),
							'benef_acct'    => array('StringTrim','StripTags','StringToUpper'),
							'benef_name'    => array('StringTrim','StripTags'),
							'favorit'     	=> array('StringTrim','StripTags')
		);

		$zf_filter = new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		$filter = $zf_filter->getEscaped('filter');

		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;

		$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$select   = $CustUser->getBeneficiaries($this->view->hasPrivilege('BLBU'));
		$select->where("B.BENEFICIARY_TYPE = ?", '8');
		$select->where("B.BENEFICIARY_ISAPPROVE = 1");
		$select->where("B.BANK_CODE IS NOT NULL");

		// view bene existed in bank table domestic
		$select->join(array('K' => 'M_DOMESTIC_ONLINE_BANK_TABLE'), 'B.BANK_CODE = K.BANK_OL_CODE OR B.CLR_CODE = K.BANK_OL_CODE', array());

		if($filter == 'Set Filter')
		{
			$fAlias = $zf_filter->getEscaped('alias');
			$fAcct = $zf_filter->getEscaped('benef_acct');
			$fName = $zf_filter->getEscaped('benef_name');
			$fFav = $zf_filter->getEscaped('favorit');

	        if($fAlias)$select->where('UPPER(B.BENEFICIARY_ALIAS) LIKE '.$this->_db->quote('%'.strtoupper($fAlias).'%'));
	        if($fAcct)$select->where('B.BENEFICIARY_ACCOUNT LIKE '.$this->_db->quote('%'.strtoupper($fAcct).'%'));
	        if($fName)$select->where('UPPER(B.BENEFICIARY_NAME) LIKE '.$this->_db->quote('%'.strtoupper($fName).'%'));
	        if($fFav==1)$select->where('B.ISFAVORITE=1');

			$this->view->alias = $fAlias;
			$this->view->benef_acct = $fAcct;
			$this->view->benef_name = $fName;
			if($fFav==1)$this->view->favorit = true;
			else $this->view->favorit = false;
		}
		else $this->view->favorit = false;
		 //echo $select;die;
	    $select->order($sortBy.' '.$sortDir);
		$this->paging($select);
		$this->view->fields = $fields;
		$this->view->filter = $filter;

		//$this->_helper->download->csv(array('Group ID','Group Name','Status'),null,$select,'download group');
	}


	private function fillParam($zf_filter_input)
	{
		if(isset($zf_filter_input->BENEFICIARY_ID))$this->view->BENEFICIARY_ID = ($zf_filter_input->isValid('BENEFICIARY_ID')) ? $zf_filter_input->BENEFICIARY_ID : $this->_getParam('BENEFICIARY_ID');
		$this->view->ACBENEF_ALIAS = ($zf_filter_input->isValid('ACBENEF_ALIAS')) ? $zf_filter_input->ACBENEF_ALIAS : $this->_getParam('ACBENEF_ALIAS');
		$this->view->ACBENEF = ($zf_filter_input->isValid('ACBENEF')) ? $zf_filter_input->ACBENEF : $this->_getParam('ACBENEF');
		$this->view->CURR_CODE = ($zf_filter_input->isValid('CURR_CODE')) ? $zf_filter_input->CURR_CODE : $this->_getParam('CURR_CODE');
		$this->view->ACBENEF_EMAIL = ($zf_filter_input->isValid('ACBENEF_EMAIL')) ? $zf_filter_input->ACBENEF_EMAIL : $this->_getParam('ACBENEF_EMAIL');
	}

}
