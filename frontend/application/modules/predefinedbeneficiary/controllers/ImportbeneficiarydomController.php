<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';
require_once 'CMD/Validate/ValidateAccountBeneficiary.php';

class predefinedbeneficiary_ImportbeneficiarydomController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti

	public function initController(){
		$this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';
		$this->_listCCYValidate = Application_Helper_Array::simpleArray($this->getCcy(),'CCY_ID');
	}

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		if($this->_request->isPost() )
		{

			// Set variables needed in view
			$settings = new Application_Settings();
			$ccyList  = $settings->setCurrencyRegistered();
			$privibenelinkage = $this->view->hasPrivilege('BLBU');


			$adapter = new Zend_File_Transfer_Adapter_Http ();
			$adapter->setDestination ( $this->_destinationUploadDir );
			$extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv'));
			$extensionValidator->setMessage(
				//$language->_('Error: Extension file must be').' *.csv'
				$this->language->_('Error: Extension file must be').'*.csv'
			);

			$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
			$sizeValidator->setMessage(
				$this->language->_('Error: File size must not more than').' '.$this->getSetting('Fe_attachment_maxbyte')
			);

			$adapter->setValidators ( array (
				$extensionValidator,
				$sizeValidator,
			));

			//ambil data m_user ->ambil sourceAccount, acctType, statusnya aktif=1
			$acctStatus = 1;
			$dataAcct = $this->_db->fetchRow(
				$this->_db->select()
				->from(array('C' => 'M_CUSTOMER_ACCT'))
				->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin))
				->where("ACCT_STATUS = ?",$acctStatus)
				->limit(1)
			);
			$ACCT_NO = (!empty($dataAcct['ACCT_NO'])?$dataAcct['ACCT_NO']:'');
			$ACCT_TYPE = (!empty($dataAcct['ACCT_TYPE'])?$dataAcct['ACCT_TYPE']:'');

			if ($adapter->isValid ())
			{

				$sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
				$newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

				$adapter->addFilter ( 'Rename',$newFileName  );

				if ($adapter->receive ())
				{
					//PARSING CSV HERE
					$csvData = $this->parseCSV($newFileName);
					//after parse delete document temporary
					@unlink($newFileName);
					//end

					$totalRecords = count($csvData);
					if($totalRecords)
					{
						unset($csvData[0]);
						$totalRecords = count($csvData);
					}

					if($totalRecords)
					{
						$maxTotalRows 	= $this->getSetting('max_import_bene');

						if($totalRecords <= $maxTotalRows)
						{
							$rowNum = 0;
							$totalSucces = 0;
							$totalFailed = 0;
							$errmsg = array();

							foreach ( $csvData as $columns )
							{
//								Zend_Debug::dump($columns);
//								Zend_Debug::dump(count($columns));
//								die;
								if(count($columns)==12)
								{
									$BENEFICIARY_TYPE_COL = trim($columns[0]);
									$BENEFICIARY_ACCOUNT_COL = strtoupper(trim($columns[1]));
									$BENEFICIARY_ACCOUNT_NAME_COL = trim($columns[2]);
									$CITIZENSHIP_COL = trim($columns[3]);
									$NATIONALITY_COL = trim($columns[4]);
									$CATEGORY_COL = trim($columns[5]);
									$IDENTIFICATION_TYPE_COL = trim($columns[6]);
									$IDENTIFICATION_NUMBER_COL = trim($columns[7]);
									$ADDRESS_COL = trim($columns[8]);
									$BANK_CODE_COL = trim($columns[9]);
									$CITY_CODE_COL = trim($columns[10]);
									$EMAIL_ADDRESS_COL = trim($columns[11]);
									$filter = new Application_Filtering();
						//-------------------------------------------------------------START VALIDATION---------------------------------------------------------------------//
									$BENEFICIARY_TYPE = $filter->filter($BENEFICIARY_TYPE_COL  , "SELECTION");
									$BENEFICIARY_ACCOUNT = $filter->filter($BENEFICIARY_ACCOUNT_COL , "SELECTION");
									$CITIZENSHIP = $filter->filter($CITIZENSHIP_COL  , "SELECTION");
									$NATIONALITY = $filter->filter($NATIONALITY_COL  , "SELECTION");
									$CATEGORY = $filter->filter($CATEGORY_COL  , "SELECTION");
									$IDENTIFICATION_TYPE = $filter->filter($IDENTIFICATION_TYPE_COL  , "IDENTIFICATION_TYPE_COL");
									$IDENTIFICATION_NUMBER = $filter->filter($IDENTIFICATION_NUMBER_COL  , "IDENTIFICATION_NUMBER_");
									$ADDRESS = $filter->filter($ADDRESS_COL  , "SELECTION");
									$BENEFICIARY_ACCOUNT_NAME = $filter->filter($BENEFICIARY_ACCOUNT_NAME_COL  , "BENEFICIARY_ACCOUNT_NAME_COL");
									$BANK_CODE = $filter->filter($BANK_CODE_COL  , "BANK_CODE_COL");
									$CITY_CODE = $filter->filter($CITY_CODE_COL  , "CITY_CODE_COL"); //jadi input city name
									$EMAIL_ADDRESS = $filter->filter($EMAIL_ADDRESS_COL  , "EMAIL");

									if($BENEFICIARY_TYPE == 'SKN' || $BENEFICIARY_TYPE == 'RTGS'){
										$dataBank = $this->_db->fetchRow(
											$this->_db->select()
											->from(array('C' => 'M_DOMESTIC_BANK_TABLE'))
											->join(array('B'=>'M_BANK_TABLE'), 'B.CLR_CODE = C.CLR_CODE',array('BANK_CODE'))
	//										->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin))
											->where("C.CLR_CODE = ?",$BANK_CODE)
											->limit(1)
										);


										$CLR_CODE = (!empty($dataBank['CLR_CODE'])?TRUE:FALSE);
										$CLR_CODE_ORI = (!empty($dataBank['CLR_CODE'])?$dataBank['CLR_CODE']:'');
										$BANK_NAME = (!empty($dataBank['BANK_NAME'])?$dataBank['BANK_NAME']:'');
										$BENEFICIARY_BANK_CODE = (!empty($dataBank['BANK_CODE'])?$dataBank['BANK_CODE']:'');

										$dataCity = $this->_db->fetchRow(
											$this->_db->select()
											->from(array('C' => 'M_CITY'))
	//										->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin))
											//->where("CITY_CODE = ?",$CITY_CODE)
											->where('BANK_NAME = ?', $BANK_NAME)
											->where('CITY_NAME = ?', strtoupper($CITY_CODE)) //query city code by bank_name & city_name
											->limit(1)
										);
										$CITY_CODE = (!empty($dataCity['CITY_CODE'])?TRUE:FALSE);
										$CITY_CODE_ORI = (!empty($dataCity['CITY_CODE'])?$dataCity['CITY_CODE']:FALSE);
										$CITY_NAME = (!empty($dataCity['CITY_NAME'])?$dataCity['CITY_NAME']:'');
										///////////////////////////
										if($CATEGORY == 'COMPANY'){
											$CATEGORY_CODE = '5';
										}
										elseif($CATEGORY == 'INDIVIDUAL'){
											$CATEGORY_CODE = '1';
										}
										elseif($CATEGORY == 'GOVERNMENT'){
											$CATEGORY_CODE = '2';
										}
										elseif($CATEGORY == 'BANK'){
											$CATEGORY_CODE = '3';
										}
										elseif($CATEGORY == 'NON BANK FINANCIAL INSTITUTION'){
											$CATEGORY_CODE = '4';
										}
										elseif($CATEGORY == 'OTHER'){
											$CATEGORY_CODE = '6';
										}

										if($IDENTIFICATION_TYPE == 'KTP'){
											$IDENTIFICATION_TYPE_CODE = 'KTP';
										}
										elseif($IDENTIFICATION_TYPE == 'INDIVIDUAL'){
											$IDENTIFICATION_TYPE_CODE = 'SIM';
										}
										elseif($IDENTIFICATION_TYPE == 'PASPOR'){
											$IDENTIFICATION_TYPE_CODE = 'PAS';
										}
										elseif($IDENTIFICATION_TYPE == 'KITAS'){
											$IDENTIFICATION_TYPE_CODE = 'KIT';
										}

										if($NATIONALITY == 'WNI'){
											$NATIONALITY_CODE = 'W';
										}
										elseif($NATIONALITY == 'WNA'){
											$NATIONALITY_CODE = 'N';
										}
									}
									else{
										$dataBank = $this->_db->fetchRow(
											$this->_db->select()
											->from(array('C' => 'M_DOMESTIC_ONLINE_BANK_TABLE'))
	//										->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin))
											->where("BANK_OL_CODE = ?",$BANK_CODE)
											->limit(1)
										);
										$CLR_CODE = (!empty($dataBank['BANK_OL_CODE'])?TRUE:FALSE);
										$CLR_CODE_ORI = (!empty($dataBank['BANK_OL_CODE'])?$dataBank['BANK_OL_CODE']:'');
										$BANK_NAME = (!empty($dataBank['BANK_OL_NAME'])?$dataBank['BANK_OL_NAME']:'');
									}
									$paramBene = array(
													"FROM"      			=> 'B',
													 "ACBENEF"     			=> $BENEFICIARY_ACCOUNT,
													 "ACBENEF_CCY"    		=> 'IDR',
//													 "ACBENEF_ALIAS"    	=> $ACBENEF_ALIAS,
													 "ACBENEF_EMAIL"   		=> $EMAIL_ADDRESS,
													 "BANK_CODE"    	 	=> $BANK_CODE,
													 "BENEFICIARY_BANK_CODE"   => $BENEFICIARY_BANK_CODE,
													 "_addBeneficiary"   	=> $this->view->hasPrivilege('BADA'),
													 "_beneLinkage"    		=> $privibenelinkage,
													 "ACCTSRC"     			=> $ACCT_NO, //source account m_user yg login
													 "VALIDATE"     		=> '1',
													 "sourceAccountType"    => $ACCT_TYPE,
													 "IMPORTDOM"    		=> TRUE,
												);
									if($BENEFICIARY_TYPE == 'SKN' || $BENEFICIARY_TYPE == 'RTGS'){
										$paramAdd = array("TRANSFER_TYPE"      	=>  'SKN',
																			 "ACBENEF_BANKNAME" 	=> $BENEFICIARY_ACCOUNT_NAME,
																			 "ACBENEF_ADDRESS1"   	=> $ADDRESS,
																			 "ACBENEF_CITIZENSHIP"  => $CITIZENSHIP,
																			 "ACBENEF_RESIDENT"  	=> $NATIONALITY,

																			 "NATIONALITY"  		=> $NATIONALITY,
										 									 "NATIONALITY_CODE"  	=> $NATIONALITY_CODE,

																			 "LLD_CATEGORY"  		=> $CATEGORY,
																			 "LLD_BENEIDENTIF"  	=> $IDENTIFICATION_TYPE,

																			 "LLD_CATEGORY_ORI"  	=> $CATEGORY_CODE,
																			 "LLD_BENEIDENTIF_ORI"  => $IDENTIFICATION_TYPE_CODE,

																			 "LLD_BENENUMBER"  		=> $IDENTIFICATION_NUMBER,
																			 "CITY_CODE"  			=> $CITY_CODE,
																			 "CITY_CODE_ORI"  		=> $CITY_CODE_ORI,
//																			 "BANK_CODE"  			=> $BANK_CODE,
																			 "CLR_CODE"  			=> $CLR_CODE,
																			 "CLR_CODE_ORI"  		=> $CLR_CODE_ORI,
																			 "TYPE"  				=> 2,
																			 "BANK_NAME"  			=> $BANK_NAME,
																			 "CITY_NAME"  			=> $CITY_NAME,
																			);
									}
									else{
										$paramAdd = array("TRANSFER_TYPE"      	=>  'ONLINE',
																			 "ACBENEF_BANKNAME" 	=> $ACBENEF_BANKNAME,
																			 "ACBENEF_ADDRESS1"   	=> '',
																			 "ACBENEF_CITIZENSHIP"  => 'R', //RESIDENT
																			 "ACBENEF_RESIDENT"  	=> 'W', //CITIZENSHIP

																			 "NATIONALITY"  		=> '',
										 									 "NATIONALITY_CODE"  	=> 'W',


																			 "LLD_CATEGORY"  		=> '',
																			 "LLD_BENEIDENTIF"  	=> '',
																			 "LLD_BENENUMBER"  		=> '',
																			 "LLD_CATEGORY_ORI"  	=> '',
																			 "LLD_BENEIDENTIF_ORI"  => '',

																			 "CITY_CODE"  			=> '',
																			 "TYPE"  				=> 8,
//																			 "BANK_CODE"  			=> $BANK_CODE,
																			 "CLR_CODE"  			=> $CLR_CODE,
																			 "CLR_CODE_ORI"  		=> $CLR_CODE_ORI,
																			 "BANK_NAME"  			=> $BANK_NAME,
																			 "CITY_NAME"  			=> '',
																			);
									}

						     		$paramBene += array_merge($paramBene,$paramAdd);

									$zf_filter_input = array('*' => array('HtmlEntities', 'StripTags', 'StringTrim'));
									$validators = array('BENEFICIARY_ACCOUNT' 		=> array(	'NotEmpty',
																					'Digits',
																					new Zend_Validate_StringLength(array('max'=>15,'min'=>8)),
																					'messages' => array(
																						'Beneficiary Account cannot be left blank. Please correct it.',
																						'Beneficiary Account must be numbers. Please correct it.',
																						'Beneficiary Account length must be 10 digits. Please correct it.',
																						)
																					),
														'CURR_CODE' 	=> array(	'NotEmpty',
																						array('InArray', array('haystack' => $this->_listCCYValidate)),
																						'messages' => array(
																						'Currency cannot be left blank. Please correct it.',
																						'Currency is not on the database.')
																				),
																);

									$zf_filter_input = new Zend_Filter_Input($zf_filter_input, $validators, $this->_getAllParams(), array('breakChainOnFailure' => true));

									if($zf_filter_input->isValid()){
//										Zend_Debug::dump($paramBene);
//										die;
									 	$validateACBENEF = new ValidateAccountBeneficiary($BENEFICIARY_ACCOUNT, $this->_custIdLogin, $this->_userIdLogin);
									 	$validateACBENEF->check($paramBene);

									 	$content[$rowNum] = array(
													'CUST_ID_LOGIN' 			=> $this->_custIdLogin,
													'USER_ID_LOGIN' 			=> $this->_userIdLogin,
													//'BENEFICIARY_ALIAS' 		=> $ACBENEF_ALIAS,
													'BENEFICIARY_ACCOUNT' 		=> $BENEFICIARY_ACCOUNT,
													'BENEFICIARY_NAME' 			=> $BENEFICIARY_ACCOUNT_NAME, //$paramBene['ACBENEF_BANKNAME'],
													'BENEFICIARY_NAME_CEK'		=> $paramBene['ACBENEF_BANKNAME'],
													'CURR_CODE' 				=> 'IDR',
													'BENEFICIARY_EMAIL' 		=> $EMAIL_ADDRESS,
													'BENEFICIARY_CITIZENSHIP_ori' 	=> $NATIONALITY,
													'BENEFICIARY_CITIZENSHIP' 	=> $paramBene['NATIONALITY_CODE'],
													'BENEFICIARY_ADDRESS'		=> $paramBene['ACBENEF_ADDRESS1'],//$ADDRESS,
													'BENEFICIARY_RESIDENT' 		=> $paramBene['ACBENEF_CITIZENSHIP'],//$CITIZENSHIP,

													'BANK_NAME'					=> $paramBene['BANK_NAME'],
													'BENEFICIARY_TYPE' 			=> $paramBene['TYPE'],
													'BENEFICIARY_TYPE_NAME' 	=> $BENEFICIARY_TYPE,

													'BENEFICIARY_CATEGORY' 		=> $paramBene['LLD_CATEGORY_ORI'],
													'BENEFICIARY_ID_TYPE' 		=> $paramBene['LLD_BENEIDENTIF_ORI'],

													'BENEFICIARY_CATEGORY_ORI' 	=> $paramBene['LLD_CATEGORY'],
													'BENEFICIARY_ID_TYPE_ORI' 	=> $paramBene['LLD_BENEIDENTIF'],

													'BENEFICIARY_ID_NUMBER' 	=> $paramBene['LLD_BENENUMBER'],
													'BENEFICIARY_CITY_CODE' 	=> $paramBene['CITY_CODE_ORI'],
									 				'CLR_CODE' 					=> $paramBene['CLR_CODE_ORI'],
//													'BANK_CODE' 				=> $BANK_CODE,
									 				'BANK_NAME' 				=> $paramBene['BANK_NAME'],
									 				'CITY_NAME' 				=> $paramBene['CITY_NAME'],
													'_beneLinkage'     			=> $privibenelinkage,
										);


										 if ($validateACBENEF->isError())
										 {
											$content[$rowNum] += array(
													'ERROR_MESSAGE' => $validateACBENEF->getErrorMsg()
											);
										 }

										 $validateACBENEF->__destruct();
										 unset($validateACBENEF);
									 }
									 else{
									 	//Zend_Debug::dump($zf_filter_input->getMessages());die;
//									 	die;
									 	$content[$rowNum] += array( 'ERROR_MESSAGE' => 	$this->language->_('Benficiary Account data is invalid'));
//									 	$content[$rowNum] += array( 'ERROR_MESSAGE' => 	$zf_filter_input->getMessages());
									 }

								 $rowNum++;
								}else{
									$this->view->error =1;
									//$errmsg[] = 'Number of column for all rows to be imported should be 4';
									$errmsg[] = 'Wrong File Format';
									$this->view->errmsg = $this->DisplayError($errmsg);
									$sessionNamespace = new Zend_Session_Namespace('errimportdom');
									 $sessionNamespace->error = $this->DisplayError($errmsg);
									 $this->_redirect('/predefinedbeneficiary/domesticcct'); 
									break;
								}
							}
							if(!$errmsg)
							{
								$sessionNamespace = new Zend_Session_Namespace('import_predefbenef');
								$sessionNamespace->content = $content;
								//Zend_Debug::dump($sessionNamespace->content);die;
								//$_SESSION['import_predefbenef'] = $content;
								$this->_redirect("predefinedbeneficiary/importbeneficiaryapprovedom");
								//Zend_Debug::dump($_SESSION['import_predefbenef']);die;
								Application_Helper_General::writeLog('BIMA','Import Beneficiary');
							}
						}
						else{
							$this->view->error =1;
							$errmsg[] = 'Total row imported cannot be more than'.$maxTotalRows;
							$this->view->errmsg = $this->DisplayError($errmsg);
							$sessionNamespace = new Zend_Session_Namespace('errimportdom');
									 $sessionNamespace->error = $this->DisplayError($errmsg);
									 $this->_redirect('/predefinedbeneficiary/domesticcct'); 
						}
					}
					else{
						$this->view->error =1;
// 						$errmsg[] = 'Number of column for all rows to be imported should be 4';
						$errmsg[] = 'Wrong File Format. There is no data on csv File.';
						$this->view->errmsg = $this->DisplayError($errmsg);
						$sessionNamespace = new Zend_Session_Namespace('errimportdom');
									 $sessionNamespace->error = $this->DisplayError($errmsg);
									 $this->_redirect('/predefinedbeneficiary/domesticcct'); 
					}
				}
			}
			else
			{
				$this->view->error =1;
				$this->view->errmsg = $this->DisplayError($adapter->getMessages());
				$sessionNamespace = new Zend_Session_Namespace('errimportdom');
									 $sessionNamespace->error = $this->DisplayError($adapter->getMessages());
									 $this->_redirect('/predefinedbeneficiary/domesticcct'); 
				//Zend_Debug::dump($adapter->getMessages());die;
			}
		}
		Application_Helper_General::writeLog('BIMA','Viewing Import Beneficiary');
	}

	private function parseCSV($fileName){
		$csvData = false;
		try {
				$Csv = new Application_Csv (  $fileName, $separator = "," );
				$csvData = $Csv->readAll ();
			} catch ( Exception $e ) {
				echo nl2br ( $e->getTraceAsString () );
			}
			return $csvData;
	}

}
