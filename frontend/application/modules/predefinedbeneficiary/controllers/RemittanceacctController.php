<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';
require_once 'General/CustomerUser.php';
require_once 'CMD/Validate/ValidateAccountBeneficiary.php';
require_once 'Service/Token.php'; //added new
require_once 'CMD/Validate/Validate.php';
require_once 'Crypt/AESMYSQL.php';

class predefinedbeneficiary_RemittanceacctController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti
	protected $_payType;
	public function initController()
	{
		$selectCurrency = '-- ' . $this->language->_('Select Currency') . '--';
		$listCcy = array('' => $selectCurrency);
		if (count($this->getCcy()) == 1) { //remove useless value
			$listCcy = array();
		}
		$listCcy = array_merge($listCcy, Application_Helper_Array::listArray($this->getCcy(), 'CCY_ID', 'CCY_ID'));
		$this->view->ccy = $listCcy;
		//$this->_payType = "'".$this->_beneftype["code"]["domestic"]."' , '".$this->_beneftype["code"]["online"]."'";
		//$this->_payType = "'".$this->_beneftype["code"]["remittance"]."'";


	}

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$model = new predefinedbeneficiary_Model_Predefinedbeneficiary();

		$setting = new Settings();
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$pw_hash = md5($enc_salt . $enc_pass);
		$rand = $this->_userIdLogin . date('dHis') . $pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;



		$aliasname = $this->language->_('Alias Name');
		$beneficiaryaccount = $this->language->_('Beneficiary Account');

		$beneficiaryaccountname = $this->language->_('Beneficiary Account Name');
		$emailaddress = $this->language->_('Email Address');
		$ccy = $this->language->_('Currency');
		$favorite = $this->language->_('Favorite');
		$bankcode = $this->language->_('Bank Code');
		$status = $this->language->_('Status');
		$bankname = $this->language->_('Bank Name');
		$delete = $this->language->_('Delete');
		$address = $this->language->_('Address');
		$citizenship = $this->language->_('Citizenship');
		$nationality = $this->language->_('Nationality');
		$beneftype = $this->language->_('Beneficiary Type');
		$favorite = $this->language->_('Favorite');




		$fields = array(
			/*'alias'  => array('field' => 'BENEFICIARY_ALIAS',
											   'label' => $aliasname,
											   'sortable' => true),*/
			'bankname'   => array(
				'field'    => 'BANK_NAME',
				'label'    => $bankname,
				'sortable' => true
			),
			'benef_acct'  => array(
				'field' => 'BENEFICIARY_ACCOUNT',
				'label' => $beneficiaryaccount,
				'sortable' => true
			),
			'benef_name'  => array(
				'field' => 'BENEFICIARY_NAME',
				'label' => $beneficiaryaccountname,
				'sortable' => true
			),
			'email'  => array(
				'field' => 'BENEFICIARY_EMAIL',
				'label' => $emailaddress,
				'sortable' => true
			),
			'ccy'   => array(
				'field'    => 'CURR_CODE',
				'label'    => $ccy,
				'sortable' => true
			),
			//'favorite'   => array('field'    => 'FAVORITE',
			//					  'label'    => $favorite,
			//					  'sortable' => true),
			'status'   => array(
				'field'    => 'STATUS',
				'label'    => $status,
				'sortable' => true
			),
			// 'citizenship'   => array('field'    => 'BENEFICIARY_CITIZENSHIP_disp',
			// 					  'label'    => $citizenship,
			// 					  'sortable' => true),
			// 'nationality'   => array('field'    => 'BENEFICIARY_NATIONALITY_disp',
			// 					  'label'    => $nationality,
			// 					  'sortable' => true),
			/*'address'   => array('field'    => 'BENEFICIARY_ADDRESS',
											  'label'    => $address,
											  'sortable' => true),
						'bankcode'   => array('field'    => 'BANK_CODE_disp',
											  'label'    => $bankcode,
											  'sortable' => true),*/

			/*'beneType'   => array('field'    => 'BENEFICIARY_TYPE_disp',
											  'label'    => $beneftype,
											  'sortable' => true),*/
			//'bankchecked'   => array('field'    => 'CHECK_BY_BANK',
			//						'label'    => 'Checked by Bank',
			//						'sortable' => true),
			// 'bankcode'   => array('field'    => 'BANK_CODE_disp',
			// 						'label'    => 'Bank Code',
			// 						'sortable' => true),

			/*'beneType'   => array('field'    => 'BENEFICIARY_TYPE_disp',
											  'label'    => $this->language->_('Beneficiary Type'),
											  'sortable' => true),*/
			// 'status'   => array('field'    => 'BENEFICIARY_ISAPPROVE_disp',
			// 					  'label'    => $status,
			// 					  'sortable' => true),
			//						'createddate'   => array('field'    => 'CREATED_DATE',
			//												'label'    => 'Created Date',
			//												'sortable' => true)
			// 'delete'   => array('field'    => 'BENEFICIARY_ISREQUEST_DELETE',
			// 					  'label'    => $delete,
			// 					  'sortable' => false)
		);
		$filterlist = array('BENEFICIARY_ACCOUNT', 'BENEFICIARY_NAME');

		$this->view->filterlist = $filterlist;

		//get page, sortby, sortdir
		$csv    = $this->_getParam('csv');
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby', 'doc_no');
		$sortDir = $this->_getParam('sortdir', 'asc');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page, 'Digits') && ($page > 0)) ? $page : 1;

		$sortBy = (Zend_Validate::is(
			$sortBy,
			'InArray',
			array(array_keys($fields))
		)) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is(
			$sortDir,
			'InArray',
			array('haystack' => array('asc', 'desc'))
		)) ? $sortDir : 'asc';


		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(
			'filter' 	  	=> array('StringTrim', 'StripTags'),
			'alias' 	  	=> array('StringTrim', 'StripTags'),
			'BENEFICIARY_ACCOUNT'    => array('StringTrim', 'StripTags', 'StringToUpper'),
			'BENEFICIARY_NAME'    => array('StringTrim', 'StripTags')
		);
		$dataParam = array('BENEFICIARY_ACCOUNT', 'BENEFICIARY_NAME');
		$dataParamValue = array();
		foreach ($dataParam as $dtParam) {

			// print_r($dtParam);die;
			if (!empty($this->_request->getParam('wherecol'))) {
				$dataval = $this->_request->getParam('whereval');
				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if ($dtParam == $value) {
						$dataParamValue[$dtParam] = $dataval[$key];
					}
				}
			}

			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}

		$zf_filter = new Zend_Filter_Input($filterArr, array(), $dataParamValue);
		// $filter = $zf_filter->getEscaped('filter');
		$filter = $this->_request->getParam('filter');

		$alpha = $this->_getParam('alpha');

		$this->view->currentPage = $page;
		$this->view->sortBy = $param['sortBy'] = $sortBy;
		$this->view->sortDir = $param['sortDir'] = $sortDir;
		$this->view->alpha = $alpha;

		if ($filter == TRUE) {
			$fAlias = $zf_filter->getEscaped('alias');
			$fAcct = $zf_filter->getEscaped('BENEFICIARY_ACCOUNT');
			$fName = $zf_filter->getEscaped('BENEFICIARY_NAME');
			$payType = $zf_filter->getEscaped('benef_type');

			if ($fAlias) $param['fAlias'] = $fAlias;
			if ($fAcct) $param['fAcct'] = $fAcct;
			if ($fName) $param['fName'] = $fName;
			if ($payType) $param['payType'] = $payType;

			$this->view->alias = $fAlias;
			$this->view->benef_acct = $fAcct;
			$this->view->benef_name = $fName;
			$this->view->benef_type = $payType;
		}

		$param['user_id'] = $this->_userIdLogin;
		$param['cust_id'] = $this->_custIdLogin;
		$param['payType'] = $this->_beneftype["code"]["remittance"];
		$param['beneLinkage'] = $this->view->hasPrivilege('BLBU');
		/*if(empty($payType))
			$param['payType'] = $this->_payType;*/
		if ($alpha) $param['alpha'] = $alpha;

		$select   = $model->getBeneficiaries($param);
		// print_r($select);die;
		$this->paging($select);

		$data_val = array();
		foreach ($select as $valu) {
			$paramTrx = array(
				"BENEFICIARY_ACCOUNT" => $valu['BENEFICIARY_ACCOUNT'],
				"BENEFICIARY_NAME"	=> $valu['BENEFICIARY_NAME'],
				"BENEFICIARY_EMAIL"	=> $valu['BENEFICIARY_EMAIL'],
				"CURR_CODE"	=> $valu['CURR_CODE'],
				"ISFAVORITE"	=> $valu['ISFAVORITE'],
				"BENEFICIARY_ISAPPROVE_disp"	=> $valu['BENEFICIARY_ISAPPROVE_disp'],
				"BENEFICIARY_BANKSTATUS_disp"	=> $valu['BENEFICIARY_BANKSTATUS_disp'],
				"BENEFICIARY_CREATED"	=> $valu['BENEFICIARY_CREATED'],
			);
			array_push($data_val, $paramTrx);
		}

		if ($csv) {

			$header  = Application_Helper_Array::simpleArray($fields, "label");
			$this->_helper->download->csv($header, $data_val, null, 'Beneficiary List');
		}
		$this->view->payType = array('' => ' -- Please Select -- ', $this->_beneftype["code"]["domestic"] => $this->_beneftype["desc"]["domestic"], $this->_beneftype["code"]["online"] => $this->_beneftype["desc"]["online"]);
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		if (!empty($this->_request->getParam('wherecol'))) {
			$this->view->wherecol			= $this->_request->getParam('wherecol');
		}

		if (!empty($this->_request->getParam('whereopt'))) {
			$this->view->whereopt			= $this->_request->getParam('whereopt');
		}

		if (!empty($this->_request->getParam('whereval'))) {
			$this->view->whereval			= $this->_request->getParam('whereval');
		}
		Application_Helper_General::writeLog('BLBA', 'View Destination Account');
	}

	public function addbeneficiaryAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$this->view->bankpop = 'disabled';
		$anyValue = '-- ' . $this->language->_('Select City') . ' --';
		//		$payment					= new payment_Model_Payment();
		$modelCity = new predefinedbeneficiary_Model_Predefinedbeneficiary();

		//		$arr 					= $modelCity->getCity();
		//		$cityCodeArr 			= array(''=> $anyValue);
		//		$cityCodeArr 			+= Application_Helper_Array::listArray($arr,'CITY_CODE','CITY_NAME');
		//		$this->view->cityCodeArr 	= $cityCodeArr;
		//		$SWIFT_CODE   = $this->_request->getParam('SWIFT_CODE');
		$list 							= $modelCity->getCity();
		$this->view->cityCodeList 		= $list;
		//		$CITY_CODE = $this->_getParam('CITY_CODE');
		//		$this->view->CITY_CODE = $CITY_CODE; //INSERT CITY_CODE FIELD - BENEFICIARY_CITY_CODE

		$Settings = new Settings();
		$address_mandatory = $Settings->getSetting('address_mandatory');
		$this->view->address_mandatory = $address_mandatory;

		$settings 			= new Application_Settings();
		$lldCategoryArr  	= $settings->getLLDDOMCategory();
		$this->view->lldCategoryArr 	= $lldCategoryArr;
		//		$LLD_CATEGORY 		= $this->_request->getParam('LLD_CATEGORY');
		//		$this->view->LLD_CATEGORY = $LLD_CATEGORY; //INSERT CATEGORY FIELD - BENEFICAIRY_CATEGORY

		$ccyList  = $settings->setCurrencyRegistered();
		$this->view->ccyArr 			= $ccyList;

		$lldBeneIdentifArr 	= $settings->getLLDDOMBeneIdentification();
		$this->view->lldBeneIdentifArr  = $lldBeneIdentifArr;
		//		$LLD_BENEIDENTIF 		= $this->_request->getParam('LLD_BENEIDENTIF');
		//		$this->view->LLD_BENEIDENTIF = $LLD_BENEIDENTIF; //INSERT IDENTIFICATION TYPE FIELD - BENEFICIARY_ID_TYPE

		$this->view->CITIZENSHIP = $this->_getParam('CITIZENSHIP');
		//$this->view->benefType = '1';
		$privibenelinkage = $this->view->hasPrivilege('BLBA');
		$Beneficiary = new Beneficiary();
		$filter  = new Application_Filtering();
		$settings = new Application_Settings();
		$ccyList  = $settings->setCurrencyRegistered();

		$this->view->ccyArr 			= $ccyList;

		//ambil data m_user ->ambil sourceAccount, acctType, statusnya aktif=1
		$acctStatus = 1;
		$dataAcct = $this->_db->fetchRow(
			$this->_db->select()
				->from(array('C' => 'M_USER_ACCT'))
				->where("USER_ID = " . $this->_db->quote($this->_userIdLogin))
				->where("ACCT_STATUS = ?", $acctStatus)
				->limit(1)
		);
		$ACCT_NO = (!empty($dataAcct['ACCT_NO']) ? $dataAcct['ACCT_NO'] : '');
		$ACCT_TYPE = (!empty($dataAcct['ACCT_TYPE']) ? $dataAcct['ACCT_TYPE'] : '');

		//validasi token -- begin
		$select3 = $this->_db->select()
			->from(array('C' => 'M_USER'));
		$select3->where("USER_ID = " . $this->_db->quote($this->_userIdLogin));
		$data2 					= $this->_db->fetchRow($select3);
		$tokenIdUser 			= $data2['TOKEN_ID'];
		//$tokenType 				= $data2['TOKEN_TYPE'];
		$tokenType = "";
		$usermobilephone 		= $data2['USER_MOBILE_PHONE'];
		//$this->view->tokentype 	= $data2['TOKEN_TYPE'];
		$this->view->tokentype = "";

		//added new hard token
		$HardToken = new Service_Token($this->_custIdLogin, $this->_userIdLogin, $tokenIdUser);
		$challengeCode = $HardToken->generateChallengeCode();

		$challengeCodeSub = substr($challengeCode, 0, 2);
		$this->view->challengeCodeReq = $challengeCodeSub;
		//validasi token -- end

		if ($this->_request->isPost()) {
			$BENEFICIARY_BANK_CODE   = $this->_getParam('BENEFICIARY_BANK_CODE');
			$BANK_CITY   = '';
			$CLR_CODE   = '';
			//$CLR_CODE   = $this->_request->getParam('CLR_CODE');
			$ACBENEF_ADDRESS = '';
			$ACBENEF_CITIZENSHIP = '';
			$ACBENEF_RESIDENT = '';
			$ACBENEF_BANKNAME  = '';

			$LLD_CATEGORY = '';
			$LLD_BENEIDENTIF  = '';
			$LLD_BENENUMBER = '';
			$CITY_CODE  = '';

			//$benefType = $this->_getParam('benefType');
			//setelah disamakan dgn FSD
			$benefType = '3'; //$this->_getParam('benefType'); //kalo dua ONLINE harus panggil fundtransferinquiry, kalo 1 skn rtgs langsung lewat

			/*if($_POST){
				Zend_Debug::dump($this->_request->getParams());
			}*/

			//if($benefType == 1){
			$BANK_CITY   = $this->_request->getParam('BANK_CITY');
			$CLR_CODE   = $this->_request->getParam('BENEFICIARY_BANK_CODE');

			$ACBENEF_ADDRESS = $this->_request->getParam('ADDRESS');
			$ACBENEF_CITIZENSHIP = $this->_request->getParam('NATIONALITY');
			$ACBENEF_RESIDENT = $this->_request->getParam('CITIZENSHIP');
			$ACBENEF_BANKNAME  = $this->_request->getParam('ACBENEF_BANKNAME');

			$LLD_CATEGORY = $this->_request->getParam('LLD_CATEGORY'); //INSERT FIELD - BENEFICIARY_CATEGORY
			$LLD_BENEIDENTIF = $this->_request->getParam('LLD_BENEIDENTIF'); //INSERT FIELD - BENEFICIARY_ID_TYPE
			$LLD_BENENUMBER = $this->_request->getParam('LLD_BENENUMBER'); //INSERT FIELD - BENEFICIARY_ID_NUMBER
			$CITY_CODE = $this->_request->getParam('CITY_CODE'); //INSERT FIELD - BENEFICIARY_CITY_CODE
			//}
			$BENEFICIARY_ACCOUNT = $this->_getParam('ACBENEF');
			$CURR_CODE = $this->_getParam('CURR_CODE');
			$BENEFICIARY_ALIAS = $this->_getParam('ACBENEF_ALIAS');
			$BENEFICIARY_EMAIL = $this->_getParam('ACBENEF_EMAIL');
			$BANK_NAME   = $this->_request->getParam('BENEFICIARY_BANK_NAME');
			$BANK_CODE   = $this->_request->getParam('BENEFICIARY_BANK_CODE');

			$BANK_ADD1   = $this->_request->getParam('BENEFICIARY_BANK_ADD1');
			$BANK_ADD2   = $this->_request->getParam('BENEFICIARY_BANK_ADD2');
			$BANK_CITY   = $this->_request->getParam('BENEFICIARY_BANK_CITY');
			$BANK_COUNTRY   = $this->_request->getParam('BENEFICIARY_BANK_COUNTRY');
			$BANK_POB   = $this->_request->getParam('BENEFICIARY_BANK_POB');

			$ACBENEF_ADDRESS = $this->_request->getParam('ADDRESS');
			//$ACBENEF_CITIZENSHIP= $this->_request->getParam('CITIZENSHIP');
			//$ACBENEF_RESIDENT= $this->_request->getParam('NATIONALITY');

			/*$LLD_CATEGORY = $this->_request->getParam('LLD_CATEGORY');
			$LLD_BENEIDENTIF = $this->_request->getParam('LLD_BENEIDENTIF');
			$LLD_BENENUMBER = $this->_request->getParam('LLD_BENENUMBER');
			$CITY_CODE = $this->_request->getParam('CITY_CODE');*/

			//$SWIFT_CODE   = $this->_request->getParam('SWIFT_CODE');

			$this->view->CURR_CODE = $CURR_CODE;


			//-------------------------------------------------------------START VALIDATION---------------------------------------------------------------------//
			//if($benefType == 1){
			// 				$BANK_CITY   = $filter->filter($BANK_CITY  , "ADDRESS");
			$CLR_CODE   = $filter->filter($CLR_CODE, "BANK_CODE");
			$ACBENEF_ADDRESS = $filter->filter($ACBENEF_ADDRESS, "ADDRESS");
			$ACBENEF_CITIZENSHIP = $filter->filter($ACBENEF_CITIZENSHIP, "SELECTION");
			$ACBENEF_RESIDENT = $filter->filter($ACBENEF_RESIDENT, "SELECTION");
			$ACBENEF_BANKNAME  = $filter->filter($ACBENEF_BANKNAME, "ACCOUNT_NAME");

			$LLD_CATEGORY = $filter->filter($LLD_CATEGORY, "LLD_CATEGORY");
			$LLD_BENEIDENTIF = $filter->filter($LLD_BENEIDENTIF, "LLD_BENEIDENTIF");
			$LLD_BENENUMBER = $filter->filter($LLD_BENENUMBER, "LLD_BENENUMBER");
			$CITY_CODE = $filter->filter($CITY_CODE, "CITY_CODE");

			$BANK_NAME   = $filter->filter($BANK_NAME, "BANK_NAME");

			/*}
			else{
				if($BANK_NAME){
					$BANK = explode('-',$BANK_NAME);
					$BANK_CODE = trim($BANK[0]);
					$BANK_NAME = trim($BANK[1]);
				}
				else{
					$BANK_CODE = '';
					$BANK_NAME = '';
				}
				$CLR_CODE   	= $filter->filter($BANK_CODE  , "BANK_CODE");
				//$CLR_CODE   	= $filter->filter($CLR_CODE);
				$BANK_NAME   	= $filter->filter($BANK_NAME , "BANK_NAME");
			}*/
			$ACBENEF    		= $filter->filter($BENEFICIARY_ACCOUNT, "ACCOUNT_NO");
			$ACBENEF_ALIAS   	= $filter->filter($BENEFICIARY_ALIAS, "ACCOUNT_ALIAS");
			$ACBENEF_EMAIL   	= $filter->filter($BENEFICIARY_EMAIL, "EMAIL");
			$ACBENEF_CCY   		= $filter->filter($CURR_CODE, "SELECTION");

			$ACBENEF_ADDRESS = $filter->filter($ACBENEF_ADDRESS, "ADDRESS");
			$ACBENEF_CITIZENSHIP = $filter->filter($ACBENEF_CITIZENSHIP, "SELECTION");
			$ACBENEF_RESIDENT = $filter->filter($ACBENEF_RESIDENT, "SELECTION");

			$LLD_CATEGORY = $filter->filter($LLD_CATEGORY, "LLD_CATEGORY");
			//$LLD_BENEIDENTIF = $filter->filter($LLD_BENEIDENTIF , "LLD_BENEIDENTIF");
			//$LLD_BENENUMBER = $filter->filter($LLD_BENENUMBER , "LLD_BENENUMBER");
			$CITY_CODE = $filter->filter($CITY_CODE, "CITY_CODE");

			//validasi token -- begin
			$challengeCodeReq2 	= $filter->filter($this->_request->getParam('challengeCodeReq2'), "challengeCodeReq2");
			$challengeCodeReq 	= $filter->filter($this->_request->getParam('challengeCodeReq'), "challengeCodeReq");

			$responseCodeReq 	= $filter->filter($this->_request->getParam('responseCodeReq'), "responseCodeReq");
			//$random = str_pad(mt_rand(0, 9), 2, '0', STR_PAD_LEFT);
			$this->view->challengeCodeReq1 = substr($challengeCodeReq, -4);
			//validasi token -- end
			// 			print_r($responseCodeReq);die;
			$paramBene = array(
				"FROM"      			=> 'B',
				"ACBENEF"     			=> $ACBENEF,
				"ACBENEF_CCY"    		=> $ACBENEF_CCY,
				"ACBENEF_ALIAS"    	=> $ACBENEF_ALIAS,
				"ACBENEF_EMAIL"   		=> $ACBENEF_EMAIL,
				"BANK_CODE"    	 				=> $BANK_CODE,
				"_addBeneficiary"   	=> $this->view->hasPrivilege('BADA'),
				"_beneLinkage"    		=> $privibenelinkage,
				"ACCTSRC"     			=> $ACCT_NO, //source account m_user yg login
				"VALIDATE"     		=> '1',
				"sourceAccountType"    => $ACCT_TYPE,
				"BANK_NAME"  			=> $BANK_NAME
			);
			//if($benefType == 1){
			$paramAdd = array(
				"TRANSFER_TYPE"      	=>  'REM',
				"ACBENEF_BANKNAME" 	=> $ACBENEF_BANKNAME,
				"ACBENEF_ADDRESS1"   	=> $ACBENEF_ADDRESS,
				"ACBENEF_CITIZENSHIP"  => $ACBENEF_CITIZENSHIP,
				"ACBENEF_RESIDENT"  	=> $ACBENEF_RESIDENT,

				"LLD_CATEGORY"  	=> $LLD_CATEGORY,
				"LLD_BENEIDENTIF"  	=> $LLD_BENEIDENTIF,
				"LLD_BENENUMBER"  	=> $LLD_BENENUMBER,
				"CITY_CODE"  	=> $CITY_CODE,
			);
			/*}else{
				$paramAdd = array("TRANSFER_TYPE"      	=>  'ONLINE',
													 "ACBENEF_BANKNAME" 	=> '',
													 "ACBENEF_ADDRESS1"   	=> '',
													 "ACBENEF_CITIZENSHIP"  => '',
													 "ACBENEF_RESIDENT"  => '',


													 "LLD_CATEGORY"  	=> '',
													 "LLD_BENEIDENTIF"  	=> '',
													 "LLD_BENENUMBER"  	=> '',
													 "CITY_CODE"  	=> '',
													);
			}*/
			$paramBene += array_merge($paramBene, $paramAdd);

			$validateACBENEF = new ValidateAccountBeneficiary($ACBENEF, $this->_custIdLogin, $this->_userIdLogin);
			// echo "<pre>";
			// 			print_r($paramBene);die;
			$checkedParams = $validateACBENEF->checkRemittance($paramBene);

			if (!$validateACBENEF->isError()) {
				// 				echo 'here';die;
				$content = array(
					'CUST_ID_LOGIN' 			=> $this->_custIdLogin,
					'USER_ID_LOGIN' 			=> $this->_userIdLogin,
					'BENEFICIARY_ALIAS' 		=> $ACBENEF_ALIAS,
					'BENEFICIARY_ACCOUNT' 		=> $ACBENEF,
					'BENEFICIARY_NAME' 			=> $ACBENEF_BANKNAME,
					'CURR_CODE' 				=> $ACBENEF_CCY,
					'BENEFICIARY_EMAIL' 		=> $ACBENEF_EMAIL,
					'BENEFICIARY_CITIZENSHIP' 	=> $ACBENEF_CITIZENSHIP,
					'BENEFICIARY_RESIDENT' 		=> $ACBENEF_RESIDENT,
					'BENEFICIARY_ADDRESS' 		=> $ACBENEF_ADDRESS,
					'BANK_NAME' 				=> $BANK_NAME,
					'BANK_CITY'			 		=> $BANK_CITY,
					'_beneLinkage'    			=> $privibenelinkage,
					'ACCTSRC'    				=> $ACBENEF,
					'BENEFICIARY_BANK_CODE'    	=> $BENEFICIARY_BANK_CODE,
					// 											'CITIZENSHIP_COUNTRY'		=> $BANK_COUNTRY,
					'POB_NUMBER'				=> $BANK_POB,
					'BANK_ADDRESS1'				=> $BANK_ADD1,
					'BANK_ADDRESS2'				=> $BANK_ADD2,
					'BANK_COUNTRY'				=> $BANK_COUNTRY,
					'BENEFICIARY_CATEGORY'    	=> $LLD_CATEGORY,
					'BENEFICIARY_ID_TYPE'    	=> $LLD_BENEIDENTIF,
					'BENEFICIARY_ID_NUMBER'    	=> $LLD_BENENUMBER,
					'BENEFICIARY_CITY_CODE'    	=> $CITY_CODE,
					'SWIFT_CODE'    			=> $CLR_CODE,
				);

				//validasi hard token page 1 -- begin
				$message = $Settings->getSetting('token_confirm_message');
				$trans = array("[Response_Code]" => $responseCode);
				$message = strtr($message, $trans);

				if ($tokenType == '1') { //sms token
					if (empty($responseCodeReq)) {
						$resultToken = FALSE;
						$this->view->error = true;
						$errMessage = 'Error : Response Token cannot be left blank.';
						$this->view->report_msg = $errMessage;
					} else {
						$token = new Service_Token(NULL, $this->_userIdLogin);
						$token->setMsisdn($usermobilephone);
						$token->setMessage($message);

						$res = $token->confirm();
						$resultToken = $res['ResponseCode'] == '0000';
					}
				} elseif ($tokenType == '2') { //hard token
					if (empty($responseCodeReq)) {
						$resultToken = FALSE;
						$this->view->error = true;
						$errMessage = 'Error : Response Token cannot be left blank.';
						$this->view->report_msg = $errMessage;
					} else {
						$resHard = $HardToken->verifyHardToken($challengeCodeReq2 . $challengeCodeReq, $responseCodeReq);
						$resultToken = $resHard['ResponseCode'] == '0000';

						//set user lock token gagal
						$CustUser = new CustomerUser($this->_userIdLogin);
						if ($resHard['ResponseCode'] != '0000') {
							$tokenFailed = $CustUser->setFailedTokenMustLogout($resHard['ResponseTO']);
							if ($tokenFailed)
								$this->_forward('home');
						}
					}
				} elseif ($tokenType == '3') { //mobile token
					$resultToken = TRUE;
				} else {
					$resultToken = TRUE;
				}
				//validasi hard token page 1 -- end

				if ($resultToken == TRUE) {

					$content['BENEFICIARY_TYPE'] = 3; // 2 = domestic ( SKN/RTGS )
					//$content['CLR_CODE'] = $CLR_CODE;
					$sessionNamespace = new Zend_Session_Namespace('beneficiaryAccountAddEdit');
					$sessionNamespace->mode = 'Add';
					$sessionNamespace->content = $content;
					$this->_redirect('/predefinedbeneficiary/remittanceacct/confirm');

					// try
					// {
					// -----insert benef--------------
					// $this->_db->beginTransaction();
					// $add = $Beneficiary->add($content);
					// Application_Helper_General::writeLog('BADA','Add Beneficiary Account '.$ACBENEF);
					// $this->_db->commit();
					// $this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
					// $this->_redirect('/notification/success');
					// }
					// catch(Exception $e)
					// {
					// rollback changes
					// $this->_db->rollBack();
					// }


				} else {
					$this->view->ACBENEF = $ACBENEF;

					//if($benefType == 1){
					$this->view->BANK_NAME = $BANK_NAME;
					/*}
						elseif($benefType == 2){
							$this->view->BANK_NAME = $CLR_CODE.' - '.$BANK_NAME;
						}*/

					$this->view->CLR_CODE = $CLR_CODE;
					//$this->view->SWIFT_CODE = $SWIFT_CODE;

					//$this->view->benefType = $benefType;
					$this->view->ACBENEF_BANKNAME = $ACBENEF_BANKNAME;
					$this->view->ADDRESS = $ACBENEF_ADDRESS;

					$this->view->LLD_CATEGORY = $LLD_CATEGORY;
					$this->view->LLD_BENEIDENTIF = $LLD_BENEIDENTIF;
					$this->view->LLD_BENENUMBER = $LLD_BENENUMBER;
					$this->view->CITY_CODE = $CITY_CODE;
					if ($resultToken == FALSE) {
						if ($res['ResponseCode'] == 'XT') {
							$errMessage = 'Error : Service Rejected';
							$this->view->resulttoken = $errMessage;
						} else {
							$errMessage = 'Error : Invalid Token';
							$this->view->resulttoken = $errMessage;
						}
					}
				}
			} else {
				// if($benefType == '2'){
				//  $BANK_NAME = $BANK_CODE.' - '.$BANK_NAME;
				// }
				$this->view->error = true;

				if (!empty($ACBENEF_CCY)) {
					$this->view->CCY_HID = $ACBENEF_CCY;
					$this->view->bankpop = '';
				} else
					$this->view->bankpop = 'disabled';

				$this->fillParams($benefType, $ACBENEF_ALIAS, $ACBENEF, $ACBENEF_BANKNAME, $ACBENEF_CCY, $ACBENEF_EMAIL, $ACBENEF_CITIZENSHIP, $ACBENEF_RESIDENT, $ACBENEF_ADDRESS, $BANK_NAME, $CLR_CODE, $BANK_CITY, $LLD_BENENUMBER, $LLD_BENEIDENTIF, $LLD_CATEGORY, $LLD_BENEIDENTIF, $LLD_BENENUMBER, $CITY_CODE);
				$docErr = $this->language->_('Error') . ': ' . $validateACBENEF->getErrorMsg();
				$validateACBENEF->__destruct();
				unset($validateACBENEF);
				$this->view->report_msg = $docErr;
			}
		}

		$this->view->LLD_CATEGORY = $LLD_CATEGORY;
		$this->view->LLD_BENEIDENTIF = $LLD_BENEIDENTIF;
		$this->view->LLD_BENENUMBER = $LLD_BENENUMBER;
		$this->view->CITY_CODE = $CITY_CODE;
		//$this->view->SWIFT_CODE = $SWIFT_CODE;
		$this->view->CITIZENSHIP = $this->_getParam('CITIZENSHIP');
		$this->view->NATIONALITY = $this->_getParam('NATIONALITY');
		$this->view->ACBENEF_EMAIL = $this->_getParam('ACBENEF_EMAIL');

		$this->view->BANK_ADD1 = $BANK_ADD1;
		$this->view->BANK_ADD2 = $BANK_ADD2;
		$this->view->BANK_CITY = $BANK_CITY;
		$this->view->BANK_COUNTRY = $BANK_COUNTRY;
		$this->view->BANK_POB = $BANK_POB;
		Application_Helper_General::writeLog('BADA', 'Add Destination Account');
	}


	public function editAction()
	{


		$this->_helper->layout()->setLayout('newlayout');
		$this->view->bankpop = 'disabled';
		$anyValue = '-- ' . $this->language->_('Select City') . ' --';
		//		$payment					= new payment_Model_Payment();
		$modelCity = new predefinedbeneficiary_Model_Predefinedbeneficiary();


		$model = new predefinedbeneficiary_Model_Predefinedbeneficiary();
		$param['user_id'] = $this->_userIdLogin;
		$param['payType'] =  $this->_payType;


		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$this->view->token = $sessionNamespace->token;

		$AESMYSQL = new Crypt_AESMYSQL();
		$benef_id = $AESMYSQL->decrypt($this->_getParam('benef_id'), $password);
		$this->view->benef_id = $benef_id;

		$param['cust_id'] = $this->_custIdLogin;
		//echo $benef_id."asfd"; die;

		if (!$this->_request->isPost()) {
			//$benef_id = $this->_getParam('benef_id');
			$param['benef_id'] = $benef_id = (Zend_Validate::is($benef_id, 'Digits')) ? $benef_id : null;
			if ($benef_id) {
				//print_r($param);die;
				$temp = $model->getBeneficiaries($param);
				$resultdata = $temp[0];
				if ($resultdata) {
					$this->view->BENEFICIARY_ID 	= $resultdata['BENEFICIARY_ID'];
					$this->view->ACBENEF_ALIAS 		= $resultdata['BENEFICIARY_ALIAS'];
					$this->view->ACBENEF  			= $resultdata['BENEFICIARY_ACCOUNT'];
					$this->view->ACBENEF_BANKNAME 	= $resultdata['BENEFICIARY_NAME'];
					$this->view->CURR_CODE    		= $resultdata['CURR_CODE'];
					$this->view->ADDRESS    		= $resultdata['BENEFICIARY_ADDRESS'];
					$this->view->CITIZENSHIP    	= $resultdata['BENEFICIARY_CITIZENSHIP'];
					$this->view->BENEFICIARY_RESIDENT   = $resultdata['BENEFICIARY_RESIDENT'];
					$this->view->ACBENEF_EMAIL  	= $resultdata['BENEFICIARY_EMAIL'];
					$this->view->BANK_NAME  		= $resultdata['BANK_NAME'];
					$this->view->CLR_CODE  	= $resultdata['CLR_CODE'];
					$this->view->BANK_CITY  		= $resultdata['BANK_CITY'];
					$this->view->SWIFT_CODE  		= $resultdata['SWIFT_CODE'];


					// 					$this->view->CITIZENSHIP		  		= $resultdata['BENEFICIARY_RESIDENT'];
					// 					$category_name = $model->getCategoryCode($resultdata['BENEFICIARY_CATEGORY']);
					// 					print_r($temp);;die;
					// 					$this->view->CATEGORY_NAME_LLD		  		= $category_name['0']['sm_text1'];


					// 					$this->view->CITIZENSHIP		  		= $resultdata['BENEFICIARY_RESIDENT'];
					$this->view->BENEIDENTIF_NAME_LLD		  		= $resultdata['BENEFICIARY_ID_TYPE'];
					$this->view->LLD_BENENUMBER		  		= $resultdata['BENEFICIARY_ID_NUMBER'];

					$this->view->BANK_POB = $resultdata['POB_NUMBER'];
					$this->view->CLR_CODE = $resultdata['SWIFT_CODE'];
					$this->view->BANK_CITY = $resultdata['BANK_CITY'];
					$this->view->BANK_ADD1 = $resultdata['BANK_ADDRESS1'];
					$this->view->BANK_ADD2 = $resultdata['BANK_ADDRESS2'];
					$this->view->BANK_COUNTRY = $resultdata['BANK_COUNTRY'];


					$CITY_CODE 		= $resultdata['BENEFICIARY_CITY_CODE'];
					$LLD_CATEGORY 	= $resultdata['BENEFICIARY_CATEGORY'];

					$modelCity 			= new predefinedbeneficiary_Model_Predefinedbeneficiary();
					$arr 				= $modelCity->getCountry($resultdata['BANK_COUNTRY']);
					//print_r($arr);die;
					//$this->view->BANK_COUNTRY = $arr['0']['COUNTRY_NAME'];
					$CITY_CODEGet 		= (!empty($CITY_CODE) ? $CITY_CODE : '');
					$arr 				= $modelCity->getCityCode($CITY_CODEGet);

					// 9. Create LLD string
					$settings 			= new Application_Settings();
					$LLD_array 			= array();
					$LLD_DESC_arrayCat 	= array();
					$lldTypeArr  		= $settings->getLLDDOMType();

					if (!empty($LLD_CATEGORY)) {
						$lldCategoryArr  	= $settings->getLLDDOMCategory();
						$LLD_array["CT"] 	= $LLD_CATEGORY;
						$LLD_CATEGORY_POST  = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
					}

					$this->view->LLD_CATEGORY  		= $LLD_CATEGORY_POST;
					$this->view->LLD_BENEIDENTIF  	= $resultdata['BENEFICIARY_ID_TYPE'];
					$this->view->LLD_BENENUMBER  	= $resultdata['BENEFICIARY_ID_NUMBER'];
					$this->view->CITY_CODE  		= $arr[0]['CITY_NAME'];
				}
			} else {
				$error_remark = 'Beneficiary ID does not exist.';
				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				$this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
				$this->_redirect('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName());
			}
		}

		//		$arr 					= $modelCity->getCity();
		//		$cityCodeArr 			= array(''=> $anyValue);
		//		$cityCodeArr 			+= Application_Helper_Array::listArray($arr,'CITY_CODE','CITY_NAME');
		//		$this->view->cityCodeArr 	= $cityCodeArr;
		//		$SWIFT_CODE   = $this->_request->getParam('SWIFT_CODE');
		$list 							= $modelCity->getCity();
		$this->view->cityCodeList 		= $list;
		//		$CITY_CODE = $this->_getParam('CITY_CODE');
		//		$this->view->CITY_CODE = $CITY_CODE; //INSERT CITY_CODE FIELD - BENEFICIARY_CITY_CODE

		$Settings = new Settings();
		$address_mandatory = $Settings->getSetting('address_mandatory');
		$this->view->address_mandatory = $address_mandatory;

		$settings 			= new Application_Settings();
		$lldCategoryArr  	= $settings->getLLDDOMCategory();
		$this->view->lldCategoryArr 	= $lldCategoryArr;
		//		$LLD_CATEGORY 		= $this->_request->getParam('LLD_CATEGORY');
		//		$this->view->LLD_CATEGORY = $LLD_CATEGORY; //INSERT CATEGORY FIELD - BENEFICAIRY_CATEGORY

		$ccyList  = $settings->setCurrencyRegistered();
		$this->view->ccyArr 			= $ccyList;

		$lldBeneIdentifArr 	= $settings->getLLDDOMBeneIdentification();
		$this->view->lldBeneIdentifArr  = $lldBeneIdentifArr;
		//		$LLD_BENEIDENTIF 		= $this->_request->getParam('LLD_BENEIDENTIF');
		//		$this->view->LLD_BENEIDENTIF = $LLD_BENEIDENTIF; //INSERT IDENTIFICATION TYPE FIELD - BENEFICIARY_ID_TYPE

		$this->view->CITIZENSHIP = $this->_getParam('CITIZENSHIP');
		//$this->view->benefType = '1';
		$privibenelinkage = $this->view->hasPrivilege('BLBA');
		$Beneficiary = new Beneficiary();
		$filter  = new Application_Filtering();
		$settings = new Application_Settings();
		$ccyList  = $settings->setCurrencyRegistered();

		$this->view->ccyArr 			= $ccyList;

		//ambil data m_user ->ambil sourceAccount, acctType, statusnya aktif=1
		$acctStatus = 1;
		$dataAcct = $this->_db->fetchRow(
			$this->_db->select()
				->from(array('C' => 'M_USER_ACCT'))
				->where("USER_ID = " . $this->_db->quote($this->_userIdLogin))
				->where("ACCT_STATUS = ?", $acctStatus)
				->limit(1)
		);
		$ACCT_NO = (!empty($dataAcct['ACCT_NO']) ? $dataAcct['ACCT_NO'] : '');
		$ACCT_TYPE = (!empty($dataAcct['ACCT_TYPE']) ? $dataAcct['ACCT_TYPE'] : '');

		//validasi token -- begin
		$select3 = $this->_db->select()
			->from(array('C' => 'M_USER'));
		$select3->where("USER_ID = " . $this->_db->quote($this->_userIdLogin));
		$data2 					= $this->_db->fetchRow($select3);
		$tokenIdUser 			= $data2['TOKEN_ID'];
		//$tokenType 				= $data2['TOKEN_TYPE'];
		$tokenType = "";
		$usermobilephone 		= $data2['USER_MOBILE_PHONE'];
		//$this->view->tokentype 	= $data2['TOKEN_TYPE'];
		$this->view->tokentype = "";

		//added new hard token
		$HardToken = new Service_Token($this->_custIdLogin, $this->_userIdLogin, $tokenIdUser);
		$challengeCode = $HardToken->generateChallengeCode();

		$challengeCodeSub = substr($challengeCode, 0, 2);
		$this->view->challengeCodeReq = $challengeCodeSub;
		//validasi token -- end

		if ($this->_request->isPost()) {
			$BENEFICIARY_BANK_CODE   = $this->_getParam('BENEFICIARY_BANK_CODE');
			$BANK_CITY   = '';
			$CLR_CODE   = '';
			//$CLR_CODE   = $this->_request->getParam('CLR_CODE');
			$ACBENEF_ADDRESS = '';
			$ACBENEF_CITIZENSHIP = '';
			$ACBENEF_RESIDENT = '';
			$ACBENEF_BANKNAME  = '';

			$LLD_CATEGORY = '';
			$LLD_BENEIDENTIF  = '';
			$LLD_BENENUMBER = '';
			$CITY_CODE  = '';

			//$benefType = $this->_getParam('benefType');
			//setelah disamakan dgn FSD
			$benefType = '3'; //$this->_getParam('benefType'); //kalo dua ONLINE harus panggil fundtransferinquiry, kalo 1 skn rtgs langsung lewat

			/*if($_POST){
				Zend_Debug::dump($this->_request->getParams());
			}*/

			//if($benefType == 1){
			$BANK_CITY   = $this->_request->getParam('BANK_CITY');
			$CLR_CODE   = $this->_request->getParam('BENEFICIARY_BANK_CODE');

			$ACBENEF_ADDRESS = $this->_request->getParam('ADDRESS');
			$ACBENEF_CITIZENSHIP = $this->_request->getParam('NATIONALITY');
			$ACBENEF_RESIDENT = $this->_request->getParam('CITIZENSHIP');
			$ACBENEF_BANKNAME  = $this->_request->getParam('ACBENEF_BANKNAME');

			$LLD_CATEGORY = $this->_request->getParam('LLD_CATEGORY'); //INSERT FIELD - BENEFICIARY_CATEGORY
			$LLD_BENEIDENTIF = $this->_request->getParam('LLD_BENEIDENTIF'); //INSERT FIELD - BENEFICIARY_ID_TYPE
			$LLD_BENENUMBER = $this->_request->getParam('LLD_BENENUMBER'); //INSERT FIELD - BENEFICIARY_ID_NUMBER
			$CITY_CODE = $this->_request->getParam('CITY_CODE'); //INSERT FIELD - BENEFICIARY_CITY_CODE
			//}
			$BENEFICIARY_ACCOUNT = $this->_getParam('ACBENEF');
			$CURR_CODE = $this->_getParam('CURR_CODE');
			$BENEFICIARY_ALIAS = $this->_getParam('ACBENEF_ALIAS');
			$BENEFICIARY_EMAIL = $this->_getParam('ACBENEF_EMAIL');
			$BANK_NAME   = $this->_request->getParam('BENEFICIARY_BANK_NAME');
			$BANK_CODE   = $this->_request->getParam('BENEFICIARY_BANK_CODE');

			$BANK_ADD1   = $this->_request->getParam('BENEFICIARY_BANK_ADD1');
			$BANK_ADD2   = $this->_request->getParam('BENEFICIARY_BANK_ADD2');
			$BANK_CITY   = $this->_request->getParam('BENEFICIARY_BANK_CITY');
			$BANK_COUNTRY   = $this->_request->getParam('BENEFICIARY_BANK_COUNTRY');
			$BANK_POB   = $this->_request->getParam('BENEFICIARY_BANK_POB');

			$ACBENEF_ADDRESS = $this->_request->getParam('ADDRESS');
			//$ACBENEF_CITIZENSHIP= $this->_request->getParam('CITIZENSHIP');
			//$ACBENEF_RESIDENT= $this->_request->getParam('NATIONALITY');

			/*$LLD_CATEGORY = $this->_request->getParam('LLD_CATEGORY');
			$LLD_BENEIDENTIF = $this->_request->getParam('LLD_BENEIDENTIF');
			$LLD_BENENUMBER = $this->_request->getParam('LLD_BENENUMBER');
			$CITY_CODE = $this->_request->getParam('CITY_CODE');*/

			//$SWIFT_CODE   = $this->_request->getParam('SWIFT_CODE');

			$this->view->CURR_CODE = $CURR_CODE;


			//-------------------------------------------------------------START VALIDATION---------------------------------------------------------------------//
			//if($benefType == 1){
			// 				$BANK_CITY   = $filter->filter($BANK_CITY  , "ADDRESS");
			$CLR_CODE   = $filter->filter($CLR_CODE, "BANK_CODE");
			$ACBENEF_ADDRESS = $filter->filter($ACBENEF_ADDRESS, "ADDRESS");
			$ACBENEF_CITIZENSHIP = $filter->filter($ACBENEF_CITIZENSHIP, "SELECTION");
			$ACBENEF_RESIDENT = $filter->filter($ACBENEF_RESIDENT, "SELECTION");
			$ACBENEF_BANKNAME  = $filter->filter($ACBENEF_BANKNAME, "ACCOUNT_NAME");

			$LLD_CATEGORY = $filter->filter($LLD_CATEGORY, "LLD_CATEGORY");
			$LLD_BENEIDENTIF = $filter->filter($LLD_BENEIDENTIF, "LLD_BENEIDENTIF");
			$LLD_BENENUMBER = $filter->filter($LLD_BENENUMBER, "LLD_BENENUMBER");
			$CITY_CODE = $filter->filter($CITY_CODE, "CITY_CODE");

			$BANK_NAME   = $filter->filter($BANK_NAME, "BANK_NAME");

			/*} 
			else{
				if($BANK_NAME){
					$BANK = explode('-',$BANK_NAME);
					$BANK_CODE = trim($BANK[0]);
					$BANK_NAME = trim($BANK[1]);
				}
				else{
					$BANK_CODE = '';
					$BANK_NAME = '';
				}
				$CLR_CODE   	= $filter->filter($BANK_CODE  , "BANK_CODE");
				//$CLR_CODE   	= $filter->filter($CLR_CODE);
				$BANK_NAME   	= $filter->filter($BANK_NAME , "BANK_NAME");
			}*/
			$ACBENEF    		= $filter->filter($BENEFICIARY_ACCOUNT, "ACCOUNT_NO");
			$ACBENEF_ALIAS   	= $filter->filter($BENEFICIARY_ALIAS, "ACCOUNT_ALIAS");
			$ACBENEF_EMAIL   	= $filter->filter($BENEFICIARY_EMAIL, "EMAIL");
			$ACBENEF_CCY   		= $filter->filter($CURR_CODE, "SELECTION");

			$ACBENEF_ADDRESS = $filter->filter($ACBENEF_ADDRESS, "ADDRESS");
			$ACBENEF_CITIZENSHIP = $filter->filter($ACBENEF_CITIZENSHIP, "SELECTION");
			$ACBENEF_RESIDENT = $filter->filter($ACBENEF_RESIDENT, "SELECTION");

			$LLD_CATEGORY = $filter->filter($LLD_CATEGORY, "LLD_CATEGORY");
			//$LLD_BENEIDENTIF = $filter->filter($LLD_BENEIDENTIF , "LLD_BENEIDENTIF");
			//$LLD_BENENUMBER = $filter->filter($LLD_BENENUMBER , "LLD_BENENUMBER");
			$CITY_CODE = $filter->filter($CITY_CODE, "CITY_CODE");

			//validasi token -- begin
			$challengeCodeReq2 	= $filter->filter($this->_request->getParam('challengeCodeReq2'), "challengeCodeReq2");
			$challengeCodeReq 	= $filter->filter($this->_request->getParam('challengeCodeReq'), "challengeCodeReq");

			$responseCodeReq 	= $filter->filter($this->_request->getParam('responseCodeReq'), "responseCodeReq");
			//$random = str_pad(mt_rand(0, 9), 2, '0', STR_PAD_LEFT);
			$this->view->challengeCodeReq1 = substr($challengeCodeReq, -4);
			//validasi token -- end
			// 			print_r($responseCodeReq);die;
			$paramBene = array(
				"FROM"      			=> 'F',
				"ACBENEF"     			=> $ACBENEF,
				"ACBENEF_CCY"    		=> $ACBENEF_CCY,
				"ACBENEF_ALIAS"    	=> $ACBENEF_ALIAS,
				"ACBENEF_EMAIL"   		=> $ACBENEF_EMAIL,
				"BANK_CODE"    	 				=> $BANK_CODE,
				"_addBeneficiary"   	=> $this->view->hasPrivilege('BADA'),
				"_beneLinkage"    		=> $privibenelinkage,
				"ACCTSRC"     			=> $ACCT_NO, //source account m_user yg login
				"VALIDATE"     		=> '1',
				"sourceAccountType"    => $ACCT_TYPE,
				"BANK_NAME"  			=> $BANK_NAME
			);
			//if($benefType == 1){
			$paramAdd = array(
				"TRANSFER_TYPE"      	=>  'REM',
				"ACBENEF_BANKNAME" 	=> $ACBENEF_BANKNAME,
				"ACBENEF_ADDRESS1"   	=> $ACBENEF_ADDRESS,
				"ACBENEF_CITIZENSHIP"  => $ACBENEF_CITIZENSHIP,
				"ACBENEF_RESIDENT"  	=> $ACBENEF_RESIDENT,

				"LLD_CATEGORY"  	=> $LLD_CATEGORY,
				"LLD_BENEIDENTIF"  	=> $LLD_BENEIDENTIF,
				"LLD_BENENUMBER"  	=> $LLD_BENENUMBER,
				"CITY_CODE"  	=> $CITY_CODE,
			);
			/*}else{
				$paramAdd = array("TRANSFER_TYPE"      	=>  'ONLINE',
													 "ACBENEF_BANKNAME" 	=> '',
													 "ACBENEF_ADDRESS1"   	=> '',
													 "ACBENEF_CITIZENSHIP"  => '',
													 "ACBENEF_RESIDENT"  => '',


													 "LLD_CATEGORY"  	=> '',
													 "LLD_BENEIDENTIF"  	=> '',
													 "LLD_BENENUMBER"  	=> '',
													 "CITY_CODE"  	=> '',
													);
			}*/
			$paramBene += array_merge($paramBene, $paramAdd);

			$validateACBENEF = new ValidateAccountBeneficiary($ACBENEF, $this->_custIdLogin, $this->_userIdLogin);
			// echo "<pre>";
			// 			print_r($paramBene);die;
			$checkedParams = $validateACBENEF->checkRemittance($paramBene);

			if (!$validateACBENEF->isError()) {
				// 				echo 'here';die;
				$content = array(
					'CUST_ID_LOGIN' 			=> $this->_custIdLogin,
					'USER_ID_LOGIN' 			=> $this->_userIdLogin,
					'BENEFICIARY_ALIAS' 		=> $ACBENEF_ALIAS,
					'BENEFICIARY_ACCOUNT' 		=> $ACBENEF,
					'BENEFICIARY_NAME' 			=> $ACBENEF_BANKNAME,
					'CURR_CODE' 				=> $ACBENEF_CCY,
					'BENEFICIARY_EMAIL' 		=> $ACBENEF_EMAIL,
					'BENEFICIARY_CITIZENSHIP' 	=> $ACBENEF_CITIZENSHIP,
					'BENEFICIARY_RESIDENT' 		=> $ACBENEF_RESIDENT,
					'BENEFICIARY_ADDRESS' 		=> $ACBENEF_ADDRESS,
					'BANK_NAME' 				=> $BANK_NAME,
					'BANK_CITY'			 		=> $BANK_CITY,
					'_beneLinkage'    			=> $privibenelinkage,
					'ACCTSRC'    				=> $ACBENEF,
					'BENEFICIARY_BANK_CODE'    	=> $BENEFICIARY_BANK_CODE,
					// 											'CITIZENSHIP_COUNTRY'		=> $BANK_COUNTRY,
					'POB_NUMBER'				=> $BANK_POB,
					'BANK_ADDRESS1'				=> $BANK_ADD1,
					'BANK_ADDRESS2'				=> $BANK_ADD2,
					'BANK_COUNTRY'				=> $BANK_COUNTRY,
					'BENEFICIARY_CATEGORY'    	=> $LLD_CATEGORY,
					'BENEFICIARY_ID_TYPE'    	=> $LLD_BENEIDENTIF,
					'BENEFICIARY_ID_NUMBER'    	=> $LLD_BENENUMBER,
					'BENEFICIARY_CITY_CODE'    	=> $CITY_CODE,
					'SWIFT_CODE'    			=> $CLR_CODE,
					'BENEFICIARY_ID'			=> $benef_id,
					'BENEF_ENC'					=> $this->_getParam('benef_id')
				);

				//validasi hard token page 1 -- begin
				$message = $Settings->getSetting('token_confirm_message');
				$trans = array("[Response_Code]" => $responseCode);
				$message = strtr($message, $trans);

				if ($tokenType == '1') { //sms token
					if (empty($responseCodeReq)) {
						$resultToken = FALSE;
						$this->view->error = true;
						$errMessage = 'Error : Response Token cannot be left blank.';
						$this->view->report_msg = $errMessage;
					} else {
						$token = new Service_Token(NULL, $this->_userIdLogin);
						$token->setMsisdn($usermobilephone);
						$token->setMessage($message);

						$res = $token->confirm();
						$resultToken = $res['ResponseCode'] == '0000';
					}
				} elseif ($tokenType == '2') { //hard token
					if (empty($responseCodeReq)) {
						$resultToken = FALSE;
						$this->view->error = true;
						$errMessage = 'Error : Response Token cannot be left blank.';
						$this->view->report_msg = $errMessage;
					} else {
						$resHard = $HardToken->verifyHardToken($challengeCodeReq2 . $challengeCodeReq, $responseCodeReq);
						$resultToken = $resHard['ResponseCode'] == '0000';

						//set user lock token gagal
						$CustUser = new CustomerUser($this->_userIdLogin);
						if ($resHard['ResponseCode'] != '0000') {
							$tokenFailed = $CustUser->setFailedTokenMustLogout($resHard['ResponseTO']);
							if ($tokenFailed)
								$this->_forward('home');
						}
					}
				} elseif ($tokenType == '3') { //mobile token
					$resultToken = TRUE;
				} else {
					$resultToken = TRUE;
				}
				//validasi hard token page 1 -- end

				if ($resultToken == TRUE) {

					$content['BENEFICIARY_TYPE'] = 3; // 2 = domestic ( SKN/RTGS )
					//$content['CLR_CODE'] = $CLR_CODE;
					$sessionNamespace = new Zend_Session_Namespace('beneficiaryAccountAddEdit');
					$sessionNamespace->mode = 'Edit';
					$sessionNamespace->content = $content;
					$this->_redirect('/predefinedbeneficiary/remittanceacct/confirm');

					// try
					// {
					// -----insert benef--------------
					// $this->_db->beginTransaction();
					// $add = $Beneficiary->add($content);
					// Application_Helper_General::writeLog('BADA','Add Beneficiary Account '.$ACBENEF);
					// $this->_db->commit();
					// $this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
					// $this->_redirect('/notification/success');
					// }
					// catch(Exception $e)
					// {
					// rollback changes
					// $this->_db->rollBack();
					// }


				} else {
					$this->view->ACBENEF = $ACBENEF;

					//if($benefType == 1){
					$this->view->BANK_NAME = $BANK_NAME;
					/*}
						elseif($benefType == 2){
							$this->view->BANK_NAME = $CLR_CODE.' - '.$BANK_NAME;
						}*/

					$this->view->CLR_CODE = $CLR_CODE;
					//$this->view->SWIFT_CODE = $SWIFT_CODE;

					//$this->view->benefType = $benefType;
					$this->view->ACBENEF_BANKNAME = $ACBENEF_BANKNAME;
					$this->view->ADDRESS = $ACBENEF_ADDRESS;

					$this->view->LLD_CATEGORY = $LLD_CATEGORY;
					$this->view->LLD_BENEIDENTIF = $LLD_BENEIDENTIF;
					$this->view->LLD_BENENUMBER = $LLD_BENENUMBER;
					$this->view->CITY_CODE = $CITY_CODE;
					if ($resultToken == FALSE) {
						if ($res['ResponseCode'] == 'XT') {
							$errMessage = 'Error : Service Rejected';
							$this->view->resulttoken = $errMessage;
						} else {
							$errMessage = 'Error : Invalid Token';
							$this->view->resulttoken = $errMessage;
						}
					}
				}
			} else {
				// if($benefType == '2'){
				//  $BANK_NAME = $BANK_CODE.' - '.$BANK_NAME;
				// }
				$this->view->error = true;

				if (!empty($ACBENEF_CCY)) {
					$this->view->CCY_HID = $ACBENEF_CCY;
					$this->view->bankpop = '';
				} else
					$this->view->bankpop = 'disabled';

				$this->fillParams($benefType, $ACBENEF_ALIAS, $ACBENEF, $ACBENEF_BANKNAME, $ACBENEF_CCY, $ACBENEF_EMAIL, $ACBENEF_CITIZENSHIP, $ACBENEF_RESIDENT, $ACBENEF_ADDRESS, $BANK_NAME, $CLR_CODE, $BANK_CITY, $LLD_BENENUMBER, $LLD_BENEIDENTIF, $LLD_CATEGORY, $LLD_BENEIDENTIF, $LLD_BENENUMBER, $CITY_CODE);
				$docErr = $this->language->_('Error') . ': ' . $validateACBENEF->getErrorMsg();
				$validateACBENEF->__destruct();
				unset($validateACBENEF);
				$this->view->report_msg = $docErr;
			}
		}

		$this->view->LLD_CATEGORY = $LLD_CATEGORY;
		$this->view->LLD_BENEIDENTIF = $LLD_BENEIDENTIF;
		$this->view->LLD_BENENUMBER = $LLD_BENENUMBER;
		$this->view->CITY_CODE = $CITY_CODE;
		//$this->view->SWIFT_CODE = $SWIFT_CODE;
		$this->view->CITIZENSHIP = $this->_getParam('CITIZENSHIP');
		$this->view->NATIONALITY = $this->_getParam('NATIONALITY');
		$this->view->ACBENEF_EMAIL = $this->_getParam('ACBENEF_EMAIL');

		//$this->view->BANK_ADD1 = $BANK_ADD1;
		//$this->view->BANK_ADD2 = $BANK_ADD2;
		//$this->view->BANK_CITY = $BANK_CITY;
		//$this->view->BANK_COUNTRY = $BANK_COUNTRY;
		//$this->view->BANK_POB = $BANK_POB;
		Application_Helper_General::writeLog('BADA', 'Add Destination Account');
	}

	public function confirmAction()
	{


		$this->_helper->layout()->setLayout('newlayout');
		$data = $this->_db->fetchRow(
			$this->_db->select()
				->from(array('C' => 'M_USER'))
				->where("USER_ID = " . $this->_db->quote($this->_userIdLogin))
				->limit(1)
		);

		$this->view->userId				= $data['USER_ID'];

		$sessionNamespace = new Zend_Session_Namespace('beneficiaryAccountAddEdit');
		$content = $sessionNamespace->content;
		$mode = $sessionNamespace->mode;
		$this->view->mode = $mode;
		foreach ($content as $a => $key) {
			// 			print_r($a);
			// 			echo ' ';

			$country = $this->_db->fetchRow(
				$this->_db->select()
					->from(array('C' => 'M_COUNTRY'))
					->where("COUNTRY_CODE = " . $this->_db->quote($content['BANK_COUNTRY']))
			);
			$countryName = $country['COUNTRY_NAME'];

			$this->view->POB_NUMBER = $content['POB_NUMBER'];
			$this->view->BENEFICIARY_BANK_CODE = $content['BENEFICIARY_BANK_CODE'];
			$this->view->BANK_CITY = $content['BANK_CITY'];
			$this->view->BANK_ADDRESS1 = $content['BANK_ADDRESS1'];
			$this->view->BANK_ADDRESS2 = $content['BANK_ADDRESS2'];
			$this->view->BANK_COUNTRY = $countryName;
		}

		// 		die;
		$this->fillParams(null, $content['BENEFICIARY_ALIAS'], $content['BENEFICIARY_ACCOUNT'], $content['BENEFICIARY_NAME'], $content['CURR_CODE'], $content['BENEFICIARY_EMAIL'], $content['BENEFICIARY_CITIZENSHIP'], $content['BENEFICIARY_RESIDENT'], $content['BENEFICIARY_ADDRESS'], $content['BANK_NAME'], null, $content['BANK_CITY'], null, $content['BENEFICIARY_TYPE'], $content['BENEFICIARY_CATEGORY'], $content['BENEFICIARY_ID_TYPE'], $content['BENEFICIARY_ID_NUMBER'], $content['BENEFICIARY_CITY_CODE'], $content['SWIFT_CODE'], $content['POB_NUMBER']);
		if ($this->_request->isPost()) {
			if ($this->_getParam('submit1') == $this->language->_('Back')) {
				if ($mode == 'Add') {
					$this->_redirect('/predefinedbeneficiary/remittanceacct/addbeneficiary/isback/1');
				} elseif ($mode == 'Edit') {
					$this->_redirect('/predefinedbeneficiary/remittanceacct/editbeneficiary/benef_id/' . $content['BENEF_ENC'] . '/isback/1');
				}
			} else {
				try {
					//-----insert benef--------------
					$this->_db->beginTransaction();
					$Beneficiary = new Beneficiary();
					//echo '<pre>';
					//var_dump($mode);
					//var_dump($content);die;
					if ($mode == 'Add') {
						$add = $Beneficiary->add($content);
					} else if ($mode == 'Edit') {
						$add = $Beneficiary->update($content);
					}


					Application_Helper_General::writeLog('BADA', 'Add Destination Account ' . $content['BENEFICIARY_ACCOUNT']);
					$this->_db->commit();
					unset($_SESSION['beneficiaryAccountAddEdit']);
					$this->setbackURL('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName() . '/index/');
					$this->_redirect('/notification/success');
				} catch (Exception $e) {
					//rollback changes
					$this->_db->rollBack();
				}
			}
		}
		Application_Helper_General::writeLog('BADA', 'Confirm Destination Account');
	}

	public function editemailAction()
	{
		$model = new predefinedbeneficiary_Model_Predefinedbeneficiary();
		$param['user_id'] = $this->_userIdLogin;
		$param['payType'] =  $this->_payType;
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$this->view->token = $sessionNamespace->token;

		$AESMYSQL = new Crypt_AESMYSQL();
		$benef_id = $AESMYSQL->decrypt($this->_getParam('benef_id'), $password);

		$param['cust_id'] = $this->_custIdLogin;
		//echo $benef_id."asfd"; die;

		if (!$this->_request->isPost()) {
			//$benef_id = $this->_getParam('benef_id');
			$param['benef_id'] = $benef_id = (Zend_Validate::is($benef_id, 'Digits')) ? $benef_id : null;
			if ($benef_id) {
				$temp = $model->getBeneficiaries($param);
				$resultdata = $temp[0];
				if ($resultdata) {
					$this->view->BENEFICIARY_ID 	= $this->_getParam('benef_id');
					$this->view->ACBENEF_ALIAS 		= $resultdata['BENEFICIARY_ALIAS'];
					$this->view->ACBENEF  			= $resultdata['BENEFICIARY_ACCOUNT'];
					$this->view->ACBENEF_BANKNAME 	= $resultdata['BENEFICIARY_NAME'];
					$this->view->CURR_CODE    		= $resultdata['CURR_CODE'];
					$this->view->ADDRESS    		= $resultdata['BENEFICIARY_ADDRESS'];
					$this->view->CITIZENSHIP    	= $resultdata['BENEFICIARY_CITIZENSHIP'];
					$this->view->BENEFICIARY_RESIDENT   = $resultdata['BENEFICIARY_RESIDENT'];
					$this->view->ACBENEF_EMAIL  	= $resultdata['BENEFICIARY_EMAIL'];
					$this->view->BANK_NAME  		= $resultdata['BANK_NAME'];
					$this->view->CLR_CODE  	= $resultdata['CLR_CODE'];
					$this->view->BANK_CITY  		= $resultdata['BANK_CITY'];

					// 					$this->view->CITIZENSHIP		  		= $resultdata['BENEFICIARY_RESIDENT'];
					// 					$category_name = $model->getCategoryCode($resultdata['BENEFICIARY_CATEGORY']);
					// 					print_r($temp);;die;
					// 					$this->view->CATEGORY_NAME_LLD		  		= $category_name['0']['sm_text1'];


					// 					$this->view->CITIZENSHIP		  		= $resultdata['BENEFICIARY_RESIDENT'];
					$this->view->BENEIDENTIF_NAME_LLD		  		= $resultdata['BENEFICIARY_ID_TYPE'];
					$this->view->LLD_BENENUMBER		  		= $resultdata['BENEFICIARY_ID_NUMBER'];

					$this->view->POB_NUMBER = $resultdata['POB_NUMBER'];
					$this->view->BENEFICIARY_BANK_CODE = $resultdata['BENEFICIARY_BANK_CODE'];
					$this->view->BANK_CITY = $resultdata['BANK_CITY'];
					$this->view->BANK_ADDRESS1 = $resultdata['BANK_ADDRESS1'];
					$this->view->BANK_ADDRESS2 = $resultdata['BANK_ADDRESS2'];
					$this->view->BANK_COUNTRY = $resultdata['BANK_COUNTRY'];
					$this->view->SWIFT_CODE = $resultdata['SWIFT_CODE'];

					$CITY_CODE 		= $resultdata['BENEFICIARY_CITY_CODE'];
					$LLD_CATEGORY 	= $resultdata['BENEFICIARY_CATEGORY'];
					$modelCity 			= new predefinedbeneficiary_Model_Predefinedbeneficiary();
					$arr 				= $modelCity->getCountry($resultdata['BANK_COUNTRY']);
					//print_r($arr);die;
					$this->view->BANK_COUNTRY = $arr['0']['COUNTRY_NAME'];

					$CITY_CODEGet 		= (!empty($CITY_CODE) ? $CITY_CODE : '');
					$arr 				= $modelCity->getCityCode($CITY_CODEGet);

					// 9. Create LLD string
					$settings 			= new Application_Settings();
					$LLD_array 			= array();
					$LLD_DESC_arrayCat 	= array();
					$lldTypeArr  		= $settings->getLLDDOMType();

					if (!empty($LLD_CATEGORY)) {
						$lldCategoryArr  	= $settings->getLLDDOMCategory();
						$LLD_array["CT"] 	= $LLD_CATEGORY;
						$LLD_CATEGORY_POST  = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
					}

					$this->view->LLD_CATEGORY  		= $LLD_CATEGORY_POST;
					$this->view->LLD_BENEIDENTIF  	= $resultdata['BENEFICIARY_ID_TYPE'];
					$this->view->LLD_BENENUMBER  	= $resultdata['BENEFICIARY_ID_NUMBER'];
					$this->view->CITY_CODE  		= $arr[0]['CITY_NAME'];
				}
			} else {
				$error_remark = 'Beneficiary ID does not exist.';
				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				$this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
				$this->_redirect('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName());
			}
		} else {

			$get_request = $this->_request->getParams();
			$get_id = $get_request["BENEFICIARY_ID"];

			$get_id = urldecode($get_id);

			$get_id = $AESMYSQL->decrypt($get_id, $password);
			$get_request["BENEFICIARY_ID"] = $get_id;

			$filters = array(
				'BENEFICIARY_ID' => array('StringTrim', 'StripTags'),
				'ACBENEF_EMAIL' 	=> array('StringTrim', 'StripTags')
			);

			$validators = array(
				'BENEFICIARY_ID' => array(
					'NotEmpty',
					'Digits',
					'messages' => array(
						$this->language->_('Error') . ': ' . $this->language->_('Beneficiary ID cannot be left blank') . '.',
						$this->language->_('Error') . ': ' . $this->language->_('Wrong Format Beneficiary ID') . '.'
					)
				),
				'ACBENEF_EMAIL' => array(	//new SGO_Validate_EmailAddress(),
					'allowEmpty' => true,
					//'messages' => array(
					//	'Error: Wrong Format Email Address of {$email}. Please correct it.')
				)
			);

			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $get_request, $this->_optionsValidator);
			if ($zf_filter_input->isValid()) {
				$error = false;

				if (trim($zf_filter_input->ACBENEF_EMAIL) != "") {
					$arr_email = explode(";", $zf_filter_input->ACBENEF_EMAIL);
					foreach ($arr_email as $value) {
						if (Zend_Validate::is(trim($value), 'EmailAddress') == false) {
							$error = true;
							break;
						}
					}
				}

				if (!$error) {
					try {
						//-----edit email--------------
						$this->_db->beginTransaction();

						$Beneficiary = new Beneficiary();
						//echo $benef_id; die;
						$Beneficiary->editEmail($zf_filter_input->BENEFICIARY_ID, $zf_filter_input->ACBENEF_EMAIL);

						Application_Helper_General::writeLog('BEDA', 'Edit Email Destination Account ' . $this->_getParam('ACBENEF'));

						$this->_db->commit();
						$this->setbackURL('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName() . '/index/');
						$this->_redirect('/notification/success');
					} catch (Exception $e) {
						//rollback changes
						$this->_db->rollBack();
						$this->fillParam($zf_filter_input);
					}
				} else {
					$this->view->error = true;
					$param['benef_id'] = $zf_filter_input->BENEFICIARY_ID;
					$temp = $model->getBeneficiaries($param);
					$resultdata = $temp[0];

					if ($resultdata) {
						$this->view->BENEFICIARY_ID 	= $resultdata['BENEFICIARY_ID'];
						$this->view->ACBENEF_ALIAS 		= $resultdata['BENEFICIARY_ALIAS'];
						$this->view->ACBENEF  			= $resultdata['BENEFICIARY_ACCOUNT'];
						$this->view->ACBENEF_BANKNAME 	= $resultdata['BENEFICIARY_NAME'];
						$this->view->CURR_CODE    		= $resultdata['CURR_CODE'];
						$this->view->ADDRESS    		= $resultdata['BENEFICIARY_ADDRESS'];
						$this->view->BENEFICIARY_RESIDENT   = $resultdata['BENEFICIARY_RESIDENT'];
						$this->view->CITIZENSHIP    	= $resultdata['BENEFICIARY_CITIZENSHIP'];
						$this->view->BANK_NAME  		= $resultdata['BANK_NAME'];
						$this->view->CLR_CODE  	= $resultdata['CLR_CODE'];
						$this->view->BANK_CITY  		= $resultdata['BANK_CITY'];

						$this->view->POB_NUMBER = $resultdata['POB_NUMBER'];
						$this->view->BENEFICIARY_BANK_CODE = $resultdata['BENEFICIARY_BANK_CODE'];
						$this->view->BANK_CITY = $resultdata['BANK_CITY'];
						$this->view->BANK_ADDRESS1 = $resultdata['BANK_ADDRESS1'];
						$this->view->BANK_ADDRESS2 = $resultdata['BANK_ADDRESS2'];
						$this->view->BANK_COUNTRY = $resultdata['BANK_COUNTRY'];

						$CITY_CODE 		= $resultdata['BENEFICIARY_CITY_CODE'];
						$LLD_CATEGORY 	= $resultdata['BENEFICIARY_CATEGORY'];

						$modelCity 			= new predefinedbeneficiary_Model_Predefinedbeneficiary();
						$CITY_CODEGet 		= (!empty($CITY_CODE) ? $CITY_CODE : '');
						$arr 				= $modelCity->getCityCode($CITY_CODEGet);

						// 9. Create LLD string
						$settings 			= new Application_Settings();
						$LLD_array 			= array();
						$LLD_DESC_arrayCat 	= array();
						$lldTypeArr  		= $settings->getLLDDOMType();

						if (!empty($LLD_CATEGORY)) {
							$lldCategoryArr  	= $settings->getLLDDOMCategory();
							$LLD_array["CT"] 	= $LLD_CATEGORY;
							$LLD_CATEGORY_POST  = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
						}

						$this->view->LLD_CATEGORY  		= $LLD_CATEGORY_POST;
						$this->view->LLD_BENEIDENTIF  	= $resultdata['BENEFICIARY_ID_TYPE'];
						$this->view->LLD_BENENUMBER  	= $resultdata['BENEFICIARY_ID_NUMBER'];
						$this->view->CITY_CODE  		= $arr[0]['CITY_NAME'];
					}
					$this->view->ACBENEF_EMAIL = ($zf_filter_input->isValid('ACBENEF_EMAIL')) ? $zf_filter_input->ACBENEF_EMAIL : $this->_getParam('ACBENEF_EMAIL');
					$docErr = $this->language->_('Error') . ': ' . $this->language->_('Invalid Format - Email Address') . '.';
					$this->view->report_msg = $docErr;
				}
			} else {
				$this->view->error = true;
				$param['benef_id'] = $zf_filter_input->BENEFICIARY_ID;
				$temp = $model->getBeneficiaries($param);
				$resultdata = $temp[0];
				if ($resultdata) {
					$this->view->BENEFICIARY_ID 	= $resultdata['BENEFICIARY_ID'];
					$this->view->ACBENEF_ALIAS 		= $resultdata['BENEFICIARY_ALIAS'];
					$this->view->ACBENEF  			= $resultdata['BENEFICIARY_ACCOUNT'];
					$this->view->ACBENEF_BANKNAME 	= $resultdata['BENEFICIARY_NAME'];
					$this->view->CURR_CODE    		= $resultdata['CURR_CODE'];
					$this->view->ADDRESS    		= $resultdata['BENEFICIARY_ADDRESS'];
					$this->view->BENEFICIARY_RESIDENT   = $resultdata['BENEFICIARY_RESIDENT'];
					$this->view->CITIZENSHIP    	= $resultdata['BENEFICIARY_CITIZENSHIP'];
					$this->view->BANK_NAME  		= $resultdata['BANK_NAME'];
					$this->view->CLR_CODE  	= $resultdata['CLR_CODE'];
					$this->view->BANK_CITY  		= $resultdata['BANK_CITY'];

					$CITY_CODE 		= $resultdata['BENEFICIARY_CITY_CODE'];
					$LLD_CATEGORY 	= $resultdata['BENEFICIARY_CATEGORY'];

					$modelCity 			= new predefinedbeneficiary_Model_Predefinedbeneficiary();
					$CITY_CODEGet 		= (!empty($CITY_CODE) ? $CITY_CODE : '');
					$arr 				= $modelCity->getCityCode($CITY_CODEGet);

					// 9. Create LLD string
					$settings 			= new Application_Settings();
					$LLD_array 			= array();
					$LLD_DESC_arrayCat 	= array();
					$lldTypeArr  		= $settings->getLLDDOMType();

					if (!empty($LLD_CATEGORY)) {
						$lldCategoryArr  	= $settings->getLLDDOMCategory();
						$LLD_array["CT"] 	= $LLD_CATEGORY;
						$LLD_CATEGORY_POST  = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
					}

					$this->view->LLD_CATEGORY  		= $LLD_CATEGORY_POST;
					$this->view->LLD_BENEIDENTIF  	= $resultdata['BENEFICIARY_ID_TYPE'];
					$this->view->LLD_BENENUMBER  	= $resultdata['BENEFICIARY_ID_NUMBER'];
					$this->view->CITY_CODE  		= $arr[0]['CITY_NAME'];
				}
				$this->view->ACBENEF_EMAIL = ($zf_filter_input->isValid('ACBENEF_EMAIL')) ? $zf_filter_input->ACBENEF_EMAIL : $this->_getParam('ACBENEF_EMAIL');
				$docErr = $this->displayError($zf_filter_input->getMessages());
				$this->view->report_msg = $docErr;
				//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $docErr);
			}

			$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
			if (count($temp) > 1) {
				if ($temp[0] == 'F' || $temp[0] == 'S') {
					if ($temp[0] == 'F')
						$this->view->error = 1;
					else
						$this->view->success = 1;
					$msg = '';
					unset($temp[0]);
					foreach ($temp as $value) {
						if (!is_array($value))
							$value = array($value);
						$msg .= $this->view->formErrors($value);
					}
					$this->view->report_msg = $msg;
				}
			}
		}
		Application_Helper_General::writeLog('BEDA', 'Edit Email Destination Account ');
	}



	public function detailAction()
	{
		$model = new predefinedbeneficiary_Model_Predefinedbeneficiary();
		$param['user_id'] = $this->_userIdLogin;
		$param['payType'] =  $this->_payType;

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$this->view->token = $sessionNamespace->token;

		$AESMYSQL = new Crypt_AESMYSQL();
		$benef_id = $AESMYSQL->decrypt($this->_getParam('benef_id'), $password);

		//$benef_id = $this->_getParam('benef_id');
		$param['cust_id'] = $this->_custIdLogin;
		//echo $benef_id."asfd"; die;

		if (!$this->_request->isPost()) {
			//$benef_id = $this->_getParam('benef_id');
			$param['benef_id'] = $benef_id = (Zend_Validate::is($benef_id, 'Digits')) ? $benef_id : null;
			if ($benef_id) {
				//print_r($param);die;
				$temp = $model->getBeneficiaries($param);
				$resultdata = $temp[0];
				if ($resultdata) {
					$this->view->BENEFICIARY_ID 	= $resultdata['BENEFICIARY_ID'];
					$this->view->ACBENEF_ALIAS 		= $resultdata['BENEFICIARY_ALIAS'];
					$this->view->ACBENEF  			= $resultdata['BENEFICIARY_ACCOUNT'];
					$this->view->ACBENEF_BANKNAME 	= $resultdata['BENEFICIARY_NAME'];
					$this->view->CURR_CODE    		= $resultdata['CURR_CODE'];
					$this->view->ADDRESS    		= $resultdata['BENEFICIARY_ADDRESS'];
					$this->view->CITIZENSHIP    	= $resultdata['BENEFICIARY_CITIZENSHIP'];
					$this->view->BENEFICIARY_RESIDENT   = $resultdata['BENEFICIARY_RESIDENT'];
					$this->view->ACBENEF_EMAIL  	= $resultdata['BENEFICIARY_EMAIL'];
					$this->view->BANK_NAME  		= $resultdata['BANK_NAME'];
					$this->view->CLR_CODE  	= $resultdata['CLR_CODE'];
					$this->view->BANK_CITY  		= $resultdata['BANK_CITY'];
					$this->view->SWIFT_CODE  		= $resultdata['SWIFT_CODE'];


					// 					$this->view->CITIZENSHIP		  		= $resultdata['BENEFICIARY_RESIDENT'];
					// 					$category_name = $model->getCategoryCode($resultdata['BENEFICIARY_CATEGORY']);
					// 					print_r($temp);;die;
					// 					$this->view->CATEGORY_NAME_LLD		  		= $category_name['0']['sm_text1'];


					// 					$this->view->CITIZENSHIP		  		= $resultdata['BENEFICIARY_RESIDENT'];
					$this->view->BENEIDENTIF_NAME_LLD		  		= $resultdata['BENEFICIARY_ID_TYPE'];
					$this->view->LLD_BENENUMBER		  		= $resultdata['BENEFICIARY_ID_NUMBER'];

					$this->view->POB_NUMBER = $resultdata['POB_NUMBER'];
					$this->view->BENEFICIARY_BANK_CODE = $resultdata['BENEFICIARY_BANK_CODE'];
					$this->view->BANK_CITY = $resultdata['BANK_CITY'];
					$this->view->BANK_ADDRESS1 = $resultdata['BANK_ADDRESS1'];
					$this->view->BANK_ADDRESS2 = $resultdata['BANK_ADDRESS2'];
					$this->view->BANK_COUNTRY = $resultdata['BANK_COUNTRY'];

					$CITY_CODE 		= $resultdata['BENEFICIARY_CITY_CODE'];
					$LLD_CATEGORY 	= $resultdata['BENEFICIARY_CATEGORY'];

					$modelCity 			= new predefinedbeneficiary_Model_Predefinedbeneficiary();
					$arr 				= $modelCity->getCountry($resultdata['BANK_COUNTRY']);
					//print_r($arr);die;
					$this->view->BANK_COUNTRY = $arr['0']['COUNTRY_NAME'];
					$CITY_CODEGet 		= (!empty($CITY_CODE) ? $CITY_CODE : '');
					$arr 				= $modelCity->getCityCode($CITY_CODEGet);

					// 9. Create LLD string
					$settings 			= new Application_Settings();
					$LLD_array 			= array();
					$LLD_DESC_arrayCat 	= array();
					$lldTypeArr  		= $settings->getLLDDOMType();

					if (!empty($LLD_CATEGORY)) {
						$lldCategoryArr  	= $settings->getLLDDOMCategory();
						$LLD_array["CT"] 	= $LLD_CATEGORY;
						$LLD_CATEGORY_POST  = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
					}

					$this->view->LLD_CATEGORY  		= $LLD_CATEGORY_POST;
					$this->view->LLD_BENEIDENTIF  	= $resultdata['BENEFICIARY_ID_TYPE'];
					$this->view->LLD_BENENUMBER  	= $resultdata['BENEFICIARY_ID_NUMBER'];
					$this->view->CITY_CODE  		= $arr[0]['CITY_NAME'];
				}
			} else {
				$error_remark = 'Beneficiary ID does not exist.';
				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				$this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
				$this->_redirect('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName());
			}
		}

		Application_Helper_General::writeLog('BEDA', 'Edit Email Destination Account ');
	}

	public function deleteAction()
	{
		// echo "<pre>";
		// print_r($this->_request->getParams());die;
		$this->_helper->layout()->setLayout('newlayout');
		if (!$this->_request->isPost()) {
			$benef_id = $this->_getParam('benef_id');
			// print_r($benef_id);die;
			$checkmulti = explode(',', $benef_id);
			// var_dump($checkmulti);die;
			if (empty($checkmulti[1])) {
				$this->view->multiview = false;
				$benef_id = (Zend_Validate::is($benef_id, 'Digits')) ? $benef_id : null;

				if ($benef_id) {
					$resultdata = $this->_db->fetchRow(
						$this->_db->select()
							->from(array('M_BENEFICIARY'))
							->where("BENEFICIARY_ID=?", $benef_id)
					);
					if ($resultdata) {
						$this->view->BENEFICIARY_ID 	= $resultdata['BENEFICIARY_ID'];
						$this->view->ACBENEF_ALIAS 		= $resultdata['BENEFICIARY_ALIAS'];
						$this->view->ACBENEF  			= $resultdata['BENEFICIARY_ACCOUNT'];
						$this->view->ACBENEF_BANKNAME 	= $resultdata['BENEFICIARY_NAME'];
						$this->view->CURR_CODE    		= $resultdata['CURR_CODE'];
						$this->view->ADDRESS    		= $resultdata['BENEFICIARY_ADDRESS'];
						$this->view->CITIZENSHIP    	= $resultdata['BENEFICIARY_CITIZENSHIP'];
						$this->view->BENEFICIARY_RESIDENT   = $resultdata['BENEFICIARY_RESIDENT'];
						$this->view->ACBENEF_EMAIL  	= $resultdata['BENEFICIARY_EMAIL'];
						$this->view->BANK_NAME  		= $resultdata['BANK_NAME'];
						$this->view->CLR_CODE  	= $resultdata['CLR_CODE'];
						$this->view->BANK_CITY  		= $resultdata['BANK_CITY'];
						$this->view->ACBENEF_NAME  		= $resultdata['BENEFICIARY_NAME'];

						$this->view->CITIZENSHIP		  		= $resultdata['BENEFICIARY_RESIDENT'];
						$this->view->NATIONALITY		  		= $resultdata['BENEFICIARY_CITIZENSHIP'];

						$CATEGORY = $resultdata['BENEFICIARY_CATEGORY'];
						if ($CATEGORY == '5') {
							$CATEGORY_CODE = 'Company';
						} elseif ($CATEGORY == '1') {
							$CATEGORY_CODE = 'Individual';
						} elseif ($CATEGORY == '2') {
							$CATEGORY_CODE = 'Government';
						} elseif ($CATEGORY == '3') {
							$CATEGORY_CODE = 'Bank';
						} elseif ($CATEGORY == '4') {
							$CATEGORY_CODE = 'Non Bank Financial Institution';
						} elseif ($CATEGORY == '6') {
							$CATEGORY_CODE = 'Other';
						}
						//$category_name = $model->getCategoryCode($resultdata['BENEFICIARY_CATEGORY']);
						//print_r($resultdata['BENEFICIARY_CATEGORY']);;die;
						$this->view->CATEGORY_NAME_LLD		  		= $CATEGORY_CODE;

						// 					$this->view->CITIZENSHIP		  		= $resultdata['BENEFICIARY_RESIDENT'];
						$this->view->BENEIDENTIF_NAME_LLD		  		= $resultdata['BENEFICIARY_ID_TYPE'];
						$this->view->LLD_BENENUMBER		  		= $resultdata['BENEFICIARY_ID_NUMBER'];

						$this->view->POB_NUMBER = $resultdata['POB_NUMBER'];
						$this->view->BENEFICIARY_BANK_CODE = $resultdata['SWIFT_CODE'];

						$this->view->BANK_ADDRESS1 = $resultdata['BANK_ADDRESS1'];
						$this->view->BANK_ADDRESS2 = $resultdata['BANK_ADDRESS2'];
						$this->view->BANK_COUNTRY = $resultdata['BANK_COUNTRY'];

						$CITY_CODE 		= $resultdata['BENEFICIARY_CITY_CODE'];
						$LLD_CATEGORY 	= $resultdata['BENEFICIARY_CATEGORY'];

						$modelCity 			= new predefinedbeneficiary_Model_Predefinedbeneficiary();
						$CITY_CODEGet 		= (!empty($CITY_CODE) ? $CITY_CODE : '');
						$arr 				= $modelCity->getCountry($resultdata['BANK_COUNTRY']);
						//print_r($arr);die;
						$this->view->BANK_COUNTRY = $arr['0']['COUNTRY_NAME'];
						$this->view->SWIFT_CODE =   $resultdata['SWIFT_CODE'];

						// 9. Create LLD string
						$settings 			= new Application_Settings();
						$LLD_array 			= array();
						$LLD_DESC_arrayCat 	= array();
						$lldTypeArr  		= $settings->getLLDDOMType();

						if (!empty($LLD_CATEGORY)) {
							$lldCategoryArr  	= $settings->getLLDDOMCategory();
							$LLD_array["CT"] 	= $LLD_CATEGORY;
							$LLD_CATEGORY_POST  = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
						}

						$this->view->LLD_CATEGORY  		= $LLD_CATEGORY_POST;
						$this->view->LLD_BENEIDENTIF  	= $resultdata['BENEFICIARY_ID_TYPE'];
						$this->view->LLD_BENENUMBER  	= $resultdata['BENEFICIARY_ID_NUMBER'];
						$this->view->STATUS		  	= $resultdata['BENEFICIARY_ISAPPROVE'];
						$this->view->CITY_CODE  		= $arr[0]['CITY_NAME'];
					}
				} else {
					$error_remark = 'Error: Beneficiary ID cannot be left blank.';

					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
				}
			} else {
				$this->view->multiview = true;

				$fields = array(
					/*'alias'  => array('field' => 'BENEFICIARY_ALIAS',
											   'label' => $this->language->_('Alias Name'),
											   'sortable' => true),*/
					'benef_acct'  => array(
						'field' => 'BENEFICIARY_ACCOUNT',
						'label' => $this->language->_('Beneficiary Account'),
						'sortable' => true
					),
					'benef_name'  => array(
						'field' => 'BENEFICIARY_NAME',
						'label' => $this->language->_('Beneficiary Account Name'),
						'sortable' => true
					),
					'email'  => array(
						'field' => 'BENEFICIARY_EMAIL',
						'label' => $this->language->_('Email Address'),
						'sortable' => true
					),
					'ccy'   => array(
						'field'    => 'CURR_CODE',
						'label'    => $this->language->_('CCY'),
						'sortable' => true
					),
					// 'favorite'   => array('field'    => 'ISFAVORITE',
					// 					  'label'    => $this->language->_('Favorite'),
					// 					  'sortable' => true),
					// 'checked'   => array('field'    => 'BENEFICIARY_BANKSTATUS_disp',
					// 					  'label'    => $this->language->_('Checked by Bank'),
					// 					  'sortable' => true),

					'date'   => array(
						'field'    => 'BENEFICIARY_CREATED',
						'label'    => $this->language->_('Created Date'),
						'sortable' => true
					),

					'status'   => array(
						'field'    => 'BENEFICIARY_ISAPPROVE',
						'label'    => $this->language->_('Status'),
						'sortable' => true
					)

					// 'delete'   => array('field'    => 'BENEFICIARY_ISREQUEST_DELETE',
					// 					  'label'    => $this->language->_('Delete'),
					// 					  'sortable' => false)
				);
				$this->view->fields = $fields;

				$select = $this->_db->select()
					->from(array('M_BENEFICIARY'))
					->where("BENEFICIARY_ID in (?)", $checkmulti);
				$this->paging($select);


				$resultdata = $this->_db->fetchAll(
					$this->_db->select()
						->from(array('M_BENEFICIARY'))
						->where("BENEFICIARY_ID in (?)", $checkmulti)
				);
				// echo $resultdata;die;

				// print_r($resultdata);die;
				if ($resultdata) {
					$benef_nationality = '';
					if ($resultdata['BENEFICIARY_CITIZENSHIP'] == 'W') $benef_nationality = 'WNI';
					else if ($resultdata['BENEFICIARY_CITIZENSHIP'] == 'N') $benef_nationality = 'WNA';

					$citizenship = '';
					if ($resultdata['BENEFICIARY_RESIDENT'] == 'NR') $citizenship = 'Non Resident';
					else if ($resultdata['BENEFICIARY_RESIDENT'] == 'R') $citizenship = 'Resident';

					$settings 			= new Application_Settings();


					if (!empty($resultdata['BENEFICIARY_CATEGORY'])) {
						$lldCategoryArr  	= $settings->getLLDDOMCategory();
						$LLD_CATEGORY_POST = $lldCategoryArr[$resultdata['BENEFICIARY_CATEGORY']];
					}

					if (!empty($resultdata['BENEFICIARY_ID_TYPE'])) {
						$lldBeneIdentifArr  	= $settings->getLLDDOMBeneIdentification();
						$LLD_BENEIDENTIF_POST = $lldBeneIdentifArr[$resultdata['BENEFICIARY_ID_TYPE']];
					}

					$this->view->BENEFICIARY_ID = $resultdata['BENEFICIARY_ID'];
					$this->view->ACBENEF_ALIAS 	= $resultdata['BENEFICIARY_ALIAS'];
					$this->view->ACBENEF  		= $resultdata['BENEFICIARY_ACCOUNT'];
					$this->view->ACBENEF_NAME	= $resultdata['BENEFICIARY_NAME'];
					$this->view->CURR_CODE    	= $resultdata['CURR_CODE'];
					$this->view->ACBENEF_EMAIL  = $resultdata['BENEFICIARY_EMAIL'];
					$this->view->STATUS		  	= $resultdata['BENEFICIARY_ISAPPROVE'];
					$this->view->BENEFICIARY_CITIZENSHIP = $citizenship;
					$this->view->BENEFICIARY_NATIONALITY	= $benef_nationality;
					$this->view->BENEFICIARY_CATEGORY = $LLD_CATEGORY_POST;
					$this->view->BENEFICIARY_ID_TYPE = $LLD_BENEIDENTIF_POST;
					$this->view->BENEFICIARY_ID_NUMBER = $resultdata['BENEFICIARY_ID_NUMBER'];
				}
			}
		} else {

			//	echo "<pre>";
			// 	print_r($this->_getParam('BENEFICIARY_ID')[1]);
			//var_dump($this->_getParam('BENEFICIARY_ID')[1]);die;

			if ($this->_getParam('BENEFICIARY_ID')[1] == 1) {


				$filters = array(
					'BENEFICIARY_ID' => array('StringTrim', 'StripTags'),
					'STATUS' => array('StringTrim', 'StripTags')
				);

				$validators = array(
					'BENEFICIARY_ID' => array(
						'NotEmpty',
						'Digits',
						'messages' => array(
							'Error: Beneficiary ID cannot be left blank.',
							'Error: Wrong Format Beneficiary ID.'
						)
					),
					'STATUS' 		=> array(
						'NotEmpty',
						'Digits',
						'messages' => array(
							'Error: Status cannot be left blank.',
							'Error: Wrong Format Status.'
						)
					)
				);

				$zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);
				if ($zf_filter_input->isValid()) {
					try {

						//-----delete--------------
						$this->_db->beginTransaction();
						$Beneficiary = new Beneficiary();
						if ($zf_filter_input->STATUS)
							$Beneficiary->suggestDelete($zf_filter_input->BENEFICIARY_ID);
						else
							$Beneficiary->delete($zf_filter_input->BENEFICIARY_ID);
						$this->_db->commit();

						//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), null);
						$this->setbackURL('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName() . '/index/');
						$this->_redirect('/notification/submited');
					} catch (Exception $e) {
						//rollback changes
						$this->_db->rollBack();
						$this->fillParam($zf_filter_input);
						$errorMsg = $this->getErrorRemark('82');
						$this->_helper->getHelper('FlashMessenger')->addMessage('F');
						$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);

						//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $errorMsg);
						//Application_Log_GeneralLog::technicalLog($e);
					}
				} else {

					$this->view->error = true;
					$resultdata = $this->_db->fetchRow(
						$this->_db->select()
							->from(array('M_BENEFICIARY'))
							->where("BENEFICIARY_ID=?", $zf_filter_input->BENEFICIARY_ID)
					);
					if ($resultdata) {
						$this->view->BENEFICIARY_ID 	= $resultdata['BENEFICIARY_ID'];
						$this->view->ACBENEF_ALIAS 		= $resultdata['BENEFICIARY_ALIAS'];
						$this->view->ACBENEF  			= $resultdata['BENEFICIARY_ACCOUNT'];
						$this->view->ACBENEF_NAME		= $resultdata['BENEFICIARY_NAME'];
						$this->view->CURR_CODE    		= $resultdata['CURR_CODE'];
						$this->view->CITIZENSHIP    	= $resultdata['BENEFICIARY_CITIZENSHIP'];
						$this->view->ADDRESS    		= $resultdata['BENEFICIARY_ADDRESS'];
						$this->view->BANK_NAME  		= $resultdata['BANK_NAME'];
						$this->view->CLR_CODE  	= $resultdata['CLR_CODE'];
						$this->view->BANK_CITY  		= $resultdata['BANK_CITY'];
						$this->view->ACBENEF_EMAIL  	= $resultdata['BENEFICIARY_EMAIL'];
						$this->view->STATUS		  		= $resultdata['BENEFICIARY_ISAPPROVE'];
					}
					$docErr = $this->displayError($zf_filter_input->getMessages());
					$this->view->report_msg = $docErr;
					//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $docErr);
				}
			} else {
				//die('here');
				try {
					//-----delete--------------
					$this->_db->beginTransaction();
					$Beneficiary = new Beneficiary();

					$id = $this->_getParam('BENEFICIARY_ID');

					if (count($id) == 1) {
						$Beneficiary->suggestDelete($id);
					} else {
						foreach ($this->_getParam('BENEFICIARY_ID') as $key => $value) {
							if ($this->_getParam('STATUS')[$key])
								$Beneficiary->suggestDelete($value);
							else
								$Beneficiary->delete($value);
						}
					}

					$this->_db->commit();

					//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), null);
					$this->setbackURL('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName() . '/index/');
					$this->_redirect('/notification/submited');
				} catch (Exception $e) {
					//rollback changes
					$this->_db->rollBack();
					$this->fillParam($zf_filter_input);
					$errorMsg = $this->getErrorRemark('82');
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);

					//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $errorMsg);
					//Application_Log_GeneralLog::technicalLog($e);
				}
			}
		}

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if (count($temp) > 1) {
			if ($temp[0] == 'F' || $temp[0] == 'S') {
				if ($temp[0] == 'F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = '';
				unset($temp[0]);
				foreach ($temp as $value) {
					if (!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}
		}
		Application_Helper_General::writeLog('BADA', 'Delete Beneficiary Account ' . $benef_id);
	}

	private function fillParam($zf_filter_input)
	{
		if (isset($zf_filter_input->BENEFICIARY_ID)) $this->view->BENEFICIARY_ID = ($zf_filter_input->isValid('BENEFICIARY_ID')) ? $zf_filter_input->BENEFICIARY_ID : $this->_getParam('BENEFICIARY_ID');
		$this->view->ACBENEF_ALIAS = ($zf_filter_input->isValid('ACBENEF_ALIAS')) ? $zf_filter_input->ACBENEF_ALIAS : $this->_getParam('ACBENEF_ALIAS');
		$this->view->ACBENEF = ($zf_filter_input->isValid('ACBENEF')) ? $zf_filter_input->ACBENEF : $this->_getParam('ACBENEF');
		$this->view->ACBENEF_BANKNAME = ($zf_filter_input->isValid('ACBENEF_BANKNAME')) ? $zf_filter_input->ACBENEF_BANKNAME : $this->_getParam('ACBENEF_BANKNAME');
		$this->view->CURR_CODE = ($zf_filter_input->isValid('CURR_CODE')) ? $zf_filter_input->CURR_CODE : $this->_getParam('CURR_CODE');
		$this->view->ADDRESS = ($zf_filter_input->isValid('ADDRESS')) ? $zf_filter_input->ADDRESS : $this->_getParam('ADDRESS');
		$this->view->CITIZENSHIP = ($zf_filter_input->isValid('CITIZENSHIP')) ? $zf_filter_input->CITIZENSHIP : $this->_getParam('CITIZENSHIP');
		$this->view->NATIONALITY = ($zf_filter_input->isValid('NATIONALITY')) ? $zf_filter_input->NATIONALITY : $this->_getParam('NATIONALITY');
		$this->view->ACBENEF_EMAIL = ($zf_filter_input->isValid('ACBENEF_EMAIL')) ? $zf_filter_input->ACBENEF_EMAIL : $this->_getParam('ACBENEF_EMAIL');
		$this->view->BANK_NAME = ($zf_filter_input->isValid('BANK_NAME')) ? $zf_filter_input->BANK_NAME : $this->_getParam('BANK_NAME');
		$this->view->CLR_CODE = ($zf_filter_input->isValid('CLR_CODE')) ? $zf_filter_input->CLR_CODE : $this->_getParam('CLR_CODE');
		$this->view->BANK_CITY = ($zf_filter_input->isValid('BANK_CITY')) ? $zf_filter_input->BANK_CITY : $this->_getParam('BANK_CITY');
	}

	private function fillParams($benefType, $ACBENEF_ALIAS, $ACBENEF, $ACBENEF_BANKNAME, $ACBENEF_CCY, $ACBENEF_EMAIL, $ACBENEF_CITIZENSHIP, $ACBENEF_RESIDENT, $ACBENEF_ADDRESS, $BANK_NAME, $CLR_CODE, $BANK_CITY, $BENEFICIARY_ID = null, $BENEFICIARY_TYPE, $LLD_CATEGORY, $LLD_BENEIDENTIF, $LLD_BENENUMBER, $CITY_CODE)
	{
		$modelCity = new predefinedbeneficiary_Model_Predefinedbeneficiary();
		$CITY_CODEGet = (!empty($CITY_CODE) ? $CITY_CODE : '');
		$arr 					= $modelCity->getCityCode($CITY_CODEGet);

		// 9. Create LLD string
		$settings 			= new Application_Settings();
		$LLD_array 			= array();
		$LLD_DESC_arrayCat 	= array();
		$lldTypeArr  		= $settings->getLLDDOMType();

		if (!empty($LLD_CATEGORY)) {
			$lldCategoryArr  	= $settings->getLLDDOMCategory();
			$LLD_array["CT"] 	= $LLD_CATEGORY;
			$LLD_CATEGORY_POST = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
		}

		if (!empty($LLD_BENEIDENTIF)) {
			$lldBeneIdentifArr  	= $settings->getLLDDOMBeneIdentification();
			$LLD_array["CT"] 	= $LLD_BENEIDENTIF;
			$LLD_BENEIDENTIF_POST = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldBeneIdentifArr[$LLD_BENEIDENTIF];
		}

		if ($BENEFICIARY_ID)
			$this->view->BENEFICIARY_ID = $BENEFICIARY_ID;
		//$this->view->benefType = $benefType;
		$this->view->ACBENEF_ALIAS = $ACBENEF_ALIAS;
		$this->view->ACBENEF = $ACBENEF;
		$this->view->ACBENEF_BANKNAME = $ACBENEF_BANKNAME;
		$this->view->CURR_CODE = $ACBENEF_CCY;
		$this->view->ADDRESS = $ACBENEF_ADDRESS;
		$this->view->CITIZENSHIP = $ACBENEF_RESIDENT;
		$this->view->NATIONALITY = $ACBENEF_CITIZENSHIP;
		$this->view->ACBENEF_EMAIL = $ACBENEF_EMAIL;
		$this->view->BANK_NAME = $BANK_NAME;
		$this->view->CLR_CODE = $CLR_CODE;
		$this->view->BANK_CITY = $BANK_CITY;
		$this->view->BENEFICIARY_TYPE = $BENEFICIARY_TYPE;

		$this->view->LLD_CATEGORY = $LLD_CATEGORY;;
		$this->view->LLD_BENEIDENTIF = $LLD_BENEIDENTIF;
		$this->view->LLD_BENENUMBER = $LLD_BENENUMBER;
		$this->view->CITY_CODE = $CITY_CODE;

		$this->view->CITY_NAME = $arr[0]['CITY_NAME'];
		$this->view->CATEGORY_NAME_LLD = $LLD_CATEGORY_POST;
		$this->view->BENEIDENTIF_NAME_LLD = $LLD_BENEIDENTIF_POST;
	}

	public function suggestdeleteAction()
	{
		$filters = array('BENEFICIARY_ID' => array('StringTrim', 'StripTags'));
		$validators = array('BENEFICIARY_ID' => array(
			'NotEmpty',
			'Digits',
			'messages' => array(
				$this->language->_('Error') . ': ' . $this->language->_('Beneficiary ID cannot be left blank') . '.',
				$this->language->_('Error') . ': ' . $this->language->_('Wrong Format Beneficiary ID') . '.'
			)
		));

		$zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);
		if ($zf_filter_input->isValid()) {
			$Beneficiary = new Beneficiary();
			$Beneficiary->suggestDelete($zf_filter_input->BENEFICIARY_ID);
		}
		$this->_redirect('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName());
	}
}
