<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';
require_once 'General/CustomerUser.php';
require_once 'CMD/Validate/ValidateAccountBeneficiary.php';
require_once 'General/Settings.php';
require_once 'Service/Token.php'; //added new 
require_once 'CMD/Validate/Validate.php';

class predefinedbeneficiary_DomesticcctnewController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti 

	public function initController()
	{
		$selectCurrency = '-- '.$this->language->_('Select Currency'). '--';
		$listCcy = array(''=>$selectCurrency);
		if (count($this->getCcy()) == 1){ //remove useless value
			$listCcy = array();
		}
		$listCcy = array_merge($listCcy,Application_Helper_Array::listArray($this->getCcy(),'CCY_ID','CCY_ID'));
		$this->view->ccy = $listCcy;
	}

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		//print_r($this->_request->getParams());
		$sessionNamespace = new Zend_Session_Namespace('import_predefbenef');
		//$data = $_SESSION['import_predefbenef'];	
		$data = $sessionNamespace->content;
		if(!empty($data)){
			$this->view->importdata = $data['importdata'];
			$this->view->importconfirm = $data['importconfirm'];
			
			if(is_array($data['acct']) && count($data['acct']) > 0){
				$validimport = 0;
				$invalidimport = 0;
			foreach($data['acct'] as $content)
			{
				$beneflist[] = $content['BENEFICIARY_ACCOUNT'];
				if(!empty($content['ERROR_MESSAGE'])){
					$validimport++;
				}else{
					$invalidimport++;
				}
			}
			
			$row=0;
			foreach($beneflist as $bene)
			{
				$temp=0;
				foreach($beneflist as $cek)
				{
					if($bene == $cek)
					{
						$temp++;
					}
				}
				if($temp>1 && !(isset($data['acct'][$row]['ERROR_MESSAGE'])))
				{
					//$data[$row] += array( 'ERROR_MESSAGE' => 'Duplicate Beneficiary Account' );
				}
				$row++;
			}
			
			//$data = $_SESSION['import_predefbenef'];
		}			 
			$this->view->totalimport = count($data['acct']);
			$this->view->successimport = $validimport;
			$this->view->failedimport = $invalidimport;
			$this->view->data = $data;
		}
		 
		$detailfile = $this->_request->getParam('detailfile');
		if($detailfile){
			
			$header = array('NO','BANK NAME', 'CLR CODE', 'BENEFICIARY ACCOUNT', 'CCY','BENEFICIARY NAME','EMAIL','PHONE','ADDRESS1','ADDRESS2','CITY','CATEGORY','CITIZENSHIP','NATIONALITY','ID TYPE','ID NUMBER','ERROR NOTES');
			$filename = 'Failed Account';
			$no = 0;
			foreach($data['acct'] as $content)
			{
				//$beneflist[] = $content['BENEFICIARY_ACCOUNT'];
				if(!empty($content['ERROR_MESSAGE'])){
					$dataDetails[$no]['NO'] = $no+1;
					$dataDetails[$no]['BANK_NAME'] = $content['BANK_NAME'];
					$dataDetails[$no]['CLR_CODE'] = $content['CLR_CODE'];
					$dataDetails[$no]['BENEFICIARY_ACCOUNT'] = $content['BENEFICIARY_ACCOUNT'];
					$dataDetails[$no]['CURR_CODE'] = $content['CURR_CODE'];
					$dataDetails[$no]['BENEFICIARY_NAME'] = $content['BENEFICIARY_NAME'];
					$dataDetails[$no]['BENEFICIARY_EMAIL'] = $content['BENEFICIARY_EMAIL'];
					$dataDetails[$no]['BENEFICIARY_PHONE'] = $content['BENEFICIARY_PHONE'];
					$dataDetails[$no]['BENEFICIARY_ADDRESS'] = $content['BENEFICIARY_ADDRESS'];
					$dataDetails[$no]['BENEFICIARY_ADDRESS2'] = $content['BENEFICIARY_ADDRESS2'];
					$dataDetails[$no]['CITY_NAME'] = $content['CITY_NAME'];
					$dataDetails[$no]['BENEFICIARY_CATEGORY_ORI'] = $content['BENEFICIARY_CATEGORY_ORI'];
					$dataDetails[$no]['BENEFICIARY_RESIDENT'] = $content['BENEFICIARY_RESIDENT'];
					$dataDetails[$no]['BENEFICIARY_CITIZENSHIP_ori'] = $content['BENEFICIARY_CITIZENSHIP_ori'];
					$dataDetails[$no]['BENEFICIARY_ID_TYPE'] = $content['BENEFICIARY_ID_TYPE'];
					$dataDetails[$no]['BENEFICIARY_ID_NUMBER'] = $content['BENEFICIARY_ID_NUMBER'];
					$dataDetails[$no]['ERROR_MESSAGE'] = $content['ERROR_MESSAGE'];
					$no++;
				}
			}
			//echo '<pre>';
			//var_dump($dataDetails);
			//die('here');
			$this->_helper->download->csv($header,$dataDetails,null,$filename);  	
			//die;
		}
		
		$backbtn = $this->_request->getParam('back');
		if($backbtn){
			unset($_SESSION['import_predefbenef']); 
		}
		
		$confirmimport = $this->_request->getParam('confirmimport');
		if($confirmimport){
			$Beneficiary = new Beneficiary();
			$this->_db->beginTransaction();
			try 
			{
			
			foreach($data['acct'] as $content)
			{
				if(empty($content['ERROR_MESSAGE'])){
					
					$add = $Beneficiary->add($content);
					
				}
				
			}
			$this->_db->commit();
				unset($_SESSION['import_predefbenef']); 
				$this->setbackURL('/'.$this->_request->getModuleName().'/importbeneficiarydom/index/');
				$this->_redirect('/notification/success');
				Application_Helper_General::writeLog('BIMA','Submit Import Beneficiary');
			}catch(Exception $e) 
			{
				//rollback changes
				$this->_db->rollBack();
				
				$errorMsg = $this->getErrorRemark('82');
				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
				
				//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $errorMsg);
				//Application_Log_GeneralLog::technicalLog($e);
			}
			
		}

			
		
			
		//echo '<pre>';
		//var_dump($validimport);
		//var_dump($invalidimport);
		//var_dump($data);
		//unset($_SESSION['import_predefbenef']); 
		$setting = new Settings();         
		$system_type = $setting->getSetting('system_type');
		
		$this->view->address_mandatory = $setting->getSetting('address_mandatory');
		
		
		$settings 			= new Application_Settings();
		$lldBeneIdentifArr 	= $settings->getLLDDOMBeneIdentification();
		$this->view->lldBeneIdentifArr  = $lldBeneIdentifArr;
		$lldCategoryArr  	= $settings->getLLDDOMCategory();
		$this->view->lldCategoryArr 	= $lldCategoryArr;

		$fields = array(
						/*'alias'  => array('field' => 'BENEFICIARY_ALIAS',
											   'label' => $this->language->_('Alias Name'),
											   'sortable' => true),*/
						'beneftype'   => array('field'    => 'BENEFICIARY_TYPE_disp',
											  'label'    => $this->language->_('Type'),
											  'sortable' => true),
						'bankname'   => array('field'    => 'BANK_NAME_disp',
											  'label'    => $this->language->_('Bank Name'),
											  'sortable' => true),
						'benef_acct'  => array('field' => 'BENEFICIARY_ACCOUNT',
											   'label' => $this->language->_('Beneficiary Account'),
											   'sortable' => true),
						'ccy'   => array('field'    => 'CURR_CODE',
											  'label'    => $this->language->_('CCY'),
											  'sortable' => true),
						'email'  => array('field' => 'BENEFICIARY_EMAIL',
											   'label' => $this->language->_('Email'),  
											   'sortable' => true),
 						'citizenship'   => array('field'    => 'BENEFICIARY_CITIZENSHIP_disp',
 											  'label'    => $this->language->_('Citizenship'),
 											  'sortable' => true)
						
						
				);

		$filterlist = array('BENEFICIARY_ACCOUNT','BENEFICIARY_NAME');

		$this->view->filterlist = $filterlist;

		$conf = Zend_Registry::get('config');
		$paymentType = $conf['payment']['type'];
		$paymentTypeFlip = array_flip($paymentType['code']);

		$this->view->paymentType = $paymentType;
		$this->view->paymentTypeFlip = $paymentTypeFlip;

		//get page, sortby, sortdir
		$csv    = $this->_getParam('csv');
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby','doc_no');
		$sortDir = $this->_getParam('sortdir','asc');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page,'Digits') && ($page > 0))? $page : 1;

		$sortBy = (Zend_Validate::is($sortBy,'InArray',
									 array(array_keys($fields))
									 ))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is($sortDir,'InArray',
										  array('haystack'=>array('asc','desc'))
										  ))? $sortDir : 'asc';


		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(	
							'alias' 	  	=> array('StringTrim','StripTags'),
							'BENEFICIARY_ACCOUNT'    => array('StringTrim','StripTags','StringToUpper'),
							'BENEFICIARY_NAME'    => array('StringTrim','StripTags')
		);


		// if POST value not null, get post, else get param
	//	$dataParam = array('BENEFICIARY_ACCOUNT','BENEFICIARY_NAME');
	//	$dataParamValue = array();
	//	foreach ($dataParam as $dtParam) {
	//		$dataPost = $this->_request->getPost($dtParam);
	//		$dataParamValue[$dtParam] = ($dataPost != null) ? $dataPost : $this->_getParam($dtParam);
		//}

		$dataParam = array('BENEFICIARY_ACCOUNT','BENEFICIARY_NAME');
		$dataParamValue = array();
		foreach ($dataParam as $dtParam)
		{

			 
			 
			if(!empty($this->_request->getParam('whereco'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('whereco') as $key => $value) {
						if($dtParam==$value){
							if(!empty($dataParamValue[$dtParam])){
								$dataval[$key] = $dataParamValue[$dtParam] .','.$dataval[$key]; 
							}
							
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}

			}

			

			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}
		//var_dump($dataParamValue);

		$options = array('allowEmpty' => true);
		$validators = array(
			'BENEFICIARY_ACCOUNT'		=> array(),
			'BENEFICIARY_NAME' 		=> array(),
		);

		$zf_filter = new Zend_Filter_Input($filterArr,$validators,$dataParamValue,$options);
		$filter = $this->_request->getParam('filter');

		$alpha = $this->_getParam('alpha');

		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		$this->view->alpha = $alpha;
		$this->view->benefType = 2;

		/*$select = $this->_db->select()
					   ->from('M_BENEFICIARY',array('BENEFICIARY_ID','BENEFICIARY_ALIAS','BENEFICIARY_ACCOUNT','BENEFICIARY_NAME','BENEFICIARY_EMAIL','CURR_CODE','BENEFICIARY_CITIZENSHIP' => '(CASE BENEFICIARY_CITIZENSHIP WHEN \'R\' THEN \'Resident\' WHEN \'NR\' THEN \'Non Resident\' END)','BENEFICIARY_ADDRESS','CLR_CODE','BANK_NAME','BENEFICIARY_ISREQUEST_DELETE','BENEFICIARY_ISAPPROVE' => '(CASE BENEFICIARY_ISAPPROVE WHEN 0 THEN \'Not Approved\' WHEN 1 THEN \'Approved\' END)','BENEFICIARY_CREATED'));
		$select->where("BENEFICIARY_TYPE = 2");
		$select->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin));*/

		$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$select   = $CustUser->getBeneficiaries($this->view->hasPrivilege('BLBU'));
		//$select->where("B.BENEFICIARY_TYPE = ?", (string) $this->_paymenttype["code"]["domestic"]);
		//$select->where("B.BENEFICIARY_TYPE = ?", 8);
		$select->where("B.BENEFICIARY_TYPE in (2,8)");

		if($alpha) $select->where("BENEFICIARY_NAME LIKE ".$this->_db->quote($alpha.'%'));

		// if($filter == TRUE)
		// {
		$fAlias = $zf_filter->getEscaped('alias');
		$fAcct = $zf_filter->getEscaped('BENEFICIARY_ACCOUNT');
		$fName = $zf_filter->getEscaped('BENEFICIARY_NAME');

		if($fAlias)$select->where('UPPER(BENEFICIARY_NAME) LIKE '.$this->_db->quote('%'.strtoupper($fAlias).'%'));
		// if($fAcct)$select->where('BENEFICIARY_ACCOUNT LIKE '.$this->_db->quote('%'.strtoupper($fAcct).'%'));
		if($fAcct)
		{	
			$fAcctArr = explode(',', $fAcct);
			$select->where("UPPER(BENEFICIARY_ACCOUNT)  in (?)",$fAcctArr );	
		}
		if($fName)
		{	
			$fNameArr = explode(',', $fName);
			$select->where("UPPER(BENEFICIARY_NAME)  in (?)",$fNameArr );	
		}
		// if($fName)$select->where('UPPER(BENEFICIARY_NAME) LIKE '.$this->_db->quote('%'.strtoupper($fName).'%'));

		$this->view->alias = $fAlias;
		$this->view->benef_acct = $fAcct;
		$this->view->benef_name = $fName;
		// }
		// echo "--"; var_dump($fAcct);

		$selectlimit   = $CustUser->getBeneficiaries($this->view->hasPrivilege('BLBU'));
		$selectlimit->where("B.BENEFICIARY_TYPE = ?", (string) $this->_paymenttype["code"]["domestic"]);
		$result = $select->query()->fetchAll();

	    $select->order($sortBy.' '.$sortDir);
		// echo $select;die;
		$this->paging($select);

		$data_val=array();
		foreach($result as $valu){
			$paramTrx = array (	
								"BENEFICIARY_TYPE"	=> $valu['BENEFICIARY_TYPE'],
								"BANK_NAME" => $valu['BANK_NAME'],
								"BENEFICIARY_ACCOUNT" => $valu['BENEFICIARY_ACCOUNT'],
								"CURR_CODE"	=> $valu['CURR_CODE'],
								"BENEFICIARY_EMAIL"	=> $valu['BENEFICIARY_EMAIL'],
								"BENEFICIARY_CITIZENSHIP"	=> $valu['BENEFICIARY_CITIZENSHIP'],
			);
			array_push($data_val, $paramTrx);
		}

		$filterlistdata = array("Filter");
			foreach($dataParamValue as $fil => $val){
					$paramTrx = $fil . " - " . $val;
					array_push($filterlistdata, $paramTrx);
		}

		if ($csv) {
			
			$header  = Application_Helper_Array::simpleArray($fields, "label");
			$listable = array_merge_recursive(array($header), $data_val);
			$this->_helper->download->csv($filterlistdata, $listable, null, 'Beneficiary List');
			
		}
		Application_Helper_General::writeLog('BLBA', 'Viewing SKN/RTGS Account');
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		
		if (!empty($dataParamValue)) {
				// $this->view->createdStart = $dataParamValue['PS_CREATED'];
				// $this->view->createdEnd = $dataParamValue['PS_CREATED_END'];
			//	$this->view->efdateStart = $dataParamValue['PS_EFDATE'];
			//	$this->view->efdateEnd = $dataParamValue['PS_EFDATE_END'];

				// unset($dataParamValue['PS_CREATED_END']);
				// unset($dataParamValue['PS_EFDATE_END']);

				foreach ($dataParamValue as $key => $value) {
					$duparr = explode(',',$value);
								if(!empty($duparr)){
									
									foreach($duparr as $ss => $vs){
										$wherecol[]	= $key;
										$whereval[] = $vs;
									}
								}else{
										$wherecol[]	= $key;
										$whereval[] = $value;
								}
				}
				$this->view->wherecol     = $wherecol;
				$this->view->whereval     = $whereval;
				 
			}
		
		//if(!empty($this->_request->getParam('wherecol'))){
			//	$this->view->wherecol			= $this->_request->getParam('wherecol');
		//	}

			//if(!empty($this->_request->getParam('whereopt'))){
		//		$this->view->whereopt			= $this->_request->getParam('whereopt');
		//	}

		//	if(!empty($this->_request->getParam('whereval'))){
		//		$this->view->whereval			= $this->_request->getParam('whereval');
		//	}

		//$this->_helper->download->csv(array('Group ID','Group Name','Status'),null,$select,'download group');
	}

	public function addbeneficiaryAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$anyValue = '-- '.$this->language->_('Select City'). ' --';
		$select = $this->_db->select()
				->from(array('A' => 'M_CITY'),array('*'));
		$select ->order('CITY_NAME ASC');
		$arr = $this->_db->fetchall($select);

//		$cityCodeArr 			= array(''=> $anyValue);
//		$cityCodeArr 			+= Application_Helper_Array::listArray($arr,'CITY_CODE','CITY_NAME');
//		$this->view->cityCodeArr 	= $cityCodeArr;
		$this->view->cityCodeList 		= $arr;

		$Settings = new Settings();
		$address_mandatory = $Settings->getSettingNew('address_mandatory');
		$this->view->address_mandatory = $address_mandatory;

		$settings 			= new Application_Settings();
		$lldCategoryArr  	= $settings->getLLDDOMCategory();
		$this->view->lldCategoryArr 	= $lldCategoryArr;

		$lldBeneIdentifArr 	= $settings->getLLDDOMBeneIdentification();
		$this->view->lldBeneIdentifArr  = $lldBeneIdentifArr;

		$this->view->CITIZENSHIP = 'R';
		$this->view->RESIDENT = 'R';
		//$this->view->CITIZENSHIP = $this->_getParam('RESIDENT');
		$this->view->benefType = '1';

		$privibenelinkage = $this->view->hasPrivilege('BLBU');
		$Beneficiary = new Beneficiary();
		$filter  = new Application_Filtering();
		$settings = new Application_Settings();
		$ccyList  = $settings->setCurrencyRegistered();
		$BANK_NAME = '-- Any Value --';

		$this->view->userIdLogin  = $this->_userIdLogin;
		//$benefType = "2";
		$benefType = $this->_getParam('benefType');

		//ambil data m_user ->ambil sourceAccount, acctType, statusnya aktif=1
		$acctStatus = 1;
		$dataAcct = $this->_db->fetchRow(
			$this->_db->select()
			->from(array('C' => 'M_CUSTOMER_ACCT'))
			->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin))
			->where("ACCT_STATUS = ?",$acctStatus)
			->limit(1)
		);
		$ACCT_NO = (!empty($dataAcct['ACCT_NO'])?$dataAcct['ACCT_NO']:'');
		$ACCT_TYPE = (!empty($dataAcct['ACCT_TYPE'])?$dataAcct['ACCT_TYPE']:'');

		//validasi token -- begin
		/*$select3 = $this->_db->select()
					 ->from(array('C' => 'M_USER'));
		$select3->where("USER_ID = ".$this->_db->quote($this->_userIdLogin));
		$data2 					= $this->_db->fetchRow($select3);
		$tokenIdUser 			= $data2['TOKEN_ID'];
		$tokenType 				= $data2['TOKEN_TYPE'];
		$usermobilephone 		= $data2['USER_MOBILE_PHONE'];
		$this->view->tokentype 	= $data2['TOKEN_TYPE'];*/

		//added new hard token
		/*$HardToken = new Service_Token($this->_custIdLogin, $this->_userIdLogin, $tokenIdUser);
		$challengeCode = $HardToken->generateChallengeCode();

		$challengeCodeSub = substr($challengeCode, 0,2);
		$this->view->challengeCodeReq = $challengeCodeSub;*/
		//validasi token -- end

		if($this->_request->isPost() )
		{
			$BENEFICIARY_ACCOUNT = $this->_getParam('ACBENEF');
			$CURR_CODE = $this->_getParam('CURR_CODE');
			$BENEFICIARY_ALIAS = $this->_getParam('ACBENEF_ALIAS');
			$BENEFICIARY_EMAIL = $this->_getParam('ACBENEF_EMAIL');
			$ACBENEF_ADDRESS = $this->_request->getParam('ADDRESS');
			$ACBENEF_ADDRESS2 = $this->_request->getParam('ADDRESS2');

			$ACBENEF_CITIZENSHIP= $this->_request->getParam('CITIZENSHIP');
		    $ACBENEF_RESIDENT= $this->_request->getParam('NATIONALITY');

		    $BANK_NAME   = $this->_request->getParam('BANK_NAME');
		    $BANK_CITY   = $this->_request->getParam('BANK_CITY');
		    $CLR_CODE   = $this->_request->getParam('CLR_CODE');
			$ACBENEF_BANKNAME  = $this->_request->getParam('ACBENEF_BANKNAME');

			$LLD_CATEGORY = $this->_request->getParam('LLD_CATEGORY');
			$LLD_BENEIDENTIF = $this->_request->getParam('LLD_BENEIDENTIF');
			$LLD_BENENUMBER = $this->_request->getParam('LLD_BENENUMBER');
			$CITY_CODE = $this->_request->getParam('CITY_CODE');
			$SWIFT_CODE   = $this->_request->getParam('SWIFT_CODE');
			$BENEFICIARY_BANK_CODE = $this->_request->getParam('BENEFICIARY_BANK_CODE');
		
	//-------------------------------------------------------------START VALIDATION---------------------------------------------------------------------//
			$ACBENEF    = $filter->filter($BENEFICIARY_ACCOUNT , "ACCOUNT_NO");
			$ACBENEF_BANKNAME  = $filter->filter($ACBENEF_BANKNAME , "ACCOUNT_NAME");
			$ACBENEF_ALIAS   = $filter->filter($BENEFICIARY_ALIAS , "ACCOUNT_ALIAS");
			$ACBENEF_EMAIL   = $filter->filter($BENEFICIARY_EMAIL , "EMAIL");
			$ACBENEF_CCY   = $filter->filter($CURR_CODE  , "SELECTION");
			$ACBENEF_ADDRESS = $filter->filter($ACBENEF_ADDRESS , "ADDRESS");
			$ACBENEF_ADDRESS2 = $filter->filter($ACBENEF_ADDRESS2 , "ADDRESS");
			//$ACBENEF_CITIZENSHIP= $this->_request->getParam('RESIDENT');
			$ACBENEF_CITIZENSHIP= $filter->filter($ACBENEF_CITIZENSHIP, "SELECTION");
			$ACBENEF_RESIDENT= $filter->filter($ACBENEF_RESIDENT, "SELECTION");



			// print_r($ACBENEF_RESIDENT);
			// print_r($ACBENEF_CITIZENSHIP);die;

			$BANK_NAME   = $filter->filter($BANK_NAME , "BANK_NAME");
			$BANK_CITY   = $filter->filter($BANK_CITY  , "ADDRESS");
			$CLR_CODE   = $filter->filter($CLR_CODE  , "BANK_CODE");

			$LLD_CATEGORY = $filter->filter($LLD_CATEGORY , "LLD_CATEGORY");
			$LLD_BENEIDENTIF = $filter->filter($LLD_BENEIDENTIF , "LLD_BENEIDENTIF");
			$LLD_BENENUMBER = $filter->filter($LLD_BENENUMBER , "LLD_BENENUMBER");
			$CITY_CODE = $filter->filter($CITY_CODE , "CITY_CODE");
			$SWIFT_CODE = $filter->filter($SWIFT_CODE , "SWIFT_CODE");

			//validasi token -- begin
			/*$challengeCodeReq2 	= $filter->filter($this->_request->getParam('challengeCodeReq2')	, "challengeCodeReq2");
			$challengeCodeReq 	= $filter->filter($this->_request->getParam('challengeCodeReq')	, "challengeCodeReq");

			$responseCodeReq 	= $filter->filter($this->_request->getParam('responseCodeReq')	, "responseCodeReq");
			//$random = str_pad(mt_rand(0, 9), 2, '0', STR_PAD_LEFT);
			$this->view->challengeCodeReq1 = substr($challengeCodeReq, -4);*/
			//validasi token -- end

//     		$paramBene = array(//"TRANSFER_TYPE"      	=>  'SKN',
//								"FROM"      			=> 'B',
//								 "ACBENEF"     			=> $ACBENEF,
//								 "ACBENEF_CCY"    		=> $ACBENEF_CCY,
//								 "ACBENEF_BANKNAME" 	=> $ACBENEF_BANKNAME,
//								 "ACBENEF_ALIAS"    	=> $ACBENEF_ALIAS,
//								 "ACBENEF_EMAIL"   		=> $ACBENEF_EMAIL,
//								 "ACBENEF_ADDRESS1"   	=> $ACBENEF_ADDRESS,
//								 "ACBENEF_RESIDENT"  	=> $ACBENEF_RESIDENT,
// 								 "ACBENEF_CITIZENSHIP"  => $ACBENEF_CITIZENSHIP,
//								 "BANK_CODE"    	 	=> $CLR_CODE,
//     							 "ACCTSRC"    			=> $ACBENEF,
//								 "_addBeneficiary"   	=> $this->view->hasPrivilege('BADA'),
//								 "_beneLinkage"    		=> $privibenelinkage,
//								);
//			if($benefType == 1){
//				$paramAdd = array("TRANSFER_TYPE"      	=>  'SKN',
//													 "ACBENEF_BANKNAME" 	=> &$ACBENEF_BANKNAME,
//													 "ACBENEF_ADDRESS1"   	=> $ACBENEF_ADDRESS,
//													 "ACBENEF_CITIZENSHIP"  => $ACBENEF_CITIZENSHIP,
//													);
//			}else{
//				$paramAdd = array("TRANSFER_TYPE"      	=>  'ONLINE',
//													 "ACBENEF_BANKNAME" 	=> '',
//													 "ACBENEF_ADDRESS1"   	=> '',
//													 "ACBENEF_CITIZENSHIP"  => '',
//													);
//			}
 
			$paramBene = array(
													"FROM"      			=> 'B',
													 "ACBENEF"     			=> $ACBENEF,
													 "ACBENEF_CCY"    		=> $ACBENEF_CCY,
													 "ACBENEF_ALIAS"    	=> $ACBENEF_ALIAS,
													 "ACBENEF_EMAIL"   		=> $ACBENEF_EMAIL,
													 "BANK_CODE"    	 	=> $CLR_CODE,
													 "_addBeneficiary"   	=> $this->view->hasPrivilege('BADA'),
													 "_beneLinkage"    		=> $privibenelinkage,
													 "ACCTSRC"     			=> $ACCT_NO, //source account m_user yg login
													 "VALIDATE"     		=> '1',
													 "sourceAccountType"    => $ACCT_TYPE,
												);
			if($benefType == 1){
				$paramAdd = array("TRANSFER_TYPE"      	=>  'SKN',
													 "ACBENEF_BANKNAME" 	=> $ACBENEF_BANKNAME,
													 "ACBENEF_ADDRESS1"   	=> $ACBENEF_ADDRESS,
													 "ACBENEF_ADDRESS2"   	=> $ACBENEF_ADDRESS2,
													 "ACBENEF_CITIZENSHIP"  => $ACBENEF_CITIZENSHIP,
													 "ACBENEF_RESIDENT"  	=> $ACBENEF_RESIDENT,

													 "LLD_CATEGORY"  	=> $LLD_CATEGORY,
													 "LLD_BENEIDENTIF"  	=> $LLD_BENEIDENTIF,
													 "LLD_BENENUMBER"  	=> $LLD_BENENUMBER,
													 "CITY_CODE"  	=> $CITY_CODE,
													);
			}else{
				$paramAdd = array("TRANSFER_TYPE"      	=>  'ONLINE',
													 "ACBENEF_BANKNAME" 	=> '',
													 "ACBENEF_ADDRESS1"   	=> '',
													 "ACBENEF_CITIZENSHIP"  => '',
													 "ACBENEF_RESIDENT"  	=> '',

													"LLD_CATEGORY"  	=> '',
													 "LLD_BENEIDENTIF"  	=> '',
													 "LLD_BENENUMBER"  	=> '',
													 "CITY_CODE"  	=> '',
													);
			}


     		$paramBene += array_merge($paramBene,$paramAdd);

			 $validateACBENEF = new ValidateAccountBeneficiary($ACBENEF, $this->_custIdLogin, $this->_userIdLogin);
			 $checkedParams = $validateACBENEF->check($paramBene);


			 if (!$validateACBENEF->isError())
			 {
				$content = array(
					'CUST_ID_LOGIN' 			=> $this->_custIdLogin,
					'USER_ID_LOGIN' 			=> $this->_userIdLogin,
					'BENEFICIARY_ALIAS' 		=> $ACBENEF_ALIAS,
					'BENEFICIARY_ACCOUNT' 		=> $ACBENEF,
					'BENEFICIARY_NAME' 			=> $ACBENEF_BANKNAME,
					'CURR_CODE' 				=> $ACBENEF_CCY,
					'BENEFICIARY_EMAIL' 		=> $ACBENEF_EMAIL,
 					'BENEFICIARY_CITIZENSHIP' 	=> $ACBENEF_RESIDENT,
					'BENEFICIARY_RESIDENT' 	=> $ACBENEF_CITIZENSHIP,
					'BENEFICIARY_ADDRESS' 		=> $ACBENEF_ADDRESS,
					'BENEFICIARY_ADDRESS2' 		=> $ACBENEF_ADDRESS2,
					'BANK_NAME' 				=> $BANK_NAME,
					'CLR_CODE' 					=> $CLR_CODE,
					'BANK_CODE' 				=> $BENEFICIARY_BANK_CODE,
					'SWIFT_CODE'    			=> $SWIFT_CODE,
					'BANK_CITY'			 		=> $BANK_CITY,
					'BENEFICIARY_TYPE' 			=> 2,
					'_beneLinkage'    			=> $privibenelinkage,

					'BENEFICIARY_CATEGORY'    	=> $LLD_CATEGORY,
					'BENEFICIARY_ID_TYPE'    	=> $LLD_BENEIDENTIF,
					'BENEFICIARY_ID_NUMBER'    	=> $LLD_BENENUMBER,
					'BENEFICIARY_CITY_CODE'    	=> $CITY_CODE,
				);

				//validasi hard token page 1 -- begin
				/*$message = $Settings->getSetting('token_confirm_message');
				$trans = array("[Response_Code]" => $responseCode);
				$message = strtr($message, $trans);

				if($tokenType == '1'){ //sms token
					if(empty($responseCodeReq)){
						$resultToken = FALSE;
						$this->view->error = true;
						$errMessage = 'Error : Response Token cannot be left blank.';
						$this->view->report_msg = $errMessage;
					}
					else
					{
						$token = new Service_Token(NULL,$this->_userIdLogin);
						$token->setMsisdn($usermobilephone);
						$token->setMessage($message);

						$res = $token->confirm();
						$resultToken = $res['ResponseCode'] == '0000';
					}
				}
				elseif($tokenType == '2'){ //hard token
					if(empty($responseCodeReq)){
						$resultToken = FALSE;
						$this->view->error = true;
						$errMessage = 'Error : Response Token cannot be left blank.';
						$this->view->report_msg = $errMessage;
					}
					else
					{
						$resHard = $HardToken->verifyHardToken($challengeCodeReq2.$challengeCodeReq, $responseCodeReq);
						$resultToken = $resHard['ResponseCode'] == '0000';

						//set user lock token gagal
						$CustUser = new CustomerUser($this->_userIdLogin);
						if ($resHard['ResponseCode'] != '0000'){
							$tokenFailed = $CustUser->setFailedTokenMustLogout();
							if ($tokenFailed)
								$this->_forward('home');
						}
					}
				}
				elseif($tokenType == '3'){ //mobile token
					$resultToken = TRUE;
				}
				else{$resultToken = TRUE;}
				//validasi hard token page 1 -- end	*/

				//if($resultToken == TRUE){
					try {

						//-----insert benef--------------
						/*$this->_db->beginTransaction();

						$add = $Beneficiary->add($content);

						Application_Helper_General::writeLog('BADA','Add Beneficiary Account '.$ACBENEF);

						$this->_db->commit();

						//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), null);
						//$this->view->success = true;
						//$msg = $this->getErrorRemark('00','Add Beneficiary');
						//$this->view->report_msg = $msg;
						$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
						$this->_redirect('/notification/success');*/
						//Zend_Debug::dump($checkedParams);die;

////////////////////////////////////////////////////////////////////////////yang difault - begin//////////////////////////////////
//						$content['BENEFICIARY_NAME'] = $checkedParams['ACBENEF_BANKNAME'];
//						//$content['BENEFICIARY_TYPE'] = 8; // 8 = online
//						$content['BENEFICIARY_TYPE'] = 2; // 8 = online
//						//$content['BANK_CODE'] = $CLR_CODE;
//						$content['CLR_CODE'] = $CLR_CODE;
//
//						$sessionNamespace = new Zend_Session_Namespace('beneficiaryAccountAddEdit');
//						$sessionNamespace->mode = 'Add';
//						$sessionNamespace->content = $content;
//						$this->_redirect('/predefinedbeneficiary/domesticcct/confirm');
////////////////////////////////////////////////////////////////////////////yang difault - end//////////////////////////////////

						if($benefType == '1'){
							$content['TYPE'] = 'SKN'; // 2 = domestic ( SKN/RTGS )

							$content['BENEFICIARY_TYPE'] = 2; // 2 = domestic ( SKN/RTGS )
							$content['CLR_CODE'] = $CLR_CODE;
							//echo '<pre>';
							//var_dump($content);die;
							$sessionNamespace = new Zend_Session_Namespace('beneficiaryAccountAddEdit');
							$sessionNamespace->mode = 'Add';
							$sessionNamespace->content = $content;
							$this->_redirect('/predefinedbeneficiary/domesticcct/confirm');
						}
						else if($benefType == '2'){
							$content['TYPE'] = 'ONLINE'; // 2 = domestic ( SKN/RTGS )

							$content['BENEFICIARY_NAME'] = $checkedParams['ACBENEF_BANKNAME'];
							$content['BENEFICIARY_TYPE'] = 8; // 8 = online
							//$content['BANK_CODE'] = $CLR_CODE;
							$content['CLR_CODE'] = $CLR_CODE;
							$sessionNamespace = new Zend_Session_Namespace('beneficiaryAccountAddEdit');
							$sessionNamespace->mode = 'Add';
							$sessionNamespace->content = $content;
							$this->_redirect('/predefinedbeneficiary/domesticcct/confirm');
						}
					}
					catch(Exception $e)
					{
						//rollback changes
						$this->_db->rollBack();

						//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $errorMsg);
						//Application_Log_GeneralLog::technicalLog($e);
					}
				//}else {
						/*
						$this->view->ACBENEF = $ACBENEF;

						if($benefType == 1){
							$this->view->BANK_NAME = $BANK_NAME;
						}
						elseif($benefType == 2){
							$this->view->BANK_NAME = $CLR_CODE.' - '.$BANK_NAME;
						}

						$this->view->CLR_CODE = $CLR_CODE;
						$this->view->benefType = $benefType;
						$this->view->ACBENEF_BANKNAME = $ACBENEF_BANKNAME;
						$this->view->ADDRESS = $ACBENEF_ADDRESS;



						if($resultToken == FALSE){
							if($res['ResponseCode'] == 'XT'){
								$errMessage = 'Error : Service Rejected';
								$this->view->resulttoken = $errMessage;
							}
							else{
								$errMessage = 'Error : Invalid Token';
								$this->view->resulttoken = $errMessage;
							}
						}

				}*/

				$this->view->benefType = $benefType;
			}
			else
			{
				$this->view->error = true;
// 				$this->fillParams($ACBENEF_ALIAS,$ACBENEF,$ACBENEF_BANKNAME,$ACBENEF_CCY,$ACBENEF_EMAIL,$ACBENEF_CITIZENSHIP,$ACBENEF_RESIDENT,$ACBENEF_ADDRESS,$BANK_NAME,$CLR_CODE,$BANK_CITY);
				//$this->fillParams(null,$ACBENEF_ALIAS,$ACBENEF,$ACBENEF_BANKNAME,$ACBENEF_CCY,$ACBENEF_EMAIL,$ACBENEF_CITIZENSHIP,$ACBENEF_RESIDENT,$ACBENEF_ADDRESS,$BANK_NAME,$CLR_CODE,$BANK_CITY);
				$this->fillParams($benefType,$ACBENEF_ALIAS,$ACBENEF,$ACBENEF_BANKNAME,$ACBENEF_CCY,$ACBENEF_EMAIL,$ACBENEF_CITIZENSHIP,$ACBENEF_RESIDENT,$ACBENEF_ADDRESS,$BANK_NAME,$CLR_CODE,$BANK_CITY,$BENEFICIARY_TYPE,$LLD_CATEGORY,$LLD_BENEIDENTIF,$LLD_BENENUMBER,$CITY_CODE);

				$docErr = ''.$validateACBENEF->getErrorMsg();
				$validateACBENEF->__destruct();
				unset($validateACBENEF);
				$this->view->report_msg = $docErr;
				$this->view->benefType = $benefType;
				$this->view->CITIZENSHIP = $this->_getParam('CITIZENSHIP');
				$this->view->RESIDENT = $this->_getParam('RESIDENT');

				$this->view->LLD_CATEGORY = $LLD_CATEGORY;
				$this->view->LLD_BENEIDENTIF = $LLD_BENEIDENTIF;
				$this->view->LLD_BENENUMBER = $LLD_BENENUMBER;
				$this->view->CITY_CODE = $CITY_CODE;
				$this->view->SWIFT_CODE = $SWIFT_CODE;

			}
		}
		//$this->view->CITIZENSHIP = $this->_getParam('RESIDENT');

		Application_Helper_General::writeLog('BADA','Viewing Add Beneficiary Account ');
	}

	public function editbeneficiaryAction()
	{

		$this->_helper->layout()->setLayout('newlayout');
		$anyValue = '-- '.$this->language->_('Select City'). ' --';
		$select = $this->_db->select()
				->from(array('A' => 'M_CITY'),array('*'));
		$select ->order('CITY_NAME ASC');
		$arr = $this->_db->fetchall($select);

//		$cityCodeArr 			= array(''=> $anyValue);
//		$cityCodeArr 			+= Application_Helper_Array::listArray($arr,'CITY_CODE','CITY_NAME');
//		$this->view->cityCodeArr 	= $cityCodeArr;
		$this->view->cityCodeList 		= $arr;

		$settings 			= new Application_Settings();
		$lldCategoryArr  	= $settings->getLLDDOMCategory();
		$this->view->lldCategoryArr 	= $lldCategoryArr;

		$lldBeneIdentifArr 	= $settings->getLLDDOMBeneIdentification();
		$this->view->lldBeneIdentifArr  = $lldBeneIdentifArr;

		////////////

		$Settings = new Settings();
		$address_mandatory = $Settings->getSettingNew('address_mandatory');
		$this->view->address_mandatory = $address_mandatory;

		$privibenelinkage = $this->view->hasPrivilege('BLBU');
		$Beneficiary = new Beneficiary();
		$filter  = new Application_Filtering();
		$settings = new Application_Settings();
		$ccyList  = $settings->setCurrencyRegistered();

		if(!$this->_request->isPost())
		{
			$benef_id = $this->_getParam('benef_id');
			$benef_id = (Zend_Validate::is($benef_id,'Digits'))? $benef_id : null;
			if($benef_id)
			{
			  $resultdata = $this->_db->fetchRow(
								$this->_db->select()
									 ->from(array('M_BENEFICIARY'))
									 ->where("BENEFICIARY_ID=?", $benef_id)
							);
			  if($resultdata)
			  {
					$this->view->BENEFICIARY_ID 	= $resultdata['BENEFICIARY_ID'];
					$this->view->ACBENEF_ALIAS 		= $resultdata['BENEFICIARY_ALIAS'];
					$this->view->ACBENEF  			= $resultdata['BENEFICIARY_ACCOUNT'];
					$this->view->ACBENEF_BANKNAME 	= $resultdata['BENEFICIARY_NAME'];
					$this->view->CURR_CODE    		= $resultdata['CURR_CODE'];
					$this->view->ADDRESS    		= $resultdata['BENEFICIARY_ADDRESS'];
					$this->view->ADDRESS2    		= $resultdata['BENEFICIARY_ADDRESS2'];
					$this->view->CITIZENSHIP    	= $resultdata['BENEFICIARY_RESIDENT'];
					$this->view->RESIDENT    		= $resultdata['BENEFICIARY_CITIZENSHIP'];
					$this->view->ACBENEF_EMAIL  	= 	 $resultdata['BENEFICIARY_EMAIL'];
					$this->view->BANK_NAME  		= $resultdata['BANK_NAME'];
					$this->view->CLR_CODE  			= $resultdata['CLR_CODE'];
					$this->view->BANK_CITY  		= $resultdata['BANK_CITY'];
					$this->view->SWIFT_CODE  		= $resultdata['SWIFT_CODE'];
					$this->view->LLD_CATEGORY  		= $resultdata['BENEFICIARY_CATEGORY'];
					$this->view->LLD_BENEIDENTIF  	= $resultdata['BENEFICIARY_ID_TYPE'];
					$this->view->LLD_BENENUMBER  	= $resultdata['BENEFICIARY_ID_NUMBER'];
					$this->view->CITY_CODE  		= $resultdata['BENEFICIARY_CITY_CODE'];
			   }

					$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
					$select   = $CustUser->getBeneficiaries($this->view->hasPrivilege('BLBU'));
					$select->where("B.BENEFICIARY_TYPE = ?", (string) $this->_paymenttype["code"]["domestic"]);
					$select->where("B.BENEFICIARY_ID = ?", (string) $benef_id);

					$isEdited = $select->query()->fetchAll();

					if($isEdited)
						$this->view->isEdited = true;
					else
					{
						$this->view->isEdited = false;
						$this->view->error = true;
						$this->view->report_msg = 'Error: You have no right to edit beneficiary.';
					}
			}
			else
			{
			   $error_remark = $this->getErrorRemark('22','Beneficiary ID');
			   $this->_helper->getHelper('FlashMessenger')->addMessage('F');
			   $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
			   $this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName());
			}
		}
		else
		{
			$ACBENEF_ID		 		= $this->_getParam('BENEFICIARY_ID');
			$BENEFICIARY_ACCOUNT 	= $this->_getParam('ACBENEF');
			$CURR_CODE 				= $this->_getParam('CURR_CODE');
			$BENEFICIARY_ALIAS 		= $this->_getParam('ACBENEF_ALIAS');
			$BENEFICIARY_EMAIL 		= $this->_getParam('ACBENEF_EMAIL');
			$ACBENEF_ADDRESS 		= $this->_request->getParam('ADDRESS');
			$ACBENEF_ADDRESS2 		= $this->_request->getParam('ADDRESS2');
		    $ACBENEF_CITIZENSHIP	= $this->_request->getParam('CITIZENSHIP');
		    $ACBENEF_RESIDENT	= $this->_request->getParam('RESIDENT');
			$BANK_NAME   			= $this->_request->getParam('BANK_NAME');
		    $BANK_CITY   			= $this->_request->getParam('BANK_CITY');
		    $CLR_CODE   			= $this->_request->getParam('CLR_CODE');
			$ACBENEF_BANKNAME  		= $this->_request->getParam('ACBENEF_BANKNAME');

			$LLD_CATEGORY  			= $this->_request->getParam('LLD_CATEGORY');
			$LLD_BENEIDENTIF  		= $this->_request->getParam('LLD_BENEIDENTIF');
			$LLD_BENENUMBER  		= $this->_request->getParam('LLD_BENENUMBER');
			$CITY_CODE  			= $this->_request->getParam('CITY_CODE');
			$SWIFT_CODE  			= $this->_request->getParam('SWIFT_CODE');

	//-------------------------------------------------------------START VALIDATION---------------------------------------------------------------------//
			$ACBENEF    = $filter->filter($BENEFICIARY_ACCOUNT , "ACCOUNT_NO");
			$ACBENEF_BANKNAME  = $filter->filter($ACBENEF_BANKNAME , "ACCOUNT_NAME");
			$ACBENEF_ALIAS   = $filter->filter($BENEFICIARY_ALIAS , "ACCOUNT_ALIAS");
			$ACBENEF_EMAIL   = $filter->filter($BENEFICIARY_EMAIL , "EMAIL");
			$ACBENEF_CCY   = $filter->filter($CURR_CODE  , "SELECTION");
			$ACBENEF_ADDRESS = $filter->filter($ACBENEF_ADDRESS , "ADDRESS");
			$ACBENEF_CITIZENSHIP= $filter->filter($ACBENEF_CITIZENSHIP, "SELECTION");
			$ACBENEF_RESIDENT= $filter->filter($ACBENEF_RESIDENT, "SELECTION");
			$BANK_NAME   = $filter->filter($BANK_NAME , "BANK_NAME");
			$BANK_CITY   = $filter->filter($BANK_CITY  , "ADDRESS");
			$CLR_CODE   = $filter->filter($CLR_CODE  , "BANK_CODE");

			$LLD_CATEGORY   = $filter->filter($LLD_CATEGORY  , "LLD_CATEGORY");
			$LLD_BENEIDENTIF   = $filter->filter($LLD_BENEIDENTIF  , "LLD_BENEIDENTIF");
			$LLD_BENENUMBER   = $filter->filter($LLD_BENENUMBER  , "LLD_BENENUMBER");
			$CITY_CODE   = $filter->filter($CITY_CODE  , "CITY_CODE");
			$SWIFT_CODE   = $filter->filter($SWIFT_CODE  , "SWIFT_CODE");

     		$paramBene = array("TRANSFER_TYPE"      	=>  'SKN',
								"FROM"      			=> 'B',
     							"VALIDATE"     		=> '1',
								 "ACBENEF_ID"     		=> $ACBENEF_ID,
								 "ACBENEF"     			=> $ACBENEF,
								 "ACBENEF_CCY"    		=> $ACBENEF_CCY,
								 "ACBENEF_BANKNAME" 	=> $ACBENEF_BANKNAME,
								 "ACBENEF_ALIAS"    	=> $ACBENEF_ALIAS,
								 "ACBENEF_EMAIL"   		=> $ACBENEF_EMAIL,
								 "ACBENEF_ADDRESS1"   	=> $ACBENEF_ADDRESS,
								 "ACBENEF_ADDRESS2"   	=> $ACBENEF_ADDRESS2,
								 "ACBENEF_CITIZENSHIP"  => $ACBENEF_CITIZENSHIP,
								 "ACBENEF_RESIDENT"  => $ACBENEF_RESIDENT,
								 "BANK_CODE"    	 	=> $CLR_CODE,
								 "_editBeneficiary"   	=> $this->view->hasPrivilege('BEDA'),
								 "_beneLinkage"    		=> $privibenelinkage,

     		 					 "LLD_CATEGORY"  	=> $LLD_CATEGORY,
								 "LLD_BENEIDENTIF"  	=> $LLD_BENEIDENTIF,
								 "LLD_BENENUMBER"  	=> $LLD_BENENUMBER,
								 "CITY_CODE"  	=> $CITY_CODE,
     							 "SWIFT_CODE"  	=> $SWIFT_CODE,
								);

			 $validateACBENEF = new ValidateAccountBeneficiary($ACBENEF, $this->_custIdLogin, $this->_userIdLogin);
			 $validateACBENEF->check($paramBene);

			 if (!$validateACBENEF->isError())
			{
				$content = array(
											'CUST_ID_LOGIN' 			=> $this->_custIdLogin,
											'USER_ID_LOGIN' 			=> $this->_userIdLogin,
											'BENEFICIARY_ID' 			=> $ACBENEF_ID,
											'BENEFICIARY_ALIAS' 		=> $ACBENEF_ALIAS,
											'BENEFICIARY_ACCOUNT' 		=> $ACBENEF,
											'BENEFICIARY_NAME' 			=> $ACBENEF_BANKNAME,
											'CURR_CODE' 				=> $ACBENEF_CCY,
											'BENEFICIARY_EMAIL' 		=> $ACBENEF_EMAIL,
											'BENEFICIARY_CITIZENSHIP' 	=> $ACBENEF_CITIZENSHIP,
											'BENEFICIARY_RESIDENT' 		=> $ACBENEF_RESIDENT,
											'BENEFICIARY_ADDRESS' 		=> $ACBENEF_ADDRESS,
											'BENEFICIARY_ADDRESS2' 		=> $ACBENEF_ADDRESS2,
											'BANK_NAME' 				=> $BANK_NAME,
											'CLR_CODE' 					=> $CLR_CODE,
											'BANK_CITY'			 		=> $BANK_CITY,
											'BENEFICIARY_TYPE' 			=> 2,
											'BENEFICIARY_CATEGORY'    	=> $LLD_CATEGORY,
											'BENEFICIARY_ID_TYPE'    	=> $LLD_BENEIDENTIF,
											'BENEFICIARY_ID_NUMBER'    	=> $LLD_BENENUMBER,
											'BENEFICIARY_CITY_CODE'    	=> $CITY_CODE,
											'SWIFT_CODE'    			=> $SWIFT_CODE,

									);
				try
				{

					//-----insert benef--------------
					$this->_db->beginTransaction();
					$update = $Beneficiary->update($content);

					Application_Helper_General::writeLog('BEDA','Edit Beneficiary Account '.$ACBENEF);

					$this->_db->commit();

					//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), null);
					//$this->view->success = true;
					//$msg = $this->getErrorRemark('00','Edit Beneficiary');
					//$this->view->report_msg = $msg;
					$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
					$this->_redirect('/notification/submited');
				}
				catch(Exception $e)
				{
					//rollback changes
					$this->_db->rollBack();

					//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $errorMsg);
					//Application_Log_GeneralLog::technicalLog($e);
				}
			}
			else
			{
				$this->view->isEdited = true;
				$this->view->error = true;
				$this->fillParams(null,$ACBENEF_ALIAS,$ACBENEF,$ACBENEF_BANKNAME,$ACBENEF_CCY,$ACBENEF_EMAIL,$ACBENEF_CITIZENSHIP,$ACBENEF_RESIDENT,$ACBENEF_ADDRESS,$BANK_NAME,$CLR_CODE,$BANK_CITY,$ACBENEF_ID,$LLD_CATEGORY,$LLD_BENEIDENTIF,$LLD_BENENUMBER,$CITY_CODE);
				$docErr = $this->language->_('Error').': '.$validateACBENEF->getErrorMsg();
				$validateACBENEF->__destruct();
				unset($validateACBENEF);
				$this->view->report_msg = $docErr;
				//Zend_Debug::dump($docErr);


					$this->view->BENEFICIARY_ID 	= $ACBENEF_ID;
//					$this->view->ACBENEF_ALIAS 		= $resultdata['BENEFICIARY_ALIAS'];
					$this->view->ACBENEF  			= $ACBENEF;
//					$this->view->ACBENEF_BANKNAME 	= $resultdata['BENEFICIARY_NAME'];
//					$this->view->CURR_CODE    		= $resultdata['CURR_CODE'];
					$this->view->ADDRESS    		= $ACBENEF_ADDRESS;
					$this->view->ADDRESS2    		= $ACBENEF_ADDRESS2;
					$this->view->CITIZENSHIP    	= $ACBENEF_CITIZENSHIP;
					$this->view->RESIDENT    		= $ACBENEF_RESIDENT;
//					$this->view->ACBENEF_EMAIL  	= 	 $resultdata['BENEFICIARY_EMAIL'];
					$this->view->BANK_NAME  		= $BANK_NAME;
					$this->view->CLR_CODE  			= $CLR_CODE;
//					$this->view->BANK_CITY  		= $resultdata['BANK_CIT'];

					$this->view->LLD_CATEGORY  		= $LLD_CATEGORY;
					$this->view->LLD_BENEIDENTIF  	= $LLD_BENEIDENTIF;
					$this->view->LLD_BENENUMBER  	= $LLD_BENENUMBER;
					$this->view->CITY_CODE  		= $CITY_CODE;
					$this->view->SWIFT_CODE  		= $SWIFT_CODE;
			}
		}
	}

	public function confirmAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$data = $this->_db->fetchRow(
			$this->_db->select()
			->from(array('C' => 'M_USER'))
			->where("USER_ID = ".$this->_db->quote($this->_userIdLogin))
			->limit(1)
		);

		$this->view->userId				= $data['USER_ID'];

		$sessionNamespace = new Zend_Session_Namespace('beneficiaryAccountAddEdit');
		$content = $sessionNamespace->content;
		$mode = $sessionNamespace->mode;
		$this->view->mode = $mode;
	//	echo '<pre>';
	
		if($content['TYPE'] == 'SKN'){
			
			$type = 'SKN/RTGS';
		}else{
			$type = 'ONLINE';
		}
		$this->view->type = $type;
		// print_r($content);die;
// 		$this->fillParams(null,$content['BENEFICIARY_ALIAS'],$content['BENEFICIARY_ACCOUNT'],$content['BENEFICIARY_NAME'],$content['CURR_CODE'],$content['BENEFICIARY_EMAIL'],$content['BENEFICIARY_CITIZENSHIP'],$content['BENEFICIARY_RESIDENT'],$content['BENEFICIARY_ADDRESS'],$content['BANK_NAME'],null,null,$content['BENEFICIARY_CATEGORY'],$content['BENEFICIARY_ID_TYPE'],$content['BENEFICIARY_ID_NUMBER'],$content['BENEFICIARY_CITY_CODE']);
		$this->fillParams($content['TYPE'],$content['BENEFICIARY_ALIAS'],$content['BENEFICIARY_ACCOUNT'],$content['BENEFICIARY_NAME'],$content['CURR_CODE'],$content['BENEFICIARY_EMAIL'],$content['BENEFICIARY_CITIZENSHIP'],$content['BENEFICIARY_RESIDENT'],$content['BENEFICIARY_ADDRESS'],$content['BANK_NAME'],null,null,null,$content['BENEFICIARY_TYPE'],$content['BENEFICIARY_CATEGORY'],$content['BENEFICIARY_ID_TYPE'],$content['BENEFICIARY_ID_NUMBER'],$content['BENEFICIARY_CITY_CODE'],$content['SWIFT_CODE']);


 		if($this->_request->isPost() )
		{
			if($this->_getParam('submit1')==$this->language->_('Back'))
			{
				if($mode=='Add'){
					$this->_redirect('/predefinedbeneficiary/domesticcct/addbeneficiary/isback/1');
				}elseif ($mode=='Edit'){
					$this->_redirect('/predefinedbeneficiary/domesticcct/editbeneficiary/benef_id/'.$content['BENEFICIARY_ID'].'/isback/1');
				}
			}
			else
			{
				try
				{
					//-----insert benef--------------
					$this->_db->beginTransaction();
					$Beneficiary = new Beneficiary();
					// print_r($content);die;
					$add = $Beneficiary->add($content);
					Application_Helper_General::writeLog('BADA','Add Destination Account '.$content['BENEFICIARY_ACCOUNT']);
					$this->_db->commit();
					unset($_SESSION['beneficiaryAccountAddEdit']);
					$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
					$this->_redirect('/notification/submited');
				}
				catch(Exception $e)
				{
					//rollback changes
					$this->_db->rollBack();
				}
			}
		}
	}

	public function editemailAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		if(!$this->_request->isPost())
		{
			$benef_id = $this->_getParam('benef_id');
			$benef_id = (Zend_Validate::is($benef_id,'Digits'))? $benef_id : null;
			if($benef_id)
			{
			  $resultdata = $this->_db->fetchRow(
								$this->_db->select()
									 ->from(array('M_BENEFICIARY'))
									 ->where("BENEFICIARY_ID=?", $benef_id)
							);
			  if($resultdata)
			  {
					$this->view->BENEFICIARY_ID 	= $resultdata['BENEFICIARY_ID'];
					$this->view->ACBENEF_ALIAS 		= $resultdata['BENEFICIARY_ALIAS'];
					$this->view->ACBENEF  			= $resultdata['BENEFICIARY_ACCOUNT'];
					$this->view->ACBENEF_BANKNAME 	= $resultdata['BENEFICIARY_NAME'];
					$this->view->CURR_CODE    		= $resultdata['CURR_CODE'];
					$this->view->ADDRESS    		= $resultdata['BENEFICIARY_ADDRESS'];
					$this->view->CITIZENSHIP    	= $resultdata['BENEFICIARY_CITIZENSHIP'];
					$this->view->RESIDENT    		= $resultdata['BENEFICIARY_RESIDENT'];
					$this->view->ACBENEF_EMAIL  	= $resultdata['BENEFICIARY_EMAIL'];
					$this->view->BANK_NAME  		= $resultdata['BANK_NAME'];
					$this->view->CLR_CODE  	= $resultdata['CLR_CODE'];
					$this->view->BANK_CITY  		= $resultdata['BANK_CITY'];
					// print_r($resultdata);die;
					$CITY_CODE 		= $resultdata['BENEFICIARY_CITY_CODE'];
					$LLD_CATEGORY 	= $resultdata['BENEFICIARY_CATEGORY'];

					$CITY_CODEGet = (!empty($CITY_CODE)?$CITY_CODE:'');
					$select = $this->_db->select()
							->from(array('A' => 'M_CITY'),array('*'));
					$select->where('A.CITY_CODE = ?',$CITY_CODEGet);
					$arr = $this->_db->fetchall($select);

					// 9. Create LLD string
					$settings 			= new Application_Settings();
					$LLD_array 			= array();
					$LLD_DESC_arrayCat 	= array();
					$lldTypeArr  		= $settings->getLLDDOMType();

					if (!empty($LLD_CATEGORY))
					{
						$lldCategoryArr  	= $settings->getLLDDOMCategory();
						$LLD_array["CT"] 	= $LLD_CATEGORY;
						$LLD_CATEGORY_POST  = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
					}

					$this->view->LLD_CATEGORY  		= $LLD_CATEGORY_POST;
					$this->view->LLD_BENEIDENTIF  	= $resultdata['BENEFICIARY_ID_TYPE'];
					$this->view->LLD_BENENUMBER  	= $resultdata['BENEFICIARY_ID_NUMBER'];
					$this->view->CITY_CODE  		= $arr[0]['CITY_NAME'];
			   }
			}
			else
			{
			   $error_remark = $this->getErrorRemark('22','Beneficiary ID');

			   $this->_helper->getHelper('FlashMessenger')->addMessage('F');
			   $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
			   $this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName());
			}
		}
		else
		{
			$filters = array( 	'BENEFICIARY_ID' => array('StringTrim','StripTags'),
								'ACBENEF_EMAIL' 	=> array('StringTrim','StripTags'));

			$validators = array('BENEFICIARY_ID' => array(	'NotEmpty',
															'Digits',
															'messages' => array(
																'Error: Beneficiary ID cannot be left blank.',
																'Error: Wrong Format Beneficiary ID.')
														),
								'ACBENEF_EMAIL' => array(	//new SGO_Validate_EmailAddress(),
															'allowEmpty' => true,
															//'messages' => array(
															//	'Error: Wrong Format Email Address of {$email}. Please correct it.')
														));

			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			if($zf_filter_input->isValid())
			{
				$error = false;

				if (trim($zf_filter_input->ACBENEF_EMAIL) != "")
				{
					$arr_email = explode(";",$zf_filter_input->ACBENEF_EMAIL);
					foreach ($arr_email as $value)
					{
						if (Zend_Validate::is(trim($value), 'EmailAddress') == false)
						{
							$error = true;
							break;
						}
					}
				}

				if (!$error)
				{
					try
					{
						//-----edit email--------------
						$this->_db->beginTransaction();

						$Beneficiary = new Beneficiary();
						$Beneficiary->editEmail($zf_filter_input->BENEFICIARY_ID,$zf_filter_input->ACBENEF_EMAIL);

						$beneAcct = $this->_db->fetchOne('SELECT BENEFICIARY_ACCOUNT FROM M_BENEFICIARY WHERE BENEFICIARY_ID='.$zf_filter_input->BENEFICIARY_ID);

						Application_Helper_General::writeLog('BEDA','Edit Email Beneficiary Account '.$beneAcct);

						$this->_db->commit();

						//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), null);
						//$this->view->success = true;
						//$msg = $this->getErrorRemark('00','Edit Email');
						//$this->view->report_msg = $msg;
						$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
						$this->_redirect('/notification/submited');
					}
					catch(Exception $e)
					{
						//rollback changes
						$this->_db->rollBack();
						$this->fillParam($zf_filter_input);
						$errorMsg = $this->getErrorRemark('82');
						$this->_helper->getHelper('FlashMessenger')->addMessage('F');
						$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);

						//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $errorMsg);
						//Application_Log_GeneralLog::technicalLog($e);
					}
				}
				else
				{
					$this->view->error = true;
					$resultdata = $this->_db->fetchRow(
									$this->_db->select()
										 ->from(array('M_BENEFICIARY'))
										 ->where("BENEFICIARY_ID=?", $zf_filter_input->BENEFICIARY_ID)
								);
					  if($resultdata)
					  {
							$this->view->BENEFICIARY_ID 	= $resultdata['BENEFICIARY_ID'];
							$this->view->ACBENEF_ALIAS 		= $resultdata['BENEFICIARY_ALIAS'];
							$this->view->ACBENEF  			= $resultdata['BENEFICIARY_ACCOUNT'];
							$this->view->ACBENEF_BANKNAME 	= $resultdata['BENEFICIARY_NAME'];
							$this->view->CURR_CODE    		= $resultdata['CURR_CODE'];
							$this->view->ADDRESS    		= $resultdata['BENEFICIARY_ADDRESS'];
							$this->view->CITIZENSHIP    	= $resultdata['BENEFICIARY_CITIZENSHIP'];
							$this->view->RESIDENT    		= $resultdata['BENEFICIARY_RESIDENT'];
							$this->view->BANK_NAME  		= $resultdata['BANK_NAME'];
							$this->view->CLR_CODE  	= $resultdata['CLR_CODE'];
							$this->view->BANK_CITY  		= $resultdata['BANK_CITY'];

							$CITY_CODE 		= $resultdata['BENEFICIARY_CITY_CODE'];
							$LLD_CATEGORY 	= $resultdata['BENEFICIARY_CATEGORY'];

							$CITY_CODEGet = (!empty($CITY_CODE)?$CITY_CODE:'');
							$select = $this->_db->select()
									->from(array('A' => 'M_CITY'),array('*'));
							$select->where('A.CITY_CODE = ?',$CITY_CODEGet);
							$arr = $this->_db->fetchall($select);

							// 9. Create LLD string
							$settings 			= new Application_Settings();
							$LLD_array 			= array();
							$LLD_DESC_arrayCat 	= array();
							$lldTypeArr  		= $settings->getLLDDOMType();

							if (!empty($LLD_CATEGORY))
							{
								$lldCategoryArr  	= $settings->getLLDDOMCategory();
								$LLD_array["CT"] 	= $LLD_CATEGORY;
								$LLD_CATEGORY_POST  = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
							}

							$this->view->LLD_CATEGORY  		= $LLD_CATEGORY_POST;
							$this->view->LLD_BENEIDENTIF  	= $resultdata['BENEFICIARY_ID_TYPE'];
							$this->view->LLD_BENENUMBER  	= $resultdata['BENEFICIARY_ID_NUMBER'];
							$this->view->CITY_CODE  		= $arr[0]['CITY_NAME'];
					   }
					$this->view->ACBENEF_EMAIL = ($zf_filter_input->isValid('ACBENEF_EMAIL')) ? $zf_filter_input->ACBENEF_EMAIL : $this->_getParam('ACBENEF_EMAIL');
					$docErr = 'Error: Invalid Format - Email Address.';
					$this->view->report_msg = $docErr;
					//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $docErr);
				}
			}
			else
			{
				$this->view->error = true;
				$resultdata = $this->_db->fetchRow(
								$this->_db->select()
									 ->from(array('M_BENEFICIARY'))
									 ->where("BENEFICIARY_ID=?", $zf_filter_input->BENEFICIARY_ID)
							);
				  if($resultdata)
				  {
						$this->view->BENEFICIARY_ID 	= $resultdata['BENEFICIARY_ID'];
						$this->view->ACBENEF_ALIAS 		= $resultdata['BENEFICIARY_ALIAS'];
						$this->view->ACBENEF  			= $resultdata['BENEFICIARY_ACCOUNT'];
						$this->view->ACBENEF_BANKNAME 	= $resultdata['BENEFICIARY_NAME'];
						$this->view->CURR_CODE    		= $resultdata['CURR_CODE'];
						$this->view->ADDRESS    		= $resultdata['BENEFICIARY_ADDRESS'];
						$this->view->CITIZENSHIP    	= $resultdata['BENEFICIARY_CITIZENSHIP'];
						$this->view->RESIDENT    		= $resultdata['BENEFICIARY_RESIDENT'];
						$this->view->BANK_NAME  		= $resultdata['BANK_NAME'];
						$this->view->CLR_CODE  	= $resultdata['CLR_CODE'];
						$this->view->BANK_CITY  		= $resultdata['BANK_CITY'];

						$CITY_CODE 		= $resultdata['BENEFICIARY_CITY_CODE'];
						$LLD_CATEGORY 	= $resultdata['BENEFICIARY_CATEGORY'];

						$modelCity 			= new predefinedbeneficiary_Model_Predefinedbeneficiary();
						$CITY_CODEGet 		= (!empty($CITY_CODE)?$CITY_CODE:'');
						$arr 				= $modelCity->getCityCode($CITY_CODEGet);

						// 9. Create LLD string
						$settings 			= new Application_Settings();
						$LLD_array 			= array();
						$LLD_DESC_arrayCat 	= array();
						$lldTypeArr  		= $settings->getLLDDOMType();

						if (!empty($LLD_CATEGORY))
						{
							$lldCategoryArr  	= $settings->getLLDDOMCategory();
							$LLD_array["CT"] 	= $LLD_CATEGORY;
							$LLD_CATEGORY_POST  = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
						}

						$this->view->LLD_CATEGORY  		= $LLD_CATEGORY_POST;
						$this->view->LLD_BENEIDENTIF  	= $resultdata['BENEFICIARY_ID_TYPE'];
						$this->view->LLD_BENENUMBER  	= $resultdata['BENEFICIARY_ID_NUMBER'];
						$this->view->CITY_CODE  		= $arr[0]['CITY_NAME'];
				   }
				$this->view->ACBENEF_EMAIL = ($zf_filter_input->isValid('ACBENEF_EMAIL')) ? $zf_filter_input->ACBENEF_EMAIL : $this->_getParam('ACBENEF_EMAIL');
				$docErr = $this->displayError($zf_filter_input->getMessages());
				$this->view->report_msg = $docErr;
				//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $docErr);
			}

			$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
			if(count($temp)>1){
				if($temp[0]=='F' || $temp[0]=='S'){
					if($temp[0]=='F')
						$this->view->error = 1;
					else
						$this->view->success = 1;
					$msg = ''; unset($temp[0]);
					foreach($temp as $value)
					{
						if(!is_array($value))
							$value = array($value);
						$msg .= $this->view->formErrors($value);
					}
					$this->view->report_msg = $msg;
				}
			}
		}
	}

	public function deleteAction()
	{
		// echo "<pre>";
		// print_r($this->_request->getParams());die;
		$this->_helper->layout()->setLayout('newlayout');
		if(!$this->_request->isPost())
		{
			$benef_id = $this->_getParam('benef_id');
			// print_r($benef_id);die;
			$checkmulti = explode(',', $benef_id);
			// var_dump($checkmulti);die;
			if(empty($checkmulti[1])){
				$this->view->multiview = false;
			$benef_id = (Zend_Validate::is($benef_id,'Digits'))? $benef_id : null;

			if($benef_id)
			{
			  $resultdata = $this->_db->fetchRow(
								$this->_db->select()
									 ->from(array('M_BENEFICIARY'))
									 ->where("BENEFICIARY_ID=?", $benef_id)
							);
			  if($resultdata)
			  {
			  		$this->view->BENEFICIARY_ID 	= $resultdata['BENEFICIARY_ID'];
					$this->view->ACBENEF_ALIAS 		= $resultdata['BENEFICIARY_ALIAS'];
					$this->view->ACBENEF  			= $resultdata['BENEFICIARY_ACCOUNT'];
					$this->view->ACBENEF_NAME		= $resultdata['BENEFICIARY_NAME'];
					$this->view->CURR_CODE    		= $resultdata['CURR_CODE'];
					$this->view->CITIZENSHIP    	= $resultdata['BENEFICIARY_CITIZENSHIP'];
					$this->view->RESIDENT    		= $resultdata['BENEFICIARY_RESIDENT'];
					$this->view->ADDRESS    		= $resultdata['BENEFICIARY_ADDRESS'];
					$this->view->BANK_NAME  		= $resultdata['BANK_NAME'];
					$this->view->CLR_CODE  	= $resultdata['CLR_CODE'];
					$this->view->BANK_CITY  		= $resultdata['BANK_CITY'];
					$this->view->ACBENEF_EMAIL  	= $resultdata['BENEFICIARY_EMAIL'];
					$this->view->STATUS		  		= $resultdata['BENEFICIARY_ISAPPROVE'];
					$this->view->BENEFICIARY_TYPE	= $resultdata['BENEFICIARY_TYPE'];

					$this->view->BENEFICIARY_ID_NUMBER	= $resultdata['BENEFICIARY_ID_NUMBER'];
					$this->view->BENEFICIARY_ID_TYPE	= $resultdata['BENEFICIARY_ID_TYPE'];
					$this->view->LLD_CATEGORY	= $resultdata['BENEFICIARY_CATEGORY'];
								// print_r($resultdata);die;
			 		$CITY_CODE 		= $resultdata['BENEFICIARY_CITY_CODE'];
					$LLD_CATEGORY 	= $resultdata['BENEFICIARY_CATEGORY'];

					$CITY_CODEGet = (!empty($CITY_CODE)?$CITY_CODE:'');
					$select = $this->_db->select()
							->from(array('A' => 'M_CITY'),array('*'));
					$select->where('A.CITY_CODE = ?',$CITY_CODEGet);
					$arr = $this->_db->fetchall($select);

					// 9. Create LLD string
					$settings 			= new Application_Settings();
					$LLD_array 			= array();
					$LLD_DESC_arrayCat 	= array();
					$lldTypeArr  		= $settings->getLLDDOMType();

					if (!empty($LLD_CATEGORY))
					{
						$lldCategoryArr  	= $settings->getLLDDOMCategory();
						$LLD_array["CT"] 	= $LLD_CATEGORY;
						$LLD_CATEGORY_POST  = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
					}

					$this->view->LLD_CATEGORY  		= $LLD_CATEGORY_POST;
					$this->view->LLD_BENEIDENTIF  	= $resultdata['BENEFICIARY_ID_TYPE'];
					$this->view->LLD_BENENUMBER  	= $resultdata['BENEFICIARY_ID_NUMBER'];
					$this->view->CITY_CODE  		= $arr[0]['CITY_NAME'];
			   }
			}
			else
			{
			   $error_remark = 'Error: Beneficiary ID cannot be left blank.';

			   $this->_helper->getHelper('FlashMessenger')->addMessage('F');
			   $this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
			}
		}else{
			$this->view->multiview = true;

			$fields = array(
						/*'alias'  => array('field' => 'BENEFICIARY_ALIAS',
											   'label' => $this->language->_('Alias Name'),
											   'sortable' => true),*/
						'benef_acct'  => array('field' => 'BENEFICIARY_ACCOUNT',
											   'label' => $this->language->_('Beneficiary Account'),
											   'sortable' => true),
						'benef_name'  => array('field' => 'BENEFICIARY_NAME',
											   'label' => $this->language->_('Beneficiary Account Name'),
											   'sortable' => true),
						'email'  => array('field' => 'BENEFICIARY_EMAIL',
											   'label' => $this->language->_('Email Address'),
											   'sortable' => true),
						'ccy'   => array('field'    => 'CURR_CODE',
											  'label'    => $this->language->_('CCY'),
											  'sortable' => true),
						// 'favorite'   => array('field'    => 'ISFAVORITE',
						// 					  'label'    => $this->language->_('Favorite'),
						// 					  'sortable' => true),
						// 'checked'   => array('field'    => 'BENEFICIARY_BANKSTATUS_disp',
						// 					  'label'    => $this->language->_('Checked by Bank'),
						// 					  'sortable' => true),
						
						'date'   => array('field'    => 'BENEFICIARY_CREATED',
											  'label'    => $this->language->_('Created Date'),
											  'sortable' => true),
						
						'status'   => array('field'    => 'BENEFICIARY_ISAPPROVE',
											  'label'    => $this->language->_('Status'),
											  'sortable' => true)

						// 'delete'   => array('field'    => 'BENEFICIARY_ISREQUEST_DELETE',
						// 					  'label'    => $this->language->_('Delete'),
						// 					  'sortable' => false)
				);
			$this->view->fields = $fields;

			$select = $this->_db->select()
									 ->from(array('M_BENEFICIARY'))
									 ->where("BENEFICIARY_ID in (?)", $checkmulti);
			$this->paging($select);


			$resultdata = $this->_db->fetchAll(
								$this->_db->select()
									 ->from(array('M_BENEFICIARY'))
									 ->where("BENEFICIARY_ID in (?)", $checkmulti)
							);
									 // echo $resultdata;die;

			// print_r($resultdata);die;
			  if($resultdata)
			  {
			  		$this->view->BENEFICIARY_ID 	= $resultdata['BENEFICIARY_ID'];
					$this->view->ACBENEF_ALIAS 		= $resultdata['BENEFICIARY_ALIAS'];
					$this->view->ACBENEF  			= $resultdata['BENEFICIARY_ACCOUNT'];
					$this->view->ACBENEF_NAME		= $resultdata['BENEFICIARY_NAME'];
					$this->view->CURR_CODE    		= $resultdata['CURR_CODE'];
					$this->view->CITIZENSHIP    	= $resultdata['BENEFICIARY_CITIZENSHIP'];
					$this->view->RESIDENT    		= $resultdata['BENEFICIARY_RESIDENT'];
					$this->view->ADDRESS    		= $resultdata['BENEFICIARY_ADDRESS'];
					$this->view->BANK_NAME  		= $resultdata['BANK_NAME'];
					$this->view->CLR_CODE  	= $resultdata['CLR_CODE'];
					$this->view->BANK_CITY  		= $resultdata['BANK_CITY'];
					$this->view->ACBENEF_EMAIL  	= $resultdata['BENEFICIARY_EMAIL'];
					$this->view->STATUS		  		= $resultdata['BENEFICIARY_ISAPPROVE'];

					$CITY_CODE 		= $resultdata['BENEFICIARY_CITY_CODE'];
					$LLD_CATEGORY 	= $resultdata['BENEFICIARY_CATEGORY'];

					$CITY_CODEGet = (!empty($CITY_CODE)?$CITY_CODE:'');
					$select = $this->_db->select()
							->from(array('A' => 'M_CITY'),array('*'));
					$select->where('A.CITY_CODE = ?',$CITY_CODEGet);
					$arr = $this->_db->fetchall($select);

					// 9. Create LLD string
					$settings 			= new Application_Settings();
					$LLD_array 			= array();
					$LLD_DESC_arrayCat 	= array();
					$lldTypeArr  		= $settings->getLLDDOMType();

					if (!empty($LLD_CATEGORY))
					{
						$lldCategoryArr  	= $settings->getLLDDOMCategory();
						$LLD_array["CT"] 	= $LLD_CATEGORY;
						$LLD_CATEGORY_POST  = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
					}

					$this->view->LLD_CATEGORY  		= $LLD_CATEGORY_POST;
					$this->view->LLD_BENEIDENTIF  	= $resultdata['BENEFICIARY_ID_TYPE'];
					$this->view->LLD_BENENUMBER  	= $resultdata['BENEFICIARY_ID_NUMBER'];
					$this->view->CITY_CODE  		= $arr[0]['CITY_NAME'];
			   }





		}





		}
		else
		{

		 //	echo "<pre>";
		// 	print_r($this->_getParam('BENEFICIARY_ID')[1]);
		 //var_dump($this->_getParam('BENEFICIARY_ID')[1]);die;

			if($this->_getParam('BENEFICIARY_ID')[1] == 1) {


			$filters = array(  	'BENEFICIARY_ID' => array('StringTrim','StripTags'),
								'STATUS' => array('StringTrim','StripTags'));

			$validators = array('BENEFICIARY_ID' => array(	'NotEmpty',
															'Digits',
															'messages' => array(
																'Error: Beneficiary ID cannot be left blank.',
																'Error: Wrong Format Beneficiary ID.')
														),
								'STATUS' 		=> array(	'NotEmpty',
															'Digits',
															'messages' => array(
																'Error: Status cannot be left blank.',
																'Error: Wrong Format Status.')
														));

			$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
			if($zf_filter_input->isValid())
			{
				try
				{
					
					//-----delete--------------
					$this->_db->beginTransaction();
					$Beneficiary = new Beneficiary();
					if($zf_filter_input->STATUS)
						$Beneficiary->suggestDelete($zf_filter_input->BENEFICIARY_ID);
					else
						$Beneficiary->delete($zf_filter_input->BENEFICIARY_ID);
					$this->_db->commit();

					//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), null);
					$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
					$this->_redirect('/notification/submited');
				}
				catch(Exception $e)
				{
					//rollback changes
					$this->_db->rollBack();
					$this->fillParam($zf_filter_input);
					$errorMsg = $this->getErrorRemark('82');
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);

					//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $errorMsg);
					//Application_Log_GeneralLog::technicalLog($e);
				}
			}
			else
			{

				$this->view->error = true;
				$resultdata = $this->_db->fetchRow(
								$this->_db->select()
									 ->from(array('M_BENEFICIARY'))
									 ->where("BENEFICIARY_ID=?", $zf_filter_input->BENEFICIARY_ID)
							);
				if($resultdata)
				{
					$this->view->BENEFICIARY_ID 	= $resultdata['BENEFICIARY_ID'];
					$this->view->ACBENEF_ALIAS 		= $resultdata['BENEFICIARY_ALIAS'];
					$this->view->ACBENEF  			= $resultdata['BENEFICIARY_ACCOUNT'];
					$this->view->ACBENEF_NAME		= $resultdata['BENEFICIARY_NAME'];
					$this->view->CURR_CODE    		= $resultdata['CURR_CODE'];
					$this->view->CITIZENSHIP    	= $resultdata['BENEFICIARY_CITIZENSHIP'];
					$this->view->RESIDENT    		= $resultdata['BENEFICIARY_RESIDENT'];
					$this->view->ADDRESS    		= $resultdata['BENEFICIARY_ADDRESS'];
					$this->view->BANK_NAME  		= $resultdata['BANK_NAME'];
					$this->view->CLR_CODE  	= $resultdata['CLR_CODE'];
					$this->view->BANK_CITY  		= $resultdata['BANK_CITY'];
					$this->view->ACBENEF_EMAIL  	= $resultdata['BENEFICIARY_EMAIL'];
					$this->view->STATUS		  		= $resultdata['BENEFICIARY_ISAPPROVE'];

					$CITY_CODE 		= $resultdata['BENEFICIARY_CITY_CODE'];
					$LLD_CATEGORY 	= $resultdata['BENEFICIARY_CATEGORY'];

					$CITY_CODEGet = (!empty($CITY_CODE)?$CITY_CODE:'');
					$select = $this->_db->select()
							->from(array('A' => 'M_CITY'),array('*'));
					$select->where('A.CITY_CODE = ?',$CITY_CODEGet);
					$arr = $this->_db->fetchall($select);

					// 9. Create LLD string
					$settings 			= new Application_Settings();
					$LLD_array 			= array();
					$LLD_DESC_arrayCat 	= array();
					$lldTypeArr  		= $settings->getLLDDOMType();

					if (!empty($LLD_CATEGORY))
					{
						$lldCategoryArr  	= $settings->getLLDDOMCategory();
						$LLD_array["CT"] 	= $LLD_CATEGORY;
						$LLD_CATEGORY_POST  = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
					}

					$this->view->LLD_CATEGORY  		= $LLD_CATEGORY_POST;
					$this->view->LLD_BENEIDENTIF  	= $resultdata['BENEFICIARY_ID_TYPE'];
					$this->view->LLD_BENENUMBER  	= $resultdata['BENEFICIARY_ID_NUMBER'];
					$this->view->CITY_CODE  		= $arr[0]['CITY_NAME'];
				}
				$docErr = $this->displayError($zf_filter_input->getMessages());
				$this->view->report_msg = $docErr;
				//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $docErr);
			}

		}else{
		// die('here');
			try
				{
					//-----delete--------------
					$this->_db->beginTransaction();
					$Beneficiary = new Beneficiary();

					$id = $this->_getParam('BENEFICIARY_ID');
					
					if (count($id) == 1) {
						$Beneficiary->suggestDelete($id);
					}else{
						foreach ($this->_getParam('BENEFICIARY_ID') as $key => $value) {
						
							if($this->_getParam('STATUS')[$key])
								$Beneficiary->suggestDelete($value);
							else
								$Beneficiary->delete($value);

						}
					}
					
					$this->_db->commit();

					//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), null);
					$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
					$this->_redirect('/notification/submited');
				}
				catch(Exception $e)
				{
					//rollback changes
					$this->_db->rollBack();
					$this->fillParam($zf_filter_input);
					$errorMsg = $this->getErrorRemark('82');
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);

					//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $errorMsg);
					//Application_Log_GeneralLog::technicalLog($e);
				}

		}






		}

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}
    	}
		Application_Helper_General::writeLog('BADA','Delete Beneficiary Account '.$benef_id);
	}

	private function fillParam($zf_filter_input)
	{
		if(isset($zf_filter_input->BENEFICIARY_ID))$this->view->BENEFICIARY_ID = ($zf_filter_input->isValid('BENEFICIARY_ID')) ? $zf_filter_input->BENEFICIARY_ID : $this->_getParam('BENEFICIARY_ID');
		$this->view->ACBENEF_ALIAS = ($zf_filter_input->isValid('ACBENEF_ALIAS')) ? $zf_filter_input->ACBENEF_ALIAS : $this->_getParam('ACBENEF_ALIAS');
		$this->view->ACBENEF = ($zf_filter_input->isValid('ACBENEF')) ? $zf_filter_input->ACBENEF : $this->_getParam('ACBENEF');
		$this->view->ACBENEF_BANKNAME = ($zf_filter_input->isValid('ACBENEF_BANKNAME')) ? $zf_filter_input->ACBENEF_BANKNAME : $this->_getParam('ACBENEF_BANKNAME');
		$this->view->CURR_CODE = ($zf_filter_input->isValid('CURR_CODE')) ? $zf_filter_input->CURR_CODE : $this->_getParam('CURR_CODE');
		$this->view->ADDRESS = ($zf_filter_input->isValid('ADDRESS')) ? $zf_filter_input->ADDRESS : $this->_getParam('ADDRESS');
		$this->view->CITIZENSHIP = ($zf_filter_input->isValid('CITIZENSHIP')) ? $zf_filter_input->CITIZENSHIP : $this->_getParam('CITIZENSHIP');
		$this->view->RESIDENT = ($zf_filter_input->isValid('RESIDENT')) ? $zf_filter_input->RESIDENT : $this->_getParam('RESIDENT');
		$this->view->ACBENEF_EMAIL = ($zf_filter_input->isValid('ACBENEF_EMAIL')) ? $zf_filter_input->ACBENEF_EMAIL : $this->_getParam('ACBENEF_EMAIL');
		$this->view->BANK_NAME = ($zf_filter_input->isValid('BANK_NAME')) ? $zf_filter_input->BANK_NAME : $this->_getParam('BANK_NAME');
		$this->view->CLR_CODE = ($zf_filter_input->isValid('CLR_CODE')) ? $zf_filter_input->CLR_CODE : $this->_getParam('CLR_CODE');
		$this->view->BANK_CITY = ($zf_filter_input->isValid('BANK_CITY')) ? $zf_filter_input->BANK_CITY : $this->_getParam('BANK_CITY');
	}

//	private function fillParams($benefType,$ACBENEF_ALIAS,$ACBENEF,$ACBENEF_BANKNAME,$ACBENEF_CCY,$ACBENEF_EMAIL,$ACBENEF_CITIZENSHIP,$ACBENEF_RESIDENT,$ACBENEF_ADDRESS,$BANK_NAME,$CLR_CODE,$BANK_CITY,$BENEFICIARY_ID=null,$LLD_CATEGORY,$LLD_BENEIDENTIF,$LLD_BENENUMBER,$CITY_CODE)
//	{
//		$CITY_CODEGet = (!empty($CITY_CODE)?$CITY_CODE:'');
//		$select = $this->_db->select()
//				->from(array('A' => 'M_CITY'),array('*'));
//		$select->where('A.CITY_CODE = ?',$CITY_CODEGet);
//		$arr = $this->_db->fetchall($select);
//
//		// 9. Create LLD string
//		$settings 			= new Application_Settings();
//		$LLD_array 			= array();
//		$LLD_DESC_arrayCat 	= array();
//		$lldTypeArr  		= $settings->getLLDDOMType();
//
//		if (!empty($LLD_CATEGORY))
//		{
//			$lldCategoryArr  	= $settings->getLLDDOMCategory();
//			$LLD_array["CT"] 	= $LLD_CATEGORY;
//			$LLD_CATEGORY_POST = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
//		}
//		Zend_Debug::dump($LLD_CATEGORY);
//
//		if($BENEFICIARY_ID) $this->view->BENEFICIARY_ID = $BENEFICIARY_ID;
//		$this->view->ACBENEF_ALIAS = $ACBENEF_ALIAS;
//		$this->view->ACBENEF = $ACBENEF;
//		$this->view->ACBENEF_BANKNAME = $ACBENEF_BANKNAME;
//		$this->view->CURR_CODE = $ACBENEF_CCY;
//		$this->view->ADDRESS = $ACBENEF_ADDRESS;
// 		$this->view->CITIZENSHIP = $ACBENEF_RESIDENT;
//		$this->view->RESIDENT = $ACBENEF_CITIZENSHIP;
//		$this->view->ACBENEF_EMAIL = $ACBENEF_EMAIL;
//		$this->view->BANK_NAME = $BANK_NAME;
//		$this->view->CLR_CODE = $CLR_CODE;
//		//$this->view->BANK_CITY = $BANK_CITY;
////		$this->view->BENEFICIARY_TYPE = $BENEFICIARY_TYPE;
//
//		$this->view->LLD_CATEGORY = $LLD_CATEGORY;;
//		$this->view->LLD_BENEIDENTIF = $LLD_BENEIDENTIF;
//		$this->view->LLD_BENENUMBER = $LLD_BENENUMBER;
//		$this->view->CITY_CODE = $CITY_CODE;

//
//		$this->view->CITY_NAME = $arr[0]['CITY_NAME'];
//		$this->view->CATEGORY_NAME_LLD = $LLD_CATEGORY_POST;
//	}

	private function fillParams($benefType,$ACBENEF_ALIAS,$ACBENEF,$ACBENEF_BANKNAME,$ACBENEF_CCY,$ACBENEF_EMAIL,$ACBENEF_CITIZENSHIP,$ACBENEF_RESIDENT,$ACBENEF_ADDRESS,$BANK_NAME,$CLR_CODE,$BANK_CITY,$BENEFICIARY_ID=null,$BENEFICIARY_TYPE,$LLD_CATEGORY,$LLD_BENEIDENTIF,$LLD_BENENUMBER,$CITY_CODE = null)
//	private function fillParams($benefType,$ACBENEF_ALIAS,$ACBENEF,$ACBENEF_BANKNAME,$ACBENEF_CCY,$ACBENEF_EMAIL,$ACBENEF_CITIZENSHIP,$ACBENEF_RESIDENT,$ACBENEF_ADDRESS,$BANK_NAME,$CLR_CODE,$BANK_CITY,$BENEFICIARY_ID=null,$LLD_CATEGORY,$LLD_BENEIDENTIF,$LLD_BENENUMBER,$CITY_CODE)

	{
		$CITY_CODEGet = (!empty($CITY_CODE)?$CITY_CODE:'');
		if(!empty($CITY_CODEGet)){
		$select = $this->_db->select()
				->from(array('A' => 'M_CITY'),array('*'));
		$select->where('A.CITY_CODE = ?',$CITY_CODEGet);
		$arr = $this->_db->fetchall($select);
		
		$this->view->CITY_NAME = $arr[0]['CITY_NAME'];
		}
		// 9. Create LLD string
		$settings 			= new Application_Settings();
		$LLD_array 			= array();
		$LLD_DESC_arrayCat 	= array();
		$lldTypeArr  		= $settings->getLLDDOMType();

		if (!empty($LLD_CATEGORY))
		{
			$lldCategoryArr  	= $settings->getLLDDOMCategory();
			$LLD_array["CT"] 	= $LLD_CATEGORY;
			$LLD_CATEGORY_POST = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
		}

		if (!empty($LLD_BENEIDENTIF))
		{
			$lldBeneIdentifArr  	= $settings->getLLDDOMBeneIdentification();
			$LLD_array["CT"] 	= $LLD_BENEIDENTIF;
			$LLD_BENEIDENTIF_POST = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldBeneIdentifArr[$LLD_BENEIDENTIF];
		}
		
		
		if($benefType == 'SKN'){
			
			$benefType = 'SKN/RTGS';
		}else{
			$benefType = 'ONLINE';
		}

		if($BENEFICIARY_ID)
		$this->view->BENEFICIARY_ID = $BENEFICIARY_ID;
		$this->view->benefType = $benefType;
		$this->view->ACBENEF_ALIAS = $ACBENEF_ALIAS;
		$this->view->ACBENEF = $ACBENEF;
		$this->view->ACBENEF_BANKNAME = $ACBENEF_BANKNAME;
		$this->view->CURR_CODE = $ACBENEF_CCY;
		$this->view->ADDRESS = $ACBENEF_ADDRESS;
		$this->view->CITIZENSHIP = $ACBENEF_RESIDENT;
		$this->view->RESIDENT = $ACBENEF_CITIZENSHIP;
		$this->view->ACBENEF_EMAIL = $ACBENEF_EMAIL;
		$this->view->BANK_NAME = $BANK_NAME;
		$this->view->CLR_CODE = $CLR_CODE;
		$this->view->BANK_CITY = $BANK_CITY;
		$this->view->BENEFICIARY_TYPE = $BENEFICIARY_TYPE;
		$this->view->LLD_CATEGORY = $LLD_CATEGORY;;
		$this->view->LLD_BENEIDENTIF = $LLD_BENEIDENTIF;
		$this->view->LLD_BENENUMBER = $LLD_BENENUMBER;
		$this->view->CITY_CODE = $CITY_CODE;
		
		$this->view->CATEGORY_NAME_LLD = $LLD_CATEGORY_POST;
		$this->view->BENEIDENTIF_NAME_LLD = $LLD_BENEIDENTIF_POST;
	}

	public function deletebenefAction(){
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        
        $tblName = $this->_getParam('id');
        //$data = array('sukses' => $tblName);
		
		
			try
				{
					
					//-----delete--------------
					$this->_db->beginTransaction();
					$Beneficiary = new Beneficiary();
					//if($zf_filter_input->STATUS)
						$Beneficiary->suggestDelete($tblName);
					//else
					//	$Beneficiary->delete($zf_filter_input->BENEFICIARY_ID);
				
					$this->_db->commit();

					//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), null);
		//			$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
		//			$this->_redirect('/notification/submited');
					echo true;
				}
				catch(Exception $e)
				{
					//rollback changes
					$this->_db->rollBack();
					echo false;
				}
	}
	
	public function addbenefAction(){
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
					
					$benefType = $this->_getParam('types_value');
					$ACBENEF_ALIAS 		= '';
					$ACBENEF  			= $this->_getParam('acbenef');
					$ACBENEF_BANKNAME 	= $this->_getParam('acbenef_name');
					$ACBENEF_CCY    		= 'IDR';
					$ACBENEF_ADDRESS    		= $this->_getParam('benef_address');
					$ACBENEF_ADDRESS2    		= $this->_getParam('benef_address2');
					$ACBENEF_CITIZENSHIP    	= $this->_getParam('citizen');
					$ACBENEF_RESIDENT    		= $this->_getParam('nation');
					$ACBENEF_EMAIL  	= 	 $this->_getParam('benef_email');
					$BANK_NAME  		= $this->_getParam('bank_name');
					$BENEFICIARY_BANK_CODE = $this->_getParam('benef_bank_code');
					$CLR_CODE  			= $this->_getParam('clr_code');
					$BANK_CITY  		= $this->_getParam('benef_city');
					$SWIFT_CODE  		= $this->_getParam('swift_code');
					$LLD_CATEGORY  		= $this->_getParam('category');
					$LLD_BENEIDENTIF  	= $this->_getParam('beneidentif');
					$LLD_BENENUMBER  	= $this->_getParam('benef_lldnumb');
					$CITY_CODE  		= '-';
					$BENEF_CITY  		= $this->_getParam('benef_city');
					$BENEFICIARY_PHONE = $this->_getParam('benef_phone');
		
		
					$privibenelinkage = $this->view->hasPrivilege('BLBU');
					
					
					$acctStatus = 1;
					$dataAcct = $this->_db->fetchRow(
						$this->_db->select()
						->from(array('C' => 'M_CUSTOMER_ACCT'))
						->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin))
						->where("ACCT_STATUS = ?",$acctStatus)
						->limit(1)
					);
					$ACCT_NO = (!empty($dataAcct['ACCT_NO'])?$dataAcct['ACCT_NO']:'');
					$ACCT_TYPE = (!empty($dataAcct['ACCT_TYPE'])?$dataAcct['ACCT_TYPE']:'');
					
					$paramBene = array( 
													"FROM"      			=> 'B',
													 "ACBENEF"     			=> $ACBENEF,
													 "ACBENEF_CCY"    		=> 'IDR',
													 "ACBENEF_ALIAS"    	=> $ACBENEF_ALIAS,
													 "ACBENEF_EMAIL"   		=> $ACBENEF_EMAIL,
													 "BANK_CODE"    	 	=> $BENEFICIARY_BANK_CODE,
													 "_addBeneficiary"   	=> $this->view->hasPrivilege('BADA'),
													 "_beneLinkage"    		=> $privibenelinkage,
													 "ACCTSRC"     			=> $ACCT_NO, //source account m_user yg login
													 "VALIDATE"     		=> '1',
													 "sourceAccountType"    => $ACCT_TYPE,
												);
					if($benefType == 1){
						$paramAdd = array("TRANSFER_TYPE"      	=>  'SKN',
															 "ACBENEF_BANKNAME" 	=> $ACBENEF_BANKNAME,
															 "ACBENEF_ADDRESS1"   	=> $ACBENEF_ADDRESS,
															 "ACBENEF_ADDRESS2"   	=> $ACBENEF_ADDRESS2,
															 "ACBENEF_CITIZENSHIP"  => $ACBENEF_CITIZENSHIP,
															 "ACBENEF_RESIDENT"  	=> $ACBENEF_RESIDENT,

															 "LLD_CATEGORY"  	=> $LLD_CATEGORY,
															 "LLD_BENEIDENTIF"  	=> $LLD_BENEIDENTIF,
															 "LLD_BENENUMBER"  	=> $LLD_BENENUMBER,
															 "CITY_CODE"  	=> $CITY_CODE,
															);
					}else{
						$paramAdd = array("TRANSFER_TYPE"      	=>  'ONLINE',
															 "ACBENEF_BANKNAME" 	=> '',
															 "ACBENEF_ADDRESS1"   	=> '',
															 "ACBENEF_CITIZENSHIP"  => '',
															 "ACBENEF_RESIDENT"  	=> '',

															"LLD_CATEGORY"  	=> '',
															 "LLD_BENEIDENTIF"  	=> '',
															 "LLD_BENENUMBER"  	=> '',
															 "CITY_CODE"  	=> '',
															);
					}


					$paramBene += array_merge($paramBene,$paramAdd);
					
					 $validateACBENEF = new ValidateAccountBeneficiary($ACBENEF, $this->_custIdLogin, $this->_userIdLogin);
					 $checkedParams = $validateACBENEF->check($paramBene);

 
					 if (!$validateACBENEF->isError())
					 {
					
									$content = array(
										'CUST_ID_LOGIN' 			=> $this->_custIdLogin,
										'USER_ID_LOGIN' 			=> $this->_userIdLogin,
										'BENEFICIARY_ALIAS' 		=> $ACBENEF_ALIAS,
										'BENEFICIARY_ACCOUNT' 		=> $ACBENEF,
										'BENEFICIARY_NAME' 			=> $ACBENEF_BANKNAME,
										'CURR_CODE' 				=> $ACBENEF_CCY,
										'BENEFICIARY_EMAIL' 		=> $ACBENEF_EMAIL,
										'BENEFICIARY_CITIZENSHIP' 	=> $ACBENEF_RESIDENT,
										'BENEFICIARY_RESIDENT' 		=> $ACBENEF_CITIZENSHIP,
										'BENEFICIARY_ADDRESS' 		=> $ACBENEF_ADDRESS,
										'BENEFICIARY_ADDRESS2' 		=> $ACBENEF_ADDRESS2,
										'BANK_NAME' 				=> $BANK_NAME,
										'CLR_CODE' 					=> $CLR_CODE,
										'BANK_CODE' 				=> $BENEFICIARY_BANK_CODE,
										'SWIFT_CODE'    			=> $SWIFT_CODE,
										'BENEFICIARY_CITY'			=> $BENEF_CITY, 
										'BENEFICIARY_TYPE' 			=> 2,
										'_beneLinkage'    			=> $privibenelinkage, 
										'BENEFICIARY_PHONE'			=> $BENEFICIARY_PHONE,
										'BENEFICIARY_CATEGORY'    	=> $LLD_CATEGORY,
										'BENEFICIARY_ID_TYPE'    	=> $LLD_BENEIDENTIF,
										'BENEFICIARY_ID_NUMBER'    	=> $LLD_BENENUMBER,
										'BENEFICIARY_CITY_CODE'    	=> $CITY_CODE
										
									);
									
									if($benefType != 1){
										unset($content['BENEFICIARY_CITIZENSHIP']);
										unset($content['BENEFICIARY_RESIDENT']);
										unset($content['BENEFICIARY_ADDRESS']);
										unset($content['BENEFICIARY_ADDRESS2']);
										
										unset($content['BENEFICIARY_CATEGORY']);
										unset($content['BENEFICIARY_ID_TYPE']);
										unset($content['BENEFICIARY_ID_NUMBER']);
										unset($content['BENEFICIARY_CITY_CODE']);
										unset($content['BENEFICIARY_CITY']);
	
									}
										
		
		
					try {

					
						if($benefType == '1'){
							$content['TYPE'] = 'SKN'; // 2 = domestic ( SKN/RTGS )

							$content['BENEFICIARY_TYPE'] = 2; // 2 = domestic ( SKN/RTGS )
							$content['CLR_CODE'] = $CLR_CODE;
							
							
						}
						else if($benefType == '2'){
							$content['TYPE'] = 'ONLINE'; // 2 = domestic ( SKN/RTGS )

							$content['BENEFICIARY_NAME'] = $checkedParams['ACBENEF_BANKNAME'];
							$content['BENEFICIARY_TYPE'] = 8; // 8 = online
							
							$content['CLR_CODE'] = $CLR_CODE;
							
						}
						
						 
						          
				
							//-----insert benef--------------
							//$this->_db->beginTransaction();
							//$Beneficiary = new Beneficiary();
							// print_r($content);die;
							
							//$add = $Beneficiary->add($content);
							//Application_Helper_General::writeLog('BADA','Add Destination Account '.$content['BENEFICIARY_ACCOUNT']);
							//$this->_db->commit();
							//echo true;
			
						
						
						
					}
					catch(Exception $e)
					{
						
						//rollback changes
						$this->_db->rollBack();
						echo false;

					}
					echo json_encode($content);die;
					   
					}else{
					$docErr = $validateACBENEF->getErrorMsg();
						echo json_encode($docErr);die;
						echo false;
					}
		
		
	}
	
	
	
	public function addbenefconfirmAction(){
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
					 
					$benefType = $this->_getParam('types_value');
					$ACBENEF_ALIAS 		= '';
					$ACBENEF  			= $this->_getParam('acbenef');
					$ACBENEF_BANKNAME 	= $this->_getParam('acbenef_name');
					$ACBENEF_CCY    		= 'IDR';
					$ACBENEF_ADDRESS    		= $this->_getParam('benef_address');
					$ACBENEF_ADDRESS2    		= $this->_getParam('benef_address2');
					$ACBENEF_CITIZENSHIP    	= $this->_getParam('citizen');
					$ACBENEF_RESIDENT    		= $this->_getParam('nation');
					$ACBENEF_EMAIL  	= 	 $this->_getParam('benef_email');
					$BANK_NAME  		= $this->_getParam('bank_name');
					$BENEFICIARY_BANK_CODE = $this->_getParam('benef_bank_code');
					$CLR_CODE  			= $this->_getParam('clr_code');
					$BANK_CITY  		= $this->_getParam('benef_city');
					$SWIFT_CODE  		= $this->_getParam('swift_code');
					$LLD_CATEGORY  		= $this->_getParam('category');
					$LLD_BENEIDENTIF  	= $this->_getParam('beneidentif');
					$LLD_BENENUMBER  	= $this->_getParam('benef_lldnumb');
					$CITY_CODE  		= '-';
					$BENEF_CITY  		= $this->_getParam('benef_city');
					$BENEFICIARY_PHONE = $this->_getParam('benef_phone');
		
		
					$privibenelinkage = $this->view->hasPrivilege('BLBU');
					
					
					$acctStatus = 1;
					$dataAcct = $this->_db->fetchRow(
						$this->_db->select()
						->from(array('C' => 'M_CUSTOMER_ACCT'))
						->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin))
						->where("ACCT_STATUS = ?",$acctStatus)
						->limit(1)
					);
					$ACCT_NO = (!empty($dataAcct['ACCT_NO'])?$dataAcct['ACCT_NO']:'');
					$ACCT_TYPE = (!empty($dataAcct['ACCT_TYPE'])?$dataAcct['ACCT_TYPE']:'');
					
					$paramBene = array( 
													"FROM"      			=> 'B',
													 "ACBENEF"     			=> $ACBENEF,
													 "ACBENEF_CCY"    		=> 'IDR',
													 "ACBENEF_ALIAS"    	=> $ACBENEF_ALIAS,
													 "ACBENEF_EMAIL"   		=> $ACBENEF_EMAIL,
													 "BANK_CODE"    	 	=> $BENEFICIARY_BANK_CODE,
													 "_addBeneficiary"   	=> $this->view->hasPrivilege('BADA'),
													 "_beneLinkage"    		=> $privibenelinkage,
													 "ACCTSRC"     			=> $ACCT_NO, //source account m_user yg login
													 "VALIDATE"     		=> '1',
													 "sourceAccountType"    => $ACCT_TYPE,
												);
					if($benefType == 1){
						$paramAdd = array("TRANSFER_TYPE"      	=>  'SKN',
															 "ACBENEF_BANKNAME" 	=> $ACBENEF_BANKNAME,
															 "ACBENEF_ADDRESS1"   	=> $ACBENEF_ADDRESS,
															 "ACBENEF_ADDRESS2"   	=> $ACBENEF_ADDRESS2,
															 "ACBENEF_CITIZENSHIP"  => $ACBENEF_CITIZENSHIP,
															 "ACBENEF_RESIDENT"  	=> $ACBENEF_RESIDENT,

															 "LLD_CATEGORY"  	=> $LLD_CATEGORY,
															 "LLD_BENEIDENTIF"  	=> $LLD_BENEIDENTIF,
															 "LLD_BENENUMBER"  	=> $LLD_BENENUMBER,
															 "CITY_CODE"  	=> $CITY_CODE,
															);
					}else{
						$paramAdd = array("TRANSFER_TYPE"      	=>  'ONLINE',
															 "ACBENEF_BANKNAME" 	=> '',
															 "ACBENEF_ADDRESS1"   	=> '',
															 "ACBENEF_CITIZENSHIP"  => '',
															 "ACBENEF_RESIDENT"  	=> '',

															"LLD_CATEGORY"  	=> '',
															 "LLD_BENEIDENTIF"  	=> '',
															 "LLD_BENENUMBER"  	=> '',
															 "CITY_CODE"  	=> '',
															);
					}


					$paramBene += array_merge($paramBene,$paramAdd);
					
					
					
									$content = array(
										'CUST_ID_LOGIN' 			=> $this->_custIdLogin,
										'USER_ID_LOGIN' 			=> $this->_userIdLogin,
										'BENEFICIARY_ALIAS' 		=> $ACBENEF_ALIAS,
										'BENEFICIARY_ACCOUNT' 		=> $ACBENEF,
										'BENEFICIARY_NAME' 			=> $ACBENEF_BANKNAME,
										'CURR_CODE' 				=> $ACBENEF_CCY,
										'BENEFICIARY_EMAIL' 		=> $ACBENEF_EMAIL,
										'BENEFICIARY_CITIZENSHIP' 	=> $ACBENEF_RESIDENT,
										'BENEFICIARY_RESIDENT' 		=> $ACBENEF_CITIZENSHIP,
										'BENEFICIARY_ADDRESS' 		=> $ACBENEF_ADDRESS,
										'BENEFICIARY_ADDRESS2' 		=> $ACBENEF_ADDRESS2,
										'BANK_NAME' 				=> $BANK_NAME,
										'CLR_CODE' 					=> $CLR_CODE,
										'BANK_CODE' 				=> $BENEFICIARY_BANK_CODE,
										'SWIFT_CODE'    			=> $SWIFT_CODE,
										'BENEFICIARY_CITY'			=> $BENEF_CITY,
										'BENEFICIARY_TYPE' 			=> 2,
										'_beneLinkage'    			=> $privibenelinkage,
										'BENEFICIARY_PHONE'			=> $BENEFICIARY_PHONE,
										'BENEFICIARY_CATEGORY'    	=> $LLD_CATEGORY,
										'BENEFICIARY_ID_TYPE'    	=> $LLD_BENEIDENTIF,
										'BENEFICIARY_ID_NUMBER'    	=> $LLD_BENENUMBER,
										'BENEFICIARY_CITY_CODE'    	=> $CITY_CODE
										
									);
									
									if($benefType != 1){
										unset($content['BENEFICIARY_CITIZENSHIP']);
										unset($content['BENEFICIARY_RESIDENT']);
										unset($content['BENEFICIARY_ADDRESS']);
										unset($content['BENEFICIARY_ADDRESS2']);
										
										unset($content['BENEFICIARY_CATEGORY']);
										unset($content['BENEFICIARY_ID_TYPE']);
										unset($content['BENEFICIARY_ID_NUMBER']);
										unset($content['BENEFICIARY_CITY_CODE']);
										unset($content['BENEFICIARY_CITY']);
	
									}
										
		
		
					try {

					
						if($benefType == '1'){
							$content['TYPE'] = 'SKN'; // 2 = domestic ( SKN/RTGS )

							$content['BENEFICIARY_TYPE'] = 2; // 2 = domestic ( SKN/RTGS )
							$content['CLR_CODE'] = $CLR_CODE;
							
							
						}
						else if($benefType == '2'){
							$content['TYPE'] = 'ONLINE'; // 2 = domestic ( SKN/RTGS )

							$content['BENEFICIARY_NAME'] = $content['BENEFICIARY_NAME'];
							$content['BENEFICIARY_TYPE'] = 8; // 8 = online
							
							$content['CLR_CODE'] = $CLR_CODE;
							
						}
						
						
						
				
							//-----insert benef--------------
							$this->_db->beginTransaction();
							$Beneficiary = new Beneficiary();
							// print_r($content);die;
							
							$add = $Beneficiary->add($content);
							Application_Helper_General::writeLog('BADA','Add Destination Account '.$content['BENEFICIARY_ACCOUNT']);
							$this->_db->commit();
							echo true;
			
						
						
						
					}
					catch(Exception $e)
					{
						
						//rollback changes
						$this->_db->rollBack();
						echo false;

					}
					
					
		
		
	}
	
	
	
	public function updatebenefAction(){
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
        $Beneficiary = new Beneficiary();
        $tblName = $this->_getParam('id');
        //$data = array('sukses' => $tblName);
		
		
		$resultdata = $this->_db->fetchRow(
								$this->_db->select()
									 ->from(array('M_BENEFICIARY'))
									 ->where("BENEFICIARY_ID=?", $tblName)
							);
			  if($resultdata)
			  {
					
					$ACBENEF_ALIAS 		= $resultdata['BENEFICIARY_ALIAS'];
					$ACBENEF  			= $resultdata['BENEFICIARY_ACCOUNT'];
					$ACBENEF_BANKNAME 	= $resultdata['BENEFICIARY_NAME'];
					$ACBENEF_CCY    		= $resultdata['CURR_CODE'];
					//$this->view->ADDRESS    		= $resultdata['BENEFICIARY_ADDRESS'];
					//$this->view->ADDRESS2    		= $resultdata['BENEFICIARY_ADDRESS2'];
					$ACBENEF_CITIZENSHIP    	= $resultdata['BENEFICIARY_CITIZENSHIP'];
					$ACBENEF_RESIDENT    		= $resultdata['BENEFICIARY_RESIDENT'];
					//$this->view->ACBENEF_EMAIL  	= 	 $resultdata['BENEFICIARY_EMAIL'];
					$BANK_NAME  		= $resultdata['BANK_NAME'];
					$CLR_CODE  			= $resultdata['CLR_CODE'];
					$BANK_CITY  		= $resultdata['BANK_CITY'];
					$SWIFT_CODE  		= $resultdata['SWIFT_CODE'];
					$LLD_CATEGORY  		= $resultdata['BENEFICIARY_CATEGORY'];
					$LLD_BENEIDENTIF  	= $resultdata['BENEFICIARY_ID_TYPE'];
					$LLD_BENENUMBER  	= $resultdata['BENEFICIARY_ID_NUMBER'];
					$CITY_CODE  		= $resultdata['BENEFICIARY_CITY_CODE'];
			   }else{
				   return false;
			   }
		
		
		$ACBENEF_ADDRESS = $this->_getParam('address');
		$ACBENEF_ADDRESS2 = $this->_getParam('address2');
		$BENEFICIARY_CITY = $this->_getParam('benef_city');
		$BENEFICIARY_PHONE = $this->_getParam('phone');
		$ACBENEF_EMAIL = $this->_getParam('email');
		
		
		$content = array(
											'CUST_ID_LOGIN' 			=> $this->_custIdLogin,
											'USER_ID_LOGIN' 			=> $this->_userIdLogin,
											'BENEFICIARY_ID' 			=> $tblName,
											'BENEFICIARY_ALIAS' 		=> $ACBENEF_ALIAS,
											'BENEFICIARY_ACCOUNT' 		=> $ACBENEF,
											'BENEFICIARY_NAME' 			=> $ACBENEF_BANKNAME,
											'CURR_CODE' 				=> $ACBENEF_CCY,
											'BENEFICIARY_EMAIL' 		=> $ACBENEF_EMAIL,
											'BENEFICIARY_CITIZENSHIP' 	=> $ACBENEF_CITIZENSHIP,
											'BENEFICIARY_RESIDENT' 		=> $ACBENEF_RESIDENT,
											'BENEFICIARY_ADDRESS' 		=> $ACBENEF_ADDRESS,
											'BENEFICIARY_ADDRESS2' 		=> $ACBENEF_ADDRESS2,
											'BANK_NAME' 				=> $BANK_NAME,
											'CLR_CODE' 					=> $CLR_CODE,
											'BANK_CITY'			 		=> $BANK_CITY,
											'BENEFICIARY_TYPE' 			=> 2,
											'BENEFICIARY_CATEGORY'    	=> $LLD_CATEGORY,
											'BENEFICIARY_ID_TYPE'    	=> $LLD_BENEIDENTIF,
											'BENEFICIARY_ID_NUMBER'    	=> $LLD_BENENUMBER,
											'BENEFICIARY_CITY_CODE'    	=> $CITY_CODE,
											'BENEFICIARY_CITY'			=> $BENEFICIARY_CITY,
											'BENEFICIARY_PHONE'			=> $BENEFICIARY_PHONE,
											'SWIFT_CODE'    			=> $SWIFT_CODE,

									);
									
			try
				{

					//-----insert benef--------------
					$this->_db->beginTransaction();
					$update = $Beneficiary->update($content);

					Application_Helper_General::writeLog('BEDA','Edit Beneficiary Account '.$ACBENEF);

					$this->_db->commit();

					//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), null);
					//$this->view->success = true;
					//$msg = $this->getErrorRemark('00','Edit Beneficiary');
					//$this->view->report_msg = $msg;
					//$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
					//$this->_redirect('/notification/submited');
					echo true;
				}
				catch(Exception $e)
				{
					//rollback changes
					$this->_db->rollBack();

					//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $errorMsg);
					//Application_Log_GeneralLog::technicalLog($e);
					echo false;
				}
		
			
	}
	
	
	
	public function importbenefAction()
	{
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
		$destinationUploadDir = UPLOAD_PATH . '/document/temp/';
		
		$sessionNamespace = new Zend_Session_Namespace('import_predefbenef');
		//$data = $_SESSION['import_predefbenef'];	
		$data = $sessionNamespace->content;
		
		$confirmimport = $this->_request->getParam('confirmimport');
		if($confirmimport){
			$Beneficiary = new Beneficiary();
			$this->_db->beginTransaction();
			try 
			{
			
			foreach($data['acct'] as $content)
			{
				if(empty($content['ERROR_MESSAGE'])){
					
					$add = $Beneficiary->add($content);
					
				}
				
			}
			$this->_db->commit();
				unset($_SESSION['import_predefbenef']); 
				$this->setbackURL('/'.$this->_request->getModuleName().'/importbeneficiarydom/index/');
				$this->_redirect('/notification/success');
				Application_Helper_General::writeLog('BIMA','Submit Import Beneficiary');
			}catch(Exception $e) 
			{
				//rollback changes
				$this->_db->rollBack();
				
				$errorMsg = $this->getErrorRemark('82');
				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);
				
				//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $errorMsg);
				//Application_Log_GeneralLog::technicalLog($e);
			}
			
		}
		if($this->_request->isPost() )
		{
 
			// Set variables needed in view
			$settings = new Application_Settings();
			$ccyList  = $settings->setCurrencyRegistered();
			$privibenelinkage = $this->view->hasPrivilege('BLBU');


			$adapter = new Zend_File_Transfer_Adapter_Http ();
			$adapter->setDestination ( $destinationUploadDir );
			$extensionValidator = new Zend_Validate_File_Extension(array(false, 'txt'));
			$extensionValidator->setMessage(
				//$language->_('Error: Extension file must be').' *.csv'
				$this->language->_('Error: Extension file must be').'*.txt'
			);

			$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
			$sizeValidator->setMessage(
				$this->language->_('Error: File size must not more than').' '.$this->getSetting('Fe_attachment_maxbyte')
			);

			$adapter->setValidators ( array (
				$extensionValidator,
				$sizeValidator,
			));

			//ambil data m_user ->ambil sourceAccount, acctType, statusnya aktif=1
			$acctStatus = 1;
			$dataAcct = $this->_db->fetchRow(
				$this->_db->select()
				->from(array('C' => 'M_CUSTOMER_ACCT'))
				->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin))
				->where("ACCT_STATUS = ?",$acctStatus)
				->limit(1)
			);
			$ACCT_NO = (!empty($dataAcct['ACCT_NO'])?$dataAcct['ACCT_NO']:'');
			$ACCT_TYPE = (!empty($dataAcct['ACCT_TYPE'])?$dataAcct['ACCT_TYPE']:'');
			
			if ($adapter->isValid ())
			{
//die('here');
				$sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
				$newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

				$adapter->addFilter ( 'Rename',$newFileName  );
				
				$success = 0;
				$adapter->receive();
				$myFile = file_get_contents($newFileName);
				$arry_myFile = explode("\n", $myFile);
				@unlink($newFileName);
				
				
				if ($adapter->receive ())
				{
					unset($arry_myFile[0]);
					unset($arry_myFile[1]);
					//echo '<pre>';
					//var_dump($arry_myFile);
					
					$totalRecords = count($arry_myFile);
					
					if($totalRecords)
					{
					
					$maxTotalRows 	= $this->getSetting('max_import_bene');

					if($totalRecords <= $maxTotalRows)
					{
							
						$rowNum = 0;
						$totalSucces = 0;
						$totalFailed = 0;
						$errmsg = array();	
					
					foreach ($arry_myFile as $row){ 
								
								$columns = explode("|", $row);
								echo '<pre>';
								var_dump(count($columns));
								if(count($columns) == 16){
									
									$dataBank = $this->_db->fetchRow(
											$this->_db->select()
											->from(array('C' => 'M_BANK_TABLE'))
											->where("CLR_CODE = ?",$columns[1])
											->limit(1)
										);
									
									$CLR_CODE = (!empty($dataBank['CLR_CODE'])?TRUE:FALSE);
									$CLR_CODE_ORI = (!empty($dataBank['CLR_CODE'])?$dataBank['CLR_CODE']:'');
									$BANK_NAME = (!empty($dataBank['BANK_NAME'])?$dataBank['BANK_NAME']:'');
									$BANK_CODE_COL = (!empty($dataBank['BANK_CODE'])?TRUE:FALSE);
									$SWIFT_CODE = (!empty($dataBank['SWIFT_CODE'])?TRUE:FALSE);
									//$CLR_CODE = $dataBank[]
									
									$BENEFICIARY_TYPE_COL = 2;
									$BENEFICIARY_ACCOUNT_COL = strtoupper(trim($columns[2]));
									$BENEFICIARY_CCY = strtoupper(trim($columns[3]));
									$BENEFICIARY_ACCOUNT_NAME_COL = trim($columns[4]);
									$EMAIL_ADDRESS_COL = trim($columns[5]);
									$PHONE_COL = trim($columns[6]);
									$ADDRESS_COL = trim($columns[7]);
									$ADDRESS2_COL = trim($columns[8]);
									
									$CITY_COL = trim($columns[9]);
									
									$CATEGORY_COL = trim($columns[10]);
									
									
									$CITIZENSHIP_COL = trim($columns[11]);
									$NATIONALITY_COL = trim($columns[12]);
									
									$IDENTIFICATION_TYPE_COL = trim($columns[13]);
									$IDENTIFICATION_NUMBER_COL = trim($columns[14]);
									
									
									$filter = new Application_Filtering();
						//-------------------------------------------------------------START VALIDATION---------------------------------------------------------------------//
									$BENEFICIARY_TYPE = $filter->filter($BENEFICIARY_TYPE_COL  , "SELECTION");
									$BENEFICIARY_ACCOUNT = $filter->filter($BENEFICIARY_ACCOUNT_COL , "SELECTION");
									$CITIZENSHIP = $filter->filter($CITIZENSHIP_COL  , "SELECTION");
									$NATIONALITY = $filter->filter($NATIONALITY_COL  , "SELECTION");
									$CATEGORY = $filter->filter($CATEGORY_COL  , "SELECTION");
									$IDENTIFICATION_TYPE = $filter->filter($IDENTIFICATION_TYPE_COL  , "IDENTIFICATION_TYPE_COL");
									$IDENTIFICATION_NUMBER = $filter->filter($IDENTIFICATION_NUMBER_COL  , "IDENTIFICATION_NUMBER_");
									$ADDRESS = $filter->filter($ADDRESS_COL  , "SELECTION");
									$ADDRESS2 = $filter->filter($ADDRESS2_COL  , "SELECTION");
									$BENEFICIARY_ACCOUNT_NAME = $filter->filter($BENEFICIARY_ACCOUNT_NAME_COL  , "BENEFICIARY_ACCOUNT_NAME_COL");
									$BANK_CODE = $filter->filter($BANK_CODE_COL  , "BANK_CODE_COL");
									$CITY_CODE = $filter->filter($CITY_CODE_COL  , "CITY_CODE_COL"); //jadi input city name
									$EMAIL_ADDRESS = $filter->filter($EMAIL_ADDRESS_COL  , "EMAIL");
									$PHONE = $filter->filter($PHONE_COL  , "PHONE");
									//$BANK_CODE_COL = trim($columns[9]);
									//$CITY_CODE_COL = trim($columns[10]);
									
									
										///////////////////////////
										if($CATEGORY == 'COMPANY'){
											$CATEGORY_CODE = '5';
										}
										elseif($CATEGORY == 'INDIVIDUAL'){
											$CATEGORY_CODE = '1';
										}
										elseif($CATEGORY == 'GOVERNMENT'){
											$CATEGORY_CODE = '2';
										}
										elseif($CATEGORY == 'BANK'){
											$CATEGORY_CODE = '3';
										}
										elseif($CATEGORY == 'NON BANK FINANCIAL INSTITUTION'){
											$CATEGORY_CODE = '4';
										}
										elseif($CATEGORY == 'OTHER'){
											$CATEGORY_CODE = '6';
										}

										if($IDENTIFICATION_TYPE == 'KTP'){
											$IDENTIFICATION_TYPE_CODE = 'KTP';
										}
										elseif($IDENTIFICATION_TYPE == 'INDIVIDUAL'){
											$IDENTIFICATION_TYPE_CODE = 'SIM';
										}
										elseif($IDENTIFICATION_TYPE == 'PASPOR'){
											$IDENTIFICATION_TYPE_CODE = 'PAS';
										}
										elseif($IDENTIFICATION_TYPE == 'KITAS'){
											$IDENTIFICATION_TYPE_CODE = 'KIT';
										}

										if($NATIONALITY == 'WNI'){
											$NATIONALITY_CODE = 'W';
										}
										elseif($NATIONALITY == 'WNA'){
											$NATIONALITY_CODE = 'N';
										}
										
										if($CITIZENSHIP == 'RESIDENT'){
											$CITIZENSHIP_CODE = 'R';
										}else{
											$CITIZENSHIP_CODE = 'NR';
										}
									
										$paramBene = array(
													"FROM"      			=> 'B',
													 "ACBENEF"     			=> $BENEFICIARY_ACCOUNT,
													 "ACBENEF_CCY"    		=> 'IDR',
//													 "ACBENEF_ALIAS"    	=> $ACBENEF_ALIAS,
													 "ACBENEF_EMAIL"   		=> $EMAIL_ADDRESS,
													 "BANK_CODE"    	 	=> $BANK_CODE,
													 "_addBeneficiary"   	=> $this->view->hasPrivilege('BADA'),
													 "_beneLinkage"    		=> $privibenelinkage,
													 "ACCTSRC"     			=> $ACCT_NO, //source account m_user yg login
													 "VALIDATE"     		=> '1',
													 "sourceAccountType"    => $ACCT_TYPE,
													 "IMPORTDOM"    		=> TRUE,
												);
										
										$paramAdd = array("TRANSFER_TYPE"      	=>  'SKN',
																			 "ACBENEF_BANKNAME" 	=> $BENEFICIARY_ACCOUNT_NAME,
																			 "ACBENEF_ADDRESS1"   	=> $ADDRESS,
																			 "ACBENEF_ADDRESS2"   	=> $ADDRESS2,
																			 
																			 "ACBENEF_CITIZENSHIP"  => $CITIZENSHIP_CODE,
																			 "ACBENEF_RESIDENT"  	=> $NATIONALITY,
																			 "BENEFICIARY_PHONE"  	=> $PHONE,
																			 "NATIONALITY"  		=> $NATIONALITY,
										 									 "NATIONALITY_CODE"  	=> $NATIONALITY_CODE,

																			 "LLD_CATEGORY"  		=> $CATEGORY,
																			 "LLD_BENEIDENTIF"  	=> $IDENTIFICATION_TYPE,

																			 "LLD_CATEGORY_ORI"  	=> $CATEGORY_CODE,
																			 "LLD_BENEIDENTIF_ORI"  => $IDENTIFICATION_TYPE_CODE,

																			 "LLD_BENENUMBER"  		=> $IDENTIFICATION_NUMBER,
																			 "CITY_CODE"  			=> '-',
																			 "CITY_CODE_ORI"  		=> $CITY_CODE_ORI,
																			 //"BANK_CODE"  			=> $BANK_CODE,
																			 "CLR_CODE"  			=> $CLR_CODE,
																			 "CLR_CODE_ORI"  		=> $CLR_CODE_ORI,
																			 "SWIFT_CODE"			=> $SWIFT_CODE,
																			 "TYPE"  				=> 2,
																			 "BANK_NAME"  			=> $BANK_NAME,
																			 "CITY_NAME"  			=> $CITY_COL
																			);
																			
										$paramBene += array_merge($paramBene,$paramAdd);
									$listccy = Application_Helper_Array::simpleArray($this->getCcy(),'CCY_ID');
									$zf_filter_input = array('*' => array('HtmlEntities', 'StripTags', 'StringTrim'));
									$validators = array('BENEFICIARY_ACCOUNT' 		=> array(	'NotEmpty',
																					'Digits',
																					new Zend_Validate_StringLength(array('max'=>15,'min'=>8)),
																					'messages' => array(
																						'Beneficiary Account cannot be left blank. Please correct it.',
																						'Beneficiary Account must be numbers. Please correct it.',
																						'Beneficiary Account length must be 10 digits. Please correct it.',
																						)
																					),
														'CURR_CODE' 	=> array(	'NotEmpty',
																						array('InArray', array('haystack' => $listccy)),
																						'messages' => array(
																						'Currency cannot be left blank. Please correct it.',
																						'Currency is not on the database.')
																				),
																);
 
									$zf_filter_input = new Zend_Filter_Input($zf_filter_input, $validators, $this->_getAllParams(), array('breakChainOnFailure' => true));

									if($zf_filter_input->isValid()){
										Zend_Debug::dump($paramBene);
//										die;
									 	$validateACBENEF = new ValidateAccountBeneficiary($BENEFICIARY_ACCOUNT, $this->_custIdLogin, $this->_userIdLogin);
									 	$validateACBENEF->check($paramBene);

									 	$content['acct'][$rowNum] = array(
													'CUST_ID_LOGIN' 			=> $this->_custIdLogin,
													'USER_ID_LOGIN' 			=> $this->_userIdLogin,
													//'BENEFICIARY_ALIAS' 		=> $ACBENEF_ALIAS,
													'BENEFICIARY_ACCOUNT' 		=> $BENEFICIARY_ACCOUNT,
													'BENEFICIARY_NAME' 			=> $paramBene['ACBENEF_BANKNAME'],//$BENEFICIARY_ACCOUNT_NAME,
													'CURR_CODE' 				=> 'IDR',
													'BENEFICIARY_EMAIL' 			=> $EMAIL_ADDRESS,
													'BENEFICIARY_CITIZENSHIP_ori' 	=> $NATIONALITY,
													'BENEFICIARY_CITIZENSHIP' 	=> $paramBene['NATIONALITY_CODE'],
													'BENEFICIARY_PHONE' 		=> $paramBene['BENEFICIARY_PHONE'],
													'BENEFICIARY_ADDRESS'		=> $paramBene['ACBENEF_ADDRESS1'],//$ADDRESS,
													'BENEFICIARY_ADDRESS2'		=> $paramBene['ACBENEF_ADDRESS2'],//$ADDRESS,
													'BENEFICIARY_RESIDENT' 		=> $paramBene['ACBENEF_CITIZENSHIP'],//$CITIZENSHIP,

													'BANK_NAME'					=> $paramBene['BANK_NAME'],
													'BENEFICIARY_TYPE' 			=> $paramBene['TYPE'],
													'BENEFICIARY_TYPE_NAME' 	=> $BENEFICIARY_TYPE,

													'BENEFICIARY_CATEGORY' 		=> $paramBene['LLD_CATEGORY_ORI'],
													'BENEFICIARY_ID_TYPE' 		=> $paramBene['LLD_BENEIDENTIF_ORI'],

													'BENEFICIARY_CATEGORY_ORI' 	=> $paramBene['LLD_CATEGORY'],
													'BENEFICIARY_ID_TYPE_ORI' 	=> $paramBene['LLD_BENEIDENTIF'],

													'BENEFICIARY_ID_NUMBER' 	=> $paramBene['LLD_BENENUMBER'],
													'BENEFICIARY_CITY_CODE' 	=> $paramBene['CITY_CODE_ORI'],
									 				'CLR_CODE' 					=> $paramBene['CLR_CODE_ORI'],
//													'BANK_CODE' 				=> $BANK_CODE,
									 				'BANK_NAME' 				=> $paramBene['BANK_NAME'],
													'BANK_CODE' 				=> $paramBene['BANK_CODE'], 
													'SWIFT_CODE' 				=> $paramBene['SWIFT_CODE'],
									 				'CITY_NAME' 				=> $paramBene['CITY_NAME'],
													'_beneLinkage'     			=> $privibenelinkage,
										);


										 if ($validateACBENEF->isError())
										 {
											 //die('gere');
											$content['acct'][$rowNum] += array(
													'ERROR_MESSAGE' => $validateACBENEF->getErrorMsg()
											);
										 }

										 $validateACBENEF->__destruct();
										 unset($validateACBENEF);
									 }
									 else{
									 	//Zend_Debug::dump($zf_filter_input->getMessages());die;
//									 	die;
									 	$content['acct'][$rowNum] += array( 'ERROR_MESSAGE' => 	$this->language->_('Benficiary Account data is invalid'));
//									 	$content[$rowNum] += array( 'ERROR_MESSAGE' => 	$zf_filter_input->getMessages());
									 }

								 $rowNum++;									
																			
									
								}else{
									$this->view->error =1;
									//$errmsg[] = 'Number of column for all rows to be imported should be 4';
									$errmsg[] = 'Wrong File Format';
									$this->view->errmsg = $this->DisplayError($errmsg);
									break;
								}
								
								//var_dump($content);die;
								
						}
						
						if(!$errmsg)
							{
								$sessionNamespace = new Zend_Session_Namespace('import_predefbenef');
								$content['importdata'] = true;
								$content['importconfirm'] = true;
								$sessionNamespace->content = $content;
								
								//$_SESSION['import_predefbenef'] = $content;
								$this->_redirect("predefinedbeneficiary/domesticcctnew");
								//Zend_Debug::dump($_SESSION['import_predefbenef']);die;
								Application_Helper_General::writeLog('BIMA','Import Beneficiary');
							}	
							
							
					}else{
							
							$this->view->error =1;
							$errmsg[] = 'Total row imported cannot be more than'.$maxTotalRows;
							$content['errmsg'] = $this->DisplayError($errmsg);
							$sessionNamespace = new Zend_Session_Namespace('import_predefbenef');
								$sessionNamespace->content = $content;
								Zend_Debug::dump($sessionNamespace->content);die('gee');
								//$_SESSION['import_predefbenef'] = $content;
								$this->_redirect("predefinedbeneficiary/domesticcctnew");
					}
					
					
					}else{
						$this->view->error =1;
// 						$errmsg[] = 'Number of column for all rows to be imported should be 4';
						$errmsg[] = 'Wrong File Format. There is no data on csv File.';
						$content['errmsg'] = $this->DisplayError($errmsg);
						$sessionNamespace = new Zend_Session_Namespace('import_predefbenef');
								$sessionNamespace->content = $content;
								Zend_Debug::dump($sessionNamespace->content);die('sfa');
								//$_SESSION['import_predefbenef'] = $content;
								$this->_redirect("predefinedbeneficiary/domesticcctnew");
					}
					
					
					//PARSING CSV HERE
					//$csvData = $this->parseCSV($newFileName);
					//after parse delete document temporary
					@unlink($newFileName);
					//end

					

					
				}
			}
			else
			{
				
				 
				$this->view->error =1;
				$content['errmsg'] = $this->DisplayError($adapter->getMessages());
				//Zend_Debug::dump($adapter->getMessages());die;
				$sessionNamespace = new Zend_Session_Namespace('import_predefbenef');
								$sessionNamespace->content = $content;
								Zend_Debug::dump($sessionNamespace->content);die('asg'); 
								//$_SESSION['import_predefbenef'] = $content;
								$this->_redirect("predefinedbeneficiary/domesticcctnew");
			}
		}
		Application_Helper_General::writeLog('BIMA','Viewing Import Beneficiary');
	}

	 

	public function suggestdeleteAction()
	{
		$filters = array( 'BENEFICIARY_ID' => array('StringTrim','StripTags'));
		$validators = array('BENEFICIARY_ID' => array(	'NotEmpty',
														'Digits',
														'messages' => array(
																'Error: Beneficiary ID cannot be left blank.',
																'Error: Wrong Format Beneficiary ID.')
													));

		$zf_filter_input = new Zend_Filter_Input($filters,$validators,$this->_request->getParams(),$this->_optionsValidator);
		if($zf_filter_input->isValid())
		{
			$Beneficiary = new Beneficiary();
			$Beneficiary->suggestDelete($zf_filter_input->BENEFICIARY_ID);
		}
		$this->_redirect('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName());
	}
}
