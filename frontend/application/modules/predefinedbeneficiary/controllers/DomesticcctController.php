<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/Beneficiary.php';
require_once 'General/CustomerUser.php';
require_once 'CMD/Validate/ValidateAccountBeneficiary.php';
require_once 'General/Settings.php';
require_once 'Service/Token.php'; //added new
require_once 'CMD/Validate/Validate.php';
require_once 'Crypt/AESMYSQL.php';

class predefinedbeneficiary_DomesticcctController extends Application_Main
{
	protected $_moduleDB = 'RTF'; //masih harus diganti

	public function initController()
	{
		$selectCurrency = '-- ' . $this->language->_('Select Currency') . '--';
		$listCcy = array('' => $selectCurrency);
		if (count($this->getCcy()) == 1) { //remove useless value
			$listCcy = array();
		}
		$listCcy = array_merge($listCcy, Application_Helper_Array::listArray($this->getCcy(), 'CCY_ID', 'CCY_ID'));
		$this->view->ccy = $listCcy;
	}

	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$model = new predefinedbeneficiary_Model_Predefinedbeneficiary();

		$setting = new Settings();
		$system_type = $setting->getSetting('system_type');
		$this->view->system_type = $system_type;

		$sessionNamespace = new Zend_Session_Namespace('errimportdom');

		$this->view->error = $sessionNamespace->error;
		$sessionNamespace->unsetAll();

		$fields = array(
			/*'alias'  => array('field' => 'BENEFICIARY_ALIAS',
											   'label' => $this->language->_('Alias Name'),
											   'sortable' => true),*/
			'bankname'   => array(
				'field'    => 'BANK_NAME_disp',
				'label'    => $this->language->_('Bank Name'),
				'sortable' => true
			),
			'benef_acct'  => array(
				'field' => 'BENEFICIARY_ACCOUNT',
				'label' => $this->language->_('Beneficiary Account'),
				'sortable' => true
			),
			'benef_name'  => array(
				'field' => 'BENEFICIARY_NAME',
				'label' => $this->language->_('Beneficiary Account Name'),
				'sortable' => true
			),
			'email'  => array(
				'field' => 'BENEFICIARY_EMAIL',
				'label' => $this->language->_('Email Address'),
				'sortable' => true
			),
			'ccy'   => array(
				'field'    => 'CURR_CODE',
				'label'    => $this->language->_('CCY'),
				'sortable' => true
			),
			'beneftype'   => array(
				'field'    => 'BENEFICIARY_TYPE',
				'label'    => $this->language->_('Type'),
				'sortable' => true
			),
			// 'favorite'   => array('field'    => 'FAV',
			// 					'label'    => $this->language->_('Favorite'),
			// 					'sortable' => true),
			// 						'citizenship'   => array('field'    => 'BENEFICIARY_CITIZENSHIP_disp',
			// 											  'label'    => 'Citizenship',
			// 											  'sortable' => true),
			// 						'resident'   => array('field'    => 'BENEFICIARY_RESIDENT_disp',
			// // 											  'label'    => 'Resident Status',
			// 											  'label'    => $this->language->_('Citizenship'),
			// 											  'sortable' => true),
			// 						'citizenship'   => array('field'    => 'BENEFICIARY_CITIZENSHIP_disp',
			// 											  'label'    => $this->language->_('Nationality'),
			// 											  'sortable' => true),
			// 						'address'   => array('field'    => 'BENEFICIARY_ADDRESS',
			// 											  'label'    => $this->language->_('Address'),
			// 											  'sortable' => true),
			/*'bankcode'   => array('field'    => 'CLR_CODE',
											  'label'    => $this->language->_('Bank Code'),
											  'sortable' => true),*/
			// 'bankcode'   => array('field'    => 'CLR_CODE',
			// 					  'label'    => $this->language->_('Bank Code'),
			// 					  'sortable' => true),
			// 'status'   => array('field'    => 'BENEFICIARY_ISAPPROVE_disp',
			// 					  'label'    => $this->language->_('Status'),
			// 					  'sortable' => true),
			// 'checked'   => array('field'    => 'CHECKED_BY_BANK',
			// 						'label'    => $this->language->_('Checked by Bank'),
			// 						'sortable' => true),

			// 						'createddate'   => array('field'    => 'CREATED_DATE',
			// 												'label'    => 'Created Date',
			// 												'sortable' => true)
			// 'beneType'   => array('field'    => 'BENEFICIARY_TYPE_disp',
			// 					  'label'    => $this->language->_('Beneficiary Type'),
			// 					  'sortable' => true),
			// 'delete'   => array('field'    => 'BENEFICIARY_ISREQUEST_DELETE',
			// 					  'label'    => $this->language->_('Delete'),
			// 					  'sortable' => false)
		);

		$filterlist = array('BENEFICIARY_ACCOUNT', 'BENEFICIARY_NAME', 'BENEFICIARY_ALIAS', 'CURR_CODE', 'BENEFICIARY_BANK', 'BENEFICIARY_TYPE');

		$this->view->filterlist = $filterlist;

		$conf = Zend_Registry::get('config');
		$paymentType = $conf['payment']['type'];
		$paymentTypeFlip = array_flip($paymentType['code']);

		$this->view->paymentType = $paymentType;
		$this->view->paymentTypeFlip = $paymentTypeFlip;

		//get page, sortby, sortdir
		$csv    = $this->_getParam('csv');
		$page    = $this->_getParam('page');
		$sortBy  = $this->_getParam('sortby', 'doc_no');
		$sortDir = $this->_getParam('sortdir', 'asc');

		//validate parameters before passing to view and query
		$page = (Zend_Validate::is($page, 'Digits') && ($page > 0)) ? $page : 1;

		$sortBy = (Zend_Validate::is(
			$sortBy,
			'InArray',
			array(array_keys($fields))
		)) ? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];

		$sortDir = (Zend_Validate::is(
			$sortDir,
			'InArray',
			array('haystack' => array('asc', 'desc'))
		)) ? $sortDir : 'asc';


		//get filtering param
		$allNum = new Zend_Filter_Alnum(true);

		$filterArr = array(
			'filter' 	  	=> array('StringTrim', 'StripTags'),
			'alias' 	  	=> array('StringTrim', 'StripTags'),
			'BENEFICIARY_ALIAS' 	  	=> array('StringTrim', 'StripTags'),
			'CURR_CODE'    	=> array('StringTrim', 'StripTags', 'StringToUpper'),
			'BENEFICIARY_ACCOUNT'    => array('StringTrim', 'StripTags', 'StringToUpper'),
			'BENEFICIARY_NAME'    => array('StringTrim', 'StripTags'),
			'BENEFICIARY_BANK'    => array('StringTrim', 'StripTags'),
			'BENEFICIARY_TYPE'    => array('StringTrim', 'StripTags')
		);
		$dataParam = array('BENEFICIARY_ACCOUNT', 'BENEFICIARY_NAME', 'BENEFICIARY_ALIAS', 'BENEFICIARY_BANK', 'BENEFICIARY_TYPE', 'CURR_CODE');
		$dataParamValue = array();
		foreach ($dataParam as $dtParam) {

			// print_r($dtParam);die;
			if (!empty($this->_request->getParam('wherecol'))) {
				$dataval = $this->_request->getParam('whereval');
				foreach ($this->_request->getParam('wherecol') as $key => $value) {
					if ($dtParam == $value) {
						$dataParamValue[$dtParam] = $dataval[$key];
					}
				}
			}

			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}

		$zf_filter = new Zend_Filter_Input($filterArr, array(), $dataParamValue);
		$filter = $this->_request->getParam('filter');

		$alpha = $this->_getParam('alpha');
		//echo '<pre>';
		//var_dump($dataParamValue);die;
		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;
		$this->view->alpha = $alpha;

		/*$select = $this->_db->select()
					   ->from('M_BENEFICIARY',array('BENEFICIARY_ID','BENEFICIARY_ALIAS','BENEFICIARY_ACCOUNT','BENEFICIARY_NAME','BENEFICIARY_EMAIL','CURR_CODE','BENEFICIARY_CITIZENSHIP' => '(CASE BENEFICIARY_CITIZENSHIP WHEN \'R\' THEN \'Resident\' WHEN \'NR\' THEN \'Non Resident\' END)','BENEFICIARY_ADDRESS','CLR_CODE','BANK_NAME','BENEFICIARY_ISREQUEST_DELETE','BENEFICIARY_ISAPPROVE' => '(CASE BENEFICIARY_ISAPPROVE WHEN 0 THEN \'Not Approved\' WHEN 1 THEN \'Approved\' END)','BENEFICIARY_CREATED'));
		$select->where("BENEFICIARY_TYPE = 2");
		$select->where("CUST_ID = ".$this->_db->quote($this->_custIdLogin));*/

		$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		$select   = $CustUser->getBeneficiaries($this->view->hasPrivilege('BLBU'));
		//$select->where("B.BENEFICIARY_TYPE = ?", (string) $this->_paymenttype["code"]["domestic"]);
		//$select->where("B.BENEFICIARY_TYPE = ?", 8);
		$select->where("B.BENEFICIARY_TYPE in (2,8)");

		if ($alpha) $select->where("BENEFICIARY_NAME LIKE " . $this->_db->quote($alpha . '%'));

		if ($filter == TRUE) {
			//$fAlias = $zf_filter->getEscaped('alias');
			$fAcct = $zf_filter->getEscaped('BENEFICIARY_ACCOUNT');
			$fName = $zf_filter->getEscaped('BENEFICIARY_NAME');
			$fAlias = $zf_filter->getEscaped('BENEFICIARY_ALIAS');
			$fBank = $zf_filter->getEscaped('BENEFICIARY_BANK');
			$fType = $zf_filter->getEscaped('BENEFICIARY_TYPE');
			$fCcy = $zf_filter->getEscaped('CURR_CODE');

			if ($fAlias) $select->where('UPPER(BENEFICIARY_NAME) LIKE ' . $this->_db->quote('%' . strtoupper($fAlias) . '%'));
			if ($fAcct) $select->where('BENEFICIARY_ACCOUNT LIKE ' . $this->_db->quote('%' . strtoupper($fAcct) . '%'));
			if ($fName) $select->where('UPPER(BENEFICIARY_NAME) LIKE ' . $this->_db->quote('%' . strtoupper($fName) . '%'));
			if ($fAlias) $select->where('UPPER(BENEFICIARY_NAME) LIKE ' . $this->_db->quote('%' . strtoupper($fAlias) . '%'));
			if ($fBank) $select->where('UPPER(B.BANK_NAME) LIKE ' . $this->_db->quote('%' . strtoupper($fBank) . '%'));
			if ($fType) $select->where('UPPER(BENEFICIARY_TYPE) = ?', $fType);
			if ($fCcy) $select->where('CURR_CODE = ' . $this->_db->quote(strtoupper($fCcy)));

			$this->view->alias = $fAlias;
			$this->view->benef_acct = $fAcct;
			$this->view->benef_name = $fName;
			$this->view->benef_ccy = $fCcy;
			$this->view->alias = $fAlias;
		}
		//else{
		//	$select->where("B.BENEFICIARY_TYPE = ?", (string) $this->_paymenttype["code"]["domestic"]);
		//}

		$selectlimit   = $CustUser->getBeneficiaries($this->view->hasPrivilege('BLBU'));
		//
		$result = $select->query()->fetchAll();

		$select->order($sortBy . ' ' . $sortDir);
		$select = $select->query()->fetchAll();

		$setting = new Settings();
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$pw_hash = md5($enc_salt . $enc_pass);
		$rand = $this->_userIdLogin . date('dHis') . $pw_hash;
		$sessionNamespace->token   = $rand;
		$this->view->token = $sessionNamespace->token;

		foreach ($select as $key => $value) {
			$get_beneficiary_id = $value["BENEFICIARY_ID"];

			$AESMYSQL = new Crypt_AESMYSQL();
			$rand = $this->token;

			$encrypted_payreff = $AESMYSQL->encrypt($get_beneficiary_id, $rand);
			$encpayreff = urlencode($encrypted_payreff);

			$select[$key]["BENEFICIARY_ID_ENCRYPTED"] = $encpayreff;
		}

		$this->paging($select);

		$data_val = array();
		foreach ($result as $valu) {
			$paramTrx = array(
				"BANK_NAME" => $valu['BANK_NAME'],
				"BENEFICIARY_ACCOUNT" => $valu['BENEFICIARY_ACCOUNT'],
				"BENEFICIARY_NAME"	=> $valu['BENEFICIARY_NAME'],
				"BENEFICIARY_EMAIL"	=> $valu['BENEFICIARY_EMAIL'],
				"CURR_CODE"	=> $valu['CURR_CODE'],
				"BENEFICIARY_TYPE"	=> $valu['BENEFICIARY_TYPE'],
			);
			array_push($data_val, $paramTrx);
		}

		if ($csv) {

			$header  = Application_Helper_Array::simpleArray($fields, "label");
			$this->_helper->download->csv($header, $data_val, null, 'Beneficiary List');
		}
		Application_Helper_General::writeLog('BLBA', 'Viewing SKN/RTGS Account');
		$this->view->fields = $fields;
		$this->view->filter = $filter;
		if (!empty($this->_request->getParam('wherecol'))) {
			$this->view->wherecol			= $this->_request->getParam('wherecol');
		}

		if (!empty($this->_request->getParam('whereopt'))) {
			$this->view->whereopt			= $this->_request->getParam('whereopt');
		}

		if (!empty($this->_request->getParam('whereval'))) {
			$this->view->whereval			= $this->_request->getParam('whereval');
		}

		//$this->_helper->download->csv(array('Group ID','Group Name','Status'),null,$select,'download group');
	}

	public function addbeneficiaryAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$anyValue = '-- ' . $this->language->_('Select City') . ' --';
		$select = $this->_db->select()
			->from(array('A' => 'M_CITY'), array('*'));
		$select->order('CITY_NAME ASC');
		$arr = $this->_db->fetchall($select);

		//		$cityCodeArr 			= array(''=> $anyValue);
		//		$cityCodeArr 			+= Application_Helper_Array::listArray($arr,'CITY_CODE','CITY_NAME');
		//		$this->view->cityCodeArr 	= $cityCodeArr;
		$this->view->cityCodeList 		= $arr;

		$Settings = new Settings();
		$address_mandatory = $Settings->getSettingNew('address_mandatory');
		$this->view->address_mandatory = $address_mandatory;

		$settings 			= new Application_Settings();
		$lldCategoryArr  	= $settings->getLLDDOMCategory();
		$this->view->lldCategoryArr 	= $lldCategoryArr;

		$lldBeneIdentifArr 	= $settings->getLLDDOMBeneIdentification();
		$this->view->lldBeneIdentifArr  = $lldBeneIdentifArr;

		$this->view->CITIZENSHIP = 'R';
		$this->view->RESIDENT = 'R';
		//$this->view->CITIZENSHIP = $this->_getParam('RESIDENT');
		$this->view->benefType = '1';

		$privibenelinkage = $this->view->hasPrivilege('BLBU');
		$Beneficiary = new Beneficiary();
		$filter  = new Application_Filtering();
		$settings = new Application_Settings();
		$ccyList  = $settings->setCurrencyRegistered();
		$BANK_NAME = '-- Any Value --';

		$this->view->userIdLogin  = $this->_userIdLogin;
		//$benefType = "2";
		$benefType = $this->_getParam('benefType');

		//ambil data m_user ->ambil sourceAccount, acctType, statusnya aktif=1
		$acctStatus = 1;
		$dataAcct = $this->_db->fetchRow(
			$this->_db->select()
				->from(array('C' => 'M_CUSTOMER_ACCT'))
				->where("CUST_ID = " . $this->_db->quote($this->_custIdLogin))
				->where("ACCT_STATUS = ?", $acctStatus)
				->limit(1)
		);
		$ACCT_NO = (!empty($dataAcct['ACCT_NO']) ? $dataAcct['ACCT_NO'] : '');
		$ACCT_TYPE = (!empty($dataAcct['ACCT_TYPE']) ? $dataAcct['ACCT_TYPE'] : '');

		//validasi token -- begin
		/*$select3 = $this->_db->select()
					 ->from(array('C' => 'M_USER'));
		$select3->where("USER_ID = ".$this->_db->quote($this->_userIdLogin));
		$data2 					= $this->_db->fetchRow($select3);
		$tokenIdUser 			= $data2['TOKEN_ID'];
		$tokenType 				= $data2['TOKEN_TYPE'];
		$usermobilephone 		= $data2['USER_MOBILE_PHONE'];
		$this->view->tokentype 	= $data2['TOKEN_TYPE'];*/

		//added new hard token
		/*$HardToken = new Service_Token($this->_custIdLogin, $this->_userIdLogin, $tokenIdUser);
		$challengeCode = $HardToken->generateChallengeCode();

		$challengeCodeSub = substr($challengeCode, 0,2);
		$this->view->challengeCodeReq = $challengeCodeSub;*/
		//validasi token -- end

		if ($this->_request->isPost()) {
			//var_dump('gere');die;
			$BENEFICIARY_ACCOUNT = $this->_getParam('ACBENEF');
			$CURR_CODE = $this->_getParam('CURR_CODE');
			$BENEFICIARY_ALIAS = $this->_getParam('ACBENEF_ALIAS');
			$BENEFICIARY_EMAIL = $this->_getParam('ACBENEF_EMAIL');
			$ACBENEF_ADDRESS = $this->_request->getParam('ADDRESS');
			$ACBENEF_ADDRESS2 = $this->_request->getParam('ADDRESS2');

			$ACBENEF_CITIZENSHIP = $this->_request->getParam('CITIZENSHIP');
			$ACBENEF_RESIDENT = $this->_request->getParam('NATIONALITY');

			$BANK_NAME   = $this->_request->getParam('BANK_NAME');
			$BANK_CITY   = $this->_request->getParam('BANK_CITY');
			$CLR_CODE   = $this->_request->getParam('CLR_CODE');
			$ACBENEF_BANKNAME  = $this->_request->getParam('ACBENEF_BANKNAME');

			$LLD_CATEGORY = $this->_request->getParam('LLD_CATEGORY');
			$LLD_BENEIDENTIF = $this->_request->getParam('LLD_BENEIDENTIF');
			$LLD_BENENUMBER = $this->_request->getParam('LLD_BENENUMBER');
			$CITY_CODE = $this->_request->getParam('CITY_CODE');
			$SWIFT_CODE   = $this->_request->getParam('SWIFT_CODE');
			$BENEFICIARY_BANK_CODE = $this->_request->getParam('BENEFICIARY_BANK_CODE');

			//-------------------------------------------------------------START VALIDATION---------------------------------------------------------------------//
			$ACBENEF    = $filter->filter($BENEFICIARY_ACCOUNT, "ACCOUNT_NO");
			$ACBENEF_BANKNAME  = $filter->filter($ACBENEF_BANKNAME, "ACCOUNT_NAME");
			$ACBENEF_ALIAS   = $filter->filter($BENEFICIARY_ALIAS, "ACCOUNT_ALIAS");
			$ACBENEF_EMAIL   = $filter->filter($BENEFICIARY_EMAIL, "EMAIL");
			$ACBENEF_CCY   = $filter->filter($CURR_CODE, "SELECTION");
			$ACBENEF_ADDRESS = $filter->filter($ACBENEF_ADDRESS, "ADDRESS");
			$ACBENEF_ADDRESS2 = $filter->filter($ACBENEF_ADDRESS2, "ADDRESS2");
			//$ACBENEF_CITIZENSHIP= $this->_request->getParam('RESIDENT');
			$ACBENEF_CITIZENSHIP = $filter->filter($ACBENEF_CITIZENSHIP, "SELECTION");
			$ACBENEF_RESIDENT = $filter->filter($ACBENEF_RESIDENT, "SELECTION");



			// print_r($ACBENEF_RESIDENT);
			// print_r($ACBENEF_CITIZENSHIP);die;

			$BANK_NAME   = $filter->filter($BANK_NAME, "BANK_NAME");
			$BANK_CITY   = $filter->filter($BANK_CITY, "ADDRESS");
			$CLR_CODE   = $filter->filter($CLR_CODE, "BANK_CODE");

			$LLD_CATEGORY = $filter->filter($LLD_CATEGORY, "LLD_CATEGORY");
			$LLD_BENEIDENTIF = $filter->filter($LLD_BENEIDENTIF, "LLD_BENEIDENTIF");
			$LLD_BENENUMBER = $filter->filter($LLD_BENENUMBER, "LLD_BENENUMBER");
			$CITY_CODE = $filter->filter($CITY_CODE, "CITY_CODE");
			$SWIFT_CODE = $filter->filter($SWIFT_CODE, "SWIFT_CODE");

			//validasi token -- begin
			/*$challengeCodeReq2 	= $filter->filter($this->_request->getParam('challengeCodeReq2')	, "challengeCodeReq2");
			$challengeCodeReq 	= $filter->filter($this->_request->getParam('challengeCodeReq')	, "challengeCodeReq");

			$responseCodeReq 	= $filter->filter($this->_request->getParam('responseCodeReq')	, "responseCodeReq");
			//$random = str_pad(mt_rand(0, 9), 2, '0', STR_PAD_LEFT);
			$this->view->challengeCodeReq1 = substr($challengeCodeReq, -4);*/
			//validasi token -- end

			//     		$paramBene = array(//"TRANSFER_TYPE"      	=>  'SKN',
			//								"FROM"      			=> 'B',
			//								 "ACBENEF"     			=> $ACBENEF,
			//								 "ACBENEF_CCY"    		=> $ACBENEF_CCY,
			//								 "ACBENEF_BANKNAME" 	=> $ACBENEF_BANKNAME,
			//								 "ACBENEF_ALIAS"    	=> $ACBENEF_ALIAS,
			//								 "ACBENEF_EMAIL"   		=> $ACBENEF_EMAIL,
			//								 "ACBENEF_ADDRESS1"   	=> $ACBENEF_ADDRESS,
			//								 "ACBENEF_RESIDENT"  	=> $ACBENEF_RESIDENT,
			// 								 "ACBENEF_CITIZENSHIP"  => $ACBENEF_CITIZENSHIP,
			//								 "BANK_CODE"    	 	=> $CLR_CODE,
			//     							 "ACCTSRC"    			=> $ACBENEF,
			//								 "_addBeneficiary"   	=> $this->view->hasPrivilege('BADA'),
			//								 "_beneLinkage"    		=> $privibenelinkage,
			//								);
			//			if($benefType == 1){
			//				$paramAdd = array("TRANSFER_TYPE"      	=>  'SKN',
			//													 "ACBENEF_BANKNAME" 	=> &$ACBENEF_BANKNAME,
			//													 "ACBENEF_ADDRESS1"   	=> $ACBENEF_ADDRESS,
			//													 "ACBENEF_CITIZENSHIP"  => $ACBENEF_CITIZENSHIP,
			//													);
			//			}else{
			//				$paramAdd = array("TRANSFER_TYPE"      	=>  'ONLINE',
			//													 "ACBENEF_BANKNAME" 	=> '',
			//													 "ACBENEF_ADDRESS1"   	=> '',
			//													 "ACBENEF_CITIZENSHIP"  => '',
			//													);
			//			}

			$paramBene = array(
				"FROM"      			=> 'B',
				"ACBENEF"     			=> $ACBENEF,
				"ACBENEF_CCY"    		=> $ACBENEF_CCY,
				"ACBENEF_ALIAS"    	=> $ACBENEF_ALIAS,
				"ACBENEF_EMAIL"   		=> $ACBENEF_EMAIL,
				"BANK_CODE"    	 	=> $CLR_CODE,
				"BENEFICIARY_BANK_CODE"    	 	=> $BENEFICIARY_BANK_CODE,
				"_addBeneficiary"   	=> $this->view->hasPrivilege('BADA'),
				"_beneLinkage"    		=> $privibenelinkage,
				"ACCTSRC"     			=> $ACCT_NO, //source account m_user yg login
				"VALIDATE"     		=> '1',
				"sourceAccountType"    => $ACCT_TYPE,
			);
			if ($benefType == 1) {
				$paramAdd = array(
					"TRANSFER_TYPE"      	=>  'SKN',
					"ACBENEF_BANKNAME" 	=> $ACBENEF_BANKNAME,
					"ACBENEF_ADDRESS1"   	=> $ACBENEF_ADDRESS,
					"ACBENEF_ADDRESS2"   	=> $ACBENEF_ADDRESS2,
					"ACBENEF_CITIZENSHIP"  => $ACBENEF_CITIZENSHIP,
					"ACBENEF_RESIDENT"  	=> $ACBENEF_RESIDENT,

					"LLD_CATEGORY"  	=> $LLD_CATEGORY,
					"LLD_BENEIDENTIF"  	=> $LLD_BENEIDENTIF,
					"LLD_BENENUMBER"  	=> $LLD_BENENUMBER,
					"CITY_CODE"  	=> $CITY_CODE,
				);
			} else {
				$paramAdd = array(
					"TRANSFER_TYPE"      	=>  'ONLINE',
					"ACBENEF_BANKNAME" 	=> '',
					"ACBENEF_ADDRESS1"   	=> '',
					"ACBENEF_ADDRESS2"   	=> '',
					"ACBENEF_CITIZENSHIP"  => '',
					"ACBENEF_RESIDENT"  	=> '',

					"LLD_CATEGORY"  	=> '',
					"LLD_BENEIDENTIF"  	=> '',
					"LLD_BENENUMBER"  	=> '',
					"CITY_CODE"  	=> '',
				);
			}


			$paramBene += array_merge($paramBene, $paramAdd);

			$validateACBENEF = new ValidateAccountBeneficiary($ACBENEF, $this->_custIdLogin, $this->_userIdLogin);
			$checkedParams = $validateACBENEF->check($paramBene);
			//var_dump($checkedParams); die;

			$filters = array(
				'ACBENEF_EMAIL' 	=> array('StripTags', 'StringTrim')
			);

			$validators = array(

				'ACBENEF_EMAIL' => array(
					'NotEmpty',
					new SGO_Validate_EmailAddress(),
					array('StringLength', array('min' => 1, 'max' => 128)),
					'messages' => array(
						$this->language->_('Can not be empty'),
						$this->language->_('Email lenght cannot be more than 128'),
						$this->language->_('Invalid email format'),
					)
				),


			);
			$params = $this->_request->getParams();
			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $params, $this->_optionsValidator);



			if (!$validateACBENEF->isError() && $zf_filter_input->isValid()) {
				// var_dump('asg');die;
				$content = array(
					'CUST_ID_LOGIN' 			=> $this->_custIdLogin,
					'USER_ID_LOGIN' 			=> $this->_userIdLogin,
					'BENEFICIARY_ALIAS' 		=> $ACBENEF_ALIAS,
					'BENEFICIARY_ACCOUNT' 		=> $ACBENEF,
					'BENEFICIARY_NAME' 			=> $ACBENEF_BANKNAME,
					'CURR_CODE' 				=> $ACBENEF_CCY,
					'BENEFICIARY_EMAIL' 		=> $ACBENEF_EMAIL,
					'BENEFICIARY_CITIZENSHIP' 	=> $ACBENEF_RESIDENT,
					'BENEFICIARY_RESIDENT' 	=> $ACBENEF_CITIZENSHIP,
					'BENEFICIARY_ADDRESS' 		=> $ACBENEF_ADDRESS,
					'BENEFICIARY_ADDRESS2' 		=> $ACBENEF_ADDRESS2,
					'BANK_NAME' 				=> $BANK_NAME,
					'CLR_CODE' 					=> $CLR_CODE,
					'BANK_CODE' 				=> $BENEFICIARY_BANK_CODE,
					'SWIFT_CODE'    			=> $SWIFT_CODE,
					'BANK_CITY'			 		=> $BANK_CITY,
					'BENEFICIARY_TYPE' 			=> 2,
					'_beneLinkage'    			=> $privibenelinkage,

					'BENEFICIARY_CATEGORY'    	=> $LLD_CATEGORY,
					'BENEFICIARY_ID_TYPE'    	=> $LLD_BENEIDENTIF,
					'BENEFICIARY_ID_NUMBER'    	=> $LLD_BENENUMBER,
					'BENEFICIARY_CITY_CODE'    	=> $CITY_CODE,
				);

				//validasi hard token page 1 -- begin
				/*$message = $Settings->getSetting('token_confirm_message');
				$trans = array("[Response_Code]" => $responseCode);
				$message = strtr($message, $trans);

				if($tokenType == '1'){ //sms token
					if(empty($responseCodeReq)){
						$resultToken = FALSE;
						$this->view->error = true;
						$errMessage = 'Error : Response Token cannot be left blank.';
						$this->view->report_msg = $errMessage;
					}
					else
					{
						$token = new Service_Token(NULL,$this->_userIdLogin);
						$token->setMsisdn($usermobilephone);
						$token->setMessage($message);

						$res = $token->confirm();
						$resultToken = $res['ResponseCode'] == '0000';
					}
				}
				elseif($tokenType == '2'){ //hard token
					if(empty($responseCodeReq)){
						$resultToken = FALSE;
						$this->view->error = true;
						$errMessage = 'Error : Response Token cannot be left blank.';
						$this->view->report_msg = $errMessage;
					}
					else
					{
						$resHard = $HardToken->verifyHardToken($challengeCodeReq2.$challengeCodeReq, $responseCodeReq);
						$resultToken = $resHard['ResponseCode'] == '0000';

						//set user lock token gagal
						$CustUser = new CustomerUser($this->_userIdLogin);
						if ($resHard['ResponseCode'] != '0000'){
							$tokenFailed = $CustUser->setFailedTokenMustLogout();
							if ($tokenFailed)
								$this->_forward('home');
						}
					}
				}
				elseif($tokenType == '3'){ //mobile token
					$resultToken = TRUE;
				}
				else{$resultToken = TRUE;}
				//validasi hard token page 1 -- end	*/

				//if($resultToken == TRUE){
				try {

					//-----insert benef--------------
					/*$this->_db->beginTransaction();

						$add = $Beneficiary->add($content);

						Application_Helper_General::writeLog('BADA','Add Beneficiary Account '.$ACBENEF);

						$this->_db->commit();

						//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), null);
						//$this->view->success = true;
						//$msg = $this->getErrorRemark('00','Add Beneficiary');
						//$this->view->report_msg = $msg;
						$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
						$this->_redirect('/notification/success');*/
					//Zend_Debug::dump($checkedParams);die;

					////////////////////////////////////////////////////////////////////////////yang difault - begin//////////////////////////////////
					//						$content['BENEFICIARY_NAME'] = $checkedParams['ACBENEF_BANKNAME'];
					//						//$content['BENEFICIARY_TYPE'] = 8; // 8 = online
					//						$content['BENEFICIARY_TYPE'] = 2; // 8 = online
					//						//$content['BANK_CODE'] = $CLR_CODE;
					//						$content['CLR_CODE'] = $CLR_CODE;
					//
					//						$sessionNamespace = new Zend_Session_Namespace('beneficiaryAccountAddEdit');
					//						$sessionNamespace->mode = 'Add';
					//						$sessionNamespace->content = $content;
					//						$this->_redirect('/predefinedbeneficiary/domesticcct/confirm');
					////////////////////////////////////////////////////////////////////////////yang difault - end//////////////////////////////////

					if ($benefType == '1') {

						$setting = new Settings();
						$system_type1 = $setting->getSetting('system_type');
						//var_dump($system_type1);die;
						//if ($system_type1 == 2) {

						$acctlist = $this->_db->fetchRow(
							$this->_db->select()
								->from(array('A' => 'M_APIKEY'))
								->join(array('C' => 'M_APICREDENTIAL'), 'A.APIKEY_ID = C.ID', array('*'))
								->join(array('B' => 'M_BANKTABLE'), 'B.BANK_CODE = A.BANK_CODE', array('B.BANK_NAME'))
								// ->where('A.ACCT_STATUS = ?','5')
								->where("A.CUST_ID = ? ", $this->_custIdLogin)
								->order('A.APIKEY_ID ASC')
							// echo $acctlist;
						);




						$request['sender_id'] = $acctlist['SENDER_ID'];
						$request['AUTH_USER'] = $acctlist['AUTH_USER'];
						$request['AUTH_PASS'] = $acctlist['AUTH_PASS'];
						$request['signature'] = $acctlist['SIGNATURE_KEY'];
						$request['corporate_id_channel'] = $this->_custIdLogin;
						$request['beneficiary_account_number'] = $content['BENEFICIARY_ACCOUNT'];
						$request['beneficiary_bank_code'] = $content['BANK_CODE'];
						// echo "<pre>";
						// print_r($this->_request->getParams());
						// print_r($request);
						$clientUser  =  new SGO_Soap_ClientUser();
						$success = $clientUser->callapi('accountname', $request, 'b2b/inquiry/name');

						$result  = $clientUser->getResult();

						$paramlog = array(
							'DIGI_USER' => $this->_userIdLogin,
							'DIGI_CUST' => $this->_custIdLogin,
							'DIGI_BANK' => $request['beneficiary_bank_code'],
							'DIGI_ACCOUNT' => $request['beneficiary_account_number'],
							'DIGI_ERROR_CODE' => $result->error_code,
							'DIGI_TIMESTAMP' => new Zend_Db_Expr("now()"),
							'DIGI_SERVICE'  => 7
						);


						$this->_db->insert('T_DIGI_LOG', $paramlog);
						//$this->_helper->ServiceLog->serviceLog($paramlog);
						//var_dump($result->error_code);die;
						if ($result->error_code == "0000") {
							$checkedParams['ACBENEF_BANKNAME'] = $result->beneficiary_account_name;
							$content["reffId"] = $result->transfer_reff;
						} elseif ($result->error_code == "XT") { //
							//				$this->setError("Cant Continue the Transaction, Please Retry in a few moment");
							$errMes = (!empty($result->error_message) ? $result->error_message : "Cant Continue the Transaction, Please Retry in a few moment");
							//$errMessage = 'Error : Beneficiary account is invalid';
							$this->view->resulttoken = $errMes;
							//$this->setError($errMes);
						} else {
							$errMessage = 'Error : Beneficiary account is invalid';
							$this->view->resulttoken = $errMessage;
							//$this->setError("Beneficiary account is invalid");
							//$this->setError($result["ResponseDesc"]);
						}

						//	var_dump($result);die;

						//}

						$content['TYPE'] = 'SKN'; // 2 = domestic ( SKN/RTGS )

						$content['BENEFICIARY_TYPE'] = 2; // 2 = domestic ( SKN/RTGS )
						$content['CLR_CODE'] = $CLR_CODE;
						$content['BENEFICIARY_NAME_CEK'] = $checkedParams['ACBENEF_BANKNAME'];
						$content['BENEFICIARY_NEWNAME'] = $checkedParams['ACBENEF_BANKNAME'];
						//$content['BENEFICIARY_NAME'] = $checkedParams['ACBENEF_BANKNAME'];

						//echo '<pre>';
						//var_dump($content);die;
						$sessionNamespace = new Zend_Session_Namespace('beneficiaryAccountAddEdit');
						$sessionNamespace->mode = 'Add';
						$sessionNamespace->content = $content;
						$this->_redirect('/predefinedbeneficiary/domesticcct/confirm');
					} else if ($benefType == '2') {
						$content['TYPE'] = 'ONLINE'; // 2 = domestic ( SKN/RTGS )

						$acctlist = $this->_db->fetchRow(
							$this->_db->select()
								->from(array('A' => 'M_APIKEY'))
								->join(array('C' => 'M_APICREDENTIAL'), 'A.APIKEY_ID = C.ID', array('*'))
								->join(array('B' => 'M_BANKTABLE'), 'B.BANK_CODE = A.BANK_CODE', array('B.BANK_NAME'))
								// ->where('A.ACCT_STATUS = ?','5')
								->where("A.CUST_ID = ? ", $this->_custIdLogin)
								->order('A.APIKEY_ID ASC')
							// echo $acctlist;
						);




						$request['sender_id'] = $acctlist['SENDER_ID'];
						$request['AUTH_USER'] = $acctlist['AUTH_USER'];
						$request['AUTH_PASS'] = $acctlist['AUTH_PASS'];
						$request['signature'] = $acctlist['SIGNATURE_KEY'];
						$request['corporate_id_channel'] = $this->_custIdLogin;
						$request['beneficiary_account_number'] = $content['BENEFICIARY_ACCOUNT'];
						$request['beneficiary_bank_code'] = $content['BANK_CODE'];
						//echo "<pre>";


						// print_r($this->_request->getParams());
						//print_r($request);
						$clientUser  =  new SGO_Soap_ClientUser();
						$success = $clientUser->callapi('accountname', $request, 'b2b/inquiry/name');

						$result  = $clientUser->getResult();
						//var_dump($result);die;
						$paramlog = array(
							'DIGI_USER' => $this->_userIdLogin,
							'DIGI_CUST' => $this->_custIdLogin,
							'DIGI_BANK' => $request['beneficiary_bank_code'],
							'DIGI_ACCOUNT' => $request['beneficiary_account_number'],
							'DIGI_ERROR_CODE' => $result->error_code,
							'DIGI_TIMESTAMP' => new Zend_Db_Expr("now()"),
							'DIGI_SERVICE'  => 7
						);


						$this->_db->insert('T_DIGI_LOG', $paramlog);
						//$this->_helper->ServiceLog->serviceLog($paramlog);
						//var_dump($result->error_code);die;
						if ($result->error_code == "0000") {
							$checkedParams['ACBENEF_BANKNAME'] = $result->beneficiary_account_name;
							$content["reffId"] = $result->transfer_reff;
						} elseif ($result->error_code == "XT") { //
							//				$this->setError("Cant Continue the Transaction, Please Retry in a few moment");
							$errMes = (!empty($result->error_message) ? $result->error_message : "Cant Continue the Transaction, Please Retry in a few moment");
							//$errMessage = 'Error : Beneficiary account is invalid';
							$this->view->resulttoken = $errMes;
							//$this->setError($errMes);
						} else {
							$errMessage = 'Error : Beneficiary account is invalid';
							$this->view->resulttoken = $errMessage;
							//$this->setError("Beneficiary account is invalid");
							//$this->setError($result["ResponseDesc"]);
						}
						//var_dump($checkedParams);die; 
						$content['BENEFICIARY_NAME_CEK'] = $checkedParams['ACBENEF_BANKNAME'];
						$content['BENEFICIARY_NEWNAME'] = $checkedParams['ACBENEF_BANKNAME'];
						$content['BENEFICIARY_NAME'] = $checkedParams['ACBENEF_BANKNAME'];
						$content['BENEFICIARY_TYPE'] = 8; // 8 = online
						//$content['BANK_CODE'] = $CLR_CODE;
						$content['CLR_CODE'] = $CLR_CODE;
						$sessionNamespace = new Zend_Session_Namespace('beneficiaryAccountAddEdit');
						$sessionNamespace->mode = 'Add';
						$sessionNamespace->content = $content;
						$this->_redirect('/predefinedbeneficiary/domesticcct/confirm');
					}
				} catch (Exception $e) {
					var_dump($e);
					die;
					//rollback changes
					$this->_db->rollBack();

					//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $errorMsg);
					//Application_Log_GeneralLog::technicalLog($e);
				}
				//}else {
				/*
						$this->view->ACBENEF = $ACBENEF;

						if($benefType == 1){
							$this->view->BANK_NAME = $BANK_NAME;
						}
						elseif($benefType == 2){
							$this->view->BANK_NAME = $CLR_CODE.' - '.$BANK_NAME;
						}

						$this->view->CLR_CODE = $CLR_CODE;
						$this->view->benefType = $benefType;
						$this->view->ACBENEF_BANKNAME = $ACBENEF_BANKNAME;
						$this->view->ADDRESS = $ACBENEF_ADDRESS;



						if($resultToken == FALSE){
							if($res['ResponseCode'] == 'XT'){
								$errMessage = 'Error : Service Rejected';
								$this->view->resulttoken = $errMessage;
							}
							else{
								$errMessage = 'Error : Invalid Token';
								$this->view->resulttoken = $errMessage;
							}
						}

				}*/

				$this->view->benefType = $benefType;
			} else {
				$this->view->error = true;
				// 				$this->fillParams($ACBENEF_ALIAS,$ACBENEF,$ACBENEF_BANKNAME,$ACBENEF_CCY,$ACBENEF_EMAIL,$ACBENEF_CITIZENSHIP,$ACBENEF_RESIDENT,$ACBENEF_ADDRESS,$BANK_NAME,$CLR_CODE,$BANK_CITY);
				//$this->fillParams(null,$ACBENEF_ALIAS,$ACBENEF,$ACBENEF_BANKNAME,$ACBENEF_CCY,$ACBENEF_EMAIL,$ACBENEF_CITIZENSHIP,$ACBENEF_RESIDENT,$ACBENEF_ADDRESS,$BANK_NAME,$CLR_CODE,$BANK_CITY);
				$this->fillParams($benefType, $ACBENEF_ALIAS, $ACBENEF, $ACBENEF_BANKNAME, $ACBENEF_CCY, $ACBENEF_EMAIL, $ACBENEF_CITIZENSHIP, $ACBENEF_RESIDENT, $ACBENEF_ADDRESS, $BANK_NAME, $CLR_CODE, $BANK_CITY, $BENEFICIARY_TYPE, $LLD_CATEGORY, $LLD_BENEIDENTIF, $LLD_BENENUMBER, $CITY_CODE);

				$docErr = '' . $validateACBENEF->getErrorMsg();
				$validateACBENEF->__destruct();
				unset($validateACBENEF);
				$this->view->report_msg = $docErr;
				$this->view->benefType = $benefType;
				$this->view->CITIZENSHIP = $this->_getParam('CITIZENSHIP');
				$this->view->RESIDENT = $this->_getParam('RESIDENT');

				$this->view->LLD_CATEGORY = $LLD_CATEGORY;
				$this->view->LLD_BENEIDENTIF = $LLD_BENEIDENTIF;
				$this->view->LLD_BENENUMBER = $LLD_BENENUMBER;
				$this->view->CITY_CODE = $CITY_CODE;
				$this->view->SWIFT_CODE = $SWIFT_CODE;
				$this->view->BENEFICIARY_BANK_CODE = $BENEFICIARY_BANK_CODE;
			}
		}
		//$this->view->CITIZENSHIP = $this->_getParam('RESIDENT');

		Application_Helper_General::writeLog('BADA', 'Viewing Add Beneficiary Account ');
	}

	public function editbeneficiaryAction()
	{

		$this->_helper->layout()->setLayout('newlayout');
		$anyValue = '-- ' . $this->language->_('Select City') . ' --';
		$select = $this->_db->select()
			->from(array('A' => 'M_CITY'), array('*'));
		$select->order('CITY_NAME ASC');
		$arr = $this->_db->fetchall($select);

		//		$cityCodeArr 			= array(''=> $anyValue);
		//		$cityCodeArr 			+= Application_Helper_Array::listArray($arr,'CITY_CODE','CITY_NAME');
		//		$this->view->cityCodeArr 	= $cityCodeArr;
		$this->view->cityCodeList 		= $arr;

		$settings 			= new Application_Settings();
		$lldCategoryArr  	= $settings->getLLDDOMCategory();
		$this->view->lldCategoryArr 	= $lldCategoryArr;

		$lldBeneIdentifArr 	= $settings->getLLDDOMBeneIdentification();
		$this->view->lldBeneIdentifArr  = $lldBeneIdentifArr;

		////////////

		$Settings = new Settings();
		$address_mandatory = $Settings->getSettingNew('address_mandatory');
		$this->view->address_mandatory = $address_mandatory;

		$privibenelinkage = $this->view->hasPrivilege('BLBU');
		$Beneficiary = new Beneficiary();
		$filter  = new Application_Filtering();
		$settings = new Application_Settings();
		$ccyList  = $settings->setCurrencyRegistered();

		if (!$this->_request->isPost()) {
			$benef_id = $this->_getParam('benef_id');
			$benef_id = (Zend_Validate::is($benef_id, 'Digits')) ? $benef_id : null;
			if ($benef_id) {
				$resultdata = $this->_db->fetchRow(
					$this->_db->select()
						->from(array('M_BENEFICIARY'))
						->where("BENEFICIARY_ID=?", $benef_id)
				);
				if ($resultdata) {
					$this->view->BENEFICIARY_ID 	= $resultdata['BENEFICIARY_ID'];
					$this->view->ACBENEF_ALIAS 		= $resultdata['BENEFICIARY_ALIAS'];
					$this->view->ACBENEF  			= $resultdata['BENEFICIARY_ACCOUNT'];
					$this->view->ACBENEF_BANKNAME 	= $resultdata['BENEFICIARY_NAME'];
					$this->view->CURR_CODE    		= $resultdata['CURR_CODE'];
					$this->view->ADDRESS    		= $resultdata['BENEFICIARY_ADDRESS'];
					$this->view->ADDRESS2    		= $resultdata['BENEFICIARY_ADDRESS2'];
					$this->view->CITIZENSHIP    	= $resultdata['BENEFICIARY_RESIDENT'];
					$this->view->RESIDENT    		= $resultdata['BENEFICIARY_CITIZENSHIP'];
					$this->view->ACBENEF_EMAIL  	= 	 $resultdata['BENEFICIARY_EMAIL'];
					$this->view->BANK_NAME  		= $resultdata['BANK_NAME'];
					$this->view->CLR_CODE  			= $resultdata['CLR_CODE'];
					$this->view->BANK_CITY  		= $resultdata['BANK_CITY'];
					$this->view->SWIFT_CODE  		= $resultdata['SWIFT_CODE'];
					$this->view->LLD_CATEGORY  		= $resultdata['BENEFICIARY_CATEGORY'];
					$this->view->LLD_BENEIDENTIF  	= $resultdata['BENEFICIARY_ID_TYPE'];
					$this->view->LLD_BENENUMBER  	= $resultdata['BENEFICIARY_ID_NUMBER'];
					$this->view->CITY_CODE  		= $resultdata['BENEFICIARY_CITY_CODE'];
				}

				$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
				$select   = $CustUser->getBeneficiaries($this->view->hasPrivilege('BLBU'));
				$select->where("B.BENEFICIARY_TYPE = ?", (string) $this->_paymenttype["code"]["domestic"]);
				$select->where("B.BENEFICIARY_ID = ?", (string) $benef_id);

				$isEdited = $select->query()->fetchAll();

				if ($isEdited)
					$this->view->isEdited = true;
				else {
					$this->view->isEdited = false;
					$this->view->error = true;
					$this->view->report_msg = 'Error: You have no right to edit beneficiary.';
				}
			} else {
				$error_remark = $this->getErrorRemark('22', 'Beneficiary ID');
				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				$this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
				$this->_redirect('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName());
			}
		} else {
			$ACBENEF_ID		 		= $this->_getParam('BENEFICIARY_ID');
			$BENEFICIARY_ACCOUNT 	= $this->_getParam('ACBENEF');
			$CURR_CODE 				= $this->_getParam('CURR_CODE');
			$BENEFICIARY_ALIAS 		= $this->_getParam('ACBENEF_ALIAS');
			$BENEFICIARY_EMAIL 		= $this->_getParam('ACBENEF_EMAIL');
			$ACBENEF_ADDRESS 		= $this->_request->getParam('ADDRESS');
			$ACBENEF_ADDRESS2 		= $this->_request->getParam('ADDRESS2');
			$ACBENEF_CITIZENSHIP	= $this->_request->getParam('CITIZENSHIP');
			$ACBENEF_RESIDENT	= $this->_request->getParam('RESIDENT');
			$BANK_NAME   			= $this->_request->getParam('BANK_NAME');
			$BANK_CITY   			= $this->_request->getParam('BANK_CITY');
			$CLR_CODE   			= $this->_request->getParam('CLR_CODE');
			$ACBENEF_BANKNAME  		= $this->_request->getParam('ACBENEF_BANKNAME');

			$LLD_CATEGORY  			= $this->_request->getParam('LLD_CATEGORY');
			$LLD_BENEIDENTIF  		= $this->_request->getParam('LLD_BENEIDENTIF');
			$LLD_BENENUMBER  		= $this->_request->getParam('LLD_BENENUMBER');
			$CITY_CODE  			= $this->_request->getParam('CITY_CODE');
			$SWIFT_CODE  			= $this->_request->getParam('SWIFT_CODE');

			//-------------------------------------------------------------START VALIDATION---------------------------------------------------------------------//
			$ACBENEF    = $filter->filter($BENEFICIARY_ACCOUNT, "ACCOUNT_NO");
			$ACBENEF_BANKNAME  = $filter->filter($ACBENEF_BANKNAME, "ACCOUNT_NAME");
			$ACBENEF_ALIAS   = $filter->filter($BENEFICIARY_ALIAS, "ACCOUNT_ALIAS");
			$ACBENEF_EMAIL   = $filter->filter($BENEFICIARY_EMAIL, "EMAIL");
			$ACBENEF_CCY   = $filter->filter($CURR_CODE, "SELECTION");
			$ACBENEF_ADDRESS = $filter->filter($ACBENEF_ADDRESS, "ADDRESS");
			$ACBENEF_ADDRESS2 = $filter->filter($ACBENEF_ADDRESS2, "ADDRESS2");
			$ACBENEF_CITIZENSHIP = $filter->filter($ACBENEF_CITIZENSHIP, "SELECTION");
			$ACBENEF_RESIDENT = $filter->filter($ACBENEF_RESIDENT, "SELECTION");
			$BANK_NAME   = $filter->filter($BANK_NAME, "BANK_NAME");
			$BANK_CITY   = $filter->filter($BANK_CITY, "ADDRESS");
			$CLR_CODE   = $filter->filter($CLR_CODE, "BANK_CODE");

			$LLD_CATEGORY   = $filter->filter($LLD_CATEGORY, "LLD_CATEGORY");
			$LLD_BENEIDENTIF   = $filter->filter($LLD_BENEIDENTIF, "LLD_BENEIDENTIF");
			$LLD_BENENUMBER   = $filter->filter($LLD_BENENUMBER, "LLD_BENENUMBER");
			$CITY_CODE   = $filter->filter($CITY_CODE, "CITY_CODE");
			$SWIFT_CODE   = $filter->filter($SWIFT_CODE, "SWIFT_CODE");

			$paramBene = array(
				"TRANSFER_TYPE"      	=>  'SKN',
				"FROM"      			=> 'B',
				"VALIDATE"     		=> '1',
				"ACBENEF_ID"     		=> $ACBENEF_ID,
				"ACBENEF"     			=> $ACBENEF,
				"ACBENEF_CCY"    		=> $ACBENEF_CCY,
				"ACBENEF_BANKNAME" 	=> $ACBENEF_BANKNAME,
				"ACBENEF_ALIAS"    	=> $ACBENEF_ALIAS,
				"ACBENEF_EMAIL"   		=> $ACBENEF_EMAIL,
				"ACBENEF_ADDRESS1"   	=> $ACBENEF_ADDRESS,
				"ACBENEF_ADDRESS2"   	=> $ACBENEF_ADDRESS2,
				"ACBENEF_CITIZENSHIP"  => $ACBENEF_CITIZENSHIP,
				"ACBENEF_RESIDENT"  => $ACBENEF_RESIDENT,
				"BANK_CODE"    	 	=> $CLR_CODE,
				"_editBeneficiary"   	=> $this->view->hasPrivilege('BEDA'),
				"_beneLinkage"    		=> $privibenelinkage,

				"LLD_CATEGORY"  	=> $LLD_CATEGORY,
				"LLD_BENEIDENTIF"  	=> $LLD_BENEIDENTIF,
				"LLD_BENENUMBER"  	=> $LLD_BENENUMBER,
				"CITY_CODE"  	=> $CITY_CODE,
				"SWIFT_CODE"  	=> $SWIFT_CODE,
			);

			$validateACBENEF = new ValidateAccountBeneficiary($ACBENEF, $this->_custIdLogin, $this->_userIdLogin);
			$validateACBENEF->check($paramBene);

			if (!$validateACBENEF->isError()) {
				$content = array(
					'CUST_ID_LOGIN' 			=> $this->_custIdLogin,
					'USER_ID_LOGIN' 			=> $this->_userIdLogin,
					'BENEFICIARY_ID' 			=> $ACBENEF_ID,
					'BENEFICIARY_ALIAS' 		=> $ACBENEF_ALIAS,
					'BENEFICIARY_ACCOUNT' 		=> $ACBENEF,
					'BENEFICIARY_NAME' 			=> $ACBENEF_BANKNAME,
					'CURR_CODE' 				=> $ACBENEF_CCY,
					'BENEFICIARY_EMAIL' 		=> $ACBENEF_EMAIL,
					'BENEFICIARY_CITIZENSHIP' 	=> $ACBENEF_CITIZENSHIP,
					'BENEFICIARY_RESIDENT' 		=> $ACBENEF_RESIDENT,
					'BENEFICIARY_ADDRESS' 		=> $ACBENEF_ADDRESS,
					'BENEFICIARY_ADDRESS2' 		=> $ACBENEF_ADDRESS2,
					'BANK_NAME' 				=> $BANK_NAME,
					'CLR_CODE' 					=> $CLR_CODE,
					'BANK_CITY'			 		=> $BANK_CITY,
					'BENEFICIARY_TYPE' 			=> 2,
					'BENEFICIARY_CATEGORY'    	=> $LLD_CATEGORY,
					'BENEFICIARY_ID_TYPE'    	=> $LLD_BENEIDENTIF,
					'BENEFICIARY_ID_NUMBER'    	=> $LLD_BENENUMBER,
					'BENEFICIARY_CITY_CODE'    	=> $CITY_CODE,
					'SWIFT_CODE'    			=> $SWIFT_CODE,

				);
				try {

					//-----insert benef--------------
					$this->_db->beginTransaction();
					$update = $Beneficiary->update($content);

					Application_Helper_General::writeLog('BEDA', 'Edit Beneficiary Account ' . $ACBENEF);

					$this->_db->commit();

					//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), null);
					//$this->view->success = true;
					//$msg = $this->getErrorRemark('00','Edit Beneficiary');
					//$this->view->report_msg = $msg;
					$this->setbackURL('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName() . '/index/');
					$this->_redirect('/notification/submited');
				} catch (Exception $e) {
					//rollback changes
					$this->_db->rollBack();

					//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $errorMsg);
					//Application_Log_GeneralLog::technicalLog($e);
				}
			} else {
				$this->view->isEdited = true;
				$this->view->error = true;
				$this->fillParams(null, $ACBENEF_ALIAS, $ACBENEF, $ACBENEF_BANKNAME, $ACBENEF_CCY, $ACBENEF_EMAIL, $ACBENEF_CITIZENSHIP, $ACBENEF_RESIDENT, $ACBENEF_ADDRESS, $ACBENEF_ADDRESS2, $BANK_NAME, $CLR_CODE, $BANK_CITY, $ACBENEF_ID, $LLD_CATEGORY, $LLD_BENEIDENTIF, $LLD_BENENUMBER, $CITY_CODE);
				$docErr = $this->language->_('Error') . ': ' . $validateACBENEF->getErrorMsg();
				$validateACBENEF->__destruct();
				unset($validateACBENEF);
				$this->view->report_msg = $docErr;
				//Zend_Debug::dump($docErr);


				$this->view->BENEFICIARY_ID 	= $ACBENEF_ID;
				//					$this->view->ACBENEF_ALIAS 		= $resultdata['BENEFICIARY_ALIAS'];
				$this->view->ACBENEF  			= $ACBENEF;
				//					$this->view->ACBENEF_BANKNAME 	= $resultdata['BENEFICIARY_NAME'];
				//					$this->view->CURR_CODE    		= $resultdata['CURR_CODE'];
				$this->view->ADDRESS    		= $ACBENEF_ADDRESS;
				$this->view->ADDRESS2    		= $ACBENEF_ADDRESS2;
				$this->view->CITIZENSHIP    	= $ACBENEF_CITIZENSHIP;
				$this->view->RESIDENT    		= $ACBENEF_RESIDENT;
				//					$this->view->ACBENEF_EMAIL  	= 	 $resultdata['BENEFICIARY_EMAIL'];
				$this->view->BANK_NAME  		= $BANK_NAME;
				$this->view->CLR_CODE  			= $CLR_CODE;
				//					$this->view->BANK_CITY  		= $resultdata['BANK_CIT'];

				$this->view->LLD_CATEGORY  		= $LLD_CATEGORY;
				$this->view->LLD_BENEIDENTIF  	= $LLD_BENEIDENTIF;
				$this->view->LLD_BENENUMBER  	= $LLD_BENENUMBER;
				$this->view->CITY_CODE  		= $CITY_CODE;
				$this->view->SWIFT_CODE  		= $SWIFT_CODE;
			}
		}
	}

	public function confirmAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$data = $this->_db->fetchRow(
			$this->_db->select()
				->from(array('C' => 'M_USER'))
				->where("USER_ID = " . $this->_db->quote($this->_userIdLogin))
				->limit(1)
		);

		$this->view->userId				= $data['USER_ID'];

		$sessionNamespace = new Zend_Session_Namespace('beneficiaryAccountAddEdit');
		$content = $sessionNamespace->content;
		$mode = $sessionNamespace->mode;
		$this->view->mode = $mode;
		//	echo '<pre>';

		if ($content['TYPE'] == 'SKN') {

			$type = 'SKN/RTGS';
		} else {
			$type = 'ONLINE';
		}
		$this->view->type = $type;
		// print_r($content);die;
		// 		$this->fillParams(null,$content['BENEFICIARY_ALIAS'],$content['BENEFICIARY_ACCOUNT'],$content['BENEFICIARY_NAME'],$content['CURR_CODE'],$content['BENEFICIARY_EMAIL'],$content['BENEFICIARY_CITIZENSHIP'],$content['BENEFICIARY_RESIDENT'],$content['BENEFICIARY_ADDRESS'],$content['BANK_NAME'],null,null,$content['BENEFICIARY_CATEGORY'],$content['BENEFICIARY_ID_TYPE'],$content['BENEFICIARY_ID_NUMBER'],$content['BENEFICIARY_CITY_CODE']);
		$this->fillParams($content['TYPE'], $content['BENEFICIARY_ALIAS'], $content['BENEFICIARY_ACCOUNT'], $content['BENEFICIARY_NAME'], $content['CURR_CODE'], $content['BENEFICIARY_EMAIL'], $content['BENEFICIARY_CITIZENSHIP'], $content['BENEFICIARY_RESIDENT'], $content['BENEFICIARY_ADDRESS'], $content['BENEFICIARY_ADDRESS2'], $content['BANK_NAME'], null, null, null, $content['BENEFICIARY_TYPE'], $content['BENEFICIARY_CATEGORY'], $content['BENEFICIARY_ID_TYPE'], $content['BENEFICIARY_ID_NUMBER'], $content['BENEFICIARY_CITY_CODE'], $content['SWIFT_CODE']);


		if ($this->_request->isPost()) {
			if ($this->_getParam('submit1') == $this->language->_('Back')) {
				if ($mode == 'Add') {
					$this->_redirect('/predefinedbeneficiary/domesticcct/addbeneficiary/isback/1');
				} elseif ($mode == 'Edit') {
					$this->_redirect('/predefinedbeneficiary/domesticcct/editbeneficiary/benef_id/' . $content['BENEFICIARY_ID'] . '/isback/1');
				}
			} else {
				try {
					//-----insert benef--------------
					$this->_db->beginTransaction();
					$Beneficiary = new Beneficiary();
					//echo '<pre>';
					//print_r($content);die;
					$tempbenename = $content['BENEFICIARY_NAME'];
					$content['BENEFICIARY_NAME'] = $content['BENEFICIARY_NEWNAME'];
					$content['BENEFICIARY_NEWNAME'] = $tempbenename;
					$content['BENEFICIARY_NAME_CEK'] = $tempbenename;

					//var_dump($content);die;
					$add = $Beneficiary->add($content);
					Application_Helper_General::writeLog('BADA', 'Add Destination Account ' . $content['BENEFICIARY_ACCOUNT']);
					$this->_db->commit();
					unset($_SESSION['beneficiaryAccountAddEdit']);
					$this->setbackURL('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName() . '/index/');
					$this->_redirect('/notification/submited');
				} catch (Exception $e) {
					//rollback changes
					$this->_db->rollBack();
				}
			}
		}
	}

	public function editemailAction()
	{
		$benef_id_decrypted = $this->_getParam("benef_id");

		// decrypt benef id -------------------------------------------------------
		$setting = new Settings();
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$pw_hash = md5($enc_salt . $enc_pass);
		$rand = $this->_userIdLogin . date('dHis') . $pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;

		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');
		$password = $sessionNamespace->token;
		$this->view->token = $sessionNamespace->token;


		$AESMYSQL = new Crypt_AESMYSQL();

		$BG_NUMBER 	= urldecode($benef_id_decrypted);

		$BG_NUMBER = $AESMYSQL->decrypt($BG_NUMBER, $password);
		$benef_id_decrypted = $BG_NUMBER;

		// ------------------------------------------------------------------------------

		$this->_helper->layout()->setLayout('newlayout');
		if (!$this->_request->isPost()) {
			$benef_id = $benef_id_decrypted;
			$benef_id = (Zend_Validate::is($benef_id, 'Digits')) ? $benef_id : null;
			if ($benef_id) {
				$resultdata = $this->_db->fetchRow(
					$this->_db->select()
						->from(array('M_BENEFICIARY'))
						->where("BENEFICIARY_ID=?", $benef_id)
				);
				if ($resultdata) {
					$this->view->BENEFICIARY_ID 	= $this->_getParam("benef_id");
					$this->view->ACBENEF_ALIAS 		= $resultdata['BENEFICIARY_ALIAS'];
					$this->view->ACBENEF  			= $resultdata['BENEFICIARY_ACCOUNT'];
					$this->view->ACBENEF_BANKNAME 	= $resultdata['BENEFICIARY_NAME'];
					$this->view->CURR_CODE    		= $resultdata['CURR_CODE'];
					$this->view->ADDRESS    		= $resultdata['BENEFICIARY_ADDRESS'];
					$this->view->CITIZENSHIP    	= $resultdata['BENEFICIARY_CITIZENSHIP'];
					$this->view->RESIDENT    		= $resultdata['BENEFICIARY_RESIDENT'];
					$this->view->ACBENEF_EMAIL  	= $resultdata['BENEFICIARY_EMAIL'];
					$this->view->BANK_NAME  		= $resultdata['BANK_NAME'];
					$this->view->BENEF_TYPE  		= $resultdata['BENEFICIARY_TYPE'];
					$this->view->CLR_CODE  	= $resultdata['CLR_CODE'];
					$this->view->BENEFICIARY_BANK_CODE  	= $resultdata['BANK_CODE'];
					$this->view->BANK_CITY  		= $resultdata['BANK_CITY'];
					// print_r($resultdata);die;
					$CITY_CODE 		= $resultdata['BENEFICIARY_CITY_CODE'];
					$LLD_CATEGORY 	= $resultdata['BENEFICIARY_CATEGORY'];

					$CITY_CODEGet = (!empty($CITY_CODE) ? $CITY_CODE : '');
					$select = $this->_db->select()
						->from(array('A' => 'M_CITY'), array('*'));
					$select->where('A.CITY_CODE = ?', $CITY_CODEGet);
					$arr = $this->_db->fetchall($select);

					// 9. Create LLD string
					$settings 			= new Application_Settings();
					$LLD_array 			= array();
					$LLD_DESC_arrayCat 	= array();
					$lldTypeArr  		= $settings->getLLDDOMType();

					if (!empty($LLD_CATEGORY)) {
						$lldCategoryArr  	= $settings->getLLDDOMCategory();
						$LLD_array["CT"] 	= $LLD_CATEGORY;
						$LLD_CATEGORY_POST  = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
					}

					$this->view->LLD_CATEGORY  		= $LLD_CATEGORY_POST;
					$this->view->LLD_BENEIDENTIF  	= $resultdata['BENEFICIARY_ID_TYPE'];
					$this->view->LLD_BENENUMBER  	= $resultdata['BENEFICIARY_ID_NUMBER'];
					$this->view->CITY_CODE  		= $arr[0]['CITY_NAME'];
				}
			} else {
				$error_remark = $this->getErrorRemark('22', 'Beneficiary ID');

				$this->_helper->getHelper('FlashMessenger')->addMessage('F');
				$this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
				$this->_redirect('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName());
			}
		} else {

			$get_request = $this->_request->getParams();
			$get_id = $get_request["BENEFICIARY_ID"];

			$get_id = urldecode($get_id);

			$get_id = $AESMYSQL->decrypt($get_id, $password);
			$get_request["BENEFICIARY_ID"] = $get_id;

			$filters = array(
				'BENEFICIARY_ID' => array('StringTrim', 'StripTags'),
				'ACBENEF_EMAIL' 	=> array('StringTrim', 'StripTags')
			);

			$validators = array(
				'BENEFICIARY_ID' => array(
					'NotEmpty',
					'Digits',
					'messages' => array(
						'Error: Beneficiary ID cannot be left blank.',
						'Error: Wrong Format Beneficiary ID.'
					)
				),
				'ACBENEF_EMAIL' => array(	//new SGO_Validate_EmailAddress(),
					'allowEmpty' => true,
					//'messages' => array(
					//	'Error: Wrong Format Email Address of {$email}. Please correct it.')
				)
			);

			$zf_filter_input = new Zend_Filter_Input($filters, $validators, $get_request, $this->_optionsValidator);
			if ($zf_filter_input->isValid()) {
				$error = false;

				if (trim($zf_filter_input->ACBENEF_EMAIL) != "") {
					$arr_email = explode(";", $zf_filter_input->ACBENEF_EMAIL);
					foreach ($arr_email as $value) {
						if (Zend_Validate::is(trim($value), 'EmailAddress') == false) {
							$error = true;
							break;
						}
					}
				}

				if (!$error) {
					try {
						//-----edit email--------------
						$this->_db->beginTransaction();

						$Beneficiary = new Beneficiary();
						$Beneficiary->editEmail($zf_filter_input->BENEFICIARY_ID, $zf_filter_input->ACBENEF_EMAIL);

						$beneAcct = $this->_db->fetchOne('SELECT BENEFICIARY_ACCOUNT FROM M_BENEFICIARY WHERE BENEFICIARY_ID=' . $zf_filter_input->BENEFICIARY_ID);

						Application_Helper_General::writeLog('BEDA', 'Edit Email Beneficiary Account ' . $beneAcct);

						$this->_db->commit();

						//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), null);
						//$this->view->success = true;
						//$msg = $this->getErrorRemark('00','Edit Email');
						//$this->view->report_msg = $msg;
						$this->setbackURL('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName() . '/index/');
						$this->_redirect('/notification/submited');
					} catch (Exception $e) {
						//rollback changes
						$this->_db->rollBack();
						$this->fillParam($zf_filter_input);
						$errorMsg = $this->getErrorRemark('82');
						$this->_helper->getHelper('FlashMessenger')->addMessage('F');
						$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);

						//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $errorMsg);
						//Application_Log_GeneralLog::technicalLog($e);
					}
				} else {
					$this->view->error = true;
					$resultdata = $this->_db->fetchRow(
						$this->_db->select()
							->from(array('M_BENEFICIARY'))
							->where("BENEFICIARY_ID=?", $zf_filter_input->BENEFICIARY_ID)
					);
					if ($resultdata) {
						$this->view->BENEFICIARY_ID 	= $resultdata['BENEFICIARY_ID'];
						$this->view->ACBENEF_ALIAS 		= $resultdata['BENEFICIARY_ALIAS'];
						$this->view->ACBENEF  			= $resultdata['BENEFICIARY_ACCOUNT'];
						$this->view->ACBENEF_BANKNAME 	= $resultdata['BENEFICIARY_NAME'];
						$this->view->CURR_CODE    		= $resultdata['CURR_CODE'];
						$this->view->ADDRESS    		= $resultdata['BENEFICIARY_ADDRESS'];
						$this->view->CITIZENSHIP    	= $resultdata['BENEFICIARY_CITIZENSHIP'];
						$this->view->RESIDENT    		= $resultdata['BENEFICIARY_RESIDENT'];
						$this->view->BANK_NAME  		= $resultdata['BANK_NAME'];
						$this->view->BENEF_TYPE  		= $resultdata['BENEFICIARY_TYPE'];
						$this->view->CLR_CODE  	= $resultdata['CLR_CODE'];
						$this->view->BANK_CITY  		= $resultdata['BANK_CITY'];

						$CITY_CODE 		= $resultdata['BENEFICIARY_CITY_CODE'];
						$LLD_CATEGORY 	= $resultdata['BENEFICIARY_CATEGORY'];

						$CITY_CODEGet = (!empty($CITY_CODE) ? $CITY_CODE : '');
						$select = $this->_db->select()
							->from(array('A' => 'M_CITY'), array('*'));
						$select->where('A.CITY_CODE = ?', $CITY_CODEGet);
						$arr = $this->_db->fetchall($select);

						// 9. Create LLD string
						$settings 			= new Application_Settings();
						$LLD_array 			= array();
						$LLD_DESC_arrayCat 	= array();
						$lldTypeArr  		= $settings->getLLDDOMType();

						if (!empty($LLD_CATEGORY)) {
							$lldCategoryArr  	= $settings->getLLDDOMCategory();
							$LLD_array["CT"] 	= $LLD_CATEGORY;
							$LLD_CATEGORY_POST  = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
						}

						$this->view->LLD_CATEGORY  		= $LLD_CATEGORY_POST;
						$this->view->LLD_BENEIDENTIF  	= $resultdata['BENEFICIARY_ID_TYPE'];
						$this->view->LLD_BENENUMBER  	= $resultdata['BENEFICIARY_ID_NUMBER'];
						$this->view->CITY_CODE  		= $arr[0]['CITY_NAME'];
					}
					$this->view->ACBENEF_EMAIL = ($zf_filter_input->isValid('ACBENEF_EMAIL')) ? $zf_filter_input->ACBENEF_EMAIL : $this->_getParam('ACBENEF_EMAIL');
					$docErr = 'Error: Invalid Format - Email Address.';
					$this->view->report_msg = $docErr;
					//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $docErr);
				}
			} else {
				$this->view->error = true;
				$resultdata = $this->_db->fetchRow(
					$this->_db->select()
						->from(array('M_BENEFICIARY'))
						->where("BENEFICIARY_ID=?", $zf_filter_input->BENEFICIARY_ID)
				);
				if ($resultdata) {
					$this->view->BENEFICIARY_ID 	= $resultdata['BENEFICIARY_ID'];
					$this->view->ACBENEF_ALIAS 		= $resultdata['BENEFICIARY_ALIAS'];
					$this->view->ACBENEF  			= $resultdata['BENEFICIARY_ACCOUNT'];
					$this->view->ACBENEF_BANKNAME 	= $resultdata['BENEFICIARY_NAME'];
					$this->view->CURR_CODE    		= $resultdata['CURR_CODE'];
					$this->view->ADDRESS    		= $resultdata['BENEFICIARY_ADDRESS'];
					$this->view->CITIZENSHIP    	= $resultdata['BENEFICIARY_CITIZENSHIP'];
					$this->view->RESIDENT    		= $resultdata['BENEFICIARY_RESIDENT'];
					$this->view->BANK_NAME  		= $resultdata['BANK_NAME'];
					$this->view->BENEF_TYPE  		= $resultdata['BENEFICIARY_TYPE'];
					$this->view->CLR_CODE  	= $resultdata['CLR_CODE'];
					$this->view->BANK_CITY  		= $resultdata['BANK_CITY'];

					$CITY_CODE 		= $resultdata['BENEFICIARY_CITY_CODE'];
					$LLD_CATEGORY 	= $resultdata['BENEFICIARY_CATEGORY'];

					$modelCity 			= new predefinedbeneficiary_Model_Predefinedbeneficiary();
					$CITY_CODEGet 		= (!empty($CITY_CODE) ? $CITY_CODE : '');
					$arr 				= $modelCity->getCityCode($CITY_CODEGet);

					// 9. Create LLD string
					$settings 			= new Application_Settings();
					$LLD_array 			= array();
					$LLD_DESC_arrayCat 	= array();
					$lldTypeArr  		= $settings->getLLDDOMType();

					if (!empty($LLD_CATEGORY)) {
						$lldCategoryArr  	= $settings->getLLDDOMCategory();
						$LLD_array["CT"] 	= $LLD_CATEGORY;
						$LLD_CATEGORY_POST  = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
					}

					$this->view->LLD_CATEGORY  		= $LLD_CATEGORY_POST;
					$this->view->LLD_BENEIDENTIF  	= $resultdata['BENEFICIARY_ID_TYPE'];
					$this->view->LLD_BENENUMBER  	= $resultdata['BENEFICIARY_ID_NUMBER'];
					$this->view->CITY_CODE  		= $arr[0]['CITY_NAME'];
				}
				$this->view->ACBENEF_EMAIL = ($zf_filter_input->isValid('ACBENEF_EMAIL')) ? $zf_filter_input->ACBENEF_EMAIL : $this->_getParam('ACBENEF_EMAIL');
				$docErr = $this->displayError($zf_filter_input->getMessages());
				$this->view->report_msg = $docErr;
				//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $docErr);
			}

			$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
			if (count($temp) > 1) {
				if ($temp[0] == 'F' || $temp[0] == 'S') {
					if ($temp[0] == 'F')
						$this->view->error = 1;
					else
						$this->view->success = 1;
					$msg = '';
					unset($temp[0]);
					foreach ($temp as $value) {
						if (!is_array($value))
							$value = array($value);
						$msg .= $this->view->formErrors($value);
					}
					$this->view->report_msg = $msg;
				}
			}
		}
	}

	public function deleteAction()
	{
		// echo "<pre>";
		// print_r($this->_request->getParams());die;
		$this->_helper->layout()->setLayout('newlayout');
		if (!$this->_request->isPost()) {
			$benef_id = $this->_getParam('benef_id');
			// print_r($benef_id);die;
			$checkmulti = explode(',', $benef_id);
			// var_dump($checkmulti);die;
			if (empty($checkmulti[1])) {
				$this->view->multiview = false;
				$benef_id = (Zend_Validate::is($benef_id, 'Digits')) ? $benef_id : null;

				if ($benef_id) {
					$resultdata = $this->_db->fetchRow(
						$this->_db->select()
							->from(array('M_BENEFICIARY'))
							->where("BENEFICIARY_ID=?", $benef_id)
					);
					if ($resultdata) {
						$this->view->BENEFICIARY_ID 	= $resultdata['BENEFICIARY_ID'];
						$this->view->ACBENEF_ALIAS 		= $resultdata['BENEFICIARY_ALIAS'];
						$this->view->ACBENEF  			= $resultdata['BENEFICIARY_ACCOUNT'];
						$this->view->ACBENEF_NAME		= $resultdata['BENEFICIARY_NAME'];
						$this->view->CURR_CODE    		= $resultdata['CURR_CODE'];
						$this->view->CITIZENSHIP    	= $resultdata['BENEFICIARY_CITIZENSHIP'];
						$this->view->RESIDENT    		= $resultdata['BENEFICIARY_RESIDENT'];
						$this->view->ADDRESS    		= $resultdata['BENEFICIARY_ADDRESS'];
						$this->view->BANK_NAME  		= $resultdata['BANK_NAME'];
						$this->view->CLR_CODE  	= $resultdata['CLR_CODE'];
						$this->view->BANK_CITY  		= $resultdata['BANK_CITY'];
						$this->view->ACBENEF_EMAIL  	= $resultdata['BENEFICIARY_EMAIL'];
						$this->view->STATUS		  		= $resultdata['BENEFICIARY_ISAPPROVE'];
						$this->view->BENEFICIARY_TYPE	= $resultdata['BENEFICIARY_TYPE'];

						$this->view->BENEFICIARY_ID_NUMBER	= $resultdata['BENEFICIARY_ID_NUMBER'];
						$this->view->BENEFICIARY_ID_TYPE	= $resultdata['BENEFICIARY_ID_TYPE'];
						$this->view->LLD_CATEGORY	= $resultdata['BENEFICIARY_CATEGORY'];
						// print_r($resultdata);die;
						$CITY_CODE 		= $resultdata['BENEFICIARY_CITY_CODE'];
						$LLD_CATEGORY 	= $resultdata['BENEFICIARY_CATEGORY'];

						$CITY_CODEGet = (!empty($CITY_CODE) ? $CITY_CODE : '');
						$select = $this->_db->select()
							->from(array('A' => 'M_CITY'), array('*'));
						$select->where('A.CITY_CODE = ?', $CITY_CODEGet);
						$arr = $this->_db->fetchall($select);

						// 9. Create LLD string
						$settings 			= new Application_Settings();
						$LLD_array 			= array();
						$LLD_DESC_arrayCat 	= array();
						$lldTypeArr  		= $settings->getLLDDOMType();

						if (!empty($LLD_CATEGORY)) {
							$lldCategoryArr  	= $settings->getLLDDOMCategory();
							$LLD_array["CT"] 	= $LLD_CATEGORY;
							$LLD_CATEGORY_POST  = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
						}

						$this->view->LLD_CATEGORY  		= $LLD_CATEGORY_POST;
						$this->view->LLD_BENEIDENTIF  	= $resultdata['BENEFICIARY_ID_TYPE'];
						$this->view->LLD_BENENUMBER  	= $resultdata['BENEFICIARY_ID_NUMBER'];
						$this->view->CITY_CODE  		= $arr[0]['CITY_NAME'];
					}
				} else {
					$error_remark = 'Error: Beneficiary ID cannot be left blank.';

					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($error_remark);
				}
			} else {
				$this->view->multiview = true;

				$fields = array(
					/*'alias'  => array('field' => 'BENEFICIARY_ALIAS',
											   'label' => $this->language->_('Alias Name'),
											   'sortable' => true),*/
					'benef_acct'  => array(
						'field' => 'BENEFICIARY_ACCOUNT',
						'label' => $this->language->_('Beneficiary Account'),
						'sortable' => true
					),
					'benef_name'  => array(
						'field' => 'BENEFICIARY_NAME',
						'label' => $this->language->_('Beneficiary Account Name'),
						'sortable' => true
					),
					'email'  => array(
						'field' => 'BENEFICIARY_EMAIL',
						'label' => $this->language->_('Email Address'),
						'sortable' => true
					),
					'ccy'   => array(
						'field'    => 'CURR_CODE',
						'label'    => $this->language->_('CCY'),
						'sortable' => true
					),
					// 'favorite'   => array('field'    => 'ISFAVORITE',
					// 					  'label'    => $this->language->_('Favorite'),
					// 					  'sortable' => true),
					// 'checked'   => array('field'    => 'BENEFICIARY_BANKSTATUS_disp',
					// 					  'label'    => $this->language->_('Checked by Bank'),
					// 					  'sortable' => true),

					'date'   => array(
						'field'    => 'BENEFICIARY_CREATED',
						'label'    => $this->language->_('Created Date'),
						'sortable' => true
					),

					'status'   => array(
						'field'    => 'BENEFICIARY_ISAPPROVE',
						'label'    => $this->language->_('Status'),
						'sortable' => true
					)

					// 'delete'   => array('field'    => 'BENEFICIARY_ISREQUEST_DELETE',
					// 					  'label'    => $this->language->_('Delete'),
					// 					  'sortable' => false)
				);
				$this->view->fields = $fields;

				$select = $this->_db->select()
					->from(array('M_BENEFICIARY'))
					->where("BENEFICIARY_ID in (?)", $checkmulti);
				$this->paging($select);


				$resultdata = $this->_db->fetchAll(
					$this->_db->select()
						->from(array('M_BENEFICIARY'))
						->where("BENEFICIARY_ID in (?)", $checkmulti)
				);
				// echo $resultdata;die;

				// print_r($resultdata);die;
				if ($resultdata) {
					$this->view->BENEFICIARY_ID 	= $resultdata['BENEFICIARY_ID'];
					$this->view->ACBENEF_ALIAS 		= $resultdata['BENEFICIARY_ALIAS'];
					$this->view->ACBENEF  			= $resultdata['BENEFICIARY_ACCOUNT'];
					$this->view->ACBENEF_NAME		= $resultdata['BENEFICIARY_NAME'];
					$this->view->CURR_CODE    		= $resultdata['CURR_CODE'];
					$this->view->CITIZENSHIP    	= $resultdata['BENEFICIARY_CITIZENSHIP'];
					$this->view->RESIDENT    		= $resultdata['BENEFICIARY_RESIDENT'];
					$this->view->ADDRESS    		= $resultdata['BENEFICIARY_ADDRESS'];
					$this->view->BANK_NAME  		= $resultdata['BANK_NAME'];
					$this->view->CLR_CODE  	= $resultdata['CLR_CODE'];
					$this->view->BANK_CITY  		= $resultdata['BANK_CITY'];
					$this->view->ACBENEF_EMAIL  	= $resultdata['BENEFICIARY_EMAIL'];
					$this->view->STATUS		  		= $resultdata['BENEFICIARY_ISAPPROVE'];

					$CITY_CODE 		= $resultdata['BENEFICIARY_CITY_CODE'];
					$LLD_CATEGORY 	= $resultdata['BENEFICIARY_CATEGORY'];

					$CITY_CODEGet = (!empty($CITY_CODE) ? $CITY_CODE : '');
					$select = $this->_db->select()
						->from(array('A' => 'M_CITY'), array('*'));
					$select->where('A.CITY_CODE = ?', $CITY_CODEGet);
					$arr = $this->_db->fetchall($select);

					// 9. Create LLD string
					$settings 			= new Application_Settings();
					$LLD_array 			= array();
					$LLD_DESC_arrayCat 	= array();
					$lldTypeArr  		= $settings->getLLDDOMType();

					if (!empty($LLD_CATEGORY)) {
						$lldCategoryArr  	= $settings->getLLDDOMCategory();
						$LLD_array["CT"] 	= $LLD_CATEGORY;
						$LLD_CATEGORY_POST  = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
					}

					$this->view->LLD_CATEGORY  		= $LLD_CATEGORY_POST;
					$this->view->LLD_BENEIDENTIF  	= $resultdata['BENEFICIARY_ID_TYPE'];
					$this->view->LLD_BENENUMBER  	= $resultdata['BENEFICIARY_ID_NUMBER'];
					$this->view->CITY_CODE  		= $arr[0]['CITY_NAME'];
				}
			}
		} else {

			//	echo "<pre>";
			// 	print_r($this->_getParam('BENEFICIARY_ID')[1]);
			//var_dump($this->_getParam('BENEFICIARY_ID')[1]);die;

			if ($this->_getParam('BENEFICIARY_ID')[1] == 1) {


				$filters = array(
					'BENEFICIARY_ID' => array('StringTrim', 'StripTags'),
					'STATUS' => array('StringTrim', 'StripTags')
				);

				$validators = array(
					'BENEFICIARY_ID' => array(
						'NotEmpty',
						'Digits',
						'messages' => array(
							'Error: Beneficiary ID cannot be left blank.',
							'Error: Wrong Format Beneficiary ID.'
						)
					),
					'STATUS' 		=> array(
						'NotEmpty',
						'Digits',
						'messages' => array(
							'Error: Status cannot be left blank.',
							'Error: Wrong Format Status.'
						)
					)
				);

				$zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);
				if ($zf_filter_input->isValid()) {
					try {

						//-----delete--------------
						$this->_db->beginTransaction();
						$Beneficiary = new Beneficiary();
						if ($zf_filter_input->STATUS)
							$Beneficiary->suggestDelete($zf_filter_input->BENEFICIARY_ID);
						else
							$Beneficiary->delete($zf_filter_input->BENEFICIARY_ID);
						$this->_db->commit();

						//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), null);
						$this->setbackURL('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName() . '/index/');
						$this->_redirect('/notification/submited');
					} catch (Exception $e) {
						//rollback changes
						$this->_db->rollBack();
						$this->fillParam($zf_filter_input);
						$errorMsg = $this->getErrorRemark('82');
						$this->_helper->getHelper('FlashMessenger')->addMessage('F');
						$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);

						//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $errorMsg);
						//Application_Log_GeneralLog::technicalLog($e);
					}
				} else {

					$this->view->error = true;
					$resultdata = $this->_db->fetchRow(
						$this->_db->select()
							->from(array('M_BENEFICIARY'))
							->where("BENEFICIARY_ID=?", $zf_filter_input->BENEFICIARY_ID)
					);
					if ($resultdata) {
						$this->view->BENEFICIARY_ID 	= $resultdata['BENEFICIARY_ID'];
						$this->view->ACBENEF_ALIAS 		= $resultdata['BENEFICIARY_ALIAS'];
						$this->view->ACBENEF  			= $resultdata['BENEFICIARY_ACCOUNT'];
						$this->view->ACBENEF_NAME		= $resultdata['BENEFICIARY_NAME'];
						$this->view->CURR_CODE    		= $resultdata['CURR_CODE'];
						$this->view->CITIZENSHIP    	= $resultdata['BENEFICIARY_CITIZENSHIP'];
						$this->view->RESIDENT    		= $resultdata['BENEFICIARY_RESIDENT'];
						$this->view->ADDRESS    		= $resultdata['BENEFICIARY_ADDRESS'];
						$this->view->BANK_NAME  		= $resultdata['BANK_NAME'];
						$this->view->CLR_CODE  	= $resultdata['CLR_CODE'];
						$this->view->BANK_CITY  		= $resultdata['BANK_CITY'];
						$this->view->ACBENEF_EMAIL  	= $resultdata['BENEFICIARY_EMAIL'];
						$this->view->STATUS		  		= $resultdata['BENEFICIARY_ISAPPROVE'];

						$CITY_CODE 		= $resultdata['BENEFICIARY_CITY_CODE'];
						$LLD_CATEGORY 	= $resultdata['BENEFICIARY_CATEGORY'];

						$CITY_CODEGet = (!empty($CITY_CODE) ? $CITY_CODE : '');
						$select = $this->_db->select()
							->from(array('A' => 'M_CITY'), array('*'));
						$select->where('A.CITY_CODE = ?', $CITY_CODEGet);
						$arr = $this->_db->fetchall($select);

						// 9. Create LLD string
						$settings 			= new Application_Settings();
						$LLD_array 			= array();
						$LLD_DESC_arrayCat 	= array();
						$lldTypeArr  		= $settings->getLLDDOMType();

						if (!empty($LLD_CATEGORY)) {
							$lldCategoryArr  	= $settings->getLLDDOMCategory();
							$LLD_array["CT"] 	= $LLD_CATEGORY;
							$LLD_CATEGORY_POST  = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
						}

						$this->view->LLD_CATEGORY  		= $LLD_CATEGORY_POST;
						$this->view->LLD_BENEIDENTIF  	= $resultdata['BENEFICIARY_ID_TYPE'];
						$this->view->LLD_BENENUMBER  	= $resultdata['BENEFICIARY_ID_NUMBER'];
						$this->view->CITY_CODE  		= $arr[0]['CITY_NAME'];
					}
					$docErr = $this->displayError($zf_filter_input->getMessages());
					$this->view->report_msg = $docErr;
					//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $docErr);
				}
			} else {
				// die('here');
				try {
					//-----delete--------------
					$this->_db->beginTransaction();
					$Beneficiary = new Beneficiary();

					$id = $this->_getParam('BENEFICIARY_ID');

					if (count($id) == 1) {
						$Beneficiary->suggestDelete($id);
					} else {
						foreach ($this->_getParam('BENEFICIARY_ID') as $key => $value) {

							if ($this->_getParam('STATUS')[$key])
								$Beneficiary->suggestDelete($value);
							else
								$Beneficiary->delete($value);
						}
					}

					$this->_db->commit();

					//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), null);
					$this->setbackURL('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName() . '/index/');
					$this->_redirect('/notification/submited');
				} catch (Exception $e) {
					//rollback changes
					$this->_db->rollBack();
					$this->fillParam($zf_filter_input);
					$errorMsg = $this->getErrorRemark('82');
					$this->_helper->getHelper('FlashMessenger')->addMessage('F');
					$this->_helper->getHelper('FlashMessenger')->addMessage($errorMsg);

					//$this->backendLog('N', $this->_moduleDB, null, $this->_getAllParams(), $errorMsg);
					//Application_Log_GeneralLog::technicalLog($e);
				}
			}
		}

		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if (count($temp) > 1) {
			if ($temp[0] == 'F' || $temp[0] == 'S') {
				if ($temp[0] == 'F')
					$this->view->error = 1;
				else
					$this->view->success = 1;
				$msg = '';
				unset($temp[0]);
				foreach ($temp as $value) {
					if (!is_array($value))
						$value = array($value);
					$msg .= $this->view->formErrors($value);
				}
				$this->view->report_msg = $msg;
			}
		}
		Application_Helper_General::writeLog('BADA', 'Delete Beneficiary Account ' . $benef_id);
	}

	private function fillParam($zf_filter_input)
	{
		if (isset($zf_filter_input->BENEFICIARY_ID)) $this->view->BENEFICIARY_ID = ($zf_filter_input->isValid('BENEFICIARY_ID')) ? $zf_filter_input->BENEFICIARY_ID : $this->_getParam('BENEFICIARY_ID');
		$this->view->ACBENEF_ALIAS = ($zf_filter_input->isValid('ACBENEF_ALIAS')) ? $zf_filter_input->ACBENEF_ALIAS : $this->_getParam('ACBENEF_ALIAS');
		$this->view->ACBENEF = ($zf_filter_input->isValid('ACBENEF')) ? $zf_filter_input->ACBENEF : $this->_getParam('ACBENEF');
		$this->view->ACBENEF_BANKNAME = ($zf_filter_input->isValid('ACBENEF_BANKNAME')) ? $zf_filter_input->ACBENEF_BANKNAME : $this->_getParam('ACBENEF_BANKNAME');
		$this->view->CURR_CODE = ($zf_filter_input->isValid('CURR_CODE')) ? $zf_filter_input->CURR_CODE : $this->_getParam('CURR_CODE');
		$this->view->ADDRESS = ($zf_filter_input->isValid('ADDRESS')) ? $zf_filter_input->ADDRESS : $this->_getParam('ADDRESS');
		$this->view->CITIZENSHIP = ($zf_filter_input->isValid('CITIZENSHIP')) ? $zf_filter_input->CITIZENSHIP : $this->_getParam('CITIZENSHIP');
		$this->view->RESIDENT = ($zf_filter_input->isValid('RESIDENT')) ? $zf_filter_input->RESIDENT : $this->_getParam('RESIDENT');
		$this->view->ACBENEF_EMAIL = ($zf_filter_input->isValid('ACBENEF_EMAIL')) ? $zf_filter_input->ACBENEF_EMAIL : $this->_getParam('ACBENEF_EMAIL');
		$this->view->BANK_NAME = ($zf_filter_input->isValid('BANK_NAME')) ? $zf_filter_input->BANK_NAME : $this->_getParam('BANK_NAME');
		$this->view->CLR_CODE = ($zf_filter_input->isValid('CLR_CODE')) ? $zf_filter_input->CLR_CODE : $this->_getParam('CLR_CODE');
		$this->view->BANK_CITY = ($zf_filter_input->isValid('BANK_CITY')) ? $zf_filter_input->BANK_CITY : $this->_getParam('BANK_CITY');
	}

	//	private function fillParams($benefType,$ACBENEF_ALIAS,$ACBENEF,$ACBENEF_BANKNAME,$ACBENEF_CCY,$ACBENEF_EMAIL,$ACBENEF_CITIZENSHIP,$ACBENEF_RESIDENT,$ACBENEF_ADDRESS,$BANK_NAME,$CLR_CODE,$BANK_CITY,$BENEFICIARY_ID=null,$LLD_CATEGORY,$LLD_BENEIDENTIF,$LLD_BENENUMBER,$CITY_CODE)
	//	{
	//		$CITY_CODEGet = (!empty($CITY_CODE)?$CITY_CODE:'');
	//		$select = $this->_db->select()
	//				->from(array('A' => 'M_CITY'),array('*'));
	//		$select->where('A.CITY_CODE = ?',$CITY_CODEGet);
	//		$arr = $this->_db->fetchall($select);
	//
	//		// 9. Create LLD string
	//		$settings 			= new Application_Settings();
	//		$LLD_array 			= array();
	//		$LLD_DESC_arrayCat 	= array();
	//		$lldTypeArr  		= $settings->getLLDDOMType();
	//
	//		if (!empty($LLD_CATEGORY))
	//		{
	//			$lldCategoryArr  	= $settings->getLLDDOMCategory();
	//			$LLD_array["CT"] 	= $LLD_CATEGORY;
	//			$LLD_CATEGORY_POST = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
	//		}
	//		Zend_Debug::dump($LLD_CATEGORY);
	//
	//		if($BENEFICIARY_ID) $this->view->BENEFICIARY_ID = $BENEFICIARY_ID;
	//		$this->view->ACBENEF_ALIAS = $ACBENEF_ALIAS;
	//		$this->view->ACBENEF = $ACBENEF;
	//		$this->view->ACBENEF_BANKNAME = $ACBENEF_BANKNAME;
	//		$this->view->CURR_CODE = $ACBENEF_CCY;
	//		$this->view->ADDRESS = $ACBENEF_ADDRESS;
	// 		$this->view->CITIZENSHIP = $ACBENEF_RESIDENT;
	//		$this->view->RESIDENT = $ACBENEF_CITIZENSHIP;
	//		$this->view->ACBENEF_EMAIL = $ACBENEF_EMAIL;
	//		$this->view->BANK_NAME = $BANK_NAME;
	//		$this->view->CLR_CODE = $CLR_CODE;
	//		//$this->view->BANK_CITY = $BANK_CITY;
	////		$this->view->BENEFICIARY_TYPE = $BENEFICIARY_TYPE;
	//
	//		$this->view->LLD_CATEGORY = $LLD_CATEGORY;;
	//		$this->view->LLD_BENEIDENTIF = $LLD_BENEIDENTIF;
	//		$this->view->LLD_BENENUMBER = $LLD_BENENUMBER;
	//		$this->view->CITY_CODE = $CITY_CODE;

	//
	//		$this->view->CITY_NAME = $arr[0]['CITY_NAME'];
	//		$this->view->CATEGORY_NAME_LLD = $LLD_CATEGORY_POST;
	//	}

	private function fillParams($benefType, $ACBENEF_ALIAS, $ACBENEF, $ACBENEF_BANKNAME, $ACBENEF_CCY, $ACBENEF_EMAIL, $ACBENEF_CITIZENSHIP, $ACBENEF_RESIDENT, $ACBENEF_ADDRESS, $ACBENEF_ADDRESS2, $BANK_NAME, $CLR_CODE, $BANK_CITY, $BENEFICIARY_ID = null, $BENEFICIARY_TYPE, $LLD_CATEGORY, $LLD_BENEIDENTIF, $LLD_BENENUMBER, $CITY_CODE = null)
	//	private function fillParams($benefType,$ACBENEF_ALIAS,$ACBENEF,$ACBENEF_BANKNAME,$ACBENEF_CCY,$ACBENEF_EMAIL,$ACBENEF_CITIZENSHIP,$ACBENEF_RESIDENT,$ACBENEF_ADDRESS,$BANK_NAME,$CLR_CODE,$BANK_CITY,$BENEFICIARY_ID=null,$LLD_CATEGORY,$LLD_BENEIDENTIF,$LLD_BENENUMBER,$CITY_CODE)

	{
		$CITY_CODEGet = (!empty($CITY_CODE) ? $CITY_CODE : '');
		if (!empty($CITY_CODEGet)) {
			$select = $this->_db->select()
				->from(array('A' => 'M_CITY'), array('*'));
			$select->where('A.CITY_CODE = ?', $CITY_CODEGet);
			$arr = $this->_db->fetchall($select);

			$this->view->CITY_NAME = $arr[0]['CITY_NAME'];
		}
		// 9. Create LLD string
		$settings 			= new Application_Settings();
		$LLD_array 			= array();
		$LLD_DESC_arrayCat 	= array();
		$lldTypeArr  		= $settings->getLLDDOMType();

		if (!empty($LLD_CATEGORY)) {
			$lldCategoryArr  	= $settings->getLLDDOMCategory();
			$LLD_array["CT"] 	= $LLD_CATEGORY;
			$LLD_CATEGORY_POST = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldCategoryArr[$LLD_CATEGORY];
		}

		if (!empty($LLD_BENEIDENTIF)) {
			$lldBeneIdentifArr  	= $settings->getLLDDOMBeneIdentification();
			$LLD_array["CT"] 	= $LLD_BENEIDENTIF;
			$LLD_BENEIDENTIF_POST = $LLD_DESC_arrayCat[$lldTypeArr["CT"]] = $lldBeneIdentifArr[$LLD_BENEIDENTIF];
		}


		if ($benefType == 'SKN') {

			$benefType = 'SKN/RTGS';
		} else {
			$benefType = 'ONLINE';
		}

		if ($BENEFICIARY_ID)
			$this->view->BENEFICIARY_ID = $BENEFICIARY_ID;
		$this->view->benefType = $benefType;
		$this->view->ACBENEF_ALIAS = $ACBENEF_ALIAS;
		$this->view->ACBENEF = $ACBENEF;
		$this->view->ACBENEF_BANKNAME = $ACBENEF_BANKNAME;
		$this->view->CURR_CODE = $ACBENEF_CCY;
		$this->view->ADDRESS = $ACBENEF_ADDRESS;
		$this->view->ADDRESS2 = $ACBENEF_ADDRESS2;
		$this->view->CITIZENSHIP = $ACBENEF_RESIDENT;
		$this->view->RESIDENT = $ACBENEF_CITIZENSHIP;
		$this->view->ACBENEF_EMAIL = $ACBENEF_EMAIL;
		$this->view->BANK_NAME = $BANK_NAME;
		$this->view->CLR_CODE = $CLR_CODE;
		$this->view->BANK_CITY = $BANK_CITY;
		$this->view->BENEFICIARY_TYPE = $BENEFICIARY_TYPE;
		$this->view->LLD_CATEGORY = $LLD_CATEGORY;;
		$this->view->LLD_BENEIDENTIF = $LLD_BENEIDENTIF;
		$this->view->LLD_BENENUMBER = $LLD_BENENUMBER;
		$this->view->CITY_CODE = $CITY_CODE;

		$this->view->CATEGORY_NAME_LLD = $LLD_CATEGORY_POST;
		$this->view->BENEIDENTIF_NAME_LLD = $LLD_BENEIDENTIF_POST;
	}

	public function suggestdeleteAction()
	{
		$filters = array('BENEFICIARY_ID' => array('StringTrim', 'StripTags'));
		$validators = array('BENEFICIARY_ID' => array(
			'NotEmpty',
			'Digits',
			'messages' => array(
				'Error: Beneficiary ID cannot be left blank.',
				'Error: Wrong Format Beneficiary ID.'
			)
		));

		$zf_filter_input = new Zend_Filter_Input($filters, $validators, $this->_request->getParams(), $this->_optionsValidator);
		if ($zf_filter_input->isValid()) {
			$Beneficiary = new Beneficiary();
			$Beneficiary->suggestDelete($zf_filter_input->BENEFICIARY_ID);
		}
		$this->_redirect('/' . $this->_request->getModuleName() . '/' . $this->_request->getControllerName());
	}
}
