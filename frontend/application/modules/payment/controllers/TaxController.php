<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/SinglePayment.php';
require_once 'CMD/Validate/ValidateAccountSource.php';
require_once 'General/CustomerUser.php';
require_once 'General/Settings.php';
require_once 'SGO/Extendedmodule/Google/GoogleAuthenticator.php';
require_once 'Crypt/AESMYSQL.php';

class payment_TaxController extends Application_Main
{
	public function initModel()
    { 
		$this->model 					= new payment_Model_Payment();
		$this->param['SERVICE_TYPE'] 	= 6;
		$this->param['PROVIDER_TYPE'] 	= 1;
		$this->param['fetch'] 			= 'fetchAll';
		$this->param['USER_ID'] 		= $this->_userIdLogin;
// 		$this->CustomerUser 			= new CustomerUser($this->_userIdLogin);
		$this->CustomerUser 			= new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
    }
	
	public function indexAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');

		$biller_id = $this->_db->fetchAll(
						$this->_db->select()
					->from(array('A'=>'M_SERVICE_PROVIDER'),array('A.PROVIDER_ID'))
					->joinLeft(array('B' => 'M_SERVICES_TYPE'),'A.SERVICE_TYPE = B.SERVICE_ID',array())
					->where('B.SERVICE_ID = 6')
				);
		
		$select5 = $this->_db->fetchAll(
						$this->_db->select()
					->from(array('A'=>'T_PSLIP'),array('*'))
					->joinLeft(array('B' => 'T_TRANSACTION'),'A.PS_NUMBER = B.PS_NUMBER',array('*'))
					->where('A.CUST_ID = '.$this->_db->quote($this->_custIdLogin))
					->where('PS_BILLER_ID IN (?)',array($biller_id))
					->order('PS_EFDATE DESC')
					->limit(5)
				);
		// die($select5);
		$this->view->historyData = $select5;
		
		$sessionNamespace 			= new Zend_Session_Namespace('tax');
		$paramSession 				= $sessionNamespace->paramSession;
		$back 						= $sessionNamespace->back;

		$this->view->radioCheck		= 1;
		$paramac = array(
				'CCY_IN' => 'IDR'
		);
		$AccArr 	  				= $this->CustomerUser->getAccounts($paramac);
		$this->view->AccArr 		= $AccArr;
		
		$anyValue = '-- '.$this->language->_('Any Value'). ' --';
		
		$arr 						= $this->model->getProviderId($this->param['SERVICE_TYPE'],$this->param['PROVIDER_TYPE'],$this->_userIdLogin);
// 		print_r($arr);die;
		$providerArr 				= array(''=> $anyValue);
		$providerArr 				+= Application_Helper_Array::listArray($arr,'PROVIDER_ID','PROVIDER_NAME');
		$this->view->taxproviderArr 	= $providerArr;
		
		
		
		
		//select m_user - begin
		$userData = $this->CustomerUser->getUser($this->_userIdLogin);
		
		$this->view->tokentype 		= $userData['TOKEN_TYPE'];
		$this->view->amount 		= '5000';
		$this->view->tokenIdUser 	= $userData['TOKEN_ID'];
		$tokenIdUser 				= $userData['TOKEN_ID'];
		$tokenType 					= $userData['TOKEN_TYPE'];
		$userMobilePhone 			= trim($userData['USER_MOBILE_PHONE']);
		
		//added new hard token
		$HardToken 						= new Service_Token($this->_custIdLogin, $this->_userIdLogin, $tokenIdUser);
		$challengeCode 					= $HardToken->generateChallengeCode();
		$this->view->challengeCode 		= $challengeCode;
		$this->view->challengeCodeReq 	= $challengeCode;
		//select m_user - end
		
		$list 							= $this->model->cekList($this->param);
		$this->view->paymentList 		= $list;
		
		$inArrayTahun 					= array(
												'2010'=>'2010',
												'2011'=>'2011',
												'2012'=>'2012',
												'2013'=>'2013',
												'2014'=>'2014',
												'2015'=>'2015'
											);
		//$tahunArr 				= array(''=>$anyValue);
		$tahunArr 						= $inArrayTahun;
		$this->view->tahunArr 			= $tahunArr;
		$this->view->token = false;
		if($this->_custSameUser)
		{
			// echo 'here1';
			if ($this->_getParam('operator') == '1157')
			{ // cetak bukti
				$this->view->token = true;
			}
			
			$userOnBehalf = $this->_userIdLogin;
			$selectTokenIdUser = $this->_db->select()
			->from(
				array('M_USER'),
				array('TOKEN_ID', 'GOOGLE_CODE')
			)
			->where('USER_ID = ?',$userOnBehalf)
			->where('CUST_ID = ?',$this->_custIdLogin)
			->limit(1)
			;

			$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
			$getTokenIdUser = $this->_db->fetchRow($selectTokenIdUser);
			$tokenIdUser = $getTokenIdUser['TOKEN_ID'];
			$tokenGoogle = $getTokenIdUser['GOOGLE_CODE'];

			$Token 			= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
			$challengeCode 	= $Token->generateChallengeCode();
			$this->view->userOnBehalf		= $userOnBehalf;
			$this->view->challengeCode		= $challengeCode;
			if (!empty($tokenGoogle)) 
			{				
				$this->view->googleauth = true;
			}
		}		
		//Zend_Debug::dump($this->_getAllParams());

		//added cms purchase payment get repair - begin
		$PS_NUMBER 						= $this->_getParam('PS_NUMBER');
		$this->view->PS_NUMBER 			= (isset($PS_NUMBER))			? $PS_NUMBER			: '';
		
    	if (!empty($PS_NUMBER))
    	{
    		$paramList = array("WA" 			=> false,
							   "ACCOUNT_LIST" 	=> $this->_accountList,
							   "_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
							 );
    		$select   = $this->CustomerUser->getPayment($paramList, false);
			$select->where('P.PS_NUMBER = ?' , (string) $PS_NUMBER);
			
			$select->columns(array(	"tra_message"		=> "T.TRA_MESSAGE",
									"tra_refno"			=> "T.TRA_REFNO",
									"acbenef_email"		=> "T.BENEFICIARY_EMAIL",
									"provider_id"			=> "P.PS_BILLER_ID",
									"order_id"	=> "T.BILLER_ORDER_ID",
									//"acbenef_bankname"	=> "T.BENEFICIARY_ACCOUNT_NAME",
									//"acbenef_alias"		=> "T.BENEFICIARY_ALIAS_NAME",
							  	   )
							 );

			$pslipData 	= $this->_db->fetchRow($select);
			//Zend_Debug::dump($pslipData);
			
    		if (!empty($pslipData))	
			{
				// $PS_EFDATE  		= $pslipData['efdate'];  
				$PS_EFDATE  		= date("d/m/Y",strtotime($pslipData['efdate']));
				$sourceAcct = $pslipData['accsrc'];
				$prevoperator = $pslipData['provider_id'];

				//check if existed in paylist
				$cParam = array();
				$cParam['PROVIDER_ID'] = $pslipData['provider_id'];
				$cParam['USER_ID'] = $this->_userIdLogin;
				$cParam['CUST_ID']	= $this->_custIdLogin;
				$cParam['REF_NO']	= $pslipData['order_id'];
				$cParam['fetch']	= 'fetchRow';

				$cekPay = $this->model->cekList($cParam);

				if(!empty($cekPay))
					$typeoforderid = "2";
				else
					$typeoforderid = "1";

				$prevorderid = $pslipData['order_id'];
				
				// $param['ACCTSRC'] 	= $pslipData['accsrc']; 
				// $logObj 			= json_decode($pslipData['LOG']); 
				// $param 				= (array) $logObj;
				// $operator 			= $param['operator'];
				// $sgoProductCode 	= $param['sgoProductCode'];
				
				// $model 				= new purchasing_Model_Purchasing();
				// $listvoucherNew 	= $model->getProviderId($operator);
				// $providerIdArrtest 	= array('empty' => ' --- Please Select ---');
				// $providerIdArrtest 	+= Application_Helper_Array::listArray($listvoucherNew,'NOMINAL','NOMINAL');
				// $cekAmountList 		= $this->view->amountList 		= $listvoucherNew;
				
				// $userDataServiceProvider 	= $this->CustomerUser->getServiceProvider($operator);
				// $provInquiry 				= $this->view->provInquiry = $userDataServiceProvider['PROVIDER_INQUIRY_SERVICE_STATUS'];
				
				// $getAmount 			= $model->getProductCode($sgoProductCode);
				// $sgoProductCodeRep 	= $getAmount['0']['SGO_PRODUCT_CODE'];
				//Zend_Debug::dump($getAmount);
			}
    	}
		//added cms purchase payment get repair - end
		
		if($this->_getParam('operator'))
		{

			//select m_service_provider - begin
			$userDataServiceProvider 	= $this->CustomerUser->getServiceProvider($this->_getParam('operator'));
			$provInquiry 	= $this->view->provInquiry = $userDataServiceProvider['PROVIDER_INQUIRY_SERVICE_STATUS'];
			$provCode 		= $this->view->provCode 	= $userDataServiceProvider['PROVIDER_CODE'];
			
// 			print_r($provCode)
			
			$msg = array();
			$param = array();
			
			$param['SERVICE_TYPE'] 		= 6;
			$param['PROVIDER_TYPE'] 	= 1; //1
			
			if (empty($this->_getParam('subject')))
			{
				$param['subject']			= 'No Subject';
			}
			else
			{
				$param['subject']			= $this->_getParam('subject');
			}
			$param['operator']			= $this->_getParam('operator');
			$param['ACCTSRC']			= $this->_getParam('ACCTSRC');
			$param['cek']				= $this->_getParam('cek');
			$param['radioCheck']		= $this->_getParam('radioCheck');
			$param['payment']			= $this->_getParam('payment');
			$param['USER_ID'] 			= $this->_userIdLogin;
			$param['challengeCodeReq'] 	= $this->_getParam('challengeCodeReq');
			$param['responseCodeReq'] 	= $this->_getParam('responseCodeReq');
			$param['TOKEN_TYPE'] 		= $tokenType;
			$param['TOKEN_ID'] 			= $tokenIdUser;
			$param['CUST_ID'] 			= $this->_custIdLogin;
			$param['ACCT_NO'] 			= $this->_getParam('ACCTSRC');
			$param['year'] 				= $this->_getParam('tahun');
			
// 			$arr 						= $this->model->getProviderIdTaxOp($this->param['SERVICE_TYPE'],$this->param['PROVIDER_TYPE'],$this->_userIdLogin,$param['operator']);
			// 		
			$arr = array();
			$providerArr 				= array(''=> $anyValue);
			$providerArr 				+= Application_Helper_Array::listArray($arr,'REF_NO','REF_NO');
// 			print_r($arr);
// 			print_r($providerArr);die;
			$this->view->providerArr 	= $providerArr;
			
			if($provCode == 'PKB')
			{ //PAJAK KENDARAAN BERMOTOR
				$policeNumber  = strtoupper(str_replace(' ', '', $this->_getParam('orderId')));
				$jum 		= strlen($policeNumber);
				$sub 		= substr($policeNumber,0,1);
				$sub1 		= substr($policeNumber,1,1); 

				if($sub == 'B')
				{
					if (is_numeric($sub1) == true){
	  					//$policeNumberNew = str_replace('B', '', $policeNumber);
	  					$policeNumberNew = substr($policeNumber,1,$jum);
	  					//echo'jakarta';
					}
					else
					{
						$policeNumberNew = $policeNumber;
						//echo'lainnya';	
					}
				}
				else
				{
					$policeNumberNew = $policeNumber;
					//echo'lainnya';
				}
				
				if($policeNumber == '')
				{
					$policeNo = '';
				}
				else{
					$policeNo = $policeNumberNew;	
				}
				$convertData = $this->model->getConvertHuruf($policeNo);
				
				$orderid 			= $convertData;
			}
			else
			{
				$orderid 			= $this->_getParam('orderId');
			}
			
			if($provInquiry == 'N')
			{
				$param['provInquiry'] 	= $provInquiry;
				$param['amount'] 		= Application_Helper_General::convertDisplayMoney($this->_getParam('amount'));
			}
			else
			{
				$param['provInquiry'] 	= $provInquiry;
			}
			
			$param['USER_MOBILE_PHONE'] = $userMobilePhone;
			
			if($param['radioCheck']==1)
			{
				if($provCode == 'PKB')
				{
					$param['orderId'] = (empty($orderid) ? '' : $orderid);
				}
				else
				{
					//$param['orderId'] = (empty($orderid) ? '' : $orderid.$param['year']);
					$param['orderId'] = (empty($orderid) ? '' : $orderid);
				}
			}
			else if($param['radioCheck']==2)
			{
				$param['orderId'] = $this->_getParam('payment');
			}
			
			//tax type
			$prefixtaxType = substr($param['orderId'],0,1);
			
			if ($prefixtaxType == 0 || $prefixtaxType == 1 || $prefixtaxType == 2 || $prefixtaxType == 3)
			{
				$taxType = "DJP";
			}
			elseif ($prefixtaxType == 4 || $prefixtaxType == 5 || $prefixtaxType == 6)
			{
				$taxType = "DJBC";
			}
			elseif ($prefixtaxType == 7 || $prefixtaxType == 8 || $prefixtaxType == 9)
			{
				$taxType = "DJA";
			}			
			//end tax type
			
			$sgo_product_code_data	= $this->model->getSgoProductCode($param['operator']);
			$sgo_product_code 		= $sgo_product_code_data['0']['SGO_PRODUCT_CODE'];
			
			$param['productCode'] 	= $sgo_product_code; //STCKAI
			
			$sendProvider	 = new SinglePayment(null,$this->_userIdLogin);
			
			$operator_data = $this->_getParam('operator');
			$this->view->operator = $operator_data;
			if(!empty($operator_data))
			{
				
				$this->view->pkb_select = "selected";
				if($param['operator']=='1006')
				{
					$param['amount'] = '240000';
				}

				if($this->_custSameUser && $this->_getParam('next'))
				{
					if(!$this->view->hasPrivilege('PRLP'))
					{
							// die('here');
							$msg = $this->language->_("Error: You don't have privilege to release payment");
							$this->view->error = true;
							$resProvider = false;
							$this->view->ERROR_MSG_N = $this->language->_("Error: You don't have privilege to release payment");
					}
					else
					{
						if ($this->_getParam('operator') == '1157')
						{ //cetak bukti

							$challengeCode		= $this->_getParam('challengeCode');
							$inputtoken1 		= $this->_getParam('inputtoken1');
							$inputtoken2 		= $this->_getParam('inputtoken2');
							$inputtoken3 		= $this->_getParam('inputtoken3');
							$inputtoken4 		= $this->_getParam('inputtoken4');
							$inputtoken5 		= $this->_getParam('inputtoken5');
							$inputtoken6 		= $this->_getParam('inputtoken6');

							$responseCode		= $inputtoken1.$inputtoken2.$inputtoken3.$inputtoken4.$inputtoken5.$inputtoken6;

							if (!empty($tokenGoogle)) 
							{
								$pga = new PHPGangsta_GoogleAuthenticator();
								$setting 		= new Settings();
								$google_duration 	= $setting->getSetting('google_duration');
								$resultcapca = $pga->verifyCode($tokenGoogle, $responseCode, $google_duration);
								// var_dump($resultcapca);
								//  	var_dump($responseCode);
								//  	var_dump($tokenGoogle);die;
								if ($resultcapca) 
								{
									// $resultToken = $resHard['ResponseCode'] == '0000';
									$sendProvider	 = new SinglePayment(null, $this->_custIdLogin, $this->_userIdLogin);
									$resProvider	 = $sendProvider->createPaymentPurchase($param, $msg);
								} 
								else 
								{
									$tokenFailed = $CustUser->setLogToken(); //log token activity

									$error = true;
									$this->view->error = true;
									$errorMsg[] = $this->language->_('Invalid Google Auth Code');	//$verToken['ResponseDesc'];
									$this->view->ERROR_MSG =  $this->language->_('Invalid Google Auth Code');

									// if ($tokenFailed === true)
									// {
									// 	$this->_redirect('/default/index/logout');
									// }
								}
							}
							else
							{
								$userOnBehalf = $this->_userIdLogin;
								$HardToken 	= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
								$resHard = $HardToken->verifyHardToken($challengeCode, $responseCode);
								$resultToken = $resHard['ResponseCode'] == '0000';

								if ($resHard['ResponseCode'] != '0000')
								{
									$tokenFailed = $CustUser->setLogToken(); //log token activity

									$this->view->error = true;
									$errorMsg[] = $this->language->_('Invalid Response Code');	//$verToken['ResponseDesc'];

									if ($tokenFailed === true)
									{
										$this->_redirect('/default/index/logout');
									}
								}
								else
								{
									
									$sendProvider	 = new SinglePayment(null, $this->_custIdLogin, $this->_userIdLogin);
									$resProvider	 = $sendProvider->createPaymentPurchase($param, $msg);
								}
							}					
						}
						else
						{
							$sendProvider	 = new SinglePayment(null, $this->_custIdLogin, $this->_userIdLogin);
							$resProvider	 = $sendProvider->createPaymentPurchase($param, $msg);

						}
					}
				}
				else
				{
					$sendProvider	 = new SinglePayment(null, $this->_custIdLogin, $this->_userIdLogin);
					$resProvider	 = $sendProvider->createPaymentPurchase($param, $msg);

				}
				// $resProvider	 = $sendProvider->createPaymentPurchase($param, $msg);
				// print_r($msg);die;
				// if($msg['responseCodeReq']['0'] == 'force_logout'){
				// 	$this->_forward('home'); //harus ketendan kareng salah token sebanyak
				// 	//$this->_redirect('/home');
				// }
				// echo "<pre>";
				// print_r($msg);die;
				// print_r($param);die();
				if($resProvider == TRUE && $this->_getParam('next'))
				{	
					$paramSession['operator']			= $param['operator']; //1168 = Repirnt, 1167 = pembayaran
					$paramSession['subject']			= $param['subject'];
					$paramSession['ACCTSRC']			= $param['ACCTSRC'];
					$paramSession['ccy']				= $msg['detailCustomer']['0']['CCY_ID'];
					$paramSession['ACCT_NAME']			= $msg['responseData']['0']['customer_name'];
					$paramSession['ACCT_NAME_DATA']		= $msg['detailCustomer']['0']['ACCT_NAME'];
					$paramSession['ACCT_ALIAS_NAME']	= $msg['detailCustomer']['0']['ACCT_NAME'];
					$paramSession['ACCT_TYPE']			= $msg['detailCustomer']['0']['ACCT_TYPE'];
					$paramSession['ACCT_DESC']			= $msg['detailCustomer']['0']['ACCT_DESC'];
					$paramSession['FREEZE_STATUS']		= $msg['detailCustomer']['0']['FREEZE_STATUS'];
					$paramSession['MAXLIMIT']			= $msg['detailCustomer']['0']['MAXLIMIT'];
					$paramSession['amount']				= $msg['responseData']['0']['amount'];
					$paramSession['fee']				= $msg['fee']['0'];
					$paramSession['total']				= $msg['responseData']['0']['amount']+$msg['fee']['0'];
					$paramSession['sgoProductCode']		= $msg['param']['0']['productCode'];
					$paramSession['operatorNama']		= $msg['provider']['operatorName'];
					$paramSession['operatorAliasName']	= $msg['provider']['operatorAliasName'];
					$paramSession['CUST_ID']			= $this->_custIdLogin;
					$paramSession['USER_ID']			= $this->_userIdLogin;
					$paramSession['TOKEN_TYPE']			= $tokenType;
					$paramSession['USER_MOBILE_PHONE']	= $userMobilePhone;
					
					$paramSession['message']			= $msg['param']['0']['Message'];
					$paramSession['PS_CATEGORY']		= $msg['param']['0']['PS_CATEGORY'];
					$paramSession['TypeOfTrans']		= $msg['param']['0']['TypeOfTrans'];
					$paramSession['PS_CATEGORY_EMAIL']	= $msg['param']['0']['PS_CATEGORY_EMAIL'];
					$paramSession['PS_TYPE']			= $msg['param']['0']['PS_TYPE'];
					$paramSession['Module']				= $msg['param']['0']['Module'];
					
					//khusus pbb
					$paramSession['name']				= $msg['responseData']['0']['name'];
					$paramSession['nop']				= $msg['responseData']['0']['nop']; //orderid
					$paramSession['address']			= $msg['responseData']['0']['address'];
					$paramSession['sub_district']		= $msg['responseData']['0']['sub_district'];
					$paramSession['regency']			= $msg['responseData']['0']['regency'];
					$paramSession['province']			= $msg['responseData']['0']['province'];
					$paramSession['year']				= $param['year'];
					
					$paramSession['cek']				= $param['cek'];
					$paramSession['dataUi']				= $msg['responseData']['0']['dataUi'];
					
					if($param['radioCheck']==1)
					{
						$paramSession['orderId']		= $param['orderId'];
					}
					else if($param['radioCheck']==2)
					{
						$paramSession['orderId']		= $param['payment'];
					}
					$paramSession['taxType']			= $taxType;
						
					
					if ($param['operator'] == 1167)
					{ // penerimaan/pembayaran
						$redirect = '/payment/tax/next';
						$paramSession['reprint'] = 0;
					}
					else if($param['operator']==1156)
					{
						// $paramSession['dataUi']['NOP'] = '10.123.1234.2345';
						// $paramSession['dataUi']['customer_name'] = 'Julian Tandjung';
						// $paramSession['dataUi']['customer_address'] = 'Jl. Purwosari';
						
						$paramSession['sub_district'] = 'MIJEN';
						$paramSession['district'] = 'POLAMAN';
						$paramSession['regency'] = 'KOTA SEMARANG';
						$paramSession['province'] = 'JAWA TENGAH';
						$paramSession['total'] = 245000; 
						// $paramSession[''] = '10.123.1234.2345';
						$redirect = '/payment/tax/next';
						$paramSession['reprint'] = 0;
					}
					elseif ($param['operator'] == 1157)
					{ // reprint					
						$redirect = '/payment/tax/confirm';
						$paramSession['reprint'] = 1;
					}

					$paramSession['PS_NUMBER']			= $PS_NUMBER;
					// print_r($redirect);
					// echo '<pre>';
					// var_dump($paramSession);die;
					$sessionNamespace->paramSession = $paramSession;
									
					//$this->_redirect('/payment/tax/confirm');
					$this->_redirect($redirect);
				}
				else
				{
					$errors 	= $msg;
					$error		= true;
				}

				// echo "<pre>";
				// print_r($msg);die;
			
			
			}
			else
			{
				// $this->view->cetak_select = "selected";
				$this->view->ACCTSRC				= $param['ACCTSRC'];
				if($operator_data == '1167')
					$this->view->negara_select = "selected";
				if($operator_data == '1006')
					$this->view->pbb_select = "selected";
				if($operator_data == '1145')
					$this->view->pkb_select = "selected";
				if($operator_data == '1155')
					$this->view->cetak_select = "selected";
				
				$paramSession = array();
				$this->view->operator = true;
				$paramSession['operator']			= $paramSession['operator'];
				$paramSession['subject']			= $paramSession['subject'];
				$paramSession['ACCTSRC']			= $param['ACCTSRC'];
				$paramSession['ccy']				= $paramSession['ccy'];
				$paramSession['ACCT_NAME']			= $paramSession['ACCT_NAME'];
				$paramSession['ACCT_NAME_DATA']		= $paramSession['ACCT_NAME_DATA'];
				$paramSession['ACCT_ALIAS_NAME']	= $paramSession['ACCT_ALIAS_NAME'];
				$paramSession['amount']				= $paramSession['amount'];
				$paramSession['fee']				= $paramSession['fee'];
				$paramSession['total']				= $paramSession['total'];
				$paramSession['orderId']			= $paramSession['orderId'];
				$paramSession['sgoProductCode']		= $paramSession['sgoProductCode'];
				$paramSession['operatorNama']		= $paramSession['operatorNama'];
				$paramSession['CUST_ID']			= $paramSession['CUST_ID'];
				$paramSession['USER_ID']			= $paramSession['USER_ID'];
				$paramSession['PSNumber']			= $msg['PS_NUMBER']['0'];
				$paramSession['date']				= $paramSession['date'];
				$paramSession['paymentMessage']		= $msg['paymentMessage']['0'];
				$paramSession['dataUi']				= $paramSession['dataUi'];
				
				$sessionNamespace->paramSession 	= $paramSession;
				
				if($this->_getParam('orderId')!= '' || $this->_getParam('payment')!= '')
				{
				$this->_redirect('/payment/tax/confirm');
				}
				
			}
			
			if($error)
			{
				if($this->_getParam('next') == $this->language->_('Submit'))
				{
					$this->view->error 					= $errors;
					$this->view->operatorErr 			= (isset($errors['operator']))? $errors['operator'] : null;
					$this->view->orderIdErr 			= (isset($errors['orderId']))? $errors['orderId'] : null;
					$this->view->paymentErr 			= (isset($errors['payment']))? $errors['payment'] : null;
					$this->view->ACCTSRCErr 			= (isset($errors['ACCTSRC']))? $errors['ACCTSRC'] : null;
					$this->view->amountErr 				= (isset($errors['amount']))? $errors['amount'] : null;
					$this->view->responseCodeReqErr 	= (isset($errors['responseCodeReq']))? $errors['responseCodeReq'] : null;
					$this->view->xACCTSRC 				= (isset($errMsg))? $errMsg : null;
				}
				 
				$this->view->operator				= $param['operator'];
				$this->view->subject				= $param['subject'];
				$this->view->ACCTSRC				= $param['ACCTSRC'];
				$this->view->cek					= $param['cek'];
				$this->view->radioCheck				= $param['radioCheck'];
				$this->view->themes					= $this->_getParam('themes');
				$this->view->tahun					= $this->_getParam('tahun');
				if($provInquiry == 'N'){
					$this->view->amount				= $this->_getParam('amount');
				}
			 
				if($param['radioCheck']==1)
				{
					if($provCode == 'PKB')
					{
						$this->view->policeNumber		= $this->_getParam('orderId');
					}
					else{
						$this->view->orderId			= $this->_getParam('orderId');
					}
				}
				else if($param['radioCheck']==2)
				{
					$this->view->payment			= $param['payment'];
				}
				
				if(count($msg) < 1)
				{
					$this->view->ERROR_MSG_N 		= 'Error : Invalid Token';
				}
			}
		}
		else if($back)
		{
			$this->view->operator				= $paramSession['operator'];
			$this->view->subject				= $paramSession['subject'];
			$this->view->ACCTSRC				= $paramSession['ACCTSRC'];
			$this->view->cek					= $paramSession['cek'];
			$this->view->radioCheck				= $paramSession['radioCheck'];

			if($this->view->radioCheck == 1)
			{
				$this->view->orderId			= $paramSession['orderId'];
			}
			else if($this->view->radioCheck == 2)
			{
				$this->view->payment			= $paramSession['payment'];
			}
			$sessionNamespace->back 			= false;
		}
		else
		{
			unset($_SESSION['tax']);
		}
		Application_Helper_General::writeLog('MTAX','Tax Payment');

		///added cms purchase payment get repair - begin
		if (!empty($PS_NUMBER) && !$this->_getParam('operator'))
		{
				$this->view->ACCTSRC		= $sourceAcct;
				$this->view->operator		= $prevoperator;

				if($typeoforderid == 1){
					$this->view->radioCheck = 1;
					$this->view->orderId = $prevorderid;
				}
				else{
					$this->view->radioCheck = 2;
					$this->view->payment = $prevorderid;
				}
		}  
		//added cms purchase payment get repair - end
	}	
	
	public function nextAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');
		$this->view->userId	= $this->_userIdLogin; 
		$SinglePayment 		= new SinglePayment( null, $this->_userIdLogin);
		$sessionNamespace 	= new Zend_Session_Namespace('tax');
		$paramSession 		= $sessionNamespace->paramSession;
		
		//added repair purchase payment - begin
		$PS_NUMBER 			= $paramSession["PS_NUMBER"];
// 		print_r($paramSession);die;
		$this->view->paramSession	= $paramSession;
		$PS_EFDATE		= date('Y-m-d');
		$date			= $this->model->date();
// 		print_r($paramSession);die;
// 		operator
		$this->model 					= new payment_Model_Payment();
		$data = $this->model->getProviderName($paramSession['operator'],1);
// 		print_r($data);die;
		$paramSession['operatorNama'] = $data['PROVIDER_NAME'];

		$this->view->token = false;
		if($this->_custSameUser){
			// echo 'here1';
			$this->view->token = true;
			$userOnBehalf = $this->_userIdLogin;
			$selectTokenIdUser = $this->_db->select()
			->from(
				array('M_USER'),
				array('TOKEN_ID', 'GOOGLE_CODE')
			)
			->where('USER_ID = ?',$userOnBehalf)
			->where('CUST_ID = ?',$this->_custIdLogin)
			->limit(1)
			;
			$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
			$getTokenIdUser = $this->_db->fetchRow($selectTokenIdUser);
			$tokenIdUser = $getTokenIdUser['TOKEN_ID'];
			$tokenGoogle = $getTokenIdUser['GOOGLE_CODE'];

			$Token 			= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
			$challengeCode 	= $Token->generateChallengeCode();
			$this->view->userOnBehalf		= $userOnBehalf;
			$this->view->challengeCode		= $challengeCode;
			if (!empty($tokenGoogle)) {
				// die('here');
				$this->view->googleauth = true;
			}
		}

		//templatedetail - begin
		$htmldataDetailDetail = '';
		foreach($paramSession['dataUi'] as $datadetail=>$key)
		{
			//Zend_Debug::dump($key);
			
			if($datadetail == 'total_amount')
		   	{
		   	}
		   	else{
		   	
		   	$htmldataDetailDetail .='
			<tr>
				<td class="tbl-evencontent">'.$this->language->_($datadetail).'</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$key.'</td>
			</tr>
			';
		   	}
		}
		
		if(isSet($htmldataDetailDetail))$this->view->templateDetail = $htmldataDetailDetail;
		//templatedetail - end
		
		//select m_user - begin
		$userData = $this->CustomerUser->getUser($this->_userIdLogin);
		
		//added token type
		$this->view->tokentype 		= $userData['TOKEN_TYPE'];
		$this->view->tokenIdUser 	= $userData['TOKEN_ID'];
		$tokenIdUser 				= $userData['TOKEN_ID'];
		$tokenType 					= $userData['TOKEN_TYPE'];
		$userMobilePhone 			= trim($userData['USER_MOBILE_PHONE']);
		
		$paramSession['TOKEN_ID'] 	= $tokenIdUser;
		$paramSession['PS_EFDATE'] 	= $PS_EFDATE;
		$paramSession['date'] 		= $date;
		
		$paramSession['dateDisplayFormat'] 	= $this->_dateDisplayFormat;
		$paramSession['dateDBFormat'] 		= $this->_dateDBFormat;
		
		//ADDED PRIV
		$paramSession['priv1'] 	= $this->view->hasPrivilege('BADA');
		$paramSession['priv2'] 	= $this->view->hasPrivilege('BLBU');
		$paramSession['priv3']	= "MTAX";
		$paramSession['confirmPage']	= 1; //harus ada kalau tidak $this->_isConfirm false & cek benef

		//added cms purchase payment get repair - begin
		$this->view->PS_NUMBER 			= (isset($PS_NUMBER))? $PS_NUMBER: '';
		
    	if (!empty($PS_NUMBER))
    	{
    		$paramSession['isRepair'] = true;
    	}
    	else{
    		$paramSession['isRepair'] = false;
    	}
    	//added cms purchase payment get repair - end
		
		
		if($this->_getParam('submit1') == TRUE)
		{
			$inputtoken1 		= $this->_getParam('inputtoken1');
			$inputtoken2 		= $this->_getParam('inputtoken2');
			$inputtoken3 		= $this->_getParam('inputtoken3');
			$inputtoken4 		= $this->_getParam('inputtoken4');
			$inputtoken5 		= $this->_getParam('inputtoken5');
			$inputtoken6 		= $this->_getParam('inputtoken6');

			$responseCode		= $inputtoken1.$inputtoken2.$inputtoken3.$inputtoken4.$inputtoken5.$inputtoken6;
			
			
			if($this->_custSameUser){
				$inputtoken1 		= $this->_getParam('inputtoken1');
				$inputtoken2 		= $this->_getParam('inputtoken2');
				$inputtoken3 		= $this->_getParam('inputtoken3');
				$inputtoken4 		= $this->_getParam('inputtoken4');
				$inputtoken5 		= $this->_getParam('inputtoken5');
				$inputtoken6 		= $this->_getParam('inputtoken6');

				$responseCode		= $inputtoken1 . $inputtoken2 . $inputtoken3 . $inputtoken4 . $inputtoken5 . $inputtoken6;
				
				if (!empty($tokenGoogle)) {

					$pga = new PHPGangsta_GoogleAuthenticator();
					// var_dump($data2['GOOGLE_CODE']);
					$resultcapca = $pga->verifyCode($tokenGoogle, $responseCode, 2);
					// var_dump($resultcapca);
					//  	var_dump($responseCode);
					//  	var_dump($tokenGoogle);die;
					if ($resultcapca) {
						$resultToken = $resHard['ResponseCode'] == '0000';
						$sendProvider 		= new SinglePayment(null,$this->_userIdLogin);
						$resProvider 		= $sendProvider->createValidatePayment($paramSession, $msg, $resWS);
					} else {
						$tokenFailed = $CustUser->setLogToken(); //log token activity

						$error = true;
						$this->view->error = true;
						$msg = $this->language->_('Invalid Google Auth Code');	//$verToken['ResponseDesc'];
						$this->view->ERROR_MSG =  $this->language->_('Invalid Google Auth Code');
						

						$resProvider = false;
					}
				} else {
					$Token 		= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
					$verToken 	= $Token->verify($challengeCode, $responseCode);

					if ($verToken['ResponseCode'] != '00'){
						$tokenFailed = $CustUser->setLogToken(); //log token activity

						$error = true;
						$errorMsg[] = $this->language->_('Invalid Response Code');	//$verToken['ResponseDesc'];

						if ($tokenFailed === true)
						{
							$this->_redirect('/default/index/logout');
						}
					}
					if(!$error){
						$sendProvider 		= new SinglePayment(null,$this->_userIdLogin);
						$resProvider 		= $sendProvider->createValidatePayment($paramSession, $msg, $resWS);
					}else{
						$resProvider = false;
					}
				}				
			}else{
				$sendProvider 		= new SinglePayment(null,$this->_userIdLogin);
				$resProvider 		= $sendProvider->createValidatePayment($paramSession, $msg, $resWS);
			}
			// if($msg == 'force_logout'){
			// 	$this->_forward('home'); //harus ketendan kareng salah token sebanyak
			// 	//$this->_redirect('/home');
			// }
			
			if($resProvider == TRUE){
				
				$paramSession['operator']			= $paramSession['operator'];
				$paramSession['ACCTSRC']			= $paramSession['ACCTSRC'];
				$paramSession['ccy']				= $paramSession['ccy'];
				$paramSession['ACCT_NAME']			= $paramSession['ACCT_NAME'];
				$paramSession['ACCT_NAME_DATA']		= $paramSession['ACCT_NAME_DATA'];
				$paramSession['ACCT_ALIAS_NAME']	= $paramSession['ACCT_ALIAS_NAME'];
				$paramSession['amount']				= $paramSession['amount'];
				$paramSession['fee']				= $paramSession['fee'];
				$paramSession['total']				= $paramSession['total'];
				$paramSession['orderId']			= $paramSession['orderId'];
				$paramSession['sgoProductCode']		= $paramSession['sgoProductCode'];
				$paramSession['operatorNama']		= $paramSession['operatorNama'];
				$paramSession['CUST_ID']			= $paramSession['CUST_ID'];
				$paramSession['USER_ID']			= $paramSession['USER_ID'];
				$paramSession['PSNumber']			= $msg['PS_NUMBER']['0'];
				$paramSession['date']				= $paramSession['date'];
				$paramSession['paymentMessage']		= $msg['paymentMessage']['0'];
				$paramSession['dataUi']				= $resWS['ResponseData']['dataUi'];
				$paramSession['taxType']			= $paramSession['taxType'];	
				
				
				$sessionNamespace->paramSession 	= $paramSession;
				$ns->backURL = '/'.$this->view->modulename.'/'.$this->view->controllername.'/index';
				if($this->_custSameUser){
										//		die('sini');
											$paramSQL = array("WA" 				=> false,
															  "ACCOUNT_LIST" 	=> $this->_accountList,
															  "_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
															 );

											// get payment query
											$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
											$select   = $CustUser->getPayment($paramSQL);
											$select->where('P.PS_NUMBER = ?' , (string) $msg['PS_NUMBER']['0']);
											// echo $select;
											$pslip = $this->_db->fetchRow($select);
											$settingObj = new Settings();
											$setting = array("COT_SKN"  			=> $settingObj->getSetting("cut_off_time_skn", 		"00:00:00"),
															 "COT_RTGS" 			=> $settingObj->getSetting("cut_off_time_rtgs", 	"00:00:00"),
															 "COT_BULK" 			=> $settingObj->getSetting("cut_off_time_bulkpayments", 	"00:00:00"),
															 "COT_BI" 				=> $settingObj->getSetting("cut_off_time_bi", 	"00:00:00"),
															 "COT_REMIT"			=> $settingObj->getSetting("cut_off_time_remittance", "00:00:00"),
															 'start_remit'			=> $settingObj->getSetting("starting_time_remittance", "00:00:00"),
															 "range_futuredate" 	=> $settingObj->getSetting("range_futuredate", 		"0"),
															 "auto_release" 		=> $settingObj->getSetting("auto_release_payment",	"0"),
															 "_dateFormat" 			=> $this->_dateDisplayFormat,
															 "_dateDBFormat" 		=> $this->_dateDBDefaultFormat,
															 "_transfertype" 		=> array_flip($this->_transfertype["code"]),
															);

											$isBackDated  = ($pslip['efdate'] < date('Y-m-d'));
											$isFutureDate = ($pslip['efdate'] > date('Y-m-d'));

											$caseTransferType 	= Application_Helper_General::caseArray($this->_transfertype);
											$caseTransferStatus = Application_Helper_General::caseArray($this->_transferstatus);

											$app = Zend_Registry::get('config');
											$appBankname = $app['app']['bankname'];

											$selectTrx = $this->_db->select()
											  ->from(	array(	'TT' => 'T_TRANSACTION'),
														array(
																'ACCTSRC'				=> 'TT.SOURCE_ACCOUNT',
																'ACCTSRC_CCY'			=> 'TT.SOURCE_ACCOUNT_CCY',
																'ACCTSRC_NAME'			=> new Zend_Db_Expr("
																							CASE WHEN TT.SOURCE_ACCOUNT_ALIAS_NAME is null THEN TT.SOURCE_ACCOUNT_NAME
																								 ELSE CONCAT(TT.SOURCE_ACCOUNT_NAME )
																							END"),
																//'ACCTSRC_ALIAS'		=> 'TT.SOURCE_ACCOUNT_ALIAS_NAME',
																'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
																'ACBENEF_NAME'			=> new Zend_Db_Expr("
																								CONCAT(TT.BENEFICIARY_ACCOUNT_NAME )"),
																'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
																'ACBENEF_EMAIL'			=> 'TT.BENEFICIARY_EMAIL',
																'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
																'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',
																'TRA_REFNO'				=> 'TT.TRA_REFNO',
																'TRANSFER_TYPE'			=> 'TT.TRANSFER_TYPE',
																'TRANSFER_TYPE_disp'	=> new Zend_Db_Expr("CASE TT.TRANSFER_TYPE $caseTransferType ELSE 'N/A' END"),
																'TRA_STATUS'			=> 'TT.TRA_STATUS',
																'TRA_STATUS_disp'		=> new Zend_Db_Expr("CASE TT.TRA_STATUS $caseTransferStatus ELSE 'N/A' END"),
																'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
																'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
																'TRA_REMAIN'			=> 'TT.TRA_REMAIN',
																'EQUIVALENT_AMOUNT_IDR'	=> 'TT.EQUIVALENT_AMOUNT_IDR',
																'CLR_CODE'				=> 'TT.CLR_CODE',
																'TT.RATE',
																'TT.PROVISION_FEE',
																'TT.NOSTRO_NAME',
																'TT.FULL_AMOUNT_FEE',
																'C.PS_CCY','C.CUST_ID',
																'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
																'EQUIVALEN_USD'			=> 'TT.EQUIVALENT_AMOUNT_USD',
																'EQUIVALEN_IDR'			=> 'TT.EQUIVALENT_AMOUNT_IDR',
																'BANK_NAME'				=> new Zend_Db_Expr("
																								CASE WHEN TT.TRANSFER_TYPE = '0' THEN '".$appBankname."'
																								WHEN TT.TRANSFER_TYPE = '9' THEN CONCAT('".$appBankname."',' - ' ,TT.BENEFICIARY_BANK_NAME)
																								 WHEN TT.TRANSFER_TYPE = '10' THEN CONCAT('".$appBankname."',' - ',TT.BENEFICIARY_BANK_NAME)
																									 ELSE TT.BENEFICIARY_BANK_NAME
																								END"),
																'BALANCE_TYPE'			=> new Zend_Db_Expr("(SELECT BALANCE_TYPE
					        																FROM T_PERIODIC_DETAIL Y
					        																inner join T_PSLIP Z
					        																on Y.PS_PERIODIC = Z.PS_PERIODIC
					        																where
					        																Z.PS_NUMBER = TT.PS_NUMBER limit 1)"),
															  )
														)
												->joinLeft(	array(	'C' => 'T_PSLIP' ),'C.PS_NUMBER = TT.PS_NUMBER',array())
												->where('TT.PS_NUMBER = ?', $msg['PS_NUMBER']['0']);
												// var_dump($msg);
							// echo $selectTrx;die;
												$paramTrxArr = $this->_db->fetchAll($selectTrx);

												$validate  	  = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin, $msg['PS_NUMBER']['0']);
												$paramPayment = array_merge($pslip, $setting);
												// echo '<pre>';
												// print_r($paramPayment);
												// print_r($paramTrxArr);
												// die;
												$check 		  = $validate->checkRelease($paramPayment, $paramTrxArr);
												$infoWarnOri = (!empty($check['infoWarning'])?'*) '.$check['infoWarning']:'');
												$sessionNameConfrim->infoWarnOri = $infoWarnOri;
												
												if($validate->isError() === true)
												{
													$error = true;
													$errorMsg = array_merge($errorMsg, $check["errorMsg"]);
												}

												$Payment = new Payment($msg['PS_NUMBER']['0'], $this->_custIdLogin, $this->_userIdLogin);
												// if ($this->_hasPriviReleasePayment){
													$resultRelease = $Payment->releasePayment();
													// print_r($msg);
													// print_r($resultRelease);die;
													$this->view->ps_numb = $msg['PS_NUMBER'][0];
													$this->view->ntb = $resultRelease['data']['ResponseData']['ntb'];
													$this->view->ntpn = $resultRelease['data']['ResponseData']['ntpn'];
													$this->view->stan = $resultRelease['data']['ResponseData']['stan'];
													$this->view->hidetoken = true;
													if ($resultRelease['status'] == '00'){
														$ns = new Zend_Session_Namespace('FVC');
										    			$ns->backURL = $this->view->backURL;
										    			$this->view->releaseresult = true;
														// $this->_redirect('/notification/success/index');
													}
													else
													{
														// die('here');
														$this->view->releaseresult = false;
														$this->_helper->getHelper('FlashMessenger')->addMessage($result);
									//					$this->_helper->getHelper('FlashMessenger')->addMessage($resultRelease['status']);
									//					$this->_helper->getHelper('FlashMessenger')->addMessage($resultRelease['errorMessage']);
														$this->_redirect('/notification/index/release');
													}
												// }
										}else{
											//die('cek');
											$ns = new Zend_Session_Namespace('FVC');
											$ns->backURL = '/payment/tax';
											$this->_redirect('/notification/success/index');	
										}
				//$this->_redirect('/payment/tax/confirm');
			}
			else{
				$errors 	= $msg;
				$error		= true;
			}
			
			if($error)
			{
				$this->view->error 		= $error;
				$this->view->ERROR_MSG 	= (isset($errors))? $errors : null;
			}
		}
		else if ($this->_getParam('submit') == true)
		{
			$this->_redirect('/payment/tax/index');
		}
		Application_Helper_General::writeLog('MTAX','Tax Payment');
	}



	public function nexttaxAction() 
	{
		$this->_helper->layout()->setLayout('newlayout');
		$this->view->userId	= $this->_userIdLogin; 
		$SinglePayment 		= new SinglePayment( null, $this->_userIdLogin);
		$sessionNamespace 	= new Zend_Session_Namespace('newtaxs');
		$paramSession 		= $sessionNamespace->paramSession;

		//added repair purchase payment - begin
		$PS_NUMBER 			= $paramSession["PS_NUMBER"];
// 		print_r($paramSession);die;
		$paramSession['ACCTSRC'] = $sessionNamespace->ACCTSRC;
		$paramSession['ccy'] = 'IDR';
		$paramSession['ACCT_NAME_DATA'] = 'PT FEDERAL INTERNATIONAL FINANCE';
		$paramSession['ACCT_DESC'] = 'SAVING';
		$paramSession['operatorNama'] = 'NNPWP';
		$paramSession['orderId'] = $sessionNamespace->idnum;
		$paramSession['amount'] = 1663000;
		
		 
		$this->view->paramSession	= $paramSession;
		$PS_EFDATE		= date('Y-m-d');
		$date			= $this->model->date();
		// print_r($sessionNamespace->ACCTSRC);die;
// 		operator
		$this->model 					= new payment_Model_Payment();
		$data = $this->model->getProviderName(1156,1);
// 		print_r($data);die;
		$paramSession['operatorNama'] = $data['PROVIDER_NAME'];

		$this->view->token = false;
		if($this->_custSameUser){
			// echo 'here1';
			$this->view->token = true;
			$userOnBehalf = $this->_userIdLogin;
			$tokenIdUser = $this->_db->select()
			->from(
				array('M_USER'),
				array('TOKEN_ID')
			)
			->where('USER_ID = ?',$userOnBehalf)
			->where('CUST_ID = ?',$this->_custIdLogin)
			->limit(1)
		;

		$tokenIdUser = $this->_db->fetchRow($tokenIdUser);
		$tokenIdUser = $tokenIdUser['TOKEN_ID'];

			$Token 			= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
			$challengeCode 	= $Token->generateChallengeCode();
			$this->view->userOnBehalf		= $userOnBehalf;
			$this->view->challengeCode		= $challengeCode;
		}
		//templatedetail - begin
		$htmldataDetailDetail = '';
		foreach($paramSession['dataUi'] as $datadetail=>$key)
		{
			//Zend_Debug::dump($key);
			
			if($datadetail == 'total_amount')
		   	{
		   	}
		   	else{
		   	
		   	$htmldataDetailDetail .='
			<tr>
				<td class="tbl-evencontent">'.$this->language->_($datadetail).'</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$key.'</td>
			</tr>
			';
		   	}
		}
		
		if(isSet($htmldataDetailDetail))$this->view->templateDetail = $htmldataDetailDetail;
		//templatedetail - end
		
		//select m_user - begin
		$userData = $this->CustomerUser->getUser($this->_userIdLogin);
		
		//added token type
		$this->view->tokentype 		= $userData['TOKEN_TYPE'];
		$this->view->tokenIdUser 	= $userData['TOKEN_ID'];
		$tokenIdUser 				= $userData['TOKEN_ID'];
		$tokenType 					= $userData['TOKEN_TYPE'];
		$userMobilePhone 			= trim($userData['USER_MOBILE_PHONE']);
		
		$paramSession['TOKEN_ID'] 	= $tokenIdUser;
		$paramSession['PS_EFDATE'] 	= $PS_EFDATE;
		$paramSession['date'] 		= $date;
		
		$paramSession['dateDisplayFormat'] 	= $this->_dateDisplayFormat;
		$paramSession['dateDBFormat'] 		= $this->_dateDBFormat;
		
		//ADDED PRIV
		$paramSession['priv1'] 	= $this->view->hasPrivilege('BADA');
		$paramSession['priv2'] 	= $this->view->hasPrivilege('BLBU');
		$paramSession['priv3']	= "MTAX";
		$paramSession['confirmPage']	= 1; //harus ada kalau tidak $this->_isConfirm false & cek benef

		//added cms purchase payment get repair - begin
		$this->view->PS_NUMBER 			= (isset($PS_NUMBER))? $PS_NUMBER: '';
		
    	if (!empty($PS_NUMBER))
    	{
    		$paramSession['isRepair'] = true;
    	}
    	else{
    		$paramSession['isRepair'] = false;
    	}
    	//added cms purchase payment get repair - end
		
		
		if($this->_getParam('submit1') == TRUE)
		{
			$inputtoken1 		= $this->_getParam('inputtoken1');
			$inputtoken2 		= $this->_getParam('inputtoken2');
			$inputtoken3 		= $this->_getParam('inputtoken3');
			$inputtoken4 		= $this->_getParam('inputtoken4');
			$inputtoken5 		= $this->_getParam('inputtoken5');
			$inputtoken6 		= $this->_getParam('inputtoken6');

			$responseCode		= $inputtoken1.$inputtoken2.$inputtoken3.$inputtoken4.$inputtoken5.$inputtoken6;
			
			
			if($this->_custSameUser){
				$Token 		= new Service_Token($this->_custIdLogin, $userOnBehalf, $tokenIdUser);
				$verToken 	= $Token->verify($challengeCode, $responseCode);

				if ($verToken['ResponseCode'] != '00'){
					$tokenFailed = $CustUser->setLogToken(); //log token activity

					$error = true;
					$errorMsg[] = $this->language->_('Invalid Response Code');	//$verToken['ResponseDesc'];

					if ($tokenFailed === true)
					{
						$this->_redirect('/default/index/logout');
					}
				}
				if(!$error){
					$sendProvider 		= new SinglePayment(null,$this->_userIdLogin);
					$resProvider 		= $sendProvider->createValidatePayment($paramSession, $msg, $resWS);
				}else{
					$resProvider = false;
				}
			}else{
				$sendProvider 		= new SinglePayment(null,$this->_userIdLogin);
				// $resProvider 		= $sendProvider->createValidatePayment($paramSession, $msg, $resWS);
				$this->_redirect('/notification/success/index');	
			}
			// if($msg == 'force_logout'){
			// 	$this->_forward('home'); //harus ketendan kareng salah token sebanyak
			// 	//$this->_redirect('/home');
			// }
			
			if($resProvider == TRUE){
				
				$paramSession['operator']			= $paramSession['operator'];
				$paramSession['ACCTSRC']			= $paramSession['ACCTSRC'];
				$paramSession['ccy']				= $paramSession['ccy'];
				$paramSession['ACCT_NAME']			= $paramSession['ACCT_NAME'];
				$paramSession['ACCT_NAME_DATA']		= $paramSession['ACCT_NAME_DATA'];
				$paramSession['ACCT_ALIAS_NAME']	= $paramSession['ACCT_ALIAS_NAME'];
				$paramSession['amount']				= $paramSession['amount'];
				$paramSession['fee']				= $paramSession['fee'];
				$paramSession['total']				= $paramSession['total'];
				$paramSession['orderId']			= $paramSession['orderId'];
				$paramSession['sgoProductCode']		= $paramSession['sgoProductCode'];
				$paramSession['operatorNama']		= $paramSession['operatorNama'];
				$paramSession['CUST_ID']			= $paramSession['CUST_ID'];
				$paramSession['USER_ID']			= $paramSession['USER_ID'];
				$paramSession['PSNumber']			= $msg['PS_NUMBER']['0'];
				$paramSession['date']				= $paramSession['date'];
				$paramSession['paymentMessage']		= $msg['paymentMessage']['0'];
				$paramSession['dataUi']				= $resWS['ResponseData']['dataUi'];
				$paramSession['taxType']			= $paramSession['taxType'];	
				
				
				$sessionNamespace->paramSession 	= $paramSession;
				$ns->backURL = '/'.$this->view->modulename.'/'.$this->view->controllername.'/index';
				if($this->_custSameUser){
										//		die('sini');
											$paramSQL = array("WA" 				=> false,
															  "ACCOUNT_LIST" 	=> $this->_accountList,
															  "_beneLinkage" 	=> $this->view->hasPrivilege('BLBU'),
															 );

											// get payment query
											$CustUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
											$select   = $CustUser->getPayment($paramSQL);
											$select->where('P.PS_NUMBER = ?' , (string) $msg['PS_NUMBER']['0']);
											// echo $select;
											$pslip = $this->_db->fetchRow($select);
											$settingObj = new Settings();
											$setting = array("COT_SKN"  			=> $settingObj->getSetting("cut_off_time_skn", 		"00:00:00"),
															 "COT_RTGS" 			=> $settingObj->getSetting("cut_off_time_rtgs", 	"00:00:00"),
															 "COT_BULK" 			=> $settingObj->getSetting("cut_off_time_bulkpayments", 	"00:00:00"),
															 "COT_BI" 				=> $settingObj->getSetting("cut_off_time_bi", 	"00:00:00"),
															 "COT_REMIT"			=> $settingObj->getSetting("cut_off_time_remittance", "00:00:00"),
															 'start_remit'			=> $settingObj->getSetting("starting_time_remittance", "00:00:00"),
															 "range_futuredate" 	=> $settingObj->getSetting("range_futuredate", 		"0"),
															 "auto_release" 		=> $settingObj->getSetting("auto_release_payment",	"0"),
															 "_dateFormat" 			=> $this->_dateDisplayFormat,
															 "_dateDBFormat" 		=> $this->_dateDBDefaultFormat,
															 "_transfertype" 		=> array_flip($this->_transfertype["code"]),
															);

											$isBackDated  = ($pslip['efdate'] < date('Y-m-d'));
											$isFutureDate = ($pslip['efdate'] > date('Y-m-d'));

											$caseTransferType 	= Application_Helper_General::caseArray($this->_transfertype);
											$caseTransferStatus = Application_Helper_General::caseArray($this->_transferstatus);

											$app = Zend_Registry::get('config');
											$appBankname = $app['app']['bankname'];

											$selectTrx = $this->_db->select()
											  ->from(	array(	'TT' => 'T_TRANSACTION'),
														array(
																'ACCTSRC'				=> 'TT.SOURCE_ACCOUNT',
																'ACCTSRC_CCY'			=> 'TT.SOURCE_ACCOUNT_CCY',
																'ACCTSRC_NAME'			=> new Zend_Db_Expr("
																							CASE WHEN TT.SOURCE_ACCOUNT_ALIAS_NAME is null THEN TT.SOURCE_ACCOUNT_NAME
																								 ELSE CONCAT(TT.SOURCE_ACCOUNT_NAME )
																							END"),
																//'ACCTSRC_ALIAS'		=> 'TT.SOURCE_ACCOUNT_ALIAS_NAME',
																'ACBENEF_ID'			=> 'TT.BENEFICIARY_ID',
																'ACBENEF_NAME'			=> new Zend_Db_Expr("
																								CONCAT(TT.BENEFICIARY_ACCOUNT_NAME )"),
																'ACBENEF'				=> 'TT.BENEFICIARY_ACCOUNT',
																'ACBENEF_EMAIL'			=> 'TT.BENEFICIARY_EMAIL',
																'TRA_MESSAGE'			=> 'TT.TRA_MESSAGE',
																'TRA_ADDMESSAGE'			=> 'TT.TRA_ADDITIONAL_MESSAGE',
																'TRA_REFNO'				=> 'TT.TRA_REFNO',
																'TRANSFER_TYPE'			=> 'TT.TRANSFER_TYPE',
																'TRANSFER_TYPE_disp'	=> new Zend_Db_Expr("CASE TT.TRANSFER_TYPE $caseTransferType ELSE 'N/A' END"),
																'TRA_STATUS'			=> 'TT.TRA_STATUS',
																'TRA_STATUS_disp'		=> new Zend_Db_Expr("CASE TT.TRA_STATUS $caseTransferStatus ELSE 'N/A' END"),
																'ACBENEF_CCY'			=> 'TT.BENEFICIARY_ACCOUNT_CCY',
																'TRA_AMOUNT'			=> 'TT.TRA_AMOUNT',
																'TRA_REMAIN'			=> 'TT.TRA_REMAIN',
																'EQUIVALENT_AMOUNT_IDR'	=> 'TT.EQUIVALENT_AMOUNT_IDR',
																'CLR_CODE'				=> 'TT.CLR_CODE',
																'TT.RATE',
																'TT.PROVISION_FEE',
																'TT.NOSTRO_NAME',
																'TT.FULL_AMOUNT_FEE',
																'C.PS_CCY','C.CUST_ID',
																'TRANSFER_FEE'			=> 'TT.TRANSFER_FEE',
																'EQUIVALEN_USD'			=> 'TT.EQUIVALENT_AMOUNT_USD',
																'EQUIVALEN_IDR'			=> 'TT.EQUIVALENT_AMOUNT_IDR',
																'BANK_NAME'				=> new Zend_Db_Expr("
																								CASE WHEN TT.TRANSFER_TYPE = '0' THEN '".$appBankname."'
																								WHEN TT.TRANSFER_TYPE = '9' THEN CONCAT('".$appBankname."',' - ' ,TT.BENEFICIARY_BANK_NAME)
																								 WHEN TT.TRANSFER_TYPE = '10' THEN CONCAT('".$appBankname."',' - ',TT.BENEFICIARY_BANK_NAME)
																									 ELSE TT.BENEFICIARY_BANK_NAME
																								END"),
																'BALANCE_TYPE'			=> new Zend_Db_Expr("(SELECT BALANCE_TYPE
					        																FROM T_PERIODIC_DETAIL Y
					        																inner join T_PSLIP Z
					        																on Y.PS_PERIODIC = Z.PS_PERIODIC
					        																where
					        																Z.PS_NUMBER = TT.PS_NUMBER limit 1)"),
															  )
														)
												->joinLeft(	array(	'C' => 'T_PSLIP' ),'C.PS_NUMBER = TT.PS_NUMBER',array())
												->where('TT.PS_NUMBER = ?', $msg['PS_NUMBER']['0']);
												// var_dump($msg);
							// echo $selectTrx;die;
												$paramTrxArr = $this->_db->fetchAll($selectTrx);

												$validate  	  = new ValidatePaymentSingle($this->_custIdLogin, $this->_userIdLogin, $msg['PS_NUMBER']['0']);
												$paramPayment = array_merge($pslip, $setting);
												// echo '<pre>';
												// print_r($paramPayment);
												// print_r($paramTrxArr);
												// die;
												$check 		  = $validate->checkRelease($paramPayment, $paramTrxArr);
												$infoWarnOri = (!empty($check['infoWarning'])?'*) '.$check['infoWarning']:'');
												$sessionNameConfrim->infoWarnOri = $infoWarnOri;
												
												if($validate->isError() === true)
												{
													$error = true;
													$errorMsg = array_merge($errorMsg, $check["errorMsg"]);
												}

												$Payment = new Payment($msg['PS_NUMBER']['0'], $this->_custIdLogin, $this->_userIdLogin);
												// if ($this->_hasPriviReleasePayment){
													$resultRelease = $Payment->releasePayment();
													// print_r($msg);
													// print_r($resultRelease);die;
													$this->view->ps_numb = $msg['PS_NUMBER'][0];
													$this->view->hidetoken = true;
													if ($resultRelease['status'] == '00'){
														$ns = new Zend_Session_Namespace('FVC');
										    			$ns->backURL = $this->view->backURL;
										    			$this->view->releaseresult = true;
														// $this->_redirect('/notification/success/index');
													}
													else
													{
														// die('here');
														$this->view->releaseresult = false;
														$this->_helper->getHelper('FlashMessenger')->addMessage($result);
									//					$this->_helper->getHelper('FlashMessenger')->addMessage($resultRelease['status']);
									//					$this->_helper->getHelper('FlashMessenger')->addMessage($resultRelease['errorMessage']);
														$this->_redirect('/notification/index/release');
													}
												// }
										}else{
											//die('cek');
											$this->_redirect('/notification/success/index');	
										}
				//$this->_redirect('/payment/tax/confirm');
			}
			else{
				$errors 	= $msg;
				$error		= true;
			}
			
			if($error)
			{
				$this->view->error 		= $error;
				$this->view->ERROR_MSG 	= (isset($errors))? $errors : null;
			}
		}
		else if ($this->_getParam('submit') == true)
		{
			$sessionNamespace->back 	= true;
			$this->_redirect('/payment/tax/index');
		}
		Application_Helper_General::writeLog('MTAX','Tax Payment');
	}
	
	public function confirmAction() 
	{
		$pdf = $this->_getParam('pdf');
		$sessionNamespace 					= new Zend_Session_Namespace('tax');
		$this->view->ERROR_MSG_N			= $paramSession['paymentMessage'];
		$paramSession 						= $sessionNamespace->paramSession;
		
		if ($paramSession['reprint'] == 1){ //if reprint
			$select	= $this->_db->select()
			->from(
				array('A'	 		=> 'T_TRANSACTION'),
				array('PS_NUMBER' 	=> 'A.PS_NUMBER',)
			)
			->joinLeft(	array('B' => 'T_PSLIP'), 'A.PS_NUMBER = B.PS_NUMBER', array() )
			//->where("A.USER_ID 			 = ?", $paramSession['USER_ID'])
			//->where("A.SOURCE_ACCOUNT 	 = ?", $paramSession['ACCTSRC'])
			->where("A.BILLER_ORDER_ID 	 = ?", $paramSession['orderId'])
			->order('B.PS_CREATED DESC')
			->limit(1)
			;
			$psnumber = $this->_db->fetchRow($select);
			$this->view->psnumber = $psnumber['PS_NUMBER'];
		
		}else{ // if payment tax
			$this->view->psnumber = $paramSession['PSNumber'];
		}
		
		//templatedetail - begin
		$htmldataDetailDetail = '';
		foreach($paramSession['dataUi'] as $datadetail=>$key)
		{
			//Zend_Debug::dump($key);
			
			if($datadetail == 'total_amount')
		   	{
		   	}
		   	else{
		   	
		   	$htmldataDetailDetail .='
			<tr>
				<td class="tbl-evencontent">'.$this->language->_($datadetail).'</td>
				<td class="tbl-evencontent">:</td>
				<td class="tbl-evencontent">'.$key.'</td>
			</tr>
			';
		   	}
		}
		
		if(isSet($htmldataDetailDetail))$this->view->templateDetail = $htmldataDetailDetail;
		//templatedetail - end
		
		if(!$paramSession)
		{
			$this->cancel();
		}
		
		if ($this->_getParam('submit') == TRUE)
		{
			$this->_redirect('/payment/tax/index');
		}
		
		$sessionNamespace->paramSession 	= $paramSession;
		$this->view->paramSession			= $paramSession;

		if($pdf)
		{			
			$htmldataDetail .=
			'<table border="1" cellspacing="0" cellpadding="0" class="tableform" width="550px">
				<tr>
					<td class="tbl-evencontent">&nbsp; Payment Ref</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$this->view->psnumber.'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; Payment Subject</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$paramSession['subject'].'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; Source Account</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$paramSession['ACCTSRC'].'['.$paramSession['ccy'].'] - '.$paramSession['ACCT_NAME_DATA'].' ('.$paramSession['ACCT_DESC'].') </td>
				</tr>
    			<tr>
					<td class="tbl-evencontent">&nbsp; Type Of Tax</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$paramSession['operatorNama'].'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; Billing ID</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$paramSession['orderId'].'</td>
				</tr>';
			
			if ($paramSession['taxType'] == "DJP"){ //0,1,2,3
				$htmldataDetail .=
				'<tr>
					<td class="tbl-evencontent">&nbsp; NPWP</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$paramSession['dataUi']['npwp'].'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; Customer Name</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$paramSession['dataUi']['customer_name'].'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; Customer Address</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$paramSession['dataUi']['customer_address'].'</td>
				</tr>
		    	<tr>
					<td class="tbl-evencontent">&nbsp; Tax Object Number</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$paramSession['dataUi']['NOP'].'</td>
				</tr>
		        <tr>
					<td class="tbl-evencontent">&nbsp; Account Code</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$paramSession['dataUi']['account_map'].'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; Payment Type</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$paramSession['dataUi']['type'].'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; Tax Period</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$paramSession['dataUi']['period'].'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; SK Number</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$paramSession['dataUi']['sk_number'].'</td>
				</tr>';
			}elseif ($paramSession['taxType'] == "DJA"){ //7,8,9
				$htmldataDetail .=
				'<tr>
					<td class="tbl-evencontent">&nbsp; Customer Name</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$paramSession['dataUi']['customer_name'].'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; K/L</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$paramSession['dataUi']['k_l'].'</td>
				</tr>
		    	<tr>
					<td class="tbl-evencontent">&nbsp; Echelon Unit 1</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$paramSession['dataUi']['eselon_unit'].'</td>
				</tr>
		        <tr>
					<td class="tbl-evencontent">&nbsp; Code Unit</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$paramSession['dataUi']['work_unit'].'</td>
				</tr>';
			}elseif ($paramSession['taxType'] == "DJBC"){ //4,5,6
				$htmldataDetail .=
				'<tr>
					<td class="tbl-evencontent">&nbsp; Customer Name</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$paramSession['dataUi']['customer_name'].'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; ID Type Customer</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$paramSession['dataUi']['customer_id'].'</td>
				</tr>
		    	<tr>
					<td class="tbl-evencontent">&nbsp; Document Type</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$paramSession['dataUi']['document_type'].'</td>
				</tr>
		        <tr>
					<td class="tbl-evencontent">&nbsp; Document Number</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$paramSession['dataUi']['document_number'].'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; Document Date</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$paramSession['dataUi']['document_date'].'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; KPBC Code</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$paramSession['dataUi']['kppbc_code'].'</td>
				</tr>';
			}
			
			$amount = $paramSession['ccy']." ".Application_Helper_General::displayMoney($paramSession['amount']);
			$htmldataDetail .=
		        '<tr>
					<td class="tbl-evencontent">&nbsp; Amount</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$amount.'</td>
				</tr>
		        <tr>
					<td class="tbl-evencontent">&nbsp; NTB/NTP</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$paramSession['dataUi']['ntb'].'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; NTPN</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$paramSession['dataUi']['ntpn'].'</td>
				</tr>
				<tr>
					<td class="tbl-evencontent">&nbsp; STAN</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$paramSession['dataUi']['stan'].'</td>
				</tr>
			
				<tr>
					<td class="tbl-evencontent">&nbsp; Status</td>
					<td class="tbl-evencontent">:</td>
					<td class="tbl-evencontent">'.$this->language->_('Success').'</td>
				</tr>
			</table>';
			
			//$datapdf = SGO_Helper_GeneralFunction::formatPurchaseNote($this->view->psnumber, $paramSession);
			$this->_helper->download->pdf(null,null,null,'Payment Tax Report Detail',$htmldataDetail);
		}
		if($this->_request->getParam('printtrxtax') == 1){
// 			echo  "<pre>";
// 			print_r($paramSession);die;
// 			$paramSessionData = array();
// 			$paramSessionData[''] = ;
			$this->_forward('printtrxtax', 'index', 'widget', array('psnumber' => $this->view->psnumber, 'param' => $paramSession));
		}
		Application_Helper_General::writeLog('MTAX','Tax Payment');
	}
	
	public function cancel() 
	{
		unset($_SESSION['tax']);
		$this->_redirect("/home/index");
	}
}
