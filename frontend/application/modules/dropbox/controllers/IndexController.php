<?php

require_once 'Zend/Controller/Action.php';

//NOTE:
//Watch the modulename, filename and classname carefully
class dropbox_IndexController extends Application_Main
{
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$temp = $this->_helper->getHelper('FlashMessenger')->getMessages();
		if(count($temp)>1){
      		if($temp[0]=='F' || $temp[0]=='S'){
      			if($temp[0]=='F')
        			$this->view->error = 1;
        		else
        			$this->view->success = 1;
      			$msg = ''; unset($temp[0]);
      			foreach($temp as $value)
      			{
      				if(!is_array($value))
      					$value = array($value);
      				$msg .= $this->view->formErrors($value);
      			}
        		$this->view->report_msg = $msg;
     		}
    	}

		$arr = null;
		$viewFilter = null;

		$custid =  $this->_custIdLogin;
		$setting = new Settings();			  	
		$enc_pass = $setting->getSetting('enc_pass');
		$enc_salt = $setting->getSetting('enc_salt');
		$sessionNamespace = new Zend_Session_Namespace('Tokenenc');			 
		$pw_hash = md5($enc_salt.$enc_pass);
		$rand = $this->_userIdLogin.date('dHis').$pw_hash;
		$sessionNamespace->token 	= $rand;
		$this->view->token = $sessionNamespace->token;
		$companyCode = $this->language->_('Company');
		$companyName = $this->language->_('Company Name');
		$fileName = $this->language->_('File Name');
		$fileDescription = $this->language->_('Description');
		$uploadedBy = $this->language->_('Uploaded By');
		$uploadDateTime = $this->language->_('Uploaded Date');

		$fields = array	(
							'FileName'  				=> array	(
																	'field' => 'FILE_NAME',
																	'label' => $fileName,
																	'sortable' => true
																),
							'FileDescription'  			=> array	(
																	'field' => 'FILE_DESCRIPTION',
																	'label' => $fileDescription,
																	'sortable' => true
																),
							// 'Company Code'  			=> array	(
							// 										'field' => 'TFS.CUST_ID',
							// 										'label' => $companyCode,
							// 										'sortable' => true
							// 									),
							// 'Company Name'  			=> array	(
							// 										'field' => 'MC.CUST_NAME',
							// 										'label' => $companyName,
							// 										'sortable' => true
							// 									),
							
							
							'Upload Date and Time'  		=> array	(
																		'field' => 'FILE_UPLOADED_TIME',
																		'label' => $uploadDateTime,
																		'sortable' => true
																	),
							'Uploaded By'  					=> array	(
																		'field' => 'FILE_UPLOADEDBY',
																		'label' => $uploadedBy,
																		'sortable' => true
																	)
							
						);
		$this->view->fields = $fields;
		
		$delete 	= $this->_getParam('delete');
		
		if($delete)
		{
			$postreq_id	= $this->_request->getParam('req_id');
			// var_dump($postreq_id); die();
			if($postreq_id){

				foreach ($postreq_id as $key => $value){
					if($postreq_id[$key]==0){ unset($postreq_id[$key]); }
				}
			}

			if($postreq_id == null) $params['req_id'] = null;
			else $params['req_id'] = 1;


			$validators = array	(
									'req_id' => array	(
															'NotEmpty',
															'messages' => array	(
																					'Error File ID Submitted',
																				)
														),
								);

			$filtersVal = array	( 'req_id' => array('StringTrim','StripTags'));

			$zf_filter_input = new Zend_Filter_Input($filtersVal,$validators,$params,$this->_optionsValidator);
				$success = false;
			foreach ($postreq_id as $key => $value) {
				if(!empty($value)){
					$success = true;
				}
			}


			if($zf_filter_input->isValid() && $success)
			{
				try
				{
					foreach ($postreq_id as  $key =>$value)
					{
						$FILE_ID_DELETE =  $postreq_id[$key];

						$this->_db->beginTransaction();

						$param = array();
						$param['FILE_DELETED'] = '1';
						$param['FILE_DELETEDBY'] = $this->_userIdLogin;

						$where = array('FILE_ID = ?' => $FILE_ID_DELETE);
						$query = $this->_db->update( "T_FILE_SUBMIT", $param, $where );
						Application_Helper_General::writeLog('DSUD','Delete File Sharing');
						$this->_db->commit();
					}
				}
				catch(Exception $e)
				{
					$this->_db->rollBack();
				}
				$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
				$this->_redirect('/notification/success/index');
			}
			else
			{
				$error 			= true;
				$errors 		= $zf_filter_input->getMessages();
				$req_idErr 		= (isset($errors['req_id']))? $errors['req_id'] : null;

				//$this->_redirect("/datasubmision/index?error=true&req_idErr=$req_idErr&filter=Clear+Filter");
			}

		}

		if(isset($error))
		{
			$this->view->error 			= $errors;

			$this->view->req_idErr 		= $req_idErr;
			$filter = true;

		}

		$page = $this->_getParam('page');
		$page = (Zend_Validate::is($page,'Digits'))? $page : 1;
		$sortBy  = $this->_getParam('sortby');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'asc';

		$this->view->currentPage = $page;
		$this->view->sortBy = $sortBy;
		$this->view->sortDir = $sortDir;

		$select =	$this->_db->select()
							->from	(
										array('TFS'=>'T_FILE_SUBMIT'),
										array(
												'FILE_ID' => 'TFS.FILE_ID',
												'FILE_NAME' => 'TFS.FILE_NAME',
												'FILE_DESCRIPTION' => 'TFS.FILE_DESCRIPTION',
												'FILE_UPLOADED_TIME' => 'TFS.FILE_UPLOADED_TIME',
												'FILE_SYSNAME' => 'TFS.FILE_SYSNAME',
												'CUST_ID'	 => 'TFS.CUST_ID',
												'FILE_UPLOADEDBY' => 'TFS.FILE_UPLOADEDBY',
												'CUST_NAME' => 'MC.CUST_NAME',
												'FILE_DOWNLOADED' => 'TFS.FILE_DOWNLOADED',
											)
									)
							->joinLeft	(
											array('MC' => 'M_CUSTOMER'),
											'TFS.CUST_ID = MC.CUST_ID'
										)
							->where("FILE_DELETED != 1")
							->where("TFS.CUST_ID =?", $custid)
							// ->where("FILE_NAME LIKE %testing%")
							->order('TFS.FILE_UPLOADED_TIME DESC');


		// advance search
		$filterlist = array("NAMA FILE" => "FILE_NAME","DESKRIPSI" => "FILE_DESCRIPTION", "DIUNGGAH OLEH" => "FILE_UPLOADEDBY","TANGGAL DIUNGGAH"=>"QUEST_DATE");

        $this->view->filterlist = $filterlist;


        $filterArr = array(
            'filter'    =>  array('StripTags'),
            'FILE_NAME'    =>  array('StringTrim', 'StripTags'),
            'FILE_DESCRIPTION'    =>  array('StringTrim', 'StripTags'),
            'FILE_UPLOADEDBY'    =>  array('StringTrim', 'StripTags'),
            'QUEST_DATE' =>  array('StringTrim', 'StripTags'),
            'QUEST_DATE_END'    =>  array('StringTrim', 'StripTags'),
        );

        $validator = array(
            'filter'                 => array(),
            'FILE_NAME' => array(),
            'FILE_DESCRIPTION' => array(),
            'FILE_UPLOADEDBY' => array(),
            'QUEST_DATE'         => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
            'QUEST_DATE_END'             => array(new Zend_Validate_Date($this->_dateDisplayFormat)),
        );


        $dataParam = array("FILE_NAME", "FILE_DESCRIPTION","FILE_UPLOADEDBY","QUEST_DATE");
        $dataParamValue = array();

        $clean2 = array_diff($this->_request->getParam('wherecol'), $dataParam);
        $dataParam = array_diff($this->_request->getParam('wherecol'), $clean2);
        // print_r($this->_request->getParam('wherecol'));
        foreach ($dataParam as $no => $dtParam) {

            if (!empty($this->_request->getParam('wherecol'))) {
                $dataval = $this->_request->getParam('whereval');
                // print_r($dataval);
                $order = 0;
                foreach ($this->_request->getParam('wherecol') as $key => $value) {
                    if ($value == "QUEST_DATE") {
                        $order--;
                    }
                    if ($dtParam == $value) {
                        $dataParamValue[$dtParam] = $dataval[$order];
                    }
                    $order++;
                }
            }
        }
			
		 if (!empty($this->_request->getParam('questdate'))) {
            $questdateArr = $this->_request->getParam('questdate');
            $dataParamValue['QUEST_DATE'] = $questdateArr[0];
            $dataParamValue['QUEST_DATE_END'] = $questdateArr[1];
        }


        $zf_filter = new Zend_Filter_Input($filterArr, $validator, $dataParamValue);
        // $filter 	= $zf_filter->getEscaped('filter');
        $filter         = $this->_getParam('filter');
		
		$FILE_NAME     = html_entity_decode($zf_filter->getEscaped('FILE_NAME'));
		$FILE_DESCRIPTION     = html_entity_decode($zf_filter->getEscaped('FILE_DESCRIPTION'));
		$FILE_UPLOADEDBY     = html_entity_decode($zf_filter->getEscaped('FILE_UPLOADEDBY'));

        $datefrom     = html_entity_decode($zf_filter->getEscaped('QUEST_DATE'));
        $dateto     = html_entity_decode($zf_filter->getEscaped('QUEST_DATE_END'));
		
		if ($filter_clear == true) {
            $datefrom     = '';
            $dateto     = '';
        }
		
		if ($filter == TRUE) {

            $this->view->fDateTo    = $dateto;
            $this->view->fDateFrom  = $datefrom;
			
			if($FILE_NAME != null)
			{
				// $select->where("UPPER(FILE_NAME) like ".$this->_db->quote('%'.$UPLOADED_BY.'%'));
				$FILE_NAMEarr = explode(',', $FILE_NAME);
				foreach ($FILE_NAMEarr as $fileN) {
					$select->where("FILE_NAME  LIKE ?", "%".$fileN."%");
				}
			}

			if($FILE_DESCRIPTION != null)
			{
				// $select->where("UPPER(FILE_DESCRIPTION) like ".$this->_db->quote('%'.$UPLOADED_BY.'%'));
				$FILE_DESCRIPTIONarr = explode(',', $FILE_DESCRIPTION);
				foreach ($FILE_DESCRIPTIONarr as $fileD) {
					$select->where("FILE_DESCRIPTION  like ?", "%".$fileD."%" );
				}
			}
			
			if($FILE_UPLOADEDBY != null)
			{
				// $select->where("UPPER(FILE_UPLOADEDBY) like ".$this->_db->quote('%'.$UPLOADED_BY.'%'));
				$FILE_UPLOADEDBYarr = explode(',', $FILE_UPLOADEDBY);
				$select->where("UPPER(FILE_UPLOADEDBY)  in (?)", $FILE_UPLOADEDBYarr );
			}

			if($datefrom)
			{
				$FormatDate 	= new Zend_Date($datefrom, $this->_dateDisplayFormat);
				$datefrom  	= $FormatDate->toString($this->_dateDBFormat);
				$select->where("DATE(FILE_UPLOADED_TIME) >= DATE(".$this->_db->quote($datefrom).")");
			}

			if($dateto)
			{
				$FormatDate 	= new Zend_Date($dateto, $this->_dateDisplayFormat);
				$dateto  	= $FormatDate->toString($this->_dateDBFormat);
				$select->where("DATE(FILE_UPLOADED_TIME) <= DATE(".$this->_db->quote($dateto).")");
			}
		
        }

        unset($dataParamValue['QUEST_DATE_END']);
        if (!empty($dataParamValue)) {
            foreach ($dataParamValue as $key => $value) {
                $wherecol[]    = $key;
                $whereval[] = $value;
            }

            $this->view->wherecol     = $wherecol;
            $this->view->whereval     = $whereval;
        }
		
		//
		$FILE_ID   	= $this->_getParam('FILE_ID');
		if($FILE_ID && $this->view->hasPrivilege('DSDB'))
		{
			$select->where('FILE_ID =?',$FILE_ID);
			$data = $this->_db->fetchRow($select);
			$attahmentDestination = UPLOAD_PATH . '/document/submit/';
			$this->_helper->download->file($data['FILE_NAME'],$attahmentDestination.$data['FILE_SYSNAME']);
			Application_Helper_General::writeLog('DSDL','Download File DrobBox');
			$updateArr = array();
			$updateArr['FILE_DOWNLOADED'] = $data['FILE_DOWNLOADED']+1;
			$updateArr['FILE_DOWNLOADEDBY'] = $this->_userIdLogin;

			$whereArr = array('FILE_ID = ?'=>$FILE_ID);

			$fileupdate = $this->_db->update('T_FILE_SUBMIT',$updateArr,$whereArr);
		}

		
	   	// }
	   	if(!$FILE_ID && $this->view->hasPrivilege('DSDL')){
		Application_Helper_General::writeLog('DSLS','View File Sharing List');
	   	}
	   	$select->where("FILE_TYPE = ?",'1');
		$select->order($sortBy.' '.$sortDir);
		//echo $select;die;
		$arr = $this->_db->fetchAll($select);
		$this->paging($arr);
		//unset($dataParamValue['QUEST_DATE_END']);
		$size = file($data['FILE_NAME'],$attahmentDestination.$data['FILE_SYSNAME']);
		$this->view->size = $size;
		// unset($dataParamValue['an']);
		
	// 	if(!empty($dataParamValue)){
	// 		foreach ($dataParamValue as $key => $value) {
	// 			$wherecol[]	= $key;
	// 			$whereval[] = $value;
	// 		}
    //     $this->view->wherecol     = $wherecol;
    //     $this->view->whereval     = $whereval;

    //   }
	}

}
