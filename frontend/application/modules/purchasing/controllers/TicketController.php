<?php
require_once 'Zend/Controller/Action.php';
require_once 'CMD/SinglePayment.php';
require_once 'CMD/Validate/ValidateAccountSource.php';
require_once 'General/CoreBank.php';
require_once 'General/CustomerUser.php';

class purchasing_ticketController extends Application_Main
{
	public function indexAction()
	{
		$sessionNamespace 		= new Zend_Session_Namespace('Tikets');
		$paramSession 			= $sessionNamespace->paramSession;
		$CustomerUser 			= new CustomerUser($this->_userIdLogin);
		$AccArr 	  			= $CustomerUser->getAccounts();
		$this->view->AccArr 	= $AccArr;
		
		$model = new purchasing_Model_Purchasing();
		$listticket = $model->getProviderList(10,2);
		$tiketrArr = array('' => ' --- Please Select ---');
		$tiketrArr += Application_Helper_Array::listArray($listticket,'PROVIDER_ID','PROVIDER_NAME');
		$this->view->tiketrArr = $tiketrArr;
		
		if($this->_getParam('submit') == 'Next')
		{
			$inArrayOperator 		= Application_Helper_Array::listArray($listticket,'PROVIDER_NAME','PROVIDER_ID');
			
			$param['operator']		= $this->_getParam('operator');
			$param['ACCTSRC']		= $this->_getParam('ACCTSRC');
			$param['paymentCode']	= $this->_getParam('paymentCode');
			
			$filters = 	array( 
									'ACCTSRC'		=> 	array('StringTrim','StripTags'),
									'operator'		=> 	array('StringTrim','StripTags'),
									'paymentCode'	=> 	array('StringTrim','StripTags'),
								);
			$validator = array( 
									'ACCTSRC'		=> 	array	(	
																	'NotEmpty',
																	'messages' => array	(
																							"Error: Source Account Has Not Fill yet.",
																						)
																),
									'operator' 		=> 	array	(	
																	'NotEmpty',
																	array('inArray',$inArrayOperator),
																	'messages' => array	(
																							"Error: Company Name Has Not Fill yet.",
																							"Error: Company Name not Exist.",
																						)
																),
									'paymentCode' 	=> 	array	(	
																'NotEmpty',
																'Alnum',
																array	(
																			'StringLength',array	(
																										'min'=>1,
																										'max'=>50
																									)
																		),
																'messages' => array	(
																						"Error: Payment Code is Empty.",
																						"Error: Payment Code invalid format.",
																						"Error: Payment Code max length min 1 max 50.",
																					)
															),
								);
			$zf_filter_input = new Zend_Filter_Input($filters, $validator, $param, $this->_optionsValidator);
			
			$error = false;
			
			if($zf_filter_input->isValid())
			{
				$validateACCTSRC = new ValidateAccountSource($param['ACCTSRC'], $this->_userIdLogin); 
				$charges = $this->getCharges($param['operator']);

				$fee = $charges['IDR']['CHARGES_AMT'];
				$param['amount'] = 0;// TODO GET AMOUNT FROM BILLER
				$total = $param['amount'] + $fee;
				$check = $validateACCTSRC->check($total);

				if ($check == true)
				{
				// TO DO GET DATA FROM BILLER
					$paramSession['operator']			= $param['operator'];
					$paramSession['paymentCode']		= $param['paymentCode'];
					$paramSession['ACCTSRC']			= $param['ACCTSRC'];
					$paramSession['setNo']				= '02F';
					$paramSession['bookingCode']		= 'QF142';
					$paramSession['name']				= 'Fajar Jaya';
					$paramSession['flight']				= 'B-777';
					$paramSession['amount']				= $param['amount'];
					$paramSession['fee']				= $fee;
					$paramSession['total']				= $total;
					$sessionNamespace->paramSession 	= $paramSession;
					
					$this->_redirect('/purchasing/ticket/next');
				}
				else
				{
					$errMsg 			= $validateACCTSRC->getErrorMsg();
					$error				= true;
				}
			}
			else
			{
				$errors 	= $zf_filter_input->getMessages();
				$error		= true;
			}
			
			if($error)
			{
				$this->view->error 					= $error;
				$this->view->operatorErr 			= (isset($errors['operator']))? $errors['operator'] : null;
				$this->view->paymentCodeErr 		= (isset($errors['paymentCode']))? $errors['paymentCode'] : null;
				$this->view->ACCTSRCErr 			= (isset($errors['ACCTSRC']))? $errors['ACCTSRC'] : null;
				$this->view->xACCTSRC 				= (isset($errMsg))? $errMsg : null;
				
				$this->view->operator				= $param['operator'];
				$this->view->paymentCode			= $param['paymentCode'];
				$this->view->ACCTSRC				= $param['ACCTSRC'];
				
			}
		}
		else if($this->_getParam('submit') == 'Cancel')
		{
			$this->cancel();
		}
		else
		{
			unset($_SESSION['Tikets']);
		}
	
	}	
	
	public function nextAction() 
	{
		$model = new purchasing_Model_Purchasing();
		
		$SinglePayment 		= new SinglePayment( null, $this->_userIdLogin);
		$sessionNamespace 	= new Zend_Session_Namespace('Tikets');
		$paramSession 		= $sessionNamespace->paramSession;
		
		$acctInfo = $model->getAcctInfo($paramSession['ACCTSRC']);
		$ACCTSRC   		= $paramSession['ACCTSRC'];
		$ACCTSRC_CCY   	= $acctInfo["CCY_ID"];
		$ACCTSRC_NAME   = $acctInfo["ACCT_NAME"];
		$ACCTSRC_ALIAS  = $acctInfo["ACCT_ALIAS_NAME"];
		$ACCTSRC_TYPE   = $acctInfo["ACCT_TYPE"];
		$paramSession['ACCTSRC_view'] 	= Application_Helper_General::viewAccount($ACCTSRC, $ACCTSRC_CCY, $ACCTSRC_NAME, $ACCTSRC_ALIAS, $ACCTSRC_TYPE);

		if(!$paramSession)
		{
			$this->cancel();
		}
		
		$listticket = $model->getProviderList(10,2);
		$tiketrArr = Application_Helper_Array::listArray($listticket,'PROVIDER_ID','PROVIDER_NAME');
		$operator = $tiketrArr[$paramSession['operator']];
		if($this->_getParam('submit') == 'Submit')
		{
			$PS_NUMBER 	= "P".$SinglePayment->generatePaymentID(1);
			$PS_EFDATE		= date("d M Y H:i:s");
			
			$SinglePayment 				= new SinglePayment( $PS_NUMBER, $this->_userIdLogin);

			$setting 	= new Settings();
			$bank_code 	= $setting->getSetting('bank_code');
	
			$param	= array(
					'PS_CCY'			=> $ACCTSRC_CCY,
					'PS_TYPE'			=> 11,
					'PS_EFDATE'			=> Application_Helper_General::convertDate($PS_EFDATE, $this->_dateDBFormat, $this->_dateDisplayFormat),
					'TRA_AMOUNT'		=> $paramSession['total'],
					'SOURCE_ACCOUNT'	=> $paramSession['ACCTSRC'],
					'TRA_MESSAGE'		=> 'Pembelian Tiket '.$operator.', No = '.$paramSession['paymentCode'].', Booking Code = '.$paramSession['bookingCode'],
					'_addBeneficiary'	=> $this->view->hasPrivilege('BADA'),
					'_beneLinkage'		=> $this->view->hasPrivilege('BLBU'),
					'_priviCreate'		=> 'CRSP',
					'PS_CATEGORY'		=> 'PURCHASE TICKET',
					'PROVIDER_ID'		=> $paramSession['operator'],
					'REF_NO'			=> $paramSession['paymentCode'],
					'USER_BANK_CODE'	=> $bank_code,
					'DETAIL'			=>	array(
											'0' => array(
															'PS_FIELDNAME' 	=> 'Company Name',
															'PS_FIELDTYPE'	=> '3',
															'PS_FIELDVALUE'	=> $operator
														),
											'1' => array(
															'PS_FIELDNAME' 	=> 'Payment Code',
															'PS_FIELDTYPE'	=> '3',
															'PS_FIELDVALUE'	=> $paramSession['paymentCode']
														),
											'2' => array(
															'PS_FIELDNAME' 	=> 'Booking Code',
															'PS_FIELDTYPE'	=> '3',
															'PS_FIELDVALUE'	=> $paramSession['bookingCode']
														),
											'3' => array(
															'PS_FIELDNAME' 	=> 'Name',
															'PS_FIELDTYPE'	=> '3',
															'PS_FIELDVALUE'	=> $paramSession['name']
														),
											'4' => array(
															'PS_FIELDNAME' 	=> 'Seat Number',
															'PS_FIELDTYPE'	=> '3',
															'PS_FIELDVALUE'	=> $paramSession['setNo']
														),
											'5' => array(
															'PS_FIELDNAME' 	=> 'Flight No',
															'PS_FIELDTYPE'	=> '3',
															'PS_FIELDVALUE'	=> $paramSession['flight']
														),
											'6' => array(
															'PS_FIELDNAME' 	=> 'Amount',
															'PS_FIELDTYPE'	=> '1',
															'PS_FIELDVALUE'	=> $paramSession['amount']
														),
											'7' => array(
															'PS_FIELDNAME' 	=> 'Fee',
															'PS_FIELDTYPE'	=> '1',
															'PS_FIELDVALUE'	=> $paramSession['fee']
														),
											'8' => array(
															'PS_FIELDNAME' 	=> 'Total Payment',
															'PS_FIELDTYPE'	=> '1',
															'PS_FIELDVALUE'	=> $paramSession['amount']+$paramSession['fee']
														),
											)
			);
			$return = $SinglePayment->createPembelianPembayaran($param);
			
			if($return)
			{
				$paramSession['PSNumber'] 		= $PS_NUMBER;
				$paramSession['date']			= $PS_EFDATE;
				$sessionNamespace->paramSession = $paramSession;
				
				$this->_redirect('/purchasing/ticket/confirm');
			}
		}
		else if ($this->_getParam('submit') == 'Back')
		{
			$this->_redirect('/purchasing/ticket/index');
		}
		$this->view->operator = $operator;
		$this->view->paramSession				= $paramSession;
	}
	
	public function confirmAction() 
	{
		$sessionNamespace 	= new Zend_Session_Namespace('Tikets');
		$paramSession 		= $sessionNamespace->paramSession;
		
		if(!$paramSession)
		{
			$this->cancel();
		}
		
		if ($this->_getParam('submit') == 'Back')
		{
			$this->_redirect('/purchasing/ticket/index');
		}
		
		$sessionNamespace->paramSession 	= $paramSession;
		$this->view->paramSession			= $paramSession;
	}
	
	public function cancel() 
	{
		unset($_SESSION['Tikets']);
		$this->_redirect("/home/index");
	}
}
