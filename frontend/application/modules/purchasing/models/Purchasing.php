<?php

Class purchasing_Model_Purchasing
{
    protected $_db;
   
    public function __construct()
    { 
		$this->_db 		= Zend_Db_Table::getDefaultAdapter();
    }
    
	public function getProviderList($servicetype, $providertype)
	{	
		//modify agung
		
		$select = $this->_db->select()
				->from(array('A' => 'M_SERVICE_PROVIDER'),array('PROVIDER_ID','PROVIDER_NAME'));
		$select->joinLeft(array('MU'=>'M_PROVIDER_ACCT'), 'MU.PROVIDER_ID = A.PROVIDER_ID', array('A.PROVIDER_ID','A.PROVIDER_NAME'));		
		$select->where("A.SERVICE_TYPE = ? ", $servicetype);
		$select->where("A.PROVIDER_TYPE = ? ", $providertype);
		$select->where('A.PROVIDER_STATUS = ?',1);
		//$select->where('MU.ACCT_NO !="" ');
		$select ->order('PROVIDER_NAME ASC');
		$result = $select->query()->FetchAll();
		
		return $result;
		
    	/*$select = $this->_db->select()
				->from(array('A' => 'M_SERVICE_PROVIDER'),array('PROVIDER_ID','PROVIDER_NAME'));
		$select->where("A.SERVICE_TYPE = ? ", $servicetype);
		$select->where("A.PROVIDER_TYPE = ? ", $providertype);
		$select ->order('PROVIDER_NAME ASC');
		$result = $select->query()->FetchAll();
		
		return $result;*/
	}

	public function getSourceAccount($id)
	{
	
		$select = $this->_db->select()
		->from(array('A' => 'M_CUSTOMER_ACCT'),array('A.*'));
		$select->where("A.ACCT_NO = ? ", $id);
		$result = $select->query()->FetchAll();
		return $result;
	}
	
    //added agung
	public function getProviderId($getProvid)
	{	
		
		$select = $this->_db->select()
				->from(array('A' => 'M_PROVIDER_CATALOG'),array('PROVIDER_ID','PRODUCT_CODE','NOMINAL','SGO_PRODUCT_CODE','CATALOG_ID'));
		$select->where("A.PROVIDER_ID = ? ", $getProvid);
		$select->where("A.CATALOG_STATUS = ? ", 'A');
		$select->where("A.NOMINAL > 0");
		//$select ->order('PROVIDER_NAME ASC');
		$result = $select->query()->FetchAll();
		
		return $result;
	}
	
	public function getBeneficiaryByAccount($id)
	{
	
		$select = $this->_db->select()
		->from(array('A' => 'M_BENEFICIARY'),array('A.*'));
		$select->where("A.BENEFICIARY_ACCOUNT = ? ", $id);
		$result = $select->query()->FetchAll();
		return $result;
	}
	
	public function getProductCode($getProCode)
	{	
		
		$select = $this->_db->select()
				->from(array('A' => 'M_PROVIDER_CATALOG'),array('PROVIDER_ID','PRODUCT_CODE','NOMINAL','SGO_PRODUCT_CODE','CATALOG_ID'));
		$select->where("A.SGO_PRODUCT_CODE = ? ", $getProCode);
		$select->where("A.CATALOG_STATUS = ? ", 'A');
		$select->where("A.NOMINAL > 0");
		$result = $select->query()->FetchAll();
		
		return $result;
	}
	
	public function getCatalogId($CATALOG_ID)
	{	
		
		
		$select = $this->_db->select()
				->from(array('A' => 'M_PROVIDER_CATALOG'),array('PROVIDER_ID','PRODUCT_CODE','NOMINAL','CATALOG_ID','SGO_PRODUCT_CODE'));
		$select->where("A.CATALOG_ID = ? ", $CATALOG_ID);
		$select->where("A.CATALOG_STATUS = ? ", 'A');
		$select->where("A.NOMINAL > 0");
		//$select ->order('PROVIDER_NAME ASC');
		$result = $select->query()->FetchAll();
		//Zend_Debug::dump($result);
		return $result;
		
		
	}
	
	public function getProviderIdNew()
	{	
		
		$select = $this->_db->select()
				->from(array('A' => 'M_PROVIDER_CATALOG'),array('PROVIDER_ID','PRODUCT_CODE','NOMINAL','CATALOG_ID','SGO_PRODUCT_CODE'));
		//$select->where("A.PROVIDER_ID = ? ", $getProvid);
		$select->where("A.CATALOG_STATUS = ? ", 'A');
		$select->where("A.NOMINAL > 0");
		//$select ->order('PROVIDER_NAME ASC');
		$result = $select->query()->FetchAll();
		
		return $result;
	}
	
	function getProvider($PROVIDER_ID)
	{
		$select	= $this->_db->select()
							->from	(
									array(	'MSC' => 'M_SERVICE_PROVIDER'),
									array(	
											'PROVIDER_ID'	=> 'MSC.PROVIDER_ID',
											'PROVIDER_CODE'	=> 'MSC.PROVIDER_CODE',
											'PROVIDER_NAME'	=> 'MSC.PROVIDER_NAME',
											'SERVICE_TYPE' 	=> 'MSC.SERVICE_TYPE',
											'PROVIDER_TYPE' => 'MSC.PROVIDER_TYPE',
										))
							->where("MSC.PROVIDER_ID = ? ", $PROVIDER_ID);
		
		$result =  $this->_db->fetchRow($select);
		return $result;
	}
	
	function getTranspurpose()
	{
		$result = $this->_db->fetchAll("SELECT DESCRIPTION, CODE, CATEGORY FROM M_TRANSACTION_PURPOSE");
		return $result;
	}
	
	function getTranspurposeById($id)
	{
		$select	= $this->_db->select()
		->from	(
				array(	'MSC' => 'M_TRANSACTION_PURPOSE'),
				array(
						'DESCRIPTION', 'CODE', 'CATEGORY'
				))
				->where("MSC.CODE = ? ", $id);
		$result =  $this->_db->fetchRow($select);
// 		$result = $this->_db->fetchAll("SELECT DESCRIPTION, CODE, CATEGORY FROM M_TRANSACTION_PURPOSE WHERE CODE = '.$id.'");
		return $result;
	}
	
	function getIdenty()
	{
//die;
		$result = $this->_db->fetchAll("SELECT M_KEY2, M_KEY1 FROM M_MASTER WHERE M_KEY1 = 'IDENTICAL'");
//print_r($result);die;
		return $result;
	}
	
	function getTransactor()
	{
		$result = $this->_db->fetchAll("SELECT M_KEY2, M_KEY1 FROM M_MASTER WHERE M_KEY1 = 'RELATIONSHIP'");
		return $result;
	}
	
	function getTransactorById($id,$type)
	{
		$select = $this->_db->select()
		->from(array('A' => 'M_MASTER'),array('A.*'));
		$select->where("A.sm_key1 = ? ", $type);
		$select->where("A.sm_key2 = ? ", $id);
		$result = $select->query()->FetchAll();
		// 		$result = $this->_db->fetchAll("SELECT sm_key2, sm_text1 FROM M_MASTER WHERE sm_key1 = 'RELATIONSHIP'");
		return $result;
	}
	
	function getAcctInfo($ACCTSRC)
	{ 
		$result = $this->_db->fetchRow("SELECT CCY_ID, ACCT_NAME, ACCT_ALIAS_NAME, ACCT_TYPE FROM M_USER_ACCT WHERE ACCT_NO='".$ACCTSRC."'");
		return $result;
	}
    
	function date()
	{
		return date("d M Y H:i:s");
	}
}