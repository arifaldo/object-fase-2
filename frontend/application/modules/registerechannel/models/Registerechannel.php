<?php
class registerechannel_Model_Registerechannel
{
	protected $_db;

	// constructor
	public function __construct()
	{
		$this->_db = Zend_Db_Table::getDefaultAdapter();
	}

	public function getCountry()
	{
		$select = $this->_db->select()
			->from('M_COUNTRY');

		return $this->_db->fetchAll($select);
	}

	public function getCompanyType()
	{
		$select = $this->_db->select()
			->from('M_COMPANY_TYPE');

		return $this->_db->fetchAll($select);
	}

	public function getBusinessType()
	{
		$select = $this->_db->select()
			->from('M_BUSINESS_ENTITY')
			->query()->fetchAll();

		return $select;
	}

	public function getDebiturCode()
	{
		$select = $this->_db->select()
			->from('M_DEBITUR')
			->query()->fetchAll();

		return $select;
	}

	public function getCity()
	{
		$select = $this->_db->select()
			->from('M_CITYLIST')
			->query()->fetchAll();

		return $select;
	}



	public function getCCY()
	{
		$select = $this->_db->select()
			->from('M_MINAMT_CCY');

		return $this->_db->fetchAll($select);
	}

	public function getCodeLength()
	{
		$select = $this->_db->select()
			->from('M_SETTING', array('SETTING_VALUE'))
			->where('SETTING_ID = ?', 'company_code_len');

		return $this->_db->fetchOne($select);
	}


	public function getTempBis($change_id)
	{
		$result = $this->_db->select()
			->from('TEMP_MYONLINE_BIS')
			->where('CHANGES_ID = ?', $change_id);

		return $this->_db->fetchRow($result);
	}

	public function getTempInd($change_id)
	{
		$result = $this->_db->select()
			->from('TEMP_MYONLINE_IND')
			->where('CHANGES_ID = ?', $change_id);

		return $this->_db->fetchRow($result);
	}

	public function getBisUser($custid)
	{
		$result = $this->_db->select()
			->from('T_MYONLINE_BIS_USER')
			->where('CUST_ID = ?', $custid);

		return $this->_db->fetchAll($result);
	}

	public function getBisUserDet($id)
	{
		$result = $this->_db->select()
			->from('T_MYONLINE_BIS_USER')
			->where('ID = ?', $id);

		return $this->_db->fetchRow($result);
	}

	public function getBisAcct($custid)
	{
		$result = $this->_db->select()
			->from('T_MYONLINE_BIS_ACC')
			->where('CUST_ID = ?', $custid);

		return $this->_db->fetchAll($result);
	}

	public function getBisAcctDet($id)
	{
		$result = $this->_db->select()
			->from('T_MYONLINE_BIS_ACC')
			->where('ID = ?', $id);

		return $this->_db->fetchRow($result);
	}

	public function getIndAcct($tempid)
	{
		$result = $this->_db->select()
			->from('T_MYONLINE_IND_ACC')
			->where('REG_ID = ?', $tempid);

		return $this->_db->fetchAll($result);
	}

	public function getIndAcctDet($id)
	{
		$result = $this->_db->select()
			->from('T_MYONLINE_IND_ACC')
			->where('REG_ID = ?', $id);

		return $this->_db->fetchRow($result);
	}



	public function getBranch()
	{
		$result = $this->_db->select()
			->from('M_BANK_BRANCH')
			->query()
			->fetchAll();

		return $result;
	}

	public function getFileName($custID, $col)
	{
		$result = $this->_db->select()
			->from('TEMP_MYONLINE_BIS', array($col))
			->where('CUST_ID = ?', $custID);

		return $this->_db->fetchOne($result);
	}

	public function insertMyOnlineReg($type, $data)
	{
		if ($type == 'ind')
			$tablename = 'TEMP_MYONLINE_IND';
		else
			$tablename = 'TEMP_ONLINECUST_BUS';

		$this->_db->insert($tablename, $data);

		return $this->_db->lastInsertId();
	}

	public function insertMyOnlineRegs($change_id, $insertArr)
	{
		
		$content = [
			'CHANGES_ID' => $change_id,
			'CUST_REG_NUMBER' => $insertArr['CUST_REG_NUMBER'],
			'CUST_ID' => $insertArr['CUST_ID'],
			'CUST_NAME' => $insertArr['CUST_NAME'],
			'CUST_CIF' => $insertArr['CUST_CIF'],
			'CUST_MODEL' => $insertArr['CUST_MODEL'],
			'CUST_TYPE' => $insertArr['CUST_TYPE'],
			'BUSINESS_TYPE' => $insertArr['CUST_WORKFIELD'],
			'CUST_NPWP' => $insertArr['CUST_NPWP'],
			'GO_PUBLIC' => $insertArr['GO_PUBLIC'] == 'Y' ? 'Y' : 'N',
			'DEBITUR_CODE' => $insertArr['DEBITUR_CODE'],
			'CUST_ADDRESS' => $insertArr['CUST_ADDRESS'],
			'CUST_VILLAGE' => $insertArr['CUST_VILLAGE'],
			'CUST_DISTRICT' => $insertArr['CUST_DISTRICT'],
			'CUST_CITY' => $insertArr['CUST_CITY'],
			'CUST_ZIP' => $insertArr['CUST_ZIP'],
			'COUNTRY_CODE' => $insertArr['COUNTRY_CODE'],
			'CUST_CONTACT' => $insertArr['CUST_CONTACT'],
			'CUST_CONTACT_PHONE' => $insertArr['CUST_CONTACT_PHONE'],
			'CUST_EMAIL' => $insertArr['CUST_EMAIL'],
			'CUST_PHONE' => $insertArr['CUST_PHONE'],
			'CUST_WEBSITE' => $insertArr['CUST_WEBSITE'],
			'CUST_CREATED' => $insertArr['CREATED'],
			'CUST_CREATEDBY' => $insertArr['CREATEDBY'],
			'CUST_UPDATED'		=> $insertArr['CUST_UPDATED'],
			'CUST_UPDATEDBY'	=> $insertArr['CUST_UPDATEDBY'],
			'CUST_APPROVER'	=> $insertArr['CUST_APPROVER'],
			'CUST_SAME_USER'	=> $insertArr['CUST_SAME_USER'],
			'BANK_BRANCH'	=> $insertArr['BANK_BRANCH'],
			'GRUP_BUMN' => $insertArr['GROUP_BUMN'] == 'Y' ? '1' : '0',

		];



		$this->_db->insert('TEMP_ONLINECUST_BIS', $content);
	}

	public function updateMyOnlineRegs($insertArr, $cusdt)
	{

		$content = [
			//'CHANGES_ID' => $change_id,
			//'CUST_REG_NUMBER' => $insertArr['CUST_REG_NUMBER'],
			//'CUST_ID' => $insertArr['CUST_ID'],
			'CUST_NAME' => $insertArr['CUST_NAME'],
			'CUST_CIF' => $insertArr['CUST_CIF'],
			'CUST_MODEL' => '1',
			'CUST_TYPE' => $insertArr['CUST_TYPE'],
			'BUSINESS_TYPE' => $insertArr['CUST_WORKFIELD'],
			'CUST_NPWP' => $insertArr['CUST_NPWP'],
			'GO_PUBLIC' => $insertArr['GO_PUBLIC'] == 'Y' ? 'Y' : 'N',
			'GRUP_BUMN' => $insertArr['GROUP_BUMN'] == 'Y' ? '1' : '0',
			'DEBITUR_CODE' => $insertArr['DEBITUR_CODE'],
			'CUST_ADDRESS' => $insertArr['CUST_ADDRESS'],
			'CUST_VILLAGE' => $insertArr['CUST_VILLAGE'],
			'CUST_DISTRICT' => $insertArr['CUST_DISTRICT'],
			'CUST_CITY' => $insertArr['CUST_CITY'],
			'CUST_ZIP' => $insertArr['CUST_ZIP'],
			'COUNTRY_CODE' => $insertArr['COUNTRY_CODE'],
			'CUST_CONTACT' => $insertArr['CUST_CONTACT'],
			'CUST_CONTACT_PHONE' => $insertArr['CUST_CONTACT_PHONE'],
			'CUST_EMAIL' => $insertArr['CUST_EMAIL'],
			'CUST_PHONE' => $insertArr['CUST_PHONE'],
			'CUST_WEBSITE' => $insertArr['CUST_WEBSITE'],
			'CUST_CREATED' => $insertArr['CREATED'],
			'CUST_CREATEDBY' => $insertArr['CREATEDBY'],
			'CUST_UPDATED'		=> $insertArr['CUST_UPDATED'],
			'CUST_UPDATEDBY'	=> $insertArr['CUST_UPDATEDBY'],
			'CUST_APPROVER'	=> $insertArr['CUST_APPROVER'],
			'CUST_SAME_USER'	=> $insertArr['CUST_SAME_USER'],

		];

		$where = array('CUST_ID = ?' => $cusdt);
		
		$this->_db->update('TEMP_ONLINECUST_BIS', $content, $where);
	}


	// public function insertMyOnlineReg($data){

	// 	$tablename = 'TEMP_ONLINECUST_BIS';

	// 	$this->_db->insert($tablename,$data);

	// 	return $this->_db->lastInsertId();
	// }

	public function insertMyOnlineIndAcc($data)
	{
		$this->_db->insert('T_MYONLINE_IND_ACC', $data);
	}

	// public function insertMyOnlineBisAcc($data){
	// 	$this->_db->insert('T_MYONLINE_BIS_ACC',$data);
	// }

	public function insertMyOnlineBisAcc($data)
	{
		$this->_db->insert('T_ONLINECUST_BIS_ACC', $data);
	}

	public function updateMyOnlineBisAcc($data, $cusdt)
	{
		$where = array('CUST_ID = ?' => $cusdt);
		$this->_db->update('T_ONLINECUST_BIS_ACC', $data, $where);
	}

	// public function insertMyOnlineBisUser($data){
	// 	$this->_db->insert('T_MYONLINE_BIS_USER',$data);
	// }

	public function insertMyOnlineBisUser($data)
	{
		$this->_db->insert('T_ONLINECUST_BIS_USER', $data);
	}

	public function updateMyOnlineBisUser($data, $cusdt)
	{
		$where = array('CUST_ID = ?' => $cusdt);
		$this->_db->update('T_ONLINECUST_BIS_USER', $data, $where);
	}
}
