<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/Account.php';
class userdebit_IndexController extends Application_Main
{
	protected $_moduleDB = 'RTF'; // masih harus diganti

	
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$app = Zend_Registry::get('config');
		$app = $app['app']['bankcode'];
				
		$userId = $this->_userIdLogin;
		$custId = $this->_custIdLogin;
		
		$param = array('USER_ID'=>$userId, 'CUST_ID'=>$custId);

		$data  = $this->_db->fetchAll(
                     $this->_db->select()
                               ->from(array('T' => 'T_DEBITCARD'))
                               ->joinleft(array('C' => 'M_CUSTOMER'),'C.CUST_ID=T.CUST_ID',array('CUST_NAME','CUST_EMAIL'))
							   ->joinleft(array('D' => 'T_CUST_DEBIT'),'D.REG_NUMBER=T.REG_NUMBER',array('*',
							   'd_sugest'=>'D.DEBIT_SUGGESTED',
							   'd_sugestby'=>'D.DEBIT_SUGGESTEDBY',
							   'd_approve'=>'D.DEBIT_APPROVED',
							   'd_approveby'=>'D.DEBIT_APPROVEDBY',
							   'd_status'=>'T.DEBIT_STATUS',
							   ))
							   ->joinleft(array('E' => 'T_DEBIT_GROUP'),'E.DEBIT_NUMBER=T.DEBIT_NUMBER',array('GROUP_ID'))
							   ->joinleft(array('EG' => 'M_DEBITGROUP'),'EG.GROUP_ID=E.GROUP_ID',array('GROUP_NAME'))
							   ->joinleft(array('F' => 'M_USER_DEBIT'),'F.USER_DEBITNUMBER=T.DEBIT_NUMBER',array('USER_ID'))
							   ->joinleft(array('G' => 'M_USER'),'F.USER_ID=G.USER_ID',array('USER_FULLNAME','USER_EMAIL'))
                               #->where('T.DEBIT_NUMBER = ?', $DEBIT_NUMBER)
							   ->where('F.USER_ID =?',$this->_userIdLogin)
							   ->where('T.CUST_ID =?',$this->_custIdLogin)
							   ->group('T.DEBIT_NUMBER')
                               );
		
		if(!empty($data)){
			foreach($data as $key => $val){
				
				if(!empty($val['USER_FULLNAME'])){
								$data[$key]['DEBIT_NAME'] = $val['USER_FULLNAME'];
							}else{
								$data[$key]['DEBIT_NAME'] = $val['CUST_NAME'];
							}	
							
							if(!empty($val['USER_EMAIL'])){
								$data[$key]['DEBIT_EMAIL'] = $val['USER_EMAIL'];
							}else{
								$data[$key]['DEBIT_EMAIL'] = $val['CUST_EMAIL'];
							}
				
						$datainq =  new Service_Account($val['ACCT_NO'],'000');
						$dataInquiry = $datainq->miniaccountInquiry('AB',FALSE); 
						
						if($dataInquiry['ResponseCode'] == '00'){
							$data[$key]['BALANCE'] = $dataInquiry['Balance'];
						}else{
							$data[$key]['BALANCE'] = 'N/A';
						}
			
										
				
			}
		}
		//var_dump($data);die;
		$statusarr = array(
					'1' => $this->language->_('Active'),
					'3' => $this->language->_('Delete'),
					'4' => $this->language->_('Suspend')
				);
				$this->view->statusarr = $statusarr;
		
		$fields = array(
						'number'  => array('field' => '',
											   'label' => $this->language->_('Debit Card Number'),
											   'sortable' => false),
						'name'  => array('field' => '',
											   'label' => $this->language->_('Debit Card Name'),
											   'sortable' => false),
						'email'   => array('field'    => '',
											  'label'    => $this->language->_('Debit Card Email'),
											  'sortable' => false),
						'balance'   => array('field'    => '',
											  'label'    => $this->language->_('Credit Balance'),
											  'sortable' => false),
						'status'   => array('field'    => '',
											  'label'    => $this->language->_('Status'),
											  'sortable' => false),
						'action'   => array('field'    => '',
											  'label'    => $this->language->_('Action'),
											  'sortable' => false),
				);

		//$data = array();
		
		//$data = $result['DataList'];
		
		//print_r($result);die;
		Application_Helper_General::writeLog('ERIQ',$logDesc);

		$this->view->userId = $this->_userIdLogin;
		$this->view->resultdata = $data;
		$this->view->fields = $fields;
	}
	
	public function changepinAction(){
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $params     = $this->_request->getParams();
		
		
		$request = array();
		$request['card_no'] = trim($params['cardno']);
		$request['old_pin'] = trim($params['curpass']);
		$request['new_pin'] = trim($params['newpass']);
		$number = rand(0,99999999);
		$trace = str_pad($number,8,0,STR_PAD_LEFT);
		$request['trace'] = trim($trace);
		$clientUser  =  new SGO_Soap_ClientUser();						
		$success = $clientUser->callapi('resetpin',$request);
		$result  = $clientUser->getResult();
		//$this->setbackURL('/'.$this->_request->getModuleName().'/index');
		//$this->_redirect('/notification/success/index');
		//echo '<pre>';
		//var_dump($result);die;
	}
	
}
