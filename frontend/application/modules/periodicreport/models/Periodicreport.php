<?php
class periodicreport_Model_Periodicreport
{
	protected $_db;
	
    // constructor
	public function __construct()
	{	
		$config = Zend_Registry::get('config');
		$this->_masterglobalstatus 	= $config['master']['globalstatus'];
		$this->_beneftype 			= $config['account']['beneftype'];
		$this->_paymentStatus 		= $config["payment"]["status"]; 
		$this->_paymentType 		= $config["payment"]["type"]; 
		$this->_transferType 		= $config["transfer"]["type"]; 
		$this->_transferStatus 		= $config["transfer"]["status"]; 
		$this->_historystatus 		= $config["history"]["status"]; 
		$this->_db 					= Zend_Db_Table::getDefaultAdapter();
		//periodic
		$this->_periodicStatus 		= $config["periodic"]["status"]; 
	}
	
 	public function getPayment($fParam,$sorting,$select)
    {//Zend_Debug::dump($fParam);die;
		$paytypesarr = array_combine(array_values($this->_paymentType['code']),array_values($this->_paymentType['desc']));
		$paystatusarr = array_combine(array_values($this->_paymentStatus['code']),array_values($this->_paymentStatus['desc']));

		if(isSet($fParam['psnumber']))
		{ 
			$select->where("UPPER(P.PS_NUMBER) LIKE ".$this->_db->quote('%'.$fParam['psnumber'].'%')); 
		}
		
		if(isSet($fParam['transferfrom']))
		{
			$select->where('date(P.PS_EFDATE) >= ?', $fParam['transferfrom']);
		}
		
		if(isSet($fParam['transferto']))
		{
			$select->where('date(P.PS_EFDATE) <= ?', $fParam['transferto']);
		}
		
		if(isSet($fParam['createdfrom']))
		{
			$select->where('date(P.PS_CREATED) >= ?', $fParam['createdfrom']);
		}
		
		if(isSet($fParam['createdto']))
		{
			$select->where('date(P.PS_CREATED) <= ?', $fParam['createdto']);
		}
		
		if(isSet($fParam['updatedfrom']))
		{
			$select->where('date(P.PS_UPDATED) >= ?', $fParam['updatedfrom']);
		}
		
		if(isSet($fParam['updatedto']))
		{
			$select->where('date(P.PS_UPDATED) <= ?', $fParam['updatedto']);
		}
		
		if(isSet($fParam['acctsource']))
		{ 
			$select->where("T.SOURCE_ACCOUNT LIKE ".$this->_db->quote('%'.$fParam['acctsource'].'%')); }


    	if(isSet($fParam['paymentreff']))
		{ 
			$select->where("UPPER(P.PS_NUMBER) LIKE ".$this->_db->quote('%'.$fParam['paymentreff'].'%')); 
		}	
			
			
		if(isSet($fParam['acctbene']))	
		{ 
			$select->where("T.BENEFICIARY_ACCOUNT LIKE ".$this->_db->quote('%'.$fParam['acctbene'].'%')); }
		
		if(isSet($fParam['paymentstatus']))	
		{ 
			$select->where("P.PS_STATUS = ? ", $fParam['paymentstatus']); 
		}
		
		if(isSet($fParam['paymenttype']))
		{ 
			$fPayType 	 	= explode(",", $fParam['paymenttype']);
			$select->where("P.PS_TYPE in (?) ", $fPayType);		
			//$select->where("P.PS_TYPE = ? ", $fParam['paymenttype']); 
		}
		
		if(isSet($fParam['transfertype']))
		{
			$select->where("UPPER(T.TRANSFER_TYPE) = ".$this->_db->quote($fParam['transfertype']));
		}
		
		$select->order($sorting);
 		//Zend_Debug::dump($select) ;
		return $this->_db->fetchAll($select);
    }
    
	public function getPeriodic($fParam,$sorting,$select)
    {
    	//Zend_Debug::dump($fParam);die;		
		$paystatusarr = array_combine(array_values($this->_paymentStatus['code']),array_values($this->_paymentStatus['desc']));
				
    	/*if(isSet($fParam['psnumber']))
		{ 
			$select->where("UPPER(P.PS_NUMBER) LIKE ".$this->_db->quote('%'.$fParam['psnumber'].'%')); 
		}*/
		
		/*if(isSet($fParam['updatedfrom']))
		{
			$select->where('date(P.PS_UPDATED) >= ?', $fParam['updatedfrom']);
		}
		
		if(isSet($fParam['updatedto']))
		{
			$select->where('date(P.PS_UPDATED) <= ?', $fParam['updatedto']);
		}*/
		
    	if(isSet($fParam['createdfrom']))
		{
			$select->where('date(P.PS_CREATED) >= ?', $fParam['createdfrom']);
		}
		
		if(isSet($fParam['createdto']))
		{
			$select->where('date(P.PS_CREATED) <= ?', $fParam['createdto']);
		}
				
    	/*if(isSet($fParam['paymentreff']))
		{ 
			$select->where("UPPER(P.PS_NUMBER) LIKE ".$this->_db->quote('%'.$fParam['paymentreff'].'%')); 
		}*/			

    	if(isSet($fParam['acctsource']))
		{ 
			$select->where("T.SOURCE_ACCOUNT LIKE ".$this->_db->quote('%'.$fParam['acctsource'].'%')); }
			
		
    	if(isSet($fParam['beneaccount']))	
		{ 
			$select->where("T.BENEFICIARY_ACCOUNT LIKE ".$this->_db->quote('%'.$fParam['beneaccount'].'%')); }
								
		/*if(isSet($fParam['paymentstatus']))	
		{ 
			$select->where("P.PS_STATUS = ? ", $fParam['paymentstatus']); 
		}*/
    	if(isSet($fParam['paymenttype']))
		{ 			
			$fPayType 	 	= explode(",", $fParam['paymenttype']);
			$select->where("P.PS_TYPE in (?) ", $fPayType);
		}

    	if(isSet($fParam['periodicstatus']))
		{ 
			$select->where("PR.PS_PERIODIC_STATUS in (?) ", $fParam['periodicstatus']);
		}
				
		$select->where('P.PS_STATUS != ?', '5');
		$select->where('P.PS_TYPE not in (14,15)');
		
		$select->order($sorting);
 		//echo $select; die;
		return $this->_db->fetchAll($select);
    }
    
    public function getPaymentDetail($psNumber)
    {
    	$paytypesarr = array_combine(array_values($this->_paymentType['code']),array_values($this->_paymentType['desc']));
		$paystatusarr = array_combine(array_values($this->_paymentStatus['code']),array_values($this->_paymentStatus['desc']));
		$transtypesarr = array_combine(array_values($this->_transferType['code']),array_values($this->_transferType['desc']));
		$transstatusarr = array_combine(array_values($this->_transferStatus['code']),array_values($this->_transferStatus['desc']));
		
    	$casePayType = "(CASE P.PS_TYPE ";
  		foreach($paytypesarr as $key=>$val)
  		{
   			$casePayType .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$casePayType .= " WHEN '110' OR '120' THEN PS_CATEGORY";
  			$casePayType .= " ELSE 'N/A' END)";
  			
  		$casePayStatus = "(CASE P.PS_STATUS ";
  		foreach($paystatusarr as $key=>$val)
  		{
   			$casePayStatus .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$casePayStatus .= " ELSE 'N/A' END)";
  			
  		$caseTransType = "(CASE T.TRANSFER_TYPE ";
  		foreach($transtypesarr as $key=>$val)
  		{
   			$caseTransType .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseTransType .= " ELSE 'N/A' END)";
  			
  		$caseTransType = "(CASE T.TRANSFER_TYPE ";
  		foreach($transtypesarr as $key=>$val)
  		{
   			$caseTransType .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseTransType .= " ELSE 'N/A' END)";
  			
  		$caseTransStatus = "(CASE T.TRA_STATUS ";
  		foreach($transstatusarr as $key=>$val)
  		{
   			$caseTransStatus .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseTransStatus .= " ELSE 'N/A' END)";
  			
		$select	= $this->_db->select()
						->from(	array('P' => 'T_PSLIP'),array())
						->join(	array('U' => 'M_USER'), 'P.USER_ID = U.USER_ID', array())
						->join(	array('T' => 'T_TRANSACTION'), 'P.PS_NUMBER = T.PS_NUMBER', array(	'P.*','U.*','T.*',
																									'ACCT_SOURCE' => new Zend_Db_Expr('CONCAT(T.SOURCE_ACCOUNT," (",T.SOURCE_ACCOUNT_CCY,") ",T.SOURCE_ACCOUNT_NAME," / ",T.SOURCE_ACCOUNT_ALIAS_NAME)'),									
																									'ACCT_BENE' => new Zend_Db_Expr('CONCAT(T.BENEFICIARY_ACCOUNT," (",T.BENEFICIARY_ACCOUNT_CCY,") ",T.BENEFICIARY_ACCOUNT_NAME," / ",T.BENEFICIARY_ALIAS_NAME)'),
																									'PS_STATUS'	=> $casePayStatus,
																									'PS_TYPE' => $casePayType,
																									'TRANSFER_TYPE' => $caseTransType,
																									'CEK_PS_TYPE' => 'P.PS_TYPE',
																									'TRANSFER_STATUS' => $caseTransStatus))
						->where("P.PS_NUMBER = ? ", $psNumber);
		return $this->_db->fetchAll($select);
    }
    
	public function getPslipDetail($psNumber)
    {
    	$select	= $this->_db->select()
						->from(	array('P' => 'T_PSLIP_DETAIL'),array('*'))
						->where("P.PS_NUMBER = ? ", $psNumber);
		return $this->_db->fetchAll($select);
    }
    
 	public function getHistory($psNumber)
    {
    	$historyarr = array_combine(array_values($this->_historystatus['code']),array_values($this->_historystatus['desc']));
    	
    	$caseHistory = "(CASE P.HISTORY_STATUS ";
  		foreach($historyarr as $key=>$val)
  		{
   			$caseHistory .= " WHEN ".$key." THEN '".$val."'";
  		}
  			$caseHistory .= " ELSE 'N/A' END)";
    	
    	$select	= $this->_db->select()
						->from(	array('P' => 'T_PSLIP_HISTORY'),array('*','HISTORY_STATUS' => $caseHistory))
						->where("P.PS_NUMBER = ? ", $psNumber);
    	$select -> order('DATE_TIME DESC');
		return $this->_db->fetchAll($select);
    }
}