<?php

require_once 'Zend/Controller/Action.php';
require_once 'General/Customer.php';
require_once 'General/CustomerUser.php';

class periodicreport_IndexController extends Application_Main 
{	
	public function indexAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$getPeriodic 	= new periodicreport_Model_Periodicreport();
		$CustomerUser 	= new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
		
		$filterSend = array();
		
		$payType 	= Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
		
		//$arrPayType 	= array_combine($this->_paymenttype["code"],$this->_paymenttype["desc"]);
		
		$arrPayType 	= Application_Helper_General::filterPaymentType($this->_paymenttype, $this->_transfertype);
		
		$arrPayStatus 	= array_combine($this->_paymentstatus["code"],$this->_paymentstatus["desc"]);
		$trfType 		= array_combine($this->_transfertype["code"], $this->_transfertype["desc"]);
		
		//periodic
		$arrPerStatus 	= array_combine($this->_periodicstatus["code"],$this->_periodicstatus["desc"]);
		//Zend_Debug::dump($arrPerStatus); die;
		
		$arrAccount 	= $CustomerUser->getAccounts();
		
		foreach($payType as $key => $value){ if($key != 3) $optpaytypeRaw[$key] = $this->language->_($value); }
		foreach($trfType as $key => $value){ if($key != 3 && $key != 4) $filterTrfType[$key] = $value; }
		
		foreach($arrPayStatus as $key => $value){ if($key != 3) $optpayStatusRaw[$key] = $this->language->_($value); }

		//periodic
		foreach($arrPerStatus as $key => $value){
			 $optperStatusRaw[$key] = $this->language->_($value); 
		}
						
		foreach($arrPayType as $key => $value){
			if($key != 3){
				$paytypeRaw[$key] = $value;
			}
		}
		
		if(is_array($arrAccount) && count($arrAccount) > 0){
			foreach($arrAccount as $key => $value){
				
				$val 		= $arrAccount[$key]["ACCT_NO"];
				$ccy 		= $arrAccount[$key]["CCY_ID"];
				$acctname 	= $arrAccount[$key]["ACCT_NAME"];
				$acctalias 	= $arrAccount[$key]["ACCT_ALIAS_NAME"];
				$accttype 	= $arrAccount[$key]["ACCT_TYPE"];
				
				$arrAccountRaw[$val] = $val.' ['.$ccy.'] '.$acctname.'/'.$acctalias.' ('.$accttype.')';
				
			}
		}
		else { $arrAccountRaw = array();}
		
		$paramPayment = array(
							  "ACCOUNT_LIST" 	=> $this->_accountList,
							 );
		
		// get periodic query
		$select   = $CustomerUser->getPeriodic($paramPayment);
		$pleaseSelect = $this->language->_('Please Select');
		
		$opt[""] = "-- $pleaseSelect --";
		$optPayType 	= $opt + $optpaytypeRaw;		
		unset($optPayType["6,4"]);
		unset($optPayType["7,5"]);
		unset($optPayType["11,11"]);
		unset($optPayType["14,14"]);
		unset($optPayType["15,15"]);
		
		//$optPayStatus 	= $opt + $arrPayStatus;
		$optPayStatus 	= $opt + $optpayStatusRaw;
		unset($optPayStatus[3]);
		unset($optPayStatus[1]);
		unset($optPayStatus[2]);
		unset($optPayStatus[10]);
		unset($optPayStatus[15]);
		unset($optPayStatus[16]);
		
		
		//periodic
		//Zend_Debug::dump($optpayStatusRaw); die;
		$optPerStatus 	= $opt + $optperStatusRaw;
		
		
		$optarrAccount 	= $opt + $arrAccountRaw;
		unset($optPayType[8]);
		//Zend_Debug::dump($arrPayStatus);
		$this->view->optPayType 	= $optPayType;
		$this->view->optPayStatus 	= $optPayStatus;
		$this->view->optPerStatus 	= $optPerStatus;
		$this->view->optarrAccount 	= $optarrAccount;	
		
		$filterSend = array();
		
		$paymentref = $this->language->_('Payment Ref');
		$paymenttype_lg = $this->language->_('Payment Type');
		$tranfertype = $this->language->_('Tranfer Type');
		$anyvalue = $this->language->_('Any Value'); 
		$status = $this->language->_('Status'); 
		$setfilter = $this->language->_('Set Filter'); 
		$clearfilter = $this->language->_('Clear Filter'); 
		$nodata = $this->language->_('No Data'); 
		
		$createddate = $this->language->_('Created Date');
		$subject = $this->language->_('Subject'); 
		$sourceaccount = $this->language->_('Source Account'); 
		$sourceaccountname = $this->language->_('Source Account Name'); 
		$beneficiaryaccount = $this->language->_('Beneficiary Account'); 
		$beneficiarynamealias = $this->language->_('Beneficiary Name'); 
		$amount = $this->language->_('Amount'); 		
		$periodic = $this->language->_('Periodic');
		$startdate = $this->language->_('Star Date');
		$enddate = $this->language->_('End Date');
		$periodicstatus = $this->language->_('Status'); 
		$ccy_lg = $this->language->_('CCY');
		$action = $this->language->_('Action');
		
		
		
		
		$fields = array	(	/*'payReff'  					=> array	(
																		'field' => 'payReff',
																		'label' => $paymentref,
																		'sortable' => true
																),
							'created'  					=> array	(
																		'field' => 'created',
																		'label' => $createddate,
																		'sortable' => true
																	),*/							
							'accsrc'  					=> array	(
																		'field' => 'accsrc',
																		'label' => $sourceaccount,
																		'sortable' => true
																	),
							'accsrc_name' 				=> array	(
																		'field' => 'accsrc_name',
																		'label' => $sourceaccountname,
																		'sortable' => true
																	),
							'acbenef'  					=> array	(
																		'field' => 'acbenef',
																		'label' => $beneficiaryaccount,
																		'sortable' => true
																	),
							'acbenef_name'  			=> array	(
																		'field' => 'acbenef_name',
																		'label' => $beneficiarynamealias,
																		'sortable' => true
																	),							
							'bankName' 					 => array	(
																		'field' => 'bankName',
																		'label' => 'Bank',
																		'sortable' => true
																	),
							'ccy' 						 => array	(
																		'field' => 'ccy',
																		'label' => $ccy_lg,
																		'sortable' => false
																	),
							'amount'  					=> array	(
																		'field' => 'amount',
																		'label' => $amount,
																		'sortable' => true
																	),
							'periodicVal'  					=> array	(
																		'field' => 'periodicVal',
																		'label' => $periodic,
																		'sortable' => true
																	),
							'StartDate'  					=> array	(
																		'field' => 'StartDate',
																		'label' => $startdate,
																		'sortable' => true
																	),
							'EndDate'  					=> array	(
																		'field' => 'EndDate',
																		'label' => $enddate,
																		'sortable' => true
																	),																	
							'perStatus'    				=> array	(
																		'field' => 'perStatus',
																		'label'  => $periodicstatus,
																		'sortable' => false
																	),
							/*'action'    				=> array	(
																		'field' => '',
																		'label'  => $action,
																		'sortable' => false
																	),*/
							/*'payType'  					=> array	(
																		'field'	=> 'payType',
																		'label' 	=> $paymenttype_lg,
																		'sortable' => true
																	),*/
						);
		
		//validasi page, jika input page bukan angka               
		$page 			= $this->_getParam('page');
		$csv 			= $this->_getParam('csv');
		$pdf 			= $this->_getParam('pdf');
		$filter 		= $this->_getParam('filter');
		$filter_clear 		= $this->_getParam('filter_clear');
			
		$page 	= (Zend_Validate::is($page,'Digits'))? $page : 1;
		
		//validasi sort, jika input sort bukan ASC atau DESC
		// $sortBy  = $this->_getParam('sortby');
		$sortBy  = ($this->_getParam('sortby')) ?  $this->_getParam('sortby') : ('updateddate');
		$sortBy  = (Zend_Validate::is($sortBy,'InArray',array(array_keys($fields))))? $fields[$sortBy]['field'] : $fields[key($fields)]['field'];
		
		$sortDir = $this->_getParam('sortdir');
		$sortDir = (Zend_Validate::is($sortDir,'InArray',array('haystack'=>array('asc','desc'))))? $sortDir : 'desc';
		
		$this->view->currentPage = $page;
		$this->view->sortBy      = $sortBy;
		$this->view->sortDir     = $sortDir;
		
		$data = array();
		
		$filterArr = array(	/*'updatedStart' 	=> array('StringTrim','StripTags'),
							'updatedEnd' 	=> array('StringTrim','StripTags'),*/
							'createdStart' 	=> array('StringTrim','StripTags'),
							'createdEnd' 	=> array('StringTrim','StripTags'),
							/*'paymentStart' 	=> array('StringTrim','StripTags'),
							'paymentEnd' 	=> array('StringTrim','StripTags'),*/
							'accsrc' 		=> array('StringTrim','StripTags'),
							'accbene' 		=> array('StringTrim','StripTags'),
							/*'payReff' 		=> array('StringTrim','StripTags','StringToUpper'),*/
							'periodicStatus'	=> array('StringTrim','StripTags'),
							'paymentType' 	=> array('StringTrim','StripTags'),
							//'transferType' 	=> array('StringTrim','StripTags'),	*/		
		);
		
		// if POST value not null, get post, else get param
		//$dataParam = array("updatedStart","updatedEnd","createdStart","createdEnd","paymentStart","paymentEnd","accsrc","accbene","payReff","paymentStatus","paymentType","transferType");
		$dataParam = array('PS_CREATED','SOURCE_ACCOUNT','PS_EFDATE','PS_STATUS','PS_TYPE');
		$dataParamValue = array();

		foreach ($dataParam as $dtParam)
		{

			// print_r($dtParam);die;
			if(!empty($this->_request->getParam('wherecol'))){
				$dataval = $this->_request->getParam('whereval');
					foreach ($this->_request->getParam('wherecol') as $key => $value) {
						if($dtParam==$value){
							$dataParamValue[$dtParam] = $dataval[$key];
						}
					}

			}

			// $dataPost = $this->_request->getPost($dtParam);
			// $dataParamValue[$dtParam] = ($dataPost != null)? $dataPost: $this->_getParam($dtParam);
		}



		// The default is set so all fields allow an empty string		
		$options = array('allowEmpty' => true);
			$validators = array(
						/*'updatedStart' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'updatedEnd' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),*/
						'createdStart'	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'createdEnd' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						/*'paymentStart' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),	
						'paymentEnd' 	=> array(new Zend_Validate_Date($this->_dateDisplayFormat)),*/	
						'accsrc' 		=> array(array('InArray', array('haystack' => array_keys($optarrAccount)))),	// $filter!
						'accbene'		=> array(),	
						//'payReff'		=> array(),	
						'periodicStatus' => array(array('InArray', array('haystack' => array_keys($arrPerStatus)))),	// $filter!
						'paymentType' => array(array('InArray', array('haystack' => array_keys($arrPayType)))),	// $filter!
						//'paymentType' 	=> array(array('InArray', array('haystack' => array_keys($arrAccountRaw)))),	// $filter!
						//'transferType' => array(array('InArray', array('haystack' => array_keys($filterTrfType)))),	// $filter!
							
						);

		$filterlist = array('PS_CREATED','PS_NUMBER','SOURCE_ACCOUNT','PS_EFDATE','PS_UPDATED','PS_STATUS','PS_TYPE','TRANSFER_TYPE');

		$this->view->filterlist = $filterlist;

		$zf_filter  	= new Zend_Filter_Input($filterArr, $validators, $dataParamValue, $options, $this->_request->getParams());
		
		$fCreatedStart 	= $zf_filter->getEscaped('createdStart');
		$fCreatedEnd 	= $zf_filter->getEscaped('createdEnd');
		/*$fUpdatedStart 	= $zf_filter->getEscaped('updatedStart');
		$fUpdatedEnd 	= $zf_filter->getEscaped('updatedEnd');
		$fPaymentStart 	= $zf_filter->getEscaped('paymentStart');
		$fPaymentEnd 	= $zf_filter->getEscaped('paymentEnd');*/
		
		$fAcctsrc 		= $zf_filter->getEscaped('accsrc');
		$fAcctbene		= $zf_filter->getEscaped('accbene');
	
		//$fPaymentReff 	= $zf_filter->getEscaped('payReff');
		$fPeriodicStatus = $zf_filter->getEscaped('periodicStatus');
		$fPaymentType 	= $zf_filter->getEscaped('paymentType');
		//$fTransferType 	= $zf_filter->getEscaped('transferType');*/
		
		$zf_filterx 	= new Zend_Filter_Input($filterArr,array(),$this->_request->getParams());
		$action_approve 	= html_entity_decode($zf_filterx->getEscaped('reqaction_approve'));
		if($action_approve == true)
		{
			echo "test";
		}
		
		
		if($filter == null)
		{	
			$fUpdatedStart 	= date("d/m/Y");
			$fUpdatedEnd 	= date("d/m/Y");
			$this->view->updatedStart  = $fUpdatedStart;
			$this->view->updatedEnd  = $fUpdatedEnd;
			
			$FormatDate 	= new Zend_Date($fUpdatedStart, $this->_dateDisplayFormat);
			$fUpdatedStart  = $FormatDate->toString($this->_dateDBFormat);	
			$filterSend['updatedfrom'] = $fUpdatedStart;
			
			$FormatDate 	= new Zend_Date($fUpdatedEnd, $this->_dateDisplayFormat);
			$fUpdatedEnd  = $FormatDate->toString($this->_dateDBFormat);	
			$filterSend['updatedto'] = $fUpdatedEnd;
			
		}


		
		if($filter == TRUE || $csv || $pdf )
		{
			/*if($fPaymentStart!=null)
			{
				$this->view->paymentStart = $fPaymentStart;
				$FormatDate 	= new Zend_Date($fPaymentStart, $this->_dateDisplayFormat);
				$fPaymentStart  = $FormatDate->toString($this->_dateDBFormat);	
				$filterSend['transferfrom'] = $fPaymentStart;
			}
			
			if($fPaymentEnd!=null)
			{
				$this->view->paymentEnd = $fPaymentEnd;
				$FormatDate 	= new Zend_Date($fPaymentEnd, $this->_dateDisplayFormat);
				$fPaymentEnd  	= $FormatDate->toString($this->_dateDBFormat);	
				$filterSend['transferto'] = $fPaymentEnd;
			}*/
			
			if($fCreatedStart!=null)
			{
				$this->view->createdStart = $fCreatedStart;
				$FormatDate 	= new Zend_Date($fCreatedStart, $this->_dateDisplayFormat);
				$fCreatedStart  	= $FormatDate->toString($this->_dateDBFormat);	
				$filterSend['createdfrom'] = $fCreatedStart;
			}
			
			if($fCreatedEnd!=null)
			{
				$this->view->createdEnd = $fCreatedEnd;
				$FormatDate 	= new Zend_Date($fCreatedEnd, $this->_dateDisplayFormat);
				$fCreatedEnd  	= $FormatDate->toString($this->_dateDBFormat);	
				$filterSend['createdto'] = $fCreatedEnd;
			}
			
			/*if($fUpdatedStart!=null)
			{
				$this->view->updatedStart = $fUpdatedStart;
				$FormatDate 	= new Zend_Date($fUpdatedStart, $this->_dateDisplayFormat);
				$fUpdatedStart  	= $FormatDate->toString($this->_dateDBFormat);	
				$filterSend['updatedfrom'] = $fUpdatedStart;
			}
			
			if($fUpdatedEnd!=null)
			{
				$this->view->updatedEnd = $fUpdatedEnd;
				$FormatDate 	= new Zend_Date($fUpdatedEnd, $this->_dateDisplayFormat);
				$fUpdatedEnd  	= $FormatDate->toString($this->_dateDBFormat);	
				$filterSend['updatedto'] = $fUpdatedEnd;
			}*/
			
			if($fAcctsrc!=null)
			{ 
				$filterSend['acctsource'] = $fAcctsrc; 
				$this->view->accsrc = $fAcctsrc;
			}
			
						
			/*if($fPaymentReff!=null)
			{ 
				$filterSend['paymentreff'] = $fPaymentReff;
				$this->view->payReff = $fPaymentReff;
			}*/
			
			
			if($fAcctbene!=null)	
			{ 
				$filterSend['beneaccount'] = $fAcctbene;
				$this->view->accbene = $fAcctbene;
			}
			
			if($fPeriodicStatus!=null)
			{ 
				$filterSend['periodicstatus'] = $fPeriodicStatus;
				$this->view->periodicStatus = $fPeriodicStatus;		
			}
			
			//echo $fPaymentType; die;
			if($fPaymentType!=null)
			{ 
				$filterSend['paymenttype'] = $fPaymentType;
				$this->view->paymentType = $fPaymentType;
			}
			
			/*if($fTransferType != null)
			{
				$filterSend['transfertype'] = $fTransferType;
				$this->view->transferType = $fTransferType;
			}*/
		}
		
		if($filter_clear == TRUE){
			$fUpdatedStart 	= date("d/m/Y");
			$fUpdatedEnd 	= date("d/m/Y");
			$this->view->updatedStart  = '';
			$this->view->updatedEnd  = '';
			
			$FormatDate 	= new Zend_Date('00/00/0000', $this->_dateDisplayFormat);
			$fUpdatedStart  = $FormatDate->toString($this->_dateDBFormat);	
			$filterSend['updatedfrom'] = $fUpdatedStart;
			
			$FormatDate 	= new Zend_Date($fUpdatedEnd, $this->_dateDisplayFormat);
			$fUpdatedEnd  = $FormatDate->toString($this->_dateDBFormat);	
			$filterSend['updatedto'] = $fUpdatedEnd;
		}
		
		$sorting = $sortBy.' '.$sortDir;
		
		//$result = $getPayment -> getPayment($filterSend,$sorting,$select);
		//Zend_Debug::dump($select);die;
		$result = $getPeriodic -> getPeriodic($filterSend,$sorting,$select);
		
		if ($csv || $pdf) 
		{
			$arr = $result;
			$header  = Application_Helper_Array::simpleArray($fields, "label");
			//Zend_Debug::dump($result); die;
			foreach($result as $key => $val)
			{
				$arr[$key]["amount"] = Application_Helper_General::displayMoney($val["amount"]);
				//$arr[$key]["created"] = Application_Helper_General::convertDate($val["created"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
				//$arr[$key]["updated"] = Application_Helper_General::convertDate($val["updated"],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
				$arr[$key]["StartDate"] = Application_Helper_General::convertDate($val["StartDate"],$this->view->viewDateFormat,$this->view->defaultDateFormat);
				$arr[$key]["EndDate"] = Application_Helper_General::convertDate($val["EndDate"],$this->view->viewDateFormat,$this->view->defaultDateFormat);
				
				$arr[$key]["perStatus"] = $this->language->_($val['perStatus']);
				
				if ($arr[$key]["periodicVal"] == 5){
					
					$as = array(
    								'1' => $this->language->_('Monday'), 
    								'2' => $this->language->_('Tuesday'), 
    								'3' => $this->language->_('Wednesday'), 
    								'4' => $this->language->_('Thursday'), 
    								'5' => $this->language->_('Friday'), 
    								'6' => $this->language->_('Saturday'), 
    								'0' => $this->language->_('Sunday'),);
					$dayName = $as [$arr[$key]["periodicType"]];
					$arr[$key]["periodicVal"] = $arr[$key]["perTypeVal"]. " ".$dayName ; 
				}elseif ($arr[$key]["periodicVal"] == 6){
					$arr[$key]["periodicVal"] = $arr[$key]["perTypeVal"]. " ".$arr[$key]["periodicType"] ; 
				}
								
								
				$data[] = array_intersect_key(array_merge(array_flip(array_keys($fields)), $arr[$key]),$fields);
			}
			
			if($csv)
			{
				//Zend_Debug::dump($data); die;
				$this->_helper->download->csv($header,$data,null,'Periodic Report');  
				Application_Helper_General::writeLog('DARC','Export CSV Periodic Report');
			}
			if($pdf)
			{
				$this->_helper->download->pdf($header,$data,null,'Periodic Report');  
				Application_Helper_General::writeLog('DARC','Export PDF Periodic Report');
			}
		}
		else
		{
			//Application_Helper_General::writeLog('DARC','View Payment');
			Application_Helper_General::writeLog('RPPY','View Periodic Report');
		}
		
		if(!empty($dataParamValue)){

			$this->view->createdStart = $dataParamValue['PS_CREATED'];
			$this->view->createdEnd = $dataParamValue['PS_CREATED_END'];
			$this->view->efdateStart = $dataParamValue['PS_UPDATED'];
			$this->view->efdateEnd = $dataParamValue['PS_UPDATED_END'];
			$this->view->efdateStart = $dataParamValue['PS_EFDATE'];
			$this->view->efdateEnd = $dataParamValue['PS_EFDATE_END'];


		  	unset($dataParamValue['PS_CREATED_END']);
			unset($dataParamValue['PS_EFDATE_END']);
			unset($dataParamValue['PS_UPDATED_END']);

				foreach ($dataParamValue as $key => $value) {
					$wherecol[]	= $key;
					$whereval[] = $value;
				}
		    $this->view->wherecol     = $wherecol;
		    $this->view->whereval     = $whereval;
		}

		$this->paging($result);
		$this->view->fields = $fields;
		$this->view->filter = $filter;
	}
	
	public function cancelAction()
	{
		$PS_NUMBER = $this->_getParam('PS_NUMBER');
		$PS_PERIODIC = $this->_getParam('PS_PERIODIC');
		$this->_db->beginTransaction();
		
		// update PSLIP
		$data = array ('PS_STATUS' => '14');
		$where['PS_NUMBER = ?'] = $PS_NUMBER;
		$this->_db->update('T_PSLIP',$data,$where);
		//die;
		
		// update PERIODIC
		$data2 = array ('PS_PERIODIC_STATUS' => '0');
		$where2['PS_PERIODIC = ?'] = $PS_PERIODIC;
		$this->_db->update('T_PERIODIC',$data2,$where2);
		
		// update TRANSACTION
		$data3 = array ('TRA_STATUS' => '1');
		$where3['PS_NUMBER = ?'] = $PS_NUMBER;
		$this->_db->update('T_TRANSACTION',$data3,$where3);
		
		$this->_db->commit();
		$this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
		$this->_redirect('/notification/success');
		
	}
}
