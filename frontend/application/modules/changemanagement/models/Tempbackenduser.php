<?php
/**
 * Tempuser model
 * 
 * @author 
 * @version
 */
require_once ('Crypt/AESMYSQL.php');
require_once 'General/BankUser.php';

class Changemanagement_Model_Tempbackenduser extends Changemanagement_Model_Tempchanges 
{
	protected $_moduleId = 'CUS';
	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function approveNew($actor = null){
	  
		//user password = autogenerate
		//perlu insert ke T_REQ_PIN_MAILER
		//query from TEMP_BUSER
		$user = $this->dbObj->select()
						  ->from('TEMP_BUSER')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($user)){
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(User)';
			return false;
        }
        
        $encrypt = new Crypt_AESMYSQL ();
        $password = new BankUser($user['BUSER_ID']);
        $generate = $password->generateNewPassword();
        
		
		//--------------------------------INSERT TO TABLE M_BUSER----------------------------------------------------------
		$insertArr = array_diff_key($user,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		$insertArr['CREATED']     = new Zend_Db_Expr('now()');
		$insertArr['CREATEDBY']   = $actor;
		$insertArr['SUGGESTED']     = new Zend_Db_Expr('now()');
		$insertArr['SUGGESTEDBY']   = $actor;
		$insertArr['BUSER_ISLOCKED'] = 0;
		$insertArr['BUSER_ISNEW'] = 1;
		$insertArr['BUSER_RCHANGE'] = 1;
		$insertArr['BUSER_RRESET'] = 0;
		$insertArr['BUSER_RPASSWORD_ISEMAILED'] = 0;
		$insertArr['BUSER_RPASSWORD_ISPRINTED'] = 0;
		$insertArr['BUSER_ISEMAILPWD'] = 0;
		//$insertArr['BUSER_LASTACTIVITY'] = new Zend_Db_Expr('now()');
		$insertArr['BUSER_ISREQUIRE_CHANGEPWD'] = 1;
		$insertArr['BUSER_STATUS'] = 2;
		$insertArr['BUSER_CLEARTEXT_PASSWORD'] = $generate['CLEAR_TEXT_PASSWORD'];
		$insertArr['BUSER_PASSWORD'] = $generate['ENCRYPTED_PASSWORD'];
		
		//Zend_Debug::dump($insertArr);die;
		
		$userins = $this->dbObj->insert('M_BUSER',$insertArr);
		
		if(!(boolean)$userins) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(User)';
			return false;
		}	
		//-------------------------------END INSERT TO TABLE M_BUSER-------------------------------------------------------
		
		$deleteChanges = $this->deleteNew();
		if(!$deleteChanges)
			return false;
			
		return true;
	}
	
	/**
     * Approve Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function approveEdit($actor = null) {
		//query from TEMP_BUSER
		$user = $this->dbObj->select()
						  	->from('TEMP_BUSER')
						  	->where('CHANGES_ID = ?',$this->_changeId)
						  	->query()
						  	->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($user)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(User)';
			return false;
        }

		//Validation, user can only be activated if this user's customer is active
		
        //query from M_BUSER
		$m_BUSER = $this->dbObj->select()
						  	  ->from('M_BUSER')
						  	  ->where('BUSER_ID = ?',$user['BUSER_ID'])
						  	  ->query()
						  	  ->fetch(Zend_Db::FETCH_ASSOC);
		//Zend_Debug::dump($m_BUSER);die;
		if(!count($m_BUSER))
		{
					
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(User)';
			return false;
        }								  	  
		
      
						  	
		//update record
		$updateArr = array_diff_key($user,array('TEMP_ID'=>'','CHANGES_ID'=>'','BUSER_ID'=>'','BUSER_STATUS'=>'','BUSER_PASSWORD'=>'',
		                                        'BUSER_ISLOGIN'=>'','BUSER_ISLOCKED'=>'','BUSER_FAILEDATTEMPT'=>'','BUSER_CREATED'=>'','BUSER_CREATEDBY'=>''));
		$updateArr['UPDATED']     = new Zend_Db_Expr('now()');
		$updateArr['UPDATEDBY']   = $actor;
		
		$whereArr = array('BUSER_ID = ?'=>$user['BUSER_ID']);
		
		$userupdate = $this->dbObj->update('M_BUSER',$updateArr,$whereArr);
		
		if(!(boolean)$userupdate) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(User)';
			return false;
		}

		$deleteChanges  = $this->deleteEdit();
		if(!$deleteChanges)return false;
		
		return true;
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveActivate($actor = null) {
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveDeactivate($actor = null) {
		
	}
	
	public function approveSuspend($actor=NULL) {
		$backendUser = $this->dbObj->select()
									 ->from('TEMP_BUSER')
									 ->where('CHANGES_ID = ?',$this->_changeId)
									 ->query()
									 ->fetch(Zend_Db::FETCH_ASSOC);

		if(!count($backendUser)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Backend User)';
			return false;
        }
        
        $whereArr = array('BUSER_ID = ?'=>$backendUser['BUSER_ID']);
		//update to master table M_BACKEND GROUP
		$updateArr = array_diff_key($backendUser,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		$updateArr['UPDATED'] = new Zend_Db_Expr('now()');
		$updateArr['UPDATEDBY'] = $actor;
		$updateBackendGroup = $this->dbObj->UPDATE('M_BUSER',$updateArr,$whereArr);
		
		$deleteChanges  = $this->deleteSuspend();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	public function approveUnsuspend($actor=NULL) {
		$backendUser = $this->dbObj->select()
									 ->from('TEMP_BUSER')
									 ->where('CHANGES_ID = ?',$this->_changeId)
									 ->query()
									 ->fetch(Zend_Db::FETCH_ASSOC);

		if(!count($backendUser)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Backend User)';
			return false;
        }
        
        $whereArr = array('BUSER_ID = ?'=>$backendUser['BUSER_ID']);
		//update to master table M_BACKEND GROUP
		$updateArr = array_diff_key($backendUser,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		$updateArr['UPDATED'] = new Zend_Db_Expr('now()');
		$updateArr['UPDATEDBY'] = $actor;
		$updateBackendGroup = $this->dbObj->UPDATE('M_BUSER',$updateArr,$whereArr);
		
		$deleteChanges  = $this->deleteUnsuspend();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	public function approveDelete($actor = null) 
	{
		//query from TEMP_BUSER
		$backendUser = $this->dbObj->select()
						  ->from('TEMP_BUSER')
						  ->where('CHANGES_ID = ?',$this->_changeId)
						  ->query()
						  ->fetch(Zend_Db::FETCH_ASSOC);
						  
		if(!count($backendUser))
		{
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(User)';
			return false;
        }

		//Validation, user can only be activated if this user's customer is active
	 	$whereArr = array('BUSER_ID = ?'=>$backendUser['BUSER_ID']);
		//update to master table M_BACKEND GROUP
		$updateArr = array_diff_key($backendUser,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		$updateArr['BUSER_ISLOGIN']='0';
		$updateArr['BUSER_ISLOCKED']='0';
		$updateArr['BUSER_FAILEDATTEMPT']='0';
		$updateArr['BUSER_LOCKREASON']='';
		$updateArr['UPDATED'] = new Zend_Db_Expr('now()');
		$updateArr['UPDATEDBY'] = $actor;
		$updateBackendGroup = $this->dbObj->UPDATE('M_BUSER',$updateArr,$whereArr);
		
		if(!(boolean)$updateBackendGroup) 
		{
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(User)';
			return false;
		}
		
		$deleteChanges  = $this->deleteDelete();
		if(!$deleteChanges)
			      return false;
		
		return true;
	}
	
    /**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew() {

		//delete from TEMP_BUSER
		$userdelete = $this->dbObj->delete('TEMP_BUSER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		
		return true;
	}
	
	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteEdit() {

		//delete from TEMP_BUSER
		$userdelete = $this->dbObj->delete('TEMP_BUSER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		
		return true;
	}
	
/**
     * Delete Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteActivate() {
	}
	
	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteDeactivate() {
	}
	
	public function deleteSuspend() {
		$userdelete = $this->dbObj->delete('TEMP_BUSER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		return true;
	}
	
	public function deleteUnsuspend() {
		$userdelete = $this->dbObj->delete('TEMP_BUSER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		return true;
	}
	
	public function deleteDelete() 
	{
		$userdelete = $this->dbObj->delete('TEMP_BUSER',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
		return true;
	}
}