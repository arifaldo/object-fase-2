<?php
/**
 * Tempwelcome model
 * 
 * @author
 * @version
 */

class Changemanagement_Model_Tempminimumamount extends Changemanagement_Model_Tempchanges {
	
	protected $_moduleId = 'MAC';
	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function approveNew($actor = null) {
		//query from TEMP_SETTING
		$minamt = $this->dbObj->select()
						  	  ->from('TEMP_MINAMT_CCY')
						  	  ->where('CHANGES_ID = ?',$this->_changeId)
						  	  ->query()
						  	  ->fetch(Zend_Db::FETCH_ASSOC);

        if(!count($minamt)){
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Min Amt)';
			return false;
		}
						  	  
		$insertArr = array_diff_key($minamt,array('TEMP_ID'=>'','CHANGES_ID'=>''));
		$insertArr['CREATED'] = new Zend_Db_Expr('now()');
		$insertArr['LASTUPDATED'] = new Zend_Db_Expr('now()');
		$insertArr['CREATEDBY'] = $actor;
		$minamtinsert = $this->dbObj->insert('M_MINAMT_CCY',$insertArr);
		if(!(boolean)$minamtinsert) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Min Amt)';
			return false;
		}

		$deleteChanges  = $this->deleteNew();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	/**
     * Approve Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function approveEdit($actor = null) {
		//query from TEMP_SETTING
		$minamtData = $this->dbObj->fetchAll($this->dbObj->select()
						  	  ->from('TEMP_MINAMT_CCY')
						  	  ->where('CHANGES_ID = ?',$this->_changeId)
						  	  );
        if(is_array($minamtData)){
        	
        	$this->deleteMaster();
        	foreach ($minamtData as $minamt) {
        		
			$insertArr = array_diff_key($minamt,array('TEMP_ID'=>'','CHANGES_ID'=>'','CCY_STATUS'=>''));
			$insertArr['CREATED'] = new Zend_Db_Expr('now()');
			$insertArr['LASTUPDATED'] = new Zend_Db_Expr('now()');
			$insertArr['CREATEDBY'] = $actor;
			$minamtinsert = $this->dbObj->insert('M_MINAMT_CCY',$insertArr);
				if(!(boolean)$minamtinsert) {
					$this->_errorCode = '82';
					$this->_errorMsg = 'Query failed(Min Amt)';
					return false;
				}
        	}
        }
		//Application_Helper_General::writeLog('CRCA','Approve Minimum Amount Currency');
		
		$deleteChanges  = $this->deleteNew();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveActivate($actor = null) {
		//query from TEMP_MINAMT_CCY
		$minamt = $this->dbObj->select()
								 ->from('TEMP_MINAMT_CCY')
								 ->where('CHANGES_ID = ?',$this->_changeId)
								 ->query()
								 ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($minamt)){
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Min Amt)';
			return false;
		}									 
						  
		$updateArr = array('CCY_STATUS' => 'A');
		$updateArr['LASTUPDATED'] = new Zend_Db_Expr('now()');
		$whereArr = array('CCY_ID = ?'=>(string)$minamt['CCY_ID']);
		$minamtccyupdate = $this->dbObj->update('M_MINAMT_CCY',$updateArr,$whereArr);
		if(!(boolean)$minamtccyupdate) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Min amt)';
			return false;
		}
		
		$deleteChanges  = $this->deleteActivate();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	/**
     * Approve Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveDeactivate($actor = null) {
		//query from TEMP_MINAMT_CCY
		$minamt = $this->dbObj->select()
								 ->from('TEMP_MINAMT_CCY')
								 ->where('CHANGES_ID = ?',$this->_changeId)
								 ->query()
								 ->fetch(Zend_Db::FETCH_ASSOC);
		if(!count($minamt)){
			$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(Min Amt)';
			return false;
		}

		$activeTransaction = $this->dbObj->select()
										 ->from('T_TX')
										 ->where('CCY_ID = ?',(string)$minamt['CCY_ID'])
										 ->where("TX_STATUS NOT IN ('S','F','SP','RJ')")
										 ->query()
										 ->fetchAll(Zend_Db::FETCH_ASSOC);
		if(count($activeTransaction)) {
			$this->_errorCode = '80';
			$this->_errorMsg = 'Cannot approve. Unsettled transaction';
			return false;
		}
		
		$activeLoan = $this->dbObj->select()
								  ->from('T_LOAN')
								  ->where('CCY_ID = ?',(string)$minamt['CCY_ID'])
								  ->where("LOAN_STATUS NOT IN ('S')")
								  ->query()
								  ->fetchAll(Zend_Db::FETCH_ASSOC);
		if(count($activeLoan)) {
			$this->_errorCode = '80';
			$this->_errorMsg = 'Cannot approve. Unsettled loan';
			return false;
		}
		
		$activeRepayment = $this->dbObj->select()
									   ->from('T_REPAYMENT')
									   ->where('CCY_ID = ?',(string)$minamt['CCY_ID'])
									   ->where("REPAY_STATUS NOT IN ('S','F','SP','RJ')")
									   ->query()
									   ->fetchAll(Zend_Db::FETCH_ASSOC);
		if(count($activeRepayment)) {
			$this->_errorCode = '80';
			$this->_errorMsg = 'Cannot approve. Unsettled repayment';
			return false;
		}
		
		$updateArr = array('CCY_STATUS' => 'I');
		$updateArr['LASTUPDATED'] = new Zend_Db_Expr('now()');
		$whereArr = array('CCY_ID = ?'=>(string)$minamt['CCY_ID']);
		$minamtccyupdate = $this->dbObj->update('M_MINAMT_CCY',$updateArr,$whereArr);
		if(!(boolean)$minamtccyupdate) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(Min amt)';
			return false;
		}
		
		$deleteChanges  = $this->deleteDeactivate();
		if(!$deleteChanges)
			return false;
		
		return true;
	}
	
	public function approveDelete() {
		//will never be called
	}
	
	/**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew() {
		//delete from TEMP_MINAMT_CCY
		$settingdelete = $this->dbObj->delete('TEMP_MINAMT_CCY',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$settingdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Min Amt)';
//			return false;
//		}
		
		return true;
	}

	protected function deleteMaster() {
		//delete from TEMP_MINAMT_CCY
		$settingdelete = $this->dbObj->delete('M_MINAMT_CCY');
		
		return true;
	}
	
	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteEdit() {

		//delete from TEMP_MINAMT_CCY
		$settingdelete = $this->dbObj->delete('TEMP_MINAMT_CCY',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$settingdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Min Amt)';
//			return false;
//		}
		
		return true;
	}
	
	/**
     * Delete Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteActivate() {
		//delete from TEMP_MINAMT_CCY
		$settingdelete = $this->dbObj->delete('TEMP_MINAMT_CCY',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$settingdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Min Amt)';
//			return false;
//		}
		
		return true;
	}
	
	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteDeactivate() {
		//delete from TEMP_MINAMT_CCY
		$settingdelete = $this->dbObj->delete('TEMP_MINAMT_CCY',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$settingdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(Min Amt)';
//			return false;
//		}
		
		return true;
	}
	
	public function deleteDelete() {
		//will never be called
	}
}