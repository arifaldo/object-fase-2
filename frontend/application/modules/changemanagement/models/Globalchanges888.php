<?php

/**
 * Globalchanges model for BACKEND
 * 
 * @author 
 * @version 
 */

require_once 'Zend/Db/Table/Abstract.php';

class Changemanagement_Model_Globalchanges extends Zend_Db_Table_Abstract {
	/**
	 * The default table name
	 */
	protected $_name = 'T_GLOBAL_CHANGES';
	protected $_primary = 'CHANGES_ID';
	/*protected $_dependentTables = array('Changemanagement_Model_Tempanchor',
										'Changemanagement_Model_Tempanchorlimit',
										'Changemanagement_Model_Tempbgroup',
										'Changemanagement_Model_Tempbprivigroup',
										'Changemanagement_Model_Tempcommunity',
										'Changemanagement_Model_Tempcommacct',
										'Changemanagement_Model_Tempcommfee',
										'Changemanagement_Model_Tempcustomer',
										'Changemanagement_Model_Tempfgroup',
										'Changemanagement_Model_Tempfprivigroup',
										'Changemanagement_Model_Tempmember',
										'Changemanagement_Model_Tempmemberacct',
										'Changemanagement_Model_Tempminamtccy',
										'Changemanagement_Model_Temprootcomm',
										'Changemanagement_Model_Tempscheme',
										'Changemanagement_Model_Tempschemelimit', 
										'Changemanagement_Model_Tempsetting',
										'Changemanagement_Model_Tempuser',
										'Changemanagement_Model_Tempuserlimit'
										);
	*/									
	protected $dbObj;
	protected $_id;
	protected $_row;
	protected $_errorMsg;
	protected $_errorCode;
	protected $_moduleId;
	
	/*protected $_detailModelMap = array( 'TEMP_ANCHOR' => 'Changemanagement_Model_Tempanchor',
										'TEMP_ANCHOR_LIMIT' => 'Changemanagement_Model_Tempanchorlimit',
										'TEMP_BGROUP' => 'Changemanagement_Model_Tempbgroup',
										'TEMP_BPRIVIGROUP' => 'Changemanagement_Model_Tempbprivigroup',
										'TEMP_COMMUNITY' => 'Changemanagement_Model_Tempcommunity',
										'TEMP_COMM_ACCT' => 'Changemanagement_Model_Tempcommacct',
										'TEMP_COMM_FEE' => 'Changemanagement_Model_Tempcommfee',
										'TEMP_CUSTOMER' => 'Changemanagement_Model_Tempcustomer',
										'TEMP_FGROUP' => 'Changemanagement_Model_Tempfgroup',
										'TEMP_FPRIVI_GROUP' => 'Changemanagement_Model_Tempfprivigroup',
										'TEMP_MEMBER' => 'Changemanagement_Model_Tempmember',
										'TEMP_MEMBER_ACCT' => 'Changemanagement_Model_Tempmemberacct',
										'TEMP_MINAMT_CCY' => 'Changemanagement_Model_Tempminamtccy',
										'TEMP_ROOT_COMM' => 'Changemanagement_Model_Temprootcomm',
										'TEMP_SCHEME' => 'Changemanagement_Model_Tempscheme',
										'TEMP_SCHEME_LIMIT' => 'Changemanagement_Model_Tempschemelimit',
										'TEMP_SETTING' => 'Changemanagement_Model_Tempsetting',
										'TEMP_USER' => 'Changemanagement_Model_Tempuser',
										'TEMP_USER_LIMIT' => 'Changemanagement_Model_Tempuserlimit'
										);
	 */
	//model mapping,
	//array key = obtained from field MODULE, TABLE T_GLOBAL_CHANGES								
	//array value = model name in this module
	//mapping for backend
	protected $_detailModelMap = array('anchor' => 'Changemanagement_Model_Tempanchor',
									   'community' => 'Changemanagement_Model_Tempcommunity',
									   'customer' => 'Changemanagement_Model_Tempcustomer',
									   'user' => 'Changemanagement_Model_Tempuser',
									   'scheme' => 'Changemanagement_Model_Tempscheme',
									   'member' => 'Changemanagement_Model_Tempmember',
									   'rootcommunity' => 'Changemanagement_Model_Temprootcommunity',
									   'group' => 'Changemanagement_Model_Tempgroup',
									   'welcome' => 'Changemanagement_Model_Tempwelcome',
									   'cutofftime' => 'Changemanagement_Model_Tempcutofftime',
									   'minimumamount' => 'Changemanagement_Model_Tempminimumamount',
									   'frontendlogin' => 'Changemanagement_Model_Tempfrontendlogin',
									   'frontendsetting' => 'Changemanagement_Model_Tempfrontendsetting',
									   'cleansing' => 'Changemanagement_Model_Tempcleansing'
									  );

	protected $_detailModelIdMap = array('anchor' => 'ANC',
									    'community' => 'SCM',
									    'customer' => 'CST',
									    'user' => 'CUS',
									    'scheme' => 'SCH',
									    'member' => 'SMB',
									    'rootcommunity' => 'RCM',
									    'group' => 'BGR',
									    'welcome' => 'WLT',
									    'cutofftime' => 'COT',
									    'minimumamount' => 'MAC',
									    'frontendlogin' => 'FLS',
									    'frontendsetting' => 'FES',
									    'cleansing' => 'CLS'
									  );							  
	/**
     * Constructor
     * @param  String $id Change Id
     */
	public function __construct($id) {
		parent::__construct();
		$this->dbObj = $this->getDefaultAdapter();
		$this->setId($id);
		
			
		$this->dbObj->getProfiler()->setEnabled(true);	
	}
	
	/**
     * Destructor
     */
	public function __destruct() {
//		Zend_Debug::dump($this->dbObj->getProfiler()->getQueryProfiles());
		$this->dbObj->getProfiler()->clear();	
	}
	
	/**
     * set Id for this object
     * @param  String $id Change Id
     * @return boolean 
     */
	public function setId($id) {
		
		
		//$privi = Zend_Registry::get('privilege');	// get privi ID
		 $privi = array();
		
		if (!empty($privi))		// kalo privilege is empty, gak bisa generate report, for all data
		{
			$whPriviID = "'".implode("','", $privi)."'";
			
			$sqlprv = $this->dbObj->select()
						->distinct()
						->from (array('M' => 'M_MODULE'), 
								array('M.SYSTEM_MODULE'))
						->join (array('A' => 'M_MODULE_ACTION'), 'M.MODULE_ID = A.MODULE_ID', array())
						->join (array('P' => 'M_BPRIVI_ACTION'), 'A.MODULE_ACTION_ID = P.MODULE_ACTION_ID', array())
						->where("M.SYSTEM_MODULE <> ''")
						->where("M.SYSTEM_MODULE IS NOT NULL")
						->where("P.BPRIVI_ID IN (".$whPriviID.")");
			$priviMod = $this->dbObj->fetchAll($sqlprv);
			
			
			
			if (!empty($priviMod))		// kalo priviModule is empty, means gak punya akses ke semua module, jadi gak bisa generate all report
			{
				$priviMod = Application_Backend_Menu::simpleArray($priviMod,'SYSTEM_MODULE');
				$whModuleDesc = "'".implode("','", $priviMod)."'";
				
				
				$row = $this->dbObj
					->select()
					->from($this->_name)
    				->where($this->dbObj->quoteInto($this->_primary.' = ? ',$id))
					->where("CHANGES_STATUS = 'WA' OR CHANGES_STATUS = 'RR'")
					->where("MODULE IN (".$whModuleDesc.")")
					->where($this->dbObj->quoteInto("CHANGES_FLAG = ?",'B'))
					->query()
    			   	->fetch(Zend_Db::FETCH_ASSOC);
					
			}	// if (!empty($priviMod))
		}
		
		
//		$row = $this->dbObj
//					->select()
//					->from($this->_name)
//    				->where($this->dbObj->quoteInto($this->_primary.' = ? ',$id))
//					->where("CHANGES_STATUS = 'WA' OR CHANGES_STATUS = 'RR'")
//					->where($this->dbObj->quoteInto("CHANGES_FLAG = ?",'B'))
//					->query()
//    			   	->fetch(Zend_Db::FETCH_ASSOC);

		
    	if($row) {
    		$this->_id = $id;
			$this->_row = $row;
			$this->_moduleId = $this->_detailModelIdMap[$row['MODULE']];
			//Zend_Debug::dump($this->_id);
			//Zend_Debug::dump($this->_row);
			//Zend_Debug::dump($this->_moduleId);
			return true;
    	}
    	else {
    		$this->_errorCode = '82';
    		$this->_errorMsg = 'Record with given id not found';
    		throw new Exception($this->_errorMsg);
    	}
    		
	}
	
	/**
     * Reject changes
     * Update status in changes table to "request repair"
     *
     * @param  mixed $actor User name/id who request changes repair,for use in logging into database
     * @param  String $note OPTIONAL Approval note to set in database
     * @return boolean indicating operation success/failure
     */
	public function reject($actor,$note = null) {
		
		$this->dbObj->beginTransaction();
		try {
			//check whether $this->_row['MODULE'] value exists in $this->_detailModelMap
			if(in_array($this->_row['MODULE'],array_keys($this->_detailModelMap))) {
				//create detail model
				$detailModel = new $this->_detailModelMap[$this->_row['MODULE']]($this->_id);
				switch($this->_row['CHANGES_TYPE']) {
					case 'N':
						$updated = $detailModel->deleteNew();
						$this->_errorCode = $detailModel->getErrorCode();
						$this->_errorMsg = $detailModel->getErrorMessage();
						break;
					case 'E':
						$updated = $detailModel->deleteEdit();
						$this->_errorCode = $detailModel->getErrorCode();
						$this->_errorMsg = $detailModel->getErrorMessage();
						break;
					case 'A':
						$updated = $detailModel->deleteActivate();
						$this->_errorCode = $detailModel->getErrorCode();
						$this->_errorMsg = $detailModel->getErrorMessage();
						break;
					case 'D':
						$updated = $detailModel->deleteDeactivate();
						$this->_errorCode = $detailModel->getErrorCode();
						$this->_errorMsg = $detailModel->getErrorMessage();
						break;
					case 'L':
						$updated = $detailModel->deleteDeactivate();
						$this->_errorCode = $detailModel->getErrorCode();
						$this->_errorMsg = $detailModel->getErrorMessage();
						break;				
					default:
						$updated = false;
						$this->_errorCode = '22';
						$this->_errorMsg = 'Unknown changes type';
						throw new Exception($this->_errorMsg);
						break;
				}
				
				if($updated) {		
					$rowUpdated = $this->dbObj->update($this->_name, array('CHANGES_STATUS' => 'RJ',
																		   'CHANGES_REASON' => $note,
																		   //'LASTUPDATEDBY' => $actor,
																		   //NOTE: this field expression is Ms Sql Specific
																		   'LASTUPDATED' => new Zend_Db_Expr("now()"),
																		   ),
													   $this->dbObj->quoteInto('CHANGES_ID = ?',$this->_id)
													  );
					if(!(boolean)$rowUpdated) {								  
						$this->_errorCode = '82';
						$this->_errorMsg = $this->getErrorRemark('82');
						throw new Exception($this->_errorMsg);
					}							  
				}
				else
					throw new Exception($this->_errorMsg);
			}
			else {
				$this->_errorCode = '22';
				$this->_errorMsg = 'Unknown Module';
				throw new Exception($this->_errorMsg);
			}
		}
		catch(Exception $e) {
			$this->dbObj->rollBack();
			Application_Log_GeneralLog::technicalLog($e);
			//$this->_errorCode = '82';
			//$this->_errorMsg = 'Query Failed (Global Changes)';
			
			//rethrow exception (use this for debugging)
			//throw $e;
			return false;
		}
		
		$this->dbObj->commit();
		return true;
	}
	
	/**
     * Delete changes
     * @return boolean indicating operation success/failure
     */
	public function delete() {
		
		$this->dbObj->beginTransaction();
		try {
			//check whether $this->_row['MODULE'] value exists in $this->_detailModelMap
			if(in_array($this->_row['MODULE'],array_keys($this->_detailModelMap))) {
				//create detail model
				$detailModel = new $this->_detailModelMap[$this->_row['MODULE']]($this->_id);
				switch($this->_row['CHANGES_TYPE']) {
					case 'N':
						$updated = $detailModel->deleteNew();
						$this->_errorCode = $detailModel->getErrorCode();
						$this->_errorMsg = $detailModel->getErrorMessage();
						break;
					case 'E':
						$updated = $detailModel->deleteEdit();
						$this->_errorCode = $detailModel->getErrorCode();
						$this->_errorMsg = $detailModel->getErrorMessage();
						break;
					case 'A':
						$updated = $detailModel->deleteActivate();
						$this->_errorCode = $detailModel->getErrorCode();
						$this->_errorMsg = $detailModel->getErrorMessage();
						break;
					case 'D':
						$updated = $detailModel->deleteDeactivate();
						$this->_errorCode = $detailModel->getErrorCode();
						$this->_errorMsg = $detailModel->getErrorMessage();
						break;
					case 'L':
						$updated = $detailModel->deleteDeactivate();
						$this->_errorCode = $detailModel->getErrorCode();
						$this->_errorMsg = $detailModel->getErrorMessage();
						break;				
					default:
						$updated = false;
						$this->_errorCode = '22';
						$this->_errorMsg = 'Unknown changes type';
						throw new Exception($this->_errorMsg);
						break;
				}
				
				if($updated) {	
					$rowDeleted = $this->dbObj->delete($this->_name, $this->dbObj->quoteInto('CHANGES_ID = ?',$this->_id));
					if(!$rowDeleted) {				
						$this->_errorCode = '82';
						$this->_errorMsg = 'Query Failed (Global Changes)';
						throw new Exception($this->_errorMsg);
					}							  
				}
				else
					throw new Exception($this->_errorMsg);
			}
			else {
				$this->_errorCode = '22';
				$this->_errorMsg = 'Unknown Module';
				throw new Exception($this->_errorMsg);
			}
		}
		catch(Exception $e) {
			
			$this->dbObj->rollBack();
			Application_Log_GeneralLog::technicalLog($e);
			//$this->_errorCode = '82';
			//$this->_errorMsg = 'Query Failed (Global Changes)';
			//rethrow exception (use this for debugging)
//			throw $e;
			return false;
		}
		
		$this->dbObj->commit();
		return true;
	}
	
	/**
     * Request change repair
     * Update status in changes table to "request repair"
     *
     * @param  mixed $actor User name/id who request changes repair,for use in logging into database
     * @param  String $note OPTIONAL Approval note to set in database
     * @return boolean indicating operation success/failure
     */
	public function requestRepair($actor,$note = null) {
		
		$rowsUpdated = $this->dbObj->update($this->_name, array('CHANGES_STATUS' => 'RR',
																'CHANGES_REASON' => $note,
																//'LASTUPDATEDBY' => $actor,
																//NOTE: this field expression is Ms Sql Specific
																'LASTUPDATED' => new Zend_Db_Expr("now()")
																),
											$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_id)
											);

		if(!(boolean)$rowsUpdated) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query Failed (Global Changes)';
			return false;
		}
											
		return true;
	}
	
	/**
     * Approve changes
     *
     * @param  mixed $actor User name/id who approved changes,for use in logging into database
     * @param  String $note OPTIONAL Approval note to set in database
     * @return boolean indicating operation success/failure
     */
	public function approve($actor = null,$note = null) {
		$this->dbObj->beginTransaction();
		try {
			//check whether $this->_row['MODULE'] value exists in $this->_detailModelMap
			if(in_array($this->_row['MODULE'],array_keys($this->_detailModelMap))) {
				//create detail model
				$detailModel = new $this->_detailModelMap[$this->_row['MODULE']]($this->_id);
				switch($this->_row['CHANGES_TYPE']) {
					case 'N':
						//$updated = $detailModel->approveNew();
						$updated = $detailModel->approveNew($actor);
						$this->_errorCode = $detailModel->getErrorCode();
						$this->_errorMsg = $detailModel->getErrorMessage();
						break;
					case 'E':
						$updated = $detailModel->approveEdit();
						$this->_errorCode = $detailModel->getErrorCode();
						$this->_errorMsg = $detailModel->getErrorMessage();
						break;
					case 'A':
						$updated = $detailModel->approveActivate();
						$this->_errorCode = $detailModel->getErrorCode();
						$this->_errorMsg = $detailModel->getErrorMessage();
						break;
					case 'D':
						$updated = $detailModel->approveDeactivate();
						$this->_errorCode = $detailModel->getErrorCode();
						$this->_errorMsg = $detailModel->getErrorMessage();
						break;
					case 'L':
						$updated = $detailModel->approveDelete();
						$this->_errorCode = $detailModel->getErrorCode();
						$this->_errorMsg = $detailModel->getErrorMessage();
						break;							
					default:
						$updated = false;
						$this->_errorCode = '82';
						$this->_errorMsg = 'Unknown changes type';
						throw new Exception($this->_errorMsg);
						break;
				}
				if($updated) {
					$rowUpdated = $this->dbObj->update($this->_name, array('CHANGES_STATUS' => 'AP',
					                                                       //'CHANGES_STATUS' => 'RJ',
																		   'CHANGES_REASON' => $note,
																		   //'LASTUPDATEDBY' => $actor,
																		   //NOTE: this field expression is Ms Sql Specific
																		   'LASTUPDATED' => new Zend_Db_Expr("now()"),
																		   ),
													   $this->dbObj->quoteInto('CHANGES_ID = ?',$this->_id)
													  );
					if(!(boolean)$rowUpdated) {								  
						$this->_errorCode = '82';
						$this->_errorMsg = 'Query Failed (Global Changes)';
						throw new Exception($this->_errorMsg);
					}					  
				}
				else
					throw new Exception($this->_errorMsg);
			}
			else {
				$this->_errorCode = '82';
				$this->_errorMsg = 'Unknown Module';
				throw new Exception($this->_errorMsg);
			}
		}
		catch(Exception $e) {
			
			$this->dbObj->rollBack();
			Application_Log_GeneralLog::technicalLog($e);
			//$this->_errorCode = '82';
			//$this->_errorMsg = 'Query Failed (Global Changes)';
			//rethrow exception (use this for debugging)
//			throw $e;
			return false;
		}
		
		$this->dbObj->commit();
		return true;
	}
	
	/**
     * Get Error Message
     *
     * @return String Error Message
     */
	public function getErrorMessage() {
		return $this->_errorMsg;
	}
	
	public function getErrorCode() {
		return $this->_errorCode;
	}
	
	public function getModuleId() {
		return $this->_moduleId;
	}
	
	/**
     * Get changes detail link
     *
     * @return array detail fields as array
    */
	public function getDetailLink() {
		if(!isset($this->_id)) {
			throw new Exception('Can not get detail. id is not set');
		}
		
		$detailLink = array('controller' => 'suggestiondetail',
							'action' => 'index',
							'module' => $this->_row['MODULE'],
							'params' => array('changes_id' => $this->_id)
							);
		return $detailLink;			
		//create detail model 
		//$detailModel = new $this->_detailModelMap[$this->_row['MODULE']]($this->_id);
		//return $detailModel->getDetailLink($this->_id);
		/*
		if($this->_row['MODULE']=='anchor')
		{			
			//changes for anchor and scheme
			$detailLink = array('controller' => '',
								 'action' => '',
								 'module' => '',
								 'params' => array()
								);
		}
		*/
	}
}