<?php
/**
 * Tempuser model
 * 
 * @author 
 * @version
 */

class Changemanagement_Model_Tempsetsystembalance extends Changemanagement_Model_Tempchanges 
{
	protected $_moduleId = 'SBL';
	/**
     * Approve Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function approveNew($actor = null) {
	}
	
	/**
     * Approve Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function approveEdit($actor = null) {
		//query from TEMP_SET_SYSTEMBALANCE
		$listAccount = $this->dbObj->select()
						  	->from('TEMP_SET_SYSTEMBALANCE')
						  	->where('CHANGES_ID = ?',$this->_changeId)
						  	->query()
						  	->fetchAll(Zend_Db::FETCH_ASSOC);
		if(!count($listAccount)){
        	$this->_errorCode = '22';
			$this->_errorMsg = 'Query failed(listAccount)';
			return false;
        }

		if(is_array($listAccount)){
			foreach ($listAccount as $row) {
					$updateArr = array_diff_key($row,array('TEMP_ID'=>'','CHANGES_ID'=>'','CUST_ID'=>'','ACCT_NO'=>'','ACC_NAME' =>''));
					$whereArr = array('CUST_ID = ?'=> (string) $row['CUST_ID'],
						  'ACCT_NO = ?'=> (string)$row['ACCT_NO']);
					$userupdate = $this->dbObj->update('M_CUSTOMER_ACCT',$updateArr,$whereArr);
				
					$insertArr = $updateArr;
					$insertArr['ACCT_NO'] = $row['ACCT_NO'];			
					$insertArr['SUGGESTEDBY'] = $this->_changesInfo['CREATED_BY'];			
					$insertArr['APPROVE']	 = new Zend_Db_Expr("now()");		
					$insertArr['APPROVEDBY'] =  $actor;			
					$userupdate = $this->dbObj->insert('T_SET_SYSTEMBALANCE',$insertArr);			
								
					
					
				
				
			}
		}				  	
		//update record
		if(!(boolean)$userupdate) {
			$this->_errorCode = '82';
			$this->_errorMsg = 'Query failed(System Balance)';
			return false;
		}
		
		$deleteChanges  = $this->deleteEdit();
		if(!$deleteChanges)return false;
		
		return true;
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveActivate($actor = null) {
	}
	
	/**
     * Approve Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function approveDeactivate($actor = null) {
	}
	
	public function approveDelete($actor = null) {
		//will never be called
	}
	
/**
     * Delete Changes for changes type: New
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteNew() {
	}
	
	/**
     * Delete Changes for changes type: Edit
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteEdit() {
		//delete from TEMP_USER
		$userdelete = $this->dbObj->delete('TEMP_SET_SYSTEMBALANCE',$this->dbObj->quoteInto('CHANGES_ID = ?',$this->_changeId));
//		if(!(boolean)$userdelete) {
//			$this->_errorCode = '82';
//			$this->_errorMsg = 'Query failed(User)';
//			return false;
//		}
		
		return true;
	}
	
/**
     * Delete Changes for changes type: Activate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteActivate() {
	}
	
	/**
     * Delete Changes for changes type: Deactivate
     *
     * @return boolean indicating operation success/failure
     */
	public function deleteDeactivate() {
	}
	
	public function deleteDelete() {
	}
}