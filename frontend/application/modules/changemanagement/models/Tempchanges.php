<?php
/**
 * Tempchanges model
 * 
 * @author 
 * @version
 */

abstract class Changemanagement_Model_Tempchanges {
	
	protected $dbObj;
    protected $_changeId;
	protected $_errorMsg;
	protected $_errorCode;
	protected $_moduleId;
	protected $_changesInfo;
	
	/**
     * Constructor
     * @param  String $id Change Id
     */
    public function __construct($changeId,$changesInfo=null) {
    	$this->dbObj = Zend_Db_Table::getDefaultAdapter();
    	$this->_changeId = $changeId;			
		$this->_changesInfo = $changesInfo;
	}
	
	abstract function approveNew();
	
	abstract function approveEdit();
	
	abstract function approveActivate();
	
	abstract function approveDeactivate();
	
	abstract function approveDelete();
	
	abstract function deleteNew();
	
	abstract function deleteEdit();
	
	abstract function deleteActivate();
	
	abstract function deleteDeactivate();
	
	abstract function deleteDelete();
	
	/**
     * Get Error Message
     *
     * @return String Error Message
     */
	public function getErrorMessage() {
		return $this->_errorMsg;
	}
	
	public function getErrorCode() {
		return $this->_errorCode;
	}
	
	public function getModuleId() {
		return $this->_moduleId;
	}	
}