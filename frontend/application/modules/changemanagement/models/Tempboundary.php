<?php

/**
 * Temprootcommunity model
 * 
 * @author
 * @version
 */

class Changemanagement_Model_Tempboundary extends Changemanagement_Model_Tempchanges
{
	protected $_moduleId = 'AUM';
	/**
	 * Approve Changes for changes type: New
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function approveNew($actor = null)
	{
		
		$boundary = $this->dbObj->fetchAll(
			$this->dbObj->select()
				->from('TEMP_APP_BOUNDARY')
				->where('CHANGES_ID = ?', $this->_changeId)
		);
		$custid = $boundary[0]['CUST_ID'];
		// Zend_Debug::dump($boundary);die;

	}

	/**
	 * Approve Changes for changes type: Edit
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function approveEdit($actor = null)
	{
		
		/*$select = $this->dbObj->select()
									 ->from('T_GLOBAL_CHANGES')
									 ->where('CHANGES_ID = ?',$this->_changeId)
									 ->query()
									 ->fetch(Zend_Db::FETCH_ASSOC);	*/
		// $psid = array("1", "17");
		// $select	= $this->dbObj->select()
		// 					 ->from(array('P' => 'T_PSLIP'),array())
		// 					 ->join(array('T' => 'T_TRANSACTION'), 'P.PS_NUMBER = T.PS_NUMBER', array('P.*','T.*'))
		// 					 ->where('P.PS_STATUS IN(?)',$psid);
		// echo "<pre>";					
		// $results = $this->dbObj->fetchAll($select);
		// // foreach($results as $key => $value){
		// // 	$update = array("POLICY" => $value['PS_TYPE'],
		// // 					"PS_NUMBER"=> $value['PS_NUMBER'],
		// // 					"CUST_ID" => $value["CUST_ID"]
		// // 					);                            
		// // 	// $this->dbObj->update('T_TRANSACTION',$update,$where);
		// // 	print_r($update);				
		// // }



		// echo "--------------";
		// print_r($results);die;

		$boundary = $this->dbObj->fetchAll(
			$this->dbObj->select()
				->from('TEMP_APP_BOUNDARY')
				->where('CHANGES_ID = ?', $this->_changeId)
		);
		$custid = $boundary[0]['CUST_ID'];
		//Zend_Debug::dump($boundary);die;
		
		if ($boundary) {
			$whereArr2 = array('GROUP_USER_ID LIKE ' . $this->dbObj->quote('%' . $custid . '%'));
			$delete2 = $this->dbObj->delete('M_APP_BOUNDARY_GROUP', $whereArr2);
			$whereArr1 = array('CUST_ID = ?' => $custid);
			$delete1 = $this->dbObj->delete('M_APP_BOUNDARY', $whereArr1);

			$whereArr2 = array('CUST_ID = ?' => $custid);
			$delete3 = $this->dbObj->delete('M_APP_GROUP_USER', $whereArr2);

			// Zend_Debug::dump($delete2.' '.$delete1.' '.$custid);die;
			// echo "<pre>";
			// print_r($boundary);die;
			foreach ($boundary as $list) {
				// print_r($list);die;
				$insertArr = array_diff_key($list, array('TEMP_ID' => '', 'CHANGES_ID' => '', 'BOUNDARY_ID' => ''));
				$insertArr['LASTUPDATED'] = new Zend_Db_Expr('now()');
				$insertArr['CREATED'] = new Zend_Db_Expr('now()');
				$insertArr['CREATEDBY'] = $actor;
				// INsert To M_APP_BOUNDARY
				$insert = $this->dbObj->insert('M_APP_BOUNDARY', $insertArr);
				$lastId = $this->dbObj->query('SELECT @@identity AS LASTID')->fetch(Zend_Db::FETCH_ASSOC);
				$last = $lastId['LASTID'];

				$query = $this->dbObj->select()
					->from('TEMP_APP_BOUNDARY_GROUP')
					->where('CHANGES_ID = ?', $this->_changeId)
					->where('ROW_INDEX = ?', $list['ROW_INDEX']);
				// Zend_Debug::dump($boundary);
				// Zend_Debug::dump($query->__toString());
				// Zend_Debug::dump($list);

				// print_r($query);die;
				// echo $query;die;
				$group = $this->dbObj->fetchAll($query);
				// Zend_Debug::dump($group);die;
				foreach ($group as $grouplist) {
					// Zend_Debug::dump($last);die;
					$groupinsertArr = array_diff_key($grouplist, array('TEMP_ID' => '', 'CHANGES_ID' => '', 'BOUNDARY_ID' => ''));
					$groupinsertArr['BOUNDARY_ID'] = $last;
					try {
						// die('here');
						// echo '<pre>';
						// print_r($groupinsertArr);
						$groupinsert = $this->dbObj->insert('M_APP_BOUNDARY_GROUP', $groupinsertArr);
					} catch (Exception $e) {
						// print_r($e);die;
					}
				}
			}

			// echo "<pre>";
			$psid = array("1", "17");
			$select	= $this->dbObj->select()
				->from(array('P' => 'T_PSLIP'), array())
				->join(array('T' => 'T_TRANSACTION'), 'P.PS_NUMBER = T.PS_NUMBER', array('P.*', 'T.*'))
				->where('P.CUST_ID = ?', $custid)
				->where('P.PS_STATUS IN(?)', $psid);

			$results = $this->dbObj->fetchAll($select);
			foreach ($boundary as $boundval) {
				foreach ($results as $key => $value) {
					$updateon = array(
						'PS_NUMBER = ?' => $value['PS_NUMBER'],
						'TRANSFER_TYPE = ?' => $boundval['TRANSFER_TYPE']
					);
					$update['POLICY'] = $boundval['POLICY'];
					$this->dbObj->update('T_TRANSACTION', $update, $updateon);
				}
			}
		}else{

			$global_change = $this->dbObj->fetchAll(
				$this->dbObj->select()
					->from('T_GLOBAL_CHANGES')
					->where('CHANGES_ID = ?', $this->_changeId)
			);
			$custid = $global_change[0]['CUST_ID'];

			$whereArr2 = array('GROUP_USER_ID LIKE ' . $this->dbObj->quote('%' . $custid . '%'));
			$delete2 = $this->dbObj->delete('M_APP_BOUNDARY_GROUP', $whereArr2);
			$whereArr1 = array('CUST_ID = ?' => $custid);
			$delete1 = $this->dbObj->delete('M_APP_BOUNDARY', $whereArr1);

			$whereArr2 = array('CUST_ID = ?' => $custid);
			$delete3 = $this->dbObj->delete('M_APP_GROUP_USER', $whereArr2);

		}

		$queryg = $this->dbObj->select()
			->from('TEMP_APP_GROUP_USER')
			->where('CHANGES_ID = ?', $this->_changeId);
		// ->where('ROW_INDEX = ?',$list['ROW_INDEX']);
		// Zend_Debug::dump($boundary);
		// Zend_Debug::dump($query->__toString());
		// Zend_Debug::dump($list);


		$groupuser = $this->dbObj->fetchAll($queryg);
		// Zend_Debug::dump($groupuser);die;
		foreach ($groupuser as $grouplist) {
			// Zend_Debug::dump($last);die;
			$groupinsertArr = array_diff_key($grouplist, array('TEMP_ID' => '', 'CHANGES_ID' => ''));
			// $groupinsertArr['BOUNDARY_ID'] = $last;
			try {
				// die('here');
				// echo '<pre>';
				// print_r($groupinsertArr);
				$groupinsert = $this->dbObj->insert('M_APP_GROUP_USER', $groupinsertArr);
			} catch (Exception $e) {
				// print_r($e);
			}
		}
		// die('12');
		Application_Helper_General::writeLog('ABGC', 'Menyetujui Usulan Perubahan Matriks Persetujuan');



		$deleteChanges  = $this->deleteEdit(1);
		// if(!$deleteChanges)
		// return false;

		return true;
	}

	/**
	 * Approve Changes for changes type: Activate
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function approveActivate()
	{
	}

	/**
	 * Approve Changes for changes type: Deactivate
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function approveDeactivate()
	{
	}

	public function approveDelete()
	{
		//will never be called
	}


	/**
	 * Delete Changes for changes type: New
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function deleteNew()
	{
	}

	/**
	 * Delete Changes for changes type: Edit
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function deleteEdit($approve = NULL)
	{
		// die('here');
		$delete = $this->dbObj->delete('TEMP_APP_BOUNDARY', $this->dbObj->quoteInto('CHANGES_ID = ?', $this->_changeId));
		$groupdelete = $this->dbObj->delete('TEMP_APP_BOUNDARY_GROUP', $this->dbObj->quoteInto('CHANGES_ID = ?', $this->_changeId));

		$groupdelete = $this->dbObj->delete('TEMP_APP_GROUP_USER', $this->dbObj->quoteInto('CHANGES_ID = ?', $this->_changeId));

		if ($approve === NULL) {
			Application_Helper_General::writeLog('ABGC', 'Tolak Usulan Perubahan Matriks Persetujuan');
		}


		return true;
	}

	/**
	 * Delete Changes for changes type: Activate
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function deleteActivate()
	{
	}

	/**
	 * Delete Changes for changes type: Deactivate
	 *
	 * @return boolean indicating operation success/failure
	 */
	public function deleteDeactivate()
	{
	}

	public function deleteDelete()
	{
		//will never be called
	}
}
