<?php

/**
 * privilege mapping for dual control
 * 
 * @author 
 * @version 
 */
class Changemanagement_Model_Privilege  {
	
	protected $_privilegeMap = array(
									'AULC' => 'user',
									'ASLC' => 'userlimit',
									//'ASOB' => 'useropenlimit',
									//'ADLC' => 'userdailylimit',
									'ADLC' => "useropenlimit','userdailylimit",
									'AAGC' => 'appgroup',
									'ABGC' => 'authorizationmatrix',
									'APAC'     =>  'customeraccount',
									'ABGC' => 'boundary',
									'APAC' => 'apikeymanagement',
									'VBTU' => 'airlines',
									'VCDC' => 'debitbalance',
									'VSPD' => 'preliminary'
						);
	
						
						
	protected $_privilegeApproveMap = array(
									'AULC' => 'user',
									'ASLC' => 'userlimit',
									//'ASOB' => 'useropenlimit',
									//'ADLC' => 'userdailylimit',
									'ADLC' => "useropenlimit','userdailylimit",
									'AAGC' => 'appgroup',
									'ABGC' => 'authorizationmatrix',
									'ABCL'     =>  'customeraccount',
									'ABGC' => 'boundary',
									'APAC' => 'apikeymanagement',
									'UBTU' => 'airlines',
									'VCDC' => 'debitbalance',
									'VSPD' => 'preliminary'
						);
						
	protected $_privilegeRepairMap = array(
									'RULC'	=> 'user',
									'RSLC'	=> 'userlimit',
									//'RSOB' => 'useropenlimit',
									//'RDLC'	=> 'userdailylimit',
									'RDLC' => "useropenlimit','userdailylimit",
									'RAGC'  => 'appgroup',
									'RBGC'	=> 'authorizationmatrix',
									'RBCL'     =>  'customeraccount',
									'RBGC' => 'boundary',
									'VCDC' => 'debitbalance',
									'VSPD' => 'preliminary'
						);
											
	protected $_privilegeRequestRepairMap = array();
	protected $_privilegeRejectMap = array();
						
	public function __construct(){
		$this->_privilegeRepairMap = $this->_privilegeRepairMap;
		$this->_privilegeRequestRepairMap = $this->_privilegeApproveMap;
		$this->_privilegeRejectMap = $this->_privilegeApproveMap;
	}					

	public function getAuthorizeModule() {

			$myPrivilege =  array_flip(Zend_Registry::get('privilege')); 
			$authorizeModuleArr = array_intersect_key($this->_privilegeMap, $myPrivilege);
//				echo '<pre>';print_r($authorizeModuleArr );die;
			return $authorizeModuleArr;
	}
	
	
	public function isAuthorizeApprove($moduleName) {
			$myPrivilege =  array_flip(Zend_Registry::get('privilege')); 
			$authorizeModuleArr = array_intersect_key($this->_privilegeApproveMap, $myPrivilege);
			$returnValue = false;
			if(is_array($authorizeModuleArr)){
				$newArr = array_flip($authorizeModuleArr);

					if (array_key_exists('ADLC', $authorizeModuleArr)) {
					$newArr['useropenlimit'] = 'ADLC';
					$newArr['userdailylimit'] = 'ADLC';
					}

				if (array_key_exists('CHCA', $authorizeModuleArr)){
					$newArr['adminfeeaccount'] = 'CHCA';
					$newArr['adminfeecompany'] = 'CHCA';
					$newArr['chargesdomestic'] = 'CHCA';
					$newArr['disbursementcharges'] = 'CHCA';
					$newArr['monthlyaccount'] = 'CHCA';
					$newArr['monthlycompany'] = 'CHCA';
					$newArr['realtimecharges'] = 'CHCA';
					$newArr['settlementcharges'] = 'CHCA';
				}
				
				
				if(array_key_exists($moduleName, $newArr)){
					$returnValue =  true;
				}
			}
			return $returnValue;
	}
	public function isAuthorizeReject($moduleName) {
			$myPrivilege =  array_flip(Zend_Registry::get('privilege')); 
			$authorizeModuleArr = array_intersect_key($this->_privilegeApproveMap, $myPrivilege);
			$returnValue = false;
			if(is_array($authorizeModuleArr)){
				$newArr = array_flip($authorizeModuleArr);
				
				if (array_key_exists('ADLC', $authorizeModuleArr)) {
					$newArr['useropenlimit'] = 'ADLC';
					$newArr['userdailylimit'] = 'ADLC';
					}
					
				if (array_key_exists('CHCA', $authorizeModuleArr)){
					$newArr['adminfeeaccount'] = 'CHCA';
					$newArr['adminfeecompany'] = 'CHCA';
					$newArr['chargesdomestic'] = 'CHCA';
					$newArr['disbursementcharges'] = 'CHCA';
					$newArr['monthlyaccount'] = 'CHCA';
					$newArr['monthlycompany'] = 'CHCA';
					$newArr['realtimecharges'] = 'CHCA';
					$newArr['settlementcharges'] = 'CHCA';
				}
				if(array_key_exists($moduleName, $newArr)){
					$returnValue =  true;
				}
			}
			return $returnValue;
	}
	public function isAuthorizeRequestRepair($moduleName) {
			$myPrivilege =  array_flip(Zend_Registry::get('privilege')); 
			$authorizeModuleArr = array_intersect_key($this->_privilegeRequestRepairMap, $myPrivilege);
			$returnValue = false;
			if(is_array($authorizeModuleArr)){
				$newArr = array_flip($authorizeModuleArr);
				
				if (array_key_exists('RDLC', $authorizeModuleArr)) {
					$newArr['useropenlimit'] = 'RDLC';
					$newArr['userdailylimit'] = 'RDLC';
					}
				
				if (array_key_exists('CHCA', $authorizeModuleArr)){
					$newArr['adminfeeaccount'] = 'CHCA';
					$newArr['adminfeecompany'] = 'CHCA';
					$newArr['chargesdomestic'] = 'CHCA';
					$newArr['disbursementcharges'] = 'CHCA';
					$newArr['monthlyaccount'] = 'CHCA';
					$newArr['monthlycompany'] = 'CHCA';
					$newArr['realtimecharges'] = 'CHCA';
					$newArr['settlementcharges'] = 'CHCA';
				}
				if(array_key_exists($moduleName, $newArr)){
					$returnValue =  true;
				}
			}
			return $returnValue;
	}
	
	public function getPrivilege($moduleName,$action){
		
			$myPrivilege =  array_flip(Zend_Registry::get('privilege'));
			if($action=='Approve'){ 
				$authorizeModuleArr = array_intersect_key($this->_privilegeApproveMap, $myPrivilege);
			}elseif ($action=='Reject'){
				$authorizeModuleArr = array_intersect_key($this->_privilegeRejectMap, $myPrivilege);
			}elseif ($action =='Request Repair'){
				$authorizeModuleArr = array_intersect_key($this->_privilegeRequestRepairMap, $myPrivilege);
			}
			
			$returnValue = "REQC";
			if(is_array($authorizeModuleArr)){
				$newArr = array_flip($authorizeModuleArr);
				
				if (array_key_exists('ADLC', $authorizeModuleArr)) {
					$newArr['useropenlimit'] = 'ADLC';
					$newArr['userdailylimit'] = 'ADLC';
					}
				
				if (array_key_exists('CHCA', $authorizeModuleArr)){
					$newArr['adminfeeaccount'] = 'CHCR';
					$newArr['adminfeecompany'] = 'CHCR';
					$newArr['chargesdomestic'] = 'CHCR';
					$newArr['disbursementcharges'] = 'CHCR';
					$newArr['monthlyaccount'] = 'CHCR';
					$newArr['monthlycompany'] = 'CHCR';
					$newArr['realtimecharges'] = 'CHCR';
					$newArr['settlementcharges'] = 'CHCR';;
				}
				if(array_key_exists($moduleName, $newArr)){
					$returnValue =  $newArr[$moduleName];
				}
			}
			return $returnValue;
	
	}
	
	
	public function isAuthorizeRepair($moduleName) {
			$myPrivilege =  array_flip(Zend_Registry::get('privilege')); 
			$authorizeModuleArr = array_intersect_key($this->_privilegeRepairMap, $myPrivilege);
			$returnValue = false;
			if(is_array($authorizeModuleArr)){
				$newArr = array_flip($authorizeModuleArr);
				
				if (array_key_exists('RDLC', $authorizeModuleArr)) {
					$newArr['useropenlimit'] = 'RDLC';
					$newArr['userdailylimit'] = 'RDLC';
					}
				
				if (array_key_exists('CHCR', $authorizeModuleArr)){
					$newArr['adminfeeaccount'] = 'CHCR';
					$newArr['adminfeecompany'] = 'CHCR';
					$newArr['chargesdomestic'] = 'CHCR';
					$newArr['disbursementcharges'] = 'CHCR';
					$newArr['monthlyaccount'] = 'CHCR';
					$newArr['monthlycompany'] = 'CHCR';
					$newArr['realtimecharges'] = 'CHCR';
					$newArr['settlementcharges'] = 'CHCR';
				}
				if(array_key_exists($moduleName, $newArr)){
					$returnValue =  true;
				}
			}
			return $returnValue;
	}
	
	
	
}
