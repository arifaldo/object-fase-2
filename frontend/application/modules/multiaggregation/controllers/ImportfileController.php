<?php
require_once 'Zend/Controller/Action.php';
require_once 'General/SystemBalance.php';
require_once 'General/CustomerUser.php';


class multiaggregation_ImportfileController extends Application_Main
{
	protected $_moduleDB = 'RTF'; // masih harus diganti
	protected $_destinationUploadDir = '';

	public function initController()
    {
        $this->_destinationUploadDir = UPLOAD_PATH . '/document/temp/';

        $setting = new Settings();
        $this->_maxRow = $setting->getSetting('max_import_bulk');
    }

	public function indexAction()
	{
		// $this->_helper->viewRenderer->setNoRender();
  //       $this->_helper->layout()->disableLayout();
		$this->_helper->layout()->setLayout('newlayout');
		// $par = array('031','032','014','009','002','008','153','013','011');

		$banklist = $this->_db->fetchAll(
                    $this->_db->select()
                         ->from(array('A' => 'M_APIKEY'),array('A.BANK_CODE'))
                         // ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
                         //  ->join(array('B' => 'M_BANKTABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
                         //  // ->join(array('D' => 'M_DOMESTIC_BANK_TABLE'),'D.BANK_NAME = B.BANK_NAME',array('D.SWIFT_CODE'))
                         ->where('A.CUST_ID = ?',$this->_custIdLogin)
                         ->group('A.BANK_CODE')
                         // ->where("A.BANK_CODE IN ('008','014','013','009','032','031','153','002') ")
                         // ->order('A.APIKEY_ID ASC')
       );
		// var_dump($banklist);
		$par = array();
		if(!empty($banklist)){
			foreach ($banklist as $key => $value) {
				$par[] = $value['BANK_CODE'];
			}
		}
		// var_dump($par);
		// var_dump($banklist);die;
		if(empty($par)){
			$par = array('000');
		}

		$selectbank = $this->_db->select()
					 ->from(array('C' => 'M_BANK_TABLE'),array('*'))
					->where('C.BANK_CODE IN (?)',$par);
					// echo $selectbank;die;
		$databank 					= $this->_db->fetchAll($selectbank);

		$this->view->databank = $databank;

		if ($this->_getParam('submit')) {

			$confirm = false;
			$error_msg[0] = "";

			$bankcode = $this->_getParam('bank_code');

			if(!$bankcode)
			{
				$error_msg[0] = $this->language->_('Bank cannot be left blank.').'';
				$this->view->error 		= true;
				$this->view->report_msg	= $this->displayError($error_msg);
			}
			else {
				
				$adapter = new Zend_File_Transfer_Adapter_Http();

				$adapter->setDestination ( $this->_destinationUploadDir );

				$extensionValidator = new Zend_Validate_File_Extension(array(false, 'csv'));
				$extensionValidator->setMessage(
					$this->language->_('Extension file must be').' *.csv'
				);

				$sizeValidator = new Zend_Validate_File_Size(array('min' => 1, 'max' => $this->getSetting('Fe_attachment_maxbyte')));
				$sizeValidator->setMessage(
					'File exceeds maximum size'
				);

				$adapter->setValidators ( array (
					$extensionValidator,
					$sizeValidator,
				));

				if ($adapter->isValid ())
				{
					$sourceFileName = substr_replace(basename($adapter->getFileName ()),'',100);
					$newFileName = $adapter->getFileName () . '-' . strtr(microtime(), array("." => "-", " " => "-")) . '.tmp';

					$adapter->addFilter ( 'Rename',$newFileName  );

					if ($adapter->receive ())
					{
						//PARSING CSV HERE
						$csvData = $this->parseCSV($newFileName);
						//after parse delete document temporary
						@unlink($newFileName);
						//end

						$totalRecords = count($csvData);
						if($totalRecords)
						{
							unset($csvData[0]);
							$totalRecords = count($csvData);
						}

							$paramTrxArr = array();
							$success = 0;
							$failed = 0;
							$rowNum = 0;
							foreach ( $csvData as $numb => $row )
							{
								if(count($row)==2)
								{

									$rowNum++;
									
									$selectdata = $this->_db->select()
									->from(array('C' => 'M_APIKEY'),array('*'))
												->where('C.FIELD = ?', 'account_number')
												->where('C.CUST_ID = ?', $this->_custIdLogin)
												->where('C.BANK_CODE = ?', $bankcode)
												->where('C.VALUE = ?', $row['0']);
												// echo $selectbank;die;
									$datacheck 					= $this->_db->fetchAll($selectdata);

									if(empty($datacheck)){
										

										if(preg_match("/[a-z]/i", $row['0'])){
											$valid = 0;
											$failed++;
											$errorMsg[$rowNum] = $row['0'];
										}else{
											$valid = 1;
											$success++;
										}
									}else{
										$valid = 0;
										$failed++;
										$errorMsg[$rowNum] = $row['0'];
									}

									$paramTrx = array( 
										'BANK_CODE'		=> $bankcode,
										'ACCOUNT_NUMBER' => $row['0'],
										'ACCOUNT_ALIAS' => $row['1'],
										'VALID' => $valid
									);


									array_push($paramTrxArr,$paramTrx);

								}
								else
								{
									$error_msg[0] = $this->language->_('Error').': '.$this->language->_('Wrong File Format').'.';
									$this->view->error 		= true;
									$this->view->report_msg	= $this->displayError($error_msg);
									break;
								}

						}

						// kalo gak ada error
						if(!$error_msg[0])
						{
							$confirm = true;
						}
							
					}

				}
				else
				{
					$this->view->error = true;
					foreach($adapter->getMessages() as $key=>$val)
					{
						if($key=='fileUploadErrorNoFile')
							$error_msg[0] = $this->language->_('Error').': '.$this->language->_('File cannot be left blank. Please correct it').'.';
						else
							$error_msg[0] = $val;
						break;
					}
					$errors = $this->displayError($error_msg);
					$this->view->report_msg = $errors;
				}


				if($confirm)
				{
					$result['success'] = $success;
					$result['failed'] = $failed;
					// $result['success'] = $total;
					$content['paramcount'] = $result;
					$content['paramTrxArr'] = $paramTrxArr;
					$content['errorMsg'] = $errorMsg;
					// $content['errorTrxMsg'] = $errorTrxMsg;
					// $content['payment'] = $payment;
					// $content['sourceAccountType'] = $sourceAccountType;


					$sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
					$sessionNamespace->content = $content;
					$this->_redirect('/multiaggregation/importfile/confirm');
				}
			}
		}
	}

	public function confirmAction()
	{
		$this->_helper->layout()->setLayout('newlayout');
		$sessionNamespace = new Zend_Session_Namespace('confirmBulkCredit');
		$data = $sessionNamespace->content;

		$selectbank = $this->_db->select()
					 ->from(array('C' => 'M_BANK_TABLE'),array('*'))
					->where('C.BANK_CODE = ?',$data['paramTrxArr'][0]['BANK_CODE']);
					// echo $selectbank;die;
		$databank 					= $this->_db->fetchAll($selectbank);
		$this->view->BANK_NAME = $databank['0']['BANK_NAME'];

		$this->view->countdata = $data['paramcount'];

		if ($data['paramcount']['failed'] > 0) {
			$this->view->failed = true;
		}

		$complist = $this->_db->fetchAll(
                    $this->_db->select()
                         ->from(array('A' => 'M_USER'),array('CUST_ID'))
                       
                         ->where("A.USER_ID = ? ", $this->_userIdLogin)
       );	
    	$comp = "'";
    	// print_r($complist);die;
    	foreach ($complist as $key => $value) {
    		$comp .= "','".$value['CUST_ID']."','";
    	}
    	$comp .= "'";
		$acctlist = $this->_db->fetchAll(
					$this->_db->select()
						 ->from(array('A' => 'M_APIKEY'))
						 ->join(array('C' => 'M_APICREDENTIAL'),'A.APIKEY_ID = C.ID',array('*'))
						  ->join(array('B' => 'M_BANK_TABLE'),'B.BANK_CODE = A.BANK_CODE',array('B.BANK_NAME'))
						 // ->where('A.ACCT_STATUS = ?','5')
						 ->where("A.CUST_ID IN (".$comp.")")
						 ->order('A.APIKEY_ID ASC')
						 // echo $acctlist;
				);

		$bankcode = $data['paramTrxArr'][0]['BANK_CODE'];

		$account = array();
		foreach ($acctlist as $key => $value) {
			if($bankcode == $value['BANK_CODE']){
			$account[$value['FIELD']] = $value['VALUE'];
			$account['BANK_CODE'] = $value['BANK_CODE'];
			$account['SENDER_ID'] = $value['SENDER_ID'];
			$account['AUTH_USER'] = $value['AUTH_USER'];
			$account['AUTH_PASS'] = $value['AUTH_PASS'];
			$account['BANK_NAME'] = $value['BANK_NAME'];
			$account['SIGNATURE_KEY'] = $value['SIGNATURE_KEY'];
			}
		}

		if($this->_request->isPost() )
		{
			if($this->_getParam('submit_cancel') == TRUE)
			{
				unset($_SESSION['confirmBulkCredit']);
				$this->_redirect('/multiaggregation/importfile');
			}

			$request = array();

			$errordata = 'Invalid Account ';
			$error = false;
			
			//echo '<pre>';
			//var_dump($data['paramTrxArr']);die;
			
			foreach($data['paramTrxArr'] as $row)
			{
				
				if($row['VALID']){
				 // $this->_db->beginTransaction();

					$selectapikey = $this->_db->select()
	 				->from(array('A' => 'M_APIKEY'))
	 				->order('A.APIKEY_ID DESC')
	 				->limit(1);

					$apikey = $this->_db->fetchAll($selectapikey);

					if (!empty($apikey)) {
						
						$id = $apikey[0]['APIKEY_ID'] + 1 ;
					}else{

						$id = 1;
					}

				
			
						
					$request = $account;
					$request['account_number'] = $row['ACCOUNT_NUMBER'];

					//call api
					$clientUser  =  new SGO_Soap_ClientUser();
					
					$auth = Zend_Auth::getInstance()->getIdentity();
					$request['corporate_id_channel'] = $this->_custIdLogin;
					$request['corporate_name'] = $this->_custIdLogin;
					$request['bankcode'] = $bankcode;
					$request['account_alias'] = $row['ACCOUNT_ALIAS'];
					// echo "<pre>";
					// var_dump($row);
					// var_dump($request);
					// die;
					$field = array_keys($request);
					$value = array_values($request);
					// echo "<pre>";
					// var_dump($field);
					// var_dump($value);
					// die;
					//echo '<pre>';
					//var_dump($account);
					//var_dump($request);die;
					$success = $clientUser->callapi('registerapi',$request,'b2b/account/register');
					//var_dump($success);die;
					try {
						$paramlog = array(
			                    'DIGI_USER' => $this->_userIdLogin,
			                    'DIGI_CUST'	=> $this->_custIdLogin,
			                    'DIGI_BANK' => $request['BANK_CODE'],
			                    'DIGI_ACCOUNT' => $request['account_number'],
			                    'DIGI_ERROR_CODE' => $result->error_code,
			                    'DIGI_TIMESTAMP' => new Zend_Db_Expr("now()"),
			                    'DIGI_SERVICE'	=> 2
				        );

				        $this->_db->insert('T_DIGI_LOG',$paramlog);

					} catch (Exception $e) {
						var_dump($e);die;				
					}
				//	$resultregist  = $clientUser->getResult();
					//	 print_r($resultregist->error_code);die;
				//	if(!$success){
				//		$error = true;
				//		$returnStruct = array(
				//				'ResponseCode'	=>'XT',
				//				'ResponseDesc'	=>'Service Timeout',					
				//				'Cif'			=>'',
				//				'AccountList'	=> '',
				//		);
				//	}else{
						$resultregist  = $clientUser->getResult();
					
						if($resultregist->error_code == '0000'){

							$CustomerUser = new CustomerUser($this->_custIdLogin, $this->_userIdLogin);
							$request['BANK_CODE'] = $bankcode;
							$request['SENDER_ID'] = $CustomerUser->sslencrypt($resultregist->eoa_credentials->sender_id);
							$request['AUTH_USER'] = $CustomerUser->sslencrypt($resultregist->eoa_credentials->auth_username);
							$request['AUTH_PASS'] = $CustomerUser->sslencrypt($resultregist->eoa_credentials->auth_password);
							$request['EOA_SENDER'] = $CustomerUser->sslencrypt($resultregist->eoa_credentials->sender_id);
							// $request['BANK_NAME'] = $resultregist->eoa_credentials->signature_key;
							$request['SIGNATURE_KEY'] = $CustomerUser->sslencrypt($resultregist->eoa_credentials->signature_key);
							
							// echo "<pre>";
							// var_dump($request);

							$success 		= $clientUser->callapi('balance',$request,'b2b/inquiry/balance');
							$result  = $clientUser->getResult();
							try {
								$paramlog = array(
					                    'DIGI_USER' => $this->_userIdLogin,
					                    'DIGI_CUST'	=> $this->_custIdLogin,
					                    'DIGI_BANK' => $request['BANK_CODE'],
					                    'DIGI_ACCOUNT' => $request['account_number'],
					                    'DIGI_ERROR_CODE' => $result->error_code,
					                    'DIGI_TIMESTAMP' => new Zend_Db_Expr("now()"),
					                    'DIGI_SERVICE'	=> 1
						        );

						        $this->_db->insert('T_DIGI_LOG',$paramlog);

							} catch (Exception $e) {
								var_dump($e);die;				
							}

							//echo '<pre>';
							// var_dump($result);die;
							if($result->error_code == '0000'){
							try{

								
								$fieldArr = array('account_currency','account_name','account_number','account_alias');
								$fieldnotallow = array('BANK_CODE','SENDER_ID','AUTH_USER','SIGNATURE_KEY','AUTH_PASS','SIGNATURE_KEY');
		//						echo '<pre>';
	//							var_dump($request);
								for($i=0; $i<count($request); $i++){
								
									
									if(!in_array($field[$i], $fieldnotallow)){
										if(!in_array($field[$i], $fieldArr)){
										//		$value[$i] 	  = $CustomerUser->sslencrypt($value[$i]);
										}
										$content = array(
											'APIKEY_ID' 	 => $id,
											'BANK_CODE' 	 => $request['bankcode'],
											'CUST_ID' 		 => $this->_custIdLogin,
											'FIELD' 	 	 => $field[$i],
											'VALUE' 	 	 => $value[$i]
										);
										//echo '<pre>';
										//var_dump($content);
										if($field[$i] != NULL && $field[$i] != ''){
										$this->_db->insert('M_APIKEY',$content);
										}
									
									}
									
								}
								
								$selectapikey = $this->_db->select()
								->from(array('A' => 'M_BALANCE'))
								
								->limit(1);

								$apikey = $this->_db->fetchAll($selectapikey);
								
								
//								die;

								$insertkey['ID'] = $id;
								$insertkey['SENDER_ID']  = $CustomerUser->sslencrypt($resultregist->eoa_credentials->sender_id);
								$insertkey['AUTH_USER']  = $CustomerUser->sslencrypt($resultregist->eoa_credentials->auth_username);
								$insertkey['AUTH_PASS']  = $CustomerUser->sslencrypt($resultregist->eoa_credentials->auth_password);
								$insertkey['SIGNATURE_KEY']  = $CustomerUser->sslencrypt($resultregist->eoa_credentials->signature_key);
								$insertkey['ALLOW_IP']  = $resultregist->eoa_credentials->allowed_ip;
								$insertkey['CREATEDBY']  = $this->_userIdLogin;
								$insertkey['CREATED']  = new Zend_Db_Expr("now()");
								try{
								$this->_db->insert('M_APICREDENTIAL',$insertkey);
								}catch(Exception $e){
									//var_dump($e);die;
								}
								
								//updatecahce-----------------------
								$frontendOptions = array ('lifetime' => 86400,
		                                  'automatic_serialization' => true );
								$backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
								$cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
								$cacheID = 'MAGR'.$this->_custIdLogin;
								$data = $cache->load($cacheID);
								//echo '<pre>';
								//var_dump($data);die;
								$cache->remove($cacheID);
	
								//$bankcode = $this->_getParam('bankcode');
								//$acct = $this->_getParam('acct');
								$balance = $this->_db->fetchAll(
											$this->_db->select()
												 ->from(array('A' => 'M_BALANCE'))
												 
												 
												 ->where("A.CUST_ID = ? ",$this->_custIdLogin)
												 //->where("A.ACCT_NO = ? ",$result->account_number)
												 ->where("A.BANK_CODE = ? ",$request['bankcode'])
												 
											);
								 $param = array(
										 'ACCT_NO' => $result->account_number,
										 'BANK_CODE' => $request['bankcode'],
										 'ACCT_NAME' => $result->account_name,
										 'CUST_ID' => $this->_custIdLogin,
										 'CCY' => $result->account_currency,
										 'BALANCE' => $result->account_balance,
										 'UPDATED' => new Zend_Db_Expr("now()"),
										 'RABBIT_ID' => $balance['0']['RABBIT_ID']
									 );
									 $this->_db->insert('M_BALANCE',$param);			

								$save['lastupdate']	= date('Y-m-d H:i:s');
								
								$save['totalmanual'] = $data['totalmanual'];
								$save['totalonline'] = $data['totalonline']+$result->account_balance;
								
								$totaldata = count($data['datapers']);
								$data['datapers'][$totaldata+1]['apikey_id']  = $id;
								$data['datapers'][$totaldata+1]['bank_name']  = $request['BANK_NAME'];
								$data['datapers'][$totaldata+1]['bank_code']  = $request['bankcode'];
								$data['datapers'][$totaldata+1]['account_number']  = $result->account_number;
								$data['datapers'][$totaldata+1]['account_name']  = $result->account_name;

								if (isset($request['account_alias'])) {
									$data['datapers'][$totaldata+1]['account_alias']  = $request['account_alias'];
								}

								$data['datapers'][$totaldata+1]['account_currency']  = $result->account_currency;
								$data['datapers'][$totaldata+1]['account_balance']  = $balance['0']['BALANCE'];
								$data['datapers'][$totaldata+1]['rs_datetime']	=  Application_Helper_General::convertDate($balance['0']['UPDATED'],$this->view->displayDateTimeFormat,$this->view->defaultDateFormat);
								$data['datapers'][$totaldata+1]['account_status'] = '1';
								$data['datapers'][$totaldata+1]['user'] = $this->_userIdLogin;
								$data['datapers'][$totaldata+1]['cust'] = $this->_custIdLogin;

								$save['datapers'] = $data['datapers'];
								//echo "<pre>";
								// var_dump($save);die;
								$totalonline = $data['totalonline'] + $result->account_balance;
								$save['totalonline'] = $totalonline;
								$cache->save($save,$cacheID);
								
								//updatecahce-----------------------
								
								
								
								$result = $this->_db->fetchRow(
									$this->_db->select()
										->from(array('C' => 'M_USER'))
										->where("USER_ID = ".$this->_db->quote($this->_userIdLogin))
								);

								if($result['USER_HOME_CONTENT'] == ''){
									$updateArr = array(
													'USER_HOME_CONTENT' => '1'
												);

								}else{

									$strdata = $result['USER_HOME_CONTENT'].',1';

									$updateArr = array(
													'USER_HOME_CONTENT' => $strdata
												);
								}
								

  	 							$updateWhere['USER_ID = ?'] = (string)$this->_userIdLogin;
  	 							$this->_db->update('M_USER',$updateArr,$updateWhere);


  	 							//clear cache if account added
								// $frontendOptions = array ('lifetime' => 600,
			     //                              'automatic_serialization' => true );
						  //       $backendOptions = array ('cache_dir' => LIBRARY_PATH.'/data/cache/acctbalance/' ); // Directory where to put the cache files
						  //       $cache = Zend_Cache::factory ( 'Core', 'File', $frontendOptions, $backendOptions );
						  //       $cacheID = 'BAL'.$custid;

						  //       $cache->remove($cacheID);

  	 							$this->view->success = true;
								$this->view->report_msg = array();

								// $this->setbackURL('/'.$this->_request->getModuleName().'/'.$this->_request->getControllerName().'/index/');
								// $this->_redirect('/notification/success');

							}catch(Exception $e){
								// var_dump($e);die;
								$this->view->report_msg = array('error'=>true);
							}

							}else{

								$errordata .= ', '.$request['account_number'];
								$error = true;
								
							}


						}else{
							$error = true;
							$this->view->report_msg_service = array('error'=>true);
							$this->view->service_error = $result->error_message;
						}
				//	}
				}
			}

			if($error){
				$sessionData = new Zend_Session_Namespace('ACCT_STATS');
				$sessionData->error 			= $errordata;
				$this->view->error = $errordata;
				$this->_redirect('/multiaggregation/importfile');	
			}else{
				$this->setbackURL('/'.$this->_request->getModuleName().'/importfile');
				

				$this->_redirect('/notification/success');	
			}
		}
	}


	private function parseCSV($fileName){
		$csvData = false;
		try {
				$Csv = new Application_Csv (  $fileName, $separator = "," );
				$csvData = $Csv->readAll ();
			} catch ( Exception $e ) {
				echo nl2br ( $e->getTraceAsString () );
			}
			return $csvData;
	}

	public function banksuggestionAction()
	{
		$this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();

        $bankarr = array(
        	'008' => array(
        		'name' 		=> 'Bank Mandiri',
        		'imgsource' => '/assets/themes/assets/newlayout/img/mandiri.png' 
        	),
        	'009' => array(
        		'name' 		=> 'Bank BNI',
        		'imgsource' => '/assets/themes/assets/newlayout/img/bni.png' 
        	),
        	'013' => array(
        		'name' 		=> 'Bank Permata',
        		'imgsource' => '/assets/themes/assets/newlayout/img/permata.png' 
        	),
        	'014' => array(
        		'name' 		=> 'Bank BCA',
        		'imgsource' => '/assets/themes/assets/newlayout/img/bca.png' 
        	)
        );

        $searchTerm = $_GET['term'];

        $bankData = array();
        foreach ($bankarr as $key => $value) {

        	if (strpos(strtolower($value['name']), strtolower($searchTerm)) !== false) {
        		$data['id']    = $key;
		        $data['value'] = $value['name'];
		        $data['label'] = '
		        <a href="javascript:void(0);" style="text-decoration:none;">
		            <img class="img-fluid" src="'.$value['imgsource'].'" width="70"/>
		            <span>  '.$value['name'].'</span>
		        </a>';
		        array_push($bankData, $data);
        	}
        }

        echo json_encode($bankData);

	}
}
