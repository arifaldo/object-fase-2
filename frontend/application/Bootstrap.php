<?php
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{	
	
	protected function _initApplication() 
	{
		Zend_Registry::set('bootstrap', $this);

		$resource = $this->getPluginResource('db');
		$db = $resource->getDbAdapter();
		try {  
				$db->getConnection();
		} catch( Exception $e ) {
				header('Location: ../../failed.html');
		}

		//set the Zend_Db_Table default adapter
		$config = $this->getOptions();
		$dbAdapter = Zend_Db::factory($config['resources']['db']['adapter'],$config['resources']['db']['params']);
		Zend_Db_Table_Abstract::setDefaultAdapter($dbAdapter);
		
	    //FORMAT NUMBERf
		$locale = new Zend_Locale();
		$locale->setDefault('en_US');
		Zend_Registry::set('Zend_Locale', 'en_US');
		Zend_Locale_Format::setOptions(array('precision'=>2));
		
	    
		// Zend_View_Helper_PaginationControl::setDefaultViewPartial('controls.phtml');
       
		$config = $this->getOptions();
		
		
		//// Start Multi Language
		// $defaultlanguage='en';
		$defaultlanguage='id';
		$ns = new Zend_Session_Namespace('language');
		$sessionLanguage = $ns->langCode ;
		// $sessionLanguage='id';
		
		
		if(!is_null($sessionLanguage))
		{
			$setlang = $sessionLanguage;
			// echo "<code>TIDAK KOSONG = setlang = $setlang</code>"; 
		}
		else
		{
			// $setlang = $locale->getLanguage();
			$setlang='id';
			// echo "<code>KOSONG = setlang = $setlang</code>"; 
		}
		if (!empty($setlang) && isset($setlang)) {
			$defaultlanguage=$setlang;
			$ns->langCode = $defaultlanguage;
		}
		
		Zend_Loader::loadClass('Zend_Translate');
		
		$frontendOptions = array(
			'cache_id_prefix' => 'MyLanguage',
			'lifetime' => 86400,
			'automatic_serialization' => true
		);
		$backendOptions = array('cache_dir' => APPLICATION_PATH.'/../../library/data/cache/language/frontend');
		$cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
		Zend_Translate::setCache($cache);
		
		$translate = new Zend_Translate('tmx', APPLICATION_PATH.'/../../library/data/languages/frontend/language.tmx', $defaultlanguage);
// 		Zend_Debug::dump($translate);
		Zend_Registry::set('language', $translate);
		//// End Multi Language
		
		Zend_Registry::set('itemCountPerPage',$config['paging']['defaults']['itemCountPerPage']);
		Zend_Registry::set('pageRangeCount',$config['paging']['defaults']['pageRangeCount']);
		Zend_Registry::set('scrollingStyle',$config['paging']['defaults']['scrollingStyle']);
		//Zend_View_Helper_PaginationControl::setDefaultViewPartial('controls.phtml');
		
		Zend_Registry::set('bootstrap', $this);
		
		Zend_Registry::set('config',$config);
		Zend_Registry::set('date',$config['date']);
		
		/* Hary */
		if(isset($config['app']))
			Zend_Registry::set('app',$config['app']);
		
		if(isset($config['token']))
			Zend_Registry::set('token',$config['token']);
		
		if(isset($config['sms']))
			Zend_Registry::set('sms',$config['sms']);
					
	}
	
	protected function _initAutoLoader()	
	{
	    //set autoloader for custom library
		$loader = Zend_Loader_Autoloader::getInstance();
		$loader->registerNamespace('Application_');			
		$loader->registerNamespace('SGO_');		
		$loader->registerNamespace('Service_');		
	}
	
	
	protected function _initView() 
	{
	  $view = new Zend_View();
   	   
	  
	  // membuka folder partials agar file phtml didalamnya dpt dipakai di view
	  $view->addScriptPath(APPLICATION_PATH . '/partials'); 
	 
	  // utk manggil fungsi yang ada di application/view/helper agar bisa dipakai di view 
	  $view->addHelperPath('Application/View/Helper/', 'Application_View_Helper');
	  $view->addHelperPath('ZendX/JQuery/View/Helper/', 'ZendX_JQuery_View_Helper');
		
	  //setup jQuery, agar dapat dipakai di view
      $view->addHelperPath('ZendX/JQuery/View/Helper/', 'ZendX_JQuery_View_Helper');
      $view->jQuery()
			 ->setLocalPath('/js/jquery/jquery-1.4.2.min.js')
      		 ->setUiLocalPath('/js/jquery/jquery-ui-1.8.2.custom.min.js')
      		 ->addStylesheet('/js/jquery/css/ui-smoothness/jquery-ui-1.8.2.custom.css');
      //enable jQuery in view
      $view->jQuery()->enable()->uiEnable();
      
	  Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer')->setView($view);   
	}

	
	protected function _initSession()
	{
		// if (!Zend_Registry::isRegistered("IS_CRON")){
			try
			{
				$config = array(
					'name'           => 'fosession',
					'primary'        => 'id',
					'modifiedColumn' => 'modified',
					'dataColumn'     => 'data',
					'lifetimeColumn' => 'lifetime',
					'lifetime' => '86400',
					'overrideLifetime' => '86400',
					'cookie_httponly'	=> false,
				);
				
				 
				 
				//create your Zend_Session_SaveHandler_DbTable and
				//set the save handler for Zend_Session
				Zend_Session::setSaveHandler(new Zend_Session_SaveHandler_DbTable($config));
				 
				//start your session!
				// Zend_Session::start();
			}catch(Exception $e)
			{
				Zend_Debug::dump($e->getMessage());
			}
		// }
		//now you can use Zend_Session like any other time
	}
	
	


	/**
	 * initiate user login for all field related
	 */
	protected function _initUserLogin()
	{

	
	}
	
	/**
	 * initiate action helper
	 
	*/
	protected function _initActionHelper()
	{
		Zend_Controller_Action_HelperBroker::addPrefix('SGO_Helper');
	}
	
	
	protected function _initPlugin(){
		$front=Zend_Controller_Front::getInstance();
	    $front->registerPlugin(new Application_Frontend_Auth());
//	    $front->registerPlugin(new Application_Frontend_Acl());
		
	    $protect = new SGO_Security_CsrfProtect(array(
    		'expiryTime' => 86400, //will make the CSRF key expire in 5 minutes.
    		'keyName' => 'safetycheck', //will make the CSRF form element be called "safetycheck". Defaults to "csrf"
	    ));
	    
	    $front->registerPlugin($protect);
//	    $front->registerPlugin(new Application_AutoEscapeViewVariable());
	}
}