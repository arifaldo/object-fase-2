<?php 
$bankCode = (isset($bankCode) ? $bankCode : NULL);

// CSS
//$this->headLink()->appendStylesheet('/css/allStyle'.$bankCode.'.css');
//$this->headLink()->appendStylesheet('/css/login.css');

// JS
//$this->headScript()->appendFile('/js/allJs.php');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
		<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8" ><title>Frontend - INTERNET BANKING</title><link rel="stylesheet" href="/js/jquery/css/ui-smoothness/jquery-ui-1.8.2.custom.css" type="text/css" media="screen">
		
		<script type="text/javascript" src="/js/jquery/jquery-1.4.2.min.js"></script>
		<script type="text/javascript" src="/js/jquery/jquery-ui-1.8.2.custom.min.js"></script>
		<script type="text/javascript">
			var currenttime = new Date();
		</script>
		<script type="text/javascript" src="/js/allJs.js"></script>
		<link href="/css/allStyle<?=$bankCode;?>.css" media="screen" rel="stylesheet" type="text/css" >
		<link rel="shortcut icon" href="/favicon<?=$bankCode;?>.ico"> 
		<link href="/css/login.css" media="screen" rel="stylesheet" type="text/css" >
		<link href="/js/allJs.php" media="screen" rel="stylesheet" type="text/css" >
		
		<style>
			body{background:white;}
		</style>
	</head>

	<body>
	<span id="cdcontainer" style="display:none"></span>
		<!--<table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-top:20px">
			<tr>
				<td valign="BOTTOM" width="720px" height="200px" style="background:WHITE no-repeat;background-position:center;">
					<div align='CENTER' style='padding-bottom:10px'>
				        <img src="/images/LOGO/logo<?=$bankCode;?>.jpg"> 
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<table align = "CENTER" border="0" cellpadding="8" cellspacing="8" width="50%" class="loginscreencontent gradient">
						<tbody>
							<tr>
								<td align="center">
									<div id="box_outer">
										<div class="tbl-logincontent">
											<p><h3>Sorry</h3></p>
											<p>&nbsp;</p>
											<p><font>We are unable to continue transaction.<br>Please try again</font></p>
											<p>&nbsp;</p>
											<div><input  class ="inputbtn" value="Go to home page" onclick="location.href = '../../home' " type="button"></div>
										</div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>		
					<br/>
				</td>
			</tr>
		</table>		
		-->
		
		<div id="container">
			<div id="new" class="wrap" align="center">
				<div id="headermayapada" >
					
				</div>
				
				<div class="content">
					
					<div class="left">
						
						<div id="nivo-slider" class="nivoSlider">
							<!--
							<img src="/images/slider/banner.jpg" style="border: none;" />
							<img src="/images/slider/banner.jpg" style="border: none;" />
							<img src="/images/slider/banner.jpg" style="border: none;" />
							<img src="/images/slider/banner.jpg" style="border: none;" />
							-->
						</div>						
						
					</div>
						
					<div class="right gradientlogin">
						<p><h3>Sorry</h3></p>
						<p>&nbsp;</p>
						<p><font>We are unable to continue transaction.<br>Please try again</font></p>
						<p>&nbsp;</p>
						<div><input  class ="inputbtn" value="Go to home page" onclick="location.href = '../../home' " type="button"></div>
					</div>
				</div>
			</div>
		</div>
		
		<br/>
		
		<div align="center">
			Supported By <a href="http://www.sgo.co.id/"><b>Square Gate One</b></a>
		</div>
	</body>
</html>