<?php

	$dir = realpath(dirname(__FILE__));

	$global = file_get_contents($dir.'/global.min.js');
	$jqueryUiCore = file_get_contents($dir.'/jquery/ui/jquery.ui.core.js');
	$jqueryUiWidget = file_get_contents($dir.'/jquery/ui/jquery.ui.widget.js');
	$jqueryUiAutocomplete = file_get_contents($dir.'/jquery/ui/jquery.ui.autocomplete.js');
	$jqueryUiDatepicker = file_get_contents($dir.'/jquery/ui/jquery.ui.datepicker.js');
	$custom = file_get_contents($dir.'/sgo/custom.js');
	$ddsmoothmenu = file_get_contents($dir.'/ddsmoothmenu.min.js');
	// $JsSimpleDateFormat = file_get_contents($dir.'/JsSimpleDateFormat.js');

	//added by DW (14-10-2019)
	// $jquery3 				= file_get_contents($dir.'/jquery-3.3.1.min.js');
	$jqueryMin 				= file_get_contents($dir.'/jquery.min.js');
	$jsChart 				= file_get_contents($dir.'/chart.min.js');
	// $kendo 					= file_get_contents($dir.'/kendo.all.min.js');
	$jqueryNiceScroll 		= file_get_contents($dir.'/jquery.nicescroll.min.js');
	$popper 				= file_get_contents($dir.'/popper.js');
	$bootstrap 				= file_get_contents($dir.'/bootstrap.min.js');
	$jqueryForm 			= file_get_contents($dir.'/jquery.form.min.js');
	$gijgo 					= file_get_contents($dir.'/gijgo.min.js');
	$fixTable 				= file_get_contents($dir.'/jQuery.fixTableHeader.min.js');
	$cleave 				= file_get_contents($dir.'/cleave.min.js');
	$muriHammer 			= file_get_contents($dir.'/muri/hammer.min.js');
	$muriWebAnimations 		= file_get_contents($dir.'/muri/web-animations.min.js');
	$muriJs					= file_get_contents($dir.'/muri/muuri.min.js');
	$croppie 				= file_get_contents($dir.'/croppie.min.js');
	$cropper 				= file_get_contents($dir.'/cropper.min.js');
	// $facedetection 			= file_get_contents($dir.'/jquery.facedetection.min.js');
	$webcam 				= file_get_contents($dir.'/webcam.min.js');
	//---------------------------------------------------------------------------------
	header('Content-Type: application/javascript');
	//added by DW (14-10-2019)
	// echo $jquery3."\n\n\n\n";
	// echo $jqueryMin."\n\n\n\n";
	echo $jsChart;
	echo $jqueryNiceScroll."\n\n\n\n";
	echo $popper."\n\n\n\n";
	//echo $bootstrap."\n\n\n\n";
	//echo $jqueryForm."\n\n\n\n";
	//echo $gijgo."\n\n\n\n";
	//echo $fixTable."\n\n\n\n";
	//echo $cleave."\n\n\n\n";
	// echo $croppie."\n\n\n\n";
	// echo $muriHammer."\n\n\n\n";
	// echo $muriWebAnimations."\n\n\n\n";
	// echo $muriJs."\n\n\n\n";
	// echo $cropper."\n\n\n\n";
	// // echo $facedetection."\n\n\n\n";
	// echo $webcam."\n\n\n\n";
	// echo $global."\n\n\n\n";
	// //-------------------------

	// echo $ddsmoothmenu."\n\n\n\n";
	// echo $jqueryUiCore."\n\n\n\n";
	// echo $jqueryUiWidget."\n\n\n\n";
	// echo $jqueryUiAutocomplete."\n\n\n\n";
	// echo $jqueryUiDatepicker."\n\n\n\n";
	// echo $custom."\n\n\n\n";
	// echo $JsSimpleDateFormat."\n\n\n\n";

	//added by DW (14-10-2019)
	// echo $kendo."\n\n\n\n";
	
	//-------------------------

?>

<?php
	
?>

