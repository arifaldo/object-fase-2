<?php
//die('here');
require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
require_once 'SGO/Helper/Parser.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
	
	
	//$thisClass = $this;
	$queueService = SGO_Extendedmodule_Queueservice_Service::getInstance();
		
		$datapers = array();
		$logName = 'Service_SMS::sendSMS';
		$pathFile = null;
		$printToDisplay = true;
		$logFileType = "ALL";
		
	
		$loggerService = SGO_Extendedmodule_Loggerservice_Service::getInstance();
		$logger = $loggerService->getLogger($logName, $pathFile, $printToDisplay, $logFileType);
		//var_dump($logger);
		//var_dump($queueService);die;
		$sms = array();
		$callback = function($sms) use ($logger,$queueService) {

			$db = Zend_Db_Table::getDefaultAdapter(); 
				
			$rabData = json_decode($sms->body, true); //data from rabbit

			$bankCode = $rabData['BANK_CODE'];
	        $select = $db->select()
                ->from(array('R' => 'M_RECON_ADAPTER'),array('*'))
                ->join(array('D' => 'M_RECON_ADAPTER_DETAIL'), 'R.ADAPTER_ID = D.ADAPTER_ID', array('*'))
                ->where('R.BANK_CODE = ?', (string)$bankCode);
        	$reconData = $db->fetchAll($select); 

        	//startrow 
        	$statementStartRow  = $reconData[0]['STATEMENT_START_FROM_ROW'];
        	$reportStartRow 	= $reconData[0]['REPORT_START_FROM_ROW'];

        	$statementData = json_decode($rabData['STATEMENT_FILE_DATA'], 1);
            $reportData = json_decode($rabData['REPORT_FILE_DATA'], 1);

        	//header data
        	$statementHeaderData = $statementData[$statementStartRow-1];
            $reportHeaderData = $reportData[$reportStartRow-1]; 

            //--------------unset data from above start row---------------------
            if (!empty($statementStartRow)) {
                for($i=0; $i<$statementStartRow;$i++){
                    unset($statementData[$i]);
                }
            }

            if (!empty($reportStartRow)) {
                for($i=0; $i<$reportStartRow;$i++){
                    unset($reportData[$i]);
                }
            } 

            //---------get rid of empty spaces between column on statement data---------
            $statementEmptySpaces = array();
            foreach ($statementHeaderData as $key => $value) {
                if (empty($value)) {
                    array_push($statementEmptySpaces, $key);
                    unset($statementHeaderData[$key]);
                }
            }
            foreach ($statementData as $key => $value) {
                foreach ($statementEmptySpaces as $spaces) {
                    unset($statementData[$key][$spaces]);
                }
            }

            //---------get rid of empty spaces between column on report data---------
            $reportEmptySpaces = array();
            foreach ($reportHeaderData as $key => $value) {
                if (empty($value)) {
                    array_push($reportEmptySpaces, $key);
                    unset($reportHeaderData[$key]);
                }
            }
            foreach ($reportData as $key => $value) {
                foreach ($reportEmptySpaces as $spaces) {
                    unset($reportData[$key][$spaces]);
                }
            }

            //remove empty array
            foreach ($statementData as $key => $value) {
                $limit = count($value) / 2;

                $count = 0;
                foreach ($value as $key2 => $value2) {
                    if (empty($value2)) {
                        $count++;
                    }
                }

                if ($count >= $limit) {
                    unset($statementData[$key]);
                }
            }

             //remove empty array
            foreach ($reportData as $key => $value) {
                $limit = count($value) / 2;

                $count = 0;
                foreach ($value as $key2 => $value2) {
                    if (empty($value2)) {
                        $count++;
                    }
                }

                if ($count >= $limit) {
                    unset($reportData[$key]);
                }
            }
        	
        	//--------------------------------------------------sync data with adapter setting---------------------------------------- 

        	foreach ($reconData as $key => $value) {
        		foreach ($statementData as $key2 => $value2) {

                    
                    $skip = false;

                    //if selected column more than 1, then concat string
                    $columnValue = '';
                    if (strpos($value['STATEMENT_COLUMN'],',') !== false) {

                        $explodeColumnValue = explode(',', $value['STATEMENT_COLUMN']);

                        foreach ($explodeColumnValue as $explodeKey => $explodeValue) {
                            $columnValue .= $value2[$explodeValue];
                        }
                    }
                    else{
                        $columnValue = $value2[$value['STATEMENT_COLUMN']];
                    }

                    //if equal date, covert to standard date format
                    if ($value['OPERATOR'] == 'equaldate') {
                        $val = Application_Helper_General::convertDate($columnValue, 'dd-mm-yy', 'dd-mm-yy');
                    }

                    //if equal amount, check if amount is 0 or empty, then it is a debet column, not credit, so skip
                    else if($value['OPERATOR'] == 'equalamount'){

                        if (empty($columnValue) || intval($columnValue) == '0') {
                            $skip = true;
                        }
                        else{
                            $val = removeSpaces($columnValue);
                        }
                    }
                    else{
                         $val = removeSpaces($columnValue);
                    }

                    if (!$skip) {
                        $newStatementData[$key2][$value['HEADER_NAME']] = array(
                            'value' => $val,
                            'operator' => $value['OPERATOR']
                        );
                    }
        		}

        		foreach ($reportData as $key2 => $value2) {

                    //if selected column more than 1, then concat string
                    $columnValue = '';
                    if (strpos($value['REPORT_COLUMN'],',') !== false) {

                        $explodeColumnValue = explode(',', $value['REPORT_COLUMN']);

                        foreach ($explodeColumnValue as $explodeKey => $explodeValue) {
                            $columnValue .= $value2[$explodeValue];
                        }
                    }
                    else{
                        $columnValue = $value2[$value['REPORT_COLUMN']];
                    }

        			$newReportData[$key2][$value['HEADER_NAME']] = array(
        				'value' => ($value['OPERATOR'] == 'equaldate') ? Application_Helper_General::convertDate($columnValue, 'dd-mm-yy', 'dd-mm-yy') : removeSpaces($columnValue),
        				'operator' => $value['OPERATOR']
        			);
        		}
        	}

            //--------------------------------------------------end sync data with adapter setting---------------------------------------- 

            //limit recon data Ex: (date, amount, desc), then limit will be 3
            $limit = count($reconData);

        	$result = reconciliate($newStatementData, $newReportData, $limit);

        	//update recon_result
			$updateArr['RECON_RESULT'] = json_encode($result);
			$updateArr['FLAG'] = 2; // 2 == done
            $whereArr = array(
                'ID = ? ' => (string)$rabData['ID']
            );

            try{
            	$reconUpdate = $db->update('TEMP_RECON',$updateArr,$whereArr);
            }catch (Exception $e) {
                print_r($e);die;
			}

		};
		//$queueConsumer = '';
		//var_dump($queueConsumer);
		$queueConsumer = $queueService->getQueueByProfileName("RECON_PRODUCER");
		
		try {
			$queueConsumer->waitAndProcess($callback);
		} catch (Exception $e) {
			$param = [];
			Application_Log_GeneralLog::backgroundLog(Zend_Debug::dump($e->getMessage(),NULL,FALSE),$param);
			SGO_Helper_Supervisor::doExitByExitCodeName("RESTART");
		}


	//reconciliate data
	function reconciliate($statementData, $reportData, $limit){

        //the larger array will be leftdata
		// if (count($statementData) > count($reportData)) {
		// 	$leftData = $statementData;
		// 	$rightData = $reportData;
		// }
		// else{
		// 	$leftData = $reportData;
		// 	$rightData = $statementData;
		// }

        $leftData = $statementData;
        $rightData = $reportData;

		foreach ($leftData as $leftKey => $leftValue) {

            foreach ($rightData as $rightKey => $rightValue) {
                
                $match = 0;
                foreach ($leftValue as $leftKey2 => $leftValue2) {
                        
                    if ($leftValue2['operator'] == 'equal' || $leftValue2['operator'] == 'equaldate' || $leftValue2['operator'] == 'equalamount') {

                        if ($leftValue2['value'] == $rightValue[$leftKey2]['value']) {
                           $match++; 
                        }

                    }
                    else if($leftValue2['operator'] == 'contain' || $leftValue2['operator'] == 'containinverted'){

                        $inverted = false;
                        if ($leftValue2['operator'] == 'containinverted') {
                            $inverted = true;
                        }

                        if (compareContain($leftValue2['value'], $rightValue[$leftKey2]['value'], $inverted)) {
                            $match++;
                        }

                    }
                }

                //if all match, then add to matchData
                if ($match == $limit) {
                    $statementMatchData[] = $leftKey;
                    $reportMatchData[] = $rightKey;
                    
                    unset($leftData[$leftKey]);
                    unset($rightData[$rightKey]);
                }
            }
        }

        $statementDiffData = array_keys($leftData);
        $reportDiffData = array_keys($rightData);

        $result = array(
            'statementmatchdata' => $statementMatchData,
            'reportmatchdata' => $reportMatchData,
            'statementdiff' => $statementDiffData,
            'reportdiff' => $reportDiffData
        );


        return $result;
	}

    function compareContain($string1, $string2, $inverted){

        // $string1 = 'ATMLTRBCA000810142111591234500027TRFPRIMA FROM';
        // $string2 = '111591234500027B11EMERALDLANDATMLTRBCA000810142111591234500027';

        $string1 = preg_replace('/\D/', '', $string1);
        $string2 = preg_replace('/\D/', '', $string2);

        // echo $string1.' | '.$string2;die();

        $haystack = $string2;
        $needle = $string1;
        //if inverted is true
        if ($inverted) {
            $haystack = $string1;
            $needle = $string2;
        }

        $substr = substr($needle, 0, 15); //utk kasus va, ambil 15 huruf terdepan

        if (strpos($haystack, $substr) !== false) {
            return true;
        }
        else{
            return false;
        }

    }

	function removeSpaces($string){
		return str_replace(' ', '', $string);
	}

?>
