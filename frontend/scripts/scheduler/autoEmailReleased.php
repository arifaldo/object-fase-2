<?php
	require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
	require_once 'General/Customer.php';		
	require_once 'CMD/Payment.php';
	require_once 'General/Settings.php'; 
	
	$emailSent = 0;
	$payReffArr = array();
	$type = 'RELEASED';
	
	$config    		= Zend_Registry::get('config');
	$Settings = new Settings();
	$templateEmailMasterBankAddress = $Settings->getSetting('master_bank_address');
	$templateEmailMasterBankAppName = $Settings->getSetting('master_bank_app_name');
	$templateEmailMasterBankAppUrl = $Settings->getSetting('master_bank_app_url');
	$templateEmailMasterBankCity = $Settings->getSetting('master_bank_city');
	$templateEmailMasterBankCountry = $Settings->getSetting('master_bank_country');
	$templateEmailMasterBankEmail = $Settings->getSetting('master_bank_email');
	$templateEmailMasterBankFax = $Settings->getSetting('master_bank_fax');
	$templateEmailMasterBankName = $Settings->getSetting('master_bank_name');
	$templateEmailMasterBankProvince = $Settings->getSetting('master_bank_province');
	$templateEmailMasterBankTelp = $Settings->getSetting('master_bank_telp');
	$templateEmailMasterBankWebsite = $Settings->getSetting('master_bank_website');
	
	$template = file_get_contents(LIBRARY_PATH.'/email template/Approver Releaser Maker Email Notification.html');
	$FOOTER_ID = file_get_contents(LIBRARY_PATH.'/email template/footer_id.txt');
	$FOOTER_EN = file_get_contents(LIBRARY_PATH.'/email template/footer_en.txt');
	
	$transFT   = array( '[[APP_NAME]]' 			=> $templateEmailMasterBankAppName,
						'[[BANK_NAME]]' 		=> $templateEmailMasterBankName,
						'[[APP_URL]]' 			=> $templateEmailMasterBankAppUrl													
					  );			
	$FOOTER_ID = strtr($FOOTER_ID, $transFT);
	$FOOTER_EN = strtr($FOOTER_EN, $transFT);
				
	$db = Zend_Db_Table::getDefaultAdapter();
	
	$data = $db->SELECT()->DISTINCT()
						->FROM(array('P'=>'T_PSLIP'),array('P.CUST_ID','P.PS_CREATEDBY'))
						->JOINLEFT(array('E' => 'T_EMAIL_NOTIFICATION'),'P.PS_NUMBER = E.PS_NUMBER',array())
						->WHERE('P.PS_STATUS in (5,6)')
						->WHERE('E.PS_ISRELEASEDEMAILED = 0 OR E.PS_ISRELEASEDEMAILED is null')
						->QUERY()->FETCHALL();
// Zend_Debug::dump($data);
	if(is_array($data))
	{
		foreach($data as $cust)
		{	
			$releasedList = $db->SELECT()
								->FROM('M_USER',array('CUST_ID','USER_ID','USER_FULLNAME','USER_EMAIL'))
								->WHERE('CUST_ID =?',$cust['CUST_ID'])
								->WHERE('USER_ID = ?',$cust['PS_CREATEDBY'])
								->WHERE('USER_ISEMAIL = 1')
								->QUERY()->FETCHALL();
			// Zend_Debug::dump($releasedList);			
			if($releasedList)
			{
				
				foreach($releasedList as $cust_user)
				{					
					$paymentStatus 	= $config["payment"]["status"];
					$casePaymentStatus = Application_Helper_General::casePaymentStatus($paymentStatus);
					$payment = new Payment();
					$query = $payment->getPaymentList();
					$query ->JOINLEFT(array('TEN'=>'T_EMAIL_NOTIFICATION'),'P.PS_NUMBER = TEN.PS_NUMBER',array())
						   //->JOINLEFT(array('PH' => 'T_PSLIP_HISTORY'),'PS.PS_NUMBER = PH.PS_NUMBER',array())
						   ->JOINLEFT(array('V' => 'V_PSLIP_TRA')  , 'P.PS_NUMBER = V.PS_NUMBER', array('paystatus'		=> new Zend_Db_Expr("CASE P.PS_STATUS $casePaymentStatus ELSE 'N/A' END"),) )
								->WHERE('PS_ISRELEASEDEMAILED <> 1 or PS_ISRELEASEDEMAILED is NULL')
								->WHERE('UPPER(CUST_ID) = ?',$cust_user['CUST_ID'])
								->WHERE('UPPER(PS_CREATEDBY) = ?',$cust_user['USER_ID'])
								->where("DATE(P.PS_EFDATE) <= DATE(ADDDATE(now(), 1))")
								//->WHERE('PH.HISTORY_STATUS in (5,7)')
								->WHERE('P.PS_STATUS in (5,6)');
					$paymentList = $db->FETCHALL($query);					
                   	// Zend_Debug::dump($paymentList);
					if($paymentList)
					{
						$content = '';
						$counter = 0;
						foreach($paymentList as $paymentdata)
						{
							$caseBeneBank = "CASE P.PS_CATEGORY WHEN 'BULK PAYMENT' THEN CASE P.PS_TYPE WHEN '5' THEN '".$config['app']['bankname']."' ELSE '-' END ELSE CASE P.PS_TYPE WHEN '2' THEN T.BENEFICIARY_BANK_NAME ELSE '".$config['app']['bankname']."' END END";
							$caseMessage = "CASE P.PS_CATEGORY WHEN 'SINGLE PAYMENT' THEN T.TRA_MESSAGE ELSE '-' END";
							
							$info = $db->select()->distinct()
										->from(array('P' => 'T_PSLIP'),array(	'benebank' 	=> new Zend_Db_Expr($caseBeneBank),
																				'message' 	=> new Zend_Db_Expr($caseMessage)))
										->join(array('T' => 'T_TRANSACTION'),'P.PS_NUMBER = T.PS_NUMBER',array())
										->where('P.PS_NUMBER LIKE '.$db->quote($paymentdata['payReff']));
							//echo $info;die;
							$inforesult = $db->fetchRow($info);
							
							$fail = $db->select()->distinct()
										->from(array('T' => 'T_TRANSACTION'),array( 'fail' 		=> 'COUNT(T.TRANSACTION_ID)',
																					'ps_number' => 'T.PS_NUMBER'))
										->where('T.PS_NUMBER LIKE '.$db->quote($paymentdata['payReff']))
										->where('T.TRA_STATUS = 4')
										->group('T.PS_NUMBER');
							$failresult = $db->fetchRow($fail);
							
							if($paymentdata['accsrc'] == '-')
							{
								$accsrc = $paymentdata['accsrc'];
							}
							else
							{
								$accsrc = $paymentdata['accsrc'].'( '.$paymentdata['accsrc_ccy'].' ) - '.$paymentdata['accsrc_name'];
							}
							
							if($paymentdata['acbenef'] == '-')
							{
								$acbenef = $paymentdata['acbenef'];
							}
							else
							{
								$acbenef = $paymentdata['acbenef'].'( '.$paymentdata['acbenef_ccy'].' ) - '.$paymentdata['acbenef_name'];
							}
							
							if($failresult['fail'] && $paymentdata['PS_STATUS'] == 5)
							{
								$status_display = $paymentdata['paystatus'].' with '.$failresult['fail']. 'Transaction(s) Fail';
							}
							else
							{
								$status_display = $paymentdata['paystatus'];
							}
							
							$content .= '<tr>
								<td><font>'.++$counter.'</font></td>
								<td><font>'.$paymentdata['payReff'].'</font></td>
								<td><font>'.$paymentdata['paySubj'].'</font></td>
								<td><font>'.$accsrc.'</font></td>
								<td><font>'.$acbenef.'</font></td>
								<td><font>'.$paymentdata['ccy'].' '.Application_Helper_General::displayMoney($paymentdata['amount']).'</font></td>
								<td><font>'.$inforesult['message'].'</font></td>
								<td><font>'.Application_Helper_General::convertDate($paymentdata['efdate'],'dd MMMM yyyy','dd MMM yyyy HH:mm:ss').'</font></td>
								<td><font>'.$inforesult['benebank'].'</font></td>
								<td><font>'.$paymentdata['PS_CREATEDBY'].'</font></td>								
								<td><font>'.Application_Helper_General::convertDate($paymentdata['created'],'dd MMMM yyyy','dd MMM yyyy HH:mm:ss').'</font></td>
								<td><font>'.$paymentdata['payType'].'</font></td>
								<td><font>'.$status_display.'</font></td>
							</tr>';	
							$translate = array( '[[TITLE]]' => 'Released Payment Notification for Maker',
												'[[USER_FULLNAME]]' => $cust_user['USER_FULLNAME'],
												'[[USER_ID]]' => $cust_user['USER_ID'],
												'[[CUST_ID]]' => $cust_user['CUST_ID'],
												'[[FOOTER_ID]]' 		=> $FOOTER_ID,
												'[[FOOTER_EN]]' 		=> $FOOTER_EN,
												'[[APP_NAME]]' 			=> $templateEmailMasterBankAppName,
												'[[BANK_NAME]]' 		=> $templateEmailMasterBankName,
												'[[APP_URL]]' 				=> $templateEmailMasterBankAppUrl,	
												'[[master_bank_name]]' 	=> $templateEmailMasterBankName,
												'[[master_bank_fax]]' 	=> $templateEmailMasterBankFax,
												'[[master_bank_address]]' 	=> $templateEmailMasterBankAddress,
												'[[master_bank_telp]]' 	=> $templateEmailMasterBankTelp,	 										
									);	
									
							$mailContent = strtr($template,$translate);			
						
						}														
							
						if($content != '')
						{
							$translate = array( '[[DATA_CONTENT]]' => $content);
							$mailContent = strtr($mailContent,$translate);																		
														
							foreach($paymentList as $paymentdata)
							{		
								  $payReffArr[] = $paymentdata['payReff'];						
							}
							//echo $paymentdata['payReff'];
							$subject = 'Released Payment Email Notification for Maker';
							$status = Application_Helper_Email::sendEmail($cust_user['USER_EMAIL'],$subject,$mailContent);
							//echo $mailContent;
							Zend_Debug::dump($cust_user['USER_EMAIL']);
							Zend_Debug::dump($status);
							if($status)
							{
								$emailSent++; //akan di buka jika pengiriman email di buka
							}
							//echo $emailSent.' '.$status.' Sent';
							$string  = "Sending Released Payment Email Notification for Maker";
							Application_Helper_General::cronLog(basename(__FILE__),$string,1);
						}
						else
						{
							$string  = "Sending Released Payment Email Notification for Maker Failed";
							Application_Helper_General::cronLog(basename(__FILE__),$string,0);
						}
					}
					
					$payment->__destruct();
					unset($payment);
				}
			}
			else
			{
				echo $emailSent.' Email Sent';
			}
		}
		//Zend_Debug::dump($payReffArr);die;
		Application_Helper_General::setPaymentEmailNotification($type, $payReffArr);
	}

?>