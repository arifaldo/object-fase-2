<?php
require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
require_once 'General/Settings.php';
require_once 'SGO/Cron.php';
require_once 'CMD/Payment.php';
require_once 'Service/BulkPayment.php';
date_default_timezone_set('Asia/Jakarta');
$cron = new SGO_Cron();
$cron->buildCron('inprogressBulkMultiCredit',realpath(dirname(__FILE__)),3);
$flag = $cron->getCronDirFlag();
$time = $cron->getCronTime();
// echo realpath(dirname(__FILE__));die;
// echo $flag;die;
$i = 0;
echo $cron->start();

// if (file_exists($flag)){
//     echo 'here1';die;
// 	echo $cron->inprogress(date('Y-m-d H:i:s'));
// }else{
//     echo 'here0';
	echo $cron->prediction();
	$cron->touchCronDirFlag(basename(__FILE__));
	$ii = 0;
// 	echo 'here';die;
	while(strtotime(date('Y-m-d H:i:s')) <= strtotime($time['end'])){
		$ii ++;
		echo $ii.'.';
		
		businessLogic($i);
		sleep(1);
	}
	$cron->rmCronDirFlag(basename(__FILE__));
// }
echo $cron->finish(basename(__FILE__),$i);

/****************/
/*** BL Start ***/
/****************/
function businessLogic(&$i = 0){
	$db 		= Zend_Db_Table::getDefaultAdapter();
	$config 	= Zend_Registry::get('config');
	$setting 	= new Settings();

	$listpayroll = array('11','25','31','32');

	$data = $db->select()
		->from(array('P' => 'T_PSLIP'),array('*'))
		->where('P.PS_STATUS = 8')
		->where('P.SENDFILE_STATUS = 0')
		->where('DATE(P.PS_EFDATE) = DATE(now())')
		->where('P.PS_TYPE NOT IN (?)', $listpayroll) //do not execute payroll
		->order('P.PS_UPDATED ASC')
		->limit(5)
	;
// 	Application_Log_GeneralLog::backgroundLog(Zend_Debug::dump($data,'PSLIP',FALSE));
	$data = $db->fetchAll($data);
// 	Zend_Debug::dump($data,'PSLIP');
	if($data){
		foreach($data as $pslip){
			$i ++;
			$result = array();

			$trx = $db->select()
				->from(array('TT' => 'T_TRANSACTION'), array('*'))
				->joinLeft(array('TP'=>'T_PSLIP'), 'TP.PS_NUMBER = TT.PS_NUMBER', array('CUST_ID','USER_ID' => 'PS_CREATEDBY'))
				->joinLeft(array('MB'=>'M_BENEFICIARY'), 'MB.BENEFICIARY_ID = TT.BENEFICIARY_ID', array('BENEFICIARY_ADDRESS'))
				->where ('TT.PS_NUMBER = ?', $pslip['PS_NUMBER'])
			;
// 			Application_Log_GeneralLog::backgroundLog(Zend_Debug::dump($data,'TTRX',FALSE));
			$trx = $db->fetchAll($trx);

			$transactionList = array();
			foreach ($trx as $key => $val){
				if ($val['TRANSFER_TYPE'] == 1 ){
					$tranferType	= 'RTGS';
					$chargeAMount	= $setting->getSetting("global_charges_rtgs");
					$chargeCode		= $setting->getSetting("charges_acct_DOM");
				}elseif ($val['TRANSFER_TYPE'] == 2 ){
					$tranferType	= 'SKN';
					$chargeAMount	= $setting->getSetting("global_charges_skn");
					$chargeCode		= $setting->getSetting("charges_acct_DOM");
				}else{
					$tranferType	= '';
					$chargeAMount	= '';
					$chargeCode		= '';
				}
                
				$param['transactionId'] 			= $val['TRANSACTION_ID'];
				$param['paymentType']				= $tranferType;
				$param['ccy']						= $pslip['PS_CCY'];
				$param['sourceAccountNo'] 			= $val['SOURCE_ACCOUNT'];
				$param['destinationAccountNo'] 		= $val['BENEFICIARY_ACCOUNT'];
				$param['destinationAccountName'] 	= $val['BENEFICIARY_ACCOUNT_NAME'];
				$param['destinationAccountAddress']	= $val['BENEFICIARY_ADDRESS'];
				$param['transactionAmount'] 		= preg_replace("/[^0-9]/", "", Application_Helper_General::truncateDecimal($val['TRA_AMOUNT']*100)); //$val['TRA_AMOUNT'];
				$param['chargeAmount'] 				= $chargeAMount;
				$param['chargeCode'] 				= $chargeCode;
				$param['destinationBankCode'] 		= $val['BANK_CODE'];
				$param['description'] 				= $val['TRA_MESSAGE'];
				$param['chargeDescription'] 		= '';
				$transactionList[] 					= $param;
			}

			$data 	= new Service_BulkPayment();
			$result = $data->bulkPayment('CMS',$pslip['PS_NUMBER'],$transactionList);

// 			if ($result['ResponseCode'] !== 'XT'){
// 			if ($result['ResponseCode'] == '0000'){
				$dataUpd  = array('SENDFILE_STATUS' => 1, 'PS_UPDATED' => new Zend_Db_Expr("GETDATE()"));
				$whereUpd = array('PS_NUMBER = ?' => (string) $pslip['PS_NUMBER']);
				$db->update('T_PSLIP', $dataUpd, $whereUpd);
// 			}

			if ($result['ResponseCode'] == 'XT'){
				$customer = new Customer($pslip['CUST_ID']);
				$emailList = $customer->getEmailReleaserList();
				
				//$emailList 	= array();
				$payment 	= new Payment($pslip['PS_NUMBER'],$pslip['CUST_ID'],$pslip['PS_RELEASER_USER_LOGIN']);
				$payment->setPaymentException($result,$emailList);
			}
		}
	}
}
/****************/
/**** BL End ****/
/****************/
?>