<?php
require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
require_once 'General/Settings.php';


function lockUser($result,$timeout)
{
	$db = Zend_Db_Table::getDefaultAdapter();
	
	try{
		$db->beginTransaction();
		
		$res 	= null;
		$string = '';
		foreach($result as $row)
		{
			unset($where);
			$userid = $row['userid'];
			$lockreason = "New User is suspended. No login after ".$timeout." day(s). Last created was at ".$row['lastcreated'];	
			$update = array('BUSER_STATUS'  	=> '2',
							'BUSER_LOCKREASON'	=> $lockreason);		
			$where[] = "BUSER_ID = '".$userid."'";	
			$where[] = "BUSER_ID != 'SUPERADMIN01'";
			$res += $db->update('M_BUSER', $update, $where);
			
			$string .= "$custid -> $userid , "; 
			
			// Buser Status 1: active, 3: User deleted, 2: User Suspended
		}
		
		echo "ROW EFFECTED = ".$res;
		
		$db->commit();
		Application_Helper_General::cronLog('autoLockNewUserBackend', $string, '1');			   
	}
	catch(Exception $e) 
	{
		$db->rollBack();
		$string = $e->getMessage();
		Application_Helper_General::cronLog('autoLockNewUserBackend', $string, '0');		
	}
}


function autoLockNewUserBackend($timeout){
	
	if($timeout >= 0){
	
		$db = Zend_Db_Table::getDefaultAdapter();
			
		$result = $db->select()
					->from (array('U'=>'M_BUSER'),
								array(
										'userid'		=>'U.BUSER_ID',
										'lastcreated'	=>'U.CREATED',
										
									))
										
					->where ( "	U.BUSER_STATUS = '1' 
								AND U.BUSER_ISNEW = '1' 
								AND U.CREATED is not NULL 
								AND U.BUSER_LASTLOGIN is NULL" )
								
					->group (array('U.BUSER_ID','U.CREATED'))
						 
					->having('ADDDATE(CREATED,'.$timeout.') <= now() ')
					->query()->fetchall()
					;
					
// 					die($result);
			
			if(is_array($result) && count($result) > 0){
				
				//echo "Ada Data";
// 				print_r ($result);
				lockUser($result,$timeout);
				
			}
			else{
				echo "No Data";
			}
		
		//echo "";
		
		
	}
	
}

autoLockNewUserBackend(5);

?>