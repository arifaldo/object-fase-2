<?php
require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
require_once 'General/Settings.php';

$db = Zend_Db_Table::getDefaultAdapter();

function userIdle($diff)
{
	$db = Zend_Db_Table::getDefaultAdapter();	
	
	$getUserIdle = $db->select()
				  ->from('M_BUSER')
				  ->where('BUSER_ISLOGIN  = 1 AND TIMESTAMPDIFF(second, BUSER_LASTACTIVITY, now()) > ?', $diff)
				  ->query()
				  ->fetchall();
	forceLogout($getUserIdle);
}

function forceLogout($userIdle)
{
	$db = Zend_Db_Table::getDefaultAdapter();
	
	try{
		$db->beginTransaction();
		
		$res = null;
		foreach($userIdle as $row)
		{
			unset($where);
			$BUSER_ID = $row['BUSER_ID'];
			
			$logout = array(    'BUSER_ISLOGIN'      => '0' );		
			$where[] = "BUSER_ID = '".$BUSER_ID."'";
			$res += $db->update('M_BUSER', $logout, $where);
			echo "auto logout for user $BUSER_ID";
		}
		Application_Helper_General::cronLog('autoLogoutBackend', $res, '1');
		$db->commit();		   
	}
	catch(Exception $e) 
	{
		$db->rollBack();
		echo $e->getMessage();
	}
}

function autoLogout()
{
	$actionID = 'AULF';
	$moduleID = 'ALF';

	$set = new Settings();
	$minDiff = $set->getSetting('b_timeoutafter');
	
	userIdle($minDiff);
}

autoLogout();

?>