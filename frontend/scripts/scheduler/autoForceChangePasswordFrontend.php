<?php
require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
require_once 'General/Settings.php';

/*
SELECT t.CUST_ID, t.USER_ID, MAX(PASSWORD_DATETIME)
FROM "T_FPASSWORD_HISTORY" AS "T" 
group by t.CUST_ID, t.USER_ID
HAVING (DATEADD(day, 3,  MAX(PASSWORD_DATETIME)) < now())

*/

function forcechange($result,$timeout_pwd){
	
	
	$db = Zend_Db_Table::getDefaultAdapter();	
	
	try{
		
		$db->beginTransaction();
		$res = NULL;
		
		//print_r($result);
		if(is_array($result) && count($result) > 0){
		
			foreach($result as $row){
				
				unset($where);
				
				$userid = $row['userid'];
				$custid = $row['custid'];
				
				//echo "userid: $userid, custid: $custid <br />";
				$lockreason = "User is required to change password. Password will expire after ".$timeout_pwd." day(s). Last change password was at ".$row['datetime'];
				$update = array('USER_ISREQUIRE_CHANGEPWD'=>'1',
								'USER_ISLOGIN'=>'0',
								'USER_LOCKREASON'	=> $lockreason
								);
				$where[] = "USER_ID = '".$userid."'";
				$where[] = "CUST_ID = '".$custid."'";
				
				$res = $db->update('M_USER', $update, $where);
				
			}
// 			echo "ROW EFFECTED = ".$res."OOOOOOOOOOOOOOOO";
		}
		$db->commit();	
		Application_Helper_General::cronLog('autoForceChangePasswordUserFrontend', $res, '1');
	
	}
	catch(Exception $e){
		
		$db->rollBack();
		$string = $e->getMessage();
		Application_Helper_General::cronLog('autoForceChangePasswordUserFrontend', $string, '0');
		
	}

}


function forcepwdfrontend($timeout_pwd){
	
	$db = Zend_Db_Table::getDefaultAdapter();
	
	$result = $db->select()
				 ->from(array('A'=>'M_USER'),array( 'userid'	=>'USER_ID',
													'custid'	=>'CUST_ID',
													'datetime'	=>'USER_LASTCHANGEPWD'
													))
				 ->where("	A.USER_STATUS = '1' 
							AND A.USER_ISREQUIRE_CHANGEPWD != '1'
							AND A.USER_CREATED is not NULL
							AND TIMESTAMPDIFF(day, date(USER_LASTCHANGEPWD) , date(now()) ) > ?", $timeout_pwd
						)
				->query()
				->fetchall();
				
	// echo $result ->__toString();
	
	if(is_array($result) && count($result) > 0){
	
		//print_r($result);
		forcechange($result,$timeout_pwd);
		
	}
	
	else echo "No Data";
	
	
}

function autoforcechangepwdfrontend(){
	
	$set = new Settings();
	$timeout_pwd = $set->getSetting('pwd_expired_day');
	forcepwdfrontend($timeout_pwd);

}

autoforcechangepwdfrontend();

?>