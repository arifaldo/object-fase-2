<?php
	require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
	require_once 'General/Account.php';
	require_once 'General/Settings.php';
	
	$setting = new Settings();
	$starttime = $setting->getSetting('start_running_balance');
	$endtime = $setting->getSetting('cut_off_running_balance');	
	$begin = new DateTime($starttime);
	$end = new DateTime($endtime);
	$now = new DateTime();
		
			  	


	// if ($now >= $begin && $now <= $end){
		// die;
	
	// die('here');
	$db = Zend_Db_Table::getDefaultAdapter();
	$actionid = 'scheduler';
	$moduleid = 'reject payment';
	$interval = 2;
	$truecounter = 0;
	$falsecounter = 0;
	
	$dataCM = $db->select()
						->FROM('M_CUSTOMER_ACCT',array('ACCT_NO','CUST_ID','CCY_ID'))
						->WHERE('ACCT_STATUS = 1 OR ACCT_STATUS = 3 ')
						->QUERY()->FETCHALL();
	
	if($dataCM)
	{
		foreach($dataCM as $row)
		{
			$ccy = Application_Helper_General::getCurrNum($row['CCY_ID']);
			$account 	  = new Account($row['ACCT_NO'], $ccy);
			$accountCheck = $account->checkBalance();	// return array('checkBalanceStatus'=>$resultCheckBalance,'errorMessage'=>$errorMsgListener);
			$accountInfo  = $account->getCoreAccountInfo();
			if($accountInfo['availableBalance']=='N/A'){
				$accountInfo['availableBalance'] = 0;
			}
			if($accountCheck['checkBalanceStatus']!=''){

				$cekdata = $db->select()
						->FROM('T_BALANCE',array('*'))
						->WHERE('ACCT_NO = ?',$row['ACCT_NO'])
						->WHERE('DATE(PLAFOND_END) = DATE(NOW())')
						 // echo $cekdata;die;
						->QUERY()->FETCHALL();

				if(!empty($cekdata)){
					$logData = array(	
										'CUST_ID' 			=> $row['CUST_ID'],
										'CCY_ID' 			=> $row['CCY_ID'],
										'ACCT_NAME'			=> $accountInfo['accountName'],
										'PLAFOND' 			=> $accountInfo['availableBalance'],
										'ACCT_STATUS' 				=> 'A'
									);
					$where = array(
										'ACCT_NO'	=> $row['ACCT_NO'],
										'PLAFOND_END' 		=> new Zend_Db_Expr("now()")
					);
					// $db->insert('T_BALANCE', $logData);
					$db->update('T_BALANCE', $logData, $where); 

				}else{
					$logData = array(	'PLAFOND_END' 		=> new Zend_Db_Expr("now()"),
										'CUST_ID' 			=> $row['CUST_ID'],
										'ACCT_NO' 			=> $row['ACCT_NO'],
										'CCY_ID' 			=> $row['CCY_ID'],
										'ACCT_NAME'			=> $accountInfo['accountName'],
										'PLAFOND' 			=> $accountInfo['availableBalance'],
										'ACCT_STATUS' 				=> 'A'
									);
					$db->insert('T_BALANCE', $logData);

				}

				
			// 	print_r($accountInfo);
			// print_r($accountCheck);die;	
			}
		}
		
				
	}

// }

	
?>