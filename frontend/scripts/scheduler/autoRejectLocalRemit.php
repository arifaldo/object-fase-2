<?php
	require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
	require_once 'CMD/Payment.php';

	$db = Zend_Db_Table::getDefaultAdapter();
	$actionid = 'scheduler';
	$moduleid = 'reject payment';
	$interval = 2;

	$truecounter = 0;
	$falsecounter = 0;
// $select = $db->select()->distinct()
// 	->from(array('A' => 'T_TRANSACTION'),array())
// 	->joinleft(array('B' => 'T_PSLIP'), 'A.PS_NUMBER = B.PS_NUMBER',array('B.PS_STATUS','B.PS_CREATED'))
// 	->WHERE('A.TRANSFER_TYPE = ?','9')
// 	->WHERE('B.PS_STATUS = ?','6');

$setting 		= new Settings();
$auto_enabled 	= $setting->getSetting("cancel_local_remittance", 0);

	$dataCM = $db->select()->distinct()
	->from(array('A' => 'T_TRANSACTION'),array())
	->joinleft(array('B' => 'T_PSLIP'), 'A.PS_NUMBER = B.PS_NUMBER',array('B.PS_NUMBER','B.PS_STATUS','B.PS_CREATED'))
	->WHERE('A.TRANSFER_TYPE IN (9,10)')
	->WHERE('B.PS_STATUS = ?','11')
	->WHERE('B.PS_TYPE = ?','12')
	->QUERY()->FETCHALL();
	// print_r($dataCM);die;
	if($dataCM)
	{
		foreach($dataCM as $row)
		{
			$datenow = date('d-m-Y H:i:s');
			$days = '+'.$auto_enabled.' days';
			$s = $row['PS_CREATED'];
			// $date = strtotime($s);
			// $datep =  date('d/M/Y H:i:s', $date);
			// $param = date_add($datep, date_interval_create_from_date_string($menit));


			$minutes_to_add = $auto_enabled;

			// $Date1 = '2010-09-17';
// $date = new DateTime($Date1);

// $Date2 = $date->format('Y-m-d');

			$time = new DateTime($s);
			$time->modify($days);
			// $time->add(new DateInterval('PT' . $minutes_to_add . 'M'));

			$stamp = $time->format('Y-m-d H:i:s');

			// print_r($stamp);
			if(strtotime($datenow)>=strtotime($stamp)){
			
			$Payment = new Payment($row['PS_NUMBER'], null, $row['USER_ID'], 'System');
			$PS_REASON = "Authorization Overdue";

			$Payment->rejectPaymentLocalRemit($PS_REASON);


			}

			

			// $Payment = new Payment($row['PS_NUMBER'], $row['CUST_ID'], 'System');
			// $PS_REASON = "Auto Reject by System";			
			// if($Payment->rejectPayment($PS_REASON))
			// {
			// 	$truecounter++;
			// }
			// else
			// {
			// 	$falsecounter++;
			// }
		}
		
		// if($truecounter>0)
		// {
		// 	Application_Helper_General::cronLog(basename(__FILE__), 'Cron Reject Succes', '1');
		// }
		// if($falsecounter>0)
		// {
		// 	Application_Helper_General::cronLog(basename(__FILE__), 'Cron Reject Failed', '0');
		// }		
	}

	if($truecounter == 0 && $falsecounter == 0)
	{
		Application_Helper_General::cronLog('autoRejectPayment', 'No Data Payment for Auto Rejected', '1');
	}
?>