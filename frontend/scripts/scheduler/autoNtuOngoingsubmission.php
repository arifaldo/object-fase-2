<?php
	require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
	require_once 'CMD/Payment.php';
	require_once 'CMD/SinglePayment.php';
	require_once 'CMD/Validate/ValidatePaymentSingle.php';

	$db = Zend_Db_Table::getDefaultAdapter();
	

	$data = $db->select()
			->from(array('A' => 'TEMP_BANK_GUARANTEE'),array('*'))
			->where('A.BG_STATUS = 2')
			->orwhere('A.BG_STATUS = 3')
			->order('BG_CREATED DESC')->query()->fetchAll();
				
		 
	if($data)
	{
		foreach($data as $row)
		{
			$BG_CREATED							= $row['BG_CREATED'];
			$NTU_COUNTING								= $row['NTU_COUNTING'];

			$NTU_DATE = date('Y-m-d', strtotime('+'.$NTU_COUNTING.' days', strtotime($BG_CREATED))); 

			if(date('Y-m-d') >= $NTU_DATE){
				$data = array ('BG_STATUS' => 18);
				$where['BG_REG_NUMBER = ?'] = $row['BG_REG_NUMBER'];
				$db->update('TEMP_BANK_GUARANTEE',$data,$where);
			}
			
		}		
	}
	else
	{
		$cronResult .= "No data released";
	}

	$filename = basename(__FILE__);
	Application_Helper_General::cronLog($filename, $cronResult, 1);
?>