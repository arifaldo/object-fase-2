<?php
require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
require_once 'General/Settings.php';

$db = Zend_Db_Table::getDefaultAdapter();

function userIdle($timeout_login)
{
	$db = Zend_Db_Table::getDefaultAdapter();
	
	$userNoLogin = $db->select()
				  ->from('M_USER')
				  ->where('TIMESTAMPDIFF(day, date(USER_LASTLOGIN), date(now()) ) > ?', $timeout_login)
				  ->where('USER_STATUS != ?', '2')
				  ->query()
				  ->fetchall();
	forcelock($userNoLogin,$timeout_login); #temporary deactive
}

function forcelock($userNoLogin,$timeout_login)
{
	$db = Zend_Db_Table::getDefaultAdapter();
	
	try{
		$db->beginTransaction();
		
		$res = null;
		$string ='';
		foreach($userNoLogin as $row)
		{
			unset($where);
			$custid = $row['CUST_ID'];
			$userid = $row['USER_ID'];
			$lockreason = "User Suspended. No login after ".$timeout_login." day(s). Last login was at ".$row['USER_LASTLOGIN'];	
			$update = array('USER_ISLOGIN'  	=> '0',
							'USER_STATUS'		=> '2',
							'USER_LOCKREASON'	=> $lockreason);		
			$where[] = "CUST_ID = '".$custid."'";
			$where[] = "USER_ID = '".$userid."'";
			$where[] = "USER_STATUS != '3'";
			$where[] = "USER_STATUS != '2'";
			$res += $db->update('M_USER', $update, $where);
			
			$string .= "$custid -> $userid , "; 
			
				
		}	
		//echo "ROW EFFECTED = ".$res."OOOOOOOOOOOOOOOO";
		$db->commit();
		Application_Helper_General::cronLog('autoLockBackend', $string, '1');			   
	}
	catch(Exception $e) 
	{
		$db->rollBack();
		$string = $e->getMessage();
		Application_Helper_General::cronLog('autoLockBackend', $string, '0');		
	}
}

function autolock()
{
	$set = new Settings();
	$timeout_login = $set->getSetting('locknologinafter');
	userIdle($timeout_login);
}
autolock();
	
?>