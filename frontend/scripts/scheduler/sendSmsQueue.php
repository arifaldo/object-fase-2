<?php
require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
$db = Zend_Db_Table::getDefaultAdapter();

date_default_timezone_set('Asia/Makassar');

$time = array();
$time['start'] = date('Y-m-d H:i:s');

echo 'Start queue at '.$time['start'];
echo PHP_EOL;

$date = date_create($time['start']);
date_add($date, date_interval_create_from_date_string('5 minutes'));

$time['end'] = date_format($date, 'Y-m-d H:i:s');

//$flag = '/app/ib/frontend/scripts/scheduler/inprogressSms';
$flag	= realpath(dirname(__FILE__)).'/inprogressSms'; //path to flag file

$stringSuccess 	= 'Send SMS Queue';
$stringFailed	= 'Send SMS Queue Failed';

if (file_exists($flag)) {
	echo 'Queue inprogress at '.date('Y-m-d H:i:s');
	echo PHP_EOL;
}else{
	echo 'Estimation end queue at '.$time['end'];
	echo PHP_EOL;

	try{
		$fp = fopen($flag, 'w');
		fclose($fp);
		echo 'Create flag at: '.date('Y-m-d H:i:s');
		echo PHP_EOL;
	}catch(Exception $e){
		Application_Helper_General::cronLog(basename(__FILE__),$stringFailed,0);
		Application_Log_GeneralLog::backgroundLog(Zend_Debug::dump($e,$stringFailed,FALSE),array());

		echo 'Can\'t create flag at: '.date('Y-m-d H:i:s');
		echo PHP_EOL;
		Zend_Debug::dump($e->getMessage());
		echo PHP_EOL;
		die();
	}

	$i = 0;
	echo 'Scan Interval: ';
	while(strtotime(date('Y-m-d H:i:s')) <= strtotime($time['end'])){
		$i ++;
		echo $i.'.';

		$sms = new Service_SMS();
		$sms->sendSMS();

		sleep(1);
	}

	try{
		echo PHP_EOL;
		unlink($flag);
		echo 'Delete flag at: '.date('Y-m-d H:i:s');
		echo PHP_EOL;
	}catch(Exception $e){
		Application_Helper_General::cronLog(basename(__FILE__),$stringFailed,0);
		Application_Log_GeneralLog::backgroundLog(Zend_Debug::dump($e,$stringFailed,FALSE),array());

		echo PHP_EOL;
		echo 'Can\'t delete flag at: '.date('Y-m-d H:i:s');
		echo PHP_EOL;
		Zend_Debug::dump($e->getMessage());
		echo PHP_EOL;
		die();
	}
}

Application_Helper_General::cronLog(basename(__FILE__),$stringSuccess,1);

echo 'End queue at '.date('Y-m-d H:i:s');
echo PHP_EOL;
?>