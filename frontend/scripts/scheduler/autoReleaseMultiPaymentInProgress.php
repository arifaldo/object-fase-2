<?php
require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
require_once 'CMD/Payment.php';
$db = Zend_Db_Table::getDefaultAdapter();
$config = Zend_Registry::get('config');
$actionId = 'scheduler';
$moduleId = 'auto get response multi payment';

$select = $db->select()
	->FROM	(array('P' => 'T_PSLIP'),array('*'))
	->WHERE('PS_STATUS = 8')						
	->WHERE('SENDFILE_STATUS = 0')
	->WHERE('DATE(P.PS_EFDATE) = DATE(now())')				
	->ORDER('PS_UPDATED ASC')
// 	->limit(1)
	->limit(50)
;

// die($select);
$data =  $db->fetchAll($select);

$res = 0;
if($data){
	foreach($data as $pslip){
		$res ++;
		$trx = $db->select()
			->from(array('TT' => 'T_TRANSACTION'), array('*'))
			->where ('TT.PS_NUMBER = ?', $pslip['PS_NUMBER'])
			->joinLeft(array('TP'=>'T_PSLIP'), 'TP.PS_NUMBER = TT.PS_NUMBER', array('CUST_ID','USER_ID' => 'PS_CREATEDBY'))
			->joinLeft(array('MB'=>'M_BENEFICIARY'), 'MB.BENEFICIARY_ID = TT.BENEFICIARY_ID', array('BENEFICIARY_ADDRESS'))
			->query()->fetchAll()
		;
				
		$paymentObj 	=  new Payment($pslip['PS_NUMBER'], $pslip['CUST_ID']);
		$sendTransfer 	= $paymentObj->sendTransferMulti($pslip, $trx);
		$paymentObj->__destruct();
		unset($paymentObj);
	}
}else{
	$res = " No Multi Payment In Progress";
// 	SGO_Helper_GeneralFunction::systemLog($actionId, $moduleId, null, "No Multi Payment waiting response");
}

echo 'AFFECTED ROW(S): '.$res.PHP_EOL;
Application_Helper_General::cronLog('autoReleaseMultiPaymentInProgress', $res, '1');
?>