<?php
	require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
	require_once 'CMD/Payment.php';
	require_once 'CMD/BulkPayment.php';
	require_once 'CMD/Validate/ValidatePaymentSingle.php';
	require_once 'General/CustomerUser.php';
	require_once 'General/Customer.php';
	require_once 'General/Settings.php';
	require_once 'General/Charges.php';
	require_once 'CMD/SinglePayment.php';
	require_once 'CMD/Validate/ValidatePaymentMultiple.php';
	
	require_once 'SGO/Extendedmodule/PHPExcel.php';
require_once 'SGO/Extendedmodule/PHPExcel/IOFactory.php';



	$db = Zend_Db_Table::getDefaultAdapter();
	 
	$truecounter = 0;
	$falsecounter = 0;
	$cronResult = "";

	// INNER JOIN KE T_TRANSACTION
	// PS STATUS  7
	// EFTDATE (today)
	// PS CATEGORY (SINGLE PAYMENT)


	$flag = realpath(dirname(__FILE__)).'/inprogressBatchValidate'; //path to flag file

	$res = 'Validate Payment';
	$x = 0;

	echo 'Validate Payment Executor'.PHP_EOL;

	if (file_exists($flag)) {
		echo 'Queue inprogress at '.date('Y-m-d H:i:s').PHP_EOL;
	}else{
/*
		try{
			$fp = fopen($flag, 'w');
			@fclose($fp);
			echo 'Create flag '.$flag.' at: '.date('Y-m-d H:i:s').PHP_EOL;
		}catch(Exception $e){
			$stringFailed = 'Can\'t create flag at: '.date('Y-m-d H:i:s');
			Application_Helper_General::cronLog(basename(__FILE__),$stringFailed,0);
			echo $stringFailed.PHP_EOL;

			die();
		}
*/
		
		$selectTrx1	= $db->select()
					->from(array('P' => 'TEMP_BULKPSLIP'), array(
																	'PS_TYPE'					=> 'P.PS_TYPE',
																	'PS_FILE'					=> 'P.PS_FILE',
																	'CUST_ID'					=> 'P.CUST_ID',
																	'USER_ID'					=> 'P.UPLOADBY',
																	
																	'PS_SUBJECT'				=> 'P.PS_SUBJECT',
																	'FILE_ID'					=> 'P.FILE_ID',
																	'SOURCE_ACCOUNT'			=> 'T.SOURCE_ACCOUNT',
																	'PS_EFDATE'					=> 'P.PS_EFDATE',
																	'BS_ID'						=> 'P.BS_ID',
																	'BENEFICIARY_ACCOUNT'		=> 'T.BENEFICIARY_ACCOUNT',
																	'BENEFICIARY_ACCOUNT_CCY'	=> 'T.BENEFICIARY_ACCOUNT_CCY',
																	'BENEFICIARY_ACCOUNT_NAME'	=> 'T.BENEFICIARY_ACCOUNT_NAME',
																	'BENEFICIARY_ALIAS_NAME' 	=> 'T.BENEFICIARY_ALIAS_NAME',
																	'T.*','P.*'
																))
					->joinLeft(array('T' => 'TEMP_BULKTRANSACTION'), 'P.BS_ID = T.BS_ID', array())
					// ->order('P.VALIDATION DESC')
					->where('P.VALIDATION = ?','2')
					//->where('DATE(P.PS_CREATED) = DATE(NOW())')
					->group('P.BS_ID');
//echo $selectTrx1;die;
			$pslipTrxData1 = $db->fetchAll($selectTrx1);
			//echo '<pre>';
			//var_dump($pslipTrxData1);die;
		$setting = new Settings();
		$maxrow		= $setting->getSetting('max_row_multi_transaction');
		
		$config = Zend_Registry::get('config');
		$dateDBFormat          		= $config['date']['database']['save']['format'];
         $dateDisplayFormat     		= $config['date']['display']['format'];
		 $paymenttype   		= $config['payment']['type'];
         //$this->_dateViewFormat        		= $config['date']['view']['format'];

// 	if(empty($data)){
// 		$data = $db->select	()
// 				->FROM	(array('P' => 'T_PSLIP'),array('PS_NUMBER','CUST_ID'))
// //				->JOIN	(array('T' => 'T_TRANSACTION'),'T.PS_NUMBER = P.PS_NUMBER',array('*'))
// 				->WHERE('P.PS_STATUS = 7')						
// 				->WHERE('P.PS_TYPE = 22')						
// 				->WHERE('DATE(P.PS_EFDATE) = DATE(now())')
// 				//->WHERE('HOUR(P.PS_EFTIME) = HOUR(NOW()) ')
// 				->WHERE("P.PS_CATEGORY = 'SWEEP PAYMENT' ")
// 				// echo $data;
// 				->QUERY()->FETCHALL();
// 	}
				
				//var_dump($pslipTrxData1);
			if(!empty($pslipTrxData1))
			{
			
			

		

		//$select = $db->select()
		//				->from(array('C' => 'M_CUST_ADAPTER_PROFILE'),array('*'))
		//				->join(array('A' => 'M_ADAPTER_PROFILE'), 'C.ADAPTER_PROFILE_ID = A.PROFILE_ID', '*')
		//				->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'A.PROFILE_ID = D.PROFILE_ID', '*')
		//				->where("CUST_ID = ?",$dataTemp['CUST_ID'])
		//				->where("TRF_TYPE = ?", 'emoney');
		//$adapterDataEmoney = $db->FetchAll($select);

		
		
			//die('here');
		foreach ($pslipTrxData1 as $dataTemp) {
			
//
			if ($dataTemp['PS_TYPE'] == '32') {
				
				
				
				$PS_FILE = $dataTemp['PS_FILE'];

				$imgsrc = LIBRARY_PATH.'/data/uploads/document/temp/'.$PS_FILE;
				$file_contents = file_get_contents($imgsrc);
				
				
				$file = explode(PHP_EOL, $file_contents);
				$newFileName = $imgsrc;
				$delimitedWith = '|';
			
					$extension = 'txt';	
				
				if($file_contents){
				$data = convertFileToArray($newFileName, $extension, $delimitedWith);
				}else{
					$data = array();
				}
				
				//@unlink($newFileName);

				//else{
					
					unset($data[0]);
					unset($data[1]);
				
				//}	

				$totalRecords = count($data);
				
				//proses convert ke order yg benar
				if($totalRecords)
				{

					

					foreach ($data as $datakey => $datavalue) {

						if (!empty($headerOrder)) {
							foreach ($headerOrder as $key => $value) {

								$headerOrderArr = explode(',', $value);

								if (count($headerOrderArr) > 1) {
									$i = 0;
									$contentStr = '';
									foreach ($headerOrderArr as $key2 => $value2) {

										if ($i != count($headerOrderArr)) {
											$contentStr .= $data[$datakey][$value2].' ';
										}
										$i++;
									}	
									$newData[$datakey][] = $contentStr;
								}
								else{
									$newData[$datakey][] = $data[$datakey][$value];
								}
							}	
						}
						else{
							$newData[$datakey][] = null;
						}
					}
				}
				//var_dump($newData);
				//$mandatoryCheck = validateField($newData, $adapterDataPayroll);

				//auto mapping agar isi field file sesuai dengan yang di setup di m_mapping bo
				$fixData = $data;
				//var_dump($data);die;
				if(empty($fixData)){
					$fixData = $data;	
					$totalRecords = count($data);
				}
				
				if($totalRecords)
				{
					if($totalRecords <= $maxrow)
					{
						$rowNum = 0;
 
						$paramPayment = array( 	"CATEGORY"      	=> "BULK PAYROLL",
												"PS_TYPE"       		=> "32",
												"FROM"       		=> "I",
												"PS_NUMBER"     	=> "",
												"PS_SUBJECT"   	 	=> $dataTemp['PS_SUBJECT'],
												"PSFILEID"			=> $dataTemp['FILE_ID'],
												"PS_EFDATE"     	=> $dataTemp['PS_EFDATE'],
												"PS_FILE"     		=> $dataTemp['PS_FILE'],
												"_dateFormat"    	=> $dateDBFormat,
												"_dateDBFormat"    	=> $dateDBFormat,
												"_addBeneficiary"   => true, // privi BADA (Add Beneficiary)
												"_beneLinkage"    	=> false, // privi BLBU (Linkage Beneficiary User)
												"_createPB"     	=> true, // privi CBPW (Create Bulk Credit Payment PB by Import File (CSV))
												"_createDOM"    	=> true, // privi CBPI (Create Bulk Credit Payment Domestic by Import File (CSV))
												"_createREM"    	=> false,        // cannot create REM trx
											  );

						$paramTrxArr = array();
						 //Zend_Debug::dump($fixData); die;

						foreach ( $fixData as $row )
						{
							// if(count($row)==4)
							// {
								 //var_dump($row);die;
								$rowNum++;
								if (!empty($row) ) {
									$row = explode('|', $row[0]);
									//var_dump($row);die;
									$benefAcct 		= trim($row[1]);
	// 											$benefName 		= trim($row[1]);
									$ccy 			= "IDR";
									$amount 		= trim($row[2]);
									$message 		= trim($row[3]);
									$reference 		= '';
									$sms_notif 		= '';
									$email_notif 	= '';
									$addMessage 	= '';
								}
								
// 											$email 			= trim($row[6]);
// 											$phoneNumber	= trim($row[7]);
								$type 			= 'PB';
// 											$bankCode 		= trim($row[6]);
								//$bankName 		= trim($row[10]);
//										$bankCity = trim($row[10]);
// 											$benefAdd		= trim($row[9]);
// 											$citizenship	= strtoupper(trim($row[10]));
								//$resident = strtoupper(trim($row[11]));

								/*
								 * Change parameter into document
								 */
								$fullDesc = array(
									'BENEFICIARY_ACCOUNT' 		=> $benefAcct,
									'BENEFICIARY_NAME' 			=> '',
									'BENEFICIARY_ACCOUNT_CCY' 	=> $ccy,
									'TRA_AMOUNT' 				=> $amount,
									'TRA_MESSAGE'				=> $message,
									'REFNO' 					=> $addMessage,
									'SMS_NOTIF'					=> $sms_notif,
									'EMAIL_NOTIF' 				=> $email_notif,
									'BENEFICIARY_EMAIL' 		=> '',
									'BENEFICIARY_MOBILE_PHONE_NUMBER' => '',
									'TRANSFER_TYPE' 			=> 'PB',
									//'CLR_CODE' 					=> $bankCode,
									//'BENEFICIARY_BANK_NAME' 	=> $bankName,
//											'BENEFICIARY_CITY' => $bankCity,
									'BENEFICIARY_ADDRESS'		=> '',
									'BENEFICIARY_CITIZENSHIP' 	=> ''
									//'BENEFICIARY_RESIDENT' => $resident
								);

								$filter = new Application_Filtering();

								$SMS_NOTIF 		= $filter->filter($sms_notif, "SMS");
								$EMAIL_NOTIF 		= $filter->filter($email_notif, "EMAIL");

								$TRA_AMOUNT 		= $filter->filter($amount, "AMOUNT");
								$TRA_MESSAGE 		= $filter->filter($message, "TRA_MESSAGE");
								$TRA_REFNO 			= $filter->filter($addMessage, "TRA_REFNO");
								$ACCTSRC 			= $filter->filter($benefAcct, "ACCOUNT_NO");
								$ACBENEF_BANKNAME 	= $filter->filter('', "ACCOUNT_NAME");
								$ACBENEF_ALIAS 		= $filter->filter('', "ACCOUNT_ALIAS");
								$ACBENEF_EMAIL 		= $filter->filter('', "EMAIL");
								$ACBENEF_PHONE 		= $filter->filter('', "MOBILE_PHONE_NUMBER");
								$ACBENEF_CCY 		= $filter->filter($ccy, "SELECTION");
								$ACBENEF_ADDRESS	= $filter->filter('', "ADDRESS");
								$ACBENEF_CITIZENSHIP= $filter->filter('', "SELECTION");
								//$ACBENEF_RESIDENT= $filter->filter($resident, "SELECTION");
								//$BANK_NAME			= $filter->filter($bankName, "BANK_NAME");
								//$BANK_CITY			= $filter->filter($bankCity, "ADDRESS");
								//$CLR_CODE			= $filter->filter($bankCode, "BANK_CODE");
								$TRANSFER_TYPE 		= $filter->filter($type, "SELECTION");

								$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

								if($TRANSFER_TYPE == 1){
									$TRANSFER_TYPE = 'RTGS';
								}else if($TRANSFER_TYPE == 2){
									$TRANSFER_TYPE = 'SKN';
								}else if($TRANSFER_TYPE == 5){
									$TRANSFER_TYPE = 'ONLINE';
								}else if($TRANSFER_TYPE == '0'){
									$TRANSFER_TYPE = 'PB';
								}
								//else{
								//	$TRANSFER_TYPE = $params['TRANSFER_TYPE']; 
								//}
									
								$selectcust = $db->select()
													->from('M_CUSTOMER',array('CUST_CHARGESID'))
													->where("CUST_ID = ?",$dataTemp['CUST_ID']);
									$custdata = $db->fetchrow($selectcust);
									$chrgdatainhouse = array();
									if(!empty($custdata['CUST_CHARGESID'])){
											$selectchrginhouse = $db->select()
													->from('M_CHARGES_WITHIN',array('CUST_ID','AMOUNT'))
													->where("CHARGES_ID = ?",$custdata['CUST_CHARGESID']);
											$chrgdatainhouse = $db->fetchrow($selectchrginhouse);
											
											
									}else{
										$chrgdatainhouse['CUST_ID'] = 'GLOBAL';
									}
									

								
									$chargeAmt = '0';
									//$param['TRANSFER_FEE'] = $chargeAmt2;
								

								$filter->__destruct();
								unset($filter);

								$paramTrx = array("TRANSFER_TYPE" 			=> $TRANSFER_TYPE,
													"TRA_AMOUNT" 				=> $TRA_AMOUNT_num,
													"TRANSFER_FEE" 				=> $chargeAmt,
													"TRA_MESSAGE" 				=> $TRA_MESSAGE,
													"TRA_REFNO" 				=> $TRA_REFNO,
													"ACCTSRC" 					=> $ACCTSRC,
													"ACBENEF" 					=> $dataTemp['BENEFICIARY_ACCOUNT'],
													"ACBENEF_CCY" 				=> $ACBENEF_CCY,
													"ACBENEF_EMAIL" 			=> $ACBENEF_EMAIL,
													"ACBENEF_PHONE" 			=> $ACBENEF_PHONE,
													"SMS_NOTIF"					=> $SMS_NOTIF,
													"EMAIL_NOTIF"				=> $EMAIL_NOTIF,
												// for Beneficiary data, except (bene CCY and email), must be passed by reference
													"ACBENEF_BANKNAME" 			=> $ACBENEF_BANKNAME,
													"ACBENEF_ALIAS" 			=> $ACBENEF_ALIAS,
													"ACBENEF_CITIZENSHIP" 		=> $ACBENEF_CITIZENSHIP,		// W: WNI, N: WNA
												//	"ACBENEF_RESIDENT" 			=> $ACBENEF_RESIDENT,		// R: RESIDENT, NR: NON-RESIDENT
													"ACBENEF_ADDRESS1" 			=> $ACBENEF_ADDRESS,
													"REFERENCE"					=> $reference,
												//	"ACBENEF_ADDRESS2" 			=> &$ACBENEF_ADDRESS2,
												//	"ACBENEF_ADDRESS3" 			=> &$ACBENEF_ADDRESS3,

												//	"ORG_DIR" 					=> $ORG_DIR,
													//"BANK_CODE" 				=> $CLR_CODE,
												//	"BANK_NAME" 				=> $BANK_NAME,
												//	"BANK_BRANCH" 				=> $BANK_BRANCH,
												//	"BANK_ADDRESS1" 			=> $BANK_ADDRESS1,
												//	"BANK_ADDRESS2" 			=> $BANK_ADDRESS2,
												//	"BANK_ADDRESS3" 			=> $BANK_ADDRESS3,
												 );

								array_push($paramTrxArr,$paramTrx);
						}
					}
					// kalo jumlah trx lebih dari setting
					// else
					// {
					// 	$error_msg[] = 'Error: The number of rows to be imported should not more than '.$maxrow.'.';
					// 	$this->view->error 		= true;
					// 	$this->view->report_msg	= $this->displayError($error_msg);
					// }

					//$confirm = true;

					//kalo gak ada error
					// if(!empty($error_msg))
					// {
						//echo '<pre>';
						$validate   = new ValidatePaymentSingle($dataTemp['CUST_ID'], $dataTemp['USER_ID']);
						//var_dump($paramTrxArr);
						//var_dump($paramPayment);die;
						$resWs = array();
						$resultVal	= $validate->checkCreateTopup($paramPayment, $paramTrxArr, $resWs);
						$payment 		= $validate->getPaymentInfo();
						
						$pstype = $dataTemp['PS_TYPE'];
						$bsid   = $dataTemp['BS_ID'];

						if($validate->isError() === false)	// payment data is valid
						{
							//die('tere');
							$confirm = true;
							$validate->__destruct();
							unset($validate);
						}
						else
						{
							
							$errorMsg 		= $validate->getErrorMsg();
							//var_dump($errorMsg);
							$errorTrxMsg 	= $validate->getErrorTrxMsg();	// array
							//var_dump($errorTrxMsg);
							//die('tere1');
							

							foreach($errorTrxMsg as $key)
							{
								foreach($key as $ccy)
								{
									foreach ($ccy as $row => $val) {
										
										$trxvalidasi	= $db->select()
														->from(array('P' => 'TEMP_BULKTRANSACTION'), array('P.*'))
														// ->joinLeft(array('T' => 'TEMP_BULKTRANSACTION'), 'P.BS_ID = T.BS_ID', array('T.*'))
														// ->order('P.VALIDATION DESC')
														->where('P.BS_ID = ?',$bsid);
														//->where('DATE(P.PS_CREATED) = DATE(NOW())');

										$transactionvalidasi = $db->fetchAll($trxvalidasi);

										$sourceAccountFailed = $transactionvalidasi[$row]['TRANSACTION_ID'];
										
										$validated = array(	
											'VALIDATED'			=> 0,
											'ERROR_DESC'		=> $val,
										   );

										$where = array(
														'BS_ID = ?' => $bsid,
														'TRANSACTION_ID = ?' => $sourceAccountFailed);
														//var_dump($validated);
														//var_dump($where);
										$db->update('TEMP_BULKTRANSACTION', $validated, $where);

									}
								}
							}

							$validate->__destruct();

							unset($validate);
							// echo "<pre>";
							// print_r($errorMsg);die;
							// Zend_Debug::dump($validate);die;
							// if($errorMsg)
							// {
							// 	$error_msg[] = 'Error: '.$errorMsg;
							// 	$this->view->error 		= true;
							// 	$this->view->report_msg	= $this->displayError($error_msg);
							// }
							// else
							// {
							$confirm = true;
							//}
						}

						// die;
					// }
				}
				// else //kalo total record = 0
				// {
				// 	$error_msg[] = $this->language->_('Error').': '.$this->language->_('Wrong File Formatss').'.';
				// 	$this->view->error 		= true;
				// 	$this->view->report_msg	= $this->displayError($error_msg);
				// }
			//akhir
			}

			if ($dataTemp['PS_TYPE'] == '31') {
			//awal
			
				/*$select = $db->select()
						->from(array('C' => 'M_CUST_ADAPTER_PROFILE'),array('*'))
						->join(array('A' => 'M_ADAPTER_PROFILE'), 'C.ADAPTER_PROFILE_ID = A.PROFILE_ID', '*')
						->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'A.PROFILE_ID = D.PROFILE_ID', '*')
						->where("CUST_ID = ?",$dataTemp['CUST_ID'])
						->where("TRF_TYPE = ?", 'payroll');
				$adapterDataPayroll = $db->FetchAll($select);
				*/
				$PS_FILE = $dataTemp['PS_FILE'];

				$imgsrc = LIBRARY_PATH.'/data/uploads/document/temp/'.$PS_FILE;
				$file_contents = file_get_contents($imgsrc);
				
				
				$file = explode(PHP_EOL, $file_contents);
				$newFileName = $imgsrc;
				$delimitedWith = '|';
				//$extension = 'txt';
			/*	$delimitedWith = '|';
				$fileName = $adapterDataPayroll[0]['FILE_PATH'];
		        $fixLength = $adapterDataPayroll[0]['FIXLENGTH'];
		        $fixLengthType = $adapterDataPayroll[0]['FIXLENGTH_TYPE'];
		        $fixLengthHeader = $adapterDataPayroll[0]['FIXLENGTH_HEADER_ORDER'];
		        $fixLengthHeaderName = $adapterDataPayroll[0]['FIXLENGTH_HEADER_NAME'];
		        $fixLengthContent = $adapterDataPayroll[0]['FIXLENGTH_CONTENT_ORDER'];
				$newFileName = $imgsrc;
			*///	
			//	if (!empty($adapterDataPayroll)) {
			//		$extension = $adapterDataPayroll[0]['FILE_FORMAT'];
			//	}
				//else{
					$extension = 'txt';	
				//}
				  
				if($file_contents){
				$data = convertFileToArray($newFileName, $extension, $delimitedWith);
				}else{
					$data = array();
				}
				
				//@unlink($newFileName);

				//else{
					
					unset($data[0]);
					unset($data[1]);
				
				//}	

				$totalRecords = count($data);
				
				//proses convert ke order yg benar
				if($totalRecords)
				{

					

					foreach ($data as $datakey => $datavalue) {

						if (!empty($headerOrder)) {
							foreach ($headerOrder as $key => $value) {

								$headerOrderArr = explode(',', $value);

								if (count($headerOrderArr) > 1) {
									$i = 0;
									$contentStr = '';
									foreach ($headerOrderArr as $key2 => $value2) {

										if ($i != count($headerOrderArr)) {
											$contentStr .= $data[$datakey][$value2].' ';
										}
										$i++;
									}	
									$newData[$datakey][] = $contentStr;
								}
								else{
									$newData[$datakey][] = $data[$datakey][$value];
								}
							}	
						}
						else{
							$newData[$datakey][] = null;
						}
					}
				}
				//var_dump($newData);
				//$mandatoryCheck = validateField($newData, $adapterDataPayroll);

				//auto mapping agar isi field file sesuai dengan yang di setup di m_mapping bo
				$fixData = $data;
				//var_dump($data);die;
				if(empty($fixData)){
					$fixData = $data;	
					$totalRecords = count($data);
				}
				
				if($totalRecords)
				{
					if($totalRecords <= $maxrow)
					{
						$rowNum = 0;
 
						$paramPayment = array( 	"CATEGORY"      	=> "BULK PAYROLL",
												"FROM"       		=> "I",
												"PS_NUMBER"     	=> "",
												"PS_SUBJECT"   	 	=> $dataTemp['PS_SUBJECT'],
												"PSFILEID"			=> $dataTemp['FILE_ID'],
												"PS_EFDATE"     	=> $dataTemp['PS_EFDATE'],
												"PS_FILE"     		=> $dataTemp['PS_FILE'],
												"_dateFormat"    	=> $dateDBFormat,
												"_dateDBFormat"    	=> $dateDBFormat,
												"_addBeneficiary"   => true, // privi BADA (Add Beneficiary)
												"_beneLinkage"    	=> false, // privi BLBU (Linkage Beneficiary User)
												"_createPB"     	=> true, // privi CBPW (Create Bulk Credit Payment PB by Import File (CSV))
												"_createDOM"    	=> true, // privi CBPI (Create Bulk Credit Payment Domestic by Import File (CSV))
												"_createREM"    	=> false,        // cannot create REM trx
											  );

						$paramTrxArr = array();
						 //Zend_Debug::dump($fixData); die;

						foreach ( $fixData as $row )
						{
							// if(count($row)==4)
							// {
								 //var_dump($row);die;
								$rowNum++;
								if (!empty($row) ) {
									$row = explode('|', $row[0]);
									//var_dump($row);die;
									$benefAcct 		= trim($row[2]);
	// 											$benefName 		= trim($row[1]);
									$ccy 			= "IDR";
									$amount 		= trim($row[3]);
									$message 		= trim($row[4]);
									$reference 		= '';
									$sms_notif 		= '';
									$email_notif 	= '';
									$addMessage 	= '';
								}
								
// 											$email 			= trim($row[6]);
// 											$phoneNumber	= trim($row[7]);
								$type 			= 'PB';
// 											$bankCode 		= trim($row[6]);
								//$bankName 		= trim($row[10]);
//										$bankCity = trim($row[10]);
// 											$benefAdd		= trim($row[9]);
// 											$citizenship	= strtoupper(trim($row[10]));
								//$resident = strtoupper(trim($row[11]));

								/*
								 * Change parameter into document
								 */
								$fullDesc = array(
									'BENEFICIARY_ACCOUNT' 		=> $benefAcct,
									'BENEFICIARY_NAME' 			=> '',
									'BENEFICIARY_ACCOUNT_CCY' 	=> $ccy,
									'TRA_AMOUNT' 				=> $amount,
									'TRA_MESSAGE'				=> $message,
									'REFNO' 					=> $addMessage,
									'SMS_NOTIF'					=> $sms_notif,
									'EMAIL_NOTIF' 				=> $email_notif,
									'BENEFICIARY_EMAIL' 		=> '',
									'BENEFICIARY_MOBILE_PHONE_NUMBER' => '',
									'TRANSFER_TYPE' 			=> 'PB',
									//'CLR_CODE' 					=> $bankCode,
									//'BENEFICIARY_BANK_NAME' 	=> $bankName,
//											'BENEFICIARY_CITY' => $bankCity,
									'BENEFICIARY_ADDRESS'		=> '',
									'BENEFICIARY_CITIZENSHIP' 	=> ''
									//'BENEFICIARY_RESIDENT' => $resident
								);

								$filter = new Application_Filtering();

								$SMS_NOTIF 		= $filter->filter($sms_notif, "SMS");
								$EMAIL_NOTIF 		= $filter->filter($email_notif, "EMAIL");

								$TRA_AMOUNT 		= $filter->filter($amount, "AMOUNT");
								$TRA_MESSAGE 		= $filter->filter($message, "TRA_MESSAGE");
								$TRA_REFNO 			= $filter->filter($addMessage, "TRA_REFNO");
								$ACBENEF 			= $filter->filter($benefAcct, "ACCOUNT_NO");
								$ACBENEF_BANKNAME 	= $filter->filter('', "ACCOUNT_NAME");
								$ACBENEF_ALIAS 		= $filter->filter('', "ACCOUNT_ALIAS");
								$ACBENEF_EMAIL 		= $filter->filter('', "EMAIL");
								$ACBENEF_PHONE 		= $filter->filter('', "MOBILE_PHONE_NUMBER");
								$ACBENEF_CCY 		= $filter->filter($ccy, "SELECTION");
								$ACBENEF_ADDRESS	= $filter->filter('', "ADDRESS");
								$ACBENEF_CITIZENSHIP= $filter->filter('', "SELECTION");
								//$ACBENEF_RESIDENT= $filter->filter($resident, "SELECTION");
								//$BANK_NAME			= $filter->filter($bankName, "BANK_NAME");
								//$BANK_CITY			= $filter->filter($bankCity, "ADDRESS");
								//$CLR_CODE			= $filter->filter($bankCode, "BANK_CODE");
								$TRANSFER_TYPE 		= $filter->filter($type, "SELECTION");

								$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

								if($TRANSFER_TYPE == 1){
									$TRANSFER_TYPE = 'RTGS';
								}else if($TRANSFER_TYPE == 2){
									$TRANSFER_TYPE = 'SKN';
								}else if($TRANSFER_TYPE == 5){
									$TRANSFER_TYPE = 'ONLINE';
								}else if($TRANSFER_TYPE == '0'){
									$TRANSFER_TYPE = 'PB';
								}
								//else{
								//	$TRANSFER_TYPE = $params['TRANSFER_TYPE']; 
								//}
									
								$selectcust = $db->select()
													->from('M_CUSTOMER',array('CUST_CHARGESID'))
													->where("CUST_ID = ?",$dataTemp['CUST_ID']);
									$custdata = $db->fetchrow($selectcust);
									$chrgdatainhouse = array();
									if(!empty($custdata['CUST_CHARGESID'])){
											$selectchrginhouse = $db->select()
													->from('M_CHARGES_WITHIN',array('CUST_ID','AMOUNT'))
													->where("CHARGES_ID = ?",$custdata['CUST_CHARGESID']);
											$chrgdatainhouse = $db->fetchrow($selectchrginhouse);
											
											
									}else{
										$chrgdatainhouse['CUST_ID'] = 'GLOBAL';
									}
									

								
									$chargeAmt = '0';
									//$param['TRANSFER_FEE'] = $chargeAmt2;
								

								$filter->__destruct();
								unset($filter);

								$paramTrx = array("TRANSFER_TYPE" 			=> $TRANSFER_TYPE,
													"TRA_AMOUNT" 				=> $TRA_AMOUNT_num,
													"TRANSFER_FEE" 				=> $chargeAmt,
													"TRA_MESSAGE" 				=> $TRA_MESSAGE,
													"TRA_REFNO" 				=> $TRA_REFNO,
													"ACCTSRC" 					=> $dataTemp['SOURCE_ACCOUNT'],
													"ACBENEF" 					=> $ACBENEF,
													"ACBENEF_CCY" 				=> $ACBENEF_CCY,
													"ACBENEF_EMAIL" 			=> $ACBENEF_EMAIL,
													"ACBENEF_PHONE" 			=> $ACBENEF_PHONE,
													"SMS_NOTIF"					=> $SMS_NOTIF,
													"EMAIL_NOTIF"				=> $EMAIL_NOTIF,
												// for Beneficiary data, except (bene CCY and email), must be passed by reference
													"ACBENEF_BANKNAME" 			=> $ACBENEF_BANKNAME,
													"ACBENEF_ALIAS" 			=> $ACBENEF_ALIAS,
													"ACBENEF_CITIZENSHIP" 		=> $ACBENEF_CITIZENSHIP,		// W: WNI, N: WNA
												//	"ACBENEF_RESIDENT" 			=> $ACBENEF_RESIDENT,		// R: RESIDENT, NR: NON-RESIDENT
													"ACBENEF_ADDRESS1" 			=> $ACBENEF_ADDRESS,
													"REFERENCE"					=> $reference,
												//	"ACBENEF_ADDRESS2" 			=> &$ACBENEF_ADDRESS2,
												//	"ACBENEF_ADDRESS3" 			=> &$ACBENEF_ADDRESS3,

												//	"ORG_DIR" 					=> $ORG_DIR,
													//"BANK_CODE" 				=> $CLR_CODE,
												//	"BANK_NAME" 				=> $BANK_NAME,
												//	"BANK_BRANCH" 				=> $BANK_BRANCH,
												//	"BANK_ADDRESS1" 			=> $BANK_ADDRESS1,
												//	"BANK_ADDRESS2" 			=> $BANK_ADDRESS2,
												//	"BANK_ADDRESS3" 			=> $BANK_ADDRESS3,
												 );

								array_push($paramTrxArr,$paramTrx);
						}
					}
					// kalo jumlah trx lebih dari setting
					// else
					// {
					// 	$error_msg[] = 'Error: The number of rows to be imported should not more than '.$maxrow.'.';
					// 	$this->view->error 		= true;
					// 	$this->view->report_msg	= $this->displayError($error_msg);
					// }

					//$confirm = true;

					//kalo gak ada error
					// if(!empty($error_msg))
					// {
						//echo '<pre>';
						$validate   = new ValidatePaymentSingle($dataTemp['CUST_ID'], $dataTemp['USER_ID']);
						//var_dump($paramTrxArr);
						//var_dump($paramPayment);die;
						$resWs = array();
						$resultVal	= $validate->checkCreateTopup($paramPayment, $paramTrxArr, $resWs);
						$payment 		= $validate->getPaymentInfo();
						
						$pstype = $dataTemp['PS_TYPE'];
						$bsid   = $dataTemp['BS_ID'];

						if($validate->isError() === false)	// payment data is valid
						{
							//die('tere');
							$confirm = true;
							$validate->__destruct();
							unset($validate);
						}
						else
						{
							
							$errorMsg 		= $validate->getErrorMsg();
							//var_dump($errorMsg);
							$errorTrxMsg 	= $validate->getErrorTrxMsg();	// array
							//var_dump($errorTrxMsg);
							//die('tere1');
							

							foreach($errorTrxMsg as $key)
							{
								foreach($key as $ccy)
								{
									foreach ($ccy as $row => $val) {
										
										$trxvalidasi	= $db->select()
														->from(array('P' => 'TEMP_BULKTRANSACTION'), array('P.*'))
														// ->joinLeft(array('T' => 'TEMP_BULKTRANSACTION'), 'P.BS_ID = T.BS_ID', array('T.*'))
														// ->order('P.VALIDATION DESC')
														->where('P.BS_ID = ?',$bsid);
														//->where('DATE(P.PS_CREATED) = DATE(NOW())');

										$transactionvalidasi = $db->fetchAll($trxvalidasi);

										$sourceAccountFailed = $transactionvalidasi[$row]['TRANSACTION_ID'];
										
										$validated = array(	
											'VALIDATED'			=> 0,
											'ERROR_DESC'		=> $val,
										   );

										$where = array(
														'BS_ID = ?' => $bsid,
														'TRANSACTION_ID = ?' => $sourceAccountFailed);
														//var_dump($validated);
														//var_dump($where);
										$db->update('TEMP_BULKTRANSACTION', $validated, $where);

									}
								}
							}

							$validate->__destruct();

							unset($validate);
							// echo "<pre>";
							// print_r($errorMsg);die;
							// Zend_Debug::dump($validate);die;
							// if($errorMsg)
							// {
							// 	$error_msg[] = 'Error: '.$errorMsg;
							// 	$this->view->error 		= true;
							// 	$this->view->report_msg	= $this->displayError($error_msg);
							// }
							// else
							// {
							$confirm = true;
							//}
						}

						// die;
					// }
				}
				// else //kalo total record = 0
				// {
				// 	$error_msg[] = $this->language->_('Error').': '.$this->language->_('Wrong File Formatss').'.';
				// 	$this->view->error 		= true;
				// 	$this->view->report_msg	= $this->displayError($error_msg);
				// }
			//akhir
			}

//var_dump($dataTemp['PS_TYPE']);die;
			if ($dataTemp['PS_TYPE'] == '11') {
			//awal
				$select = $db->select()
						->from(array('C' => 'M_CUST_ADAPTER_PROFILE'),array('*'))
						->join(array('A' => 'M_ADAPTER_PROFILE'), 'C.ADAPTER_PROFILE_ID = A.PROFILE_ID', '*')
						->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'A.PROFILE_ID = D.PROFILE_ID', '*')
						->where("CUST_ID = ?",$dataTemp['CUST_ID'])
						->where("TRF_TYPE = ?", 'payroll');
				$adapterDataPayroll = $db->FetchAll($select);
				
				$PS_FILE = $dataTemp['PS_FILE'];

				$imgsrc = LIBRARY_PATH.'/data/uploads/document/temp/'.$PS_FILE;
				$file_contents = file_get_contents($imgsrc);
				
				
				$file = explode(PHP_EOL, $file_contents);
				//$extension = 'txt';
				$delimitedWith = '|';
				$fileName = $adapterDataPayroll[0]['FILE_PATH'];
		        $fixLength = $adapterDataPayroll[0]['FIXLENGTH'];
		        $fixLengthType = $adapterDataPayroll[0]['FIXLENGTH_TYPE'];
		        $fixLengthHeader = $adapterDataPayroll[0]['FIXLENGTH_HEADER_ORDER'];
		        $fixLengthHeaderName = $adapterDataPayroll[0]['FIXLENGTH_HEADER_NAME'];
		        $fixLengthContent = $adapterDataPayroll[0]['FIXLENGTH_CONTENT_ORDER'];
				$newFileName = $imgsrc;
				
				if (!empty($adapterDataPayroll)) {
					$extension = $adapterDataPayroll[0]['FILE_FORMAT'];
				}
				else{
					$extension = 'txt';	
				}
				  
				if($file_contents){
				$data = convertFileToArray($newFileName, $extension, $delimitedWith);
				}else{
					$data = array();
				}
				
				//@unlink($newFileName);

				if (!empty($adapterDataPayroll)) {
					//unset header if not fixlength
					if ($fixLength != 1) {
						unset($data[0]);
						//unset($data[1]);	
					}
				}
				else{
					
					unset($data[0]);
					unset($data[1]);
				
				}	

				$totalRecords = count($data);
				
				//proses convert ke order yg benar
				if($totalRecords)
				{

					foreach ($adapterDataPayroll as $key => $value) {
						$headerOrder[] = $value['HEADER_CONTENT'];
					}

					foreach ($data as $datakey => $datavalue) {

						if (!empty($headerOrder)) {
							foreach ($headerOrder as $key => $value) {

								$headerOrderArr = explode(',', $value);

								if (count($headerOrderArr) > 1) {
									$i = 0;
									$contentStr = '';
									foreach ($headerOrderArr as $key2 => $value2) {

										if ($i != count($headerOrderArr)) {
											$contentStr .= $data[$datakey][$value2].' ';
										}
										$i++;
									}	
									$newData[$datakey][] = $contentStr;
								}
								else{
									$newData[$datakey][] = $data[$datakey][$value];
								}
							}	
						}
						else{
							$newData[$datakey][] = null;
						}
					}
				}
				//var_dump($newData);
				$mandatoryCheck = validateField($newData, $adapterDataPayroll);

				//auto mapping agar isi field file sesuai dengan yang di setup di m_mapping bo
				$fixData = autoMapping($newData, $adapterDataPayroll);
				//var_dump($fixData);die;
				if(empty($fixData)){
					$fixData = $data;	
					$totalRecords = count($data);
				}
				
				if($totalRecords)
				{
					if($totalRecords <= $maxrow)
					{
						$rowNum = 0;
 
						$paramPayment = array( 	"CATEGORY"      	=> "BULK PAYROLL",
												"FROM"       		=> "I",
												"PS_NUMBER"     	=> "",
												"PS_SUBJECT"   	 	=> $dataTemp['PS_SUBJECT'],
												"PSFILEID"			=> $dataTemp['FILE_ID'],
												"PS_EFDATE"     	=> $dataTemp['PS_EFDATE'],
												"PS_FILE"     		=> $dataTemp['PS_FILE'],
												"_dateFormat"    	=> $dateDBFormat,
												"_dateDBFormat"    	=> $dateDBFormat,
												"_addBeneficiary"   => true, // privi BADA (Add Beneficiary)
												"_beneLinkage"    	=> false, // privi BLBU (Linkage Beneficiary User)
												"_createPB"     	=> true, // privi CBPW (Create Bulk Credit Payment PB by Import File (CSV))
												"_createDOM"    	=> true, // privi CBPI (Create Bulk Credit Payment Domestic by Import File (CSV))
												"_createREM"    	=> false,        // cannot create REM trx
											  );

						$paramTrxArr = array();
						 //Zend_Debug::dump($fixData); die;

						foreach ( $fixData as $row )
						{
							// if(count($row)==4)
							// {
								 //var_dump($row);die;
								$rowNum++;
								if (!empty($row) && empty($adapterDataPayroll)) {
									$row = explode('|', $row[0]);
									//var_dump($row);die;
									$benefAcct 		= trim($row[1]);
	// 											$benefName 		= trim($row[1]);
									$ccy 			= "IDR";
									$amount 		= trim($row[2]);
									$message 		= trim($row[3]);
									$reference 		= '';
									$sms_notif 		= '';
									$email_notif 	= '';
									$addMessage 	= trim($row[4]);
								}else{
									$benefAcct 		= trim($row[0]);
// 											$benefName 		= trim($row[1]);
									$ccy 			= "IDR";
									$amount 		= trim($row[1]);
									$message 		= trim($row[2]);
									$reference 		= '';
									$sms_notif 		= '';
									$email_notif 	= '';
									$addMessage 	= trim($row[3]);
								}
								
// 											$email 			= trim($row[6]);
// 											$phoneNumber	= trim($row[7]);
								$type 			= 'PB';
// 											$bankCode 		= trim($row[6]);
								//$bankName 		= trim($row[10]);
//										$bankCity = trim($row[10]);
// 											$benefAdd		= trim($row[9]);
// 											$citizenship	= strtoupper(trim($row[10]));
								//$resident = strtoupper(trim($row[11]));

								/*
								 * Change parameter into document
								 */
								$fullDesc = array(
									'BENEFICIARY_ACCOUNT' 		=> $benefAcct,
									'BENEFICIARY_NAME' 			=> '',
									'BENEFICIARY_ACCOUNT_CCY' 	=> $ccy,
									'TRA_AMOUNT' 				=> $amount,
									'TRA_MESSAGE'				=> $message,
									'REFNO' 					=> $addMessage,
									'SMS_NOTIF'					=> $sms_notif,
									'EMAIL_NOTIF' 				=> $email_notif,
									'BENEFICIARY_EMAIL' 		=> '',
									'BENEFICIARY_MOBILE_PHONE_NUMBER' => '',
									'TRANSFER_TYPE' 			=> 'PB',
									//'CLR_CODE' 					=> $bankCode,
									//'BENEFICIARY_BANK_NAME' 	=> $bankName,
//											'BENEFICIARY_CITY' => $bankCity,
									'BENEFICIARY_ADDRESS'		=> '',
									'BENEFICIARY_CITIZENSHIP' 	=> ''
									//'BENEFICIARY_RESIDENT' => $resident
								);

								$filter = new Application_Filtering();

								$SMS_NOTIF 		= $filter->filter($sms_notif, "SMS");
								$EMAIL_NOTIF 		= $filter->filter($email_notif, "EMAIL");

								$TRA_AMOUNT 		= $filter->filter($amount, "AMOUNT");
								$TRA_MESSAGE 		= $filter->filter($message, "TRA_MESSAGE");
								$TRA_REFNO 			= $filter->filter($addMessage, "TRA_REFNO");
								$ACBENEF 			= $filter->filter($benefAcct, "ACCOUNT_NO");
								$ACBENEF_BANKNAME 	= $filter->filter('', "ACCOUNT_NAME");
								$ACBENEF_ALIAS 		= $filter->filter('', "ACCOUNT_ALIAS");
								$ACBENEF_EMAIL 		= $filter->filter('', "EMAIL");
								$ACBENEF_PHONE 		= $filter->filter('', "MOBILE_PHONE_NUMBER");
								$ACBENEF_CCY 		= $filter->filter($ccy, "SELECTION");
								$ACBENEF_ADDRESS	= $filter->filter('', "ADDRESS");
								$ACBENEF_CITIZENSHIP= $filter->filter('', "SELECTION");
								//$ACBENEF_RESIDENT= $filter->filter($resident, "SELECTION");
								//$BANK_NAME			= $filter->filter($bankName, "BANK_NAME");
								//$BANK_CITY			= $filter->filter($bankCity, "ADDRESS");
								//$CLR_CODE			= $filter->filter($bankCode, "BANK_CODE");
								$TRANSFER_TYPE 		= $filter->filter($type, "SELECTION");

								$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

								if($TRANSFER_TYPE == 1){
									$TRANSFER_TYPE = 'RTGS';
								}else if($TRANSFER_TYPE == 2){
									$TRANSFER_TYPE = 'SKN';
								}else if($TRANSFER_TYPE == 5){
									$TRANSFER_TYPE = 'ONLINE';
								}else if($TRANSFER_TYPE == '0'){
									$TRANSFER_TYPE = 'PB';
								}
								//else{
								//	$TRANSFER_TYPE = $params['TRANSFER_TYPE']; 
								//}
									
								$selectcust = $db->select()
													->from('M_CUSTOMER',array('CUST_CHARGESID'))
													->where("CUST_ID = ?",$dataTemp['CUST_ID']);
									$custdata = $db->fetchrow($selectcust);
									$chrgdatainhouse = array();
									if(!empty($custdata)){
											$selectchrginhouse = $db->select()
													->from('M_CHARGES_WITHIN',array('CUST_ID','AMOUNT'))
													->where("CHARGES_ID = ?",$custdata['CUST_CHARGESID']);
											$chrgdatainhouse = $db->fetchrow($selectchrginhouse);
											
											
									}else{
										$chrgdatainhouse['CUST_ID'] = 'GLOBAL';
									}
									

								if($TRANSFER_TYPE == 'RTGS'){
									$chargeType = '1';
									$select = $db->select()
													->from('M_CHARGES_OTHER',array('*'))
													->where("CUST_ID = ?",$chrgdatainhouse['CUST_ID'])
													->where("CHARGES_TYPE = ?",$chargeType);
									$resultSelecet = $db->FetchAll($select);
									$chargeAmt = $resultSelecet['0']['CHARGES_AMT'];

									//$param['TRANSFER_FEE'] = $chargeAmt;
								}
								else if($TRANSFER_TYPE == 'SKN'){
									$chargeType1 = '2';
									$select1 = $db->select()
													->from('M_CHARGES_OTHER',array('*'))
													->where("CUST_ID = ?",$chrgdatainhouse['CUST_ID'])
													->where("CHARGES_TYPE = ?",$chargeType1);
									$resultSelecet1 = $db->FetchAll($select1);
									$chargeAmt = $resultSelecet1['0']['CHARGES_AMT'];

									//$param['TRANSFER_FEE'] = $chargeAmt1;
								}else if($TRANSFER_TYPE == 'ONLINE'){
									$chargeType1 = '8';
									$select1 = $db->select()
													->from('M_CHARGES_OTHER',array('*'))
													->where("CUST_ID = ?",$chrgdatainhouse['CUST_ID'])
													->where("CHARGES_TYPE = ?",$chargeType1);
									$resultSelecet1 = $db->FetchAll($select1);
									$chargeAmt = $resultSelecet1['0']['CHARGES_AMT'];

									//$param['TRANSFER_FEE'] = $chargeAmt1;
								}
								else{
									$chargeAmt = '0';
									//$param['TRANSFER_FEE'] = $chargeAmt2;
								}

								$filter->__destruct();
								unset($filter);

								$paramTrx = array("TRANSFER_TYPE" 			=> $TRANSFER_TYPE,
													"TRA_AMOUNT" 				=> $TRA_AMOUNT_num,
													"TRANSFER_FEE" 				=> $chargeAmt,
													"TRA_MESSAGE" 				=> $TRA_MESSAGE,
													"TRA_REFNO" 				=> $TRA_REFNO,
													"ACCTSRC" 					=> $dataTemp['SOURCE_ACCOUNT'],
													"ACBENEF" 					=> $ACBENEF,
													"ACBENEF_CCY" 				=> $ACBENEF_CCY,
													"ACBENEF_EMAIL" 			=> $ACBENEF_EMAIL,
													"ACBENEF_PHONE" 			=> $ACBENEF_PHONE,
													"SMS_NOTIF"					=> $SMS_NOTIF,
													"EMAIL_NOTIF"				=> $EMAIL_NOTIF,
												// for Beneficiary data, except (bene CCY and email), must be passed by reference
													"ACBENEF_BANKNAME" 			=> $ACBENEF_BANKNAME,
													"ACBENEF_ALIAS" 			=> $ACBENEF_ALIAS,
													"ACBENEF_CITIZENSHIP" 		=> $ACBENEF_CITIZENSHIP,		// W: WNI, N: WNA
												//	"ACBENEF_RESIDENT" 			=> $ACBENEF_RESIDENT,		// R: RESIDENT, NR: NON-RESIDENT
													"ACBENEF_ADDRESS1" 			=> $ACBENEF_ADDRESS,
													"REFERENCE"					=> $reference,
												//	"ACBENEF_ADDRESS2" 			=> &$ACBENEF_ADDRESS2,
												//	"ACBENEF_ADDRESS3" 			=> &$ACBENEF_ADDRESS3,

												//	"ORG_DIR" 					=> $ORG_DIR,
													//"BANK_CODE" 				=> $CLR_CODE,
												//	"BANK_NAME" 				=> $BANK_NAME,
												//	"BANK_BRANCH" 				=> $BANK_BRANCH,
												//	"BANK_ADDRESS1" 			=> $BANK_ADDRESS1,
												//	"BANK_ADDRESS2" 			=> $BANK_ADDRESS2,
												//	"BANK_ADDRESS3" 			=> $BANK_ADDRESS3,
												 );

								array_push($paramTrxArr,$paramTrx);
						}
					}
					// kalo jumlah trx lebih dari setting
					// else
					// {
					// 	$error_msg[] = 'Error: The number of rows to be imported should not more than '.$maxrow.'.';
					// 	$this->view->error 		= true;
					// 	$this->view->report_msg	= $this->displayError($error_msg);
					// }

					//$confirm = true;

					//kalo gak ada error
					// if(!empty($error_msg))
					// {
						//echo '<pre>';
						$validate   = new ValidatePaymentSingle($dataTemp['CUST_ID'], $dataTemp['USER_ID']);
						//var_dump($paramTrxArr);
						$resWs = array();
						$resultVal	= $validate->checkCreatePayroll($paramPayment, $paramTrxArr, $resWs);
						$payment 		= $validate->getPaymentInfo();
						
						$pstype = $dataTemp['PS_TYPE'];
						$bsid   = $dataTemp['BS_ID'];

						if($validate->isError() === false)	// payment data is valid
						{
							//die('tere');
							$confirm = true;
							$validate->__destruct();
							unset($validate);
						}
						else
						{
							
							$errorMsg 		= $validate->getErrorMsg();
							//var_dump($errorMsg);
							$errorTrxMsg 	= $validate->getErrorTrxMsg();	// array
							//var_dump($errorTrxMsg);
							//die('tere1');
							

							foreach($errorTrxMsg as $key)
							{
								foreach($key as $ccy)
								{
									foreach ($ccy as $row => $val) {
										
										$trxvalidasi	= $db->select()
														->from(array('P' => 'TEMP_BULKTRANSACTION'), array('P.*'))
														// ->joinLeft(array('T' => 'TEMP_BULKTRANSACTION'), 'P.BS_ID = T.BS_ID', array('T.*'))
														// ->order('P.VALIDATION DESC')
														->where('P.BS_ID = ?',$bsid);
														//->where('DATE(P.PS_CREATED) = DATE(NOW())');

										$transactionvalidasi = $db->fetchAll($trxvalidasi);

										$sourceAccountFailed = $transactionvalidasi[$row]['TRANSACTION_ID'];
										
										$validated = array(	
											'VALIDATED'			=> 0,
											'ERROR_DESC'		=> $val,
										   );

										$where = array(
														'BS_ID = ?' => $bsid,
														'TRANSACTION_ID = ?' => $sourceAccountFailed);
														//var_dump($validated);
														//var_dump($where);
										$db->update('TEMP_BULKTRANSACTION', $validated, $where);

									}
								}
							}

							$validate->__destruct();

							unset($validate);
							// echo "<pre>";
							// print_r($errorMsg);die;
							// Zend_Debug::dump($validate);die;
							// if($errorMsg)
							// {
							// 	$error_msg[] = 'Error: '.$errorMsg;
							// 	$this->view->error 		= true;
							// 	$this->view->report_msg	= $this->displayError($error_msg);
							// }
							// else
							// {
							$confirm = true;
							//}
						}

						// die;
					// }
				}
				// else //kalo total record = 0
				// {
				// 	$error_msg[] = $this->language->_('Error').': '.$this->language->_('Wrong File Formatss').'.';
				// 	$this->view->error 		= true;
				// 	$this->view->report_msg	= $this->displayError($error_msg);
				// }
			//akhir
			}
			
			
			if ($dataTemp['PS_TYPE'] == '28') {
			//awal
				$select = $db->select()
						->from(array('C' => 'M_CUST_ADAPTER_PROFILE'),array('*'))
						->join(array('A' => 'M_ADAPTER_PROFILE'), 'C.ADAPTER_PROFILE_ID = A.PROFILE_ID', '*')
						->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'A.PROFILE_ID = D.PROFILE_ID', '*')
						->where("CUST_ID = ?",$dataTemp['CUST_ID'])
						->where("TRF_TYPE = ?", 'autodebit');
				$adapterDataAutodebit = $db->FetchAll($select);
				
				$PS_FILE = $dataTemp['PS_FILE'];

				$imgsrc = LIBRARY_PATH.'/data/uploads/document/temp/'.$PS_FILE;
				$file_contents = file_get_contents($imgsrc);
				
				
				$file = explode(PHP_EOL, $file_contents);
				//$extension = 'txt';
				$delimitedWith = '|';
				$fileName = $adapterDataAutodebit[0]['FILE_PATH'];
		        $fixLength = $adapterDataAutodebit[0]['FIXLENGTH'];
		        $fixLengthType = $adapterDataAutodebit[0]['FIXLENGTH_TYPE'];
		        $fixLengthHeader = $adapterDataAutodebit[0]['FIXLENGTH_HEADER_ORDER'];
		        $fixLengthHeaderName = $adapterDataAutodebit[0]['FIXLENGTH_HEADER_NAME'];
		        $fixLengthContent = $adapterDataAutodebit[0]['FIXLENGTH_CONTENT_ORDER'];
				$newFileName = $imgsrc;
				
				if (!empty($adapterDataAutodebit)) {
					$extension = $adapterDataAutodebit[0]['FILE_FORMAT'];
				}
				else{
					$extension = 'txt';	
				}
				  
				if($file_contents){
				$data = convertFileToArray($newFileName, $extension, $delimitedWith);
				}else{
					$data = array();
				}
				//var_dump($data);die;
				//@unlink($newFileName);

				if (!empty($adapterDataAutodebit)) {
					//unset header if not fixlength
					if ($fixLength != 1) {
						unset($data[0]);
						//unset($data[1]);	
					}
				}
				else{
					
					unset($data[0]);
					unset($data[1]);
				
				}	

				$totalRecords = count($data);
				
				//proses convert ke order yg benar
				if($totalRecords)
				{

					foreach ($adapterDataAutodebit as $key => $value) {
						$headerOrder[] = $value['HEADER_CONTENT'];
					}

					foreach ($data as $datakey => $datavalue) {

						if (!empty($headerOrder)) {
							foreach ($headerOrder as $key => $value) {

								$headerOrderArr = explode(',', $value);

								if (count($headerOrderArr) > 1) {
									$i = 0;
									$contentStr = '';
									foreach ($headerOrderArr as $key2 => $value2) {

										if ($i != count($headerOrderArr)) {
											$contentStr .= $data[$datakey][$value2].' ';
										}
										$i++;
									}	
									$newData[$datakey][] = $contentStr;
								}
								else{
									$newData[$datakey][] = $data[$datakey][$value];
								}
							}	
						}
						else{
							$newData[$datakey][] = null;
						}
					}
				}
				
				$mandatoryCheck = validateField($newData, $adapterDataAutodebit);

				//auto mapping agar isi field file sesuai dengan yang di setup di m_mapping bo
				$fixData = autoMapping($newData, $adapterDataAutodebit);
				
				
				if($fixData['2']['0'] == NULL){
					$fixData = $data;	
					$totalRecords = count($data);
				}
				//Zend_Debug::dump($fixData); die;
				if($totalRecords)
				{
					if($totalRecords <= $maxrow)
					{
						$rowNum = 0;
  
						$paramPayment = array( 	"CATEGORY"      	=> "DISBURSEMENT",
												"FROM"       		=> "F",
												"TRA_CCY"					=> 'IDR',
												"PS_NUMBER"     	=> "",
												"COMM_ROLE"			=> 'P',
												"PS_SUBJECT"   	 	=> $dataTemp['PS_SUBJECT'],
												"PSFILEID"			=> $dataTemp['FILE_ID'],
												"PS_EFDATE"     	=> $dataTemp['PS_EFDATE'],
												"PS_FILE"     		=> $dataTemp['PS_FILE'],
												"_dateFormat"    	=> $dateDBFormat,
												"_dateDBFormat"    	=> $dateDBFormat,
												"_addBeneficiary"   => true, // privi BADA (Add Beneficiary)
												"_beneLinkage"    	=> true, // privi BLBU (Linkage Beneficiary User)
												"_createPB"     	=> true, // privi CBPW (Create Bulk Credit Payment PB by Import File (CSV))
												"_createDOM"    	=> true, // privi CBPI (Create Bulk Credit Payment Domestic by Import File (CSV))
												"_createREM"    	=> false,        // cannot create REM trx
											  );

						$paramTrxArr = array();
						 
									$duplctArr = array();
									 
									
									
									
						foreach ( $fixData as $row )
						{
							// if(count($row)==4)
							// {
								 //var_dump($row);die;
								 $benefAcct 	= trim($row[2]);
											$ccy 		= 'IDR';
											$amount 	= trim($row[3]);
											$email 		= trim($row[4]);
											$purpose 	= '';
											$message 	= trim($row[5]);
											$addMessage = '';
											$type 		= 'PB';
											//$bankCode 	= trim($row[4]);
											$cust_ref 	= trim($row[1]);
								$rowNum++;
								if (!empty($row) && empty($adapterDataAutodebit)) {
									$row = explode('|', $row[0]);
									//var_dump($row);die;
									$benefAcct 		= trim($row[2]);
	// 											$benefName 		= trim($row[1]);
									$ccy 			= "IDR";
									$amount 		= trim($row[3]);
									$message 		= trim($row[5]);
									$email 			= trim($row[4]);
									$reference 		= '';
									$sms_notif 		= '';
									$email_notif 	= '';
									$addMessage 	= '';
									$cust_ref 		= trim($row[1]);
									$duplctArr[] = trim($row[1]);
								}else{
									$benefAcct 		= trim($row[2]);
	// 											$benefName 		= trim($row[1]);
									$ccy 			= "IDR";
									$amount 		= trim($row[3]);
									$message 		= trim($row[5]);
									$email 			= trim($row[4]);
									$reference 		= '';
									$sms_notif 		= '';
									$email_notif 	= '';
									$addMessage 	= '';
									$cust_ref 		= trim($row[1]);
									$duplctArr[] = trim($row[1]);
								}
								
// 											$email 			= trim($row[6]);
// 											$phoneNumber	= trim($row[7]);
								$type 			= 'PB';
// 											$bankCode 		= trim($row[6]);
								//$bankName 		= trim($row[10]);
//										$bankCity = trim($row[10]);
// 											$benefAdd		= trim($row[9]);
// 											$citizenship	= strtoupper(trim($row[10]));
								//$resident = strtoupper(trim($row[11]));

								/*
								 * Change parameter into document
								 */
								$fullDesc = array(
									'BENEFICIARY_ACCOUNT' 		=> $benefAcct,
									'BENEFICIARY_NAME' 			=> '',
									'BENEFICIARY_ACCOUNT_CCY' 	=> $ccy,
									'TRA_AMOUNT' 				=> $amount,
									'TRA_MESSAGE'				=> $message,
									'REFNO' 					=> $addMessage,
									'SMS_NOTIF'					=> $sms_notif,
									'EMAIL_NOTIF' 				=> $email_notif,
									'BENEFICIARY_EMAIL' 		=> $email,
									'BENEFICIARY_MOBILE_PHONE_NUMBER' => '',
									'TRANSFER_TYPE' 			=> 'PB',
									//'CLR_CODE' 					=> $bankCode,
									//'BENEFICIARY_BANK_NAME' 	=> $bankName,
//											'BENEFICIARY_CITY' => $bankCity,
									'BENEFICIARY_ADDRESS'		=> '',
									'BENEFICIARY_CITIZENSHIP' 	=> '',
									'REFF_ID'					=> $cust_ref
									//'BENEFICIARY_RESIDENT' => $resident
								);

								$filter = new Application_Filtering();

								$SMS_NOTIF 		= $filter->filter($sms_notif, "SMS");
								$EMAIL_NOTIF 		= $filter->filter($email_notif, "EMAIL");

								$TRA_AMOUNT 		= $filter->filter($amount, "AMOUNT");
								$TRA_MESSAGE 		= $filter->filter($message, "TRA_MESSAGE");
								$TRA_REFNO 			= $filter->filter($addMessage, "TRA_REFNO");
								$ACBENEF 			= $filter->filter($benefAcct, "ACCOUNT_NO");
								$ACBENEF_BANKNAME 	= $filter->filter('', "ACCOUNT_NAME");
								$ACBENEF_ALIAS 		= $filter->filter('', "ACCOUNT_ALIAS");
								$ACBENEF_EMAIL 		= $filter->filter('', "EMAIL");
								$ACBENEF_PHONE 		= $filter->filter('', "MOBILE_PHONE_NUMBER");
								$ACBENEF_CCY 		= $filter->filter($ccy, "SELECTION");
								$ACBENEF_ADDRESS	= $filter->filter('', "ADDRESS");
								$ACBENEF_CITIZENSHIP= $filter->filter('', "SELECTION");
								$CUST_REF 			= $filter->filter($cust_ref, "CUST_REF");
								//$ACBENEF_RESIDENT= $filter->filter($resident, "SELECTION");
								//$BANK_NAME			= $filter->filter($bankName, "BANK_NAME");
								//$BANK_CITY			= $filter->filter($bankCity, "ADDRESS");
								//$CLR_CODE			= $filter->filter($bankCode, "BANK_CODE");
								$TRANSFER_TYPE 		= $filter->filter($type, "SELECTION");

								$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

								if($TRANSFER_TYPE == 1){
									$TRANSFER_TYPE = 'RTGS';
								}else if($TRANSFER_TYPE == 2){
									$TRANSFER_TYPE = 'SKN';
								}else if($TRANSFER_TYPE == 5){
									$TRANSFER_TYPE = 'ONLINE';
								}else if($TRANSFER_TYPE == '0'){
									$TRANSFER_TYPE = 'PB';
								}
								//else{
								//	$TRANSFER_TYPE = $params['TRANSFER_TYPE']; 
								//}
									
								$selectcust = $db->select()
													->from('M_CUSTOMER',array('CUST_CHARGESID'))
													->where("CUST_ID = ?",$dataTemp['CUST_ID']);
									$custdata = $db->fetchrow($selectcust);
									$chrgdatainhouse = array();
									if(!empty($custdata)){
											$selectchrginhouse = $db->select()
													->from('M_CHARGES_WITHIN',array('CUST_ID','AMOUNT'))
													->where("CHARGES_ID = ?",$custdata['CUST_CHARGESID']);
											$chrgdatainhouse = $db->fetchrow($selectchrginhouse);
											
											
									}else{
										$chrgdatainhouse['CUST_ID'] = 'GLOBAL';
									}
									

								if($TRANSFER_TYPE == 'RTGS'){
									$chargeType = '1';
									$select = $db->select()
													->from('M_CHARGES_OTHER',array('*'))
													->where("CUST_ID = ?",$chrgdatainhouse['CUST_ID'])
													->where("CHARGES_TYPE = ?",$chargeType);
									$resultSelecet = $db->FetchAll($select);
									$chargeAmt = $resultSelecet['0']['CHARGES_AMT'];

									//$param['TRANSFER_FEE'] = $chargeAmt;
								}
								else if($TRANSFER_TYPE == 'SKN'){
									$chargeType1 = '2';
									$select1 = $db->select()
													->from('M_CHARGES_OTHER',array('*'))
													->where("CUST_ID = ?",$chrgdatainhouse['CUST_ID'])
													->where("CHARGES_TYPE = ?",$chargeType1);
									$resultSelecet1 = $db->FetchAll($select1);
									$chargeAmt = $resultSelecet1['0']['CHARGES_AMT'];

									//$param['TRANSFER_FEE'] = $chargeAmt1;
								}else if($TRANSFER_TYPE == 'ONLINE'){
									$chargeType1 = '8';
									$select1 = $db->select()
													->from('M_CHARGES_OTHER',array('*'))
													->where("CUST_ID = ?",$chrgdatainhouse['CUST_ID'])
													->where("CHARGES_TYPE = ?",$chargeType1);
									$resultSelecet1 = $db->FetchAll($select1);
									$chargeAmt = $resultSelecet1['0']['CHARGES_AMT'];

									//$param['TRANSFER_FEE'] = $chargeAmt1;
								}
								else{
									$chargeAmt = '0';
									//$param['TRANSFER_FEE'] = $chargeAmt2;
								}

								$filter->__destruct();
								unset($filter);

								//var_dump($dataTemp);die;

								$paramTrx = array("TRANSFER_TYPE" 			=> 'PB',
													"TRA_AMOUNT" 				=> $TRA_AMOUNT_num,
													
													"TRANSFER_FEE" 				=> $chargeAmt,
													"TRA_MESSAGE" 				=> $TRA_MESSAGE,
													"TRA_REFNO" 				=> $TRA_REFNO,
													"ACCTSRC" 					=> $dataTemp["BENEFICIARY_ACCOUNT"],
													"ACBENEF" 					=> $ACBENEF,
													"ACBENEF_CCY" 				=> $ACBENEF_CCY,
													"ACBENEF_EMAIL" 			=> $ACBENEF_EMAIL,
													"ACBENEF_PHONE" 			=> $ACBENEF_PHONE,
													"SMS_NOTIF"					=> $SMS_NOTIF,
													"EMAIL_NOTIF"				=> $EMAIL_NOTIF,
												// for Beneficiary data, except (bene CCY and email), must be passed by reference
													"ACBENEF_BANKNAME" 			=> $ACBENEF_BANKNAME,
													"ACBENEF_ALIAS" 			=> $ACBENEF_ALIAS,
													"ACBENEF_CITIZENSHIP" 		=> $ACBENEF_CITIZENSHIP,		// W: WNI, N: WNA
												//	"ACBENEF_RESIDENT" 			=> $ACBENEF_RESIDENT,		// R: RESIDENT, NR: NON-RESIDENT
													"ACBENEF_ADDRESS1" 			=> $ACBENEF_ADDRESS,
													"CUST_REF"					=> $CUST_REF,
													"REFERENCE"					=> $reference,
												//	"ACBENEF_ADDRESS2" 			=> &$ACBENEF_ADDRESS2,
												//	"ACBENEF_ADDRESS3" 			=> &$ACBENEF_ADDRESS3,

												//	"ORG_DIR" 					=> $ORG_DIR,
													//"BANK_CODE" 				=> $CLR_CODE,
												//	"BANK_NAME" 				=> $BANK_NAME,
												//	"BANK_BRANCH" 				=> $BANK_BRANCH,
												//	"BANK_ADDRESS1" 			=> $BANK_ADDRESS1,
												//	"BANK_ADDRESS2" 			=> $BANK_ADDRESS2,
												//	"BANK_ADDRESS3" 			=> $BANK_ADDRESS3,
												 );

								array_push($paramTrxArr,$paramTrx);
						}
					}
					// kalo jumlah trx lebih dari setting
					// else
					// {
					// 	$error_msg[] = 'Error: The number of rows to be imported should not more than '.$maxrow.'.';
					// 	$this->view->error 		= true;
					// 	$this->view->report_msg	= $this->displayError($error_msg);
					// }

					//$confirm = true;

					//kalo gak ada error
					// if(!empty($error_msg))
					// { 
						//echo '<pre>';
						$validate   = new ValidatePaymentSingle($dataTemp['CUST_ID'], $dataTemp['USER_ID']);
						//var_dump($paramPayment);
						//var_dump($paramTrxArr);die;
						$resWs = array();
						$resultVal	= $validate->checkCreate($paramPayment, $paramTrxArr, $resWs);
						$payment 		= $validate->getPaymentInfo();
						//var_dump($payment);die; 
						$pstype = $dataTemp['PS_TYPE'];
						$bsid   = $dataTemp['BS_ID'];
						
						
						
						$duplicates = array_unique( array_diff_assoc( $duplctArr, array_unique( $duplctArr ) ) );
						$checkduplicate = true;
						if(!empty($duplicates)){
							
									$checkduplicate = false;
								
						}
						

						if($validate->isError() === false && $checkduplicate)	// payment data is valid
						{
							//die('tere');
							$confirm = true;
							$validate->__destruct();
							unset($validate);
						}
						else
						{
							//$duplctArr = array_unique( array_diff_assoc( $duplctArr, array_unique( $duplctArr ) ) );
							
							
							$errorMsg 		= $validate->getErrorMsg();
							//var_dump($errorMsg);
							$errorTrxMsg 	= $validate->getErrorTrxMsg();	// array
							//var_dump($errorTrxMsg);
							//var_dump($duplicates);
						//	var_dump($withoutDuplicates);
							//var_dump($duplicates);
							foreach($paramTrxArr as $kl => $vl){
								if(!empty($duplicates)){
									foreach($duplicates as $vd){
											if($vl['CUST_REF'] == $vd){
												$errorTrxMsg['PB']['IDR'][$kl] = $errorTrxMsg['PB']['IDR'][$kl].' '.'Error: Duplicate client reference.';
												}
									}
								}
								
							}
							//var_dump($errorTrxMsg);
							//die('tere1'); 
							 
							

							foreach($errorTrxMsg as $key)
							{
								foreach($key as $ccy)
								{
									foreach ($ccy as $row => $val) {
										
										$trxvalidasi	= $db->select()
														->from(array('P' => 'TEMP_BULKTRANSACTION'), array('P.*'))
														// ->joinLeft(array('T' => 'TEMP_BULKTRANSACTION'), 'P.BS_ID = T.BS_ID', array('T.*'))
														// ->order('P.VALIDATION DESC')
														->where('P.BS_ID = ?',$bsid);
														//->where('DATE(P.PS_CREATED) = DATE(NOW())');

										$transactionvalidasi = $db->fetchAll($trxvalidasi);

										$sourceAccountFailed = $transactionvalidasi[$row]['TRANSACTION_ID'];
										
										
										
										
										
										$validated = array(	
											'VALIDATED'			=> 0,
											'ERROR_DESC'		=> $val,
										   );

										$where = array(
														'BS_ID = ?' => $bsid,
														'TRANSACTION_ID = ?' => $sourceAccountFailed);
										$db->update('TEMP_BULKTRANSACTION', $validated, $where);

									}
								}
							}

							$validate->__destruct();

							unset($validate);
							// echo "<pre>";
							// print_r($errorMsg);die;
							// Zend_Debug::dump($validate);die;
							// if($errorMsg)
							// {
							// 	$error_msg[] = 'Error: '.$errorMsg;
							// 	$this->view->error 		= true;
							// 	$this->view->report_msg	= $this->displayError($error_msg);
							// }
							// else
							// {
							$confirm = true;
							//}
						}

						// die;
					// }
				}
				// else //kalo total record = 0
				// {
				// 	$error_msg[] = $this->language->_('Error').': '.$this->language->_('Wrong File Formatss').'.';
				// 	$this->view->error 		= true;
				// 	$this->view->report_msg	= $this->displayError($error_msg);
				// }
			//akhir
			}
			//var_dump($confirm);die;
			
			if ($dataTemp['PS_TYPE'] == '26') {
			//awal
				$select = $db->select()
						->from(array('C' => 'M_CUST_ADAPTER_PROFILE'),array('*'))
						->join(array('A' => 'M_ADAPTER_PROFILE'), 'C.ADAPTER_PROFILE_ID = A.PROFILE_ID', '*')
						->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'A.PROFILE_ID = D.PROFILE_ID', '*')
						->where("CUST_ID = ?",$dataTemp['CUST_ID'])
						->where("TRF_TYPE = ?", 'payroll');
				$adapterDataEmoney = $db->FetchAll($select);
				
				$PS_FILE = $dataTemp['PS_FILE'];

				$imgsrc = LIBRARY_PATH.'/data/uploads/document/temp/'.$PS_FILE;
				$file_contents = file_get_contents($imgsrc);
				
				
				$file = explode(PHP_EOL, $file_contents);
				//$extension = 'txt';
				$delimitedWith = '|';
				$fileName = $adapterDataEmoney[0]['FILE_PATH'];
		        $fixLength = $adapterDataEmoney[0]['FIXLENGTH'];
		        $fixLengthType = $adapterDataEmoney[0]['FIXLENGTH_TYPE'];
		        $fixLengthHeader = $adapterDataEmoney[0]['FIXLENGTH_HEADER_ORDER'];
		        $fixLengthHeaderName = $adapterDataEmoney[0]['FIXLENGTH_HEADER_NAME'];
		        $fixLengthContent = $adapterDataEmoney[0]['FIXLENGTH_CONTENT_ORDER'];
				$newFileName = $imgsrc;
				
				if (!empty($adapterDataEmoney)) {
					$extension = $adapterDataEmoney[0]['FILE_FORMAT'];
				}
				else{
					$extension = 'txt';	
				}
				  
				if($file_contents){
				$data = convertFileToArray($newFileName, $extension, $delimitedWith);
				}else{
					$data = array();
				}
				//var_dump($data);die;
				//@unlink($newFileName);

				if (!empty($adapterDataPayroll)) {
					//unset header if not fixlength
					if ($fixLength != 1) {
						unset($data[0]);
						//unset($data[1]);	
					}
				}
				else{
					
					unset($data[0]);
					unset($data[1]);
				
				}	

				$totalRecords = count($data);
				
				//proses convert ke order yg benar
				if($totalRecords)
				{

					foreach ($adapterDataEmoney as $key => $value) {
						$headerOrder[] = $value['HEADER_CONTENT'];
					}

					foreach ($data as $datakey => $datavalue) {

						if (!empty($headerOrder)) {
							foreach ($headerOrder as $key => $value) {

								$headerOrderArr = explode(',', $value);

								if (count($headerOrderArr) > 1) {
									$i = 0;
									$contentStr = '';
									foreach ($headerOrderArr as $key2 => $value2) {

										if ($i != count($headerOrderArr)) {
											$contentStr .= $data[$datakey][$value2].' ';
										}
										$i++;
									}	
									$newData[$datakey][] = $contentStr;
								}
								else{
									$newData[$datakey][] = $data[$datakey][$value];
								}
							}	
						}
						else{
							$newData[$datakey][] = null;
						}
					}
				}
				
				$mandatoryCheck = validateField($newData, $adapterDataEmoney);

				//auto mapping agar isi field file sesuai dengan yang di setup di m_mapping bo
				$fixData = autoMapping($newData, $adapterDataEmoney);
				
				if(empty($fixData)){
					$fixData = $data;	
					$totalRecords = count($data);
				}
				
				if($totalRecords)
				{
					if($totalRecords <= $maxrow)
					{
						$rowNum = 0;
 
						$paramPayment = array( 	"CATEGORY"      	=> "BULK BILLER",
												"FROM"       		=> "I",
												"PS_NUMBER"     	=> "",
												"PS_SUBJECT"   	 	=> $dataTemp['PS_SUBJECT'],
												"PSFILEID"			=> $dataTemp['FILE_ID'],
												"PS_EFDATE"     	=> $dataTemp['PS_EFDATE'],
												"PS_FILE"     		=> $dataTemp['PS_FILE'],
												"_dateFormat"    	=> $dateDBFormat,
												"_dateDBFormat"    	=> $dateDBFormat,
												"_addBeneficiary"   => true, // privi BADA (Add Beneficiary)
												"_beneLinkage"    	=> true, // privi BLBU (Linkage Beneficiary User)
												"_createPB"     	=> true, // privi CBPW (Create Bulk Credit Payment PB by Import File (CSV))
												"_createDOM"    	=> true, // privi CBPI (Create Bulk Credit Payment Domestic by Import File (CSV))
												"_createREM"    	=> false,        // cannot create REM trx
											  );

						$paramTrxArr = array();
						 //Zend_Debug::dump($fixData); die;
						$totalamount = 0;
						foreach ( $fixData as $row )
						{
							// if(count($row)==4)
							// {
								 
								
								if (!empty($row) && empty($adapterDataEmoney)) {
									//var_dump($row);die;
									$row = explode('|', $row[0]);
									
									$biller 		= trim($row[1]);
	// 											$benefName 		= trim($row[1]);
									$ccy 			= "IDR";
									$amount 		= trim($row[3]);
									$message 		= '';
									$reference 		= '';
									$sms_notif 		= '';
									$email_notif 	= '';
									$addMessage 	= '';
									$orderId 	 	= trim($row[2]);
								}else{
									$row = explode('|', $row[0]);
									//echo 'ger';
									$benefAcct 		= '';
// 											$benefName 		= trim($row[1]);
									$ccy 			= "IDR";
									$biller 		= trim($row[1]);
									$amount 		= trim($row[3]);
									$message 		= '';
									$reference 		= '';
									$sms_notif 		= '';
									$email_notif 	= '';
									$addMessage 	= '';
									$orderId 	 	= trim($row[2]);
								}
								
// 											$email 			= trim($row[6]);
// 											$phoneNumber	= trim($row[7]);
								$type 			= 'PB';
// 											$bankCode 		= trim($row[6]);
								//$bankName 		= trim($row[10]);
//										$bankCity = trim($row[10]);
// 											$benefAdd		= trim($row[9]);
// 											$citizenship	= strtoupper(trim($row[10]));
								//$resident = strtoupper(trim($row[11]));

								/*
								 * Change parameter into document
								 */
								$fullDesc = array(
									'BENEFICIARY_ACCOUNT' 		=> $benefAcct,
									'BENEFICIARY_NAME' 			=> '',
									'BILLER_ORDER_ID' 			=> $orderId,
									'BENEFICIARY_ACCOUNT_CCY' 	=> $ccy,
									'TRA_AMOUNT' 				=> $amount,
									'TRA_MESSAGE'				=> $message,
									'REFNO' 					=> $addMessage,
									'SMS_NOTIF'					=> $sms_notif,
									'EMAIL_NOTIF' 				=> $email_notif,
									'BENEFICIARY_EMAIL' 		=> '',
									'BENEFICIARY_MOBILE_PHONE_NUMBER' => '',
									'TRANSFER_TYPE' 			=> 'PB',
									//'CLR_CODE' 					=> $bankCode,
									//'BENEFICIARY_BANK_NAME' 	=> $bankName,
//											'BENEFICIARY_CITY' => $bankCity,
									'BENEFICIARY_ADDRESS'		=> '',
									'BENEFICIARY_CITIZENSHIP' 	=> ''
									//'BENEFICIARY_RESIDENT' => $resident
								);

								$filter = new Application_Filtering();

								$SMS_NOTIF 		= $filter->filter($sms_notif, "SMS");
								$EMAIL_NOTIF 		= $filter->filter($email_notif, "EMAIL");

								$TRA_AMOUNT 		= $filter->filter($amount, "AMOUNT");
								$TRA_MESSAGE 		= $filter->filter($message, "TRA_MESSAGE");
								$TRA_REFNO 			= $filter->filter($addMessage, "TRA_REFNO");
								$ACBENEF 			= $filter->filter($benefAcct, "ACCOUNT_NO");
								$ACBENEF_BANKNAME 	= $filter->filter('', "ACCOUNT_NAME");
								$ACBENEF_ALIAS 		= $filter->filter('', "ACCOUNT_ALIAS");
								$ACBENEF_EMAIL 		= $filter->filter('', "EMAIL");
								$ACBENEF_PHONE 		= $filter->filter('', "MOBILE_PHONE_NUMBER");
								$ACBENEF_CCY 		= $filter->filter($ccy, "SELECTION");
								$ACBENEF_ADDRESS	= $filter->filter('', "ADDRESS");
								$ACBENEF_CITIZENSHIP= $filter->filter('', "SELECTION");
								$BILLER_ORDER_ID  	= $fullDesc['BILLER_ORDER_ID'];
								//$ACBENEF_RESIDENT= $filter->filter($resident, "SELECTION");
								//$BANK_NAME			= $filter->filter($bankName, "BANK_NAME");
								//$BANK_CITY			= $filter->filter($bankCity, "ADDRESS");
								//$CLR_CODE			= $filter->filter($bankCode, "BANK_CODE");
								$TRANSFER_TYPE 		= 0;

								$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

								
									$chargeAmt = '0';
								

								$filter->__destruct();
								unset($filter);
								
								
									//$userDataServiceProvider 	= $this->CustomerUser->getServiceProvider($this->_getParam('operator'));
									//$provInquiry = $this->view->provInquiry = $userDataServiceProvider['PROVIDER_INQUIRY_SERVICE_STATUS'];
									
									$selectoperator	= $db->select()
														->from(array('P' => 'M_PROVIDER_CATALOG'), array('P.*'))
														
														->where('P.SGO_PRODUCT_CODE = ?',$biller);
														

										$operatorid = $db->fetchAll($selectoperator);
										
										$selectoperatorprovider	= $db->select()
														->from(array('P' => 'M_SERVICE_PROVIDER'), array('P.*'))
														
														->where('P.PROVIDER_ID = ?',$operatorid['0']['PROVIDER_ID']);
														
										//var_dump($biller);
										$providerid = $db->fetchAll($selectoperatorprovider);
									
									
									$msg = array();
									$param = array();
									$param['SERVICE_TYPE'] 		= 3;
									$param['PROVIDER_TYPE'] 	= 1; //1
									$param['operator']			= $operatorid['0']['PROVIDER_ID'];
									$param['ACCTSRC']			= $dataTemp['SOURCE_ACCOUNT'];
									$param['cek']				= 0;
									$param['radioCheck']		= 1;
									$param['payment']			= NULL;
									$param['USER_ID'] 			= $dataTemp['USER_ID'];
									$param['responseCodeReq'] 	= NULL;
									$param['TOKEN_TYPE'] 		= 1;
									$param['TOKEN_ID'] 			= 1;
									$param['CUST_ID'] 			= $dataTemp['CUST_ID'];
									$param['ACCT_NO'] 			= $dataTemp['SOURCE_ACCOUNT'];
									$provInquiry = $providerid['0']['PROVIDER_INQUIRY_SERVICE_STATUS'];
									if($provInquiry == 'N'){
										$param['provInquiry'] 	= $provInquiry;
										$param['amount'] 		= Application_Helper_General::convertDisplayMoney($amount);
									}
									else{
										$param['provInquiry'] 	= $provInquiry;
									}
									
									$param['USER_MOBILE_PHONE'] = '085713544966';
									
									if($param['radioCheck']==1)
									{
										$param['orderId'] = $orderId;
									}
									
	//die('here');
									
									$param['productCode'] 	= $biller; //STCKAI
									
									
									$sendProvider	 = new SinglePayment(null, $dataTemp['CUST_ID'], $dataTemp['USER_ID']);
									$resProvider	 = $sendProvider->createPaymentPurchase($param, $msg);
									//var_dump($msg);die;
									$paramSession = array();
									$paramSession['operator']			= $param['operator'];
									$paramSession['ACCTSRC']			= $param['ACCTSRC'];
									$paramSession['BILLER_ID']			= $biller;
									$paramSession['ccy']				= $msg['detailCustomer']['0']['CCY_ID'];
									$paramSession['ACCT_NAME']			= $msg['responseData']['0']['customer_name'];
									$paramSession['ACCT_NAME_DATA']		= $msg['detailCustomer']['0']['ACCT_NAME'];
									$paramSession['ACCT_ALIAS_NAME']	= $msg['detailCustomer']['0']['ACCT_NAME'];
									$paramSession['ACCT_TYPE']			= $msg['detailCustomer']['0']['ACCT_TYPE'];
									$paramSession['ACCT_DESC']			= $msg['detailCustomer']['0']['ACCT_DESC'];
									$paramSession['FREEZE_STATUS']		= $msg['detailCustomer']['0']['FREEZE_STATUS'];
									$paramSession['MAXLIMIT']			= $msg['detailCustomer']['0']['MAXLIMIT'];
									$paramSession['amount']				= $msg['responseData']['0']['amount'];
									$paramSession['fee']				= $msg['fee']['0'];
									$paramSession['total']				= $msg['responseData']['0']['amount']+$msg['fee']['0'];
									$paramSession['sgoProductCode']		= $msg['param']['0']['productCode'];
									$paramSession['operatorNama']		= $msg['provider']['operatorName'];
									$paramSession['CUST_ID']			= $dataTemp['CUST_ID'];
									$paramSession['USER_ID']			= $dataTemp['USER_ID'];
									$paramSession['TOKEN_TYPE']			= 1;
									$paramSession['USER_MOBILE_PHONE']	= '085713544966';
									
									$paramSession['message']			= $msg['param']['0']['Message'];
									$paramSession['PS_CATEGORY']		= $msg['param']['0']['PS_CATEGORY'];
									$paramSession['TypeOfTrans']		= $msg['param']['0']['TypeOfTrans'];
									$paramSession['PS_CATEGORY_EMAIL']	= $msg['param']['0']['PS_CATEGORY_EMAIL'];
									$paramSession['PS_TYPE']			= $msg['param']['0']['PS_TYPE'];
									$paramSession['Module']				= $msg['param']['0']['Module'];
									
									$paramSession['cek']				= $param['cek'];
									
									if($param['radioCheck']==1)
									{
										$paramSession['orderId']		= $param['orderId'];
									}
									else if($param['radioCheck']==2)
									{
										$paramSession['orderId']		= $param['payment'];
									}
												
									$paramSession['PS_NUMBER']			= NULL;

									$paramSession['radioCheck']			= $param['radioCheck'];
									$totalamount = $totalamount + $msg['responseData']['0']['amount'];
									$rowNum++;
									array_push($paramTrxArr,$paramSession);
									
								
						}
					}
					
					//var_dump($paramTrxArr);die;
					// kalo jumlah trx lebih dari setting
					// else
					// {
					// 	$error_msg[] = 'Error: The number of rows to be imported should not more than '.$maxrow.'.';
					// 	$this->view->error 		= true;
					// 	$this->view->report_msg	= $this->displayError($error_msg);
					// }

					//$confirm = true;

					//kalo gak ada error
					// if(!empty($error_msg))
					// {
						//echo '<pre>';
							
						$pstype = $dataTemp['PS_TYPE'];
						$bsid   = $dataTemp['BS_ID'];
						
							//die('tere');
							$confirm = true;
						

						// die;
					// }
				}
				// else //kalo total record = 0
				// {
				// 	$error_msg[] = $this->language->_('Error').': '.$this->language->_('Wrong File Formatss').'.';
				// 	$this->view->error 		= true;
				// 	$this->view->report_msg	= $this->displayError($error_msg);
				// }
			//akhir
			}



			if ($dataTemp['PS_TYPE'] == '25') {
			//awal
				$select = $db->select()
						->from(array('C' => 'M_CUST_ADAPTER_PROFILE'),array('*'))
						->join(array('A' => 'M_ADAPTER_PROFILE'), 'C.ADAPTER_PROFILE_ID = A.PROFILE_ID', '*')
						->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'A.PROFILE_ID = D.PROFILE_ID', '*')
						->where("CUST_ID = ?",$dataTemp['CUST_ID'])
						->where("TRF_TYPE = ?", 'payrollother');
				$adapterDataPayrollother = $db->FetchAll($select);
				
				$PS_FILE = $dataTemp['PS_FILE'];

				$imgsrc = LIBRARY_PATH.'/data/uploads/document/temp/'.$PS_FILE;
				$file_contents = file_get_contents($imgsrc);
				
				
				$file = explode(PHP_EOL, $file_contents);
				//$extension = 'txt';
				$delimitedWith = '|';
				$fileName = $adapterDataPayrollother[0]['FILE_PATH'];
		        $fixLength = $adapterDataPayrollother[0]['FIXLENGTH'];
		        $fixLengthType = $adapterDataPayrollother[0]['FIXLENGTH_TYPE'];
		        $fixLengthHeader = $adapterDataPayrollother[0]['FIXLENGTH_HEADER_ORDER'];
		        $fixLengthHeaderName = $adapterDataPayrollother[0]['FIXLENGTH_HEADER_NAME'];
		        $fixLengthContent = $adapterDataPayrollother[0]['FIXLENGTH_CONTENT_ORDER'];
				$newFileName = $imgsrc;
				
				if (!empty($adapterDataPayrollother)) {
					$extension = $adapterDataPayrollother[0]['FILE_FORMAT'];
				}
				else{
					$extension = 'txt';	
				}
				  
				if($file_contents){
				$data = convertFileToArray($newFileName, $extension, $delimitedWith);
				}else{
					$data = array();
				}
				//var_dump($data);die;
				//@unlink($newFileName);

				if (!empty($adapterDataPayrollother)) {
					//unset header if not fixlength
					if ($fixLength != 1) {
						unset($data[0]);
						//unset($data[1]);	
					}
				}
				else{
					
					unset($data[0]);
					unset($data[1]);
				
				}	

				$totalRecords = count($data);
				
				//proses convert ke order yg benar
				if($totalRecords && !empty($adapterDataPayrollother))
				{

					foreach ($adapterDataPayrollother as $key => $value) {
						$headerOrder[] = $value['HEADER_CONTENT'];
					}
	//var_dump($data);die;
					foreach ($data as $datakey => $datavalue) {

						if (!empty($headerOrder)) {
							foreach ($headerOrder as $key => $value) {

								$headerOrderArr = explode(',', $value);

								if (count($headerOrderArr) > 1) {
									$i = 0;
									$contentStr = '';
									foreach ($headerOrderArr as $key2 => $value2) {

										if ($i != count($headerOrderArr)) {
											$contentStr .= $data[$datakey][$value2].' ';
										}
										$i++;
									}	
									$newData[$datakey][] = $contentStr;
								}
								else{
									$newData[$datakey][] = $data[$datakey][$value];
								}
							}	
						}
						else{
							$newData[$datakey][] = null;
						}
					}
				}
				if($newData[0]== null){
					$newData = $data;
				}
				//var_dump($newData);die;
				$mandatoryCheck = validateField($newData, $adapterDataPayrollother);

				//auto mapping agar isi field file sesuai dengan yang di setup di m_mapping bo
				$fixData = autoMapping($newData, $adapterDataPayrollother);
				//var_dump($fixData);die;
				if(empty($fixData)){
					$fixData = $data;	
					$totalRecords = count($data);
				}
				
				if($totalRecords)
				{
					if($totalRecords <= $maxrow)
					{
						$rowNum = 0;
 
						$paramPayment = array( 	"CATEGORY"      	=> "BULK PAYROLL",
												"FROM"       		=> "I",
												"PS_NUMBER"     	=> "",
												"PS_SUBJECT"   	 	=> $dataTemp['PS_SUBJECT'],
												"PSFILEID"			=> $dataTemp['FILE_ID'],
												"PS_EFDATE"     	=> $dataTemp['PS_EFDATE'],
												"PS_FILE"     		=> $dataTemp['PS_FILE'],
												"_dateFormat"    	=> $dateDBFormat,
												"_dateDBFormat"    	=> $dateDBFormat,
												"_addBeneficiary"   => true, // privi BADA (Add Beneficiary)
												"_beneLinkage"    	=> false, // privi BLBU (Linkage Beneficiary User)
												"_createPB"     	=> true, // privi CBPW (Create Bulk Credit Payment PB by Import File (CSV))
												"_createDOM"    	=> true, // privi CBPI (Create Bulk Credit Payment Domestic by Import File (CSV))
												"_createREM"    	=> false,        // cannot create REM trx
											  );

						$paramTrxArr = array();
						 //Zend_Debug::dump($fixData); die;

						foreach ( $fixData as $row )
						{
							// if(count($row)==4)
							// {
						//		 var_dump($row);
								$rowNum++;
								if (!empty($row) && empty($adapterDataPayrollother)) {
									$row = explode('|', $row[0]);
									//var_dump($row);die;
									$benefAcct 		= trim($row[3]);
// 											$benefName 		= trim($row[1]);
												$ccy 			= "IDR";
												$amount 		= trim($row[4]);
												$message 		= trim($row[15]);
												//$reference 		= trim($row[4]);

												//$benefAcct 		= trim($row[1]);
	// 											$benefName 		= trim($row[1]);
												//$ccy 			= "IDR";
												//$amount 		= trim($row[2]);
												//$message 		= trim($row[3]);
												$reference 		= '';
												$sms_notif 		= '';
												$email_notif 	= '';
												$addMessage 	= trim($row[16]);
	 											$email 			= trim($row[5]);
	 											$phoneNumber	= trim($row[6]);
												$type 			= trim($row[1]);
	 											$bankCode 		= trim($row[2]);
												//$bankName 		= trim($row[10]);
												$bankCity = trim($row[9]);
												$category = trim($row[10]);
	 											$benefAdd		= trim($row[7]);
												$benefAdd2		= trim($row[8]);
	 											$citizenship	= strtoupper(trim($row[11]));
												$resident = strtoupper(trim($row[12]));
												
												$idtype = strtoupper(trim($row[13]));
												$idnumber = strtoupper(trim($row[14]));
												
												$totalamount = (int)$totalamount + (int)$amount;
								}else{
									$benefAcct 		= trim($row[3]);
// 											$benefName 		= trim($row[1]);
												$ccy 			= "IDR";
												$amount 		= trim($row[4]);
												$message 		= trim($row[15]);
												//$reference 		= trim($row[4]);

												//$benefAcct 		= trim($row[1]);
	// 											$benefName 		= trim($row[1]);
												//$ccy 			= "IDR";
												//$amount 		= trim($row[2]);
												//$message 		= trim($row[3]);
												$reference 		= '';
												$sms_notif 		= '';
												$email_notif 	= '';
												$addMessage 	= trim($row[16]);
	 											$email 			= trim($row[5]);
	 											$phoneNumber	= trim($row[6]);
												$type 			= trim($row[1]);
	 											$bankCode 		= trim($row[2]);
												//$bankName 		= trim($row[10]);
												$bankCity = trim($row[9]);
												$category = trim($row[10]);
	 											$benefAdd		= trim($row[7]);
												$benefAdd2		= trim($row[8]);
	 											$citizenship	= strtoupper(trim($row[11]));
												$resident = strtoupper(trim($row[12]));
												
												$idtype = strtoupper(trim($row[13]));
												$idnumber = strtoupper(trim($row[14]));
												
												$totalamount = (int)$totalamount + (int)$amount;
								}
								
// 											$email 			= trim($row[6]);
// 											$phoneNumber	= trim($row[7]);
							//	$type 			= 'PB';
// 											$bankCode 		= trim($row[6]);
								//$bankName 		= trim($row[10]);
//										$bankCity = trim($row[10]);
// 											$benefAdd		= trim($row[9]);
// 											$citizenship	= strtoupper(trim($row[10]));
								//$resident = strtoupper(trim($row[11]));

								/*
								 * Change parameter into document
								 */
								 if($citizenship == 'R'){
													 $citizenship = 'R';
												 }else{
													 $citizenship = 'NR';
												 }
												 
												 if($resident == 'WNI'){
													 $resident = 'W';
												 }else{
													 $resident = 'R';
												 }
												
												 if(strtoupper($category)== 'INDIVIDUAL'){
													 $category = 'A0';
												 }else if(strtoupper($category)== 'GOVERNMENT'){
													 $category = 'B0';
												 }else if(strtoupper($category)== 'OTHER BANK'){
													 $category = 'C9';
												 }else if(strtoupper($category)== 'NON BANK FINANCIAL INSTITUTION'){
													 $category = 'D0';
												 }else if(strtoupper($category)== 'COMPANY'){
													 $category = 'E0';
												 }else if(strtoupper($category)== 'OTHER'){
													 $category = 'Z9';
												 }

												 
												$fullDesc = array(
													'BENEFICIARY_ACCOUNT' 		=> $benefAcct,
													'BENEFICIARY_NAME' 			=> '',
													'BENEFICIARY_ACCOUNT_CCY' 	=> $ccy,
													'TRA_AMOUNT' 				=> $amount,
													'TRA_MESSAGE'				=> $message,
													'TRA_ADDITIONAL_MESSAGE'	=> $addMessage,
													'SMS_NOTIF'					=> $sms_notif,
													'EMAIL_NOTIF' 				=> $email_notif,
													'BENEFICIARY_EMAIL' 		=> $email,
													'BENEFICIARY_MOBILE_PHONE_NUMBER' => $phoneNumber,
													'TRANSFER_TYPE' 			=> $type,
													'CLR_CODE' 					=> $bankCode,
													'BANK_CODE' 				=> $bankCode,
													//'BENEFICIARY_BANK_NAME' 	=> $bankName,
													'BENEFICIARY_CITY' => $bankCity,
													'BENEFICIARY_ADDRESS'		=> $benefAdd,
													'BENEFICIARY_ADDRESS2'		=> $benefAdd2,
													'BENEFICIARY_CITIZENSHIP' 	=> $citizenship,
													'BENEFICIARY_RESIDENT' => $resident,
													'BENEFICIARY_CATEGORY' => $category,
													'BENEFICIARY_ID_TYPE' => $idtype,
													'BENEFICIARY_ID_NUMBER' => $idnumber
												);

								$filter = new Application_Filtering();

								$SMS_NOTIF 		= $filter->filter($sms_notif, "SMS");
												$EMAIL_NOTIF 		= $filter->filter($email_notif, "EMAIL");

												$TRA_AMOUNT 		= $filter->filter($amount, "AMOUNT");
												$TRA_MESSAGE 		= $filter->filter($message, "TRA_MESSAGE");
												$TRA_REFNO 			= $filter->filter($addMessage, "TRA_REFNO");
												$ACBENEF 			= $filter->filter($benefAcct, "ACCOUNT_NO");
												$ACBENEF_BANKNAME 	= $filter->filter('', "ACCOUNT_NAME");
												$ACBENEF_ALIAS 		= $filter->filter('', "ACCOUNT_ALIAS");
												$ACBENEF_EMAIL 		= $fullDesc['BENEFICIARY_EMAIL'];
												$ACBENEF_PHONE 		= $fullDesc['BENEFICIARY_MOBILE_PHONE_NUMBER'];
												$ACBENEF_CCY 		= $filter->filter($ccy, "SELECTION");
												$ACBENEF_ADDRESS	= $fullDesc['BENEFICIARY_ADDRESS'];
												$BENEFICIARY_ADDRESS2= $fullDesc['BENEFICIARY_ADDRESS2'];
												
												$BENEFICIARY_ID_NUMBER	= $fullDesc['BENEFICIARY_ID_NUMBER'];
												//$BENEFICIARY_ADDRESS2	= $fullDesc['BENEFICIARY_ADDRESS2'];
												$ACBENEF_CITIZENSHIP= $fullDesc['BENEFICIARY_CITIZENSHIP'];
												$BENEFICIARY_RESIDENT= $fullDesc['BENEFICIARY_RESIDENT'];
												$BENEFICIARY_CATEGORY= $fullDesc['BENEFICIARY_CATEGORY'];
												$BENEFICIARY_ID_TYPE= $fullDesc['BENEFICIARY_ID_TYPE'];
												$BENEFICIARY_CITY= $fullDesc['BENEFICIARY_CITY'];
												//$BENEFICIARY_ID_TYPE= $fullDesc['BENEFICIARY_ID_TYPE'];
												//$BENEFICIARY_ID_TYPE= $fullDesc['BENEFICIARY_ID_TYPE'];
												//$ACBENEF_RESIDENT= $filter->filter($resident, "SELECTION");
												//$BANK_NAME			= $filter->filter($bankName, "BANK_NAME");
												//$BANK_CITY			= $filter->filter($bankCity, "ADDRESS");
												$CLR_CODE			= $filter->filter($bankCode, "BANK_CODE");
												$TRANSFER_TYPE 		= $fullDesc['TRANSFER_TYPE'];
												$ACCTSRC = 				$dataTemp['SOURCE_ACCOUNT'];

								$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

								if($TRANSFER_TYPE == 1){
									$TRANSFER_TYPE = 'RTGS';
								}else if($TRANSFER_TYPE == 2){
									$TRANSFER_TYPE = 'SKN';
								}else if($TRANSFER_TYPE == 5){
									$TRANSFER_TYPE = 'ONLINE';
								}else if($TRANSFER_TYPE == '0'){
									$TRANSFER_TYPE = 'PB';
								}
								//else{
								//	$TRANSFER_TYPE = $params['TRANSFER_TYPE']; 
								//}
									
								$selectcust = $db->select()
													->from('M_CUSTOMER',array('CUST_CHARGESID'))
													->where("CUST_ID = ?",$dataTemp['CUST_ID']);
									$custdata = $db->fetchrow($selectcust);
									$chrgdatainhouse = array();
									if(!empty($custdata)){
											$selectchrginhouse = $db->select()
													->from('M_CHARGES_WITHIN',array('CUST_ID','AMOUNT'))
													->where("CHARGES_ID = ?",$custdata['CUST_CHARGESID']);
											$chrgdatainhouse = $db->fetchrow($selectchrginhouse);
											
											
									}else{
										$chrgdatainhouse['CUST_ID'] = 'GLOBAL';
									}
									

								if($TRANSFER_TYPE == 'RTGS'){
									$chargeType = '1';
									$select = $db->select()
													->from('M_CHARGES_OTHER',array('*'))
													->where("CUST_ID = ?",$chrgdatainhouse['CUST_ID'])
													->where("CHARGES_TYPE = ?",$chargeType);
									$resultSelecet = $db->FetchAll($select);
									$chargeAmt = $resultSelecet['0']['CHARGES_AMT'];

									//$param['TRANSFER_FEE'] = $chargeAmt;
								}
								else if($TRANSFER_TYPE == 'SKN'){
									$chargeType1 = '2';
									$select1 = $db->select()
													->from('M_CHARGES_OTHER',array('*'))
													->where("CUST_ID = ?",$chrgdatainhouse['CUST_ID'])
													->where("CHARGES_TYPE = ?",$chargeType1);
									$resultSelecet1 = $db->FetchAll($select1);
									$chargeAmt = $resultSelecet1['0']['CHARGES_AMT'];

									//$param['TRANSFER_FEE'] = $chargeAmt1;
								}else if($TRANSFER_TYPE == 'ONLINE'){
									$chargeType1 = '8';
									$select1 = $db->select()
													->from('M_CHARGES_OTHER',array('*'))
													->where("CUST_ID = ?",$chrgdatainhouse['CUST_ID'])
													->where("CHARGES_TYPE = ?",$chargeType1);
									$resultSelecet1 = $db->FetchAll($select1);
									$chargeAmt = $resultSelecet1['0']['CHARGES_AMT'];

									//$param['TRANSFER_FEE'] = $chargeAmt1;
								}
								else{
									$chargeAmt = '0';
									//$param['TRANSFER_FEE'] = $chargeAmt2;
								}
												
												

												$filter->__destruct();
												unset($filter);
			//die('here');
												$dataSource = $db->fetchrow(
													$db->select(array('BENEFICIARY_NAME','BENEFICIARY_ALIAS'))
														->FROM('M_BENEFICIARY')
														->WHERE('CUST_ID = ?',$dataTemp['CUST_ID'])
														->WHERE('BENEFICIARY_ACCOUNT = ?',$ACBENEF)
														
												);
												$paramTrx = array("TRANSFER_TYPE" 			=> $TRANSFER_TYPE,
																	"TRA_AMOUNT" 				=> $TRA_AMOUNT_num,
																	"TRANSFER_FEE" 				=> $chargeAmt,
																	"TRA_MESSAGE" 				=> $TRA_MESSAGE,
																	"TRA_REFNO" 				=> $TRA_REFNO,
																	"ACCTSRC" 					=> $ACCTSRC,
																	"ACBENEF" 					=> $ACBENEF,
																	"ACBENEF_CCY" 				=> $ACBENEF_CCY,
																	"ACBENEF_EMAIL" 			=> $ACBENEF_EMAIL,
																	"ACBENEF_PHONE" 			=> $ACBENEF_PHONE,
																	"SMS_NOTIF"					=> $SMS_NOTIF,
																	"EMAIL_NOTIF"				=> $EMAIL_NOTIF,
																// for Beneficiary data, except (bene CCY and email), must be passed by reference
																	"ACBENEF_BANKNAME" 			=> $dataSource['BENEFICIARY_NAME'],
																	"ACBENEF_ALIAS" 			=> $dataSource['BENEFICIARY_ALIAS'],
																	"ACBENEF_CITIZENSHIP" 		=> $ACBENEF_CITIZENSHIP,		// W: WNI, N: WNA
																	"ACBENEF_RESIDENT" 			=> $BENEFICIARY_RESIDENT,		// R: RESIDENT, NR: NON-RESIDENT
																	"ACBENEF_ADDRESS1" 			=> $ACBENEF_ADDRESS,
																	"REFERENCE"					=> $reference,
																	"ACBENEF_ADDRESS2" 			=> $BENEFICIARY_ADDRESS2,
																	"ACBENEF_ADDRESS3" 			=> &$ACBENEF_ADDRESS3,

																//	"ORG_DIR" 					=> $ORG_DIR,
																	"BANK_CODE" 				=> $CLR_CODE,
																	"BENEFICIARY_CATEGORY" 				=> $dataSource['BENEFICIARY_CATEGORY'],
																	"BENEFICIARY_ID_TYPE" 				=> $BENEFICIARY_ID_TYPE,
																	"BENEFICIARY_ID_NUMBER" 				=> $BENEFICIARY_ID_NUMBER,
																	"LLD_CATEGORY" => $dataSource['BENEFICIARY_CATEGORY'],
																	"BENEFICIARY_CITY" 				=> $BENEFICIARY_CITY,
																//	"BENEFICIARY_ID_NUMBER" 				=> $BENEFICIARY_ID_NUMBER,
																//	"BANK_NAME" 				=> $BANK_NAME,
																//	"BANK_BRANCH" 				=> $BANK_BRANCH,
																//	"BANK_ADDRESS1" 			=> $BANK_ADDRESS1,
																//	"BANK_ADDRESS2" 			=> $BANK_ADDRESS2,
																//	"BANK_ADDRESS3" 			=> $BANK_ADDRESS3,
																 );
												$paramTrx['BENEFICIARY_ADDRESS'] 			= $ACBENEF_ADDRESS;
												//$paramTrx['BENEFICIARY_CITIZENSHIP'] 		= $ACBENEF_CITIZENSHIP;
												$paramTrx['BENEFICIARY_RESIDENT'] 			= $BENEFICIARY_RESIDENT;
												//$paramTrx['LLD_CATEGORY']		 			= $LLD_CATEGORY;
												//$paramTrx['LLD_IDENTICAL']		 			= $LLD_IDENTICAL;
											//	$paramTrx['LLD_RELATIONSHIP']		 		= $LLD_RELATIONSHIP;
											//	$paramTrx['LLD_PURPOSE']		 			= $LLD_PURPOSE;
											//	$paramTrx['LLD_DESCRIPTION']		 		= $LLD_DESCRIPTION;
												$paramTrx['LLD_BENEIDENTIF']		 		= $recreate['BENEFICIARY_ID_TYPE'];
												$paramTrx['LLD_BENENUMBER']		 		= $recreate['BENEFICIARY_ID_NUMBER'];
//												$paramTrx['LLD_SENDERIDENTIF']		 		= $LLD_SENDERIDENTIF;
	//											$paramTrx['LLD_SENDERNUMBER']		 		= $LLD_SENDERNUMBER;
												$paramTrx['CITY_CODE']		 				= $bankCity;				 

												array_push($paramTrxArr,$paramTrx);
						}
					}
					// kalo jumlah trx lebih dari setting
					// else
					// {
					// 	$error_msg[] = 'Error: The number of rows to be imported should not more than '.$maxrow.'.';
					// 	$this->view->error 		= true;
					// 	$this->view->report_msg	= $this->displayError($error_msg);
					// }

					//$confirm = true;

					//kalo gak ada error
					// if(!empty($error_msg))
					// {
						//echo '<pre>';
						$validate   = new ValidatePaymentSingle($dataTemp['CUST_ID'], $dataTemp['USER_ID']);
						//var_dump($paramTrxArr);
						//die;
						$resWs = array();
						$resultVal	= $validate->checkCreatePayroll($paramPayment, $paramTrxArr, $resWs);
						$payment 		= $validate->getPaymentInfo();
						
						$pstype = $dataTemp['PS_TYPE'];
						$bsid   = $dataTemp['BS_ID'];

						if($validate->isError() === false)	// payment data is valid
						{
							//die('tere');
							$confirm = true;
							$validate->__destruct();
							unset($validate);
						}
						else
						{
							
							$errorMsg 		= $validate->getErrorMsg();
					//		var_dump($errorMsg);
							$errorTrxMsg 	= $validate->getErrorTrxMsg();	// array
						//	var_dump($errorTrxMsg);
						//	die('tere1');
							

							foreach($errorTrxMsg as $key)
							{
								foreach($key as $ccy)
								{
									foreach ($ccy as $row => $val) {
										
										$trxvalidasi	= $db->select()
														->from(array('P' => 'TEMP_BULKTRANSACTION'), array('P.*'))
														// ->joinLeft(array('T' => 'TEMP_BULKTRANSACTION'), 'P.BS_ID = T.BS_ID', array('T.*'))
														// ->order('P.VALIDATION DESC')
														->where('P.BS_ID = ?',$bsid);
														//->where('DATE(P.PS_CREATED) = DATE(NOW())');

										$transactionvalidasi = $db->fetchAll($trxvalidasi);

										$sourceAccountFailed = $transactionvalidasi[$row]['TRANSACTION_ID'];
										
										$validated = array(	
											'VALIDATED'			=> 0,
											'ERROR_DESC'		=> $val,
										   );

										$where = array(
														'BS_ID = ?' => $bsid,
														'TRANSACTION_ID = ?' => $sourceAccountFailed);
										$db->update('TEMP_BULKTRANSACTION', $validated, $where);

									}
								}
							}

							$validate->__destruct();

							unset($validate);
							// echo "<pre>";
							// print_r($errorMsg);die;
							// Zend_Debug::dump($validate);die;
							// if($errorMsg)
							// {
							// 	$error_msg[] = 'Error: '.$errorMsg;
							// 	$this->view->error 		= true;
							// 	$this->view->report_msg	= $this->displayError($error_msg);
							// }
							// else
							// {
							$confirm = true;
							//}
						}

						// die;
					// }
				}
				// else //kalo total record = 0
				// {
				// 	$error_msg[] = $this->language->_('Error').': '.$this->language->_('Wrong File Formatss').'.';
				// 	$this->view->error 		= true;
				// 	$this->view->report_msg	= $this->displayError($error_msg);
				// }
			//akhir
			}
				
			if ($dataTemp['PS_TYPE'] == '5') {
				//awal

					$PS_FILE = $dataTemp['PS_FILE'];

					$imgsrc = LIBRARY_PATH.'/data/uploads/document/temp/'.$PS_FILE;
					$file_contents = file_get_contents($imgsrc);
					

					$file = explode(PHP_EOL, $file_contents);
					//$extension = 'txt';
					$delimitedWith = '|';
					$select = $db->select()
						->from(array('C' => 'M_CUST_ADAPTER_PROFILE'),array('*'))
						->join(array('A' => 'M_ADAPTER_PROFILE'), 'C.ADAPTER_PROFILE_ID = A.PROFILE_ID', '*')
						->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'A.PROFILE_ID = D.PROFILE_ID', '*')
						->where("CUST_ID = ?",$dataTemp['CUST_ID'])
						->where("TRF_TYPE = ?", 'manytoone');
					$adapterDataMto = $db->FetchAll($select);

					$fileName = $adapterDataMto[0]['FILE_PATH'];
			        $fixLength = $adapterDataMto[0]['FIXLENGTH'];
			        $fixLengthType = $adapterDataMto[0]['FIXLENGTH_TYPE'];
			        $fixLengthHeader = $adapterDataMto[0]['FIXLENGTH_HEADER_ORDER'];
			        $fixLengthHeaderName = $adapterDataMto[0]['FIXLENGTH_HEADER_NAME'];
			        $fixLengthContent = $adapterDataMto[0]['FIXLENGTH_CONTENT_ORDER'];
			        //$delimitedWith = $adapterDataMto[0]['DELIMITED_WITH'];
			        
					if (!empty($adapterDataMto)) {
						$extension = $adapterDataMto[0]['FILE_FORMAT'];
					}
					else{
						$extension = 'txt';	
					}
					
					$newFileName = $imgsrc;

					$data = convertFileToArray($newFileName, $extension, $delimitedWith);

					//@unlink($newFileName);
					
					if (!empty($adapterDataMto)) {
						//unset header if not fixlength
						if ($fixLength != 1) {
							unset($data[0]);
							unset($data[1]);	
						}
					}
					else{
						//unset defaults
						unset($data[0]);
						unset($data[1]);
						// unset($data[2]);
						// unset($data[3]);
						// unset($data[4]);
						// unset($data[5]);
						// unset($data[6]);
						// unset($data[7]);
					}

					$totalRecords = count($data);

					//proses convert ke order yg benar
					if($totalRecords)
					{

						foreach ($adapterDataMto as $key => $value) {
							$headerOrder[] = $value['HEADER_CONTENT'];
						}

						foreach ($data as $datakey => $datavalue) {

							if (!empty($headerOrder)) {
								foreach ($headerOrder as $key => $value) {

									$headerOrderArr = explode(',', $value);

									if (count($headerOrderArr) > 1) {
										$i = 0;
										$contentStr = '';
										foreach ($headerOrderArr as $key2 => $value2) {

											if ($i != count($headerOrderArr)) {
												$contentStr .= $data[$datakey][$value2].' ';
											}
											$i++;
										}	
										$newData[$datakey][] = $contentStr;
									}
									else{
										$newData[$datakey][] = $data[$datakey][$value];
									}
								}	
							}
							else{
								$newData[$datakey][] = null;
							}
						}
					}

					//check mandatory yang bo setup saat buat business adapter profile
					$mandatoryCheck = validateField($newData, $adapterDataMto);

					//auto mapping agar isi field file sesuai dengan yang di setup di m_mapping bo
					//$fixData = autoMapping($newData, $adapterDataMto);
				//	$fixData = $data;
					//var_dump($data);die('here');
					//if(empty($fixData)){
						$fixData = $data;
						$totalRecords = count($data);
					//}
					if($totalRecords)
					{
						if($totalRecords <= $maxrow)
						{
							// die('here');
							$rowNum = 0;

							$paramPayment = array( "CATEGORY"      		=> "BULK DEBET",
												   "FROM"       		=> "I",
												   "PS_NUMBER"     		=> "",
												   "PS_SUBJECT"    		=> $dataTemp['PS_SUBJECT'],
												   "PS_EFDATE"     		=> Application_Helper_General::convertDate($dataTemp['PS_EFDATE'], $dateDisplayFormat),
												   "PSFILEID"			=> $dataTemp['FILE_ID'],
												   "PS_FILE"     		=> $dataTemp['PS_FILE'],
												   "_dateFormat"    	=> $dateDisplayFormat,
												   "_dateDBFormat"    	=> $dateDBFormat,
												   "_addBeneficiary"   	=> true, // privi BADA (Add Beneficiary)
												   "_beneLinkage"    	=> true, // privi BLBU (Linkage Beneficiary User)
												   "_createPB"     		=> true, // privi IPMO (Create Bulk Debet Payment by Import File (CSV))
												   "_createDOM"    		=> false,        // cannot create DOM trx
												   "_createREM"    		=> false,        // cannot create REM trx
												  );

							$paramTrxArr = array();
							//var_dump($fixData)
							foreach ( $fixData as $key=>$row)
							{
								// var_dump(count($row));
								// if(count($row)==5)
								// {
									// die('here');
									$row = explode('|', $row[0]);
									$rowNum++;
									$sourceAcct 	= trim($row[1]);
									$ccy 			= "IDR";
									$amount 		= trim($row[2]);
									$purpose 		= '';
									$message 		= trim($row[3]);
									$cust_ref 		= '';
									$addMessage 	= trim($row[4]);

									// if(!empty($row[4]) || !empty($row[5])){
										$TRA_NOTIF			= '2';
									// }else{
									// 	$TRA_NOTIF			= '1';
									// }
									$TRA_SMS				= '';
									$TRA_EMAIL				= '';
									$REFRENCE				= '';


									$filter = new Application_Filtering();

									$TRA_AMOUNT 		= $filter->filter($amount, "AMOUNT");
									$TRA_MESSAGE 		= $filter->filter($message, "TRA_MESSAGE");
									$TRA_REFNO 			= $filter->filter($addMessage, "TRA_REFNO");
									// $ACCTSRC 			= $filter->filter($sourceAcct, "ACCOUNT_NO");
									$ACCTSRC 			= $sourceAcct;
									$TRANSFER_TYPE 		= 'PB';

									$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);

									$filter->__destruct();
									unset($filter);

									$paramTrx = array(	"TRANSFER_TYPE" 	=> $TRANSFER_TYPE,
														"TRA_AMOUNT" 		=> $TRA_AMOUNT_num,
														"TRA_MESSAGE" 		=> $TRA_MESSAGE,
														"TRA_REFNO" 		=> $TRA_REFNO,
														"ACCTSRC" 			=> $ACCTSRC,
														"ACBENEF" 			=> $dataTemp['BENEFICIARY_ACCOUNT'],
														"ACBENEF_CCY" 		=> $dataTemp['BENEFICIARY_ACCOUNT_CCY'],
														"ACBENEF_EMAIL" 	=> '',

													// for Beneficiary data, except (bene CCY and email), must be passed by reference
														"ACBENEF_BANKNAME" 			=> $dataTemp['BENEFICIARY_ACCOUNT_NAME'],
														"ACBENEF_ALIAS" 			=> $dataTemp['BENEFICIARY_ALIAS_NAME'],
														"PS_NOTIF"					=> $TRA_NOTIF,
														"CUST_REF"					=> $cust_ref,
														"PS_SMS"					=> $TRA_SMS,
														"PS_EMAIL"					=> $TRA_EMAIL,
														"REFRENCE"					=> $REFRENCE,
													//	"ACBENEF_CITIZENSHIP" 		=> $ACBENEF_CITIZENSHIP,		// 1/R: RESIDENT, 2/NR: NON-RESIDENT
													//	"ACBENEF_ADDRESS1" 			=> $ACBENEF_ADDRESS,
													//	"ACBENEF_ADDRESS2" 			=> &$ACBENEF_ADDRESS2,
													//	"ACBENEF_ADDRESS3" 			=> &$ACBENEF_ADDRESS3,

													//	"ORG_DIR" 					=> $ORG_DIR,
													//	"BANK_CODE" 				=> $CLR_CODE,
													//	"BANK_NAME" 				=> $BANK_NAME,
													//	"BANK_BRANCH" 				=> $BANK_BRANCH,
													//	"BANK_ADDRESS1" 			=> $BANK_ADDRESS1,
													//	"BANK_ADDRESS2" 			=> $BANK_ADDRESS2,
													//	"BANK_ADDRESS3" 			=> $BANK_ADDRESS3,
													 );

									array_push($paramTrxArr,$paramTrx);
								// }
								// else
								// {
								
								// 	$error_msg[] = $this->language->_('Wrong File Format').'';
								// 	$this->view->error 		= true;
								// 	$this->view->report_msg	= $this->displayError($error_msg);
								// 	break;
								// }
							}
						}
						// kalo jumlah trx lebih dari setting
						// else
						// {
						// 	// die('here1');
						// 	$error_msg[] = 'The number of rows to be imported should not more than '.$maxrow.'.';
						// 	$this->view->error 		= true;
						// 	$this->view->report_msg	= $this->displayError($error_msg);
						// }
						// print_r($error_msg);die;
						// kalo gak ada error

						// if(!empty($error_msg))
						// {
							$resWs = array();
							$validate   = new ValidatePaymentSingle($dataTemp['CUST_ID'], $dataTemp['USER_ID']);
							$resultVal	= $validate->checkCreate($paramPayment , $paramTrxArr, $resWs);

							$payment 		= $validate->getPaymentInfo();
							// var_dump($validate->isError()); die;

							$pstype = $dataTemp['PS_TYPE'];
							$bsid   = $dataTemp['BS_ID'];

							if($validate->isError() === false)	// payment data is valid
							{
								$confirm = true;

								$validate->__destruct();
								unset($validate);

							}
							else
							{
								//$errorMsg 		= $validate->getErrorMsg();
								$errorTrxMsg 	= $validate->getErrorTrxMsg();	// array
								//var_dump($errorTrxMsg);
								foreach($errorTrxMsg as $key)
								{
									foreach($key as $ccy)
									{
										foreach ($ccy as $row => $val) {
											
											$trxvalidasi	= $db->select()
															->from(array('P' => 'TEMP_BULKTRANSACTION'), array('P.*'))
															// ->joinLeft(array('T' => 'TEMP_BULKTRANSACTION'), 'P.BS_ID = T.BS_ID', array('T.*'))
															// ->order('P.VALIDATION DESC')
															->where('P.BS_ID = ?',$bsid);
															//->where('DATE(P.PS_CREATED) = DATE(NOW())');

											$transactionvalidasi = $db->fetchAll($trxvalidasi);

											$sourceAccountFailed = $transactionvalidasi[$row]['TRANSACTION_ID'];
											
											$validated = array(	
												'VALIDATED'			=> 0,
												'ERROR_DESC'		=> $val,
											   );

											$where = array(
															'BS_ID = ?' => $bsid,
															'TRANSACTION_ID = ?' => $sourceAccountFailed);
											$db->update('TEMP_BULKTRANSACTION', $validated, $where);

										}
									}
								}
								
								$confirm = true;
								
								$validate->__destruct();
								
								unset($validate);
								

								// if($errorMsg)
								// {
								// 	$error_msg[] = 'Error: '.$errorMsg;
								// 	$this->view->error 		= true;
								// 	$this->view->report_msg	= $this->displayError($error_msg);
								// }
								// else
								// {
								// 	$confirm = true;
								// }
							}
						//}
					}
					// else //kalo total record = 0
					// {
					// 	//$error_msg[] = 'Wrong File Format. There is no data on csv File.';
					// 	$error_msg[] = $this->language->_('Wrong File Format').'.';
					// 	$this->view->error 		= true;
					// 	$this->view->report_msg	= $this->displayError($error_msg);
					// }

				//akhir
			}

			if ($dataTemp['PS_TYPE'] == '4') {
				//awal

					$PS_FILE = $dataTemp['PS_FILE'];

					$imgsrc = LIBRARY_PATH.'/data/uploads/document/temp/'.$PS_FILE;
					$file_contents = file_get_contents($imgsrc);
					//var_dump($file_contents);die;
					$file = explode(PHP_EOL, $file_contents);
					
					$select = $db->select()
						->from(array('C' => 'M_CUST_ADAPTER_PROFILE'),array('*'))
						->join(array('A' => 'M_ADAPTER_PROFILE'), 'C.ADAPTER_PROFILE_ID = A.PROFILE_ID', '*')
						->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'A.PROFILE_ID = D.PROFILE_ID', '*')
						->where("CUST_ID = ?",$dataTemp['CUST_ID'])
						->where("TRF_TYPE = ?", 'onetomany');
					$adapterDataOtm = $db->FetchAll($select);

					//$extension = 'txt';
					$delimitedWith = '|';
					$fileName = $adapterDataOtm[0]['FILE_PATH'];
			        $fixLength = $adapterDataOtm[0]['FIXLENGTH'];
			        $fixLengthType = $adapterDataOtm[0]['FIXLENGTH_TYPE'];
			        $fixLengthHeader = $adapterDataOtm[0]['FIXLENGTH_HEADER_ORDER'];
			        $fixLengthHeaderName = $adapterDataOtm[0]['FIXLENGTH_HEADER_NAME'];
			        $fixLengthContent = $adapterDataOtm[0]['FIXLENGTH_CONTENT_ORDER'];
			        //$delimitedWith = $adapterDataOtm[0]['DELIMITED_WITH'];
					$newFileName = $imgsrc;
					
					if (!empty($adapterDataOtm)) {
						$extension = $adapterDataOtm[0]['FILE_FORMAT'];
					}
					else{
						$extension = 'txt';	
					}

					$data = convertFileToArray($newFileName, $extension, $delimitedWith);
					//var_dump($adapterDataOtm);die;
					//@unlink($newFileName);
					//end

					if (!empty($adapterDataOtm)) {
						//unset header if not fixlength
						if ($fixLength != 1) {
							unset($data[0]);
							unset($data[1]);	
						}
					}
					else{
						//unset defaults
						unset($data[0]);
						unset($data[1]);
						// unset($data[2]);
						// unset($data[3]);
						// unset($data[4]);
						// unset($data[5]);
						// unset($data[6]);
						// unset($data[7]);
					}

					$totalRecords = count($data);

					//proses convert ke order yg benar
					if($totalRecords)
					{

						foreach ($adapterDataOtm as $key => $value) {
							$headerOrder[] = $value['HEADER_CONTENT'];
						}

						foreach ($data as $datakey => $datavalue) {

							if (!empty($headerOrder)) {
								foreach ($headerOrder as $key => $value) {

									$headerOrderArr = explode(',', $value);

									if (count($headerOrderArr) > 1) {
										$i = 0;
										$contentStr = '';
										foreach ($headerOrderArr as $key2 => $value2) {

											if ($i != count($headerOrderArr)) {
												$contentStr .= $data[$datakey][$value2].' ';
											}
											$i++;
										}	
										$newData[$datakey][] = $contentStr;
									}
									else{
										$newData[$datakey][] = $data[$datakey][$value];
									}
								}	
							}
							else{
								$newData[$datakey][] = null;
							}
						}
					}

					//check mandatory yang bo setup saat buat business adapter profile
					$mandatoryCheck = validateField($newData, $adapterDataOtm);

					//auto mapping agar isi field file sesuai dengan yang di setup di m_mapping bo
						 
					//$fixData = autoMapping($newData, $adapterDataOtm);
					$fixData = $data;
					if($totalRecords)
					{
						if($totalRecords <= $maxrow)
						{
							//die('here');
							$rowNum = 0;

							$paramPayment = array( 	"CATEGORY"      	=> "BULK CREDITT",
													"FROM"       		=> "I",
													"PS_NUMBER"     	=> "",
													"PS_SUBJECT"   	 	=> $dataTemp['PS_SUBJECT'],
													"PS_EFDATE"     	=> Application_Helper_General::convertDate($dataTemp['PS_EFDATE'], $dateDisplayFormat),
													"PSFILEID"			=> $dataTemp['FILE_ID'],
													"PS_FILE"     		=> $dataTemp['PS_FILE'],
													"_dateFormat"    	=> $dateDisplayFormat,
													"_dateDBFormat"    	=> $dateDBFormat,
													"_addBeneficiary"   => true,
													"_beneLinkage"    	=> true,
													"_createPB"     	=> true,
													"_createDOM"    	=> true,
													"_createREM"    	=> false,
												  );

							$paramTrxArr = array();
							
							foreach ( $fixData as $row )
							{
								 //echo '<pre>';
								 
								 //print_r(count($row));
								// if(count($row)==8)
								// {
									$rowNum++;
									if (!empty($row) && empty($adapterDataOtm)) {
										$row = explode('|', $row[0]);
									}
									//var_dump($newrow);die('here');
									$benefAcct 	= trim($row[1]);
									$ccy 		= 'IDR';
									$amount 	= trim($row[2]);
									$purpose 	= '';
									$message 	= trim($row[5]);
									$addMessage = trim($row[6]);
									$type 		= trim($row[3]);
									$bankCode 	= trim($row[4]);
									$cust_ref 	= '';
									//var_dump($type);die;
									//$TRA_SMS					= trim($row[7]);
									$TRA_SMS	= '';
									$TRA_EMAIL	= '';
									$REFRENCE				= '';

									$fullDesc = array(
										'BENEFICIARY_ACCOUNT'		 => $benefAcct,
										'BENEFICIARY_ACCOUNT_CCY' 	 => $ccy,
										'TRA_AMOUNT' 				 => $amount,
										'TRA_MESSAGE' 				 => $message,
										'TRA_PURPOSE' 				 => $purpose,
										'REFNO'	 					 => $addMessage,
										'TRANSFER_TYPE' 			 => $type,
										'CLR_CODE' 					 => $bankCode,
										'CUST_REF'					 => $cust_ref,
									);

									$filter = new Application_Filtering();

									$TRA_AMOUNT 		= $filter->filter($amount, "AMOUNT");
									$TRA_MESSAGE 		= $filter->filter($message, "TRA_MESSAGE");
									$TRA_REFNO 			= $filter->filter($addMessage, "TRA_REFNO");
									$ACBENEF 			= $filter->filter($benefAcct, "ACCOUNT_NO");
									$ACBENEF_EMAIL 		= $filter->filter($email, "EMAIL");
									$ACBENEF_CCY 		= $filter->filter($ccy, "SELECTION");
									$ACBENEF_ADDRESS	= $filter->filter($bankCity, "ADDRESS");
									$CLR_CODE			= $filter->filter($bankCode, "BANK_CODE");
									$TRANSFER_TYPE 		= $type;
									$TRANS_PURPOSE 		= $filter->filter($purpose, "SELECTION");
									$CUST_REF 			= $filter->filter($cust_ref, "CUST_REF");
									$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);
									//var_dump($fullDesc);die;
									if($TRANSFER_TYPE == 1){
									$TRANSFER_TYPE = 'RTGS';
								}else if($TRANSFER_TYPE == 2){
									$TRANSFER_TYPE = 'SKN';
								}else if($TRANSFER_TYPE == 5){
									$TRANSFER_TYPE = 'ONLINE';
								}else if($TRANSFER_TYPE == '0'){
									$TRANSFER_TYPE = 'PB';
								}
								//else{
								//	$TRANSFER_TYPE = $params['TRANSFER_TYPE']; 
								//}
									
								$selectcust = $db->select()
													->from('M_CUSTOMER',array('CUST_CHARGESID'))
													->where("CUST_ID = ?",$dataTemp['CUST_ID']);
									$custdata = $db->fetchrow($selectcust);
									$chrgdatainhouse = array();
									if(!empty($custdata)){
											$selectchrginhouse = $db->select()
													->from('M_CHARGES_WITHIN',array('CUST_ID','AMOUNT'))
													->where("CHARGES_ID = ?",$custdata['CUST_CHARGESID']);
											$chrgdatainhouse = $db->fetchrow($selectchrginhouse);
											
											
									}else{
										$chrgdatainhouse['CUST_ID'] = 'GLOBAL';
									}
									

								if($TRANSFER_TYPE == 'RTGS'){
									$chargeType = '1';
									$select = $db->select()
													->from('M_CHARGES_OTHER',array('*'))
													->where("CUST_ID = ?",$chrgdatainhouse['CUST_ID'])
													->where("CHARGES_TYPE = ?",$chargeType);
									$resultSelecet = $db->FetchAll($select);
									$chargeAmt = $resultSelecet['0']['CHARGES_AMT'];

									//$param['TRANSFER_FEE'] = $chargeAmt;
								}
								else if($TRANSFER_TYPE == 'SKN'){
									$chargeType1 = '2';
									$select1 = $db->select()
													->from('M_CHARGES_OTHER',array('*'))
													->where("CUST_ID = ?",$chrgdatainhouse['CUST_ID'])
													->where("CHARGES_TYPE = ?",$chargeType1);
									$resultSelecet1 = $db->FetchAll($select1);
									$chargeAmt = $resultSelecet1['0']['CHARGES_AMT'];

									//$param['TRANSFER_FEE'] = $chargeAmt1;
								}else if($TRANSFER_TYPE == 'ONLINE'){
									$chargeType1 = '8';
									$select1 = $db->select()
													->from('M_CHARGES_OTHER',array('*'))
													->where("CUST_ID = ?",$chrgdatainhouse['CUST_ID'])
													->where("CHARGES_TYPE = ?",$chargeType1);
									$resultSelecet1 = $db->FetchAll($select1);
									$chargeAmt = $resultSelecet1['0']['CHARGES_AMT'];

									//$param['TRANSFER_FEE'] = $chargeAmt1;
								}
								else{
									$chargeAmt = '0';
									//$param['TRANSFER_FEE'] = $chargeAmt2;
								}

									$filter->__destruct();
									unset($filter);
									if($accsccy != 'IDR' ){
										
										$TRA_AMOUNT_NET =	$TRA_AMOUNT_num;
										$TRA_AMOUNT_EQ	=  $TRA_AMOUNT_num/$kurs;
									}else{
										$TRA_AMOUNT_NET = $TRA_AMOUNT_num;
										$TRA_AMOUNT_EQ = $TRA_AMOUNT_num;
									}

									$paramTrx = array("TRANSFER_TYPE" 			=> $TRANSFER_TYPE,
														"TRA_AMOUNT" 				=> $TRA_AMOUNT_NET,
														"TRA_AMOUNTEQ" 				=> $TRA_AMOUNT_EQ,
														"TRANSFER_FEE" 				=> $chargeAmt,
														"TRA_MESSAGE" 				=> $TRA_MESSAGE,
														"TRA_REFNO" 				=> $TRA_REFNO,
														"ACCTSRC" 					=> $dataTemp['SOURCE_ACCOUNT'],
														"ACBENEF" 					=> $ACBENEF,
														"ACBENEF_CCY" 				=> $ACBENEF_CCY,
														"ACBENEF_EMAIL" 			=> $ACBENEF_EMAIL,
														"BENEFICIARY_RESIDENT" 		=> $BENEFICIARY_RESIDENT,
														"ACBENEF_CITIZENSHIP" 		=> $ACBENEF_CITIZENSHIP,
														"BANK_CODE" 				=> $CLR_CODE,
														"BENEFICIARY_ID_NUMBER" 	=> $BENEFICIARY_ID_NUMBER,
														"BENEFICIARY_ID_TYPE" 		=> $BENEFICIARY_ID_TYPE,
														"BENEFICIARY_CITY_CODE" 	=> $BENEFICIARY_CITY_CODE,
														"BENEFICIARY_CATEGORY" 		=> $BENEFICIARY_CATEGORY,
														"BANK_NAME" 				=> $BANK_NAME,
														"TRANS_PURPOSE"				=> $TRANS_PURPOSE,
														"PS_NOTIF"					=> $TRA_NOTIF,
														"CUST_REF"					=> $CUST_REF,
														"PS_SMS"					=> $TRA_SMS,
														"PS_EMAIL"					=> $TRA_EMAIL,
														"REFRENCE"					=> $REFRENCE,

													 );
									if($accsccy != 'IDR' ){
										$paramTrx['RATE'] 		=	$kurssell;
										$paramTrx['RATE_BUY'] 	=	$kurs;
										$paramTrx['BOOKRATE'] 	=	$book;
									}
								
									array_push($paramTrxArr,$paramTrx);
									
								// }
								// else
								// {
								// //	die('here');
								// 	$error_msg[] = $this->language->_('Error').': '.$this->language->_('Wrong File Format').'.';
								// 	$this->view->error 		= true;
								// 	$this->view->report_msg	= $this->displayError($error_msg);
									
									
								// 	break;
								// }
							}
						}

						// else
						// {
						// 	$error_msg[] = 'Error: The number of rows to be imported should not more than '.$maxrow.'.';
						// 	$this->view->error 		= true;
						// 	$this->view->report_msg	= $this->displayError($error_msg);
						// }
							
						// if(!empty($error_msg))
						// {

							$resWs = array();

							$validate   = new ValidatePaymentSingle($dataTemp['CUST_ID'], $dataTemp['USER_ID']);
							$resWs = array(); 
							//var_dump($paramTrxArr);die; 
							$resultVal	= $validate->checkCreateBulk($paramPayment, $paramTrxArr,$resWs);
							//$resultVal	= true;
							$payment 		= $validate->getPaymentInfo();

							$pstype = $dataTemp['PS_TYPE'];
							$bsid   = $dataTemp['BS_ID'];
							// Zend_Debug::dump($validate->getErrorMsg(),'err');
							// Zend_Debug::dump($validate->getErrorTrxMsg(),'errT');
							// die('asd');
							
							if($validate->isError() === false)	// payment data is valid
							{

								$confirm = true;

								$validate->__destruct();
								unset($validate);
							}
							else
							{
								 $errorMsg 		= $validate->getErrorMsg();
								$errorTrxMsg 	= $validate->getErrorTrxMsg();	// array
								//var_dump($errorTrxMsg);
								//var_dump($errorMsg);die;

								foreach($errorTrxMsg as $key)
								{
									foreach($key as $ccy)
									{
										foreach ($ccy as $row => $val) {
											
											$trxvalidasi	= $db->select()
															->from(array('P' => 'TEMP_BULKTRANSACTION'), array('P.*'))
															// ->joinLeft(array('T' => 'TEMP_BULKTRANSACTION'), 'P.BS_ID = T.BS_ID', array('T.*'))
															// ->order('P.VALIDATION DESC')
															->where('P.BS_ID = ?',$bsid);
															//->where('DATE(P.PS_CREATED) = DATE(NOW())');

											$transactionvalidasi = $db->fetchAll($trxvalidasi);

											$sourceAccountFailed = $transactionvalidasi[$row]['TRANSACTION_ID'];
											
											$validated = array(	
												'VALIDATED'			=> 0,
												'ERROR_DESC'		=> $val,
											   );

											$where = array(
															'BS_ID = ?' => $bsid,
															'TRANSACTION_ID = ?' => $sourceAccountFailed);
											$db->update('TEMP_BULKTRANSACTION', $validated, $where);

										}
									}
								}

								$confirm = true;
								$validate->__destruct();

								unset($validate);
								 //print_r($);
								 
								 //Zend_Debug::dump($errorMsg);die;
								// if($errorMsg)
								// {
								// 	$confirm = true;
								// 	$this->view->PSEFDATE	= $PS_EFDATE;
								// 	$this->view->BULK_TYPE	= $BULK_TYPE;
								// 	$error_msg[] = 'Error: '.$errorMsg;
								// 	$this->view->error 		= true;
								// 	$this->view->report_msg	= $this->displayError($error_msg);
								// }
								// else
								// {
								// 	$confirm = true;
								// }
							}

						//}
					}
					// else
					// {
					// 	$error_msg[] = $this->language->_('Error').': '.$this->language->_('Wrong File Format').'.';
					// 	$this->view->error 		= true;
					// 	$this->view->report_msg	= $this->displayError($error_msg);
					// }

				//akhir
			}
		
			if ($dataTemp['PS_TYPE'] == '22') {
			//awal

				$PS_FILE = $dataTemp['PS_FILE'];
				$PSFILEID = $dataTemp['FILE_ID'];

		        $imgsrc = LIBRARY_PATH.'/data/uploads/document/temp/'.$PS_FILE;
				$file_contents = file_get_contents($imgsrc);
				$select = $db->select()
						->from(array('C' => 'M_CUST_ADAPTER_PROFILE'),array('*'))
						->join(array('A' => 'M_ADAPTER_PROFILE'), 'C.ADAPTER_PROFILE_ID = A.PROFILE_ID', '*')
						->join(array('D' => 'M_ADAPTER_PROFILE_DETAIL'), 'A.PROFILE_ID = D.PROFILE_ID', '*')
						->where("CUST_ID = ?",$dataTemp['CUST_ID'])
						->where("TRF_TYPE = ?", 'manytomany');
				$adapterDataMtm = $db->FetchAll($select);
		
				$file = explode(PHP_EOL, $file_contents);
				//$extension = 'txt';
				$delimitedWith = '|';

				$fileName = $adapterDataMtm[0]['FILE_PATH'];
		        $fixLength = $adapterDataMtm[0]['FIXLENGTH'];
		        $fixLengthType = $adapterData[0]['FIXLENGTH_TYPE'];
		        $fixLengthHeader = $adapterDataMtm[0]['FIXLENGTH_HEADER_ORDER'];
		        $fixLengthHeaderName = $adapterDataMtm[0]['FIXLENGTH_HEADER_NAME'];
		        $fixLengthContent = $adapterDataMtm[0]['FIXLENGTH_CONTENT_ORDER'];
		        //$delimitedWith = $adapterDataMtm[0]['DELIMITED_WITH'];
				
				$newFileName = $imgsrc;
				
				if (!empty($adapterDataMtm)) {
						$extension = $adapterDataMtm[0]['FILE_FORMAT'];
					}
					else{
						$extension = 'txt';	
					}

				$filter 	= new Application_Filtering();
				$setting = new Settings();
				$max		= $setting->getSetting('max_import_single_payment');
				//var_dump($dataTemp['PS_TYPE']);die;
				$data = convertFileToArray($newFileName, $extension, $delimitedWith);

				//@unlink($newFileName);
				//echo '<pre>';
				
				if (!empty($adapterDataMtm)) {
					//unset header if not fixlength
					if ($fixLength != 1) {
						unset($data[0]);
						unset($data[1]);	
					}
				}
				else{
					//unset defaults
					unset($data[0]);
					unset($data[1]);
					// unset($data[2]);
					// unset($data[3]);
					// unset($data[4]);
					// unset($data[5]);
					// unset($data[6]);
					// unset($data[7]);
				}
				
				$totalRecords = count($data);

				//proses convert ke order yg benar
				if($totalRecords && !empty($adapterDataMtm))
				{

					foreach ($adapterDataMtm as $key => $value) {
						$headerOrder[] = $value['HEADER_CONTENT'];
					}

					foreach ($data as $datakey => $datavalue) {

						if (!empty($headerOrder)) {
							foreach ($headerOrder as $key => $value) {

								$headerOrderArr = explode(',', $value);

								if (count($headerOrderArr) > 1) {
									$i = 0;
									$contentStr = '';
									foreach ($headerOrderArr as $key2 => $value2) {

										if ($i != count($headerOrderArr)) {
											$contentStr .= $data[$datakey][$value2].' ';
										}
										$i++;
									}	
									$newData[$datakey][] = $contentStr;
								}
								else{
									$newData[$datakey][] = $data[$datakey][$value];
								}
							}	
						}
						else{
							$newData[$datakey][] = null;
						}
					}

					//check mandatory yang bo setup saat buat business adapter profile
					$mandatoryCheck = validateField($newData, $adapterDataMtm);

					//auto mapping agar isi field file sesuai dengan yang di setup di m_mapping bo
					$fixData = autoMapping($newData, $adapterDataMtm);

				}
				//var_dump($data);die('gh'); 
				if(empty($fixData)){
					$fixData = $data;
					$totalRecords = count($data);
				}
				
				if ($totalRecords && empty($mandatoryCheck)){
					if($totalRecords <= $max)
					{
						$no =0;
						$paramTrxArray = array();
				
						foreach ( $fixData as $columns )
						{
							// if(count($columns)==14)
							// {
								if (!empty($columns) && empty($adapterDataMtm)) {
									$columns = explode('|', $columns[0]);
								}
								
								$params['PAYMENT_SUBJECT'] 			= trim($columns[1]);
								$params['SOURCE_ACCT_NO'] 			= trim($columns[2]);
								$params['CCY'] 						= trim('IDR');
								$params['BENEFICIARY_ACCT_NO'] 		= trim($columns[3]);
								$params['BENEFICIARY_ACCT_CCY'] 	= trim('IDR');
								$params['AMOUNT'] 					= trim($columns[4]);
								$params['MESSAGE'] 					= trim($columns[8]);
								$params['TRANS_PURPOSE'] 			= '';
								$params['ADDITIONAL_MESSAGE'] 		= trim($columns[9]);
								// $origDate = trim($columns[8]);
								// $date = str_replace('/', '-', $origDate );
								// $date2 = date_create($date);
								$params['PAYMENT_DATE'] 			= trim($columns[7]);
								$params['TRANSFER_TYPE'] 			= trim($columns[5]);
								//var_dump($params['TRANSFER_TYPE']);
								$params['BANK_CODE'] 				= trim($columns[6]);
								$params['CUST_REF']					= '';
								
								// if(!empty($columns[12]) || !empty($columns[13])){
								// 	$params['TRA_NOTIF']			= '2';
								// }else{
								// 	$params['TRA_NOTIF']			= '1';
								// }
								$params['BENEFICIARY_NAME']				= '';
								$params['BENEFICIARY_NAME']				= '';
								$params['TRA_NOTIF']				= '';
								$params['PS_SMS']					= '';
								$params['PS_EMAIL']					= '';
								$params['TREASURY_NUM']				= '';
								// $params['LLD_TRANSACTION_PURPOSE']  = trim($columns[14]);
								$params['LLD_TRANSACTION_PURPOSE']  = '';
				
								$PS_SUBJECT 		= $filter->filter($params['PAYMENT_SUBJECT'],"PS_SUBJECT");
								$PS_EFDATE 			= $filter->filter($params['PAYMENT_DATE'],"PS_DATE");
								$TRA_AMOUNT 		= $filter->filter($params['AMOUNT'],"AMOUNT");
								$TRA_MESSAGE 		= $filter->filter($params['MESSAGE'],"TRA_MESSAGE");
								$TRA_REFNO 			= $filter->filter($params['ADDITIONAL_MESSAGE'],"TRA_REFNO");
								$ACCTSRC 			= $filter->filter($params['SOURCE_ACCT_NO'],"ACCOUNT_NO");
								$ACBENEF 			= $filter->filter($params['BENEFICIARY_ACCT_NO'],"ACCOUNT_NO");
								$ACBENEF_BANKNAME 	= $filter->filter($dataTemp['BENEFICIARY_NAME'],"ACCOUNT_NAME");
								$ACBENEF_CCY 		= $filter->filter($params['CCY'],"SELECTION");
								$CLR_CODE			= $filter->filter($params['BANK_CODE'], "BANK_CODE");
								//var_dump($params['TRANSFER_TYPE']);
								$TRANSFER_TYPE 		= $params['TRANSFER_TYPE'];
								$CUST_REF 			= $filter->filter($dataTemp['CUST_REF'], "SELECTION");
								$BENEFICIARY_ACCT_CCY			= $filter->filter($dataTemp['BENEFICIARY_ACCOUNT_CCY'], "BENEFICIARY_ACCT_CCY");
								$TRA_NOTIF			= $filter->filter($dataTemp['TRA_NOTIF'], "TRA_NOTIF");
								$TRA_SMS			= $filter->filter($dataTemp['PS_SMS'], "PS_SMS");
								$TRA_EMAIL			= $filter->filter($dataTemp['PS_EMAIL'], "PS_EMAIL");
								$TREASURY_NUM		= $filter->filter($dataTemp['TREASURY_NUM'], "TREASURY_NUM");
								$LLD_TRANSACTION_PURPOSE		= $filter->filter($dataTemp['LLD_TRANSACTION_PURPOSE'], "LLD_TRANSACTION_PURPOSE");

								$TRA_AMOUNT_num 	= Application_Helper_General::convertDisplayMoney($TRA_AMOUNT);
								//var_dump($TRANSFER_TYPE);
								if($TRANSFER_TYPE == 1){
									$TRANSFER_TYPE = 'RTGS';
								}else if($TRANSFER_TYPE == 2){
									$TRANSFER_TYPE = 'SKN';
								}else if($TRANSFER_TYPE == 5){
									$TRANSFER_TYPE = 'ONLINE';
								}else if($TRANSFER_TYPE == '0'){
									$TRANSFER_TYPE = 'PB';
								}
								//else{
								//	$TRANSFER_TYPE = $params['TRANSFER_TYPE']; 
								//}
									
								$selectcust = $db->select()
													->from('M_CUSTOMER',array('CUST_CHARGESID'))
													->where("CUST_ID = ?",$dataTemp['CUST_ID']);
									$custdata = $db->fetchrow($selectcust);
									$chrgdatainhouse = array();
									if(!empty($custdata)){
											$selectchrginhouse = $db->select()
													->from('M_CHARGES_WITHIN',array('CUST_ID','AMOUNT'))
													->where("CHARGES_ID = ?",$custdata['CUST_CHARGESID']);
											$chrgdatainhouse = $db->fetchrow($selectchrginhouse);
											
											
									}else{
										$chrgdatainhouse['CUST_ID'] = 'GLOBAL';
									}
									

								if($TRANSFER_TYPE == 'RTGS'){
									$chargeType = '1';
									$select = $db->select()
													->from('M_CHARGES_OTHER',array('*'))
													->where("CUST_ID = ?",$chrgdatainhouse['CUST_ID'])
													->where("CHARGES_TYPE = ?",$chargeType);
									$resultSelecet = $db->FetchAll($select);
									$chargeAmt = $resultSelecet['0']['CHARGES_AMT'];

									//$param['TRANSFER_FEE'] = $chargeAmt;
								}
								else if($TRANSFER_TYPE == 'SKN'){
									$chargeType1 = '2';
									$select1 = $db->select()
													->from('M_CHARGES_OTHER',array('*'))
													->where("CUST_ID = ?",$chrgdatainhouse['CUST_ID'])
													->where("CHARGES_TYPE = ?",$chargeType1);
									$resultSelecet1 = $db->FetchAll($select1);
									$chargeAmt = $resultSelecet1['0']['CHARGES_AMT'];

									//$param['TRANSFER_FEE'] = $chargeAmt1;
								}else if($TRANSFER_TYPE == 'ONLINE'){
									$chargeType1 = '8';
									$select1 = $db->select()
													->from('M_CHARGES_OTHER',array('*'))
													->where("CUST_ID = ?",$chrgdatainhouse['CUST_ID'])
													->where("CHARGES_TYPE = ?",$chargeType1);
									$resultSelecet1 = $db->FetchAll($select1);
									$chargeAmt = $resultSelecet1['0']['CHARGES_AMT'];

									//$param['TRANSFER_FEE'] = $chargeAmt1;
								}
								else{
									$chargeAmt = '0';
									//$param['TRANSFER_FEE'] = $chargeAmt2;
								}


								if($BENEFICIARY_ACCT_CCY != $ACBENEF_CCY){
									$CROSS_CURR = '2';
								}else{
									$CROSS_CURR = '1';
								}

								$paramPayment = array(
										"CATEGORY" 					=> "SINGLE PAYMENT",
										"FROM" 						=> "I",				// F: Form, I: Import
										"PS_NUMBER"					=> "",
										"PS_SUBJECT"				=> $PS_SUBJECT,
										"PS_EFDATE"					=> $PS_EFDATE,
										"PSFILEID"					=> $PSFILEID,
										"PS_FILE"     				=> $PS_FILE,
										"_dateFormat"				=> 'yyyy-MM-dd',
										"_dateDBFormat"				=> $dateDBFormat,
										"_addBeneficiary"			=> true,	// privi BADA (Add Beneficiary)
										"_beneLinkage"				=> true,	// privi BLBU (Linkage Beneficiary User)
										"_createPB"					=> true,								// cannot create PB trx
										"_createDOM"				=> true,	// privi CDFT (Create Domestic Fund Transfer)
										"_createREM"				=> true,								// cannot create REM trx
										"TRA_CCY"					=> $BENEFICIARY_ACCT_CCY,
										"CROSS_CURR"				=> $CROSS_CURR
								);
								
								$paramTrxArr[0] = array(
										"TRANSFER_TYPE" 			=> $TRANSFER_TYPE,
										"TRA_AMOUNT" 				=> $TRA_AMOUNT_num,
										"TRANSFER_FEE" 				=> $chargeAmt,
										"TRA_MESSAGE" 				=> $TRA_MESSAGE,
										"TRA_REFNO" 				=> $TRA_REFNO,
										"ACCTSRC" 					=> $ACCTSRC,
										"ACBENEF" 					=> $ACBENEF,
										"ACBENEF_CCY" 				=> $ACBENEF_CCY,
										"ACBENEF_EMAIL" 			=> $ACBENEF_EMAIL,
										"ACBENEF_BANKNAME" 			=> $ACBENEF_BANKNAME,
										
//												"ACBENEF_ALIAS" 			=> $ACBENEF_ALIAS,
										"ACBENEF_CITIZENSHIP" 		=> $ACBENEF_CITIZENSHIP,		// 1/R: RESIDENT, 2/NR: NON-RESIDENT
										// 																"ACBENEF_ADDRESS1" 			=> $BANK_CITY,
//												"ACBENEF_RESIDENT" 			=> $ACBENEF_RESIDENT,
										"CUST_REF"					=> $CUST_REF,
										"BENEFICIARY_RESIDENT" 		=> $BENEFICIARY_RESIDENT,
										"BANK_CODE" 				=> $CLR_CODE,
//												"BENEFICIARY_BANK_NAME"		=> $BANK_NAME,
//												"LLD_IDENTICAL" 			=> "",
//												"LLD_CATEGORY" 				=> "",
//												"LLD_RELATIONSHIP" 			=> "",
//												"LLD_PURPOSE" 				=> "",
//												"LLD_DESCRIPTION" 			=> "",
										"LLD_TRANSACTION_PURPOSE"	=> $LLD_TRANSACTION_PURPOSE,
										"BENEFICIARY_ID_NUMBER" 	=> $BENEFICIARY_ID_NUMBER,
										"BENEFICIARY_ID_TYPE" 		=> $BENEFICIARY_ID_TYPE,
										"BENEFICIARY_CITY_CODE" 	=> $BENEFICIARY_CITY_CODE,
										"BENEFICIARY_ACCT_CCY" 		=> $BENEFICIARY_ACCT_CCY,
										"PS_NOTIF" 				=> $TRA_NOTIF,
										"PS_SMS" 					=> $TRA_SMS,
										"PS_EMAIL" 				=> $TRA_EMAIL,
										"REFERENCE" 				=> $TREASURY_NUM,

										"BANK_NAME" 	=> $BANK_NAME,

								);
								//var_dump($paramTrxArr);
								$arr[$no]['paramPayment'] = $paramPayment;
								$arr[$no]['paramTrxArr'] = $paramTrxArr;

							// }
							// else
							// {
							// 	// die('ge');
							// 	$this->view->error 		= true;
							// 	break;
							// }
							$no++;
						}
//die;
						// if(!$this->view->error)
						// {
							$resWs = array();
							$err 	= array();
							//var_dump($arr);die;
							$validate   = new ValidatePaymentMultiple($dataTemp['CUST_ID'], $dataTemp['USER_ID']);
							$resultVal	= $validate->checkCreate($arr, $resWs);
							$payment 	= $validate->getPaymentInfo();
							$errorTrxMsg 	= $validate->getErrorTrxMsg();
							var_dump($errorTrxMsg);

							$pstype = $dataTemp['PS_TYPE'];
							$bsid   = $dataTemp['BS_ID'];

							if($validate->isError() === false)	// payment data is valid
							{

								$confirm = true;

								$validate->__destruct();
								unset($validate);
							}
							else
							{


								$errorTrxMsg 	= $validate->getErrorTrxMsg();	// array
								//var_dump($errorTrxMsg);die;
								foreach($errorTrxMsg as $key)
								{
									foreach($key as $ccy)
									{
										foreach ($ccy as $row => $val) {
											
											$trxvalidasi	= $db->select()
															->from(array('P' => 'TEMP_BULKTRANSACTION'), array('P.*'))
															// ->joinLeft(array('T' => 'TEMP_BULKTRANSACTION'), 'P.BS_ID = T.BS_ID', array('T.*'))
															// ->order('P.VALIDATION DESC')
															->where('P.BS_ID = ?',$bsid);
															//->where('DATE(P.PS_CREATED) = DATE(NOW())');

											$transactionvalidasi = $db->fetchAll($trxvalidasi);

											$sourceAccountFailed = $transactionvalidasi[$row]['TRANSACTION_ID'];
											
											$validated = array(	
												'VALIDATED'			=> 0,
												'ERROR_DESC'		=> $val,
											   );

											$where = array(
															'BS_ID = ?' => $bsid,
															'TRANSACTION_ID = ?' => $sourceAccountFailed);
											$db->update('TEMP_BULKTRANSACTION', $validated, $where);

										}
									}
								}

								$confirm = true;

								$validate->__destruct();
								unset($validate);

							}

							// $i = 0;
							// foreach($resWs as $key=>$dataAcctType){
							// 	//Zend_Debug::dump($dataAcctType);
							// 	$arr[$key]['paramTrxArr'][0]['ACCOUNT_TYPE'] = $dataAcctType['accountType'];
							// }

							// $sourceAccountType 	= $resWs['accountType'];

							// $content['payment'] = $payment;
							// $content['arr'] 	= $arr;
							// $content['errorTrxMsg'] 	= $errorTrxMsg;
							// $content['sourceAccountType'] 	= $sourceAccountType;
							// $content['paramPayment'] = $paramPayment;
							// $content['paramTrxArray'] = $paramTrxArray;

							// $sessionNamespace = new Zend_Session_Namespace('confirmImportCreditBatch');
							// $sessionNamespace->content = $content;

							// $this->_redirect('/singlepayment/importbatch/confirm');
						//}

					}
					// else
					// {
					// 	// die('here');
					// 	$this->view->error2 = true;
					// 	$this->view->max 	= $max;
					// }
				}
				// else{
				// 	// die('here1');
				// 	$this->view->error = true;

				// 	if (!empty($adapter->getMessages())) {
				// 		$error_msg = array($adapter->getMessages());
				// 	}

				// 	if (!empty($mandatoryCheck)) {
				// 		foreach ($mandatoryCheck as $key => $value) {
				// 			array_push($error_msg, $value.' Field Cannot be left blank');
				// 		}
				// 	}

				// 	$this->view->report_msg = $this->displayError($error_msg);
				// }
				
			//akhir
			}
			$bsid = $dataTemp['BS_ID'];
			$totalamount = $dataTemp['PS_TOTAL_AMOUNT'];
			$totaltrx = $dataTemp['PS_TXCOUNT'];
		}
		//var_dump($confirm);
		//var_dump($pstype);die;
		if($confirm){
			
			if ($pstype == '31') {

				$totalSuccess = 0;
				foreach($payment["countTrxCCY"] as $row)
				{
					foreach($row as $ccy)
					{
						$totalSuccess += $ccy['success'];
					}
				}

				$amountSuccess = 0;
				foreach($payment["sumTrxCCY"] as $row)
				{
					foreach($row as $ccy)
					{
						$amountSuccess += $ccy['success'];
					}
				}

				$totalFailed = 0;
				foreach($payment["countTrxCCY"] as $row)
				{
					foreach($row as $ccy)
					{
						$totalFailed += $ccy['failed'];
					}
				}

				$amountFailed = 0;
				foreach($payment["sumTrxCCY"] as $row)
				{
					foreach($row as $ccy)
					{
						$amountFailed += $ccy['failed'];
					}
				}

				if ($totalFailed >= 1) {
					$validation = 3;
				}else{
					$validation = 1;
				}
				//var_dump($validation);die;
				//var_dump($validation);die;
				
				if ($validation == 1) {

					if ($payment["countTrxPB"] == 0)
						$priviCreate = 'CBPI';
					else
						$priviCreate = 'CBPW';

					$param['PS_SUBJECT'] = $paramPayment['PS_SUBJECT'];
					$param['PS_EFDATE']  = Application_Helper_General::convertDate($paramPayment['PS_EFDATE'], $dateDBFormat, $dateDisplayFormat);
					$param['PS_TYPE'] 	= '31';
					$param['PS_CCY']  = $paramTrxArr[0]['ACBENEF_CCY'];
					$param['PS_FILE'] = $paramPayment['PS_FILE'];

					$param['TRANSACTION_DATA'] = array();
					foreach($paramTrxArr as $row)
					{
						$param['TRANSACTION_DATA'][] = array(
							'SOURCE_ACCOUNT' 					=> $row['ACCTSRC'],
							'BENEFICIARY_ACCOUNT' 				=> $row['ACBENEF'],
							'BENEFICIARY_ACCOUNT_CCY' 			=> $row['ACBENEF_CCY'],
							'BENEFICIARY_ACCOUNT_NAME' 			=> $row['ACBENEF_BANKNAME'],
							'BENEFICIARY_ALIAS_NAME' 			=> $row['ACBENEF_ALIAS'],
							'BENEFICIARY_EMAIL' 				=> $row['ACBENEF_EMAIL'],
							'BENEFICIARY_MOBILE_PHONE_NUMBER' 	=> $row['ACBENEF_PHONE'],
		// 					'BENEFICIARY_ADDRESS' 				=> $row['ACBENEF_ADDRESS1'],
							'BENEFICIARY_CITIZENSHIP' 			=> $row['ACBENEF_CITIZENSHIP'],
							'BENEFICIARY_RESIDENT' 				=> $row['ACBENEF_RESIDENT'],
							'CLR_CODE' 							=> $row['BANK_CODE'],
							'TRANSFER_TYPE' 					=> $row['TRANSFER_TYPE'],
							'TRA_AMOUNT' 						=> $row['TRA_AMOUNT'],
							'TRANSFER_FEE' 						=> $row['TRANSFER_FEE'],
							'TRA_MESSAGE' 						=> $row['TRA_MESSAGE'],
							'TRA_REFNO' 						=> $row['TRA_REFNO'],
						);

					}

					$param['_addBeneficiary'] = $paramPayment['_addBeneficiary'];
					$param['_beneLinkage'] = $paramPayment['_beneLinkage'];
					$param['_priviCreate'] = $priviCreate;

					$BulkPayment = new BulkPayment("", $dataTemp['CUST_ID'], $dataTemp['USER_ID']);
					$paymentRef = NULL;
					//var_dump($param);
					//var_dump($paymentRef);die;
					$result = $BulkPayment->createPayment($param,$paymentRef);

				}

				$transactionInsert = array(	

					'PS_SUCCESS_AMOUNT' 	=> $amountSuccess,
					'PS_SUCCESS_TXAMOUNT' 	=> $totalSuccess,
					'PS_FAILED_AMOUNT' 		=> $amountFailed,
					'PS_FAILED_TXAMOUNT'	=> $totalFailed,
					'VALIDATION'			=> $validation
				   );
			
				//var_dump($transactionInsert);die;
			
				$where = array('BS_ID = ?' => $bsid);
				$db->update('TEMP_BULKPSLIP', $transactionInsert, $where);

			}
			
			
			if ($pstype == '32') {

				$totalSuccess = 0;
				foreach($payment["countTrxCCY"] as $row)
				{
					foreach($row as $ccy)
					{
						$totalSuccess += $ccy['success'];
					}
				}

				$amountSuccess = 0;
				foreach($payment["sumTrxCCY"] as $row)
				{
					foreach($row as $ccy)
					{
						$amountSuccess += $ccy['success'];
					}
				}

				$totalFailed = 0;
				foreach($payment["countTrxCCY"] as $row)
				{
					foreach($row as $ccy)
					{
						$totalFailed += $ccy['failed'];
					}
				}

				$amountFailed = 0;
				foreach($payment["sumTrxCCY"] as $row)
				{
					foreach($row as $ccy)
					{
						$amountFailed += $ccy['failed'];
					}
				}

				if ($totalFailed >= 1) {
					$validation = 3;
				}else{
					$validation = 1;
				}
				//var_dump($validation);die;
				//var_dump($validation);die;
				
				if ($validation == 1) {

					if ($payment["countTrxPB"] == 0)
						$priviCreate = 'CBPI';
					else
						$priviCreate = 'CBPW';

					$param['PS_SUBJECT'] = $paramPayment['PS_SUBJECT'];
					$param['PS_EFDATE']  = Application_Helper_General::convertDate($paramPayment['PS_EFDATE'], $dateDBFormat, $dateDisplayFormat);
					$param['PS_TYPE'] 	= '32';
					$param['PS_CCY']  = $paramTrxArr[0]['ACBENEF_CCY'];
					$param['PS_FILE'] = $paramPayment['PS_FILE'];

					$param['TRANSACTION_DATA'] = array();
					foreach($paramTrxArr as $row)
					{
						$param['TRANSACTION_DATA'][] = array(
							'SOURCE_ACCOUNT' 					=> $row['ACCTSRC'],
							'BENEFICIARY_ACCOUNT' 				=> $row['ACBENEF'],
							'BENEFICIARY_ACCOUNT_CCY' 			=> $row['ACBENEF_CCY'],
							'BENEFICIARY_ACCOUNT_NAME' 			=> $row['ACBENEF_BANKNAME'],
							'BENEFICIARY_ALIAS_NAME' 			=> $row['ACBENEF_ALIAS'],
							'BENEFICIARY_EMAIL' 				=> $row['ACBENEF_EMAIL'],
							'BENEFICIARY_MOBILE_PHONE_NUMBER' 	=> $row['ACBENEF_PHONE'],
		// 					'BENEFICIARY_ADDRESS' 				=> $row['ACBENEF_ADDRESS1'],
							'BENEFICIARY_CITIZENSHIP' 			=> $row['ACBENEF_CITIZENSHIP'],
							'BENEFICIARY_RESIDENT' 				=> $row['ACBENEF_RESIDENT'],
							'CLR_CODE' 							=> $row['BANK_CODE'],
							'TRANSFER_TYPE' 					=> $row['TRANSFER_TYPE'],
							'TRA_AMOUNT' 						=> $row['TRA_AMOUNT'],
							'TRANSFER_FEE' 						=> $row['TRANSFER_FEE'],
							'TRA_MESSAGE' 						=> $row['TRA_MESSAGE'],
							'TRA_REFNO' 						=> $row['TRA_REFNO'],
						);

					}

					$param['_addBeneficiary'] = $paramPayment['_addBeneficiary'];
					$param['_beneLinkage'] = $paramPayment['_beneLinkage'];
					$param['_priviCreate'] = $priviCreate;

					$BulkPayment = new BulkPayment("", $dataTemp['CUST_ID'], $dataTemp['USER_ID']);
					$paymentRef = NULL;
					$result = $BulkPayment->createPayment($param,$paymentRef);

				}

				$transactionInsert = array(	

					'PS_SUCCESS_AMOUNT' 	=> $amountSuccess,
					'PS_SUCCESS_TXAMOUNT' 	=> $totalSuccess,
					'PS_FAILED_AMOUNT' 		=> $amountFailed,
					'PS_FAILED_TXAMOUNT'	=> $totalFailed,
					'VALIDATION'			=> $validation
				   );
			
				//var_dump($transactionInsert);die;
			
				$where = array('BS_ID = ?' => $bsid);
				$db->update('TEMP_BULKPSLIP', $transactionInsert, $where);

			}
			
			if ($pstype == '11') {

				$totalSuccess = 0;
				foreach($payment["countTrxCCY"] as $row)
				{
					foreach($row as $ccy)
					{
						$totalSuccess += $ccy['success'];
					}
				}

				$amountSuccess = 0;
				foreach($payment["sumTrxCCY"] as $row)
				{
					foreach($row as $ccy)
					{
						$amountSuccess += $ccy['success'];
					}
				}

				$totalFailed = 0;
				foreach($payment["countTrxCCY"] as $row)
				{
					foreach($row as $ccy)
					{
						$totalFailed += $ccy['failed'];
					}
				}

				$amountFailed = 0;
				foreach($payment["sumTrxCCY"] as $row)
				{
					foreach($row as $ccy)
					{
						$amountFailed += $ccy['failed'];
					}
				}

				if ($totalFailed >= 1) {
					$validation = 3;
				}else{
					$validation = 1;
				}
				//var_dump($validation);die;
				//var_dump($validation);die;
				
				if ($validation == 1) {

					if ($payment["countTrxPB"] == 0)
						$priviCreate = 'CBPI';
					else
						$priviCreate = 'CBPW';

					$param['PS_SUBJECT'] = $paramPayment['PS_SUBJECT'];
					$param['PS_EFDATE']  = Application_Helper_General::convertDate($paramPayment['PS_EFDATE'], $dateDBFormat, $dateDisplayFormat);
					$param['PS_TYPE'] 	= $paymenttype['code']['payroll'];
					$param['PS_CCY']  = $paramTrxArr[0]['ACBENEF_CCY'];
					$param['PS_FILE'] = $paramPayment['PS_FILE'];

					$param['TRANSACTION_DATA'] = array();
					foreach($paramTrxArr as $row)
					{
						$param['TRANSACTION_DATA'][] = array(
							'SOURCE_ACCOUNT' 					=> $row['ACCTSRC'],
							'BENEFICIARY_ACCOUNT' 				=> $row['ACBENEF'],
							'BENEFICIARY_ACCOUNT_CCY' 			=> $row['ACBENEF_CCY'],
							'BENEFICIARY_ACCOUNT_NAME' 			=> $row['ACBENEF_BANKNAME'],
							'BENEFICIARY_ALIAS_NAME' 			=> $row['ACBENEF_ALIAS'],
							'BENEFICIARY_EMAIL' 				=> $row['ACBENEF_EMAIL'],
							'BENEFICIARY_MOBILE_PHONE_NUMBER' 	=> $row['ACBENEF_PHONE'],
		// 					'BENEFICIARY_ADDRESS' 				=> $row['ACBENEF_ADDRESS1'],
							'BENEFICIARY_CITIZENSHIP' 			=> $row['ACBENEF_CITIZENSHIP'],
							'BENEFICIARY_RESIDENT' 				=> $row['ACBENEF_RESIDENT'],
							'CLR_CODE' 							=> $row['BANK_CODE'],
							'TRANSFER_TYPE' 					=> $row['TRANSFER_TYPE'],
							'TRA_AMOUNT' 						=> $row['TRA_AMOUNT'],
							'TRANSFER_FEE' 						=> $row['TRANSFER_FEE'],
							'TRA_MESSAGE' 						=> $row['TRA_MESSAGE'],
							'TRA_REFNO' 						=> $row['TRA_REFNO'],
						);

					}

					$param['_addBeneficiary'] = $paramPayment['_addBeneficiary'];
					$param['_beneLinkage'] = $paramPayment['_beneLinkage'];
					$param['_priviCreate'] = $priviCreate;

					$BulkPayment = new BulkPayment("", $dataTemp['CUST_ID'], $dataTemp['USER_ID']);
					$paymentRef = NULL;
					$result = $BulkPayment->createPayment($param,$paymentRef);

				}

				$transactionInsert = array(	

					'PS_SUCCESS_AMOUNT' 	=> $amountSuccess,
					'PS_SUCCESS_TXAMOUNT' 	=> $totalSuccess,
					'PS_FAILED_AMOUNT' 		=> $amountFailed,
					'PS_FAILED_TXAMOUNT'	=> $totalFailed,
					'VALIDATION'			=> $validation
				   );
			
				//var_dump($transactionInsert);die;
			
				$where = array('BS_ID = ?' => $bsid);
				$db->update('TEMP_BULKPSLIP', $transactionInsert, $where);

			}
			
			//var_dump($payment);die;
			if ($pstype == '28') {

				$totalSuccess = 0;
				foreach($payment["countTrxCCY"] as $row)
				{
					foreach($row as $ccy)
					{
						$totalSuccess += $ccy['success'];
					}
				}

				$amountSuccess = 0;
				foreach($payment["sumTrxCCY"] as $row)
				{
					foreach($row as $ccy)
					{
						$amountSuccess += $ccy['success'];
					}
				}

				$totalFailed = 0;
				foreach($payment["countTrxCCY"] as $row)
				{
					foreach($row as $ccy)
					{
						$totalFailed += $ccy['failed'];
					}
				}

				$amountFailed = 0;
				foreach($payment["sumTrxCCY"] as $row)
				{
					foreach($row as $ccy)
					{
						$amountFailed += $ccy['failed'];
					}
				}

				if ($totalFailed >= 1) {
					$validation = 3;
				}else{
					$validation = 1;
				}
				//var_dump($payment);
				//var_dump($totalFailed);die;
				
				if ($validation == 1) {

					if ($payment["countTrxPB"] == 0)
						$priviCreate = 'CBPI';
					else
						$priviCreate = 'CBPW';

					$param['PS_SUBJECT'] = $paramPayment['PS_SUBJECT'];
					$param['PS_EFDATE']  = Application_Helper_General::convertDate($paramPayment['PS_EFDATE'], $dateDBFormat, $dateDisplayFormat);
					$param['PS_TYPE'] 	= 28;
					$param['PS_CCY']  = $paramTrxArr[0]['ACBENEF_CCY'];
					$param['PS_FILE'] = $paramPayment['PS_FILE'];

					$param['TRANSACTION_DATA'] = array();
					foreach($paramTrxArr as $row)
					{
						$param['TRANSACTION_DATA'][] = array(
							'SOURCE_ACCOUNT' 					=> $row['ACCTSRC'],
							'BENEFICIARY_ACCOUNT' 				=> $row['ACBENEF'],
							'BENEFICIARY_ACCOUNT_CCY' 			=> $row['ACBENEF_CCY'],
							'BENEFICIARY_ACCOUNT_NAME' 			=> $row['ACBENEF_BANKNAME'],
							'BENEFICIARY_ALIAS_NAME' 			=> $row['ACBENEF_ALIAS'],
							'BENEFICIARY_EMAIL' 				=> $row['ACBENEF_EMAIL'],
							'BENEFICIARY_MOBILE_PHONE_NUMBER' 	=> $row['ACBENEF_PHONE'],
		// 					'BENEFICIARY_ADDRESS' 				=> $row['ACBENEF_ADDRESS1'],
							'BENEFICIARY_CITIZENSHIP' 			=> $row['ACBENEF_CITIZENSHIP'],
							'BENEFICIARY_RESIDENT' 				=> $row['ACBENEF_RESIDENT'],
							'CLR_CODE' 							=> $row['BANK_CODE'],
							'TRANSFER_TYPE' 					=> $row['TRANSFER_TYPE'],
							'TRA_AMOUNT' 						=> $row['TRA_AMOUNT'],
							'TRANSFER_FEE' 						=> $row['TRANSFER_FEE'],
							'TRA_MESSAGE' 						=> $row['TRA_MESSAGE'],
							'TRA_REFNO' 						=> $row['TRA_REFNO'],
						);

					}

					$param['_addBeneficiary'] = $paramPayment['_addBeneficiary'];
					$param['_beneLinkage'] = $paramPayment['_beneLinkage'];
					$param['_priviCreate'] = $priviCreate;

					$BulkPayment = new BulkPayment("", $dataTemp['CUST_ID'], $dataTemp['USER_ID']);
					$paymentRef = NULL;
					$result = $BulkPayment->createPayment($param,$paymentRef);

				}

				$transactionInsert = array(	

					'PS_SUCCESS_AMOUNT' 	=> $amountSuccess,
					'PS_SUCCESS_TXAMOUNT' 	=> $totalSuccess,
					'PS_FAILED_AMOUNT' 		=> $amountFailed,
					'PS_FAILED_TXAMOUNT'	=> $totalFailed,
					'VALIDATION'			=> $validation
				   );
			
				//var_dump($transactionInsert);die;
			
				$where = array('BS_ID = ?' => $bsid);
				$db->update('TEMP_BULKPSLIP', $transactionInsert, $where);

			}
			
			
			if ($pstype == '26') {

				
				$validation = 1;
				
				//var_dump($validation);die;
				//var_dump($paramSession);die;
				
				if ($validation == 1) {

					if ($payment["countTrxPB"] == 0)
						$priviCreate = 'CBPI';
					else
						$priviCreate = 'CBPW';

					
					//$paramSession['A']
					$paramTrxArrNew['TRANSACTION_DATA'] = array();
					$totalamount = 0;
					//var_dump($paramTrxArr);die;
					foreach($paramTrxArr as $row)
					{
						
						$totalamount = $row["amount"]+$totalamount;
						$paramTrxArrNew['TRANSACTION_DATA'][] = array(
							'SOURCE_ACCOUNT' 					=> $row["ACCTSRC"],
							'ACCT_NAME_DATA' 					=> $row["ACCT_NAME_DATA"],
							'ACCT_ALIAS_NAME' 					=> $row["ACCT_ALIAS_NAME"],
							'BENEFICIARY_ACCOUNT' 				=> $row['ACBENEF'],
							'BENEFICIARY_ACCOUNT_CCY' 			=> $row['ACBENEF_CCY'],
							'BENEFICIARY_ACCOUNT_NAME' 			=> $row['ACBENEF_BANKNAME'],
							'BENEFICIARY_ALIAS_NAME' 			=> $row['ACBENEF_ALIAS'],
							'BENEFICIARY_EMAIL' 				=> $row['ACBENEF_EMAIL'],
							'BENEFICIARY_MOBILE_PHONE_NUMBER' 	=> $row['ACBENEF_PHONE'],
		// 					'BENEFICIARY_ADDRESS' 				=> $row['ACBENEF_ADDRESS1'],
							'BENEFICIARY_CITIZENSHIP' 			=> $row['ACBENEF_CITIZENSHIP'],
							'BENEFICIARY_RESIDENT' 				=> $row['ACBENEF_RESIDENT'],
							'CLR_CODE' 							=> $row['BANK_CODE'],
							'BILLER_ID'							=> $row['BILLER_ID'],
							//'TRANSFER_TYPE' 					=> 0,
							'TRA_AMOUNT' 						=> $row["amount"],
							'TRANSFER_FEE' 						=> $row["fee"],
							'TRA_MESSAGE' 						=> $row['TRA_MESSAGE'],
							'TRA_REFNO' 						=> $row['TRA_REFNO'],
							'orderId' 							=> $row["orderId"]
						);

					}
					
					$paramTrxArrNew['PS_SUBJECT'] = $paramPayment['PS_SUBJECT'];
					$paramTrxArrNew['PS_EFDATE']  = Application_Helper_General::convertDate($paramPayment['PS_EFDATE'], $dateDBFormat, $dateDisplayFormat);
					$paramTrxArrNew['PS_TYPE'] 	= 26;
					$paramTrxArrNew['PS_CCY']  = 'IDR';
					$paramTrxArrNew['PS_FILE'] = $paramPayment['PS_FILE'];
					$paramTrxArrNew['_addBeneficiary'] = $paramPayment['_addBeneficiary'];
					$paramTrxArrNew['_beneLinkage'] = $paramPayment['_beneLinkage'];
					$paramTrxArrNew['_priviCreate'] = $priviCreate;

					$BulkPayment = new BulkPayment("", $dataTemp['CUST_ID'], $dataTemp['USER_ID']);
					$paymentRef = NULL;
					//$result = $BulkPayment->createPayment($param,$paymentRef);
					//$sendProvider 		= new SinglePayment(null,$this->_custIdLogin, $this->_userIdLogin);
					$resProvider 		= $BulkPayment->createValidatePayment($paramTrxArrNew,$paymentRef, $msg);

				}

				$transactionInsert = array(	

					'PS_SUCCESS_AMOUNT' 	=> $totalamount,
					'PS_SUCCESS_TXAMOUNT' 	=> count($paramTrxArrNew['TRANSACTION_DATA']),
					'PS_FAILED_AMOUNT' 		=> 0,
					'PS_FAILED_TXAMOUNT'	=> 0,
					'VALIDATION'			=> $validation
				   );
			
				//var_dump($transactionInsert);die;
			
				$where = array('BS_ID = ?' => $bsid);
				$db->update('TEMP_BULKPSLIP', $transactionInsert, $where);

			}

			if ($pstype == '5') {
				
				$totalSuccess = 0;
				foreach($payment["countTrxCCY"] as $row)
				{
					foreach($row as $ccy)
					{
						$totalSuccess += $ccy['success'];
					}
				}

				$amountSuccess = 0;
				foreach($payment["sumTrxCCY"] as $row)
				{
					foreach($row as $ccy)
					{
						$amountSuccess += $ccy['success'];
					}
				}

				$totalFailed = 0;
				foreach($payment["countTrxCCY"] as $row)
				{
					foreach($row as $ccy)
					{
						$totalFailed += $ccy['failed'];
					}
				}

				$amountFailed = 0;
				foreach($payment["sumTrxCCY"] as $row)
				{
					foreach($row as $ccy)
					{
						$amountFailed += $ccy['failed'];
					}
				}

				if ($totalFailed >= 1) {
					$validation = 3;
				}else{
					$validation = 1;
				}

				if ($validation == 1) {
					//var_dump($paramTrxArr);die;

					$param['PS_SUBJECT'] = $paramPayment['PS_SUBJECT'];
					$param['PS_EFDATE']  = Application_Helper_General::convertDate($paramPayment['PS_EFDATE'], $dateDBFormat, $dateDisplayFormat);
					$param['PS_TYPE'] 	= $paymenttype['code']['bulkdebet'];
					$param['PS_CCY']  = $paramTrxArr[0]['ACBENEF_CCY'];
					$param['PS_FILE']    = $paramPayment['PS_FILE'];

					$param['TRANSACTION_DATA'] = array();
					foreach($paramTrxArr as $row)
					{
						$param['TRANSACTION_DATA'][] = array(
								'SOURCE_ACCOUNT' 			=> $row['ACCTSRC'],
								'BENEFICIARY_ACCOUNT' 		=> $row['ACBENEF'],
								'BENEFICIARY_ACCOUNT_CCY' 	=> $row['ACBENEF_CCY'],
								'BENEFICIARY_ACCOUNT_NAME' 	=> $row['ACBENEF_BANKNAME'],
								'BENEFICIARY_ALIAS_NAME' 	=> $row['ACBENEF_ALIAS'],
								// 'BENEFICIARY_EMAIL' 		=> $row['ACBENEF_EMAIL'],
								// 'BENEFICIARY_ADDRESS' 		=> $row['ACBENEF_ADDRESS1'],
								// 'BENEFICIARY_CITIZENSHIP' 	=> $row['ACBENEF_CITIZENSHIP'],
								// 'CLR_CODE' 					=> $row['BANK_CODE'],
								'TRANSFER_TYPE' 			=> $row['TRANSFER_TYPE'],
								'TRA_AMOUNT' 				=> $row['TRA_AMOUNT'],
								'TRA_MESSAGE' 				=> $row['TRA_MESSAGE'],
								'TRA_REFNO' 				=> $row['TRA_REFNO'],
								'sourceAccountType' 		=> $row['ACCOUNT_TYPE'],
						);

					}

					$param['_addBeneficiary'] 	= $paramPayment['_addBeneficiary'];
					$param['_beneLinkage'] 		= $paramPayment['_beneLinkage'];
					$param['_priviCreate'] 		= 'IPMO';

					$BulkPayment = new BulkPayment("", $dataTemp['CUST_ID'], $dataTemp['USER_ID']);
					$paymentRef = NULL;
					$result = $BulkPayment->createPayment($param,$paymentRef);
				}

				$transactionInsert = array(	

					'PS_SUCCESS_AMOUNT' 	=> $amountSuccess,
					'PS_SUCCESS_TXAMOUNT' 	=> $totalSuccess,
					'PS_FAILED_AMOUNT' 		=> $amountFailed,
					'PS_FAILED_TXAMOUNT'	=> $totalFailed,
					'VALIDATION'			=> $validation
				   );

				$where = array('BS_ID = ?' => $bsid);
				$db->update('TEMP_BULKPSLIP', $transactionInsert, $where);

			}
			
			if ($pstype == '25') {
				
				$totalSuccess = 0;
				foreach($payment["countTrxCCY"] as $row)
				{
					foreach($row as $ccy)
					{
						$totalSuccess += $ccy['success'];
					}
				}

				$amountSuccess = 0;
				foreach($payment["sumTrxCCY"] as $row)
				{
					foreach($row as $ccy)
					{
						$amountSuccess += $ccy['success'];
					}
				}

				$totalFailed = 0;
				foreach($payment["countTrxCCY"] as $row)
				{
					foreach($row as $ccy)
					{
						$totalFailed += $ccy['failed'];
					}
				}

				$amountFailed = 0;
				foreach($payment["sumTrxCCY"] as $row)
				{
					foreach($row as $ccy)
					{
						$amountFailed += $ccy['failed'];
					}
				}

				if ($totalFailed >= 1) {
					$validation = 3;
				}else{
					$validation = 1;
				}
				
				//var_dump($validation);die;
				if ($validation == 1) {
					
					if ($payment["countTrxPB"] == 0)
						$priviCreate = 'CBPI';
					else
						$priviCreate = 'CBPW';

					$param['PS_SUBJECT'] 	= $paramPayment['PS_SUBJECT'];
					$param['PS_EFDATE']  	= Application_Helper_General::convertDate($paramPayment['PS_EFDATE'], $dateDBFormat, $dateDisplayFormat);
					$param['PS_TYPE'] 		= 25;
					// $param['PS_CCY']  	= $data['payment']['acctsrcArr'][$data['paramTrxArr'][0]['ACCTSRC']]['CCY_ID'];
					$param['PS_CCY']  		= $paramTrxArr[0]['ACBENEF_CCY'];
					$param['PS_FILE']    	= $paramPayment['PS_FILE'];

					$param['TRANSACTION_DATA'] = array();
					
					foreach($paramTrxArr as $row)
					{
						$param['TRANSACTION_DATA'][] = array(
								'SOURCE_ACCOUNT' 			=> $row['ACCTSRC'],
								'BENEFICIARY_ACCOUNT' 		=> $row['ACBENEF'],
								'BENEFICIARY_ACCOUNT_CCY' 	=> $row['ACBENEF_CCY'],
								'BENEFICIARY_ACCOUNT_NAME' 	=> $row['ACBENEF_BANKNAME'], //ADA
		//						'BENEFICIARY_ALIAS_NAME' 	=> $row['ACBENEF_ALIAS'], //ADA
								'BENEFICIARY_ALIAS_NAME' 	=> $row['ACBENEF_BANKNAME'], //ADA
								'BENEFICIARY_EMAIL' 		=> $row['ACBENEF_EMAIL'],
								'BENEFICIARY_ADDRESS' 		=> $row['ACBENEF_ADDRESS1'], //ADA
								'BENEFICIARY_CITIZENSHIP' 	=> $row['ACBENEF_CITIZENSHIP'],
								//'ACBENEF_ADDRESS1' 			=> $row['ACBENEF_ADDRESS1'],
								'BENEFICIARY_RESIDENT' 		=> $row['BENEFICIARY_RESIDENT'],
								'CLR_CODE' 					=> $row['BANK_CODE'],
								'TRANSFER_TYPE' 			=> $row['TRANSFER_TYPE'], //SKN,RTGS,PB
								'TRA_AMOUNT' 				=> $row['TRA_AMOUNT'],
								'TRANSFER_FEE' 				=> $row['TRANSFER_FEE'],
								'TRA_MESSAGE' 				=> $row['TRA_MESSAGE'],
								'TRA_REFNO' 				=> $row['TRA_REFNO'],
								'sourceAccountType' 		=> $data['sourceAccountType'],

								'BENEFICIARY_BANK_NAME' 	=> $row['BANK_NAME'], //ADA
								'LLD_CATEGORY' 				=> $row['BENEFICIARY_CATEGORY'],
								'CITY_CODE' 				=> $row['BENEFICIARY_CITY_CODE'],
								'LLD_BENEIDENTIF' 			=> $row['BENEFICIARY_ID_TYPE'],
								'LLD_BENENUMBER' 			=> $row['BENEFICIARY_ID_NUMBER'],
								'LLD_TRANSACTION_PURPOSE'	=> $row['TRANS_PURPOSE'],
								'TRA_AMOUNTEQ'				=> $row['TRA_AMOUNTEQ'],
								'RATE'						=> $row['RATE'],
								'RATE_BUY'					=> $row['RATE_BUY'],
								'BOOKRATE'					=> $row['BOOKRATE'],

						);

					}

					$param['_addBeneficiary'] = $paramPayment['_addBeneficiary'];
					$param['_beneLinkage'] = $paramPayment['_beneLinkage'];
					$param['_priviCreate'] = $priviCreate;

					$BulkPayment = new BulkPayment("", $dataTemp['CUST_ID'], $dataTemp['USER_ID']);
					$paymentRef = NULL;
					
					$result = $BulkPayment->createPayment($param,$paymentRef);
					//var_dump($result);die;
				}
 
				$transactionInsert = array(	

					'PS_SUCCESS_AMOUNT' 	=> $amountSuccess/2,
					'PS_SUCCESS_TXAMOUNT' 	=> $totalSuccess/2,
					'PS_FAILED_AMOUNT' 		=> $amountFailed,
					'PS_FAILED_TXAMOUNT'	=> $totalFailed,
					'VALIDATION'			=> $validation
				   );

				$where = array('BS_ID = ?' => $bsid);
				$db->update('TEMP_BULKPSLIP', $transactionInsert, $where);

			}

			if ($pstype == '4') {
				
				$totalSuccess = 0;
				foreach($payment["countTrxCCY"] as $row)
				{
					foreach($row as $ccy)
					{
						$totalSuccess += $ccy['success'];
					}
				}

				$amountSuccess = 0;
				foreach($payment["sumTrxCCY"] as $row)
				{
					foreach($row as $ccy)
					{
						$amountSuccess += $ccy['success'];
					}
				}

				$totalFailed = 0;
				foreach($payment["countTrxCCY"] as $row)
				{
					foreach($row as $ccy)
					{
						$totalFailed += $ccy['failed'];
					}
				}

				$amountFailed = 0;
				foreach($payment["sumTrxCCY"] as $row)
				{
					foreach($row as $ccy)
					{
						$amountFailed += $ccy['failed'];
					}
				}

				if ($totalFailed >= 1) {
					$validation = 3;
				}else{
					$validation = 1;
				}
				
				//var_dump($validation);die;
				if ($validation == 1) {
					
					if ($payment["countTrxPB"] == 0)
						$priviCreate = 'CBPI';
					else
						$priviCreate = 'CBPW';

					$param['PS_SUBJECT'] 	= $paramPayment['PS_SUBJECT'];
					$param['PS_EFDATE']  	= Application_Helper_General::convertDate($paramPayment['PS_EFDATE'], $dateDBFormat, $dateDisplayFormat);
					$param['PS_TYPE'] 		= $paymenttype['code']['bulkcredit'];
					// $param['PS_CCY']  	= $data['payment']['acctsrcArr'][$data['paramTrxArr'][0]['ACCTSRC']]['CCY_ID'];
					$param['PS_CCY']  		= $paramTrxArr[0]['ACBENEF_CCY'];
					$param['PS_FILE']    	= $paramPayment['PS_FILE'];

					$param['TRANSACTION_DATA'] = array();
					
					

					foreach($paramTrxArr as $row)
					{
						$dataSource = $db->fetchrow(
						$db->select(array('ACCT_NAME','ACCT_ALIAS_NAME','CCY_ID'))
							->FROM('M_CUSTOMER_ACCT')
							->WHERE('CUST_ID = ?',$dataTemp['CUST_ID'])
							->WHERE('ACCT_NO = ?',$row['ACCTSRC'])
							
					);
						
						$param['TRANSACTION_DATA'][] = array(
								'SOURCE_ACCOUNT' 			=> $row['ACCTSRC'],
								//'SOURCE_ACCT_BANK_CODE'		=> $row['sfa'],
								'SOURCE_ACCOUNT_CCY'		=> $dataSource['CCY_ID'],
								'SOURCE_ALIAS_NAME'			=> $dataSource['ACCT_ALIAS_NAME'],
								'SOURCE_ACCOUNT_NAME'		=> $dataSource['ACCT_NAME'],
								'BENEFICIARY_ACCOUNT' 		=> $row['ACBENEF'],
								'BENEFICIARY_ACCOUNT_CCY' 	=> $row['ACBENEF_CCY'],
								'BENEFICIARY_ACCOUNT_NAME' 	=> $row['ACBENEF_BANKNAME'], //ADA
		//						'BENEFICIARY_ALIAS_NAME' 	=> $row['ACBENEF_ALIAS'], //ADA
								'BENEFICIARY_ALIAS_NAME' 	=> $row['ACBENEF_BANKNAME'], //ADA
								'BENEFICIARY_EMAIL' 		=> $row['ACBENEF_EMAIL'],
								'BENEFICIARY_ADDRESS' 		=> $row['ACBENEF_ADDRESS1'], //ADA
								'BENEFICIARY_CITIZENSHIP' 	=> $row['ACBENEF_CITIZENSHIP'],
								//'ACBENEF_ADDRESS1' 			=> $row['ACBENEF_ADDRESS1'],
								'BENEFICIARY_RESIDENT' 		=> $row['BENEFICIARY_RESIDENT'],
								'CLR_CODE' 					=> $row['BANK_CODE'],
								'TRANSFER_TYPE' 			=> $row['TRANSFER_TYPE'], //SKN,RTGS,PB
								'TRA_AMOUNT' 				=> $row['TRA_AMOUNT'],
								'TRANSFER_FEE' 				=> $row['TRANSFER_FEE'],
								'TRA_MESSAGE' 				=> $row['TRA_MESSAGE'],
								'TRA_REFNO' 				=> $row['TRA_REFNO'],
								'sourceAccountType' 		=> $data['sourceAccountType'],
								
								'BENEFICIARY_BANK_NAME' 	=> $row['BANK_NAME'], //ADA
								'LLD_CATEGORY' 				=> $row['BENEFICIARY_CATEGORY'],
								'CITY_CODE' 				=> $row['BENEFICIARY_CITY_CODE'],
								'LLD_BENEIDENTIF' 			=> $row['BENEFICIARY_ID_TYPE'],
								'LLD_BENENUMBER' 			=> $row['BENEFICIARY_ID_NUMBER'],
								'LLD_TRANSACTION_PURPOSE'	=> $row['TRANS_PURPOSE'],
								'TRA_AMOUNTEQ'				=> $row['TRA_AMOUNTEQ'],
								'RATE'						=> $row['RATE'],
								'RATE_BUY'					=> $row['RATE_BUY'],
								'BOOKRATE'					=> $row['BOOKRATE'],

						);

					}

					$param['_addBeneficiary'] = $paramPayment['_addBeneficiary'];
					$param['_beneLinkage'] = $paramPayment['_beneLinkage'];
					$param['_priviCreate'] = $priviCreate;

					$BulkPayment = new BulkPayment("", $dataTemp['CUST_ID'], $dataTemp['USER_ID']);
					$paymentRef = NULL;
					try{
						//var_dump($param);
					$result = $BulkPayment->createPayment($param,$paymentRef);
					
					}catch(Exception $e){
						//var_dump($e);die;
					}
					//var_dump($result);die;
				}

				$transactionInsert = array(	

					'PS_SUCCESS_AMOUNT' 	=> $amountSuccess,
					'PS_SUCCESS_TXAMOUNT' 	=> $totalSuccess,
					'PS_FAILED_AMOUNT' 		=> $amountFailed,
					'PS_FAILED_TXAMOUNT'	=> $totalFailed,
					'VALIDATION'			=> $validation
				   );

				$where = array('BS_ID = ?' => $bsid);
				$db->update('TEMP_BULKPSLIP', $transactionInsert, $where);

			}

			if ($pstype == '22') {
				//var_dump($payment);
				$totalSuccess = 0;
				foreach($payment["countTrxCCY"] as $row)
				{
					foreach($row as $ccy)
					{
						$totalSuccess += $ccy['success'];
					}
				}

				$amountSuccess = 0;
				foreach($payment["sumTrxCCY"] as $row)
				{
					foreach($row as $ccy)
					{
						$amountSuccess += $ccy['success'];
					}
				}

				$totalFailed = 0;
				foreach($payment["countTrxCCY"] as $row)
				{
					foreach($row as $ccy)
					{
						$totalFailed += $ccy['failed'];
					}
				}

				$amountFailed = 0;
				foreach($payment["sumTrxCCY"] as $row)
				{
					foreach($row as $ccy)
					{
						$amountFailed += $ccy['failed'];
					}
				}

				if ($totalFailed >= 1) {
					$validation = 3;
				}else{
					$validation = 1;
				} 
				//var_dump($payment);die;
		//		var_dump($arr);
		//die('here');
				if ($validation == 1) {
					if($totalSuccess != 0){
					//	var_dump($arr);die;
					foreach($arr as $row)
					{
						$i = 1;
						if($row['paramTrxArr']['0']['TRANSFER_TYPE'] == 'ONLINE'){
							$pstype = '2';
						}
						if($row['paramTrxArr']['0']['TRANSFER_TYPE'] == 'PB'){
							$pstype = '1';
						}
						
						if($row['paramTrxArr']['0']['TRANSFER_TYPE'] == 'RTGS' || $row['paramTrxArr']['0']['TRANSFER_TYPE'] == 'SKN'){
							$pstype = '2';
						}
						
						$dataSource = $db->fetchrow(
						$db->select(array('ACCT_NAME','ACCT_ALIAS_NAME','CCY_ID'))
							->FROM('M_CUSTOMER_ACCT')
							->WHERE('CUST_ID = ?',$dataTemp['CUST_ID'])
							->WHERE('ACCT_NO = ?',$row['paramTrxArr'][0]['ACCTSRC'])
							
					);
					 //var_dump($row['paramPayment']);die;
						$param['PS_SUBJECT'] 				= $row['paramPayment']['PS_SUBJECT'];
						$param['PS_FILE'] 					= $row['paramPayment']['PS_FILE'];
						$param['PS_EFDATE']  				= $row['paramPayment']['PS_EFDATE'];
						$param['PS_TYPE'] 					= $pstype;
						$param['PS_CCY']  					= $row['paramTrxArr'][0]['ACBENEF_CCY'];
						$param['SOURCE_ACCOUNT']			= $row['paramTrxArr'][0]['ACCTSRC'];
						$param['SOURCE_ACCOUNT_CCY']		= $dataSource['CCY_ID'];
						$param['SOURCE_ALIAS_NAME']			= $dataSource['ACCT_ALIAS_NAME'];
						$param['SOURCE_ACCOUNT_NAME']		= $dataSource['ACCT_NAME'];
						$param['BENEFICIARY_ACCOUNT'] 		= $row['paramTrxArr'][0]['ACBENEF'];
						$param['BENEFICIARY_ACCOUNT_CCY'] 	= $row['paramTrxArr'][0]['ACBENEF_CCY'];
						$param['BENEFICIARY_ACCOUNT_NAME'] 	= $row['paramTrxArr'][0]['ACBENEF_BANKNAME'];
		//				$param['BENEFICIARY_ALIAS_NAME'] 	= $row['paramTrxArr'][0]['ACBENEF_ALIAS'];
						$param['BENEFICIARY_ALIAS_NAME'] 	= $row['paramTrxArr'][0]['ACBENEF_BANKNAME'];
						$param['BENEFICIARY_EMAIL'] 		= $row['paramTrxArr'][0]['ACBENEF_EMAIL'];
						$param['BENEFICIARY_ADDRESS'] 		= $row['paramTrxArr'][0]['ACBENEF_ADDRESS1'];
						$param['BENEFICIARY_CITIZENSHIP'] 	= $row['paramTrxArr'][0]['ACBENEF_CITIZENSHIP'];
						$param['CLR_CODE'] 					= $row['paramTrxArr'][0]['BANK_CODE'];
		//				$param['BENEFICIARY_BANK_NAME'] 	= $row['paramTrxArr'][0]['BENEFICIARY_BANK_NAME'];
						$param['TRANSFER_TYPE'] 			= $row['paramTrxArr'][0]['TRANSFER_TYPE'];
						$param['TRA_AMOUNT'] 				= $row['paramTrxArr'][0]['TRA_AMOUNT'];
						$param['TRANSFER_FEE'] 				= $row['paramTrxArr'][0]['TRANSFER_FEE'];
						$param['TRA_MESSAGE'] 				= $row['paramTrxArr'][0]['TRA_MESSAGE'];
						$param['TRA_REFNO'] 				= $row['paramTrxArr'][0]['TRA_REFNO'];
						$param['_addBeneficiary'] 			= $row['paramPayment']['_addBeneficiary'];
						$param['_beneLinkage'] 				= $row['paramPayment']['_beneLinkage'];
						$param["_dateFormat"]				= $row['paramPayment']['_dateFormat'];
						$param["_dateDBFormat"]				= $row['paramPayment']['_dateDBFormat'];
						$param["_createPB"]					= $row['paramPayment']['_createPB'];
						$param["_createDOM"]				= $row['paramPayment']['_createDOM'];
						$param["_createREM"]				= $row['paramPayment']['_createREM'];
						$param["sourceAccountType"]			= $row['paramTrxArr'][0]['ACCOUNT_TYPE'];

						$param['BENEFICIARY_BANK_NAME'] 	= $row['paramTrxArr'][0]['BANK_NAME'];
						$param['LLD_CATEGORY'] 				= $row['paramTrxArr'][0]['BENEFICIARY_CATEGORY'];
						$param['CITY_CODE'] 				= $row['paramTrxArr'][0]['BENEFICIARY_CITY_CODE'];
						$param['LLD_BENEIDENTIF'] 			= $row['paramTrxArr'][0]['BENEFICIARY_ID_TYPE'];
						$param['LLD_BENENUMBER'] 			= $row['paramTrxArr'][0]['BENEFICIARY_ID_NUMBER'];

						$SinglePayment = new SinglePayment("", $dataTemp['CUST_ID'], $dataTemp['USER_ID']);
						$result = $SinglePayment->createPayment($param);
						
						$i++;
						}
					}
					
				}

				$transactionInsert = array(	

					'PS_SUCCESS_AMOUNT' 	=> $amountSuccess,
					'PS_SUCCESS_TXAMOUNT' 	=> $totalSuccess,
					'PS_FAILED_AMOUNT' 		=> $amountFailed,
					'PS_FAILED_TXAMOUNT'	=> $totalFailed,
					'VALIDATION'			=> $validation
				   );

				$where = array('BS_ID = ?' => $bsid);
				$db->update('TEMP_BULKPSLIP', $transactionInsert, $where);

			}

			//var_dump($result);die;
						$dataCustomer = $db->fetchrow(
							$db->select(array('CUST_ID','CUST_NAME','CUST_SAME_USER'))
								->FROM('M_CUSTOMER')
								->WHERE('CUST_ID = ?',$dataTemp['CUST_ID'])
								->WHERE('CUST_STATUS = 1')
						);
						//var_dump($paramTrxArr);
						//var_dump($paramPayment);die;
						$paramPayment['efdate'] = $paramPayment['PS_EFDATE'];
						if($dataCustomer['CUST_SAME_USER'] == '1'){
								$validate  	  = new ValidatePaymentSingle($dataTemp['CUST_ID'], $dataTemp['USER_ID'], $result);
								$check 		  = $validate->checkRelease($paramPayment, $paramTrxArr, 1);
								
								$Payment = new Payment($result, $dataTemp['CUST_ID'], $dataTemp['USER_ID']);
								
								$resultRelease = $Payment->releasePayment();
									
						}
						
						
		
		}else{
			$transactionInsert = array(	

					'PS_SUCCESS_AMOUNT' 	=> 0,
					'PS_SUCCESS_TXAMOUNT' 	=> 0,
					'PS_FAILED_AMOUNT' 		=> $totalamount,
					'PS_FAILED_TXAMOUNT'	=> $totaltrx,
					'VALIDATION'			=> 3
				   );
			
				
			
				$where = array('BS_ID = ?' => $bsid);
				//var_dump($transactionInsert);
				//var_dump($where);die;
				$db->update('TEMP_BULKPSLIP', $transactionInsert, $where);
		}	





			}
			else
			{
				$cronResult .= "No data released";
			}


			try{
				unlink($flag);
				echo 'Delete flag '.$flag.' at: '.date('Y-m-d H:i:s').PHP_EOL;
			}catch(Exception $e){
				$stringFailed = 'Can\'t delete flag at: '.date('Y-m-d H:i:s');
				Application_Helper_General::cronLog(basename(__FILE__),$stringFailed,0);
				echo $stringFailed.PHP_EOL;

				die();
			}
			

	}
	 
	 function parseCSV($fileName,$separator = null){
		$csvData = false;
		try {
				$Csv = new Application_Csv (  $fileName, $separator = "," );
				$csvData = $Csv->readAll ();
				// var_dump($csvData);die;
			} catch ( Exception $e ) {
				
				echo nl2br ( $e->getTraceAsString () );
			}
			return $csvData;
	}

	 function convertFileToArray($newFileName, $extension, $delimitedWith){

		$file_contents = file_get_contents($newFileName);
		
        //if csv occured
        if ($extension === 'csv') {

            //if (!empty($delimitedWith)) {
                $data = $this->parseCSV($newFileName);
            //}
            //if fix length
             if ($fixLength == 1) {

            	$fileContents = file($newFileName);

            	$contentOrder = $fixLengthContent;

            	// if with header
                if ($fixLengthType == 1) {
                    //karena yg diambil hanya order dri row ke 2 saja
                    $startArrIndex = 1;
                    $surplusIndex = 0;
                }
                else if ($fixLengthType == 3) {
                    //karena yg diambil hanya order dri row ke 1 saja
                    $startArrIndex = 0;
                    $surplusIndex = 1;
                }

                $contentOrderArr = explode(',', $contentOrder);

                if (count($contentOrderArr) > 1) {
                    foreach ($fileContents as $key => $value) {
                        if ($key >= $startArrIndex) {
                            foreach ($contentOrderArr as $key2 => $value2) {
                                //first order, startIndex from 0
                                if ($key2 == 0) {
                                    $startIndex = 0;
                                    $endIndex = (int) $value2 + 1;

                                    $data[$key + $surplusIndex][] = substr($value,$startIndex,$endIndex);
                                }
                                else{
                                    $startIndex = (int) ($contentOrderArr[$key2 - 1] + 1);
                                    $endIndex = (int) ($value2 - ($startIndex - 1));

                                    $data[$key + $surplusIndex][] = substr($value,$startIndex,$endIndex);
                                }
                            }       
                        }   
                    }
                }
            }
            else{
                $data = $this->parseCSV($newFileName);
            }
        }
        //if txt occured
        else if ($extension === 'txt') {
        	$delimitedWith = '|';
           $lines = file($newFileName);

           $checkMt940 = false;
           $checkMt101 = false;
           foreach ($lines as $line) {
               if (strpos($line, '{1:') !== false) {
                   $checkMt940 = true;
               }
               else if (strpos($line, ':20:') !== false) {
                   $checkMt101 = true;
               }
           }
		
           //if mt940 format
           if ($checkMt940) {
               
                $data = $this->_helper->parser->mt940($newFileName);
                $mtFile = true;
           }
           else if($checkMt101){
                $data = $this->_helper->parser->mt101($newFileName);
                $mtFile = true;
           }
           else{
			   //var_dump($delimitedWith);
				//die('ger');
           		//parse csv jg bs utk txt
                if (!empty($delimitedWith)) { 
                    $data = parseCSV($newFileName);
					
                }
				
                //if fix length
                else if ($fixLength == 1) {

                    $fileContents = file($newFileName);

	            	$contentOrder = $fixLengthContent;

	            	// if with header
	                if ($fixLengthType == 1) {
	                    //karena yg diambil hanya order dri row ke 2 saja
	                    $startArrIndex = 1;
	                    $surplusIndex = 0;
	                }
	                else if ($fixLengthType == 3) {
	                    //karena yg diambil hanya order dri row ke 1 saja
	                    $startArrIndex = 0;
	                    $surplusIndex = 1;
	                }

	                $contentOrderArr = explode(',', $contentOrder);

	                if (count($contentOrderArr) > 1) {
	                    foreach ($fileContents as $key => $value) {
	                        if ($key >= $startArrIndex) {
	                            foreach ($contentOrderArr as $key2 => $value2) {
	                                //first order, startIndex from 0
	                                if ($key2 == 0) {
	                                    $startIndex = 0;
	                                    $endIndex = (int) $value2 + 1;

	                                    $data[$key + $surplusIndex][] = substr($value,$startIndex,$endIndex);
	                                }
	                                else{
	                                    $startIndex = (int) ($contentOrderArr[$key2 - 1] + 1);
	                                    $endIndex = (int) ($value2 - ($startIndex - 1));

	                                    $data[$key + $surplusIndex][] = substr($value,$startIndex,$endIndex);
	                                }
	                            }       
	                        }   
	                    }
	                }                   
                }
                else{
                 	$data = $this->parseCSV($newFileName);
                }
           }
        }
        //if json occured
        else if ($extension === 'json') {

            $datajson = json_decode($file_contents, 1);
            $i = 0;
            foreach ($datajson as $key => $value) {
                if ($i == 0) {
                    $data[$i] = array_keys($value);
                    $data[$i + 1] = array_values($value);
                }
                else{
                    $data[$i+1] = array_values($value);
                }

                $i++;
            }
        }
        //if xml occured
        else if($extension === 'xml'){

            $xml = (array) simplexml_load_string($file_contents);

             $i = 0;
            foreach ($xml as $key => $value) {
                foreach ($value as $key2 => $value2) {
                    if ($i == 0) {
                        $data[$i] = array_keys((array)$value2);
                        $data[$i + 1] = array_values((array)$value2);
                    }
                    else{
                        $data[$i+1] = array_values((array)$value2);
                    }

                    $i++;
                }
            }

            for($i=0; $i<count($data); $i++){
                if ($i > 0) {
                    foreach ($data[$i] as $key => $value) {
                        if (empty($value)) {
                            $data[$i][$key] = null;
                        }
                    }
                }
            }

            // print_r($data);die();
        }
        else if($extension === 'xls' || $extension === 'xlsx'){
            try {
                $inputFileType = IOFactory::identify($newFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($newFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($newFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
             
            for ($row = 1; $row <= $highestRow; $row++){                        
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row);

                $data[$row - 1] = $rowData[0];
            }
        }
		
        return $data;
	}

	//validate mandatory
	//validate mandatory
	 function validateField($data, $adapterData){

		foreach ($adapterData as $key => $value) {
			if ($value['MANDATORY'] == 1) {
				$mandatoryArr[] = $value['HEADER_INDEX'];
			}
		}

		$field = array();
		foreach ($data as $key => $value) {

			foreach ($mandatoryArr as $key2 => $value2) {
				if(empty($value[$value2])){
					$field[] = $adapterData[$value2]['HEADER_NAME'];
				}
			}
		}
		return $field;
	}

	//validate 
	 function autoMapping($data, $adapterData){

		foreach ($adapterData as $key => $value) {
			if (!empty($value['HEADER_FUNCTION'])) {

				$mappingTag = explode('_', $value['HEADER_FUNCTION']);

				$functionField[] = array(
					'headerIndex' => $value['HEADER_INDEX'],
					'mappingTag' => $mappingTag[0]
				);
			}
		}
		//var_dump($adapterData);
		//die('here');
		$newData = array();
		foreach ($functionField as $key => $value) {
			
			if (!empty($newData)) {

				foreach ($newData as $newdatakey => $newdatavalue) {
					//select m_mapping
					$select = $db->select()
							->from('M_MAPPING',array('*'))
							->where("TAG = ?", $value['mappingTag'])
							->where("TEXT LIKE ?", $newdatavalue[$value['headerIndex']]);
					$mappingData = $db->fetchRow($select);

					if (!empty($mappingData)) {
						$newdatavalue[$value['headerIndex']] = $mappingData['MAPPING_TO'];

					}
					$newData[$newdatakey] = $newdatavalue;
				}
			}
			else{
 
				foreach ($data as $datakey => $datavalue) {
					//select m_mapping
					$select = $db->select()
							->from('M_MAPPING',array('*'))
							->where("TAG = ?", $value['mappingTag'])
							->where("TEXT LIKE ?", $datavalue[$value['headerIndex']]);
					$mappingData = $db->fetchRow($select);
					
					if (!empty($mappingData)) {
						$datavalue[$value['headerIndex']] = $mappingData['MAPPING_TO'];

					}
					$newData[$datakey] = $datavalue;
					
				}
			}
		}
		
		if(empty($newData)){
			$newData = $data;
		}

		return $newData;
	}

	$filename = basename(__FILE__);
			Application_Helper_General::cronLog($filename, $cronResult, 1);
?>