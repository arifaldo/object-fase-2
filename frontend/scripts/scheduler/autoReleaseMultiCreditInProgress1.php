<?php
require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
require_once 'General/Settings.php';
require_once 'Service/TransferWithin.php';
require_once 'CMD/Payment.php';

$flag = realpath(dirname(__FILE__)).'/inprogressBulkMultiCredit'; //path to flag file

$res = 'Execute Multi Credit In Progress';
$x = 0;

echo 'Bulk Multi Credit Executor'.PHP_EOL;

// if (file_exists($flag)) {
// 	echo 'Queue inprogress at '.date('Y-m-d H:i:s').PHP_EOL;
// }else{
	try{
		$fp = fopen($flag, 'w');
		@fclose($fp);
		echo 'Create flag '.$flag.' at: '.date('Y-m-d H:i:s').PHP_EOL;
	}catch(Exception $e){
		$stringFailed = 'Can\'t create flag at: '.date('Y-m-d H:i:s');
		Application_Helper_General::cronLog(basename(__FILE__),$stringFailed,0);
		echo $stringFailed.PHP_EOL;

		die();
	}

	$db = Zend_Db_Table::getDefaultAdapter();
	$config = Zend_Registry::get('config');
	$listpayroll = array('11','25','31','32');

	$data = $db->select()
		->from	(
			array('P' => 'T_PSLIP'),
			array('*')
		)
		->where('P.PS_STATUS = 8')
		->where('P.SENDFILE_STATUS = 0')
		->where('DATE(P.PS_EFDATE) = DATE(now())')
		->where('P.PS_TYPE NOT IN (?)', $listpayroll) //do not execute payroll

		->order('P.PS_UPDATED ASC')
		->query()->fetchAll();
	;
	
	// Zend_Debug::dump($data);

	if($data){
		foreach($data as $pslip){
			$x = 0;
			$result = array();

			$trx = $db->select()
				->from(array('TT' => 'T_TRANSACTION'), array('*'))
				->joinLeft(array('TP'=>'T_PSLIP'), 'TP.PS_NUMBER = TT.PS_NUMBER', array('CUST_ID','USER_ID' => 'PS_CREATEDBY'))
				->joinLeft(array('MB'=>'M_BENEFICIARY'), 'MB.BENEFICIARY_ID = TT.BENEFICIARY_ID', array('BENEFICIARY_ADDRESS'))
				->where ('TT.PS_NUMBER = ?', $pslip['PS_NUMBER'])
				->where('RIGHT(TT.TRANSACTION_ID, 1) = ?', '1')
				->query()->fetchAll()
			;

			foreach ($trx as $key => $val){
				$x ++;
				if (!(empty($val['SOURCE_ACCOUNT'])) && !(empty($val['BENEFICIARY_ACCOUNT']))){
					$paymentObj = new Payment($val['PS_NUMBER'], $val['CUST_ID'], $val['USER_ID']);

					if ($val['TRANSFER_TYPE'] == "0")
						$return = $paymentObj->sendTransferSingleWithin($pslip, $val);

					if ($val['TRANSFER_TYPE'] == "1" || $val['TRANSFER_TYPE'] == "2")
						$return = $paymentObj->sendTransferSingleDomestic($pslip, $val);

					$result = array_merge_recursive($result, array('TRX' => $return['TRX']));
				}
			}

			if ($result && is_array($result) && count($result))
				$paymentObj->setPaymentCompleted($trx, $result);
		}
	}

	try{
		unlink($flag);
		echo 'Delete flag '.$flag.' at: '.date('Y-m-d H:i:s').PHP_EOL;
	}catch(Exception $e){
		$stringFailed = 'Can\'t delete flag at: '.date('Y-m-d H:i:s');
		Application_Helper_General::cronLog(basename(__FILE__),$stringFailed,0);
		echo $stringFailed.PHP_EOL;

		die();
	}
// }

echo 'AFFECTED ROW(S): '.$res.'('.$x.')'.PHP_EOL;
Application_Helper_General::cronLog(basename(__FILE__), $res, '1');

?>