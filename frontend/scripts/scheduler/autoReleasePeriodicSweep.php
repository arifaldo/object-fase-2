<?php
	require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
	require_once 'CMD/Payment.php';
	require_once 'CMD/SinglePayment.php';
	require_once 'CMD/Validate/ValidatePaymentSingle.php';
	require_once 'CMD/SweepPayment.php';

	$db = Zend_Db_Table::getDefaultAdapter();
	
	$truecounter = 0;
	$falsecounter = 0;
	$cronResult = "";

	// INNER JOIN KE T_TRANSACTION
	// PS PERIODIC_STATUS  2
	// PERIODIC_NEXTDATE (today)
	
	$data = $db->select	()
				->FROM	(array('P' => 'T_PERIODIC'),array('*'))
				->JOIN	(array('D' => 'T_PERIODIC_DETAIL'),'P.PS_PERIODIC = D.PS_PERIODIC',array('*'))
				->JOIN	(array('PS' => 'T_PSLIP'),'P.PS_PERIODIC = PS.PS_PERIODIC AND P.PS_PERIODIC_NEXTDATE = PS.PS_EFDATE',array('*')) 
				->JOIN	(array('T' => 'T_TRANSACTION'),'PS.PS_NUMBER = T.PS_NUMBER',array('*'))
				->WHERE('P.PS_PERIODIC_STATUS = 2')			
				->WHERE('PS.PS_STATUS IN (7,5)')		
				->WHERE('PS.PS_TYPE IN (23,19,20)')		
				//->WHERE('PS.PS_NUMBER = ? ','S20200923Q6W8TYC6')
				->WHERE('DATE(P.PS_PERIODIC_NEXTDATE) = '."'".date('Y-m-d')."'".'')
				#->WHERE('HOUR(PS.PS_EFTIME) = (HOUR(NOW())+1) ')
				->WHERE('DATE(P.PS_PERIODIC_ENDDATE) >= '."'".date('Y-m-d')."'".'')				
				->query()->fetchAll()
				;
				//echo $data;die;
	//echo $data->__toString();die;
	
	//Zend_Debug::dump($data, 'Test'); die;	
		
	if($data)
	{
		//Zend_Debug::dump($data); die;
		foreach($data as $row)
		{
			// $PS_EVERY_PERIODIC_UOM 					= $row['PS_EVERY_PERIODIC_UOM'];
			// $PERIODIC_EVERY							= $row['PS_EVERY_PERIODIC'];
			// $END_DATE								= $row['PS_PERIODIC_ENDDATE'];
			// $PS_TYPE								= $row['PS_TYPE'];
			
			$PS_EVERY_PERIODIC_UOM 					= $row['PS_EVERY_PERIODIC_UOM'];
			$PERIODIC_EVERY							= $row['PS_EVERY_PERIODIC'];
			$END_DATE								= $row['PS_PERIODIC_ENDDATE'];
			$PS_TYPE								= $row['PS_TYPE'];
			$PS_NUMBER								= $row['PS_NUMBER'];
			
			$dataday = $db->select	()
				->FROM	(array('P' => 'T_PERIODIC_DAY'),array('P.*'))
				->JOIN	(array('PS' => 'T_PSLIP'),'P.PERIODIC_ID = PS.PS_PERIODIC',array()) 
				->WHERE('PS.PS_NUMBER = ?',$PS_NUMBER)
				->group('P.DAY_ID')				
				->query()->fetchAll()
				;
				// var_dump($PS_EVERY_PERIODIC_UOM);die;
			// NextDate														
			if ($PS_EVERY_PERIODIC_UOM ==1){ //every day

				
				
				$nextDate = $row['PS_PERIODIC_NEXTDATE'];
				///
				//var_dump($dataday);
				//$dataday = $repeatOn;
																	$arrday = array(
																		'0' => 'sunday',
																		'1' => 'monday',
																		'2' => 'tuesday',
																		'3' => 'wednesday',
																		'4' => 'thursday',
																		'5' => 'friday',
																		'6' => 'saturday'
																	);

																	// get number of day in a week of startdate
																	$datenumb = date("w", strtotime($nextDate));
											//var_dump($nextDate);						
//var_dump($datenumb);die;												
																		foreach ($dataday as $key => $value) {
																			if($value== '0' || $value == '6'){
																					unset($dataday[$key]);
																			}
																		}
																		if (!empty($dataday)) {
																			$nextDateArr = array();
																		foreach ($dataday as $key => $value) {

																			if ($datenumb == 6 || $nextdatenumb == 6) {
																				$nextdatenumb = 0;
																			//	continue;
																			}
																			else{
																				$nextdatenumb++;
																			}

																			//die('here');
																			//else if($nextdatenumb == $value){
																				$string = 'next ' . $arrday[$value['DAY_ID']];
																				//var_dump($string);
																			//	$nextDate = DateTime::createFromFormat('d/m/Y', $date);
																				$date = new DateTime();
																				$date->modify($string);
																				$date = $date->format('Y-m-d');
																				//var_dump($date);
																				//var_dump($nextDate);
																				//$nextDatenow = DateTime::createFromFormat('d/m/Y', $nextDate);
																				//var_dump($nextDatenow);
																				//$nextDatenow = $nextDatenow->format('Y-m-d');
																				//$datenumb = date("w", strtotime($nextDate));
																				
																				$nextDateArr[] = $date;
																				
																			
																			
																			
																		}
																		
																		//var_dump($nextDateArr);die;
																		if(!empty($nextDateArr)){
																				usort($nextDateArr, function($a, $b) {
																						$dateTimestamp1 = strtotime($a);
																						$dateTimestamp2 = strtotime($b);

																						return $dateTimestamp1 < $dateTimestamp2 ? -1: 1;
																					});
																			}
																				$NEXT_DATE = $nextDateArr['0'];
																			
																			//$param['PS_EFDATE'] = $nextDateArr['0'];
																	}
				
				//var_dump($nextDateArr);die;
				
				///
				/*
				$arrday = array(
							'0' => 'sunday',
							'1' => 'monday',
							'2' => 'tuesday',
							'3' => 'wednesday',
							'4' => 'thursday',
							'5' => 'friday',
							'6' => 'saturday'

						);
				// Modify the date it contains
				$datenumb = date('w');
				if($datenumb==6){
					$datename = 0;
				}else{
					$datename = $datenumb+1;
				}
				// var_dump($datename);
				if(!empty($dataday)){
					$resutnext = 0;
					// echo 'here';
					foreach ($dataday as $key => $value) {
						if($datename == $value['DAY_ID']){
							$string = 'next '.$arrday[$datename];
							// var_dump($string);
							$date->modify($string);

							$NEXT_DATE = $date->format('Y-m-d');
							
							$resutnext = 1;
						}
					}
					// var_dump($NEXT_DATE);echo 'ge';
					if(!$resutnext){
						// ;

						$string = 'next '.$arrday[$dataday['0']['DAY_ID']];
						var_dump($string);
						$date->modify($string);
						$NEXT_DATE = $date->format('Y-m-d');
					}

				}else{
					$NEXT_DATE =  date('Y-m-d', strtotime($row['PS_PERIODIC_NEXTDATE'])); 
				}				
				*/
				// $nextDateVal = strtotime($nextDate);
				
				
				// var_dump($NEXT_DATE);
				//echo $NEXT_DATE; die;
				
			}else if($PS_EVERY_PERIODIC_UOM ==2){

				$nextDate =  date('Y-m-d', strtotime($row['PS_PERIODIC_NEXTDATE'])); 
				$nextDateVal = strtotime($nextDate);
				$NEXT_DATE = date("Y-m-d",strtotime("+7 day", $nextDateVal));

			}elseif ($PS_EVERY_PERIODIC_UOM ==3){ //every date
			
				$nextDate =  date('Y-m-d', strtotime($row['PS_PERIODIC_NEXTDATE'])); 
				$nextDateVal = strtotime($nextDate);
				$dateNextMonth = date("Y-m-d",strtotime("+29 days", $nextDateVal));
				$nextMontheVal = strtotime($dateNextMonth);				
				$maxDays=date('t', $nextMontheVal); // hari terakhir bulan berikutnya
				$every = date('j', $nextDateVal);	
				

				if($every == 'last'){
					$NEXT_DATE = date("Y-m-t", strtotime($dateNextMonth));
				}else{
					if ($maxDays >=  $every){
						$every = $every;
					}else{
						$every = $maxDays;
					}
					$nextDate = mktime(0,0,0,date("n", strtotime($dateNextMonth)),$every,date("Y", strtotime($dateNextMonth)));
					$NEXT_DATE = date("Y-m-d",$nextDate);
				}	
				
				
			}
			
			$param = array();			
			$param['PS_SUBJECT'] 					= $row['PS_SUBJECT'];
			$param['PS_CCY'] 						= $row['PS_CCY'];
			$param['PS_EFDATE'] 					= $NEXT_DATE;//Application_Helper_General::convertDate($PS_EFDATE, $this->_dateDBFormat, $this->_dateDisplayFormat);
			$param['EXPIRY_DATE'] 					= "";//Application_Helper_General::convertDate($EXPIRY_DATE, $this->_dateDBFormat, $this->_dateDisplayFormat);
			$param['TRA_AMOUNT'] 					= $row['TRA_AMOUNT'];
			$param['PS_EFTIME'] 					= $row['PS_EFTIME'];
			$param['TRA_MESSAGE'] 					= $row['TRA_MESSAGE'];
			$param['TRA_REFNO'] 					= $row['TRA_REFNO'];
			$param['SOURCE_ACCOUNT'] 				= $row['SOURCE_ACCOUNT'];
			$param['BENEFICIARY_ACCOUNT'] 			= $row['BENEFICIARY_ACCOUNT'];
			$param['BENEFICIARY_ACCOUNT_CCY'] 		= $row['BENEFICIARY_ACCOUNT_CCY'];
			$param['BENEFICIARY_ACCOUNT_NAME'] 		= $row['BENEFICIARY_ACCOUNT_NAME'];
			$param['BENEFICIARY_BANK_NAME'] 		= $row['BENEFICIARY_BANK_NAME'];		// for domestic
			$param['BENEFICIARY_ALIAS_NAME'] 		= $row['BENEFICIARY_ALIAS_NAME'];
			$param['BENEFICIARY_EMAIL'] 			= $row['BENEFICIARY_EMAIL'];
			$param["BENEFICIARY_ADDRESS"] 			= $row['BENEFICIARY_ADDRESS'];			// for domestic
			$param["BENEFICIARY_CITIZENSHIP"] 		= $row['BENEFICIARY_RESIDENT'];		// for domestic
			$param["BENEFICIARY_RESIDENT"] 			= $row['BENEFICIARY_CITIZENSHIP'];			// for domestic
			$param["BENEFICIARY_CITY_CODE"] 		= $row['BENEFICIARY_CITY_CODE'];		// for domestic
			$param["USER_ID"] 						= $row['USER_ID'];						// for domestic
			$param["SOURCE_ACCOUNT_CCY"] 			= $row['SOURCE_ACCOUNT_CCY'];			// for domestic
			$param["SOURCE_ACCOUNT_NAME"] 			= $row['SOURCE_ACCOUNT_NAME'];			// for domestic
			$param["sourceAccountType"] 			= $row['SOURCE_ACCOUNT_TYPE'];			// for domestic
			$param["BANK_CODE"] 					= $row['CLR_CODE'];						// for domestic
			$param["CLR_CODE"]						= $row['CLR_CODE'];						// for domestic
			if ($row['TRANSFER_TYPE'] == 1){												// for domestic
				$param["TRANSFER_TYPE"] = "RTGS";
			}elseif ($row['TRANSFER_TYPE'] == 2){
				$param["TRANSFER_TYPE"] = "SKN";
			}elseif ($row['TRANSFER_TYPE'] == 5){
				$param["TRANSFER_TYPE"] = "ONLINE";				
			}elseif ($row['TRANSFER_TYPE'] == 0){
				$param["TRANSFER_TYPE"] = "PB";				
			}			
			//$param["TRANSFER_TYPE"] 				= $row['TRANSFER_TYPE'];				// for domestic
			$param["LLD_CATEGORY"] 					= (isset($row['BENEFICIARY_CATEGORY']))	? $row['BENEFICIARY_CATEGORY']	: "";			// for domestic
			$param["LLD_IDENTICAL"] 				= (isset($row['TRA_PURPOSE']))		? $row['TRA_PURPOSE']		: "";					// for domestic
			$param["LLD_RELATIONSHIP"] 				= (isset($row['LLD_RELATIONSHIP']))	? $row['LLD_RELATIONSHIP']	: "";					// for domestic
			$param["LLD_PURPOSE"] 					= (isset($row['LLD_PURPOSE']))		? $row['LLD_PURPOSE']		: "";					// for domestic
			$param["LLD_DESCRIPTION"] 				= (isset($row['LLD_DESCRIPTION']))	? $row['LLD_DESCRIPTION']	: "";					// for domestic
			$param["LLD_BENEIDENTIF"] 				= (isset($row['BENEFICIARY_ID_TYPE']))	? $row['BENEFICIARY_ID_TYPE']	: "";					// for domestic
			$param["LLD_BENENUMBER"] 				= (isset($row['BENEFICIARY_ID_NUMBER']))	? $row['BENEFICIARY_ID_NUMBER']	: "";					// for domestic
			//$param["LLD_SENDERIDENTIF"] 			= (isset($row['LLD_SENDERIDENTIF']))	? $row['LLD_SENDERIDENTIF']	: "";					// for domestic
			//$param["LLD_SENDERNUMBER"] 				= (isset($row['LLD_SENDERNUMBER']))	? $row['LLD_SENDERNUMBER']	: "";					// for domestic
			//$param["LLD_CODE"] 						= (isset($row['LLD_CODE']))			? $row['LLD_CODE']			: "";					// for domestic
			//$param["LLD_DESC"] 						= (isset($row['LLD_DESC']))			? $row['LLD_DESC']			: "";					// for domestic
			$param["BENEFICIARY_CITY_CODE"] 		= (isset($row['BENEFICIARY_CITY_CODE']))			? $row['BENEFICIARY_CITY_CODE']			: "";					// for domestic
			
			$param["TRANSFER_FEE"] 					= $row['TRANSFER_FEE'];					// for domestic
			$param['_addBeneficiary'] 				= true; //$this->view->hasPrivilege('BADA');
			$param['_beneLinkage'] 					= false; //$this->view->hasPrivilege('BLBU');
			
			$param['TrfDateType']					= 3;
			$param['PS_PERIODIC']					= $row['PS_PERIODIC'];
			$param['PS_PERIODICID']					= $row['PS_PERIODIC'];
			$param['CROND']							= true;
			$param['TRA_MIN_AMOUNT']				= $row['PS_MIN_AMOUNT']; 
			$param['PS_TYPE']						= $PS_TYPE; 
			$PS_NUMBER								= "";
			//var_dump($param);die;
			//$db->beginTransaction();
			
			if (strtotime($END_DATE) >= strtotime($NEXT_DATE)){

				$param['PHISTORY_STATUS'] = 22; //p_slip_history status recreate
				$param['TRX_ID'] = $row['TRANSACTION_ID']; //p_slip_history status recreate
				//var_dump($row);die;
				// create T_PSLIP
				$SinglePayment = new SinglePayment($PS_NUMBER, $row['CUST_ID'],'System');
				// if ($PS_TYPE == 1){ // within					
				// 	$param['_priviCreate'] 					= 'CRSP';
				// 	$result = $SinglePayment->createPaymentWithin($param);
				// }elseif ($PS_TYPE == 2){ //domesitic					
				// 	$param['_priviCreate'] 					= 'CDFT';
				// 	$result = $SinglePayment->createPaymentDomestic($param);
				// }elseif ($PS_TYPE == 8){ //online			
				// 	$param['_priviCreate'] 					= 'CDFT';
				// 	$result = $SinglePayment->createPaymentDomestic($param);
				if ($PS_TYPE == 23){ //online			
					$param['_priviCreate'] 					= 'CDFT';
					$param['PS_STATUS']						= 7;
					$param['PS_EFDATE']						= $NEXT_DATE;
					$param['SWEEP_SPLIT']					= $row['PS_SWEEP_TYPE'];
					$param['TRA_MIN_AMOUNT']				= $row['PS_MIN_AMOUNT'];
					$param['BENEF_ACCT_BANK_CODE']			= $row['BENEF_ACCT_BANK_CODE'];
					
					$param['ACBENEF_IDNUM']			= $row['BENEFICIARY_ID_NUMBER'];
					$param['ACBENEF_IDTYPE']			= $row['BENEFICIARY_ID_TYPE'];
					$param['ACBENEF_RESIDENT']			= $row['BENEFICIARY_CITIZENSHIP'];
					$param['ACBENEF_CITIZEN']			= $row['BENEFICIARY_RESIDENT'];
					$param['ACBENEF_CATEGORY']			= $row['BENEFICIARY_CATEGORY'];
					$param['BENEFICIARY_ADDRESS2']			= $row['BENEFICIARY_ADDRESS2'];
					$param['SWIFT_CODE']			= $row['SWIFT_CODE'];
					$param['CLR_CODE']			= $row['CLR_CODE'];
					$param['BENEFICIARY_BANK_CITY'] = $row['BENEFICIARY_BANK_CITY'];
					
					


					$result = $SinglePayment->createPaymentSknRtgsSweep($param, $msg);
				// 	var_dump($result);die;
				 }elseif ($PS_TYPE == 19){ //online			
					$param['_priviCreate'] 					= 'CDFT';
					$param['SWEEP_TYPE'] = '19';
					$param['REFERENCE'] = $row['REFERENCE'];
					$SweepPayment = new SweepPayment(null, $row['CUST_ID'], $row['USER_ID']);
					$param['TRANSACTION_DATA'][] = array(
									'SOURCE_ACCOUNT' 			=> $row['SOURCE_ACCOUNT'],
									'SOURCE_ACCOUNT_NAME' 		=> $row['SOURCE_ACCOUNT_NAME'],
							      	'SOURCE_ACCOUNT_TYPE' 		=> 50, //new
							      	'SOURCE_ACCOUNT_BANK_CODE' 	=> $row['SOURCE_ACCOUNT_BANK_CODE'],
							      	'SOURCE_ACCOUNT_CCY' 		=> 'IDR',
							      	'BENEFICIARY_ID' 			=> $row['BENEFICIARY_ID'],
									'BENEFICIARY_ACCOUNT' 		=> $row['BENEFICIARY_ACCOUNT'],
									'BENEFICIARY_ACCOUNT_CCY' 	=> 'IDR',
									'BENEFICIARY_ACCOUNT_NAME' 	=> $row['BENEFICIARY_ACCOUNT_NAME'],
									'BENEFICIARY_ALIAS_NAME' 	=> '',
									'TRANSFER_TYPE' 			=> 'PB',
									'TRA_AMOUNT' 				=> $row['TRA_AMOUNT'],
									'TRA_MESSAGE' 				=> $row['TRA_MESSAGE'],
									'BENEFICIARY_EMAIL' 		=> $row['BENEFICIARY_EMAIL'],
									'TRA_NOTIF' 				=> $row['TRA_NOTIF'],
									'TRA_EMAIL' 				=> $row['TRA_EMAIL'],
									'TRA_SMS' 					=> $row['TRA_SMS'],
									'PS_TYPE'					=> $row['PS_TYPE'],
									'TRANSFER_FEE'				=> null,
									'TRA_REFNO'					=> '',
									
									'TRA_ADDITIONAL_MESSAGE' 	=> '',
									'TRA_REMAIN'				=> $row['TRA_REMAIN']
							);
					$result = $SweepPayment->createOpenSweep($param);
				}elseif ($PS_TYPE == 20){ //online			
					$param['_priviCreate'] 					= 'CDFT';
					$param['SWEEP_TYPE'] = '20';
					$param['REFERENCE'] = $row['REFERENCE'];
					$SweepPayment = new SweepPayment(null, $row['CUST_ID'], $row['USER_ID']);
					$param['TRANSACTION_DATA'][] = array(
									'SOURCE_ACCOUNT' 			=> $row['SOURCE_ACCOUNT'],
									'SOURCE_ACCOUNT_NAME' 		=> $row['SOURCE_ACCOUNT_NAME'],
							      	'SOURCE_ACCOUNT_TYPE' 		=> 50, //new
							      	'SOURCE_ACCOUNT_BANK_CODE' 	=> $row['SOURCE_ACCOUNT_BANK_CODE'],
							      	'SOURCE_ACCOUNT_CCY' 		=> 'IDR',
							      	'BENEFICIARY_ID' 			=> $row['BENEFICIARY_ID'],
									'BENEFICIARY_ACCOUNT' 		=> $row['BENEFICIARY_ACCOUNT'],
									'BENEFICIARY_ACCOUNT_CCY' 	=> 'IDR',
									'BENEFICIARY_ACCOUNT_NAME' 	=> $row['BENEFICIARY_ACCOUNT_NAME'],
									'BENEFICIARY_ALIAS_NAME' 	=> '',
									'TRANSFER_TYPE' 			=> 'PB',
									'TRA_AMOUNT' 				=> $row['TRA_AMOUNT'],
									'TRA_MESSAGE' 				=> $row['TRA_MESSAGE'],
									'BENEFICIARY_EMAIL' 		=> $row['BENEFICIARY_EMAIL'],
									'TRA_NOTIF' 				=> $row['TRA_NOTIF'],
									'TRA_EMAIL' 				=> $row['TRA_EMAIL'],
									'TRA_SMS' 					=> $row['TRA_SMS'],
									'PS_TYPE'					=> $row['PS_TYPE'],
									'TRANSFER_FEE'				=> null,
									'TRA_REFNO'					=> '',
									'TRA_ADDITIONAL_MESSAGE' 	=> '',
									'TRA_REMAIN'				=> $row['TRA_REMAIN']
							);
					$result = $SweepPayment->createOpenSweep($param);
				 }

				

				
							
				$db->beginTransaction();
				// update NEXTDATE PERIOIDC				
				$data = array ('PS_PERIODIC_NEXTDATE' => $NEXT_DATE, 'PS_PERIODIC_CURRENTDATE' => date('Y-m-d'));
				$where['PS_PERIODIC = ?'] = $row['PS_PERIODIC'];
				$db->update('T_PERIODIC',$data,$where);
				
				$db->commit();
								
			}else{
				$db->beginTransaction();
				// update STATUS PERIODIC to be COMPLETED
				$data = array ('PS_PERIODIC_STATUS' => 1, 'PS_PERIODIC_CURRENTDATE' => date('Y-m-d'));
				$where['PS_PERIODIC = ?'] = $row['PS_PERIODIC'];
				$db->update('T_PERIODIC',$data,$where);
				
				$db->commit();
			}
			
			//$db->commit();
						
			
			/*$Payment = new Payment($row['PS_NUMBER'], $row['CUST_ID'], $row['USER_ID']);
			$resultRelease = $Payment->releasePayment();
					
			if ($resultRelease['status'] == '00')
			{
				$truecounter++;
			}
			else
			{
				$falsecounter++;
			}*/
		}
		
		/*if($truecounter>0)
		{
			$cronResult .= " ".$truecounter." payment(s) success";
		}
		if($falsecounter>0)
		{
			$cronResult .= " ".$falsecounter." payment(s) failed";
		}*/		
	}
	else
	{
		$cronResult .= "No data released";
	}

	$filename = basename(__FILE__);
	Application_Helper_General::cronLog($filename, $cronResult, 1);
?>