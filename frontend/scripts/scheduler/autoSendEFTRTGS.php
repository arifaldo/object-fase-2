<?php
	require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
	require_once 'General/RTGSGen1.php';
	require_once 'General/RTGSGen2.php';
	
	$db 	  = Zend_Db_Table::getDefaultAdapter();
	$config   = Zend_Registry::get('config');
	$actionId = 'scheduler';
	$moduleId = 'auto send EFT RTGS';
	
	$now			= date('_Ymd_His');
	$filename1 		= "CIBRTGS1".$now.".txt";			
	$filename2 		= "CIBRTGS2".$now.".txt";	
	$filepath		= LIBRARY_PATH.'data/file/RTGS/';

	$typeCode = $config['transfer']['type']['code']['RTGS'];
	
	$select = $db->select()
				 ->from(array('P' => 'T_PSLIP'),array('T.*', 'P.PS_EFDATE'))
				 ->joinLeft(	array('T' => 'T_TRANSACTION'), 'P.PS_NUMBER = T.PS_NUMBER', array() )
				 ->where('P.PS_STATUS 		= 5')						
				 ->where('T.TRA_STATUS 		= 3')
				 ->where('T.TRANSFER_TYPE	= ?', $typeCode)
				 ->where('T.EFT_STATUS		= 3')	// 1 =  Success, 0 =  Failed, 2 =  Not Delivered, 3 = waiting to send EFT, 4 = in progress sending EFT
				 ->where('DATE(P.PS_EFDATE) = DATE(now())')				
				 ->order('P.PS_UPDATED ASC');
	$data   = $db->fetchAll($select);
	
	echo "<pre>";
	print_r($data);

	if($data)
	{
		// locked trx
		foreach($data as $trx)
		{
			$paymentData = array('EFT_STATUS' 			=> 4);
			$whereData   = array('TRANSACTION_ID = ?'	=> (string) $trx['TRANSACTION_ID']);
			$db->update('T_TRANSACTION', $paymentData, $whereData);
		}
		
		$EFT1 = new RTGSGen1();
		$EFT2 = new RTGSGen2();
		
		foreach($data as $trx)
		{
			$eftBankStatus = $trx['EFT_BANKCODE'];	// 0 = all success, 1 = only GEN 1 success, 2 = only GEN 2 success, 99 = not yet sent
			
			$gen1 = false;
			$gen2 = false;
			
			if ($eftBankStatus != 0)
			{
				if ($eftBankStatus == 99 || $eftBankStatus != 1)
				{	$gen1 = true;	}
				if ($eftBankStatus == 99 || $eftBankStatus != 2)
				{	$gen2 = true;	}
			}
			
			// Generate EFT Gen 1
			if ($gen1 == true)
			{
				$contentEFT1 	= $EFT1->setEFTDetail($trx);
			}
			
			// Generate EFT Gen 2
			if ($gen2 == true)
			{
				$contentEFT2 	= $EFT2->setEFT($trx);
			}

		}
		
		
		$dataEFT1 = $EFT1->getEFT();
		$dataEFT2 = $EFT2->getEFT();
		
		echo "<pre>";
		print_r($dataEFT1);
		echo "<br>";
		print_r($dataEFT2);
		
		$isSuccessEFT1 = true;
		$isSuccessEFT2 = true;
		
		if (!empty($dataEFT1))
		{
			$eftfile1 = file_put_contents($filepath."/".$filename1, $dataEFT1);
			
			if($eftfile1 !== false){
				$sendEFT1Code = "00";	
				$sendEFT1Desc = "RTGS 1 Success write file";	
			}else{
				$sendEFT1Code = "11";	
				$sendEFT1Desc = "RTGS 1 Failed write file";	
				$isSuccessEFT1 = false;
			}
		}
		
		if (!empty($dataEFT2))
		{
			$eftfile2 = file_put_contents($filepath."/".$filename2, $dataEFT2);
			
			if($eftfile2 !== false){
				$sendEFT2Code = "00";	
				$sendEFT2Desc = "RTGS 1 Success write file";	
			}else{
				$sendEFT2Code = "11";	
				$sendEFT2Desc = "RTGS 1 Failed write file";	
				$isSuccessEFT2 = false;
			}
		}
		
		if ($isSuccessEFT1 == true && $isSuccessEFT2 == true) {
			$eftStatus 		= 1; // 1 =  Success, 0 =  Failed, 2 =  Not Delivered, 3 = waiting to send EFT, 4 = in progress sending EFT, 9 = Exception 
			$eftBankCode 	= 0; // 0 = all success, 1 = only GEN 1 success, 2 = only GEN 2 success, 99 = not yet sent 
		} elseif ($isSuccessEFT1 == true && $isSuccessEFT2 == false) {
			$eftStatus 		= 3;
			$eftBankCode 	= 1;
		} elseif ($isSuccessEFT1 == false && $isSuccessEFT2 == true) {
			$eftStatus 		= 3;
			$eftBankCode 	= 2;
		} else {
			$eftStatus 		= 3;
			$eftBankCode 	= 99;
		}
		
		echo "<br>eftStatus: $eftStatus";
		echo "<br>eftBankCode: $eftBankCode";
		
		foreach($data as $trx)
		{
			$paymentData = array('EFT_STATUS' 			=> $eftStatus,
								 'EFT_BANKCODE' 		=> $eftBankCode,
								 'EFT_BANKRESPONSE' 	=> $sendEFT1Code."|".$sendEFT2Code.":".$sendEFT1Desc."|".$sendEFT2Desc,
								);
			$whereData   = array('TRANSACTION_ID = ?'	=> (string) $trx['TRANSACTION_ID']);
			$db->update('T_TRANSACTION', $paymentData, $whereData);
		}
		
	}
	else
	{
		echo " No RTGS In Progress";
	}

?>