<?php

	require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
	require_once 'General/Customer.php';		
	require_once 'CMD/Payment.php';
	require_once 'General/Settings.php';  
	
	// $emailSent = 0;
	/*
		multi error: Host Timeout
		
	*/
	
	$type 		= 'EXCEPTION';
	$config    		= Zend_Registry::get('config');
	
	$Settings = new Settings();
	$templateEmailMasterBankAddress = $Settings->getSetting('master_bank_address');
	$templateEmailMasterBankAppName = $Settings->getSetting('master_bank_app_name');
	$templateEmailMasterBankAppUrl = $Settings->getSetting('master_bank_app_url');
	$templateEmailMasterBankCity = $Settings->getSetting('master_bank_city');
	$templateEmailMasterBankCountry = $Settings->getSetting('master_bank_country');
	$templateEmailMasterBankEmail = $Settings->getSetting('master_bank_email');
	$templateEmailMasterBankFax = $Settings->getSetting('master_bank_fax');
	$templateEmailMasterBankName = $Settings->getSetting('master_bank_name');
	$templateEmailMasterBankProvince = $Settings->getSetting('master_bank_province');
	$templateEmailMasterBankTelp = $Settings->getSetting('master_bank_telp');
	$templateEmailMasterBankWebsite = $Settings->getSetting('master_bank_website');
	
	$template 	= file_get_contents(LIBRARY_PATH.'/email template/Exception Email Notification.html');
	$FOOTER_ID = file_get_contents(LIBRARY_PATH.'/email template/footer_id.txt');
	$FOOTER_EN = file_get_contents(LIBRARY_PATH.'/email template/footer_en.txt');
	
	$transFT   = array( '[[APP_NAME]]' 			=> $templateEmailMasterBankAppName,
						'[[BANK_NAME]]' 		=> $templateEmailMasterBankName,
						'[[APP_URL]]' 			=> $templateEmailMasterBankAppUrl													
					  );			
	$FOOTER_ID = strtr($FOOTER_ID, $transFT);
	$FOOTER_EN = strtr($FOOTER_EN, $transFT);

	$db 		= Zend_Db_Table::getDefaultAdapter();
	
	$subject 	= 'Payment Exception Notification';
	
	
	$paymentType  	= $config["payment"]["type"];	
	$transferType 	= $config["transfer"]["type"];
	$paymentStatus 	= $config["payment"]["status"];  // 9 exception
	
	$arrPaySingle = array('1','2');
	
	$casePaymentStatus 	= Application_Helper_General::caseArray($paymentStatus);
	$caseArr 		 	= Application_Helper_General::casePaymentType($paymentType, $transferType);
	$casePaymentType 	= $caseArr["PS_TYPE"];
	$casePaymentTXType 	= $caseArr["TX_TYPE"];
	
	$payReffArr = array();
	//$settings 	= new Settings();
	$emailTo  	= $Settings->getSetting('email_exception');
	$emailTo 	= explode(";", $emailTo);
	
	$selectTPSLIP = $db->select()
					   ->from(		array(	'P'	=>'T_PSLIP'),array())
					    ->joinLeft(	array(	'T'	=>'T_TRANSACTION'),'P.PS_NUMBER = T.PS_NUMBER',
									array(
											'paymentref'	=> 'P.PS_NUMBER',
											'errorcode'		=> new Zend_Db_Expr("CASE 	WHEN P.PS_TXCOUNT = '1' 
																						THEN T.BANK_RESPONSE
																						ELSE 'Host Timeout' END"),
											'description'	=> new Zend_Db_Expr("CASE 	WHEN P.PS_TXCOUNT = '1' 
																						THEN T.TRA_MESSAGE
																						ELSE '-' END"),
											'paymentdate'	=> 'P.PS_EFDATE',
											'paymenttype'	=> new Zend_Db_Expr("CASE $casePaymentType ELSE 'N/A' END"),
											'paymentstatus'	=> new Zend_Db_Expr("CASE P.PS_STATUS $casePaymentStatus ELSE 'N/A' END"),
											
										))
					   ->joinLeft(	array(	'E' =>'T_EMAIL_NOTIFICATION'),'P.PS_NUMBER = E.PS_NUMBER',array())
					  
					   ->where('P.PS_STATUS = ?', (string)$paymentStatus["code"]["exception"])
					   // ->where("P.PS_TYPE in (?)", $arrPaySingle)
					   ->where('E.PS_ISEXCEPTIONEMAILED <> 1 OR E.PS_ISEXCEPTIONEMAILED is null');
						
//	$sTPSLIP 	= $selectTPSLIP ->__toString();
//	$sTTX		= $selectTTX	->__toString();
	
	// Zend_Debug::dump($data);die;
	
//	$unionquery = $db->select()
//					 ->union(array($sTPSLIP,$sTTX));
		
//	$select 	= $db->select()
//					 ->from(($unionquery),array('*'));
	
//	$dataSQL 	= $db->fetchAll($select);	
	$dataSQL 	= $db->fetchAll($selectTPSLIP);	

// 	Zend_Debug::dump($dataSQL);die;	
// 	Zend_Debug::dump($emailTo);die;	
	
	if(is_array($emailTo) && count($emailTo) > 0){
	
		if(is_array($dataSQL) && count($dataSQL) > 0){
			
			try{
				
				$db->beginTransaction();
				$no = 0;
				$datacontent = NULL;
				foreach($dataSQL as $pslip){
				
					$payReffArr[] = $pslip["paymentref"];
					
					$no++;
					
					if($pslip["errorcode"]) $val = $pslip["errorcode"];
					else 					$val = '-';	
					
					if($pslip["description"])	$valdesc = $pslip["description"];
					else 						$valdesc = '-';
					
					$datacontent .= "<tr>
										<td>".$no."</td>
										<td>".$pslip["paymentref"]."</td>
										<td>".$val."</td>
										<td>".$valdesc."</td>
										<td>".Application_Helper_General::convertDate($pslip["paymentdate"],'dd MMM yyyy')."</td>
										<td>".$pslip["paymenttype"]."</td>
									</tr>";
					
				}
				
				$translate = array( 
											'[[DATA_CONTENT]]' => $datacontent,
											'[[FOOTER_ID]]' 		=> $FOOTER_ID,
											'[[FOOTER_EN]]' 		=> $FOOTER_EN,
											'[[APP_NAME]]' 			=> $templateEmailMasterBankAppName,
											'[[BANK_NAME]]' 		=> $templateEmailMasterBankName,
											'[[APP_URL]]' 			=> $templateEmailMasterBankAppUrl,
											'[[master_bank_name]]' 	=> $templateEmailMasterBankName,
											'[[master_bank_fax]]' 	=> $templateEmailMasterBankFax,
											'[[master_bank_address]]' 	=> $templateEmailMasterBankAddress,
											'[[master_bank_telp]]' 	=> $templateEmailMasterBankTelp,
										);
				$mailContent 	= strtr($template,$translate);
				
				// echo $datacontent;
				
				foreach($emailTo as $key => $val){
					
					$result = Application_Helper_Email::sendEmail($val,$subject,$mailContent);
					// echo "$result - $val <br />";
				}
				
				// echo $mailContent;
				// print_r ($payReffArr);
				
				$db->commit();
				
				Application_Helper_General::setPaymentEmailNotification($type, $payReffArr);	
// 				Application_Helper_General::setTxEmailNotification($type, $payReffArr);	
				
				
			}
			catch(Exception $e) 
			{
				$db->rollBack();
				$string = $e->getMessage();
				echo "Failed!"; die;
			}
			
		}
	}
	$msg = implode(" | ", $payReffArr);

	Application_Helper_General::cronLog(basename(__FILE__),$msg,1);
	
?>
