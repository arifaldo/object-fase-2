<?php
require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
require_once 'General/Settings.php';
require_once 'Service/TransferWithin.php';
require_once 'CMD/Payment.php';

$flag	= realpath(dirname(__FILE__)).'/inprogressBulkPayroll'; //path to flag file
//unlink($flag);

$time = array();
$time['start'] = date('Y-m-d H:i:s');

$date = date_create($time['start']);
date_add($date, date_interval_create_from_date_string('5 minutes'));

$time['end'] = date_format($date, 'Y-m-d H:i:s');


$Settings = new Settings();
$escrow['NO']			= $Settings->getSetting('acct_payroll');
$escrow['CCY']			= 'IDR';
$escrow['NAME']			= 'ESCROW PAYROLL';

$res 	= 'Execute Payroll In Progress';
$x 		= 0;

echo 'Bulk Payroll Executor'.PHP_EOL;

//if (file_exists($flag)) {
	///echo 'Queue inprogress at '.date('Y-m-d H:i:s').PHP_EOL;

/*}else{
	try{
		$fp = fopen($flag, 'w');
		@fclose($fp);
		echo 'Create flag '.$flag.' at: '.date('Y-m-d H:i:s').PHP_EOL;
	}catch(Exception $e){
		$stringFailed = 'Can\'t create flag at: '.date('Y-m-d H:i:s');
		Application_Helper_General::cronLog(basename(__FILE__),$stringFailed,0);
		echo $stringFailed.PHP_EOL;

		die();
	}
*/
	$db = Zend_Db_Table::getDefaultAdapter();
	$config = Zend_Registry::get('config');
	$Settings = new Settings();

	//while(strtotime(date('Y-m-d H:i:s')) <= strtotime($time['end'])){
		echo '.';
		//Zend_Debug::dump($data);die;

		$data = $db->select()
			->from(
				array('P' => 'T_PSLIP'),
				array('*')
			)
			->where('P.PS_STATUS = 8')
			->where('P.SENDFILE_STATUS = 0')
			->where('DATE(P.PS_EFDATE) = DATE(now())')
			->where('P.PS_TYPE = ?', '11') //only execute payroll
			->order('P.PS_UPDATED ASC')
			->query()->fetchAll();
		;

		//Zend_Debug::dump($data);die;
		if($data){
			//echo 'masuk'; die;
			foreach($data as $pslip){
				$x = 0;
				$result = array();
				
				$trxx = $db->select()
					->from(array('TT' => 'T_TRANSACTION'), array('*'))
					->joinLeft(array('TP'=>'T_PSLIP'), 'TP.PS_NUMBER = TT.PS_NUMBER', array('CUST_ID','USER_ID' => 'PS_CREATEDBY'))
					->joinLeft(array('MB'=>'M_BENEFICIARY'), 'MB.BENEFICIARY_ID = TT.BENEFICIARY_ID', array('BENEFICIARY_ADDRESS'))
					->where ('TT.PS_NUMBER = ?', $pslip['PS_NUMBER'])
					;
					
				$valtrx = $db->fetchRow($trxx);
				$valCustId = $valtrx['CUST_ID'];
				$valUserId = $valtrx['USER_ID'];
				$source['NO'] 	= $valtrx['SOURCE_ACCOUNT'];
				$source['CCY'] 	= $valtrx['SOURCE_ACCOUNT_CCY'];
				$source['NAME'] = $valtrx['SOURCE_ACCOUNT_NAME'];
				
												
				$trxEA["TRANSACTION_ID"] 					= "PAYROLL"; //
				$trxEA["PS_NUMBER"] 						= $pslip['PS_NUMBER']; //
				$trxEA["SOURCE_ACCOUNT"] 					= $source['NO']; //
				$trxEA["SOURCE_ACCOUNT_CCY"] 				= $source['CCY']; //
				$trxEA["SOURCE_ACCOUNT_NAME"] 				= $source['NAME']; //
				$trxEA["SOURCE_ACCOUNT_ALIAS_NAME"] 		= $source['NAME']; //
				$trxEA["BENEFICIARY_ID"] 					= "464"; //
// 				$trxEA["BENEFICIARY_ACCOUNT"] 				= "6000000001"; //
// 				$trxEA["BENEFICIARY_ID"] 					= $Settings->getSetting('acct_payroll');
				$trxEA["BENEFICIARY_ACCOUNT"] 				= $Settings->getSetting('acct_payroll');
				
				$trxEA["BENEFICIARY_ACCOUNT_CCY"] 			= "IDR"; //
				$trxEA["BENEFICIARY_ACCOUNT_NAME"] 			= "ESCROW PAYROLL"; //
				$trxEA["BENEFICIARY_ALIAS_NAME"] 			= "ESCROW PAYROLL"; //
				$trxEA["BENEFICIARY_EMAIL"] 				= "";
				$trxEA["BENEFICIARY_MOBILE_PHONE_NUMBER"] 	= "";
				$trxEA["BENEFICIARY_CITIZENSHIP"] 			= NULL;
				$trxEA["BENEFICIARY_CITIZENSHIP_COUNTRY"] 	= NULL;
				$trxEA["BENEFICIARY_RESIDENT"] 				= NULL;
				$trxEA["BENEFICIARY_ADDRESS"] 				= "";
				$trxEA["BENEFICIARY_ADDRESS2"] 				= NULL;
				$trxEA["BENEFICIARY_ADDRESS3"] 				= NULL;
				$trxEA["BENEFICIARY_DATA"] 					= NULL;
				$trxEA["SWIFT_CODE"] 						= NULL;
				$trxEA["CLR_CODE"] 							= NULL;
				$trxEA["ORG_DIR"] 							= NULL;
				$trxEA["BENEFICIARY_BI_ACCOUNT"] 			= NULL;
				$trxEA["BENEFICIARY_BANK_NAME"] 			= NULL;
				$trxEA["BENEFICIARY_BANK_ADDRESS1"] 		= NULL;
				$trxEA["BENEFICIARY_BANK_ADDRESS2"] 		= NULL;
				$trxEA["BENEFICIARY_BANK_ADDRESS3"] 		= NULL;
				$trxEA["BENEFICIARY_BANK_CITY"] 			= NULL;
				$trxEA["BENEFICIARY_BANK_BRANCH"] 			= NULL;
				$trxEA["BENEFICIARY_BANK_COUNTRY"] 			= NULL;
				$trxEA["NOSTRO_CODE"] 						= NULL;
				$trxEA["NOSTRO_NAME"] 						= NULL;
				$trxEA["LLD_CODE"] 							= NULL;
				$trxEA["LLD_DESC"] 							= NULL;
				$trxEA["EFT_STATUS"] 						= "2";
				$trxEA["EFT_BANKCODE"] 						= "99";
				$trxEA["EFT_BANKRESPONSE"] 					= NULL;
				$trxEA["TRA_AMOUNT"] 						= $pslip['PS_TOTAL_AMOUNT']; //
				$trxEA["TRA_MESSAGE"] 						= "PAYROLL"." ".$valCustId;
				$trxEA["TRA_ADDITIONAL_MESSAGE"] 			= NULL;
				$trxEA["TRA_REFNO"] 						= "";
				$trxEA["TRA_STATUS"] 						= "2";
				$trxEA["TRA_CHARGE_TO"] 					= "0";
				$trxEA["TRANSFER_TYPE"] 					= "0";
				$trxEA["TRANSFER_FEE"] 						= "0.00";
				$trxEA["TRANSFER_FEE_STATUS"] 				= NULL;
				$trxEA["FULL_AMOUNT_FEE"] 					= "0.00";
				$trxEA["PROVISION_FEE"] 					= "0.00";
				$trxEA["DISKONTO_AMOUNT"] 					= NULL;
				$trxEA["BANK_RESPONSE"] 					= NULL;
				$trxEA["REVERSAL_DESC"] 					= NULL;
				$trxEA["RAW_REQUEST"] 						= NULL;
				$trxEA["DISPLAY_FLAG"] 						= "0"; //
				$trxEA["UUID"] 								= NULL;
				$trxEA["CUST_ID"] 							= $valCustId; //
				$trxEA["USER_ID"] 							= $valUserId; //

				//debt SA to EA
				$paymentObj = new Payment($pslip['PS_NUMBER'], $valtrx['CUST_ID'], $valtrx['USER_ID']);
				$resSA = $paymentObj->sendTransferSinglePayroll($pslip, $trxEA);

				if ($resSA['ResponseCode'] == '00' || $resSA['ResponseCode'] == '0000' || $resSA['ResponseCode'] == 'XT' || $resSA['ResponseCode'] == '9999' || $resSA['ResponseCode'] == '99' || $resSA['ResponseCode'] == '68'){
					
					$trx = $db->select()
						->from(array('TT' => 'T_TRANSACTION'), array('*'))
						->joinLeft(array('TP'=>'T_PSLIP'), 'TP.PS_NUMBER = TT.PS_NUMBER', array('CUST_ID','USER_ID' => 'PS_CREATEDBY','PS_TYPE'))
						->joinLeft(array('MB'=>'M_BENEFICIARY'), 'MB.BENEFICIARY_ID = TT.BENEFICIARY_ID', array('BENEFICIARY_ADDRESS'))
						->where ('TT.PS_NUMBER = ?', $pslip['PS_NUMBER'])
						->query()->fetchAll()
					;
							
	
					foreach ($trx as $key => $val){
						$x ++;
						if (!(empty($val['SOURCE_ACCOUNT'])) && !(empty($val['BENEFICIARY_ACCOUNT']))){
							
	
							
	
							$valCustId = $val['CUST_ID'];
							$valUserId = $val['USER_ID'];
							
							$select3 = $db->select()
							          ->from(array('C' => 'M_CUSTOMER'), array('*'))
									  ->where('CUST_ID = ?',$valCustId)
									  ;
							$data2 = $db->fetchRow($select3);
							$custName = $data2['CUST_NAME'];
													
	
							//override existing value and FT from EA to BA
							$val['SOURCE_ACCOUNT'] 				= $escrow['NO'];
							$val['SOURCE_ACCOUNT_CCY'] 			= $escrow['CCY'];
							$val['SOURCE_ACCOUNT_ALIAS_NAME'] 	= $escrow['NAME'];
							$val['SOURCE_ACCOUNT_NAME'] 		= $escrow['NAME'];
	
							if ($val['TRANSFER_TYPE'] == "0")
								$return = $paymentObj->sendTransferSinglePayrollDistribution($pslip, $val);
	
							if ($val['TRANSFER_TYPE'] == "1" || $val['TRANSFER_TYPE'] == "2")
								$return = $paymentObj->sendTransferSingleDomestic($pslip, $val);
	
							$result = array_merge_recursive($result, array('TRX' => $return['TRX']));
	
							// add for send email
	
							// hardcode param
							$param['PAYMENT_REF'] = "";
							$param['AMOUNT_CCY'] = "";
							$param['SOURCE_AMOUNT'] = "";
							$param['TRX_FEE'] = "0";
							$param['COA_ACCOUNT'] = "0";
							$param['MESSAGE'] = "PAYROLL"." ".$valCustId;;
							$param['TRA_AMOUNT'] = "0";
							$param['REF_NO'] = "0";
							$param['TRANSACTION_ID'] = "";
							$param['CUST_ID'] = "";
							$param['USER_ID'] = "";
							$param['SOURCE_ACCOUNT'] = "";
							$param['SOURCE_ACCOUNT_CCY'] = "";
							$param['SOURCE_ACCOUNT_NAME'] = "";
							$param['BENEFICIARY_ACCOUNT'] = "";
							$param['BENEFICIARY_ACCOUNT_NAME'] = "";
	
							/*$withinTransfer 	  = new Service_TransferWithin($param);
							$returnStruct = $withinTransfer->sendTransfer();*/
	
							//insert template email - begin
	
							/*$template 	= file_get_contents(LIBRARY_PATH.'/email template/email notification payroll.html');
							$subject 	= 'Payroll Notification';
	
							$FOOTER_ID = file_get_contents(LIBRARY_PATH.'/email template/footer_id.txt');
							$FOOTER_EN = file_get_contents(LIBRARY_PATH.'/email template/footer_en.txt');
	
							//hardcode _paymentRef
							$PS_NUMBER_EMAIL = ""; //$this->_paymentRef;
							
							//tambahan
							$config	= Zend_Registry::get('config');
							$dateSmsFormat = $config['date']['sms']['format'];
							$statusSMS = $config['transfer']['status']['code'];
							$statusSMS = array_flip($statusSMS);
							$ts = $statusSMS[$val['TRA_STATUS']];
							$statusSMS = $config['transfer']['status']['desc'][$ts];
	
							$STATUS_EMAIL = $config['transfer']['status']['desc'][$ts]; //$val['SOURCE_ACCOUNT_CCY'].' '.Application_Helper_General::displayMoney($val['TRA_AMOUNT']);
							
							//$TRA_AMOUNT_EMAIL .= $val['SOURCE_ACCOUNT_CCY'].' '.Application_Helper_General::displayMoney($val['TRA_AMOUNT']);
	
							$fee = $val['SOURCE_ACCOUNT_CCY'].' 0.00';
							
							$totalPembayaran = $val['TRA_AMOUNT'] + $fee;
							
							$TRA_AMOUNT_EMAIL .= $val['SOURCE_ACCOUNT_CCY'].' '.Application_Helper_General::displayMoney($totalPembayaran);
							
							$DATE_EMAIL = date("d M Y");
							$SOURCE_ACCOUNT_EMAIL = $val['SOURCE_ACCOUNT'];
							//$STATUS_EMAIL .= "Berhasil";
							$templateEmailMasterBankName = $Settings->getSetting('master_bank_name');
							$templateEmailMasterBankTelp = $Settings->getSetting('master_bank_telp');
							$templateEmailMasterBankEmail = $Settings->getSetting('master_bank_email');
							$templateEmailMasterBankEmail1 = $Settings->getSetting('master_bank_email1');
							$templateEmailMasterBankAddress = $Settings->getSetting('master_bank_address');
							$templateEmailMasterBankFax = $Settings->getSetting('master_bank_fax');
	
							//$STATUS_EMAIL = ($returnStruct['ResponseCode'] == '0000')?'Success':'Failed';
	
							$MENU_NAME = 'Order ID';
							$translate = array(
								'[[PS_NUMBER_EMAIL]]' 	=> $PS_NUMBER_EMAIL,
								'[[TRANSACTION_MENU]]' 	=> "", //$PS_CATEGORY_EMAIL,
								'[[TRA_AMOUNT_EMAIL]]' 	=> $TRA_AMOUNT_EMAIL,
								'[[DATE_EMAIL]]' 		=> $DATE_EMAIL,
								'[[HANDPHONE_NO]]' 		=> $val['TRA_REFNO'],
								'[[MENU_NAME]]' 		=> $MENU_NAME,
								'[[PROVIDER_NAME]]' 	=> "",// $PROVIDER_NAME,
								'[[CUSTOMER_NAME]]' 	=> "", //$CUSTOMER_NAME,
								//'[[SOURCE_ACCOUNT_EMAIL]]' => $SOURCE_ACCOUNT_EMAIL,
								'[[STATUS_EMAIL]]' 		=> $STATUS_EMAIL,
								'[[master_bank_name]]' 	=> $templateEmailMasterBankName,
								'[[master_bank_telp]]' 	=> $templateEmailMasterBankTelp,
								'[[MASTER_BANK_EMAIL]]' => $templateEmailMasterBankEmail,
								'[[MASTER_BANK_EMAIL1]]'=> $templateEmailMasterBankEmail1,
								'[[master_bank_address]]' 	=> $templateEmailMasterBankAddress,
								'[[master_bank_fax]]' 	=> $templateEmailMasterBankFax,
								'[[CHARGES]]' 			=> $fee,
								'[[FOOTER_ID]]' 		=> $FOOTER_ID,
								'[[FOOTER_EN]]' 		=> $FOOTER_EN,
								'[[CompanyCode]]' 		=> $valCustId,
								'[[CompanyName]]'		=> $custName,
								'[[Ref]]' 				=> $val['TRANSACTION_ID'],
								'[[BENE_ACCOUNT]]' 		=> "xxxxxxx".''.substr($val['BENEFICIARY_ACCOUNT'], -3),
								'[[BENE_ACCOUNT_NAME]]'	=> $val['BENEFICIARY_ACCOUNT_NAME'],
								'[[CCY]]'				=> $val['SOURCE_ACCOUNT_CCY'],
								'[[AMOUNT]]'			=> Application_Helper_General::displayMoney($val['TRA_AMOUNT']),
								'[[MESSAGE]]'			=> $val['TRA_MESSAGE'],							
							);
	
	
							$userData = $db->select()
									->from(array('M_USER'),array('USER_ID','USER_EMAIL'))
									->where('USER_ID = ?', $val['USER_ID'])
									->limit(1)
								;
							$userData = $db->fetchRow($userData);
	
							$useremail = trim($val['BENEFICIARY_EMAIL']);
							$userMobilePhone = trim(str_replace('(','',$val['BENEFICIARY_MOBILE_PHONE_NUMBER']));
							$userMobilePhone = str_replace(')','',$userMobilePhone);
							$userMobilePhone = str_replace('-','',$userMobilePhone);
	
							//kirim email
							$mailContent 	= strtr($template,$translate);
							if (!(empty($useremail)))
								Application_Helper_Email::queueEmail($useremail,$subject,$mailContent);
	
							//insert template email - end
				
														
							//kirim sms
							$message = $Settings->getSetting('transaction_success_payroll');
	
							$trans = array(
								"[[CompanyCode]]" 		=> $valCustId,
								"[[CompanyName]]" 		=> $custName,
								"[[Amount]]" 			=> $val['SOURCE_ACCOUNT_CCY'].' '.Application_Helper_General::displayMoney($val['TRA_AMOUNT']), //$val['TRA_AMOUNT'],
								"[[DateTime]]" 			=> date("d M Y"),
								"[[Ref]]" 				=> $val['TRANSACTION_ID'],
								"[[Desc]]" 				=> $val['TRA_MESSAGE'],
								"[[Status]]" 			=> $statusSMS);
	
							$newMessage = strtr($message, $trans);
							$sms = new Service_SMS($userMobilePhone,$newMessage);
							$sms->queueSMS();
							*/
						}
					}
	
					if ($result && is_array($result) && count($result))
						
					$releaserEmailList = array();
						if (true){
							//var_dump($trx);
							//try{
							$selectTrx	= $db->select()
									->from(array('M_EMAIL_SETTING'), array('*'))
									->where('CUST_ID = ?', $trx[0]['CUST_ID'])
									->where('PS_TYPE = ?', $trx[0]['PS_TYPE'])
									->where('STATUS  = ?', '1');
							//	echo $selectTrx;
									$releaserEmail = $db->fetchAll($selectTrx);
			//Zend_Debug::dump($releaserEmail);die('gere');						
									$releaserEmailList = array();
									if(!empty($releaserEmail)){
										$releaserEmailList = explode(';', $releaserEmail['0']['EMAIL']);
									}
									
							//}catch(Exception $e){
							//	var_dump($e);
						//	}
							
							$newreleaserEmailList = array();
							if(!empty($releaserEmailList)){
								
								foreach($releaserEmailList as $key => $emailval){
									$newreleaserEmailList[$key]['USER_EMAIL'] = $emailval;
								}
								$releaserEmailList = $newreleaserEmailList;
							}
							//var_dump($releaserEmailList);die;
							$paymentObj->setPaymentCompleted($trx, $result, false, array(), $releaserEmailList);
						}
						
						//$paymentObj->setPaymentCompleted($trx, $result);
											
				}else{ // if trf to escrow failed
					
					//update T_PSLIP
					$paymentData = array(
				    'PS_UPDATED'=>new Zend_Db_Expr("GETDATE()"),
				    'PS_STATUS' => 6,
				    );
				 
				    $wherePayment = array(
				       'PS_NUMBER = ?'  => (string) $pslip['PS_NUMBER']
				       //'USER_ID = ?'  	=> $valUserId,
				    );
				    
				    $db->update('T_PSLIP', $paymentData,$wherePayment);
				    
				    //update T_TRANSACTION
				    $paymentTrx  = array ( 'TRA_STATUS'   => 4,
				          'BANK_RESPONSE'  => $bankResponse,				          
				         );
				    $whereTrx = array('PS_NUMBER = ?'  => $pslip['PS_NUMBER']
				          //'TRANSACTION_ID = ?' => (string) $trxID
				         );
				
				    $db->update('T_TRANSACTION', $paymentTrx, $whereTrx);
				}			
			}
		}

	//	sleep(1);
	//}
/*
	try{
		unlink($flag);
		echo 'Delete flag '.$flag.' at: '.date('Y-m-d H:i:s').PHP_EOL;
	}catch(Exception $e){
		$stringFailed = 'Can\'t delete flag at: '.date('Y-m-d H:i:s');
		Application_Helper_General::cronLog(basename(__FILE__),$stringFailed,0);
		echo $stringFailed.PHP_EOL; */

//		die();
	//}
//}

echo 'AFFECTED ROW(S): '.$res.'('.$x.')'.PHP_EOL;
Application_Helper_General::cronLog(basename(__FILE__), $res, '1');

?>