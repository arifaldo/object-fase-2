<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once realpath(dirname(__FILE__)) .'/../../../library/SGO/Helper/Encryption.php';
$key 	= 'kbsjgu7y9684vyhifq3796487d4ka0x5';
$iv 	= '86ipndw356yj79rf';

class Mcrypt {
	
	private $privateKey;

	public function setPrivateKey($privateKey){
		$this->privateKey = $privateKey;
	}
	
	public function getPrivateKey(){
		return $this->privateKey;
	}
	
	function encrypt($input,$iv){
		$td = mcrypt_module_open(MCRYPT_RIJNDAEL_128, "", MCRYPT_MODE_CBC, $iv);
		$key = $this->getPrivateKey();
		
		mcrypt_generic_init($td, $key, $iv);
		$cyper_text = mcrypt_generic($td, $input);
		mcrypt_generic_deinit($td);
		mcrypt_module_close($td);

		return base64_encode($cyper_text);
	}

	function decrypt($input,$iv){
		$td = mcrypt_module_open(MCRYPT_RIJNDAEL_128, "", MCRYPT_MODE_CBC, $iv);
		$key = $this->getPrivateKey();
		mcrypt_generic_init($td, $key, $iv);
		$decrypted_text = mdecrypt_generic($td, base64_decode($input));
		mcrypt_generic_deinit($td);
		mcrypt_module_close($td);

		return trim($decrypted_text);
	}
}

$input     		= (array_key_exists(1, $argv)) ? $argv[1] : null;
$type     		= (array_key_exists(2, $argv)) ? $argv[2] : null;
$method		= (array_key_exists(3, $argv)) ? $argv[3] : null;

if(empty($input))
{
	echo '-- No Input Found --'.PHP_EOL;
	echo 'Execute command with :'.PHP_EOL;
	echo '- First Argument	: String to process'.PHP_EOL;
	echo '- Second Argument	: encrypt/decrypt default(encrypt)'.PHP_EOL;
	echo '- Third Argument	: int(1|2) default(1)'.PHP_EOL;
	echo '    1 = mcrypt'.PHP_EOL;
	echo '    2 = openssl'.PHP_EOL;
	exit();
}
else{
	if( empty($type) ||  !in_array($type,array('encrypt','decrypt')) ) $type = 'encrypt';
	if( empty($method) ||  !in_array($method,array('1','2')) ) $method = 1;
	
	echo PHP_EOL;
	echo PHP_EOL;
	echo "Input String = ".$input.PHP_EOL;
	
	if( $method == '1' )
	{
		echo '-- USING MCRYPT-- '.PHP_EOL; 
		$encrypt = new Mcrypt();
		$encrypt->setPrivateKey($key);
	}
	else{
		echo '-- USING OPENSSL-- '.PHP_EOL; 
		$encrypt = new SGO_Helper_Encryption();
		$encrypt->setPrivateKey($key);
	}
	
	if( $type == 'encrypt' )
		$result = $encrypt->encrypt($input,$iv);
	else
		$result = $encrypt->decrypt($input,$iv);
		
	echo "Processed String = ".$result;
	echo PHP_EOL;
	echo PHP_EOL;
}



/*
php encryption.php
-- No Input Found --
Execute command with :
- First Argument        : String to process
- Second Argument       : encrypt/decrypt default(Encrypt)
- Third Argument        : int(1|2) default(1)
    1 = mcrypt
    2 = openssl
	
==================================
		ENCRYPT USING OPENSSL
---------------------------------------------------------------
php encryption.php test encrypt 2

Input String = test
-- USING OPENSSL--
Processed String = Qk1KN2t3c3prMkw3VitHdEN4NDUxdz09OjozcjB2YXd2MGR0c2FvZnhv

---------------------------------------------------------------
		DECRYPT USING OPENSSL
---------------------------------------------------------------
php encryption.php Qk1KN2t3c3prMkw3VitHdEN4NDUxdz09OjozcjB2YXd2MGR0c2FvZnhv decrypt 2

Input String = Qk1KN2t3c3prMkw3VitHdEN4NDUxdz09OjozcjB2YXd2MGR0c2FvZnhv
-- USING OPENSSL--
Processed String = test
==================================


==================================
		ENCRYPT USING MCRYPT
---------------------------------------------------------------
php encryption.php test encrypt 1

Input String = test
-- USING MCRYPT--
Processed String = JwwvWU4q70jHQuc7yITfbA==

---------------------------------------------------------------
		DECRYPT USING MCRYPT
---------------------------------------------------------------
php encryption.php JwwvWU4q70jHQuc7yITfbA== decrypt 1

Input String = JwwvWU4q70jHQuc7yITfbA
-- USING MCRYPT--
Processed String = test
==================================

*/