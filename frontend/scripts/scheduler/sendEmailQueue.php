<?php
require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
$db = Zend_Db_Table::getDefaultAdapter();

date_default_timezone_set('Asia/Makassar');

$time = array();
$time['start'] = date('Y-m-d H:i:s');

echo 'Start queue at '.$time['start'];
echo PHP_EOL;

$date = date_create($time['start']);
date_add($date, date_interval_create_from_date_string('5 minutes'));

$time['end'] = date_format($date, 'Y-m-d H:i:s');

//$flag = '/app/ib/frontend/scripts/scheduler/inprogressEmail';
$flag	= realpath(dirname(__FILE__)).'/inprogressEmail'; //path to flag file
$stringSuccess 	= 'Send Email Queue';
$stringFailed	= 'Send Email Queue Failed';

if (file_exists($flag)) {
	echo 'Queue inprogress at '.date('Y-m-d H:i:s');
	echo PHP_EOL;
}else{
	echo 'Estimation end queue at '.$time['end'];
	echo PHP_EOL;

	try{
		$fp = fopen($flag, 'w');
		fclose($fp);
		echo 'Create flag at: '.date('Y-m-d H:i:s');
		echo PHP_EOL;
	}catch(Exception $e){
		Application_Helper_General::cronLog(basename(__FILE__),$stringFailed,0);
		Application_Log_GeneralLog::backgroundLog(Zend_Debug::dump($e,$stringFailed,FALSE),array());

		echo 'Can\'t create flag at: '.date('Y-m-d H:i:s');
		echo PHP_EOL;
		Zend_Debug::dump($e->getMessage());
		echo PHP_EOL;
		die();
	}

	$db = Zend_Db_Table::getDefaultAdapter();

	$i = 0;
	echo 'Scan: ';
	while(strtotime(date('Y-m-d H:i:s')) <= strtotime($time['end'])){
		$i ++;
		echo $i.'.';

		$rawData = $db->select()
			->from('T_EMAIL_QUEUE')
			->where('EMAIL_SENT_FLAG = ?', '0')
			->order('EMAIL_DATE ASC')
			->limit(1)
			->query()
			->fetchAll();

		if (count($rawData)){
			foreach ($rawData as $key => $val){
				$log = NULL;
				$emailStatus = Application_Helper_Email::sendEmail($val['EMAIL_TO'],$val['EMAIL_SUBJECT'],$val['EMAIL_MESSAGE'],$log);

				if ($emailStatus == 1){ //SUCCESS
					$db->update('T_EMAIL_QUEUE',
						array(
							'EMAIL_SENT_FLAG' 	=> 1,
							'EMAIL_SENT_LOG' 	=> $log
						),
						array('EMAIL_ID = ?' => $val['EMAIL_ID'])
					);
					echo 'OK.';
				}else{ //FAILED
					$db->update('T_EMAIL_QUEUE',
						array(
							'EMAIL_SENT_FLAG' 	=> 2,
							'EMAIL_SENT_LOG' 	=> $log
						),
						array('EMAIL_ID = ?' => $val['EMAIL_ID'])
					);
					echo 'KO.';
				}
			}
		}

		sleep(1);
	}

	try{
		echo PHP_EOL;
		unlink($flag);
		echo 'Delete flag at: '.date('Y-m-d H:i:s');
		echo PHP_EOL;
	}catch(Exception $e){
		Application_Helper_General::cronLog(basename(__FILE__),$stringFailed,0);
		Application_Log_GeneralLog::backgroundLog(Zend_Debug::dump($e,$stringFailed,FALSE),array());

		echo PHP_EOL;
		echo 'Can\'t delete flag at: '.date('Y-m-d H:i:s');
		echo PHP_EOL;
		Zend_Debug::dump($e->getMessage());
		echo PHP_EOL;
		die();
	}
}

Application_Helper_General::cronLog(basename(__FILE__),$stringSuccess,1);

echo 'End queue at '.date('Y-m-d H:i:s');
echo PHP_EOL;
?>