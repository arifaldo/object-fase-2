<?php
set_time_limit(0);
$time_start = explode(" ",microtime());
$time['timestart'] = $time_start[0] + $time_start[1];

require_once('autoCleansingData.php');// file yg berisi data table2 yg perlu dicleansing

// Trx jalan terakhir, karena spy data master trx tidak hilang
housekeeping("LOG");		// menjalankan cleansing table2 LOG
housekeeping("TRX");		// menjalankan cleansing table2 TRANSACTION

// Optimize Table
// $optTables = array();
// $optTablesStr = "";

// LOG
// if (!empty($tables["LOG"]))
// {
	// foreach ($tables["LOG"] as $t => $tbl)
	// {
		// $optTablesStr .= table_list($tbl);
	// }
// }

// TRX
// if (!empty($tables["TRX"]))
// {
	// foreach ($tables["TRX"] as $t => $tbl)
	// {
		// $optTablesStr .= table_list($tbl);
	// }
// }

// $optTablesStr = substr($optTablesStr, 0, -1);

// $optTables = explode(",", $optTablesStr);

// $optTables = array_unique($optTables);
// echo "<pre>".count($optTables);
// print_r($optTables);
// echo "</pre>";

// if (!empty($optTables))
// {
	// foreach ($optTables as $t => $tbl)
	// {
		// $sql = "OPTIMIZE TABLE ".$tbl;
		// $db->query($sql);
		
		// $sqlLog = " INSERT INTO `sb_cronlog` ( `id` , `cron_datetime` , `cron_file` , `cron_result` , `cron_issuccess` )
					// VALUES (NULL, NOW(), 
							// '".basename(__FILE__)."', 
							// '[Optimize] Table: ".$tbl."', '1');";
		// $db->query($sqlLog);
	// }
// }

$time_end = explode(" ",microtime());
$time['timeend'] = $time_end[0] + $time_end[1];
 
$timestart = $time['timestart'];
$timeend   = $time['timeend'];

$interval 	 = round($timeend - $timestart, 0);
$intervalMsg = round($interval/60, 0) ." m ".round($interval % 60, 0). " s";
echo PHP_EOL;
echo "Interval: ".$intervalMsg;
echo PHP_EOL;

function housekeeping($hkType){
	global $tables;
	global $limit, $limitDel, $maxLoop, $maxLoopDel;
	global $db;
	global $wh;
	
	echo "@$hkType".PHP_EOL;
	
	if (!empty($tables[$hkType])){
		foreach ($tables[$hkType] as $t => $tbl){
			echo PHP_EOL;
			echo "@@".($t+1).". ".$tbl['table_name'];
			Zend_Debug::dump($tbl,'tbl:');
			
			/* BEGIN special table per bank */
// 			if ($tbl['table_name'] == 'SEND_EOD'){
// 				deleteEODBODFile();
// 			}
			
			// END special table per bank
							
			// Bila tidak ada table detail dan tbl_key nya kosong ( kondisi tanpa join ), maka langsung delete table tsb
			// Bila tidak ada table detail, tapi ada tbl_key, maka akan select dulu, baru delete sesuai key yg didapat (masuk ke else)
			if (empty($tbl['tbl_dtl']) && empty($tbl['tbl_key'])){
				// delete sebanyak $limitDel, hingga batas maksimum $maxLoopDel
//				$loop = 0;
//				$rowDeleted = 0;
//				do 
//				{
//					$loop += 1;
//					if ($loop > $maxLoopDel)
//					{
//						echo "<br>Loop ended here, reach maximum loop..............<br>";
//						break;
//					}
					try{
						$db->beginTransaction();
						// -- count Start Time
						$time1 = explode(" ",microtime());
						$time['t1'] = $time1[0] + $time1[1];
						// -- End count Start Time
						//$sql = createSQL($hkType, "delete", $tbl);
						//$result = $db->query($sql);
						$whereArr   = array();
						$whereArr[] = str_replace("[VARDATE]", $wh["[DATE_$hkType]"], $tbl["where_date"]);
						// apabila ada where tambahan
						if (!empty($tbl["where"]))
							$whereArr[] = $tbl["where"];
						
						
// 						Zend_Debug::dump($whereArr,$tbl["table_name"].__LINE__);	
						$rowDeleted = $db->delete($tbl["table_name"], $whereArr);
						//echo "Loop: ".$loop."<br>";
						echo "Num Rows Delete: ".$rowDeleted.PHP_EOL;
						// -- count End Time
						$t2 = explode(" ",microtime());
						$time['t2'] = $t2[0] + $t2[1];
						$ti1   = $time['t1'];
						$ti2   = $time['t2'];
						$loopTime 	 = round($ti2 - $ti1, 0);
						$loopTimeMsg = round($loopTime/60, 0) ." m ".round($loopTime % 60, 0). " s";
						echo "Time: ".$loopTimeMsg.PHP_EOL;
						// -- End count End Time
						$string  = "[HouseKeeping $hkType] Table: ".$tbl['table_name']." ($rowDeleted) $loopTimeMsg";
						Application_Helper_General::cronLog(basename(__FILE__),$string);
						$db->commit();
					}catch (Exception $e){
						$db->rollBack();
						$string  = "[HouseKeeping $hkType] Table: ".$tbl['table_name'].", Message: ".$e->getMessage();
						Application_Helper_General::cronLog(basename(__FILE__),$string,0);
					}
//				} 
//				while ($rowDeleted >= $limitDel);	
				// while $rowDeleted tidak lebih kecil dari $limitDel, karena kalo lebih kecil berarti udah data terakhir, exit loop...
			}else{
				// 1. Select table_key from table master
				$sql 		= createSQL($hkType, "count", $tbl); 										
				$numRows   	= $db->fetchOne($sql);
									
				echo "Num Rows Select: ".$numRows.PHP_EOL;
				
				// if empty, no rows deleted
				if ($numRows < 1){
					$string  = "[HouseKeeping $hkType] Table: ".$tbl['table_name']." ($numRows)";
					Application_Helper_General::cronLog(basename(__FILE__),$string);	
				}else{
					//percona archiever master table here 
					Zend_Debug::dump($sql->__toString(),'666');
					
					$loop = 0;

					// selama numrows > 0, akan recheck lg, apakah utk limit berikutnya masih ada data, 
					// kalo ada, jalankan script delete berikutnya...
					while ($numRows > 0){
						$loop += 1;
					
						// Preventing infinite loop, set $maxLoop di cron_housekeeping_data.php
						if ($loop > $maxLoop){
							echo "Loop ended here, reach maximum loop.".PHP_EOL;
							break;
						}
						
						// kalo $loop > 1, cek apakah previous numrows lebih kecil dari limit
						if ($loop > 1){
							// $numRows lebih kecil dari limit, artinya udah select terakhir, break utk keluar dari while loop
							if ($numRows < $limit){
								break;
							}else{
								// kalo == limit, buat query baru, utk select berikutnya
								echo "New Query looping:".PHP_EOL;
								$sql 		= createSQL($hkType, "count", $tbl); 										
								$numRows   	= $db->fetchOne($sql);
				
								echo "Num Rows Select: ".$numRows.PHP_EOL;
								if ($numRows == 0)
									break;	
								
							}
						}
						
						try{
							$db->beginTransaction();
							
							// -- count Start Time
							$time1 = explode(" ",microtime());
							$time['t1'] = $time1[0] + $time1[1];
							// -- End count Start Time
						
							echo "Loop: ".$loop.PHP_EOL;
							
							// retrieve data
							$sql = createSQL($hkType, "select", $tbl); 	
							
							// khusus FSCM loan, add column ps_number, doc_no_id
							if ($tbl['table_name'] == 'T_LOAN'){	
								$sql->columns(
									array(
										'PS_NUMBER'		=> 'PS_NUMBER',
							   			'DOC_NO_ID'		=> 'DOC_NO_ID',
							  		)
								);
							}
							
							$data = $db->fetchAll($sql);
							$tbl_keys = Application_Helper_Array::simpleArray($data,$tbl["tbl_key"]);		// table ID to be deleted
							
							// List of Table Turunannya.... 
							$tbl_details = explode(",", $tbl['tbl_dtl']);
							$key_details = (empty($tbl['tbl_key_dtl']))? "": explode(",", $tbl['tbl_key_dtl']);
						
							Zend_Debug::dump($tbl_keys,'tbl_keys:');
							Zend_Debug::dump($tbl_details,'tbl_details:');
							Zend_Debug::dump($key_details,'key_details:');
							
							$dtlMsg = "";
							
							// Delete table turunan
//							if ($tbl_details[0] != "")		// karena explode kosong, menghasilkan array[0] dgn string ""
							if (!empty($tbl['tbl_dtl'])){
								foreach ($tbl_details as $td => $tbl_detail){
									$keyTblDetail = (empty($key_details)) ? $tbl['tbl_key']: $key_details[$td];
									
									$whereArr   = array();
									$whereArr["$keyTblDetail in ( ? )"] = $tbl_keys;
									
// 									Zend_Debug::dump($whereArr,$tbl_detail.__LINE__);
									$rowDeleted = $db->delete($tbl_detail, $whereArr);
									
									echo "Num Rows Delete for ".$tbl_detail." : ".$rowDeleted.PHP_EOL;
									$dtlMsg .= ", $tbl_detail ($rowDeleted)";
								}
							}
							
							// Delete table master
							$whereArr   = array();
							$whereArr[$tbl['tbl_key']." in ( ? )"] = $tbl_keys;
							
// 							Zend_Debug::dump($whereArr,$tbl["table_name"].__LINE__);
							$rowDeleted = $db->delete($tbl['table_name'], $whereArr);
							
							echo "Num Rows Delete for ".$tbl['table_name']." : ".$rowDeleted.PHP_EOL;
							
							// Delete file request and response (only multi payment)
							if ($tbl['table_name'] == 'T_PSLIP'){
								foreach ($tbl_keys as $ps_number){
									$firstChar 		= substr($ps_number, 0, 1);
									$fileArchive  	= substr($ps_number, -17);
									
									if ($firstChar == 'M' || $firstChar == 'B'){
										$fileToBeDeleted = array();
										$fileToBeDeleted[] = LIBRARY_PATH."data/multipayment/input/F01".$fileArchive;	// multi credit
										$fileToBeDeleted[] = LIBRARY_PATH."data/multipayment/output/F02".$fileArchive;	// multi debet
										$fileToBeDeleted[] = LIBRARY_PATH."data/multipayment/input/F03".$fileArchive;	// multi credit
										$fileToBeDeleted[] = LIBRARY_PATH."data/multipayment/output/F04".$fileArchive;	// multi debet
										
										deleteFile($fileToBeDeleted);
									}
								}
							}
							
							if ($tbl['table_name'] == 'T_LOAN'){
								// delete T_TX dan turunannya
								$psnumberArr 	= Application_Helper_Array::simpleArray($data,'PS_NUMBER');
								$TX_tbl_dtl 	= explode(",", $tbl['TX_tbl_dtl']);
								$TX_key_dtl 	= explode(",", $tbl['TX_key_dtl']);
								deleteAdditionalTables($TX_tbl_dtl, $TX_key_dtl, $psnumberArr);
								
								// delete DOC_INI dan turunannya
								$docNoIDArr = array();
								foreach ($data as $value) {
									if (!empty($value['DOC_NO_ID']))
										$docNoIDArr[] = $value['DOC_NO_ID'];
									
								}
								
								if (!empty($docNoIDArr)){
									$DOC_tbl_dtl 	= explode(",", $tbl['DOC_tbl_dtl']);
									$DOC_key_dtl 	= explode(",", $tbl['DOC_key_dtl']);
									deleteAdditionalTables($DOC_tbl_dtl, $DOC_key_dtl, $docNoIDArr);
								}
							}
							
							// -- count End Time
							$t2 = explode(" ",microtime());
							$time['t2'] = $t2[0] + $t2[1];
							$ti1   = $time['t1'];
							$ti2   = $time['t2'];
							$loopTime 	 = round($ti2 - $ti1, 0);
							$loopTimeMsg = round($loopTime/60, 0) ." m ".round($loopTime % 60, 0). " s";
							echo "Time: ".$loopTimeMsg.PHP_EOL;
							// -- End count End Time
							
							$string  = "[HouseKeeping $hkType] Table: ".$tbl['table_name']." ($rowDeleted)$dtlMsg $loopTimeMsg";
							Application_Helper_General::cronLog(basename(__FILE__),$string);	
														
							$db->commit();
						}catch (Exception $e){
							$db->rollBack();
										
							$string  = "[HouseKeeping $hkType] Table: ".$tbl['table_name'].", Message: ".$e->getMessage();
							Application_Helper_General::cronLog(basename(__FILE__),$string,0);	
						}
					}	// End While
				}	// End if ($numRows >= 1)
			}
		}
	}
	else	// Empty array table yg mau di-clean
	{
		echo $string = "[HouseKeeping $hkType] No data to be deleted";
		echo PHP_EOL;
		Application_Helper_General::cronLog(basename(__FILE__),$string);	
	}
}

function createSQL($hkType, $sqlType = "select", $tbl = array(), $setLimit = true){
	global $wh;
	global $db;
	global $limit, $limitDel;
	
	if ($sqlType == "count"){
		$select = $db->select()
			->from(
				array('A' => $tbl['table_name']),
				array('NUMROWS' => 'count(*)')
			)
			->where(str_replace("[VARDATE]", $wh["[DATE_$hkType]"], $tbl["where_date"]));	

		if (!empty($tbl['where']))
			$select->where($tbl['where']);		

	}elseif ($sqlType == "select"){
//		$distinct = (empty($tbl["distinct"]))? "": "distinct";
//		$limitRows = ($setLimit == false) ? "": " TOP ($limit) ";		
//
//		$sql  = (empty($tbl["tbl_key"])) ? "SELECT $limitRows *": "SELECT $distinct $limitRows a.".$tbl["tbl_key"];
//		$sql .= " FROM ". $tbl["table_name"]. " a ";
//		$sql .= (empty($tbl["join"]))  ? "": " ". $tbl["join"]." ";
//		$sql .= " WHERE " . str_replace("[VARDATE]", "[DATE_$hkType]", $tbl["where_date"]) ;	// change [VARDATE] to [DATE_LOG]/[DATE_CHG]/[DATE_TRX]
//		$sql .= (empty($tbl["where"])) ? "": " AND ". $tbl["where"];
		
		$select = $db->select()
			->from(
				array('A' => $tbl['table_name']),
				array($tbl['tbl_key'] => $tbl['tbl_key'])			// primary key
			)
			->where(str_replace("[VARDATE]", $wh["[DATE_$hkType]"], $tbl["where_date"]));	

		if (!empty($tbl['where']))
			$select->where($tbl['where']);
		
		if ($setLimit === true)
			$select->limit($limit);
	}
	
// 	Zend_Debug::dump($select->__toString(),__FUNCTION__);
	return $select;
}

// Function to list table name, used for optimizing
function table_list($tbl = array())
{
	$str  = $tbl["table_name"].",";
	$str .= (empty($tbl["tbl_dtl"]))? "": $tbl["tbl_dtl"].",";

	return $str;
}

function deleteFile($fileToBeDeleted)
{
	echo "<br>File To Be Deleted: <br>";
	Zend_Debug::dump($fileToBeDeleted);
	
	foreach ($fileToBeDeleted as $fileDeleted)
	{
		if (file_exists($fileDeleted)) {
			unlink($fileDeleted);
		}
	}
}

function deleteEODBODFile()
{
	global $db;
	global $month_trx;
	
	$select				= "SELECT CONVERT(DATE, DATEADD(MONTH, -$month_trx, GETDATE()))";
	$dateToDeleteOri 	= $db->fetchOne($select);
	$dateToDelete 		= str_replace("-", "", $dateToDeleteOri);
	$dateToDelete 		= substr($dateToDelete, 2);
	
	echo "<br>dateToDeleteOri: ".$dateToDeleteOri;
	echo "<br>dateToDelete: ".$dateToDelete;
	
	$select	   = $db->select()
					->distinct()
					->from(array('A'	 			=> 'T_RECON'),
						   array('BRANCH_CODE' 		=> 'A.BRANCH_CODE')	
						   )
					->where("CONVERT(DATE,LOG_DATE) 	= ?", $dateToDeleteOri)
					->where("A.BRANCH_CODE <> ''");
	$branchArr = $db->fetchAll($select);
					
	
	$fileToBeDeleted = array();
	$fileToBeDeleted[] = LIBRARY_PATH."data/EOD/F07".$dateToDelete;
	$fileToBeDeleted[] = LIBRARY_PATH."data/BOD/F10".$dateToDelete;	
	$fileToBeDeleted[] = LIBRARY_PATH."data/BOD/F08".$dateToDelete;	
	
	foreach ($branchArr as $row)
	{
		$fileToBeDeleted[] = LIBRARY_PATH."data/BOD/F09".$dateToDelete.$row['BRANCH_CODE'];	
	}
	
	deleteFile($fileToBeDeleted);
										
}

function deleteAdditionalTables($tbl_details, $key_details, $tbl_keys)
{
	global $db;
	global $dtlMsg;
							
	foreach ($tbl_details as $td => $tbl_detail)
	{
		$keyTblDetail = $key_details[$td];
		
		$whereArr   = array();
		$whereArr["$keyTblDetail in ( ? )"] = $tbl_keys;
		
// 		Zend_Debug::dump($whereArr,$tbl_detail.__LINE__);
		$rowDeleted = $db->delete($tbl_detail, $whereArr);
		
		echo "Num Rows Delete for ".$tbl_detail." : ".$rowDeleted."<br>";
		$dtlMsg .= ", $tbl_detail ($rowDeleted)";
	}
}
?>