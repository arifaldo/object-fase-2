<?php

require_once(realpath(dirname(__FILE__) . '/zf-cli.php'));
require_once 'General/Settings.php';

function getAllPrepareBg()
{
    $db = Zend_Db_Table::getDefaultAdapter();

    $getBg = $db->select()
        ->from('TEMP_BANK_GUARANTEE')
        ->where('BG_STATUS = ?', '17')
        ->query()->fetchall();

    $tableData = '';

    foreach ($getBg as $bg) {
        try {
            //code...

            $clientUser  =  new SGO_Soap_ClientUser();

            $maxsend = 3;
            $count = 0;

            // for ($i = 0; $i < 3; $i++) {
            while (true) {
                // if ($count == $maxsend) break;

                if ($bg['METERAI_CHECK'] == '1') {
                    $clientUser->callapi('deleteDS', [
                        'document_id' => $bg['DOCUMENT_ID'],
                    ]);
                }
                // $response  = $clientUser->getResult();

                // if (strtolower($response->status) == 'ok') {
                if (true) {
                    $db->beginTransaction();

                    // hapus signer dari history signer
                    $db->delete("T_BANK_GUARANTEE_SIGNER", [
                        'DOCUMENT_ID = ? ' => $bg['DOCUMENT_ID']
                    ]);

                    $deleteData = [
                        'BG_REG_NUMBER' => $bg['BG_REG_NUMBER'],
                        'BG_NUMBER' => $bg['BG_NUMBER'],
                        // 'DELETED_TIME' => new Zend_Db_Expr('now()'),
                        'DELETED_TIME' => date('Y-m-d H:i:s'),
                        'DOC_ID' => $bg['DOCUMENT_ID'],
                        'DELETED_BY' => 'SYSTEM',
                        'DELETED_NOTES' => $bg['METERAI_CHECK'] ? 'TTD tidak lengkap' : 'gagal tempel eMeterai'
                    ];

                    $db->insert('T_DELETE_DOC', $deleteData);

                    // update status jadi 14
                    $db->update('TEMP_BANK_GUARANTEE', [
                        'BG_STATUS' => '14',
                        'DOCUMENT_ID' => '',
                        'SIGNING_STATUS' => '',
                        'METERAI_CHECK' => 0
                    ], [
                        'BG_REG_NUMBER = ?' => $bg['BG_REG_NUMBER'],
                    ]);

                    $db->commit();
                    $tableData .= '<tr><td style="padding: 5px 5px">' . $deleteData['DOC_ID'] . '</td> <td style="padding: 5px 5px">' . $deleteData['BG_REG_NUMBER'] . '</td> <td style="padding: 5px 5px">' . date('H:i:s', strtotime($deleteData['DELETED_TIME'])) . '</td> <td style="padding: 5px 5px">SISTEM</td> <td style="padding: 5px 5px">' . $deleteData['DELETED_NOTES'] . '</td></tr>';
                    break;
                }

                $count++;
            }
        } catch (\Throwable $th) {
            //throw $th;
            var_dump($th);
            die();
        }
    }

    $setting = new Settings();
    $getEmailTemplate = $setting->getSetting('bemailtemplate_delete_doc');
    $emailDeleteDoc = $setting->getSetting('email_delete_doc');

    $getEmailTemplate = strtr($getEmailTemplate, [
        '[[master_bank_name]]' => $setting->getSetting('master_bank_app_name'),
        '[[master_app_name]]' => $setting->getSetting('master_app_name'),
        '[[content]]' => $tableData,
        '[[deleted_date]]' => date('d M Y')
    ]);
    if ($emailDeleteDoc && $tableData != '') {
        $emails = explode(';', $emailDeleteDoc);
        $emails = array_map('trim', $emails);
        foreach ($emails as $emailDD) {
            Application_Helper_Email::sendEmail($emailDD, 'Notifikasi Hapus Dokumen', $getEmailTemplate);
        }
    }
}

getAllPrepareBg();
