<?php
//==========================================
// List of tables need to be cleansed
// used in autoCleansing.php
//==========================================
 
require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
$db = Zend_Db_Table::getDefaultAdapter();
$setting 		= new Settings();
$month_trx 		= $setting->getSetting('archiveafter', 3, false);	// interval months for transaction tables
$month_log = 1;	// interval months for log tables

echo "month_trx: ".$month_trx;
echo PHP_EOL;
echo "month_log: ".$month_log;
echo PHP_EOL;

$limit      = 10000;		// 100000
$maxLoop	= 20;		// set timeout, preventing infinite loop 		10
//$limit      = 2;		// 100000
//$maxLoop	= 1;

$limitDel   = 100000;	// limit saat delete langsung, (FIXED)  100000
$maxLoopDel	= 20;		// set timeout, preventing infinite loop 		(FIXED)

// Begin Variables
// (DATE(admfeelog_datetime) < DATE( DATE_SUB( CURDATE( ) , INTERVAL $month_interval MONTH ) ) )
// (DATE(admfeelog_datetime) [VARDATE] ) 
// (DATE(admfeelog_datetime) [VARDATE] OR  DATE(admfeelog_datetime) [VARDATE] ) 
// Note: Utk trx, liat dari ps_updated, karena ps_updated should be the last date

// condition where deleted
$wh = array();
$wh['[DATE_LOG]'] 		= " < CONVERT(DATE_SUB(GETDATE(),INTERVAL $month_log MONTH),DATE)";
$wh['[DATE_TRX]'] 		= " < CONVERT(DATE_SUB(GETDATE(),INTERVAL $month_trx MONTH),DATE)";
$wh['[PS_STATUS]'] 		= " IN (4,5,6,14) ";		// 4: Rejected, 5: Completed, 6: Failed, 14: Cancel Future Date
$wh['[SUGGEST_STATUS]'] = " IN (2,3,6) ";			// 2: Granted, 3: Rejected, 6: Deleted Suggestion
// End Variables

$tables = array();				// table2 yg perlu di-housekeeping....
$tables['LOG'] = array();		// list of table Log
$tables['TRX'] = array();		// list of table Transaction

// ========== Contoh isi $tables[] array
//$tables[$hkType][] = array("table_name" 	=> "sb_ticket_pslip",											// *nama table utama, otomatis dialiaskan sbg "a"
	//						 "join" 		=> "LEFT JOIN sb_pslip x ON a.ps_number = x.ps_number",			// ** NOTE: tdk dipakai di sini. kondisi join utk ambil data yg perlu didelete
	//					   	 "where_date" 	=> " (DATE(x.ps_updated) [VARDATE] ) ",							// *kondisi where utk date_column, bisa pake AND/OR, [VARDATE] diganti dgn [DATE_hktype]
	//						 "where" 		=> "x.ps_status [PS_STATUS] ",									// kondisi where tambahan yg diperlukan
	//						 "tbl_dtl"		=> "sb_pslip,sb_transaction,sb_pslip_history,sb_releaselog",	// tabel turunannya yg jg perlu dihapus sehubungan dgn tabel utama, separated by comma
	//						 "tbl_key"		=> "ps_number",													// primary key used in table utama
	//						 "tbl_key_dtl"	=> "",															// key di tabel turunan, empty bila key tabel turunan sama dengan primary key di table utama, 
																											// bila diisi, harus diisi sesuai jumlah tabel turunan dan separated by comma
    //						 "distinct"		=> "distinct",													// bila diisi, berarti hasil select tbl_key akan di-distinct (utk penyamaan, isi dgn 'distinct')
	//					  	);	
// ========== End Contoh isi $tables[] array


// Table Log
		  	
$tables['LOG'][] = array("table_name" 	=> "T_BACTIVITY",
						 "join" 		=> "",
					   	 "where_date" 	=> " (CONVERT(LOG_DATE,DATE) [VARDATE] ) ",
						 "where" 		=> "",		
						 "tbl_dtl"		=> "",
						 "tbl_key"		=> "",	
						 "tbl_key_dtl"	=> "",		
					  	);	
					  	
$tables['LOG'][] = array("table_name" 	=> "T_FACTIVITY",
						 "join" 		=> "",
					   	 "where_date" 	=> " (CONVERT(LOG_DATE,DATE) [VARDATE] ) ",
						 "where" 		=> "",
						 "tbl_dtl"		=> "",
						 "tbl_key"		=> "",	
						 "tbl_key_dtl"	=> "",	
					  	);	
					  	
$tables['LOG'][] = array("table_name" 	=> "T_CRON_LOG",
						 "join" 		=> "",
					   	 "where_date" 	=> " (CONVERT(CRON_DATETIME,DATE) [VARDATE] ) ",
						 "where" 		=> "",
						 "tbl_dtl"		=> "",
						 "tbl_key"		=> "",	
						 "tbl_key_dtl"	=> "",	
					  	);	

$tables['LOG'][] = array("table_name" 	=> "T_EMAIL_LOG",
						 "join" 		=> "",
					   	 "where_date" 	=> " (CONVERT(EMAIL_DATE,DATE) [VARDATE] ) ",
						 "where" 		=> "",
						 "tbl_dtl"		=> "",
						 "tbl_key"		=> "",	
						 "tbl_key_dtl"	=> "",	
					  	);

$tables['LOG'][] = array("table_name" 	=> "T_GLOBAL_CHANGES",
						 "join" 		=> "",
					   	 "where_date" 	=> " (CONVERT(LASTUPDATED,DATE) [VARDATE] ) ",
						 "where" 		=> "CHANGES_STATUS IN ('RJ', 'AP')",
						 "tbl_dtl"		=> "",
						 "tbl_key"		=> "",	
						 "tbl_key_dtl"	=> "",	
					  	);		

//$tables['LOG'][] = array("table_name" 	=> "T_INTERFACE_LOG",
//						 "join" 		=> "",
//					   	 "where_date" 	=> " (CONVERT(LOG_DATE,DATE) [VARDATE] ) ",
//						 "where" 		=> "FUNCTION_NAME in ('AccountInquiry','AccountStatement')",
//						 "tbl_dtl"		=> "",
//						 "tbl_key"		=> "",	
//						 "tbl_key_dtl"	=> "",	
//					  	);

$tables['LOG'][] = array("table_name" 	=> "T_INTERFACE_LOG",
						 "join" 		=> "",
					   	 "where_date" 	=> " (CONVERT(LOG_DATE,DATE) [VARDATE] ) ",
						 "where" 		=> "",
						 "tbl_dtl"		=> "",
						 "tbl_key"		=> "",	
						 "tbl_key_dtl"	=> "",	
					  	);	 
					  	
$tables['LOG'][] = array("table_name" 	=> "T_EMAIL_QUEUE",
						"join" 		=> "",
						"where_date" 	=> " (CONVERT(EMAIL_DATE,DATE) [VARDATE] ) ",
						"where" 		=> "",
						"tbl_dtl"		=> "",
						"tbl_key"		=> "",
						"tbl_key_dtl"	=> "",
);

$tables['LOG'][] = array("table_name" 	=> "T_SMS_QUEUE",
						"join" 		=> "",
						"where_date" 	=> " (CONVERT(SMS_DATE,DATE) [VARDATE] ) ",
						"where" 		=> "",
						"tbl_dtl"		=> "",
						"tbl_key"		=> "",
						"tbl_key_dtl"	=> "",
);
// Table Transaction 
$tables['TRX'][] = array("table_name" 	=> "T_CHARGES_LOG",
						 "join" 		=> "",
					   	 "where_date" 	=> " (CONVERT(LOG_DATETIME,DATE) [VARDATE] ) ",
						 "where" 		=> "STATUS IN (0,1)",	// 0: failed, 1: success, 13: pending process
						 "tbl_dtl"		=> "",
						 "tbl_key"		=> "",	
						 "tbl_key_dtl"	=> "",	
					  	);	
					  	
// $tables['TRX'][] = array("table_name" 	=> "T_INTERFACE_LOG",
// 						 "join" 		=> "",
// 					   	 "where_date" 	=> " (CONVERT(DATE,LOG_REQ) [VARDATE] ) ",
// 						 "where" 		=> "SERVICE_NAME in ('MonthlyCharge')",
// 						 "tbl_dtl"		=> "",
// 						 "tbl_key"		=> "",	
// 						 "tbl_key_dtl"	=> "",	
// 					  	);					  	
					  	
$tables['TRX'][] = array("table_name" 	=> "T_PSLIP_EXCEPTION_REPAIR",
						 "join" 		=> "",
					   	 "where_date" 	=> " CONVERT(SUGGEST_GRANTDATE,DATE) [VARDATE] OR CONVERT(SUGGEST_REJECTDATE,DATE) [VARDATE] ",
						 "where" 		=> " SUGGEST_STATUS IN ('RJ', 'AP') ",
						 "tbl_dtl"		=> "T_PSLIP_EXCEPTION_REPAIR_DETAIL",
						 "tbl_key"		=> "SUGGEST_ID",
						 "tbl_key_dtl"	=> "",
					  	);	
					  	
$tables['TRX'][] = array("table_name" 	=> "T_PSLIP",
						 "join" 		=> "",
					   	 "where_date" 	=> " (CONVERT(PS_UPDATED,DATE) [VARDATE] ) ",
						 "where" 		=> "PS_STATUS  IN (4,5,6,14)",
						 "tbl_dtl"		=> "T_TRANSACTION,T_PSLIP_DETAIL,T_PSLIP_HISTORY,T_EMAIL_NOTIFICATION,T_EMAIL_NOTIFICATION_TRX",
						 "tbl_key"		=> "PS_NUMBER",	
						 "tbl_key_dtl"	=> "PS_NUMBER,PS_NUMBER,PS_NUMBER,PS_NUMBER,PS_NUMBER",	
					  	);

// $tables['TRX'][] = array("table_name" 	=> "SEND_EOD",
// 						 "join" 		=> "",
// 					   	 "where_date" 	=> " (CONVERT(DATE,LASTUPDATED) [VARDATE] ) ",
// 						 "where" 		=> "",
// 						 "tbl_dtl"		=> "",
// 						 "tbl_key"		=> "",	
// 						 "tbl_key_dtl"	=> "",	
// 					  	);		

// $tables['TRX'][] = array("table_name" 	=> "T_RECON_DELETED",
// 						 "join" 		=> "",
// 					   	 "where_date" 	=> " (CONVERT(DATE,UPDATED_DATE) [VARDATE] ) ",
// 						 "where" 		=> "",
// 						 "tbl_dtl"		=> "",
// 						 "tbl_key"		=> "",	
// 						 "tbl_key_dtl"	=> "",	
// 					  	);
				  	
// $tables['TRX'][] = array("table_name" 	=> "T_RECON",
// 						 "join" 		=> "",
// 					   	 "where_date" 	=> " (CONVERT(DATE,UPDATED_DATE) [VARDATE] ) ",
// 						 "where" 		=> "(RECON_TYPE = 'S' AND REMAIN_BAL <= 0) OR (RECON_TYPE = 'L' AND RECON_STATUS = 1)",
// 						 "tbl_dtl"		=> "",	
// 						 "tbl_key"		=> "",	
// 						 "tbl_key_dtl"	=> "",	
// 					  	);	

// $tables['TRX'][] = array("table_name" 	=> "T_RECON",
// 						 "join" 		=> "",
// 					   	 "where_date" 	=> " (CONVERT(DATE,UPDATED_DATE) [VARDATE] ) ",
// 						 "where" 		=> "(RECON_TYPE = 'F' AND RECON_STATUS = 1) OR (TC_CODE = 'BL' AND RECON_STATUS = 1)",
// 						 "tbl_dtl"		=> "T_SETTLEMENT_DETAIL",	
// 						 "tbl_key"		=> "RECON_CODE",	
// 						 "tbl_key_dtl"	=> "LOAN_ID",	
// 					  	);						  	
					  	
// not installment dan loan status settled/closed
// $tables['TRX'][] = array("table_name" 	=> "T_LOAN",
// 						 "join" 		=> "",
// 					   	 "where_date" 	=> " (CONVERT(DATE,SETTLED_DATE) [VARDATE] ) ",
// 						 "where" 		=> " LOAN_STATUS in (1,5) AND INSTALLMENT_OF = 0 ",
// 						 "tbl_dtl"		=> "T_LOAN_DETAIL,T_ACCRUAL,T_SETTLEMENT,T_SETTLEMENT_DETAIL",
// 						 "tbl_key"		=> "LOAN_ID",	
// 						 "tbl_key_dtl"	=> "",	

// 					  	 "TX_tbl_dtl"	=> "T_TX,T_TX_DETAIL,T_TX_FEE,T_HISTORY,T_EMAIL_NOTIFICATION_TRX,T_EMAIL_NOTIFICATION_TX,T_ACCOUNTS,T_DISBURSEMENT_WORKFLOW,T_INSTALLMENT,T_PARAMETERS,T_INTERFACE_LOG",
// 					  	 "TX_key_dtl"	=> "PS_NUMBER,PS_NUMBER,PS_NUMBER,PS_NUMBER,PS_NUMBER,PS_NUMBER,PS_NUMBER,PS_NUMBER,PS_NUMBER,PS_NUMBER,REF_ID",	

// 					  	 "DOC_tbl_dtl"	=> "M_DOC_INI,M_DOC_HISTORY,M_DOC_INI_DETAILS,M_DOC_INI_DOC_CHECKLIST,M_DOC_ATTACHMENT,T_DOCUMENT_SETTING,T_EMAIL_NOTIFICATION_DOC", // ,DOC_CHECKLIST_DUPLICATE_DETAIL
// 						 "DOC_key_dtl"	=> "DOC_NO_ID,DOC_NO_ID,DOC_NO_ID,DOC_NO_ID,DOC_NO_ID,DOC_NO_ID,DOC_NO_ID",						  	
// 					  	);	

// Utk tx yg tidak terbentuk loan
// $tables['TRX'][] = array("table_name" 	=> "T_TX",
// 						 "join" 		=> "",
// 					   	 "where_date" 	=> " (CONVERT(DATE,UPDATED) [VARDATE] ) ",
// 						 "where" 		=> "TX_STATUS IN (4,6,14,15) OR (TX_STATUS = 5 AND PRK_AMOUNT <= 0)",
// 						 "tbl_dtl"		=> "T_TX_DETAIL,T_TX_FEE,T_HISTORY,T_EMAIL_NOTIFICATION_TRX,T_EMAIL_NOTIFICATION_TX,T_ACCOUNTS,T_DISBURSEMENT_WORKFLOW,T_INSTALLMENT,T_PARAMETERS,T_SETTLEMENT,T_INTERFACE_LOG",
// 						 "tbl_key"		=> "PS_NUMBER",	
// 						 "tbl_key_dtl"	=> "PS_NUMBER,PS_NUMBER,PS_NUMBER,PS_NUMBER,PS_NUMBER,PS_NUMBER,PS_NUMBER,PS_NUMBER,PS_NUMBER,PS_NUMBER,REF_ID",	
// 					  	);	

// Utk doc yg rejected dan deleted
// $tables['TRX'][] = array("table_name" 	=> "M_DOC_INI",
// 						 "join" 		=> "",
// 					   	 "where_date" 	=> " (CONVERT(DATE,UPDATED) [VARDATE] ) ",
// 						 "where" 		=> "DOC_STATUS IN (3, 15) ",
// 						 "tbl_dtl"		=> "M_DOC_HISTORY,M_DOC_INI_DETAILS,M_DOC_INI_DOC_CHECKLIST,M_DOC_ATTACHMENT,T_DOCUMENT_SETTING,T_EMAIL_NOTIFICATION_DOC", // ,DOC_CHECKLIST_DUPLICATE_DETAIL
// 						 "tbl_key"		=> "DOC_NO_ID",	
// 						 "tbl_key_dtl"	=> "DOC_NO_ID,DOC_NO_ID,DOC_NO_ID,DOC_NO_ID,DOC_NO_ID,DOC_NO_ID",	// ,DOC_CHECKLIST_ID, DOC_DUPLICATE_ID
// 					  	);			  
?>