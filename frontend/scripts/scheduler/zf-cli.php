<?php
$key 	= '6a1f325be4c0492063e83a8cb2cb9ae7';
$iv 	= '3r0vawv0dtsaofxo';

date_default_timezone_set('Asia/Jakarta');

$beforeexecute = date("Y-m-d H:i:s");
$start_time = microtime(true);
$GLOBALS['start_time'] = $start_time;

// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../../application'));
//    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../../../cm/frontend/application'));

// Define application environment
   defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

//define('SHARE_UPLOAD_PATH',APPLICATION_PATH . '/../../data/uploads');
define('LIBRARY_PATH',APPLICATION_PATH.'/../../library/');
define('UPLOAD_PATH',LIBRARY_PATH . '/data/uploads');
define('SHARE_UPLOAD_PATH',LIBRARY_PATH . '/data/uploads/submit');
define('HELPER_PATH',LIBRARY_PATH . '/data/uploads');

define('DEBUG_MODE',1);

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(LIBRARY_PATH)
)));

//get_include_path()

/** Zend_Application */
require_once 'Zend/Application.php';
require_once 'Zend/Config/Ini.php';

//Create a Zend_Config_Ini object as the main config
$config = new Zend_Config_Ini(LIBRARY_PATH.'/configs/application.ini',
    APPLICATION_ENV,
    array('allowModifications'=>true)
);

/** LOAD CONFIG */
require_once LIBRARY_PATH . 'configs/Loader.php';
$config = Configs_Loader::loadConfig();

// Create application, bootstrap, and run
$application = new Zend_Application( APPLICATION_ENV, $config);
$application->bootstrap()->run();
//$application->gbootstrap();
$bootstrap = $application->getBootstrap();
Zend_Controller_Front::getInstance()->setParam('bootstrap', $bootstrap);
//define error remark
//$data->preDispatch(); 

require_once 'General/Settings.php';
$configAPP 		= Zend_Registry::get('config');
$settingAPP		= new Settings();
$cron_app 		= $settingAPP->getSetting('cron_app', null, false);

echo "Config APP		: ".$configAPP['appid'];
echo " | Setting APP	: ".$cron_app."\n";
$filenameFullPath = $argv[0];
$fileTemp = explode("/", $filenameFullPath);
$fileName = $fileTemp[count($fileTemp)-1];

if ($configAPP['appid'] != $cron_app && $fileName != 'autoChangeCronAppSetting.php')
{	echo " --exit-- "; die();	}

Zend_Registry::set('IS_CRON',true);