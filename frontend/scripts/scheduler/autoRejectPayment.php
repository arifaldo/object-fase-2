<?php
	require_once (realpath(dirname(__FILE__) . '/zf-cli.php'));
	require_once 'CMD/Payment.php';

	$db = Zend_Db_Table::getDefaultAdapter();
	$actionid = 'scheduler';
	$moduleid = 'reject payment';
	$interval = 2;
	$truecounter = 0;
	$falsecounter = 0;
	
	$dataCM = $db->select()
						->FROM('T_PSLIP',array('PS_NUMBER','CUST_ID','PS_CREATEDBY','PS_TYPE'))
						->WHERE('PS_STATUS IN (17,1,2,3)')
						->WHERE('DATE(T_PSLIP.PS_UPDATED) BETWEEN DATE(DATE_ADD(NOW(), INTERVAL - 2 MONTH)) AND DATE(NOW())')
						->WHERE('DATE(T_PSLIP.PS_EFDATE) <  DATE(now())')
						//->where('PS_CREATEDBY = ?','PTFE_BENY014')
						->WHERE('PS_TYPE NOT IN (27,28)')
						->group('CUST_ID')
						->group('PS_CREATEDBY')
						//->where('PS_NUMBER = ?','20150624TE7PENA4E')
						->QUERY()->FETCHALL() 
	;  

	$dataCMDebit = $db->select()
						->FROM('T_PSLIP',array('PS_NUMBER','CUST_ID','PS_CREATEDBY','PS_TYPE'))
						->WHERE('PS_STATUS IN (17,1,2,3)')
						->WHERE('DATE(T_PSLIP.PS_UPDATED) BETWEEN DATE(DATE_ADD(NOW(), INTERVAL - 2 MONTH)) AND DATE(NOW())')
						->WHERE('DATE(T_PSLIP.PS_EFDATE) <  DATE(now())')
						//->where('PS_CREATEDBY = ?','PTFE_BENY014')
						->WHERE('PS_TYPE IN (27,28)')
						->group('CUST_ID')
						->group('PS_CREATEDBY')
						//->where('PS_NUMBER = ?','20150624TE7PENA4E')
						->QUERY()->FETCHALL() 
	;      
	//var_dump($dataCM);die;
	//echo $dataCM;die;
	if($dataCM || $dataCMDebit)
	{ 

		if(!empty($dataCM)){
		foreach($dataCM as $row)
		{
			
			$dataReject = $db->select()
						->FROM('T_PSLIP',array('PS_NUMBER','CUST_ID','PS_TYPE'))
						->WHERE('PS_STATUS IN (17,1,2,3)')
						->WHERE('DATE(T_PSLIP.PS_UPDATED) BETWEEN DATE(DATE_ADD(NOW(), INTERVAL - 2 MONTH)) AND DATE(NOW())')
						->WHERE('DATE(T_PSLIP.PS_EFDATE) <  DATE(now())')
						->WHERE('CUST_ID = ?',$row['CUST_ID'])
						->WHERE('PS_TYPE NOT IN (27,28)')
						->where('PS_CREATEDBY = ?',$row['PS_CREATEDBY'])
						->QUERY()->FETCHALL() ;
			 
			if(count($dataReject)>1){ 
			//die('here');
				$Payment = new Payment($row['PS_NUMBER'], $row['CUST_ID'], 'System');
					$PS_REASON = "Authorization Overdue";	
//var_dump($dataReject);die;					
					if($Payment->rejectPaymentMulti($PS_REASON,$dataReject))
						{
							$truecounter++;
						}
						else
						{
							$falsecounter++;
						}
			}else{
				//die('here1');
					$Payment = new Payment($row['PS_NUMBER'], $row['CUST_ID'], 'System');
					$PS_REASON = "Authorization Overdue";			
					if($Payment->rejectPayment($PS_REASON))
						{
							$truecounter++;
						}
						else
						{
							$falsecounter++;
						}
			}
			
			
		}
		
		}
		
		if(!empty($dataCMDebit)){ 
		foreach($dataCMDebit as $row)
		{
			
			$dataReject = $db->select()
						->FROM('T_PSLIP',array('PS_NUMBER','CUST_ID','PS_TYPE'))
						->WHERE('PS_STATUS IN (17,1,2,3)')
						->WHERE('DATE(T_PSLIP.PS_UPDATED) BETWEEN DATE(DATE_ADD(NOW(), INTERVAL - 2 MONTH)) AND DATE(NOW())')
						->WHERE('DATE(T_PSLIP.PS_EFDATE) <  DATE(now())')
						->WHERE('CUST_ID = ?',$row['CUST_ID'])
						->WHERE('PS_TYPE IN (27,28)')
						->where('PS_CREATEDBY = ?',$row['PS_CREATEDBY'])
						->QUERY()->FETCHALL() ;
			 
			
			//die('here');
				$Payment = new Payment($row['PS_NUMBER'], $row['CUST_ID'], 'System');
					$PS_REASON = "Authorization Overdue";	
//var_dump($dataReject);die;					
					if($Payment->rejectPaymentMultiDebit($PS_REASON,$dataReject))
						{
							$truecounter++;
						}
						else
						{
							$falsecounter++;
						}
			
			
			
		}
		
		}
		
		
		
		if($truecounter>0)
		{
			Application_Helper_General::cronLog(basename(__FILE__), 'Cron Reject Succes', '1');
		}
		if($falsecounter>0)
		{
			Application_Helper_General::cronLog(basename(__FILE__), 'Cron Reject Failed', '0');
		}		
	}

	if($truecounter == 0 && $falsecounter == 0)
	{
		Application_Helper_General::cronLog('autoRejectPayment', 'No Data Payment for Auto Rejected', '1');
	}
?>