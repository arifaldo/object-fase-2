<?php
require_once(realpath(dirname(__FILE__) . '/zf-cli.php'));
require_once 'Service/Account.php';
require_once 'General/Settings.php';
require_once LIBRARY_PATH . 'configs/Loader.php';

function generateTransactionID($paymentRef, $counter = 0)
{
	//		$sql_countTransaction = dbQuery("select count(transaction_id) as countTransaction from sb_transaction where ps_number = '{$paymentID}'");
	//		$rs_countTransaction = tep_db_fetch_array($sql_countTransaction);

	$countTransaction = ($counter < 1) ? 1 : $counter + 1;
	$seqNumberTransaction = str_pad($countTransaction, ((strlen($countTransaction) == 1) ? 2 : ((0 + strlen($countTransaction)) - strlen($countTransaction))), "0", STR_PAD_LEFT);
	$transactionID = $paymentRef . $seqNumberTransaction;

	return $transactionID;
}

function generateCloseNumber($kode)
{

	$db = Zend_Db_Table::getDefaultAdapter();

	while (true) {
		$currentDate = date("ymd");
		$length = 4;
		$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[random_int(0, $charactersLength - 1)];
		}

		$trxId = $kode . $currentDate . $randomString;

		$cekOnTBGClose = $db->select()
			->from('TEMP_BANK_GUARANTEE_CLOSE')
			->where('CLOSE_REF_NUMBER = ?', $trxId)
			->query()->fetch();

		$cekOnTBG = $db->select()
			->from('T_BANK_GUARANTEE')
			->where('CLOSE_REF_NUMBER = ?', $trxId)
			->query()->fetch();

		$cekOnSlip = $db->select()
			->from('T_BG_PSLIP')
			->where('CLOSE_REF_NUMBER = ?', $trxId)
			->query()->fetch();

		$cekOnTempExecute = $db->select()
			->from('TEMP_TRANSACTION_EXECUTE')
			->where('CLOSE_REF_NUMBER = ?', $trxId)
			->query()->fetch();

		if (!$cekOnTBG && !$cekOnSlip && !$cekOnTBGClose && !$cekOnTempExecute) break;
	}

	return $trxId;
}

function index()
{
	$db = Zend_Db_Table::getDefaultAdapter();

	$db->beginTransaction();
	$insertBGTransaction = array(
		'DUMP_TEXT' => uniqid('index'),
	);

	$db->insert('DUMP_TEST', $insertBGTransaction);
	$db->commit();
}

function runProg()
{
	$db = Zend_Db_Table::getDefaultAdapter();

	$x = 0;
	$data = $db->select()
		->from(array('A' => 'T_BANK_GUARANTEE'), array('*'))
		->where("A.BG_STATUS = 15")
		->where("DATE(A.BG_CLAIM_DATE) <= DATE(NOW())")
		->where("A.BG_NUMBER NOT IN (SELECT B.BG_NUMBER FROM TEMP_BANK_GUARANTEE_CLOSE B WHERE B.SUGGESTION_STATUS NOT IN (7,11) AND B.CHANGE_TYPE != 3)")
		->query()->fetchAll();

	$saveTransaction = [];

	if ($data) {

		foreach ($data as $row) {

			$checkIfClaim = $db->select()
				->from('TEMP_BANK_GUARANTEE_CLOSE')
				->where('BG_NUMBER = ?', $row['BG_NUMBER'])
				->where('CHANGE_TYPE = ?', 3)
				->where('SUGGESTION_STATUS NOT IN (7,11)')
				->query()->fetch();

			if ($checkIfClaim) continue;

			try {
				$checkBgAmen = $db->select()
					->from('TEMP_BANK_GUARANTEE')
					->where('BG_OLD = ?', $row['BG_NUMBER'])
					->where('BG_STATUS NOT IN (?)', [25, 13, 12, 9])
					->query()->fetchAll();

				if ($checkBgAmen) {
					foreach ($checkBgAmen as $dataBgOngoing) {
						updateTable('TEMP_BANK_GUARANTEE', [
							'BG_STATUS' => 26
						], [
							'BG_REG_NUMBER = ?' => $dataBgOngoing['BG_REG_NUMBER']
						]);

						$historyInsert = array(
							'DATE_TIME'         => new Zend_Db_Expr("now()"),
							'BG_REG_NUMBER'			=> $row['BG_NUMBER'],
							'CUST_ID'			=> '-',
							'USER_LOGIN'		=> 'SYSTEM',
							'BG_REASON'			=> 'BG Induk telah berakhir',
							'HISTORY_STATUS'	=> 35, //pengajuan dibatalkan
						);

						$db->insert('T_BANK_GUARANTEE_HISTORY', $historyInsert);
					}
				}

				$generateCloseRefNumber = generateCloseNumber("TK");

				insertTable('TEMP_TRANSACTION_EXECUTE', [
					'BG_NUMBER' => $row['BG_NUMBER'],
					'CLOSE_REF_NUMBER' => $generateCloseRefNumber
				]);
			} catch (Exception $e) {
				$message = $e->getMessage();

				$today = date("Ymd");
				$nametext = $today . '.txt';
				$path_file = LIBRARY_PATH . '/data/logs/scheduler/';

				$dirname = dirname($path_file . $nametext);

				if (!is_dir($dirname)) {
					mkdir($dirname, 0755, true);
				}

				$fp = fopen($path_file . $nametext, 'a'); //opens file in append mode
				fwrite($fp, '1 : ' . $message);
				fwrite($fp, PHP_EOL);
				fclose($fp);

				shell_exec('chown nginx ' . $path_file);
				shell_exec('chown nginx ' . $path_file . $nametext);
				// $db->rollBack();
			}

			try {
				//$db->beginTransaction();

				$dataUpdate  = ['BG_STATUS' => 16, 'CLOSING_TYPE' => 2, 'CLOSE_REF_NUMBER' => $generateCloseRefNumber, 'BG_OFFRISK_DATE' => $row['BG_CLAIM_DATE'], 'BG_UPDATED' => new Zend_Db_Expr('now()'), 'BG_UPDATEDBY' => 'SYSTEM'];
				$whereUpdate = ['BG_NUMBER = ?' => $row['BG_NUMBER']];
				updateTable('T_BANK_GUARANTEE', $dataUpdate, $whereUpdate);
				// $db->update('T_BANK_GUARANTEE', $dataUpdate, $whereUpdate);

				$dataUpdateTemp  = ['SUGGESTION_STATUS' => 11, 'LASTUPDATEDFROM' => '-', 'LASTUPDATEDBY' => 'SYSTEM'];
				$whereUpdateTemp = ['BG_NUMBER = ?' => $row['BG_NUMBER']];
				updateTable('TEMP_BANK_GUARANTEE_CLOSE', $dataUpdateTemp, $whereUpdateTemp);
				// $db->update('TEMP_BANK_GUARANTEE_CLOSE', $dataUpdateTemp, $whereUpdateTemp);

				$historyInsert = array(
					'DATE_TIME'         => new Zend_Db_Expr("now()"),
					'CLOSE_REF_NUMBER'        => $generateCloseRefNumber,
					'BG_NUMBER'        => $row['BG_NUMBER'],
					'USER_FROM'        => 'SYSTEM',
					'USER_LOGIN'        => 'SYSTEM',
					'BG_REASON'        => 'Penutupan Tanpa Klaim',
					'HISTORY_STATUS'        => 25, //pengajuan dibatalkan
				);

				$db->insert('T_BANK_GUARANTEE_HISTORY_CLOSE', $historyInsert);

				// continue;

				// $dataAcct = $db->select()
				// 	->from(array('A' => 'T_BANK_GUARANTEE_SPLIT'), array('*'))
				// 	->where("A.BG_NUMBER = ?", $row['BG_NUMBER'])
				// 	->query()->fetchAll();

				// $haveEscrow = false;

				// if ($dataAcct) {
				// 	$no = 0;
				// 	$statusService = true;
				// 	foreach ($dataAcct as $rowAcct) {
				// 		if ($rowAcct['ACCT_DESC'] != 'Escrow') {
				// 			$svcAccount = new Service_Account($rowAcct['ACCT'], null);
				// 			$result = $svcAccount->inquiryAccountBalance();

				// 			if ($result['response_code'] == '0000') {
				// 				if (strtolower($rowAcct['ACCT_DESC']) == 'giro' || strtolower($rowAcct['ACCT_TYPE']) == 'giro') continue;

				// 				$unlockSaving = $svcAccount->unlockSaving([
				// 					'branch_code' => $rowAcct['HOLD_BY_BRANCH'],
				// 					'sequence_number' => $rowAcct['HOLD_SEQUENCE']
				// 				]);

				// 				//var_dump($unlockSaving);

				// 				$parseDateTime = DateTime::createFromFormat('Y-m-d\TH:i:s+', $unlockSaving['datetime']);
				// 				$traTime = $parseDateTime->format('Y-m-d H:i:s');

				// 				if ($unlockSaving['response_code'] == "0000") {

				// 					$transactionID = generateTransactionID($generateCloseRefNumber, $no++);
				// 					$insertBGTransaction = array(
				// 						'CLOSE_REF_NUMBER' => $generateCloseRefNumber,
				// 						'TRANSACTION_ID' => $transactionID,
				// 						'BG_NUMBER' => $rowAcct['BG_NUMBER'],
				// 						'SERVICE' => 3,
				// 						'HOLD_SEQUENCE'        => $rowAcct['HOLD_SEQUENCE'],
				// 						'HOLD_BY_BRANCH'        => $rowAcct['HOLD_BY_BRANCH'],
				// 						'SOURCE_ACCT_CCY' => 'IDR',
				// 						'SOURCE_ACCT' => $rowAcct['ACCT'],
				// 						'SOURCE_ACCT_NAME' => $result['account_name'],
				// 						'SOURCE_ACCT_TYPE' => $result['account_type'],
				// 						'SOURCE_PRODUCT_TYPE' => $result['product_type'],
				// 						'TRA_CCY' => 'IDR',
				// 						'TRA_AMOUNT' => $rowAcct['AMOUNT'],
				// 						'REMARKS1' => 'Pelepasan MD BG NO.' . $rowAcct['BG_NUMBER'] . '',
				// 						'TRA_STATUS' => 1,
				// 						'BANK_RESPONSE' => $unlockSaving["response_code"] . " - " . $unlockSaving["response_desc"],
				// 						'UUID' => $unlockSaving["uuid"],
				// 						'TRA_TIME' => $traTime,
				// 						'UPDATE_APPROVED' => new Zend_Db_Expr('now()'),
				// 						'UPDATE_APPROVEDBY' => 'SYSTEM',
				// 						'RESPONSE_CODE' => $unlockSaving['response_code'],
				// 						'RESPONSE_DESC' => $unlockSaving['response_desc'],
				// 						'TRA_REF' => $unlockSaving['ref']
				// 					);

				// 					// $saveTransaction[] = [
				// 					// 	'tableName' => 'T_BG_TRANSACTION',
				// 					// 	'data' => $insertBGTransaction
				// 					// ];

				// 					insertTable('T_BG_TRANSACTION', $insertBGTransaction);

				// 					$historyServiceInsert = array(
				// 						'DATE_TIME'         => new Zend_Db_Expr("now()"),
				// 						'CLOSE_REF_NUMBER'        =>  $transactionID,
				// 						'BG_NUMBER'        => $row['BG_NUMBER'],
				// 						'CUST_ID'        => $row['CUST_ID'],
				// 						'USER_FROM'        => 'SYSTEM',
				// 						'USER_LOGIN'        => 'SYSTEM',
				// 						'BG_REASON'        => 'NoRef Penyelesaian : ' . $transactionID . '',
				// 						'HISTORY_STATUS'        => 12, //penutupan tanpa claim
				// 					);

				// 					// $saveTransaction[] = [
				// 					// 	'tableName' => 'T_BANK_GUARANTEE_HISTORY_CLOSE',
				// 					// 	'data' => $historyServiceInsert
				// 					// ];
				// 				} else if ($unlockSaving['response_code'] == "9999" || $unlockSaving['response_code'] == "0012") {
				// 					$statusService = $statusService ? false : $statusService;
				// 					$statusService = false;
				// 					$transactionID = generateTransactionID($generateCloseRefNumber, $no++);
				// 					$insertBGTransaction = array(
				// 						'CLOSE_REF_NUMBER' => $generateCloseRefNumber,
				// 						'TRANSACTION_ID' => $transactionID,
				// 						'BG_NUMBER' => $rowAcct['BG_NUMBER'],
				// 						'SERVICE' => 3,
				// 						'HOLD_SEQUENCE'        => $rowAcct['HOLD_SEQUENCE'],
				// 						'HOLD_BY_BRANCH'        => $rowAcct['HOLD_BY_BRANCH'],
				// 						'SOURCE_ACCT_CCY' => 'IDR',
				// 						'SOURCE_ACCT' => $rowAcct['ACCT'],
				// 						'SOURCE_ACCT_NAME' => $result['account_name'],
				// 						'SOURCE_ACCT_TYPE' => $result['account_type'],
				// 						'SOURCE_PRODUCT_TYPE' => $result['product_type'],
				// 						'TRA_CCY' => 'IDR',
				// 						'TRA_AMOUNT' => $rowAcct['AMOUNT'],
				// 						'REMARKS1' => 'Pelepasan MD BG NO.' . $rowAcct['BG_NUMBER'] . '',
				// 						'TRA_STATUS' => 3,
				// 						'BANK_RESPONSE' => $unlockSaving["response_code"] . " - " . $unlockSaving["response_desc"],
				// 						'UUID' => $unlockSaving["uuid"],
				// 						'TRA_TIME' => $traTime,
				// 						'RESPONSE_CODE' => $unlockSaving['response_code'],
				// 						'RESPONSE_DESC' => $unlockSaving['response_desc'],
				// 						'TRA_REF' => $unlockSaving['ref']
				// 					);

				// 					// $saveTransaction[] = [
				// 					// 	'tableName' => 'T_BG_TRANSACTION',
				// 					// 	'data' => $insertBGTransaction
				// 					// ];

				// 					insertTable('T_BG_TRANSACTION', $insertBGTransaction);

				// 					$historyServiceInsert = array(
				// 						'DATE_TIME'         => new Zend_Db_Expr("now()"),
				// 						'CLOSE_REF_NUMBER'        =>  $transactionID,
				// 						'BG_NUMBER'        => $row['BG_NUMBER'],
				// 						'CUST_ID'        => $row['CUST_ID'],
				// 						'USER_FROM'        => 'SYSTEM',
				// 						'USER_LOGIN'        => 'SYSTEM',
				// 						'BG_REASON'        => 'NoRef Penyelesaian : ' . $transactionID . '',
				// 						'HISTORY_STATUS'        => 12, //penutupan tanpa claim
				// 					);

				// 					// $saveTransaction[] = [
				// 					// 	'tableName' => 'T_BANK_GUARANTEE_HISTORY_CLOSE',
				// 					// 	'data' => $historyServiceInsert
				// 					// ];
				// 				} else {
				// 					$statusService = $statusService ? false : $statusService;

				// 					$transactionID = generateTransactionID($generateCloseRefNumber, $no++);
				// 					$insertBGTransaction = array(
				// 						'CLOSE_REF_NUMBER' => $generateCloseRefNumber,
				// 						'TRANSACTION_ID' => $transactionID,
				// 						'BG_NUMBER' => $rowAcct['BG_NUMBER'],
				// 						'SERVICE' => 3,
				// 						'HOLD_SEQUENCE'        => $rowAcct['HOLD_SEQUENCE'],
				// 						'HOLD_BY_BRANCH'        => $rowAcct['HOLD_BY_BRANCH'],
				// 						'SOURCE_ACCT_CCY' => 'IDR',
				// 						'SOURCE_ACCT' => $rowAcct['ACCT'],
				// 						'SOURCE_ACCT_NAME' => $result['account_name'],
				// 						'SOURCE_ACCT_TYPE' => $result['account_type'],
				// 						'SOURCE_PRODUCT_TYPE' => $result['product_type'],
				// 						'TRA_CCY' => 'IDR',
				// 						'TRA_AMOUNT' => $rowAcct['AMOUNT'],
				// 						'REMARKS1' => 'Pelepasan MD BG NO.' . $rowAcct['BG_NUMBER'] . '',
				// 						'TRA_STATUS' => 4,
				// 						'BANK_RESPONSE' => $unlockSaving["response_code"] . " - " . $unlockSaving["response_desc"],
				// 						'UUID' => $unlockSaving["uuid"],
				// 						'TRA_TIME' => $traTime,
				// 						'RESPONSE_CODE' => $unlockSaving['response_code'],
				// 						'RESPONSE_DESC' => $unlockSaving['response_desc'],
				// 						'TRA_REF' => $unlockSaving['ref']
				// 					);

				// 					// $saveTransaction[] = [
				// 					// 	'tableName' => 'T_BG_TRANSACTION',
				// 					// 	'data' => $insertBGTransaction
				// 					// ];

				// 					insertTable('T_BG_TRANSACTION', $insertBGTransaction);

				// 					$historyServiceInsert = array(
				// 						'DATE_TIME'         => new Zend_Db_Expr("now()"),
				// 						'CLOSE_REF_NUMBER'        =>  $transactionID,
				// 						'BG_NUMBER'        => $row['BG_NUMBER'],
				// 						'CUST_ID'        => $row['CUST_ID'],
				// 						'USER_FROM'        => 'SYSTEM',
				// 						'USER_LOGIN'        => 'SYSTEM',
				// 						'BG_REASON'        => 'NoRef Penyelesaian : ' . $transactionID . '',
				// 						'HISTORY_STATUS'        => 12, //penutupan tanpa claim
				// 					);

				// 					// $saveTransaction[] = [
				// 					// 	'tableName' => 'T_BANK_GUARANTEE_HISTORY_CLOSE',
				// 					// 	'data' => $historyServiceInsert
				// 					// ];
				// 				}
				// 			}

				// 			if ($result['response_code'] != '0000') {

				// 				$result2 = $svcAccount->inquiryDeposito();

				// 				$unlockDeposito = $svcAccount->unlockDeposito('T', $rowAcct['HOLD_BY_BRANCH'], $rowAcct['HOLD_SEQUENCE']);

				// 				$parseDateTime = DateTime::createFromFormat('Y-m-d\TH:i:s+', $unlockDeposito['datetime']);
				// 				$traTime = $parseDateTime->format('Y-m-d H:i:s');

				// 				if ($unlockDeposito['response_code'] == "0000") {

				// 					$transactionID = generateTransactionID($generateCloseRefNumber, $no++);
				// 					$insertBGTransaction = array(
				// 						'CLOSE_REF_NUMBER'         => $generateCloseRefNumber,
				// 						'TRANSACTION_ID'        => $transactionID,
				// 						'BG_NUMBER'        => $rowAcct['BG_NUMBER'],
				// 						'SERVICE'        => 2,
				// 						'HOLD_SEQUENCE'        => $rowAcct['HOLD_SEQUENCE'],
				// 						'HOLD_BY_BRANCH'        => $rowAcct['HOLD_BY_BRANCH'],
				// 						'SOURCE_ACCT_CCY'        => 'IDR',
				// 						'SOURCE_ACCT'        => $rowAcct['ACCT'],
				// 						'SOURCE_ACCT_NAME'        => $result2['name'],
				// 						'SOURCE_ACCT_TYPE'        => $result2['account_type'],
				// 						'SOURCE_PRODUCT_TYPE'        => $result2['product_type'],
				// 						'TRA_CCY'        => 'IDR',
				// 						'TRA_AMOUNT' => $rowAcct['AMOUNT'],
				// 						'REMARKS1'        => 'Pelepasan MD BG NO.' . $rowAcct['BG_NUMBER'] . '',
				// 						'TRA_STATUS' => 1,
				// 						'BANK_RESPONSE' => $unlockDeposito["response_code"] . " - " . $unlockDeposito["response_desc"],
				// 						'UUID' => $unlockDeposito["uuid"],
				// 						'TRA_TIME' => $traTime,
				// 						'UPDATE_APPROVED' => new Zend_Db_Expr('now()'),
				// 						'UPDATE_APPROVEDBY' => 'SYSTEM',
				// 						'RESPONSE_CODE' => $unlockDeposito['response_code'],
				// 						'RESPONSE_DESC' => $unlockDeposito['response_desc'],
				// 						'TRA_REF' => $unlockDeposito['ref']

				// 					);

				// 					// $saveTransaction[] = [
				// 					// 	'tableName' => 'T_BG_TRANSACTION',
				// 					// 	'data' => $insertBGTransaction
				// 					// ];

				// 					insertTable('T_BG_TRANSACTION', $insertBGTransaction);

				// 					$historyServiceInsert = array(
				// 						'DATE_TIME'         => new Zend_Db_Expr("now()"),
				// 						'CLOSE_REF_NUMBER'        =>  $transactionID,
				// 						'BG_NUMBER'        => $row['BG_NUMBER'],
				// 						'CUST_ID'        => $row['CUST_ID'],
				// 						'USER_FROM'        => 'SYSTEM',
				// 						'USER_LOGIN'        => 'SYSTEM',
				// 						'BG_REASON'        => 'NoRef Penyelesaian : ' . $transactionID . '',
				// 						'HISTORY_STATUS'        => 12, //penutupan tanpa claim
				// 					);

				// 					// $saveTransaction[] = [
				// 					// 	'tableName' => 'T_BANK_GUARANTEE_HISTORY_CLOSE',
				// 					// 	'data' => $historyServiceInsert
				// 					// ];
				// 				} else if ($unlockDeposito['response_code'] == "9999" || $unlockSaving['response_code'] == "0012") {
				// 					$statusService = $statusService ? false : $statusService;

				// 					$transactionID = generateTransactionID($generateCloseRefNumber, $no++);
				// 					$insertBGTransaction = array(
				// 						'CLOSE_REF_NUMBER'         => $generateCloseRefNumber,
				// 						'TRANSACTION_ID'        => $transactionID,
				// 						'BG_NUMBER'        => $rowAcct['BG_NUMBER'],
				// 						'SERVICE'        => 2,
				// 						'HOLD_SEQUENCE'        => $rowAcct['HOLD_SEQUENCE'],
				// 						'HOLD_BY_BRANCH'        => $rowAcct['HOLD_BY_BRANCH'],
				// 						'SOURCE_ACCT_CCY'        => 'IDR',
				// 						'SOURCE_ACCT'        => $rowAcct['ACCT'],
				// 						'SOURCE_ACCT_NAME'        => $result2['name'],
				// 						'SOURCE_ACCT_TYPE'        => $result2['account_type'],
				// 						'SOURCE_PRODUCT_TYPE'        => $result2['product_type'],
				// 						'TRA_CCY'        => 'IDR',
				// 						'TRA_AMOUNT' => $rowAcct['AMOUNT'],
				// 						'REMARKS1'        => 'Pelepasan MD BG NO.' . $rowAcct['BG_NUMBER'] . '',
				// 						'TRA_STATUS' => 3,
				// 						'BANK_RESPONSE' => $unlockDeposito["response_code"] . " - " . $unlockDeposito["response_desc"],
				// 						'UUID' => $unlockDeposito["uuid"],
				// 						'TRA_TIME' => $traTime,
				// 						'RESPONSE_CODE' => $unlockDeposito['response_code'],
				// 						'RESPONSE_DESC' => $unlockDeposito['response_desc'],
				// 						'TRA_REF' => $unlockDeposito['ref']
				// 					);

				// 					// $saveTransaction[] = [
				// 					// 	'tableName' => 'T_BG_TRANSACTION',
				// 					// 	'data' => $insertBGTransaction
				// 					// ];

				// 					insertTable('T_BG_TRANSACTION', $insertBGTransaction);

				// 					$historyServiceInsert = array(
				// 						'DATE_TIME'         => new Zend_Db_Expr("now()"),
				// 						'CLOSE_REF_NUMBER'        =>  $transactionID,
				// 						'BG_NUMBER'        => $row['BG_NUMBER'],
				// 						'CUST_ID'        => $row['CUST_ID'],
				// 						'USER_FROM'        => 'SYSTEM',
				// 						'USER_LOGIN'        => 'SYSTEM',
				// 						'BG_REASON'        => 'NoRef Penyelesaian : ' . $transactionID . '',
				// 						'HISTORY_STATUS'        => 12, //penutupan tanpa claim
				// 					);

				// 					// $saveTransaction[] = [
				// 					// 	'tableName' => 'T_BANK_GUARANTEE_HISTORY_CLOSE',
				// 					// 	'data' => $historyServiceInsert
				// 					// ];
				// 				} else {
				// 					$statusService = $statusService ? false : $statusService;

				// 					$transactionID = generateTransactionID($generateCloseRefNumber, $no++);
				// 					$insertBGTransaction = array(
				// 						'CLOSE_REF_NUMBER'         => $generateCloseRefNumber,
				// 						'TRANSACTION_ID'        => $transactionID,
				// 						'BG_NUMBER'        => $rowAcct['BG_NUMBER'],
				// 						'SERVICE'        => 2,
				// 						'HOLD_SEQUENCE'        => $rowAcct['HOLD_SEQUENCE'],
				// 						'HOLD_BY_BRANCH'        => $rowAcct['HOLD_BY_BRANCH'],
				// 						'SOURCE_ACCT_CCY'        => 'IDR',
				// 						'SOURCE_ACCT'        => $rowAcct['ACCT'],
				// 						'SOURCE_ACCT_NAME'        => $result2['name'],
				// 						'SOURCE_ACCT_TYPE'        => $result2['account_type'],
				// 						'SOURCE_PRODUCT_TYPE'        => $result2['product_type'],
				// 						'TRA_CCY'        => 'IDR',
				// 						'TRA_AMOUNT' => $rowAcct['AMOUNT'],
				// 						'REMARKS1'        => 'Pelepasan MD BG NO.' . $rowAcct['BG_NUMBER'] . '',
				// 						'TRA_STATUS' => 4,
				// 						'BANK_RESPONSE' => $unlockDeposito["response_code"] . " - " . $unlockDeposito["response_desc"],
				// 						'UUID' => $unlockDeposito["uuid"],
				// 						'TRA_TIME' => $traTime,
				// 						'RESPONSE_CODE' => $unlockDeposito['response_code'],
				// 						'RESPONSE_DESC' => $unlockDeposito['response_desc'],
				// 						'TRA_REF' => $unlockDeposito['ref']
				// 					);

				// 					// $saveTransaction[] = [
				// 					// 	'tableName' => 'T_BG_TRANSACTION',
				// 					// 	'data' => $insertBGTransaction
				// 					// ];

				// 					insertTable('T_BG_TRANSACTION', $insertBGTransaction);

				// 					$historyServiceInsert = array(
				// 						'DATE_TIME'         => new Zend_Db_Expr("now()"),
				// 						'CLOSE_REF_NUMBER'        =>  $transactionID,
				// 						'BG_NUMBER'        => $row['BG_NUMBER'],
				// 						'CUST_ID'        => $row['CUST_ID'],
				// 						'USER_FROM'        => 'SYSTEM',
				// 						'USER_LOGIN'        => 'SYSTEM',
				// 						'BG_REASON'        => 'NoRef Penyelesaian : ' . $transactionID . '',
				// 						'HISTORY_STATUS'        => 12, //penutupan tanpa claim
				// 					);

				// 					// $saveTransaction[] = [
				// 					// 	'tableName' => 'T_BANK_GUARANTEE_HISTORY_CLOSE',
				// 					// 	'data' => $historyServiceInsert
				// 					// ];
				// 				}
				// 			}
				// 		} else {
				// 			$haveEscrow = true;
				// 		}
				// 	}

				// 	if (!$haveEscrow) {
				// 		if ($statusService == true) {
				// 			$insertBGPslip = array(
				// 				'CLOSE_REF_NUMBER'         => $generateCloseRefNumber,
				// 				'BG_NUMBER'        => $row['BG_NUMBER'],
				// 				'PS_CONFIRMED'        => new Zend_Db_Expr("now()"),
				// 				'PS_CONFIRMEDBY'        => 'SYSTEM',
				// 				'PS_APPROVED'        => new Zend_Db_Expr("now()"),
				// 				'PS_APPROVEDBY'        => 'SYSTEM',
				// 				'PS_DONE' => new Zend_Db_Expr("now()"),
				// 				'PS_EFDATE'        => $row['BG_CLAIM_DATE'],
				// 				'PS_STATUS'        => '5', // Proses Selesai
				// 				'TYPE'        => '2', // Tanpa Claim
				// 			);

				// 			// $saveTransaction[] = [
				// 			// 	'tableName' => 'T_BG_PSLIP',
				// 			// 	'data' => $insertBGPslip
				// 			// ];

				// 			insertTable('T_BG_PSLIP', $insertBGPslip);
				// 		} else {
				// 			$insertBGPslip = array(
				// 				'CLOSE_REF_NUMBER'         => $generateCloseRefNumber,
				// 				'BG_NUMBER'        => $row['BG_NUMBER'],
				// 				'PS_CONFIRMED'        => new Zend_Db_Expr("now()"),
				// 				'PS_CONFIRMEDBY'        => 'SYSTEM',
				// 				'PS_APPROVED'        => new Zend_Db_Expr("now()"),
				// 				'PS_APPROVEDBY'        => 'SYSTEM',
				// 				'PS_EFDATE'        => $row['BG_CLAIM_DATE'],
				// 				'PS_STATUS'        => '4', //Service Dalam Proses
				// 				'TYPE'        => '2', // Tanpa Claim
				// 			);

				// 			// $saveTransaction[] = [
				// 			// 	'tableName' => 'T_BG_PSLIP',
				// 			// 	'data' => $insertBGPslip
				// 			// ];

				// 			insertTable('T_BG_PSLIP', $insertBGPslip);
				// 		}
				// 	}
				// } else {

				// 	if ($haveEscrow = false) {
				// 		// insert jika tidak ada jaminan escrow 
				// 		$insertBGPslip = array(
				// 			'CLOSE_REF_NUMBER' => $generateCloseRefNumber,
				// 			'BG_NUMBER' => $row['BG_NUMBER'],
				// 			'PS_CONFIRMED' => new Zend_Db_Expr("now()"),
				// 			'PS_CONFIRMEDBY' => 'SYSTEM',
				// 			'PS_APPROVED' => new Zend_Db_Expr("now()"),
				// 			'PS_APPROVEDBY' => 'SYSTEM',
				// 			'PS_DONE' => new Zend_Db_Expr("now()"),
				// 			'PS_EFDATE' => $row['BG_CLAIM_DATE'],
				// 			'PS_STATUS' => '5', // Proses Selesai
				// 			'TYPE' => '2', // Tanpa Claim
				// 		);

				// 		$saveTransaction[] = [
				// 			'tableName' => 'T_BG_PSLIP',
				// 			'data' => $insertBGPslip
				// 		];

				// 		insertTable('T_BG_PSLIP', $insertBGPslip);
				// 	}

				// 	// insert jika tidak ada jaminan 
				// 	$PSLIP = $db->select()
				// 		->from(array('A' => 'T_BG_PSLIP'), array('*'))
				// 		->where("A.CLOSE_REF_NUMBER = ?", $generateCloseRefNumber)
				// 		->query()->fetchAll();

				// 	if (empty($PSLIP)) {
				// 		$insertBGPslip = array(
				// 			'CLOSE_REF_NUMBER' => $generateCloseRefNumber,
				// 			'BG_NUMBER' => $row['BG_NUMBER'],
				// 			'PS_CONFIRMED' => new Zend_Db_Expr("now()"),
				// 			'PS_CONFIRMEDBY' => 'SYSTEM',
				// 			'PS_APPROVED' => new Zend_Db_Expr("now()"),
				// 			'PS_APPROVEDBY' => 'SYSTEM',
				// 			'PS_DONE' => new Zend_Db_Expr("now()"),
				// 			'PS_EFDATE' => $row['BG_CLAIM_DATE'],
				// 			'PS_STATUS' => '5', // Proses Selesai
				// 			'TYPE' => '2', // Tanpa Claim
				// 		);

				// 		// $saveTransaction[] = [
				// 		// 	'tableName' => 'T_BG_PSLIP',
				// 		// 	'data' => $insertBGPslip
				// 		// ];

				// 		insertTable('T_BG_PSLIP', $insertBGPslip);
				// 	}
				// }

				// get referensi data
				$getCustomerName = $db->select()
					->from('M_CUSTOMER')
					->where('CUST_ID = ?', $row["CUST_ID"])
					->query()->fetch();

				$bank_guarantee_detail = $db->select()
					->from("T_BANK_GUARANTEE_DETAIL")
					->where('BG_REG_NUMBER = ? ', $row["BG_REG_NUMBER"])
					->query()->fetchAll();
				$currencyBG = array_shift(array_filter($bank_guarantee_detail, function ($test) {
					return strtolower($test['PS_FIELDNAME']) == 'currency';
				}))['PS_FIELDVALUE'];

				$time1 = date_create($row['TIME_PERIOD_START']);
				$time2 = date_create($row['TIME_PERIOD_END']);

				$saveDiff = date_diff($time1, $time2);
				$selisih = intval($saveDiff->format('%a'));
				$totalHari = $selisih + 1;

				// BEGIN SEND EMAIL
				$Settings = new Settings();
				$templateEmailMasterBankAddress = $Settings->getSetting('master_bank_address');
				$templateEmailMasterBankAppName = $Settings->getSetting('master_bank_app_name');
				$templateEmailMasterBankAppUrl = $Settings->getSetting('master_bank_app_url');
				$templateEmailMasterBankCity = $Settings->getSetting('master_bank_city');
				$templateEmailMasterBankCountry = $Settings->getSetting('master_bank_country');
				$templateEmailMasterBankEmail = $Settings->getSetting('master_bank_email');
				$templateEmailMasterBankFax = $Settings->getSetting('master_bank_fax');
				$templateEmailMasterBankName = $Settings->getSetting('master_bank_name');
				$templateEmailMasterBankProvince = $Settings->getSetting('master_bank_province');
				$templateEmailMasterBankTelp = $Settings->getSetting('master_bank_telp');
				$templateEmailMasterBankWebsite = $Settings->getSetting('master_bank_website');
				$templateEmailMasterBankWapp = $Settings->getSetting('master_bank_wapp');
				$url_bo  = $Settings->getSetting('url_bo');
				//$template = $Settings->getSetting('femailtemplate_closebg');
				$email_grup_bts = $Settings->getSetting('email_grup_bts');
				$email_grup_cstd = $Settings->getSetting('email_grup_cstd');
				$email_grup_ficd = $Settings->getSetting('email_grup_ficd');

				/*$translate = array(
					'[[close_ref_number]]' => $generateCloseRefNumber,
					'[[bg_number]]' => $row['BG_NUMBER'],
					'[[bg_subject]]' => $row['BG_SUBJECT'],
					'[[bg_status]]' => 'Off Risk',
					'[[master_bank_name]]' 	=> $templateEmailMasterBankName,
					'[[master_bank_app_name]]' 	=> $templateEmailMasterBankAppName
				);*/

				// TEMP BANK GUARANTEE UNDERLYING
				$getAllUnderlying = $db->select()
					->from('TEMP_BANK_GUARANTEE_UNDERLYING')
					->where('BG_REG_NUMBER = ?', $row['BG_REG_NUMBER'])
					->query()->fetchAll();

				$underlyingText = implode(', ', array_column($getAllUnderlying, 'DOC_NAME'));

				$underlyingForEmail = '
				<div style="display:inline-block;max-width: 394px;overflow:hidden;text-overflow: ellipsis;white-space: nowrap;">' . $underlyingText . '</div>
					<br>
				<div style="display:inline-block; margin-top: 5px">(Total : ' . count($getAllUnderlying) . ' Dokumen)</div>
				';

				// LOAD CONFIG GLOBAL VAR
				$config = Configs_Loader::loadConfig();
				$config = $config->toArray();

				// JENIS KONTRA
				$bgcgType = $config["bgcg"]["type"]["desc"];
				$bgcgCode = $config["bgcg"]["type"]["code"];

				$arrbgcg = array_combine(array_values($bgcgCode), array_values($bgcgType));

				$jenisKontra = $arrbgcg[$row['COUNTER_WARRANTY_TYPE']];

				// BG STATUS
				$BgType = $config["bg"]["status"]["desc"];
				$BgCode = $config["bg"]["status"]["code"];

				$arrStatus = array_combine(array_values($BgCode), array_values($BgType));

				$bgStatus = $arrStatus['16'];

				// BG TYPE
				$bgType         = $config["bg"]["type"]["desc"];
				$bgCode         = $config["bg"]["type"]["code"];

				$arrbgType = array_combine(array_values($bgCode), array_values($bgType));

				//1. Principal (email kontak principal pada table T_BANK_GUARANTEE)
				if (!empty($data)) {

					$template = $Settings->getSetting('bemailtemplate_0A');

					$translate = array(
						'[[cust_cp]]' =>  $row["CUST_CP"],
						'[[cust_name]]' =>  $getCustomerName["CUST_NAME"],
						'[[bg_number]]' => $row['BG_NUMBER'],
						'[[usage_purpose]]' => $row['USAGE_PURPOSE_DESC'],
						'[[contract_name]]' => $underlyingForEmail,
						'[[recipient_name]]' => $row['RECIPIENT_NAME'],
						'[[bg_amount]]' => $currencyBG . ' ' . number_format($row["BG_AMOUNT"], 2),
						'[[counter_warranty_type]]' =>  $jenisKontra,
						'[[bg_status]]' =>  $bgStatus,
						'[[bg_offrisk_date]]' =>  date('d-m-Y'),
						'[[closing_type]]' => 'Penutupan Tanpa Klaim',
						'[[settlement_ref_number]]' => $generateCloseRefNumber,
						'[[master_app_name]]' => $templateEmailMasterBankAppName,
						'[[title_counter_type]]' => $row['COUNTER_WARRANTY_TYPE'] == 1 ? 'Jaminan' : 'MD',
						'[[master_bank_email]]' => $templateEmailMasterBankEmail,
						'[[master_bank_telp]]' => $templateEmailMasterBankTelp,
						'[[master_bank_name]]' 	=> $templateEmailMasterBankName,
						'[[time_period]]' =>  $totalHari . ' hari',
					);


					$mailContent = strtr($template, $translate);
					$subject = 'PEMBERITAHUAN BANK GARANSI BERAKHIR';

					$email_principal = $row['CUST_EMAIL'];
					Application_Helper_Email::sendEmail($email_principal, $subject, $mailContent);
				}

				//2. Obligee (email kontak obligee pada table T_BANK_GUARANTEE)
				if (!empty($data)) {

					$template = $Settings->getSetting('bemailtemplate_0B');

					$translate = array(
						'[[recipient_cp]]' => $row['RECIPIENT_CP'],
						'[[recipient_name]]' => $row['RECIPIENT_NAME'],
						'[[bg_number]]' => $row['BG_NUMBER'],
						'[[usage_purpose]]' => $row['USAGE_PURPOSE_DESC'],
						'[[contract_name]]' => $underlyingForEmail,
						'[[cust_name]]' =>  $getCustomerName["CUST_NAME"],
						'[[recipient_name]]' => $row['RECIPIENT_NAME'],
						'[[bg_amount]]' => $currencyBG . ' ' . number_format($row["BG_AMOUNT"], 2),
						'[[counter_warranty_type]]' =>  $jenisKontra,
						'[[bg_status]]' =>  $bgStatus,
						'[[bg_offrisk_date]]' =>  date('d-m-Y'),
						'[[closing_type]]' => 'Penutupan Tanpa Klaim',
						'[[settlement_ref_number]]' => $generateCloseRefNumber,
						'[[master_app_name]]' => $templateEmailMasterBankAppName,
						'[[title_counter_type]]' => $row['COUNTER_WARRANTY_TYPE'] == 1 ? 'Jaminan' : 'MD',
						'[[master_bank_email]]' => $templateEmailMasterBankEmail,
						'[[master_bank_telp]]' => $templateEmailMasterBankTelp,
						'[[master_bank_name]]' 	=> $templateEmailMasterBankName,
						'[[time_period]]' =>  $totalHari . ' hari',
					);


					$mailContent = strtr($template, $translate);
					$subject = 'PEMBERITAHUAN BANK GARANSI BERAKHIR';

					$email_principal = $row['CUST_EMAIL'];
					Application_Helper_Email::sendEmail($row['RECIPIENT_EMAIL'], $subject, $mailContent);

					// $email_obligee = $row['RECIPIENT_EMAIL'];
					// $mail = Application_Helper_Email::sendEmail($email_obligee, $subject, $mailContent);
				}

				//3. Kantor cabang penerbit (email cabang pada table M_BRANCH)
				if (!empty($data)) {

					$template = $Settings->getSetting('bemailtemplate_0C');

					$translate = array(
						'[[cust_name]]' =>  $getCustomerName["CUST_NAME"],
						'[[recipient_cp]]' => $row['RECIPIENT_CP'],
						'[[recipient_name]]' => $row['RECIPIENT_NAME'],
						'[[bg_number]]' => $row['BG_NUMBER'],
						'[[usage_purpose]]' => $row['USAGE_PURPOSE_DESC'],
						'[[contract_name]]' => $underlyingForEmail,
						'[[cust_name]]' =>  $getCustomerName["CUST_NAME"],
						'[[recipient_name]]' => $row['RECIPIENT_NAME'],
						'[[bg_amount]]' => $currencyBG . ' ' . number_format($row["BG_AMOUNT"], 2),
						'[[counter_warranty_type]]' =>  $jenisKontra,
						'[[bg_status]]' =>  $bgStatus,
						'[[bg_offrisk_date]]' =>  date('d-m-Y'),
						'[[closing_type]]' => 'Penutupan Tanpa Klaim',
						'[[settlement_ref_number]]' => $generateCloseRefNumber,
						'[[master_app_name]]' => $templateEmailMasterBankAppName,
						'[[title_counter_type]]' => $row['COUNTER_WARRANTY_TYPE'] == 1 ? 'Jaminan' : 'MD',
						'[[master_bank_email]]' => $templateEmailMasterBankEmail,
						'[[master_bank_telp]]' => $templateEmailMasterBankTelp,
						'[[master_bank_name]]' 	=> $templateEmailMasterBankName,
						'[[time_period]]' =>  $totalHari . ' hari',
					);


					$mailContent = strtr($template, $translate);
					$subject = 'PEMBERITAHUAN BANK GARANSI BERAKHIR';

					$selectBranch	= $db->select()
						->from(
							array('A'	 		=> 'M_BRANCH'),
							array('BRANCH_EMAIL' => 'A.BRANCH_EMAIL')
						)
						->where("A.BRANCH_CODE = ?", $row['BG_BRANCH']);
					$email_cabang = $db->fetchOne($selectBranch);
					if (!empty($email_cabang)) {
						Application_Helper_Email::sendEmail($email_cabang, $subject, $mailContent);
					}
				}

				// 4. Notifikasi email ke group cash collateral - Full cover - 1
				if ($row['COUNTER_WARRANTY_TYPE'] == '1') {

					$template = $Settings->getSetting('bemailtemplate_0C');

					$translate = array(
						'[[cust_name]]' =>  $getCustomerName["CUST_NAME"],
						'[[recipient_cp]]' => $row['RECIPIENT_CP'],
						'[[recipient_name]]' => $row['RECIPIENT_NAME'],
						'[[bg_number]]' => $row['BG_NUMBER'],
						'[[usage_purpose]]' => $row['USAGE_PURPOSE_DESC'],
						'[[contract_name]]' => $underlyingForEmail,
						'[[cust_name]]' =>  $getCustomerName["CUST_NAME"],
						'[[recipient_name]]' => $row['RECIPIENT_NAME'],
						'[[bg_amount]]' => $currencyBG . ' ' . number_format($row["BG_AMOUNT"], 2),
						'[[counter_warranty_type]]' =>  $jenisKontra,
						'[[bg_status]]' =>  $bgStatus,
						'[[bg_offrisk_date]]' =>  date('d-m-Y'),
						'[[closing_type]]' => 'Penutupan Tanpa Klaim',
						'[[settlement_ref_number]]' => $generateCloseRefNumber,
						'[[master_app_name]]' => $templateEmailMasterBankAppName,
						'[[title_counter_type]]' => $row['COUNTER_WARRANTY_TYPE'] == 1 ? 'Jaminan' : 'MD',
						'[[master_bank_email]]' => $templateEmailMasterBankEmail,
						'[[master_bank_telp]]' => $templateEmailMasterBankTelp,
						'[[master_bank_name]]' 	=> $templateEmailMasterBankName,
						'[[time_period]]' =>  $totalHari . ' hari',
					);


					$mailContent = strtr($template, $translate);
					$subject = 'PEMBERITAHUAN BANK GARANSI BERAKHIR';

					$email_group_cashcoll = $Settings->getSettingNew('email_group_cashcoll');
					$email_group_cashcoll = array_map('trim', explode(';', $email_group_cashcoll));

					if (!empty($data)) {
						foreach ($email_group_cashcoll as $emailTemp) {
							Application_Helper_Email::sendEmail($emailTemp, $subject, $mailContent);
						}
					}
				}

				// 5. Notifikasi email ke group non cash collateral - Line facility - 2
				if ($row['COUNTER_WARRANTY_TYPE'] == '2') {

					$template = $Settings->getSetting('bemailtemplate_0C');

					$translate = array(
						'[[cust_name]]' =>  $getCustomerName["CUST_NAME"],
						'[[recipient_cp]]' => $row['RECIPIENT_CP'],
						'[[recipient_name]]' => $row['RECIPIENT_NAME'],
						'[[bg_number]]' => $row['BG_NUMBER'],
						'[[usage_purpose]]' => $row['USAGE_PURPOSE_DESC'],
						'[[contract_name]]' => $underlyingForEmail,
						'[[cust_name]]' =>  $getCustomerName["CUST_NAME"],
						'[[recipient_name]]' => $row['RECIPIENT_NAME'],
						'[[bg_amount]]' => $currencyBG . ' ' . number_format($row["BG_AMOUNT"], 2),
						'[[counter_warranty_type]]' =>  $jenisKontra,
						'[[bg_status]]' =>  $bgStatus,
						'[[bg_offrisk_date]]' =>  date('d-m-Y'),
						'[[closing_type]]' => 'Penutupan Tanpa Klaim',
						'[[settlement_ref_number]]' => $generateCloseRefNumber,
						'[[master_app_name]]' => $templateEmailMasterBankAppName,
						'[[title_counter_type]]' => $row['COUNTER_WARRANTY_TYPE'] == 1 ? 'Jaminan' : 'MD',
						'[[master_bank_email]]' => $templateEmailMasterBankEmail,
						'[[master_bank_telp]]' => $templateEmailMasterBankTelp,
						'[[master_bank_name]]' 	=> $templateEmailMasterBankName,
						'[[time_period]]' =>  $totalHari . ' hari',
					);


					$mailContent = strtr($template, $translate);
					$subject = 'PEMBERITAHUAN BANK GARANSI BERAKHIR';

					$email_group_linefacility = $Settings->getSettingNew('email_group_linefacility');
					$email_group_linefacility = array_map('trim', explode(';', $email_group_linefacility));

					if (!empty($data)) {
						foreach ($email_group_linefacility as $emailTemp) {
							Application_Helper_Email::sendEmail($emailTemp, $subject, $mailContent);
						}
					}
				}

				// 6. Notifikasi email ke group non cash collateral - Asuransi - 3
				if ($row['COUNTER_WARRANTY_TYPE'] == '3') {

					$template = $Settings->getSetting('bemailtemplate_0C');

					$translate = array(
						'[[cust_name]]' =>  $getCustomerName["CUST_NAME"],
						'[[recipient_cp]]' => $row['RECIPIENT_CP'],
						'[[recipient_name]]' => $row['RECIPIENT_NAME'],
						'[[bg_number]]' => $row['BG_NUMBER'],
						'[[usage_purpose]]' => $row['USAGE_PURPOSE_DESC'],
						'[[contract_name]]' => $underlyingForEmail,
						'[[cust_name]]' =>  $getCustomerName["CUST_NAME"],
						'[[recipient_name]]' => $row['RECIPIENT_NAME'],
						'[[bg_amount]]' => $currencyBG . ' ' . number_format($row["BG_AMOUNT"], 2),
						'[[counter_warranty_type]]' =>  $jenisKontra,
						'[[bg_status]]' =>  $bgStatus,
						'[[bg_offrisk_date]]' =>  date('d-m-Y'),
						'[[closing_type]]' => 'Penutupan Tanpa Klaim',
						'[[settlement_ref_number]]' => $generateCloseRefNumber,
						'[[master_app_name]]' => $templateEmailMasterBankAppName,
						'[[title_counter_type]]' => $row['COUNTER_WARRANTY_TYPE'] == 1 ? 'Jaminan' : 'MD',
						'[[master_bank_email]]' => $templateEmailMasterBankEmail,
						'[[master_bank_telp]]' => $templateEmailMasterBankTelp,
						'[[master_bank_name]]' 	=> $templateEmailMasterBankName,
						'[[time_period]]' =>  $totalHari . ' hari',
					);


					$mailContent = strtr($template, $translate);
					$subject = 'PEMBERITAHUAN BANK GARANSI BERAKHIR';

					$email_group_insurance = $Settings->getSetting('email_group_insurance');
					$email_group_insurance = array_map('trim', explode(';', $email_group_insurance));

					if (!empty($data)) {
						foreach ($email_group_insurance as $emailTemp) {
							Application_Helper_Email::sendEmail($emailTemp, $subject, $mailContent);
						}
					}
				}

				// 7. Notifikasi email ke kantor cabang asuransi
				if ($row['COUNTER_WARRANTY_TYPE'] == '3') {
					if (!empty($data)) {
						foreach ($bank_guarantee_detail as $key => $col) {
							if ($col['PS_FIELDNAME'] == 'Insurance Branch') {
								$insurance_branch = $db->select()
									->from(['MINS' => "M_INS_BRANCH"])
									->joinLeft(['MCUST' => 'M_CUSTOMER'], 'MINS.CUST_ID = MCUST.CUST_ID', ['INSURANCE_NAME' => 'MCUST.CUST_NAME'])
									->where('INS_BRANCH_CODE = ? ', $col['PS_FIELDVALUE'])
									->query()->fetch();

								$template = $Settings->getSetting('bemailtemplate_0D');

								$translate = array(
									'[[cust_name]]' =>  $getCustomerName["CUST_NAME"],
									'[[insurance_name]]' => $insurance_branch['INSURANCE_NAME'],
									'[[insurance_branch_name]]' => $insurance_branch['INS_BRANCH_NAME'],
									'[[recipient_cp]]' => $row['RECIPIENT_CP'],
									'[[recipient_name]]' => $row['RECIPIENT_NAME'],
									'[[bg_number]]' => $row['BG_NUMBER'],
									'[[usage_purpose]]' => $row['USAGE_PURPOSE_DESC'],
									'[[contract_name]]' => $underlyingForEmail,
									'[[cust_name]]' =>  $getCustomerName["CUST_NAME"],
									'[[recipient_name]]' => $row['RECIPIENT_NAME'],
									'[[bg_amount]]' => $currencyBG . ' ' . number_format($row["BG_AMOUNT"], 2),
									'[[counter_warranty_type]]' =>  $jenisKontra,
									'[[bg_status]]' =>  $bgStatus,
									'[[bg_offrisk_date]]' =>  date('d-m-Y'),
									'[[closing_type]]' => 'Penutupan Tanpa Klaim',
									'[[settlement_ref_number]]' => $generateCloseRefNumber,
									'[[master_app_name]]' => $templateEmailMasterBankAppName,
									'[[title_counter_type]]' => $row['COUNTER_WARRANTY_TYPE'] == 1 ? 'Jaminan' : 'MD',
									'[[master_bank_email]]' => $templateEmailMasterBankEmail,
									'[[master_bank_telp]]' => $templateEmailMasterBankTelp,
									'[[master_bank_name]]' 	=> $templateEmailMasterBankName,
									'[[time_period]]' =>  $totalHari . ' hari',
								);


								$mailContent = strtr($template, $translate);
								$subject = 'PEMBERITAHUAN BANK GARANSI BERAKHIR';

								Application_Helper_Email::sendEmail($insurance_branch['INS_BRANCH_EMAIL'], $subject, $mailContent);
							}
						}
					}
				}

				// 8. Notifikasi ke principal - penutupan tanpa klaim
				if ($row['COUNTER_WARRANTY_TYPE'] == '1' || $row['COUNTER_WARRANTY_TYPE'] == '3' && false) {

					// CHECK APAKAH TERDAPAT ESCROW ATAU TIDAK
					$existEscrow = $db->select()
						->from(['TBGS' => 'T_BANK_GUARANTEE_SPLIT'])
						->where("TBGS.BG_NUMBER = ?", $row['BG_NUMBER'])
						->where('LOWER(TBGS.ACCT_DESC) = ?', 'escrow')
						->query()->fetch();

					if (!$existEscrow) {

						$template = $Settings->getSetting('bemailtemplate_2A');

						$translate = array(
							'[[cust_name]]' =>  $getCustomerName["CUST_NAME"],
							'[[cust_cp]]' =>  $row["CUST_CP"],
							'[[recipient_cp]]' => $row['RECIPIENT_CP'],
							'[[ps_status]]' => 'Proses Selesai',
							'[[ps_done]]' =>  date('d-m-Y'),
							'[[recipient_name]]' => $row['RECIPIENT_NAME'],
							'[[bg_number]]' => $row['BG_NUMBER'],
							'[[usage_purpose]]' => $row['USAGE_PURPOSE_DESC'],
							'[[contract_name]]' => $underlyingForEmail,
							'[[cust_name]]' =>  $getCustomerName["CUST_NAME"],
							'[[recipient_name]]' => $row['RECIPIENT_NAME'],
							'[[bg_amount]]' => $currencyBG . ' ' . number_format($row["BG_AMOUNT"], 2),
							'[[counter_warranty_type]]' =>  $jenisKontra,
							'[[bg_status]]' =>  $bgStatus,
							'[[bg_offrisk_date]]' =>  date('d-m-Y'),
							'[[closing_type]]' => 'Penutupan Tanpa Klaim',
							'[[settlement_ref_number]]' => $generateCloseRefNumber,
							'[[master_app_name]]' => $templateEmailMasterBankAppName,
							'[[title_counter_type]]' => $row['COUNTER_WARRANTY_TYPE'] == 1 ? 'Jaminan' : 'MD',
							'[[master_bank_email]]' => $templateEmailMasterBankEmail,
							'[[master_bank_telp]]' => $templateEmailMasterBankTelp,
							'[[master_bank_name]]' 	=> $templateEmailMasterBankName,
							'[[time_period]]' =>  $totalHari . ' hari',
						);


						$mailContent = strtr($template, $translate);
						$subject = 'PEMBERITAHUAN BANK GARANSI BERAKHIR';

						// Application_Helper_Email::sendEmail($row['CUST_EMAIL'], $subject, $mailContent);
					}
				}

				// //4. Bila kontra Line Facility, email ke BTS Grup dan CSTD Grup (bts_email dan cstd_email pada table M_SETTING)
				// if ($row['COUNTER_WARRANTY_TYPE'] == '2') {
				// 	if (!empty($data)) {
				// 		$mail = Application_Helper_Email::sendEmail($email_grup_cstd, $subject, $mailContent);
				// 	}
				// }

				// //5. Bila kontra Asuransi, email ke BTS Grup dan FICD Grup (bts_email dan ficd_email pada table M_SETTING)
				// if ($row['COUNTER_WARRANTY_TYPE'] == '3') {
				// 	if (!empty($data)) {
				// 		$mail = Application_Helper_Email::sendEmail($email_grup_ficd, $subject, $mailContent);
				// 	}
				// }

				if (!empty($data)) {
					$mail = Application_Helper_Email::sendEmail($email_grup_bts, $subject, $mailContent);
				}
				// END SEND EMAIL


				$x = 1;
				//$db->commit();		   
			} catch (Exception $e) {
				$message = $e->getMessage();

				$today = date("Ymd");
				$nametext = $today . '.txt';
				$path_file = LIBRARY_PATH . '/data/logs/scheduler/';

				$dirname = dirname($path_file . $nametext);

				if (!is_dir($dirname)) {
					mkdir($dirname, 0755, true);
				}

				$fp = fopen($path_file . $nametext, 'a'); //opens file in append mode
				fwrite($fp, $message);
				fwrite($fp, PHP_EOL);
				fclose($fp);

				shell_exec('chown nginx ' . $path_file);
				shell_exec('chown nginx ' . $path_file . $nametext);
				// $db->rollBack();
			}
		}

		// foreach ($saveTransaction as $key => $value) {
		// 	insertTable($value['tableName'], $value['data']);
		// }
	}

	// simpan log cron
	echo 'AFFECTED ROW(S): BG_CLOSING (' . $x . ')' . PHP_EOL;
	Application_Helper_General::cronLog(basename(__FILE__), 'BG_CLOSING', '1');
}

function insertTable($tableName, $data)
{
	$db = Zend_Db_Table::getDefaultAdapter();

	try {
		//code...
		$db->insert($tableName, $data);
		// $db->beginTransaction();
		// $db->commit();
	} catch (\Throwable $th) {
		$message = $th->getMessage();

		$today = date("Ymd");
		$nametext = $today . '.txt';
		$path_file = LIBRARY_PATH . '/data/logs/scheduler/';

		$dirname = dirname($path_file . $nametext);

		if (!is_dir($dirname)) {
			mkdir($dirname, 0755, true);
		}

		$fp = fopen($path_file . $nametext, 'a'); //opens file in append mode
		fwrite($fp, $message);
		fwrite($fp, PHP_EOL);
		fclose($fp);

		shell_exec('chown nginx ' . $path_file);
		shell_exec('chown nginx ' . $path_file . $nametext);
	}
}

function updateTable($tableName, $data, $where = [])
{
	$db = Zend_Db_Table::getDefaultAdapter();

	try {
		//code...
		$db->update($tableName, $data, $where);
		// $db->beginTransaction();
		// $db->commit();
	} catch (\Throwable $th) {
		$message = $th->getMessage();

		$today = date("Ymd");
		$nametext = $today . '.txt';
		$path_file = LIBRARY_PATH . '/data/logs/scheduler/';

		$dirname = dirname($path_file . $nametext);

		if (!is_dir($dirname)) {
			mkdir($dirname, 0755, true);
		}

		$fp = fopen($path_file . $nametext, 'a'); //opens file in append mode
		fwrite($fp, $message);
		fwrite($fp, PHP_EOL);
		fclose($fp);

		shell_exec('chown nginx ' . $path_file);
		shell_exec('chown nginx ' . $path_file . $nametext);
	}
}

// insertTable('DUMP_TEST', ['DUMP_TEXT' => uniqid('inserttable')]);
runProg();
